trigger UtilizationSnapshotTrigger on Utilization_Snapshot__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(Utilization_Snapshot__c.sObjectType); 
}