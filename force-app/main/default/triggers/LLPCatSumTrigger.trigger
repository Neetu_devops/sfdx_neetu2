trigger LLPCatSumTrigger on LLP_Catalog_Summary__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(LLP_Catalog_Summary__c.sObjectType);    
}