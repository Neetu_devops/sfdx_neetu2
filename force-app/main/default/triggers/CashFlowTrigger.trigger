trigger CashFlowTrigger on Scenario_Input__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    if( LeaseWareUtils.isTriggerDisabled())return;
    TriggerFactory.createHandler(Scenario_Input__c.sObjectType);

}