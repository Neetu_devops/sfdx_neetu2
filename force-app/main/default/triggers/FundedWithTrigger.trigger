trigger FundedWithTrigger on Funded_WIth__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Funded_WIth__c.sObjectType);

}