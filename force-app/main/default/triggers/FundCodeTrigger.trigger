trigger FundCodeTrigger on Fund_Code__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Fund_Code__c.sObjectType);
}