trigger AssemblyUtilizationSnapshotTrigger on Assembly_Utilization_Snapshot_New__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(Assembly_Utilization_Snapshot_New__c.sObjectType); 
}