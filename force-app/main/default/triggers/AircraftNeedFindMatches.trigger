trigger AircraftNeedFindMatches on Aircraft_Need__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    
    if(LeaseWareUtils.isTriggerDisabled())return;

    if(LeaseWareUtils.isFromFuture())return;
    
    TriggerFactory.createHandler(Aircraft_Need__c.sObjectType);

}