trigger CreditMemoTrigger on Credit_memo__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(credit_memo__c.sObjectType);
}