trigger AssemblyEventInfoTrigger on Assembly_Event_Info__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Assembly_Event_Info__c.sObjectType);          
}