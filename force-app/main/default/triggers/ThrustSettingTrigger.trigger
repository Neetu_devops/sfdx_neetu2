trigger ThrustSettingTrigger on Thrust_Setting__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
   
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Thrust_Setting__c.sObjectType);   
    
}