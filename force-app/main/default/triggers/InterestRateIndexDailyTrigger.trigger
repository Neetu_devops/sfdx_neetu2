trigger InterestRateIndexDailyTrigger on Interest_Rate_Index_Daily__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Interest_Rate_Index_Daily__c.sObjectType);
}