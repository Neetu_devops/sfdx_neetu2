trigger PaymentLineItemTrigger on Payment_Line_Item__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Payment_Line_Item__c.sObjectType);
    
}