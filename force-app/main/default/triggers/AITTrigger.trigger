trigger AITTrigger on Aircraft_In_Transaction__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Aircraft_In_Transaction__c.sObjectType);
    
    
}