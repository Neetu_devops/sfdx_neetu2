trigger Deal_Proposal_OneOpOneProposal on Deal_Proposal__c (after undelete, before insert, before update) {

/* - 20140509 - Meeting note 05/02/2014 "Change validation of deal proposal so that the combination of aircraft and operator is checked - not just the operator"  

	set<Id> setDealIds = new set<Id>();

	for(Deal_Proposal__c curDealP: trigger.new){
		setDealIds.add(curDealP.Deal__c);
	}

	Deal_Proposal__c[] existingDealPropOps=[select Id, Prospect__c, Deal__c, Prospect__r.Name from Deal_Proposal__c where Deal__c in :setDealIds];
	if(existingDealPropOps.size()<1)return;
	
	for(Deal_Proposal__c curDealP: trigger.new){
		if(trigger.isUpdate && curDealP.Prospect__c == trigger.oldMap.get(curDealP.Id).Prospect__c)continue;
		for(Deal_Proposal__c chkDealP: existingDealPropOps){
			if(chkDealP.Deal__c==curDealP.Deal__c && chkDealP.Prospect__c == curDealP.Prospect__c){
				curDealP.Prospect__c.addError( chkDealP.Prospect__r.Name + ' already has a proposal under the same campaign.');
			}
		}
	}
*/

}