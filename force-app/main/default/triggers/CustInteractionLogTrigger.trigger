trigger CustInteractionLogTrigger on Customer_Interaction_Log__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Customer_Interaction_Log__c.sObjectType);

}