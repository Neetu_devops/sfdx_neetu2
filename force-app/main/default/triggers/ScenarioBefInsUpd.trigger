trigger ScenarioBefInsUpd on Scenario__c (before insert, before update) {
/*    
    if(LeaseWareUtils.isTriggerDisabled())return;


    private static integer MAX_STEPS=10;
    
    set<id> SetAcIds = new set<id>();
    for ( Scenario__c curScenario : Trigger.new){

        curScenario.Y_Hidden_Months_Till_Lease_End__c=curScenario.Custom_Months__c;

        SetAcIds.add(curScenario.Aircraft__c);
    }

    list<Lease__c> RelatedLeases = [select Id, Aircraft__c, Aircraft__r.Cash_Maintenance_Reserve_TOTAL__c, Rent__c, 
            Latest_Period__c, Lease_End_Date_New__c
        from Lease__c 
        where Aircraft__c in :SetAcIds];

    map<id, Lease__c> mapLeases = new map<id, Lease__c>(); 
    for(Lease__c curLease: RelatedLeases){
        mapLeases.put(curLease.Aircraft__c, curLease);
    }

    set<string> setEngTypesWithIntRecs = new set<string>(); 
    list<AggregateResult> listMTBRDetails = [select Engine_Type_Variant__c EngType from MTBR_Interpolation_Table__c group by Engine_Type_Variant__c];
    for(AggregateResult curMTBRDet : listMTBRDetails){
        setEngTypesWithIntRecs.add((string)curMTBRDet.get('EngType'));
    }
    
    list<AggregateResult> listEngTypes = [select Attached_Aircraft__c ACId, Engine_Model__c EngType, Engine_Thrust__c EngVariant 
                from Constituent_Assembly__c
                where Attached_Aircraft__c in :SetAcIds group by Attached_Aircraft__c, Engine_Model__c, Engine_Thrust__c];
    
    map<id, string> mapEngineTypeVariants = new map<id, string>(); 
    for(AggregateResult curEngType : listEngTypes){
        string strCurEngTypeVar;
        strCurEngTypeVar = (curEngType.get('EngType')==null?'<None>':(string)curEngType.get('EngType')) 
                    + (curEngType.get('EngVariant')==null?'':('-'+(string)curEngType.get('EngVariant')));
        mapEngineTypeVariants.put((Id)curEngType.get('ACId'), strCurEngTypeVar);
    }


    for ( Scenario__c curScenario : Trigger.new){

        integer cnt;
        if(curScenario.Max__c != null && curScenario.Min__c != null && curScenario.Step__c != null && curScenario.Step__c != 0){
            cnt=((curScenario.Max__c-curScenario.Min__c)/curScenario.Step__c).intValue();
            if(cnt>MAX_STEPS){
                curScenario.Step__c.addError('Please increase the step value such that total number of steps is not more than ' + MAX_STEPS 
                + '.\n Suggested value is '+ ((curScenario.Max__c-curScenario.Min__c)/MAX_STEPS).longValue().format());
                continue;
            }
        }
        string strCurEng = mapEngineTypeVariants.get(curScenario.Aircraft__c);
        if(curScenario.Use_Interpolation_For_Engine_Removal__c==true){//If the checkbox is checked, interpolation table must exist for the engine type.
            if(!setEngTypesWithIntRecs.contains(strCurEng)){
                curScenario.Use_Interpolation_For_Engine_Removal__c.addError('There are no interpolation records for Engine Type ' 
                        + strCurEng + '. Please enter interpolation records or unchek this.');
                continue;
            }
        }

            // For past Rent, always consider what is there on Lease
            Lease__c curLease = mapLeases.get(curScenario.Aircraft__c);
            if(curLease!=null){
    
    
            //Find earliest month ending based on the latest UR, not based on today(). - Haseem-Bobby email 130722
            Date EarliestMonthEnding;
            DateTime curForecastMonth;
            try{
                Utilization_Report__c lastUR = [select Month_Ending__c from Utilization_Report__c 
                    where Aircraft__c = :curScenario.Aircraft__c and (Status__c = :'Approved By Lessor' OR Legacy_Data__c= :true ) 
                    and True_Up__c != :true order by Month_Ending__c desc limit 1];
                    EarliestMonthEnding=lastUR.Month_Ending__c.addDays(1).addMonths(-(Integer)(curScenario.Number_of_mos_of_historical_data_to_use__c));
                    curForecastMonth = lastUR.Month_Ending__c.toStartOfMonth();
            }catch(exception e){
                system.debug('Exception '+ e + ' while trying to find the latest UR. Looking for historical records from today.');
                EarliestMonthEnding = Date.today().addMonths(-(Integer)(curScenario.Number_of_mos_of_historical_data_to_use__c));
                curForecastMonth = Date.Today().toStartOfMonth();
            }
    
            Date ForecastAsOf=curForecastMonth.dateGMT();
            Integer nMonths = (Integer)curScenario.Custom_Months__c;
            if(nMonths>ForecastAsOf.MonthsBetween(curLease.Lease_End_Date_New__c)){
                nMonths=ForecastAsOf.MonthsBetween(curLease.Lease_End_Date_New__c);
                curScenario.Y_Hidden_Months_Till_Lease_End__c=nMonths;
            }

            curScenario.Y_Hidden_Rent_So_Far__c = LeaseWareUtils.zeroIfNull(curLease.Latest_Period__c) * LeaseWareUtils.zeroIfNull(curLease.Rent__c);
            //Simple interest
            curScenario.Y_Hidden_Interest_So_Far__c = LeaseWareUtils.zeroIfNull(curLease.Latest_Period__c)/12 * LeaseWareUtils.zeroIfNull(curScenario.Interest_Rate__c)/100 * curScenario.Equity_Amount__c;
            boolean bCashProjection = 'Cash Maintenance Reserve'.equals(curScenario.Type_Of_Projection__c);
            boolean bSweepEquity = curScenario.Sweep_MR_Into_Equity__c;
            curScenario.MR_Sweep_Into_Equity__c=(bCashProjection && bSweepEquity && curLease.Aircraft__c != null)?LeaseWareUtils.zeroIfNull(curLease.Aircraft__r.Cash_Maintenance_Reserve_TOTAL__c):0;
        }
                
        // Residual_Value__c : Avg calculation
        double AvgResidualValue =0;
        cnt=0; 
    
        if(curScenario.Residual_Value_Ascend__c!=null){
            AvgResidualValue+=curScenario.Residual_Value_Ascend__c;
            cnt++;
        }
        if(curScenario.Residual_Value_Avitas__c!=null){
            AvgResidualValue+=curScenario.Residual_Value_Avitas__c;
            cnt++;
        }
        if(curScenario.Residual_Value_IBA__c!=null){
            AvgResidualValue+=curScenario.Residual_Value_IBA__c;
            cnt++;
        }
        if(curScenario.Residual_Value_Other__c!=null){
            AvgResidualValue+=curScenario.Residual_Value_Other__c;
            cnt++;
        }
        //Anjani : If any residual value is present ,then calculate avg else user passed value .            
        if(cnt>0){
            AvgResidualValue/=cnt;
            curScenario.Residual_Value__c=AvgResidualValue;
        }
        else if( trigger.IsUpdate && 
                (trigger.oldMap.get(curScenario.Id).Residual_Value_Ascend__c != null 
                || trigger.oldMap.get(curScenario.Id).Residual_Value_Avitas__c != null 
                || trigger.oldMap.get(curScenario.Id).Residual_Value_IBA__c != null
                || trigger.oldMap.get(curScenario.Id).Residual_Value_Other__c != null
                )
                && trigger.oldMap.get(curScenario.Id).Residual_Value__c == curScenario.Residual_Value__c){
                    
            curScenario.Residual_Value__c = null;
        }
   
        // Average_CMV__c : Avg calculation    
        double AvgMarketValue=0;
        cnt=0;
        
        if(curScenario.Current_Market_Value_Ascend__c!=null){
            AvgMarketValue+=curScenario.Current_Market_Value_Ascend__c;
            cnt++;
        }
        if(curScenario.Current_Market_Value_Avitas__c!=null){
            AvgMarketValue+=curScenario.Current_Market_Value_Avitas__c;
            cnt++;
        }
        if(curScenario.Current_Market_Value_IBA__c!=null){
            AvgMarketValue+=curScenario.Current_Market_Value_IBA__c;
            cnt++;
        }
        if(curScenario.Current_Market_Value_Other__c!=null){
            AvgMarketValue+=curScenario.Current_Market_Value_Other__c;
            cnt++;
        }
        if(cnt>0)AvgMarketValue/=cnt;
        curScenario.Average_CMV__c=AvgMarketValue;
        
    }
*/
}