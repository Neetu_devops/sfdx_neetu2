trigger LeaseSetAircraftForEngineLease on Lease__c (before insert, before update) {

 /*   
    if(LeaseWareUtils.isTriggerDisabled())return;


    set<ID> setRTIds = new set<ID>();
    map<Id, RecordType> mapRTs = new map<Id, RecordType>();
    set<ID> setEngineIds = new set<ID>();
    set<ID> setOpIds = new set<ID>();
    map<Id, Constituent_Assembly__c> mapAllEngines = new map<Id, Constituent_Assembly__c>();
    map<Id, Operator__c> mapAllOps = new map<Id, Operator__c>();

    Lease__c oldLeaseRec;  
    for(Lease__c thisLease:trigger.new){
    
        //Bobby20170103 - Moving the below two validation to trigger.
        boolean bErr=false;
        if( thisLease.Rent_Due_Day__c==null || thisLease.Rent_Due_Day__c<1 || thisLease.Rent_Due_Day__c>28 ){
            thisLease.Rent_Due_Day__c.addError('Please enter a day between 1 and 28.');
            bErr=true;
        }
        if(thisLease.Overdue_Check_Days__c==null || thisLease.Overdue_Check_Days__c<1 || thisLease.Overdue_Check_Days__c>28){
            thisLease.Overdue_Check_Days__c.addError('Rent overdue check cannot span over a month. Please enter a value between 1 and 28.');
            bErr=true;
        }
        if(bErr)continue;
    
        setRTIds.add(thisLease.RecordTypeId);
        if(thisLease.Lease_End_Date_New__c!= null && thisLease.Deposit_Received_Date__c!= null && thisLease.Lease_End_Date_New__c>=thisLease.Deposit_Received_Date__c && thisLease.Deposit_Return_Date__c==null ){
            thisLease.Deposit_Return_Date__c =thisLease.Lease_End_Date_New__c;
        }



        // calculate gross Contract
        Decimal tempRentAmount=0;
        Date tempDate;        
        if(thisLease.Base_Rent__c!= null && thisLease.Rent_Date__c!=null && thisLease.Rent_End_Date__c!=null){
            if(thisLease.Escalation_Month__c==null || leasewareUtils.ZeroIfNull(thisLease.Escalation__c) == 0){ //There is a validation to ensure % is entered if Month is entered, however, we can skip the calc if Esc % is zero.
            	if('Daily'.equals(thisLease.Rent_Period__c)){
            		tempRentAmount = thisLease.Base_Rent__c * thisLease.Rent_Date__c.daysBetween(thisLease.Rent_End_Date__c);
            	}else{
	                tempDate = thisLease.Rent_Date__c.toStartOfMonth().addMonths(1).addDays(-1);
	                if(thisLease.Rent_Date__c != thisLease.Rent_Date__c.toStartOfMonth()) tempRentAmount= thisLease.Base_Rent__c * (thisLease.Rent_Date__c.daysBetween(tempDate)+1)/(365.0/12) ;
	                else tempRentAmount = thisLease.Base_Rent__c;
	                
	                system.debug('thisLease.Rent_Date__c.daysBetween(tempDate)'+ thisLease.Rent_Date__c.daysBetween(tempDate));
	                system.debug('if tempRentAmount bef'+tempRentAmount);
	                
	                for(Date nextDate= thisLease.Rent_Date__c.toStartOfMonth().addMonths(1);nextDate<=thisLease.Rent_End_Date__c.addMonths(-1);nextDate=nextDate.addMonths(1)){
	                    system.debug('nextDate'+nextDate + '=' + thisLease.Base_Rent__c );
	                    tempRentAmount = tempRentAmount + thisLease.Base_Rent__c;
	                }
	                
	                system.debug('if tempRentAmount after'+tempRentAmount);
	                tempDate = thisLease.Rent_End_Date__c.toStartOfMonth();
	               
	                if(thisLease.Rent_End_Date__c.toStartOfMonth().addMonths(1).addDays(-1)!= thisLease.Rent_End_Date__c) tempRentAmount += thisLease.Base_Rent__c * (tempDate.daysBetween(thisLease.Rent_End_Date__c)+1)/(365.0/12) ;
	                else tempRentAmount = tempRentAmount + thisLease.Base_Rent__c;
            	}
            }else{//Escalation.
                Integer escMonth=LeasewareUtils.getMonthNumber(thisLease.Escalation_Month__c);
                date firstEscDate = date.newInstance(thisLease.Rent_Date__c.year() + (escMonth<=thisLease.Rent_Date__c.month()?1:0),(integer)escMonth,1);
                Decimal EscRent = thisLease.Base_Rent__c;
                Integer counter=1;
                system.debug('if firstEscDate'+firstEscDate);
                tempDate = thisLease.Rent_Date__c.toStartOfMonth().addMonths(1).addDays(-1);
                system.debug('esc rent '+EscRent );
                system.debug('partial days '+ (thisLease.Rent_Date__c.daysBetween(tempDate)+1));
                system.debug('rent '+EscRent * (thisLease.Rent_Date__c.daysBetween(tempDate)+1)/(365.0/12));
            	if('Daily'.equals(thisLease.Rent_Period__c)){
            		tempDate = thisLease.Rent_Date__c;
            		date dtTempEndDate = (firstEscDate<thisLease.Rent_End_Date__c?firstEscDate:thisLease.Rent_End_Date__c);
            		tempRentAmount = EscRent * tempDate.daysBetween(dtTempEndDate);
					while(dtTempEndDate<thisLease.Rent_End_Date__c){
						EscRent *= (1 + thisLease.Escalation__c/100);
						tempDate = dtTempEndDate; //.addDays(1);
						firstEscDate=firstEscDate.addYears(1);
						dtTempEndDate = (firstEscDate<thisLease.Rent_End_Date__c?firstEscDate:thisLease.Rent_End_Date__c);
	            		tempRentAmount += EscRent * tempDate.daysBetween(dtTempEndDate);
					}

            	}else{
	                if(thisLease.Rent_Date__c != thisLease.Rent_Date__c.toStartOfMonth()) tempRentAmount= EscRent * (thisLease.Rent_Date__c.daysBetween(tempDate)+1)/(365.0/12) ;
	                else tempRentAmount = EscRent ;
	                system.debug('else tempRentAmount'+tempRentAmount);                
	                for(Date nextDate= thisLease.Rent_Date__c.toStartOfMonth().addMonths(1);nextDate<=thisLease.Rent_End_Date__c.addMonths(-1);nextDate=nextDate.addMonths(1)){
	                        if(nextDate==firstEscDate || counter>12) {counter =1;EscRent = EscRent + EscRent * thisLease.Escalation__c/100;}
	                        system.debug('nextDate'+nextDate + '=' + EscRent + '=' + counter );
	                        tempRentAmount = tempRentAmount + EscRent;
	                        counter++;
	                }
	                if(counter>12) {EscRent = EscRent + EscRent * thisLease.Escalation__c/100;}
	                system.debug( EscRent + '=' + counter );
	                tempDate = thisLease.Rent_End_Date__c.toStartOfMonth();
	                system.debug('partial days end '+ (tempDate.daysBetween(thisLease.Rent_End_Date__c)+1));
	                system.debug('rent '+EscRent * (tempDate.daysBetween(thisLease.Rent_End_Date__c)+1)/(365.0/12) );               
	                if(thisLease.Rent_End_Date__c.toStartOfMonth().addMonths(1).addDays(-1)!= thisLease.Rent_End_Date__c) tempRentAmount += EscRent * (tempDate.daysBetween(thisLease.Rent_End_Date__c)+1)/(365.0/12) ;
	                else tempRentAmount += EscRent ;
            	}//Not Daily  
            }//Escalation
        }//Gross Crontract  
        system.debug('if tempRentAmount'+tempRentAmount);
        thisLease.Gross_Contract__c = tempRentAmount;
        if(trigger.IsUpdate){
            oldLeaseRec = trigger.oldmap.get(thisLease.Id);
            if(oldLeaseRec.Operator__c!= null && oldLeaseRec.Operator__c!=thisLease.Operator__c){
                thisLease.Previous_Operator__c = oldLeaseRec.Operator__c;
            }/* 
            if(oldLeaseRec.Base_MRR_20_Month_C_Check__c!=thisLease.Base_MRR_20_Month_C_Check__c 
                || oldLeaseRec.BaseMRR_Airframe_Heavy_Maint_1__c!=thisLease.BaseMRR_Airframe_Heavy_Maint_1__c 
                || oldLeaseRec.Base_MRR_Airframe_Heavy_Maint_Check__c!=thisLease.Base_MRR_Airframe_Heavy_Maint_Check__c 
                || oldLeaseRec.Base_MRR_Airframe_Heavy_Maint_Check_3__c!=thisLease.Base_MRR_Airframe_Heavy_Maint_Check_3__c 
                || oldLeaseRec.Base_MRR_Airframe_Heavy_Maint_Check_4__c!=thisLease.Base_MRR_Airframe_Heavy_Maint_Check_4__c 
                || oldLeaseRec.Base_MRR_APU__c!=thisLease.Base_MRR_APU__c 
                || oldLeaseRec.Base_MRR_Engine__c!=thisLease.Base_MRR_Engine__c 
                || oldLeaseRec.Base_MRR_Engine_LLP__c!=thisLease.Base_MRR_Engine_LLP__c 
                || oldLeaseRec.Base_MRR_Engine_2__c!=thisLease.Base_MRR_Engine_2__c 
                || oldLeaseRec.Base_MRR_Engine_2_LLP__c!=thisLease.Base_MRR_Engine_2_LLP__c 
                || oldLeaseRec.Base_MRR_Engine_3__c!=thisLease.Base_MRR_Engine_3__c 
                || oldLeaseRec.Base_MRR_Engine_3_LLP__c!=thisLease.Base_MRR_Engine_3_LLP__c 
                || oldLeaseRec.Base_MRR_Engine_4__c!=thisLease.Base_MRR_Engine_4__c 
                || oldLeaseRec.Base_MRR_Engine_4_LLP__c!=thisLease.Base_MRR_Engine_4_LLP__c 
                || oldLeaseRec.Base_MRR_Landing_Gear_Per_Hr__c!=thisLease.Base_MRR_Landing_Gear_Per_Hr__c 
                || oldLeaseRec.Base_MRR_Landing_Gear_Per_Mo__c!=thisLease.Base_MRR_Landing_Gear_Per_Mo__c 
                || oldLeaseRec.Base_MRR_LG_Right_Main_Per_Hr__c!=thisLease.Base_MRR_LG_Right_Main_Per_Hr__c 
                || oldLeaseRec.Base_MRR_LG_Right_Main_Per_Mo__c!=thisLease.Base_MRR_LG_Right_Main_Per_Mo__c 
                || oldLeaseRec.Base_MRR_LG_Left_Main_Per_Hr__c!=thisLease.Base_MRR_LG_Left_Main_Per_Hr__c 
                || oldLeaseRec.Base_MRR_LG_Left_Main_Per_Mo__c!=thisLease.Base_MRR_LG_Left_Main_Per_Mo__c 
                || oldLeaseRec.Base_MRR_LG_Right_Wing_Per_Hr__c!=thisLease.Base_MRR_LG_Right_Wing_Per_Hr__c 
                || oldLeaseRec.Base_MRR_LG_Right_Wing_Per_Mo__c!=thisLease.Base_MRR_LG_Right_Wing_Per_Mo__c 
                || oldLeaseRec.Base_MRR_LG_Left_Wing_Per_Hr__c!=thisLease.Base_MRR_LG_Left_Wing_Per_Hr__c 
                || oldLeaseRec.Base_MRR_LG_Left_Wing_Per_Mo__c!=thisLease.Base_MRR_LG_Left_Wing_Per_Mo__c 
                || oldLeaseRec.Base_MRR_LG_Nose_Per_Hr__c!=thisLease.Base_MRR_LG_Nose_Per_Hr__c 
                || oldLeaseRec.Base_MRR_LG_Nose_Per_Mo__c!=thisLease.Base_MRR_LG_Nose_Per_Mo__c 
                || oldLeaseRec.MR_Escalation__c!=thisLease.MR_Escalation__c 
                || oldLeaseRec.MR_Escalation_Month__c!=thisLease.MR_Escalation_Month__c 
 
                )
            {
                thisLease.MR_Last_Escalation_Year__c = null; 
            }
            ***
        }        
    }
    if(setRTIds.size()>0)
        mapRTs=new map<ID, RecordType>([select Name from RecordType where Id in :setRTIds]);
        
    for(Lease__c thisLease:trigger.new){
        if(thisLease.Engine__c!=null && 'Engine'.equals( mapRTs.get(thisLease.RecordTypeId).Name)){
            setEngineIds.add(thisLease.Engine__c);
        }
        if(thisLease.Operator__c!=null){
            setOpIds.add(thisLease.Operator__c);
        }
    }
    if(setEngineIds.size()>0)       
        mapAllEngines = new map<Id, Constituent_Assembly__c>([select Attached_Aircraft__c, Attached_Aircraft__r.Status__c from Constituent_Assembly__c where id in :setEngineIds]);
    if(setOpIds.size()>0)       
        mapAllOps = new map<Id, Operator__c>([select Id, Marketing_Representative_Contact__c from Operator__c where id in :setOpIds]);
    
    for(Lease__c thisLease:trigger.new){
        try{
            if(trigger.isUpdate && thisLease.Operator__c==null && trigger.oldMap.get(thisLease.id).Operator__c != null){
                thisLease.Marketing_Representative_Lookup__c=null;
            }else if( thisLease.Operator__c != null &&  (trigger.isInsert || thisLease.Marketing_Representative_Lookup__c==null 
                            ||(trigger.isUpdate && trigger.oldMap.get(thisLease.id).Operator__c != thisLease.Operator__c))){
                thisLease.Marketing_Representative_Lookup__c=mapAllOps.get(thisLease.Operator__c).Marketing_Representative_Contact__c;
            }
        }catch(exception e){
            system.debug('Ignoring Exception ' + e.getMessage());
        }


        try{
            if(thisLease.Engine__c!=null && 'Engine'.equals( mapRTs.get(thisLease.RecordTypeId).Name)){
                system.debug('It is an Engine!');
//              Constituent_Assembly__c theCA=[select Attached_Aircraft__c, Attached_Aircraft__r.Status__c from Constituent_Assembly__c where id = :thisLease.Engine__c limit 1];
                Constituent_Assembly__c theCA=mapAllEngines.get(thisLease.Engine__c);
                if(thisLease.Aircraft__c==null || thisLease.Aircraft__c!=theCA.Attached_Aircraft__c){
                    thisLease.Aircraft__c=theCA.Attached_Aircraft__c;
                }
            }
            
            if(trigger.isUpdate && thisLease.Y_Hidden_Recalc_Buckets__c){
                /* Bobby 20141217
                Not going for bulkification here for two reasons:
                    1. It is not very common to update many leases in one go.
                        Even if multiple leases are loaded in one go, Y_Hidden_Recalc_Buckets__c 
                        is not expected to be set on Lease on data loading as Invoices won't be existing and this gets executed only on Update.
                    2. Refresh Aging must be clicked on each lease for this to be executed.
                    
                    Multiple leases will undergo update together only when leases are forcefully updated using scripts. Do it responsibly. 
                ***

                thisLease.Not_Due_Yet__c=0;
                thisLease.X0_15_Days_Outstanding__c=0;
                thisLease.X16_30_Days_Outstanding__c=0;
                thisLease.X31_60_Days_Outstanding__c=0;
                thisLease.X61_90_Days_Outstanding__c=0;
                thisLease.X91_120_Days_Outstanding__c=0; 
                thisLease.X120_Plus_Days_Outstanding__c=0;          


                list<Invoice__c> Invs = [select id, Ageing__c, Balance_Due__c, Date_of_MR_Payment_Due__c, Invoice_Date__c from Invoice__c where Lease__c = :thisLease.id 
                        and Balance_Due__c >0 
                        AND ((Date_of_MR_Payment_Due__c!=null AND Date_of_MR_Payment_Due__c >TODAY) OR (Date_of_MR_Payment_Due__c=null AND Invoice_Date__c >TODAY) ) ];

                Decimal BucketNotYetDue=0;
                
                for(Invoice__c curInv: Invs){
                    if(curInv.Date_of_MR_Payment_Due__c==null)curInv.Date_of_MR_Payment_Due__c=curInv.Invoice_Date__c; //temporarily using Due date field. Not saving.
                    if(date.today()<curInv.Date_of_MR_Payment_Due__c){
                        
                        system.debug('Inv id ' + thisLease.Name +  ', ' + curInv.Id);
                        
                        BucketNotYetDue+=curInv.Balance_Due__c;
                    }
                }
                thisLease.Not_Due_Yet__c=BucketNotYetDue;

system.debug('Not Yet Due - '+ BucketNotYetDue);

                Invs = [select id, Ageing__c, Balance_Due__c, Date_of_MR_Payment_Due__c, Invoice_Date__c 
                        from Invoice__c where Lease__c = :thisLease.id AND Ageing__c>=0];
                
                Decimal Bucket0_15=0, Bucket16_30=0, Bucket31_60=0, Bucket61_90=0,  Bucket91_120=0, Bucket120_Plus=0;
                for(Invoice__c curInv: Invs){
// No longer required. Days Outstanding on paid invoices are set to null.                   if(curInv.Ageing__c<=0)continue; //Needed to avoid paid invoices.
                    if(curInv.Date_of_MR_Payment_Due__c==null)curInv.Date_of_MR_Payment_Due__c=curInv.Invoice_Date__c; //temporarily using Due date field. Not saving.
                    if(date.today()<curInv.Date_of_MR_Payment_Due__c)continue; //Exclude future date payments. 
                    if(curInv.Ageing__c<16)Bucket0_15+=curInv.Balance_Due__c;
                    else if(curInv.Ageing__c<31)Bucket16_30+=curInv.Balance_Due__c;
                    else if(curInv.Ageing__c<61)Bucket31_60+=curInv.Balance_Due__c;
                    else if(curInv.Ageing__c<91)Bucket61_90+=curInv.Balance_Due__c;
                    else if(curInv.Ageing__c<121)Bucket91_120+=curInv.Balance_Due__c;
                    else Bucket120_Plus+=curInv.Balance_Due__c;
                }
                
                thisLease.X0_15_Days_Outstanding__c=Bucket0_15;
                thisLease.X16_30_Days_Outstanding__c=Bucket16_30;
                thisLease.X31_60_Days_Outstanding__c=Bucket31_60;
                thisLease.X61_90_Days_Outstanding__c=Bucket61_90;
                thisLease.X91_120_Days_Outstanding__c=Bucket91_120;
                thisLease.X120_Plus_Days_Outstanding__c=Bucket120_Plus;
                thisLease.Y_Hidden_Recalc_Buckets__c=false;
            }
/* Moving these methods to LWGlobalUtils so that Lease record need not be updated.

            if(trigger.isUpdate && thisLease.Y_Hidden_Create_Interest_Schedule__c){
                 Anjani 20160105 : Similar approach like others 
                Not going for bulkification here for two reasons:
                    1. This gets executed only on Lease Update.
                    2. Refresh Aging must be clicked on each lease for this to be executed.
                    Multiple leases will undergo update together only when leases are forcefully updated using scripts. Do it responsibly. 

				thisLease.Y_Hidden_Create_Interest_Schedule__c=false;

                system.debug('leaseId='+thisLease.Id );
                system.debug('SecurityDeposit='+thisLease.Total_Cash_Transactions_Security__c );
                system.debug('InterestRate='+thisLease.Deposit_Interest_Rate__c );
                system.debug('StartDate='+thisLease.Deposit_Received_Date__c );
                system.debug('EndDate='+thisLease.Deposit_Return_Date__c );
                
                if(thisLease.Total_Cash_Transactions_Security__c==null ){
                    thisLease.Total_Cash_Transactions_Security__c.addError('Please enter "Security Deposit (Cash)" to create Interest payments');
                    continue;
                }
                else if(thisLease.Deposit_Interest_Rate__c==null ){
                    thisLease.Deposit_Interest_Rate__c.addError('Please enter "Deposit Interest Rate" to create Interest payments');
                    continue;
                }
                else if(thisLease.Deposit_Received_Date__c==null ){
                    thisLease.Deposit_Received_Date__c.addError('Please enter "Deposit Received Date" to create Interest payments');
                    continue;
                }   
                // commenting as we are doing defaulting for Deposit_Return_Date__c 
                //else if(thisLease.Deposit_Return_Date__c==null ){
                    //thisLease.Deposit_Return_Date__c.addError('Please enter "Deposit Return Date" to create Interest payments');
                //}                                         
                
                leasewareUtils.InsertInterestRecs(thisLease.Id,thisLease.Total_Cash_Transactions_Security__c,thisLease.Deposit_Interest_Rate__c,thisLease.Deposit_Received_Date__c,thisLease.Deposit_Return_Date__c);
            }   
 * 
 * 
 *             if(trigger.isUpdate && thisLease.Y_Hidden_Create_Rent_Schedule__c){
                 Bobby 20141218 
                Not going for bulkification here for two reasons:
                    1. This gets executed only on Lease Update.
                    2. Refresh Aging must be clicked on each lease for this to be executed.
                    Multiple leases will undergo update together only when leases are forcefully updated using scripts. Do it responsibly. 
                thisLease.Y_Hidden_Create_Rent_Schedule__c=false;
                
                //if('Fixed Rent'.equals(thisLease.Rent_Type__c))
                // we are doing the Rent Type check in leasewareUtils.InsertRentRecs()
                //leasewareUtils.InsertRentRecs(thisLease.Id);
                // anjani : call as queueclass instead of future
                System.enqueueJob(new RentQueue.IncomeScheduleQ(thisLease.Id));                
            }
            if(trigger.isUpdate && thisLease.Y_Hidden_Create_Audit_Schedule__c){
                thisLease.Y_Hidden_Create_Audit_Schedule__c=false;
                leasewareUtils.InsertAuditRecs(thisLease.Id);
            }
***            
            
        }catch(exception e){
            thisLease.addError('Can\'t update - ' + e.getMessage());
            continue;
        }
    
    }
*/
}