trigger AssemblyMRFundTransferTrigger on Assembly_MR_Fund_Transfer__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if(LeaseWareUtils.isTriggerDisabled())return;
    TriggerFactory.createHandler(Assembly_MR_Fund_Transfer__c.sObjectType);   
}