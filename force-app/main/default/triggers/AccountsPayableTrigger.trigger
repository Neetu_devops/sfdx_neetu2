trigger AccountsPayableTrigger on Payable_line_item__c (after delete, after insert, after undelete, 
                                    after update, before delete, before insert, before update) {
            TriggerFactory.createHandler(Payable_line_item__c.sObjectType);
}
