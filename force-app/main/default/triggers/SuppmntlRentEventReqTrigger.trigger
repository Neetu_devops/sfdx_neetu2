trigger SuppmntlRentEventReqTrigger on Supp_Rent_Event_Reqmnt__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Supp_Rent_Event_Reqmnt__c.sObjectType);
}