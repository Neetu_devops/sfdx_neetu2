trigger TransactionTriggerHandler on Transaction__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

        if(LeaseWareUtils.isTriggerDisabled())return;
        TriggerFactory.createHandler(Transaction__c.sObjectType);
            
}