trigger CashSDTrigger on cash_security_deposit__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(cash_security_deposit__c.sObjectType);
}