trigger HistoricalEventTaskTrigger on Historical_Event_Task__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Historical_Event_Task__c.sObjectType);
}