trigger DepartmentDefaultTrigger on Department_Default__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Department_Default__c.sObjectType);

}