trigger Stepped_RentTrigger on Stepped_Rent__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Stepped_Rent__c.sObjectType);


}