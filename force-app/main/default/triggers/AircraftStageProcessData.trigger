trigger AircraftStageProcessData on Aircraft_Stage__c (after insert) {
    
    boolean bDisableAll=false, bDisableAircraft=false;
    
    LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
    if(lwSettings!=null){
        bDisableAll=lwSettings.Disable_All_Integrations__c;
        bDisableAircraft=lwSettings.Disable_Aircraft_Integration__c;
    }
    
    if(bDisableAll || bDisableAircraft){
        system.debug('Aircraft integration disabled. Returning.');
        return;
    }
    
    map<string, Aircraft__c> mapExistingAircraft = new map <string, Aircraft__c>();
    Map<String, Schema.SObjectField> mapAircraftFields = Schema.SObjectType.Aircraft_Stage__c.fields.getMap();
    Map<String,Aircraft_Stage__c> mapAircraftToUpdate = new Map<String,Aircraft_Stage__c>();
    Map<Id,String> mapAircraftId = new Map<Id,String>();
    Map<String,String> mapVariant = new Map<String,String>();
    private Map<String,String> listToMap;  
    private List<Mapping__c> listVariantMap;
    if(listVariantMap == null) listVariantMap = [Select Name,Map_To__c from Mapping__c where IsActive__c = true];
    
    string strQry = 'select ';
    for(String Field:mapAircraftFields.keyset()){
        Schema.DescribeFieldResult thisFieldDesc = mapAircraftFields.get(Field).getDescribe();
        if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue;
        strQry+= (thisFieldDesc.getName() + ', ');     
    } 
    strQry = strQry.left(strQry.length()-2);
    strQry+=' from ' + Schema.SObjectType.Aircraft__c.getName();
    system.debug('strQry===='+strQry);
    Aircraft__c[] listAllAircraft =  database.query(strQry);
    List<String> liststrWithLeadingZeroes = new List<String>();
    String strCurMSN = '';
    String aircraftId = '' ;

    for(Aircraft__c curAircraft: listAllAircraft){
        
        if(curAircraft.Aircraft_Id__c==null)continue; //External Id cannot be null

        //Removing any leading zeroes from MSN and putting in map
        strCurMSN = curAircraft.MSN_Number__c.replaceFirst('^0+','');
        aircraftId = curAircraft.Aircraft_Type__c+'-'+strCurMSN;

        //System.debug('Existing aircraftID = ' + aircraftId);
        mapExistingAircraft.put(aircraftId, curAircraft);
        if(String.isNotBlank(curAircraft.Asset_Id_External__c)){mapExistingAircraft.put(curAircraft.Asset_Id_External__c,curAircraft);}
    }

    Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName();
    list<Aircraft__c> listAircraftToUpdate = new list<Aircraft__c>();
    
    List<Aircraft_Stage__c> listNew = trigger.new;
    Map<String,List<String>> mapTypeVariant = new Map<String,List<String>>();
    String variant = ''; 
    String type = '';
    for(Aircraft_Stage__c ac: trigger.new){
        // first check for existing type , then reset the type variant from mapping table and check for AC with the new type .
        aircraftId = ac.Aircraft_Id__c;
        System.debug('Type variant coming from Aircraft Stage  :' + ac.Aircraft_Type__c +'::'+ ac.Aircraft_Variant__c +'::'+ aircraftId);
         //variant = ac.Aircraft_Variant__c;
         variant = (String.isNotBlank(ac.Aircraft_Type__c) ?  ac.Aircraft_Type__c.trim() : '') +'-'+
         (String.isNotBlank(ac.Aircraft_Variant__c) ?  ac.Aircraft_Variant__c.trim() : '') ;

         System.debug('Type variant coming from Incoming Data :' + ac.Aircraft_Type__c +'::'+ variant +'::'+ aircraftId);
        if(String.isNotBlank(variant)){
            List<String> listTypeVariant = LeasewareUtils.setAssetTypeVariant(listVariantMap, variant);
            variant = listTypeVariant[1]==null?'':listTypeVariant[1]; 
            strCurMSN = ac.MSN_Number__c.replaceFirst('^0+','');
            aircraftId = listTypeVariant[0]+'-'+strCurMSN;
            if(!mapTypeVariant.containsKey(aircraftId)){
                mapTypeVariant.put(aircraftId,listTypeVariant); // to avoid Duplicate Aircraft_Id__c issue in Integration.
            }
        }
        System.debug('Final : Type: ' + mapTypeVariant.get(aircraftId)[0]);
        System.debug('Final : Variant: ' + variant);
        System.debug('Final : Aircraft_Id__c: ' + aircraftId);
        mapAircraftId.put(ac.Incoming_Data__c,aircraftId); // need this to get the correct aircraftId for the AircraftStage
        mapVariant.put(ac.Incoming_Data__c,variant); // to avoid updating variant with wrong value in Integration when data comes in bulk.
        mapAircraftToUpdate.put(aircraftId, ac);
    }
    liststrWithLeadingZeroes.clear();

    for(Aircraft_Stage__c curAcStage : mapAircraftToUpdate.values()){
        if(curAcStage.Aircraft_Id__c==null && String.isBlank(curAcStage.Asset_Id_External__c))continue; //External Id cannot be null
        
        String aircraftId2 = mapAircraftId.get(curAcStage.Incoming_Data__c);
        System.debug('Checking for Aircraft with Aircraft Id ---' + aircraftId2);
        System.debug('Checking for Aircraft with variant ---' + mapVariant.get(curAcStage.Incoming_Data__c));
        
        Aircraft__c thisAc = mapExistingAircraft.get(aircraftId2);

           //check if the AircraftType + ProductionNumber key is available in system or not?
        if(thisAc==null && curAcStage.Production_Number__c!=null){
            thisAc = mapExistingAircraft.get(curAcStage.Aircraft_Type__c + '-' + curAcStage.Production_Number__c);
        }
        //If Aircraft_Id__c does not return a match check with External Id
        if(thisAc == null && String.isNotBlank(curAcStage.Asset_Id_External__c)){
            System.debug('Checking for Aircraft with Aircraft Id(External) ---' + curAcStage.Asset_Id_External__c);
            thisAc = mapExistingAircraft.get(curAcStage.Asset_Id_External__c);
        }

        System.debug('Existing Aircraft ::' + thisAc);
        
        // Setting RecordType Id for a new Aircraft. 
        Id recordTypeId ;
        if(curAcStage.Asset_Class__c !=null && rtMapByName.get(curAcStage.Asset_Class__c)!=null){
            recordTypeId = rtMapByName.get(curAcStage.Asset_Class__c).getRecordTypeId();
        }else{
            recordTypeId = rtMapByName.get('Aircraft').getRecordTypeId();
        }

        System.debug('Asset_class :: ' + curAcStage.Asset_Class__c +'::'+ recordTypeId);
        
        if(thisAc==null){ //New aircraft.
            thisAc = new Aircraft__c();
            for(String Field:mapAircraftFields.keyset()){
                Schema.DescribeFieldResult thisFieldDesc = mapAircraftFields.get(Field).getDescribe();
                if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue;
                if(!thisFieldDesc.isCustom() && !Field.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
                
                thisAc.put(thisFieldDesc.getName(), curAcStage.get(Field));
            }
            //Required fields if null
            thisAc.RecordTypeId = recordTypeId == null ? '' : recordTypeId ;
            thisAc.TSN__c=thisAc.TSN__c==null?0:thisAc.TSN__c;
            thisAc.CSN__c=thisAc.CSN__c==null?0:thisAc.CSN__c;
            thisAc.Status__c=thisAc.Status__c==null?'Available':thisAc.Status__c;
            thisAc.Aircraft_Type__c = mapTypeVariant.get(aircraftId2)[0];
            thisAc.Aircraft_Variant__c = mapVariant.get(curAcStage.Incoming_Data__c);
            System.debug('Aircraft Id for new asset : ' + aircraftId2); 
            thisAc.Aircraft_ID__C = aircraftId2; 
            System.debug('RecordTypeId for New Aircraft :' + thisAc.RecordTypeId +' :: '+ ' for incoming Class value :' + curAcStage.Asset_Class__c);         
            listAircraftToUpdate.add(thisAc);
        }else{//Existing aircraft.
            boolean bUpdated = false;   
            System.debug('aircraftId for existing Aircraft ---' + aircraftId2);
            System.debug('mapTypeVariant.get(aircraftId2)[0]---' + mapTypeVariant.get(aircraftId2)[0]);
            for(String Field:mapAircraftFields.keyset()){
                Schema.DescribeFieldResult thisFieldDesc = mapAircraftFields.get(Field).getDescribe();
                if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue; //Skip all system fields like createdBy.
                if(!thisFieldDesc.isCustom() && !Field.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
                
                //Update the Asset if Enigne Type is coming null
                if(curAcStage.get(Field)!=null || 'Engine_Type__c'.equals(thisFieldDesc.getLocalName())  ){ // Update only if incoming field is not null. //thisAc.get(Field)!=null || 
                    if('Name'.equals(thisFieldDesc.getLocalName()))continue;
                    if(LeaseWareUtils.IsNotSame(thisAc.get(Field), curAcStage.get(Field), thisFieldDesc.getType())){
                        if('Aircraft_ID__c'.equals(thisFieldDesc.getLocalName()))continue;
                        if('Aircraft_Type__c'.equals(thisFieldDesc.getLocalName()))continue;
                        if('Aircraft_Variant__c'.equals(thisFieldDesc.getLocalName()))continue;
                        if('Status__c'.equals(thisFieldDesc.getLocalName()) 
                           && ('Assigned'.equals((string)(curAcStage.get(Field))) || 'Assigned'.equals((string)(thisAc.get(Field))) ) )continue; //Assigned status is special.
                        system.debug('Field updated ' + thisFieldDesc.getName() + ' : ' + thisAc.get(Field) + '->' + curAcStage.get(Field));
                        thisAc.put(thisFieldDesc.getName(), curAcStage.get(Field));
                        bUpdated=true;
                    }
                    System.debug('mapTypeVariant---' + mapTypeVariant);
                    System.debug('thisAc.Aircraft_Type__c---' + thisAc.Aircraft_Type__c);
                    System.debug('aircraftId2---' + aircraftId2);
                    System.debug('mapTypeVariant.get(aircraftId2)[0]---' + mapTypeVariant.get(aircraftId2)[0]);
                    System.debug('variant---' + mapVariant.get(curAcStage.Incoming_Data__c));
                    System.debug('thisAc.Aircraft_Variant__c---' + thisAc.Aircraft_Variant__c);

                    if((String.isNotBlank(thisAc.Aircraft_Type__c) && !thisAc.Aircraft_Type__c.equals(mapTypeVariant.get(aircraftId2)[0])) 
                    || (String.isNotBlank(thisAc.Aircraft_Variant__c) && !thisAc.Aircraft_Variant__c.equals(mapVariant.get(curAcStage.Incoming_Data__c)))
                    || (String.isBlank(thisAc.Aircraft_Variant__c) || (String.isBlank(thisAc.Aircraft_Type__c)))){ 
                // Need to check for blank value of variant and type , for scenario when variant is set as null initially, and on update value is added in mapping__c
                // Then variant should get updated to correct value, which is coming in incoming data and aircraft type should get updated to value from mapping table
                        system.debug('Type updated ' +  thisAc.Aircraft_Type__c + ' --> ' + mapTypeVariant.get(aircraftId2)[0]);
                        system.debug('variant  updated ' +  thisAc.Aircraft_Variant__c + ' --> ' + mapVariant.get(curAcStage.Incoming_Data__c));
                        system.debug('Aircraft Id updated ' +  thisAc.Aircraft_ID__C + ' --> ' + aircraftId2);
                        thisAc.Aircraft_Type__c = mapTypeVariant.get(aircraftId2)[0];
                        thisAc.Aircraft_Variant__c = mapVariant.get(curAcStage.Incoming_Data__c);
                        thisAc.Aircraft_ID__C = aircraftId2;
                        bUpdated=true;
                    }
                }
            }
            if(bUpdated)listAircraftToUpdate.add(thisAc);
        }           
    }
    system.debug('Upserting '+ listAircraftToUpdate);
    try{
        LeaseWareUtils.clearFromTrigger(); 
        if(listAircraftToUpdate.size()>0)upsert listAircraftToUpdate ; 
    }catch(Exception e){
            System.debug('Exception in inserting/updating Aircraft : ' + e.getMessage());
            LeaseWareUtils.createExceptionLog(e,'Exception in inserting/updating Aircraft ', 'Aircraft__c', String.valueOf(listAircraftToUpdate[0].id),'AircraftStageProcessData', true);
    }
}
