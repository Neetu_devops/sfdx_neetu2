trigger DepreciationHandlerTrigger on Depreciation_schedule__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Depreciation_schedule__c.sObjectType);

}