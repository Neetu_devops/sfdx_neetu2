trigger FetchCurrentInvoice on Invoice_Summary__c (after insert, after update) {

        list<Invoice_Summary__c> listNew =  (list<Invoice_Summary__c>)trigger.new;
        
        if(LWGlobalReportUtils.isFromTrigger('FetchCurrentInvoice')) return;
        LWGlobalReportUtils.setFromTrigger('FetchCurrentInvoice');
        
        string returnMessage = 'Invoice created successfully';
        for(Invoice_Summary__c curInvSummary :listNew ){
            system.debug('curInvSummary.Id ='+curInvSummary.Id);
            system.debug('curInvSummary.For_Month_Ending__c ='+curInvSummary.For_Month_Ending__c);
            system.debug('curInvSummary.Y_Hidden_Month__c = '+curInvSummary.Id);
            system.debug('curInvSummary.Y_Hidden_Year__c ='+curInvSummary.Y_Hidden_Year__c);
            if(Trigger.isInsert){
                returnMessage = LWGlobalReportUtils.createInvoiceSummary(curInvSummary.Id, ''+curInvSummary.For_Month_Ending__c.format(), curInvSummary.Y_Hidden_Month__c, curInvSummary.Y_Hidden_Year__c);
            	// if any error  
                 system.debug('returnMessage='+returnMessage); 
                 if(!returnMessage.contains('Invoice created successfully')){
                            listNew[0].addError(returnMessage);
                 }
            }
                
                          
        }
        //This is for quick Action call
			if (Trigger.isUpdate){
                returnMessage = 'Invoice created successfully';
                if(listNew[0].getQuickActionName() == Schema.Invoice_Summary__c.QuickAction.Fetch_Current_Invoices){
                    returnMessage = LWGlobalReportUtils.createInvoiceSummary(listNew[0].Id, ''+listNew[0].For_Month_Ending__c.format(), listNew[0].Y_Hidden_Month__c, listNew[0].Y_Hidden_Year__c);
                }   
                
                if(!returnMessage.contains('Invoice created successfully')){
                        listNew[0].addError(returnMessage);
             }   
       
            }
}