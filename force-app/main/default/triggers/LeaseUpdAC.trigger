trigger LeaseUpdAC on Lease__c (after insert, after update) {


    /*
    if(LeaseWareUtils.isTriggerDisabled())return;

    set<ID> setAllACIDs = new set<ID>();
    map<id, Aircraft__c> mapAllACs =new map<ID, Aircraft__c>();

    if(trigger.isUpdate){
        for(Lease__c thisLease: trigger.new){
            ID ACID = Trigger.oldMap.get(thisLease.Id).Aircraft__c;
            if(AcID!=null)setAllACIDs.add(ACID);
        }
    }
    for (Lease__c thisLease: trigger.new){
        if(thisLease.Aircraft__c!=null)setAllACIDs.add(thisLease.Aircraft__c);
    }

    map<id, Aircraft_In_Deal__c> mapAiCs = new map<id, Aircraft_In_Deal__c>();
    if(setAllACIDs.size()>0){
        mapAllACs = new map<ID, Aircraft__c>([select id, Name, Status__c from Aircraft__c where id in :setAllACIDs]);

        list<Aircraft_In_Deal__c> listAiCs = [select id, Aircraft__c, Next_Operator__c from Aircraft_In_Deal__c 
                where Marketing_Deal__r.Campaign_Active__c = true
                AND Aircraft__c in :setAllACIDs];

        for(Aircraft_In_Deal__c curAiC: listAiCs){
            mapAiCs.put(curAiC.Aircraft__c, curAiC);
        }
    }      
     

    if(LeasewareUtils.isFromTrigger())return;
    LeasewareUtils.setFromTrigger();
    
    list<Aircraft_In_Deal__c> listAiCsToUpd = new list<Aircraft_In_Deal__c>();
system.debug('UpdAC - before loop.');
    for (Lease__c thisLease: trigger.new){
system.debug('UpdAC - Inside loop.');

        if(thisLease.Aircraft__c!=null){
system.debug('UpdAC - AC not null.');
            Aircraft_In_Deal__c AiCToUpd = mapAiCs.get(thisLease.Aircraft__c);
            if(AiCToUpd != null && ( AiCToUpd.Next_Operator__c == null || thisLease.Next_Operator__c == null || AiCToUpd.Next_Operator__c != thisLease.Next_Operator__c)){
                AiCToUpd.Next_Operator__c = thisLease.Next_Operator__c;
                listAiCsToUpd.add(AiCToUpd);
            }
        }

        String[] PhbRates, monthThresholds;
        Boolean isErr1=false, isErr2=false;
        integer cntRates=0, cntMonths=0;
        try{
            if(thisLease.PBH_Rates__c!=null){
                PhbRates = thisLease.PBH_Rates__c.deleteWhitespace().split(',' , 0);
                for(String curRate : PhbRates){
                     if(Decimal.valueOf(curRate) < 1) throw new LeaseWareException();
                     cntRates++;
                }
            }  
        }catch(Exception e){
            isErr1=true;
        }  

        try{
            if(thisLease.Month_Thresholds__c!=null){
                MonthThresholds = thisLease.Month_Thresholds__c.deleteWhitespace().split(',' , 0);
                for(String curMonth : monthThresholds){
                     if(Integer.valueOf(curMonth) < 0) throw new LeaseWareException();
                     cntMonths++;
                }
            }  
        }catch(Exception e){
            isErr2=true;
        }  
        
        if(isErr1 || isErr2){
            thisLease.addError('Please specify ' + (isErr1 ? 'PHB Rates':'') + (isErr1 && isErr2?' and ':'') + (isErr2?'Month Thresholds':'') +' as comma separated positive numbers.');
            LeasewareUtils.unsetFromTrigger();
            return;
        }

        if(cntRates!=cntMonths){
            thisLease.addError('You have entered ' + cntRates + ' PHB Rates and ' + cntMonths + ' Month Thresholds. Please ensure number of PHB Rates are equal to the number of Month Thresholds.');
            LeasewareUtils.unsetFromTrigger();
            return;
        }
        
            Lease__c oldLease;
            Aircraft__c thisAC, oldAC;
            Integer iNewAC=0, iOldAC=0;


system.debug('UpdAC - Checking IsUpdate.');
            if(trigger.isUpdate  ){
system.debug('UpdAC - IsUpdate.');
                
                oldLease = Trigger.oldMap.get(thisLease.Id);

                if( oldLease.Aircraft__c != null && oldLease.Lease_End_Date__c !=null && thisLease.Lease_End_Date__c!=null){  
                    system.debug('Lease End Dates ' +  oldLease.Lease_End_Date__c + ' - ' +thisLease.Lease_End_Date__c);                
                    if(!oldLease.Lease_End_Date__c.isSameDay(thisLease.Lease_End_Date__c)){
                        system.debug('Lease Extended.');
                        Lease_Extension_History__c newLeaseHist = new Lease_Extension_History__c(Name=(thisLease.Name + ' Term Change From ' + oldLease.Lease_End_Date__c).left(80), Lease__c=thisLease.id, 
                                Previous_Lease_End_Date_Time__c=oldLease.Lease_End_Date__c, Lease_End_Date_Time_Changed_To__c=thisLease.Lease_End_Date__c, Is_Created_By_Trigger__c=true);
                        insert newLeaseHist;
                        Asset_History__c newAssetHist = new Asset_History__c(Name=oldLease.Serial_Number__c + ' Lease Extension', 
                            Aircraft__c=oldLease.Aircraft__c, Assembly__c=oldLease.Engine__c, Lease__c=oldLease.id, 
                            Former_Lease_End_Date_Lease_Extension__c=oldLease.Lease_End_Date_New__c, Status_Change_Date__c=date.today(),
                            Comments__c='Auto-created on lease end date change.', Status__c='Lease Extension');
                        insert newAssetHist;
    //                  system.debug('Inserted rec with Lease End Dates ' +  oldLease.Lease_End_Date__c + ' - ' +thisLease.Lease_End_Date__c);
                    }
                }
                if((oldLease.AOG_Date__c == null && thisLease.AOG_Date__c != null) || (oldLease.Aircraft__c !=null && thisLease.Aircraft__c==null)){
                //Lease has been terminated.    
                        system.debug('Lease Terminated..');
                        list<Asset_History__c> listNewAssetHist = new list<Asset_History__c>();
                        listNewAssetHist.add(new Asset_History__c(Name=oldLease.Serial_Number__c + ' Lease Termination', 
                            Aircraft__c=oldLease.Aircraft__c, Assembly__c=oldLease.Engine__c, Lease__c=oldLease.id, 
                            Status_Change_Date__c=date.today(),
                            Comments__c='Auto-created on lease termination.', Status__c='Lease Termination'));
                        listNewAssetHist.add(new Asset_History__c(Name=oldLease.Serial_Number__c + ' AOG', 
                            Aircraft__c=oldLease.Aircraft__c, Assembly__c=oldLease.Engine__c, 
                            Status_Change_Date__c=(oldLease.AOG_Date__c==null?date.today():oldLease.AOG_Date__c),
                            Comments__c='Auto-created on lease termination.', Status__c='AOG'));
                        insert listNewAssetHist;
                }else if(oldLease.Aircraft__c ==null && thisLease.Aircraft__c!=null){
                        system.debug('Lease Added..');
                        Asset_History__c newAssetHist = new Asset_History__c(Name=thisLease.Serial_Number__c + ' On Lease', 
                            Aircraft__c=thisLease.Aircraft__c, Assembly__c=thisLease.Engine__c, Lease__c=thisLease.id, 
                            Status_Change_Date__c=date.today(), Comments__c='Auto-created on adding the asset to the lease.', Status__c='On Lease');
                        insert newAssetHist;
                }
                
                if(oldLease.Aircraft__c!=null){
//                    oldAC = [select id, Name, Status__c from Aircraft__c where id = :oldLease.Aircraft__c];
                    oldAC = mapAllACs.get(oldLease.Aircraft__c);
                    iOldAC=1;
    System.debug('Old AC is :'+oldAC.Name);
                }           
            }else if(trigger.isInsert && thisLease.Aircraft__c!=null){
                system.debug('Lease Added..');
                Asset_History__c newAssetHist = new Asset_History__c(Name=thisLease.Serial_Number__c + ' On Lease', 
                    Aircraft__c=thisLease.Aircraft__c, Assembly__c=thisLease.Engine__c, Lease__c=thisLease.id, 
                    Status_Change_Date__c=date.today(), Comments__c='Auto-created on adding the asset to the lease.', Status__c='On Lease');
                insert newAssetHist;
            }

        if(thisLease.Aircraft__c!=null){
//            thisAC = [select id, Name, Status__c from Aircraft__c where id = :thisLease.Aircraft__c];
            thisAC = mapAllACs.get(thisLease.Aircraft__c);
            iNewAC=1;
System.debug('New AC is :'+thisAC.Name);
            }           

            //No AC, not to worry about making any change to the AC
            if(iNewAC + iOldAC == 0){
                continue;
            }

                //Old and New exist. Check if same AC, not to worry about making any change to the AC
            if(iNewAC + iOldAC == 2){
                if(thisAC.id == oldAC.id){
                    continue;
                }
            }

            if(iNewAC==0){ //Here iOldAC == 1 -> AC removed from Lease
                
                oldAC.isTrigger__c='Yes';
                oldAC.Status__c='Available';
                oldAC.Lease__c=null;
                update oldAC;
                continue;
            }
            
            // A new AC is assigned to the Lease. Ensure that it is Available.
            if(thisAC.Status__c != 'Available'){
                thisLease.addError('The selected aircraft/engine is not available. Status is ' + thisAC.Status__c);
                continue;
            }

            //Now mark the new one Assigned and old available if applicable.
            thisAC.Status__c='Assigned';
            thisAC.Lease__c=thisLease.Id;
            thisAC.isTrigger__c='Yes';
            update thisAC;
            if(iOldAC==1){
                oldAC.Status__c='Available';
                oldAC.Lease__c=null;
                oldAC.isTrigger__c='Yes';
                update oldAC;
            }
     }
    if(listAiCsToUpd.size()>0)update listAiCsToUpd;
    LeasewareUtils.unsetFromTrigger();
*/
}