trigger InvoiceAdjustmentLineItemTrigger on Invoice_Adjustment_Line_Item__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Invoice_Adjustment_Line_Item__c.sObjectType);
}