trigger LLPTrigger on Sub_Components_LLPs__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
   
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Sub_Components_LLPs__c.sObjectType);
       
    
}