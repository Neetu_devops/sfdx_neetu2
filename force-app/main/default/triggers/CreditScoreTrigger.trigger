trigger CreditScoreTrigger on Credit_Score__c (after insert,after update,before insert,before update,after delete, before delete) {

    if(LeaseWareUtils.isTriggerDisabled())return;
      TriggerFactory.createHandler(Credit_Score__c.sObjectType); 
}