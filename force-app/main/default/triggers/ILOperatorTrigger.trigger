trigger ILOperatorTrigger on IL_Operator__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
  if(LeaseWareUtils.isTriggerDisabled())return;
  TriggerFactory.createHandler(IL_Operator__c.sObjectType);
}