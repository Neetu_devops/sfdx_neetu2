/*
	Created By : Anjani
	Validation : created a column called Check_Duplicate__c (Unique) 
		--> Update combination of currRec.name + currRec.Lookup_Type__c + currRec.Sub_LookupType__c + currRec.Sub_LookupType2__c+currRec.Sub_LookupType3__c+currRec.Source__c+currRec.Target__c
		--> If any value is duplicate it will throw an error .


*/

trigger Custom_lookupInsUpd on Custom_Lookup__c (before delete, before insert, before update) {

	if(LeaseWareUtils.isTriggerDisabled())return;
	if(trigger.IsDelete)return;
	for(Custom_Lookup__c currRec : trigger.new){
		currRec.Check_Duplicate__c = currRec.name + currRec.Lookup_Type__c + currRec.Sub_LookupType__c + currRec.Sub_LookupType2__c+currRec.Sub_LookupType3__c+currRec.Source__c+currRec.Target__c ;
	}

}