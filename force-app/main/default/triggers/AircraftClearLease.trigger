trigger AircraftClearLease on Aircraft__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if(LeaseWareUtils.isTriggerDisabled())return;
	TriggerFactory.createHandler(Aircraft__c.sObjectType);
	// Moved all code of this trigger to AircraftTriggerHandler (iTrigger class file)
}
/*    
    if(LeaseWareUtils.isTriggerDisabled())return;


    Aircraft__c[] newACs = trigger.new;

System.debug('isTrigger is '+newACs[0].isTrigger__c);
    if(newACs[0].isTrigger__c=='Yes'){
        newACs[0].isTrigger__c='No';
        return;
    }
    for(Aircraft__c newAC : newACs){
    
        
        if(trigger.isInsert){
            if( newAC.Lease__c != null){
                newAC.addError('You cannot assign an aircraft to a lease while adding the aircraft.');
            }
            continue;
        }
        
        Aircraft__c oldAC = Trigger.oldMap.get(newAC.Id);
        if(newAC.Wide_body_Narrow_body__c==null||(newAc.Aircraft_Type__c!=null && !newAc.Aircraft_Type__c.equals(oldAc.Aircraft_Type__c)))
            newAC.Wide_body_Narrow_body__c=leasewareUtils.getBodyType(newAc.Aircraft_Type__c);
        
        if((oldAC.Status__c=='Assigned' && newAC.Status__c !='Assigned' )||(oldAC.Status__c != 'Assigned' && newAC.Status__c =='Assigned')){
            newAC.addError('You can assign an aircraft to a lease or remove one from a lease only on the Lease page.');
            return;
        }
        if(oldAC.Lease__c != newAC.Lease__c){
            newAC.addError('Lease assignments can only be done from the lease detail page. Please go to the corresponding lease page and assign this aircraft to the corresponding lease.');
        }
    }
    
}
*/