trigger CAEETrigger on Assembly_Eligible_Event__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Assembly_Eligible_Event__c.sObjectType);
        
    
}