trigger InvoiceCopyRefInfo on Invoice__c (before insert, before update) {

/*	
	if(LeaseWareUtils.isTriggerDisabled())return;
	
	// for XMLScheduler Job ,need to skip trigger for updating XML_sent__c field
	if(LeaseWareUtils.isFromTrigger('XMLFinSchedulerforInvoice'))return;
	
	if(LeaseWareUtils.isFromTrigger('PaymentAftInsBefDelUpdMR'))return;

	// commenting this code as it is preventing MR snapshot creation at insert.	
    ***if(LeaseWareUtils.isFromTrigger('InvoiceCopyRefInfo')) return;
	LeaseWareUtils.setFromTrigger('InvoiceCopyRefInfo');	***
        
    //Do not allow update of Paid invoice. Only Status can be updated on Approved Invoices. (//TODO check and update if any other fields are updatable) 
    if(trigger.isUpdate){
            
         Map<String, Schema.SObjectField> mapInvField = Schema.SObjectType.Invoice__c.fields.getMap();
         list<String> listFieldsToCheck = new list<String>(); 
         
         for(String Field:mapInvField.keyset()){
            Schema.DescribeFieldResult thisFieldDesc = mapInvField.get(Field).getDescribe();
            System.debug('Field Name is ' + thisFieldDesc.getName() + '. Local ' + thisFieldDesc.getLocalName());
            
            //TODO - Fix rounding issue with leaseworks__Rent_Interest__c and remove it from below list. 
            
            if( !thisFieldDesc.isUpdateable() || 'leaseworks__Date_Paid__c'.equals(thisFieldDesc.getName()) || 'leaseworks__Refresh_Values_From_UR__c'.equals(thisFieldDesc.getName()) 
                || 'leaseworks__Rent_Interest__c'.equals(thisFieldDesc.getName()) )continue; //Add the fields that are always updatable to the list. || ('leaseworks__Comments__c'.equals(thisFieldDesc.getName()))
            
            listFieldsToCheck.add(thisFieldDesc.getName());
         }
         
        for(Invoice__c curInv:trigger.new){
            if('Pending'.equals(trigger.oldMap.get(curInv.id).Status__c) || 'Declined'.equals(trigger.oldMap.get(curInv.id).Status__c))continue; //Pending and Declined can be freely updated.
            if('Paid'.equals(trigger.oldMap.get(curInv.id).Status__c)){
                curInv.addError('Paid Invoice cannot be updated.');
                break;
            }else{ //Approved
                for(String curField:listFieldsToCheck){
                    if(trigger.newMap.get(curInv.id).get(curField) !=  trigger.oldMap.get(curInv.id).get(curField)){ // Value has changed. Only allowed change is Status field, from Approved to Paid.
                        if('leaseworks__Status__c'.equals(curField)){
                            if('Approved'.equals(String.valueOf(trigger.oldMap.get(curInv.id).get(curField))) && 'Paid'.equals(String.valueOf(trigger.newMap.get(curInv.id).get(curField))))continue;
                        }
                        curInv.addError('This is an Approved Invoice. Only allowed change is to set it as Paid. No other changes are allowed.');
                        curInv.addError(curField +' changed from ' + trigger.oldMap.get(curInv.id).get(curField) + ' to ' + trigger.newMap.get(curInv.id).get(curField) + '.');
                        break;
                    }
                }
            }
        }
    }
    
    set<Id> setRefUrIds = new set<Id>();
    set<Id> setRefRentIds = new set<Id>();
    set<Id> setRefAssemblyIds = new set<Id>();
    set<Id> setLeases = new set<ID>();
    map<String, Id> mapInvIds = new map<String, Id>();

    //map<string, MR_Snapshot__c> mapAllMRSnapshots = new map<string, MR_Snapshot__c>(); 
    //list<MR_Snapshot__c> listAllMRSnapshots = new list<MR_Snapshot__c>(); 

    map<Id, Utilization_Report__c> mapUrRefIds = new map<Id, Utilization_Report__c>();
    map<Id, Rent__c> mapRentRefIds = new map<Id, Rent__c>();
    map<Id, Utilization_Report_List_Item__c> mapAssemblyRefIds = new map<Id, Utilization_Report_List_Item__c>();
    boolean bIntInv = false, bAircraftInv=false, bAssemblyInv=false;
    system.debug('checkpoint 1');
    for(Invoice__c curInv:trigger.new){
        if ('Interest'.equals(curInv.Invoice_Type__c) ){
        	bIntInv=true;
        }
        //Do not copy over values for Paid and Approved Invoices and if Refresh Value checkbox is unchecked.
        if( (trigger.isUpdate && 'Paid'.equals(trigger.oldMap.get(curInv.id).Status__c)) || (trigger.isUpdate && 'Approved'.equals(trigger.oldMap.get(curInv.id).Status__c)) || !curInv.Refresh_Values_From_UR__c)
            continue;
        if('Aircraft MR'.equals(curInv.Invoice_Type__c)){
        	bAircraftInv=true;
            setRefUrIds.add(curInv.Utilization_Report__c);
            setLeases.add(curInv.Lease__c);
        }else if('Rent'.equals(curInv.Invoice_Type__c)){
            setRefRentIds.add(curInv.Rent__c);
        }else if('Assembly MR'.equals(curInv.Invoice_Type__c)){
            setRefAssemblyIds.add(curInv.Utilization_Report_List_Item__c);
            setLeases.add(curInv.Lease__c);
        	bAssemblyInv=true;
        }
    }
    
    if(bIntInv){
        Invoice__c[] IntInvs = [select Id, Invoice_Date__c, Lease__c from Invoice__c where lease__c in :setLeases and Invoice_Type__c = 'Interest'];
        for(Invoice__c curInv: IntInvs){
            string key = curInv.Lease__c +'-' + '0000'+String.valueOf(curInv.Invoice_Date__c.year()).right(4)+ '-' + '00' + String.valueOf(curInv.Invoice_Date__c.month()).right(2);
            mapInvIds.put(key, curInv.id);
        }
    }


	***
	if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
		system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
	}else if(bAircraftInv || bAssemblyInv){
        listAllMRSnapshots = [select Id, Month_Ending__c, Lease__c, Invoice__c, Type__c,
							Y_H_NMRInv_APU__c,
							Y_H_NMRInv_Engine_1__c ,
							Y_H_NMRInv_Engine_1_LLP__c ,
							Y_H_NMRInv_Engine_2__c ,
							Y_H_NMRInv_Engine_2_LLP__c ,
							Y_H_NMRInv_Engine_3__c ,
							Y_H_NMRInv_Engine_3_LLP__c ,
							Y_H_NMRInv_Engine_4__c ,
							Y_H_NMRInv_Engine_4_LLP__c ,
							Y_H_NMRInv_Heavy_Maint_1_Airframe__c ,
							Y_H_NMRInv_Heavy_Maint_2_Airframe__c ,
							Y_H_NMRInv_Heavy_Maint_3_Airframe__c ,
							Y_H_NMRInv_Heavy_Maint_4_Airframe__c ,
							Y_H_NMRInv_Landing_Gear_Left_Main__c ,
							Y_H_NMRInv_Landing_Gear_Left_Wing__c ,
							Y_H_NMRInv_Landing_Gear_Nose__c ,
							Y_H_NMRInv_Landing_Gear_Right_Main__c ,
							Y_H_NMRInv_Landing_Gear_Right_Wing__c ,
							
							Y_H_NMRCol_APU__c ,
							Y_H_NMRCol_Engine_1__c ,
							Y_H_NMRCol_Engine_1_LLP__c ,
							Y_H_NMRCol_Engine_2__c ,
							Y_H_NMRCol_Engine_2_LLP__c ,
							Y_H_NMRCol_Engine_3__c ,
							Y_H_NMRCol_Engine_3_LLP__c ,
							Y_H_NMRCol_Engine_4__c ,
							Y_H_NMRCol_Engine_4_LLP__c ,
							Y_H_NMRCol_Heavy_Maint_1_Airframe__c ,
							Y_H_NMRCol_Heavy_Maint_2_Airframe__c ,
							Y_H_NMRCol_Heavy_Maint_3_Airframe__c ,
							Y_H_NMRCol_Heavy_Maint_4_Airframe__c ,
							Y_H_NMRCol_Landing_Gear_Left_Main__c ,
							Y_H_NMRCol_Landing_Gear_Left_Wing__c ,
							Y_H_NMRCol_Landing_Gear_Nose__c ,
							Y_H_NMRCol_Landing_Gear_Right_Main__c ,
							Y_H_NMRCol_Landing_Gear_Right_Wing__c
							        									
							from MR_Snapshot__c 
							where lease__c in :setLeases
							order by lease__c, Month_Ending__c desc];
        for(MR_Snapshot__c curMrSs: listAllMRSnapshots){
			mapAllMRSnapshots.put(curMrSs.Lease__c+'-'+curMrSs.Invoice__c, curMrSs);
        }
    }
	***
    if(setRefUrIds.size()>0){
        mapUrRefIds = new map<Id, Utilization_Report__c>([select Id, Airframe_Cycles_Landing_During_Month__c,
                        Airframe_Flight_Hours_Month__c,
                        Assumed_Estimated__c,
                        True_Up_Gross__c,
                        True_Up__c,
                        True_Up_Type__c,
                        FH_FC_Ratio__c,
                        MR_20_Month_C_Check__c,
                        Maint_Reserve_Heavy_Maint_1_Airframe__c,
                        Maint_Reserve_Heavy_Maint_2_Airframe__c,
                        Maint_Reserve_Heavy_Maint_3_Airframe__c,
                        Maint_Reserve_Heavy_Maint_4_Airframe__c,
                        Maintenance_Reserve_APU__c,
                        Maintenance_Reserve_Engine_1_LLP__c,
                        Maintenance_Reserve_Engine_1__c,
                        Maintenance_Reserve_Engine_2_LLP__c,
                        Maintenance_Reserve_Engine_2__c,
                        Maintenance_Reserve_Engine_3_LLP__c,
                        Maintenance_Reserve_Engine_3__c,
                        Maintenance_Reserve_Engine_4_LLP__c,
                        Maintenance_Reserve_Engine_4__c,
                        Maintenance_Reserve_LG_Center_Main__c,
                        Maintenance_Reserve_LG_Left_Main__c,
                        Maintenance_Reserve_LG_Left_Wing__c,
                        Maintenance_Reserve_LG_Nose__c,
                        Maintenance_Reserve_LG_Right_Main__c,
                        Maintenance_Reserve_LG_Right_Wing__c,
                        Maintenance_Reserve_Landing_Gear__c,
                        Maintenance_Reserve_Other__c, Period__c,
                        Month_Ending__c, Status__c, Total_Maintenance_Reserve__c,
                        
						Aircraft__r.Maintenance_Reserve_APU__c,
						Aircraft__r.Maintenance_Reserve_Engine_1__c,
						Aircraft__r.Maintenance_Reserve_Engine_1_LLP__c,
						Aircraft__r.Maintenance_Reserve_Engine_2__c,
						Aircraft__r.Maintenance_Reserve_Engine_2_LLP__c,
						Aircraft__r.Maintenance_Reserve_Engine_3__c,
						Aircraft__r.Maintenance_Reserve_Engine_3_LLP__c,
						Aircraft__r.Maintenance_Reserve_Engine_4__c,
						Aircraft__r.Maintenance_Reserve_Engine_4_LLP__c,
						Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_1__c,
						Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_2__c,
						Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_3__c,
						Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_4__c,
						Aircraft__r.Maintenance_Reserve_Landing_Gear__c,
						Aircraft__r.Maintenance_Reserve_LG_Left_Main__c,
						Aircraft__r.Maintenance_Reserve_LG_Left_Wing__c,
						Aircraft__r.Maintenance_Reserve_LG_Nose__c,
						Aircraft__r.Maintenance_Reserve_LG_Right_Main__c,
						Aircraft__r.Maintenance_Reserve_LG_Right_Wing__c,
						Aircraft__r.Maintenance_Reserve_TOTAL__c
                        
                        from Utilization_Report__c where Id in :setRefUrIds]);
    }
    if(setRefRentIds.size()>0){
        mapRentRefIds = new map<Id, Rent__c>([select Id, Escalation_Factor__c,
            Flight_Cycles_For_Month__c,
            Flight_Hours_For_Month__c,
            For_Month_Ending__c,
            PBH_Rate_Applicable__c,
            Prorate__c,
            Net_Rent__c,
            Rent_Due_Date__c,
            Rent_Interest__c,
            Total_Due__c,
            Total_Engine_Cycles__c, Period_Since_Lease_Start__c,
            Total_Engine_Hours__c from Rent__c where Id in :setRefRentIds]);
    }
    
    if(setRefAssemblyIds.size()>0){
        mapAssemblyRefIds = new map<Id, Utilization_Report_List_Item__c>([select Id, Cycles_During_Month__c,
            Running_Hours_During_Month__c,
            For_The_Month_Ending__c,
            Maintenance_Reserve__c,
            Maintenance_Reserve_LLP__c,
            Total_Maintenance_Reserve__c,
            Utilization_Report__r.Period__c,
            Utilization_Report__r.Status__c, Utilization_Report__r.Assumed_Estimated__c,Utilization_Report__r.True_Up_Gross__c, Assumed_Estimated__c,True_Up_Gross__c,
            Utilization_Report__r.True_Up_Type__c,True_Up_Type__c,
            Utilization_Report__r.True_Up__c,
            Constituent_Assembly__r.Maintenance_Reserve__c, Constituent_Assembly__r.Maintenance_Reserve_LLP__c,
            Constituent_Assembly__r.Total_Maintenance_Reserve__c 
            
            from Utilization_Report_List_Item__c where Id in :setRefAssemblyIds]);
    }

	//list<MR_Snapshot__c> listMRSsToUpsert = new list<MR_Snapshot__c>(); 
	system.debug('checkpoint 2');
    for(Invoice__c curInv:trigger.new){

        Lease__c curLease;
        string prevLeaseId;
        if(curLease == null || prevLeaseId ==null || !curInv.lease__c.equals(prevLeaseId)){
            curLease = [select Id, Lease_Start_Date__c, Days_payment_due_after_invoice__c, Security_Deposit__c,
            			Pct_Maintenance_Reserve__c, Pct_Revenue__c, Pct_AR_Account__c, Pct_AR_Sub_Account__c  from Lease__c where id = :curInv.Lease__c limit 1];
            prevLeaseId=curInv.lease__c;
        }

        //Do not update Approved/Paid invoices. Update only if Refresh flag is checked.     
        if(trigger.isUpdate && ( 'Paid'.equals(trigger.oldMap.get(curInv.id).Status__c) || 'Approved'.equals(trigger.oldMap.get(curInv.id).Status__c)
             || !curInv.Refresh_Values_From_UR__c))continue;


		***MR_Snapshot__c CurMrSs, prevMrSS;
		
		if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
			system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
		}else{
			CurMrSs=null; 
			prevMrSS=null;
			for(MR_Snapshot__c curSS: listAllMRSnapshots){
				if(curSS.Invoice__c == curInv.id)continue;
				if(curSS.Month_Ending__c > curInv.Month_Ending__c)listMRSsToUpsert.add(curSS);
				if(prevMrSS==null){
					if(curSS.Month_Ending__c < curInv.Month_Ending__c)prevMrSS=curSS;
					continue;
				}
				if(curSS.Invoice__c != curInv.id && curSS.Month_Ending__c < curInv.Month_Ending__c && prevMrSS.Month_Ending__c <curSS.Month_Ending__c)prevMrSS=curSS;    
			}
		}***

        if('Aircraft MR'.equals(curInv.Invoice_Type__c)){

            if(curInv.Utilization_Report__c==null){
                curInv.Utilization_Report__c.addError('Please select a Monthly Utilization.');
                continue;
            }
            Utilization_Report__c curUR = mapUrRefIds.get(curInv.Utilization_Report__c);
            if(curUR==null){
                if(trigger.isInsert){
                    curInv.addError('Error - Cannot find the Monthly Utilization to refresh values from. Refresh Values checkbox must be checked while creating the invoice.');
                }else{
                    curInv.addError('Error - Cannot find the Monthly Utilization to refresh values from. Monthly Utilization Id is '+curInv.Utilization_Report__c);
                }
                continue;
            }

            if(trigger.isInsert && !curUR.Assumed_Estimated__c && !'Approved By Lessor'.equals(curUR.Status__c)){
                curInv.addError('Error - Invoice can be created only after the Monthly Utilization is approved.');
                continue;
            }

            boolean bAssumedExists=false;
            Invoice__c AssumedInvoice;
            if(!curUR.Assumed_Estimated__c && !curUR.True_Up_Gross__c && !curUR.True_Up__c){ //Actual. Check if there is a corresponding Assumed Invoice.
                try{
                    AssumedInvoice = [SELECT Id, Amount__c, Utilization_Report__c FROM invoice__c WHERE Lease__c = :curInv.Lease__c 
                    AND Month_Ending__c = :curUR.Month_Ending__c AND Invoice_Type__c = 'Aircraft MR' and id != :curInv.Id
                    AND (Assumed_Estimated__c = true) limit 1];
                    bAssumedExists=true;
                }catch(exception e){
                    bAssumedExists=false;
                }
            }
            // True_Up_Gross__c
            if(curUR.True_Up_Gross__c && !'Incremental'.equals(curUR.True_Up_Type__c)){ //Actual. Check if there is a corresponding Original Invoice before True Up Gross.
                try{
                    AssumedInvoice = [SELECT Id, Amount__c, Utilization_Report__c FROM invoice__c WHERE Lease__c = :curInv.Lease__c 
                    AND Month_Ending__c = :curUR.Month_Ending__c AND Invoice_Type__c = 'Aircraft MR'  and id != :curInv.Id
                    AND Assumed_Estimated__c = false and Utilization_Report__r.True_Up__c=false and True_Up_Gross__c=false limit 1];
                    bAssumedExists=true; // exist Prev Invoice for True Up Gross
                }catch(exception e){
                    bAssumedExists=false;
                }
            }
            if(bAssumedExists){

system.debug('Found corresponding assumed invoice - ' + AssumedInvoice.Id);
                curInv.Adjustment_1__c=-AssumedInvoice.Amount__c;
                curInv.Reason_Code_1__c='Credits';
                curInv.Assumed_Monthly_Utilization__c=AssumedInvoice.Utilization_Report__c;
                curInv.Assumed_Estimated_Invoice__c=AssumedInvoice.Id;
            }

            curInv.Airframe_Cycles_Landing_During_Month__c=curUR.Airframe_Cycles_Landing_During_Month__c;
            curInv.Airframe_Flight_Hours_Month__c=curUR.Airframe_Flight_Hours_Month__c;
            curInv.MR_20_Month_C_Check__c=curUR.MR_20_Month_C_Check__c;
            curInv.Maint_Reserve_Heavy_Maint_1_Airframe__c=curUR.Maint_Reserve_Heavy_Maint_1_Airframe__c;
            curInv.Maint_Reserve_Heavy_Maint_2_Airframe__c=curUR.Maint_Reserve_Heavy_Maint_2_Airframe__c;
            curInv.Maint_Reserve_Heavy_Maint_3_Airframe__c=curUR.Maint_Reserve_Heavy_Maint_3_Airframe__c;
            curInv.Maint_Reserve_Heavy_Maint_4_Airframe__c=curUR.Maint_Reserve_Heavy_Maint_4_Airframe__c;
            curInv.Maintenance_Reserve_APU__c=curUR.Maintenance_Reserve_APU__c;
            curInv.Maintenance_Reserve_Engine_1_LLP__c=curUR.Maintenance_Reserve_Engine_1_LLP__c;
            curInv.Maintenance_Reserve_Engine_1__c=curUR.Maintenance_Reserve_Engine_1__c;
            curInv.Maintenance_Reserve_Engine_2_LLP__c=curUR.Maintenance_Reserve_Engine_2_LLP__c;
            curInv.Maintenance_Reserve_Engine_2__c=curUR.Maintenance_Reserve_Engine_2__c;
            curInv.Maintenance_Reserve_Engine_3_LLP__c=curUR.Maintenance_Reserve_Engine_3_LLP__c;
            curInv.Maintenance_Reserve_Engine_3__c=curUR.Maintenance_Reserve_Engine_3__c;
            curInv.Maintenance_Reserve_Engine_4_LLP__c=curUR.Maintenance_Reserve_Engine_4_LLP__c;
            curInv.Maintenance_Reserve_Engine_4__c=curUR.Maintenance_Reserve_Engine_4__c;
            curInv.Maintenance_Reserve_LG_Center_Main__c=curUR.Maintenance_Reserve_LG_Center_Main__c;
            curInv.Maintenance_Reserve_LG_Left_Main__c=curUR.Maintenance_Reserve_LG_Left_Main__c;
            curInv.Maintenance_Reserve_LG_Left_Wing__c=curUR.Maintenance_Reserve_LG_Left_Wing__c;
            curInv.Maintenance_Reserve_LG_Nose__c=curUR.Maintenance_Reserve_LG_Nose__c;
            curInv.Maintenance_Reserve_LG_Right_Main__c=curUR.Maintenance_Reserve_LG_Right_Main__c;
            curInv.Maintenance_Reserve_LG_Right_Wing__c=curUR.Maintenance_Reserve_LG_Right_Wing__c;
            curInv.Maintenance_Reserve_Landing_Gear__c=curUR.Maintenance_Reserve_Landing_Gear__c;
            curInv.Maintenance_Reserve_Other__c=curUR.Maintenance_Reserve_Other__c;
            curInv.Month_Ending__c=curUR.Month_Ending__c;
            curInv.Amount__c=curUR.Total_Maintenance_Reserve__c;
            curInv.Period_Calculated__c=curUR.Period__c;
            curInv.Assumed_Estimated__c=curUR.Assumed_Estimated__c;// Dont know whether need to add for true Up gross
			curInv.True_Up_Gross__c=curUR.True_Up_Gross__c;  // anjani : No need to add True type Type, I am using formula field
            curInv.Date_of_MR_Payment_Due__c=curInv.Invoice_Date__c.addDays(leasewareUtils.zeroIfNull((integer)curLease.Days_payment_due_after_invoice__c));
			curInv.Pct_Maintenance_Reserve__c=curLease.Pct_Maintenance_Reserve__c;
			curInv.Pct_Revenue__c=curLease.Pct_Revenue__c;
			curInv.Pct_AR_Account__c=curLease.Pct_AR_Account__c;
			curInv.Pct_AR_Sub_Account__c=curLease.Pct_AR_Sub_Account__c;
			if(curInv.Accounting_Description__c==null){
				curInv.Accounting_Description__c='Additional Rent '+curInv.Name+' ' + curInv.Invoice_Date__c.format();
			}

			system.debug('checkpoint 2.1' );
			***
			if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
				system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
			}else if(trigger.isUpdate){
				if(mapAllMRSnapshots.containsKey(curInv.Lease__c+'-'+curInv.Id)) CurMrSs = mapAllMRSnapshots.get(curInv.Lease__c+'-'+curInv.Id);  
				if(CurMrSs==null){
					CurMrSs = new MR_Snapshot__c();
					if(prevMrSS!=null)CurMrSs=prevMrSS.clone();
					//Prev MR may not be of same type. Need to set the below fields.
					CurMrSs.Name='Invoice - ' + curInv.Month_Ending__c.format();
					CurMrSs.lease__c=curInv.lease__c; 
					CurMrSs.invoice__c = curInv.Id;
	
	system.debug('Inv Id is ' + CurMrSs.invoice__c);
	
					CurMrSs.Month_Ending__c=curInv.Month_Ending__c;
					CurMrSs.Type__c = 'Aircraft MR Invoice';
				}
				system.debug('checkpoint 2.2' ); 
				if(prevMrSS == null){
					
					CurMrSs.Y_H_NMRInv_APU__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_APU__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_APU__c);
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_1__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_2__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_2__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2__c);
					CurMrSs.Y_H_NMRInv_Engine_2_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_3__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_3__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3__c);
					CurMrSs.Y_H_NMRInv_Engine_3_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_4__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_4__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4__c);
					CurMrSs.Y_H_NMRInv_Engine_4_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4_LLP__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_1__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_2__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_3__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_4__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Wing__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Nose__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Nose__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Wing__c);
		
					CurMrSs.Y_H_NMRCol_APU__c = curUR.Aircraft__r.Maintenance_Reserve_APU__c;
					CurMrSs.Y_H_NMRCol_Engine_1__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_1__c;
					CurMrSs.Y_H_NMRCol_Engine_1_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_1_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_2__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_2__c;
					CurMrSs.Y_H_NMRCol_Engine_2_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_2_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_3__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_3__c;
					CurMrSs.Y_H_NMRCol_Engine_3_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_3_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_4__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_4__c;
					CurMrSs.Y_H_NMRCol_Engine_4_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_4_LLP__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_1__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_2__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_3__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_4__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Main__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Main__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Wing__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Nose__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Nose__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Main__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Main__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Wing__c;
	
				}else{
	
					CurMrSs.Y_H_NMRInv_APU__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_APU__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_APU__c);
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_2__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_2__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2__c);
					CurMrSs.Y_H_NMRInv_Engine_2_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_3__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_3__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3__c);
					CurMrSs.Y_H_NMRInv_Engine_3_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_4__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_4__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4__c);
					CurMrSs.Y_H_NMRInv_Engine_4_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4_LLP__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_1_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_2_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_3_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_4_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Left_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Left_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Wing__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Nose__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Nose__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Right_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Right_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Wing__c);
					
				}
				system.debug('checkpoint 2.3' );
				mapAllMRSnapshots.put(curInv.Lease__c+'-'+curMrSs.Invoice__c, curMrSs);
				listAllMRSnapshots.add(curMrSs);
				system.debug('checkpoint 2.4' );
				for(MR_Snapshot__c thisMRSs: listMRSsToUpsert){
					thisMRSs.Y_H_NMRInv_APU__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_APU__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_APU__c);
					thisMRSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_1__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1__c);
					thisMRSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1_LLP__c);
					thisMRSs.Y_H_NMRInv_Engine_2__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_2__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2__c);
					thisMRSs.Y_H_NMRInv_Engine_2_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2_LLP__c);
					thisMRSs.Y_H_NMRInv_Engine_3__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_3__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3__c);
					thisMRSs.Y_H_NMRInv_Engine_3_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3_LLP__c);
					thisMRSs.Y_H_NMRInv_Engine_4__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_4__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4__c);
					thisMRSs.Y_H_NMRInv_Engine_4_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4_LLP__c);
					thisMRSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_1_Airframe__c);
					thisMRSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_2_Airframe__c);
					thisMRSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_3_Airframe__c);
					thisMRSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_4_Airframe__c);
					thisMRSs.Y_H_NMRInv_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Landing_Gear_Left_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Main__c);
					thisMRSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Wing__c);
					thisMRSs.Y_H_NMRInv_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Landing_Gear_Nose__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Nose__c);
					thisMRSs.Y_H_NMRInv_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Landing_Gear_Right_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Main__c);
					thisMRSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Wing__c);
					
				}
				system.debug('checkpoint 3' + listMRSsToUpsert.size());
				listMRSsToUpsert.add(curMrSs);
				system.debug('checkpoint 4' + listMRSsToUpsert.size());
			}			
			***
//          curInv.Rent_Due_Date__c=curInv.Date_of_MR_Payment_Due__c;


//          if(trigger.isInsert && curInv.Invoiced_Amount__c==null)curInv.Invoiced_Amount__c=curUR.Total_Maintenance_Reserve__c;

        }else if('Rent'.equals(curInv.Invoice_Type__c)){
            if(curInv.Rent__c==null){
                curInv.Rent__c.addError('Please select a Rent.');
                continue;
            }
            Rent__c curRent = mapRentRefIds.get(curInv.Rent__c);
            if(curRent==null){
                if(trigger.isInsert){
                    curInv.addError('Error - Error - Cannot find the Rent to refresh values from. Refresh Values checkbox must be checked while creating the invoice.');
                }else{
                    curInv.addError('Error - Cannot find the Rent to refresh values from. Rent Id is '+curInv.Rent__c);
                }
                continue;
            }

            curInv.Rent_Escalation_Factor__c=curRent.Escalation_Factor__c;
            curInv.Flight_Cycles_For_Month__c=curRent.Flight_Cycles_For_Month__c;
            curInv.Flight_Hours_For_Month__c=curRent.Flight_Hours_For_Month__c;
            curInv.Month_Ending__c=curRent.For_Month_Ending__c;
            curInv.PBH_Rate_Applicable__c=curRent.PBH_Rate_Applicable__c;
            curInv.Prorate__c=curRent.Prorate__c;
            curInv.Rent_Due_Amount__c=curRent.Total_Due__c; // Rent due + Intereset
            curInv.Amount__c=curRent.Net_Rent__c; // Rent due + Intereset - MRO
//          if(trigger.isInsert && curInv.Invoiced_Amount__c==null)curInv.Invoiced_Amount__c=curRent.Net_Rent__c;
//          curInv.Rent_Due_Date__c=curRent.Rent_Due_Date__c;
            curInv.Date_of_MR_Payment_Due__c=curRent.Rent_Due_Date__c;
            curInv.Rent_Interest__c=curRent.Rent_Interest__c;
            curInv.Total_Engine_Cycles__c=curRent.Total_Engine_Cycles__c;
            curInv.Total_Engine_Hours__c=curRent.Total_Engine_Hours__c;
            curInv.Period_Calculated__c=curRent.Period_Since_Lease_Start__c;
        }else if('Assembly MR'.equals(curInv.Invoice_Type__c)){
            if(curInv.Utilization_Report_List_Item__c==null){
                    curInv.Utilization_Report_List_Item__c.addError('Please select an Assembly Utilization.');
                continue;
            }

            Utilization_Report_List_Item__c curAsmblyUR = mapAssemblyRefIds.get(curInv.Utilization_Report_List_Item__c);
            if(curAsmblyUR==null){
                if(trigger.isInsert){
                    curInv.addError('Error - Cannot find the Assembly Utilization to refresh values from. Refresh Values checkbox must be checked while creating the invoice.');
                }else{
                    curInv.addError('Error - Cannot find the Assembly Utilization to refresh values from. Assembly Utilization Id is '+curInv.Utilization_Report_List_Item__c);
                }
                continue;
            }
            if(trigger.isInsert && !curAsmblyUR.Utilization_Report__r.Assumed_Estimated__c && !'Approved By Lessor'.equals(curAsmblyUR.Utilization_Report__r.Status__c)){
                curInv.addError('Error - Invoice can be created only after the Utilization is approved.');
                continue;
            }

            boolean bAssumedExists=false;
            Invoice__c AssumedInvoice;
            if(!curAsmblyUR.Assumed_Estimated__c && !curAsmblyUR.True_Up_Gross__c && !curAsmblyUR.Utilization_Report__r.True_Up__c){ //Actual. Check if there is a corresponding Assumed Invoice.
                try{
                    AssumedInvoice = [SELECT Id, Amount__c, Utilization_Report_List_Item__c FROM invoice__c WHERE Lease__c = :curInv.Lease__c 
                    AND Month_Ending__c = :curAsmblyUR.For_The_Month_Ending__c  AND Invoice_Type__c = 'Assembly MR' and id != :curInv.Id
                    AND Assumed_Estimated__c = true limit 1];
                    bAssumedExists=true;
                }catch(exception e){
                    bAssumedExists=false;
                }
            }
            //True_Up_Gross__c at Assembly Level
            if(curAsmblyUR.True_Up_Gross__c && !'Incremental'.equals(curAsmblyUR.True_Up_Type__c)){ //Actual. Check if there is a corresponding Assumed Invoice.
                try{
                    AssumedInvoice = [SELECT Id, Amount__c, Utilization_Report_List_Item__c FROM invoice__c WHERE Lease__c = :curInv.Lease__c 
                    AND Month_Ending__c = :curAsmblyUR.For_The_Month_Ending__c  AND Invoice_Type__c = 'Assembly MR' and id != :curInv.Id
                    AND Assumed_Estimated__c = false and Utilization_Report_List_Item__r.Utilization_Report__r.True_Up__c=false and True_Up_Gross__c=false limit 1];
                    bAssumedExists=true;
                    
                }catch(exception e){
                    bAssumedExists=false;
                }
            }  
          
//          

            curInv.Flight_Cycles_For_Month__c=curAsmblyUR.Cycles_During_Month__c;
            curInv.Flight_Hours_For_Month__c=curAsmblyUR.Running_Hours_During_Month__c;
            curInv.Month_Ending__c=curAsmblyUR.For_The_Month_Ending__c;
            curInv.Amount__c=curAsmblyUR.Total_Maintenance_Reserve__c;
            if(bAssumedExists){
                curInv.Adjustment_1__c=-AssumedInvoice.Amount__c;
                curInv.Reason_Code_1__c='Credits';
                curInv.Assumed_Assembly_Utilization__c=AssumedInvoice.Utilization_Report_List_Item__c;
                curInv.Assumed_Estimated_Invoice__c=AssumedInvoice.Id;
            }
            curInv.Assumed_Estimated__c=curAsmblyUR.Assumed_Estimated__c;
            curInv.True_Up_Gross__c=curAsmblyUR.True_Up_Gross__c;
//          if(trigger.isInsert && curInv.Invoiced_Amount__c==null)curInv.Invoiced_Amount__c=curAsmblyUR.Total_Maintenance_Reserve__c;
            curInv.Date_of_MR_Payment_Due__c=curInv.Invoice_Date__c.addDays(leasewareUtils.zeroIfNull((integer)curLease.Days_payment_due_after_invoice__c));
//          curInv.Rent_Due_Date__c=curInv.Date_of_MR_Payment_Due__c;

            curInv.Maintenance_Reserve__c=curAsmblyUR.Maintenance_Reserve__c;
            curInv.Maintenance_Reserve_LLP__c=curAsmblyUR.Maintenance_Reserve_LLP__c;
            curInv.Period_Calculated__c=curAsmblyUR.Utilization_Report__r.Period__c;
			curInv.Pct_Maintenance_Reserve__c=curLease.Pct_Maintenance_Reserve__c;
			curInv.Pct_Revenue__c=curLease.Pct_Revenue__c;
			curInv.Pct_AR_Account__c=curLease.Pct_AR_Account__c;
			curInv.Pct_AR_Sub_Account__c =curLease.Pct_AR_Sub_Account__c;
			if(curInv.Accounting_Description__c==null){
				curInv.Accounting_Description__c='Additional Rent '+curInv.Name+' ' + curInv.Invoice_Date__c.format();
			}
			***
			if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
				system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
			}else if(trigger.isUpdate){

				CurMrSs = mapAllMRSnapshots.get(curInv.Lease__c+'-'+curInv.Id);  
				if(CurMrSs==null)CurMrSs = new MR_Snapshot__c(Name='Invoice - ' + curInv.Invoice_Date__c.format(), 
						lease__c=curInv.lease__c, invoice__c = curInv.Id, Month_Ending__c=curInv.Invoice_Date__c, Type__c = 'Assembly MR Invoice'); 
				if(prevMrSS == null){
					
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(curAsmblyUR.Constituent_Assembly__r.Maintenance_Reserve__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(curAsmblyUR.Constituent_Assembly__r.Maintenance_Reserve_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LLP__c);
		
					CurMrSs.Y_H_NMRCol_Engine_1__c = leasewareUtils.zeroIfNull(curAsmblyUR.Constituent_Assembly__r.Maintenance_Reserve__c);
					CurMrSs.Y_H_NMRCol_Engine_1_LLP__c = leasewareUtils.zeroIfNull(curAsmblyUR.Constituent_Assembly__r.Maintenance_Reserve_LLP__c);
	
				}else{
	
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LLP__c);
					
				}
				mapAllMRSnapshots.put(curInv.Lease__c+'-'+curMrSs.Invoice__c, curMrSs);
				listAllMRSnapshots.add(curMrSs);
				
				listMRSsToUpsert.add(curMrSs);
			}
			***
        }else if('Interest'.equals(curInv.Invoice_Type__c)){ //Interest

            string key = curInv.Lease__c +'-' + '0000'+String.valueOf(curInv.Invoice_Date__c.year()).right(4)+ '-' + '00' + String.valueOf(curInv.Invoice_Date__c.month()).right(2);
            if(mapInvIds.containsKey(key) && mapInvIds.get(key) != curInv.id){
                curInv.addError('Only one Interest Invoice can be created for a month. (Id - ' + mapInvIds.get(key) + ')' );
                continue;
            }
            mapInvIds.put(key, curInv.id);

            if(curInv.Amount__c==null || curInv.Refresh_Values_From_UR__c){ // Update interest only if Refresh value checkbox is checked or Amount is null
                if(curInv.Invoice_Date__c==null)curInv.Invoice_Date__c=date.today();
                date thisMonth = Date.newInstance(curInv.Invoice_Date__c.year(), curInv.Invoice_Date__c.month(), 1);
                try{
                    AggregateResult aggOutstanding = [select sum(Balance_Due__c) TotBalDue from Invoice__c where Lease__c = :curInv.Lease__c and Invoice_Date__c < :thisMonth];
                    curInv.Outstanding_Balance_As_Of_Previous_Month__c=aggOutstanding.get('TotBalDue')==null?0:(decimal)aggOutstanding.get('TotBalDue');
                    curInv.Amount__c=curInv.Outstanding_Balance_As_Of_Previous_Month__c * (leasewareUtils.zeroIfNull([select Late_Payment_Interest__c from lease__c where id = :curInv.Lease__c][0].Late_Payment_Interest__c)/100/12);
                }catch(exception e){
                    system.debug('Exception while calculating invoice amount. ' + e );
                    curInv.Amount__c=0;
                }
            }
        }else if('Security Deposit'.equals(curInv.Invoice_Type__c)){ //SD
            integer nPeriod;
            nPeriod=curInv.Invoice_Date__c.Month() + 12 - curLease.Lease_Start_Date__c.date().Month() + 12 * (curInv.Invoice_Date__c.Year() - curLease.Lease_Start_Date__c.date().Year() -1 ) + 1;
            curInv.Period_Calculated__c=nPeriod;
            curInv.Amount__c=curLease.Security_Deposit__c; // Get SD from Lease.
            
        }else{ //Other and SD
            integer nPeriod;
            nPeriod=curInv.Invoice_Date__c.Month() + 12 - curLease.Lease_Start_Date__c.date().Month() + 12 * (curInv.Invoice_Date__c.Year() - curLease.Lease_Start_Date__c.date().Year() -1 ) + 1;
            curInv.Period_Calculated__c=nPeriod;
        }//Inv Type

    }//for Inv in trigger.new
    
    //if(listMRSsToUpsert.size()>0)upsert listMRSsToUpsert;
    
 */   
}