trigger ProspectTermTrigger on MSN_s_Of_Interest__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(MSN_s_Of_Interest__c.sObjectType);    
    
}