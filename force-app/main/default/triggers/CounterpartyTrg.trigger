//@lastupdated : 2016.01.20 3:30PM
trigger CounterpartyTrg on Counterparty__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
  if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Counterparty__c.sObjectType);
}