/* 
* Created by A.K.G on 07/03/2019
*/
trigger PaymentStudioTrigger on Payment_Receipt__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  
    if(LeaseWareUtils.isTriggerDisabled()) return;
    TriggerFactory.createHandler(Payment_Receipt__c.sObjectType);
    
}