trigger DealTeamTrigger on Deal_Team__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Deal_Team__c.sObjectType);
    
     
    
}