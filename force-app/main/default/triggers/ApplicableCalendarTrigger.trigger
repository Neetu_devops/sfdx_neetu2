trigger ApplicableCalendarTrigger on Applicable_Calendar__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {


if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Applicable_Calendar__c.sObjectType);
  
  
}