trigger AdminDataValidationTrigger on Admin_Data_Validation__c (after update) {
    if( LeaseWareUtils.isTriggerDisabled() )return;
    BackgroundDataValidation.executeQuickAction();

}