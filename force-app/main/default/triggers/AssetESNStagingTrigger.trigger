trigger AssetESNStagingTrigger on Asset_ESN_Staging__c (after insert) {

	list<Assembly_Stage__c> listESNs = new list<Assembly_Stage__c>();
    for(Asset_ESN_Staging__c curESN : trigger.new){
    	if(curESN.Manufacturer_Company_Short_Name__c==null || curESN.Component_Serial_Number__c==null){
    		system.debug('Skipping ' + curESN.Id + '-' + curESN.Name);  
    		continue;
    	}
      	listESNs.add(new Assembly_Stage__c(Name=curESN.Manufacturer_Company_Short_Name__c + '-'+curESN.Component_Serial_Number__c,
											Engine_ID__c=curESN.Manufacturer_Company_Short_Name__c + '-'+curESN.Component_Serial_Number__c,
											Serial_Number__c=curESN.Component_Serial_Number__c,
											Status__c=curESN.Component_Status__c,
											CSLV__c=curESN.CSLV__c,
											CSN__c=curESN.CSN__c,
											Chosen_Thrust__c=curESN.Current_Thrust__c,
											Date_of_Manufacture__c=(curESN.DOM__c==null?null:curESN.DOM__c.date()),
											Lookup_Lessee__c=curESN.Lessee_Company_Short_Name__c,										
											Location__c=curESN.Location__c,
											Manufacturer_List__c=curESN.Manufacturer_Company_Short_Name__c,
											Type__c=curESN.Position__c,
											TSLV__c=curESN.TSLV__c,
											TSN__c=curESN.TSN__c
      									 ));
    }
    insert listESNs;
    
}