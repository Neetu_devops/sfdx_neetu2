trigger DelinquencyStagingTrigger on Delinquency_Staging__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
 system.debug('DelinquencyStagingTrigger+');   
 if(LWGlobalUtils.isTriggerDisabled()) return; 
 TriggerFactory.createHandler(Delinquency_Staging__c.sObjectType);      
 system.debug('DelinquencyStagingTrigger-');   
}