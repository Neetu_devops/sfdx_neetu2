trigger PortalAssetStageProcessData on Portal_Asset_Staging__c (after insert) {
    
    Map<String, Schema.SObjectField> mapPortalAssetsStageFields = Schema.SObjectType.Portal_Asset_Staging__c.fields.getMap();
    list<Portal_Asset_Staging__c> newList = (list<Portal_Asset_Staging__c>)trigger.new;
    map<String,Portal_Asset_Staging__c> mapPortalAssetStaging = new map<String,Portal_Asset_Staging__c>();
    map<String,portal_asset__c> mapExistingPortalAsset = new   map<String,portal_asset__c>();
    map<String,portal_asset__c> mapToInsetUpdate = new  map<String,portal_asset__c> ();
    map<string,Portal_Asset_Template__c> mapPortalAssetTemp = new  map<string,Portal_Asset_Template__c>();
    List<Mapping__c> listVariantMap = [Select Name,Map_To__c from Mapping__c where IsActive__c = true];
    map<String,aircraft__c> mapAircrafts = new map<String,aircraft__c>(); 

    system.debug('Size of list from trigger: portal Stage --- '+newList.size());
    string strQry = 'select ';
    for(String Field:mapPortalAssetsStageFields.keyset()){
        Schema.DescribeFieldResult thisFieldDesc = mapPortalAssetsStageFields.get(Field).getDescribe();
        if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue;
        strQry+= (thisFieldDesc.getName() + ', ');     
    } 
    strQry = strQry.left(strQry.length()-2);
    strQry+=' from ' + Schema.SObjectType.Portal_Asset__c.getName() + ' where Asset_Class__c !=\'Engine Module\'';
    system.debug('strQry===='+strQry);
    list<portal_asset__c> listAllPortalAssets =  database.query(strQry);
    system.debug('listAllPortalAssets.size()---'+listAllPortalAssets.size());
    
    for(Portal_Asset_Template__c passetTemp : [select id , Asset_Type__c, Asset_Variant__c from Portal_Asset_Template__c ]){
        mapPortalAssetTemp.put(passetTemp.Asset_Type__c+'-'+passetTemp.Asset_Variant__c,passetTemp);
    }
    for(aircraft__c tempAC :[select id,name,Aircraft_ID__c from aircraft__c where Aircraft_ID__c!=null ]){
        if(!mapAircrafts.containsKey(tempAc.Aircraft_ID__c)){
            mapAircrafts.put(tempAC.Aircraft_ID__c,tempAC);
        }
        
    }
   
    for(Portal_Asset_Staging__c pas : newList){//to avoid duplicates if any from staging/incoming data
        if(String.isNotBlank(pas.external_id__c))mapPortalAssetStaging.put(pas.external_id__c,pas);
        else if(String.isNotBlank(pas.internal_id__c))mapPortalAssetStaging.put(pas.internal_id__c,pas);
    }
    for(portal_asset__c pa : listAllPortalAssets){
        if(String.isNotBlank(pa.external_id__c)){mapExistingPortalAsset.put(pa.external_id__c,pa);}//LWAssets -->assetNo
        if(String.isNotBlank(pa.internal_id__c)){mapExistingPortalAsset.put(pa.internal_id__c,pa);}//strAcType + '-' + strCurMSN;
    }
    system.debug('Size of list  mapPortalAssetStaging --- '+ mapPortalAssetStaging.size());
    for(Portal_Asset_Staging__c pastStg : mapPortalAssetStaging.values()){
        system.debug('pastStg Internal ID---'+ pastStg.internal_id__c);
        system.debug('pastStg External ID---'+ pastStg.external_id__c);
        system.debug('pastStg Asset_Type__c---'+ pastStg.Asset_Type__c);
        system.debug('pastStg Asset_Variant__c---'+ pastStg.Asset_Variant__c);
        portal_asset__c passet = null;
        List<String> listTypeVariant = LeasewareUtils.setAssetTypeVariant(listVariantMap, pastStg.Asset_Variant__c.trim());

        if(mapExistingPortalAsset.containsKey(pastStg.external_id__c) || mapExistingPortalAsset.containsKey(pastStg.internal_id__c)){
            passet = mapExistingPortalAsset.get(pastStg.external_id__c) == null? mapExistingPortalAsset.get(pastStg.internal_id__c):mapExistingPortalAsset.get(pastStg.external_id__c);  
            system.debug('Match Found----Updating----');
        }
        else{
            passet = new portal_asset__c();
            system.debug('----Inserting-----');
        }
        for(String Field:mapPortalAssetsStageFields.keyset()){
            Schema.DescribeFieldResult thisFieldDesc = mapPortalAssetsStageFields.get(Field).getDescribe();
            if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue; //Skip all system fields like createdBy.
            if(!thisFieldDesc.isCustom() && !Field.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
            passet.put(thisFieldDesc.getName(), pastStg.get(Field));
        }
         system.debug('passet Internal ID---'+ passet.internal_id__c);
         system.debug('passet External ID---'+ passet.external_id__c);
         passet.Portal_Asset_Template__c = mapPortalAssetTemp.get(pastStg.Asset_Type__c+'-'+pastStg.Asset_Variant__c)?.id;
         passet.link_to_asset__c =  mapAircrafts.get(pastStg.internal_id__c)?.id;
         passet.Asset_Type__c = listTypeVariant[0];
         String key = String.isBlank(pastStg.External_Id__c)?pastStg.internal_id__c :pastStg.External_Id__c;
         mapToInsetUpdate.put(key,passet);
    }

    system.debug('Upserting mapToInsetUpdate.size()--- '+ mapToInsetUpdate.size());
    database.UpsertResult[] res = new List<database.UpsertResult>();
    String errormsgForAssetType='';
    String errormsgForAssetVariant='';
        if(mapToInsetUpdate.size()>0){
            res =  database.upsert(mapToInsetUpdate.Values(),false) ; 
            for(database.UpsertResult sr : res) {
                if (!sr.isSuccess()) {
                    for (Database.Error err:sr.getErrors()) {
                        system.debug('Err code::'+err.getStatusCode());
                        system.debug('Err Message::'+err.getMessage());
                        system.debug('Fields that affected this error: ' + err.getFields() );
                        if(err.getStatusCode() == StatusCode.INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST){
                            String[] errorMsgSplit = err.getMessage().split(':');
                            if(errorMsgSplit.size()<3)continue;
                            if(err.getFields().contains('Asset_Variant__c')){
                                errormsgForAssetVariant+=errorMsgSplit[2].trim()+',';
                            }
                            if(err.getFields().contains('Asset_Type__c')){
                                errormsgForAssetType+=errorMsgSplit[2].trim()+',';
                            }
                        }
                    }
                }
            }
            errormsgForAssetVariant = errormsgForAssetVariant.removeEnd(',');
            errormsgForAssetType =  errormsgForAssetType.removeEnd(',');
            system.debug('errormsgForAssetVariant::'+errormsgForAssetVariant);
            system.debug('errormsgForAssetType::'+errormsgForAssetType);
            if(errormsgForAssetVariant!='' || errormsgForAssetType!=''){
                //sending email if the asset type or variant coming through integration is not present in the global picklist for asset type and asset variant
                string subjectName = 'Restricted Pick list value inserted to Portal Asset through Intergration '+ system.today().format() ;
                string emailBody = 'Please find the attachment.';
                list<Document> documents = new list<Document>();
                //log file
                if(errormsgForAssetType!=''){
                    String msg = 'New Asset Type(s) '+'\n\n'+errormsgForAssetType;
                    string fieldType = 'Asset Type'; 
                    documents.add(LeaseWareStreamUtils.createDoc(msg,fieldType + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
                }
                if(errormsgForAssetVariant!=''){
                    String msg = 'New Asset Variant(s) '+'\n\n'+errormsgForAssetVariant;
                    string fieldType = 'Asset Variant'; 
                    documents.add(LeaseWareStreamUtils.createDoc(msg,fieldType + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
                }
                LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),subjectName ,emailBody,documents);
            }
    
        }
           
}