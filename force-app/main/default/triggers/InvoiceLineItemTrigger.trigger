trigger InvoiceLineItemTrigger on Invoice_Line_Item__c (before insert, before update, before delete, after insert, 
after update, after delete, after undelete) {

    if( LeaseWareUtils.isTriggerDisabled() )return;
    
    TriggerFactory.createHandler(Invoice_Line_Item__c.sObjectType);
}