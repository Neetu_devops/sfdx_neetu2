trigger CertificateDetailTrigger on Certificate_Detail__c(after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

if( LeaseWareUtils.isTriggerDisabled() )return;
TriggerFactory.createHandler(Certificate_Detail__c.sObjectType);

}