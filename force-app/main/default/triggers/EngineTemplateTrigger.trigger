trigger EngineTemplateTrigger on Engine_Template__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Engine_Template__c.sObjectType);    

    
}