trigger MRRateTrigger on MR_Rate__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(MR_Rate__c.sObjectType);

}