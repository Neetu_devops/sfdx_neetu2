trigger LicenseBefInsUpd on License__c (before insert, before update, after undelete) {
 /*   
    if(LeaseWareUtils.isTriggerDisabled())return;


    if(trigger.new.size()>1){
        trigger.new[0].addError('There can be only one license record in the system.');
        return;
    }
    
    License__c[] curCount = [select id from License__c];
    
    if(!( Trigger.isUpdate || (Trigger.isInsert && curCount.size()==0 ) || ( curCount.size()==1 && trigger.isUnDelete)) ){//There will be only one record after insert/undelete.
        trigger.new[0].addError('There can be only one license record in the system.');
        return;
    }
    
    LeaseWorksLicense.Modules selectedModules;
    LeaseWorksLicense.TravelClass selecteClass;

    selectedModules=trigger.new[0].Asset__c?(trigger.new[0].Match__c?LeaseWorksLicense.Modules.Both:LeaseWorksLicense.Modules.Asset):trigger.new[0].Match__c?LeaseWorksLicense.Modules.Match:null;
    if(selectedModules==null){
        trigger.new[0].addError('Please select at least one module.');
        return;
    }

    if('Basic'.equals(trigger.new[0].Class__c))selecteClass=LeaseWorksLicense.TravelClass.Economy;
    else if('Light'.equals(trigger.new[0].Class__c))selecteClass=LeaseWorksLicense.TravelClass.Business;
    else if('Glider'.equals(trigger.new[0].Class__c))selecteClass=LeaseWorksLicense.TravelClass.First;
    
    if(selecteClass==null){
        trigger.new[0].Class__c.addError('Please select the license class.');
        return;
    }
    
    if(trigger.new[0].Number_of_Aircraft__c==null){
        trigger.new[0].Number_of_Aircraft__c.addError('Please enter the number of aircraft.');
        return;
    }
    
    
    if(!LeaseWorksLicense.isLicenseValid(trigger.new[0].Name, selectedModules, Integer.valueOf(trigger.new[0].Number_of_Aircraft__c), selecteClass)){
        trigger.new[0].Name.addError('Invalid license key. Please check and enter again.');
        return;
    }
    */
}