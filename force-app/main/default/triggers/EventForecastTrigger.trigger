trigger EventForecastTrigger on Event_Forecast__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	
	if(LeaseWareUtils.isFromTrigger('EventForecastTrigger')) return;
	
    if(trigger.isDelete ) trigger.old[0].addError('Event Forecasts are created automatically. They cannot be created, edited or deleted manually.');
    else trigger.new[0].addError('Event Forecasts are created automatically. They cannot be created, edited or deleted manually.');
 
	//TriggerFactory.createHandler(Event_Forecast__c.sObjectType);    
    
}