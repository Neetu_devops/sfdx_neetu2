trigger JournalEntryTrigger on Journal_Entry__c(after delete, after insert, after undelete, 
					after update, before delete, before insert, before update) {
	if( LeaseWareUtils.isTriggerDisabled() )return;
		TriggerFactory.createHandler(Journal_Entry__c.sObjectType);
}