trigger SuppRentAdjustmentTrigger on Supplemental_Rent_Adjustment__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Supplemental_Rent_Adjustment__c.sObjectType);
}