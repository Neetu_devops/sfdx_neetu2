trigger Notify_NotificationSettingTrigger on Notification_Setting__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) { 
    
    Notify_Utility.triggerHandler(Notification_Setting__c.sObjectType);
}