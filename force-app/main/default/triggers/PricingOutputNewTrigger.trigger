trigger PricingOutputNewTrigger on Pricing_Output_New__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
 
    if(LeaseWareUtils.isTriggerDisabled()) return;
    TriggerFactory.createHandler(Pricing_Output_New__c.sObjectType);  

}