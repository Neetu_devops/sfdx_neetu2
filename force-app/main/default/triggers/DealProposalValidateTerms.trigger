trigger DealProposalValidateTerms on Deal_Proposal__c (after delete, after insert, after undelete,after update, before delete, before insert, before update) {
	
	if(LeaseWareUtils.isTriggerDisabled())return;
	TriggerFactory.createHandler(Deal_Proposal__c.sObjectType);
	// Moved all code of this trigger to DealProposalTriggerHandler (iTrigger class file)
	
	}