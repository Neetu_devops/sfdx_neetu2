trigger URTrigger on Utilization_Report__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Utilization_Report__c.sObjectType);
}