trigger PDPTrigger on Pre_Delivery_Payment__c (before insert, before update, after insert, after update) {
    if(LeaseWareUtils.isTriggerDisabled())return;
    TriggerFactory.createHandler(Pre_Delivery_Payment__c.sObjectType);
}