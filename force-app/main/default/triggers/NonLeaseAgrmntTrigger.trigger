trigger NonLeaseAgrmntTrigger on Non_Lease_Agreements__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Non_Lease_Agreements__c.sObjectType);
}