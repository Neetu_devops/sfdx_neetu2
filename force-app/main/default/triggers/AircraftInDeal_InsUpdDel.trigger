/*
    created By : anjani 02 Sept 2015


*/


trigger AircraftInDeal_InsUpdDel on Aircraft_In_Deal__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if( LeaseWareUtils.isTriggerDisabled() || LeaseWareUtils.isFromInvoice())return;
    TriggerFactory.createHandler(Aircraft_In_Deal__c.sObjectType);
 

// moved to itrigger class
    
}