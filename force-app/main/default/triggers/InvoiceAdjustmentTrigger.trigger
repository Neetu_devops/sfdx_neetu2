trigger InvoiceAdjustmentTrigger on Invoice_Adjustment__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Invoice_Adjustment__c.sObjectType);
}