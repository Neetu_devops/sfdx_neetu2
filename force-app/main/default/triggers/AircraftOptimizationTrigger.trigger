trigger AircraftOptimizationTrigger on Aircraft_Optimization__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    system.debug('Aircraft Optimization Trigger');
    if(LeaseWareUtils.isTriggerDisabled()) return;
    TriggerFactory.createHandler(Aircraft_Optimization__c.sObjectType); 
    
}