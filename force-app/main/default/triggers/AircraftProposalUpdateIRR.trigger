trigger AircraftProposalUpdateIRR on Aircraft_Proposal__c (before insert){
/*
    (before insert, before update) {
  anjani Date = 10th Nov 2016
    Commenting this trigger as no customer is using this till date.


    if(LeaseWareUtils.isTriggerDisabled())return;



    for(Aircraft_Proposal__c curAircraftProp: trigger.new){
        try{
            Decimal annualIRR, investment;
    
            double[] CFs = new Decimal[curAircraftProp.Term_Mos__c.intValue()];
            
            for(integer i=0;i<curAircraftProp.Term_Mos__c.intValue();i++){
                CFs[i]=curAircraftProp.Rent__c==null?0:curAircraftProp.Rent__c.doubleValue();
            }
            
            
            
            if('Amortize'.equals(curAircraftProp.Investment_Timing__c)){
                investment=0;
                for(integer i=0;i<curAircraftProp.Term_Mos__c.intValue();i++){
                    CFs[i]-=(curAircraftProp.Investment_Required__c==null?0:curAircraftProp.Investment_Required__c/curAircraftProp.Term_Mos__c);
                }
    
            }else if('End of Lease'.equals(curAircraftProp.Investment_Timing__c)){
                investment=0;
                CFs[curAircraftProp.Term_Mos__c.intValue()-1]-=(curAircraftProp.Investment_Required__c==null?0:curAircraftProp.Investment_Required__c);
            }else{//Defaulting to Upfront
                investment=curAircraftProp.Investment_Required__c==null?0:curAircraftProp.Investment_Required__c;
            }
            CFs[curAircraftProp.Term_Mos__c.intValue()-1]+=curAircraftProp.Residual_Value__c==null?0:curAircraftProp.Residual_Value__c;
            
//          annualIRR=100*LeaseWareUtils.CalculateIRR(curAircraftProp.Investment_Required__c, curAircraftProp.Rent__c, curAircraftProp.Term_Mos__c, null, null, LeaseWareUtils.Periodicity.Monthly);
            if('IRR'.equals(curAircraftProp.Evaluation_Approach__c)){
                annualIRR=100*LeaseWareUtils.CalculateIRR(investment.doubleValue(), CFs, null, null, LeaseWareUtils.Periodicity.Monthly);
                curAircraftProp.IRR__c=annualIRR>999.99?999.99:annualIRR;
                curAircraftProp.NPV__c=null;
            }else{//NPV
                curAircraftProp.IRR__c=null;
                curAircraftProp.NPV__c=LeaseWareUtils.CalculateNPV(investment.doubleValue(), CFs, (double)(curAircraftProp.Expected_Rate_Of_Return__c==null?0:curAircraftProp.Expected_Rate_Of_Return__c/100), LeaseWareUtils.Periodicity.MONTHLY);
            }
        }catch(exception e){
            system.debug('Exception '+ e);
            curAircraftProp.IRR__c=null;
            curAircraftProp.NPV__c=null;
        }
    }


*/}