trigger CreditMemoIssuance on Credit_Memo_Issuance__c (before insert,after insert,before update, after update,before delete,after delete,after UnDelete) {

  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Credit_Memo_Issuance__c.sObjectType);
}