trigger InvoiceHandlerTrigger on Invoice__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	// for XMLScheduler Job ,need to skip trigger for updating XML_sent__c field
	if( LeaseWareUtils.isTriggerDisabled() )return;
	//if(LeaseWareUtils.isFromTrigger('XMLFinSchedulerforInvoice'))return;
	
	LeaseWareUtils.setFromTrigger('MRSnapshotTrigger'); // to make insert/changes/delete on MR Snapshot
		TriggerFactory.createHandler(Invoice__c.sObjectType);
	LeaseWareUtils.unsetTriggers('MRSnapshotTrigger');

}