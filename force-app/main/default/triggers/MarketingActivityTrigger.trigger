trigger MarketingActivityTrigger on Marketing_Activity__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Marketing_Activity__c.sObjectType);
    
    
}