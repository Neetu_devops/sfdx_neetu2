trigger AccountingSetupTrigger on Parent_Accounting_Setup__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Parent_Accounting_Setup__c.sObjectType);
}