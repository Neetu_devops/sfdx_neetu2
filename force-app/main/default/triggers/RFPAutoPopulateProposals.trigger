trigger RFPAutoPopulateProposals on RFP__c (after insert) {
	
	if(LeaseWareUtils.isTriggerDisabled())return;


	list<RFP_Proposal__c> listRfpProps = new list<RFP_Proposal__c>();
	try{
		//TODO - Need to bulkify the below query
		for(RFP__c curRFPProp : trigger.new){
			list<Counterparty__c> listLessors = [select id, Name from Counterparty__c 
			where Fleet_Aircraft_Of_Interest__c includes (:curRFPProp.Aircraft_Type__c)];
			
			for(Counterparty__c curLessor: listLessors){
				system.debug('Found matching lessor :' + curRFPProp.Aircraft_Type__c + ' - ' +curLessor.Name);
				listRfpProps.add(new RFP_Proposal__c(Name=curRFPProp.Aircraft_Type__c + ' From ' + curLessor.Name, RFP__c=curRFPProp.id, Lessor__c=curLessor.id));
			}
		}
	
	}catch(exception e){
		system.debug('Cannot auto-create RFP Proposals. '+ e.getMessage() + ' at ' + e.getLineNumber());
	}	
	 
	if(listRfpProps.size()>0)insert listRfpProps;


}