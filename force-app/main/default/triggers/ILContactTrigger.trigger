trigger ILContactTrigger on IL_Contact__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

  if(LeaseWareUtils.isTriggerDisabled())return;
  TriggerFactory.createHandler(IL_Contact__c.sObjectType);
}