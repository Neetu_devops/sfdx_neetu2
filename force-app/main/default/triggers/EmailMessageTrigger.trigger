trigger EmailMessageTrigger on EmailMessage (after insert, after delete) {
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactoryForStandard.createHandler(EmailMessage.sObjectType);
}