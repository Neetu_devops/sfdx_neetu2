trigger LeaseHistPreventMod on Lease_Extension_History__c (before delete, after insert, after update) {
	
	if(LeaseWareUtils.isTriggerDisabled())return;
/*
		if(trigger.isDelete){ 
			trigger.old[0].addError('Lease Extension History is maintained automatically. History records cannot be deleted.');
		}else if( trigger.isUpdate){
			
			 Map<String, Schema.SObjectField> mapLHField = Schema.SObjectType.Lease_Extension_History__c.fields.getMap();
			 list<String> listFieldsToCheck = new list<String>(); 
			 
			 for(String Field:mapLHField.keyset()){
			 	Schema.DescribeFieldResult thisFieldDesc = mapLHField.get(Field).getDescribe();
			 	if( !thisFieldDesc.isUpdateable() || (LeaseWareUtils.getNamespacePrefix() + 'Comment__c').equals(thisFieldDesc.getName()))continue; //(!thisFieldDesc.isCustom() && !'Name'.equals(thisFieldDesc.getName())) ||
			 	System.debug('Field Name is ' + thisFieldDesc.getName() + '. Local ' + thisFieldDesc.getLocalName());
			 	
			 	listFieldsToCheck.add(thisFieldDesc.getName());
			 }
			 
			for(Lease_Extension_History__c curLH:trigger.new){
				
				for(String curField:listFieldsToCheck){
					if(trigger.newMap.get(curLH.id).get(curField) !=  trigger.oldMap.get(curLH.id).get(curField)){
						curLH.addError('Lease Extension History is maintained automatically. Only comment can be updated.');
						break;
					}
				}
			}
		}else if((trigger.isInsert) && !trigger.new[0].Is_Created_By_Trigger__c){
			trigger.new[0].addError('Lease Extension History is maintained automatically. History records cannot be created manually.');
		}
*/
}