trigger PDPInterestTrigger on PDP_Interest__c (before insert,before update ) {
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(PDP_Interest__c.sObjectType);
    
}