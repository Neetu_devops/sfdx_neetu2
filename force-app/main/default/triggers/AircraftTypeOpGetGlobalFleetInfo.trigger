trigger AircraftTypeOpGetGlobalFleetInfo on Aircraft_Type_Operator__c(after insert, after update) {


system.debug('Val is ' + trigger.new[0].Consider_For_Campaigns__c);

	if(System.isFuture())return;
    //LeaseWareUtils.setFromTrigger();

    set<Id> setIds = new set<Id>();
    for(Aircraft_Type_Operator__c curAcOp: trigger.new){
    	if(trigger.isUpdate && curAcOp.Consider_For_Campaigns__c != trigger.oldmap.get(curAcOp.id).Consider_For_Campaigns__c)continue;
        if(curAcOp.id!=null)setIds.add(curAcOp.id);
    }
    if(setIds.size()>0)LeasewareUtils.ftrBulkUpdateAcTypeOps(setIds);

}