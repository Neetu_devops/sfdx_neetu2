trigger CampaignDelAiCs on Deal__c (before delete) {

	//Child record delete triggers are not fired when a parent is deleted. Manually enforce execution. 
    delete [select id from Aircraft_In_Deal__c where Marketing_Deal__c in :trigger.old];

}