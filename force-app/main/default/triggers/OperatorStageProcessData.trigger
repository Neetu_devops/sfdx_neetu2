trigger OperatorStageProcessData on Operator_Stage__c (after insert) {

    boolean bDisableAll=false, bDisableOps=false;
    LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
    if(lwSettings!=null){
        bDisableAll=lwSettings.Disable_All_Integrations__c;
        bDisableOps=lwSettings.Disable_Operator_Integration__c;
    }

	if(bDisableAll || bDisableOps){
		system.debug('Operator integration disabled. Returning.');
		return;
	}


	map<string, Operator__c> mapExistingOperator = new map <string, Operator__c>();
	set<String> setOperatorName =new set<String>();
	Map<String, Schema.SObjectField> mapOperatorFields = Schema.SObjectType.Operator_Stage__c.fields.getMap();

	string strQry = 'select ';
	for(String Field:mapOperatorFields.keyset()){
        Schema.DescribeFieldResult thisFieldDesc = mapOperatorFields.get(Field).getDescribe();
        if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue;
        if(thisFieldDesc.getLocalName().startsWith('Lookup_')){//Special processing for lookup fields. 
			//While naming lookup fields, get the field name from the actual object just prefix with 'Lookup_' 
        	 strQry+= (thisFieldDesc.getName().replace('Lookup_', '') + ', ');
        }else{
	        strQry+= (thisFieldDesc.getName() + ', ');
        }
	} 
	strQry = strQry.left(strQry.length()-2);
	strQry+=' from ' + Schema.SObjectType.Operator__c.getName();
	
	Operator__c[] listAllOperator =  database.query(strQry);

	for(Operator__c curOperator: listAllOperator){
		mapExistingOperator.put(curOperator.Name.toLowerCase(), curOperator);
		
		if(String.isNotBlank(curOperator.Lessee_Legal_Name__c)){
			mapExistingOperator.put(curOperator.Lessee_Legal_Name__c.toLowerCase(),curOperator);
		} 
		if(String.isNotBlank(curOperator.World_Fleet_Operator_Name__c)){
			mapExistingOperator.put(curOperator.World_Fleet_Operator_Name__c.toLowerCase(),curOperator);
		}
		if(String.isNotBlank(curOperator.Alias__c)){
			mapExistingOperator.put(curOperator.Alias__c.toLowerCase(),curOperator);
		}  
        if(curOperator.Alias__c!=null && !curOperator.Name.toLowerCase().equals(curOperator.Alias__c.toLowerCase())){
            for(string strCurAlias : curOperator.Alias__c.toLowerCase().split(',',0)){
                strCurAlias=strCurAlias.trim();
                if(''.equals(strCurAlias))continue;
                if(mapExistingOperator.containsKey(strCurAlias))continue; //Do not override operator name with alias
            	mapExistingOperator.put(strCurAlias, curOperator);
            }
        }
	}
	System.debug('Map from Operator Stage:' + mapExistingOperator);
	
	map<string, Id> mapAddresses = new map<string, Id>();
	for(Address__c curAd : [select id, Name from Address__c]){
		mapAddresses.put(curAd.Name, curAd.Id);
	}
	
	list<Operator__c> listOperatorToUpdate = new list<Operator__c>();
	set<string> setOpNames = new set<string>(); 

	for(Operator_Stage__c curOpStage : trigger.new){

		if(curOpStage.Name==null)continue; //External Id cannot be null

		if(setOpNames.contains(curOpStage.Name.toLowerCase()) || 
			(String.isNotBlank(curOpStage.Lessee_Legal_Name__c) && setOpNames.contains(curOpStage.Lessee_Legal_Name__c.toLowerCase()))) {
			continue; //Update one operator only once even if there are more than one record in the stage. 
		}

		Operator__c thisOp = mapExistingOperator.get(curOpStage.Name.toLowerCase());
		if(thisOp == null && (String.isNotBlank(curOpStage.Lessee_Legal_Name__c))){
			thisOp = mapExistingOperator.get(curOpStage.Lessee_Legal_Name__c.toLowerCase());
		}
		if(thisOp!=null && setOpNames.contains(thisOp.id)) {continue;} //Update one operator only once even if there are more than one record in the stage. 

		if(thisOp==null){ //New Operator.
			thisOp = new Operator__c();
			for(String Field:mapOperatorFields.keyset()){
	            Schema.DescribeFieldResult thisFieldDesc = mapOperatorFields.get(Field).getDescribe();
	            string strLocalName = thisFieldDesc.getLocalName();
	            if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue;
	            if(!thisFieldDesc.isCustom() && !Field.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
	            if(strLocalName.startsWith('Lookup_')){//Special processing for lookup fields
	            	strLocalName=strLocalName.replace('Lookup_','');
	            	Id idLookup;
            		idLookup = (id)(mapAddresses.get((string)curOpStage.get(Field)));
	            	if('Contracts_Representative_Contact__c'.equals(strLocalName)){
	            		if(idLookup==null){//create aircraft?
	            			//TODO
	            		}else{
			            	thisOp.put('Contracts_Representative_Contact__c', idLookup);
	            		}
	            	}else if('Marketing_Representative_Contact__c'.equals(strLocalName)){
	            		if(idLookup==null){//create Operator?
	            			//TODO
	            		}else{
			            	thisOp.put('Marketing_Representative_Contact__c', idLookup);
	            		}
	            	}else if('Technical_Representative_Contact__c'.equals(strLocalName)){
	            		if(idLookup==null){//create Operator?
	            			//TODO
	            		}else{
			            	thisOp.put('Technical_Representative_Contact__c', idLookup);
	            		}
	            	}
	            }else{
		            thisOp.put(thisFieldDesc.getName(), curOpStage.get(Field));
	            }
			}
			listOperatorToUpdate.add(thisOp);
			setOpNames.add(thisOp.Name.toLowerCase());
			
		}else{//Existing Operator.
			boolean bUpdated = false;	
			for(String Field:mapOperatorFields.keyset()){
	            Schema.DescribeFieldResult thisFieldDesc = mapOperatorFields.get(Field).getDescribe();
	            string strLocalName = thisFieldDesc.getLocalName();
	            if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue; //Skip all system fields like createdBy and Don't update Name.
	            if(!thisFieldDesc.isCustom() )continue; //skip standard field other than Name

	            if(strLocalName.startsWith('Lookup_')){//Special processing for lookup fields
	            	strLocalName=strLocalName.replace('Lookup_','');
	            	Id idLookup, oldId;
            		oldId = (id)thisOp.get(strLocalName);
            		idLookup = (id)(mapAddresses.get((string)curOpStage.get(Field)));
	            	if('Contracts_Representative_Contact__c'.equals(strLocalName)){
	            		if(idLookup==null){//create Contact?
	            			//TODO
	            		}else{
			            	thisOp.put('Contracts_Representative_Contact__c', idLookup);
	            		}
	            	}else if('Marketing_Representative_Contact__c'.equals(strLocalName)){
	            		if(idLookup==null){//create Contact?
	            			//TODO
	            		}else{
			            	thisOp.put('Marketing_Representative_Contact__c', idLookup);
	            		}
	            	}else if('Technical_Representative_Contact__c'.equals(strLocalName)){
	            		if(idLookup==null){//create Contact?
	            			//TODO
	            		}else{
			            	thisOp.put('Technical_Representative_Contact__c', idLookup);
	            		}
	            	}
					if(oldId!=null || idLookup!=null){
						if(LeaseWareUtils.IsNotSame(oldId, idLookup, Schema.DisplayType.Id)){
				            thisOp.put(strLocalName, idLookup);
							bUpdated=true;
						}
					}
	            }else{

					if(curOpStage.get(Field)!=null){// Update only if incoming value is not null. thisOp.get(Field)!=null || 
						if(LeaseWareUtils.IsNotSame(thisOp.get(Field), curOpStage.get(Field), thisFieldDesc.getType())){
	system.debug('Field updated ' + thisFieldDesc.getName() + ' : ' + thisOp.get(Field) + '->' + curOpStage.get(Field));
				            thisOp.put(thisFieldDesc.getName(), curOpStage.get(Field));
							bUpdated=true;
						}
					}
	            }
			}
			if(bUpdated){
				listOperatorToUpdate.add(thisOp);
				setOpNames.add(thisOp.Name.toLowerCase());
				setOpNames.add(thisOp.id);
			}
		}			
			
	}

	system.debug('Upserting '+ listOperatorToUpdate);
	if(listOperatorToUpdate.size()>0){
		leasewareUtils.unsetTriggers('All');
		upsert listOperatorToUpdate Name; 
	} 

}