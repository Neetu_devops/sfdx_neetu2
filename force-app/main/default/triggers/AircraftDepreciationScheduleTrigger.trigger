trigger AircraftDepreciationScheduleTrigger on Aircraft_Depreciation_Schedule__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Aircraft_Depreciation_Schedule__c.sObjectType);


}