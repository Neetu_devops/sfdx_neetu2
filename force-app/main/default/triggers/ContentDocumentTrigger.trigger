trigger ContentDocumentTrigger on ContentDocument (before insert,before update,before delete,after delete, after update,after insert ) {
	if( LeaseWareUtils.isTriggerDisabled())return;
    TriggerFactoryForStandard.createHandler(ContentDocument.sObjectType);
	
}