trigger PaymentTrigger on Payment__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  
	if( LeaseWareUtils.isTriggerDisabled() )return;
	//if(LeaseWareUtils.isFromTrigger('XMLFinSchedulerforPayment'))return;
	
	LeaseWareUtils.setFromTrigger('MRSnapshotTrigger'); // to make insert/changes/delete on MR Snapshot
		TriggerFactory.createHandler(Payment__c.sObjectType);
    LeaseWareUtils.unsetTriggers('MRSnapshotTrigger');
    
}