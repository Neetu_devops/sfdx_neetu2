trigger PayablesTrigger on payable__c  (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(payable__c.sObjectType);
}