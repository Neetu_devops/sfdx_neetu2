trigger InsuranceCertificateTrigger on Insurance_Cert__c(after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

if( LeaseWareUtils.isTriggerDisabled() )return;
TriggerFactory.createHandler(Insurance_Cert__c.sObjectType);

}