trigger PaymentAftInsBefDelUpdMR on Payment__c (after insert, after update, before delete) {

	
	if(LeaseWareUtils.isTriggerDisabled())return;
	
	// for XMLScheduler Job ,need to skip trigger for updating XML_sent__c field
	if(LeaseWareUtils.isFromTrigger('XMLFinSchedulerforPayment'))return;
    	if(LeaseWareUtils.isFromTrigger('PaymentAftInsBefDelUpdMR')) return;
		LeaseWareUtils.setFromTrigger('PaymentAftInsBefDelUpdMR');

    Decimal TwencyMoCCheck=0, APU=0, Engine1=0, Engine1LLP=0, Engine2=0, Engine2LLP =0, 
        Engine3=0, Engine3LLP =0, Engine4=0, Engine4LLP=0, Heavy1=0, Heavy2=0, Heavy3=0, Heavy4=0, LandingGear=0,
        LandingGearCM=0, LandingGearLM=0, LandingGearRM=0, LandingGearNose=0, LandingGearLW=0, LandingGearRW=0,      
        Other=0, AmountTotal=0;
        
    Id AcID, LeaseID;
    Boolean isImpacted=false;
    Payment__c[] allPayments;
    //map<Id, Id> mapInvIds = new map<Id, Id>();
    //map<Id, Id> mapLeaseIds = new map<Id, Id>(); 

    if(trigger.isDelete){
        allPayments=trigger.old;
    }else{
        allPayments=trigger.new;
    }

    for(Payment__c curPayment: allPayments){
        //mapInvIds.put(curPayment.Id, curPayment.Invoice__c);
        LeaseID = curPayment.Lease__c;
        if(AcID==null){
        	AcID = curPayment.Aircraft__c ;
        }
        if(AcID!=curPayment.Aircraft__c){
            curPayment.addError('Please load/delete payments belonging to only one aircraft at a time.');
            return;
        }        
    }

/*
    Invoice__c[] allInvs = [select Id, Lease__c, Lease__r.Aircraft__c
     from Invoice__c where Id in :mapInvIds.values()];
    for(Invoice__c curInv: allInvs){

        //mapLeaseIds.put(curInv.Id, curInv.Lease__c);
    	
        if(AcID==null){
            AcID=curInv.Lease__r.Aircraft__c;
            LeaseID=curInv.Lease__c;
            continue;
        }
        if(AcID!=curInv.Lease__r.Aircraft__c){
            allPayments[0].addError('Please load/delete payments belonging to only one aircraft at a time.');
            return;
        }   
    }
*/
    if(AcID==null)return;

//    Aircraft__c thisAC=LeaseWareUtils.getAttachedAC(AcID);
    Aircraft__c thisAC= [select id, Name, Maintenance_Reserve_20_Month_C_Check__c, Maintenance_Reserve_APU__c, 
            Maintenance_Reserve_Engine_1__c, Maintenance_Reserve_Engine_1_LLP__c, Maintenance_Reserve_Engine_2__c, 
            Maintenance_Reserve_Engine_2_LLP__c, Maintenance_Reserve_Engine_3__c, Maintenance_Reserve_Engine_3_LLP__c, 
            Maintenance_Reserve_Engine_4__c, Maintenance_Reserve_Engine_4_LLP__c, 
            Maintenance_Reserve_Heavy_Maintenance_1__c, 
            Maintenance_Reserve_Heavy_Maintenance_2__c, 
            Maintenance_Reserve_Heavy_Maintenance_3__c, 
            Maintenance_Reserve_Heavy_Maintenance_4__c, 
            Maintenance_Reserve_LG_Center_Main__c,
			Maintenance_Reserve_LG_Left_Main__c,
			Maintenance_Reserve_LG_Left_Wing__c,
			Maintenance_Reserve_LG_Nose__c,
			Maintenance_Reserve_LG_Right_Main__c,
			Maintenance_Reserve_LG_Right_Wing__c, 
			Maintenance_Reserve_Landing_Gear__c,
            Maintenance_Reserve_TOTAL__c,
            Maintenance_Reserve_Other__c, TSN__c, CSN__c, 
            lease__r.Operator__c, lease__r.Operator__r.Name, lease__r.Operator__r.Flight_Hours_Format__c, lease__r.Operator__r.Country__c,
            Cash_Maintenance_Reserve_APU__c, Cash_Maintenance_Reserve_Engine_1__c , Cash_Maintenance_Reserve_Engine_1_LLP__c,
            Cash_Maintenance_Reserve_Engine_2__c, Cash_Maintenance_Reserve_Engine_2_LLP__c , Cash_Maintenance_Reserve_Engine_3__c ,
            Cash_Maintenance_Reserve_Engine_3_LLP__c , Cash_Maintenance_Reserve_Engine_4__c , Cash_Maintenance_Reserve_Engine_4_LLP__c ,
            Cash_MR_Heavy_Maintenance_1__c , Cash_MR_Heavy_Maintenance_2__c , 
            Cash_MR_Heavy_Maintenance_3__c , Cash_MR_Heavy_Maintenance_4__c , 
            Cash_Maintenance_Reserve_LG_Center_Main__c,
			Cash_Maintenance_Reserve_LG_Left_Main__c,
			Cash_Maintenance_Reserve_LG_Left_Wing__c,
			Cash_Maintenance_Reserve_LG_Nose__c,
			Cash_Maintenance_Reserve_LG_Right_Main__c,
			Cash_Maintenance_Reserve_LG_Right_Wing__c,
			Cash_Maintenance_Reserve_Landing_Gear__c,

            Cash_Maintenance_Reserve_Other__c, Cash_MR_20_Month_C_Check__c 
             
            from Aircraft__c where Id=:AcID limit 1];

    //map<string, MR_Snapshot__c> mapAllMRSnapshots = new map<string, MR_Snapshot__c>(); 
    //list<MR_Snapshot__c> listAllMRSnapshots = new list<MR_Snapshot__c>(); 

	LeaseWareUtils.setFromTrigger('PaymentAftInsBefDelUpdMR');

	/*if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
		system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
	}else{
	
		if(mapLeaseIds.size()>0){
			listAllMRSnapshots = [select
				Lease__c, Type__c, 
				
				Y_H_NMRInv_APU__c,
				Y_H_NMRInv_Engine_1__c ,
				Y_H_NMRInv_Engine_1_LLP__c ,
				Y_H_NMRInv_Engine_2__c ,
				Y_H_NMRInv_Engine_2_LLP__c ,
				Y_H_NMRInv_Engine_3__c ,
				Y_H_NMRInv_Engine_3_LLP__c ,
				Y_H_NMRInv_Engine_4__c ,
				Y_H_NMRInv_Engine_4_LLP__c ,
				Y_H_NMRInv_Heavy_Maint_1_Airframe__c ,
				Y_H_NMRInv_Heavy_Maint_2_Airframe__c ,
				Y_H_NMRInv_Heavy_Maint_3_Airframe__c ,
				Y_H_NMRInv_Heavy_Maint_4_Airframe__c ,
				Y_H_NMRInv_Landing_Gear_Left_Main__c ,
				Y_H_NMRInv_Landing_Gear_Left_Wing__c ,
				Y_H_NMRInv_Landing_Gear_Nose__c ,
				Y_H_NMRInv_Landing_Gear_Right_Main__c ,
				Y_H_NMRInv_Landing_Gear_Right_Wing__c ,
				
				Y_H_NMRCol_APU__c ,
				Y_H_NMRCol_Engine_1__c ,
				Y_H_NMRCol_Engine_1_LLP__c ,
				Y_H_NMRCol_Engine_2__c ,
				Y_H_NMRCol_Engine_2_LLP__c ,
				Y_H_NMRCol_Engine_3__c ,
				Y_H_NMRCol_Engine_3_LLP__c ,
				Y_H_NMRCol_Engine_4__c ,
				Y_H_NMRCol_Engine_4_LLP__c ,
				Y_H_NMRCol_Heavy_Maint_1_Airframe__c ,
				Y_H_NMRCol_Heavy_Maint_2_Airframe__c ,
				Y_H_NMRCol_Heavy_Maint_3_Airframe__c ,
				Y_H_NMRCol_Heavy_Maint_4_Airframe__c ,
				Y_H_NMRCol_Landing_Gear_Left_Main__c ,
				Y_H_NMRCol_Landing_Gear_Left_Wing__c ,
				Y_H_NMRCol_Landing_Gear_Nose__c ,
				Y_H_NMRCol_Landing_Gear_Right_Main__c ,
				Y_H_NMRCol_Landing_Gear_Right_Wing__c, 
				
				Payment_Date__c, Month_Ending__c, Payment__c    
				from MR_Snapshot__c 
				where lease__c in :mapLeaseIds.values() 
				order by lease__c, Payment_Date__c  desc];
		    for(MR_Snapshot__c curMrSs: listAllMRSnapshots){
				mapAllMRSnapshots.put(curMrSs.Lease__c+'-'+curMrSs.Payment__c, curMrSs);
		    }
		}
	}

	MR_Snapshot__c CurMrSs, prevMrSS;
	list<MR_Snapshot__c> listMRSsToUpsert = new list<MR_Snapshot__c>();
*/
    if(trigger.isDelete){
        for(Payment__c curPayment: trigger.old){
			if('Provision'.equals(curPayment.Y_Hidden_Sub_Type_F__c) || curPayment.End_Of_Lease_Adjustments_Only_F__c) continue;
            if('Aircraft MR'.equals(curPayment.Invoice_Type__c) || 'Assembly MR'.equals(curPayment.Invoice_Type__c))
                isImpacted=true;
            else 
                continue;

            TwencyMoCCheck += -LeaseWareUtils.zeroIfNull(curPayment.Twenty_Month_C_Check__c);
            APU += -LeaseWareUtils.zeroIfNull(curPayment.APU__c);
            Engine1 += -LeaseWareUtils.zeroIfNull(curPayment.Engine_1__c);
            Engine1LLP += -LeaseWareUtils.zeroIfNull(curPayment.Engine_1_LLP__c);
            Engine2 += -LeaseWareUtils.zeroIfNull(curPayment.Engine_2__c);
            Engine2LLP += -LeaseWareUtils.zeroIfNull(curPayment.Engine_2_LLP__c);
            Engine3 += -LeaseWareUtils.zeroIfNull(curPayment.Engine_3__c);
            Engine3LLP += -LeaseWareUtils.zeroIfNull(curPayment.Engine_3_LLP__c);
            Engine4 += -LeaseWareUtils.zeroIfNull(curPayment.Engine_4__c);
            Engine4LLP += -LeaseWareUtils.zeroIfNull(curPayment.Engine_4_LLP__c);
            Heavy1 += -LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_1_Airframe__c);
            Heavy2 += -LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_2_Airframe__c);
            Heavy3 += -LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_3_Airframe__c);
            Heavy4 += -LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_4_Airframe__c);

            LandingGearCM += -LeaseWareUtils.zeroIfNull(curPayment.LG_Center__c);
            LandingGearLM += -LeaseWareUtils.zeroIfNull(curPayment.LG_Left_Main__c);
            LandingGearRM += -LeaseWareUtils.zeroIfNull(curPayment.LG_Right_Main__c);
            LandingGearLW += -LeaseWareUtils.zeroIfNull(curPayment.LG_Left_Wing__c);
            LandingGearRW += -LeaseWareUtils.zeroIfNull(curPayment.LG_Right_Wing__c);
            LandingGearNose += -LeaseWareUtils.zeroIfNull(curPayment.LG_Nose__c);

            Other+= -LeaseWareUtils.zeroIfNull(curPayment.Other__c);
            
            AmountTotal += -LeaseWareUtils.zeroIfNull(curPayment.Amount__c);

        }

    }else if(trigger.isUpdate){

        for(Payment__c curPayment: trigger.new){
			if('Provision'.equals(curPayment.Y_Hidden_Sub_Type_F__c) || curPayment.End_Of_Lease_Adjustments_Only_F__c) continue;
            if('Aircraft MR'.equals(curPayment.Invoice_Type__c) || 'Assembly MR'.equals(curPayment.Invoice_Type__c))
                isImpacted=true;
            else 
                continue;

            TwencyMoCCheck += (LeaseWareUtils.zeroIfNull(curPayment.Twenty_Month_C_Check__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Twenty_Month_C_Check__c));
            APU += (LeaseWareUtils.zeroIfNull(curPayment.APU__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).APU__c));
            Engine1 += (LeaseWareUtils.zeroIfNull(curPayment.Engine_1__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_1__c));
            Engine1LLP += (LeaseWareUtils.zeroIfNull(curPayment.Engine_1_LLP__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_1_LLP__c));
            Engine2 += (LeaseWareUtils.zeroIfNull(curPayment.Engine_2__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_2__c));
            Engine2LLP += (LeaseWareUtils.zeroIfNull(curPayment.Engine_2_LLP__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_2_LLP__c));
            Engine3 += (LeaseWareUtils.zeroIfNull(curPayment.Engine_3__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_3__c));
            Engine3LLP += (LeaseWareUtils.zeroIfNull(curPayment.Engine_3_LLP__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_3_LLP__c));
            Engine4 += (LeaseWareUtils.zeroIfNull(curPayment.Engine_4__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_4__c));
            Engine4LLP += (LeaseWareUtils.zeroIfNull(curPayment.Engine_4_LLP__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Engine_4_LLP__c));
            Heavy1 += (LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_1_Airframe__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Heavy_Maint_1_Airframe__c));
            Heavy2 += (LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_2_Airframe__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Heavy_Maint_2_Airframe__c));
            Heavy3 += (LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_3_Airframe__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Heavy_Maint_3_Airframe__c));
            Heavy4 += (LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_4_Airframe__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Heavy_Maint_4_Airframe__c));

            LandingGearCM += (LeaseWareUtils.zeroIfNull(curPayment.LG_Center__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).LG_Center__c));
            LandingGearLM += (LeaseWareUtils.zeroIfNull(curPayment.LG_Left_Main__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).LG_Left_Main__c));
            LandingGearRM += (LeaseWareUtils.zeroIfNull(curPayment.LG_Right_Main__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).LG_Right_Main__c));
            LandingGearLW += (LeaseWareUtils.zeroIfNull(curPayment.LG_Left_Wing__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).LG_Left_Wing__c));
            LandingGearRW += (LeaseWareUtils.zeroIfNull(curPayment.LG_Right_Wing__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).LG_Right_Wing__c));
            LandingGearNose += (LeaseWareUtils.zeroIfNull(curPayment.LG_Nose__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).LG_Nose__c));

            Other += (LeaseWareUtils.zeroIfNull(curPayment.Other__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Other__c));
            AmountTotal += (LeaseWareUtils.zeroIfNull(curPayment.Amount__c) - LeaseWareUtils.zeroIfNull(trigger.oldMap.get(curPayment.id).Amount__c));
/*
			if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
				system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
			}else{
				CurMrSs=null; 
				prevMrSS=null;
				for(MR_Snapshot__c curSS: listAllMRSnapshots){
					if(curSS.Payment__c == curPayment.id)continue;
					if(curSS.Month_Ending__c > curPayment.Payment_Date__c)listMRSsToUpsert.add(curSS);
					if(prevMrSS==null){
						if(curSS.Month_Ending__c < curPayment.Payment_Date__c)prevMrSS=curSS;
						continue;
					}
					if(curSS.Month_Ending__c < curPayment.Payment_Date__c && prevMrSS.Month_Ending__c <curSS.Month_Ending__c)prevMrSS=curSS;    
				}
	
				LeaseId = mapLeaseIds.get(mapInvIds.get(curPayment.id));
				CurMrSs = mapAllMRSnapshots.get(LeaseId+'-'+curPayment.id);
				if(CurMrSs==null){
					CurMrSs = new MR_Snapshot__c();
					if(prevMrSS!=null)CurMrSs=prevMrSS.clone();
					CurMrSs.Name='Payment - ' + curPayment.Payment_Date__c.format();
					CurMrSs.lease__c=LeaseId;
					CurMrSs.Payment__c = curPayment.Id;
					CurMrSs.Type__c = curPayment.Invoice_Type__c+' Payment'; 
					CurMrSs.Month_Ending__c=curPayment.Payment_Date__c;
				}
	
				if(prevMrSS == null){
					//Should this be an error condition? There should at least be an Invoice SS present.
					
	***				CurMrSs.Y_H_NMRInv_APU__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_APU__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_APU__c);
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_1__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_2__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_2__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2__c);
					CurMrSs.Y_H_NMRInv_Engine_2_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_3__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_3__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3__c);
					CurMrSs.Y_H_NMRInv_Engine_3_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_4__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_4__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4__c);
					CurMrSs.Y_H_NMRInv_Engine_4_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4_LLP__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_1__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_2__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_3__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_4__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Wing__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Nose__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Nose__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Wing__c);
		
					CurMrSs.Y_H_NMRCol_APU__c = curUR.Aircraft__r.Maintenance_Reserve_APU__c;
					CurMrSs.Y_H_NMRCol_Engine_1__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_1__c;
					CurMrSs.Y_H_NMRCol_Engine_1_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_1_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_2__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_2__c;
					CurMrSs.Y_H_NMRCol_Engine_2_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_2_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_3__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_3__c;
					CurMrSs.Y_H_NMRCol_Engine_3_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_3_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_4__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_4__c;
					CurMrSs.Y_H_NMRCol_Engine_4_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_4_LLP__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_1__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_2__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_3__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_4__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Main__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Main__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Wing__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Nose__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Nose__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Main__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Main__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Wing__c;
	***
				}else{
	
	***
	## This is not required. CurMrSS is already a clone of PrevMrSs. 
	
					CurMrSs.Y_H_NMRInv_APU__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_APU__c) ;
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_2__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_2__c);
					CurMrSs.Y_H_NMRInv_Engine_2_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_2_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_3__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_3__c);
					CurMrSs.Y_H_NMRInv_Engine_3_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_3_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_4__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_4__c);
					CurMrSs.Y_H_NMRInv_Engine_4_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_4_LLP__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Left_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Left_Wing__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Nose__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Right_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Right_Wing__c);
	***				
					CurMrSs.Y_H_NMRCol_APU__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_APU__c)+ leasewareUtils.zeroIfNull(curPayment.APU__c);
					CurMrSs.Y_H_NMRCol_Engine_1__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_1__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1__c);
					CurMrSs.Y_H_NMRCol_Engine_1_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1_LLP__c);
					CurMrSs.Y_H_NMRCol_Engine_2__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_2__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2__c);
					CurMrSs.Y_H_NMRCol_Engine_2_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2_LLP__c);
					CurMrSs.Y_H_NMRCol_Engine_3__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_3__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3__c);
					CurMrSs.Y_H_NMRCol_Engine_3_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3_LLP__c);
					CurMrSs.Y_H_NMRCol_Engine_4__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_4__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4__c);
					CurMrSs.Y_H_NMRCol_Engine_4_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4_LLP__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_1_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_2_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_3_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_4_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Left_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Main__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Left_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Wing__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Nose__c) + leasewareUtils.zeroIfNull(curPayment.LG_Nose__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Right_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Main__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Right_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Wing__c);
				}
	
				for(MR_Snapshot__c thisMRSs: listMRSsToUpsert){
					thisMRSs.Y_H_NMRCol_APU__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_APU__c)+ leasewareUtils.zeroIfNull(curPayment.APU__c);
					thisMRSs.Y_H_NMRCol_Engine_1__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_1__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1__c);
					thisMRSs.Y_H_NMRCol_Engine_1_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1_LLP__c);
					thisMRSs.Y_H_NMRCol_Engine_2__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_2__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2__c);
					thisMRSs.Y_H_NMRCol_Engine_2_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2_LLP__c);
					thisMRSs.Y_H_NMRCol_Engine_3__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_3__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3__c);
					thisMRSs.Y_H_NMRCol_Engine_3_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3_LLP__c);
					thisMRSs.Y_H_NMRCol_Engine_4__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_4__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4__c);
					thisMRSs.Y_H_NMRCol_Engine_4_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4_LLP__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_1_Airframe__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_2_Airframe__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_3_Airframe__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_4_Airframe__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Main__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Wing__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Nose__c) + leasewareUtils.zeroIfNull(curPayment.LG_Nose__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Main__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Wing__c);
				}
	
				listMRSsToUpsert.add(curMrSs);
				mapAllMRSnapshots.put(LeaseId+'-'+curPayment.Id, CurMrSs);
				listAllMRSnapshots.add(CurMrSs);
			}
			*/
        }

    }else{ //Insert
        for(Payment__c curPayment: trigger.new){
			if('Provision'.equals(curPayment.Y_Hidden_Sub_Type_F__c) || curPayment.End_Of_Lease_Adjustments_Only_F__c) continue;
            if('Aircraft MR'.equals(curPayment.Invoice_Type__c) || 'Assembly MR'.equals(curPayment.Invoice_Type__c))
                isImpacted=true;
            else 
                continue;

            TwencyMoCCheck += LeaseWareUtils.zeroIfNull(curPayment.Twenty_Month_C_Check__c);
            APU += LeaseWareUtils.zeroIfNull(curPayment.APU__c);
            Engine1 += LeaseWareUtils.zeroIfNull(curPayment.Engine_1__c);
            Engine1LLP += LeaseWareUtils.zeroIfNull(curPayment.Engine_1_LLP__c);
            Engine2 += LeaseWareUtils.zeroIfNull(curPayment.Engine_2__c);
            Engine2LLP += LeaseWareUtils.zeroIfNull(curPayment.Engine_2_LLP__c);
            Engine3 += LeaseWareUtils.zeroIfNull(curPayment.Engine_3__c);
            Engine3LLP += LeaseWareUtils.zeroIfNull(curPayment.Engine_3_LLP__c);
            Engine4 += LeaseWareUtils.zeroIfNull(curPayment.Engine_4__c);
            Engine4LLP += LeaseWareUtils.zeroIfNull(curPayment.Engine_4_LLP__c);
            Heavy1 += LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_1_Airframe__c);
            Heavy2 += LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_2_Airframe__c);
            Heavy3 += LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_3_Airframe__c);
            Heavy4 += LeaseWareUtils.zeroIfNull(curPayment.Heavy_Maint_4_Airframe__c);

            LandingGearCM += LeaseWareUtils.zeroIfNull(curPayment.LG_Center__c);
            LandingGearLM += LeaseWareUtils.zeroIfNull(curPayment.LG_Left_Main__c);
            LandingGearRM += LeaseWareUtils.zeroIfNull(curPayment.LG_Right_Main__c);
            LandingGearLW += LeaseWareUtils.zeroIfNull(curPayment.LG_Left_Wing__c);
            LandingGearRW += LeaseWareUtils.zeroIfNull(curPayment.LG_Right_Wing__c);
            LandingGearNose += LeaseWareUtils.zeroIfNull(curPayment.LG_Nose__c);
            

            Other += LeaseWareUtils.zeroIfNull(curPayment.Other__c);
            AmountTotal += (TwencyMoCCheck +
	            APU +
	            Engine1 +
	            Engine1LLP +
	            Engine2 +
	            Engine2LLP +
	            Engine3 +
	            Engine3LLP +
	            Engine4 +
	            Engine4LLP +
	            Heavy1 +
	            Heavy2 +
	            Heavy3 +
	            Heavy4 +
	            LandingGearLM +
	            LandingGearRM +
	            LandingGearLW +
	            LandingGearRW +
	            LandingGearNose);
/*
			if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
				system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
			}else {
				CurMrSs=null; 
				prevMrSS=null;
				for(MR_Snapshot__c curSS: listAllMRSnapshots){
					if(curSS.Payment__c == curPayment.id)continue;
					if(curSS.Month_Ending__c > curPayment.Payment_Date__c)listMRSsToUpsert.add(curSS);
					if(prevMrSS==null){
						if(curSS.Month_Ending__c < curPayment.Payment_Date__c)prevMrSS=curSS;
						continue;
					}
					if(curSS.Month_Ending__c < curPayment.Payment_Date__c && prevMrSS.Month_Ending__c <curSS.Month_Ending__c)prevMrSS=curSS;    
				}
	
				LeaseId = mapLeaseIds.get(mapInvIds.get(curPayment.id));
	
				CurMrSs = mapAllMRSnapshots.get(LeaseId+'-'+curPayment.Id);
				if(CurMrSs==null){
					CurMrSs = new MR_Snapshot__c();
					if(prevMrSS!=null)CurMrSs=prevMrSS.clone();
					CurMrSs.Name='Payment - ' + curPayment.Payment_Date__c.format();
					CurMrSs.lease__c=LeaseId;
					CurMrSs.Payment__c = curPayment.Id;
					CurMrSs.Type__c = curPayment.Invoice_Type__c+' Payment'; 
					CurMrSs.Month_Ending__c=curPayment.Payment_Date__c;
				}
	
	
				if(prevMrSS == null){
					
	***				CurMrSs.Y_H_NMRInv_APU__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_APU__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_APU__c);
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_1__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_1_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_2__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_2__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2__c);
					CurMrSs.Y_H_NMRInv_Engine_2_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_2_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_3__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_3__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3__c);
					CurMrSs.Y_H_NMRInv_Engine_3_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_3_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_4__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_4__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4__c);
					CurMrSs.Y_H_NMRInv_Engine_4_LLP__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_Engine_4_LLP__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_1__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_2__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_3__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_4__c) + leasewareUtils.zeroIfNull(curInv.Maint_Reserve_Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Left_Wing__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Nose__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Nose__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Main__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Wing__c) + leasewareUtils.zeroIfNull(curInv.Maintenance_Reserve_LG_Right_Wing__c);
		
					CurMrSs.Y_H_NMRCol_APU__c = curUR.Aircraft__r.Maintenance_Reserve_APU__c;
					CurMrSs.Y_H_NMRCol_Engine_1__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_1__c;
					CurMrSs.Y_H_NMRCol_Engine_1_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_1_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_2__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_2__c;
					CurMrSs.Y_H_NMRCol_Engine_2_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_2_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_3__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_3__c;
					CurMrSs.Y_H_NMRCol_Engine_3_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_3_LLP__c;
					CurMrSs.Y_H_NMRCol_Engine_4__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_4__c;
					CurMrSs.Y_H_NMRCol_Engine_4_LLP__c = curUR.Aircraft__r.Maintenance_Reserve_Engine_4_LLP__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_1__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_2__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_3__c;
					CurMrSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c = curUR.Aircraft__r.Maintenance_Reserve_Heavy_Maintenance_4__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Main__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Main__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Left_Wing__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Nose__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Nose__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Main__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Main__c;
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c = curUR.Aircraft__r.Maintenance_Reserve_LG_Right_Wing__c;
	***
				}else{
	
	*** Not required. CurMrSS is a clone of PrevMrSs
	
					CurMrSs.Y_H_NMRInv_APU__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_APU__c) ;
					CurMrSs.Y_H_NMRInv_Engine_1__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1__c);
					CurMrSs.Y_H_NMRInv_Engine_1_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_1_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_2__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_2__c);
					CurMrSs.Y_H_NMRInv_Engine_2_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_2_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_3__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_3__c);
					CurMrSs.Y_H_NMRInv_Engine_3_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_3_LLP__c);
					CurMrSs.Y_H_NMRInv_Engine_4__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_4__c);
					CurMrSs.Y_H_NMRInv_Engine_4_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Engine_4_LLP__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRInv_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Left_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Left_Wing__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Nose__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Right_Main__c);
					CurMrSs.Y_H_NMRInv_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRInv_Landing_Gear_Right_Wing__c);
	***
					
					CurMrSs.Y_H_NMRCol_APU__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_APU__c)+ leasewareUtils.zeroIfNull(curPayment.APU__c);
					CurMrSs.Y_H_NMRCol_Engine_1__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_1__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1__c);
					CurMrSs.Y_H_NMRCol_Engine_1_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1_LLP__c);
					CurMrSs.Y_H_NMRCol_Engine_2__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_2__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2__c);
					CurMrSs.Y_H_NMRCol_Engine_2_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2_LLP__c);
					CurMrSs.Y_H_NMRCol_Engine_3__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_3__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3__c);
					CurMrSs.Y_H_NMRCol_Engine_3_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3_LLP__c);
					CurMrSs.Y_H_NMRCol_Engine_4__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_4__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4__c);
					CurMrSs.Y_H_NMRCol_Engine_4_LLP__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4_LLP__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_1_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_1_Airframe__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_2_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_2_Airframe__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_3_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_3_Airframe__c);
					CurMrSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Heavy_Maint_4_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_4_Airframe__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Left_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Main__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Left_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Wing__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Nose__c) + leasewareUtils.zeroIfNull(curPayment.LG_Nose__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Right_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Main__c);
					CurMrSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(prevMrSS.Y_H_NMRCol_Landing_Gear_Right_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Wing__c);
				}
				for(MR_Snapshot__c thisMRSs: listMRSsToUpsert){
					thisMRSs.Y_H_NMRCol_APU__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_APU__c)+ leasewareUtils.zeroIfNull(curPayment.APU__c);
					thisMRSs.Y_H_NMRCol_Engine_1__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_1__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1__c);
					thisMRSs.Y_H_NMRCol_Engine_1_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_1_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_1_LLP__c);
					thisMRSs.Y_H_NMRCol_Engine_2__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_2__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2__c);
					thisMRSs.Y_H_NMRCol_Engine_2_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_2_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_2_LLP__c);
					thisMRSs.Y_H_NMRCol_Engine_3__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_3__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3__c);
					thisMRSs.Y_H_NMRCol_Engine_3_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_3_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_3_LLP__c);
					thisMRSs.Y_H_NMRCol_Engine_4__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_4__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4__c);
					thisMRSs.Y_H_NMRCol_Engine_4_LLP__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Engine_4_LLP__c) + leasewareUtils.zeroIfNull(curPayment.Engine_4_LLP__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_1_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_1_Airframe__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_2_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_2_Airframe__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_3_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_3_Airframe__c);
					thisMRSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Heavy_Maint_4_Airframe__c) + leasewareUtils.zeroIfNull(curPayment.Heavy_Maint_4_Airframe__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Main__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Main__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Left_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Left_Wing__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Nose__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Nose__c) + leasewareUtils.zeroIfNull(curPayment.LG_Nose__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Main__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Main__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Main__c);
					thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c = leasewareUtils.zeroIfNull(thisMRSs.Y_H_NMRCol_Landing_Gear_Right_Wing__c) + leasewareUtils.zeroIfNull(curPayment.LG_Right_Wing__c);
				}
				listMRSsToUpsert.add(curMrSs);
				mapAllMRSnapshots.put(LeaseId+'-'+curPayment.Id, CurMrSs); 
				listAllMRSnapshots.add(CurMrSs);
			} 
			*/
        }
        
    }
    LandingGear = (LandingGearLM+LandingGearRM+LandingGearNose+LandingGearLW+LandingGearRW);

    //Future call to be sure it works even when payment date is updated to an earlier date. 
    leasewareUtils.updateLatestPaymentOnLease(LeaseID);

    if(!isImpacted)return;

system.debug('Engine1 is ' + Engine1);
system.debug('Engine1 LLP is ' + Engine1LLP);

    //Update NMR Balances.
    thisAC.Maintenance_Reserve_APU__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_APU__c)+APU;
    thisAC.Maintenance_Reserve_Engine_1__c =LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_1__c) + Engine1;
    thisAC.Maintenance_Reserve_Engine_1_LLP__c= LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_1_LLP__c) + Engine1LLP;
    thisAC.Maintenance_Reserve_Engine_2__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_2__c) + Engine2;
    thisAC.Maintenance_Reserve_Engine_2_LLP__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_2_LLP__c) + Engine2LLP;
    thisAC.Maintenance_Reserve_Engine_3__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_3__c) + Engine3;
    thisAC.Maintenance_Reserve_Engine_3_LLP__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_3_LLP__c) + Engine3LLP;
    thisAC.Maintenance_Reserve_Engine_4__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_4__c) + Engine4; 
    thisAC.Maintenance_Reserve_Engine_4_LLP__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Engine_4_LLP__c) + Engine4LLP;
    thisAC.Maintenance_Reserve_Heavy_Maintenance_1__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Heavy_Maintenance_1__c) + Heavy1;
    thisAC.Maintenance_Reserve_Heavy_Maintenance_2__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Heavy_Maintenance_2__c) + Heavy2;
    thisAC.Maintenance_Reserve_Heavy_Maintenance_3__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Heavy_Maintenance_3__c) + Heavy3;
    thisAC.Maintenance_Reserve_Heavy_Maintenance_4__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Heavy_Maintenance_4__c) + Heavy4;
    thisAC.Maintenance_Reserve_Landing_Gear__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Landing_Gear__c) + LandingGear;
    thisAC.Maintenance_Reserve_Other__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_Other__c) + Other;
    thisAC.Maintenance_Reserve_20_Month_C_Check__c = LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_20_Month_C_Check__c) + TwencyMoCCheck;


    //Update CMR balances
    thisAC.Cash_Maintenance_Reserve_APU__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_APU__c)+APU;
    thisAC.Cash_Maintenance_Reserve_Engine_1__c =LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_1__c) + Engine1;
    thisAC.Cash_Maintenance_Reserve_Engine_1_LLP__c= LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_1_LLP__c) + Engine1LLP;
    thisAC.Cash_Maintenance_Reserve_Engine_2__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_2__c) + Engine2;
    thisAC.Cash_Maintenance_Reserve_Engine_2_LLP__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_2_LLP__c) + Engine2LLP;
    thisAC.Cash_Maintenance_Reserve_Engine_3__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_3__c) + Engine3;
    thisAC.Cash_Maintenance_Reserve_Engine_3_LLP__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_3_LLP__c) + Engine3LLP;
    thisAC.Cash_Maintenance_Reserve_Engine_4__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_4__c) + Engine4; 
    thisAC.Cash_Maintenance_Reserve_Engine_4_LLP__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Engine_4_LLP__c) + Engine4LLP;
    thisAC.Cash_MR_Heavy_Maintenance_1__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_MR_Heavy_Maintenance_1__c) + Heavy1;
    thisAC.Cash_MR_Heavy_Maintenance_2__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_MR_Heavy_Maintenance_2__c) + Heavy2;
    thisAC.Cash_MR_Heavy_Maintenance_3__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_MR_Heavy_Maintenance_3__c) + Heavy3;
    thisAC.Cash_MR_Heavy_Maintenance_4__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_MR_Heavy_Maintenance_4__c) + Heavy4;
    thisAC.Cash_Maintenance_Reserve_Landing_Gear__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Landing_Gear__c) + LandingGear;
    thisAC.Cash_Maintenance_Reserve_Other__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_Other__c) + Other;
    thisAC.Cash_MR_20_Month_C_Check__c = LeaseWareUtils.zeroIfNull(thisAC.Cash_MR_20_Month_C_Check__c) + TwencyMoCCheck;

    //Update LG NMR/CMR balances
    thisAC.Maintenance_Reserve_LG_Center_Main__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_LG_Center_Main__c) + LandingGearCM;
    thisAC.Maintenance_Reserve_LG_Left_Main__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_LG_Left_Main__c) + LandingGearLM;
    thisAC.Maintenance_Reserve_LG_Right_Main__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_LG_Right_Main__c) + LandingGearRM;
    thisAC.Maintenance_Reserve_LG_Left_Wing__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_LG_Left_Wing__c) + LandingGearLW;
    thisAC.Maintenance_Reserve_LG_Right_Wing__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_LG_Right_Wing__c) + LandingGearRW;
    thisAC.Maintenance_Reserve_LG_Nose__c=LeaseWareUtils.zeroIfNull(thisAC.Maintenance_Reserve_LG_Nose__c) + LandingGearNose;

    thisAC.Cash_Maintenance_Reserve_LG_Center_Main__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_LG_Center_Main__c) + LandingGearCM;
    thisAC.Cash_Maintenance_Reserve_LG_Left_Main__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_LG_Left_Main__c) + LandingGearLM;
    thisAC.Cash_Maintenance_Reserve_LG_Right_Main__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_LG_Right_Main__c) + LandingGearRM;
    thisAC.Cash_Maintenance_Reserve_LG_Left_Wing__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_LG_Left_Wing__c) + LandingGearLW;
    thisAC.Cash_Maintenance_Reserve_LG_Right_Wing__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_LG_Right_Wing__c) + LandingGearRW;
    thisAC.Cash_Maintenance_Reserve_LG_Nose__c=LeaseWareUtils.zeroIfNull(thisAC.Cash_Maintenance_Reserve_LG_Nose__c) + LandingGearNose;


/*
	//TODO - Here, assumption is that all payments belong to the same invoice. Need to remove that BIG assumption.
	 
 	Invoice__c curInv = allInvs[0];
	curInv.Y_H_NMRCol_APU__c = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_APU__c) + APU;
	curInv.Y_H_NMRCol_Engine_1__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_1__c ) + Engine1;
	curInv.Y_H_NMRCol_Engine_1_LLP__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_1_LLP__c ) + Engine1LLP;
	curInv.Y_H_NMRCol_Engine_2__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_2__c ) + Engine2;
	curInv.Y_H_NMRCol_Engine_2_LLP__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_2_LLP__c ) + Engine2LLP;
	curInv.Y_H_NMRCol_Engine_3__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_3__c ) + Engine3;
	curInv.Y_H_NMRCol_Engine_3_LLP__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_3_LLP__c ) + Engine3LLP;
	curInv.Y_H_NMRCol_Engine_4__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_4__c ) + Engine4;
	curInv.Y_H_NMRCol_Engine_4_LLP__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Engine_4_LLP__c ) + Engine4LLP;
	curInv.Y_H_NMRCol_Heavy_Maint_1_Airframe__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Heavy_Maint_1_Airframe__c ) + Heavy1;
	curInv.Y_H_NMRCol_Heavy_Maint_2_Airframe__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Heavy_Maint_2_Airframe__c ) + Heavy2;
	curInv.Y_H_NMRCol_Heavy_Maint_3_Airframe__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Heavy_Maint_3_Airframe__c ) + Heavy3;
	curInv.Y_H_NMRCol_Heavy_Maint_4_Airframe__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Heavy_Maint_4_Airframe__c ) + Heavy4;
	curInv.Y_H_NMRCol_Landing_Gear_Left_Main__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Landing_Gear_Left_Main__c ) + LandingGearLM;
	curInv.Y_H_NMRCol_Landing_Gear_Left_Wing__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Landing_Gear_Left_Wing__c ) + LandingGearLW;
	curInv.Y_H_NMRCol_Landing_Gear_Nose__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Landing_Gear_Nose__c ) + LandingGearNose;
	curInv.Y_H_NMRCol_Landing_Gear_Right_Main__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Landing_Gear_Right_Main__c ) + LandingGearRM;
	curInv.Y_H_NMRCol_Landing_Gear_Right_Wing__c  = leasewareUtils.zeroIfNull(curInv.Y_H_NMRCol_Landing_Gear_Right_Wing__c ) + LandingGearRW;
*/	 
    update thisAC;
/*
	if(LeasewareUtils.getLessor().Disable_MR_Snapshots__c==true){
		system.debug('MR Snapshot creation is disabled on Setup. Not creating snapshots.');
	}else {
	    if(listMRSsToUpsert.size()>0)upsert listMRSsToUpsert;
	}
*/
}