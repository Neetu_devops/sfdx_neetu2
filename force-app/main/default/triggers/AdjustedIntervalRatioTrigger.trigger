trigger AdjustedIntervalRatioTrigger on Adjusted_Interval_Ratio__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
     if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Adjusted_Interval_Ratio__c.sObjectType);
}