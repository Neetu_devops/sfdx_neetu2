trigger PayoutTrigger on payout__c (after delete, after insert, after undelete, 
                                    after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(payout__c.sObjectType);
}