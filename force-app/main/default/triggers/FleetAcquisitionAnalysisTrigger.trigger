trigger FleetAcquisitionAnalysisTrigger on Fleet_Acquisition_Analysis__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Fleet_Acquisition_Analysis__c.sObjectType);

}