trigger AircraftTypeMapGetGlobalFleetInfo on Aircraft_Type_Map__c (before insert, after insert, before update, after update, before delete) {

	if(LeaseWareUtils.isTriggerDisabled())return;

/*	if(LeaseWareUtils.isFromTrigger())return;
	LeaseWareUtils.setFromTrigger();
*/
    string strQuery='SELECT AircraftStatus__c Status, count(Id) RecCount FROM Global_Fleet__c ';
    string strGroupBy=' GROUP BY AircraftStatus__c';

    string strOpQuery='SELECT Aircraft_Operator__c Op, Aircraft_Operator__r.Name OpName FROM Global_Fleet__c ';
    string strOpGroupBy=' GROUP BY Aircraft_Operator__c, Aircraft_Operator__r.Name';

    string strThisQry, strCondition;
    AggregateResult[] allGFs;

    list<Aircraft_Type_Map_Operator__c> listNewAcMapOps = new list<Aircraft_Type_Map_Operator__c>();
    list<Aircraft_Type_Map_Operator__c> listAcMapOpsToDelete = new list<Aircraft_Type_Map_Operator__c>();

    list<Aircraft_Type_Operator__c> listNewAcOps = new list<Aircraft_Type_Operator__c>();
    list<Aircraft_Type_Operator__c> listAcOpsToDelete = new list<Aircraft_Type_Operator__c>();
    
	set<string> setExistingOps = new set<string>();
	
    list<Aircraft_Type_Map__c> listTrigRecs = (trigger.isDelete?trigger.old:trigger.new);
    if(trigger.isAfter){
    	set<id> setAcTypes = new set<Id>();
	    for(Aircraft_Type_Map__c curAcType :listTrigRecs){
	    	setAcTypes.add(curAcType.Aircraft_Type__c);
	    }
    	Aircraft_Type_Operator__c[] listExistingOps = [select id, Aircraft_Type__c, Operator__c from Aircraft_Type_Operator__c where Aircraft_Type__c in :setAcTypes];
    	for(Aircraft_Type_Operator__c curAcOp: listExistingOps){
    		setExistingOps.add(String.valueOf(curAcOp.Aircraft_Type__c).left(15)+'-'+String.valueOf(curAcOp.Operator__c).left(15));
    	}
    }
    
    for(Aircraft_Type_Map__c curAcType :listTrigRecs){
        if(curAcType.Type__c==null){
            curAcType.Type__c.addError('Please choose an aircraft type.');
            continue;
        }
        strCondition=('AircraftType__c like \'' + curAcType.Type__c  + '%\' AND ');
        strCondition+=('AircraftSeries__c like \'' + (curAcType.Series__c==null?'':curAcType.Series__c)  + '%\' AND ');
        strCondition+=('AircraftVariant__c like \'' + (curAcType.Variant__c==null?'':curAcType.Variant__c) + '%\' AND ');
        if(curAcType.Engine_Type__c==null){
        	strCondition+=('EngineType__c like \'%\' AND ');
        }else{
        	string strEngType=curAcType.Engine_Type__c;
        	if(strEngType.startsWith('CFM56'))strEngType='CFM56';
        	else if(strEngType.startsWith('CF6'))strEngType='CF6';
        	else if(strEngType.startsWith('PW4'))strEngType='PW4';
        	else if(strEngType.startsWith('RB211'))strEngType='RB211';
        	else if(strEngType.startsWith('GE90'))strEngType='GE90';
        	else if(strEngType.startsWith('Trent'))strEngType='Trent';
        	else if(strEngType.startsWith('V25'))strEngType='V25';
        	
        	strCondition+=('EngineType__c like \'' + strEngType + '%\' AND ');
        }
        
	    strCondition=' WHERE ' + strCondition.left(strCondition.length()-4);

        if(trigger.isBefore ){// Update summary fields on current rec.

            if(trigger.isUpdate || trigger.isDelete){//Delete the child records
system.debug('Deleting existing recs..');
                listAcMapOpsToDelete.addAll([select id from Aircraft_Type_Map_Operator__c where Aircraft_Type_Map__c = :curAcType.id]);
                listAcOpsToDelete.addAll([select id from Aircraft_Type_Operator__c where Aircraft_Type__c = :curAcType.Aircraft_Type__c AND CreatedByMap__c=:curAcType.id]);
            }

			if(trigger.isDelete)continue; //Nothing more to do if delete.
			
            curAcType.No_Of_Aircraft_in_Service__c=0;
            curAcType.No_Of_Aircraft_in_LoI__c=0;
            curAcType.No_Of_Aircraft_in_Storage__c=0;
            curAcType.No_Of_Aircraft_On_Order__c=0;
    
            strThisQry =  strQuery + strCondition + strGroupBy;
    system.debug('Query is ' + strThisQry);
            allGFs = database.query(strThisQry);
    
            for(AggregateResult curGF: allGFs){
    system.debug('Status and Count ' + curGF.get('Status') + ', ' + curGF.get('RecCount'));
                string strStatus=(string)(curGF.get('Status'));
                if('In Service'.equals(strStatus))curAcType.No_Of_Aircraft_in_Service__c=(decimal)(curGF.get('RecCount'));
                else if('LoI'.equals(strStatus))curAcType.No_Of_Aircraft_in_LoI__c=(decimal)(curGF.get('RecCount'));
                else if('Storage'.equals(strStatus))curAcType.No_Of_Aircraft_in_Storage__c=(decimal)(curGF.get('RecCount'));
                else if('Order'.equals(strStatus))curAcType.No_Of_Aircraft_On_Order__c=(decimal)(curGF.get('RecCount'));
            }
        	
        }else{// Insert/update child records
            strThisQry =  strOpQuery + strCondition + strOpGroupBy;
            allGFs = database.query(strThisQry);
            for(AggregateResult curGF: allGFs){
                Id OpId=(Id)curGF.get('Op');
                if(OpId==null)continue;

system.debug('Inserting recs..');
                listNewAcMapOps.add(new Aircraft_Type_Map_Operator__c(Aircraft_Type_Map__c=curAcType.id, Operator__c=OpId, 
                    Name=curAcType.Type__c + '-' + curGF.get('OpName')));
                if(!setExistingOps.contains(String.valueOf(curAcType.Aircraft_Type__c).left(15)+'-'+String.valueOf(OpId).left(15))){    
                	listNewAcOps.add(new Aircraft_Type_Operator__c(Aircraft_Type__c=curAcType.Aircraft_Type__c, Operator__c=OpId, 
                    	Name=curAcType.Type__c + '-' + curGF.get('OpName'), CreatedByMap__c=curAcType.id));
                    setExistingOps.add(String.valueOf(curAcType.Aircraft_Type__c).left(15)+'-'+String.valueOf(OpId).left(15));
                }
            }
        }
    }
    if(listAcMapOpsToDelete.size()>0)delete listAcMapOpsToDelete;
    if(listNewAcMapOps.size()>0)insert listNewAcMapOps;

    if(listAcOpsToDelete.size()>0)delete listAcOpsToDelete;
    if(listNewAcOps.size()>0)insert listNewAcOps;
}