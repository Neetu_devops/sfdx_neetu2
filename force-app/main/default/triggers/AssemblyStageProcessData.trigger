trigger AssemblyStageProcessData on Assembly_Stage__c (after insert) {

    boolean bDisableAll=false, bDisableEngine=false;

    LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
    if(lwSettings!=null){
        bDisableAll=lwSettings.Disable_All_Integrations__c;
        bDisableEngine=lwSettings.Disable_Aircraft_Integration__c;
    }

  if(bDisableAll || bDisableEngine){
    system.debug('Engine integration disabled. Returning.');
    return;
  }

  map<string, Constituent_Assembly__c> mapExistingEngines = new map <string, Constituent_Assembly__c>();

  Map<String, Schema.SObjectField> mapEngineFields = Schema.SObjectType.Assembly_Stage__c.fields.getMap();

  string strQry = 'select ';
  for(String Field:mapEngineFields.keyset()){
        Schema.DescribeFieldResult thisFieldDesc = mapEngineFields.get(Field).getDescribe();

system.debug('Field ' + thisFieldDesc.getLocalName());

        if(!thisFieldDesc.isUpdateable())continue;
        if(thisFieldDesc.getLocalName().startsWith('Lookup_')){
//TODO - Handle lookup fields.              
//           strQry+= (thisFieldDesc.getName().replace('Lookup_', '') + ', ');
        }else{
system.debug('Getting val for ' + thisFieldDesc.getLocalName());
            strQry+= thisFieldDesc.getLocalName() + ', ';
        }
  } 
  strQry = strQry.left(strQry.length()-2);
  strQry+=' from ' + Schema.SObjectType.Constituent_Assembly__c.getName();
  system.debug('strQry===='+strQry);
  Constituent_Assembly__c[] listAllEngine =  database.query(strQry);
  
  for(Constituent_Assembly__c curEngine: listAllEngine){
    mapExistingEngines.put(curEngine.Engine_ID__c, curEngine);
  }
  
  set<Constituent_Assembly__c> setEngineToUpdate = new set<Constituent_Assembly__c>();

  for(Assembly_Stage__c curEngineStage : trigger.new){

    if(curEngineStage.Engine_ID__c==null)continue; //External Id cannot be null

    Constituent_Assembly__c thisEngine = mapExistingEngines.get(curEngineStage.Engine_ID__c);

    if(thisEngine==null){ //New Engine.
      Id RcTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();  
      thisEngine = new Constituent_Assembly__c(RecordTypeId=RcTypeId, Engine_ID__c=curEngineStage.Engine_ID__c);
      for(String Field:mapEngineFields.keyset()){
              Schema.DescribeFieldResult thisFieldDesc = mapEngineFields.get(Field).getDescribe();
              if(!thisFieldDesc.isUpdateable())continue;
              if(!thisFieldDesc.isCustom() && !Field.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
              
              if(thisFieldDesc.getLocalName().startsWith('Lookup')){
    //TODO - Handle lookup fields.              
              }else{
system.debug('Putting ' + thisFieldDesc.getLocalName().toLowerCase());              
                thisEngine.put(thisFieldDesc.getLocalName(), curEngineStage.get(Field));
              }
      }
      //Required fields if null
      thisEngine.Type__c=thisEngine.Type__c==null?'Engine 1':thisEngine.Type__c;
      thisEngine.TSN__c=thisEngine.TSN__c==null?0:thisEngine.TSN__c;
      thisEngine.CSN__c=thisEngine.CSN__c==null?0:thisEngine.CSN__c;
      thisEngine.TSLV__c=thisEngine.TSLV__c==null?0:thisEngine.TSLV__c;
      thisEngine.CSLV__c=thisEngine.CSLV__c==null?0:thisEngine.CSLV__c;
      

      setEngineToUpdate.add(thisEngine);
    }else{//Existing Engine.

      boolean bUpdated = false;  
      for(String fld:mapEngineFields.keyset()){
        Schema.DescribeFieldResult thisFieldDesc = mapEngineFields.get(fld).getDescribe();
        if(!thisFieldDesc.isUpdateable())continue; //Skip all system fields like createdBy.
        if(!thisFieldDesc.isCustom() && !fld.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
        if(curEngineStage.get(fld)!=null){ // Update only if incoming field is not null. //thisEngine.get(Field)!=null || 
          if('Name'.equals(thisFieldDesc.getLocalName()))continue;
          if(thisFieldDesc.getLocalName().startsWith('Lookup')){
//TODO - Handle lookup fields.              
          }else{
              if(LeaseWareUtils.IsNotSame(thisEngine.get(fld), curEngineStage.get(fld), thisFieldDesc.getType())){
    system.debug('Field updated ' + thisFieldDesc.getLocalName() + ' : ' + thisEngine.get(fld) + '->' + curEngineStage.get(fld));
                      thisEngine.put(fld, curEngineStage.get(fld)); //mapLWEngineFieldsStripped.get(thisFieldDesc.getName().toLowerCase())
                bUpdated=true;
              }
          }
        }
      }
      if(bUpdated)setEngineToUpdate.add(thisEngine);
    }      
  }

system.debug('Upserting '+ setEngineToUpdate.size() + '->' + setEngineToUpdate);
  if(setEngineToUpdate.size()>0){
    list<Constituent_Assembly__c> listEngineToUpdate = new list<Constituent_Assembly__c>(setEngineToUpdate);
    upsert listEngineToUpdate Engine_ID__c;
  } 

    
}