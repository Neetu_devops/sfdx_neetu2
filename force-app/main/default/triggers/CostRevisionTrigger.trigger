trigger CostRevisionTrigger on Cost_Revision__c (after update) {
    if(LeaseWareUtils.isTriggerDisabled())return;
    TriggerFactory.createHandler(Cost_Revision__c.sObjectType);
}