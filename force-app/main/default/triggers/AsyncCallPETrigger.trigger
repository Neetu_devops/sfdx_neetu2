trigger AsyncCallPETrigger on AsyncCallPE__e (after insert) {

    if(LeaseWareUtils.isTriggerDisabled()) {return;}
    AsyncUtil.aynchPETriggerHandlerCall();

}