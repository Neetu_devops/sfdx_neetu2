trigger AssetHistoryTrigger on Asset_History__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Asset_History__c.sObjectType);       
    
}