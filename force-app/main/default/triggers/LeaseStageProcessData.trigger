trigger LeaseStageProcessData on Lease_Stage__c (after insert) {

    boolean bDisableAll=false, bDisableLease=false, bCreateDummyLeases=false;

    LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
    if(lwSettings!=null){
        bDisableAll=lwSettings.Disable_All_Integrations__c;
        bDisableLease=lwSettings.Disable_Lease_Integration__c;
        bCreateDummyLeases = lwSettings.Create_Dummy_Leases__c;
    }

    if(bDisableAll || bDisableLease){
        system.debug('Lease integration disabled. Returning.');
        return;
    }

    private List<Mapping__c> listVariantMap = [Select Name,Map_To__c from Mapping__c where IsActive__c = true];

    map<string, Lease__c> mapExistingLease = new map <string, Lease__c>();
    map<string, list<Lease__c>> mapExistingLeasesByAicraft = new map <string, list<Lease__c>>();
    

    Map<String, Schema.SObjectField> mapLeaseFields = Schema.SObjectType.Lease_Stage__c.fields.getMap();

    string strQry = 'select ';
    for(String Field:mapLeaseFields.keyset()){
        Schema.DescribeFieldResult thisFieldDesc = mapLeaseFields.get(Field).getDescribe();
        if(!thisFieldDesc.isUpdateable() || thisFieldDesc.getLocalName().contains('Incoming_Data__c'))continue;
        if(thisFieldDesc.getLocalName().contains('Lookup_')){//Special processing for lookup fields. 
            //While naming lookup fields, get the field name from the actual object just prefix with 'Lookup_' 
             strQry+= (thisFieldDesc.getName().replace('Lookup_', '') + ', ');
        }else{
            strQry+= (thisFieldDesc.getName() + ', ');
        }
    } 
    strQry = strQry.left(strQry.length()-2);
    strQry+=' from ' + Schema.SObjectType.Lease__c.getName();
    
    Lease__c[] listAllLease =  database.query(strQry);
    
    list<Lease__c> tempListLease ;
    for(Lease__c curLease: listAllLease){
        mapExistingLease.put(curLease.Lease_External_Id__c, curLease);
        if(String.isNotBlank(curLease.Lease_Id_External__c)){mapExistingLease.put(curLease.Lease_Id_External__c, curLease);}
        if(curLease.Aircraft__c!=null){
            if(!mapExistingLeasesByAicraft.containsKey(String.ValueOf(curLease.Aircraft__c).left(15))){
                tempListLease = new list<Lease__c>();
            }else{
                tempListLease = mapExistingLeasesByAicraft.get(String.ValueOf(curLease.Aircraft__c).left(15));
            }
            tempListLease.add(curLease);
            mapExistingLeasesByAicraft.put(String.ValueOf(curLease.Aircraft__c).left(15),tempListLease);
        }
    }

    map<string, Id> mapAircraft = new map<string, Id>();
    for(aircraft__c curAC : [select id, aircraft_Id__c from aircraft__c]){
        mapAircraft.put(curAC.aircraft_Id__c, curAC.Id);
    }
    
    map<string, Id> mapOperators = new map<string, Id>();
    for(Operator__c curOp : [select id, Name, Alias__c,Lessee_Legal_Name__c,World_Fleet_Operator_Name__c from Operator__c]){
        mapOperators.put(curOp.Name.toLowerCase(), curOp.Id);
         if(String.isNotBlank(curOp.Lessee_Legal_Name__c)){
                mapOperators.put(curOp.Lessee_Legal_Name__c.toLowerCase(),curOp.Id);                    
            }
            if(String.isNotBlank(curOp.World_Fleet_Operator_Name__c)){
                mapOperators.put(curOp.World_Fleet_Operator_Name__c.toLowerCase(),curOp.Id);
            }
            if(String.isNotBlank(curOp.Alias__c)){
                mapOperators.put(curOp.Alias__c.toLowerCase(),curOp.Id);
            }
        if(String.isNotBlank(curOp.Alias__c)){
            for(string strAlias : curOp.Alias__c.split(',')){
                strAlias=strAlias.trim();
                if(curOp.Name.equals(strAlias)){continue;}
                if(''.equals(strAlias))continue;
                mapOperators.put(strAlias.toLowerCase(), curOp.Id);
            }
        }
    }
    
    list<Lease__c> listLeaseToUpdate = new list<Lease__c>();
    Map<String,Lease__c> mapLeasesToUpsert = new Map<String,Lease__c>();
    list<Lease__c> listAOGLeaseToUpdate = new list<Lease__c>();
    Map<String, Lease_Stage__c> mapToLease = new Map<String, Lease_Stage__c>();
    Map<Id,String> mapIdToTypeVariant = new Map<Id,String>();
   	Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Lease__c.getRecordTypeInfosByDeveloperName();
    Set<String> setAircraftId = new Set<String>();
    Set<String> setIncomingDataId = new Set<String>();
    String lookupAircraftId = '';
    for(Lease_Stage__c curLeaseStage : trigger.new){ 	
        setIncomingDataId.add(curLeaseStage.Incoming_Data__c);
    }
    Map<String,String> mapAcToClass = new Map<String,String>();
    for(Aircraft_Stage__c curRec:[select Aircraft_ID__c,Asset_Id_External__c,Incoming_Data__c,MSN_Number__c,Aircraft_Variant__c,Aircraft_Type__c,Asset_Class__c from Aircraft_Stage__c where Incoming_Data__c =:setIncomingDataId]){
        List<String> listTypeAndVariant = new List<String>();
        //String variant = curRec.Aircraft_Type__c +'-'+ curRec.Aircraft_Variant__c;
        String variant = (String.isNotBlank(curRec.Aircraft_Type__c) ?  curRec.Aircraft_Type__c.trim() : '') +'-'+
        (String.isNotBlank(curRec.Aircraft_Variant__c) ?  curRec.Aircraft_Variant__c.trim() : '') ;
        listTypeAndVariant = LeaseWareUtils.setAssetTypeVariant(listVariantMap, variant);
        String aircraftId =listTypeAndVariant[0]+'-'+curRec.MSN_Number__c;    
        mapAcToClass.put(aircraftId, curRec.Asset_Class__c);
        mapIdToTypeVariant.put(curRec.Incoming_Data__c,aircraftId); // need this for cases if there is bulk upload of incoming data.
    } 
    //clearing heap memory
    setIncomingDataId.clear();
      
    List<Lease_Stage__c> listLease = trigger.new;
    for(Lease_Stage__c lease: trigger.new){
            mapToLease.put(lease.Name, lease);
    }
  
    for(Lease_Stage__c curLeaseStage : mapToLease.values()){ 
        System.debug('LookupAircraftId coming from Incomingdata :' + curLeaseStage.Lookup_Aircraft__c); 
        String lookupAircraftId = mapIdToTypeVariant.get(curLeaseStage.Incoming_Data__c);
        System.debug('LookupAircraftId after checking in Map  :' + lookupAircraftId); 
        Id assetId = (id)(mapAircraft.get(lookupAircraftId));
        Id operatorId ;

        if(String.isNotBlank(curLeaseStage.Lookup_Lessee__c)){
            operatorId = (id)(mapOperators.get(curLeaseStage.Lookup_Lessee__c.toLowerCase()));
        }   
        if(operatorId == null && String.isNotBlank(curLeaseStage.Lookup_Operator__c)){
            operatorId = (id)(mapOperators.get(curLeaseStage.Lookup_Operator__c.toLowerCase()));
        }

        if(assetId == null){
            LeaseWareUtils.createExceptionLog(null,'Asset Id is null for Asset:'+ (lookupAircraftId == null ? null :lookupAircraftId.replace('Lookup_','')),
                                              'Lease_Stage__c',curLeaseStage.Id,'Check for Lease External Id', true);
        	continue;
        }
        Lease__c thisLease;
        String strkey = assetId +'-'+LeaseWareUtils.getDatetoString(curLeaseStage.Lease_Start_Date_New__c,'YYYYMMDD');
        String strkey2 = assetId +'-'+operatorId +'-'+LeaseWareUtils.getDatetoString(curLeaseStage.Lease_Start_Date_New__c,'YYYYMMDD');
        
        system.debug('Looking for Lease -'+curLeaseStage.Name+' - with ExternalId--'+ strkey);
        thisLease = mapExistingLease.get(strkey);
        if(thisLease == null){
            system.debug('Looking for Lease again -'+curLeaseStage.Name+' - with ExternalId--'+ strkey2);
            thisLease = mapExistingLease.get(strkey2);
         }
         // if thisLease is null check with the integration external id 
         if(thisLease == null && String.isNotBlank(curLeaseStage.Lease_Id_External__c)){
            system.debug('Looking for Lease with Lease(External)Id -'+curLeaseStage.Name+'- with Lease_Id_External__c --'+ curLeaseStage.Lease_Id_External__c);
            thisLease = mapExistingLease.get(curLeaseStage.Lease_Id_External__c);
        }
         system.debug('Selected Lease---' + thisLease);

        if(thisLease==null && curLeaseStage.AOG_Date__c != null && curLeaseStage.Lookup_Operator__c==null){//Check if this MSN is AOG and if AOG lease already exists
            string[] strLeaseNameParts = curLeaseStage.Name.split(' \\| ');
            string[] strAircraftType = lookupAircraftId.split('-');
            // As type can get reset based on value in Mapping table so need to use the same type here also .
            string strTempLeaseName = strLeaseNameParts[0] +  ' | AOG | ' + strAircraftType[0]; 
            thisLease = mapExistingLease.get(strTempLeaseName);
        }
        String assetClass;
        Id recordTypeId ;
        if(String.isNotBlank(lookupAircraftId)){
        	assetClass = mapAcToClass.get(lookupAircraftId);
        }
        // Setting RecordType Id for a new Lease. 
        if(assetClass != null && rtMapByName.get(AssetClass)!=null){
            recordTypeId = rtMapByName.get(AssetClass).getRecordTypeId();
        }else{
            recordTypeId = rtMapByName.get('Aircraft').getRecordTypeId();
        }
	
        if(thisLease==null){ //New Lease. Could be an AOG lease.
            thisLease = new Lease__c();
            thisLease.RecordTypeId = recordTypeId== null ?'':recordTypeId; 
            for(String Field:mapLeaseFields.keyset()){
                Schema.DescribeFieldResult thisFieldDesc = mapLeaseFields.get(Field).getDescribe();
                string strLocalName = thisFieldDesc.getLocalName();
                if(!thisFieldDesc.isUpdateable() || strLocalName.contains('Incoming_Data__c'))continue;
                if(!thisFieldDesc.isCustom() && !Field.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
                if(strLocalName.contains('Lookup_')){//Special processing for lookup fields
                    strLocalName=strLocalName.replace('Lookup_','');
                    Id idLookup;
                    if('Aircraft__c'.equals(strLocalName)){
                        idLookup = (id)(mapAircraft.get((string)curLeaseStage.get(Field)));
                        if(idLookup == null){
                            idLookup = (id)(mapAircraft.get(mapIdToTypeVariant.get(curLeaseStage.Incoming_Data__c)));
                        }
                    }else if('Operator__c'.equals(strLocalName)  && String.isNotBlank((string)curLeaseStage.get(Field))){
                        idLookup = (id)mapOperators.get(((string)curLeaseStage.get(Field)).toLowerCase());
                    }else if('Lessee__c'.equals(strLocalName) && String.isNotBlank((string)curLeaseStage.get(Field))){
                        idLookup = (id)mapOperators.get(((string)curLeaseStage.get(Field)).toLowerCase());
                    }
                    thisLease.put(strLocalName, idLookup);
                }else{
                    thisLease.put(thisFieldDesc.getName(), curLeaseStage.get(Field));
                }
            }
            if(!bCreateDummyLeases && (thisLease.Lease_Start_Date_New__c==null || thisLease.Lease_End_Date_New__c==null))continue;
            thisLease.Overdue_Check_Days__c=thisLease.Overdue_Check_Days__c==null?(10):thisLease.Overdue_Check_Days__c;
            thisLease.Rent_Due_Day__c=thisLease.Rent_Due_Day__c==null?(5):thisLease.Rent_Due_Day__c;
            thisLease.Lease_Start_Date_New__c=thisLease.Lease_Start_Date_New__c==null?Date.today():thisLease.Lease_Start_Date_New__c;
            thisLease.Lease_End_Date_New__c=thisLease.Lease_End_Date_New__c==null?Date.today().addDays(1):thisLease.Lease_End_Date_New__c;

            if(thisLease.Rent_Date__c!=null && thisLease.Rent_Date__c < thisLease.Lease_Start_Date_New__c)thisLease.Rent_Date__c = thisLease.Lease_Start_Date_New__c;
            if(thisLease.Rent_End_Date__c != null && thisLease.Rent_Date__c != null && thisLease.Rent_End_Date__c < thisLease.Rent_Date__c)thisLease.Rent_End_Date__c=thisLease.Lease_End_Date_New__c;
            
            //Special processing for AOG Aircraft.
            //If a record comes through without an operator and AOG Date filled, 
            //   create a new lease and add 'AOG' to the lease name. Set status to 'Lease Expired/Terminated'.

            if(thisLease.AOG_Date__c != null && thisLease.Operator__c==null){


                string[] strLeaseNameParts = thisLease.Name.split(' \\| ');
                string[] strAircraftType = lookupAircraftId.split('-');
                string strTempLeaseName = strLeaseNameParts[0] +  ' | AOG | ' + strAircraftType[0];
                System.debug('AOG Lease Name :' + strTempLeaseName);
                thisLease.Name=strTempLeaseName.left(80); 
				system.debug('AOG Lease.. ' + thisLease.Name);
                thisLease.Status__c='Active';

                //Get the existing lease that this aircraft is on.
                System.debug('mapExistingLeasesByAicraft:' + mapExistingLeasesByAicraft);
                System.debug('thisLease.Aircraft__c:' + thisLease.Aircraft__c);
                if(mapExistingLeasesByAicraft.containsKey(String.ValueOf(thisLease.Aircraft__c).left(15))){
                    for(Lease__c oldLease : mapExistingLeasesByAicraft.get(String.ValueOf(thisLease.Aircraft__c).left(15)))
                    {
                        oldLease.Lease_Type_Status__c='Lease Expired/Terminated';
                        //oldLease.Aircraft__c=null;
                        //Copy the Remarketing report controls from original Lease to AOG record when AOG record is created
                        //And set them to null on old lease.
                        thisLease.Anticipated_Remarketing_Status__c=oldLease.Anticipated_Remarketing_Status__c;
                        thisLease.Next_Operator__c=oldLease.Next_Operator__c;
                        thisLease.Placement_By_Date__c=oldLease.Placement_By_Date__c;
                        
                        oldLease.Anticipated_Remarketing_Status__c=null;
                        oldLease.Next_Operator__c=null;
                        oldLease.Placement_By_Date__c=null;
                        
                        listAOGLeaseToUpdate.add(oldLease);
                    }
                }

            }else{//Check if the aircraft is already on an existing lease.
                if(mapExistingLeasesByAicraft.containsKey(String.ValueOf(thisLease.Aircraft__c).left(15))){
                    for(Lease__c oldLease : mapExistingLeasesByAicraft.get(String.ValueOf(thisLease.Aircraft__c).left(15)))
                    {
                        //Lease exists..
                        //Terminating the current lease and creating a new one for now.
                        oldLease.Lease_Type_Status__c='Lease Expired/Terminated';
                        //oldLease.Aircraft__c=null;
                        listAOGLeaseToUpdate.add(oldLease);                        
                    }
                }
            }
            System.debug('RecordTypeId for Lease:' + thisLease.RecordTypeId + 'for Asset_Class:' + assetClass);
            listLeaseToUpdate.add(thisLease);
        }else{//Existing Lease.

            system.debug('Existing Lease.. ' + thisLease.Name);

            boolean bUpdated = false;   
            for(String Field:mapLeaseFields.keyset()){
                Schema.DescribeFieldResult thisFieldDesc = mapLeaseFields.get(Field).getDescribe();
                string strLocalName = thisFieldDesc.getLocalName();
                //Skip all system fields like createdBy 
                //  and do not update Name as AOG will not be prefixed in stage.
                if(!thisFieldDesc.isUpdateable() || strLocalName.equals('Name') || strLocalName.contains('Incoming_Data__c'))continue;
                if(!thisFieldDesc.isCustom() )continue; //skip standard field other than Name

                if(strLocalName.contains('Lookup_')){//Special processing for lookup fields
                    strLocalName=strLocalName.replace('Lookup_','');
                    Id idLookup, oldId;
                    oldId = (id)thisLease.get(strLocalName);
                    if('Aircraft__c'.equals(strLocalName)){
                        idLookup = (id)mapAircraft.get((string)curLeaseStage.get(Field));
                        if(idLookup == null){
                            idLookup = (id)(mapAircraft.get(mapIdToTypeVariant.get(curLeaseStage.Incoming_Data__c)));
                        }
                    }else if('Operator__c'.equals(strLocalName) && String.isNotBlank((string)curLeaseStage.get(Field))){
                        idLookup = (id)mapOperators.get(((string)curLeaseStage.get(Field)).toLowerCase());
                    }else if('Lessee__c'.equals(strLocalName) && String.isNotBlank((string)curLeaseStage.get(Field))){
                        idLookup = (id)mapOperators.get(((string)curLeaseStage.get(Field)).toLowerCase());
                    }
                    if(idLookup!=null){
                        if(LeaseWareUtils.IsNotSame(oldId, idLookup, Schema.DisplayType.Id)){
                            thisLease.put(strLocalName, idLookup);
                            bUpdated=true;
                        }
                    }
                }else{//Not lookup, ordinary field.
                    if(curLeaseStage.get(Field)!=null){// Update only if incoming value is not null.  thisLease.get(Field)!=null || 
                        if(LeaseWareUtils.IsNotSame(thisLease.get(Field), curLeaseStage.get(Field), thisFieldDesc.getType())){
                        system.debug('Field updated ' + thisFieldDesc.getName() + ' : ' + thisLease.get(Field) + '->' + curLeaseStage.get(Field));
                            thisLease.put(thisFieldDesc.getName(), curLeaseStage.get(Field));
                            bUpdated=true;
                        }
                    }
                }
            }

            if(bUpdated){
                 if(mapExistingLeasesByAicraft.containsKey(String.ValueOf(thisLease.Aircraft__c).left(15))){
                    for(Lease__c oldLease : mapExistingLeasesByAicraft.get(String.ValueOf(thisLease.Aircraft__c).left(15)))
                    {            
                        if(thisLease.id != oldLease.id){
                            //Lease exists..
                            //Terminating the current lease and creating a new one for now.
                            oldLease.Lease_Type_Status__c ='Lease Expired/Terminated';
                            //oldLease.Aircraft__c=null;
                            listAOGLeaseToUpdate.add(oldLease);
                        }
                        if(thisLease.Rent_Date__c!=null && thisLease.Rent_Date__c < thisLease.Lease_Start_Date_New__c)thisLease.Rent_Date__c = thisLease.Lease_Start_Date_New__c;
                        if(thisLease.Rent_End_Date__c != null && thisLease.Rent_Date__c != null && thisLease.Rent_End_Date__c < thisLease.Rent_Date__c)thisLease.Rent_End_Date__c=thisLease.Lease_End_Date_New__c;
                        listLeaseToUpdate.add(thisLease); 
                    }
                }
            }
        }                      
    }   
    system.debug('Upserting AOG Leases SIZE : '+ listAOGLeaseToUpdate.size());
    system.debug('Upserting AOG Leases '+ listAOGLeaseToUpdate);
    if(listAOGLeaseToUpdate.size()>0){database.update(listAOGLeaseToUpdate, false);} 
	system.debug('Upserting Leases '+ listLeaseToUpdate);
    System.debug('List size for LEASE :' + listLeaseToUpdate.size());
    try{
        LeaseWareUtils.clearFromTrigger();
        if(listLeaseToUpdate.size()>0){
           
            for(Lease__c curRec : listLeaseToUpdate){
                String key = curRec.Id+'-'+curRec.Aircraft__c;
                mapLeasesToUpsert.put(key,curRec);
            }
            system.debug('Upserting Leases MAP:'+ mapLeasesToUpsert);
            System.debug('MAP size for LEASE :' + mapLeasesToUpsert.size());
            upsert mapLeasesToUpsert.values() ;
        }
    }catch(Exception e){
            System.debug('Exception in inserting/updating Lease : ' + e.getMessage());
            LeaseWareUtils.createExceptionLog(e,'Exception in inserting/updating Lease ', 'Lease__c', String.valueOf(mapLeasesToUpsert.values()[0].id),'LeaseStageProcessData', true);
    }

}