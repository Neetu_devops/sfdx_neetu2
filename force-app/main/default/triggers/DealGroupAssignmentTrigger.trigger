trigger DealGroupAssignmentTrigger on Deal_And_Group_Assignment__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Deal_And_Group_Assignment__c.sObjectType);
}