trigger LLPAtSVTrigger on LLP_SV__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(LLP_SV__c.sObjectType);
  }