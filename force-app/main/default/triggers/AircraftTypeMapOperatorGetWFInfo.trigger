trigger AircraftTypeMapOperatorGetWFInfo on Aircraft_Type_Map_Operator__c (after insert, after update) {


    if(!Test.isRunningTest()){
		if(System.isFuture())return;
	    //LeaseWareUtils.setFromTrigger();
    }
    
    set<Id> setIds = new set<Id>();
    for(Aircraft_Type_Map_Operator__c curMapAcOp: trigger.new){
        if(curMapAcOp.id!=null)setIds.add(curMapAcOp.id);
    }
    if(!Test.isRunningTest() && setIds.size()>0)LeasewareUtils.ftrBulkUpdateAcTypeMapOps(setIds); 

}