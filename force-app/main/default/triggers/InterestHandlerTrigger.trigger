trigger InterestHandlerTrigger on Interest__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if(LeaseWareUtils.isTriggerDisabled())return;
	TriggerFactory.createHandler(Interest__c.sObjectType);

}