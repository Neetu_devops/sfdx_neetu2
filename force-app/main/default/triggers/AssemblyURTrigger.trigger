trigger AssemblyURTrigger on Assembly_Utilization__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(Assembly_Utilization__c.sObjectType); 
}