trigger PDPScheduleTrigger on PDP_Payment_Schedule__c (before update) {
    if( LeaseWareUtils.isTriggerDisabled() )return;
 	TriggerFactory.createHandler(PDP_Payment_Schedule__c.sObjectType); 
}