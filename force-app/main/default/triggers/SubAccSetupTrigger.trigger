trigger SubAccSetupTrigger on Accounting_Setup__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Accounting_Setup__c.sObjectType);
}