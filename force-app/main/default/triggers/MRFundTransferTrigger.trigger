trigger MRFundTransferTrigger on MR_Fund_Transfer__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if(LeaseWareUtils.isTriggerDisabled())return;
    TriggerFactory.createHandler(MR_Fund_Transfer__c.sObjectType);
    
}