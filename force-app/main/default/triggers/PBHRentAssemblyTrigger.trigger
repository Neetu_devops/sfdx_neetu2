trigger PBHRentAssemblyTrigger on PBH_Rent_Per_Assembly__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    TriggerFactory.createHandler(PBH_Rent_Per_Assembly__c.sObjectType);
}