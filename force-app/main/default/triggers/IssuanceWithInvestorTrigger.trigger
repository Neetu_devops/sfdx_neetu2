trigger IssuanceWithInvestorTrigger on List_of_Investors__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
   
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(List_of_Investors__c.sObjectType);   
    
}