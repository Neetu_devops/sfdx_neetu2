//Last-modified : 2016.11.20
trigger IncomingDataProcessInfo on Incoming_Data__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    if(LeaseWareUtils.isTriggerDisabled())return;
	TriggerFactory.createHandler(Incoming_Data__c.sObjectType);

 // moved to itrigger class file
   
}