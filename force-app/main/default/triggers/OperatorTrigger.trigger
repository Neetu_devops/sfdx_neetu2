trigger OperatorTrigger on Operator__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Operator__c.sObjectType);

}