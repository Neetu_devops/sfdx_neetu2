trigger VendorInvSummaryTrigger on Vendor_Invoice_Summary__c (after update) {
    Vendor_Invoice_Summary__c tempVendorInvSummary;
    
    set<string> setIds=new set<string>();
    map<string,Vendor_Invoice_Summary__c> mapVendorInvoiceSummary=new map<string,Vendor_Invoice_Summary__c>();
    for(Vendor_Invoice_Summary__c currRec: Trigger.new)
    {
        tempVendorInvSummary=Trigger.OldMap.get(currRec.Id);
        if(currRec.Approved__c != tempVendorInvSummary.Approved__c || currRec.Reviewed__c != tempVendorInvSummary.Reviewed__c || currRec.Approved_by__c != tempVendorInvSummary.Approved_by__c 
                    ||currRec.Reviewed_By__c != tempVendorInvSummary.Reviewed_By__c  )
        {
               setIds.add(currRec.Id); 
               mapVendorInvoiceSummary.put(currRec.Id,currRec);
                 
        }
        
            
    }
    
   list<Vendor_Invoice__c> vendorInvoiceList=[select Id , Cost_Center_Grouping__r.Vendor_Invoice_Summary__c
                                              from Vendor_Invoice__c 
                                              where Cost_Center_Grouping__r.Vendor_Invoice_Summary__c in : setIds];
   Vendor_Invoice_Summary__c tempVIS;
   for(Vendor_Invoice__c currRec: vendorInvoiceList)
   {
       tempVIS=mapVendorInvoiceSummary.get(currRec.Cost_Center_Grouping__r.Vendor_Invoice_Summary__c);
       currRec.Approved__c=tempVIS.Approved__c;
       currRec.Reviewed__c=tempVIS.Reviewed__c;
       currRec.Reviewed_By__c=tempVIS.Reviewed_By__c;
       currRec.Approved_by__c=tempVIS.Approved_by__c;
   }                                           
    
   update vendorInvoiceList;

}