trigger SteppedMultiRentTriggerHandler on SteppedMultiRent__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(SteppedMultiRent__c.sObjectType);
}