trigger SpecMassandBalance on Spec_Mass_and_Balance__c (before insert,after insert,before update, after update,before delete,after delete,after UnDelete) {
  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Spec_Mass_and_Balance__c.sObjectType);
}