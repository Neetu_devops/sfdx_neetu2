trigger PayoutLineItemTrigger on payout_line_item__c (after delete, after insert, after undelete, 
                    after update, before delete, before insert, before update) {
        
        if( LeaseWareUtils.isTriggerDisabled() ){return;}
        TriggerFactory.createHandler(payout_line_item__c.sObjectType);
}