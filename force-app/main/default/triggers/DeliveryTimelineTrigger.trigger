trigger DeliveryTimelineTrigger on Delivery_Timeline__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
 
 	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Delivery_Timeline__c.sObjectType);
 
}