trigger InsuranceStipulationTrigger on Insurance_Stipulation__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

if( LeaseWareUtils.isTriggerDisabled() )return;
TriggerFactory.createHandler(Insurance_Stipulation__c.sObjectType);

}