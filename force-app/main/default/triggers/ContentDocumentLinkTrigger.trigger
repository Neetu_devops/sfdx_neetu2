trigger ContentDocumentLinkTrigger on ContentDocumentLink (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    if( LeaseWareUtils.isTriggerDisabled())return;
    TriggerFactoryForStandard.createHandler(ContentDocumentLink.sObjectType);
}