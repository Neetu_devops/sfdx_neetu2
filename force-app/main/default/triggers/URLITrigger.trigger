trigger URLITrigger on Utilization_Report_List_Item__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Utilization_Report_List_Item__c.sObjectType);    
}