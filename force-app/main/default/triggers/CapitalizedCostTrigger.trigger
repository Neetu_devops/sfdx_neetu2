trigger CapitalizedCostTrigger on Capitalized_Cost__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Capitalized_Cost__c.sObjectType);


}