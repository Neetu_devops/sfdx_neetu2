trigger PurchaseAgreementMilestoneTrigger on Purchase_Agreement_Milestone__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  if( LeaseWareUtils.isTriggerDisabled() )return;
  TriggerFactory.createHandler(Purchase_Agreement_Milestone__c.sObjectType);          
}