trigger CountePartyNewTrigger on Bank__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Bank__c.sObjectType);    
    
}