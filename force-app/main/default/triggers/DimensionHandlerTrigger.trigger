trigger DimensionHandlerTrigger on Dimension__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if( LeaseWareUtils.isTriggerDisabled() )return;
		TriggerFactory.createHandler(Dimension__c.sObjectType);
	
}