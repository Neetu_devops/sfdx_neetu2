trigger AttendeeContactsTrigger on Attendee_Contacts__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Attendee_Contacts__c.sObjectType);
    
    
}