trigger Notify_NotificationSubscriptionTrigger on Notifications_Subscription__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) { 
   
    Notify_Utility.triggerHandler(Notifications_Subscription__c.sObjectType);
    system.debug('Notify Trigger...');
}