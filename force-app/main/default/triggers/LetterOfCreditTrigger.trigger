trigger LetterOfCreditTrigger on Letter_Of_Credit__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Letter_Of_Credit__c.sObjectType);    
    
}