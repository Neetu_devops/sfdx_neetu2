trigger AircraftTypeGetGlobalFleetInfo on Aircraft_Type__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	if(LeaseWareUtils.isTriggerDisabled())return;
	TriggerFactory.createHandler(Aircraft_Type__c.sObjectType);
	
}

