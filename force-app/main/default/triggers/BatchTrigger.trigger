trigger BatchTrigger on Batch__c (after update) {
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactory.createHandler(Batch__c.sObjectType);
}