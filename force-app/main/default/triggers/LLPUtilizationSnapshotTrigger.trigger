trigger LLPUtilizationSnapshotTrigger on LLP_Utilization_Snapshot__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(LLP_Utilization_Snapshot__c.sObjectType); 
}