trigger AssemblyTrigger on Constituent_Assembly__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(Constituent_Assembly__c.sObjectType);       
    
}