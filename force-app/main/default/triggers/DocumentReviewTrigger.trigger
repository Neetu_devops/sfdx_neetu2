trigger DocumentReviewTrigger on AC_Document_Review__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
	if( LeaseWareUtils.isTriggerDisabled() )return;
	TriggerFactory.createHandler(AC_Document_Review__c.sObjectType);    
    
}