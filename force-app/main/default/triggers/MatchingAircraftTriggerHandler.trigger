trigger MatchingAircraftTriggerHandler on Matching_Aircraft__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	if(LeaseWareUtils.isTriggerDisabled())return;
	TriggerFactory.createHandler(Matching_Aircraft__c.sObjectType);

}