trigger Tasktrigger on Task(before delete,after delete){
    if( LeaseWareUtils.isTriggerDisabled() )return;
    TriggerFactoryForStandard.createHandler(Task.sObjectType);    
}