({
    doInit: function (component, event, helper) {
        var action = component.get("c.getImageOptions");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                component.set("v.imageSizeOptions", response.getReturnValue());
                var imageSizeOptions = component.get("v.imageSizeOptions");
                var imageOptionList = [];
                var optName;
                for (var i = 0; i < imageSizeOptions.length; i++) {
                    if (imageSizeOptions[i].Option_Name__c == 'Custom') {
                        optName = imageSizeOptions[i].Option_Name__c + ' (W x H)';
                    }
                    else {
                        optName = imageSizeOptions[i].Option_Name__c + ' (' + imageSizeOptions[i].Width__c + ' x ' + imageSizeOptions[i].Height__c + ')';
                    }
                    imageOptionList.push({
                        'label': optName,
                        'value': imageSizeOptions[i].Option_Name__c
                    });
                }
            }
            component.set("v.options", imageOptionList);
            component.set("v.imageSize", 'Default');
        });
        $A.enqueueAction(action);
    },

    fileLoadJS: function (component, event, helper) {
        if (event.getSource().get("v.files")) {
            var imageSizeOptions = component.get("v.imageSizeOptions");
            let contId = component.get('v.storeImageURLTo');
            var canvas = $("#imageCanvas"+contId)[0];
            var file = event.getSource().get("v.files")[0];
            var reader = new FileReader();
            var imageFileName = component.find("imageFile").get("v.value");

            imageFileName = imageFileName.replace(/.*[\/\\]/, '');
            component.set("v.imageDocName", imageFileName);

            component.find("sizeImg").set("v.disabled", false);
            component.set("v.cropCoords", undefined);
            component.set("v.imageSize", 'Default');
            component.set("v.aspectRatio", false);

            $A.util.removeClass(component.find("updateButton"), "slds-hide");
            $A.util.removeClass(component.find("nameImg"), "slds-hide");
            $A.util.addClass(component.find("customSize"), "slds-hide");

            reader.onload = function (event) {
                var img = new Image();
                img.onload = function () {
                    component.set("v.imageFile", img);
                    component.set("v.defaultWidth", img.width);
                    component.set("v.defaultHeight", img.height);

                    component.set("v.imageHeight", img.width);
                    component.set("v.imageWidth", img.height);

                    var jcrop_api = component.get("v.jcrop_api");

                    if (jcrop_api != undefined && jcrop_api != null) {
                        jcrop_api.destroy();
                    }
                    
                    $("#container"+contId).empty();
                    $("#container"+contId).append("<canvas id=\"imageCanvas"+contId+"\">");
                    $("#container"+contId).addClass('slds-m-top_medium');
                    canvas = $("#imageCanvas"+contId)[0];

                    var ctx = canvas.getContext('2d');
                    for (var i = 0; i < imageSizeOptions.length; i++) {
                        if (imageSizeOptions[i].Option_Name__c == 'Default') {
                            canvas.width = imageSizeOptions[i].Width__c;
                            canvas.height = imageSizeOptions[i].Height__c;

                            break;
                        }
                    }
                    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

                    component.validateImage(component);
                };
                img.src = event.target.result;
            }
            reader.readAsDataURL(file);
        }
    },

    validateImage: function (component) {
        let contId = component.get('v.storeImageURLTo');
        var canvas = $("#imageCanvas"+contId)[0];
        var img = new Image();
        img.onload = function () {
            var jcrop_api = component.get("v.jcrop_api");

            if (jcrop_api != undefined && jcrop_api != null) {
                jcrop_api.destroy();
            }
            
            $("#container"+contId).empty();
            $("#container"+contId).append("<canvas id=\"imageCanvas"+contId+"\">");
            canvas = $("#imageCanvas"+contId)[0];

            var ctx = canvas.getContext('2d');
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

            $('#imageCanvas'+contId).Jcrop({
                onSelect: function (coords) {
                    component.set("v.cropCoords", coords);
                },
                onRelease: function () {
                    var prefsize = component.get("v.cropCoords");
                    prefsize = {
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 0,
                        w: canvas.width,
                        h: canvas.height,
                    };
                    component.set("v.cropCoords", prefsize);
                },
                boxWidth: canvas.width,
                boxHeight: canvas.height
            }, function () {
                jcrop_api = this;
                component.set("v.jcrop_api", jcrop_api);
            });
        };
        img.src = canvas.toDataURL('image/png');
    },

    handleCrop: function (component, event, helper) {
        
        let urlId = component.get('v.storeImageURLTo');
        var canvas = $("#imageCanvas"+urlId)[0];
        var img;
        img = new Image();
        img.src = canvas.toDataURL('image/png');

        var ctx = canvas.getContext('2d');
        var prefsize = component.get("v.cropCoords");
        component.find("sizeImg").set("v.disabled", true);
        $A.util.addClass(component.find("customSize"), "slds-hide");

        canvas.width = prefsize.w;
        canvas.height = prefsize.h;

        console.log("prefsizex:", prefsize.w);
        console.log("prefsizex:", prefsize.h);
        console.log("prefsizex:", prefsize.y);
        console.log("prefsizex:", prefsize.x);

        ctx.drawImage(img, prefsize.x, prefsize.y, prefsize.w, prefsize.h, 0, 0, canvas.width, canvas.height);

        component.validateImage(component);
    },

    
    handleRotate: function (component, event, helper) {
        console.log('handleRotate ');
        let urlId = component.get('v.storeImageURLTo');
        var canvas = $("#imageCanvas"+urlId)[0];
        if(!canvas) { return;}
        var ctx = canvas.getContext('2d');
        var image;
        image = new Image();
        image.src = canvas.toDataURL('image/png');
		
        var surfaceContext = canvas.getContext('2d');  
   	    surfaceContext.fillStyle = "#ffffff";  
        
        canvas.width = image.height;
        canvas.height = image.width;
        
        surfaceContext.clearRect(0,0,canvas.width,canvas.height);
        surfaceContext.save();
        surfaceContext.translate(canvas.width/2,canvas.height/2);
        surfaceContext.rotate(90*Math.PI/180);
        surfaceContext.drawImage(image,-image.width/2,-image.height/2);
        surfaceContext.restore();
        
        component.validateImage(component);
    },

    updateImageName: function (component, event, helper) {
        $A.util.removeClass(component.find("updateName"), "slds-hide");
        $A.util.addClass(component.find("imageName"), "slds-hide");
        $A.util.addClass(component.find("updateButton"), "slds-hide");
    },

    updateImageSize: function (component, event, helper) {
        var imageSize = component.get("v.imageSize");
        var customSize = component.find("customSize");
        let urlId = component.get('v.storeImageURLTo');
        var canvas = $("#imageCanvas"+urlId)[0];
        var ctx = canvas.getContext('2d');
        var img = component.get("v.imageFile");

        if (imageSize == 'Custom') {
            $A.util.removeClass(customSize, "slds-hide");

            canvas.width = component.get("v.imageWidth");
            canvas.height = component.get("v.imageHeight");
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

            component.validateImage(component);
        }
        else {
            $A.util.addClass(customSize, "slds-hide");

            var imageSizeOptions = component.get("v.imageSizeOptions");

            for (var i = 0; i < imageSizeOptions.length; i++) {
                if (imageSizeOptions[i].Option_Name__c == imageSize) {
                    canvas.width = imageSizeOptions[i].Width__c;
                    canvas.height = imageSizeOptions[i].Height__c
                    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

                    component.validateImage(component);
                    break;
                }
            }
        }
    },

    calculateWidth: function (component, event, helper) {
        if (component.get("v.aspectRatio")) {
            var imgWidth = component.get("v.defaultWidth");
            var imgHeight = component.get("v.defaultHeight");

            var calculatedHeight = component.get("v.imageWidth") * (imgHeight / imgWidth);
            component.set("v.imageHeight", calculatedHeight);
        }

    },

    calculateHeight: function (component, event, helper) {
        if (component.get("v.aspectRatio")) {
            var imgWidth = component.get("v.defaultWidth");
            var imgHeight = component.get("v.defaultHeight");
            var calculatedWidth = component.get("v.imageHeight") * (imgWidth / imgHeight);

            component.set("v.imageWidth", calculatedWidth);
        }
    },

    setCustomSize: function (component, event, helper) {
        let urlId = component.get('v.storeImageURLTo');
        var canvas = $("#imageCanvas"+urlId)[0];
        var ctx = canvas.getContext('2d');
        var img = component.get("v.imageFile");

        canvas.width = component.get("v.imageWidth");
        canvas.height = component.get("v.imageHeight");
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

        component.validateImage(component);
    },

    saveImage: function (component, event, helper) {
        var recordId = component.get("v.recordId");
        let urlId = component.get('v.storeImageURLTo');
        var canvas = $("#imageCanvas"+urlId)[0];
        var image = canvas.toDataURL().split(',')[1];

        component.set("v.showModal", true);

        var action = component.get("c.saveChosenImage");
        action.setParams({
            'selectedImage': image,
            'imageName': component.get("v.imageDocName"),
            'recordId': recordId,
            'generatePublicLink': component.get("v.generatePublicUrl"),
            'sObjectName': component.get("v.sObjectName"),
            'storeImageURLTo': component.get("v.storeImageURLTo")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": response.getReturnValue(),
                    "type": "success"
                });
                toastEvent.fire();
                $A.util.addClass(component.find("nameImg"), "slds-hide");
                $A.util.addClass(component.find("customSize"), "slds-hide");
                component.find("sizeImg").set("v.disabled", true);
                component.set("v.imageSize", 'Default');
                component.set("v.imageFile", undefined);
                component.set("v.cropCoords", undefined);

                component.set("v.fileUploaded", 100);
                component.set("v.showModal", false);
                let contId = component.get('v.storeImageURLTo');
                $("#container"+contId).empty();
                $("#container"+contId).append("<canvas id=\"imageCanvas"+contId+"\" width=\"0\" height=\"0\">");
            	$A.get('e.force:refreshView').fire();
            }
            else if (state === 'ERROR') {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})