({
    doInit: function (component, event, helper) {
        component.set("v.isLoading", true);
        component.set("v.loadedInPhone", helper.isCmpLoadonPhone());
        helper.getDealTypeForRecord(component);

        var heightInp = component.get('v.dropdownHeight');
        console.log('doInit heightInp: ' + heightInp);
        if ($A.util.isEmpty(heightInp) || $A.util.isUndefined(heightInp) || heightInp == 0 || heightInp == null)
            component.set("v.overrideHeight", false);
        else
            component.set("v.overrideHeight", true);
        
        component.set("v.dropdownHeight" , heightInp);
        
    },
    
    closeModal: function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    },
    
    hideSpinner : function(component, event, helper) {
        var loaderCount = component.get("v.loaderCount");
        var data = component.get("v.assetInDealData");
        if (data != undefined || data != null) {
            //console.log("hideSpinner data.length "+data.length + ' loaderCount ' + loaderCount); 
            if (loaderCount < (data.length - 1)) {
                loaderCount++;
                component.set("v.loaderCount", loaderCount);
            } else {
                helper.updateNameColumnWidth(component);
                component.set("v.isLoading", false);
            }
        }
        else {
            component.set("v.isLoading", false);
        }


        console.log("hideSpinner");
    },

    onEdit: function (component, event, helper) {
        var recordIdL = event.currentTarget.id;
        console.log('onEdit recordId ' + recordIdL);
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": recordIdL
        });
        editRecordEvent.fire();

        /* component.set("v.selectedRecordId", recordIdL);
         component.set("v.editRecord" , true); */
    },
    
    onDelete: function (component, event, helper) {
        var recordIdL = event.currentTarget.id;
        console.log('onDelete recordId ' + recordIdL);
        component.set("v.deleteRecordId", recordIdL);
        component.set("v.showConfirmDialog ", true);
    },

    handleConfirmDialogYes: function (component, event, helper) {
        console.log('Yes');
        component.set('v.showConfirmDialog', false);
        component.set("v.isLoading", true);
        helper.deleteAID(component);
    },

    handleConfirmDialogNo: function (component, event, helper) {
        console.log('No');
        component.set("v.deleteRecordId", null);
        component.set('v.showConfirmDialog', false);
    },

    handleCopyDialog: function (component, event, helper) {
        console.log('handleCopyDialog');
        component.set('v.showCopyDialog', false);
    },

    createAssetInDeal: function (component, event, helper) {
        console.log('createAID id ' + component.get("v.recordId"));
        /*var createEvent = $A.get("e.force:createRecord");
        createEvent.setParams({
            "entityApiName": "Aircraft_Proposal__c",
            "defaultFieldValues": {
                'Marketing_Activity__c' : component.get("v.recordId")
            },
            "navigationLocation": "RELATED_LIST"
        });
        createEvent.fire();
        
        
        component.set("v.createNewRecord", true); */
    },

    onAllChange: function (component, event, helper) {
        var isAll = component.get("v.isAll");
        console.log('onAllChange isAll: ' + isAll);
        if ($A.util.isEmpty(isAll) || $A.util.isUndefined(isAll))
            return;

        var AIDData = component.get("v.assetInDealData");
        console.log("onAllChange values " + AIDData);
        if ($A.util.isEmpty(AIDData) || $A.util.isUndefined(AIDData) || AIDData == null)
            return;

        //console.log("onAllChange AIDData " +JSON.stringify(AIDData));
        for (var i = 0; i < AIDData.length; i++) {
            AIDData[i].isSelect = isAll;
        }
        component.set("v.assetInDealData", AIDData);
    },

    onCheckboxChange: function (component, event, helper) {
        var AIDData = component.get("v.assetInDealData");
        console.log("onAllChange values " + AIDData);
        if ($A.util.isEmpty(AIDData) || $A.util.isUndefined(AIDData) || AIDData == null)
            return;

        //console.log("onAllChange AIDData " +JSON.stringify(AIDData));
        var isAllSelected = true;
        for (var i = 0; i < AIDData.length; i++) {
            if (AIDData[i].isSelect == false)
                isAllSelected = false;
        }
        component.set("v.isAll", isAllSelected);
    },

    onCopyToSelected: function (component, event, helper) {
        console.log('onCopyToSelected ');
        var AIDData = component.get("v.assetInDealData");
        console.log("onCopyToSelected values " + AIDData);
        if ($A.util.isEmpty(AIDData) || $A.util.isUndefined(AIDData) || AIDData == null)
            return;

        //console.log("onAllChange AIDData " +JSON.stringify(AIDData));
        var isSelected = false;
        for (var i = 0; i < AIDData.length; i++) {
            if (AIDData[i].isSelect == true)
                isSelected = true;
        }
        if (isSelected == false) {
            component.set("v.showCopyDialog", true);
            return;
        }

        var commonData = component.get("v.selectedFields");
        console.log("onCopyToSelected values " , commonData);
        if ($A.util.isEmpty(commonData) || $A.util.isUndefined(commonData) || commonData == null)
            return;

        var aidData = component.get("v.assetInDealData");
        console.log("onAllChange values " + aidData);
        if ($A.util.isEmpty(aidData) || $A.util.isUndefined(aidData) || aidData == null)
            return;

        //console.log("onCopyToSelected aidData " +JSON.stringify(aidData));
        for (var i = 0; i < aidData.length; i++) {
            if (aidData[i].isSelect == true) {
                for (var j = 0; j < aidData[i].fields.length; j++) {
                    var apiName = aidData[i].fields[j].apiName;
                    for (var k = 0; k < commonData.length; k++) {
                        if (apiName.localeCompare("Name") != 0 && commonData[k].value !== null &&
                            commonData[k].value !== '' &&
                            commonData[k].apiName == aidData[i].fields[j].apiName) {
                            aidData[i].fields[j].value = commonData[k].value;
                        }
                    }
                }
            }
        }
        component.set("v.assetInDealData", aidData);
        //console.log("onCopyToSelected aidData " +JSON.stringify(aidData));
    },

    onSave: function (component, event, helper) {
        console.log('onSave');
        helper.updateAIDs(component);
    },

    reloadData: function (component, event, helper) {
        console.log('reloadData');
        helper.refreshData(component);
    },

    showRecord: function (component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log('showRecord recordId ' + recordId);
        window.open('/' + recordId);
    },

    refreshAll: function (component, event, helper) {
        var eventType = event.getParam('type').toUpperCase();
        var eventMsg = event.getParam("message").toUpperCase();
        var indexCall = 0; //making sure function not firing too many times
        console.log("refreshAll " + eventType + " eventMsg " + eventMsg);
        if (eventType == 'SUCCESS') {
            if (indexCall == 0) {
                indexCall += 1;
                var msgData = event.getParam('messageTemplateData');
                console.log("refreshAll msgData " + msgData);
                if (msgData != undefined && msgData != null && msgData.length > 0) {
                    var msgObjData = msgData[0].toUpperCase();
                    console.log("refreshAll msgData " + msgObjData);
                    if (eventMsg && msgObjData && msgObjData.includes("ASSET IN DEAL") &&
                        (eventMsg.includes("WAS SAVED") || eventMsg.includes("WAS CREATED"))
                    ) {
                        component.set("v.isLoading", true);
                        helper.refreshData(component);
                    }
                }
            }
        }
    },

})