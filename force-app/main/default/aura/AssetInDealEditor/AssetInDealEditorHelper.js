({
    getDealTypeForRecord: function (component) {
        var recordIdL = component.get("v.recordId");
        console.log('getDealTypeForRecord recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getAIDRecordType');
        action.setParams({
            parentId: recordIdL
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getDealTypeForRecord : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getDealTypeForRecord values " + allValues);
                if (allValues != null) {
                    component.set("v.dealType", allValues);
                    var fs = component.get('v.fieldSet');
                    if ($A.util.isEmpty(fs) || $A.util.isUndefined(fs) || fs == '' || fs == null)
                        self.getFieldsetName(component);
                    else
                        self.getFields(component);
                }
                else {
                    component.set("v.isLoading", false);
                    self.showErrorMsg(component, "Deal type not found!");
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getDealTypeForRecord errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getDealTypeForRecord unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getFieldsetName: function (component) {
        var recordIdL = component.get("v.recordId");
        console.log('getFieldsetName recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getFieldSetBasedOnType');
        action.setParams({
            recordId: recordIdL
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getFieldsetName : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getFieldsetName values " + allValues);
                if (allValues != null) {
                    component.set("v.fieldSetBasedOnType", allValues);
                    console.log("getFields fieldSetBasedOnType " + component.get("v.fieldSetBasedOnType"));

                    self.getFields(component);
                }
                else {
                    component.set("v.isLoading", false);
                    self.showErrorMsg(component, "Incorrect configuration: cannot find field set");
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getFieldsetName errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getFieldsetName unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getFields: function (component) {
        var self = this;
        var action = component.get('c.getColumns');
        action.setParams({
            recordId: component.get('v.recordId'),
            fieldSetName: component.get('v.fieldSet'),
            additionalField: component.get("v.additionalField")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getFields : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getFields values " + allValues);
                if (allValues != null) {
                    var resObj = JSON.parse(allValues);
                    console.log("getFields values " ,resObj);
                    component.set("v.selectedFields", resObj);

                    console.log("getFields allValues.length " + resObj.length);

                    self.getAIDs(component);
                }
                else {
                    component.set("v.isLoading", false);
                    var fieldSetDefault = component.get("v.fieldSetBasedOnType");
                    console.log("getFields fieldSetBasedOnType " + fieldSetDefault);
                    self.showErrorMsg(component, "Incorrect configuration: cannot find field set " + fieldSetDefault);
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getFields errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getFields Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getAIDs: function (component) {
        var recordIdL = component.get("v.recordId");
        console.log('getAIDs recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getAssetInDeal');
        action.setParams({
            recordId: recordIdL,
            fieldSetName: component.get('v.fieldSet'),
            additionalField: component.get("v.additionalField")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getAIDs : state " + state);
            //component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != null) {
                    var resObj = JSON.parse(allValues);
                    //console.log("getAIDs values " +JSON.stringify(resObj));
                    component.set("v.assetInDealData", resObj);
                    if (allValues.length > 0) {
                        component.set("v.aidDummyId", resObj[0].aidId);
                    }
                    else {
                        component.set("v.isLoading", false);
                    }
                    console.log('getAIDs aidDummyId: ' + component.get("v.aidDummyId"));
                }
                else {
                    component.set("v.assetInDealData", null);
                    component.set("v.aidDummyId", null);
                    component.set("v.isLoading", false);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.isLoading", false);
                console.log("getAIDs errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getAIDs Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            self.resetData(component);
        });
        $A.enqueueAction(action);
    },
  
    isCmpLoadonPhone: function() {
        let deviceType = $A.get("$Browser.formFactor");
        let isTablet = $A.get("$Browser.isTablet");
        console.log('deviceType : '+deviceType+'isTablet : '+isTablet);
        if(deviceType === 'PHONE' || isTablet){
            return true;
        }
        return false;
    },
    
    deleteAID: function (component) {
        var recordIdL = component.get("v.deleteRecordId");
        console.log('deleteAID recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            return;
        }
        var action = component.get('c.deleteAssetInDeal');
        action.setParams({
            recordId: recordIdL
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("deleteAID : state " + state);
            component.set("v.isLoading", false);
            component.set("v.deleteRecordId", null);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("deleteAID values " + allValues);
                component.set("v.isLoading", true);
                self.getAIDs(component);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("deleteAID errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("deleteAID Unknown error");
                    self.showErrorMsg(component, "Error in deleting record");
                }
            }
        });
        $A.enqueueAction(action);
    },

    updateAIDs: function (component) {
        var assetInDealData = component.get("v.assetInDealData");
        console.log('updateAIDs assetInDealData: ' + JSON.stringify(assetInDealData));
        var self = this;
        if ($A.util.isEmpty(assetInDealData) || $A.util.isUndefined(assetInDealData))
            return;
        component.set("v.isLoading", true);
        var assetInDealObjList = [];
        for (var i = 0; i < assetInDealData.length; i++) {
            var fields = assetInDealData[i].fields;
            var assetInDealObj = {};
            assetInDealObj.Id = assetInDealData[i].aidId;
            if (!$A.util.isEmpty(fields) && !$A.util.isUndefined(fields)) {
                for (var j = 0; j < fields.length; j++) {
                    assetInDealObj[fields[j].apiName] = fields[j].value;
                }
                assetInDealObjList.push(assetInDealObj);
            }
        }
        console.log('updateAIDs assetInDealObjList: ' + JSON.stringify(assetInDealObjList));

        var action = component.get('c.updateAssetInDeal');
        action.setParams({
            assetInDealList: assetInDealObjList
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("updateAIDs : state " + state);
            component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                component.set("v.isLoading", true);
                self.getFields(component);
                self.showSuccessMsg(component, 'Saved Successfully!');
                $A.get("e.force:closeQuickAction").fire();
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("updateAIDs errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("updateAIDs Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            component.set("v.isAll", false);
        });
        $A.enqueueAction(action);
    },
    
    updateNameColumnWidth: function(component) {
        var elements = document.getElementsByClassName("nameColumnClass");
        var highestWidth = 0;
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var totalWidth = elements[i].offsetWidth;
                console.log('updateColumnWidth totalWidth Name: '+totalWidth);
                if (highestWidth < totalWidth) {
                    highestWidth = totalWidth;
                }
            }
        }
        component.set("v.nameColumnWidth", highestWidth);
        console.log('updateColumnWidth highestWidth Name: '+highestWidth);
        let ele = document.querySelector('.nameColumnClass');
        console.log('ele '+ele);
        if(ele) {
            document.querySelectorAll('.nameColumnClass').forEach((element, index) => {
                element.classList.add('updateWidth');
            }); 
        }
        
        this.updateColumnWidth(component);
    },
    
    updateColumnWidth: function(component){
        var elements = document.getElementsByClassName("parentDiv");
        var highestWidth = 0;
        var lowestWidth = 0;
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var totalWidth = elements[i].offsetWidth;
                if (i == 0) {
                    lowestWidth = totalWidth;
                }
                if (highestWidth < totalWidth) {
                    highestWidth = totalWidth;
                }
                if (lowestWidth > totalWidth) {
                    lowestWidth = totalWidth;
                }
            }
            console.log("getFields highestWidth: " + highestWidth + " lowestWidth: " + lowestWidth);
            var cmpWidth = lowestWidth;
            if (component.get("v.isEmbedded") == false) {
                cmpWidth = highestWidth;
            }
            //LW-AKG: Added to handle the width of the table when loaded from Bid Matrix.
            let loadedFromBidMatrix = component.get("v.loadedFromBidMatrix");
            if (!loadedFromBidMatrix) {
                if (component.get("v.showActionIcon") == true) {
                    cmpWidth = cmpWidth - 80;
                }
                else {
                    cmpWidth = cmpWidth - 150;
                }
                cmpWidth = cmpWidth - 150;
            }
            //As Name field is with fixed width we are reducing it from total width
            let nameWidth = component.get("v.nameColumnWidth");
            cmpWidth = cmpWidth - nameWidth;
            var tdWidth = 0;
            let resObj = component.get("v.selectedFields");
            if(!resObj) { return;}
            if(resObj.length > 0) {
                tdWidth = cmpWidth / (resObj.length - 1);
            }
            console.log("getFields tdWidth" + tdWidth);
            var defaultWidth = component.get("v.divWidth");
            console.log("getFields tdWidth extra " + tdWidth + " defaultWidth: " + defaultWidth);
            if (tdWidth > defaultWidth) {
                component.set("v.divWidth", Math.round(tdWidth));
            }
            console.log("final td width: " + component.get("v.divWidth"));
        }
    },

    refreshData: function (component) {
        component.set("v.isAll", false);
        component.set("v.isLoading", true);
        component.set("v.deleteRecordId", null);
        component.set("v.loaderCount", 0);
        console.log('reloadData');
        this.getDealTypeForRecord(component);
    },

    resetData: function (component) {
        component.set("v.deleteRecordId", null);
        component.set("v.showConfirmDialog ", false);
        component.set('v.showCopyDialog', false);
        component.set("v.isAll", false);
    },

    showSuccessMsg: function (component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: msg,
            duration: '5000',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    showErrorMsg: function (component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration: '5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    //to handle errors
    handleErrors: function (errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error",
            type: "error"
        };
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    console.log('error ' + toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        console.log('error ' + toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            console.log('error ' + toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
    }
    
})