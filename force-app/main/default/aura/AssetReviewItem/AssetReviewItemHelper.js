({
    createChildComp : function(cmp, itemId, name, desc) {
        var self = this;
        var container = cmp.find("childContainer");
        var object = cmp.get("v.childObject");
        var cond = cmp.get("v.childCond");
        var fieldSet = cmp.get("v.childFieldSet");
        var parentStatusCond = cmp.get("v.parentStatusCond");
        $A.createComponent("c:AssetReviewItemChild",
                           {
                               "recordId" : itemId,
                               "recordName" : name,
                               "recordDesc" : desc,
                               "childObject": object,
                               "childCond" : cond, 
                               "childFieldSet" : fieldSet,
                               "parentStatusCond" : parentStatusCond,
                               "parentObject" : cmp.get("v.parentObject"),
                               "sectionIndex" : cmp.get("v.sectionIndex"),
                               "childIndex" : cmp.get("v.childIndex"),
                               "parentRecordFieldset" : cmp.get("v.parentRecordFieldset")
                           },
                           function(cmp) {
                               container.set("v.body", [cmp]);
                           });
        
    }
    
})