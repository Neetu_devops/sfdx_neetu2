({
    doInit: function (component, event, helper) {
     
      helper.hideSpinner(component, event, helper);
         console.log('Inside Init');
    },
    
     updateSnapshotComment: function (component, event, helper) {
        var commentsValue = component.get("v.comments");
        console.log('commentsValue::',commentsValue);
        
        if(commentsValue === undefined || !commentsValue.trim() || commentsValue === null){
            var inputField = component.find("snapshotComment");
            inputField.reportValidity();
        }
        else{
     	 helper.showSpinner(component, event, helper);
         var recordId =  component.get("v.recordId");
         var cmt =  component.get("v.comments");
      
        var updateSnapshotAction = component.get("c.updateSnapshotCommentFunc");
        
        
            updateSnapshotAction.setParams({
                "recordId": recordId,
                "comments": cmt
                
            });
         	
            updateSnapshotAction.setCallback(this, function (response) {
                helper.hideSpinner(component, event, helper);
                var state = response.getState();
                console.log(state);
               
                if (state === "SUCCESS") {
                    console.log('Success');
					$A.get('e.force:refreshView').fire();
                     var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "Title": "Snapshot Comment Updated",
                        "type": "success",
                        "message": "Success: Snapshot Comment updated successfully"
                    });
                    
                    // Update the UI: close panel, show toast, refresh  page
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire(); 
					
                } else{
                    var errors = response.getError();
                    helper.handleErrors(component,errors);
                }
            });
            
            // Send the request to create the new contact
            $A.enqueueAction(updateSnapshotAction); 
        }
       
        
    },
    
    handleCancel: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
     
})