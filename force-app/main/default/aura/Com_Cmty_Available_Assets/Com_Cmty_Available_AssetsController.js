({
    doInit: function(cmp, event, helper) {
        helper.setColumns(cmp);		
        window.setTimeout(
            $A.getCallback(function() {
                helper.loadData(cmp, event, helper);
            }), 200
        );
    },
})