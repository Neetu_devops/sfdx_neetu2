({
    setColumns: function (cmp) {

        var aircraftColumns = [
            { label: 'MSN', fieldName: 'msnURL', type: 'url', typeAttributes: { target: '_blank', label: { fieldName: 'msn' } }, sortable: true },
            { label: 'Aircraft Type', fieldName: 'aircraftType', sortable: true },
            //{ label: 'Asset Variant', fieldName: 'assetVariant', sortable: true },
            { label: 'Vintage', fieldName: 'vintage', sortable: true },
            { label: 'Registration', fieldName: 'registration', sortable: true },
            { label: 'Engine Type', fieldName: 'aircraftEngineType', sortable: true },
            { label: 'ESN #1', fieldName: 'esn1', sortable: true },
            { label: 'ESN #2', fieldName: 'esn2', sortable: true },
            { label: 'ESN #3', fieldName: 'esn3', sortable: true },
            { label: 'ESN #4', fieldName: 'esn4', sortable: true },
        ];

        cmp.set('v.aircraftColumns', aircraftColumns);

        var engineColumns = [
            { label: 'ESN', fieldName: 'esnURL', type: 'url', typeAttributes: { target: '_blank', label: { fieldName: 'esn' } }, sortable: true },
            { label: 'Engine Type', fieldName: 'engineType', sortable: true },
            { label: 'Configuration', fieldName: 'currentThrust', sortable: true },
            {
                label: 'TSN', fieldName: 'tsn', type: 'number',
                typeAttributes: {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                },
                sortable: true
            },
            { label: 'CSN', fieldName: 'csn', type: 'number', sortable: true },
            {
                label: 'TSO', fieldName: 'tso', type: 'number',
                typeAttributes: {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                }
                , sortable: true
            },
            { label: 'CSO', fieldName: 'cso', type: 'number', sortable: true },
            { label: 'Limiter', fieldName: 'limiter', type: 'number', sortable: true },
            { label: 'Limiting Part', fieldName: 'limitingPart', sortable: true },
            { label: 'Current Location', fieldName: 'currentLocation', sortable: true },
        ];

        cmp.set('v.engineColumns', engineColumns);
    },

    loadData: function (cmp, event, helper) {

        //set Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title": "Available Assets" });
        appEvent.fire();

        helper.showSpinner(cmp);

        var action = cmp.get("c.getAvailableAssets");

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                cmp.set("v.data", data);
                console.log(data);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                } else {
                    console.log("Unknown error");
                }
            }
            helper.hideSpinner(cmp);
        });

        $A.enqueueAction(action);
    },

    showSpinner: function (component) {
        var spinner = component.find("spinner");
        console.log('spinner' + spinner);
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (component) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    handleErrors: function (errors) {

        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
    }
})