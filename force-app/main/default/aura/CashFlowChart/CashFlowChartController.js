({
    doInit:function(cmp,event,helper){
        var action = cmp.get("c.isForecastGenerated");
        action.setParams({
            "recordId": cmp.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.isForecastGenerated",response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    scriptLoaded :function(cmp,event,helper){
        cmp.set("v.isScriptLoaded",true);
        var action = cmp.get("c.checkLessor");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.isLessor",response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    navigateToMyComponent : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:ForcastDashboard",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        evt.fire();
    }
})