({
	doInit : function(component, event, helper) {
		component.set("v.LLPRepl", "0.5");
		//helper.getRatioSteplist(component);
        helper.getLLPVisibility(component);
       
        var sObj = component.get("v.sobjecttype");
        console.log('doinit sObj: '+sObj + ' RoundOption '+component.get("v.RoundOption"));
    }, 
    
    onResetClick : function(component, event, helper) {
		component.set("v.isFilterData", false);
        component.set("v.filterData", "");
        
		component.set("v.LLPRepl", "0.5");
        component.set("v.showOutput", false);
        helper.getObjectAircraftType(component);
       // helper.getEngineRunPickList(component);
        helper.getRatioSteplist(component);
        helper.getOpEnvlist(component);
        
    }, 
    
    onRoundOffChange : function(component, event, helper) {
        var isRoundOff = event.getSource().get("v.value");
        console.log("onRoundOffChange "+isRoundOff);
        if(isRoundOff == true) {
            helper.roundOff(component);
        }
        else
            helper.validateInput(component, event, helper);
        
        
    },
    
    onMoreLessClick : function(component, event, helper) {
        console.log('onMoreLessClick');
        var elements = document.getElementsByClassName("moreless");
        console.log('onMoreLessClick style: ' +elements[0].style.display);
        var sty = elements[0].style.display;
        var moreLessLb = document.getElementById("moreLabel");
        var dataVal = moreLessLb.getAttribute("data-content");
        var newData = "";
        if(sty == 'none'){
            elements[0].style.display = 'block';
            newData = "Less";
        }
        else if(sty == 'block'){
            elements[0].style.display = 'none';
            newData = "More";
        }
        else{
            elements[0].style.display = 'none';
            newData = "More";
        }
        moreLessLb.setAttribute("data-content",newData);
        console.log('onMoreLessClick sty: ' +sty);
    },
    
    onAircraftTypeChange: function(component, event, helper) {
        component.set("v.isFilterData", false);
        var aircraftType = event.getSource().get("v.value");
        
        var LLPVisiblity = component.get("v.LLPVisiblity");
        if(LLPVisiblity == true)
        	component.find('engineTempId').set("v.value", "");
        
        console.log('onAircraftTypeChange: aircraftType ' +aircraftType);
        
        component.set("v.showOutput", false);
        component.set("v.filterData", "");
        helper.getDefaultMP(component, aircraftType);
        helper.getNumEnglist(component, aircraftType);
    }, 
    
    onETChange: function(component, event, helper) {
        component.set("v.isFilterData", false);
        var etId = component.find('engineTempId').get("v.value");
        console.log('onETChange: etId ' +etId);
        component.set("v.showOutput", false);
        helper.getETCatalogYear(component, etId);
    }, 
    
    onEMChange: function(component, event, helper) {
        component.set("v.isFilterData", false);
        var etId = component.find('engineModelId').get("v.value");
        console.log('onEMChange: etId ' +etId);
        component.set("v.showOutput", false);
        helper.getEngineTemplateForEM(component, etId);
        
        var mpId = component.find('maintProgId').get("v.value");
        helper.getEngineRunPickList(component);
    }, 
    
    onMPChange: function(component, event, helper) {
        component.set("v.isFilterData", false);
        var mpId = component.find('maintProgId').get("v.value");
        console.log('onMPChange: mpId ' +mpId);
        
        component.set("v.showOutput", false);
        
        helper.getCatalogYear(component, mpId);
        helper.getEngineModelPicklist(component, mpId);
        helper.getAPUModelPicklist(component, mpId);
        helper.getLGModelPicklist(component, mpId);
		//helper.getRatioPicklist(component, mpId);
    }, 
    
    onEngineRunChange:function(component, event, helper) {
        console.log('onEngineRunChange');
        component.set("v.isFilterData", false);
        var mpId = component.find('maintProgId').get("v.value");
        console.log('onEngineRunChange mpId :'+mpId);
        var emId = component.find('engineModelId').get("v.value");
        console.log('onEngineRunChange emId :'+emId);
        component.set("v.showOutput", false); 
        //helper.getEngineModelPicklist(component, mpId);
        
        helper.getRatioPicklist(component, mpId, emId);
    },
    
    onGenerateClick: function(component, event, helper) {
        component.set("v.isFilterData", false);
        console.log('onGenerateClick');
        
        helper.validateInput(component, event, helper);
        
    },
    
    showRecord: function(component,event,helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord "+recordId);
        window.open('/' + recordId);  
    },
    
})