({
    
    getLLPVisibility : function(component) {
        console.log('getLLPVisibility');
        var self = this;
        var action = component.get('c.getLLPVisiblity');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getTitle : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getLLPVisibility : response "+allValues);
                if(allValues != null) 
                    component.set('v.LLPVisiblity', allValues); 
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getLLPVisibility errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getLLPVisibility Unknown error");
                    self.showErrorMsg(component, "Error in checking permission");
                }
            }
            self.getFilterData(component);
        });
        $A.enqueueAction(action);
	},
    
    getFilterData : function(component) {
        var recordId = component.get("v.recordId");
        console.log('getFilterData recordId'+recordId);
        var action = component.get("c.getFilter");
        console.log('action of getfilter '+action);
        action.setParams({
            refrenceId: recordId
        });
        action.setCallback(this, function(response) {
            console.log('getFilterData state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getFilterData  result:'+allValues);
                if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues)) {
                    console.log('getFilterData  result:'+JSON.stringify(allValues));
                    component.set("v.LLPRepl", (allValues.LLPBuildup / 100));
                }
                component.set("v.filterData", allValues );
            }
            else{
                var errors = response.getError();
                console.log("getFilterData errors "+JSON.stringify(errors));
            }
            this.getObjectAircraftType(component);
            //this.getEngineRunPickList(component);
            this.getOpEnvlist(component);
            
        });
        $A.enqueueAction(action);
    },
    
    getObjectAircraftType: function(component) {
        var sObj = component.get("v.sobjecttype");
        console.log('getObject '+sObj);
        var recordId = component.get("v.recordId");
        console.log('getObjectAircraftType recordId'+recordId);
        
        var action = component.get("c.getObjectAircraftType");
        console.log('action of getfilter '+action);
        action.setParams({
            objectName: sObj,
            refrenceId: recordId
        });
        action.setCallback(this, function(response) {
            console.log('getObjectAircraftType state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.objectAircraftType" , allValues);
                console.log('getObjectAircraftType  result:'+allValues);
            }
            else{
                var errors = response.getError();
                console.log("getObjectAircraftType errors "+JSON.stringify(errors));
            }
            this.getMPAircraftTypeList(component);
        });
        $A.enqueueAction(action);
    },
    
    getMPAircraftTypeList: function(component) {
        console.log('getMPAircraftTypeList ');
        var action = component.get("c.getMaintenanceProgramAircraftType");
        action.setCallback(this, function(response) {
            console.log('getMPAircraftTypeList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.MPAircraftTypeList" , allValues)
                console.log('getMPAircraftTypeList  result:'+allValues);
                this.getAircraftTypeBasedOnRecordType(component);
            }
        });
        $A.enqueueAction(action);
    },
    
    getAircraftTypeBasedOnRecordType: function(component) {
        console.log('getAircraftTypeBasedOnRecordType ');
        var action = component.get("c.getSectionsRecordType");
        action.setParams({
            objectAPIName : 'Aircraft__c',
            fieldAPIName : 'Aircraft_Type__c',
            recordTypeDeveloperName : 'Aircraft'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getAircraftTypeBasedOnRecordType state: ',response.getState() );
            var allValues = response.getReturnValue();
            //console.log('getAircraftTypeBasedOnRecordType  result:'+allValues);
            component.set("v.aircraftTypeListRecord", allValues);
            this.fetchPicklistValues(component);
        });
        $A.enqueueAction(action);
    },
    
    fetchPicklistValues: function(component, aircraftType) {
        var self = this;
        var action = component.get("c.getDependentMap");
        action.setParams({
            'objDetail' : 'Aircraft__c',
            'contrfieldApiName': 'Aircraft_Type__c',
            'depfieldApiName': 'Engine_Type__c' 
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set("v.dependentEngineFieldMap",StoreResponse);
                // console.log('fetchPicklistValues StoreResponse: ' + JSON.stringify(StoreResponse));
                var listOfkeys = []; 
                var ControllerField = []; 
                
                listOfkeys = component.get("v.aircraftTypeListRecord");
                
                var mpAircraftTypeList = component.get("v.MPAircraftTypeList");
                var objAircraftType = component.get("v.objectAircraftType");
                console.log("fetchPicklistValues objAircraftType: "+objAircraftType + ' mpAircraftTypeList: '+mpAircraftTypeList);
                // console.log("fetchPicklistValues listOfkeys : "+listOfkeys);
                if(!$A.util.isEmpty(mpAircraftTypeList) && !$A.util.isUndefined(mpAircraftTypeList)) {
                    if (listOfkeys != undefined && listOfkeys.length > 0) {
                        var aircraftType;
                        
                        var filterData = component.get("v.filterData");
                        var isFilter = component.get("v.isFilterData");
                        console.log('fetchPicklistValues filterData: '+filterData + ' isFilter: '+isFilter);
                        if(isFilter == true) {
                            if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                                if(!$A.util.isEmpty(filterData.AircraftType) && !$A.util.isUndefined(filterData.AircraftType)) {
                                    objAircraftType = filterData.AircraftType;
                                }
                            }
                        }
                        console.log("After filter fetchPicklistValues ATS "+aircraftType );
                        
                        
                        for (var i = 0; i < listOfkeys.length; i++) {
                            if(mpAircraftTypeList.includes(listOfkeys[i])) {
                                if(objAircraftType != null && objAircraftType.includes(listOfkeys[i])){
                                    ControllerField.push({value: listOfkeys[i], selected: true});
                                    aircraftType = listOfkeys[i];
                                }
                                else
                                    ControllerField.push({value: listOfkeys[i], selected: false});
                                component.find('aircraftTypeId').set("v.value", listOfkeys[i]);
                            }
                        }  
                        console.log('fetchPicklistValues ControllerField '+ControllerField);
                        if(!$A.util.isEmpty(ControllerField) && !$A.util.isUndefined(ControllerField)) {
                            if($A.util.isEmpty(aircraftType) || $A.util.isUndefined(aircraftType)) {
                                aircraftType = ControllerField[0].value;
                            }
                            console.log("fetchPicklistValues ATS "+aircraftType +" ControllerField: "+ControllerField);
                            
                            component.find('aircraftTypeId').set("v.value", aircraftType);
                            var airType = aircraftType;
                            
                            component.set("v.listControllingValues", ControllerField);
                            
                            self.getDefaultMP(component, airType);
                            self.getNumEnglist(component, airType);
                        }
                        else{
                            console.log('fetchPicklistValuesno mp data  : no data ');
                            self.setAircraftTypeNotAvailable(component);
                        }
                    }
                    else{
                        console.log('fetchPicklistValues else : no data ');
                        self.setAircraftTypeNotAvailable(component);
                    }
                }
                else{
                    self.setAircraftTypeNotAvailable(component);
                }
            }else{
                console.log('response.getState() == ERROR');
            }
        });
        $A.enqueueAction(action);
    },
    
    setAircraftTypeNotAvailable : function (component){
        console.log('setAircraftTypeNotAvailable : no data ');
        
        var ControllerField = []; 
        ControllerField.push({value: 'No Data', selected: false});
        component.set("v.listControllingValues", ControllerField);
        
        var dependentFields = [];
        dependentFields.push({"class": "optionClass", label: 'No Data', value: ''});
        
        var LLPVisiblity = component.get("v.LLPVisiblity");
        if(LLPVisiblity == true)
            component.find('engineTempId').set("v.options", dependentFields);
        
        var opts = [];
        opts.push({class: "optionClass",label: "No Data",value: ""});
        component.find('maintProgId').set("v.options", opts);
        
        this.resetAllPickList(component);
    },
    
    getDefaultMP : function(component, aircraftType) {
        console.log('getDefaultMP for :'+aircraftType);
        var filterData = component.get("v.filterData");
        var self = this;
        console.log('getDefaultMP filterData: '+filterData);
        var filterFlag = component.get("v.isFilterData");
        if($A.util.isEmpty(filterData) || $A.util.isUndefined(filterData) || filterFlag == false) {
            var action = component.get("c.getDefaultMP");
            action.setParams({
                "aircraftType": aircraftType
            });
            action.setCallback(this, function(response) {
                console.log('getDefaultMP state: ',response.getState() );
                if (response.getState() == "SUCCESS") {
                    var allValues = response.getReturnValue();
                    console.log('getDefaultMP  result:'+allValues);
                    if(allValues != null){
                        console.log('getDefaultMP  if:');
                        component.set('v.defaultMP', allValues);
                    }
                    else {
                        console.log('getDefaultMP  else:');
                        component.set('v.defaultMP', '');}
                }
                self.getMaintanceProgram(component, aircraftType);
            });
            $A.enqueueAction(action);
        }  
        else{
            self.getMaintanceProgram(component, aircraftType);
        } 
    },
    
    getMaintanceProgram: function(component, aircraftType) {
        console.log('getMaintanceProgram '+aircraftType);
        var action = component.get("c.getMaintanceProgramList");
        action.setParams({
            "aircraftType": aircraftType
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('getMaintanceProgram state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null && allValues.length > 0){
                    console.log('getMaintanceProgram  result:'+JSON.stringify(allValues));
                    
                    var mpData = allValues[0].ObjId;
                    console.log('getMaintanceProgram mpData first item: '+mpData );
                    
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getMaintanceProgram filterData: '+filterData + ' isFilter: '+isFilter);
                    
                    if($A.util.isUndefined(filterData) || $A.util.isEmpty(filterData) ) {
                        //if filterData is null
                        var defaultMP = component.get("v.defaultMP");
                        console.log('defult :'+defaultMP+ ' hhh');
                        //if defaultMP  null
                        if(defaultMP == '' || defaultMP == null || defaultMP.length == 0){
                            //if defaultMP is null fetch first element
                            if(allValues != null && allValues.length > 0){
                                mpData = allValues[0].ObjId;  
                                console.log('defult mp null'+mpData);    
                            }
                            else{
                                console.log('data missing');
                                opts.push({class: "optionClass",label: "No Data",value: ""});  
                            }
                       
                        }
                        else{ 
                            // defaultMP not null
                            mpData = defaultMP
                            console.log('defult mp not null mpData :'+mpData);
                    }
                    console.log('getMaintanceProgram mpData if1: '+mpData );
                }
                    else if(!$A.util.isUndefined(filterData) && !$A.util.isEmpty(filterData) ) {
                    if(!$A.util.isUndefined(filterData.MaintenanceProgram) && !$A.util.isEmpty(filterData.MaintenanceProgram) ) {
                        mpData = filterData.MaintenanceProgram;
                        console.log('getMaintanceProgram mpData else: '+mpData);
                    }
                }
                    
                
                console.log('getMaintanceProgram mpData after: '+mpData);
                var isExists = false;
                for(var i=0;i< response.getReturnValue().length; i++) {
                    console.log('getMaintanceProgram for: '+response.getReturnValue().length );
                    
                    if(mpData == allValues[i].ObjId){
                        isExists = true;
                        opts.push({"class": "optionClass", label: allValues[i].ObjName, value: allValues[i].ObjId, selected: true});
                        console.log('getMaintanceProgram mpData if3: '+opts );
                    }
                    else{
                        opts.push({"class": "optionClass", label: allValues[i].ObjName, value: allValues[i].ObjId});
                        console.log('getMaintanceProgram mpData else3: '+opts );
                    }
                }
                component.find('maintProgId').set("v.options", opts);
                console.log("After filter fetchPicklistValues ATS "+opts );
                
                console.log('getMaintanceProgram mpData final: '+mpData);
                self.getCatalogYear(component, mpData);
                self.getEngineModelPicklist(component, mpData);
                self.getAPUModelPicklist(component, mpData);
                self.getLGModelPicklist(component, mpData);
            }
            else {
                console.log('Maintain Program not available');
                opts.push({class: "optionClass",label: "No Data",value: ""});
                component.find('maintProgId').set("v.options", opts);
                self.resetAllPickList(component);
            }
            
        }
                           });
        $A.enqueueAction(action);
    },
    
    getCatalogYear: function(component, mpId) {
        console.log('getCatalogYear '+mpId);
        var action = component.get("c.getMPCatalogYear");
        action.setParams({
            "MPId": mpId
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getCatalogYear state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getCatalogYear  result:'+allValues);
                if(allValues != null)
                    component.set('v.catalogYear', allValues);
                else
                    component.set('v.catalogYear', 'No Data');
            }
        });
        $A.enqueueAction(action);
    },
    
    getEngineModelPicklist: function(component, mpId) {
        console.log('getEngineModelPicklist '+mpId);
        var action = component.get("c.getEngineModel");
        action.setParams({
            "MPId": mpId
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getEngineModelPicklist state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getEngineModelPicklist  allValues:'+allValues);
                if(allValues != null && allValues.length > 0) {
                    var engineModel = response.getReturnValue()[0];
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getEngineModelPicklist filterData: '+filterData + ' isFilter: '+isFilter);
                    if(isFilter == true) {
                        console.log('getEngineModelPicklist  if isfilter:');
                        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                            if(!$A.util.isEmpty(filterData.EngineModel) && !$A.util.isUndefined(filterData.EngineModel)) {
                                engineModel = filterData.EngineModel;
                                console.log('getEngineModelPicklist  if1:'+engineModel);
                            }
                        }
                    }
                    var isExist = false;
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        console.log('getEngineModelPicklist  for:'+response.getReturnValue().length);
                        if(isFilter == true){
                            if(response.getReturnValue()[i] == engineModel) {
                                isExist = true;
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                                console.log('getEngineModelPicklist  if2:'+opts);
                            }
                            else
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                            console.log('getEngineModelPicklist  else2:'+opts); 
                        }
                        else
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        console.log('getEngineModelPicklist  result:'+allValues);
                    }
                    if(isExist == false)
                        console.log('getEngineModelPicklist  isExist false:');
                    engineModel = response.getReturnValue()[0];
                    console.log('getEngineModelPicklist  engineModel:'+engineModel);
                    component.find('engineModelId').set("v.options", opts);
                    console.log('getEngineModelPicklist  output:'+opts);
                    this.getEngineTemplateForEM(component, engineModel );
                    this.getEngineRunPickList(component);
                    //this.getRatioPicklist(component, mpId,engineModel);
                }
                else {
                    console.log('Engine Type not available');
                    opts.push({class: "optionClass",label: "No Data",value: ""});
                    component.find('engineModelId').set("v.options", opts);
                    
                    var dependentFields = [];
                    /*dependentFields.push({value: 'No Data', selected: false});
                    component.set("v.listDependingEngineValues", dependentFields);*/
                    
                    dependentFields.push({"class": "optionClass", label: 'No Data', value: ''});
                    
                    var LLPVisiblity = component.get("v.LLPVisiblity");
                    if(LLPVisiblity == true) {
                        component.find('engineTempId').set("v.options", dependentFields);
                    
                    	component.set('v.etCatalogYear', 'No Data');
                    }
                    
                    component.find('engineRunId').set("v.options", opts);
                    component.find('ratioId').set("v.options", opts);
                    component.find('ratioFromId').set("v.options", opts);
                    component.find('ratioToId').set("v.options", opts);
                }
            }
            else{
            console.log('Engine Type not available');
                    opts.push({class: "optionClass",label: "No Data",value: ""});
                    component.find('engineModelId').set("v.options", opts);
                    
                    var dependentFields = [];
                    /*dependentFields.push({value: 'No Data', selected: false});
                    component.set("v.listDependingEngineValues", dependentFields);*/
                    
                    dependentFields.push({"class": "optionClass", label: 'No Data', value: ''});
                	var LLPVisiblity = component.get("v.LLPVisiblity");
                	if(LLPVisiblity == true) {
                    	component.find('engineTempId').set("v.options", dependentFields);
                    
                    	component.set('v.etCatalogYear', 'No Data');
                	}
                	component.find('engineRunId').set("v.options", opts);
                    component.find('ratioId').set("v.options", opts);
                    component.find('ratioFromId').set("v.options", opts);
                    component.find('ratioToId').set("v.options", opts);
                }    
            
        });
        $A.enqueueAction(action);
    },
    
    getAPUModelPicklist: function(component, mpId) {
        console.log('getAPUModelPicklist '+mpId);
        var action = component.get("c.getAPUModel");
        action.setParams({
            "MPId": mpId
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getAPUModelPicklist state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getAPUModelPicklist  result:'+allValues);
                if(allValues != null && allValues.length > 0) {
                    var apuModel = response.getReturnValue()[0];
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getAPUModelPicklist filterData: '+filterData + ' isFilter: '+isFilter);
                    if(isFilter == true) {
                        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                            if(!$A.util.isEmpty(filterData.APUModel) && !$A.util.isUndefined(filterData.APUModel)) {
                                apuModel = filterData.APUModel;
                            }
                        }
                    }
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        if(isFilter == true) {
                            if(apuModel == response.getReturnValue()[i])
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected: true});
                            else
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        }
                        else
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                    component.find('APUModelId').set("v.options", opts);
                }
                else {
                    console.log('APU Model not available');
                    opts.push({class: "optionClass",label: "No Data",value: ""});
                    component.find('APUModelId').set("v.options", opts);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getLGModelPicklist: function(component, mpId) {
        console.log('getLGModelPicklist '+mpId);
        var action = component.get("c.getLGModel");
        action.setParams({
            "MPId": mpId
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getLGModelPicklist state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getLGModelPicklist  result:'+allValues);
                if(allValues != null && allValues.length > 0) {
                    var LGModel = response.getReturnValue()[0];
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getLGModelPicklist filterData: '+filterData + ' isFilter: '+isFilter);
                    if(isFilter == true) {
                        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                            if(!$A.util.isEmpty(filterData.LGModel) && !$A.util.isUndefined(filterData.LGModel)) {
                                LGModel = filterData.LGModel;
                            }
                        }
                    }
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        if(isFilter == true) {
                            if(LGModel == response.getReturnValue()[i])
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected: true});
                            else
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        }
                        else
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                    component.find('LGModelId').set("v.options", opts);
                }
                else {
                    console.log('LG Model not available');
                    opts.push({class: "optionClass",label: "No Data",value: ""});
                    component.find('LGModelId').set("v.options", opts);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    
    getRatioPicklist: function(component, mpId, applicability) {
        console.log('getRatioPicklist '+mpId);
        var engineRunId = component.find('engineRunId').get("v.value");
        var action = component.get("c.getRatio");
        action.setParams({
            "MPId": mpId,
            "Applicability" : applicability,
            "Run" : engineRunId
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRatioPicklist state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getRatioPicklist  result:'+allValues);
                if(allValues != null && allValues.length > 0) {
                    var ratioD = response.getReturnValue()[0];
                    var ratioRange = component.get("v.ratioRange");
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getRatioPicklist filterData: '+filterData + ' isFilter: '+isFilter + 'ratioD: '+ratioD);
                    if(isFilter == true) {
                        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                            if(!$A.util.isEmpty(filterData.FHFCRatio) && !$A.util.isUndefined(filterData.FHFCRatio)) {
                                ratioD = filterData.FHFCRatio;
                                console.log('ratioD : '+ratioD);
                            }
                            if(!$A.util.isEmpty(filterData.RatioRange) && !$A.util.isUndefined(filterData.RatioRange)) {
                                ratioRange = filterData.RatioRange;
                                console.log('ratioRange : '+ratioRange);
                            }
                        }
                    }
                    var ratioFromOpts = [];
                    var ratioToOpts = [];
                    var defaultRatioFrom = 0, defaultRatioTo = 0, defaultRatioStep = 0;
                    if(!$A.util.isEmpty(ratioRange) && !$A.util.isUndefined(ratioRange)){
                        var range = ratioRange.split(';');
                        console.log('getRatioPicklist range ' + range);
                        if(range != null && range.length > 2){
                            defaultRatioFrom = range[0];
                            defaultRatioTo = range[1];
                            defaultRatioStep = range[2];
                        }
                    }
                    console.log('getRatioPicklist defaultRatioFrom ' + defaultRatioFrom +
                                ' defaultRatioTo:'+defaultRatioTo + ' defaultRatioStep:'+defaultRatioStep);
                    
                    var isUpperLimitExists = false;
                    var totalItem = response.getReturnValue().length;
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        //if(isFilter == true) {
                        if(ratioD == response.getReturnValue()[i])
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                        else
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        //}
                        //else
                        //	opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        
                        if(defaultRatioFrom != 0 && parseFloat(defaultRatioFrom) == parseFloat(response.getReturnValue()[i]))
                            ratioFromOpts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                        else if(defaultRatioFrom == 0 &&  i == 0)
                            ratioFromOpts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                            else 
                                ratioFromOpts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        
                        
                        if(i == (totalItem-1) && isUpperLimitExists == false){
                            ratioToOpts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                        }
                        else{
                            if(defaultRatioTo != 0 && parseFloat(defaultRatioTo) == parseFloat(response.getReturnValue()[i])) {
                                ratioToOpts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                                isUpperLimitExists = true;
                            }
                            else if(defaultRatioTo == 0 && i == response.getReturnValue().length - 1)  {
                                ratioToOpts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                                isUpperLimitExists = true;
                            }
                                else 
                                    ratioToOpts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        }
                        
                    }
                    //if(isUpperLimitExists == false)
                    component.find('ratioId').set("v.options", opts);
                    
                    //console.log('FH:FC ratio'+opts);
                    //console.log('ratioFromOpts :'+JSON.stringify(ratioFromOpts));
                    component.find('ratioFromId').set("v.options", ratioFromOpts);
                    //console.log('ratioToOpts :'+ratioToOpts);
                    component.find('ratioToId').set("v.options", ratioToOpts);
                }
                else {
                    console.log('Ratio not available');
                    opts.push({class: "optionClass",label: "No Data",value: ""});
                    component.find('ratioId').set("v.options", opts);
                    console.log('FH:FC ratio'+opts);
                    component.find('ratioFromId').set("v.options", opts);
                    component.find('ratioToId').set("v.options", opts);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getRatioSteplist: function(component) {
        console.log('getRatioSteplist ');
        
        var ratioRange = component.get("v.ratioRange");
        var defaultRatioStep = 0;
        var filterData = component.get("v.filterData");
        console.log('getRatioSteplist filterData: '+filterData );
        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
            if(!$A.util.isEmpty(filterData.RatioRange) && !$A.util.isUndefined(filterData.RatioRange)) {
                ratioRange = filterData.RatioRange;
            }
        }
        
        if(!$A.util.isEmpty(ratioRange) && !$A.util.isUndefined(ratioRange)){
            var range = ratioRange.split(';');
            console.log('getRatioPicklist range ' + range);
            if(range != null && range.length > 2){
                defaultRatioStep = range[2];
            }
        }
        
        var opts = [];
        if(defaultRatioStep == 0.1)
            opts.push({"class": "optionClass", label: 0.1, value: 0.1, selected : true});
        else
            opts.push({"class": "optionClass", label: 0.1, value: 0.1});
        
        if(defaultRatioStep == 0.2)
            opts.push({"class": "optionClass", label: 0.2, value: 0.2, selected : true});
        else
            opts.push({"class": "optionClass", label: 0.2, value: 0.2});
        
        if(defaultRatioStep == 0.5)
            opts.push({"class": "optionClass", label: 0.5, value: 0.5, selected : true});
        else
            opts.push({"class": "optionClass", label: 0.5, value: 0.5});
        
        if(defaultRatioStep == 1.0)
            opts.push({"class": "optionClass", label: 1.0, value: 1.0, selected : true});
        else
            opts.push({"class": "optionClass", label: 1.0, value: 1.0});
        component.find('ratioStepId').set("v.options", opts);
        
    },
    
    resetAllPickList: function(component) {
        console.log('resetAllPickList ');
        component.set('v.catalogYear', 'No Data');
        component.set('v.etCatalogYear', 'No Data');
        component.set('v.NumEngine', 'No Data');
        component.set('v.LLPRepl', '0.5');
        var opts = [];
        opts.push({class: "optionClass",label: "No Data",value: ""});
        component.find('engineModelId').set("v.options", opts);
        component.find('APUModelId').set("v.options", opts);
        component.find('engineRunId').set("v.options", opts);
        component.find('ratioId').set("v.options", opts);
        console.log('resetAllPickList : FH:FC ratio values '+opts);
        component.find('ratioFromId').set("v.options", opts);
        component.find('ratioToId').set("v.options", opts);
        var LLPVisiblity = component.get("v.LLPVisiblity");
        if(LLPVisiblity == true)
            component.find('engineTempId').set("v.options", opts);
    },
    
    getEngineTemplateForEM: function(component, emValue) {
        console.log('getEngineTemplateForEM ');
        var action = component.get("c.getEngineTemplateListForEM");
        var self = this;
        action.setParams({
            EMValue : emValue
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getEngineTemplateForEM state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.engineTemplateList" , allValues)
                console.log('getEngineTemplateForEM  result:'+allValues);
                
                //var aircraftType = component.find('aircraftTypeId').get("v.value");
                //console.log('getEngineTemplateForEM: aircraftType ' +aircraftType);
                
                var opts = [];
                if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues) && allValues.length > 0){
                    var engTemp = allValues[0];
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getEngineType filterData: '+filterData + ' isFilter: '+isFilter);
                    if(isFilter == true) {
                        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                            if(!$A.util.isEmpty(filterData.EngineTemplate) && !$A.util.isUndefined(filterData.EngineTemplate)) {
                                engTemp = filterData.EngineTemplate;
                            }
                        }
                    }
                    var isExists = false;
                    for(var i = 0; i < allValues.length; i++){
                        if(isFilter == true) {
                            if(engTemp == allValues[i]){
                                isExists = true;
                                opts.push({"class": "optionClass", label: allValues[i], value: allValues[i], selected:true});  
                            }
                            else
                                opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});  
                        }
                        else
                            opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});  
                    }
                    if(isExists == false)
                        engTemp = allValues[0];
                    this.getETCatalogYear(component, engTemp);
                }else{
                    component.set('v.etCatalogYear', 'No Data');
                    opts.push({"class": "optionClass", label: 'No Data', value: ''});
                } 
                var LLPVisiblity = component.get("v.LLPVisiblity");
                if(LLPVisiblity == true)
                    component.find('engineTempId').set("v.options", opts);
                
            }
        });
        $A.enqueueAction(action);
    },
    
    getETCatalogYear: function(component, ET) {
        console.log('getETCatalogYear '+ET);
        var action = component.get("c.getETCatalogYear");
        action.setParams({
            "ETName": ET
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getETCatalogYear state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getETCatalogYear  result:'+allValues);
                if(allValues != null)
                    component.set('v.etCatalogYear', allValues);
                else
                    component.set('v.etCatalogYear', 'No Data');
            }
        });
        $A.enqueueAction(action);
    },
    
    
    getEngineRunPickList: function(component) {
        console.log('getEngineRunPickList');
        var mpIdV = component.find('maintProgId').get("v.value");
        var emId = component.find('engineModelId').get("v.value");
        console.log('getEngineRunPickList emId: '+emId);
        console.log('getEngineRunPickList mpId: '+mpIdV);
        var action = component.get("c.getEngineRun");
        action.setParams({
            "MPId": mpIdV,
            "Applicability": emId
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('getEngineRunPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getEngineRunPickList  result:'+allValues);
                if(allValues != null && allValues.length > 0) {
                    var engRun = response.getReturnValue()[0];
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getEngineRunPickList filterData: '+filterData + ' isFilter: '+isFilter);
                    if(isFilter == true) {
                        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                            if(!$A.util.isEmpty(filterData.EngineRun) && !$A.util.isUndefined(filterData.EngineRun)) {
                                engRun = filterData.EngineRun;
                            }
                        }
                    }
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        if(isFilter == true){
                            if(engRun == response.getReturnValue()[i])
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected:true});
                            else
                                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        }
                        else
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                    component.find('engineRunId').set("v.options", opts);
                    
                    var etId = component.find('engineModelId').get("v.value");
                    var mpId = component.find('maintProgId').get("v.value");
                    self.getRatioPicklist(component, mpId,etId);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getOpEnvlist: function(component) {
        console.log('getOperatorEnvlist ');
        var action = component.get("c.getOperatorEnvList");
        action.setParams({
            
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getOperatorEnvlist state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getOperatorEnvlist  result:'+allValues);
                if(allValues != null && allValues.length > 0) {
                    console.log('getOperatorEnvlist  result:'+JSON.stringify(allValues));    
                    var opEnv = response.getReturnValue()[0].ObjId;
                    var filterData = component.get("v.filterData");
                    var isFilter = component.get("v.isFilterData");
                    console.log('getEngineType filterData: '+filterData + ' isFilter: '+isFilter);
                    if(isFilter == true) {
                        if(!$A.util.isEmpty(filterData) && !$A.util.isUndefined(filterData)) {
                            if(!$A.util.isEmpty(filterData.EnvironmentFactor) && !$A.util.isUndefined(filterData.EnvironmentFactor)) {
                                opEnv = filterData.EnvironmentFactor;
                            }
                        }
                    }
                    
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        if(opEnv == response.getReturnValue()[i].ObjId)
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i].ObjName, value: response.getReturnValue()[i].ObjId, selected:true});
                        else
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i].ObjName, value: response.getReturnValue()[i].ObjId});
                    }
                    component.find('operatorEnvId').set("v.options", opts);
                }
                else {
                    console.log('Operator Env not available');
                    opts.push({class: "optionClass",label: "No Data",value: ""});
                    component.find('operatorEnvId').set("v.options", opts);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getNumEnglist: function(component, type) {
        console.log('getNumEnglist ');
        var action = component.get("c.getNumberOfEngines");
        action.setParams({
            aircraftType : type
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getNumEnglist state: ',response.getState() );
            var allValues = response.getReturnValue();
            console.log('getNumEnglist  result:'+allValues);
            if(allValues != null) {
                component.set('v.NumEngine', allValues.NumOfEngines);
                component.set('v.aircraftClass', allValues.AircraftClass);
                var ratioRange = allValues.RatioRange;
                component.set("v.ratioRange", ratioRange);
            }
            else {
                console.log('Number of Engines not available');
                component.set('v.NumEngine', 'No Data');
                component.set("v.ratioRange", "");
            }
            this.getRatioSteplist(component);
        });
        $A.enqueueAction(action);
    },
    
    saveFilterData: function(component) {
        console.log('saveFilterData ');
        var aircraftType = component.find('aircraftTypeId').get("v.value");
        var mpId = component.find('maintProgId').get("v.value");
        
        var engineTempId = null;
        var LLPVisiblity = component.get("v.LLPVisiblity");
        if(LLPVisiblity == true)
            engineTempId = component.find('engineTempId').get("v.value");
        
        var etCatalogYear = component.get('v.etCatalogYear');
        var engineModelId = component.find('engineModelId').get("v.value");
        var catalogYearId = component.get('v.catalogYear');
        var APUModelId = component.find('APUModelId').get("v.value");
        var LGModelId = component.find('LGModelId').get("v.value");
        var engineRunId = component.find('engineRunId').get("v.value");
        var ratioId = component.find('ratioId').get("v.value");
        var operatorEnvId = (component.find('operatorEnvId').get("v.value") / 100);
        var llpRep = component.get("v.LLPRepl");
        var ratioFromId = component.find('ratioFromId').get("v.value");
        var ratioToId = component.find('ratioToId').get("v.value");
        var ratioStepId = component.find('ratioStepId').get("v.value");
        var ratioRg = ratioFromId+";"+ratioToId+";"+ratioStepId;
        
        console.log('saveFilterData aircraftType: '+aircraftType);
        console.log('saveFilterData mpId: '+mpId);
        console.log('saveFilterData engineTempId: '+engineTempId);
        console.log('saveFilterData etCatalogYear: '+etCatalogYear);
        console.log('saveFilterData engineModelId: '+engineModelId);
        console.log('saveFilterData catalogYearId: '+catalogYearId);
        console.log('saveFilterData APUModelId: '+APUModelId);
        console.log('saveFilterData LGModelId: '+LGModelId);
        console.log('saveFilterData engineRunId: '+engineRunId);
        console.log('saveFilterData ratioId: '+ratioId);
        console.log('saveFilterData operatorEnvId: '+operatorEnvId);
        console.log('saveFilterData llpRep: '+llpRep);
        console.log('saveFilterData ratioRg: '+ratioRg);
        
        var action = component.get("c.saveFilter");
        action.setParams({
            rName : component.get("v.recordId"),
            aircraftType : aircraftType,
            maintProg: mpId,
            engineModel: engineModelId,
            apuModel: APUModelId,
            LGModel: LGModelId,
            engineRun: engineRunId,
            ratio: ratioId,
            engineTempl: engineTempId,
            operEnv: operatorEnvId,
            llpbuildUp: llpRep,
            ratioRange: ratioRg
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('saveFilter state: ',response.getState() );
            var allValues = response.getReturnValue();
            console.log('saveFilter  result:'+allValues);
            component.set("v.showOutput", true);
            component.set("v.isLoading", false);                
        });
        $A.enqueueAction(action);
    },
    
    
    
    getMaintProgOuputData: function(component) {
        console.log('getMaintProgOuputData');
        var mpId = component.find('maintProgId').get("v.value");
        
        var engineTempId = null;
        var LLPVisiblity = component.get("v.LLPVisiblity");
        if(LLPVisiblity == true) {
        	engineTempId = component.find('engineTempId').get("v.value");
        
        var etCatalogYear = component.get('v.etCatalogYear');
        }
        var engineModelId = component.find('engineModelId').get("v.value");
        var catalogYearId = component.get('v.catalogYear');
        var APUModelId = component.find('APUModelId').get("v.value");
        var LGModelId = component.find('LGModelId').get("v.value");
        var engineRunId = component.find('engineRunId').get("v.value");
        var ratioId = component.find('ratioId').get("v.value");
        var operatorEnvId = (component.find('operatorEnvId').get("v.value") / 100);
        
        console.log('getMPOuputData mpId: '+mpId);
        console.log('getMPOuputData engineTempId: '+engineTempId);
        console.log('getMPOuputData etCatalogYear: '+etCatalogYear);
        console.log('getMPOuputData engineModelId: '+engineModelId);
        console.log('getMPOuputData catalogYearId: '+catalogYearId);
        console.log('getMPOuputData APUModelId: '+APUModelId);
        console.log('getMPOuputData APUModelId: '+LGModelId);
        console.log('getMPOuputData engineRunId: '+engineRunId);
        console.log('getMPOuputData ratioId: '+ratioId);
        console.log('getMPOuputData operatorEnvId: '+operatorEnvId);
        
        var self = this;
        //var action = component.get("c.sampleData");
        var action = component.get("c.generateOutput");
        action.setParams({
            MPId : mpId,
            EngineTemplate: engineTempId,
            ETCatalogYr: etCatalogYear,
            EngineModel: engineModelId,
            CatalogYr: catalogYearId,
            APUModel: APUModelId,
            LGModel: LGModelId,
            EngineRun: engineRunId,
            Ratio: ratioId,    
            OperatorEnv: operatorEnvId,
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var mpeList = response.getReturnValue();
                console.log("getOuput response: "+JSON.stringify(mpeList));
                component.set("v.rentGeneratedList",mpeList);
                if(mpeList != null) {
                    var halfLife = 0; 
                    var engineCost = 0;
                    var engineLLP = 0;
                    var otherCost = 0;
                    var numOfEngines = component.get("v.NumEngine");
                    for(var i = 0 ; i < mpeList.length ; i++){
                        var type = mpeList[i].EventType;
                        if(type == 'Engine' && !$A.util.isEmpty(mpeList[i].BaseCost) && !$A.util.isUndefined(mpeList[i].BaseCost)) {
                            engineCost = engineCost + mpeList[i].BaseCost;
                            //halfLife = halfLife + mpeList[i].BaseCost * numOfEngines;
                            console.log('BaseCost '+mpeList[i].BaseCost + ' type: ' +type);
                        }
                        else if (type == 'Engine LLP' && !$A.util.isEmpty(mpeList[i].BaseCost) && !$A.util.isUndefined(mpeList[i].BaseCost)) {
                            
                            //halfLife = halfLife + mpeList[i].BaseCost * numOfEngines * percent;
                            engineLLP = engineLLP + mpeList[i].BaseCost;
                            console.log('BaseCost '+mpeList[i].BaseCost + ' type: ' +type);
                        }
                            else if(type != 'LLPStub' && !$A.util.isEmpty(mpeList[i].BaseCost) && !$A.util.isUndefined(mpeList[i].BaseCost)) {
                                //halfLife = halfLife + mpeList[i].BaseCost;    
                                otherCost = otherCost + mpeList[i].BaseCost;    
                                console.log('BaseCost '+mpeList[i].BaseCost + ' type: ' +type);
                            }
                    }
                    //LLPRepl value is coming as 0.5 for 50% so not dividing by 100 
                    //this is because of the inputfield type percentage
                    
                    var percent = component.get("v.LLPRepl");
                    halfLife = (engineCost * numOfEngines) + (engineLLP * numOfEngines * percent) + otherCost;
                    halfLife = halfLife * 0.5;
                    component.set("v.halfLife" , halfLife);
                    console.log('getMPOuputData halfLife: '+halfLife);
                    
                    self.roundOff(component);
                    
                }
                this.saveFilterData(component);
            }
            else if (state === "ERROR") {
                component.set("v.rentGeneratedList", null);
                component.set("v.showOutput", true);
                component.set("v.isLoading", false);    
                var errors = response.getError();
                console.log("getMPOuputData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        if(errors[0].message.includes('No Catalog Data')) {
                            var mpeMissingData = {};
                            try{
                                var msg = errors[0].message;
                                var splitMsg = msg.split('{');
                                var splitMsg2 = splitMsg[1].split('}'); 
                                var mpeMsg = splitMsg2[0].split(',');
                                var nameMsg = mpeMsg[0].split(':');
                                var name = nameMsg[1];
                                var IdMsg = mpeMsg[1].split(':');
                                var Id = IdMsg[1];
                                
                                mpeMissingData.Id = Id;
                                mpeMissingData.Name = name;
                                
                                console.log('mpeMissingData '+JSON.stringify(mpeMissingData));
                                
                                component.set('v.mpeMissingData', mpeMissingData);
                                component.set("v.errorMsg", "Maintenance Event has no Catalog record for current year. Please check Maintenance Program for missing data");
                            }
                            catch(err) {
                                console.log('Exception '+err);
                                component.set("v.errorMsg", "Maintenance Event has no Catalog record for current year. Please check Maintenance Program for missing data");
                            }
                            
                        }
                        else if(errors[0].message.includes('No Template Data')) 
                            component.set("v.errorMsg", "Maintenance Event has no Template record. Please check Engine Template for missing data");
                            else if(errors[0].message.includes('No MPE')) 
                                component.set("v.errorMsg", "Maintenance Event has no MPE. Please check Maintenance Program for missing data");
                                else
                                    component.set("v.errorMsg","There's source data missing, cannot generate output!");
                        //self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set("v.errorMsg","There's source data missing, cannot generate output!");
                    //self.showErrorMsg(component, "Error in creating record");
                }
            }
            this.getRatioTable(component);
        });
        $A.enqueueAction(action);                
    },
    
    getRatioTable: function(component) {
        console.log('getRatioTable ');
        
        var mpId = component.find('maintProgId').get("v.value");
        var engineModelId = component.find('engineModelId').get("v.value");
        var engineRunId = component.find('engineRunId').get("v.value");
        var ratioFromId = component.find('ratioFromId').get("v.value");
        var ratioToId = component.find('ratioToId').get("v.value");
        var ratioStepId = component.find('ratioStepId').get("v.value");
        var operatorEnvId = component.find('operatorEnvId').get("v.value");
        
        var action = component.get("c.getAdjustedIntervalRatio");
        action.setParams({
            MPId : mpId,
            Applicability : engineModelId,
            Run: engineRunId,
            EnvFactor: operatorEnvId
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRatioTable state: ',response.getState() );
            var allValues = response.getReturnValue();
            console.log('getRatioTable  result:'+allValues);
            if(allValues != null && allValues.length > 0){
               // console.log('getRatioTable ratioFromId: '+ratioFromId);
               // console.log('getRatioTable ratioToId: '+ratioToId);
               // console.log('getRatioTable ratioStepId: '+ratioStepId);
                var ratioTableData=[];
                var j = 0;
                var stepVal = parseFloat(ratioFromId);
                console.log('stepVal :'+stepVal);
                var RoundOption = component.get("v.RoundOption");
                console.log(' RoundOption '+RoundOption);
                for(var i =0; i< allValues.length; i++){
                    //console.log('allValues.length '+allValues.length);
                    if(parseFloat(allValues[i].Name) >= parseFloat(ratioFromId) && parseFloat(allValues[i].Name) <= parseFloat(ratioToId)) {
                        //console.log('if1 allValues[i].Name :'+allValues[i].Name +' ratioFromId : '+ratioFromId+ ' ratioToId : '+ratioToId );
                        if(parseFloat(allValues[i].Name) == parseFloat(stepVal)) {
                            //console.log('if2 stepVal : '+stepVal);
                            var mapp = {};
                            mapp.Id = allValues[i].Id;
                            if(RoundOption == 'Round up')
                                mapp.Cost = Math.ceil(allValues[i].Cost);
                            else if(RoundOption == 'Round down')
                            	mapp.Cost = Math.floor(allValues[i].Cost);
                            else
                                mapp.Cost = Math.round(allValues[i].Cost);
                            //console.log('FH:FC cost values :'+mapp.Cost);
                            mapp.Name = allValues[i].Name;
                            ratioTableData[j] = mapp;
                            stepVal = parseFloat(ratioStepId) + parseFloat(stepVal);
                            //console.log('if2 stepVal ON INC : '+stepVal);
                            stepVal = stepVal.toFixed(2);
                            j++;
                        }
                        else if(parseFloat(allValues[i].Name) > parseFloat(stepVal))  {
                            //console.log('else');
                            //console.log('else : allValues[i].Name :'+allValues[i].Name+ ' parseFloat(stepVal : '+parseFloat(stepVal));
                            var mapp = {};
                            mapp.Id = '';
                            mapp.Cost = -1;
                            mapp.Name = stepVal;
                            ratioTableData[j] = mapp;
                            stepVal = parseFloat(ratioStepId) + parseFloat(stepVal);
                            stepVal = stepVal.toFixed(2);
                            j++;
                            i--;
                        }
                    }
                }
                component.set("v.ratioTableData", ratioTableData);
                
               // console.log('getRatioTable ratioTableData '+JSON.stringify(ratioTableData));
                
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields, isFirstLoad) {
        console.log("fetchDepValues ListOfDependentFields: "+JSON.stringify(ListOfDependentFields));
        var dependentFields = [];
        //dependentFields.push({value: 'None', selected: false});
        var engineTemplateList = component.get("v.engineTemplateList");
        console.log("fetchDepValues engineTemplateList: "+engineTemplateList);
        if(!$A.util.isEmpty(engineTemplateList) && !$A.util.isUndefined(engineTemplateList)) {
            console.log("fetchDepValues engineTemplateList: "+JSON.stringify(engineTemplateList));
            for (var i = 0; i < ListOfDependentFields.length; i++) { 
                if(engineTemplateList.includes(ListOfDependentFields[i])) {
                    console.log("fetchDepValues if case: "+ListOfDependentFields[i]);
                    //dependentFields.push({value: ListOfDependentFields[i], selected: false});
                    dependentFields.push({"class": "optionClass", label: ListOfDependentFields[i], value: ListOfDependentFields[i]});
                }
            }
        }
        if(!$A.util.isEmpty(dependentFields) && 
           !$A.util.isUndefined(dependentFields) && 
           dependentFields.length > 0){
            if(isFirstLoad) {
                this.getETCatalogYear(component, dependentFields[0].value);
            }
        }
        else{
            //dependentFields.push({value: 'No Data', selected: false});
            dependentFields.push({"class": "optionClass", label: 'No Data', value: ''});
        }
        //component.set("v.listDependingEngineValues", dependentFields);
        var LLPVisiblity = component.get("v.LLPVisiblity");
        if(LLPVisiblity == true)
            component.find('engineTempId').set("v.options", dependentFields);
    },
    
    showWarningMsg : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Warning!",
            message: "There's source data missing, cannot generate output",
            duration:'5000',
            key: 'info_alt',
            type: 'warning',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    roundOff: function(component) {
        var isRoundOff = component.get("v.isRoundOff");
        console.log("roundOff "+isRoundOff);
        var rentGeneratedList = component.get("v.rentGeneratedList");
        var RoundOption = component.get("v.RoundOption");
        console.log("roundOff "+isRoundOff + ' RoundOption '+RoundOption);
        if(!$A.util.isEmpty(rentGeneratedList) && !$A.util.isUndefined(rentGeneratedList) ) {
            for(var i = 0; i < rentGeneratedList.length; i++) {
                if(RoundOption == 'Round up')
                    rentGeneratedList[i].CostPerUnit = Math.ceil(rentGeneratedList[i].CostPerUnit);
                else if(RoundOption == 'Round down')
                    rentGeneratedList[i].CostPerUnit = Math.floor(rentGeneratedList[i].CostPerUnit);
                else
                    rentGeneratedList[i].CostPerUnit = Math.round(rentGeneratedList[i].CostPerUnit);
                component.set("v.rentGeneratedList", rentGeneratedList);
            }   
        }
        if(!$A.util.isEmpty(rentGeneratedList) && !$A.util.isUndefined(rentGeneratedList) && isRoundOff == true) {
            var halfLife = component.get("v.halfLife");
            halfLife = Math.round(halfLife/1000)*1000;
            component.set("v.halfLife",halfLife);
            component.set("v.rentGeneratedList", rentGeneratedList);
        }
    },
    
    
    validateInput: function(component, event, helper) {
        component.set("v.isFilterData", false);
        console.log('validateInput');
        
        var mpId = component.find('maintProgId').get("v.value");
        var engineTempId = null;
        var LLPVisiblity = component.get("v.LLPVisiblity");
        if(LLPVisiblity == true)
            engineTempId = component.find('engineTempId').get("v.value");
        var etCatalogYear = component.get('v.etCatalogYear');
        var engineModelId = component.find('engineModelId').get("v.value");
        var catalogYearId = component.get('v.catalogYear');
        var APUModelId = component.find('APUModelId').get("v.value");
        var LGModelId = component.find('LGModelId').get("v.value");
        var engineRunId = component.find('engineRunId').get("v.value");
        var ratioId = component.find('ratioId').get("v.value");
        var operatorEnvId = component.find('operatorEnvId').get("v.value");
        var NumEngine = component.get('v.NumEngine');
        var LLPRepl = component.get('v.LLPRepl');
        
        console.log('onGenerateClick mpId: '+mpId);
        console.log('onGenerateClick engineTempId: '+engineTempId);
        console.log('onGenerateClick etCatalogYear: '+etCatalogYear);
        console.log('onGenerateClick engineModelId: '+engineModelId);
        console.log('onGenerateClick catalogYearId: '+catalogYearId);
        console.log('onGenerateClick APUModelId: '+APUModelId);
        console.log('onGenerateClick LGModelId: '+LGModelId);
        console.log('onGenerateClick engineRunId: '+engineRunId);
        console.log('onGenerateClick ratioId: '+ratioId);
        console.log('onGenerateClick operatorEnvId: '+operatorEnvId);
        console.log('onGenerateClick NumEngine: '+NumEngine);
        console.log('onGenerateClick LLPRepl: '+LLPRepl);
        
        var isValid = true;
        if($A.util.isEmpty(mpId) || $A.util.isUndefined(mpId) || mpId == 'No Data') 
            isValid = false;
        
        if(LLPVisiblity == true && ($A.util.isEmpty(engineTempId) || $A.util.isUndefined(engineTempId) || engineTempId.includes('No Data'))) 
            isValid = false;
        
        if(LLPVisiblity == true && ($A.util.isEmpty(etCatalogYear) || $A.util.isUndefined(etCatalogYear) || etCatalogYear.includes('No Data'))) 
            isValid = false;
        
        if($A.util.isEmpty(engineModelId)  || engineModelId.includes('No Data')) 
            isValid = false;
        if($A.util.isEmpty(catalogYearId) || $A.util.isUndefined(catalogYearId) || catalogYearId.includes('No Data')) 
            isValid = false;
        if($A.util.isEmpty(APUModelId) || APUModelId.includes('No Data')) 
            isValid = false;
        if($A.util.isEmpty(LGModelId) || LGModelId.includes('No Data')) 
            isValid = false;
        if($A.util.isEmpty(engineRunId) || $A.util.isUndefined(engineRunId) || engineRunId.includes('No Data')) 
            isValid = false;
        if($A.util.isEmpty(ratioId) || $A.util.isUndefined(ratioId) || ratioId.includes('No Data')) 
            isValid = false;
        if($A.util.isEmpty(operatorEnvId) || $A.util.isUndefined(operatorEnvId) || operatorEnvId.includes('No Data')) 
            isValid = false;
        if($A.util.isEmpty(LLPRepl) || $A.util.isUndefined(LLPRepl)) 
            isValid = false;
        if($A.util.isEmpty(NumEngine) || $A.util.isUndefined(NumEngine) || NumEngine == 'No Data') 
            isValid = false;
        console.log('onGenerateClick valid: '+isValid);
        
        if(isValid) {
            component.set("v.isLoading", true);
            helper.getMaintProgOuputData(component);
        }
        else{
            helper.showWarningMsg(component);
        }
    },
    
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
    
    
    
    
})