({
    addSelected : function(component, event, helper) {        
        component.set("v.proccessOperator",true);
        
        //openFilterModal attribute used to handle modal on campaign 
        component.set("v.openFilterModal", false);
        
        if(component.get("v.loadedInPhone")){
            component.find("overlayLib").notifyClose();
        }
    },
    
    
    closeAddOperatorPopup: function(component, event, helper) {
        console.log('closeAddOperatorPopup ',component.get("v.openFilterModal"));
        if(!component.get("v.openFilterModal")){
            document.getElementById("addOperatorFrontdrop").style.display = "none";
            document.getElementById("addOperatorBackdrop").style.display = "none";     
        } 
        //openFilterModal attribute used to handle modal on campaign 
        component.set("v.openFilterModal", false);
        
        if(component.get("v.loadedInPhone")){
            component.find("overlayLib").notifyClose();
        }
    } 
})