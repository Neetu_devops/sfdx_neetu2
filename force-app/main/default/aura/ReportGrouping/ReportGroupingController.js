({
    setLabel: function (component, event, helper) {
        //console.log('groupings: ',component.get("v.groupings"));
        var level = component.get("v.level");
        var groupingLevelToLabel = component.get("v.groupingLevelToLabel");
        if (groupingLevelToLabel) {
            component.set("v.fieldLabel", groupingLevelToLabel[level]);
        }
    },
    editRecord: function (component, event, helper) {
        var recordId = event.currentTarget.dataset.recordid;
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": recordId
        });
        editRecordEvent.fire();
    }
})