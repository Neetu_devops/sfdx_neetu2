({
	doInit : function(component, event, helper) {
        
        window.setTimeout(
            $A.getCallback(function() {
                helper.loadData(component, event, helper);
            }), 200);
	},
    
    saveData : function(component, event, helper) {
        
        var recordId = helper.getURLParameterValue().recordId;
        var data = component.get("v.data");
        if(data.periodFrom === undefined || data.periodFrom === '' || data.periodFrom === null){
            helper.handleErrors('Please enter the Utilization Period');
            return;
        }
        if(data.periodTo === undefined || data.periodTo === '' || data.periodTo === null){
            helper.handleErrors('Please enter the Utilization Period');
            return;
        }
        let d1 = new Date(data.periodFrom);
        let d2 = new Date(data.periodTo);
        if(d2 < d1){
            helper.handleErrors('Period To cannot be before Period From');
            return;
        }
        //check if asset has assemblies or not.
        if(data.row1.length>0 || data.row2.length>0 || data.row3.length>0){
            
        	helper.showSpinner(component);
            var action = component.get("c.save");
            action.setParams({"jsonData" : JSON.stringify(data),
                              "assetId" : recordId});
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.data.urStagingId", response.getReturnValue());
                    helper.showToast("Success!", "success", "Utilizations saved successfully.");
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        helper.handleErrors(errors);
                    } else {
                        console.log("Unknown error");
                    }
                }
                helper.hideSpinner(component);
            });
            
            $A.enqueueAction(action);
        }  
	},
    
    handleErrors : function(component, event, helper) {
        
        var errors = event.getParam("errors");
        helper.handleErrors(errors);
    },
    
    handleUploadFinished : function(component, event, helper) {
    	helper.showToast("Success!", "success", "File(s) uploaded successfully.");
    },
    
    /*contactUs: function(cmp, event, helper) {
        
        //Create component dynamically        
        $A.createComponent(
            "c:Com_Cmty_Contact_Us",{},
            function(msgBox){                
                if (cmp.isValid()) {
                    var targetCmp = cmp.find('contact-us');
                    var body = targetCmp.get("v.body");
                    body.push(msgBox);
                    targetCmp.set("v.body", body); 
                }
            }
        );
    },*/
    
    submitUR : function (component, event, helper) {

        var data = component.get("v.data");
        if(data.periodFrom === undefined || data.periodFrom === '' || data.periodFrom === null){
            helper.handleErrors('Please enter the Utilization Period');
            return;
        }
        if(data.periodTo === undefined || data.periodTo === '' || data.periodTo === null){
            helper.handleErrors('Please enter the Utilization Period');
            return;
        }
        let d1 = new Date(data.periodFrom);
        let d2 = new Date(data.periodTo);
        if(d2 < d1){
            helper.handleErrors('Period To cannot be before Period From');
            return;
        }
        var urStagingId = component.get("v.data.urStagingId");
		var assetId = helper.getURLParameterValue().recordId;
        
        helper.showSpinner(component);
        var action = component.get("c.submitUtilization");
        action.setParams({"assetId" : assetId,
                          "urStagingId" : urStagingId});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast("Success!", "success", "Utilizations submitted successfully.");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                } else {
                    console.log("Unknown error");
                }
            }
            helper.hideSpinner(component);
        });
        
        $A.enqueueAction(action);
    },

    toggleRows : function(component, event, helper){
        
       let index = event.getSource().get("v.value");
       let engineList = component.get("v.data.row2");
       engineList[index].showChildRows =  engineList[index].showChildRows == undefined ? true : !engineList[index].showChildRows; 
       component.set("v.data.row2", engineList); 
    }

})