({
	  loadData: function(cmp, event, helper){

        //set Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title" : "Submit Monthly Utilization"});
        appEvent.fire();
       
        
        //read parameter from URL.
        var recordId = helper.getURLParameterValue().recordId;
     
        if(recordId != undefined && recordId !=''){
        
            helper.showSpinner(cmp);
            
            var action = cmp.get("c.loadData");
            action.setParams({"assetId" : recordId});
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var data =  response.getReturnValue();
                    
                    if(data.assetType == 'Engine' && data.row2.length>0){
                        data.row2[0].showChildRows = true;
                    }
                    console.log(data);
                    cmp.set("v.data", data);
                    
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        helper.handleErrors(errors);
                    } else {
                        console.log("Unknown error");
                    }
                }
                helper.hideSpinner(cmp);
            });
            
            $A.enqueueAction(action);
        }  
    },

    showToast: function(title, type, msg){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": msg
        });
        toastEvent.fire();
    },
    
	getURLParameterValue: function() {
 
        var querystring = location.search.substr(1);
        var paramValue = {};
        querystring.split("&").forEach(function(part) {
            var param = part.split("=");
            paramValue[param[0]] = decodeURIComponent(param[1]);
        });
 
        return paramValue;
    },
    
    showSpinner: function (component) {
        var spinner = component.find("spinner");
        console.log('spinner' + spinner);
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    handleErrors: function (errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errors === 'object') {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });
                    };
                }
            });
        }
        else if (typeof errors === "string" && errors.length != 0) {
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})