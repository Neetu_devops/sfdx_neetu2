({
    init: function (component, event, helper) {

        var action = component.get("c.getCommunityPrefix");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.data", result.data);
            }

        });

        $A.enqueueAction(action);
    },
    
})