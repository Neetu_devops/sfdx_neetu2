({
	doInit : function(component, event, helper) {
		
	},
    
    refreshPage : function(component,event,helper) {
        console.log("refreshPage");
        helper.getParentData(component,event);
        helper.getColumns(component, event);
    },
    
    updateItem : function(component,event,helper) {
        console.log("updateItem");
        var status = component.find('status').get("v.value");
        var id = component.get("v.recordId");
        console.log("updateItem status "+status + " id : "+id);
        helper.updateParentRecord(component,event,id, status);
    },
    
    handleRowAction : function(cmp,event,helper){
        var row = event.getParam('row');
        var id = row.Id;
        //console.log("handleRowAction " + id);
        window.open('/' +id); 	
    },
    
    onSubmit  : function(component, event, helper) {
        var eventFields = event.getParam("fields");
        console.log("onSubmit "+JSON.stringify(eventFields));
        component.find('parentRecord').submit(eventFields);
    },
    
    onRecordSuccess : function(component, event, helper){
        console.log("onRecordSuccess");
        helper.fireSuccessToast(component);  
        var index = component.get("v.sectionIndex")
        var onStatusChanged = component.getEvent("onStatusUpdate");
        onStatusChanged.setParams({
            "itemId": component.get("v.recordId"),
            "itemIndex" : index,
            "childIndex" : component.get("v.childIndex")
        });
        onStatusChanged.fire();
    },
    
    onRecordFailure : function(component,event,helper) {
        console.log("onRecordFailure");
        helper.fireFailureToast(component);  
    },
    
    createNewFinding : function(component, event, helper) {
        console.log("createNewFinding ");
        var createCustFindings = $A.get("e.force.createRecord");
        createCustFindings.setParams({
            'entityApiName': component.get("v.parentObject"), 
            
        });
        createCustFindings.fire();
    }

})