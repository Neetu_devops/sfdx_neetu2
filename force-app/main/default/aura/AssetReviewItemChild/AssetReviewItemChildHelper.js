({
    getNameSpacePrefix : function(component){
        var self = this;
        var action = component.get('c.getNamespacePrefix');
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            component.set('v.namespace', namespace);
            console.log("getNameSpacePrefix "+namespace);
            
            self.getParentData(component);
        	self.getColumns(component);
        });
        $A.enqueueAction(action);
    },
    
    getColumns : function(component) {
        var self = this;
        var recordId = component.get("v.recordId");
        console.log("getColumns recordId "+recordId);
        var action = component.get('c.getColumns');
        var objectName = component.get('v.childObject');
        var fields = component.get('v.childFieldSet');
        action.setParams({
            objectName : objectName,
            fieldSetName : fields
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getColumns values " +JSON.stringify(allValues));
            component.set("v.columns", allValues); 
            var cols = [];
            for(var i = 0; i < allValues.length; i++) {
                if(allValues[i].apiName == 'Name') {
                    cols.push({label: allValues[i].label, fieldName: allValues[i].apiName, type: 'button', 
                               typeAttributes: {label: { fieldName: allValues[i].apiName }}, sortable: true,
                               initialWidth: 150});
				}
                else if(allValues[i].apiName.includes('Status')) {
                    cols.push({label :allValues[i].label, fieldName: allValues[i].apiName , 
                               type : allValues[i].feildType, initialWidth: 100});
                }
				else if(allValues[i].apiName == 'Id') {}
                else
                	cols.push({label :allValues[i].label, fieldName: allValues[i].apiName , 
                               type : allValues[i].feildType, sortable: true, initialWidth: 400,});
            }
            component.set("v.columns", cols);
            self.getRows(component, "OPEN");
            self.getRows(component, "CLOSED");
        });
        $A.enqueueAction(action);
    },
    
    getRows : function(component , status) {
        var recordId = component.get("v.recordId");
        console.log("getRows recordId "+recordId);
        var action = component.get('c.getRowsBasedOnStatus');
        var objectName = component.get('v.childObject');
        var fields = component.get('v.childFieldSet');
        var parentCond = component.get('v.childCond');
        var statusCond = component.get('v.childStatusCond');
        
        action.setParams({
            objectName : objectName,
            mappingCond : parentCond,
            recordId : recordId,
            fieldSetName : fields,
            status : status,
            statusCond :statusCond
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
          //  console.log("getRowsBasedOnStatus status " +status);
            console.log("getRowsBasedOnStatus values " +JSON.stringify(allValues));
            if(status == "OPEN")
            	component.set("v.openData", allValues); 
            else
                component.set("v.closedData", allValues); 
        });
        $A.enqueueAction(action);
    },
    
    getParentData : function(component){
        var action = component.get('c.getRecordDetails');
        action.setParams({
            objectName : component.get('v.parentObject'),
            recordId : component.get('v.recordId'), //immediate parent record id(review item id)
            fieldSetName : component.get('v.parentRecordFieldset')
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getParentData child values " +JSON.stringify(allValues));
            if(!$A.util.isUndefined(allValues) && !$A.util.isEmpty(allValues)) {
                var data = allValues[0];
                var parentData = [];
                var parentFields = [];
                for (var key in data) {
                    parentData.push({key: key, value: data[key]});
                    if(key != "Name" && key != "Id")
                    parentFields.push(key);
                };
                var parentObjectNamespace = component.get('v.namespace') + component.get('v.parentObject');
                console.log("getParentData parentObjectNamespace"+ parentObjectNamespace);
                component.set("v.parentObjectNamespace" , parentObjectNamespace);
                component.set("v.parentData" , parentData);
                component.set("v.parentFields" , parentFields);
                console.log("getParentData values " +JSON.stringify(parentFields));
            }
        });
        $A.enqueueAction(action);
    },
    
    updateParentRecord : function(component, event, recordId, status) {
       // console.log("updateParentRecord "+component.get("v.childIndex"));
        var self = this;
        var action = component.get('c.updateStatus');
        action.setParams({
            recordId : recordId,
            status : status, 
            objectName: component.get('v.parentObject'),
            statusCond : component.get('v.parentStatusCond')            
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("updateParentRecord status " +status);
            var state = response.getState();
            if (state === "SUCCESS") {
                self.fireSuccessToast(component);  
              //  helper.fireRefreshEvt(component);
                
                var index = component.get("v.sectionIndex")
                var onStatusChanged = component.getEvent("onStatusUpdate");
                onStatusChanged.setParams({
                    "itemId": recordId,
                    "itemIndex" : index,
                    "childIndex" : component.get("v.childIndex")
                });
                onStatusChanged.fire();
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(JSON.stringify(errors));
                self.fireFailureToast(component);  
            }
          	
        });
        $A.enqueueAction(action);
    },
    
    fireSuccessToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'title' : 'Success', 
            'message' : 'Record updated sucessfully.' ,
            'type':'success'
        }); 
        toastEvent.fire(); 
    },
    
    fireFailureToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'title' : 'Failed', 
            'message' : 'An error occurred.',
            'type':'error'
        }); 
        toastEvent.fire(); 
    },
    
    fireRefreshEvt : function(component) {
        var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent){
            console.log("fireRefreshEvt");
            refreshEvent.fire();
        }
    }
})