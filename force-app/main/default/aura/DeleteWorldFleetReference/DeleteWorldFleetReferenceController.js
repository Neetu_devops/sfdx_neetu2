({
    deleteAllWorldFleet: function(component, event, helper) {
        var answer = event.currentTarget.dataset.answer;
        
        if (answer == "Yes") {
            var action = component.get("c.deleteFleetRecord");
            helper.getNamespacePrefix(component);
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if (response.getState() == "SUCCESS") {
                    var event1 = $A.get("e.force:navigateToObjectHome");
                    
                    event1.setParams({
                        scope: component.get("v.namespace") + "Lessor__c"
                    });
                    
                    event1.fire();
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        Title: "Deleted All World Fleets and Summaries",
                        type: "success",
                        message: "An email will be sent after all records have been processed"
                    });
                    resultsToast.fire();
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
            });
            $A.enqueueAction(action);
        } else {
            $A.get("e.force:closeQuickAction").fire();
        }
    }
});