({
    doInit : function(component, event, helper) {
        console.log("doInit: " + component.get('v.recordId'));
        helper.getParamValue('AuditScheduler', 'c__AuditId', component);
        helper.fetchLeaseData(component); 
        
    },
    getfirstAuditDate : function(component){
        console.log("getfirstAuditDate");
        var contractDate = component.get("v.contractDate");
        var firstAuditDate = component.find("auditStartId").get("v.value");
        var periodicity = component.find("periodicity").get("v.value");
        console.log("contractDate :"+contractDate+ " periodicity :"+periodicity+"firstAuditDate :"+firstAuditDate);
        if(contractDate != null && contractDate != undefined){
        var conDate = new Date(contractDate);
        var day =   conDate.getDate();  
        //for addition we use +periodicity, else it will concatenate like strings    
        var months = conDate.getMonth() + 1 + +periodicity;
        console.log('conDate.getMonth() : '+conDate.getMonth()+ 'months :'+months);
        conDate.setMonth(months-1);
        var year = conDate.getFullYear(); 
        console.log('year :'+year+' months :'+months+' day :'+day);
        var newdate= new Date(year,conDate.getMonth(),day);    
        var finalDate=($A.localizationService.formatDate(newdate, "yyyy-MM-dd"));
             
        
       component.set("v.auditStartValue",finalDate);
        console.log("newdate:",newdate);
       console.log("finalDate:",finalDate);
        }
        else{
        component.set("v.auditStartValue",firstAuditDate);
        console.log('firstAuditDate :'+firstAuditDate);
        }
        },
           
    openRecord: function(component,event,helper) {
        var record = event.currentTarget.id;
        console.log("openRecord "+record);
        window.open('/' + record);  
    },
    openModel: function(component, event, helper) {
      // Set isModalOpen attribute to true
      component.set("v.isModalOpen", true);
   },
  
   closeModel: function(component, event, helper) {
      // Set isModalOpen attribute to false  
      component.set("v.isModalOpen", false);
       helper.leasePagePreview(component);
   },
  
   submitData: function(component, event, helper) {
       var isdelete = component.find("deleteopenId").get("v.checked");
       var periodicity = component.find("periodicity").get("v.value");
       var firstAuditDate = component.find("auditStartId").get("v.value");
       
       console.log('submitData isdelete :'+isdelete);
       component.set("v.isdelete",isdelete);
       component.set("v.auditPeriodicity",periodicity);
       console.log('auditStartValue :',component.get("v.auditStartValue"));
       console.log('recordId :',component.get("v.recordId"));
       console.log('isdelete :',component.get("v.isdelete"));
       console.log('contractDate :',component.get("v.contractDate"));
       console.log('auditPeriodicity :',component.get("v.auditPeriodicity"));
      // Set isModalOpen attribute to false
      //Add your code to call apex method or do some processing
       if(periodicity == null || firstAuditDate == null){
      helper.showErrorMsg(component, "Please enter values to Generate Audit Schedules");
       }else{
           component.set("v.isModalOpen", false);
      helper.generateSchedule(component);
      helper.fetchLeaseData(component); 
      
       } 
   },
    
})