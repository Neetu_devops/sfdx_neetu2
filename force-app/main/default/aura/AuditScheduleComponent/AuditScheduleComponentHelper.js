({
	getParamValue : function( tabName, paramName,component ) {
 		var url = window.location.href;
        var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&amp;amp;amp;');
        var paramValue = '';
        console.log("getParamValue url "+url);
        console.log("getParamValue allParams "+allParams);
        if(allParams != null && allParams != undefined){
        for(var i=0; i < allParams.length; i++) {
            if(allParams[i].split('=')[0] == paramName)
                paramValue = allParams[i].split('=')[1];
        }
        console.log("getParamValue: " + paramValue);
        component.set('v.recordId', paramValue);
        }
    }, 

    generateSchedule: function(component ) {
            console.log('generateSchedule');
        var auditStartDate = new Date(component.get("v.auditStartValue"));
        console.log('generateSchedule auditStartDate ;'+auditStartDate);
     var action = component.get("c.InsertAuditRecs");   
        action.setParams({
            auditStartValue : auditStartDate,
            auditPeriodicity : component.get("v.auditPeriodicity"),
            isdelete : component.get("v.isdelete"),
            contractDate : component.get("v.contractDate"),
            recordId : component.get("v.recordId")
             });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("generateSchedule response-state: "+state);
            if (state === "SUCCESS") {
                var auditList = response.getReturnValue();
                //$A.get('e.force:refreshView').fire();
                this.leasePagePreview(component);
                //location.reload();

            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("generateSchedule errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("generateSchedule Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action); 
        
    },
    
    leasePagePreview : function(component, event, helper) {
        console.log('leasePagePreview');
        var leaseRec = component.get("v.recordId");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
           "recordId": leaseRec,
           "slideDevName": "related"
       });
       navEvt.fire();
        $A.get('e.force:refreshView').fire();
        //self.location.reload(true);
    },
    
    fetchLeaseData : function(component){
     console.log('fetchLeaseData');
        
     var action = component.get("c.getLeaseData");   
        action.setParams({
            recordId : component.get("v.recordId")
             });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("fetchLeaseData response-state: "+state);
            if (state === "SUCCESS") {
                var leaseInfo = response.getReturnValue();
                if(leaseInfo != null){
                console.log("fetchLeaseData response: "+JSON.stringify(leaseInfo));
                component.set("v.contractDate",leaseInfo.ContractDeliverDate);
                component.set("v.agreementDate",leaseInfo.AgreementDate); 
                }
                
              }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("fetchLeaseData errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("fetchLeaseData Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
             
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
     //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})