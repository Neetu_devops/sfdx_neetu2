({
    doInit: function (cmp, event, helper) {
        helper.loadInitData(cmp, event, helper);
    },
    
    handleValidateAction: function (cmp, event, helper) {
        let creditMemoData = cmp.get("v.creditMemoData");
        let creditToAllocate = cmp.get("v.creditToAllocate");
        let amountToCredit = cmp.get("v.creditMemoAmountCredited");
        let hasError = false;
        
        if (creditMemoData.loadedFromInvoice && (helper.checkUndefined(creditMemoData.creditMemoRec) || helper.checkUndefined(creditMemoData.creditMemoRec.Id))) {
            hasError = true;
            helper.handleMessage("Please select a Credit Memo record to be applied to an Invoice.");
        }
        else if (!creditMemoData.loadedFromInvoice && (helper.checkUndefined(creditMemoData.invoiceRec) || helper.checkUndefined(creditMemoData.invoiceRec.Id))) {
            hasError = true;
            helper.handleMessage("Please select an Invoice record to allocate the Credit Memo.");
        }
        else if(!helper.checkUndefined(amountToCredit)&& amountToCredit <= 0.0){
            hasError = true;
            helper.handleMessage("'Amount To Credit' should be greater than 0");
        }
        else if (cmp.get("v.hasError")) {
            hasError = true;
            helper.handleMessage("Please review Amount to be Credited.");
        }
        
        if (!hasError && !helper.checkUndefined(creditMemoData.mrBreakOut) && creditMemoData.isMRInvoice) {
            for (let key in creditMemoData.mrBreakOut) {
                if (!hasError && creditMemoData.mrBreakOut[key].overPayed) {
                    hasError = true;
                    helper.handleMessage("Credit allocated exceeds 'Balance Due' on the line item.");
                    break;
                }
            }
            
            if (!hasError && creditMemoData.mrBreakOut.length > 0 && !helper.checkUndefined(creditToAllocate) && creditToAllocate < 0) {
                hasError = true;
                helper.handleMessage("Sum of individual credit allocations exceeds the 'Amount to Credit'.");
            }
            
            if (!hasError && creditMemoData.mrBreakOut.length > 0 && !helper.checkUndefined(creditToAllocate) && creditToAllocate > 0) {
                hasError = true;
                helper.handleMessage("Remaining Allocation Amount should be 0 to continue with the allocation. Please allocate the remaining allocation amount to the line items accordingly.");
            }
        }
        
        if (!hasError && !helper.checkUndefined(creditMemoData.accountingPeriodRec) && !helper.checkUndefined(creditMemoData.accountingPeriodRec.Id)) {
            if (creditMemoData.accountingPeriodRec.Closed__c) {
                hasError = true;
                helper.handleMessage("The Accounting Period is closed. Please reopen the period or select a new accounting period.");
            }
        }
        
        if (!hasError && !helper.checkUndefined(creditMemoData.invoiceRec) && !helper.checkUndefined(creditMemoData.invoiceRec.Id) && !helper.checkUndefined(creditMemoData.invoiceRec.Y_hidden_AccEnabled__c)) {
            if (creditMemoData.invoiceRec.Y_hidden_AccEnabled__c && (helper.checkUndefined(creditMemoData.accountingPeriodRec) || helper.checkUndefined(creditMemoData.accountingPeriodRec.Id))) {
                hasError = true;
                helper.handleMessage("Please make sure you have selected an accounting period to continue.");
            }
        }
        
        if (!hasError) {
            helper.handleSaveAction(cmp, event, helper);
        }
    },
    
    handleCancel: function (cmp, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
    handleRecordChange: function (cmp, event, helper) {
        let creditMemoData = cmp.get("v.creditMemoData");
        let creditMemoAmountCredited = cmp.get("v.creditMemoAmountCredited");
        let updateAmount = false;
        
        if (helper.checkUndefined(creditMemoAmountCredited)) { creditMemoAmountCredited = 0.0; }
        
        if (creditMemoData.loadedFromInvoice) {
            if (!helper.checkUndefined(creditMemoData.creditMemoRec) && !helper.checkUndefined(creditMemoData.creditMemoRec.Id) && !helper.checkUndefined(creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c)) {
                if (!helper.checkUndefined(creditMemoData.invoiceRec.Balance_Due__c) && creditMemoData.invoiceRec.Balance_Due__c > 0) {
                    if (creditMemoData.invoiceRec.Balance_Due__c > creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c) {
                        creditMemoAmountCredited = creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c;
                        updateAmount = true;
                    }
                    else {
                        creditMemoAmountCredited = creditMemoData.invoiceRec.Balance_Due__c;
                        updateAmount = true;
                    }
                }
            }
            else if (creditMemoAmountCredited !== creditMemoData.invoiceRec.Balance_Due__c) {
                creditMemoAmountCredited = creditMemoData.invoiceRec.Balance_Due__c;
                updateAmount = true;
            }
        }
        else {
            if (!helper.checkUndefined(creditMemoData.invoiceRec) && !helper.checkUndefined(creditMemoData.invoiceRec.Id) && !helper.checkUndefined(creditMemoData.invoiceRec.Balance_Due__c)) {
                if (!helper.checkUndefined(creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c) && creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c > 0) {
                    if (creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c > creditMemoData.invoiceRec.Balance_Due__c) {
                        creditMemoAmountCredited = creditMemoData.invoiceRec.Balance_Due__c;
                        updateAmount = true;
                    }
                    else {
                        creditMemoAmountCredited = creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c;
                        updateAmount = true;
                    }
                }
                if (!helper.checkUndefined(creditMemoData.invoiceRec.Accounting_Period__c) && !helper.checkUndefined(creditMemoData.invoiceRec.Accounting_Period__r.Id)) {
                    cmp.set("v.creditMemoData.accountingPeriodRec", creditMemoData.invoiceRec.Accounting_Period__r);
                }
                if (!helper.checkUndefined(creditMemoData.invoiceRec.RecordType.DeveloperName) || !helper.checkUndefined(creditMemoData.invoiceRec.Invoice_Type__c)) {
                             
                    helper.getInvoiceLineItems(cmp, event, helper);                       

                }
            }
            else if (creditMemoAmountCredited !== creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c) {
                cmp.set("v.creditMemoData.mrBreakOut", undefined);
                cmp.set("v.creditMemoData.isMRInvoice", false);
                cmp.set("v.creditMemoData.accountingPeriodRec", undefined);
                creditMemoAmountCredited = creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c;
                updateAmount = true;
            }
        }
        if (updateAmount) {
            cmp.set("v.hasError", false);
            cmp.set("v.creditMemoAmountCredited", parseFloat(creditMemoAmountCredited).toFixed(2));
            if (!helper.checkUndefined(creditMemoData.mrBreakOut)) {
                for (let key in creditMemoData.mrBreakOut) {
                    if (!helper.checkUndefined(creditMemoData.mrBreakOut[key].amountAllocated)) {
                        creditMemoAmountCredited = parseFloat(creditMemoAmountCredited) - parseFloat(creditMemoData.mrBreakOut[key].amountAllocated);
                    }
                }
            }
            cmp.set("v.creditToAllocate", parseFloat(creditMemoAmountCredited).toFixed(2));
        }
    },
    
    onCreditedAmountChange: function (cmp, event, helper) {
        let creditMemoData = cmp.get("v.creditMemoData");
        let creditMemoAmountCredited = cmp.get("v.creditMemoAmountCredited");
        
        if (helper.checkUndefined(creditMemoAmountCredited)) { creditMemoAmountCredited = 0.0; }
        
        if (creditMemoAmountCredited > 0 && !helper.checkUndefined(creditMemoData.invoiceRec.Balance_Due__c) && creditMemoData.invoiceRec.Balance_Due__c < creditMemoAmountCredited) {
            cmp.set("v.hasError", true);
        }
        else if (creditMemoAmountCredited > 0 && !helper.checkUndefined(creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c) && creditMemoData.creditMemoRec.Available_Credit_Memo_Amount_F__c < creditMemoAmountCredited) {
            cmp.set("v.hasError", true);
        }
            else {
                cmp.set("v.hasError", false);
                if (!helper.checkUndefined(creditMemoData.mrBreakOut)) {
                    for (let key in creditMemoData.mrBreakOut) {
                        if (!helper.checkUndefined(creditMemoData.mrBreakOut[key].amountAllocated)) {
                            creditMemoAmountCredited = parseFloat(creditMemoAmountCredited) - parseFloat(creditMemoData.mrBreakOut[key].amountAllocated);
                        }
                    }
                }
                cmp.set("v.creditToAllocate", parseFloat(creditMemoAmountCredited).toFixed(2));
            }
    },
    
    onAllocatedAmountChange: function (cmp, event, helper) {
        var recordIndex = parseInt(event.getSource().get("v.name"));
        let mrBreakOut = cmp.get("v.creditMemoData.mrBreakOut");
        let amountAllocated = event.getSource().get("v.value");
        let creditToAllocate = cmp.get("v.creditToAllocate");
        let creditMemoAmountCredited = cmp.get("v.creditMemoAmountCredited");
        
        if (helper.checkUndefined(creditMemoAmountCredited)) { creditMemoAmountCredited = 0.0; }
        if (helper.checkUndefined(amountAllocated)) { amountAllocated = 0.0; }
        
        if (!helper.checkUndefined(mrBreakOut[recordIndex])) {
            if (!helper.checkUndefined(amountAllocated)) {
                if (!helper.checkUndefined(mrBreakOut[recordIndex].lineItemRec.Balance_Due__c) && amountAllocated > mrBreakOut[recordIndex].lineItemRec.Balance_Due__c) {
                    creditToAllocate = parseFloat(creditMemoAmountCredited) - parseFloat(amountAllocated);
                    helper.handleMessage('Credit allocated exceeds the Balance Due on line item.', "Warning");
                    mrBreakOut[recordIndex].overPayed = true;
                }
                else {
                    creditToAllocate = parseFloat(creditMemoAmountCredited) - parseFloat(amountAllocated);
                    if (amountAllocated === 0) { mrBreakOut[recordIndex].amountAllocated = 0.0; }
                    mrBreakOut[recordIndex].overPayed = false;
                }
            }
            else {
                creditToAllocate = parseFloat(creditMemoAmountCredited);
            }
        }
        
        for (let key in mrBreakOut) {
            if (key != recordIndex && !helper.checkUndefined(mrBreakOut[key].amountAllocated)) {
                creditToAllocate = parseFloat(creditToAllocate) - parseFloat(mrBreakOut[key].amountAllocated);
            }
        }
        cmp.set("v.creditToAllocate", creditToAllocate.toFixed(2));
        cmp.set("v.creditMemoData.mrBreakOut", mrBreakOut);
    },
})