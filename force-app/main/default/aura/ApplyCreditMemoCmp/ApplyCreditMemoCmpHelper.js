({
    loadInitData: function (cmp, event, helper) {
        this.showSpinner(cmp);
        let action = cmp.get("c.fetchInitData");
        action.setParams({
            "recordId": cmp.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.creditMemoData", response.getReturnValue());
            } else {
                var errors = response.getError();
                this.handleMessage(errors);
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
    },

    getInvoiceLineItems: function (cmp, event, helper) {
        this.showSpinner(cmp);
        let action = cmp.get("c.fetchInvoiceLineItemData");
        action.setParams({
            "creditMemoData": JSON.stringify(cmp.get("v.creditMemoData")),
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.creditMemoData.mrBreakOut", response.getReturnValue().mrBreakOut);
                cmp.set("v.creditMemoData.isMRInvoice", response.getReturnValue().isMRInvoice);
            } else {
                var errors = response.getError();
                this.handleMessage(errors);
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
    },

    handleSaveAction: function (cmp, event, helper) {
        this.showSpinner(cmp);
        let creditMemoData = cmp.get("v.creditMemoData");
        creditMemoData.amountToCredit = cmp.get("v.creditMemoAmountCredited");

        if (!this.checkUndefined(creditMemoData.mrBreakOut)) {
            let actualAmountAllocated = 0;
            for (let key in creditMemoData.mrBreakOut) {
                if (!this.checkUndefined(creditMemoData.mrBreakOut[key].amountAllocated)) {
                    actualAmountAllocated = parseFloat(actualAmountAllocated) + parseFloat(creditMemoData.mrBreakOut[key].amountAllocated);
                }
            }
            creditMemoData.actualAllocatedAmount = actualAmountAllocated;
        }

        let action = cmp.get("c.applyCreditMemo");
        action.setParams({
            "creditMemoData": JSON.stringify(creditMemoData),
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                if (creditMemoData.loadedFromInvoice) {
                    this.handleMessage("Credit Memo applied successfully", "Success");
                }
                else {
                    this.handleMessage("Credit Memo applied to Invoice successfully", "Success");
                }
                $A.get("e.force:closeQuickAction").fire();
            } else {
                var errors = response.getError();
                this.handleMessage(errors);
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
    },

    checkUndefined: function (input) {
        if (input !== undefined && input !== '' && input !== null) { return false; }
        return true;
    },

    showSpinner: function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    //Show the error toast when any error occured
    handleMessage: function (errors, type, duration) {
        duration = "10000"; // time to display the Toast message.
        _jsLibrary.handleErrors(errors, type, duration); // Calling the JS Library function to handle the error
    }
})