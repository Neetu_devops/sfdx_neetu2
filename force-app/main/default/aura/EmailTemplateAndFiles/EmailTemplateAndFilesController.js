({
    doInit: function (component, event, helper) {
        helper.showSpinner(component);
        var action = component.get("c.sendEmail");
        action.setParams({
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() === 'SUCCESS') {
                    component.set("v.msgBody", "Email was sent successfully.");
                }
                else {
                    component.set("v.msgBody", response.getReturnValue());
                }
            }
            else {
                var errors = response.getError();
                helper.handleMessage(errors);
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    handleClose: function (cmp, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    }
})