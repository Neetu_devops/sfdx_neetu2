({
    handleOnClick : function(component, event, helper) {
        $A.util.toggleClass(component.find("divHelp"), 'slds-fall-into-ground');
    },
    
    
    handleMouseLeave : function(component, event, helper) {
        $A.util.addClass(component.find("divHelp"), 'slds-fall-into-ground');
    },
    
    
    handleMouseEnter : function(component, event, helper) {
        $A.util.removeClass(component.find("divHelp"), 'slds-fall-into-ground');
    }
})