({
    doInit : function(component, event, helper) {
    	var selectedVal = component.get("v.assetType") == 'Engine' ? 'CFM56' : 'A320';
        component.set("v.controllingSelectedVal", selectedVal);
    },
    
        
    addSelected : function(component, event, helper) {        
        var selectedRows = component.get("v.selectedRows");
        var compEvent = $A.get("e.c:MSNSearchResultEvent");
        compEvent.setParams({assets : selectedRows,
                             objectName : 'Aircraft__c',
                             fieldName : 'Name'}).fire();
        component.set("v.proccessMSN",true);
        if(component.get("v.loadedInPhone")){
            component.find("overlayLib").notifyClose();
        }
    },
    
    
    closeAddMSNPopup: function(component, event, helper) {
        document.getElementById("addMSNFrontdrop").style.display = "none";
        document.getElementById("addMSNBackdrop").style.display = "none";
        if(component.get("v.loadedInPhone")){
            component.find("overlayLib").notifyClose();
        }
    }           
})