({
	doInit : function(component, event, helper) {
		helper.getAssetData(component);
	},

	showRecord : function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log('showRecord recordId '+recordId);
        window.open('/' + recordId);  
    },

})