({
    //to get the asset data for the record id
	getAssetData : function(component) {
        component.set("v.isLoading", true);
        var recordIdL = component.get("v.recordId");
        console.log('getAssetData recordId: '+recordIdL );
        var self = this;
        if($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getAssetScoreDetails');
        action.setParams({
            recordId : recordIdL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getAssetData : state "+state);
            component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getAssetData values " +allValues);
                if(allValues != null) {
                    console.log("getAssetData values " +JSON.stringify(allValues));
                    component.set("v.assetData", allValues);
                    self.setColor(component);
                }
                else {
                    component.set("v.isLoading", false);
                    self.showErrorMsg(component, "Asset Data not found!");
                }
            }
            else if (state === "ERROR") {    
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getAssetData errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getAssetData unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            //self.getCovidData(component);
        });
        $A.enqueueAction(action);
    },

    //setting color for all factors
    setColor : function(component) {
        var assetData = component.get("v.assetData");
        if(assetData == null)
            return;
        var red = "#FF0000";
        var green = "#228B22";
        var orange = "#FFA500";
        
        if(assetData.operatorRiskFactorScore > 4.5 && assetData.operatorRiskFactorScore <= 6)  {
            component.set("v.operatorRiskFactorColor" ,red);
        }
        else if(assetData.operatorRiskFactorScore > 2.5 && assetData.operatorRiskFactorScore <= 4.5)  {
            
            component.set("v.operatorRiskFactorColor" ,orange);
        }
        else if(assetData.operatorRiskFactorScore > 0 && assetData.operatorRiskFactorScore <= 2.5)  {
            component.set("v.operatorRiskFactorColor" ,green);
        }
        
        if(assetData.govSupportScore > 0 && assetData.govSupportScore <= 2.5)  {
            component.set("v.govSupportColor", green);
        }
        else if(assetData.govSupportScore > 2.5 && assetData.govSupportScore <= 4.5)  {
            component.set("v.govSupportColor" ,orange);
        }
        else if(assetData.govSupportScore > 4.5 && assetData.govSupportScore <= 6)  {
            component.set("v.govSupportColor" ,red);
        }
        
        if(assetData.countryRepossessionScore > 0 && assetData.countryRepossessionScore <= 2.5)  {            
            component.set("v.countryRepossessionColor" ,green);
        }
        else if(assetData.countryRepossessionScore > 4.5 && assetData.countryRepossessionScore <= 6)  {
            component.set("v.countryRepossessionColor" ,red);
        }
        else if(assetData.countryRepossessionScore > 2.5 && assetData.countryRepossessionScore <= 4.5)  {
            component.set("v.countryRepossessionColor" ,orange);
        }

        if(assetData.assetRiskAssessment != null) {
            var riskScore = assetData.assetRiskAssessment;
            if(riskScore >= 1 && riskScore <=2.5)  {
                component.set("v.assetRiskAssessmentColor", green);
            }
            else if (riskScore > 2.5 && riskScore <= 4.5 )  {
                component.set("v.assetRiskAssessmentColor", orange);
            }
            else if (riskScore > 4.5 )  {
                component.set("v.assetRiskAssessmentColor", red);
            }
        }
        
        if(assetData.assetOverallScore != null) {
            var riskScore = assetData.assetOverallScore;
            if(riskScore >= 1 && riskScore <= 2.5)  {
                component.set("v.assetOverallScoreColor", green);
            }
            else if (riskScore > 2.5 && riskScore <= 4.5 )  {
                component.set("v.assetOverallScoreColor", orange);
            }
            else if (riskScore > 4.5 )  {
                component.set("v.assetOverallScoreColor", red);
            }
        }
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('error '+ toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('error '+ toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('error '+ toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})