({
    getNameSpacePrefix : function(component){
        var action = component.get('c.getNameSpacePrefix');
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            component.set('v.namespace', namespace);
            
            console.log("getNameSpacePrefix "+namespace);
            this.getAssetDetails(component, this);
        });
        $A.enqueueAction(action);
    },
    
    getAircraftType: function(component,event, helper){
        console.log('Get Aircraft Type Called');
        var action = component.get('c.getAssetType');
        action.setParams({
            recordId: component.get("v.recordId")
        })
        action.setCallback(this, function(response) {
            var assetType = response.getReturnValue();
            component.set('v.controllingFieldAPI', assetType);
            console.log("assetType "+assetType);
        });
        $A.enqueueAction(action);
    },
    getAssetDetails: function(component, helper) {
        var action = component.get('c.getAssetDetails');
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log('getAssetDetails: '+JSON.stringify(allValues));
            component.set("v.assetData", allValues);
            helper.populateFilterValues(component, helper);
            helper.getDatePicklist(component, helper);
            var namespace = component.get('v.namespace');
            var date = new Date().getFullYear();
            
            if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues) && allValues != null){
                console.log('getAssetDetails  if');
                if(allValues[namespace+'Delivery_Date_N__c'] != null)
                date = new Date(allValues[namespace+'Delivery_Date_N__c']).getFullYear();
                console.log('getAssetDetails date :'+date);
                var engine = allValues[namespace+'Engine_Type__c'];
                console.log('getAssetDetails engine :'+engine);
                var state = allValues[namespace+'New_Used__c'];
                console.log('getAssetDetails state :'+state);
                if(state != null) {
                	var stateLowC = state.toLowerCase();
                    component.set("v.aircraftState", stateLowC);
                }
                var worldFleet = allValues[namespace + 'World_Fleet_Aircraft__c'];
                var aircraftData = allValues[namespace + 'Aircraft__c'];
                var aircraft = null;
                if(worldFleet != null && worldFleet != undefined) {
                    var wf = allValues[namespace + 'World_Fleet_Aircraft__r'];
                	aircraft = wf[namespace + 'AircraftType__c'];
                }
                else if(aircraftData != null && aircraftData != undefined) {
                    var af = allValues[namespace + 'Aircraft__r'];
                	aircraft = af[namespace + 'Aircraft_Type__c'];
                }
                console.log('getAssetDetails aircraft :'+aircraft);
                    if($A.util.isEmpty(engine) || $A.util.isUndefined(engine))
                    engine = null;
                helper.getCompetitorsData(component,  helper, 
                                          aircraft,engine, "ALL", date, 3,state );
            } 
            else { //send default values 
               helper.loadDefaultData(component, helper, 1);
            }
        });
          
        $A.enqueueAction(action);
    },
  
    loadDefaultData : function(component, helper, count){
        var self = this;
        window.setTimeout(
            $A.getCallback(function() {
                var aircraftList = component.get("v.dependentEngineFieldMap");
                console.log("default :" + aircraftList);
                if(aircraftList != null) {
                    var aircraftType = null;
                    for (var singlekey in aircraftList) {
                        aircraftType = singlekey;
                        break;
                    }
                    console.log("default :" + aircraftType);
                    helper.getCompetitorsData(component,  helper, aircraftType, null, "ALL", new Date().getFullYear(), 3,"New");
                    var ListOfDependentEngineFields = aircraftList[aircraftType];
                    // for engine
                    if(ListOfDependentEngineFields.length > 0){
                        component.set("v.bDisabledDependentEngineFld" , false);  
                        self.fetchDepValues(component, ListOfDependentEngineFields, true);    
                    }else{
                        component.set("v.bDisabledDependentEngineFld" , true); 
                        component.set("v.listDependingEngineValues", ['--- None ---']);
                    }
                }
                else{
                    count++;
                    console.log("showChart data not there "+count);
                    if(count < 5)
                    	self.loadDefaultData(component,helper, count);
                    else
                        component.set("v.isLoading", false);
                }
            }), 5000
        );  
    },
    
    getDatePicklist: function(component, helper) {
        console.log('getDatePicklist');
        var action = component.get('c.getDatePicklist');
        var self = this;
        var opts = [];
        
        var date ;//= new Date().getFullYear();
        var assetData = component.get("v.assetData");
        console.log('getDatePicklist assetData : '+JSON.stringify(assetData));
        var namespace = component.get('v.namespace');
        if(assetData != null && assetData[namespace+'Delivery_Date_N__c'] != null) {  
        	date = new Date(assetData[namespace+'Delivery_Date_N__c']).getFullYear();
         console.log("getDatePicklist date if: "+date);}
        
        action.setParams({
            deliveryDate : date
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            for(var i=0;i< response.getReturnValue().length; i++) {
                if(response.getReturnValue()[i] == date) {
                    console.log('getDatePicklist if');
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                    component.find('deliveryDateId').set("v.value", response.getReturnValue()[i]);
                }
                else
                    console.log('getDatePicklist else');
                	opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
        	}
            component.find("deliveryDateId").set("v.options", opts); 
        });
        $A.enqueueAction(action);
    },
    
    getLessorPicklist: function(component, helper, elementId) {
        var action = component.get('c.getLessor');
        var self = this;
        var opts = [];
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            if(allValues != null) {
                opts.push({"class": "optionClass", label: 'ALL', value: 'ALL'});
                for(var i=0;i< allValues.length; i++) {
                    opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                }
                component.find(elementId).set("v.options", opts); 
            }
        });
        $A.enqueueAction(action);
    },
    
    getCompetitorsData: function(component,  helper, aircraftType, engine, lessor, deliveryYear, range, state ){
        console.log('getCompetitorsData aircraftType:'+aircraftType + ',engine: '+engine+', lessor:'+lessor+ ', range:'+range);
        console.log('getCompetitorsData deliveryYear:'+deliveryYear+ ' range: '+range+ ' state :'+state) ;
        
       
        var action = component.get('c.getCompetitorsOrderbook');
        action.setParams({
            'aircraftType' : aircraftType,
            'engine' : engine,
            'dateOfManufacture' : deliveryYear,
            'quarterRange' : range,
            'lessor' : lessor,
            'state' : state
        }); 
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            //console.log('getCompetitorsData output:'+allValues);
            component.set('v.competitorsList', allValues);
            if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues)){
                console.log('getCompetitorsData output:'+JSON.stringify(allValues));
                if(component.get('v.isDraw') == false) { // we need to call draw only if customer has clicked filter
                    helper.createOrderbookGraph(component,helper);
                }
            }
            else {
                console.log('getCompetitorsData output:'+allValues);
                component.set("v.isLoading", false);
            }
        });
        $A.enqueueAction(action);
    },
    
	showChart : function (component, event,helper, count) {
        var self = this;
        window.setTimeout(
            $A.getCallback(function() {
                var item = component.get('v.competitorsList');
                if(!$A.util.isEmpty(item) && !$A.util.isUndefined(item)) {
                	self.createOrderbookGraph(component,helper);
                }
                else{
                    count++;
                    console.log("showChart data not there "+count);
                    if(count < 10)
                    	self.showChart(component,event,helper, count);
                }
            }), 5000
        );  
    },

    createOrderbookGraph : function(cmp,  helper ) { 
    	if(this.chart)
            this.chart.destroy();
        cmp.set('v.isDraw',true);
        var self = this;
        var el = cmp.find('barChart').getElement();
        
        var competitorsList = cmp.get('v.competitorsList');
        if(competitorsList == null || competitorsList.length <= 0) {
            cmp.set("v.isLoading", false);        
            return;
        }
        var data = [], label = [];
        for(var i = 0; i < competitorsList.length; i++) {
            data[i] = competitorsList[i].unplacedCount;
            label[i] = competitorsList[i].dateOfManufacture;
        }
        console.log('createOrderbookGraph data: '+data);
        console.log('createOrderbookGraph label: '+label);
        var ctx = el.getContext('2d');
        this.chart = new Chart(ctx, {
            plugins: [{
                 beforeDraw: function (chart, easing) {
                        if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                            var ctx = chart.chart.ctx;
                            var chartArea = chart.chartArea;
                            ctx.save();
                            ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                            ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                            ctx.restore();
                        }
                    }
            }],
            type: 'bar',
            data: {
                labels: label,
                datasets: [{
                    label: 'Unplaced aircraft',
                    data: data,
                    backgroundColor: '#005fb2',
                    borderColor: '#005fb2',
                    borderWidth: 0
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var index = tooltipItem.index;
                            var competitorData = " ";
                            if(!$A.util.isEmpty(index) && !$A.util.isUndefined(index)) {
                                competitorData = competitorsList[index].unplacedCount;
                            }
                            return competitorData;
                        }
                    }
                },
                onClick: function(evt, activeElements) {
                    console.log("onClick "+activeElements +" evt "+activeElements[0]);
                    if(!$A.util.isEmpty(activeElements[0]) && !$A.util.isUndefined(activeElements[0])) {
                        var elementIndex = activeElements[0]._index;
                        var competitorData = competitorsList[elementIndex];
                        self.openPop(cmp,evt,competitorData);
                    }
                },
                legend: {
                    display: false,
                },
                chartArea: {
                    backgroundColor: '#FAFAF9'
                }
            } 
        });
        cmp.set("v.isLoading", false);
    },    
    
    openPop : function(component,event, competitorData) {
        console.log("openPop "+JSON.stringify(competitorData));
        component.set("v.competitorData", competitorData.fleetData);
        component.set("v.totalCount", competitorData.totalCount);
        component.set("v.unPlacedCount", competitorData.unplacedCount);
        var placedCount = competitorData.totalCount - competitorData.unplacedCount;
        if(placedCount < 0)
            placedCount = 0;
        component.set("v.placedCount", placedCount);
        var percentage = (competitorData.unplacedCount / competitorData.totalCount ) * 100;
        var trend = '#A9A9A9';
        if(percentage >= 50)
            trend='#ff0000';
        else if(percentage > 25 && percentage < 50)
            trend ='#FFA500';
        else if(percentage <= 25)
            trend='#FFFF00'; 
        component.set("v.trend", trend);   
        
        window.setTimeout(
            $A.getCallback(function() {
        var cmpTarget = component.find('pop');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        $A.util.addClass(cmpTarget, 'slds-show');
        //$A.util.removeClass(cmpTarget, 'slds-hide');
        var popup = component.find("pop").getElement();
        console.log("open cmpTarget "+cmpTarget);
        if(popup) {
            var offset = component.find('parentDiv').getElement().getBoundingClientRect().top + 
                document.documentElement.scrollTop ;
            var topPadding = (event.clientY + document.documentElement.scrollTop) - offset + 40;
            var leftOffset = component.find('parentDiv').getElement().getBoundingClientRect().left;
            var documentWidth = document.documentElement.clientWidth - 20;
            var left = event.clientX - leftOffset;
            
            popup.style.top = (topPadding)+'px';
            //popup.style.left = -(leftOffset - 10)+'px';
            popup.style.left = (left - 650)+'px';
            popup.style.display = "inline";
            popup.style.position = "absolute";
            //popup.style.width = documentWidth+ "px";
        }
            }), 200);
    },
    
    fetchPicklistValues: function(component,objDetails,controllerField, dependentField) {
        var self = this;
        var action = component.get("c.getDependentMap");
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField 
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set("v.dependentEngineFieldMap",StoreResponse);
                
                var listOfkeys = []; 
                var ControllerField = []; 
                
                for (var singlekey in StoreResponse) {
                    listOfkeys.push(singlekey);
                }
                
                if (listOfkeys != undefined && listOfkeys.length > 0) {
                   // ControllerField.push({value: '--- None ---', selected: false});
                }
                
                var assetData = component.get('v.assetData');
                var namespace = component.get('v.namespace');
                
                var airType = null;
                console.log('ASSETDATA: ',assetData);
                if(assetData!= null && assetData[namespace+'Aircraft__r'] != null)
                	airType = assetData[namespace+'Aircraft__r'][namespace+'Aircraft_Type__c'];
                else if(assetData!= null && assetData[namespace+'World_Fleet_Aircraft__r'] != null)
                    airType = assetData[namespace+'World_Fleet_Aircraft__r'][namespace+'AircraftType__c'];
                
                for (var i = 0; i < listOfkeys.length; i++) {
                    if(assetData!= null && assetData[namespace+'Aircraft__r'] != null && 
                       assetData[namespace+'Aircraft__r'][namespace+'Aircraft_Type__c'] == listOfkeys[i]) {
                        ControllerField.push({value: listOfkeys[i], selected: true});
                        component.find('aircraftType').set("v.value", listOfkeys[i]);
                    }
                    else if(assetData!= null && assetData[namespace+'World_Fleet_Aircraft__r'] != null && 
                            assetData[namespace+'World_Fleet_Aircraft__r'][namespace+'AircraftType__c'] == listOfkeys[i]) {
                        ControllerField.push({value: listOfkeys[i], selected: true});
                        component.find('aircraftType').set("v.value", listOfkeys[i]);
                    }
                        else
                            ControllerField.push({value: listOfkeys[i], selected: false});
                    //ControllerField.push(listOfkeys[i]);
                }  
                
                if($A.util.isUndefined(airType) || $A.util.isEmpty(airType) || airType == null) {
                    component.find('aircraftType').set("v.value", listOfkeys[0]);
                    airType = listOfkeys[0];
                }
                
                component.set("v.listControllingValues", ControllerField);
                
                if(assetData != null && assetData[namespace+'Aircraft__r'] != null) {
                    var ListOfDependentEngineFields = StoreResponse[assetData[namespace+'Aircraft__r'][namespace+'Aircraft_Type__c']];
                    // for engine
                    if(!$A.util.isUndefined(ListOfDependentEngineFields) && 
                       ListOfDependentEngineFields != null && 
                       ListOfDependentEngineFields.length > 0){
                        component.set("v.bDisabledDependentEngineFld" , false);  
                        self.fetchDepValues(component, ListOfDependentEngineFields, true);    
                    }else{
                        component.set("v.bDisabledDependentEngineFld" , true); 
                        component.set("v.listDependingEngineValues", ['--- None ---']);
                    }
                }
                else if(assetData != null && assetData[namespace+'World_Fleet_Aircraft__r'] != null) {
                    var ListOfDependentEngineFields = StoreResponse[assetData[namespace+'World_Fleet_Aircraft__r'][namespace+'AircraftType__c']];
                    // for engine
                    if(!$A.util.isUndefined(ListOfDependentEngineFields) && 
                       ListOfDependentEngineFields != null && 
                       ListOfDependentEngineFields.length > 0){
                        component.set("v.bDisabledDependentEngineFld" , false);  
                        self.fetchDepValues(component, ListOfDependentEngineFields, true);    
                    }else{
                        component.set("v.bDisabledDependentEngineFld" , true); 
                        component.set("v.listDependingEngineValues", ['--- None ---']);
                    }
                }
            }else{
                console.log('response.getState() == ERROR fetchPicklistValues');
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields, isFirstLoad) {
        var assetData = component.get('v.assetData');
        var namespace = component.get('v.namespace');
        var engine = null;
        if(assetData!= null && assetData[namespace+'Aircraft__r'] != null){
            engine = assetData[namespace+'Aircraft__r'][namespace+'Engine_Type__c'];
            if($A.util.isEmpty(engine) || $A.util.isUndefined(engine))
                engine = null;
        }
        else if(assetData!= null && assetData[namespace+'World_Fleet_Aircraft__r'] != null){
            engine = assetData[namespace+'World_Fleet_Aircraft__r'][namespace+'EngineType__c'];
            if($A.util.isEmpty(engine) || $A.util.isUndefined(engine))
                engine = null;
        }
        var dependentFields = [];
        dependentFields.push({value: '--- None ---', selected: false});
        for (var i = 0; i < ListOfDependentFields.length; i++) { 
            if(isFirstLoad && engine != null && engine == ListOfDependentFields[i]) {
                dependentFields.push({value: ListOfDependentFields[i], selected: true});
                component.find('engine').set("v.value", ListOfDependentFields[i]);
            }
            else
                dependentFields.push({value: ListOfDependentFields[i], selected: false});
        }
        component.set("v.listDependingEngineValues", dependentFields);
    },
    
    populateFilterValues : function(component, helper) {
        console.log('populateFilterValues');
        var controllingFieldAPI = component.get("v.controllingFieldAPI");
        console.log('populateFilterValues controllingFieldAPI : '+controllingFieldAPI);
        var dependingEngineFieldAPI = component.get("v.dependingEngineFieldAPI");
        console.log('populateFilterValues dependingEngineFieldAPI :'+dependingEngineFieldAPI);
        var objDetails = component.get("v.objDetail");
        console.log('populateFilterValues objDetails :'+objDetails);
        helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingEngineFieldAPI);
        helper.getLessorPicklist(component, helper, 'lessor');    
       },
    
   
    
})