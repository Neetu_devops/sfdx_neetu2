({
 
    drawChart : function(component, event, helper) {
        helper.showChart(component, event, helper, 0);
    },
    
    onFilterClick : function(component, event, helper) {
        //close popu
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        
        console.log('onFilterClick');
        component.set('v.isDraw', false);
        component.set("v.isLoading", true);
        
        var aircraftType = component.find('aircraftType').get("v.value");
        console.log('onFilterClick aircraftType :'+aircraftType);
        var engine = component.find('engine').get("v.value");
        console.log('onFilterClick engine :'+engine);
        var lessor = component.find('lessor').get("v.value");
        console.log('onFilterClick lessor :'+lessor);
        var range = component.find('range').get("v.value");
        console.log('onFilterClick range :'+range);
        var deliveryYear = component.find('deliveryDateId').get("v.value");
        console.log('onFilterClick deliveryYear :'+deliveryYear);
        
        if (engine == '--- None ---' || engine.includes('Engine_Type__c')) 
            engine = null;
         var action = component.get('c.getAssetState');
        console.log('getAssetState action'+action);
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getReturnValue();
         console.log("getAssetState response : " +state);
            component.set("v.State", state);
        helper.getCompetitorsData(component,  helper, aircraftType, engine, lessor, deliveryYear, range,state);
          
        });
        $A.enqueueAction(action);
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
    
    onControllerFieldChange: function(component, event, helper) {     
        var controllerValueKey = event.getSource().get("v.value"); // get selected controller field value
        var dependentEngineFieldMap = component.get("v.dependentEngineFieldMap");
        
        if (controllerValueKey != '--- None ---') {
            var ListOfDependentEngineFields = dependentEngineFieldMap[controllerValueKey];
            
            // for engine
            if(ListOfDependentEngineFields.length > 0){
                component.set("v.bDisabledDependentEngineFld" , false);  
                helper.fetchDepValues(component, ListOfDependentEngineFields, false);    
            }else{
                component.set("v.bDisabledDependentEngineFld" , true); 
                component.set("v.listDependingEngineValues", ['--- None ---']);
            }
            
        } else {
            component.set("v.listDependingEngineValues", ['--- None ---']);
            component.set("v.bDisabledDependentEngineFld" , true); 
        }
    },
    
})