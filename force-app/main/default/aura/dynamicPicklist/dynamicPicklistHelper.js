({
    getPicklistWithDefault : function(cmp) {
        var action = cmp.get("c.getDynPickListDefault");
        action.setParams({
            'objectName' : cmp.get('v.objectName'),
            'fieldName' : cmp.get('v.fieldName'),
            'recordTypeName' : cmp.get('v.recordTypeName'),
            'recordTypeLabelName' : cmp.get('v.recordTypeLabelName'),
            'defaultValue' : cmp.get('v.defaultValue')
        });      
        action.setCallback(this,function(response){
            var state = response.getState();
            cmp.set('v.returnValue' , null);
            if (state === "SUCCESS") {
                console.log(cmp.get("v.fieldName"),"response: ",response.getReturnValue());
                
                var opts=[];
                var result = response.getReturnValue();
                var defaultVal;// = cmp.get('v.defaultValue');
                var retRes ;
                if(result != null) {
                    retRes = result.pickList;
                    defaultVal = result.defaultValue;
                }
                if(defaultVal == undefined)
                    defaultVal = cmp.get('v.defaultValue');
                
                var j=0;
                
                var selectedVal = false;
                var isMultiselect = cmp.get("v.isMultiselect");
                //console.log("v.defaultValue: "+defaultVal);
                                
                //Marketing rep processing ; CLWDefaultPicklist putting it prefix if it matches with deafult user
                var DefaultNeededFlag = true;
                for(var key in retRes){
                    // if condition matches 'CLWDefaultPicklist-' then DefaultNeededFlag = false
                    if(key.includes("CLWDefaultPicklist-")){
                        DefaultNeededFlag=false;
                    }
                }
                
                var additionalValues = cmp.get("v.additionalValues");
                if(additionalValues != undefined && additionalValues != '' && additionalValues != null){
                    var additionalValues = additionalValues.split(';')
                    for(let key in additionalValues){
                        if(additionalValues[key] != undefined && additionalValues[key] != ''){
                            if(defaultVal === additionalValues[key]){
                                opts[j++] = {class: "optionClass", label: additionalValues[key], value: additionalValues[key], selected: true};
                            }
                            else{
                                opts[j++] = {class: "optionClass", label: additionalValues[key], value: additionalValues[key]};
                            }
                        }
                    }
                }
                
                if(!isMultiselect){
                    if(DefaultNeededFlag && (defaultVal=="NONE" || defaultVal=='')) {
                        opts[j++] = {class: "optionClass", label:"--- None ---", value:""};
                    } 
                    else {
                        cmp.set("v.returnValue", defaultVal);  //setting api value
                    }
                }
                
                var newKey;
                for (var key in retRes){
                    selectedVal = false;  //reseting it to false
                    if(defaultVal==retRes[key]) selectedVal = true;
                    else if(!DefaultNeededFlag || defaultVal==null || defaultVal==""){  //done in  comp Explicetly
                        if(key.includes("CLWDefaultPicklist-")){
                            selectedVal = true;
                            cmp.set("v.returnValue",retRes[key]);  //setting api value 
                        }
                    }
                        else selectedVal = false;
                    newKey = key.replace('CLWDefaultPicklist-','');
                    
                    opts[j++] = { class: "optionClass", label: newKey, value: retRes[key], selected: selectedVal };
                }
                cmp.find("InputSelectDynamic").set("v.options", opts);
            }
            else if (state === "INCOMPLETE") {
                // do something
                console.log("Error message: do something" );
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log("Unknown error ",errors);
                    if (errors) {
                        if (errors[0] && errors[0].message && errors[0].message.includes('No Permission')) {
                            console.log("Error message: " + errors[0].message);
                            cmp.set('v.isFieldPermission' , false);
                        } else {
                            this.handleErrors(errors);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        console.log("DynamicPicklistController.doInit(-)==>");
        
    },
    
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('error '+ toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('error '+ toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('error '+ toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})