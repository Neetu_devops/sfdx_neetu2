({
    doInit : function(cmp, event, helper) {
        //This method is used to get picklist based on record type with default value
        //If no default is set then default will be first value or default value which is passed in the picklist
        if(cmp.get("v.isPickListWithDefault") == true) {
        	helper.getPicklistWithDefault(cmp);
            return;
        }
        
        var action = cmp.get("c.getDynPickList");
        action.setParams({
            'objectName' : cmp.get('v.objectName'),
            'fieldName' : cmp.get('v.fieldName'),
            'recordTypeName' : cmp.get('v.recordTypeName'),
            'recordTypeLabelName' : cmp.get('v.recordTypeLabelName')
        });      
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(cmp.get("v.fieldName"),"response: ",response.getReturnValue());
                
                var opts=[];
                var retRes = response.getReturnValue();
                var j=0;
                var defaultVal = cmp.get('v.defaultValue');
                var selectedVal = false;
                var isMultiselect = cmp.get("v.isMultiselect");
                //console.log("v.defaultValue: "+defaultVal);
                
                //checking default value in the response, if not present set default to first value
                var defaultExists = false;
                if(defaultVal != undefined && defaultVal != '' && defaultVal != 'NONE' && retRes != null) {
                    var firstKey = null;
                    for (var key in retRes){
                        if(defaultVal == retRes[key]) 
                            defaultExists = true;
                        if(firstKey == null)
                            firstKey = retRes[key];
                    }
                    if(defaultExists == false && firstKey != null)
                        defaultVal = firstKey;
                }

                //Marketing rep processing ; CLWDefaultPicklist putting it prefix if it matches with deafult user
                var DefaultNeededFlag = true;
                for(var key in retRes){
                    // if condition matches 'CLWDefaultPicklist-' then DefaultNeededFlag = false
                    if(key.includes("CLWDefaultPicklist-")){
                        DefaultNeededFlag=false;
                    }
                }
                
                //Adding in all the additional values passed in via additionalValues attribute. if not no impact
                var additionalValues = cmp.get("v.additionalValues");
                if(additionalValues != undefined && additionalValues != '' && additionalValues != null){
                    var additionalValues = additionalValues.split(';')
                    for(let key in additionalValues){
                        if(additionalValues[key] != undefined && additionalValues[key] != ''){
                            if (cmp.get("v.defaultValue") === additionalValues[key]){
                                defaultVal = additionalValues[key];
                                opts[j++] = {class: "optionClass", label: additionalValues[key], value: additionalValues[key], selected: true};
                            }
                            else{
                                opts[j++] = {class: "optionClass", label: additionalValues[key], value: additionalValues[key]};
                            }
                        }
                    }
                }
                
                if(!isMultiselect){
                    if(DefaultNeededFlag && (defaultVal=="NONE" || defaultVal=='')) {
                        opts[j++] = {class: "optionClass", label:"--- None ---", value:""};
                    } 
                    else {
                        cmp.set("v.returnValue", defaultVal);  //setting api value
                    }
                }
                
                var newKey;
                for (var key in retRes){
                    selectedVal = false;  //reseting it to false
                    if(defaultVal==retRes[key]) selectedVal = true;
                    else if(!DefaultNeededFlag || defaultVal==null || defaultVal==""){  //done in  comp Explicetly
                        if(key.includes("CLWDefaultPicklist-")){
                            selectedVal = true;
                            cmp.set("v.returnValue",retRes[key]);  //setting api value 
                        }
                    }
                    else selectedVal = false;
                    newKey = key.replace('CLWDefaultPicklist-','');
                    
                    opts[j++] = { class: "optionClass", label: newKey, value: retRes[key], selected: selectedVal };
                }
                cmp.find("InputSelectDynamic").set("v.options", opts);
            }
            else if (state === "INCOMPLETE") {
                // do something
                console.log("Error message: do something" );
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log("Unknown error ",errors);
                    if (errors) {
                        if (errors[0] && errors[0].message && errors[0].message.includes('No Permission')) {
                            console.log("Error message: " + errors[0].message);
                            cmp.set('v.isFieldPermission' , false);
                        } else {
                            helper.handleErrors(errors);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        console.log("DynamicPicklistController.doInit(-)==>");
    },
    
    onSelectChange : function(component, event, helper) {
        var selected = component.find("InputSelectDynamic").get("v.value");        
        console.log('DynamicPicklistController.onSelectChange value: ', selected);
        component.set("v.returnValue", selected);
        
        let compEvent = component.getEvent("DynamicPicklistResponseEvent");
        compEvent.setParams({
            "response" : component.get("v.fieldName")
        });
        compEvent.fire();
    }
})