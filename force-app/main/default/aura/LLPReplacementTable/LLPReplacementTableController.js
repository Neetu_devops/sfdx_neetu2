({
    doInit : function(cmp, event, helper) {
        helper.fetchInitData(cmp, event, helper);
    },
    getSelectedRecords: function(cmp, event, helper) {
        var allRecords = cmp.get("v.tableData");
        function LLP(Id,LLP_Cost) {
            this.Id = Id;
            this.LLP_Cost = LLP_Cost;
        }
        
        var totat_LLP_Cost = 0;
        var LLPCost_Error = false;
        var isReplaced_Error = false;
        var selectedLLPs = [];
        
        for(let key in allRecords){
            if(allRecords[key].isReplaced === true){
                if(allRecords[key].LLP_Cost != null && allRecords[key].LLP_Cost != ''){
                    selectedLLPs.push(new LLP(allRecords[key].llpRecord.Id, allRecords[key].LLP_Cost));
                    totat_LLP_Cost += parseInt(allRecords[key].LLP_Cost);
                }
                else{
                    LLPCost_Error = true;
                    break;
                }
            } else if(allRecords[key].isReplaced === false){
            	if(allRecords[key].LLP_Cost != undefined && allRecords[key].LLP_Cost != ''){
                    isReplaced_Error = true;
                    break;
                }
            }
        }
        cmp.set("v.total_LLP_Cost",totat_LLP_Cost);
        
        if(LLPCost_Error == true){
            helper.handleErrors('Please fill the "LLP Cost" field for the selected records.', "Warning");
        } else if(isReplaced_Error == true){
            helper.handleErrors('Please check the "Is Replaced?" checkbox for the selected records.', "Warning");
        } else if(selectedLLPs.length === 0){
            helper.handleErrors("Please select atleast one record to continue.", "Warning");
        } else{
            cmp.set("v.selectedLLPs",JSON.stringify(selectedLLPs));
        }
    },
    closeModal : function(component, event, helper){
    	component.set("v.closeLLPReplacementTableModal",true);    
    },
    moreLLPSelectedOrNot : function(component, event, helper){
        let tableData = component.get("v.tableData");
        console.log("tableData: ", tableData);
        let updatedNoOfSelectedLLPs = 0;
        let initialNoOfSelectedLLPs = component.get("v.initialNoOfSelectedLLPs");
        for(let key in tableData){
            if(tableData[key].isReplaced == true && tableData[key].LLP_Cost != undefined && tableData[key].LLP_Cost != ''){
            	updatedNoOfSelectedLLPs++;	    
            }else if((tableData[key].isReplaced == true && (tableData[key].LLP_Cost == undefined || tableData[key].LLP_Cost == '')) || tableData[key].LLP_Cost != undefined && tableData[key].LLP_Cost != '' && tableData[key].isReplaced == false){
                updatedNoOfSelectedLLPs = initialNoOfSelectedLLPs;
                break;
                
            }
        }
        console.log("initialNoOfSelectedLLPs: ",component.get("v.initialNoOfSelectedLLPs"));
        console.log("updatedNoOfSelectedLLPs: ",updatedNoOfSelectedLLPs);
        
        if(updatedNoOfSelectedLLPs != initialNoOfSelectedLLPs){
            component.set("v.disableSubmitBtn",false);
        } else{
            component.set("v.disableSubmitBtn",true);
        }
    }
})