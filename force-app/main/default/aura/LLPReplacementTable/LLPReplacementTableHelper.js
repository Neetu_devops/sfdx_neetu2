({
    fetchInitData : function(cmp,event) {
        this.showSpinner(cmp);
        let selectedLLPs = cmp.get("v.selectedLLPs");
        
        let action = cmp.get("c.getrecords");
        action.setParams({
            recordId: cmp.get("v.recordId"),
            engineType: cmp.get("v.engineType")
        });
        action.setCallback(this,function(response){
            const state = response.getState();
            if(state == "SUCCESS"){
                let resData = response.getReturnValue();
                let engineType = cmp.get("v.engineType");
                
                console.log('selectedLLPs: ',selectedLLPs);
                if(selectedLLPs !== undefined && selectedLLPs !== '' && selectedLLPs !== null && selectedLLPs.length !== 0){
                    selectedLLPs = JSON.parse(selectedLLPs); 
                    let initialNoOfSelectedLLPs = 0;
                    for(let key in resData){
                        for(let key2 in selectedLLPs){
                            if(selectedLLPs[key2].Id === resData[key].llpRecord.Id){
                                resData[key].isReplaced = true;
                                resData[key].LLP_Cost = selectedLLPs[key2].LLP_Cost;
                                initialNoOfSelectedLLPs++;
                            } 
                        }
                    }
                    cmp.set("v.initialNoOfSelectedLLPs",initialNoOfSelectedLLPs);
                    console.log('initialNoOfSelectedLLPs: ', initialNoOfSelectedLLPs);
                }
                cmp.set("v.tableData",resData);
            }else if(state == "ERROR"){
                this.handleErrors(response.getError());
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
    },
    showSpinner: function(cmp){
        const spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function(cmp){
        const spinner = cmp.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    handleErrors : function(errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error": type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error": type,
            duration: '7000'
        };
        if(typeof errors === 'object') {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });  
                    };
                }
            });
        }
        else if(typeof errors === "string" && errors.length != 0){
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})