({
    highcharts : function (component, event, name, container, axisValues, colorChart) {
        setTimeout($A.getCallback(function() {
            var sparkline = {
                chart: {
                    renderTo: container,
                    backgroundColor: null,
                    borderWidth: 0,
                    type: '',
                    margin: [2, 0, 2, 0],
                    width: 120,
                    height: 20,
                    style: {
                        overflow: 'visible !important'
                    },
                    skipClone: true, // small optimalization, saves 1-2 ms each sparkline
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    },
                    startOnTick: false,
                    endOnTick: false,
                    tickPositions: []
                },
                yAxis: {
                    endOnTick: false,
                    startOnTick: false,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    },
                    tickPositions: [0]
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        animation: false,
                        lineWidth: 1,
                        shadow: false,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        marker: {
                            radius: 1,
                            states: {
                                hover: {
                                    radius: 2
                                }
                            }
                        },
                        fillOpacity: 0.25
                    },
                    column: {
                        negativeColor: '#910000',
                        borderColor: 'silver'
                    }
                },
                series: [{
                    name: name,
                    data: axisValues,
                    color: colorChart
                }],
                tooltip: {
                    hideDelay: 0,
                    outside: true,
                    shared: true,
                    headerFormat: '<span style="font-size: 9px">{point.x}:</span>',
                    pointFormat: '<b>{point.y}</b>'
                }
            };
            new Highcharts.chart(container, sparkline);
        }), 1000);
    }
})