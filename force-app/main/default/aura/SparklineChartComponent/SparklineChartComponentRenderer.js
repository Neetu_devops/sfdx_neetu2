({
    // Your renderer method overrides go here
    afterRender: function (component, helper) {
        var afterRend = this.superAfterRender();
        var fhData = component.get("v.fh_data");
        var count = 0;        
        
        for(var i=((fhData.length) -1); i>=((fhData.length) - 4); i--) {
            if(fhData[i] < fhData[i - 1]) {
                count++;
            }
        }       
        
        if(count < 4) {   
            component.set("v.showTooltip", false);
            helper.highcharts(component, event, 'FH', component.get("v.container"), fhData, 'rgb(124, 181, 236)');
        }
        else {
            component.set("v.showTooltip", true);
            helper.highcharts(component, event, 'FH', component.get("v.container"), fhData, 'red');
        }
        
        return afterRend;
    }
})