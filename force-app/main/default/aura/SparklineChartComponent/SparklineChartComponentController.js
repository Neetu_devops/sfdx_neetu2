({
	handleOnClick : function(component, event, helper) {
        $A.util.toggleClass(component.find("help"), 'slds-fall-into-ground');
    },
    
    
    handleMouseLeave : function(component, event, helper) {
        $A.util.addClass(component.find("help"), 'slds-fall-into-ground');
    },
    
    
    handleMouseEnter : function(component, event, helper) {
        $A.util.removeClass(component.find("help"), 'slds-fall-into-ground');
    }
})