({
    doInit:  function(component, event, helper){
        var recordId =  component.get("v.recordId");
        var action = component.get("c.validateFunc");
        helper.hideSpinner(component, event, helper);
        action.setParams({
            "utilizationId": recordId});
        action.setCallback(this, function(response) {
          	var state = response.getState();
			if (response.getState() == "SUCCESS") {
                
                var message = response.getReturnValue();
                if(response.getReturnValue() == 'UnApprove'){
                     
                    message = 'Are you sure you want to Unapprove?';
                    component.set("v.unApproveFlag", true);
                }
                component.set("v.message", message);
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(component,errors);
            }
            
            helper.hideSpinner(component, event, helper);
        });
        $A.enqueueAction(action); 
    },
    unApprove: function(component, event, helper) {
        var answer = event.currentTarget.dataset.answer;
        
        if (answer == "Yes") {
           
             helper.showSpinner(component, event, helper);
             var recordId =  component.get("v.recordId");
			 var action = component.get("c.unapproveAction");
             action.setParams({
            "utilizationId": recordId});
            
            action.setCallback(this, function(response) {
                helper.hideSpinner(component, event, helper);
                var state = response.getState();
               
                if (response.getState() == "SUCCESS") {
                     helper.renderToast(response.getReturnValue());
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    helper.handleErrors(component,errors);
                }
            });
            $A.enqueueAction(action);
        } else {
            $A.get("e.force:closeQuickAction").fire();
        }
    }
});