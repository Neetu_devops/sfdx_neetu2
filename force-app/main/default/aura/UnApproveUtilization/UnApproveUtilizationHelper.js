({
    showSpinner: function (component, event, helper) {
       var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    renderToast: function(returnMessage) {
    var resultsToast = $A.get("e.force:showToast");
    if (returnMessage === "SUCCESS") {
      resultsToast.setParams({
        type: "success",
        message: "Utilization is unapproved"
      });
    } else {
      resultsToast.setParams({
        type: "error",
        message:
          returnMessage
      });
    }
    resultsToast.fire();
  },
  
    handleErrors : function(cmp,errors) {
        // Configure error toast
        let errorVal = '';
        let errorPopup =  cmp.find("errorM");
        $A.util.removeClass(errorPopup, "slds-hide");
        
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    errorVal = error.message;
                    
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        errorVal = pageError.message;
                        
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            errorVal = errorList.message;
                            
                        });  
                    };
                }
            });
        }
        
        cmp.set("v.errorMsg",errorVal);
    },
})