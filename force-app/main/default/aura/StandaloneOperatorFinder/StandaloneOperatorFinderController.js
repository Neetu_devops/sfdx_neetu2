({
    addSelected : function(component, event, helper) {
        component.set("v.showSpinner", true);
        
        var action = component.get("c.createDeals");
        action.setParams({
            'selectedOperators' : component.get("v.selectedOperators")
        });
        action.setCallback(this, function(response) {                   
            var state = response.getState();
            
            if(state === "SUCCESS") {
                component.set("v.selectedOperators", []);
                component.set("v.operatorRecordsList", undefined);
                component.set("v.showSpinner", false);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Deals created with selected Operators successfully',
                    type: 'success'
                });
                toastEvent.fire();  
                
                if(response.getReturnValue() == 'Success') {
                    component.find('operatorFinder').reload();
                }
                else {
                    var url = '/' + response.getReturnValue();
                    window.open(url, '_self');
                }
            }
        });
        
        $A.enqueueAction(action);
    }
})