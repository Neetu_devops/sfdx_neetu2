({
    
	initialize : function(component, event, helper) {
		//var clId = component.get("v.custILId");
		console.log("AddmultipleContacts initialize(+)==>");
        var action = component.get("c.getOperatorId");
        action.setParams({
            'recId' : component.get('v.custILId'),
        });        
        action.setCallback(this,function(a){
        	console.log("Inside setCallback");
        	console.log(a.getState());
            if(a.getState()=="SUCCESS")	{
                var con = a.getReturnValue();
                console.log(con["operatorId"]);
                component.set("v.operatorId",con["operatorId"]);
                component.set("v.operatorName",con["operatorName"]);
            }
        });
        
        $A.enqueueAction(action);
        console.log("AddmultipleContacts initialize(-)==>");
    },    
	callSave : function(component, event, helper) {
        console.log("AddmultipleContacts callSave(+)==>");
        var recId = component.get("v.custILId");
        var lookupIds = [];
        	lookupIds[0] = component.get("v.id1");
        	lookupIds[1] = component.get("v.id2");
        	lookupIds[2] = component.get("v.id3");
        	lookupIds[3] = component.get("v.id4");
        	lookupIds[4] = component.get("v.id5");
        	lookupIds[5] = component.get("v.id6");
        	lookupIds[6] = component.get("v.id7");
        	lookupIds[7] = component.get("v.id8");
        	lookupIds[8] = component.get("v.id9");
        	lookupIds[9] = component.get("v.id10");        
        var action = component.get("c.callSaveApex");
        action.setParams({ recId : recId,idsList:lookupIds });
        action.setCallback(this,function(a){
			if(a.error && a.error.length){
            	return $A.error('Unexpected error callSave: '+a.error[0].message);
            } 
            helper.helperCloseMethod(recId);
        });
        $A.enqueueAction(action);
        console.log("AddmultipleContacts callSave(-)==>");

	},
	callCancel : function(component, event, helper) {
        console.log("callCancel======================>");
        var recId = component.get("v.custILId");
		helper.helperCloseMethod(recId);       
        
	}    
})