({
	helperCloseMethod : function(canvasIdVar) {

                        if( (typeof sforce != 'undefined') && (sforce != null) ) {
                            // do your SF1 mobile button stuff
                            sforce.one.back(true);
                        }
                        else {
                            // do your desktop stuff
                            //window.close();
                            if(canvasIdVar!=null){
                            	var finalURL = window.location.protocol +"//"+window.location.hostname+"/apex/RedirectVF?paramId="+ canvasIdVar ;
                            	window.open(finalURL ,"_self");
                            }
                            else{
                                window.history.back();
                            }
                        }         
        
	}
})