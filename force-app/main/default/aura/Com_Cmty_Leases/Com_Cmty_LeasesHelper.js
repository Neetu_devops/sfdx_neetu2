({

    setColumns: function(cmp) {
        // initialWidth: 250;
         var columns = [
            { label: 'Lease No', fieldName: 'title', sortable: true },
             { label: 'Lessee Name', fieldName: 'lesseeName', sortable: true },
            { label: 'Operator Name', fieldName: 'operatorName', sortable: true },
            { label: 'Type', fieldName: 'type', sortable: true },
            { label: 'MSN/ESN', fieldName: 'msnURL',  type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'msn' } }, sortable: true },
            { label: 'Lease Start', fieldName: 'leaseStart', type: 'date', sortable: true },
            { label: 'Lease End', fieldName: 'leaseEnd', type: 'date', sortable: true },
            { label: 'Greentime Lease', fieldName: 'greentimeLease', type: 'boolean', sortable: true },
            { label: 'Status', fieldName: 'status', sortable: true },
            { label: 'Action', fieldName: 'submitUR_URL', type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'submitURTitle' } }},
        ];
         
        cmp.set('v.columns', columns);
    }
 
})