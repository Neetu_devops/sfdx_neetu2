({
    init: function(cmp, event, helper) {
        
        helper.setColumns(cmp);
        
        //set Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title" : "All Current Leases"});
        appEvent.fire();
        
        cmp.set("v.showSpinner", true);
        
        var action = cmp.get("c.getCurrentLeases");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data =  response.getReturnValue();
                cmp.set("v.data", data);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var cmpEvent = cmp.getEvent("ErrorsEvent");
                    cmpEvent.setParams( { "errors" : errors } );
                    cmpEvent.fire();
                } else {
                    console.log("Unknown error");
                }
            }
            cmp.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);

    },
    
    goBack: function(cmp, event, helper) {
        cmp.set("v.showLeases", false);
        
        //Unset Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title" : "Fleet Planning Home"});
        appEvent.fire();
    }
    
})