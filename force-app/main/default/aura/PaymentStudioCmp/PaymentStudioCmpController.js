({
    doInit: function (component, event, helper) {
        helper.loadInitData(component, event);
    },

    handlePageReload: function (component, event, helper) {
        component.set("v.isPaymentReceiptSaved", false);
        component.set("v.searchInvoiceName", undefined);
        component.set("v.selectedInvoiceType", "All");
        component.set("v.selectedFilter", '');
        component.set("v.allocatedInvoiceData", new Array());
        component.set("v.unallocatedInvoiceData", new Array());
        component.set("v.totalPaidAmount", 0.00);
        component.set("v.totalBankCharges", 0.00);
        component.set("v.relatedLegalEntityIds", undefined);
        component.set("v.isLOCSelected", false);
        component.set("v.selectedLetterOfCredit", '');
        component.set("v.selectedPaymentSourceLessee", '');
        component.set("v.locDrawDownId", '');
        component.set("v.selectedEventInvoice", '');
        component.set("v.selectedSDPayableMemo", '');
        component.set("v.paymentSource", "Cash");
        helper.loadAmountReceivedChange(component, event, helper, 0);

        helper.loadInitData(component, event);
    },

    searchInvoiceRecords: function (component, event, helper) {
        let unallocatedData = component.get("v.unallocatedInvoiceData");

        if (helper.checkUndefined(unallocatedData) || unallocatedData.length === 0) {
            helper.fetchInvoiceRecords(component, event);
        }
        else if (!helper.checkUndefined(unallocatedData) && unallocatedData.length != 0) {
            let data = unallocatedData.filter(function (data, index) {
                if (parseFloat(data.amountPaid) != 0 || parseFloat(data.bankCharges) != 0) { return data; }
            });
            if (!helper.checkUndefined(data) && data.length != 0) {
                document.getElementById("clearSearchDataFrontdrop").style.display = "block";
                document.getElementById("clearSearchDataBackdrop").style.display = "block";
            }
            else {
                helper.fetchInvoiceRecords(component, event);
            }
        }
    },

    closeClearSearchPopup: function (component, event, helper) {
        document.getElementById("clearSearchDataFrontdrop").style.display = "none";
        document.getElementById("clearSearchDataBackdrop").style.display = "none";
    },

    proceedAndSearchHandler: function (component, event, helper) {
        document.getElementById("clearSearchDataFrontdrop").style.display = "none";
        document.getElementById("clearSearchDataBackdrop").style.display = "none";
        helper.fetchInvoiceRecords(component, event);
    },

    onAmountReceivedChange: function (component, event, helper) {
        helper.loadAmountReceivedChange(component, event, helper, event.getSource().get("v.value"));
    },

    onInvoicePaidValueChange: function (component, event, helper) {
        let recordIndex = parseInt(event.getSource().get("v.name"));
        let amountPaid = event.getSource().get("v.value");
        let unallocatedData = component.get("v.unallocatedInvoiceData");

        if (!helper.checkUndefined(amountPaid) && amountPaid !== 0) { amountPaid = amountPaid.replace(/,/g, ''); }
        if (helper.checkUndefined(amountPaid)) { amountPaid = 0; }

        for (let key in unallocatedData) {
            if (key == recordIndex && !helper.checkUndefined(unallocatedData[recordIndex])) {
                let bankCharges = unallocatedData[recordIndex].bankCharges;
                if (!helper.checkUndefined(amountPaid)) {
                    unallocatedData[recordIndex].amountRemaining = parseFloat(unallocatedData[recordIndex].invoiceRecord.Balance_Due__c) - parseFloat(amountPaid);
                }
                else {
                    unallocatedData[recordIndex].amountRemaining = parseFloat(unallocatedData[recordIndex].invoiceRecord.Balance_Due__c);
                }

                if (!helper.checkUndefined(unallocatedData[recordIndex].amountRemaining)) {
                    if (unallocatedData[recordIndex].amountRemaining >= 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else if (unallocatedData[recordIndex].amountRemaining === 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else {
                        unallocatedData[recordIndex].isOverPayed = true;
                        helper.handleErrors("'Amount Paid' exceeds the amount due on the selected invoice.", "Warning");
                    }
                }
                unallocatedData[recordIndex].amountPaid = amountPaid;
                unallocatedData[recordIndex].bankCharges = bankCharges;
            }
        }
        component.set("v.unallocatedInvoiceData", unallocatedData);
    },

    onInvoiceBankChargesValueChange: function (component, event, helper) {
        let recordIndex = parseInt(event.getSource().get("v.name"));
        let bankCharges = event.getSource().get("v.value");
        let unallocatedData = component.get("v.unallocatedInvoiceData");

        if (!helper.checkUndefined(bankCharges) && bankCharges !== 0) { bankCharges = bankCharges.replace(/,/g, ''); }
        if (helper.checkUndefined(bankCharges)) { bankCharges = 0; }

        for (let key in unallocatedData) {
            if (key == recordIndex && !helper.checkUndefined(unallocatedData[recordIndex])) {
                let amountPaid = unallocatedData[recordIndex].amountPaid;
                if (!helper.checkUndefined(amountPaid)) {
                    unallocatedData[recordIndex].amountRemaining = parseFloat(unallocatedData[recordIndex].invoiceRecord.Balance_Due__c) - parseFloat(amountPaid);
                }
                else {
                    unallocatedData[recordIndex].amountRemaining = parseFloat(unallocatedData[recordIndex].invoiceRecord.Balance_Due__c);
                }

                if (!helper.checkUndefined(unallocatedData[recordIndex].amountRemaining)) {
                    if (unallocatedData[recordIndex].amountRemaining >= 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else if (unallocatedData[recordIndex].amountRemaining === 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else {
                        unallocatedData[recordIndex].isOverPayed = true;
                        helper.handleErrors("'Amount Paid' exceeds the amount due on the selected invoice.", "Warning");
                    }
                }
                unallocatedData[recordIndex].amountPaid = amountPaid;
                unallocatedData[recordIndex].bankCharges = bankCharges;
            }
        }
        component.set("v.unallocatedInvoiceData", unallocatedData);
    },

    allocateInvoiceHandler: function (component, event, helper) {
        let recordIndex = parseInt(event.target.dataset.recindex);
        let unallocatedData = component.get("v.unallocatedInvoiceData");
        let allocatedData = component.get("v.allocatedInvoiceData");
        let paymentReceiptData = component.get("v.paymentReceiptData");
        let totalPaidAmount = component.get("v.totalPaidAmount");
        let totalBankCharges = component.get("v.totalBankCharges");
        let showErrorAlert = false;
        let showBAWarning = false;
        let showAmountWarning = false;

        if (helper.checkUndefined(allocatedData)) { allocatedData = []; }
        if (helper.checkUndefined(unallocatedData)) { unallocatedData = []; }

        for (let key in unallocatedData) {
            if (key == recordIndex && !helper.checkUndefined(unallocatedData[recordIndex])) {
                if (!helper.checkUndefined(unallocatedData[recordIndex].amountRemaining)) {
                    if (parseFloat(unallocatedData[recordIndex].amountRemaining) >= 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else if (unallocatedData[recordIndex].amountRemaining === 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else {
                        unallocatedData[recordIndex].isOverPayed = true;
                        showErrorAlert = true;
                        break;
                    }
                }
                if ((helper.checkUndefined(unallocatedData[recordIndex].amountPaid) || unallocatedData[recordIndex].amountPaid === 0) && (helper.checkUndefined(unallocatedData[recordIndex].bankCharges) || unallocatedData[recordIndex].bankCharges === 0)) {
                    showAmountWarning = true;
                    break;
                }
                if (unallocatedData[recordIndex].isLeaseBankACRequired && !unallocatedData[recordIndex].isLeaseBankAC) {
                    if (helper.checkUndefined(unallocatedData[recordIndex].updatedLeaseBankAC) || helper.checkUndefined(unallocatedData[recordIndex].updatedLeaseBankAC.Id)) {
                        showBAWarning = true;
                        break;
                    }
                }
                unallocatedData[recordIndex].isAllocated = true;

                paymentReceiptData.paymentAmountToAllocate = parseFloat(paymentReceiptData.paymentAmountToAllocate) - (!helper.checkUndefined(unallocatedData[recordIndex].amountPaid) ? parseFloat(unallocatedData[recordIndex].amountPaid) : 0);
                paymentReceiptData.paymentAmountToAllocate = paymentReceiptData.paymentAmountToAllocate.toFixed(2);

                totalPaidAmount = parseFloat(totalPaidAmount) + (!helper.checkUndefined(unallocatedData[recordIndex].amountPaid) ? parseFloat(unallocatedData[recordIndex].amountPaid) : 0);

                totalBankCharges = parseFloat(totalBankCharges) + (!helper.checkUndefined(unallocatedData[recordIndex].bankCharges) ? parseFloat(unallocatedData[recordIndex].bankCharges) : 0);

                allocatedData.push(unallocatedData[recordIndex]);
                unallocatedData.splice(key, 1);
            }
        }
        component.set("v.unallocatedInvoiceData", unallocatedData);

        if (showErrorAlert) {
            document.getElementById("AllocationErrorAlertFront").style.display = "block";
            document.getElementById("AllocationErrorAlertBack").style.display = "block";
        }
        else if (showBAWarning) {
            helper.handleErrors("Please select a Bank Account to proceed with the allocation.", "Warning");
        }
        else if (showAmountWarning) {
            helper.handleErrors("An invoice with a $0 payment amount is entered. Please enter a payment amount to continue with the allocation.", "Warning");
        }
        else {
            component.set("v.allocatedInvoiceData", allocatedData);
            component.set("v.paymentReceiptData", paymentReceiptData);
            component.set("v.totalPaidAmount", totalPaidAmount);
            component.set("v.totalBankCharges", totalBankCharges);
        }
    },

    unallocateInvoiceHandler: function (component, event, helper) {
        let recordIndex = parseInt(event.target.dataset.recindex);
        let unallocatedData = component.get("v.unallocatedInvoiceData");
        let allocatedData = component.get("v.allocatedInvoiceData");
        let paymentReceiptData = component.get("v.paymentReceiptData");
        let totalPaidAmount = component.get("v.totalPaidAmount");
        let totalBankCharges = component.get("v.totalBankCharges");

        if (helper.checkUndefined(unallocatedData)) { unallocatedData = []; }
        if (helper.checkUndefined(allocatedData)) { allocatedData = []; }

        for (let key in allocatedData) {
            if (key == recordIndex && !helper.checkUndefined(allocatedData[recordIndex])) {
                allocatedData[recordIndex].isAllocated = false;

                paymentReceiptData.paymentAmountToAllocate = parseFloat(paymentReceiptData.paymentAmountToAllocate) + (!helper.checkUndefined(allocatedData[recordIndex].amountPaid) ? parseFloat(allocatedData[recordIndex].amountPaid) : 0);
                paymentReceiptData.paymentAmountToAllocate = paymentReceiptData.paymentAmountToAllocate.toFixed(2);

                totalPaidAmount = parseFloat(totalPaidAmount) - (!helper.checkUndefined(allocatedData[recordIndex].amountPaid) ? parseFloat(allocatedData[recordIndex].amountPaid) : 0);

                totalBankCharges = parseFloat(totalBankCharges) - (!helper.checkUndefined(allocatedData[recordIndex].bankCharges) ? parseFloat(allocatedData[recordIndex].bankCharges) : 0);

                if (!helper.checkUndefined(allocatedData[recordIndex].amountRemaining)) {
                    if (parseFloat(allocatedData[recordIndex].amountRemaining) >= 0) {
                        allocatedData[recordIndex].isOverPayed = false;
                    }
                    else if (allocatedData[recordIndex].amountRemaining === 0) {
                        allocatedData[recordIndex].isOverPayed = false;
                    }
                    else {
                        allocatedData[recordIndex].isOverPayed = true;
                    }
                }
                unallocatedData.push(allocatedData[recordIndex]);
                allocatedData.splice(key, 1);
            }
        }
        component.set("v.allocatedInvoiceData", allocatedData);
        component.set("v.unallocatedInvoiceData", unallocatedData);
        component.set("v.paymentReceiptData", paymentReceiptData);
        component.set("v.totalPaidAmount", totalPaidAmount);
        component.set("v.totalBankCharges", totalBankCharges);
    },

    loadCompleteAmount: function (component, event, helper) {
        let recordIndex = event.target.getAttribute("data-index");
        let unallocatedData = component.get("v.unallocatedInvoiceData");

        let amountPaid = 0;
        for (let key in unallocatedData) {
            if (key == recordIndex && !helper.checkUndefined(unallocatedData[recordIndex])) {
                amountPaid = unallocatedData[recordIndex].invoiceRecord.Balance_Due__c;

                let bankCharges = unallocatedData[recordIndex].bankCharges;
                if (!helper.checkUndefined(amountPaid)) {
                    unallocatedData[recordIndex].amountRemaining = parseFloat(unallocatedData[recordIndex].invoiceRecord.Balance_Due__c) - parseFloat(amountPaid);
                }
                else {
                    unallocatedData[recordIndex].amountRemaining = parseFloat(unallocatedData[recordIndex].invoiceRecord.Balance_Due__c);
                }

                if (!helper.checkUndefined(unallocatedData[recordIndex].amountRemaining)) {
                    if (unallocatedData[recordIndex].amountRemaining >= 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else if (unallocatedData[recordIndex].amountRemaining === 0) {
                        unallocatedData[recordIndex].isOverPayed = false;
                    }
                    else {
                        unallocatedData[recordIndex].isOverPayed = true;
                        helper.handleErrors("'Amount Paid' exceeds the amount due on the selected invoice.", "Warning");
                    }
                }
                unallocatedData[recordIndex].amountPaid = amountPaid;
                unallocatedData[recordIndex].bankCharges = bankCharges;
            }
        }
        component.set("v.unallocatedInvoiceData", unallocatedData);
    },

    paymentReceiptSaveHandler: function (component, event, helper) {
        let paymentReceiptData = component.get("v.paymentReceiptData");
        let isLOCorInventInvoiceSelected = false;

        if (paymentReceiptData.paymentPrePaymentCKB) {
            if (helper.checkUndefined(paymentReceiptData.paymentAmountReceived) || paymentReceiptData.paymentAmountReceived === 0.0) {
                helper.handleErrors("Please enter the 'Payment Amount' to save the prepayment.", "Warning");
            }
            else if (helper.checkUndefined(paymentReceiptData.paymentLessee) || helper.checkUndefined(paymentReceiptData.paymentLessee.Id)) {
                helper.handleErrors("Please select a Lessee to proceed with saving.", "Warning");
            }
            else if (helper.checkUndefined(paymentReceiptData.paymentBankAccount) || helper.checkUndefined(paymentReceiptData.paymentBankAccount.Id)) {
                helper.handleErrors("Please select a Bank Account to proceed with saving.", "Warning");
            }
            else if (!helper.checkUndefined(paymentReceiptData.paymentLease)) {
                if (!helper.checkUndefined(paymentReceiptData.paymentLease.Accounting_Setup_P__c) && (helper.checkUndefined(paymentReceiptData.paymentAccountingPeriod) || helper.checkUndefined(paymentReceiptData.paymentAccountingPeriod.Id))) {
                    helper.handleErrors("Please select an Accounting Period to proceed with saving.", "Warning");
                    return;
                }
                if (paymentReceiptData.paymentBankAccount.BA_Legal_Entity__c === paymentReceiptData.paymentLease.Legal_Entity__c) {
                    document.getElementById("bankAccValMsg").style.display = "none";
                    helper.savePaymentReceiptData(component, event, helper);
                }
                else {
                    document.getElementById("bankAccValMsg").style.display = "block";
                }
            }
            else {
                helper.savePaymentReceiptData(component, event, helper);
            }
        }
        else {
            let paymentSource = component.get("v.paymentSource");
            let selectedEventInvoice = component.get("v.selectedEventInvoice");
            if (!helper.checkUndefined(selectedEventInvoice) && !helper.checkUndefined(selectedEventInvoice.Id)) {
                let availableAmount = (!helper.checkUndefined(component.get("v.availableAmount")) ? component.get("v.availableAmount") : 0.00);
                if (availableAmount < paymentReceiptData.paymentAmountReceived) {
                    helper.handleErrors("The total payment amount is greater than the amount available on the Claim. Please enter a valid amount.", "Error");
                    return;
                }
                isLOCorInventInvoiceSelected = true;
            }

            let selectedLetterOfCredit = component.get("v.selectedLetterOfCredit");
            if (!helper.checkUndefined(selectedLetterOfCredit) && !helper.checkUndefined(selectedLetterOfCredit.Id)) {
                let currentDate = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");

                if (helper.checkUndefined(selectedLetterOfCredit.LC_Coverage_Type__c) ||
                    (selectedLetterOfCredit.LC_Coverage_Type__c != "All Lease Obligation" && selectedLetterOfCredit.LC_Coverage_Type__c != "Security Deposit")) {
                    helper.handleErrors("This LOC can only be used for MR Claims");
                    return;
                }
                if (helper.checkUndefined(selectedLetterOfCredit.Expiry_Date__c) || selectedLetterOfCredit.Expiry_Date__c < currentDate) {
                    helper.handleErrors("This LOC has expired");
                    return;
                }
                if (helper.checkUndefined(component.get("v.locDrawDownId"))) {
                    helper.handleErrors("No active Draw Down Amount found.", "Error");
                    return;
                }
                isLOCorInventInvoiceSelected = true;
            }

            if (paymentSource === "Security Deposit") {
                let selectedSDPayableMemo = component.get("v.selectedSDPayableMemo");
                if (!helper.checkUndefined(selectedSDPayableMemo) && !helper.checkUndefined(selectedSDPayableMemo.Id)) {
                    let availableAmount = (!helper.checkUndefined(component.get("v.availableAmount")) ? component.get("v.availableAmount") : 0.00);
                    if (availableAmount < paymentReceiptData.paymentAmountReceived) {
                        helper.handleErrors("The total 'Payment Amount' is greater than the 'Available Balance' on the Security Deposit. Please enter a payment amount less than or equal to the Security Deposit Available Balance.", "Error");
                        return;
                    }

                    if (!helper.checkUndefined(selectedSDPayableMemo.Lease__c)) {
                        let allocatedInvoices = component.get("v.allocatedInvoiceData");
                        let hasError = false;

                        for (let key in allocatedInvoices) {
                            let invRec = allocatedInvoices[key].invoiceRecord;
                            if (!helper.checkUndefined(invRec.Lease__c) && invRec.Lease__c !== selectedSDPayableMemo.Lease__c) { hasError = true; }
                            else if (!helper.checkUndefined(invRec.Type__c) && invRec.Type__c === 'Security Deposit') { hasError = true; }
                            if (hasError) { break; }
                        }
                        if (hasError) {
                            helper.handleErrors("The Invoice records allocated does not belong to the same lease as of the Security Deposit. Please allocate Invoice belonging to the same lease as of Security Deposit.", "Error");
                            return;
                        }
                    }
                }
                else {
                    helper.handleErrors("Please select a Security Deposit to proceed with saving.", "Error");
                    return;
                }
            }

            if (helper.checkUndefined(paymentReceiptData.paymentAmountReceived) || paymentReceiptData.paymentAmountReceived === 0.0) {
                helper.handleErrors("Please enter the amount received on 'Payment Amount' to record payments on the selected invoices.", "Error");
            }
            else if (!helper.checkUndefined(paymentReceiptData.paymentAmountToAllocate) && parseFloat(paymentReceiptData.paymentAmountToAllocate) > 0) {
                helper.handleErrors("Please allocate the remaining 'Payment Amount' to invoices.", "Error");
            }
            else if (!helper.checkUndefined(paymentReceiptData.paymentAmountToAllocate) && parseFloat(paymentReceiptData.paymentAmountToAllocate) < 0) {
                helper.handleErrors("The allocation amount exceeds the 'Payment Amount'. Please reduce the amount allocated or increase the Payment Amount.", "Error");
            }
            else {
                helper.savePaymentReceiptData(component, event, helper);
            }
        }
    },

    invoiceProcessHandler: function (component, event, helper) {
        let paymentReceiptData = component.get("v.paymentReceiptData");

        if (!helper.checkUndefined(paymentReceiptData.paymentReceiptRecord) && !helper.checkUndefined(paymentReceiptData.paymentReceiptRecord.Id)) {
            let navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": paymentReceiptData.paymentReceiptRecord.Id,
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else {
            let urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/lightning/o/Payment_Receipt__c/list"
            });
            urlEvent.fire();
        }
    },

    closeAllocationErrorAlertPopup: function (component, event, helper) {
        document.getElementById("AllocationErrorAlertFront").style.display = "none";
        document.getElementById("AllocationErrorAlertBack").style.display = "none";
    },

    handleInvoiceAmountClick: function (component, event, helper) {
        component.set("v.selectedInvoiceRecord", '');
        let dataList, selectedInvoice;
        let invoiceRecordData = event.target.dataset.recdata.split('_');
        if (invoiceRecordData[0] === 'Allocated') {
            dataList = component.get("v.allocatedInvoiceData");
        }
        else if (invoiceRecordData[0] === 'UnAllocated') {
            dataList = component.get("v.unallocatedInvoiceData");
        }
        selectedInvoice = dataList.find(record => record.invoiceRecord.Id === invoiceRecordData[1]);
        component.set("v.selectedInvoiceRecord", selectedInvoice.invoiceRecord);

        document.getElementById("invoiceMRBreakDownFront").style.display = "block";
        document.getElementById("invoiceMRBreakDownBack").style.display = "block";
    },

    closeinvoiceMRBreakDownPopup: function (component, event, helper) {
        document.getElementById("invoiceMRBreakDownFront").style.display = "none";
        document.getElementById("invoiceMRBreakDownBack").style.display = "none";
        component.set("v.selectedInvoiceRecord", '');
    },

    handleChangeEventOfLOC: function (component, event, helper) {
        let recordId = event.getParam("value").Id;
        if (!helper.checkUndefined(component.get("v.selectedLetterOfCredit").Id)) {
            component.set("v.isLOCSelected", true);
            component.set("v.selectedPaymentSourceLessee", event.getParam("value").Lessee__c);

            const action = component.get("c.loadPaymentAmount");
            action.setParams({
                recordId: recordId
            });
            action.setCallback(this, function (response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    let objLOCDrawDownAmount = response.getReturnValue();
                    if (objLOCDrawDownAmount.length > 0 && !helper.checkUndefined(objLOCDrawDownAmount[0].Amount__c)) {
                        component.set("v.paymentReceiptData.paymentAmountReceived", objLOCDrawDownAmount[0].Amount__c);
                        component.set("v.availableAmount", parseFloat(objLOCDrawDownAmount[0].Amount__c));

                        if (!helper.checkUndefined(component.get("v.selectedLetterOfCredit").Id)) {
                            component.set("v.locDrawDownId", objLOCDrawDownAmount[0].Id);
                        }
                    }
                    else {
                        component.set("v.paymentReceiptData.paymentAmountReceived", 0);
                        component.set("v.availableAmount", 0.00);
                        component.set("v.locDrawDownId", '');
                    }
                    helper.loadAmountReceivedChange(component, event, helper, component.get("v.paymentReceiptData.paymentAmountReceived"));
                }
                else if (state === "ERROR") {
                    let errors = response.getError();
                    helper.handleErrors(errors);
                }
            });
            $A.enqueueAction(action);
        }
        else {
            component.set("v.isLOCSelected", false);
            component.set("v.selectedPaymentSourceLessee", undefined);
            component.set("v.locDrawDownId", '');
            component.set("v.paymentReceiptData.paymentAmountReceived", 0);
            component.set("v.availableAmount", 0.00);
            helper.loadAmountReceivedChange(component, event, helper, 0);
        }
    },

    handleComponentEventForLease: function (component, event, helper) {
        if (!helper.checkUndefined(component.get("v.selectedFilter").Id)) {
            component.set("v.isLesseeSelected", true);
            component.set("v.selectedLeaseLessee", event.getParam("value").Lessee__c);
            if(!helper.checkUndefined( component.get("v.paymentSource"))){
                console.log('paymentSource: ', component.get("v.paymentSource"));
                if(component.get("v.paymentSource") == 'MR Claims Payable Invoice'){
                    let lessee = event.getParam("value").Lessee__c == null ?event.getParam("value").Operator__c:event.getParam("value").Lessee__c;
                    component.set("v.selectedLeaseLessee",lessee);
                    console.log('lessee: ', lessee);
                }
            }
        } else {
            component.set("v.isLesseeSelected", false);
            component.set("v.selectedLeaseLessee", undefined);
        }
    },

    handleComponentEventForMRClaimEvent: function (component, event, helper) {
        let selectedEventInvoice = component.get("v.selectedEventInvoice");
        if (!helper.checkUndefined(selectedEventInvoice) && !helper.checkUndefined(selectedEventInvoice.Id)) {
            component.set("v.selectedPaymentSourceLessee", event.getParam("value").Lessee__c);

            let availableAmount = (!helper.checkUndefined(selectedEventInvoice.Payable_Balance__c) ? selectedEventInvoice.Payable_Balance__c : 0.00);
            component.set("v.availableAmount", parseFloat(availableAmount));
        } else {
            component.set("v.selectedPaymentSourceLessee", undefined);
            component.set("v.availableAmount", 0.00);
        }
    },

    handleSDPayableMemoEvent: function (component, event, helper) {
        let selectedSDPayableMemo = component.get("v.selectedSDPayableMemo");
        if (!helper.checkUndefined(selectedSDPayableMemo) && !helper.checkUndefined(selectedSDPayableMemo.Id)) {
            component.set("v.selectedPaymentSourceLessee", event.getParam("value").Lease__r.Lessee__c);

            let availableAmount = (!helper.checkUndefined(selectedSDPayableMemo.Security_Depo_Cash__c) ? selectedSDPayableMemo.Security_Depo_Cash__c : 0.00);
            availableAmount = availableAmount - (!helper.checkUndefined(selectedSDPayableMemo.Refund_Committed__c) ? selectedSDPayableMemo.Refund_Committed__c : 0.00);
            component.set("v.availableAmount", parseFloat(availableAmount));
        }
        else {
            component.set("v.selectedPaymentSourceLessee", undefined);
            component.set("v.availableAmount", 0.00);
        }
    },

    handlePaymentDateChange: function (component, event, helper) {
        let paymentReceiptData = component.get("v.paymentReceiptData");
        if (paymentReceiptData.paymentPrePaymentCKB) {
            helper.showSpinner(component);
            const action = component.get("c.updateAccountingPeriodData");
            action.setParams({
                paymentReceipt_s: JSON.stringify(component.get("v.paymentReceiptData"))
            });
            action.setCallback(this, function (response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.paymentReceiptData", response.getReturnValue());
                }
                else if (state === "ERROR") {
                    let errors = response.getError();
                    if (errors) {
                        helper.handleErrors(errors);
                    }
                }
                helper.hideSpinner(component);
            });
            $A.enqueueAction(action);
        }
    },

    handleDefaultBankACChange: function (component, event, helper) {
        let recordIndex = parseInt(event.target.dataset.recindex);
        let unallocatedData = component.get("v.unallocatedInvoiceData");

        for (let key in unallocatedData) {
            if (key == recordIndex && !helper.checkUndefined(unallocatedData[recordIndex])) {
                if (!unallocatedData[recordIndex].isLeaseBankAC) {
                    unallocatedData[recordIndex].updatedLeaseBankAC = unallocatedData[recordIndex].defaultLeaseBankAC;
                }
            }
        }
        component.set("v.unallocatedInvoiceData", unallocatedData);
    },

    paymentSourceChange: function (component, event, helper) {
        let paymentSource = component.get("v.paymentSource");
        console.log('paymentSource: ', paymentSource);

        component.set("v.isLOCSelected", false);
        component.set("v.locDrawDownId", '');
        component.set("v.totalPaidAmount", 0.00);
        component.set("v.totalBankCharges", 0.00);
        component.set("v.availableAmount", 0.00);
        component.set("v.paymentReceiptData.paymentAmountReceived", 0.00);
        helper.loadAmountReceivedChange(component, event, helper, 0);
        component.set("v.selectedFilter", '');
        component.set("v.selectedLeaseLessee", '');
        component.set("v.allocatedInvoiceData", []);
        component.set("v.unallocatedInvoiceData", []);
        component.set("v.selectedLetterOfCredit", '');
        component.set("v.selectedEventInvoice", '');
        component.set("v.selectedSDPayableMemo", '');
        component.set("v.selectedPaymentSourceLessee", '');
    },

    handlePrePaymentLeaseChange: function (component, event, helper) {
        let paymentReceiptData = component.get("v.paymentReceiptData");
        let updateLessee = false;
        if (paymentReceiptData.paymentPrePaymentCKB) {
            if (!helper.checkUndefined(paymentReceiptData.paymentLease) && !helper.checkUndefined(paymentReceiptData.paymentLease.Id)) {
                if (helper.checkUndefined(paymentReceiptData.paymentLessee) || helper.checkUndefined(paymentReceiptData.paymentLessee.Id)) {
                    if (!helper.checkUndefined(paymentReceiptData.paymentLease.Lessee__c)) {
                        paymentReceiptData.paymentLessee = paymentReceiptData.paymentLease.Lessee__r;
                        updateLessee = true;
                    }
                    else if (!helper.checkUndefined(paymentReceiptData.paymentLease.Operator__c)) {
                        paymentReceiptData.paymentLessee = paymentReceiptData.paymentLease.Operator__r;
                        updateLessee = true;
                    }
                    if (updateLessee) {
                        component.set("v.paymentReceiptData", paymentReceiptData);
                    }
                }
            }
        }
    },

    handlePrePaymentLesseeChange: function (component, event, helper) {
        let paymentReceiptData = component.get("v.paymentReceiptData");
        if (paymentReceiptData.paymentPrePaymentCKB) {
            if (!helper.checkUndefined(paymentReceiptData.paymentLessee) && !helper.checkUndefined(paymentReceiptData.paymentLessee.Id)) {
                helper.showSpinner(component);
                const action = component.get("c.fetchRelatedLegalEntities");
                action.setParams({
                    lesseeId: paymentReceiptData.paymentLessee.Id
                });
                action.setCallback(this, function (response) {
                    let state = response.getState();
                    if (state === "SUCCESS") {
                        //console.log("result: ", response.getReturnValue());
                        let result = JSON.parse(response.getReturnValue());
                        let relatedIds = '';
                        if (result != undefined && result != '') {
                            relatedIds = '\'' + result.join('\', \'') + '\'';
                        }
                        component.set("v.relatedLegalEntityIds", relatedIds);
                    }
                    else if (state === "ERROR") {
                        let errors = response.getError();
                        if (errors) {
                            helper.handleErrors(errors);
                        }
                    }
                    helper.hideSpinner(component);
                });
                $A.enqueueAction(action);
            }
            else if (!helper.checkUndefined(paymentReceiptData.paymentLessee) && helper.checkUndefined(paymentReceiptData.paymentLessee.Id)) {
                paymentReceiptData.paymentLessee = undefined;
                paymentReceiptData.paymentLease = undefined;
                paymentReceiptData.paymentBankAccount = undefined;
                document.getElementById("bankAccValMsg").style.display = "none";
                component.set("v.paymentReceiptData", paymentReceiptData);
            }
        }
    },
})