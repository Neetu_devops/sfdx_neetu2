({
    loadInitData: function (component, event, helper) {
        this.showSpinner(component);
        const action = component.get("c.loadInitialLoadData");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                console.log("Response: ", response.getReturnValue());
                component.set("v.paymentReceiptData", response.getReturnValue());

                if (component.get("v.recordId")) {
                    component.set("v.totalPaidAmount", response.getReturnValue().paymentAmountReceived);
                    component.set("v.totalBankCharges", response.getReturnValue().totalBankCharges);
                    component.set("v.allocatedInvoiceData", response.getReturnValue().paymentReceiptInvoiceList);
                }
            }
            else if (state === "ERROR") {
                let errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    loadAmountReceivedChange: function (component, event, helper, amountReceived) {
        let paymentReceiptData = component.get("v.paymentReceiptData");
        let totalPaidAmount = component.get("v.totalPaidAmount").toFixed(2);

        if (!this.checkUndefined(amountReceived) && (amountReceived + '').indexOf(',') !== -1) {
            amountReceived = amountReceived + '';
            amountReceived = amountReceived.replace(/,/g, '');
        }

        if (parseFloat(totalPaidAmount) == 0) {
            if (!this.checkUndefined(amountReceived)) {
                paymentReceiptData.paymentAmountToAllocate = amountReceived;
            }
            else {
                paymentReceiptData.paymentAmountToAllocate = 0.0;
            }
        }
        else {
            if (!this.checkUndefined(amountReceived)) {
                paymentReceiptData.paymentAmountToAllocate = parseFloat(amountReceived) - parseFloat(totalPaidAmount);
            }
            else {
                paymentReceiptData.paymentAmountToAllocate = 0.00 - parseFloat(totalPaidAmount);
            }
        }
        paymentReceiptData.paymentAmountReceived = parseFloat(amountReceived);
        component.set("v.paymentReceiptData", paymentReceiptData);
    },

    fetchInvoiceRecords: function (component, event, helper) {
        this.showSpinner(component);
        let selectedFilter = component.get("v.selectedFilter");
        let paymentSource = component.get("v.paymentSource");

        if (this.checkUndefined(selectedFilter) || this.checkUndefined(selectedFilter.Id)) { selectedFilter = ''; }
        else { selectedFilter = selectedFilter.Id; }

        if (this.checkUndefined(selectedFilter) && !this.checkUndefined(component.get("v.selectedPaymentSourceLessee"))) {
            selectedFilter = component.get("v.selectedPaymentSourceLessee");
        }

        if (paymentSource === "Security Deposit" && !this.checkUndefined(component.get("v.selectedSDPayableMemo"))) {
            if (!this.checkUndefined(component.get("v.selectedSDPayableMemo").Lease__c)) {
                selectedFilter = component.get("v.selectedSDPayableMemo").Lease__c;
            }
        }

        let allocatedInvoices = component.get("v.allocatedInvoiceData");
        let allocatedInvoiceIds = '';
        for (let key in allocatedInvoices) {
            if (!this.checkUndefined(allocatedInvoices[key].invoiceRecord)) {
                allocatedInvoiceIds += allocatedInvoiceIds == '' ? '\'' + allocatedInvoices[key].invoiceRecord.Id + '\'' : ', \'' + allocatedInvoices[key].invoiceRecord.Id + '\'';
            }
        }

        const action = component.get("c.fetchInvoiceData");
        action.setParams({
            searchInvoiceName: component.get("v.searchInvoiceName"),
            selectedFilter: selectedFilter,
            selectedInvoiceType: component.get("v.selectedInvoiceType"),
            allocatedInvoiceIds: allocatedInvoiceIds,
            paymentSource: component.get("v.paymentSource")
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                //console.log('Response: ', response.getReturnValue());
                let result = response.getReturnValue();
                if (this.checkUndefined(result)) { result = []; }
                component.set("v.unallocatedInvoiceData", result);
            }
            else if (state === "ERROR") {
                let errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    savePaymentReceiptData: function (component, event, helper) {
        this.showSpinner(component);

        let allocatedInvoices = component.get("v.allocatedInvoiceData");
        for (let key in allocatedInvoices) {
            if (!this.checkUndefined(allocatedInvoices[key].invoiceRecord)) {
                delete allocatedInvoices[key].invoiceRecord.Invoice_Line_Items__r;
            }
        }
        
        let paymentSource = component.get("v.paymentSource");
        let selectedSourceId;
        if (paymentSource === "Letter of Credit") {
            if (!this.checkUndefined(component.get("v.locDrawDownId"))) {
                selectedSourceId = component.get("v.locDrawDownId");
            }
        }
        else if (paymentSource === "MR Claims Payable Invoice") {
            if (!this.checkUndefined(component.get("v.selectedEventInvoice")) && !this.checkUndefined(component.get("v.selectedEventInvoice").Id)) {
                selectedSourceId = component.get("v.selectedEventInvoice").Id;
            }
        }
        else if (paymentSource === "Security Deposit") {
            if (!this.checkUndefined(component.get("v.selectedSDPayableMemo")) && !this.checkUndefined(component.get("v.selectedSDPayableMemo").Id)) {
                selectedSourceId = component.get("v.selectedSDPayableMemo").Id;
                component.set("v.paymentReceiptData.paymentLease", component.get("v.selectedSDPayableMemo.Lease__r"));
            }
        }

        const action = component.get("c.savePaymentReceipt");
        action.setParams({
            paymentReceipt_s: JSON.stringify(component.get("v.paymentReceiptData")),
            allocatedInvoice_s: JSON.stringify(component.get("v.allocatedInvoiceData")),
            paymentSource: paymentSource,
            selectedSourceId: selectedSourceId
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                //console.log('Response: ', response.getReturnValue());
                let result = response.getReturnValue();
                if (!this.checkUndefined(result)) {
                    if (!this.checkUndefined(result.paymentReceiptRecord)) {
                        component.set("v.paymentReceiptData", result);
                        component.set("v.allocatedInvoiceData", result.paymentReceiptInvoiceList);
                        component.set("v.isPaymentReceiptSaved", true);
                    }
                }
            }
            else if (state === "ERROR") {
                let errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    checkUndefined: function (input) {
        if (input !== undefined && input !== '' && input !== null) { return false; }
        return true;
    },

    showSpinner: function (component) {
        let spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (component) {
        let spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    //Show the error toast when any error occured
    handleErrors: function (errors, type, duration) {
        duration = "10000"; // time to display the Toast message.
        _jsLibrary.handleErrors(errors, type, duration); // Calling the JS Library function to handle the error
    }
})