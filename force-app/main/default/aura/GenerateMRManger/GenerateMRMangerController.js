({
    doInit: function (component, event, helper) {
        helper.showSpinner(component);
        var action = component.get("c.fetchMRManager");
        action.setParams({
            "aircraftId": component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                //console.log('response:', response.getReturnValue());
                var res = response.getReturnValue();
                if (res != undefined) {
                    if (res.isForecastGenerated) {
                        component.set("v.showContent", 'Please Wait. Redirecting to MR Table..!!!');
                        helper.navigateToMyComponent(component, event, helper);
                        helper.hideSpinner(component);
                    } else {
                        component.set("v.showContent", 'MR Profile for this asset is not yet generated. This process takes some time. Generate now or cancel ?');
                        helper.hideSpinner(component);
                        component.set("v.msnValue", res);
                    }
                }
            } else {
                helper.hideSpinner(component);
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },

    generateMRRecord: function (component, event, helper) {
        helper.showSpinner(component);
        var scenarioInp = component.get("v.msnValue").ScenarioInp;
        if (scenarioInp != undefined) {
            var action = component.get("c.generateMRProfile");
            action.setParams({
                "forecastId": scenarioInp
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    helper.handleErrors(response.getReturnValue(), 'Success');
                    $A.get("e.force:closeQuickAction").fire();
                } else {
                    var errors = response.getError();
                    if (errors) {
                        helper.handleErrors(errors);
                    }
                }
                helper.hideSpinner(component);
            });
            $A.enqueueAction(action);
        }
    },

    closeQuickAction: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
})