({
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        console.log('Hide Spinner');
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    getNamespacePrefix : function(component) {
       
        var action = component.get("c.getNamespacePrefix");
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            console.log("getNamespacePrefix response: "+namespace);
            
            component.set("v.namespace",namespace);
           
            
        });
         console.log('getNamespacePrefix----'+ component.get('v.namespace'));
       
        $A.enqueueAction(action);                
    },
    gotoList : function (component, event, helper) {
     
    this.getNamespacePrefix(component);
    console.log('namespace----'+component.get("v.namespace"));
    var action = component.get("c.getListViews");
        
    action.setCallback(this, function(response){
        
        var state = response.getState();
        console.log('state---'+state);
        if (state === "SUCCESS") {
            var listviews = response.getReturnValue();
            var navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": listviews.Id,
                "listViewName": null,
                "scope": component.get("v.namespace") +"Global_Fleet__c"
            });
            navEvent.fire();
        }
    });
    $A.enqueueAction(action);
},
    
	handleErrors : function(cmp,errors) {
       // Configure error toast
        let errorVal = '';
        let errorPopup =  cmp.find("errorM");
        $A.util.removeClass(errorPopup, "slds-hide");
      
       if (errors) {
           errors.forEach( function (error){
               //top-level error. There can be only one
               if (error.message){
                   errorVal = error.message;
                  
               }
               //page-level errors (validation rules, etc)
               if (error.pageErrors){
                   let arrErr = [];
                   error.pageErrors.forEach( function(pageError) {
                       errorVal = pageError.message;
                       
                   });	
               }
               if (error.fieldErrors){
                   //field specific errors--we'll say what the field is
                   for (var fieldName in error.fieldErrors) {
                       //each field could have multiple errors
                       error.fieldErrors[fieldName].forEach( function (errorList){	
                          errorVal = errorList.message;
                          
                       });  
                   };
               }
           });
       }
        
        cmp.set("v.errorMsg",errorVal);
    },
})