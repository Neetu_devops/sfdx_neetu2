({
updateAllWorldFleetReference : function(component, event, helper) {
       
    var answer = event.currentTarget.dataset.answer;
    console.log('Answer----'+answer);
        if(answer == 'Yes'){
            console.log('Inside Yes....');
            
 			var action = component.get("c.updateWorldFleetReference");
        	action.setCallback(this, function(response) {
            	var state = response.getState();
                console.log('State-----'+state);
                if (state == "SUCCESS") {
                    console.log('Inside Success....');
   					helper.gotoList(component, event, helper);
                    
                  	var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "Title": "Updated World Fleet References",
                        "type": "success",
                        "message": "Updating World Fleet References. An email with the results will be sent shortly."
                   });
                  resultsToast.fire();
                   
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    helper.gotoList(component, event, helper);
                    helper.handleErrors(errors);
                }
        });
       
             
        }else{
           helper.gotoList(component, event, helper);
        }
     $A.enqueueAction(action);
}
})