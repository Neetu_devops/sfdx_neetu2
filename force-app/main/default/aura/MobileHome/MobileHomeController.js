/* navToRecordController.js */
({
    handleClickOperator : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Operator__c"
        });
        homeEvent.fire();
},
     handleClickWorldFleet : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Global_Fleet__c"
        });
        homeEvent.fire();
},
    handleClickNewsFeed : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "CustomNews__c"
        });
        homeEvent.fire();
},
   handleClickContact : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Contact"
        });
        homeEvent.fire();
},
    handleClickDealWizard : function (component, event, helper) {
		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:Canvasing_Block ",
            
        });
        evt.fire();
},
    handleClickIntLog : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Customer_Interaction_Log__c"
        });
        homeEvent.fire();
},
     handleClickMarketInt : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Aircraft_Need__c"
        });
        homeEvent.fire();
},
    handleClickAssetFinder : function (component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:AssetFinder",
            
        });
        evt.fire();
},
    handleClickOperatorFinder : function (component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:OperatorFinder",
            
        });
        evt.fire();
},
     handleClickLease : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Lease__c"
        });
        homeEvent.fire();
},
    
    handleClickLessors : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Counterparty__c"
        });
        homeEvent.fire();
},
    
    handleClickPurchase : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Order__c"
        });
        homeEvent.fire();
},
    handleClickExistingDealsPage : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Marketing_Activity__c"
        });
        homeEvent.fire();
},
    
    handleClickAsset : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Aircraft__c"
        });
        homeEvent.fire();
    },
    
    handleClickAssembly : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Constituent_Assembly__c"
        });
        homeEvent.fire();
    },
})