({
    fetchUtilizations: function (component, year) {

        component.set("v.showSpinner", true);

        var action = component.get("c.getUtilizations");

        action.setParams({
            "assemblyId": component.get("v.assemblyId"),
            "startDate": component.get("v.startDate"),
            "endDate": component.get("v.endDate"),
            "year": year
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                console.log(data);
                if (data != null && data.length > 0) {
                    data.forEach(util => util.urDate = this.formatDate(new Date(util.urDate)));
                }
                component.set("v.data.utilizationList", data);
                component.set("v.currentYear", year);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });

        $A.enqueueAction(action);
    },
    formatDate: function (value) {
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let date = value.getUTCDate();
        let month = value.getUTCMonth();
        let year = value.getFullYear();
        return months[month] + ' ' + date + ', ' + year;
    },
    handleErrors: function (errors) {

        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
    }
})