({
	doInit : function(component, event, helper) {

        var assemblyId = component.get("v.assemblyId");

        if(assemblyId != undefined){
            
            component.set("v.showSpinner", true);
        
            var action = component.get("c.loadData");
                
            action.setParams({"assemblyId" : assemblyId});
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var data =  response.getReturnValue();
                    console.log(data);
                    if(data != null && data.utilizationList.length > 0){
                        data.utilizationList.forEach(util => util.urDate = helper.formatDate(new Date(util.urDate)));
                    }
                    component.set("v.data", data);
                    component.set("v.currentYear", data.latestYear);
                    component.set("v.startDate", data.startDate);
                    component.set("v.endDate", data.endDate);
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        helper.handleErrors(errors);
                    } else {
                        console.log("Unknown error");
                    }
                }
                
                component.set("v.showSpinner", false);
    
            });
        
        	$A.enqueueAction(action);
		}
	},
    
    previous : function(component, event, helper) {
        var currentYear = component.get("v.currentYear") + 1;
        helper.fetchUtilizations(component, currentYear);
        
    },
    
    next : function(component, event, helper) {
   		var currentYear = component.get("v.currentYear") - 1;
        helper.fetchUtilizations(component, currentYear);
    }
    
})