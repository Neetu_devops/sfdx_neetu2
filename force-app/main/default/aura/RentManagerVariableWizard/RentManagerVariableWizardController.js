({
    //on component load if user selects Fixed Rent : checks if multiRent is Active or Inactive
    doInit : function(component, event, helper) {
        console.log("doInit: ");
        var multiRent = component.get("v.multiRent");
        if(!$A.util.isEmpty(multiRent) && !$A.util.isUndefined(multiRent)) {
            var status = multiRent.Status;
            if(status.toUpperCase() == "Active".toUpperCase())
                component.set("v.isActive", true);
            else if(status.toUpperCase() == "InActive".toUpperCase())
                component.set("v.isActive", false);
        }
    },
    
    //on load: calls previewScreen and for edit, it fetches the all the picklist values for Page1
    reload :  function(component, event, helper){
        console.log("reload:showComponent ");
        if(component.get("v.showComponent") == true) {
            if(component.get("v.isViewMode") == true) {
                component.set("v.isLoading", true);
                helper.showPreviewScreen(component);
            }
            else{
                component.set("v.showOutput", false);
                component.set("v.showRentPeriod", true);
                component.set("v.showSLR",false);
                
                helper.getSecondRentStartDate(component);
                console.log("isExtendedRent : "+component.get("v.isExtendedRent"));
                if(component.get("v.isExtendedRent")){
                var extendedRentType = component.get('v.extendRentTypePickListValue');
                console.log('reload extendedRentType: '+extendedRentType);
                if($A.util.isEmpty(extendedRentType) || $A.util.isUndefined(extendedRentType)) 
                    helper.getExtendedRentTypePickList(component);
                else
                    helper.showPreviousExtendedRentTypePickList(component);
                }
                else{
                    component.set("v.extendedRentType",null);
                }
                
                
                var lowStartMon = component.get('v.lowStartMOnthPickListValue');
                if($A.util.isEmpty(lowStartMon) || $A.util.isUndefined(lowStartMon)) 
                    helper.getStartMonthPickList(component);
                else
                    helper.showPreviousStartMonthPickList(component);
                
                var lowEndMon = component.get('v.lowEndMOnthPickListValue');
                if($A.util.isEmpty(lowEndMon) || $A.util.isUndefined(lowEndMon)) 
                    helper.getEndMonthPickList(component);
                else
                    helper.showPreviousEndMonthPickList(component);
                
                var lowSeaPeriod =   component.get('v.lowSeasonPeriodPickListValue');
                if($A.util.isEmpty(lowSeaPeriod) || $A.util.isUndefined(lowSeaPeriod)) 
                    helper.getLowSeaPeriodPickList(component);
                else
                    helper.showPreviousLowSeaPeriodPickList(component);
                
                var invoiceVal = component.get('v.invoiceDayPickListValue');
                if($A.util.isEmpty(invoiceVal) || $A.util.isUndefined(invoiceVal)) 
                    helper.getInvoiceDayPickList(component);
                else
                    helper.showPreviousInvoiceDayPickList(component);
                
                var lowStartYear = component.get('v.lowStartYearPickListValue');
                if($A.util.isEmpty(lowStartYear) || $A.util.isUndefined(lowStartYear)) 
                    helper.getStartYearPickList(component);
                else
                    helper.showPreviousLowStartYEarPickList(component);
                
                var lowEndYear = component.get('v.lowEndYearPickListValue');
                if($A.util.isEmpty(lowEndYear) || $A.util.isUndefined(lowEndYear)) 
                    helper.getEndYearPickList(component);
                else
                    helper.showPreviousLowEndYEarPickList(component); 
                
                
                if(component.get("v.isEdit") == false)
                    helper.getLeaseData(component);
                
                var val = component.get('v.periodEndDayPickListValue');
                if($A.util.isEmpty(val) || $A.util.isUndefined(val)) 
                    helper.getRentPeriodDayPickList(component);
                else
                    helper.showPreviousRentDueDayPickList(component);    
            }
        }
        
        
    },
    
    //on refresh: resets all the component parameters
    refreshWizard :  function(component, event, helper){
        console.log("refreshWizard:refreshData from Variable "+component.get("v.isRefresh"));
        if(component.get("v.isRefresh") == true) {
            
            component.set("v.baseRentValue", "");
            component.set("v.lowSeasonRentValue", "");
            component.set("v.offWingRentValue", "");
            component.set("v.rentEscalationValue", "");
            component.set("v.rentEscalationMonthValue", "");
            component.set("v.slrCheck", false);
            component.set("v.slrOpeningBalance", "");
            component.set("v.slrStartDateValue", "");
            component.set("v.slrEndDateValue", "");
            component.set("v.differentStartDate","");
            component.set("v.secondRentStartValue","");
            component.set("v.rentStartValue", "");
            component.set("v.rentEndValue", "");
            component.set("v.lowStartMOnthValue", "");
            component.set("v.lowEndMOnthValue", "");
            component.set("v.lowStartYearValue", "");
            component.set("v.lowEndYearValue", "");
            component.set("v.lowSeasonPeriodValue", "");
            component.set("v.invoiceDayValue", "");
            component.set("v.periodEndDayValue", "");
            component.set("v.rentDueTypeValue", "");
            component.set("v.rentDueSettingValue", "");
            component.set("v.dueDateCorrectionValue", "");
            component.set("v.prorataValue", "");
            component.set("v.lowSYRangeStart", "");
            component.set("v.lowEYRangeEnd", "");
            
            helper.getLeaseData(component);
        }
    },
    
    //on Cancel button click
    onCancelClick: function(component, event, helper){
        console.log("onCancelClick ");
        component.set("v.showRentAmount", false);
        component.set("v.showRentPeriod", false);
        component.set("v.showRentDue", false);
        component.set("v.showPreview", false);
        component.set("v.showOutput", false);
        component.set("v.isEdit", false);
        
        var leaseId = component.get("v.leaseId");
        console.log("onCancelClick "+leaseId);
        var onCancelSchedule = component.getEvent("onWizardCancelButton");
        onCancelSchedule.setParams({
            "leaseId" : leaseId,
            "isEdit" : component.get("v.isEdit")
        });
        helper.changeMultiRentStatus(component);
        onCancelSchedule.fire();
        window.location.reload(); 
    },
    
    //on Previous button click for all pages
    onPreviousClick: function(component, event, helper){
        console.log('onPreviousClick :');
        var showRentAmount = component.get("v.showRentAmount");
        var showRentPeriod = component.get("v.showRentPeriod");
        var showRentDue = component.get("v.showRentDue");
        var showPreview = component.get("v.showPreview");
        
        if(showRentPeriod == true){
            component.set("v.showComponent", false);
            component.set("v.showRentAmount", false);
            component.set("v.showRentPeriod", false);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            
            //need to fire event 
            var leaseId = component.get("v.leaseId");
            console.log("onPreviousClick "+leaseId);
            var onTypeWizardSchedule = component.getEvent("onWizardCancelButton");
            onTypeWizardSchedule.setParams({
                "leaseId" : leaseId,
                "showTypeWizard" : true,
                "isEdit" : component.get("v.isEdit")
            });
            onTypeWizardSchedule.fire();
        }
        else if(showRentAmount == true){
            component.set("v.showRentAmount", false);
            component.set("v.showRentPeriod", true);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            
            helper.showPreviousEscPickList(component);
        }
            else if(showRentDue == true){
                component.set("v.showRentPeriod", false);
                component.set("v.showRentAmount", true);
                component.set("v.showRentDue", false);
                component.set("v.showPreview", false);
                
                helper.showPreviousRentDueSettingPickList(component);
                helper.showPreviousRentDueDayPickList(component);
                helper.showPreviousInvoiceDayPickList(component);
            }
                else if(showPreview == true){
                    component.set("v.showRentAmount", false);
                    component.set("v.showRentPeriod", false);
                    component.set("v.showRentDue", true);
                    component.set("v.showPreview", false);
                    
                    helper.showPreviousRentDueTypePickList(component);
                    helper.showPreviousDueCorrectionPickList(component);
                    helper.showPreviousProrataPickList(component);
                    
                }
    },
    
    onNextClick: function(component, event, helper){
        console.log('source is from ShowPreview: '+component.get("v.showPreview"));
        console.log('source is from IsEdit : '+component.get("v.isEdit"));
        
        var showRentAmount = component.get("v.showRentAmount");
        console.log('showRentAmount fetch: '+showRentAmount);
        
        var showRentPeriod = component.get("v.showRentPeriod");
        console.log('showRentPeriod fetch: '+showRentPeriod);
        
        var showRentDue = component.get("v.showRentDue");
        console.log('showRentDue fetch: '+showRentDue);
        
        var showPreview = component.get("v.showPreview");
        console.log('showPreview fetch: '+showPreview);
        
        component.set("v.isRentErrorMsg1", false);
        component.set("v.isRentErrorMsg2", false);
        component.set("v.isLowSeasonRentErrorMsg", false);
        component.set("v.isRentStartErrorMsg", false);
        component.set("v.isRentEndErrorMsg", false);
        component.set("v.isLowStartMonthErrorMsg", false);
        component.set("v.isLowEndMonthErrorMsg", false);
        component.set("v.isLowPeriodErrorMsg", false);
        component.set("v.isInvoiceDayErrorMsg", false);
        component.set("v.isRentDueDayErrorMsg", false);
        component.set("v.isSecondRentStartCheckMsg", false);
        component.set("v.isSecondRentStartErrorMsg", false);
        
        console.log("onNextClick showRentAmount " +showRentAmount );
        
        if(showRentPeriod == true){
            
            var rentStart = component.find('rentStartId').get("v.value");
            
            var rentEnd = component.find('rentEndId').get("v.value");
            
            var lowRentStartMonth = component.find('lowSeaStarMonId').get("v.value");
            
            var lowRentEndMonth = component.find('lowSeaEndMonId').get("v.value");
            
            var lowRentStartYear = component.find('lowSeaStarYearId').get("v.value");
            
            var lowRentEndYear = component.find('lowSeaEndYearId').get("v.value");
            
            var lowSeaPeriod = component.find('lowSeaPeriodId').get("v.value");
            
            var invoiceDay = component.find('invoiceDayId').get("v.value");
            
            var isDiffrentStartDate = component.find('differentStart').get("v.value");
            
            var periodicity = component.get("v.periodValue");
            console.log('reload isExtendedRent :'+component.get("v.isExtendedRent"));
            
            if(component.get("v.isExtendedRent")){
            var extendedRentType = component.find('extendedRentTypeId').get("v.value");
                component.set("v.extendedRentType",extendedRentType);
                console.log("extendedRentType",extendedRentType);
                if(component.get("v.extendedRentType") == 'Extended at Holdover Rent Rate'){
                    component.set("v.isHoldOverType",true);
                }else{
                    component.set("v.isHoldOverType",false);
                }
            }else{
                component.set("v.extendedRentType",null);
            }
            
            component.set("v.invoiceDayValue",invoiceDay);
            component.set("v.lowStartMOnthValue",lowRentStartMonth);
            component.set("v.lowEndMOnthValue",lowRentEndMonth);
            component.set("v.differentStartDate",isDiffrentStartDate);
            component.set("v.lowSeasonPeriodValue",lowSeaPeriod);
            component.set("v.lowStartYearValue",lowRentStartYear);
            component.set("v.lowEndYearValue",lowRentEndYear);  
            var isValid = true;
            
            var secondStartDate;
            var checkboxVal = component.get("v.differentStartDate");
            console.log("checkboxVal",checkboxVal);
            if(checkboxVal){
                 secondStartDate = component.find('secondRentDateValue2').get("v.value");	
                if(!$A.util.isEmpty(secondStartDate) && !$A.util.isUndefined(secondStartDate) && secondStartDate != null){	
                console.log("secondStartDate",secondStartDate);	
                component.set("v.secondRentStartValue",secondStartDate);
                    
                }	
                else{	
                    component.set("v.isSecondRentStartErrorMsg", true);	
                component.set("v.secondRentStartErrorMsg" ,"Please Enter Second Rent Start Date.");	
                    isValid = false;  	
                }	
            }	
            else{	
                component.set("v.secondRentStartValue",null);	
                }
            
            console.log('onNextClick rentStart: '+rentStart +'rentEnd: '+rentEnd +'invoiceDay: '+invoiceDay +'lowSeaPeriod: '+lowSeaPeriod);
            console.log('onNextClick lowRentStartMonth: '+lowRentStartMonth +'lowRentEndMonth: '+lowRentEndMonth +'lowStartYearValue: '+lowRentStartYear+ 'lowRentEndYear: '+lowRentEndYear);
            console.log('onNextClick periodicity: '+periodicity+'secondStartDate: '+secondStartDate+ 'isDiffrentStartDate: '+isDiffrentStartDate);
            
            
            var leaseInfo = component.get("v.leaseInfo");
            
            //Rent Start Date empty
            if($A.util.isEmpty(rentStart) || $A.util.isUndefined(rentStart)){
                console.log('Case1');
                component.set("v.isRentStartErrorMsg", true);
                component.set("v.rentStartErrorMsg" ,"Please Enter Rent Start Date");
                isValid = false;
            }
            
            //if Rent Start Date is before Lease Start Date
            else if(!$A.util.isEmpty(leaseInfo.LeaseStartDate) && !$A.util.isUndefined(leaseInfo.LeaseStartDate)){
                console.log('Error 2');
               console.log('rentStart '+rentStart + ' leaseInfo: '+leaseInfo.LeaseStartDate);
             
                if(rentStart < leaseInfo.LeaseStartDate) {
                    console.log('Error 2 if');
                    isValid = false;
                    component.set("v.rentStartErrorMsg" ,"Cannot be before Lease Start Date");
                    component.set("v.isRentStartErrorMsg", true);
                }
            }
            
            //if Rent End Date is empty
            if($A.util.isEmpty(rentEnd) || $A.util.isUndefined(rentEnd)) {
                console.log('Error 3');
                component.set("v.isRentEndErrorMsg", true);
                component.set("v.rentEndErrorMsg" ,"Please Enter Rent End Date");
                isValid = false;
            }
            
           //if Lease Extended using MidTermLeaseUpdateWizard	
            else if(component.get("v.isFromLE")){	
                var newLeaseEnd = component.get("v.leaseEndDate");	
                // if rent Start date not in between Lease Start Date and Extended LEase End Date	
                if(rentStart < leaseInfo.LeaseStartDate || rentStart >newLeaseEnd ){	
                    console.log('Error 4 ');	
                    isValid = false;	
                    component.set("v.rentStartErrorMsg" ,"Rent Start Date should be in between Lease Start Date and Extension Period End Date ");	
                    component.set("v.isRentStartErrorMsg", true);	
                }	
                //if rent End Date is after Extended Lease End Date	
                if(rentEnd > newLeaseEnd){	
                    console.log('Error 5');	
                    isValid = false;	
                    component.set("v.rentEndErrorMsg" ,"Cannot be after the Extended Lease End Date");	
                    component.set("v.isRentEndErrorMsg", true);	
                }	
            }	
                	
            //if rent End Date is after  Lease End Date 
            else if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                console.log('Error 4');
                if(!component.get("v.isExtendedRent")){
              if(rentEnd > leaseInfo.LeaseEndDate) {
                    console.log('Error 4 if');
                    isValid = false;
                    component.set("v.rentEndErrorMsg" ,"Cannot be after Lease End Date");
                    component.set("v.isRentEndErrorMsg", true);
                }
            }
            }
            
            
            //Extended rent: If Rent Start or Rent End Date > LeaseRedeliveryDate
            if(component.get("v.isExtendedRent")){
                if(!$A.util.isEmpty(leaseInfo.LeaseRedeliveryDate) && !$A.util.isUndefined(leaseInfo.LeaseRedeliveryDate)){
                    console.log('Error E');
                    if(component.get("v.iscontinue") == false){
                    if(rentEnd > leaseInfo.LeaseRedeliveryDate || rentStart > leaseInfo.LeaseRedeliveryDate ) {
                        console.log('Error 4.1 if');
                        isValid = false;
                        component.set("v.isRedelivery", true);
                    }
                    }else
                    {
                       component.set("v.iscontinue",false);
                        isValid = true;
                    }
                }
            }
            
            //Low Start Month == None
            console.log('lowRentStartMonth'+lowRentStartMonth);
            if(lowRentStartMonth == 'None' || $A.util.isEmpty(lowRentStartMonth) || $A.util.isUndefined(lowRentStartMonth)) {
                console.log('Case5');
                component.set("v.isLowStartMonthErrorMsg", true);
                component.set("v.lowStartMonthErrorMsg" ,"Please fill all of the seasonal rent fields before continuing.");
                isValid = false;
            }
            
            //Low End Month == None
            if(lowRentEndMonth == 'None' || $A.util.isEmpty(lowRentEndMonth) || $A.util.isUndefined(lowRentEndMonth)) {
                console.log('Case6');
                component.set("v.isLowEndMonthErrorMsg", true);
                component.set("v.lowEndMonthErrorMsg" ,"Please fill all of the seasonal rent fields before continuing.");
                isValid = false;
            }
            
            //if Rent Start Date is greater than the Rent End Date
            if(rentStart >= rentEnd ){
                console.log('Case7');
                component.set("v.isRentStartErrorMsg", true);
                component.set("v.rentStartErrorMsg" ,"Rent Start Date cannot be later than Rent End Date");
                isValid = false; 
            }
            
            //Low Rent Period == None
            if(lowSeaPeriod == 'None' || $A.util.isEmpty(lowSeaPeriod) || $A.util.isUndefined(lowSeaPeriod)) {
                console.log('Case8');
                component.set("v.isLowPeriodErrorMsg", true);
                component.set("v.lowPeriodErrorMsg" ,"Please fill all of the seasonal rent fields before continuing.");
                isValid = false;
            }
            
            //lowStartmonth == lowEnd Month
            if(lowRentStartMonth == lowRentEndMonth){
                    console.log('Case9');
                    component.set("v.isLowEndMonthErrorMsg", true);
                    component.set("v.lowEndMonthErrorMsg" ,"The Low Season Start and End Month cannot be the same.");
                    isValid = false;
                }
            
            
            //lowseason start year earlier than Rent StartDate
            var rentStartYear = new Date(rentStart);
            if(lowRentStartYear < rentStartYear.getUTCFullYear()){
                console.log('Case10');
                component.set("v.isLowStartYearErrorMsg", true);
                component.set("v.lowStartYearErrorMsg" ,"Low Season start and end must be within the range of the Rent Start and Rent End Dates.");
                isValid = false; 
            }
            
            
            //lowseason end year later than Rent EndDate
            var rentEndYear = new Date(rentEnd);
            if(lowRentEndYear > rentEndYear.getUTCFullYear()){
                console.log('Case11');
                component.set("v.isLowEndYearErrorMsg", true);
                component.set("v.lowEndYearErrorMsg" ,"Low Season start and end must be within the range of the Rent Start and Rent End Dates.");
                isValid = false; 
            }
            
            // lowseason StartYear:StartMonth later than lowseason EndYEar:EndMonth
            if(lowRentStartYear != null && lowRentEndYear!= null && lowRentStartMonth != null && lowRentEndMonth != null){
                if(lowRentStartYear > lowRentEndYear){
                    console.log('Case12');
                    component.set("v.isLowEndYearErrorMsg", true);
                    component.set("v.lowEndYearErrorMsg" ,"Low Season ending date must be later than Low Season starting date. Please correct your date selections..");
                    isValid = false; 
                    
                }
                else if(lowRentStartYear == lowRentEndYear){
                    console.log('lowEM < lowSM ');
                    var lowSM = helper.getMonthValue(component,lowRentStartMonth);
                    var lowEM = helper.getMonthValue(component,lowRentEndMonth);
                    console.log('lowSM :'+lowSM);
                    console.log('lowEM :'+lowEM);
                    if( lowSM > lowEM){
                        console.log('Case13');
                        component.set("v.isLowStartMonthErrorMsg", true);
                        component.set("v.lowStartMonthErrorMsg" ,"Low Season ending date must be later than Low Season starting date. Please correct your date selections..");
                        isValid = false; 
                    }  
                }
                
            }
            
            //if RentEnd Date < FirstDay of lowEndSesaon combo
            if(lowRentEndYear!= null && lowRentEndMonth != null && rentEnd != null ){
                console.log('Case14');
                var lowRentEMonth = helper.getMonthValue(component,lowRentEndMonth);
                var firstDay = new Date(lowRentEndYear,lowRentEMonth-1,1);
                var rentEndDay = new Date(rentEnd);
                console.log('firstDay'+firstDay);
                console.log('rentEnd'+rentEndDay);
                if(firstDay > rentEndDay){
                    component.set("v.isLowEndMonthErrorMsg", true);
                    component.set("v.lowEndMonthErrorMsg" ,"Low Season start and end must be within the range of the Rent Start and Rent End Dates.");
                    isValid = false;  
                }
            }
            
            //if RentStart Date > lastDay of lowStartSesaon combo
            if(lowRentStartYear!= null && lowRentStartMonth != null && rentStart != null ){
                console.log('Case15');
                var lowRentSMonth = helper.getMonthValue(component,lowRentStartMonth);
                var lastDay = new Date(lowRentStartYear,lowRentSMonth,0);
                var startDay = new Date(rentStart);
                console.log('lastDay :'+lastDay);
                console.log('startDay :'+startDay);
                if(lastDay < startDay){
                    component.set("v.isLowStartMonthErrorMsg", true);
                    component.set("v.lowStartMonthErrorMsg" ,"Low Season start and end must be within the range of the Rent Start and Rent End Dates.");
                    isValid = false;  
                }
            }
            
            //if diff btw Rent Start Date and Second Rent Start Date is longer than the periodicity selected
            var isDifferentStartflag = component.get("v.differentStartDate");
            console.log('isDifferentStartflag :'+isDifferentStartflag);
            if(isDifferentStartflag == true && secondStartDate != null){
                if(!$A.util.isEmpty(periodicity)){
                    console.log("periodicity not null");
                    if (periodicity	== 'Daily'){
                        console.log('Error 6');
                        var startDateDay = new Date(rentStart);
                        var secondStDateDay =new Date(secondStartDate);
                        
                        const diffTime = Math.abs(secondStDateDay - startDateDay);
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                        console.log('diffDays' +diffDays);
                        console.log(diffDays);
                        if (diffDays >= 1){
                            console.log('Error 7 diff if2');
                            component.set("v.isSecondRentStartErrorMsg", true);
                            component.set("v.secondRentStartErrorMsg" ,"The difference between the Rent Start Date and Second Rent Period Start Date is longer than the periodicity selected.");
                            isValid = false;    
                        }
                    }
                    if (periodicity	== 'Monthly'){
                        console.log('Error 7');
                        var startDateDay = new Date(rentStart);
                        var secondStDateDay =new Date(secondStartDate);
                        
                        const diffTime = Math.abs(secondStDateDay - startDateDay);
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                        console.log('diffDays' +diffDays);
                        console.log(diffDays);
                        if (diffDays >= 31){
                            console.log('Error 7 diff if2');
                            component.set("v.isSecondRentStartErrorMsg", true);
                            component.set("v.secondRentStartErrorMsg" ,"The difference between the Rent Start Date and Second Rent Period Start Date is longer than the periodicity selected.");
                            isValid = false;    
                        }
                    }
                    if (periodicity	== 'Quarterly'){
                        console.log('Error 8');
                        var startDateDay = new Date(rentStart);
                        var secondStDateDay =new Date(secondStartDate);
                        
                        const diffTime = Math.abs(secondStDateDay - startDateDay);
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                        console.log('diffDays' +diffDays);
                        console.log(diffDays);
                        if (diffDays >= 93){
                            console.log('Error 8 diff if2');
                            component.set("v.isSecondRentStartErrorMsg", true);
                            component.set("v.secondRentStartErrorMsg" ,"The difference between the Rent Start Date and Second Rent Period Start Date is longer than the periodicity selected.");
                            isValid = false;    
                        }
                    }
                    if (periodicity	== 'Semi-Annual'){
                        console.log('Error 9');
                        var startDateDay = new Date(rentStart);
                        var secondStDateDay =new Date(secondStartDate);
                        
                        const diffTime = Math.abs(secondStDateDay - startDateDay);
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                        console.log('diffDays' +diffDays);
                        console.log(diffDays);
                        if (diffDays >= 186){
                            console.log('Error 9 diff if2');
                            component.set("v.isSecondRentStartErrorMsg", true);
                            component.set("v.secondRentStartErrorMsg" ,"The difference between the Rent Start Date and Second Rent Period Start Date is longer than the periodicity selected.");
                            isValid = false;    
                        }
                    }
                    if (periodicity	== 'Annual'){
                        console.log('Error 10');
                        var startDateDay = new Date(rentStart);
                        var secondStDateDay =new Date(secondStartDate);
                        
                        const diffTime = Math.abs(secondStDateDay - startDateDay);
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                        console.log('diffDays' +diffDays);
                        console.log(diffDays);
                        if (diffDays >= 365){
                            console.log('Error 10 diff if2');
                            component.set("v.isSecondRentStartErrorMsg", true);
                            component.set("v.secondRentStartErrorMsg" ,"The difference between the Rent Start Date and Second Rent Period Start Date is longer than the periodicity selected.");
                            isValid = false;    
                        }
                    }
                    
                }
            }
            
            
            //second Rent Period Start Date cannot be later than the Lease End Date.
            if(!component.get("v.isExtendedRent")){
            if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate) &&
               !$A.util.isEmpty(secondStartDate) && !$A.util.isUndefined(secondStartDate)){
                console.log('Error 11');
                if(secondStartDate >= leaseInfo.LeaseEndDate){
                    component.set("v.isSecondRentStartErrorMsg", true);
                    component.set("v.secondRentStartErrorMsg" ,"The Second Rent Period Start Date cannot be later than the Lease End Date");
                    isValid = false;  
                }
                
            }
            
            //second Rent Period Start Date cannot be earlier than the Rent Start Date
            if(!$A.util.isEmpty(rentStart) && !$A.util.isUndefined(rentStart) &&
               !$A.util.isEmpty(secondStartDate) && !$A.util.isUndefined(secondStartDate)){
                console.log('Error 12');
                if(secondStartDate <= rentStart){
                    component.set("v.isSecondRentStartErrorMsg", true);
                    component.set("v.secondRentStartErrorMsg" ,"The Second Rent Period Start Date cannot be earlier than the Rent Start Date.");
                    isValid = false;  
                }
                
            }
            }
            
            //Extended Rent : Second Rent Period Start Date cannot be later than the Rent End Date
            if(component.get("v.isExtendedRent")){
                var isDifferentStartflag = component.get("v.differentStartDate");
                if(isDifferentStartflag == true){
                    if(!$A.util.isEmpty(rentEnd) && !$A.util.isUndefined(rentEnd) &&
                       !$A.util.isEmpty(secondStartDate) && !$A.util.isUndefined(secondStartDate)){
                        
                        if(rentEnd < secondStartDate){
                            console.log('Error 13');
                            isValid = false;
                            component.set("v.secondRentStartErrorMsg" ,"Second Rent Period Start Date cannot be later than the Rent End Date");
                            component.set("v.isSecondRentStartErrorMsg", true);  
                        }
                    }
                    
                     if(secondStartDate == null){
                        isValid = false;
                            component.set("v.secondRentStartErrorMsg" ,"Please enter Second Rent Period Start Date");
                            component.set("v.isSecondRentStartErrorMsg", true); 
                    }
                    
                }
            }
            
            //if periodEndDayValue != null then Second RentPeriod cannot be selected
            if(component.get("v.isEdit")){
            var periodEndDayValue = component.get("v.periodEndDayValue");
            console.log("periodEndDayValue :"+periodEndDayValue);
           if(periodEndDayValue !== undefined && periodEndDayValue !== ' ' && periodEndDayValue !== null ){
               var isDifferentStartflag = component.get("v.differentStartDate");
                if(isDifferentStartflag){
                  component.set("v.isSecondRentStartErrorMsg", true);
                component.set("v.secondRentStartErrorMsg" ,"If Rent Period End Day contains a value,the Different First Rent Period? flag cannot be used.Please deselect the checkbox. ");
                    isValid = false;    
                }
           }
            }
            if(isValid == true) {
                component.set("v.showRentPeriod", false);
                component.set("v.showRentAmount", true);
                component.set("v.showRentDue", false);
                component.set("v.showPreview", false);
                var slrStartDate = component.get("v.rentStartValue");
                component.set("v.slrStartDateValue",slrStartDate);
                console.log("slrStartDate "+component.get("v.slrStartDateValue"));
                var slrEndDate = component.get("v.rentEndValue");
                component.set("v.slrEndDateValue",slrEndDate); 
                console.log("slrEndDate "+component.get("v.slrEndDateValue"));
                var val = component.get('v.rentEscalationMonthPickListValue');
                if($A.util.isEmpty(val) || $A.util.isUndefined(val)) 
                    helper.getMonthPickList(component);
                else
                    helper.showPreviousEscPickList(component);
            }
        }
        else if(showRentAmount == true) {
            
            console.log("inside showRentAmount",component.get("v.rentEscalationValue"));
             var escalationval = component.find('escalationId').get("v.value");
             if(escalationval != null){
                 component.set("v.rentEscalationValue",escalationval);
             }
             else{
                 component.set("v.rentEscalationValue",null);
             }
            
            var escalationMonth = component.find('escalationMonthId').get("v.value");
            component.set("v.rentEscalationMonthValue",escalationMonth);
            console.log('onNextClick escalationMonth'+escalationMonth); 
            
            var leaseTypeCeck = component.get("v.validLeaseType"); 
            
            if(leaseTypeCeck){
                var slrCheck = component.find('slrCheckId').get("v.value");
                component.set("v.slrCheck",slrCheck);
                console.log('onNextClick slrCheck'+slrCheck); 
                if(slrCheck){
                    var slrOpeningBalance = component.find('slrbalanceId').get("v.value");
                    component.set("v.slrOpeningBalance",slrOpeningBalance);
                    console.log('onNextClick slrOpeningBalance'+slrOpeningBalance);
                }
            }
            
             if(!component.get("v.isEdit")){
                 component.set("v.rentDueTypeValue","Fixed");
                 var rentDueType = component.get("v.rentDueTypeValue");
                 if(rentDueType == 'Fixed'){
                     component.set("v.isFixedDueType",true);
                     component.set("v.isValidSetting",true);
                     
                 }else{
                     component.set("v.isFixedDueType",false);
                     component.set("v.isValidSetting",false);
                 }
                 }
            
            var isValid = true;
            var rent = component.find('baseRentId').get("v.value");
            console.log('onNextClick rent'+rent);
            
            var lowRent = component.find('lowSeasonRentId').get("v.value");
            
            var escalationMonth = component.find('escalationMonthId').get("v.value");
            component.set("v.rentEscalationMonthValue",escalationMonth);
            console.log('onNextClick escalationMonth'+escalationMonth + ' lowRent:'+lowRent);
           
            
            if($A.util.isEmpty(rent) || $A.util.isUndefined(rent)) {
                component.set("v.isRentErrorMsg1", true);
                isValid = false;
            }
            
            var leaseInfo = component.get("v.leaseInfo");
             var currentRent = leaseInfo.CurrentRent;
            
             if(component.get("v.isHoldOverType")){
                 if(rent < currentRent){
                     component.set("v.isRentErrorMsg2", true);
                isValid = false;
                 }
             }
            
            if($A.util.isEmpty(lowRent) || $A.util.isUndefined(lowRent)) {
                component.set("v.isLowSeasonRentErrorMsg", true);
                isValid = false;
            }
            
            if(isValid == true) {
                component.set("v.showRentPeriod", false);
                component.set("v.showRentAmount", false);
                component.set("v.showRentDue", true);
                component.set("v.showPreview", false);
                
                var rentVal = component.get('v.rentDueTypePickListValue');
                if($A.util.isEmpty(rentVal) || $A.util.isUndefined(rentVal)) 
                    helper.getRentDueTypePickList(component);
                else
                    helper.showPreviousRentDueTypePickList(component);
                helper.getRentDuedayFixedVal(component);
                
                var rentSetting = component.get('v.rentDueSettingPickListValue');
                if($A.util.isEmpty(rentSetting) || $A.util.isUndefined(rentSetting)) 
                    helper.getRentDueSettingPickList(component);
                else
                    helper.showPreviousRentDueSettingPickList(component);
                
                var corVal = component.get('v.dueDateCorrectionPickListValue');
                if($A.util.isEmpty(corVal) || $A.util.isUndefined(corVal)) 
                    helper.getDueCorrectionPickList(component);
                else
                    helper.showPreviousDueCorrectionPickList(component);
                
                var prorataVal = component.get('v.prorataPickListValue');
                if($A.util.isEmpty(prorataVal) || $A.util.isUndefined(prorataVal)) 
                    helper.getProrataPickList(component);
                else
                    helper.showPreviousProrataPickList(component);
            }
        }
            else if(showRentDue == true){
                var rentDueType = component.find('rentDueTypeId').get("v.value");
                var dueCorr = component.find('dueDayCorrectionId').get("v.value");
                var prorata = component.find('prorataId').get("v.value");
                
                component.set("v.rentDueTypeValue",rentDueType);
                component.set("v.dueDateCorrectionValue",dueCorr);
                component.set("v.prorataValue",prorata);
                
                console.log('showRentDue rentDueType',rentDueType);
                if(rentDueType == 'Fixed'){
                    component.set("v.isFixedDueType",true);
                    component.set("v.isValidSetting",true);
                    
                }else{
                    component.set("v.isFixedDueType",false);
                    component.set("v.isValidSetting",false);
                }
                
                var isFixedDueType = component.get("v.isFixedDueType");
                var isEdit = component.get("v.isEdit");
                console.log('showRentDue isFixedDueType',isFixedDueType);
                console.log('showRentDue isEdit',isEdit);
                
                var rentDueDay = null;
                
                if(isFixedDueType){
                    console.log(" if isFixedDueType");
                    rentDueDay = component.find('rentDueDayId1').get("v.value");}
                else{
                    console.log("else 2 isFixedDueType");
                    rentDueDay = component.find('rentDueDayId').get("v.value");
                }
                component.set("v.rentDueDayValue",rentDueDay);
                var isValid = true;
                if($A.util.isEmpty(rentDueDay) || $A.util.isUndefined(rentDueDay))  {
                    component.set("v.isRentDueDayErrorMsg", true);
                    component.set("v.rentDueDayErrorMsg", "Please Enter Rent Due Day");
                    isValid = false;
                }
                else{
                    if(rentDueType.includes('Fixed')) {
                        if(rentDueDay > 31) {
                            isValid = false;
                            component.set("v.isRentDueDayErrorMsg", true);
                            component.set("v.rentDueDayErrorMsg", "Rent Due Type 'Fixed' has been selected, therefore value in Rent Due Day field must be between 1 and 31 (days in the calendar month).");
                        }
                    }    
                }
                
                var isValidSetting = component.get("v.isValidSetting");
                var rentDueSetting = null;
                console.log('isValidSetting',isValidSetting);
                if(isValidSetting){ 
                    rentDueSetting = component.find('settingId3').get("v.value"); 
                }
                else{
                    rentDueSetting = component.find('rentDueSettingId').get("v.value"); 
                }
                component.set("v.rentDueSettingValue",rentDueSetting); 
                console.log('showRentDue rentDueSetting',rentDueSetting);
                if(isValid == true) {
                    component.set("v.showRentAmount", false);
                    component.set("v.showRentPeriod", false);
                    component.set("v.showRentDue", false);
                    component.set("v.showPreview", true);
                }
            }
                else if(showPreview == true){
                    component.set("v.isLoading", true);
                    helper.checkLeaseApprovalStatus(component);
                }
    },
    
    //On Edit button click
    onEditData: function(component, event, helper){
        console.log('onEditData');
        
        var multiRent = component.get("v.multiRent");
        component.set("v.isExtendedRent",multiRent.isExtendedRentPeriod );
        var isValid = true;
        if(!$A.util.isEmpty(multiRent) && !$A.util.isUndefined(multiRent)) {
            var status = multiRent.Status;
            if(status.toUpperCase() == "Active".toUpperCase()){
                component.set("v.isActive", true);
            }
            else if(status.toUpperCase() == "InActive".toUpperCase()){
                component.set("v.isActive", false);
                isValid = false;
            } 
        }
        if(isValid) {
            component.set("v.showComponent", false);
            component.set("v.showRentAmount", false);
            component.set("v.showRentPeriod", false);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            component.set("v.isEdit", true);
            
            //need to fire event 
            var leaseId = component.get("v.leaseId");
            console.log("onPreviousClick "+leaseId);
            var onTypeWizardSchedule = component.getEvent("onWizardCancelButton");
            onTypeWizardSchedule.setParams({
                "leaseId" : leaseId,
                "showTypeWizard" : true,
                "isEdit" : component.get("v.isEdit")
            });
            onTypeWizardSchedule.fire();
        }
        else{
            helper.showWarningMsg(component, "Inactive", "The rent schedule is unable to edit because the schedule is ticked as Inactive.");
        }
    },
    //onDelete multiRent record
    onDeleteRecord: function(component, event, helper){
        console.log('onDeleteRecord');
        
        var multiRent = component.get("v.multiRent");
        var isValid = true;
        if(!$A.util.isEmpty(multiRent) && !$A.util.isUndefined(multiRent)) {
            var status = multiRent.Status;
            if(status.toUpperCase() == "InActive".toUpperCase())
                isValid = false;
            console.log('onEditData '+status);
        }
        
        if(isValid) {
            component.set("v.isLoading", true);
            helper.deleteMultiRentSch(component);
        }
        else{
            helper.showWarningMsg(component, "InActive", "The rent schedule is unable to delete because the schedule is ticked as Inactive.");
        }
    },
    
    //used to show the list of schedules for the clicked multirent record
    showRecord: function(component,event,helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord "+recordId);
        window.open('/' + recordId);  
    },
    
    //when user clicks on Straight Line Revenue button
    showSLR: function(component,event,helper){
        console.log("showSLR ",component.get("v.showSLR"));
        component.set("v.showSLR",true);
        helper.fetchSLRData(component);
        
    },
    
    //when user clicks on Back to Rent Schedule button
    showRentSchedule: function(component,event,helper){
        component.set("v.showSLR",false);
        component.set("v.showOutput",true);
        console.log("showRentSchedule",component.get("v.showOutput"));
    },
    
    toggleChange: function(component, event, helper){
        console.log("toggleChange");
        console.log("toggleChange isActive "+component.get("v.isActive"));
    },
    
    //used when checkbox is true or false,to hide the slr related fields
    showSLRdata : function(component, event, helper) {
        console.log("showSLRdata");
        let checkBoxState = event.getSource().get('v.value');
        console.log('checkBoxState',checkBoxState);
        
        if(checkBoxState){
            var changeElement = component.find("slrdivId2");
            // by using $A.util.toggleClass add-remove slds-hide class
            console.log('changeElement');
            $A.util.toggleClass(changeElement, "slds-hide");
        }
        else{
            var changeElement1 = component.find("slrdivId1");
            // by using $A.util.toggleClass add-remove slds-hide class
            console.log('changeElement1');
            $A.util.toggleClass(changeElement1, "slds-hide");   
        }
        
        
        
        
    },
    
    //used when checkbox is true or false,to hide the second rent Date field
    showSecondRentPeriodDate : function(component, event, helper) {
        console.log("secondRentPeriod");
        let checkBoxState = event.getSource().get('v.value');
        
        if(checkBoxState){
            console.log("secondRentPeriod if");
            var changeElement1 = component.find("secondRentStartId21");
            var changeElement2 = component.find("secondRentStartId22");
            console.log("secondRentPeriod changeElement1 : ",changeElement1);
            console.log("secondRentPeriod changeElement2 : ",changeElement2);
            $A.util.toggleClass(changeElement1, "slds-hide");
            $A.util.toggleClass(changeElement2, "slds-hide");
            component.set("v.secondRentStartErrorMsg","");
        }
        else{
            console.log("secondRentPeriod else");
            var changeElement3 = component.find("secondRentStartId11");
            var changeElement4 = component.find("secondRentStartId12");
            console.log("secondRentPeriod changeElement3 : ",changeElement3);
            console.log("secondRentPeriod changeElement4 : ",changeElement4);
            $A.util.toggleClass(changeElement3, "slds-hide");
            $A.util.toggleClass(changeElement4, "slds-hide");
            component.set("v.secondRentStartValue",null);
        }
        
        
    },
    
    //used when checkbox is true or false,to hide the rent due type picklist field
    showSetting : function(component, event, helper) {
        console.log("showSetting");
        var duetype = component.find('rentDueTypeId').get("v.value");
        console.log('showSetting',duetype);
        
        if(duetype == 'Fixed'){
            component.set("v.isValidSetting",true);   
            component.set("v.isFixedDueType",true);            
            var changeElement = component.find("settingId2");
            console.log('changeElement',changeElement);
            $A.util.toggleClass(changeElement, "slds-hide");
            helper.getRentDueSettingPickList(component);
            helper.getRentDuedayFixedVal(component);
        }
        else{
            component.set("v.isValidSetting",false); 
            component.set("v.isFixedDueType",false);
            var changeElement1 = component.find("settingId1");
            console.log('changeElement1');
            $A.util.toggleClass(changeElement1, "slds-hide"); 
            
            var changeElement3 = component.find("settingId3");
            console.log('changeElement3');
            $A.util.toggleClass(changeElement3, "slds-hide"); 
            
        }
    },
    
    getLowYearval : function(component, event, helper) {
        console.log('getLowYearval');
         
        helper.getLowSTartEndYear(component);
        
        
    },
    
    validateStartYear: function(component, event, helper) {
        console.log('validateStartYear');
        var rentStart = component.find('rentStartId').get("v.value");
        var rentEnd = component.find('rentEndId').get("v.value");
        var lowRentStartYear = component.find('lowSeaStarYearId').get("v.value");
        
        if($A.util.isEmpty(rentStart) || $A.util.isUndefined(rentStart)){
            component.set("v.isLowStartYearErrorMsg", true);
            component.set("v.lowStartYearErrorMsg" ,"Please fill Rent Start Date and Rent End Date before selecting a value in this field.");
            
        }
        else{
            component.set("v.isLowStartYearErrorMsg", false);
            
        }
    },
    
    validateEndYear: function(component, event, helper) {
        console.log('validateEndYear');
        var rentStart = component.find('rentStartId').get("v.value");
        var rentEnd = component.find('rentEndId').get("v.value");
        var lowRentEndYear = component.find('lowSeaEndYearId').get("v.value");
        
        if($A.util.isEmpty(rentStart) || $A.util.isUndefined(rentStart)){
            component.set("v.isLowEndYearErrorMsg", true);
            component.set("v.lowEndYearErrorMsg" ,"Please fill Rent Start Date and Rent End Date before selecting a value in this field.");
            
        }
        else{
            component.set("v.isLowEndYearErrorMsg", false);
            
        }
    },
    
    validateStartMonth: function(component, event, helper) {
        console.log('validateStartMonth');
        var rentStart = component.find('rentStartId').get("v.value");
        var rentEnd = component.find('rentEndId').get("v.value");
        var lowRentStartMonth = component.find('lowSeaStarMonId').get("v.value");
        
        if($A.util.isEmpty(rentStart) || $A.util.isUndefined(rentStart)){
            component.set("v.isLowStartMonthErrorMsg", true);
            component.set("v.lowStartMonthErrorMsg" ,"Please fill Rent Start Date and Rent End Date before selecting a value in this field.");
            
        }else{
            component.set("v.isLowStartMonthErrorMsg", false);
        }
        
    },
    
    validateEndMonth: function(component, event, helper) {
        console.log('validateEndMonth');
        var rentStart = component.find('rentStartId').get("v.value");
        var rentEnd = component.find('rentEndId').get("v.value");
        var lowRentEndMonth = component.find('lowSeaEndMonId').get("v.value");
        
        if($A.util.isEmpty(rentStart) || $A.util.isUndefined(rentStart)){
            component.set("v.isLowEndMonthErrorMsg", true);
            component.set("v.lowEndMonthErrorMsg" ,"Please fill Rent Start Date and Rent End Date before selecting a value in this field.");
            isValid = false; 
        }
        else{
            component.set("v.isLowEndMonthErrorMsg", false);
        }
    },
    
    //used for isRedelivery pop-up model close 
    closeModel : function(component,event,helper){
        console.log('closeModel');
         if(component.get("v.showRentPeriod") == true){
            component.set("v.showComponent", false);
            component.set("v.showRentAmount", false);
            component.set("v.showRentPeriod", false);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            
            //need to fire event 
            var leaseId = component.get("v.leaseId");
            console.log("onPreviousClick "+leaseId);
            var onTypeWizardSchedule = component.getEvent("onWizardCancelButton");
            onTypeWizardSchedule.setParams({
                "leaseId" : leaseId,
                "showTypeWizard" : true,
                "isEdit" : component.get("v.isEdit")
            });
            onTypeWizardSchedule.fire();
        }
        component.set("v.isRedelivery",false);
       
    },
    
    //used for isRedelivery pop-up model: when pressed Ok, then continue to Step3
    ProceedStep3 : function(component,event,helper){
        console.log('ProceedStep3');
        component.set("v.isRedelivery",false);
        component.set("v.iscontinue",true);
    },
    
})