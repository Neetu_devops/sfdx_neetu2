({
    //lease related data
    getLeaseData : function(component) {
        console.log('getLeaseData RentManagerVariableWizard');
        var action = component.get("c.getLeaseInfo");
        action.setParams({
            recordId : component.get("v.leaseId")
        });
        action.setCallback(this, function(response) {
            var leaseInfo = response.getReturnValue();
            if(leaseInfo != null){
                console.log("getLeaseData response: "+JSON.stringify(leaseInfo));
                component.set("v.leaseInfo",leaseInfo);
                
                if(!$A.util.isEmpty(leaseInfo) && !$A.util.isUndefined(leaseInfo)) {
                    //Case : Extended Rent checkbox True
                    if(component.get("v.isExtendedRent")){
                        //Case 1 : Rent Schedules present 
                        console.log('getLeaseData : isExtendedRentCheckbox : '+component.get("v.isExtendedRent")); 
                        if(!$A.util.isEmpty(leaseInfo.RentScheduleLastDate) && !$A.util.isUndefined(leaseInfo.RentScheduleLastDate)){
                            console.log('getLeaseData if1 Extend: '+leaseInfo.RentScheduleLastDate);
                            this.getLeaseStartDate(component,leaseInfo.RentScheduleLastDate);
                            //this.getLowSTartEndYear(component);
                        }
                        //Case 2 : Rent Schedules not present
                        else if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                            console.log('getLeaseData else1 Extend: '+leaseInfo.LeaseEndDate);
                            this.getLeaseStartDate(component,leaseInfo.LeaseEndDate);
                            //this.getLowSTartEndYear(component);
                        }
                        component.set("v.rentEndValue",null);
                        
                    }
                    else{
                        if(!$A.util.isEmpty(leaseInfo.RentScheduleLastDate) && !$A.util.isUndefined(leaseInfo.RentScheduleLastDate)){
                            console.log('if1');
                            component.set("v.rentStartValue",leaseInfo.RentScheduleLastDate);
                           // this.getLowSTartEndYear(component);
                            
                        }
                        else if(!$A.util.isEmpty(leaseInfo.LeaseStartDate) && !$A.util.isUndefined(leaseInfo.LeaseStartDate)){
                            console.log('else1');
                            component.set("v.rentStartValue",leaseInfo.LeaseStartDate);
                           // this.getLowSTartEndYear(component);
                        }
                        if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                            console.log('if2');
                            component.set("v.rentEndValue",leaseInfo.LeaseEndDate);
                           // this.getLowSTartEndYear(component);
                        }
                        if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                            if(!$A.util.isEmpty(leaseInfo.RentScheduleLastDate) && !$A.util.isUndefined(leaseInfo.RentScheduleLastDate)){
                                var leaseEndDt = new Date(leaseInfo.LeaseEndDate);
                                var ScheduleEndDt = new Date(leaseInfo.RentScheduleLastDate); 
                                console.log("leaseEndDt :"+leaseEndDt+ "ScheduleEndDt :"+ScheduleEndDt);
                                if(leaseEndDt <= ScheduleEndDt){
                                    console.log('if3');
                                    component.set("v.rentEndValue","");
                                    component.set("v.rentStartValue",""); 
                                    component.set("v.lowSYRangeStart","");
                                    component.set("v.lowEYRangeEnd","");
                                    component.set("v.lowStartYearValue","");
                                    component.set("v.lowEndYearValue","");
                                    
                                    
                                }
                            }
                        }      
                    }
                    this.getLowSTartEndYear(component);
                }
            }
        });
        $A.enqueueAction(action);                
    },
    
    //calculate Lease Start Date
    getLeaseStartDate: function(component,startDate) {
        console.log("getLeaseStartDate");
        var action = component.get("c.getLeaseStartDate");
        action.setParams({
            startDate : startDate
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getLeaseStartDate response-state: "+state);
            if (state === "SUCCESS") {
                var leaseStartDate = response.getReturnValue();
                console.log("getLeaseStartDate response: "+leaseStartDate);
                if(leaseStartDate != null){
                    component.set("v.rentStartValue",leaseStartDate);
                    
                    var lowStartYear = leaseStartDate.getUTCFullYear();
                    component.set("v.lowStartYearValue",lowStartYear);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getLeaseStartDate errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("getLeaseStartDate Unknown error");
                    this.showErrorMsg(component, "Error in fetching lease start date");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    
    checkLeaseApprovalStatus: function(component) {

        console.log('checkLeaseApprovalStatus');

        var self = this;

        var action = component.get("c.getLeaseApprovalStatus");

        action.setParams({

            leaseId : component.get("v.leaseId")

        });

        action.setCallback(this, function(response) {

            var state = response.getState();

            console.log("checkLeaseApprovalStatus response-state: "+state);

            if (state === "SUCCESS") {

                var result = response.getReturnValue();

                console.log("checkLeaseApprovalStatus"+result);

                if(result) {

                    component.set("v.isLeaseApproved", result);

                    this.generateData(component);

                    console.log("checkLeaseApprovalStatus"+component.get("v.isLeaseApproved"));

                }

                else{

                    component.set("v.isLeaseApproved", result);

                    component.set("v.isLoading", false);

                    this.showErrorMsg(component, "Rent Schedules can only be created after the Lease is approved via the Approval Process");

                }

            }

            else if (state === "ERROR") {    

                var errors = response.getError();

                console.log("checkLeaseApprovalStatus errors "+JSON.stringify(errors));

                if (errors) {

                    this.handleErrors(errors);   

                } else {

                    console.log("checkLeaseApprovalStatus Unknown error");

                    this.showErrorMsg(component, "Error in fetching record");

                }

            }

        });

        $A.enqueueAction(action); 

    },
    //fetch escalation month picklist values
    getMonthPickList: function(component) {
        console.log('getMonthPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Escalation_Month__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getMonthPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getMonthPickList  result:'+allValues);
                    component.set('v.rentEscalationMonthPickListValue', allValues);
                    opts.push({class: "optionClass",label: "--- None ---",value: ""});
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                    var escalationMonthId = component.find('escalationMonthId');
                    if(escalationMonthId != undefined){
                        escalationMonthId.set("v.options", opts); 
                    } 
                }
            }
            var fieldV = component.get("v.rentEscalationMonthValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousEscPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch rent period end day picklist values
    getRentPeriodDayPickList: function(component) {
        console.log('getRentPeriodDayPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Period_End_Day__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRentPeriodDayPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getRentPeriodDayPickList  result:'+allValues);
                    component.set('v.periodEndDayPickListValue', allValues);
                    for(var i=0;i< allValues.length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var rentPeriodEndDayId = component.find('rentPeriodEndDayId');
                if(rentPeriodEndDayId != undefined){
                    rentPeriodEndDayId.set("v.options", opts); 
                }
            }
            var fieldV = component.get("v.periodEndDayValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousRentDueDayPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch invoice generation day picklist values
    getInvoiceDayPickList: function(component) {
        console.log('getInvoiceDayPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Invoice_Generation_Day__c'
        });
        var opts = [];
        console.log("before res");
        action.setCallback(this, function(response) {
            console.log("after res");
            console.log('getInvoiceDayPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getInvoiceDayPickList  result:'+allValues);
                    component.set('v.invoiceDayPickListValue', allValues);
                    for(var i=0;i< allValues.length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var invoicedayid = component.find('invoiceDayId');
                if(invoicedayid != undefined){
                    invoicedayid.set("v.options", opts); 
                }
                
            }
            var fieldV = component.get("v.invoiceDayValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousInvoiceDayPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch extended rent type picklist values
    getExtendedRentTypePickList: function(component) {
        console.log('getExtendedRentTypePickList');
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Extended_Rent_Type__c'
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('getExtendedRentTypePickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getExtendedRentTypePickList result:'+allValues);
                    component.set("v.extendRentTypePickListValue",allValues);
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var extendedRentTypeId = component.find('extendedRentTypeId');
                if(extendedRentTypeId != undefined){
                    extendedRentTypeId.set("v.options", opts); 
                }
            }
            var fieldV = component.get("v.extendedRentType");
            console.log('getTypePickList fieldV :',fieldV);
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousExtendedRentTypePickList(component);
        });
        $A.enqueueAction(action);
    },
    
    getLowSTartEndYear: function(component){
        console.log('getLowSTartEndYear');
        var rentStart = component.find('rentStartId').get("v.value");
        var rentEnd = component.find('rentEndId').get("v.value");
        var lowStartYear;
        var lowEndYear;
        
        if(!$A.util.isEmpty(rentStart) && !$A.util.isUndefined(rentStart)) {
            rentStart = new Date(rentStart);
            console.log("getLowSTartEndYear rentStart if",rentStart);
            
            lowStartYear = rentStart.getUTCFullYear();
            console.log("getLowSTartEndYear lowStartYear if ",lowStartYear);
            
            if(lowStartYear != null){
                component.set("v.lowStartYearValue",lowStartYear);
                component.set("v.lowSYRangeStart",lowStartYear);
            }
            this.getStartYearPickList(component);
        }
        else{
            rentStart = component.get("v.rentStartValue");
            rentStart = new Date(rentStart);
            console.log("getLowSTartEndYear rentStart else ",rentStart);
            
            lowStartYear = rentStart.getUTCFullYear();
            console.log("getLowSTartEndYear lowStartYear else",lowStartYear);
            
            if(lowStartYear != null){
                component.set("v.lowStartYearValue",lowStartYear);
                component.set("v.lowSYRangeStart",lowStartYear);
            }
            this.getStartYearPickList(component);
        }
        if(!$A.util.isEmpty(rentEnd) && !$A.util.isUndefined(rentEnd)) {
            rentEnd = new Date(rentEnd);
            console.log("getLowSTartEndYear rentEnd if",rentEnd);
            
            lowEndYear = rentEnd.getUTCFullYear();
            console.log("getLowSTartEndYear lowEndYear if",lowEndYear);
            if(lowEndYear != null){
                component.set("v.lowEndYearValue",lowEndYear);
                component.set("v.lowEYRangeEnd",lowEndYear);
            }
            this.getStartYearPickList(component);
            this.getEndYearPickList(component); 
        }
        else{
          rentEnd = component.get("v.rentEndValue");
            rentEnd = new Date(rentEnd);
            console.log("getLowSTartEndYear rentEnd else",rentEnd);
            
            lowEndYear = rentEnd.getUTCFullYear();
            console.log("getLowSTartEndYear lowEndYear else",lowEndYear);
            if(lowEndYear != null){
                component.set("v.lowEndYearValue",lowEndYear);
                component.set("v.lowEYRangeEnd",lowEndYear);
            }
            this.getStartYearPickList(component);
            this.getEndYearPickList(component); 
        }
        
    },
    
    //fetch low season start month picklist values
    getStartMonthPickList: function(component) {
        console.log('getStartMonthPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Low_Season_Start_Month__c'
        });
        var opts = [];
        console.log("getStartMonthPickList before res");
        action.setCallback(this, function(response) {
            console.log('getStartMonthPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getStartMonthPickList  result:'+allValues);
                    component.set('v.lowStartMOnthPickListValue', allValues);
                    for(var i=0;i< allValues.length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var lowStartMonthid = component.find('lowSeaStarMonId');
                if(lowStartMonthid != undefined){
                    lowStartMonthid.set("v.options", opts); 
                }
                
            }
            var fieldV = component.get("v.lowStartMOnthValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousStartMonthPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch low season end month picklist values
    getEndMonthPickList: function(component) {
        console.log('getEndMonthPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Low_Season_End_Month__c'
        });
        var opts = [];
        console.log("getEndMonthPickList before res");
        action.setCallback(this, function(response) {
            console.log("getEndMonthPickList after res");
            console.log('getEndMonthPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getEndMonthPickList  result:'+allValues);
                    component.set('v.lowEndMOnthPickListValue', allValues);
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var lowSeaEndMonId = component.find('lowSeaEndMonId');
                if(lowSeaEndMonId != undefined){
                    lowSeaEndMonId.set("v.options", opts); 
                }
                
            }
            var fieldV = component.get("v.lowEndMOnthValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousEndMonthPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch low season based on picklist values
    getLowSeaPeriodPickList:function(component) {
        console.log('getLowSeaPeriodPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Low_Season_Based_On__c'
        });
        var opts = [];
        console.log("getLowSeaPeriodPickList before res");
        action.setCallback(this, function(response) {
            console.log("getLowSeaPeriodPickList after res");
            console.log('getLowSeaPeriodPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getLowSeaPeriodPickList  result:'+allValues);
                    component.set('v.lowSeasonPeriodPickListValue', allValues);
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var lowSeaPeriodId = component.find('lowSeaPeriodId');
                if(lowSeaPeriodId != undefined){
                    lowSeaPeriodId.set("v.options", opts); 
                }
                
            }
            var fieldV = component.get("v.lowSeasonPeriodValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousLowSeaPeriodPickList(component);
        });
        $A.enqueueAction(action);
        
    },
    
    //fetch low season start year picklist values
    getStartYearPickList:function(component) {
        console.log('getStartYearPickList');
        var opts = [];
        var startRange = component.get("v.lowSYRangeStart");
        var endRange = component.get("v.lowEYRangeEnd");
        var startYear = component.get("v.lowStartYearValue");
        var isEdit = component.get("v.isEdit");
        console.log('startRange: '+startRange +'endRange: '+endRange +'startYear: '+startYear +'isEdit: '+isEdit);
        if(isEdit){
            var rentStart = component.get("v.rentStartValue");
            var rentStartDate = new Date(rentStart);
            var rentEnd = component.get("v.rentEndValue");
            var rentEndDate = new Date(rentEnd);
            
            if(rentStartDate != null && rentStartDate != undefined)
            startRange = rentStartDate.getUTCFullYear();
            
            if(rentEndDate != null && rentEndDate != undefined)
            endRange = rentEndDate.getUTCFullYear();
            
            for(var i=startRange;i<=endRange; i++){
                console.log(i);
                if(i == startRange){
                    console.log('if',i);
                    opts.push({"class": "optionClass", label: i, value:i, selected:true}); 
                }
                else{
                    console.log('else',i);
                    opts.push({"class": "optionClass", label: i, value:i});
                }
            }
        }else{
            for(var i=startRange;i<=endRange; i++){
                console.log(i);
                opts.push({"class": "optionClass", label: i, value:i}); 
            }  
        }
        var lowSeaStarYearId = component.find('lowSeaStarYearId');
        if(lowSeaStarYearId != undefined){
            lowSeaStarYearId.set("v.options", opts); 
        } 
        
        var fieldV = component.get("v.lowStartYearValue");
        if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
            this.showPreviousLowStartYEarPickList(component);       
    },
    
    //fetch low season start year picklist values
    getEndYearPickList:function(component) {
        console.log('getEndYearPickList');
        var opts = [];
        var startRange = component.get("v.lowSYRangeStart");
        var endRange = component.get("v.lowEYRangeEnd");
        var endYear = component.get("v.lowEndYearValue");
        var isEdit = component.get("v.isEdit");
        console.log('startRange: '+startRange +'endRange: '+endRange +'endYear: '+endYear +'isEdit: '+isEdit);
        
        if(isEdit){
            var rentStart = component.get("v.rentStartValue");
            var rentStartDate = new Date(rentStart);
            var rentEnd = component.get("v.rentEndValue");
            var rentEndDate = new Date(rentEnd);
            
            if(rentStartDate != null && rentStartDate != undefined)
            startRange = rentStartDate.getUTCFullYear();
            
            if(rentEndDate != null && rentEndDate != undefined)
            endRange = rentEndDate.getUTCFullYear();
            
            for(var i=startRange;i<=endRange; i++){
                console.log(i);
                if(i == endRange){
                    opts.push({"class": "optionClass", label: i, value:i,selected:true}); 
                }
                else{
                    opts.push({"class": "optionClass", label: i, value:i});
                }
            }
        }else{
            for(var i=startRange;i<=endRange; i++){
                console.log(i);
                if(i == endRange){
                    opts.push({"class": "optionClass", label: i, value:i,selected:true}); 
                }
                else{
                    opts.push({"class": "optionClass", label: i, value:i});
                }
            }  
        }
        
        var lowSeaEndYearId = component.find('lowSeaEndYearId');
        if(lowSeaEndYearId != undefined){
            lowSeaEndYearId.set("v.options", opts); 
        } 
        
        var fieldV = component.get("v.lowEndYearValue");
        if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
            this.showPreviousLowEndYEarPickList(component);       
    },
    
    //fetch rent due type picklist values
    getRentDueTypePickList: function(component) {
        console.log('getRentDueTypePickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Due_Type__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRentDueTypePickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getRentDueTypePickList  result:'+allValues);
                    component.set('v.rentDueTypePickListValue', allValues);
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var rentDueTypeId = component.find('rentDueTypeId');
                if(rentDueTypeId != undefined){
                    rentDueTypeId.set("v.options", opts); 
                }
                
                
            }
            var fieldV = component.get("v.rentDueTypeValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousRentDueTypePickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch rent due day picklist values for Fixed due type
    getRentDuedayFixedVal: function(component) {
        console.log('getRentDuedayFixedVal');
        var opts = [];
        var duetype = component.find("rentDueTypeId").get("v.value");
        var dueday = component.get('v.rentDueDayValue');
        var isEdit = component.get("v.isEdit");
        console.log("duetype",duetype);    
        
        if(duetype == 'Fixed' || duetype == undefined){
            console.log('inside if');
            if(isEdit){
                console.log('inside edit');
                var dueday = component.get('v.rentDueDayValue'); 
                for(var i=1;i<=31; i++){
                    if(i== dueday){
                        opts.push({"class": "optionClass", label: i, value:i,selected: true}); 
                    }
                    else{
                        opts.push({"class": "optionClass", label: i, value:i}); 
                    }
                }
                component.set("v.isFixedDueType",true);
            }
            else{
                console.log('inside new');
                for(var i=1;i<=31; i++) {
                    opts.push({"class": "optionClass", label: i, value:i});
                } 
            }
            var rentDueDayId1 = component.find('rentDueDayId1');
            if(rentDueDayId1 != undefined){
                rentDueDayId1.set("v.options", opts); 
            }   
            component.set("v.isFixedDueType",true);
        }else{
            component.set("v.isFixedDueType",false); 
        }
    },
    
    //fetch rent due day setting picklist values
    getRentDueSettingPickList: function(component) {
        console.log('getRentDueSettingPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Due_Day_Setting__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRentDueSettingPickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getRentDueSettingPickList  result:'+allValues);
                    component.set('v.rentDueSettingPickListValue', allValues);
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var isValidSetting = component.get("v.isValidSetting");
                if(isValidSetting){ 
                    console.log('if');
                    var rentDueSetting = component.find('settingId3'); 
                    if(rentDueSetting != undefined){
                        rentDueSetting.set("v.options", opts); 
                    }
                    
                }
            }
            var fieldV = component.get("v.rentDueSettingValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousRentDueSettingPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch rent due day correction picklist values
    getDueCorrectionPickList: function(component) {
        console.log('getDueCorrectionPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Due_Day_Correction__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getDueCorrectionPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getDueCorrectionPickList  result:'+allValues);
                    component.set('v.dueDateCorrectionPickListValue', allValues);
                    //opts.push({class: "optionClass",label: "--- None ---",value: ""});
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        if(response.getReturnValue()[i] == "None")
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: ""});
                        else 
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var dueDayCorrectionId = component.find('dueDayCorrectionId');
                if(dueDayCorrectionId != undefined){
                    dueDayCorrectionId.set("v.options", opts); 
                }
                
            }
            var fieldV = component.get("v.dueDateCorrectionValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousDueCorrectionPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch pro-rata number of days picklist values
    getProrataPickList: function(component) {
        console.log('getProrataPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Pro_rata_Number_Of_Days__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getProrataPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getProrataPickList  result:'+allValues);
                    component.set('v.prorataPickListValue', allValues);
                    //opts.push({class: "optionClass",label: "--- None ---",value: ""});
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        if(response.getReturnValue()[i] == "None")
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: ""});
                        else 
                            opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var prorataId = component.find('prorataId');
                if(prorataId != undefined){
                    prorataId.set("v.options", opts); 
                }
                
            }
            var fieldV = component.get("v.prorataValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousProrataPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    showPreviousEscPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.rentEscalationMonthValue");
        var valueList = component.get("v.rentEscalationMonthPickListValue");
        opts.push({class: "optionClass",label: "--- None ---",value: ""});
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var escalationMonthId = component.find('escalationMonthId');
        if(escalationMonthId != undefined){
            escalationMonthId.set("v.options", opts); 
        }
        
        console.log('onPreviousClick fieldV: '+fieldV+" "+valueList);
    },
    
    showPreviousRentDueDayPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.periodEndDayValue");
        console.log("showPreviousRentDueDayPickList fieldV :",fieldV);
        if(fieldV == null || fieldV == '' || fieldV == undefined){
            console.log('No Rent Period End Day Value for New methodolgy');
        }
        else{   
            var valueList = component.get("v.periodEndDayPickListValue");
            if(valueList !=null){
                console.log("showPreviousRentDueDayPickList valueList :",valueList);
                for(var i=0;i< valueList.length; i++) {
                    if(valueList[i] == fieldV) {
                        opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                    }else{
                        opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
                    }
                }
                console.log("showPreviousRentDueDayPickList opts :"+opts);
                console.log("showPreviousRentDueDayPickList find",component.find("rentPeriodEndDayId"));
            }
            var rentPeriodEndDayId = component.find('rentPeriodEndDayId');
            if(rentPeriodEndDayId != undefined){
                rentPeriodEndDayId.set("v.options", opts); 
            } 
        }
    },
    
    showPreviousInvoiceDayPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.invoiceDayValue");
        var valueList = component.get("v.invoiceDayPickListValue");
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var invoiceDayId = component.find('invoiceDayId');
        if(invoiceDayId != undefined){
            invoiceDayId.set("v.options", opts); 
        } 
        
    },
    
    showPreviousExtendedRentTypePickList: function(component){
        var opts = [];
        var fieldV = component.get("v.extendedRentType");
        var valueList = component.get("v.extendRentTypePickListValue");
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var extendedRentTypeId = component.find('extendedRentTypeId');
        if(extendedRentTypeId != undefined){
            extendedRentTypeId.set("v.options", opts); 
        }
        
        console.log('onPreviousClick fieldV: '+fieldV+" "+valueList);
        
    },
    
    showPreviousStartMonthPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.lowStartMOnthValue");
        var valueList = component.get("v.lowStartMOnthPickListValue");
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var lowSeaStarMonId = component.find('lowSeaStarMonId');
        if(lowSeaStarMonId != undefined){
            lowSeaStarMonId.set("v.options", opts); 
        } 
        
    },
    
    showPreviousEndMonthPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.lowEndMOnthValue");
        var valueList = component.get("v.lowEndMOnthPickListValue");
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var lowSeaEndMonId = component.find('lowSeaEndMonId');
        if(lowSeaEndMonId != undefined){
            lowSeaEndMonId.set("v.options", opts); 
        } 
        
    },
    
    showPreviousLowSeaPeriodPickList: function(component){
        console.log('showPreviousLowSeaPeriodPickList');
        var opts = [];
        var fieldV = component.get("v.lowSeasonPeriodValue");
        var valueList = component.get("v.lowSeasonPeriodPickListValue");
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var lowSeaPeriodId = component.find('lowSeaPeriodId');
        if(lowSeaPeriodId != undefined){
            lowSeaPeriodId.set("v.options", opts); 
        } 
        
    },
    
    showPreviousLowStartYEarPickList: function(component){
        console.log('showPreviousLowStartYEarPickList');
        var opts = [];
        var rentStart = component.get("v.rentStartValue");
        var rentStartDate = new Date(rentStart);
        var rentEnd = component.get("v.rentEndValue");
        var rentEndDate = new Date(rentEnd);
        
        var startRange = rentStartDate.getUTCFullYear();
        var endRange = rentEndDate.getUTCFullYear();
        var fieldV1 = component.get("v.lowStartYearValue");
        var fieldV2 = component.get("v.lowEndYearValue");
        console.log('showPreviousLowStartYEarPickList fieldV1 :'+fieldV1+'fieldV2: '+fieldV2);
        var valueList = component.get("v.lowStartYearPickListValue");
        for(var i=startRange;i<= endRange; i++) {
            console.log('showPreviousLowStartYEarPickList ',i);
            if(i == fieldV1){ 
                console.log('showPreviousLowStartYEarPickList if:');
                opts.push({"class": "optionClass", label: i, value: i, selected: true});
            }else{
                console.log('showPreviousLowStartYEarPickList else:');
                opts.push({"class": "optionClass", label: i, value: i});
            }
        }
        var lowSeaStarYearId = component.find('lowSeaStarYearId');
        if(lowSeaStarYearId != undefined){
            lowSeaStarYearId.set("v.options", opts); 
        } 
    },
    
    showPreviousLowEndYEarPickList: function(component){
        console.log('showPreviousLowEndYEarPickList');
        var opts = [];
        var rentStart = component.get("v.rentStartValue");
        var rentStartDate = new Date(rentStart);
        var rentEnd = component.get("v.rentEndValue");
        var rentEndDate = new Date(rentEnd);
        
        var startRange = rentStartDate.getUTCFullYear();
        var endRange = rentEndDate.getUTCFullYear();
        var fieldV1 = component.get("v.lowStartYearValue");
        var fieldV2 = component.get("v.lowEndYearValue");
        console.log('showPreviousLowEndYEarPickList fieldV1 :'+fieldV1+'fieldV2: '+fieldV2);
        var valueList = component.get("v.lowEndYearPickListValue");
        for(var i=startRange;i<=endRange; i++) {
            console.log('showPreviousLowEndYEarPickList',i);
            if(i == fieldV2){ 
                console.log('showPreviousLowEndYEarPickList if:');
                opts.push({"class": "optionClass", label: i, value: i, selected: true});
            }else{
                console.log('showPreviousLowEndYEarPickList else:');
                opts.push({"class": "optionClass", label: i, value: i});
            }
        }
        var lowSeaEndYearId = component.find('lowSeaEndYearId');
        if(lowSeaEndYearId != undefined){
            lowSeaEndYearId.set("v.options", opts); 
        } 
    },
    
    showPreviousRentDueTypePickList: function(component){
        var opts = [];
        var fieldV = component.get("v.rentDueTypeValue");
        console.log('showPreviousRentDueTypePickList fieldV ',fieldV);
        if(fieldV == 'Fixed'){
            component.set("v.isValidSetting",true);   
            component.set("v.isFixedDueType",true); 
        }
        else{
            
            component.set("v.isValidSetting",false);   
            component.set("v.isFixedDueType",false); 
            
        }
        var valueList = component.get("v.rentDueTypePickListValue");
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var rentDueTypeId = component.find('rentDueTypeId');
        if(rentDueTypeId != undefined){
            rentDueTypeId.set("v.options", opts); 
        } 
        
    },
    
    showPreviousRentDueSettingPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.rentDueSettingValue");
        var valueList = component.get("v.rentDueSettingPickListValue");
        if(valueList != null){
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var isValidSetting = component.get("v.isValidSetting");
        if(isValidSetting){ 
            var rentDueSetting = component.find('settingId3'); 
            if(rentDueSetting != undefined){
                rentDueSetting.set("v.options", opts); 
            }
            
        }
        else{
            var rentDueSetting = component.find('rentDueSettingId'); 
            if(rentDueSetting != undefined){
                rentDueSetting.set("v.options", opts); 
            }
        }
        
        
    },
    
    showPreviousDueCorrectionPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.dueDateCorrectionValue");
        var valueList = component.get("v.dueDateCorrectionPickListValue");
        if(valueList != null){
            // opts.push({class: "optionClass",label: "--- None ---",value: ""});
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else if(valueList[i] == "None")
                    opts.push({"class": "optionClass", label: valueList[i], value: ""});
                    else
                        opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var dueDayCorrectionId = component.find('dueDayCorrectionId');
        if(dueDayCorrectionId != undefined){
            dueDayCorrectionId.set("v.options", opts); 
        } 
        
    },
    
    showPreviousProrataPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.prorataValue");
        var valueList = component.get("v.prorataPickListValue");
        if(valueList != null){
            // opts.push({class: "optionClass",label: "--- None ---",value: ""});
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else if(valueList[i] == "None")
                    opts.push({"class": "optionClass", label: valueList[i], value: ""});
                    else 
                        opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var prorataId = component.find('prorataId');
        if(prorataId != undefined){
            prorataId.set("v.options", opts); 
        } 
        
    },
    
    //generate Seasonal Rent MultiRent Schedule
    generateData: function(component) {
        console.log('generateData Seasonal rent');
        var self = this;
        var multiRentData = component.get("v.multiRent");
        var multiRtId = null;
        var isEdit = component.get("v.isEdit");
        console.log("generateData isEdit: "+ isEdit);
        if(isEdit && !$A.util.isEmpty(multiRentData) && !$A.util.isUndefined(multiRentData))
            multiRtId = multiRentData.RentId;
        var params = this.getParameters(component,multiRtId);
        console.log("generateData params :"+JSON.stringify(params));
        
        var action = component.get("c.generateVariableRentSchedule");
        action.setParams({jsonString : JSON.stringify(params) });
        console.log('generateData setparamss ');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("generateData response-state: "+state);
            if (state === "SUCCESS") {
                var rentList = response.getReturnValue();
                if(rentList != null){
                    console.log("generateData response: "+JSON.stringify(rentList));
                    component.set("v.multiRent",rentList); 
                    self.showPreviewScreen(component);
                    //data saved Successfully
                    //Fire the refresh view event to update Account detail view
                    //$A.get('e.force:refreshView').fire();
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("generateData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    self.showErrorMsg(component, "Error in generating rent schedule record");
                }
                component.set("v.showOutput", false);
                component.set("v.showRentAmount", false);
                component.set("v.showRentPeriod", false);
                component.set("v.showRentDue", false);
                component.set("v.showPreview", true);
                
                component.set("v.isLoading", false);
            }
        });
        $A.enqueueAction(action);  
    },
    
    //fetch SLR related details
    fetchSLRData: function(component) {
        console.log("fetchSLR");
        var multiRentData = component.get("v.multiRent");
        console.log("fetchSLR",multiRentData.RentId);
        var action = component.get("c.getSLROutputData");
        action.setParams({
            steppedRentId : multiRentData.RentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("fetchSLR state: "+state);
            if (state === "SUCCESS") {
                var rentList = response.getReturnValue();
                if(rentList != null){
                    console.log("fetchSLR response: "+rentList);
                    component.set("v.slrRentGeneratedList",rentList); 
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("fetchSLR errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    self.showErrorMsg(component, "Error in fetching slr record");
                }
            }
            component.set("v.isLoading", false);
            console.log("fetchSLR loading "+component.get("v.isLoading"));
        });
        $A.enqueueAction(action);  
    },
    
    //show all multirent data parameters
    showPreviewScreen: function(component) {
        console.log('showPreviewScreen');
        var self = this;
        component.set("v.showRentAmount", false);
        component.set("v.showRentPeriod", false);
        component.set("v.showRentDue", false);
        component.set("v.showPreview", false);
        
        component.set("v.showOutput", true);
        
        var multiRentData = component.get("v.multiRent");
        if(multiRentData != null){
            console.log("showPreviewScreen multiRentData "+JSON.stringify(multiRentData));
            component.set("v.baseRentValue",multiRentData.BaseRent);
            component.set("v.offWingRentValue",multiRentData.OffWingRent);
            component.set("v.rentEscalationValue", multiRentData.Escalation);
            component.set("v.rentEscalationMonthValue", multiRentData.EscalationMonth);
            component.set("v.rentStartValue",multiRentData.StartDate);
            component.set("v.rentEndValue", multiRentData.EndDate);
            component.set("v.invoiceDayValue", multiRentData.InvoiceGenerationDay);
            component.set("v.periodEndDayValue", multiRentData.RentPeriodEndDay);
            component.set("v.rentDueTypeValue", multiRentData.RentDueType);
            component.set("v.rentDueSettingValue", multiRentData.RentDueSetting);
            component.set("v.rentDueDayValue", multiRentData.RentDueDay);
            component.set("v.prorataValue", multiRentData.ProrataNumberOfDays);
            component.set("v.dueDateCorrectionValue", multiRentData.DueDayCorrection);
            component.set("v.lowSeasonRentValue" , multiRentData.LowSeasonRent);
            component.set("v.lowStartMOnthValue", multiRentData.LowSeasonStartMonth);
            component.set("v.lowStartYearValue", multiRentData.LowSeasonStartYear);
            component.set("v.lowEndYearValue", multiRentData.LowSeasonEndYear);
            component.set("v.lowEndMOnthValue", multiRentData.LowSeasonEndMonth);
            component.set("v.lowSeasonPeriodValue", multiRentData.LowSeasonPeriod);
            component.set("v.slrCheck", multiRentData.SLRGenerateCheck);
            component.set("v.slrOpeningBalance", multiRentData.SLROpeningBalance);
            component.set("v.slrStartDateValue", multiRentData.SLRStartDate);
            component.set("v.slrEndDateValue", multiRentData.SLREndDate);
            
            
            component.set("v.differentStartDate", multiRentData.DifferentStartDate);
            component.set("v.secondRentStartValue", multiRentData.SecondStartDate);        
            component.set("v.isExtendedRent", multiRentData.isExtendedRent);
            component.set("v.extendedRentType", multiRentData.ExtendedRentType);
            if(multiRentData.Status == 'InActive'){
                component.set("v.isActive",false);}
            else{
                component.set("v.isActive",true);}
            console.log('showPreviewScreen');
            self.getRentScheduleData(component, multiRentData);
        }
    },
    
    //fetch multirent data parameters
    getRentScheduleData : function(component, multiRentData) {
        console.log('getRentScheduleData');
        var action = component.get("c.getOutputData");
        action.setParams({
            steppedRentId : multiRentData.RentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getRentScheduleData state: "+state);
            if (state === "SUCCESS") {
                var rentList = response.getReturnValue();
                if(rentList != null){
                    console.log("getRentScheduleData response: "+rentList);
                    component.set("v.rentGeneratedList",rentList);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getRentScheduleData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in fetching rnet schedule record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);  
    },
    
    //delete multirent schedule record
    deleteMultiRentSch : function(component) {
        console.log('deleteMultiRentSch');
        var multiRentData = component.get("v.multiRent");
        var action = component.get("c.deleteMultiRent");
        var self= this;
        action.setParams({
            steppedRentId : multiRentData.RentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("deleteMultiRentSch response-state: "+state);
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if(res != null){
                    console.log("deleteMultiRentSch response: "+res);
                    if($A.util.isEmpty(res) || $A.util.isUndefined(res) || res == null)
                        self.showDeleteMsg(component, 'Success');
                    else
                        self.showErrorMsg(component, res);
                    self.getRentScheduleData(component, multiRentData);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("deleteMultiRentSch errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in deleting multirent record");
                }
                component.set("v.isLoading", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    //used to display error message
    showErrorMsg : function(component, msg) {
        console.log('showErrorMsg');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'1000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
    //used to display message when user deletes multirent record
    showDeleteMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Successfully Deleted!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    //used to display the warning message
    showWarningMsg : function(component, title, msg) {
        console.log('showWarningMsg');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'warning',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    //change multiRent status
    changeMultiRentStatus : function(component) {
        console.log("changeMultiRentStatus");
        var st = component.get("v.isActive");
        var multiRentData = component.get("v.multiRent");
        console.log("changeMultiRentStatus isActive "+st + " "+multiRentData);
        if(multiRentData===null)return;
        var action = component.get("c.updateMultiRentStatus");
        action.setParams({
            multiRentId : multiRentData.RentId,
            status : st
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("changeMultiRentStatus response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result != null){
                    console.log("changeMultiRentStatus response: "+result);
                    if(st == true)
                        multiRentData.Status = "Active";
                    else
                        multiRentData.Status = "InActive";
                    component.set("v.multiRent", multiRentData);
                    console.log("changeMultiRentStatus after update: "+JSON.stringify(multiRentData));
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("changeMultiRentStatus errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    self.showErrorMsg(component, "Error in changing the multirent record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);    
    },
    
    getSecondRentStartDate : function(component) {
        var edit = component.get("v.isEdit");
        console.log("getSecondRentStartDate Edit : "+edit); 
        
        if(edit){
            var differentStartFlag = component.get("v.differentStartDate");
            var resultcmp1 = component.find("differentStart");
            resultcmp1.set("v.value",differentStartFlag);
            
            var secondStartDate = component.get("v.secondRentStartValue");
            
            if(component.get("v.differentStartDate")){
                var resultcmp2 = component.find("secondRentDateValue2");
                resultcmp2.set("v.value",secondStartDate);
            }
            else{
                var resultcmp2 = component.find("secondRentDateValue4");
                resultcmp2.set("v.value",secondStartDate);
            }
        }
        else{
            console.log("getSecondRentStartDate Edit else: "+edit); 
            
            component.set("v.isSecondRentStartErrorMsg", false);
            component.set("v.secondRentStartErrorMsg" ,"");
            
            component.set("v.differentStartDate",false);
            component.set("v.secondRentStartValue","");
            
            var differentStartFlag = component.get("v.differentStartDate");
            var secondStartDate = component.get("v.secondRentStartValue");
        }
    },
    
    getMonthValue : function(component,month){
        console.log('getMonthValue');
        var d = month.toLowerCase();
        var months = ["january","february","march","april","may","june","july","august","september","october","november","december"];
        console.log('getMonthValue 2 d :'+(months.indexOf(d)+1));
        return (months.indexOf(d)+1);
    },
    
    //Json String formation for Rent Schedule generation
    getParameters : function(component,multiRtId){ 
        return {'nameStr' : component.get("v.nameValue"),
                'rentTypeStr' : component.get("v.typeValue"),
                'periodStr' : component.get("v.periodValue"),
                'commentStr' : component.get("v.commentValue"),
                'isRentExtendedStr':component.get("v.isExtendedRent"),
                'extendedRentTypeStr' : component.get("v.extendedRentType"),
                'includeCommentStr' : component.get("v.includeComment"),
                'baseRentStr' : component.get("v.baseRentValue"),
                'lowSeasonRentStr' : component.get("v.lowSeasonRentValue"),
                'offWingRentStr' : component.get("v.offWingRentValue"),
                'rentEscalationStr' : component.get("v.rentEscalationValue"),
                'rentEscalationMonthStr' : component.get("v.rentEscalationMonthValue"),
                'SLRGenerateCheckStr' : component.get("v.slrCheck"),
                'SLROpeningBalanceStr' : component.get("v.slrOpeningBalance"),
                'SLRStartDateStr' : component.get("v.slrStartDateValue"),
                'SLREndDateStr' : component.get("v.slrEndDateValue"),
                'rentStartStr' : component.get("v.rentStartValue"),
                'rentEndStr' : component.get("v.rentEndValue"),
                'lowStartMonthStr' : component.get("v.lowStartMOnthValue"),
                'lowEndMonthStr' : component.get("v.lowEndMOnthValue"),
                'lowStartYearStr' : component.get("v.lowStartYearValue"),
                'lowEndYearStr' : component.get("v.lowEndYearValue"),
                'lowSeaPeriodStr' : component.get("v.lowSeasonPeriodValue"),
                'invoiceDayStr' : component.get("v.invoiceDayValue"),
                'periodEndDayStr' : component.get("v.periodEndDayValue"),
                'rentDueTypeStr' : component.get("v.rentDueTypeValue"),
                'rentDueSettingStr' : component.get("v.rentDueSettingValue"),
                'rentDueDayStr': component.get("v.rentDueDayValue"),
                'differentStartStr' : component.get("v.differentStartDate"),
                'secondRentStartStr' : component.get("v.secondRentStartValue"),
                'prorataStr' : component.get("v.prorataValue"),
                'dueDateCorrectionStr' : component.get("v.dueDateCorrectionValue"),
                'leaseIdStr': component.get('v.leaseId'),
                'multiRentIdStr' : multiRtId,
                'isFromLE' : component.get("v.isFromLE")};
    },
})