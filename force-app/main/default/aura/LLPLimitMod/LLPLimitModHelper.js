({
    getLLPLimits: function(component) {
        var year = component.get("v.currentYear");
        console.log('getLLPLimits yr '+year);
        var self = this;
        var action = component.get("c.getLLPLimitData");
        action.setParams({
            llpTemplateId : component.get("v.LLPTemplateId"),
            catalogYear : year
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('getLLPLimits state: '+ state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();    
                console.log('getLLPLimits allValues '+ allValues);
                //if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues)) 
                	//console.log('getLLPLimits allValues '+JSON.stringify(allValues));
                component.set("v.LLPCatalogData", allValues);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getLLPLimits errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("getLLPLimits: Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    
    getThrustPicklistValues : function(component) {
        console.log('getThrustPicklistValues  ');
        var self = this;
        var action = component.get("c.getThrustPicklist");
        action.setParams({
            engineTemplateId : component.get("v.templateId")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('getThrustPicklist state: '+ state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();    
                //console.log('getThrustPicklist allValues '+ JSON.stringify(allValues));
                component.set('v.thrustPicklist', allValues);
                
                var opts = [];
                for(var i=0;i< allValues.length; i++){
                    opts.push({"class": "optionClass", label: allValues[i].Name, value: allValues[i].Id});
                }
                component.set("v.thrustOptions", opts);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getThrustPicklist errors "+JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
    },
    
    updateLLPCdata : function(component) {
        var data = component.get("v.LLPCatalogData");
        var self = this;
        console.log("updateLLPCdata data: "+data);
        
        var newData = component.get("v.newLLPCatalogData");
        var newDataJsonStr = null;
        console.log('updateLLPCdata newData: ' +newData);
        
        if(!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData)) {
            console.log('updateLLPCdata data:dtr '+ JSON.stringify(newData));  
            var nonNullData = [];
            var j = 0;
            for(var i = 0; i < newData.length; i++) {
                if(!$A.util.isEmpty(newData[i]) && !$A.util.isUndefined(newData[i])) {
                    /*console.log('updateLLPCdata newData[i] '+JSON.stringify(newData[i].Thrust) + " i:"+i);
                    if(!$A.util.isEmpty(newData[i].Thrust) && !$A.util.isUndefined(newData[i].Thrust)) {
                        newData[i].ThrustId =  newData[i].Thrust.Id;
                        newData[i].ThrustName =  newData[i].Thrust.Name;
                        delete newData[i].Thrust;
                        nonNullData[j] =  newData[i];
                    }
                    else { */
                    if(!$A.util.isEmpty(newData[i].PartNumber) || 
                       !$A.util.isEmpty(newData[i].LifeLimitCycles) || 
                       !$A.util.isEmpty(newData[i].Source)) {
                        nonNullData[j] = newData[i];
                    	j++;
                    }
                    //}
                	
                }
            }
            console.log('updateLLPCdata data:nonNullData em '+ JSON.stringify(nonNullData));  
            newDataJsonStr = JSON.stringify(nonNullData);
        }
        if((!$A.util.isEmpty(data) && !$A.util.isUndefined(data)) ||
        (!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData))){
            component.set("v.isLoading", true);
            console.log("updateLLPCdata data:str "+JSON.stringify(data));
            var dataJsonStr = JSON.stringify(data);
            var yr = component.get("v.currentYear");
            var action = component.get("c.updateLLDLimitData");
            action.setParams({
                dataStr : dataJsonStr,
                newDataStr : newDataJsonStr,
                LLPTemplateId : component.get("v.LLPTemplateId"),
                catalogYear : yr
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('updateLLPCdata state: '+ state);
                if (state === "SUCCESS") {
                    var allValues = response.getReturnValue();    
                    console.log('updateLLPCdata allValues '+allValues);
                    self.getLLPLimits(component);
                    self.resetEdit(component);
                }
                else if (state === "ERROR") {
                    component.set("v.isLoading", false);
                    var errors = response.getError();
                    console.log("errors "+errors);
                    component.set("v.isLLPLEdit", true);
                    if (errors) {
                        console.log("updateLLPCdata errors "+JSON.stringify(errors));
                        console.log("Error message: err " + JSON.stringify(errors[0]));
                        if (errors[0] && errors[0].pageErrors) {
                            console.log("Error message: " + errors[0].pageErrors[0].message);
                            self.showErrorMsg(component, errors[0].pageErrors[0].message);
                        }
                        else if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            if(errors[0].message.includes('Duplicate part number')) {
                                self.showErrorMsg(component, 'Duplicate Part number and Thrust is not allowed!');
                                self.getLLPLimits(component);
                                self.resetEdit(component);
                            }
                            else
                                self.showErrorMsg(component, errors[0].message);
                        }
                        else {
                            console.log("Unknown error");
                            self.showErrorMsg(component, "Error in creating record "+JSON.stringify(errors));
                        }
                    } else {
                        console.log("Unknown error");
                        self.showErrorMsg(component, "Error in creating record ");
                    }
                }
            });
            $A.enqueueAction(action);
           // component.set("v.isLoading", false);
        } 
    },
    
    resetEdit : function(component) {
        component.set("v.llpcRowIndex", "0");
        component.set("v.isLLPLEdit", false);
        component.set("v.body", []);
        component.set("v.newLLPCatalogData", []);
        component.set("v.llpcDynamicRowTotalCount", 0);
    },
    
    deleteLLPCData : function(component, llpRecord, rowIndex) {
        var action = component.get("c.deleteLLPLimit");
        var self = this;
        action.setParams({
            llpcId : llpRecord
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('deleteLLPCData state: '+ state);
            if (state === "SUCCESS") {
                console.log('rowIndex '+rowIndex + 'dele '+document.getElementById("llpctblLocationsLimit"));
                self.resetEdit(component);
                self.getLLPLimits(component);
                document.getElementById("llpctblLocationsLimit").deleteRow(rowIndex);
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("deleteLLPCData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    saveSorting : function(component) {
        var oTable = document.getElementById('llpctblLocationsLimit');
        //gets rows of table
        var rowLength = oTable.rows.length;
        var tempData = component.get("v.LLPCatalogData");
        var newData = component.get("v.newLLPCatalogData");
        
        //loops through rows    
        var index = 0;
        for (var i = 0; i < rowLength; i++){
            var rowId = oTable.rows.item(i).id;
            console.log("row id "+ rowId);
            if(!$A.util.isEmpty(rowId) && !$A.util.isUndefined(rowId))  {
                index = index + 1;
                
                console.log("row id "+ rowId + ' i:'+i +' index:' +index);
                if(rowId.includes('index_')){
                    var res = rowId.split("index_");
                    console.log('split '+res[1]);
                    console.log('split newData[res[1]]: '+newData[res[1]]);
                    if(!$A.util.isEmpty(newData[res[1]]) && !$A.util.isUndefined(newData[res[1]])) 
                    	newData[res[1]].Sorting = index;
                }
                else if(!$A.util.isEmpty(tempData) && !$A.util.isUndefined(tempData)) {
                    for(var j =0; j< tempData.length; j++) {
                        //console.log("equals out "+tempData[j].Id+ ' rowId: '+rowId);
                        if(tempData[j].Id == rowId) {
                            //console.log("equals "+tempData[j].Id+ ' '+rowId);
                            tempData[j].Sorting = index;
                        }
                    }
                }
            }
        }
        if(!$A.util.isEmpty(tempData) && !$A.util.isUndefined(tempData))  {
            console.log("after sorting tempData: "+JSON.stringify(tempData));
            component.set("v.LLPCatalogData", tempData);
        }
        
        if(!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData))  {
            console.log("after sorting newData: "+JSON.stringify(newData));
            component.set("v.newLLPCatalogData", newData);    
        }
            
    },
    
    addDynamicRow : function(component){
        var index = component.get("v.llpcRowIndex");
        var self = this;
        var namespace = component.get("v.namespace");
        //var resLoc =   $A.get('$Resource.MRCompsIcons') + '/MRCompsIcons/LLP.svg';
        //onsole.log('addDynamicRow resLoc: '+resLoc);
        // Array of components to create
        console.log('addDynamicRow '+ index);
        var opts = component.get("v.thrustOptions");
        var newComponents = [];
        newComponents.push(["aura:html", { //1
            "tag": "tr",
            "HTMLAttributes": {
                "id": "index_"+index
            }
        }]);
        
        newComponents.push(["aura:html", { //2
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", { //3
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle mouseStyle",
                "id" : "dateDivIndex_"+index,
                "onclick" : component.getReference("c.deleteRow")
            }
        }]);
        newComponents.push(["lightning:icon", {  //4
            				"iconName" : "utility:clear",
            				"size" :"xx-small"
        					}
                           ]);
        /*newComponents.push(["aura:html", { //4
            "tag": "img",
            "HTMLAttributes": {
                "class": "imgStyle",
                "src": resLoc
            }
        }]); */
        
        
        //2nd column
        newComponents.push(["aura:html", { //5
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //6
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //7
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].PartNumber")
        					}
                           ]);
        
        //3rd column
        newComponents.push(["aura:html", {  //8
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //9
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "style": "margin-top:0px;",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
            
      /*  newComponents.push(["leaseworks:CustomLookupField", {  //10
            				"objectAPIName" : namespace+"Custom_Lookup__c",
            				"criteriaFieldName" : namespace +"Lookup_Type__c",
            				"criteriaValue" : "Engine",
            				"IconName":"standard:note" ,
            				"selectedRecord" : component.getReference("v.newLLPCatalogData["+index+"].Thrust"),
            				"SearchKeyWord" :component.getReference("v.newLLPCatalogData["+index+"].Thrust")
        					}
                           ]); */
        
        newComponents.push(["ui:inputSelect", {  //10
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].ThrustId")
        					}
                           ]);
        
        //4th column
        newComponents.push(["aura:html", {  //14
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //15
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //16
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].LifeLimitCycles")
        					}
                           ]);
        
        //5th column
        newComponents.push(["aura:html", {  //11
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //12
            "tag": "div",
            "HTMLAttributes": {
                "id": "dynamicField_"+index,
                "class": "tdTextStyle",
                "style" : "text-align:center",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //13
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].Source")
        					}
                           ]);
        
        $A.createComponents(newComponents,
                            function (components, status, errorMessage) {
                                if (status === "SUCCESS") {
                                    var pageBody = component.get("v.body");
                                    var tr = components[0];
                                    var trBody = tr.get("v.body");
                                    
                                    for (var i = 1; i < components.length; i += 3) {                                           
                                        var tdDt = components[i];
                                        var tdDtBody = tdDt.get("v.body");
                                        var divDt = components[i+1];
                                        var divDtBody = divDt.get("v.body");
                                        var ip = components[i+2];
                                        if((i+2) == 9) {
                                            ip.set("v.options", opts);
                                        }
                                        divDtBody.push(ip);
                                        divDt.set("v.body", divDtBody);
                                        tdDtBody.push(divDt);
                                        tdDt.set("v.body", tdDtBody);
                                        trBody.push(tdDt);
                                    }
                                    tr.set("v.body", trBody);
                                    pageBody.push(tr);
                                    component.set("v.body", pageBody);
                                    index++;
                                    component.set("v.llpcRowIndex", index);
                                    
                                    var dyCount = component.get("v.llpcDynamicRowTotalCount");
                                    dyCount++;
                                    component.set("v.llpcDynamicRowTotalCount" , dyCount);
                                    
                                    console.log("Component has been added ");
                                }
                                else // Report any error
                                {
                                    console.log("Error", "Failed to create list components.");
                                }
                            }
                           );
        
    },
        
    disableTableDrag : function(component, event, helper) {
       // console.log("disableDrag");
      /*  jQuery("document").ready(function(){
         //   console.log("disableDrag inside");
            var $sortableTable  = $("#llpctblLocations tbody");
            $sortableTable.sortable( "disable" );
        }); */
    },
    
    enableTableDrag: function(component, event, helper) {
        //console.log("enableDrag");
      /*  jQuery("document").ready(function(){
          //  console.log("enableDrag inside");
            var $sortableTable  = $("#llpctblLocations tbody");
            $sortableTable.sortable( "enable" );
        }); */
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
     openPop : function(component,event) {
        var cmpTarget = component.find('pop');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        var popup = component.find("pop").getElement();
        if(popup) {
            var offset = component.find('parentDiv').getElement().getBoundingClientRect().top + 
                document.documentElement.scrollTop ;
            var topPadding = (event.clientY + document.documentElement.scrollTop) - (offset + 40);
            var leftOffset = component.find('parentDiv').getElement().getBoundingClientRect().left;
            var documentWidth = document.documentElement.clientWidth - 20;
            var left = event.clientX - leftOffset;
            
            popup.style.top = (topPadding)+'px';
            popup.style.left = (left - 10)+'px';
            popup.style.display = "inline";
            popup.style.position = "absolute";
        }
    },
   
    closePopup : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
    
    
})