({
    doInit : function(component, event, helper) {
        //component.set("v.isLoading", true);
        console.log('doinit limit  ');
        helper.getThrustPicklistValues(component);
    }, 
    
    refreshData : function(component, event, helper) {
        component.set("v.isLoading", true);
        var currentYr = component.get("v.currentYear");
        console.log('currentYr '+currentYr);
        
        var llpTemplId = component.get("v.LLPTemplateId");
        console.log("refreshData llpTemplId: "+llpTemplId);
        
        if(!$A.util.isEmpty(llpTemplId) && !$A.util.isUndefined(llpTemplId) && 
           !$A.util.isEmpty(currentYr) && !$A.util.isUndefined(currentYr)){
        	helper.getLLPLimits(component);
        }
    }, 
        
    disableDrag : function(component, event, helper) {
        //console.log("disableDrag");
        //helper.disableTableDrag(component, event, helper);
    },
    
    enableDrag: function(component, event, helper) {
        //console.log("enableDrag");
        //helper.enableTableDrag(component, event, helper);
    },
    
    loadJs : function(component, event, helper) {
    }, 
    
    addRow: function(component, event, helper) {
        helper.addDynamicRow(component);
    },
    
    editRow : function(component, event, helper) {
        var isEdit = component.get("v.isLLPLEdit");
        console.log("editRow "+isEdit);
        
        helper.resetEdit(component);
        
        component.set("v.isLLPLEdit", true);
        helper.enableTableDrag(component, event, helper);
        
    },
    
    saveRow : function(component, event, helper) {
        var isEdit = component.get("v.isLLPLEdit");
        console.log("saveRow "+isEdit);
        
        //component.set("v.isEdit", false);
        
        helper.disableTableDrag(component, event, helper);
        
       // helper.saveSorting(component);
        helper.updateLLPCdata(component);
        
        
    },
    
    cancelRow : function(component, event, helper) {
        var isEdit = component.get("v.isLLPLEdit");
        console.log("cancelRow "+isEdit);
        component.set("v.isLoading", true);
        helper.resetEdit(component);
        helper.disableTableDrag(component, event, helper);
        helper.getLLPLimits(component);
    },
    
    onCatalogYearChange: function(component, event, helper) {
        var value = component.get("v.currentYear");
        console.log("onCatalogYearChange value: "+value);
        var llpTemplId = component.get("v.LLPTemplateId");
        console.log("onCatalogYearChange llpTemplId: "+llpTemplId);
        if(!$A.util.isEmpty(llpTemplId) && !$A.util.isUndefined(llpTemplId) &&
           !$A.util.isEmpty(value) && !$A.util.isUndefined(value)){
            helper.resetEdit(component);
            helper.disableTableDrag(component, event, helper);
            helper.getLLPLimits(component);
        }
        
    },
    
    deleteRow : function(component, event, helper) {
        component.set("v.deleteLLPCRecordId", "");
        component.set("v.deleteLLPCRowIndex", "");
        
        var recordId = event.currentTarget.id;
        console.log("deleteRow "+recordId);
        var target = event.currentTarget;
        
        var rowIndex = target.parentNode.parentNode.rowIndex;
        console.log("Row No : " + rowIndex + ' target:');
        
        if(recordId.includes("dateDivIndex")) {
            var dynamicRowTotalCount = component.get("v.llpcDynamicRowTotalCount");
            console.log("Deleting dynamically created row "+dynamicRowTotalCount);
            if(dynamicRowTotalCount == 1) {
                component.set("v.llpcRowIndex", "0");
                component.set("v.body", []);
                component.set("v.newLLPCatalogData", []);
                component.set("v.llpcDynamicRowTotalCount", 0);
            }
            else{
                console.log("Row deleted else "+rowIndex );
                var tablee = document.getElementById("llpctblLocationsLimit")
                tablee.deleteRow(rowIndex);
                console.log("Row deleted "+rowIndex + " tablee: "+tablee);
            }
            dynamicRowTotalCount = dynamicRowTotalCount - 1;
            console.log("Deleting dynamically created row after "+dynamicRowTotalCount);
            component.set("v.llpcDynamicRowTotalCount", dynamicRowTotalCount);
        }
        else{
            console.log("Deleting data in db");
            component.set("v.deleteLLPCRecordId", recordId);
            component.set("v.deleteLLPCRowIndex", rowIndex);
            component.set("v.isllpcOpen", true);
            //helper.deleteLLPData(component, recordId, rowIndex);
            
        }
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isllpcOpen", false);
    },
    
    showRecord : function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord " + recordId);
        window.open('/' + recordId);  
    },
    
    confirmDelete: function(component, event, helper) {
        component.set("v.isllpcOpen", false);
        console.log("Deleting data in db");
        var recordId = component.get("v.deleteLLPCRecordId");
        var rowIndex = component.get("v.deleteLLPCRowIndex");
        component.set("v.isLoading", true);
        helper.deleteLLPCData(component, recordId, rowIndex);
    },
        
    onfloatingRentChanges: function(component,event,helper) {
        var floatingVal = component.get("v.floatingRentLookUpRecord")
        console.log("floatingRentChanges floatingRentValue "+JSON.stringify(floatingVal));   
    },
    
    showPopup: function(component, event, helper) {
        var isPopup = component.get("v.isPopupShown");
        if(isPopup == true) 
            return;
        helper.openPop(component,event);
        component.set("v.isPopupShown", true);
    },
    
    hidePopup: function(component, event, helper) {
        helper.closePopup(component,event,helper);
        component.set("v.isPopupShown", false);
    }
})