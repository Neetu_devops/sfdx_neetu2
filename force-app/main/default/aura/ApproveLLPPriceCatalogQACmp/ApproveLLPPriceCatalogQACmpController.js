({
	saveClickHandler: function(component, event, helper) {
		console.log('saveClickHandler - Start');
        //helper.showSpinner(component);
		const action = component.get("c.processAction");
        action.setParams({
                            recordId : component.get("v.recordId")
                        });
		action.setCallback(this, function(response){
			var state = response.getState();
            if (state === "SUCCESS") {
                helper.handleErrors('Approved.', 'Success');
            	$A.get("e.force:closeQuickAction").fire();
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if(errors){
                    helper.handleErrors(errors);
                }
            }
            //helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
        console.log('saveClickHandler - End');
	},
    cancelClickHandler: function(component, event, helper) {
		console.log('cancelClickHandler');
        $A.get("e.force:closeQuickAction").fire();
	},
})