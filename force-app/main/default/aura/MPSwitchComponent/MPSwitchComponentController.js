({
    doInit : function(component, event, helper) {
        console.log("doInit: " + component.get('v.recordId'));
        helper.getParamValue('MPSwitch', 'c__recordId', component);
        helper.fetchAssetName(component);
        helper.fetchMPName(component);
        helper.fetchTable1Data(component);
        
        
        console.log('AssetWrapper : ' +component.get("v.assetWrapper"));
        console.log('AssemblyWrapper : ' +component.get("v.assemblyWrapper"));
    },
    
    openRecord: function(component,event,helper) {
        var record = event.currentTarget.id;
        console.log("openRecord "+record);
        window.open('/' + record);  
    },
    
    onMPChanges: function(component,event,helper) {
        console.log("onMPChanges mpVal ");
        var mpVal = component.get("v.mpNameNewLookUpRecord");
        console.log("onMPChanges mpVal "+JSON.stringify(mpVal));   
        if(!$A.util.isEmpty(mpVal.Id) || !$A.util.isUndefined(mpVal.Id)) {
        console.log("onMPChanges mpVal " +mpVal.Id);   
        helper.getmpValData(component, mpVal.Id);
        }
        
    },
    
    showRecord: function(component,event,helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord Fixed wizard "+recordId);
        window.open('/' + recordId);  
    },
    
    onCancelClick :function(component,event,helper) {
        console.log('onCancelClick');
        helper.assetPagePreview(component);
    },
    
    submitData : function(component,event,helper) {
        console.log('submitData');
        helper.changeMP(component);
        helper.assetPagePreview(component);
        
    },
    
    closeModel : function(component,event,helper){
        console.log('closeModel');
        component.set("v.noMpSave",false);
    },
    
    confirmUnlink: function(component,event,helper){
        console.log('confirmUnlink');
        component.set("v.isunlink",false);
         component.set("v.noMpSave",false);
        var action = component.get("c.changeMPonAsset");
        action.setParams({
            mpId : null,
            assetId : component.get("v.recordId")
            });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("changeMP response-state: "+state);
        });
         $A.enqueueAction(action); 
        helper.assetPagePreview(component);
    },
    
})