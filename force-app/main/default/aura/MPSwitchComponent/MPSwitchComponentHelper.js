({
	getParamValue : function( tabName, paramName,component ) {
 		var url = window.location.href;
        var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&amp;amp;amp;');
        var paramValue = '';
        console.log("getParamValue url "+url);
        console.log("getParamValue allParams "+allParams);
        for(var i=0; i < allParams.length; i++) {
            if(allParams[i].split('=')[0] == paramName)
                paramValue = allParams[i].split('=')[1];
        }
        console.log("getParamValue: " + paramValue);
        component.set('v.recordId', paramValue);
        this.getNamespacePrefix(component);
    }, 
    
    getNamespacePrefix : function(component){
        console.log('getNamespacePrefix');
        var self = this;
        var action = component.get("c.getNamespacePrefix");
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            console.log("getNamespacePrefix response: "+namespace);
            component.set("v.namespace",namespace);
         });
        $A.enqueueAction(action);  
   },
    
    fetchTable1Data : function(component){
      console.log('fetchTable1Data : assetid :'+component.get("v.recordId"));
      var action = component.get("c.getTable1Data");
        action.setParams({
            refrenceId : component.get("v.recordId"),
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("fetchTable1Data response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.length > 0){
                    console.log('fetchTable1Data result '+JSON.stringify(result));
                    component.set("v.assemblyWrapper", result);
                    component.set("v.showLoadingSpinner",false);
                }else{
                    component.set("v.showLoadingSpinner",true);
                }
             }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("fetchTable1Data errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("fetchTable1Data Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action); 
        
    },
    
    fetchMPName : function(component){
      console.log('fetchMPName :'+component.get("v.recordId"));
      var action = component.get("c.getMPName");
        action.setParams({
            recordId : component.get("v.recordId")
             });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("fetchTable1Data response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("fetchMPName response: "+JSON.stringify(result));
                if(result != null) {
                    component.set("v.mpIdExisting",result.Id);
                    component.set("v.mpNameExisting",result.Name);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("fetchMPName errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("fetchMPName Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action); 
    },
    
    fetchAssetName : function(component){
        console.log('fetchAssetName :'+component.get("v.recordId"));
      var action = component.get("c.getAssetName");
        action.setParams({
            recordId : component.get("v.recordId")
             });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("fetchAssetName response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("fetchAssetName response: "+JSON.stringify(result));
                if(result != null) {
                    component.set("v.assetName", result.Name);
                    component.set("v.assetId", result.Id);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("fetchAssetName errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("fetchAssetName Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action); 
    },
    
    assetPagePreview : function(component, event, helper) {
        console.log('assetPagePreview');
        var leaseRec = component.get("v.recordId");
        console.log('assetPagePreview leaseRec :'+leaseRec);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
           "recordId": leaseRec,
           "slideDevName": "related"
       });
       navEvt.fire();
        $A.get('e.force:refreshView').fire();
     },
   
    getmpValData : function(component,mpId) {
        console.log("getmpValData",mpId);
        var newMaintePrgrmId = mpId;
        var action = component.get("c.getTable2Data");
        action.setParams({
            recordId : newMaintePrgrmId,
            assetId : component.get("v.recordId"),
            
             });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getmpValData response-state: "+state);
            if (state === "SUCCESS") {
            var result = response.getReturnValue();
            console.log("getmpValData response: "+JSON.stringify(result));
            if(result != null) {
                component.set("v.assemblyWrapper",result);
                console.log('getmpValData assemblyWrapper',component.get("v.assemblyWrapper"));
                console.log('getmpValData mpeWrapper',component.get("v.mpeWrapper"));
            }
                }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getmpValData errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("getmpValData Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action); 
       
    },
    
    changeMP : function(component) {
        console.log('changeMP');
        
        var lookupdata = component.get("v.mpNameNewLookUpRecord");
        
        console.log('changeMP :lookupdata :'+lookupdata);
        var NewmpId;
        if(lookupdata != null){
            NewmpId = lookupdata.Id;
        }
        else{
            component.set("v.noMpSave",true);
            console.log('changeMP :noMpSave :'+noMpSave);
         }
        
        var action = component.get("c.changeMPonAsset");
        action.setParams({
            mpId : NewmpId,
            assetId : component.get("v.recordId")
            });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("changeMP response-state: "+state);
            if (state === "SUCCESS") {
                        console.log("changeMP response-state: "+state);
            this.assetPagePreview(component);
             }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("changeMP errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("changeMP Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);    
        
    },
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
     //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})