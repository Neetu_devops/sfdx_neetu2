({
	doInit : function(component, event, helper) {
        var data = new Object();
        component.set("v.invoiceData", data);
        component.set("v.invoiceDataOrg", data);
        let recordId = component.get("v.recordId");
        console.log("doInit before : recordId "+ recordId + ' obj name '+component.get('v.sObjectName'));
        //If there is no recordId then the component has been launched from New in the Related list of Invoice Adjustment
        //Parsing the url to get the Invoice id
        if(!recordId || recordId == null) {
            var pageRef = component.get("v.pageReference");	        
            console.log(JSON.stringify(pageRef));	        
            var state = pageRef.state; // state holds any query params	        
            console.log('state = '+JSON.stringify(state));	        
            var base64Context = state.inContextOfRef;	        
            console.log('base64Context = '+base64Context);	        
            if (base64Context.startsWith("1\.")) {	            
                base64Context = base64Context.substring(2);	            
                console.log('base64Context = '+base64Context);	        
            }	        
            var addressableContext = JSON.parse(window.atob(base64Context));	        
            console.log('addressableContext = '+JSON.stringify(addressableContext));	        
            component.set("v.recordId", addressableContext.attributes.recordId);
        }
        console.log("doInit after : recordId "+ component.get("v.recordId"));
        
        helper.initialize(component);
    },
    
    handleCancel: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        helper.launchRecordPage(component, component.get("v.recordId"));
    },
    
    handleAdjustmentExtCancel: function (component, event, helper) {
        component.set('v.showExistingAdjustment', false);
        helper.launchRecordPage(component, component.get("v.recordId"));
    },

    handleProceed: function (component, event, helper) {
        component.set("v.showErrorPopup", false);
        component.set('v.showExistingAdjustment', false);
        helper.getInvoice(component);
    },

    onStatusChange: function(component, event, helper) {
        console.log('onStatusChange '+component.get("v.initialStatus"));
        let invoiceData = component.get("v.invoiceData");
        if(invoiceData) {
            let isApprovalStatus = invoiceData.isAprovalProcess;
            if(isApprovalStatus == true) {
                helper.showErrorMsg(component, 'An approval workflow already exists for the record. The approval status cannot be manually modified. Please approve the record via the workflow.');
                if(invoiceData.invoiceAdjustmentItem) {
                    invoiceData.invoiceAdjustmentItem.Approval_Status__c = component.get("v.initialStatus");
                    component.set("v.invoiceData" , invoiceData);
                }
            }
        }
    },

    //Validate the amount added
    validateAdjustmentAmount: function(component,event,helper) {
        let flag = helper.validateBalanceDue(component);
        console.log('validateAdjustmentAmount '+flag);
        if(flag) {
            helper.updateRemainingAmount(component);
            helper.updateAdjustedTotal(component);
        }
        helper.autoUpdateAdjustmentInlineAmt(component, 'Adjustment');
    },
    
    //Updates the Adjustment Inline amounts
    updateInlineAdjustmentTotal: function(component,event,helper) {
        helper.validateInlineAdjustment(component);
        helper.autoUpdateAdjustmentInlineAmt(component, 'Inline');
    },
    
    //Validate adjustment amount and balance due and proceeds to save the record
    validateAdjustment: function(component,event, helper) {
        let invoiceData = component.get('v.invoiceData');
       // console.log('validateAdjustment '+JSON.stringify(invoiceData));
        console.log('validateAdjustment isDisabled '+component.get("v.isDisable"));
        if(component.get("v.isDisable") == false) {
            if(invoiceData && invoiceData.invoiceAdjustmentLineItemList) {
                let totalAdjustedAmount = 0 ;
                for(let i = 0; i < invoiceData.invoiceAdjustmentLineItemList.length; i++) {
                    if(invoiceData.invoiceAdjustmentLineItemList[i].invoiceLineItemAdjustedAmount) {
                        totalAdjustedAmount = Number(totalAdjustedAmount) + Number(invoiceData.invoiceAdjustmentLineItemList[i].invoiceLineItemAdjustedAmount);
                        totalAdjustedAmount = totalAdjustedAmount.toFixed(2);
                    }
                }
                console.log('validateAdjustment totalAdjustedAmount: '+totalAdjustedAmount);
                if(invoiceData.invoiceAdjustmentItem && invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c == totalAdjustedAmount) {
                    if(helper.validateBalanceDue(component) && helper.validateInlineAdjustment(component) && helper.validateAdjustmentAmount(component)) {
                        helper.updateData(component);
                    }
                }
                else {
                    helper.showErrorMsg(component,"Sum of all line item adjustments should be equal to the 'Adjustment Amount'. Please allocate the remaining allocation amount to the line items accordingly.");
                }
            }
        }
        else {
            helper.updateData(component);
        } 
    },
})