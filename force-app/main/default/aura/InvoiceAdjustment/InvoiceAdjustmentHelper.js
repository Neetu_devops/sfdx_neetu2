({
    //Identifies from where the components has been launched and navigate accordingly
    initialize : function(component) {
        let sObj = component.get('v.sObjectName');
        console.log('initialize sObj: '+sObj);
        this.isPendingAdjustmentExists(component);
    },

    //Checking for the existing adjustments with pending state
    isPendingAdjustmentExists : function(component) {
        component.set("v.isLoading", true);
        console.log("isPendingAdjustmentExists : recordId "+ component.get("v.recordId"));
        var self = this;
        var action = component.get('c.getPendingAdjustment');
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("isPendingAdjustmentExists : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("isPendingAdjustmentExists values " +JSON.stringify(allValues));
                if(allValues != null && allValues.length > 0) {
                    component.set("v.showErrorPopup", true);
                    component.set('v.showExistingAdjustment', true);
                    component.set("v.isLoading", false);
                }
                else {
                    component.set("v.showErrorPopup", false);
                    component.set('v.showExistingAdjustment', false);
                    self.getInvoice(component);
                }
            }
            else if (state === "ERROR") {  
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("isPendingAdjustmentExists errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("isPendingAdjustmentExists unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },

    //Check the status f the adjustment if it is Approved show the error msg and stops from proceeding
    checkAdjustmentStatus : function(component) {
        component.set("v.isLoading", true);
        console.log("checkAdjustmentStatus : recordId "+ component.get("v.recordId"));
        var self = this;
        var action = component.get('c.getAdjustmentStatus');
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("checkAdjustmentStatus : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("checkAdjustmentStatus values " ,allValues);
                if(allValues != null && allValues.length > 0) {
                    component.set("v.showErrorPopup", true);
                    component.set('v.showAdjustmentApproved', true);
                    component.set("v.isLoading", false);
                }
                else {
                    component.set("v.showErrorPopup", false);
                    component.set('v.showAdjustmentApproved', false);
                    self.getInvoice(component);
                }
            }
            else if (state === "ERROR") {  
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("checkAdjustmentStatus errors ", errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("checkAdjustmentStatus unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },

    
    getPicklist : function(component){
        var self = this;
        var action = component.get('c.getPickListOptions');
        action.setParams({
            sobj : 'Invoice_Adjustment__c',
            fieldname: 'Approval_Status__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getPicklist : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getPicklist values " +allValues);
                if(allValues != null) {
                    var opts = [];
                    for(var i=0;i< allValues.length; i++) {
                        opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                    }
                    
                    var approvalStatusId = component.find('approvalStatusId');
                    console.log("getPicklist approvalStatusId: "+approvalStatusId);
                    if(approvalStatusId != undefined){
                        approvalStatusId.set("v.options", opts); 
                    }
                }
                else {
                    self.showErrorMsg(component, "Approval status picklist not found!");
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getPicklist errors ",errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getPicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            this.checkForAdjustmentStatus(component);
            
        });
        $A.enqueueAction(action);
    },

    checkForAdjustmentStatus : function(component) {
        let invoiceData = component.get("v.invoiceData");
        console.log('checkForAdjustmentStatus invoiceData '+JSON.stringify(invoiceData));
        if(invoiceData && invoiceData.invoiceAdjustmentItem && 
            (invoiceData.invoiceAdjustmentItem.Approval_Status__c == 'Approved' || invoiceData.invoiceAdjustmentItem.Approval_Status__c == 'Declined')) {
            component.set("v.isDisable", true);
        }
        else {
            component.set("v.isDisable", false);
        }
        component.set("v.isLoading", false);
    },

    //Prefetches the Invoice, Invoice line item, Adjustment Line Items and show it in UI
	getInvoice : function(component) {
        component.set("v.isLoading", true);
        console.log("getInvoice : recordId "+ component.get("v.recordId"));
        var self = this;
        var action = component.get('c.getInvoiceData');
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getInvoice : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getInvoice values " +JSON.stringify(allValues));
                if(allValues) {
                    if(allValues.invoiceStatus == 'Approved') {
                        component.set("v.showComponent", true);
                    }
                    else {
                        component.set("v.showComponent", false);
                    }
                    if(allValues.invoiceAdjustmentItem) {
                        component.set("v.initialStatus", allValues.invoiceAdjustmentItem.Approval_Status__c);
                    }
                }
                component.set("v.invoiceData", allValues);
                component.set("v.invoiceDataOrg", allValues);
            }
            else if (state === "ERROR") {  
                var errors = response.getError();
                console.log("getInvoice errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getInvoice unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            self.getPicklist(component);
        });
        $A.enqueueAction(action);
    },
    
    //Updates or creates the record based on from where the component has been launched (New or Edit)
    updateData : function(component) {
        component.set("v.isLoading", true);
        let invoiceData = component.get("v.invoiceData");
        if(!invoiceData || invoiceData == null) 
            return;
        console.log('updateData: '+JSON.stringify(invoiceData));
        var self = this;
        var action = component.get('c.addUpdateAdjustmentDataFromUI');
        action.setParams({
            adjustmentData : JSON.stringify(invoiceData),
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("updateData : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("updateData values " +JSON.stringify(allValues));
                if(allValues != null) {
                    let sObj = component.get('v.sObjectName');
                    if(sObj.includes('Invoice_Adjustment__c')) {
                        self.showSuccessMsg(component,'The Adjustment record has been updated successfully.');
                    }
                    else if(sObj.includes('Invoice__c')) {
                        self.showSuccessMsg(component,'The Adjustment record has been created successfully.');
                    }
                    self.launchRecordPage(component,allValues);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("updateData errors "+errors);
                if (errors) {
                    console.log("updateData errors "+JSON.stringify(errors));
                    self.handleErrors(errors);   
                } else {
                    console.log("updateData unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
        
    launchRecordPage : function(component, recordId) {
        console.log('launchRecordPage '+recordId);
        window.open('/' + recordId,"_self");
    },

    autoUpdateAdjustmentInlineAmt: function(component, type) {
        let invoiceData = component.get('v.invoiceData');
        console.log('autoUpdateAdjustmentInlineAmt invoiceData: '+ JSON.stringify(invoiceData));
        if(invoiceData && invoiceData.invoiceAdjustmentLineItemList && invoiceData.invoiceAdjustmentLineItemList.length == 1 && invoiceData.invoiceAdjustmentItem) {
            if(type == 'Adjustment') {
                invoiceData.invoiceAdjustmentLineItemList[0].invoiceLineItemAdjustedAmount = invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c;
            }
            else if(type == 'Inline') {
                invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c = invoiceData.invoiceAdjustmentLineItemList[0].invoiceLineItemAdjustedAmount;
            }
            component.set("v.invoiceData", invoiceData);
            component.set("v.remainingAmount", 0);
        }
    },


    validateBalanceDue: function(component) {
        console.log('validateBalanceDue');
        let invoiceData = component.get('v.invoiceData');
        if(invoiceData && (invoiceData.invoiceBalanceDue || invoiceData.invoiceBalanceDue == 0) && invoiceData.invoiceAdjustmentItem && invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c) {
            let adjAmt = invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c;
            let amt = invoiceData.invoiceBalanceDue + adjAmt;
            console.log('validateBalanceDue amt: '+amt + '  adjAmt: '+  adjAmt);
            if(amt < 0) {
                this.showErrorMsg(component, 'Balance Due cannot be reduced below 0. Please modify the Adjustment Amount.');
                invoiceData.adjustedTotal = 0;
                component.set("v.invoiceData", invoiceData);
                return false;
            }
            else {
                return true;
            }
        }
        return true;
    },

    validateAdjustmentAmount: function(component) {
        let invoiceData = component.get("v.invoiceData");
        if(invoiceData && invoiceData.invoiceAdjustmentItem && invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c == 0 ) {
            this.showErrorMsg(component, 'Adjustment Amount should not be 0');
            return false;
        }
        return true;
    },
    
    validateInlineAdjustment: function(component) {
        let invoiceData = component.get('v.invoiceData');
        let invoiceDataOrg = component.get('v.invoiceDataOrg');
        console.log('updateInlineAdjustmentTotal ++= '+JSON.stringify(invoiceData));
        let flag = true;
        let totalLineItemAmount = 0;
        if(invoiceData && invoiceData.invoiceAdjustmentLineItemList) {
            for(let i = 0; i < invoiceData.invoiceAdjustmentLineItemList.length; i++) {
                let data = invoiceData.invoiceAdjustmentLineItemList[i];
                console.log('updateInlineAdjustmentTotal data: '+JSON.stringify(data));
                if((data.invoiceLineItemAmount || data.invoiceLineItemAmount == 0) && (data.invoiceLineItemAdjustedAmount || data.invoiceLineItemAdjustedAmount == 0)) {
                    data.invoiceLineItemAdjustedTotal = Number(data.invoiceLineItemAmount) + Number(data.invoiceLineItemAdjustedAmount);
                    let balDue = Number(data.invoiceLineItemBalanceDue) + Number(data.invoiceLineItemAdjustedAmount);
                    if(balDue < 0) {
                        flag = false;
                        data.invoiceLineItemAdjustedTotal = 0;
                    }
                    totalLineItemAmount = Number(totalLineItemAmount) + Number(data.invoiceLineItemAdjustedAmount);
                }
                else {
                    data.invoiceLineItemAdjustedTotal = 0;
                }
            }
        } 
        component.set("v.totalLineItemAmount", totalLineItemAmount);
        component.set("v.invoiceData", invoiceData);
        this.updateRemainingAmount(component);
        if(flag == false) {
            this.showErrorMsg(component, 'Line Amount Balance Due cannot be reduced below 0. Please modify the Adjustment Amount per line item.');
        }
        return flag;
    },

    updateRemainingAmount: function(component) {
        let totalLineItemAmount = component.get("v.totalLineItemAmount");
        console.log('updateRemainingAmount '+totalLineItemAmount );
        let invoiceData = component.get("v.invoiceData");
        let totalAdjustment = 0; 
        if(invoiceData && invoiceData.invoiceAdjustmentItem) {
            totalAdjustment = invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c;
        }
        let remainingAmt = Number(totalAdjustment) - Number(totalLineItemAmount);
        console.log('updateRemainingAmount totalLineItemAmount: '+totalLineItemAmount + ' totalAdjustment: '+totalAdjustment + ' remainingAmt:'+remainingAmt);
        component.set("v.remainingAmount", remainingAmt);
    
    },

    updateAdjustedTotal: function(component) {
        console.log('updateAdjustedTotal');
        let invoiceData = component.get('v.invoiceData');
        if(invoiceData && (invoiceData.invoiceAmount || invoiceData.invoiceAmount == 0 ) && 
            (invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c || invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c == 0)) {
            invoiceData.adjustedTotal = Number(invoiceData.invoiceAmount) + Number(invoiceData.invoiceAdjustmentItem.Adjustment_Amount__c);
        } else if(invoiceData){
            invoiceData.adjustedTotal = 0;
        }
        component.set("v.invoiceData", invoiceData);
    },

    showSuccessMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Success!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    
    showWarning : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Warning',
            message: msg,
            duration:' 5000',
            key: 'info_alt',
            type: 'warning',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('error '+ toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('error '+ toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('error '+ toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
})