({
    doInit : function(component, event, helper) {
        var action = component.get("c.getAssetInDeals");
        action.setParams({
            'fieldSetName' : component.get("v.fieldSetName"),
            'editableFields' : component.get("v.editableFieldNames"),
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {                   
            var state = response.getState();
            console.log('state----'+ state);
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                
                result.colList[0] = {
                    label: 'Name', 
                    type: 'button', 
                    editable: false,                   
                    typeAttributes: { 
                        label: {
                            fieldName: 'Name'
                        }, 
                        name: 'open_detailPage',
                        variant: 'base'
                    }
                };
                
                
                component.set("v.columns", result.colList);
                component.set("v.data", result.assetDealList);
                component.set("v.dealName", result.dealName);                
            }
            else {
                helper.handleErrors(response.getError());
            }     
        });
        
        $A.enqueueAction(action);   
    },
    
    
    handleRowAction : function(component, event, helper) {
        var action = event.getParam('action');
        var rowId = event.getParam('row').Id;
        
        if(action.name == 'open_detailPage') {
            var url = '/' + rowId;
            window.open(url);
        }
    },
    
    
    saveUpdates : function(component, event, helper) {
        var data = component.get("v.data");
        var rowIndex;
        var draftValue = event.getParam("draftValues");
                
        for(var key in draftValue[0]) {
            if(key == 'id') {
                rowIndex = (draftValue[0][key]).split('-')[1];
            }
        }
        
        for(var key in draftValue[0]) {
            if(key != 'id') {
                if(rowIndex == 0) {
                    for(var i=0; i<data.length; i++) {
                        data[i][key] = draftValue[0][key];
                    }  
                }
                else {
                    data[rowIndex][key] = draftValue[0][key];
                }
            }
        }       
        
        component.set("v.data", data);
    },
    
    
    saveAssetsInDeal : function(component, event, helper) {
        component.set("v.showSpinner", true);
        var reasonsForInactive = component.get("v.reasonForInactive");
        
        var action = component.get("c.saveAssetInDeals");
        action.setParams({
            'assetList' : component.get("v.data"),
            'confirmed' : component.get("v.confirmed"),
            'updatedOffer' : component.get("v.updatedOffer"),
            'reasonForClosing' : component.get("v.reasonForInactive"),
            'closingComments' : component.get("v.closingComments"),
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {                   
            var state = response.getState();
            
            if(state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Records saved successfully',
                    type: 'success'
                });
                toastEvent.fire();    
                
                //var url = '/' + component.get("v.recordId");
                //window.open(url, "_self");
                $A.get('e.force:refreshView').fire(); 
            }
            else {
                helper.handleErrors(response.getError());
            } 
            
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    
    
    closeModal : function(component, event, helper) {
        var action = component.get("c.updateDeal");
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {                   
            var state = response.getState();
            
            if(state === "SUCCESS") {
                //var url = '/' + component.get("v.recordId");
                //window.open(url, "_self");
                $A.get('e.force:refreshView').fire(); 
            }
        });
        
        $A.enqueueAction(action);
    }
})