({
    initialize: function (component, event, helper) {
        helper.fetchObjectId(component, event);
        helper.setRecordId(component, event);
        helper.fetchPageLayout(component, event);
    },

    closeQuickAction: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    handleSuccess: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        var recordId = component.get("v.recordId");
        var res = component.get("v.objPrefix");

        var toastEvent = $A.get("e.force:showToast");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": event.getParam("response").id,
            "slideDevName": "detail"
        });
        navEvt.fire();
        var recId = '';
        if (recordId != undefined && res != undefined) {
            recId = recordId.substring(0, 3);
            if (recId == res) {
                toastEvent.setParams({
                    "title": "Successfully!",
                    "message": "Record has been Updated",
                    "type": "success"
                });

            } else {
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Record has been Created",
                    "type": "success"
                });
            }
        }

        toastEvent.fire();

        var deal = event.getParams().response;
        var dealId = deal.id;
        var dealName = 'New';
        helper.saveEconoicAnalysis(component, event, dealId, dealName);
    },


    handleLoad: function (component, event, helper) {
        helper.top(component, event);
    },

    handleError: function (component, event, helper) {
        //alert('Error Here' +event); 
        console.log(event);
        var errors = event.getParams();
        console.log("Error Response", JSON.stringify(errors));
        var flag = 0;
        var error = event.getParam("error");
        console.log(error.message);
		component.set("v.disabled", false);

    },
    hideErrorMsg: function (component, event, helper) {
        if (component.get("v.picklistOptsList") != null && component.get("v.picklistOptsList") != undefined && component.get("v.picklistOptsList") != '') {
            document.getElementById("error").classList.add("assetDiv");
            document.getElementById("error").innerHTML = '';
        }
    },

    handleSubmit: function (component, event, helper) {
		component.set("v.disabled", true);
        var picklistvalues = component.get('v.picklistOptsList');
        var ids = [];
        if (picklistvalues.length > 0) {
            for (var i = 0; i < picklistvalues.length; i++) {
                ids.push(picklistvalues[i].Id);
            }
            console.log('Ids: ', ids);
            component.set("v.assetlist", ids);
            console.log('--->>', component.get("v.assetlist"));
        }
        if (component.get("v.assetlist") == null || component.get("v.assetlist") == undefined || component.get("v.assetlist") == '') {
            console.log(component.get("v.picklistOptsList"), '****', document.getElementById("asset"));
            event.preventDefault();
			component.set("v.disabled", false);
            document.getElementById("error").classList.add("assetDiv");
            document.getElementById("error").innerHTML = 'At least one Asset is required on Commercial Terms';
        }
        //LW:AKG : 01/19/2021 : Added this logic to populate deal Id on CT record if the field is marked readonly on Page Layout.
        let parentFieldReadOnly = component.get("v.parentFieldReadOnly");
        if (parentFieldReadOnly) {
            event.preventDefault();
            var eventFields = event.getParam("fields");
            eventFields["Marketing_Activity__c"] = component.get("v.dealRec.Id");
            component.find('myform').submit(eventFields);
        }
    }
})