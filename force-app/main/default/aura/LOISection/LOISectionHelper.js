({
    fetchObjectId: function (component, event) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getDealId");

        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (recordId !== result) {
                    component.set("v.dealRec", JSON.parse(result));
                }
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);

    },
    top: function (component, event) {
        let top = document.getElementById('topDiv');
        top.scrollIntoView(true);
    },
    showMessage: function (message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: message,
            type: type
        });
        toastEvent.fire();
    },
    fetchManagedAsset: function (component, event) {
        this.showSpinner(component);
        var action = component.get("c.getManagedAssetType");
        action.setParams({
            recId: component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues.length > 0) {
                    var opts = [];
                    for (var i = 0; i < allValues.length; i++) {
                        if (!opts.includes(allValues[i].aircraftId))
                            opts.push(allValues[i].aircraftId);
                    }
                }
            } else if (state == "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    //To get the asset Term in case of Edit
    showSelectedAssetForDeal: function (component, event) {
        this.showSpinner(component);
        var action = component.get("c.getAssetTypeForSelectedDealAnalysis");
        action.setParams({
            dealAnalysisId: component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues.length > 0) {
                    var picklisVal = component.get("v.picklistOptsList");
                    //To show selected items
                    var opts = [];
                    for (var j = 0; j < picklisVal.length; j++) {
                        for (var i = 0; i < allValues.length; i++) {
                            if (picklisVal[j].aircraftName != undefined && picklisVal[j].aircraftId == allValues[i].assetTermId) {
                                if (!opts.includes(picklisVal[j].aircraftId))
                                    opts.push(picklisVal[j].aircraftId);
                            }
                        }
                    }
                    component.set("v.selectedAssetTypeLst", opts);
                }
            } else if (state == "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    //To fetch the asset Term while creating New Commercial Terms Record
    fetchAircrafts: function (component, event) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getManagedAssetType");
        action.setParams({
            recId: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('allValues-->', allValues);
                if (allValues.length > 0) {
                    var opts = [];
                    var assetNames = [];
                    for (var i = 0; i < allValues.length; i++) {
                        opts.push(allValues[i]);
                        if (allValues[i].aircraftName != undefined) {
                            assetNames.push(allValues[i].aircraftName);
                        }
                    }
                    console.log('opts-->', opts);
                    component.set("v.assets", assetNames.join(','));
                    component.set("v.picklistOptsList", opts);
                    if (component.get("v.recID") == '') {
                        this.fetchManagedAsset(component, event);
                    } else {
                        this.showSelectedAssetForDeal(component, event);
                    }
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },

    //Save Asset Term record when Commercial Terms is saved 
    saveEconoicAnalysis: function (component, event, dealId, dealName) {
        var action = component.get("c.saveEconoicAnalysis");
        action.setParams({
            dealId: dealId,
            dealName: dealName,
            assetIdLst: component.get("v.assetlist")
        });
        action.setCallback(this, function (response) {
            var res = response.getState();
            if (res == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('values', allValues);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },

    //To get the Page Layout fields
    fetchPageLayout: function (component, event) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getPageLayoutFields");
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if (res == null || res == undefined || res == '') {
                    component.set('v.showSection', false);
                    this.showMessage('Could not retrieve layout for this record type', 'error');
                    $A.get("e.force:closeQuickAction").fire()
                }
                else {
                    console.log("fetchPageLayout res: ",res);
                    component.set("v.layoutSections", res);
                    //LW:AKG : 01/19/2021 : Added this logic to check if the Deal field is marked readonly on Page Layout.
                    for (let key in res) {
                        if (res[key].lstFields != undefined && res[key].lstFields != '') {
                            for (let key2 in res[key].lstFields) {
                                if (res[key].lstFields[key2].fieldName === 'Marketing_Activity__c') {
                                    component.set("v.parentFieldReadOnly", res[key].lstFields[key2].isReadOnly);
                                }
                            }
                        }
                    }
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },


    //To set the recordId in case of Edit
    setRecordId: function (component, event) {
        this.showSpinner(component);
        var recordId = component.get("v.recordId");
        var action = component.get("c.getRecordIdPrefix");
        action.setParams({
            dealId: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                var res = data.prefix;
                var recordId = component.get("v.recordId");
                var recId = '';
                if (recordId != undefined && res != undefined) {
                    recId = recordId.substring(0, 3);
                    if (recId == res) {
                        component.set("v.objPrefix", res);
                        component.set("v.recID", recordId);
                        component.set("v.recName", data.dealanalysisName);
                    } else {
                        this.setRecordType(component, event);
                    }
                }
                this.fetchAircrafts(component, event);
            } else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    // Show the lightning Spinner
    showSpinner: function (component, event) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    // Hide the lightning Spinner
    hideSpinner: function (component, event) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    setRecordType: function (component, event) {
        this.showSpinner(component);
        var action = component.get("c.getDealAnalysisRecTypeId");
        action.setParams({
            MAID: component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var data = response.getReturnValue();
                if (data != undefined) {
                    var recform = component.find("myform");
                    console.log('*************', recform);
                    recform.set("v.recordTypeId", data);
                    console.log('*************', recform.get("v.recordTypeId"));
                }
            } else if (state == "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    //Show the error toast when any error occured
    handleErrors: function (errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });
                    };
                }
            });
        }
    },
})