({ 
    
    //Show the success toast when record is saved successfully
    showToast : function(component, event) {
        var sMsg = 'Saved Successfully';
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: sMsg,
            type : 'success'
        });
        toastEvent.fire();
    },
    
    //Show the error toast when any error occured
	handleErrors : function(errors) {
       // Configure error toast
       let toastParams = {
           title: "Error",
           message: "Unknown error", // Default error message
           type: "error"
       };
       if (errors) {
           errors.forEach( function (error){
               //top-level error. There can be only one
               if (error.message){
                   toastParams.message = error.message;
                   let toastEvent = $A.get("e.force:showToast");
                   toastEvent.setParams(toastParams);
                   toastEvent.fire();
               }
               //page-level errors (validation rules, etc)
               if (error.pageErrors){
                   let arrErr = [];
                   error.pageErrors.forEach( function(pageError) {
                       toastParams.message = pageError.message;
                       let toastEvent = $A.get("e.force:showToast");
                       toastEvent.setParams(toastParams);
                       toastEvent.fire();
                   });	
               }
               if (error.fieldErrors){
                   //field specific errors--we'll say what the field is
                   for (var fieldName in error.fieldErrors) {
                       //each field could have multiple errors
                       error.fieldErrors[fieldName].forEach( function (errorList){	
                           toastParams.message = errorList.message;
                           let toastEvent = $A.get("e.force:showToast");
                           toastEvent.setParams(toastParams);
                           toastEvent.fire();
                           //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                       });  
                   };
               }
           });
       }
    }
})