({
    doInit : function(component, event, helper) {
        var action = component.get("c.getDepartmentValues");
        action.setParams({
            earId : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                if(res.length>0){
                    component.set("v.listDep", res);
                    let data = [];
                    for(let i=0; i<res.length; i++){
                        let copy = Object.assign({}, res[i]);
                        data.push(copy);
                    }
                    component.set("v.lstDepartments", data);
                }   
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
            
        });
        $A.enqueueAction(action); 
    },
    
    //Enable the save button when any of the toggle button is checked
    checkboxChecked : function(component, event, helper){
        var checkboxes = component.find("chkBox");
        var departmentVal = component.get("v.listDep");
        var departmentValues = component.get("v.lstDepartments");
        component.set("v.isChecked", true);
        if(checkboxes != undefined && checkboxes.length == undefined){
             if( departmentValues[0].Complete__c  != departmentVal[0].Complete__c){
                component.set("v.isChecked", false);
            }
        }else if(checkboxes != undefined && checkboxes.length != undefined){
            for (var i = 0; i <checkboxes.length; i++){
                if( departmentValues[i].Complete__c  != departmentVal[i].Complete__c){
                    component.set("v.isChecked", false);
                    break;
                }
            }
        }
    },
    
    //Opens the Modal on click of save Button
    handleClick : function(component, event, helper){
        var openModal = document.getElementById('confirmModal');
        $A.util.removeClass(openModal, 'slds-hide');
        component.set("v.comment",'');
        var departmentVal = component.get("v.listDep");
        var departmentValues = component.get("v.lstDepartments");
        if(departmentValues !== undefined && departmentVal !== undefined){
            var deptStatus = [];
            for (var i = 0; i <departmentValues.length; i++){
                 var values = new Object ();
                if(departmentValues[i].Complete__c != departmentVal[i].Complete__c ){
                    values.value = departmentValues[i].Name+' inputs have been marked ';
                    if(departmentValues[i].Complete__c == true)
                		values.done = 'Complete';
                    else
                        values.done = 'Incomplete';
                }   
                deptStatus.push(values);
            }
             console.table(deptStatus);
            component.set("v.status",deptStatus);
        }
    },
    
    //Close Modal on click of cross
    closeModal: function(component, event, helper){
        var openModal = document.getElementById('confirmModal');
        $A.util.addClass(openModal, 'slds-hide');
    },
    
    //Refreshes to the old value on click of cancel button of Modal
    cancel: function(component, event, helper){
        var departmentVal = component.get("v.listDep");
        var departmentValues = component.get("v.lstDepartments");
        if(departmentValues !== undefined && departmentVal !== undefined){
            for (var i = 0; i <departmentValues.length; i++){
                if(departmentValues[i].Complete__c != departmentVal[i].Complete__c){
                    departmentValues[i].Complete__c = departmentVal[i].Complete__c;
                }   
            }
            component.set("v.lstDepartments",departmentValues);
            var openModal = document.getElementById('confirmModal');
            $A.util.addClass(openModal, 'slds-hide');
        }
    },
    
    //Save to the value on click of Save button of Modal
    save: function(component, event, helper){
        var action = component.get("c.saveDepartmentStatus");
        var statusLst = [];
        var departmentVal = component.get("v.listDep");
        var departmentValues = component.get("v.lstDepartments");
        if(departmentValues.length > 0 ){
            for (var i = 0; i <departmentValues.length; i++){
                if(departmentVal[i].Complete__c != departmentValues[i].Complete__c){
                	departmentValues[i].Comment__c = component.get("v.comment");   
                }
                statusLst.push(departmentValues[i]);  
            }
        }
        action.setParams({
            lstDeptStatusRec : statusLst
        });
        action.setCallback(this,function(response){
            
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                if(res.length>0){
                    console.log('Success'); 
                    component.set("v.listDep", res);
                    let data = [];
                    for(let i=0; i<res.length; i++){
                        console.log(i);
                        let copy = Object.assign({}, res[i]);
                        data.push(copy);
                    }
                    console.log('data'+data);
                    component.set("v.lstDepartments", data);
                    component.set("v.isChecked", true);
                }
                helper.showToast(component,event);
                $A.get('e.force:refreshView').fire();
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
            var openModal = document.getElementById('confirmModal');
            $A.util.addClass(openModal, 'slds-hide');
        });
        $A.enqueueAction(action); 
    },
})