<aura:application extends="ltng:outApp" access="GLOBAL" implements="flexipage:availableForAllPageTypes,force:appHostable">
	<aura:dependency resource="c:AircraftSearchComponent"/> 
	<aura:dependency resource="c:AircraftSearchResultComponent"/> 
    <aura:dependency resource="c:AircraftSearchResultDetailComponent"/>
    <aura:dependency resource="c:AircraftComponent"/>
    
</aura:application>