({
      getNameSpacePrefix : function(component){
        var action = component.get('c.getNameSpacePrefix');
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            component.set('v.namespace', namespace);
            
            console.log("getNameSpacePrefix "+namespace);
            //this.populateFilterValues(component, this);
            //this.getAssetDetail(component, this);
            this.getSeatFromWF(component);
        });
        $A.enqueueAction(action);
    },
  
    getVintagePicklist: function(component, elementId) {
        var action = component.get('c.getVintagePicklist');
        var self = this;
        var opts = [];
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            var assetData = component.get("v.assetData");
            var vintage = 0 ;
            var vintageNamespace = component.get('v.namespace') + 'Vintage__c';
            
            console.log('getVintagePicklist ' + assetData);
            if(assetData != null && assetData[vintageNamespace] != null){
                var data =  assetData[vintageNamespace].split('-');
                if(data != null)
                    vintage = data[0];
            }
            
            opts.push({"class": "optionClass", label: '-None-', value: 0});
            if(allValues != null){
                for(var i=0;i< response.getReturnValue().length; i++) {
                    if(response.getReturnValue()[i] == vintage) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected : true});
                        component.find('vintageValue').set("v.value", response.getReturnValue()[i]);
                    }
                    else
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
            }
            //console.log('getVintagePicklist '+component.find("vintageValue") + " "+elementId);
            component.find("vintageValue").set("v.options", opts); 
        });
        $A.enqueueAction(action);
    },
     
    getManagerPicklist : function(component, elementId) {
        var action = component.get('c.getManagerPicklist');
        var inputSel = component.find("ManagerPicklist");
        var options=[];

        action.setCallback(this, function(response) {
            options.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            if(response.getReturnValue() != null) {
                for (var i =0; i < response.getReturnValue().length; i++ ){
                    options.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]})
                }
            }
            inputSel.set("v.options",options);
        });
        $A.enqueueAction(action);
    },

    getRegionPickList: function(component,elementId){
        var action = component.get('c.getRegionPicklist');
        var inputSel = component.find("RegionPickList");
        var options=[];

        action.setCallback(this, function(response) {
            options.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            if(response.getReturnValue() != null) {
                for (var i =0; i < response.getReturnValue().length; i++ ){
                    options.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]})
                }
            }
            inputSel.set("v.options",options);
        });
        $A.enqueueAction(action);
    },

    getSeatFromWF : function(component){
        console.log('getSeatFromWF');
        var self = this;
        var action = component.get('c.getSeatData');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getSeatFromWF : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getSeatFromWF values " +allValues);
                if(allValues != null) {
                    console.log("getSeatFromWF values " +JSON.stringify(allValues));
                    component.set("v.seatsFrom", allValues.seatFrom);
                    component.set("v.seatsTo", allValues.seatTo);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getSeatFromWF errors "+errors);
                self.handleErrors(errors);   
            }
            this.getAssetDetail(component, this);
        });
        $A.enqueueAction(action);
    },
    
    getAssetDetail: function(component,helper) {
        var self = this;
        var action = component.get('c.getAssetForRecord');
        action.setParams({ 
            recordId : component.get("v.recordId")
        });        
        action.setCallback(this, function(response) {
            var assetData = response.getReturnValue();
            component.set("v.assetData", assetData);
            console.log("getAssetDetail output assetData "+assetData);
            if(assetData != null)
            	console.log("getAssetDetail output assetData "+JSON.stringify(assetData));
            var namespace = component.get('v.namespace');
            
            if(!$A.util.isEmpty(assetData) && !$A.util.isUndefined(assetData) && 
               !$A.util.isUndefined(assetData[namespace+'Aircraft_Type__c']) && 
               assetData[namespace+'Aircraft_Type__c'] != null) {
            
                var vintage = 0 ;
                var vintageNamespace = namespace + 'Vintage__c';
                if(assetData[vintageNamespace] != null){
                    var data =  assetData[vintageNamespace].split('-');
                    if(data != null)
                        vintage = data[0];
                }
                var mtowValue = assetData[namespace + 'MTOW_Leased__c'];
                if($A.util.isEmpty(mtowValue) || $A.util.isUndefined(mtowValue) || mtowValue == null) 
                    mtowValue = 0;
                component.find("mtow").set("v.value", mtowValue);
                
                helper.displayAirline(component, self, assetData[namespace+'Aircraft_Type__c'], assetData[namespace+'Aircraft_Variant__c'] ,
                              assetData[namespace+'Engine_Type__c'] ,vintage ,3, assetData[namespace+'MTOW_Leased__c'] ,1000);
            }
            self.populateFilterValues(component, self);
            //helper.getNameSpacePrefix(component);
        });
        $A.enqueueAction(action);
    },
    
    displayAirline: function(component, helper, aircraftType, variant, engine, vintage, vintageRange, mtow, mtowRange, manager,region) {
        //recordId : component.get("v.recordId"),
        //leaseExpiry : component.get("v.LeaseExpiry"),
        
        console.log('displayAirline inside type:'+aircraftType + ' variant:'+variant+' engine:'+engine);
        console.log('displayAirline inside mtow:'+mtow + ' mtowRange:'+mtowRange+' vintage:'+vintage+ ' vintageRange:'+vintageRange+ 'manager:'+manager);
        
        
        if($A.util.isEmpty(variant) || $A.util.isUndefined(variant) || variant == '--- None ---' ||  variant.includes('Aircraft_Variant__c'))
            variant = null;         
        if( $A.util.isEmpty(engine) ||  $A.util.isUndefined(engine) || engine == '--- None ---' || engine.includes('Engine_Type__c'))
            engine = null;
        if($A.util.isUndefined(vintage) || $A.util.isEmpty(vintage) || vintage == '-None-')
            vintage = 0;
        if($A.util.isUndefined(vintageRange) || $A.util.isEmpty(vintageRange))
            vintageRange = 0 ;
        if($A.util.isUndefined(mtow) || $A.util.isEmpty(mtow))
            mtow = 0;
        if($A.util.isUndefined(mtowRange) || $A.util.isEmpty(mtowRange))
            mtowRange = 0;
        if($A.util.isUndefined(manager) || $A.util.isEmpty(manager) || manager=='--- None ---')
            manager = null;
        if($A.util.isUndefined(region) || $A.util.isEmpty(region) || region=='--- None ---')
        	region = null;
        
        console.log('displayAirline inside after type:'+aircraftType + ' variant:'+variant+' engine:'+engine);
        console.log('displayAirline inside after mtow:'+mtow + ' mtowRange:'+mtowRange+' vintage:'+vintage+ ' vintageRange:'+vintageRange+ 'manager:'+manager);
        console.log('displayAirline inside after From '+component.get("v.seatsFrom")+', To: '+component.get("v.seatsTo"));
        var mScoreFilter = component.get("v.scoreFilter");
        console.log('displayAirline score filter '+mScoreFilter);
        var action = component.get('c.getAirlinesRecommendedList');
        action.setParams({ 
            aircraftType : aircraftType,
            variant : variant,
            engine : engine,
            vintage : vintage,
            vintageRange : vintageRange,
            mtow : mtow,
            mtowRange : mtowRange,
            manager: manager,
            region: region,
            scoreFilter : mScoreFilter,
            seatFrom: component.get("v.seatsFrom"),
            seatTo: component.get("v.seatsTo")
        });        
        action.setCallback(this, function(response) {
            component.set("v.isLoading", false);
            var airlineRecomdList = response.getReturnValue();
            component.set("v.airlineRecomd", airlineRecomdList);
            var aircraftPropList = component.get("v.airlineRecomd");
            //console.log("displayAirlinePyramid helper "+JSON.stringify(aircraftPropList));
            var dataPoints = [];
            var colorList = [];
            if(aircraftPropList != null) {
                console.log("displayAirlinePyramid helper "+aircraftPropList.length);
                for(var i=0;i < aircraftPropList.length; i++) {
                    var aircraftProp = aircraftPropList[i];
                    var text = aircraftProp.operatorName;
                    var ICAO = '';
                    if(aircraftProp.ICAO != undefined)
                    	ICAO = aircraftProp.ICAO;
                    if(aircraftProp.ICAOPrecentage != undefined) {
                        if(ICAO != '')
                            ICAO = ICAO +', ';
                    	ICAO = ICAO + aircraftProp.ICAOPrecentage +'%';
                    }
                    console.log('ICAO ' +ICAO);
                    dataPoints.push({'x' :aircraftProp.x_axis, 'y' :aircraftProp.y_axis, 'v' : ICAO});
                    colorList.push(aircraftProp.color);
                }    
                component.set("v.dataPoints", dataPoints);
                component.set("v.colorList", colorList);
            }
            if(component.get('v.isDraw') == false) { // we need to call draw only if customer has clicked filter
                component.set("v.isLoading", true);
                helper.createRecommendationGraph(component,helper);
            }
        });
        $A.enqueueAction(action);
    }, 
    
    getFields: function (component) {
        var self = this;
        component.set("v.isLoading", true);
        var action = component.get('c.getColumns');
        action.setParams({
            fieldSetName: component.get('v.fieldset')
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getFields : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getFields values ", allValues);
                if (allValues != null) {
                    console.log("getFields allValues.length ");
                    var resObj = JSON.parse(allValues);
                    component.set("v.selectedFields", resObj);
					console.log("getFields allValues.length " + resObj);
                    self.getAirlineList(component);
                }
                else {
                    component.set("v.isLoading", false);
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getFields errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getFields Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getAirlineList: function (component) {
        let airlineRecomd = component.get("v.airlineRecomd");
        console.log('getAirlineList airlineRecomd: ' , airlineRecomd);
        var self = this;
        if ($A.util.isEmpty(airlineRecomd) || $A.util.isUndefined(airlineRecomd)) {
            self.showErrorMsg(component, 'Airlines not found!');
            return;
        }
        let airlineList = [];
        
        for(let i = 0; i < airlineRecomd.length; i++) {
            let data = {};
            data.operatorId = airlineRecomd[i].operatorId;
            data.weight = airlineRecomd[i].weight;
            airlineList.push(data);
        }
        //console.log('getAirlineList ids: '+ JSON.stringify(airlineList));
        var action = component.get('c.getOperatorDetails');
        action.setParams({
            operatorStr: JSON.stringify(airlineList),
            fieldSet: component.get("v.fieldset")            
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getAirlineList : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                //console.log("getAirlineList values " , allValues);
                if (allValues != null) {
                    var resObj = JSON.parse(allValues);
                    component.set("v.airlineListData", resObj);
                    self.updateListHeight(component);
                }
                else {
                	component.set("v.isLoading", false);
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getAirlineList errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getAirlineList unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },
    
    updateListHeight: function(component) {
        window.setTimeout(
            $A.getCallback(function() {
                let ele = document.getElementById("airlineTableId");
                console.log('updateListHeight ele '+ele);
                if(ele != undefined) {
                    let hgt = ele.offsetHeight;
                    console.log('updateListHeight hgt '+hgt);
                    if(hgt > 300) {
                        let ele = document.getElementById("airlineId");
                        if(ele != undefined) {
                            ele.style.height = '300px';
                        }
                    }
                }
                component.set("v.isLoading", false);
            }), 500);
    },
    
    closePopAfterToast : function(component, event) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        component.set("v.airline",null);
    },
    
    DisplayAirlines: function(component,event,secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
              $A.util.toggleClass(acc[cmp], 'slds-show');  
              $A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
        let showAirline =  component.get("v.showAirline");
        let airlineListData = component.get("v.airlineListData");
        //console.log('DisplayAirlines showAirline '+showAirline );
        if(showAirline == false && ($A.util.isUndefined(airlineListData) || $A.util.isEmpty(airlineListData) || airlineListData == null)) {
        	this.getFields(component);    
        }
        else if(showAirline == false) {
            component.set("v.isLoading", true);
            this.updateListHeight(component);
        }
        component.set("v.showAirline", !showAirline);
        
    },
    
    showResultMsg : function(component, event, helper, recordId, msg) {
        var toastEvent = $A.get("e.force:showToast");
        if(recordId != null) {
            toastEvent.setParams({
                title: "Success!",
                message: "The record has been created successfully.",
                messageTemplate: '{1} has been created successfully.',
                messageTemplateData: ['Salesforce', {
                    url: '/'+recordId,
                    label: 'Deal',
                }],
                duration:'5000',
                key: 'info_alt',
                type: 'success',
                mode: 'dismissible'
            });
            
        }
        else{
            toastEvent.setParams({
                "title": "Failed!",
                "message": msg,
                duration:'5000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
        }
        toastEvent.fire();
	},
    
    setNoData : function(component) {
        component.set("v.isNoData",true);
        component.set("v.isAirlineList",false);
        component.set("v.isChart",false);
    },
    
    
    chart : null,
    createRecommendationGraph : function(cmp,  helper ) {  
        console.log('createRecommendationGraph this.chart: '+this.chart);
        /*if(this.chart) {
            this.chart.clear();
        }*/
        cmp.set('v.isDraw',true);
        var self = this;
        var aircraftPropList = cmp.get("v.airlineRecomd");
        
        if(aircraftPropList == null){
            cmp.set("v.renderChart",false);
            cmp.set("v.isLoading", false);
            self.setNoData(cmp);
            return;
        }
        console.log("createGraph "+aircraftPropList.length +' ,showAirlineList: '+cmp.get("v.showAirlineList"));
        if(aircraftPropList.length>0){
            cmp.set("v.renderChart",true);
            cmp.set("v.isNoData",false);
            if(cmp.get("v.showAirlineList") == true) {
                cmp.set("v.isAirlineList",true);
            }
            else {
                cmp.set("v.isAirlineList",false);
            }
            cmp.set("v.isChart",true);
        }
        window.setTimeout(
            $A.getCallback(function() {
                console.log("drawGraph timeout ");
                self.drawGraph(cmp,helper);
            }), 2500);
    },
    
    drawGraph : function(cmp,  helper ) {  
        if(this.chart) {
            document.getElementById( "scatterChart" ).remove();     
            let canvas = document.createElement('canvas');     
            canvas.setAttribute('id','scatterChart');     
            document.querySelector('#chart-container').appendChild(canvas); 
        }
        var dataPoints = cmp.get("v.dataPoints");
        var colorList = cmp.get("v.colorList");
        var self = this;
        var aircraftPropList = cmp.get("v.airlineRecomd");
		var icaoFlag = cmp.get("v.ICAOFlag");
        console.log("drawGraph icaoFlag" +icaoFlag);
        var el = document.getElementById( "scatterChart" );//cmp.find('scatterChart').getElement();
        console.log("createGraph el" +el);
        var ctx = el.getContext('2d');
        this.chart = new Chart(ctx, {
            plugins: [{
                 beforeDraw: function (chart, easing) {
                        if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                            var ctx = chart.chart.ctx;
                            var chartArea = chart.chartArea;
                            
                            ctx.save();
                            ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                            ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                            ctx.restore();
                        }
                    }
            }],
            type: 'bubble',
            data: {
                datasets: [
                {
                    label: 'Bubble',
                    data: dataPoints ,
                    backgroundColor: colorList,
                    datalabels: {
                        display: icaoFlag
                    }
                },
				{
                    data: [
                        { x: 0, y: 50 },
                        { x: 105, y: 50}
                    ],
                    type: 'line',
                    borderColor: "#8e5ea2",
                    fill: false,
                    backgroundColor: "rgba(218,83,79, .7)",
                    borderColor: "rgba(218,83,79, .7)",
                    pointRadius: 0,
                    datalabels: {
                        display: false
                    }
                },
                {
                    data: [
                        { x: 50, y: 0},
                        { x: 50, y: 105}
                    ],
                    type: 'line',
                    borderColor: "#8e5ea2",
                    fill: false,
                    backgroundColor: "rgba(218,83,79, .7)",
                    borderColor: "rgba(218,83,79, .7)",
                    pointRadius: 0,
                    datalabels: {
                        display: false
                    }
                }
                ]
            },
            options: {
                plugins: {
                    datalabels: {
                        display: true,
                        anchor: function(context) {
                            var value = context.dataset.data[context.dataIndex];
                            return 'end';
                        },
                        align: function(context) {
                            var value = context.dataset.data[context.dataIndex];
                            return 'end';
                        },
                        color: function(context) {
                            var value = context.dataset.data[context.dataIndex];
                            var color = context.dataset.backgroundColor[context.dataIndex];
                            return color;
                        },
                        formatter: function(value) {
                            return value.v;
                        },
                        offset: 2,
                        padding: 0
                    },
                   
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var index = tooltipItem.index;
                           // console.log("tooltip "+tooltipItem.datasetIndex +" data:"+data+" index"+index);
                            var airline = "";
                       //     console.log("aircraftPropList: "+JSON.stringify(aircraftPropList));
                            if(!$A.util.isEmpty(index) && !$A.util.isUndefined(index)) {
                                var airline = aircraftPropList[index].operatorName;
                               // console.log("hello index "+index + ' airline '+airline);
                            }
                            return airline;
                        }
                    }
                },
                onClick: function(evt, activeElements) {
                    console.log("onClick "+activeElements +" evt "+activeElements[0]);
                    if(!$A.util.isEmpty(activeElements[0]) && !$A.util.isUndefined(activeElements[0])) {
                        var elementIndex = activeElements[0]._index;
                        //this.data.datasets[0].pointBackgroundColor[elementIndex] = 'orange';
                        var airline = aircraftPropList[elementIndex];
                        self.closePopAfterToast(cmp,evt);
                        self.openPop(cmp,evt,airline);
                        //this.update();
                    }
                },
                layout: {
                    padding: {
                        top: 5,
                        right:5
                    }
                },
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            fontColor: "#1874CD",
                            maxTicksLimit : 12,
                            stepSize: 15,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "% Leased",
                            fontColor: "#1874CD"
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            fontColor: "#1874CD",
                            maxTicksLimit : 12,
                            stepSize: 15,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Weights",
                            fontColor: "#1874CD"
                        }
                    }]
                },
                chartArea: {
                    backgroundColor: '#FAFAF9'
                }
            },
            
            
        });
        cmp.set("v.isLoading", false);
    },
        
    openPop : function(component,event, airLine) {
        //console.log("openPop "+JSON.stringify(airLine));
        var operatorId = airLine.operatorId;
        var self = this;
        var title = airLine.operatorName;
        component.set("v.airlineTitle", title);
       // component.set("v.isLoading", true);
        var params = JSON.stringify(airLine);
        console.log("openpop params "+params);
        
        component.set('v.showMore', false);
        component.set('v.moreData', false);
        component.set('v.moreLabel','More');
        var action = component.get('c.getAirlineCriteria');
        action.setParams({ 
            airline : params
        });        
        action.setCallback(this, function(response) {
            var airline = response.getReturnValue();
            console.log('openPop airline '+airline);
            var leases = airline.leasesList;
            var optionsList = [];
            if(!$A.util.isEmpty(leases) && !$A.util.isUndefined(leases)) {
                for (var key in leases) {
                    if (leases.hasOwnProperty(key)) {
                        optionsList.push({value: key, label: leases[key]});
                    }
                };
                airline.leasesList = optionsList;
            }
            component.set("v.airline", airline);
            component.set("v.operatorId" , airline.operatorId);
            console.log('openPop airline '+JSON.stringify(airline));
            var portfolio = airline.leasesVsOwned;
            
            if(!$A.util.isEmpty(portfolio) && !$A.util.isUndefined(portfolio)) {
                var recordLmt = component.get("v.recordLimit");
                console.log('portfolio '+ portfolio.length+ ' recordLmt:' +recordLmt );
                if(portfolio.length > recordLmt) 
                    component.set('v.moreData', true);
            } 
            this.checkDealCreateStatus(component);
            
            var leaseCount = '';
            
            if(!$A.util.isEmpty(airline.numberOfAircraft) && !$A.util.isUndefined(airline.numberOfAircraft) && airline.numberOfAircraft != null) {
                if(!$A.util.isEmpty(airline.leasesList) && !$A.util.isUndefined(airline.leasesList) && airline.leasesList != null) 
                    leaseCount = ' ('+ airline.leasesList.length + ' out of '+airline.numberOfAircraft +')';
            }
            component.set("v.leaseCount", leaseCount);
            
        });
        $A.enqueueAction(action);
        window.setTimeout(
            $A.getCallback(function() {
                console.log("drawGraph timeout ");
                var cmpTarget = component.find('pop');
                $A.util.removeClass(cmpTarget, 'slds-hide');
                $A.util.addClass(cmpTarget, 'slds-show');
                var popup = component.find("pop").getElement();
                if(popup) {
                    var offset = component.find('parentDiv').getElement().getBoundingClientRect().top + 
                        document.documentElement.scrollTop ;
                    var topPadding = (event.clientY + document.documentElement.scrollTop) - offset + 40;
                    var leftOffset = component.find('parentDiv').getElement().getBoundingClientRect().left;
                    var documentWidth = document.documentElement.clientWidth - 20;
                    var left = event.clientX - leftOffset;
                    
                    popup.style.top = (topPadding)+'px';
                    //popup.style.left = -(leftOffset - 10)+'px';
                    popup.style.left = (left-600)+'px';
                    popup.style.display = "inline";
                    popup.style.position = "absolute";
                    //popup.style.width = documentWidth+ "px";
                }
                //component.set("v.isLoading", false);
            }), 500);
        
    },
    
    
    
    
    checkDealCreateStatus : function(component){
        var action = component.get("c.isDealAlreadyCreated");
        var self = this;
        action.setParams({
            operatorId : component.get("v.operatorId"),
            assetId : component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("checkDealCreateStatus : state "+state);
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                console.log("checkDealCreateStatus data: "+data);
                if(data != null) {
                    component.set("v.isDealExist", true);
                    component.set("v.dealId", data);
                }
                else
                    component.set("v.isDealExist", false);
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("checkDealCreateStatus errors "+errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    self.showErrorMsg(component, "Unknown Error");
                }
            }
        });
      	$A.enqueueAction(action);  
    },
    
    
    applyFilter: function(component, helper) {
        var aircraftType = component.find('aircraftType').get("v.value");
        var variant = component.find('variant').get("v.value");
        var engine = component.find('engine').get("v.value");
        var mtow = component.find('mtow').get("v.value");
        var mtowRange = component.find('mtowRange').get("v.value");
        var vintage = component.find("vintageValue").get("v.value");
        var vintageRange = component.find("vintageRange").get("v.value");
        var manager = component.find("ManagerPicklist").get("v.value");
        var region= component.find("RegionPickList").get("v.value");
        console.log('onFilterClick type:'+aircraftType + ' variant:'+variant+' engine:'+engine);
        console.log('onFilterClick mtow:'+mtow + ' mtowRange:'+mtowRange+' vintage:'+vintage+ ' vintageRange:'+vintageRange+ 'Manager:' +manager+ 'Region:'+region);
        
        component.set("v.isLoading", true);
        component.set('v.isDraw', false);
        this.setNoData(component);
        component.set("v.isNoData",false);
        this.displayAirline(component, helper, aircraftType, variant ,
                              engine ,vintage , vintageRange, mtow ,mtowRange,manager,region);
    },
    
    
    fetchPicklistValues: function(component,objDetails,controllerField, dependentField, field) {
        var self = this;
        var action = component.get("c.getDependentMap");
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField 
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                if(field == 'Variant')
                	component.set("v.depnedentFieldMap",StoreResponse);
                else if(field == 'Engine')
                    component.set("v.dependentEngineFieldMap",StoreResponse);
                
                var listOfkeys = []; 
                var ControllerField = []; 
                
                for (var singlekey in StoreResponse) {
                    listOfkeys.push(singlekey);
                }
                
                /*if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push('--- None ---');
                }*/
                
                var assetData = component.get('v.assetData');
                var namespace = component.get('v.namespace');
                var airType = null;
                
                if(assetData!= null)
                	airType = assetData[namespace+'Aircraft_Type__c'];
                
                for (var i = 0; i < listOfkeys.length; i++) {
                    //ControllerField.push(listOfkeys[i]);
                	if(airType == listOfkeys[i]) {
                    	ControllerField.push({value: listOfkeys[i], selected: true});
                        component.find('aircraftType').set("v.value", listOfkeys[i]);
                    }
                    else 
                        ControllerField.push({value: listOfkeys[i], selected: false});
                    
                }  
                if($A.util.isUndefined(airType) || $A.util.isEmpty(airType) || airType == null) {
                    component.find('aircraftType').set("v.value", listOfkeys[0]);
                    airType = listOfkeys[0];
                }
                component.set("v.listControllingValues", ControllerField);
                
                
                if(airType != null) {
                    var ListOfDependentEngineFields = StoreResponse[airType];
                    if(!$A.util.isUndefined(ListOfDependentEngineFields) && ListOfDependentEngineFields != null && 
                       ListOfDependentEngineFields.length > 0){
                        component.set("v.bDisabledDependentEngineFld" , false);  
                        component.set("v.bDisabledDependentFld" , false);  
                        if(field == "Variant")
                        	self.fetchDepValues(component, ListOfDependentEngineFields, "Variant" , true);    
                        else if(field == "Engine")
                        	self.fetchDepValues(component, ListOfDependentEngineFields, "Engine" , true);    
                    }else{
                        if(field == "Engine") {
                            component.set("v.bDisabledDependentEngineFld" , true); 
                            component.set("v.listDependingEngineValues", ['--- None ---']);
                        }
                        else if(field == "Variant") {
                            component.set("v.bDisabledDependentFld" , true); 
                            component.set("v.listDependingValues", ['--- None ---']);
                        }
                    }
                }
				
                var assetData = component.get("v.assetData");                
                if(field == 'Variant' && ($A.util.isEmpty(assetData) || $A.util.isUndefined(assetData) || assetData == null)) {
                    var aircraftType = component.find('aircraftType').get("v.value");
                    component.set("v.isLoading", true);
                    self.displayAirline(component, self, aircraftType, null ,
                                          null ,0 , 0, 0 ,0,null,null);
                } 
                
            }else{
                console.log('response.getState() == ERROR fetchPicklistValues');
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields, field, isFirstLoad) {
        var assetData = component.get('v.assetData');
        var engine = null;
        var variant = null;
        var namespace = component.get('v.namespace');
        if(assetData!= null ){
            engine = assetData[namespace+'Engine_Type__c'];
            if($A.util.isEmpty(engine) || $A.util.isUndefined(engine))
                engine = null;
            variant = assetData[namespace+'Aircraft_Variant__c'];
            if($A.util.isEmpty(variant) || $A.util.isUndefined(variant))
                variant = null;
        }
        console.log("fetchDepValues assetData: " + assetData);
        console.log("fetchDepValues engine:"+engine + " variant:"+variant);
        if(field == 'Variant') {
            var dependentFields = [];
            dependentFields.push({value: '--- None ---', selected: false});
            for (var i = 0; i < ListOfDependentFields.length; i++) {
                //dependentFields.push(ListOfDependentFields[i]);
                if(isFirstLoad && variant != null && variant == ListOfDependentFields[i]) {
                    dependentFields.push({value: ListOfDependentFields[i], selected: true});
                    component.find('variant').set("v.value", ListOfDependentFields[i]);
                }
                else
                    dependentFields.push({value: ListOfDependentFields[i], selected: false});
            }
            component.set("v.listDependingValues", dependentFields);
        }
        else if(field == 'Engine') {
            var dependentFields = [];
            dependentFields.push({value: '--- None ---', selected: false});
            for (var i = 0; i < ListOfDependentFields.length; i++) {
            //dependentFields.push(ListOfDependentFields[i]);
                if(isFirstLoad && engine != null && engine == ListOfDependentFields[i]) {
                    dependentFields.push({value: ListOfDependentFields[i], selected: true});
                    component.find('engine').set("v.value", ListOfDependentFields[i]);
                }
                else
                    dependentFields.push({value: ListOfDependentFields[i], selected: false});
            }
        	component.set("v.listDependingEngineValues", dependentFields);
        }
    },
    
    populateFilterValues : function(component,  helper) {
        var controllingFieldAPI = component.get("v.controllingFieldAPI");
        var dependingFieldAPI = component.get("v.dependingFieldAPI");
        var dependingEngineFieldAPI = component.get("v.dependingEngineFieldAPI");
        var objDetails = component.get("v.objDetail");
        helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingFieldAPI, 'Variant');
        helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingEngineFieldAPI, 'Engine');
        helper.getManagerPicklist(component,"ManagerPicklist");
        helper.getRegionPickList(component,"RegionPickList");
        helper.getVintagePicklist(component, "vintageValue");
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('Error '+toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('Error '+toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('Error '+toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
      
    
})