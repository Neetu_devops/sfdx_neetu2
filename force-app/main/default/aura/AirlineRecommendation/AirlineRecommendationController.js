({
    doInit: function(component, event, helper) {
        var action = component.get('c.getDealType');
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            component.set('v.isMA', false);
            if(!$A.util.isEmpty(data) && !$A.util.isUndefined(data) && data == 'MA') 
                component.set('v.isMA', true);
        });
        $A.enqueueAction(action);

        let deviceType = $A.get("$Browser.formFactor");
        console.log('deviceType : '+deviceType);
        if(deviceType.toUpperCase() != 'DESKTOP') {
            component.set("v.loadedInDesktop", false);
        }
       
    },
    
    onControllerFieldChange: function(component, event, helper) {     
        var controllerValueKey = event.getSource().get("v.value"); // get selected controller field value
        var depnedentFieldMap = component.get("v.depnedentFieldMap");
        var dependentEngineFieldMap = component.get("v.dependentEngineFieldMap");
        component.find('engine').set("v.value", null);
        component.find('variant').set("v.value", null);
        
        if (controllerValueKey != '--- None ---') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            var ListOfDependentEngineFields = dependentEngineFieldMap[controllerValueKey];
            
            // for variant
            if(ListOfDependentFields.length > 0){
                component.set("v.bDisabledDependentFld" , false);  
                helper.fetchDepValues(component, ListOfDependentFields, 'Variant', false);    
            }else{
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['--- None ---']);
            }
            
            // for engine
            if(ListOfDependentEngineFields.length > 0){
                component.set("v.bDisabledDependentEngineFld" , false);  
                helper.fetchDepValues(component, ListOfDependentEngineFields, 'Engine', false);    
            }else{
                component.set("v.bDisabledDependentEngineFld" , true); 
                component.set("v.listDependingEngineValues", ['--- None ---']);
            }
            
        } else {
            component.set("v.listDependingValues", ['--- None ---']);
            component.set("v.listDependingEngineValues", ['--- None ---']);
            component.set("v.bDisabledDependentFld" , true);
            component.set("v.bDisabledDependentEngineFld" , true); 
        }
    },
    
    onFilterClick : function(component,event,helper) {
        component.set("v.showAirline", false);
        component.set("v.airlineListData", null);
        helper.closePopAfterToast(component,event);
        helper.applyFilter(component, helper);
	},
    
    onItemClick: function(component,event,helper){
        var airline = component.get("v.airline");
        console.log("onItemClick operatorId:" +airline.operatorId);
        window.open('/' + airline.operatorId);  
    },
    
    onDealClick: function(component,event,helper){
        var deal = component.get("v.dealId");
        console.log("ondealclick deal:" +deal);
        window.open('/' + deal);  
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
    
    createMarketingActivity : function(component, event, helper) {
        helper.closePopAfterToast(component, event);
        console.log("createMarketingActivity ");
        var operatorId = component.get("v.operatorId");
        console.log("createMarketingActivity "+operatorId);
        if(operatorId == null || operatorId == undefined) {
            helper.showResultMsg(component, event, helper, null,"Cannot create the deal as the Operator is not created on LeaseWorks.");
            return;
        }
        
        var aircraftPropList = component.get("v.airlineRecomd");
        for(var i=0;i < aircraftPropList.length; i++) {
			var aircraftProp = aircraftPropList[i];
            if(aircraftProp.operatorId == operatorId) {
                console.log("matchhh "+aircraftProp.operatorName);
                var assetData = component.get("v.assetData");
                var msnValue = null;
                if(!$A.util.isEmpty(assetData) && !$A.util.isUndefined(assetData))
                    msnValue = assetData.Name;
                var action = component.get('c.newMarkActivity');
                action.setParams({
                    operatorId : aircraftProp.operatorId,
                    operatorName : aircraftProp.operatorName,
                    aircraftType : aircraftProp.aircraftType,
                    msn : msnValue,
                    fleetId : component.get("v.recordId")
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log("createMarketingActivity : state "+state);
                    if (state === "SUCCESS") {
                        var airlineCriteria = response.getReturnValue();
                        console.log('airline criteria response '+JSON.stringify(airlineCriteria));
                        console.log('airline criteria response '+airlineCriteria);
                        if(airlineCriteria != null)
                            helper.showResultMsg(component, event, helper, airlineCriteria,'');
                        else 
                            helper.showResultMsg(component, event, helper, null,"Error in creating record");
                    }
                    else if (state === "ERROR") {    
                        var errors = response.getError();
                        console.log("createMarketingActivity errors "+errors);
                        if (errors) {
                            console.log("createMarketingActivity errors mm: "+errors[0].message);
                            if(errors[0] != undefined && errors[0].message != undefined) {
                                if(errors[0].message.includes('AID no asset')) 
                                    helper.showErrorMsg(component, "Deal cannot be created because there is no asset selected");
                                else if(errors[0].message.includes('No asset Id')) 
                                    helper.showErrorMsg(component, "Asset not found. Cannot create deal");
                                else 
                                    helper.handleErrors(errors);
                            }
                            else 
                                helper.handleErrors(errors);
                        } else {
                            console.log("createMarketingActivity Unknown error");
                            helper.showErrorMsg(component, "Error in creating record");
                        }
                    }
                });
                $A.enqueueAction(action); 
                break;
            }
        }
    },
    
    onClickMore: function(component, event, helper) {
        var flag = component.get("v.showMore");
        if(flag == true) 
            component.set("v.moreLabel", "More");
        else
            component.set("v.moreLabel", "Less");
        component.set("v.showMore", !flag);
    },
    
    onScoreFilterChange: function(component, event, helper) {
        var buttonLabel= event.getSource().get("v.value") ;
        console.log("onScoreFilterChange " + buttonLabel);
        var button = event.getSource();
        var grpButtonList = component.find("grpButton");
        grpButtonList.forEach(function(item) {
            item.set("v.variant", "Neutral");
        });
        button.set("v.variant", "brand");
        component.set("v.scoreFilter",buttonLabel);
        helper.applyFilter(component, helper);
    },
    
    getToggleButtonValue:function(component,event,helper){
        var checkCmp = component.find("tglbtn").get("v.checked");
        component.set("v.ICAOFlag",checkCmp);
        console.log("getToggleButtonValue "+checkCmp);
        helper.drawGraph(component,helper);
    },
    
    drawChart : function(component, event, helper) {
        
    },

    ExpandList : function(component, event, helper) {
        helper.DisplayAirlines(component,event,'OperatorList');
    }
})