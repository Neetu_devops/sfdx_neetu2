({
    doInit : function(component, event, helper) {     
        component.set('v.showSpinner', true);
        helper.fetchPageLayout(component, event);
    },

	
    handleLoad : function(component, event, helper) {
        setTimeout(function(){ 
             if(document.getElementById('layoutSection') != null){ 
             	document.getElementById('layoutSection').scrollIntoView(true);
             }
            component.set("v.showSpinner", false);  
        }, 1000);
    },
    
    
    handleSubmit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        
        var recordName = event.getParam("fields").Name;
        component.set("v.recordName", recordName);
    },

    
    handleError : function(component, event, helper) { 
        component.set('v.showSpinner', false);
        document.getElementById("errorMsg").style.display = 'block';
        document.getElementById("errorMsg").scrollIntoView(true);
    },
    
    
    handleSuccess : function(component, event, helper) {
        document.getElementById("errorMsg").style.display = 'none';
        
        if(component.get("v.actionType") != 'Edit') {
            component.set("v.selectedRecordId", event.getParams().response.id);
        }
        
        helper.showResult(component, event);
    },
    
    
    closeComponent : function(component, event, helper) {
        component.set("v.createNewRecord", false);
    }
})