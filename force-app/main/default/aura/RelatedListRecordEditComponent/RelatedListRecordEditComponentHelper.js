({
    showMessage: function (cmp, message, type, recordId) {
        var toastEvent = $A.get("e.force:showToast");
        let toastParams;
        if (recordId != undefined) {
            toastParams = {
                message: message,
                messageTemplate: cmp.get("v.relatedListLabel") + ' "{1}" was saved',
                messageTemplateData: ['Salesforce', {
                    url: '/' + recordId,
                    label: cmp.get("v.recordName"),
                }],
                type: type
            };
        }
        else {
            toastParams = {
                message: message,
                type: type
            };
        }
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },


    //To get the Page Layout fields
    fetchPageLayout: function (component, event) {
        var recordId;
        var actionType = component.get("v.actionType");

        var action = component.get("c.getPageLayoutFields");
        action.setParams({
            'recordId': component.get("v.selectedRecordId"),
            'objectName': component.get("v.relatedListObject"),
            'recordTypeId': component.get("v.selectedRTId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var result = JSON.parse(response.getReturnValue());
                console.log('result: ', result);
                if (result == null || result == undefined || result == '') {
                    this.showMessage(component, 'Could not retrieve layout for this record type', 'error');
                }
                else {
                    component.set('v.showLayout', true);
                    var lstSections = result.lstSections;
                    console.log('lstSections: ', lstSections);

                    console.log(result, lstSections, actionType);

                    if (actionType == 'Clone') {
                        for (var key in lstSections) {
                            if (lstSections[key].lstFields != undefined && lstSections[key].lstFields != '') {
                                for (let key2 in lstSections[key].lstFields) {
                                    var field = lstSections[key].lstFields[key2];
                                    console.log(field, ' ---- ', result.selectedRecord['' + field.fieldName]);
                                    field["value"] = result.selectedRecord['' + field.fieldName];
                                }
                            }
                        }

                        component.set("v.selectedRecordId", '');
                    }

                    var recform = component.find("myform");
                    if (result.recordTypeId != undefined && result.recordTypeId != '') {
                        recform.set("v.recordTypeId", result.recordTypeId);
                    }

                    component.set("v.layoutSections", lstSections);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });

        $A.enqueueAction(action);
    },


    showResult: function (component, event) {
        var actionType = component.get("v.actionType");
        var openRecordPage = component.get("v.openRecordPage");

        if (actionType == 'Edit') {
            this.showMessage(component, component.get("v.relatedListLabel") + ' "' + component.get("v.recordName") + '" was saved', 'success');
        }
        else {
            this.showMessage(component, component.get("v.relatedListLabel") + ' "' + component.get("v.recordName") + '" was created', 'success', component.get("v.selectedRecordId"));
            if (openRecordPage) {
                this.navigateToRecordPage(component, event, component.get("v.selectedRecordId"));
            }
        }

        component.set('v.showSpinner', false);
        component.set("v.createNewRecord", false);

        var refreshEvt = component.get("e.refreshRelatedList");
        refreshEvt.fire();

        var appRefreshEvt = $A.get("e.c:CommercialTermListRefreshEvent");
        appRefreshEvt.fire();

        //Updated on 28th Aug 2019 for AssetFinder
        if (!component.get("v.isAssetFinder")) {
            $A.get('e.force:refreshView').fire();
        }
    },


    navigateToRecordPage: function (component, event, recordId) {
        if (recordId != undefined && recordId != '' && recordId != null) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId,
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
    },


    //Show the error toast when any error occured
    handleErrors: function (errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };

        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }

                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }

                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    }
                }
            });
        }
    }
})