({
    loadData: function(component, event) {
        
        if(component.get("v.selectedTab") == undefined && component.get("v.includeAirlines") == true){
            component.set("v.selectedTab", 'Airlines')
        }else if(component.get("v.selectedTab") == undefined && component.get("v.includeLessors") == true){
            component.set("v.selectedTab", 'Lessors')
        }else if(component.get("v.selectedTab") == undefined && component.get("v.includeCounterparties") == true){
            component.set("v.selectedTab", 'CounterParties')
        }
        
        var airlineCheck = false;
        var lessorCheck = false;
        var counterPartyCheck = false;
        var currentProspectList;
        var oldProspectList;
        var checkBoxControlName;
        var checkServerCall;
        
        if(component.get("v.selectedTab") == 'Airlines'){
            airlineCheck = true;
            currentProspectList = 'v.newselectedAirlineRecords';
            oldProspectList = 'v.selectedAirlineRecords';
            checkBoxControlName = 'v.airlinesCheckBoxControl';
            checkServerCall = 'v.isAirlinesLoaded';
            
        }else if(component.get("v.selectedTab") == 'Lessors'){
            lessorCheck = true;
            currentProspectList = 'v.newselectedLessorRecords';
            oldProspectList = 'v.selectedLessorRecords';
            checkBoxControlName = 'v.lessorsCheckBoxControl';
            checkServerCall = 'v.isLessorsLoaded';
            
        }else{
            counterPartyCheck = true;
            currentProspectList = 'v.newselectedCounterpartyRecords';
            oldProspectList = 'v.selectedCounterpartyRecords';
            checkBoxControlName = 'v.counterPartiesCheckBoxControl';
            checkServerCall = 'v.isCounterPartiesLoaded';
            
        }
        if(component.get(checkServerCall)){
        var action = component.get("c.getProspectData");
        this.showSpinner(component);
        
        action.setParams({
                "airlineCheck": airlineCheck,
                "lessorCheck": lessorCheck,
                "counterPartyCheck": counterPartyCheck
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
                 this.hideSpinner(component);
            if (response.getState() === "SUCCESS") {
                
                var allData =  response.getReturnValue();
                
                    if(component.get("v.selectedTab") == 'Airlines'){
                        component.set(currentProspectList, allData.airlines);
                    }else if(component.get("v.selectedTab") == 'Lessors'){
                        component.set(currentProspectList, allData.lessors);
                    }else{
                        component.set(currentProspectList, allData.counterparties);
                }
                    
                    component.set(checkServerCall, false);
                    
                    if(component.get(currentProspectList).length > 0){
                        this.check_CheckBox(component, event, component.get(currentProspectList), component.get(oldProspectList), checkBoxControlName);
                }
                    
                    if(component.get("v.checkProfile") == true){
                        this.checkTradingProfile(component, event);
                }
            }
            else if(state === "ERROR") {
                
                var errors = response.getError();
                this.handleErrors(errors);
            }
            });
            $A.enqueueAction(action);
        }
    },
    
    checkTradingProfile : function(component, event) {
        var action = component.get("c.CheckTradingProfileofProspect");
        this.showSpinner(component);
        
        action.setParams({
            "airlines": JSON.stringify(component.get("v.newselectedAirlineRecords")),
            "lessors": JSON.stringify(component.get("v.newselectedLessorRecords")),
            "counterparties": JSON.stringify(component.get("v.newselectedCounterpartyRecords")),
            "aircrafts"  : JSON.stringify(component.get("v.selectedAircraftRecords"))
        }); 
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (response.getState() === "SUCCESS") {
                var jsonData =  response.getReturnValue();
                
                if(jsonData != null){
                    var airlines = this.checkProfileData(component, event, jsonData, component.get("v.newselectedAirlineRecords"));
                    var lessors = this.checkProfileData(component, event, jsonData, component.get("v.newselectedLessorRecords"));
                    var counterParties = this.checkProfileData(component, event, jsonData, component.get("v.newselectedCounterpartyRecords"));
                    
                    component.set("v.newselectedAirlineRecords",airlines);
                    component.set("v.newselectedLessorRecords",lessors);
                    component.set("v.newselectedCounterpartyRecords",counterParties);
                    
                    component.set("v.checkProfile",true);  
                }
            }else if(state === "ERROR") {
                
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    // Checks the checkBox Of prospects which is already selected
    check_CheckBox : function(component, event, newRecordList, oldRecordList, checkBoxName) {
        
        var oldArrId = [];
        for(var i = 0; i< oldRecordList.length; i++){
            oldArrId.push(oldRecordList[i].Id);
        }
        
        for(var i = 0; i< newRecordList.length; i++){
            if(oldArrId.includes(newRecordList[i].recordId)){
                newRecordList[i].isChecked = true;
            }
        }
        var numberOfCheckedVal = 0;
        for(var i = 0; i< newRecordList.length; i++){
            if(newRecordList[i].isChecked == true){
                numberOfCheckedVal++;
            }
        }
        // Select/Unselect the Header CheckBox of Airlines/Lessors/CounterParties
        if(numberOfCheckedVal == newRecordList.length){
            component.set(checkBoxName, false);
        }
    },
    
    //Controls the Header CheckBox of Airlines/Lessors/CounterParties
    checkBoxController: function(component, event, tradeProfileCheckBox, checkBoxName) {
        
        if(component.get(checkBoxName) === false){
            component.set(checkBoxName, true);
            if(component.find(tradeProfileCheckBox) != undefined){
                component.find(tradeProfileCheckBox).set("v.value",false);
            }
        }else{
            component.set(checkBoxName, false);
        }
    },
    
    uploadData: function(component, event, recordList) {
        
        var saveId = [];
        for (var i = 0; i < recordList.length; i++) {
            if(recordList[i].isChecked){
                saveId.push({'Id':recordList[i].recordId,'Name':recordList[i].recordName});
            }
        }
        return saveId;
    },
    
    checkProfileData: function(component, event,jsonList, recordList) {
        for(var j = 0; j < recordList.length; j++){
            if(recordList[j].recordId in jsonList){
                var checkprofile = jsonList[recordList[j].recordId];
                recordList[j].details = checkprofile['details'];
                recordList[j].profileOk = checkprofile['profileOk'];
            } 
        }
        return recordList;
    },
    
    
    selectValidProfileProspect: function(component, event, checkBoxName, flag, recordList) {
        
        if(flag == true){
            for(var j = 0; j < recordList.length; j++){
                if(recordList[j].profileOk != undefined && recordList[j].profileOk== true ){
                    recordList[j].isChecked = true;
                }else{
                    recordList[j].isChecked = false;
                }
            }
        }else{
            for(var j = 0; j < recordList.length; j++){
                if(recordList[j].profileOk != undefined && recordList[j].profileOk== true ){
                    recordList[j].isChecked = false;
                }
            }
        }
        this.headerCheckboxManage(component, event, checkBoxName, recordList);
        return recordList;
    },
    
    headerCheckboxManage : function (component, event, checkBoxName, recordList) {
        var flag = false;
        for(var j = 0; j < recordList.length; j++){
            if(recordList[j].isChecked == false){
                flag = true;
                break;
            }
        }
        if(flag == true){
            component.set(checkBoxName, true);
        }else{
            component.set(checkBoxName, false);
        }
    },
    
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinnerChild",true);
       // var spinner = component.find("mySpinner");
       // $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinnerChild",false);
        //var spinner = component.find("mySpinner");
       // $A.util.addClass(spinner, "slds-hide");
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    }
})