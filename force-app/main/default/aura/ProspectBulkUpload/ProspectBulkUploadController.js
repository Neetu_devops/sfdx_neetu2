({
    doInit: function(component, event, helper) {
        helper.loadData(component, event);
    },
    
    cancelAction: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    onchangeTab : function(component, event, helper) {
        helper.loadData(component, event);
    },
    
    uploadSelected: function(component, event, helper) {
        helper.showSpinner(component);
        
        var saveAirLinesRecord = [];
        var saveLessorsRecord = [];
        var saveConterPartiesRecord = [];
       
        if(component.get("v.includeAirlines") && component.get("v.newselectedAirlineRecords").length > 0){
            saveAirLinesRecord = helper.uploadData(component, event, component.get("v.newselectedAirlineRecords"));
            component.set("v.selectedAirlineRecords", saveAirLinesRecord);
        }
        if(component.get("v.includeLessors") && component.get("v.newselectedLessorRecords").length > 0 ){
            saveLessorsRecord = helper.uploadData(component, event, component.get("v.newselectedLessorRecords"));
            component.set("v.selectedLessorRecords", saveLessorsRecord);
        }
        if(component.get("v.includeCounterparties") && component.get("v.newselectedCounterpartyRecords").length > 0){
            saveConterPartiesRecord =  helper.uploadData(component, event, component.get("v.newselectedCounterpartyRecords"));
            component.set("v.selectedCounterpartyRecords", saveConterPartiesRecord);
        }
        helper.hideSpinner(component);
        component.set("v.isOpen", false);
    },
    
    selectAll: function(component, event, helper) {
        
        var selectedHeaderId = event.getSource().getLocalId();
        
        if(selectedHeaderId === 'box3'){
            var componentToGet = 'AirLinesbox';
            var checkBoxName = 'v.airlinesCheckBoxControl';
            var tradeProfileCheckBox = 'airlineProfileCheck';
            helper.checkBoxController(component, event, tradeProfileCheckBox, checkBoxName);
            
        }else if(selectedHeaderId === 'box4'){
            var componentToGet = 'lessorbox';
            var checkBoxName = 'v.lessorsCheckBoxControl';
            var tradeProfileCheckBox = 'lessorProfileCheck';
            helper.checkBoxController(component, event, tradeProfileCheckBox, checkBoxName);
            
        }else{
            var componentToGet = 'counterpartybox';
            var checkBoxName = 'v.counterPartiesCheckBoxControl';
            var tradeProfileCheckBox = 'counterPartyProfileCheck';
            helper.checkBoxController(component, event, tradeProfileCheckBox, checkBoxName);
        }
        
        var selectedHeaderCheck = event.getSource().get("v.value");
        var getAllId = component.find(componentToGet);
        
        if(!Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find(componentToGet).set("v.value", true);
            }else{
                component.find(componentToGet).set("v.value", false);
            }
        }else{
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find(componentToGet)[i].set("v.value", true);
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find(componentToGet)[i].set("v.value", false);
                }
            } 
        }  
    },
    
    checkTradingProfile : function(component, event, helper) {
        
        helper.checkTradingProfile(component, event);
    },
    
    selectValidProfileProspect : function(component, event, helper) {
        var selectedHeaderId = event.getSource().getLocalId();
        var flag = component.find(selectedHeaderId).get("v.value");
       
        if(selectedHeaderId == 'airlineProfileCheck'){
            var checkBoxName = 'v.airlinesCheckBoxControl';
            var airlines = helper.selectValidProfileProspect(component, event, checkBoxName, flag, component.get("v.newselectedAirlineRecords"));
       		component.set("v.newselectedAirlineRecords",airlines);
        }
        else if(selectedHeaderId == 'lessorProfileCheck'){
            var checkBoxName = 'v.lessorsCheckBoxControl';
            var lessors = helper.selectValidProfileProspect(component, event, checkBoxName, flag, component.get("v.newselectedLessorRecords"));
        	component.set("v.newselectedLessorRecords",lessors);
        }
        else if(selectedHeaderId == 'counterPartyProfileCheck'){
            var checkBoxName = 'v.counterPartiesCheckBoxControl';
            var counterParties = helper.selectValidProfileProspect(component, event, checkBoxName, flag, component.get("v.newselectedCounterpartyRecords"));
        	component.set("v.newselectedCounterpartyRecords",counterParties);
        }
        
    }
    
})