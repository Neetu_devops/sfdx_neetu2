({
    doInit: function (component, event, helper) {
        helper.hideSpinner(component, event, helper);
        console.log('Inside Init');
    },
    
    createSnapshot:function(component, event, helper) {        
        var commentsValue = component.get("v.comments");
        console.log('commentsValue::',commentsValue);
        
        if(commentsValue === undefined || !commentsValue.trim() || commentsValue === null){
            var inputField = component.find("snapshotComment");
            inputField.reportValidity();
        }
        else{
            helper.showSpinner(component, event, helper);
            var recordId =  component.get("v.recordId");
            var cmt =  component.get("v.comments");
            var createSnapshotAction = component.get("c.createSnapShot");
            createSnapshotAction.setParams({
                "recordId": recordId,
                "comments": cmt
            });
            
            createSnapshotAction.setCallback(this, function (response) {
                helper.hideSpinner(component, event, helper);
                var state = response.getState();
                console.log(state);
                if(state === "SUCCESS") {
                    console.log('Success');
                    $A.get('e.force:refreshView').fire();
                    var resultsToast = $A.get("e.force:showToast");
                    if(response.getReturnValue()=='SUCCESS'){
                        resultsToast.setParams({
                            "Title": "Snapshot Created",
                            "type": "success",
                            "message": 'Snapshot created successfully'
                        });
                    }
                    else{
                        resultsToast.setParams({
                            "Title": "Error",
                            "type": "error",
                            "message": response.getReturnValue()
                        });
                    }
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire(); 
                }
                else{
                    var errors = response.getError();
                    helper.handleErrors(component,errors);
                }
            });
            $A.enqueueAction(createSnapshotAction); 
        }
    },    
    handleCancel: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
})