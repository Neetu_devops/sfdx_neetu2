({
    doInit: function (component, event, helper) {
        helper.getParamValue('METRefreshAll', 'c__SuppRentId', component);
    },

    closeModel: function (component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
        helper.suppRentPagePreview(component);
    },

    submitData: function (component, event, helper) {
        // Set isModalOpen attribute to false
        //Add your code to call apex method or do some processing
        component.set("v.isModalOpen", false);
        helper.refreshAllSuppRentEventReq(component);
    },
})