({
    fetchData : function(component, helper) {
        var action = component.get("c.getDealCommercialTerms");
        action.setParams({
            'dealId' : component.get("v.recordId"),
            'fieldSetName' : component.get("v.fieldSetName"),
            'sortOrderField': component.get("v.defaultSortOrder")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                console.log('result:', response.getReturnValue());
                var result = response.getReturnValue();
                var headers = result.headers;
                
                //Aligning list of columns so that Name should always come as a first column
                headers = helper.alignColumns(headers);   
                
                //setting up attributes data                
                component.set("v.showSpinner", false);                      
                component.set("v.ctListSize", result.cwList.length);                
                component.set("v.columns", headers);
                component.set("v.commercialTermData", result.cwList); 
				
                if (component.get("v.titleComponent") != '' && component.get("v.titleComponent") != undefined) {
                	component.set("v.relatedListLabel", component.get("v.titleComponent")); 
                }
                else {
                    component.set("v.relatedListLabel", result.objectLabel); 
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        
        $A.enqueueAction(action);
    },    
    
    
    alignColumns : function(headers) {
        var columns = [];
        
        for(var i=0; i<headers.length; i++) {
            if(headers[i].fieldName.toLowerCase() == 'name') {
                headers[i].type = headers[i].type.toLowerCase();
                
                columns.push(headers[i]);
                break;
            }			                              
        }
        
        for(var i=0; i<headers.length; i++) {
            headers[i].type = headers[i].type.toLowerCase();
            
            if(headers[i].fieldName.toLowerCase() != 'name') {
                columns.push(headers[i]);
            }			                              
        }
        
        return columns;
    },
      handleErrors : function(errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error": type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error": type,
            duration: '7000'
        };
        if(typeof errors === 'object') {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });  
                    };
                }
            });
        }
        else if(typeof errors === "string" && errors.length != 0){
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})