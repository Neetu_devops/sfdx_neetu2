({
    doInit : function(component, event, helper) {
        component.set("v.showSpinner", true);  
        helper.fetchData(component, helper);
    },
    
           
    addNewRecord : function (component, event, helper) {
       component.set("v.createNewRecord", true);
    },
    
    refreshData : function(component, event, helper) {
        component.set("v.showSpinner", true);  
        helper.fetchData(component, helper);
    },
})