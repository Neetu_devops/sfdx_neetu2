({
    doInit : function(component, event, helper) {
        var sObject = component.get("v.rowData");
        var field = component.get("v.field");   
        var fName = field.fieldName;
        var value;
        
        //retrieving record field value
        if (fName.includes(".")) {
            var string1 = fName.substring(0, fName.indexOf("."));
            var string2 = fName.substring(fName.indexOf(".") + 1);
            if(sObject[string1] != undefined){
            value = sObject[string1][string2];
            }
        }
        else {
            value = sObject[fName];
        }
                
        //updating Date or DateTime field value format
        if(value != undefined && value != '' && field.type == 'date') {
            value = $A.localizationService.formatDate(value, "MM/DD/YYYY");
        }
        else if(value != undefined && value != '' && field.type == 'datetime') {
            value = $A.localizationService.formatDateTime(value, "MM/DD/YYYY hh:mm a");
        }
        
        //retrieving Id of the given record
        if(field.fieldName == 'Name') {
            component.set("v.dataId", sObject['Id']);  
        }
        
        //retrieving Referenced field data
        if(field.type == 'reference' || field.type == 'REFERENCE') {
            var fieldName;
            if (sObject[field.fieldName] != null){
                if (field.fieldName.indexOf('__c') != -1){
                    fieldName = field.fieldName.replace('__c','__r')+'.Name';
                }
                else {
                    fieldName = field.fieldName.substring(0, field.fieldName.length - 2) +'.Name';
                }
                var string1 = fieldName.substring(0, fieldName.indexOf("."));
                var string2 = fieldName.substring(fieldName.indexOf(".") + 1);
                if(sObject[string1] != undefined){
                    value = sObject[string1][string2];
                }
                component.set("v.dataId", sObject[fName]);  
            }
        }
        
        component.set("v.fieldval", value);
    }
})