({
    searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        let action = component.get("c.fetchLookUpValuesWithAdditionalFields");
        // set param to method  
        action.setParams({
                            'searchKeyWord': getInputkeyWord,
                            'ObjectName' : component.get("v.objectAPIName"),
                            'whereClause' : component.get("v.whereClause"),
            				'fields' : component.get("v.additionalFieldsTobeDisplayed")  
                        });
        // set a callBack    
        action.setCallback(this, function(response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                    
                    //set additional field information if any.
                    if(!$A.util.isEmpty(component.get("v.additionalFieldsTobeDisplayed"))){
                        var fields = component.get("v.additionalFieldsTobeDisplayed").split(',');
                        for(var i =0 ; i < storeResponse.length; i++){
                            for(var j =0 ; j < fields.length; j++){
                                if(storeResponse[i][fields[j]] != undefined){
                                	storeResponse[i].Name = storeResponse[i].Name + ' - ' + storeResponse[i][fields[j]];
                                }
                            }
                        }    
                    }
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    }
})