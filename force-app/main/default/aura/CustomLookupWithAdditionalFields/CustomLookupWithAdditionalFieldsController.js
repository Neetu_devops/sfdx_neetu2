({
    init : function(component,event,helper){
        let selectedRecord = component.get("v.selectedRecord");
        
        if(selectedRecord && selectedRecord.Id != null && selectedRecord.Id != undefined && selectedRecord.Id != ''){
            let lookupSearch = component.find("lookupSearch");
            $A.util.removeClass(lookupSearch, 'slds-show');
            $A.util.addClass(lookupSearch, 'slds-hide');
            
            let forclose = component.find("lookup-pill");
            $A.util.addClass(forclose, 'slds-show');
            $A.util.removeClass(forclose, 'slds-hide');
            
            let forclose1 = component.find("searchRes");
            $A.util.addClass(forclose1, 'slds-is-close');
            $A.util.removeClass(forclose1, 'slds-is-open');
            
            let lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');    
        }
    },
    onfocus : function(component,event,helper){
        let forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC  
        let getInputkeyWord = '';
        helper.searchHelper(component,event,getInputkeyWord);
    },
    onblur : function(component,event,helper){
        setTimeout(function(){ 
            let forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-close');
            $A.util.removeClass(forOpen, 'slds-is-open');
        }, 500);
    },
    keyPressController : function(component, event, helper) {
        // get the search Input keyword   
        let getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if( getInputkeyWord.length > 0 ){
            let forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            let forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    
    // function for clear the Record Selaction 
    clear :function(component,event,heplper){
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null);
        //component.set("v.selectedRecord", {} );
        
        let getSelectRecord = component.get("v.selectedRecord");
        getSelectRecord.Id = "";
        getSelectRecord.Name = "";
        
        let compEvent = component.getEvent("oSelectedRecordEvent");
        // set the Selected sObject Record to the event attribute.  
        compEvent.setParams({
            "recordByEvent" : getSelectRecord,
            "fieldName" : component.get("v.fieldAPIName")
        });
        compEvent.fire();
        
        let pillTarget = component.find("lookup-pill");
        let lookUpTarget = component.find("lookupField"); 
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        // get the selected Account record from the COMPONENT event
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        component.set("v.selectedRecord", selectedAccountGetFromEvent);

        let lookupSearch = component.find("lookupSearch");
        $A.util.removeClass(lookupSearch, 'slds-show');
        $A.util.addClass(lookupSearch, 'slds-hide');
        
        let forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        let forclose1 = component.find("searchRes");
        $A.util.addClass(forclose1, 'slds-is-close');
        $A.util.removeClass(forclose1, 'slds-is-open');
        
        let lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');  
        
    },
    // automatically call when the component is done waiting for a response to a server request.  
    hideSpinner : function (component, event, helper) {
        component.set("v.showLoading", false);   
    },
    // automatically call when the component is waiting for a response to a server request.
    showSpinner : function (component, event, helper) {
        component.set("v.showLoading", true);
    },
})