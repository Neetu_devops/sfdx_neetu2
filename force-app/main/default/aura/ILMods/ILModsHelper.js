({
    
    getNamespacePrefix : function(component, event, helper) {
        var action = component.get('c.getNamespacePrefix');
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getNamespacePrefix " +allValues);
            if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues))
                component.set("v.namespace", allValues);
            else
                component.set("v.namespace", "");
            helper.getData(component);
        });
        $A.enqueueAction(action);
    },
    
	getData : function(component){
        var action = component.get('c.getColumns');
        
        var objectName = component.get('v.objectName');
        if(!component.get('v.objectName').includes('leaseworks')) {
            objectName = component.get('v.namespace') + component.get('v.objectName');
        	component.set('v.objectName', objectName);
        }
        
        console.log("getData objectName:" +objectName);
        action.setParams({
            objectName : objectName,
            fieldSetName : component.get('v.fieldSet')
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getParentData values " +JSON.stringify(allValues));
            try{
                component.set("v.parentData" , allValues);
            }
            catch(e) {console.log("Exception "+e);}
        });
        $A.enqueueAction(action);
    },
    
    fireSuccessToast : function(component, recordId) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Success!",
            message: "The record has been created successfully.",
            messageTemplate: '{1} has been created successfully.',
            messageTemplateData: ['Salesforce', {
                url: '/'+recordId,
                target: '_blank',
                label: 'Interaction Log',
            }],
            duration:'5000',
            key: 'info_alt',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
        this.refreshData(component);
    },
    
    refreshData : function(component){ 
        var self = this;
        setTimeout(function() {
            self.getData(component);
        }, 500);
    },
    
    fireFailureToast : function(component,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'title' : 'Failed', 
            'message' : message,
            'type':'error'
        }); 
        toastEvent.fire(); 
    },
    
})