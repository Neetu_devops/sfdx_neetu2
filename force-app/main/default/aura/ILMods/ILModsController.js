({
	doInit : function(component, event, helper) {
		helper.getNamespacePrefix(component,event, helper);
	},
    
    buttonClick: function(component,event,helper){  
        var buttonLabel= event.getSource().get("v.value") ;
        console.log("buttonClick " + buttonLabel);
        var button = event.getSource();
        var grpButtonList = component.find("grpButton");
        grpButtonList.forEach(function(item) {
            item.set("v.variant", "Neutral");
        });
        button.set("v.variant", "brand");
        component.set("v.callType",buttonLabel);
    },
    
    onSubmit  : function(component, event, helper) {
        console.log("onSubmit called");
        event.preventDefault();
        var eventFields = event.getParam("fields");
        
        console.log("onSubmit eventFields " + JSON.stringify(eventFields));
        
        var namespace = component.get('v.namespace');
        
        eventFields[namespace+'Type_Of_Interaction__c'] = component.get("v.callType");
        
        var sObj = component.get("v.sobjecttype");
        if(!$A.util.isEmpty(sObj) && !$A.util.isUndefined(sObj)) {
            if(sObj.includes("Marketing_Activity__c")) {
                eventFields[namespace+'Related_To_Marketing_Activity__c'] = component.get("v.recordId");
            } else if(sObj.includes("Operator__c")) {
                eventFields[namespace+'Related_To_Operator__c'] = component.get("v.recordId");
            } else if(sObj.includes("Lease__c")) {
                eventFields[namespace+'Related_To_Lease__c'] = component.get("v.recordId");
            } else if(sObj.includes("Aircraft__c")) {
                eventFields[namespace+'Related_To_Aircraft__c'] = component.get("v.recordId");
            } else if(sObj.includes("Bank__c")) {
                eventFields[namespace+'Related_To_Counterparty__c'] = component.get("v.recordId");
            } else if(sObj.includes("Delinquency_Data__c")) {
                eventFields[namespace+'Related_To_Delinquency__c'] = component.get("v.recordId");
            } else if(sObj.includes("Lease__c")) {
                eventFields[namespace+'Related_To_Lease__c'] = component.get("v.recordId");
            } else if(sObj.includes("Counterparty__c")) {
                eventFields[namespace+'Counterparty__c'] = component.get("v.recordId");
            } else if(sObj.includes("Deal_Proposal__c")) {
                eventFields[namespace+'Related_To_Deal_Proposal__c'] = component.get("v.recordId");
            } else {
                console.log('Object dosent match any type. Saving will throw error!! '+sObj);
            }
        }
        else {
            console.log('Object is empty. Saving will throw error!! '+sObj);
        }
        
        console.log("Subject entered "+eventFields.Name);
        var name = eventFields.Name;
        if($A.util.isEmpty(name) || $A.util.isUndefined(name)) {
        	console.log("subject is empty ");
            component.set("v.isErrorMsg", true);
            component.set("v.isStandardLog", false);
        }
        else {
			console.log("onSubmit final obj  "+JSON.stringify(eventFields));
        	component.find('parentRecord').submit(eventFields);
        }
    },
    
    changeToStandardLog : function(component,event,helper) {
        //component.find("parentRecord").submit();
        console.log("changeToStandardLog ");
        component.set("v.isStandardLog", true);
    },
    
    keyCheck : function(component,event,helper) {
        console.log("keyCheck ");
        component.set("v.isErrorMsg", false);
    },
    
    onRecordSuccess : function(component, event, helper){
        console.log("onRecordSuccess");
        var payload = event.getParams(); 
        console.log("onRecordSuccess id "+payload.response.id);
        helper.fireSuccessToast(component,payload.response.id );    
        
        var isStandardLog = component.get("v.isStandardLog");
        console.log("onRecordSuccess isStandardLog" +isStandardLog );
        if(isStandardLog == true) {
            var navEvt = $A.get("e.force:editRecord");
            navEvt.setParams({
                "recordId": payload.response.id
            });
            navEvt.fire();
        }
        component.set("v.isStandardLog", false);
    },
    
    onRecordFailure : function(component,event,helper) {
        console.log("onRecordFailure");
        var errors = event.getParams(); 
        console.log("Error Response", JSON.stringify(errors)); 
        
        helper.fireFailureToast(component,errors.detail);
      
    },
   
    
})