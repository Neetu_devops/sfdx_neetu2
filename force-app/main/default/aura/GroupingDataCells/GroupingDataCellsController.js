({
	doInit: function (component, event, helper) {
		var factMap = component.get("v.factMap");
		if (factMap) {
			var groupingKey = component.get("v.groupingKey");
			//commented on Oct 5, 2020 as variable was removed earlier from UI.
			/*var groupedrows = factMap[groupingKey + "!T"].rows;
			var maxColSize = 0;
			for (let key in groupedrows) {
				if (groupedrows[key].dataCells != undefined && groupedrows[key].dataCells != '') {
					console.log('groupedrows[key].dataCells---->', groupedrows[key].dataCells);
					maxColSize = groupedrows[key].dataCells.length;
					console.log('maxColSize---->', maxColSize);
				}
			}
			component.set("v.maxColSize", maxColSize);*/
			component.set("v.dataRows", factMap[groupingKey + "!T"].rows)
		}
	},
	editRecord: function (component, event, helper) {
		var recordId = event.currentTarget.dataset.recordid;
		var editRecordEvent = $A.get("e.force:editRecord");
		editRecordEvent.setParams({
			"recordId": recordId
		});
		editRecordEvent.fire();
	}
})