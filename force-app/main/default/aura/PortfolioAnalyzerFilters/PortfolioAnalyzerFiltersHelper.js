({
    getDealTypePicklist : function(component) {
        console.log('getDealTypePicklist');
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Marketing_Activity__c',
            "fld": 'Deal_Type__c'
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('PickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null && allValues.length > 0) {
                    console.log('PickList result: '+ JSON.stringify(response.getReturnValue()));
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                    component.set("v.isLoading", false);
                    component.find('DealType').set("v.options", opts);
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    }
})