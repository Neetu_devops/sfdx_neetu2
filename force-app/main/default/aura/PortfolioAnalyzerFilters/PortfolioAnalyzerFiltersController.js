({
    doInit: function(component,event,helper){
        component.set("v.isLoading", true);
        helper.getDealTypePicklist(component);
        var action = component.get("c.GetActiveDeals");
       var pp= component.get("v.SelectedKPI");
       console.log('doinit----kpisel----'+pp);
action.setCallback(this, function(response) {
    var state = response.getState();
    if (component.isValid() && state == 'SUCCESS') {  
        var resultArray = response.getReturnValue();
        console.log('result array is---'+JSON.stringify(resultArray));
        var options = [];
        resultArray.forEach(function(result)  { 
            console.log('result is------'+JSON.stringify(result));
            options.push({ value: result.Id, label: result.Name});
            console.log('optionsss'+JSON.stringify(options));
        });
        component.set("v.ActiveDeals", options);
    }else{
        console.log('Failed with state: ' + state);
    }
});
$A.enqueueAction(action);
},
handleRadioClick: function (component, event) {
      var val = document.querySelector('input[name="Filters"]:checked').value;
      console.log('value is----'+val);
      if(val=='DealType'){
          component.set("v.DealTypeFilter",true);
      }
      else{
        component.set("v.DealTypeFilter",false);
      }
    },
    saveFilters: function(component,event,helper){
        console.log('saving the filters');
        //document.getElementById("DealType").disabled= true;
        var dealTypes=component.get("v.value");
        var ActiveDeal = component.get("v.selectedArray");
        var kpis= component.get("v.SelectedKPI");

        console.log("kpi sel-----"+kpis);
        if(ActiveDeal.length>0){
            if(kpis==="Operator"){
            console.log("Cannot have both deal type and active deal groups");
            var cmpEvent = component.getEvent("cmpEvent");
            var DealSelected=JSON.stringify(ActiveDeal);
            var action = component.get('c.GetSelectedDealByOperator');
            action.setParams({
                "DealsSelected": DealSelected
            });
            action.setCallback(this, function(response) {
                var Deals = response.getReturnValue();
                cmpEvent.setParams({"ActiveSelectedDeals": Deals });
                console.log('deal types sent to soql '+Deals);
                cmpEvent.fire();    
                component.find("overlayLib").notifyClose();
        });
    }
    if(kpis=="Country"){
        console.log('in country ---specific deal');
        //enter code here to handle the part that displays the projected chart based on specific deal and for country kpi
        var cmpEvent = component.getEvent("cmpEvent");
            var DealSelected=JSON.stringify(ActiveDeal);
            var action = component.get('c.GetSelectedDealByCountry');
            action.setParams({
                "DealsSelected": DealSelected
            });
            action.setCallback(this, function(response) {
                var Deals = response.getReturnValue();
                cmpEvent.setParams({"ActiveSelectedDeals": Deals });
                console.log('deal types sent to soql '+Deals);
                cmpEvent.fire();   
                component.find("overlayLib").notifyClose(); 
        });
    }
        }
         else {
             if(kpis=="Country"){
            var cmpEvent = component.getEvent("cmpEvent");
            console.log("event----"+cmpEvent);
            var dealT=JSON.stringify(dealTypes);
            console.log('the following dealtypes selected:'+dealTypes);
            var action = component.get('c.GetDealsByCountryTrial2');
            action.setParams({
                "DealTypes": dealT
            });
            action.setCallback(this, function(response) {
                var Group = response.getReturnValue();
                cmpEvent.setParams({"dealType": Group });
                console.log('deal types sent to soql '+Group);
                cmpEvent.fire();
                component.find("overlayLib").notifyClose();      
        });
    }
   if(kpis=="Operator"){
    console.log('Code to handle deal type for projected chart operator kpi');
    var cmpEvent = component.getEvent("cmpEvent");
            console.log("event----"+cmpEvent);
            var dealT=JSON.stringify(dealTypes);
            console.log('the following dealtypes selected:'+dealTypes);
            var action = component.get('c.OperatorByDealType');
            action.setParams({
                "DealTypes": dealT
            });
            action.setCallback(this, function(response) {
                var Group = response.getReturnValue();
                cmpEvent.setParams({"dealType": Group });
                console.log('deal types sent to soql '+Group);
                cmpEvent.fire();
                component.find("overlayLib").notifyClose();     
        });
    }
    //component.find("overlayLib").notifyClose();
         }
    //if(ActiveDeal.length==0 && )
    
    $A.enqueueAction(action);
    
   // beginning of code to handle close of custom modal
   console.log("after neque");
 
    
},

sampleEvent : function(cmp, event){
    console.log("collapse code");
    cmp.find("overlayLib").notifyClose();
},

handleActiveDealsChange: function (cmp, event) {
    // This will contain an array of the "value" attribute of the selected options
    var selectedOptionValue = event.getParam("value");
    //var selectedId= event.getParam("Id");
    console.log("Option selected with value: '" + selectedOptionValue.toString() + "'");
    cmp.set("v.selectedArray", selectedOptionValue);

}
    
   /* var action = component.get('c.getDealGroups');
    action.setParams({
        "DealId": component.get("v.recordId")
    });
    console.log("getDealGroups recordId "+component.get("v.recordId"));
    action.setCallback(this, function(response) {
        var Group = response.getReturnValue();
        console.log("getDealGroups  " + Group);
        component.set("v.dealsList", Group);
    });*/
    
})