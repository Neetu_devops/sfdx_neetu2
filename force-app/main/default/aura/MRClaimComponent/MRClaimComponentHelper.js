({
    searchMRInfoRecords: function (component, event, getInputkeyWord) {
        component.set("v.showListSpinner", true);

        var action = component.get("c.getAssemblyMRInfos");
        action.setParams({
            'recordId': component.get("v.recordId"),
            'searchKeyword': getInputkeyWord
        });
        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('result: ', result);
                var listOfSearchRecords = [];
                var listOfMrShareRecords = [];

                for (var key in result) {
                    if (result[key].mrShare != undefined) {
                        listOfMrShareRecords.push(result[key].mrShare);
                    }

                    if (result[key].mrInfo != undefined) {
                        if (result[key].mrInfo.Lessor_Contribution_Available__c == undefined) {
                            result[key].mrInfo.Lessor_Contribution_Available__c = 0;
                        }
                        listOfSearchRecords.push(result[key].mrInfo);
                    }
                }
                component.set("v.listOfSearchRecords", listOfSearchRecords);
                component.set("v.listOfMrShareRecords", listOfMrShareRecords);

                let searchMRShare = component.get("v.searchMRShare");
                let mrInfoList = component.get("v.listOfSearchRecords");

                if (searchMRShare && mrInfoList.length > 0) {
                    let mrInfoList = component.get("v.listOfSearchRecords");
                    let selectedRecord = 0;

                    if (mrInfoList[selectedRecord].Assembly_Event_Info__c != undefined
                        && mrInfoList[selectedRecord].Assembly_Event_Info__r.Event_Cost_F__c != undefined) {
                        component.set("v.estCost", mrInfoList[selectedRecord].Assembly_Event_Info__r.Event_Cost_F__c);
                    }
                    else {
                        component.set("v.estCost", 0);
                    }

                    if (mrInfoList[selectedRecord] != undefined && mrInfoList[selectedRecord].Lessor_Contribution_Available__c == undefined) {
                        mrInfoList[selectedRecord].Lessor_Contribution_Available__c = 0;
                    }
                    component.set("v.selectedRecord", mrInfoList[selectedRecord]);
                    component.set("v.searchMRShare", false);
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
            component.set("v.showListSpinner", false);
        });
        $A.enqueueAction(action);
    },

    checkInvoiceAmount: function (component) {
        if (component.get("v.claimRecord.Override_Total_Amount__c")) {
            this.updateApprovedAmount(component);
        }
        else {
            this.updateInvoiceAmount(component);
        }
    },

    updateApprovedAmount: function (component) {       
        var claimRemaining = parseFloat(component.get("v.claimRecord.Approved_Claim_Amount__c")) - component.get("v.approvedPayoutsAmount"); 
        var invoiceAmount = parseFloat(component.get("v.invoiceAmount"));
        var totalRemaining = parseFloat(component.get("v.availableAmount")) - parseFloat(component.get("v.approvedPayoutsAmount")) + parseFloat(component.get("v.approvedLessorContributionAmt")) + parseFloat(component.get("v.selectedRecord.Lessor_Contribution_Available__c"));

        invoiceAmount = parseFloat(invoiceAmount.toFixed(2));
        totalRemaining = parseFloat(totalRemaining.toFixed(2));

        if (totalRemaining > invoiceAmount || invoiceAmount > claimRemaining || isNaN(invoiceAmount)) {
            this.addInvoiceColor(component);
        } else {
            this.removeInvoiceColor(component);
        }
    },

    setInvoiceAmount: function (component) {
        var mrAmount = parseFloat(component.get("v.mrAmount"));
        var contrAmount = parseFloat(component.get("v.contributionAmount"));
        if(isNaN(mrAmount) && isNaN(contrAmount)){
            component.set("v.invoiceAmount", mrAmount + contrAmount);    
        }
        else{
            if(isNaN(mrAmount)){
                component.set("v.invoiceAmount", contrAmount);
            }
            else if(isNaN(contrAmount)){
                component.set("v.invoiceAmount",mrAmount)
            }
            else{
                component.set("v.invoiceAmount", mrAmount + contrAmount); 
            }            
        }        
    },

    updateInvoiceAmount: function (component) {
        
        var mrInfo = component.get("v.selectedRecord");
        var invoiceAmount = parseFloat(component.get("v.invoiceAmount"));
        var totalAvailable = parseFloat(component.get("v.availableAmount")) + mrInfo.Lessor_Contribution_Available__c - parseFloat(component.get("v.approvedPayoutsAmount")) + parseFloat(component.get("v.approvedLessorContributionAmt"));
        var claimRemaining = parseFloat(component.get("v.claimRecord.Approved_Claim_Amount__c")) - parseFloat(component.get("v.approvedPayoutsAmount"));

        invoiceAmount = parseFloat(invoiceAmount.toFixed(2));
        claimRemaining = parseFloat(claimRemaining.toFixed(2));
        totalAvailable = parseFloat(totalAvailable.toFixed(2));      
        
        if (invoiceAmount > claimRemaining || invoiceAmount > totalAvailable || isNaN(invoiceAmount)) {        
            console.log('addInvoiceColor');
            this.addInvoiceColor(component);
        }
        else {
            console.log('removeInvoiceColor');
            this.removeInvoiceColor(component);
        }
    },

    removeInvoiceColor: function (component) {
        if (component.get("v.claimDate") != undefined
            && component.get("v.claimRecord.Claim_Status__c") != 'Claim Under Dispute') {
            component.set("v.generateInvoice", false);
        }

        $A.util.removeClass(component.find('invoice'), 'invalidAmount');
        $A.util.removeClass(component.find('mrAmount'), 'invalidAmount');
    },

    addInvoiceColor: function (component) {
        component.set("v.generateInvoice", true);
        $A.util.addClass(component.find('invoice'), 'invalidAmount');
        $A.util.addClass(component.find('mrAmount'), 'invalidAmount');
    },

    handleInsufficientMRBalance: function (component) {
        let action = component.get("c.calculateTotalInvoiceAmount");
        action.setParams({
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state === 'SUCCESS') {
                let result = JSON.parse(response.getReturnValue());
                component.set("v.totalInvoiceAmount", result.totalInvoiceAmount);
                component.set("v.remainingMRBalance", result.remainingMRBalance);
                component.set("v.latestEventInvoiceLink", result.link);
                component.set("v.latestEventInvoiceLabel", result.label);
            } else {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
        console.log("handleInsufficientMRBalance MRClaimComponent End");
    },

    dateToParseHelper: function (component, event, helper, dateToParse) {
        var dateToFormat = new Date(dateToParse);
        var formattedDate = dateToFormat.getUTCFullYear() + '-' + (dateToFormat.getUTCMonth() + 1) + '-' + dateToFormat.getUTCDate(); // yyyy-mm-dd
        //console.log('formattedDate: ', formattedDate);
        return formattedDate;
    },

    checkUndefined: function (input) {
        if (input !== undefined && input !== '' && input !== null) {
            return false;
        }
        return true;
    },

    //Show the error toast when any error occured
    handleErrors: function (errors, type) {
        _jsLibrary.handleErrors(errors, type); // Calling the JS Library function to handle the error
    }
})
