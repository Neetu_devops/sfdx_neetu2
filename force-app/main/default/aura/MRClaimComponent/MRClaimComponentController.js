({
    doInit: function (component, event, helper) {
        component.set("v.showSpinner", true);
        var action = component.get("c.getAssemblyMRInfo");
        action.setParams({
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('result: ', result);
                component.set("v.mrClaimLoadData", result);
                component.set("v.isEventSelected", result.isEventSelected);
                component.set("v.filterCriteria", result.filterCriteria);
                component.set("v.claimRecord", result.claim);
                component.set("v.eventRecord", result.event);
                
                if (!helper.checkUndefined(result.mrInfo) && helper.checkUndefined(result.mrInfo.Lessor_Contribution_Available__c)) {
                    result.mrInfo.Lessor_Contribution_Available__c = 0;
                }
                                
                if (!helper.checkUndefined(result.mrInfo)) {
                    component.set("v.selectedRecord", result.mrInfo);
                    component.set("v.searchMRShare", false);                        
                }
                else {
                    component.set("v.selectedRecord", undefined);
                    component.set("v.searchMRShare", true);
                    helper.searchMRInfoRecords(component, event, '');
                }
            
                if (result.estEventCost != undefined) {
                    component.set("v.estCost", result.estEventCost);
                }
                else {
                    component.set("v.estCost", 0);
                }
                component.set("v.invoiceListSize", result.invoiceListSize);
                component.set("v.approvedPayoutsAmount", result.approvedPayoutsAmount);
                component.set("v.approvedLessorContributionAmt", result.approvedLessorContributionAmount);
                component.set("v.approvedPayouts", result.approvedPayouts);
                component.set("v.mrAmount", 0);
                component.set("v.contributionAmount", 0);
                component.set("v.invoiceAmount", 0);

                /* Making read only as per the request. LW-AKG 11-08-2020
                let searchMRShare = component.get("v.searchMRShare");
                if (!searchMRShare) {
                    if (!helper.checkUndefined(component.get("v.selectedRecord"))) {
                        helper.handleLookupStyle(component, true);
                    }
                    else {
                        helper.handleLookupStyle(component, false);
                    }
                }
                */
            }
            else {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    fetchDataAndGenerateInvoice: function (component, event, helper) {
        component.set("v.showSpinner", true);
        var rowIndex = component.get("v.clickedRowId");

        component.find('mrClaimTable').refreshMRClaimTable();
        setTimeout($A.getCallback(function () {
            component.find('mrClaimTable').handleRowSelection(rowIndex);

            helper.handleInsufficientMRBalance(component);

            setTimeout($A.getCallback(function () {
                component.fetchAndGenerateInvoice();
            }), 3000);
        }), 3000);
    },


    generateInvoice: function (component, event, helper) {
        var selectedShareRecord = component.get("v.selectedMRShare");
        let remainingMRBalance;
        if (selectedShareRecord != undefined && selectedShareRecord != '') {
            remainingMRBalance = parseFloat(component.get("v.remainingMRBalance"));
        }
        else {
            if (component.get("v.availableAmount") != undefined && component.get("v.totalInvoiceAmount") != undefined) {
                remainingMRBalance = parseFloat(component.get("v.selectedRecord.Lessor_Contribution_Available__c")) + 
                parseFloat(component.get("v.availableAmount")) - parseFloat(component.get("v.totalInvoiceAmount")) + 
                parseFloat(component.get("v.approvedLessorContributionAmt"));
            }
        }

        let claimRecord = component.get("v.claimRecord");
        var invoiceAmount = component.get("v.invoiceAmount");

        if (invoiceAmount > remainingMRBalance && invoiceAmount != 0 && !claimRecord.Override_Total_Amount__c) {
            component.set("v.showSpinner", false);

            var toastEvent = $A.get("e.force:showToast");
            //Set Toast Parameters
            toastEvent.setParams({
                title: 'Error',
                message: 'Claim Payable Invoice is in the Approved status. Payment of this invoice will lead to insufficient MR Funds. Please pay/delete all previous Claim Payable Invoices before generating this Claim Payable Invoice',
                messageTemplate: '{1} is in the Approved status. Payment of this invoice will lead to insufficient MR Funds. Please pay/delete all previous Claim Payable Invoices before generating this Claim Payable Invoice',
                messageTemplateData: ['Salesforce', {
                    url: component.get("v.latestEventInvoiceLink"),
                    label: component.get("v.latestEventInvoiceLabel"),
                }],
                duration: ' 5000',
                type: 'Error'   // success/Warning/Error
            });
            //Fire Toast Event
            toastEvent.fire();
        } else {
            //component.set("v.showSpinner", true);            
            if (invoiceAmount <= 0) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: "The Payable Invoice amount should be greater than zero",
                    type: "error",
                });
                toastEvent.fire();

                component.set("v.showSpinner", false);
            }
            else {
                var newFormattedDate = '';
                var dateToParse = component.get("v.claimDate");

                if (dateToParse != undefined && dateToParse != '' && dateToParse != null) {
                    newFormattedDate = helper.dateToParseHelper(component, event, helper, dateToParse);
                }
                var action = component.get("c.createEventInvoice");
                action.setParams({
                    'claim': component.get("v.claimRecord"),
                    'mrInfo': component.get("v.selectedRecord"),
                    'invoiceAmount': invoiceAmount,
                    'claimDate': newFormattedDate,
                    'claimRefType': component.get("v.claimRefType"),
                    'remainingAmount': component.get("v.availableAmount") + component.get("v.selectedRecord.Lessor_Contribution_Available__c") - component.get("v.approvedPayoutsAmount") + component.get("v.approvedLessorContributionAmt"),
                    'contributionUtilized': component.get("v.contributionAmount")
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();

                    if (state === 'SUCCESS') {                                              
                        component.refreshClaim();
                        
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            message: "Claim Payable Invoice successfully generated",
                            type: "success",
                        });
                        toastEvent.fire();
                    }
                    else {
                        var errors = response.getError();
                        if (errors) {
                            helper.handleErrors(errors);
                        }
                    }

                    component.set("v.showSpinner", false);
                });
                $A.enqueueAction(action);
            }
        }
    },


    checkApprovedAmount: function (component, event, helper) {
        helper.checkInvoiceAmount(component);
    },


    copyMRRemainingToAmount: function (component, event, helper) {       
        var mrRemaining = parseFloat(component.get("v.availableAmount")) - parseFloat(component.get("v.approvedPayoutsAmount")) + parseFloat(component.get("v.approvedLessorContributionAmt"));       
        var claimRemaining = parseFloat(component.get("v.claimRecord.Approved_Claim_Amount__c")) - parseFloat(component.get("v.approvedPayoutsAmount"));

        mrRemaining = parseFloat(mrRemaining.toFixed(2));
        claimRemaining = parseFloat(claimRemaining.toFixed(2));
        
        if (claimRemaining <= mrRemaining) {
            component.set("v.mrAmount", claimRemaining);
        }
        else {
            component.set("v.mrAmount", mrRemaining);
        }

        helper.setInvoiceAmount(component);
        helper.checkInvoiceAmount(component);
    },


    copyContributionRemainingToAmount: function (component, event, helper) {
        var mrInfo = component.get("v.selectedRecord");

        component.set("v.contributionAmount", mrInfo.Lessor_Contribution_Available__c);
        helper.setInvoiceAmount(component);
        helper.checkInvoiceAmount(component);
    },


    updateContributionAmount: function (component, event, helper) {
        var mrInfo = component.get("v.selectedRecord");

        if (component.get("v.contributionAmount") > mrInfo.Lessor_Contribution_Available__c) {
            component.set("v.contributionAmount", mrInfo.Lessor_Contribution_Available__c);
        }

        helper.setInvoiceAmount(component);
        helper.checkInvoiceAmount(component);
    },


    updateMRAmount: function (component, event, helper) {
        helper.setInvoiceAmount(component);
        helper.checkInvoiceAmount(component);
    },


    openMrInfo: function (component, event, helper) {
        var mrInfo = event.getSource().get("v.name");

        if (mrInfo != undefined) {
            var url = "/" + mrInfo;

            window.open(url);
        }
    },


    selectMRShare: function (component, event, helper) {
        var selectedRecord = component.get("v.selectedRecord");
        var listOfMrShareRecords = component.get("v.listOfMrShareRecords");
        var selectedShareRecord;

        if (selectedRecord != undefined) {
            for (var key in listOfMrShareRecords) {
                if (listOfMrShareRecords[key].Assembly_MR_Info__c == selectedRecord.Id) {
                    selectedShareRecord = listOfMrShareRecords[key];
                    break;
                }
            }
        }
        component.set("v.selectedMRShare", selectedShareRecord);
    },


    updateAvailableAmount: function (component, event, helper) {
        var selectedShareRecord = component.get("v.selectedMRShare");
        var availableAmount = component.get("v.availableAmount");

        if (selectedShareRecord != undefined) {
            if (selectedShareRecord.Max_Cap_in_Cash__c != null
                && selectedShareRecord.Max_Cap_in_Cash__c != 0
                && selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != null
                && selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != 0) {
                if (selectedShareRecord.Max_Cap_in_Cash__c < selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c) {
                    availableAmount = selectedShareRecord.Max_Cap_in_Cash__c;
                }
                else {
                    availableAmount = selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c;
                }
            }
            else {
                if (selectedShareRecord.Max_Cap_in_Cash__c != null
                    && selectedShareRecord.Max_Cap_in_Cash__c != 0) {
                    availableAmount = selectedShareRecord.Max_Cap_in_Cash__c;
                }
                else if (selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != null
                    && selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != 0) {
                    availableAmount = selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c;
                }
            }
        }
        component.set("v.availableAmount", availableAmount);

        setTimeout(function () {
            helper.checkInvoiceAmount(component);
        }, 200);
    },


    refreshTable: function (component, event, helper) {
        component.find('mrClaimTable').refreshMRClaimTable();
        component.refreshClaim();
        component.set("v.availableAmount", 0);
        component.set("v.claimDate", undefined);
        component.set("v.invoiceAmount", 0);
        $A.util.removeClass(component.find('invoice'), 'invalidAmount');
    },


    updateHistoricalEventOnClaim: function (component, event, helper) {
        component.set("v.showSpinner", true);
        let eventRecord = component.get("v.eventRecord");
        let claimRecord = component.get("v.claimRecord");

        if (!helper.checkUndefined(eventRecord) && !helper.checkUndefined(eventRecord.Id) && !helper.checkUndefined(claimRecord) && helper.checkUndefined(claimRecord.Event__c)) {
            var action = component.get("c.updateHEventOnClaim");
            action.setParams({
                'recordId': component.get("v.recordId"),
                'event': component.get("v.eventRecord")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    component.set("v.isEventSelected", true);
                    $A.get('e.force:refreshView').fire();
                }
                else {
                    var errors = response.getError();
                    if (errors) {
                        helper.handleErrors(errors);
                    }
                }
                component.set("v.showSpinner", false);
            });
            $A.enqueueAction(action);
        }
        else {
            component.set("v.showSpinner", false);
            helper.handleErrors("Please select the Assembly Event for generating the Claim Invoice.");
        }
    }
})
