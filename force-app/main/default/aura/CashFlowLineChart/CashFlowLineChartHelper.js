({
	//CashFlow Chart ServerCall (First Chart)
    dataOFCashFlowChart : function(component,event,helper) {
        this.showSpinner(component,event,helper);
        var recordId =component.get("v.recordId");
        var action2 = component.get("c.getCashFlowDataforLine1");
        action2.setParams({
            "recordId": recordId
        });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                console.log('State--->>'+state);
                var dataObj= JSON.parse(response.getReturnValue()); 
                
                component.set("v.cashFlowData",dataObj);
                var monthValue = [];
                for(var i = 0 ;i<dataObj.length;i++){
                    monthValue.push(dataObj[i].month);
                }
                console.log('monthValue',monthValue);
                component.set("v.xAxisCategories",monthValue);
                this.cashFlowChart(component,event,helper);
            }
            this.hideSpinner(component,event,helper);
        });
        $A.enqueueAction(action2);
    },
    //CashFlow Chart Data as per y-axis and x-axis populated on UI   (First Chart)
    cashFlowChart : function(component,event,helper) {
        var dataObj  = component.get("v.cashFlowData");
        var lstYAxisData = [];
        for(let data of dataObj ){
            lstYAxisData.push( data.y );	    
        }
        lstYAxisData.sort();
        
        new Highcharts.Chart({
            chart: {
                zoomType: 'xy',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: component.find("linechart").getElement(),
                type: 'line'
            },
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 700
                    },
                    chartOptions: {
                        legend: {
                            enabled: false
                        }
                    }
                }]
            },
            title: {
                text: 'Cash Flow Chart'
            },
            xAxis: {
                categories: component.get("v.xAxisCategories"),
            },
            yAxis: {
                gridLineColor: '#197F07',
                gridLineWidth: 0,
                lineWidth:1,
                plotLines: [{
                    color: '#dcdbdb',
                    width: 1,
                    value: 0
                }],
                offset: 10,
                labels: {
                    formatter: function () {
                        return (this.value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) + 'K';
                    }
                },
                title: {
                    text: 'Cash Flow',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ 'Cash Flow'+
                        (this.x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) +': $'+ (this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) +'K';
                }
            },
            plotOptions: {
                "line": {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                },
                //To show table on point click
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: $A.getCallback(function (evt) {
                                console.log('this: ', this);
                                component.set("v.month",this.month);
                                var recordId =component.get("v.recordId");
                                var action2 = component.get("c.getCashFlowDataTable");
                                action2.setParams({
                                    "recordId": recordId,
                                    "startDate": evt.point.month
                                });
                                action2.setCallback(this, function(response) {
                                    var state = response.getState();
                                    if (state === "SUCCESS") { 
                                        var dataObj= response.getReturnValue();  
                                        if(dataObj !== undefined){
                                            var element = document.getElementById("modal");
                                            element.classList.remove("slds-hide");
                                            element.classList.add("slds-show");
                                            component.set("v.cashFlowTableData",dataObj); 
                                        }
                                    }
                                });
                                $A.enqueueAction(action2); 
                            })
                        }
                    }
                } 
            },
            series: [{
                name:'Time Duration',
                data:dataObj
            }]
        },function(chart){
            setTimeout(function(){
                chart.series[0].data.forEach(function(point) {
                });
            },1); 
        });  
    },
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
})