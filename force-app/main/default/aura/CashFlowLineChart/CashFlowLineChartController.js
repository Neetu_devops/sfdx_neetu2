({
    doInit : function(component, event, helper){
        helper.dataOFCashFlowChart(component,event,helper);
    },
    closeModal : function(component, event, helper){
        var element = document.getElementById("modal");
        element.classList.remove("slds-show");
        element.classList.add("slds-hide");
    },
})