({
    loadInitData: function (cmp, event, helper) {
        var navService = cmp.find("navService");
        var pageReference = {
            type: "standard__objectPage",
            attributes: {
                objectApiName: cmp.get("v.sObjectName"),
                actionName: "new"
            },
            state: {
                nooverride: "1",
                navigationLocation: (cmp.get("v.parentRecordId") == "" ? "MRU_LIST" : "RELATED_LIST"),
                backgroundContext: this.setbackgroundContext(cmp, event, helper),
                recordTypeId: cmp.get("v.recordTypeId")
            }
        };

        var action = cmp.get("c.fetchInitData");
        action.setParams({
            sObjectName: cmp.get("v.sObjectName"),
            recordTypeId: cmp.get("v.recordTypeId"),
            parentRecordId: cmp.get("v.parentRecordId")
        });
        action.setCallback(this, function (response) {
            const state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                //console.log('result: ', result);

                if (result != undefined && result != '') {
                    pageReference.state.defaultFieldValues = cmp.find("pageRefUtils").encodeDefaultFieldValues(JSON.parse(result));
                }
                cmp.set("v.pageReference", pageReference);
                pageReference.attributes.actionName = "new";
                navService.navigate(pageReference);
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                console.log('errors: ', errors);
            }
        });
        $A.enqueueAction(action);
    },
    setbackgroundContext: function (cmp, event, helper) {
        let backgroundContext = cmp.get("v.backgroundContext");
        let attributes = backgroundContext.attributes;

        if (backgroundContext.type == "standard__recordRelationshipPage") {
            return "/lightning/r/" + attributes.recordId + "/related/" + attributes.relationshipApiName + "/" + attributes.actionName;
        }
        else if (backgroundContext.type == "standard__objectPage") {
            return "/lightning/o/" + attributes.objectApiName + "/" + attributes.actionName + "?filterName=" + backgroundContext.state.filterName;
        }
        else if (backgroundContext.type == "standard__recordPage") {
            return "/lightning/r/" + attributes.objectApiName + "/" + attributes.recordId + "/" + attributes.actionName;
        }
    }
})