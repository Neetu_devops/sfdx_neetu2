({
    init: function (cmp, event, helper) {
        let pageRef = cmp.get("v.pageReference");

        let recordTypeId = pageRef.state.recordTypeId;
        cmp.set("v.recordTypeId", recordTypeId);

        //Code to fetch parentId when called from related list.
        let inContextOfRef = pageRef.state.inContextOfRef;
        if (inContextOfRef != undefined && inContextOfRef != '') {
            if (inContextOfRef.startsWith("1\.")) {
                inContextOfRef = inContextOfRef.substring(2);
            }
            let backgroundContext = JSON.parse(window.atob(inContextOfRef));
            cmp.set("v.backgroundContext", backgroundContext);

            let recordId = backgroundContext.attributes.recordId;
            if (recordId != undefined && recordId != '' && recordId != null) {
                cmp.set("v.parentRecordId", recordId);
            }
        }

        helper.loadInitData(cmp, event, helper);
    },
    navigateToURL: function (cmp, event, helper) {
        var location = window.location.href;
        var sObjectNewLink = '/lightning/o/' + cmp.get("v.sObjectName") + '/new';
        var retURL = helper.setbackgroundContext(cmp, event, helper);

        if (location.includes(retURL)) {
            cmp.set("v.isClosed", true);
        }
        if (cmp.get("v.isClosed") && location.includes(sObjectNewLink)) {
            window.setTimeout($A.getCallback(function () {
                $A.get('e.force:refreshView').fire();
                //cmp.reloadInitiated(); // LW-AKG: 07/21/2020 - Commenting this as this causes flickering on UI.
            }), 500);
        }
    }
})