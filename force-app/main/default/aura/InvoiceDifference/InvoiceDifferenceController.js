({
    loadData: function(component, event, helper) {

        var recordIdVar = component.get("v.recordId");
        var sobjName = component.get("v.sobjecttype"); 
       
        //call apex class method
        var action = component.get('c.initMethod');
        action.setParams({
            'recId' : recordIdVar,
            'objectName' : sobjName,
        }); 
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                //set response value in wrapperList attribute on component.
                component.set('v.wrapperList', response.getReturnValue());
            }else if(state === 'ERROR') {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    
	handleClick: function (component, event, helper) {
        var myId = event.currentTarget.name;
        window.open('/' + myId); 
    }    
})