({
	initialize : function(component, event, helper) {
        console.log('initialize+ -->');
        var recId=component.get("v.pricingRunInputId");
		console.log('-->'+recId);
        
        var finalURL = window.location.protocol +"//"+window.location.hostname+"/";
        component.set('v.PreURL', finalURL);
        
		helper.getPricingDetail(component);
        helper.getTermList(component);
        console.log('initialize- -->');
	},
    onCheck : function(component, event, helper) {
        console.log('onCheck+ -->');
        	var idVar = event.getSource().get("v.name");
        	var idVarBool = event.getSource().get("v.value");
			console.log('vva+ -->' + idVar);
        	console.log('vva1+ -->' +idVarBool);
        	var selectedIdsVar = component.get("v.selectedIds");
        	selectedIdsVar[idVar] = idVarBool;
                for (var key in selectedIdsVar){
                     console.log('key- -->' +key);
                    console.log('key value- -->' +selectedIdsVar[key]);
                }        
        	component.set('v.selectedIds', selectedIdsVar);
        console.log('onCheck- -->');
	},
    callURL : function(component, event, helper) {
        console.log('callURL+ -->');
           console.log('Hey There .. the anchor was clicked');
           console.log(event);
           var href = event.srcElement.href;
        	var hrefId = event.target ;
           console.log(href);
        console.log(hrefId);
       
        console.log('callURL- -->');
	}, 
    
    
      // save term List
    callSave : function(component, event, helper) {
      	console.log('saveTermList+ -->');
        
		var selectedIdsVar = component.get("v.selectedIds");
        
                var action = component.get("c.callSaveApex");
                action.setParams({
                    'recId' : component.get('v.pricingRunInputId'),
                    'selectedIds_p' : selectedIdsVar,
                });         
          
                action.setCallback(this,function(a){
                    if(a.error && a.error.length){
                        helper.throwErrorForKicks(component, a.error[0].message);
                    }   
                 
                        console.log("success");
                    	var recId = component.get('v.pricingRunInputId');
						helper.helperCloseMethod(recId);

                });
    			$A.enqueueAction(action);          
      	console.log('saveTermList- -->');
  },     
    
	callCancel : function(component, event, helper) {
        console.log("callCancel======================>");
        var recId = component.get('v.pricingRunInputId');
		helper.helperCloseMethod(recId);       
        
	},    
})