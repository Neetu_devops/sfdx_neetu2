({
    
	helperCloseMethod: function(canvasIdVar) {

                        if( (typeof sforce != 'undefined') && (sforce != null) ) {
                            // do your SF1 mobile button stuff
                            sforce.one.back(true);
                        }
                        else {
                            // do your desktop stuff
                            //window.close();
                            if(canvasIdVar!=null){
                            	var finalURL = window.location.protocol +"//"+window.location.hostname+"/apex/RedirectVF?paramId="+ canvasIdVar ;
                            	window.open(finalURL ,"_self");
                            }
                            else{
                                window.history.back();
                            }
                        }         
        
	},
    throwErrorForKicks: function(cmp,message) {
        // this sample always throws an error to demo try/catch

            $A.createComponents([
                ["ui:message",{
                    "title" : "Review all error messages below to correct your data.",
                    "severity" : "error",
                }],
                ["ui:outputText",{
                    "value" : message + "\n",
                    "class" : "div1Center",
                }]
                ],
                function(components, status, errorMessage){
                    if (status === "SUCCESS") {
                        var message = components[0];
                        var outputText = components[1];
                        // set the body of the ui:message to be the ui:outputText
                        message.set("v.body", outputText);
                        var div1 = cmp.find("errorDiv1");
                        // Replace div body with the dynamic component
                        div1.set("v.body", message);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
                }
            );// create comp

    },        
    
    
   
    
  // Fetch Pricing details from the Apex controller
  	getPricingDetail: function(component) {
      	console.log('getTermList+ -->');
		var action = component.get('c.getPricingRunDetail');
		action.setParams({
            'recId' : component.get('v.pricingRunInputId'),
        }); 
    	// Set up the callback
    	var self = this;
    	action.setCallback(this, function(actionResult) {
     		component.set('v.pricingRec', actionResult.getReturnValue());
    	});
    	$A.enqueueAction(action);
      	console.log('getTermList- -->');
  }, 
    
  // Fetch the accounts from the Apex controller
  	getTermList: function(component) {
      	console.log('getTermList+ -->');
		var action = component.get('c.getTerms');
		action.setParams({
            'recId' : component.get('v.pricingRunInputId'),
        }); 
    	// Set up the callback
    	var self = this;
    	action.setCallback(this, function(actionResult) {
     		component.set('v.termArray', actionResult.getReturnValue());
    	});
    	$A.enqueueAction(action);
      	console.log('getTermList- -->');
  },    
    

})