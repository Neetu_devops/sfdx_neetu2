({
 	updateFleetSummary: function (component, event, helper) {
       var answer = event.currentTarget.dataset.answer;
       var recordId =  component.get("v.recordId");
       
        if(answer == 'Yes'){
      
      helper.showSpinner(component, event, helper);
         console.log('Inside Init');
   		 
      	 var updateFleetSummaryAllAction = component.get("c.updateFleetSummaryAll");
      	 updateFleetSummaryAllAction.setCallback(this, function (response) {
                helper.hideSpinner(component, event, helper);
                var state = response.getState();
                console.log(state);
               
                if (state === "SUCCESS") {
                    console.log('Success');
					 helper.gotoList(component);
                     
                    // Update the UI: close panel, show toast, refresh  page
                  //  $A.get("e.force:closeQuickAction").fire();
                     
                  var resultsToast = $A.get("e.force:showToast");
                  resultsToast.setParams({
                      "Title": "Updated World Fleet References",
                      "type": "success",
                      "message": "Updating All the Fleet Summaries. An email with results will be sent shortly."
                 });
                resultsToast.fire();
        	} else{
                    var errors = response.getError();
                	helper.gotoList(component, event, helper);
                    helper.handleErrors(component,errors);
                }
                  
       	});
            $A.enqueueAction(updateFleetSummaryAllAction);
   	 }else{
           
           helper.gotoList(component, event, helper);
        }
         
        
    }
 
     
})