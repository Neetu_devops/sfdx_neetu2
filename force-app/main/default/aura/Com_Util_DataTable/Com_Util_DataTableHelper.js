({
    loadMoreData : function(cmp, event){

        if(cmp.get("v.methodName") != undefined){
        
            event.getSource().set("v.isLoading", true);
            var methodName = 'c.' + cmp.get("v.methodName");
    
            var action = cmp.get(methodName);
            
            var params = cmp.get("v.params");
            
            if(params != undefined){
                params = JSON.parse(params); 
            }
            
            if(cmp.get("v.enableInfiniteLoading")){
                //initialize
                if(params == undefined){
                    params = {};
                }

                params['offset'] = cmp.get("v.data").length;
                params['orderBy'] = cmp.get('v.orderBy');

            }
            console.log(params);
            
            if(params != undefined){
               action.setParams(params); 
            }
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var data =  response.getReturnValue();
                    console.log(data);
                    var existingData = cmp.get("v.data");
                    var newData = existingData.concat(data);
                    cmp.set('v.data', newData);
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        var cmpEvent = cmp.getEvent("ErrorsEvent");
                        cmpEvent.setParams( { "errors" : errors } );
                        cmpEvent.fire();
                    } else {
                        console.log("Unknown error");
                    }
                }
                event.getSource().set("v.isLoading", false);
            });
            
            $A.enqueueAction(action);
        }     
    },
    
	// Used to sort the 'Age' column
    sortBy: function(field, reverse, primer) {

        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

    handleSort: function(cmp, event) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var data = cmp.get('v.data');
        
        if(data != undefined && data.length>0){
            
            if(!cmp.get("v.enableInfiniteLoading") || cmp.get("v.methodName") == undefined){
            
                var cloneData = cmp.get('v.data').slice(0);
                cloneData.sort((this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
                cmp.set('v.data', cloneData);
            }else{
                //make a server call to refresh the table with new order by.
                cmp.set('v.data', []);
                cmp.set('v.orderBy', 'Order by ' + sortedBy + ' ' + sortDirection);
                this.loadMoreData(cmp, event);
            }    
            
            cmp.set('v.sortDirection', sortDirection);
            cmp.set('v.sortedBy', sortedBy);
        }    
    },

    downloadInvoice: function(cmp, invoiceId){
        
        cmp.set("v.showSpinner", true);
        var action = cmp.get("c.getInvoicePdf");

        action.setParams({"invoiceId" : invoiceId});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var data =  response.getReturnValue();
                
                if(data != null && data != undefined){
                    $A.get('e.lightning:openFiles').fire({
                        recordIds: [data]
                    }); 
                }    
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var cmpEvent = cmp.getEvent("ErrorsEvent");
                    cmpEvent.setParams( { "errors" : errors } );
                    cmpEvent.fire();
                } else {
                    console.log("Unknown error");
                }
            }
            
            cmp.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    }
    
})