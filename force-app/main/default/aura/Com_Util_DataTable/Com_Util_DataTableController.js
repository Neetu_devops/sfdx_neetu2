({ 
    doInit: function(cmp, event, helper) {

       	helper.loadMoreData(cmp,event);
    },
    
	 handleSort: function(cmp, event, helper) {
		helper.handleSort(cmp, event);
    },
    
    loadMore: function (cmp, event, helper) {
        console.log('load more');
		if(cmp.get("v.data").length < cmp.get("v.totalRecords")) {
			helper.loadMoreData(cmp,event);
        }
    },
    
    callRowAction: function(cmp, event, helper){

        if(event.getParam('action').name == 'view_invoice'){
            helper.downloadInvoice(cmp,event.getParam('row').Id); 
       	}
	}
    
})