({
    doInit : function(component, event, helper) {
  
        helper.loadData(component);
    },
    
    refreshData : function(component, event, helper) {
        if(component!=event.getSource()){
        	helper.loadData(component);
        } 
    },
    
    downloadDataAsCSV : function(component, event, helper){
        // get the Records [contact] list from 'ListOfContact' attribute 
        var prospectData = component.get("v.prospectWrapperList");
        
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.convertArrayOfObjectsToCSV(component,prospectData);   
        if (csv == null){return;} 
        
        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'ProspectsBidSummary.csv';  // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file
    },
    
    applyStatusFilter : function(component, event, helper){
        
        var options = component.get("v.projectStatusList");
        var filterStatus = '';
        if(options != undefined && options.length > 0){
            options.forEach(function(option){
                if(option.selected)
                    filterStatus += option.value + ',';
            });
            filterStatus = filterStatus.slice(0, -1);
        }
        
        $A.util.addClass(component.find("projectStatusConfigDiv"), 'slds-hide');
        
        //save the filter status.
        var action = component.get("c.updateFilterStatus");
        
        helper.showSpinner(component);
        
        action.setParams({ 
            'projectId' : component.get('v.recordId'),
            'filters' : filterStatus
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                helper.loadData( component);
                
                var appEvent = $A.get("e.c:ProjectStatusEvent");
                appEvent.fire();
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action); 
    },
    
    closeChooseProjecStatusBox : function(component, event, helper){
        $A.util.addClass(component.find("projectStatusConfigDiv"), 'slds-hide'); 
    },
    
    openChooseProjectStatusBox:  function(component, event, helper){
        $A.util.removeClass(component.find("projectStatusConfigDiv"), 'slds-hide'); 
        
    },
    
    hideProspectWithNoBids : function(component, event, helper) {
        
        helper.showSpinner(component);
        component.set("v.hideProspects", !component.get("v.hideProspects"));
     	helper.hideProspects(component);
        
        var appEvent = $A.get("e.c:ToggleInactiveProspectsEvent");
        appEvent.setParams({"hideInactiveProspects" :  component.get("v.hideProspects")});
        appEvent.fire();
    },
    
    toggleInactiveProspects : function(component, event, helper) {
        if(component!=event.getSource()){
            component.set("v.hideProspects", event.getParam("hideInactiveProspects"));
            helper.hideProspects(component);
        }    
    },
    sortBy:function(component, event, helper){
        var colName=event.currentTarget.getAttribute("data-colName");
        component.set("v.showArrowAfterClick",true);
        helper.sortData(component,event,colName);
        component.set("v.selectedColumn",colName);
    }
})