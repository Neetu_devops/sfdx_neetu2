({
    loadData : function(component) {
        
        var action = component.get("c.getBidSummary");

        this.showSpinner(component);
        
        action.setParams({ 
            'projectId' : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var value = response.getReturnValue();   
                if(value != undefined){
                    
                    component.set("v.prospectWrapperList",value.ProspectWrapperList); 
                    
                    //set project status options.
                    //var projectStatusOptions = new Array();
                    
                    if(value.projectStatusOptions != undefined){
                        
                        var selectedValues = new Array();
                        
                        if(value.selectedFilterStatus != undefined){
                            var selectedValues = value.selectedFilterStatus.split(',');
                        }  
                        
                        value.projectStatusOptions.forEach(function(option){
                            
                            /* var obj = new Object();
                            obj.label = option.label;
                            obj.value = option.value; */
                            
                            if(selectedValues.includes(option.value)){
                             	option.selected =  true;   
                            }

                           // projectStatusOptions.push(obj);
                        });
                    }
                    component.set("v.projectStatusList",value.projectStatusOptions);
                }    
            }else{
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
            component.set("v.showArrowAfterClick",false);
        });
        $A.enqueueAction(action);
    },
    
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys variable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['prospect','totalBids','sumOfBids','numberOfMaxBid', 'averageBid' ];
        var headers = ['Prospect','Number Of Assets','Total Bid','Total Max Bid', 'Average Bid' ];
        
        csvStringResult = '';
        csvStringResult += headers.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
                
                counter++;
                
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        
        // return the CSV formate String e
        return csvStringResult;       
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    },
    
     showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
     hideProspects : function (component) {
        
        var prospectData = component.get("v.prospectWrapperList");
        
		if(prospectData != undefined && prospectData != null){
			
            var hideProspects = component.get("v.hideProspects");

			for(var index in prospectData){
				if(prospectData[index].totalBids == '0'){
                    prospectData[index].isHiddenRow= hideProspects; 
    }
			}
			
			component.set("v.prospectWrapperList", prospectData);
		}
        this.hideSpinner(component);
    },
    
    sortData:function(component,event,sortFieldName){
        var currentDir = component.get("v.arrowDirection");
    
        if (currentDir == 'arrowdown') {
            component.set("v.arrowDirection", 'arrowup');
            component.set("v.isAsc", true);
        } else {
            component.set("v.arrowDirection", 'arrowdown');
            component.set("v.isAsc", false);
        }
        var sortAsc = component.get("v.isAsc");
        var records = component.get("v.prospectWrapperList");
        if(sortFieldName=='prospect'){
             records.sort(function(a,b){
            var t1 = a[sortFieldName].toLowerCase() == b[sortFieldName].toLowerCase(),
                t2 = (!a[sortFieldName].toLowerCase() && b[sortFieldName].toLowerCase()) || (a[sortFieldName].toLowerCase() < b[sortFieldName].toLowerCase());
            return t1? 0: (sortAsc?-1:1)*(t2?-1:1);
        });
        }
        else{
             records.sort(function(a,b){
            var t1 = a[sortFieldName] == b[sortFieldName],
                t2 = (!a[sortFieldName] && b[sortFieldName]) || (a[sortFieldName] < b[sortFieldName]);
            return t1? 0: (sortAsc?-1:1)*(t2?-1:1);
        });
        }
    
        component.set("v.prospectWrapperList", records);
    }    
})