({
	doInit : function(component, event, helper) {
        helper.doInit(component, event);
    },
    
    createAssumedUtilizations : function(component, event, helper) {
        
        var data = component.get("v.assumedUtilizationRec");
        
        var tableValues = data.assumedUtilizationItemList;
        var flag;
        for(var i = 0; i < tableValues.length; i++){
            if(tableValues[i].fhFC == undefined || tableValues[i].fhFC == ''){
                flag = true;
                break;
            }
        }
        
        if(data.assumptionStartDate < data.reconciliationStartDate){
            helper.showToast("Error", "Assumption Start Date can never be less than Period Start Date.","Error");
        }else if(data.assumptionEndDate < data.assumptionStartDate || data.assumptionEndDate > data.reconciliationEndDate){
            helper.showToast("Error", "Assumption End Date can never be greater than Period End Date.","Error");
        }else if(data.assumptionStartDate > data.assumptionEndDate){
            helper.showToast("Error", "Assumption Start Date cannot be greater than Assumption End Date.","Error");
        }else if(data.assumptionStartDate < data.leaseStartDate ||  data.assumptionEndDate >  data.leaseEndDate){
            helper.showToast("Error", "Assumed Utilization generation cannot happen before/after the lease term.","Error");
        }else if(flag){
            helper.showToast("Error", "Please enter Required Fields.","Error");
        }else{
            helper.createUtilizations(component, event);
        }
    },
    
    calculateDateFrequency : function(component, event, helper) {
        var data = component.get("v.assumedUtilizationRec");
        
        var startDate = new Date(data.assumptionStartDate);
        
        var endDate = new Date(data.assumptionEndDate);
        
        if(endDate.getFullYear() == startDate.getFullYear()){
            data.assumptionFrequency = endDate.getMonth() - startDate.getMonth() + 1;
        }else{
            data.assumptionFrequency = (endDate.getFullYear() - startDate.getFullYear() - 1) * 12 + 12 - startDate.getMonth() + 1 + endDate.getMonth();
        }
        component.set("v.assumedUtilizationRec",data);
    },
    
    calculateFHFCRatio  : function(component, event, helper) {
        
        var indx = event.currentTarget.getAttribute("data-index");
        
        var data = component.get("v.assumedUtilizationRec");
        var tableValues = data.assumedUtilizationItemList;
        
        if(tableValues[indx].fH != undefined && tableValues[indx].fH != '' && 
           tableValues[indx].fC != undefined && tableValues[indx].fC != '' && 
           tableValues[indx].fC != 0){
            tableValues[indx].fhFC = tableValues[indx].fH / tableValues[indx].fC;
        }else{
            tableValues[indx].fhFC = undefined;
        }
        
        data.assumedUtilizationItemList = tableValues;
        component.set("v.assumedUtilizationRec",data);
        
    },
    
    setAirframeFH : function(component, event, helper) {
        var data = component.get("v.assumedUtilizationRec");
        var tableValues = data.assumedUtilizationItemList;
        
        for(var i = 0; i < tableValues.length; i++){
            
            tableValues[i].fH = data.assumedMonthlyFH;
            tableValues[i].fhFC = data.assumedMonthlyFH / tableValues[i].fC;
            
        }
        data.assumedUtilizationItemList = tableValues;
        component.set("v.assumedUtilizationRec",data);
    },
    
    setAirframeFC : function(component, event, helper) {
        var data = component.get("v.assumedUtilizationRec");
        var tableValues = data.assumedUtilizationItemList;
        
        for(var i = 0; i < tableValues.length; i++){
            
            tableValues[i].fC = data.assumedMonthlyFC;
            tableValues[i].fhFC = tableValues[i].fH / data.assumedMonthlyFC;
            
        }
        data.assumedUtilizationItemList = tableValues;
        component.set("v.assumedUtilizationRec",data);
    },
    
    cancelDialog : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        
        if(recordId != null) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
    },
})