({
    doInit: function(component, event) {
        this.showSpinner(component);
        var action = component.get("c.getData");
        action.setParams({
            "recordId": component.get("v.recordId"),
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            
            if (response.getState() === "SUCCESS") {
                
                var allData =  response.getReturnValue();
                allData.invoiceDate = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
                component.set("v.assumedUtilizationRec", allData);
                var invoiceDayList = [];
                for(var i = 1; i < 32; i++){
                    var obj = [];
                    obj.label = i;
                    obj.value = i;
                    invoiceDayList.push(obj);
                }
                component.set("v.invoiceDayList", invoiceDayList);
                
                var screenHeight = screen.availHeight;
                screenHeight = (screenHeight * 65) / 100;
                component.set("v.screenHeight", screenHeight);
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    createUtilizations : function(component, event) {
        var data = component.get("v.assumedUtilizationRec");
        this.showSpinner(component);
        var action = component.get("c.createUtilizations");
        action.setParams({
            data : JSON.stringify(data),
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                this.showToast("Success", "Utilizations created successfully!","SUCCESS");
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                });
                navEvt.fire();
                
            }
            else if(state === 'ERROR') {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinner",true);
    },
    
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinner",false);
    },
    
    //to show the toast message
    showToast : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    }, 
    
    handleErrors : function(errors) {
        
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );
                        });  
                    };
                }
            });
        }
    },
})