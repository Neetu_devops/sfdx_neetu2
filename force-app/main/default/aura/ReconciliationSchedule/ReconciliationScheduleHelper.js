({
    doInit: function(component, event) {
        this.showSpinner(component);
        var recdId = component.get("v.recordId");
       
        var action = component.get("c.getDefaultData");
        action.setParams({
            "leaseId": recdId,
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
             
            if (response.getState() === "SUCCESS") {
                
                var allData =  response.getReturnValue();
                
                component.set("v.reconciliationObj", allData);
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    navigateTo: function(component, recId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId
        });
        navEvt.fire();
        $A.get('e.force:refreshView').fire();
        this.hideSpinner(component);
    },   
    
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinner",true);
    },
    
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinner",false);
    },
    
    showToast: function(component, title, type, message){

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : type,
            "message": message
    	});
    	toastEvent.fire();
        this.hideSpinner(component);
    },
    
    getParameterByName: function(component, event, name) {
        name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var regex = new RegExp("[?&]" + name + "(=1\.([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    
    handleErrors: function (errors, type) {
        _jsLibrary.handleErrors(errors, type); // Calling the JS Library function to handle the error
    },
})