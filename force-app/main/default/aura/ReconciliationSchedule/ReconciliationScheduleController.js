({
    doInit : function(component, event, helper) {
       
        var value = helper.getParameterByName(component , event, 'inContextOfRef');
        var context = JSON.parse(window.atob(value));
        component.set("v.recordId", context.attributes.recordId);
       
        if(component.get("v.recordId") != undefined){
            helper.doInit(component, event);
        }else{
             helper.showToast(component, 'Error','Error','No Lease found.');
        }
        
    },
    
    saveReconciliationRecords : function(component, event, helper) {
        
        if(component.get("v.recordId") != undefined){
            helper.showSpinner(component);
            var reconciliationObj = component.get("v.reconciliationObj");
            
            
            if(reconciliationObj.startDate >= reconciliationObj.leaseEndDate || reconciliationObj.endDate > reconciliationObj.leaseEndDate){
                helper.showToast(component, 'Error','error','Reconciliation Schedule generation cannot happen before/after the lease term. Please select a Start Date and End Date within the lease Period.');
            }else if(reconciliationObj.startDate < reconciliationObj.leaseStartDate ){
                helper.showToast(component, 'Error','error','Please select a Start Date within the lease period.');
            }else if(reconciliationObj.endDate != undefined && reconciliationObj.endDate != '' && reconciliationObj.endDate < reconciliationObj.startDate){
                helper.showToast(component, 'Error','error','End Date can never be less than Start Date.');
            }else if(reconciliationObj.reconciliationPeriod == undefined){
                helper.showToast(component, 'Error','error','Reconciliation Period (in months) cannot be empty.');
            }else if(reconciliationObj.endDate != undefined && reconciliationObj.endDate != '' && reconciliationObj.endDate < reconciliationObj.leaseStartDate){
                helper.showToast(component, 'Error','error','Please select a End Date within the lease period.');
            }else{
                
                var action = component.get("c.saveReconciliationSchedules");
                action.setParams({
                    "reconciliationData": JSON.stringify(component.get("v.reconciliationObj")),
                }); 
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                     
                    if (response.getState() === "SUCCESS") {
                        helper.showToast(component, 'Succcess','success','Record saved successfully.')
                        
                        helper.navigateTo(component, reconciliationObj.leaseId);
                    }
                    else if(state === "ERROR") {
                        var errors = response.getError();
                        helper.handleErrors(errors);
                    }
                    helper.hideSpinner(component);
                });
                $A.enqueueAction(action);
            }
        }else{
            helper.showToast(component, 'Error','Error','No Lease found.');
        }
    },
   
    cancelDialog : function(component, event, helper) {
       
        var recdId = component.get("v.recordId");
        
        if(recdId != undefined){
            helper.navigateTo(component, recdId);
            component.set("v.recordId", undefined);
        }else{
            
            window.location.href='/lightning/o/Reconciliation_Schedule__c/list?filterName=Recent';
        }
    }
})