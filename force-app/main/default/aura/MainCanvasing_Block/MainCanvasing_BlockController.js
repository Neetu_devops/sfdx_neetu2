({ 

    callChildCancel : function(component, event, helper) {
        var typeSelected = component.get("v.TypeSelected");
        var TypeOfDeal = component.get("v.TypeOfDeal");
        if(typeSelected == 'Deal') {
        	if(TypeOfDeal == 'MA') {
            	var childComp = component.find('childComp');
                childComp.callChildCancel();
            }
            else if(TypeOfDeal == 'MC') {
                var childComp = component.find('childCampComp');
                childComp.callChildCancel();
            }
        }
        else if(typeSelected == 'Transaction') {
            var childComp = component.find('TransactionId');
            childComp.callChildCancel();
        }
    },
    
    callChildSave : function(component, event, helper) {
        var typeSelected = component.get("v.TypeSelected");
        var TypeOfDeal = component.get("v.TypeOfDeal");
        if(typeSelected == 'Deal') {
        	if(TypeOfDeal == 'MA') {
            	var childComp = component.find('childComp');
                childComp.callChildSave();
            }
            else if(TypeOfDeal == 'MC') {
                var childComp = component.find('childCampComp');
                childComp.callChildSave();
            }
        }
        else if(typeSelected == 'Transaction') {
            var childComp = component.find('TransactionId');
            childComp.callChildSave();
        }
    },
    
    initialize : function(component, event, helper) {
        
		console.log("MainCanvassing initialize(+)==>");
        var opts = [
            { class: "optionClass", label: "Marketing", value: "Deal", selected: "true" },
            { class: "optionClass", label: "Transactions", value: "Transaction" }
          
        ];
        component.find("InputCanvassingType").set("v.options", opts);
              
        component.set("v.TypeSelected","Deal");
        
        // call custom setting to get Deal Type
        
        var action = component.get("c.getDealType_Apex");
        action.setCallback(this,function(a){
        	console.log("after getDealType_Apex");
        	console.log(a.getState());
            if(a.getState()=="SUCCESS")	{
            	console.log("after getDealType_Apex Success");
                var mapReturn = a.getReturnValue();
                console.log("DEAL_TYPE--" + mapReturn["DEAL_TYPE"]);
                console.log("WF_COUNT--" +mapReturn["WF_COUNT"]);
                console.log("DEAL_TYPE_TRX--" +mapReturn["DEAL_TYPE_TRX"]);
                component.set("v.TypeOfDeal",mapReturn["DEAL_TYPE"]);
                component.set("v.worldFleetSubscription",mapReturn["WF_COUNT"]);
                component.set("v.trxON",mapReturn["DEAL_TYPE_TRX"]);
            }
            else{
                console.log("Error");
            }
        });
        $A.enqueueAction(action);        
        
        console.log("MainCanvassing initialize(-)==>");		
	},
    
    onSelectChangeCanvassingType : function(component, event, helper) {
        console.log("MainCanvassing.onSelectChangeCanvassingType(+)==>");
        
        var selected = component.find("InputCanvassingType").get("v.value");
        console.log("selected==>"+selected);
        component.set("v.TypeSelected",selected);
      
		console.log("MainCanvassing.onSelectChangeCanvassingType(-)==>");
    },
})