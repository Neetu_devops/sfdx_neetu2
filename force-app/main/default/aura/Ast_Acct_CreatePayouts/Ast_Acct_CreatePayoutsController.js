({
    doInit: function (cmp, event, helper) {
        helper.loadInitData(cmp, event, helper);
    },

    handleValidateAction: function (cmp, event, helper) {
        let payableData = cmp.get("v.payableData");

        if (helper.checkUndefined(payableData.payoutRec.Lease__r)) {
            helper.handleMessage("Please select a Lease to create a Payout.");
        }
        else if (helper.checkUndefined(payableData.payoutRec.Payout_Amount__c)) {
            helper.handleMessage("Please enter an amount for the Payout.");
        }
        else if(!helper.checkUndefined(payableData.payoutRec.Payout_Amount__c) && payableData.payoutRec.Payout_Amount__c<=0.0){
            helper.handleMessage("The amount to be paid out cannot be less than or equal to zero.");
        }
        else if (!helper.checkUndefined(payableData.payoutRec.Payout_Amount__c) && payableData.payableRec.Payable_Balance__c < payableData.payoutRec.Payout_Amount__c) {
            helper.handleMessage("Payout Amount is greater than Payable Balance.");
        }
        else {
            helper.handleSaveAction(cmp, event, helper);
        }
    },

    handleCancel: function (cmp, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
})