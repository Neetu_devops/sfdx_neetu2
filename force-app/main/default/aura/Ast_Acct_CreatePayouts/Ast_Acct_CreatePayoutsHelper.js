({
    loadInitData: function (cmp, event, helper) {
        this.showSpinner(cmp);
        let action = cmp.get("c.fetchInitData");
        action.setParams({
            "recordId": cmp.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.payableData", response.getReturnValue());
            } else {
                var errors = response.getError();
                this.handleMessage(errors);
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
    },

    handleSaveAction: function (cmp, event, helper) {
        this.showSpinner(cmp);
        let payableData = cmp.get("v.payableData");
        if (!helper.checkUndefined(payableData.payoutRec.Lease__r)) {
            payableData.payoutRec.Lease__c = payableData.payoutRec.Lease__r.Id;
        }
        if (!helper.checkUndefined(payableData.payoutRec.Lessee__r)) {
            payableData.payoutRec.Lessee__c = payableData.payoutRec.Lessee__r.Id;
        }

        let action = cmp.get("c.savePayout");
        action.setParams({
            "payableData": JSON.stringify(payableData),
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                this.handleMessage("Payout created successfully", "Success");
                $A.get("e.force:closeQuickAction").fire();
            } else {
                var errors = response.getError();
                this.handleMessage(errors);
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
    },

    checkUndefined: function (input) {
        if (input !== undefined && input !== '' && input !== null) { return false; }
        return true;
    },

    showSpinner: function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    //Show the error toast when any error occured
    handleMessage: function (errors, type, duration) {
        duration = "10000"; // time to display the Toast message.
        _jsLibrary.handleErrors(errors, type, duration); // Calling the JS Library function to handle the error
    }
})