({
    init : function(cmp, event, helper) {
        var action = cmp.get("c.getRecordDetail");
        action.setParams({'recId':cmp.get("v.recId"),
                          'objName' : cmp.get("v.objName"),
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                cmp.set("v.record",result);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    goBack: function(cmp,event){
        var evt = $A.get("e.c:AircraftSearchEvent");
        evt.setParams({'evtName':'backToResult'
                      });
        evt.fire();
    },
    goToNewSearch : function(cmp,event){
        console.log(2);
        var evt = $A.get("e.c:AircraftSearchEvent");
        evt.setParams({'evtName':'NewSearch'
                      });
        evt.fire();
    },
    redirectToRecord : function(cmp,event){
        event.stopPropagation();
        var recId = $(event.target).attr('data-recid');
        alert(context);
        window.location.href="/"+recId;
        var context = cmp.get("v.userContext");
        console.log(context);
        if(context != undefined) {
            if(context == 'Theme4t' || context == 'Theme4d') {
                console.log('VF in S1 or LEX');
                
            } else {
                console.log('VF in Classic'); 
                
            }
        } else {
            console.log('standalone Lightning Component');
            var event = $A.get("e.force:navigateToSObject");
            event.setParams({"recordId": component.get("v.contact").Id});
            event.fire();
        }
    }
})