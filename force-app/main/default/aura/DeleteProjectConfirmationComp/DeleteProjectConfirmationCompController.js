({
	deleteProject : function(component, event, helper) {
        
    	var answer = event.currentTarget.dataset.answer;
        
        if(answer != 'Cancel'){
 
            var action = component.get("c.deleteProjectRecord");
            action.setParams({
                "projectId" : component.get("v.recordId"),
                "answer" : answer
        	});
        
        	action.setCallback(this, function(response) {
                
                var state = response.getState();
                
                if (response.getState() == "SUCCESS") {
    				
                    var event1 = $A.get('e.force:navigateToObjectHome');
                    event1.setParams({
                        scope: 'Project__c'
                    });
                    
                    event1.fire(); 
                    
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
        	});
        	$A.enqueueAction(action);
              
        }else{
            $A.get("e.force:closeQuickAction").fire() ;
        } 
	}
})