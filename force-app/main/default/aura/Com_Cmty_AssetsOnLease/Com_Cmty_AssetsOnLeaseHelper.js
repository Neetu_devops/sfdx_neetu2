({
    setColumns: function(cmp) {
        
        //{ label: 'Aircraft Type', fieldName: 'aircraftType', sortable: true },
        
        var aircraftColumns = [
            { label: 'MSN', fieldName: 'msnURL', type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'msn' } }, sortable: true },
            { label: 'Aircraft Type', fieldName: 'aircraftType', sortable: true },
           // { label: 'Asset Variant', fieldName: 'assetVariant', sortable: true },
            { label: 'Vintage', fieldName: 'vintage', sortable: true },
            { label: 'Registration', fieldName: 'registration', sortable: true },
            { label: 'Aircraft Engine Type', fieldName: 'aircraftEngineType', sortable: true },
            { label: 'ESN #1', fieldName: 'esn1', sortable: true },
            { label: 'ESN #2', fieldName: 'esn2', sortable: true },
            { label: 'ESN #3', fieldName: 'esn3', sortable: true },
            { label: 'ESN #4', fieldName: 'esn4', sortable: true },
            { label: 'Lease No.', fieldName: 'leaseNo', sortable: true },
            { label: 'Lessee Name', fieldName: 'lesseeName', sortable: true }, 
            { label: 'Operator Name', fieldName: 'operatorName', sortable: true }, 
            { label: 'Lease Start', fieldName: 'leaseStart', type: 'date', sortable: true },
            { label: 'Lease End', fieldName: 'leaseEnd', type: 'date', sortable: true },
        ];

        cmp.set('v.aircraftColumns', aircraftColumns);
            
        var engineColumns = [
            { label: 'ESN', fieldName: 'esnURL', type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'esn' } }, sortable: true },
            { label: 'Asset Series', fieldName: 'assetSeries', sortable: true },
            { label: 'Configuration', fieldName: 'currentThrust', sortable: true },
            { label: 'TSN', fieldName: 'tsn', type : 'number', sortable: true },
            { label: 'CSN', fieldName: 'csn', type : 'number', sortable: true },
            { label: 'TSO', fieldName: 'tso', type : 'number', sortable: true },
            { label: 'CSO', fieldName: 'cso', type : 'number', sortable: true },
            { label: 'Limiter', fieldName: 'limiter', type : 'number', sortable: true },
            { label: 'Limiting Part', fieldName: 'limitingPart', sortable: true },
            { label: 'Lease No.', fieldName: 'leaseNo', sortable: true },
            { label: 'Lessee Name', fieldName: 'lesseeName', sortable: true }, 
            { label: 'Operator Name', fieldName: 'operatorName', sortable: true }, 
            { label: 'Lease Start', fieldName: 'leaseStart', type: 'date', sortable: true },
            { label: 'Lease End', fieldName: 'leaseEnd', type: 'date', sortable: true },
        ];
        
        cmp.set('v.engineColumns', engineColumns);
    }

})