({
    doinit: function (component, event, helper) {
        var searchData = component.get("v.searchData");
        console.log('searchData: ',searchData);
        var assemblyWrapper = JSON.stringify(searchData);
        component.set("v.assemblyWrapper", assemblyWrapper);
        var OperatorAssetCountMap = component.get("v.operatorCountMap");
        helper.setTableStructure(component, event, helper);
        helper.setRowsInTable(component, event, helper);
    },
    onEmailChange: function(component, event, helper){
        let name = event.getSource().get("v.name");
        let value = event.getSource().get("v.value");
        console.log('Name: ',name);
        if(value != undefined && value != null &&value != '' && (value.includes(';') || value.includes(','))){
            helper.handleErrors('Please provide only one valid email address');
        }
        else{
            if(value != undefined && value != null &&value != '' && name.split('-')[2] != null && name.split('-')[2] != 'null'){
                var action = component.get("c.updateEmail");
                action.setParams({
                    operatorId: name.split('-')[2],
                    email : value
                });
                action.setCallback(this, (response) => {
                    let state = response.getState();
                    if (state == "SUCCESS") {
                        let result = response.getReturnValue();
                        console.log(result);
                    }
                    else if(state == "ERROR"){
                        let error = response.getError();
                        if(error[0].message.includes('INVALID_EMAIL_ADDRESS')){
                            helper.handleErrors('Please enter a valid email address');
                        }
                        else{
                            helper.handleErrors(response.getError());
                        }
                    }
                });
                $A.enqueueAction(action);
            }
        }
        
},
 handleSubmit: function(component, event, helper){
    var result = event.currentTarget.value;
    component.set("v.trueUpAttributes", result);
    var recId = result.split('-')[0];
    let action = component.get("c.getWarningMsg");
    action.setParams({
        utilizationId : recId
    });
    action.setCallback(this, function (response) {
        let state = response.getState();
        if (state == "SUCCESS") {
            console.log('Reconciliation Schedule: ', response.getReturnValue());
            let warning = response.getReturnValue();
            if(warning != undefined && warning != null && warning != ''){
                // Show Pop Up
                component.set("v.warningMsg", warning);
                component.set("v.showReconciliationApproveModal", true);
            }
            else{
                var action = component.get('c.handleUtilization');
                $A.enqueueAction(action);
            }
        }
        else if(state == "ERROR"){
            console.log('ERROR');
            this.handleErrors(response.getError());
        }
    });
    $A.enqueueAction(action);
    
},
    checkReconciliationCondition : function(component, event, helper){
    var searchData = component.get("v.searchData");
    var result = event.currentTarget.value;
    component.set("v.trueUpAttributes", result);
    var Id = result.split('-')[0];
    var operatorIndex = result.split('-')[1];
    var assetIndex = result.split('-')[2];
    var trueUpIndex = result.split('-')[3];
    var record = searchData.operatorData[operatorIndex].assetData[assetIndex];
    
    if(record.utilization.Y_hidden_Lease__c != undefined){
        let utilizationId = record.utilization.Id;
        let leaseId = record.utilization.Y_hidden_Lease__c;
        let action = component.get("c.getReconciliationCondition");
        action.setParams({
            utilizationId : utilizationId,
            leaseId: leaseId,
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state == "SUCCESS") {
                console.log('Reconciliation Schedule: ', response.getReturnValue());
                let hasReconciliation = response.getReturnValue().hasReconciliationSchedule;
                
                if(hasReconciliation){
                    // Show Pop Up
                        component.set("v.reconSchedule", response.getReturnValue().reconschedules);
					let url = window.location.href.substr(0, window.location.href.indexOf('/lightning'))+'/'+response.getReturnValue().reconschedules.Id;
                    component.set("v.reconciliationURL", url);
                    component.set("v.showReconciliationModal", true);
                }
                else{
                    helper.validateTrueUpValues(component, event, helper);
                }
            }
            else if(state == "ERROR"){
                console.log('ERROR');
                this.handleErrors(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
    else{
        helper.validateTrueUpValues(component, event, helper);
    }    
},
    closeReconciliationModal: function (component, event, helper) {
        component.set("v.showReconciliationModal", false);
        component.set("v.showReconciliationApproveModal", false);
        
    },
    handleTrueUpFHSave: function (component, event, helper) {
        helper.validateTrueUpValues(component, event, helper);
    },
    handleFHSave: function (component, event, helper) {
        var result = event.currentTarget.value;
        var Id = result.split('-')[0];
        var operatorIndex = result.split('-')[1];
        var assetIndex = result.split('-')[2];
        console.log(Id, operatorIndex, assetIndex);
        var searchData = component.get("v.searchData");
        var record = {};
        var assemblyRecord;
        var hasEmptyValues = true;
       // var checkcondition = false;
        if (Id == '') {
            record = searchData.operatorData[operatorIndex].assetData[assetIndex];
            record.apuValues='';
            for (let key2 in searchData.operatorData[operatorIndex].assetData[assetIndex].lstComponentWrapper) {
                assemblyRecord = searchData.operatorData[operatorIndex].assetData[assetIndex].lstComponentWrapper[key2];
                if(assemblyRecord.assembly == "APU Values"){
                    if(assemblyRecord.apuFC == "" || assemblyRecord.apuFC == null || assemblyRecord.apuFC == undefined){
                        assemblyRecord.apuFC = null; 
                    }else{
                        assemblyRecord.apuFC = parseInt(assemblyRecord.apuFC);   
                    }
                }
                if(assemblyRecord.flightHM != null || assemblyRecord.flightC != null) {
                    hasEmptyValues = false;
                }
            }
            if (record.totalFHM === '' && record.totalFC === undefined) {
                hasEmptyValues = true;
                for (let key2 in record.lstComponentWrapper) {
                    assemblyRecord = record.lstComponentWrapper[key2];
                    console.log(assemblyRecord.flightHM, assemblyRecord.flightC);
                    if((assemblyRecord.flightHM != null || assemblyRecord.flightC != null) && assemblyRecord.flightHM != '') {
                        hasEmptyValues = false;
                    }
                }
            }
        }
        else {
            record = helper.handleFHValues(Id, searchData, record, hasEmptyValues, assemblyRecord, helper);
            record.apuValues='';
            if (record.totalFHM === '' && record.totalFC === undefined) {
                hasEmptyValues = true;
                for (let key2 in record.lstComponentWrapper) {
                    assemblyRecord = record.lstComponentWrapper[key2];
                    if(assemblyRecord.assembly == "APU Values"){
                        if(assemblyRecord.apuFC == "" || assemblyRecord.apuFC == null || assemblyRecord.apuFC == undefined){
                           assemblyRecord.apuFC = null; 
                        }
                        else{
                            assemblyRecord.apuFC = parseInt(assemblyRecord.apuFC); 
                        }
                    }
                    console.log(assemblyRecord.flightHM, assemblyRecord.flightC);
                    if((assemblyRecord.flightHM != null || assemblyRecord.flightC != null) && assemblyRecord.flightHM != '') {
                        hasEmptyValues = false;
                    }
                }
            }
            else{
                for (let key2 in record.lstComponentWrapper) {
                    console.log('hasEmptyValues1: ',hasEmptyValues);
                    assemblyRecord = record.lstComponentWrapper[key2];
                    if(assemblyRecord.assembly == "APU Values"){
                        if(assemblyRecord.apuFC == "" || assemblyRecord.apuFC == null || assemblyRecord.apuFC == undefined){
                           assemblyRecord.apuFC = null; 
                        }
                        else{
                            assemblyRecord.apuFC = parseInt(assemblyRecord.apuFC);   
                        }
                    }
                    console.log(assemblyRecord.flightHM, assemblyRecord.flightC);
                    if((assemblyRecord.flightHM != null || assemblyRecord.flightC != null) && assemblyRecord.flightHM != '') {
                        hasEmptyValues = false;
                    }
                }
            }
        }
        if (hasEmptyValues) {
            helper.handleErrors('Please enter FH/FC values for the primary Assembly: Airframe for the Aircraft and Engine 1 for the Engine Asset. Other Assemblies may remain unknown, but confirm Unknown by clicking on the field');
        }
        else {
            if(record.totalFH === ''){
                record.totalFH = null;
            }
            if(record.totalFC === ''){
                record.totalFC = null;
            }
            for(let key in record.lstComponentWrapper){
                var assemblyRec = record.lstComponentWrapper[key];
                if(assemblyRec.assembly != "APU Values" && assemblyRec.assembly != undefined){
                    if(assemblyRec.flightHM === ''){
                        hasEmptyValues = true;
                    }
                    if(assemblyRec.flightC === undefined){
                        hasEmptyValues = true;
                    }
                }
                if(assemblyRec.deRate === undefined || assemblyRec.deRate === '')
                    assemblyRec.deRate = null;
            }
            console.log('record : ', record);

            // Save record if all FH/FC values are filled.
            if(hasEmptyValues){
                helper.handleErrors('Please enter FH/FC values for the primary Assembly: Airframe for the Aircraft and Engine 1 for the Engine Asset. Other Assemblies may remain unknown, but confirm Unknown by clicking on the field');
            }
            else{
                console.log('saveUtilizationRecord');
                helper.saveUtilizationRecord(component, helper, Id, searchData, assetIndex, record);
            }
        }
    },

    getIndex: function (component, event, helper) {
        console.log(event.getSource().get("v.value"));
        var result = event.getSource().get("v.value");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);


        var initData = component.get("v.searchData");
        if (initData.operatorData[opIndex].assetData != null) {
            var showDiv = initData.operatorData[opIndex].assetData[astIndex].showDiv;
            var isOpen = initData.operatorData[opIndex].assetData[astIndex].isOpen;
            if (showDiv) {
                showDiv = false;
            }
            else {
                showDiv = true;
            }
            if (isOpen) {
                isOpen = false;
            }
            else {
                isOpen = true;
            }
            initData.operatorData[opIndex].assetData[astIndex].showDiv = showDiv;
            initData.operatorData[opIndex].assetData[astIndex].isOpen = isOpen;
            component.set("v.searchData", initData);
        }
    },
    getTrueUpIndex: function (component, event, helper) {
        console.log(event.getSource().get("v.value"));
        var result = event.getSource().get("v.value");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var trueUpastIndex = parseInt(result.split('-')[2]);


        var initData = component.get("v.searchData");
        if (initData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList != null) {
            var showDiv = initData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpastIndex].showDiv;
            var isOpen = initData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpastIndex].isOpen;
            if (showDiv) {
                showDiv = false;
            }
            else {
                showDiv = true;
            }
            if (isOpen) {
                isOpen = false;
            }
            else {
                isOpen = true;
            }
            initData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpastIndex].showDiv = showDiv;
            initData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpastIndex].isOpen = isOpen;
            component.set("v.searchData", initData);
        }
    },
    handleClickShow: function (component, event, helper) {
        component.set("v.closeTable", false);
        var value = event.getSource().get("v.name");
        if (value == "Billing") {
            component.set("v.closeBilling", false);
        }
        else {
            component.set("v.closeInvoice", false);
        }
        console.log("value: ", value);
        var arr = event.getSource().get("v.class").split("_");
        console.log('*********', arr[0]);
        var headerInd = arr[0];
        var showBorderRight = document.querySelectorAll(".showBorder" + headerInd);
        console.log('showBorderRight>>>' + showBorderRight);
        for (var i = 0; i < showBorderRight.length; i++) {
            showBorderRight[i].classList.remove("borderRight");
        }

        var showSection = document.querySelectorAll("." + headerInd);
        for (var i = 0; i < showSection.length; i++) {
            showSection[i].classList.remove("slds-hide");
        }

        var topHeader = document.querySelectorAll("." + headerInd + "_topHeader");
        for (var j = 0; j < topHeader.length; j++) {
            if (headerInd.includes('Invoice'))
                topHeader[j].colSpan = "3";
            else
                topHeader[j].colSpan = "4";
        }

        if (component.get("v.isCreateNew") && !headerInd.includes('Invoice')) {
            var billingEmpty = document.querySelectorAll(".BillingEmpty");
            for (var j = 0; j < billingEmpty.length; j++) {
                billingEmpty[j].colSpan = "4";
                billingEmpty[j].classList.remove("borderRight");
            }

            var hideTableData = document.querySelectorAll(".hideTableData");
            for (var i = 0; i < hideTableData.length; i++) {
                hideTableData[i].classList.add("slds-hide");
            }
        }
        if (component.get("v.isCreateNew") && headerInd.includes('Invoice')) {
            var InvoiceEmpty = document.querySelectorAll(".InvoiceEmpty");
            for (var j = 0; j < InvoiceEmpty.length; j++) {
                InvoiceEmpty[j].colSpan = "6";
            }
        }


        var backwardSpan = document.querySelectorAll("." + headerInd + "_backward");
        var forwardSpan = document.querySelectorAll("." + headerInd + "_forward");
        for (var j = 0; j < topHeader.length; j++) {
            backwardSpan[j].classList.remove("slds-hide");
            forwardSpan[j].classList.add("slds-hide");
        }
    },

    handleClickHide: function (component, event, helper) {
        helper.handleHide(component, event, helper);
    },
    onPin: function (component, event, helper) {
        console.log(event.getSource().get("v.value"));
        var result = event.getSource().get("v.value");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);

        var initData = component.get("v.searchData");
        var newIdList = [];
        var ispinned = false;
        var assetId = initData.operatorData[opIndex].assetData[astIndex].Asset.Id;
        if (initData.operatorData[opIndex].assetData != null) {
            var pinnedList = component.get("v.pinnedIds");
            if (pinnedList.length < 5) {
                pinnedList.push(initData.operatorData[opIndex].assetData[astIndex].Asset.Id);

                var ids = component.get("v.ignoreIdsList");
                for (let key in ids) {
                    if (ids[key] !== initData.operatorData[opIndex].assetData[astIndex].Asset.Id) {
                        newIdList.push(ids[key]);
                    }
                }
                component.set("v.pinnedIds", pinnedList);
                component.set("v.ignoreIdsList", newIdList);
                ispinned = true;
            }
            else {
                helper.handleErrors('You cannot pin more than 5 records at a time');
            }
        }
        console.log('PinnedList: ', component.get("v.pinnedIds"));
        console.log('IgnoreList: ', component.get("v.ignoreIdsList"));
        initData.operatorData[opIndex].assetData[astIndex].ispinned = ispinned;
        component.set("v.searchData", initData);

    },

    onunpin: function (component, event, helper) {
        console.log(event.getSource().get("v.value"));
        var result = event.getSource().get("v.value");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);

        var initData = component.get("v.searchData");
        var ignoreIdList = component.get("v.ignoreIdsList");
        var assetId = initData.operatorData[opIndex].assetData[astIndex].Asset.Id;
        var pinnedList = component.get("v.pinnedIds");
        ignoreIdList.push(assetId);

        var newPinnedIdList = [];
        for (let key in pinnedList) {
            if (pinnedList[key] !== assetId) {
                newPinnedIdList.push(pinnedList[key]);
            }
        }
        initData.operatorData[opIndex].assetData[astIndex].ispinned = false;
        component.set("v.pinnedIds", newPinnedIdList);
        component.set("v.searchData", initData);
        console.log('PinnedList: ', newPinnedIdList);
        console.log('IgnoreList: ', ignoreIdList);
    },

    handleUtilization: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        var result = '';
        if(event == null || event == undefined){
            result = component.get("v.trueUpAttributes");
        }
        else{
            result = event.currentTarget.value;
        }
        component.set("v.showReconciliationApproveModal", false);
        console.log('recId: ', result);

        var recId = result.split('-')[0];
        var Status = result.split('-')[1];
        var type = result.split('-')[2];
        var reqType = result.split('-')[3];
	    var isTrueUp = false;
        var searchData = component.get("v.searchData");
        var operatorData = searchData.operatorData;
        var utilizationRecord;
        var Email;
        var InvoiceRecord;
        
        if(type == "True Up Gross"){
            isTrueUp = true;
            for (let key in operatorData) {
                for (let key1 in operatorData[key].assetData) {
                    if(operatorData[key].assetData[key1].trueUpWrapperList.length > 0){
                        for(let key2 in operatorData[key].assetData[key1].trueUpWrapperList){
                            if (operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization.Id == recId) {
                                utilizationRecord = operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization;
                                if (operatorData[key].assetData[key1].trueUpWrapperList[key2].email != undefined && operatorData[key].assetData[key1].trueUpWrapperList[key2].email != '')
                                    Email = operatorData[key].assetData[key1].trueUpWrapperList[key2].email;
                                if (operatorData[key].assetData[key1].trueUpWrapperList[key2].invoice != undefined && operatorData[key].assetData[key1].trueUpWrapperList[key2].invoice != '') {
                                    InvoiceRecord = operatorData[key].assetData[key1].trueUpWrapperList[key2].invoice;
                                    InvoiceRecord.Invoice_Line_Items__r = null;
                                }
                            }
                        }
                    }
                }
            } 
        }
        else{
            for (let key in operatorData) {
                for (let key1 in operatorData[key].assetData) {
                    if (operatorData[key].assetData[key1].utilization.Id == recId) {
                        utilizationRecord = operatorData[key].assetData[key1].utilization;
                        if (operatorData[key].assetData[key1].email != '' && operatorData[key].assetData[key1].email != undefined)
                            Email = operatorData[key].assetData[key1].email;
                        if (operatorData[key].assetData[key1].invoice != '' && operatorData[key].assetData[key1].invoice != undefined) {
                            InvoiceRecord = operatorData[key].assetData[key1].invoice;
                            InvoiceRecord.Invoice_Line_Items__r = null;
                        }
                    }
                }
            }
        }
        console.log('InvoiceRecord: ', InvoiceRecord);
        console.log('Email: ', Email);
        if (utilizationRecord != undefined && utilizationRecord != '' && utilizationRecord != null) {

            var action = component.get("c.updateStatus");
            action.setParams({
                utilizationRecord: JSON.stringify(utilizationRecord),
                email: Email,
                invoiceRecord: JSON.stringify(InvoiceRecord),
                requestType: reqType
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log('Result: ', result);
                    if (result != undefined && result != '' && result != null) {
                        if (result.ErrorMsg != '' && result.ErrorMsg != undefined && result.ErrorMsg != '') {
                            helper.handleErrors(result.ErrorMsg);
                        }
                        else {
                            if (InvoiceRecord != null && InvoiceRecord != undefined && InvoiceRecord != '') {
                                helper.handleErrors('The process to send Invoice has been initiated, Mail will be delivered Shortly.', 'Success');
                            }
                            console.log('Result: ', result);
                            if(isTrueUp){
                                for (let key in searchData.operatorData) {
                                    for (let key1 in searchData.operatorData[key].assetData) {
                                        for(let key2 in searchData.operatorData[key].assetData[key1].trueUpWrapperList){
                                            if (searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization.Id == recId) {
                                                searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization.Status__c = result.status;
                                                if (result.status == "Submitted To Lessor") {
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].color = "#F5CA48";
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].rowIcon = "approval";
                                                }
                                                if (result.status == "Rejected By Lessor") {
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].color = "#FF8F7F";
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].rowIcon = "save";
                                                }
                                                if (result.status == "Open") {
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].color = "#5D8EC9";
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].rowIcon = "send";
                                                }
                                                if (result.status == "Approved By Lessor") {
                                                    if(result.invoiceList.length > 0){
                                                        searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].isInvoicePresent = true;
                                                    }
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper = result.cwList;
                                                    helper.handleAPUOnApprove(searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2]);
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].color = "#1B9F88";
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].rowIcon = "description";
                                                    helper.setTableStructure(component, event, helper, 'resetChild');
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                for (let key in searchData.operatorData) {
                                    for (let key1 in searchData.operatorData[key].assetData) {
                                        if (searchData.operatorData[key].assetData[key1].utilization.Id == recId) {
                                            searchData.operatorData[key].assetData[key1].utilization.Status__c = result.status;
                                            if (result.status == "Submitted To Lessor") {
                                                searchData.operatorData[key].assetData[key1].color = "#F5CA48";
                                                searchData.operatorData[key].assetData[key1].rowIcon = "approval";
                                            }
                                            if (result.status == "Rejected By Lessor") {
                                                searchData.operatorData[key].assetData[key1].color = "#FF8F7F";
                                                searchData.operatorData[key].assetData[key1].rowIcon = "save";
                                            }
                                            if (result.status == "Open") {
                                                searchData.operatorData[key].assetData[key1].color = "#5D8EC9";
                                                searchData.operatorData[key].assetData[key1].rowIcon = "send";
                                            }
                                            if (result.status == "Approved By Lessor") {
                                                searchData.operatorData[key].assetData[key1].isTrueUp = true;
                                                searchData.operatorData[key].assetData[key1].lstComponentWrapper = result.cwList;
                                                helper.handleAPUOnApprove(searchData.operatorData[key].assetData[key1]);
                                                searchData.operatorData[key].assetData[key1].invoice = result.invoiceList[0];
                                                if(result.invoiceList.length > 0 && result.invoiceList[0].Invoice_Line_Items__r != undefined){
                                                    if(result.invoiceList[0].Invoice_Line_Items__r.length > 0){
                                                        for(let key2 in result.invoiceList[0].Invoice_Line_Items__r){
                                                            var invoiceLineItem = result.invoiceList[0].Invoice_Line_Items__r[key2];
                                                            //console.log('Invoice Line Item: ',invoiceLineItem);
                                                            for(let key3 in searchData.operatorData[key].assetData[key1].lstComponentWrapper){
                                                                let assembly = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key3];
                                                                //console.log('Assembly: ',assembly,assembly.assemblyUtilizationRecord);
                                                                if(assembly.assemblyUtilizationRecord != undefined && assembly.assemblyUtilizationRecord != null && invoiceLineItem != undefined){
                                                                    if(assembly.assemblyUtilizationRecord.Id == invoiceLineItem.Assembly_Utilization__c){
                                                                        searchData.operatorData[key].assetData[key1].lstComponentWrapper[key3].balance = invoiceLineItem.Assembly_Utilization__r.Total_Maintenance_Reserve__c;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        for(let key4 in searchData.operatorData[key].assetData[key1].lstComponentWrapper){
                                                            let bal = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key4].balance;
                                                            if(bal != undefined)
                                                            	searchData.operatorData[key].assetData[key1].totalBalance += bal;
                                                        }
                                                    }
                                                }
                                                if(result.invoiceList.length > 0){
                                                    searchData.operatorData[key].assetData[key1].isInvoicePresent = true;
                                                }
                                                searchData.operatorData[key].assetData[key1].color = "#1B9F88";
                                                searchData.operatorData[key].assetData[key1].rowIcon = "description";
                                                helper.setTableStructure(component, event, helper, 'resetChild');
                                            }
                                        }
                                    }
                                }
                            }
                            helper.getBulkButtonStatus(component,helper,searchData);
                            component.set("v.searchData", searchData);
                        }
                    }
                    console.log('Response: ', response.getReturnValue());
                    helper.hideSpinner(component, event, helper);
                }
                else if (state === "ERROR") {
                    helper.hideSpinner(component, event, helper);
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
            })
            $A.enqueueAction(action);
        }
    },

    changeFHValue: function (component, event, helper) {
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        console.log(opIndex + '@@' + astIndex);
        if (searchData.operatorData[opIndex].assetData != undefined)
            searchData.operatorData[opIndex].assetData[astIndex].ischanged = true;

        var assetFHM = searchData.operatorData[opIndex].assetData[astIndex].totalFHM;
        var assemblyWrapper = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper;
        var assemblyComponent = [];

        var assetFH;
        console.log('FH Change: ', assetFH, assetFHM, searchData.operatorData[opIndex].assetData[astIndex].totalFH);
        if (assetFHM === undefined) {
            assetFH = searchData.operatorData[opIndex].assetData[astIndex].totalFH;
        }
        if (assetFHM != undefined) {
            if (assetFHM.indexOf(':') != -1) {
                assetFH = (parseInt(assetFHM.split(':')[0]) + ((parseInt(assetFHM.split(':')[1])) / 60)).toFixed(2);
            }
            else {
                assetFH = assetFHM;
            }
        }
        if (assemblyWrapper.length > 0) {
            for (var i = 0; i < assemblyWrapper.length; i++) {
                if (assemblyWrapper[i].assembly != 'APU Values') {
                    if (assemblyWrapper[i].assocAssetIcon == undefined) {
                        assemblyWrapper[i].flightH = assetFH;
                        if (assetFHM != undefined)
                            assemblyWrapper[i].flightHM = assetFHM;
                    }
                    assemblyComponent.push(assemblyWrapper[i]);
                }
                else {
                    assemblyComponent.push(assemblyWrapper[i]);
                }
            }
        }
        searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper = assemblyComponent;
        searchData.operatorData[opIndex].assetData[astIndex].totalFH = assetFH;
        component.set("v.searchData", searchData);
    },
    changeTrueUpFHValue: function (component, event, helper) {
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var trueUpIndex = parseInt(result.split('-')[2]);
        console.log(opIndex + '@@' + astIndex);
        if (searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList != undefined)
            searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].ischanged = true;

        var assetFHM = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFHM;
        var assemblyWrapper = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper;
        var assemblyComponent = [];

        var assetFH;
        console.log('FH Change: ', assetFH, assetFHM, searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFH);
        if (assetFHM === undefined) {
            assetFH = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFH;
        }
        if (assetFHM != undefined) {
            if (assetFHM.indexOf(':') != -1) {
                assetFH = (parseInt(assetFHM.split(':')[0]) + ((parseInt(assetFHM.split(':')[1])) / 60)).toFixed(2);
            }
            else {
                assetFH = assetFHM;
            }
        }
        if (assemblyWrapper.length > 0) {
            for (var i = 0; i < assemblyWrapper.length; i++) {
                if (assemblyWrapper[i].assembly != 'APU Values') {
                    if (assemblyWrapper[i].assocAssetIcon == undefined) {
                        assemblyWrapper[i].flightH = assetFH;
                        if (assetFHM != undefined)
                            assemblyWrapper[i].flightHM = assetFHM;
                    }
                    assemblyComponent.push(assemblyWrapper[i]);
                }
                else {
                    assemblyComponent.push(assemblyWrapper[i]);
                }
            }
        }
        searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper = assemblyComponent;
        searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFH = assetFH;
        component.set("v.searchData", searchData);
    },
    
    changeTrueUpFCValue: function (component, event, helper) {
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var trueUpIndex = parseInt(result.split('-')[2]);
        console.log(result);
        var assetFHM;
        if (searchData.operatorData[opIndex].assetData.trueUpWrapperList != undefined)
            searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].ischanged = true;
        if(searchData.operatorData[opIndex] && searchData.operatorData[opIndex].assetData[astIndex] && searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex]
           && searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFC){
             assetFHM = Math.round(searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFC);
        }
        var assemblyWrapper = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper;
        var assemblyComponent = [];

        var assetFC;

        if (assetFHM.indexOf(':') != -1) {
            assetFC = (parseInt(assetFHM.split(':')[0]) + ((parseInt(assetFHM.split(':')[1])) / 60)).toFixed(0);
        }
        else {
            assetFC = assetFHM;
        }

        if (assemblyWrapper.length > 0) {
            for (var i = 0; i < assemblyWrapper.length; i++) {
                if (assemblyWrapper[i].assocAssetIcon == undefined) {
                    assemblyWrapper[i].flightC = assetFC;
                }
                assemblyComponent.push(assemblyWrapper[i]);
            }

        }
        searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper = assemblyComponent;
        if(assetFC){
        searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFC = Math.round(assetFC);
        }
        component.set("v.searchData", searchData);
    },
    changeFCValue: function (component, event, helper) {
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);

        if (searchData.operatorData[opIndex].assetData != undefined)
            searchData.operatorData[opIndex].assetData[astIndex].ischanged = true;

        var assetFHM = searchData.operatorData[opIndex].assetData[astIndex].totalFC;
        var assemblyWrapper = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper;
        var assemblyComponent = [];

        var assetFC;

        if (assetFHM.indexOf(':') != -1) {
            assetFC = (parseInt(assetFHM.split(':')[0]) + ((parseInt(assetFHM.split(':')[1])) / 60)).toFixed(0);
        }
        else {
            assetFC = assetFHM;
        }

        if (assemblyWrapper.length > 0) {
            for (var i = 0; i < assemblyWrapper.length; i++) {
                if (assemblyWrapper[i].assocAssetIcon == undefined) {
                    assemblyWrapper[i].flightC = assetFC;
                }
                assemblyComponent.push(assemblyWrapper[i]);
            }

        }
        searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper = assemblyComponent;
        searchData.operatorData[opIndex].assetData[astIndex].totalFC = assetFC;
        component.set("v.searchData", searchData);
    },
    handleFHUnknown: function (component, event, helper) {
        helper.handleFHUnknownValues(component, event, helper);
    },

    handleFCUnknown: function (component, event, helper) {
        console.log('handleFCUnknown');
        var value = event.getSource().get("v.value");
        console.log('handleFCUnknown',value);
        helper.roundoffValues(component, event, helper, value);
        helper.handleFCUnknownValues(component, event, helper);
        
    },
    onValueChange: function (component, event, helper) {
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var dateLabel = result.split('-')[2];
        if (dateLabel == 'date') {
            var value = event.getSource().get("v.value");
            var year = value.split('-')[0];
            var month = value.split('-')[1];
            var d = new Date(year, parseInt(month), 0);
            var newdate = formatDate(d);

            searchData.operatorData[opIndex].assetData[astIndex].utilization.Month_Ending__c = newdate;
            component.set("v.searchData", searchData);
            if (newdate != value) {
                var msg = 'This is the first Utilization record. Choose the last day of month of the first Utilization period for the utilization record';
                helper.handleErrors(msg, 'Warning');
            }

            function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                return [year, month, day].join('-');
            }
        }
        console.log('result: ', result);
        var searchData = component.get("v.searchData");
        if (searchData.operatorData[opIndex].assetData != undefined)
            searchData.operatorData[opIndex].assetData[astIndex].ischanged = true;
    },
    showTable: function (component, event, helper) {
        var show = component.get("v.showTable");
        if (show) {
            component.set("v.showTable", false);
            show = false;
        }
        else {
            component.set("v.showTable", true);
            show = true;
        }
        var searchData = component.get("v.searchData");
        for (let key in searchData.operatorData) {
            for (let key1 in searchData.operatorData[key].assetData) {
                if (show) {
                    searchData.operatorData[key].assetData[key1].showDiv = true;
                    searchData.operatorData[key].assetData[key1].isOpen = true;
                }
                else {
                    searchData.operatorData[key].assetData[key1].showDiv = false;
                    searchData.operatorData[key].assetData[key1].isOpen = false;
                }
            }
        }
        component.set("v.searchData", searchData);
    },
    handleTrueUp: function (component, event, helper) {
        var result = event.currentTarget.value;
        var opIndex = result.split('-')[0];
        var astIndex = result.split('-')[1];
        var searchData = component.get("v.searchData");
        var showModal = false;
        var assetRecord = searchData.operatorData[opIndex].assetData[astIndex];
        var utilId = searchData.operatorData[opIndex].assetData[astIndex].utilization.Id;
        console.log('AssetRecord',assetRecord);
        var astToSet = {
            "Id" : assetRecord.Asset.Id,
            "Name" : assetRecord.Asset.Name
        }
        
        for(let key in searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList){
            var status = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[key].utilization.Status__c;
            if(status != "Approved By Lessor"){
                showModal= true;
            }
        }
        if(showModal){
            component.set("v.showTrueUpModal", true);
        }
        else{
            component.set("v.assetRecord", astToSet);
            component.set("v.showLookup", false);
            component.set("v.showLookup", true);
            component.set("v.assetCount",0);
            component.set("v.trueUpUtilizationId",utilId);
            component.set("v.loadTrueUpData", false);
            component.set("v.loadTrueUpData", true);
        }
        
    },
    closeTrueUpModal: function (component, event, helper) {
        component.set("v.showTrueUpModal", false);
    },
    deleteUtilization: function(component,event,helper){
        helper.showSpinner(component, event, helper);
        var searchData = component.get("v.searchData");
        var idsOnUI = [];
        var totalAssetsOnPage = 0;
        for(let key in searchData.operatorData){
            for(let key1 in searchData.operatorData[key].assetData){
                if(searchData.operatorData[key].assetData[key1].showpin){
                    totalAssetsOnPage++;
                    idsOnUI.push(searchData.operatorData[key].assetData[key1].Asset.Id);
                }
            }
        }
        console.log('Total Assets: ',totalAssetsOnPage);
        console.log('IdsOnUI: ',idsOnUI);
        component.set("v.loadDataOnDelete", false);
        console.log('Id: ', event.getSource().get("v.value"));
        var recId = event.getSource().get("v.value");
        var action = component.get("c.deleteUtilizationRecord");
        action.setParams({
            recordId: recId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                console.log('SUCCESS');
                let ignoreList = component.get("v.ignoreIdsList");
                console.log('ignoreList: ',ignoreList);
                idsOnUI = ignoreList.filter(val => !idsOnUI.includes(val));
                console.log('IdsonUI: ', idsOnUI);
                component.set("v.ignoreIdsList", idsOnUI);
                console.log('AssetCount: ',component.get("v.assetCount"));
                if(component.get("v.assetCount") % 10 != 1){
                    let noOfAssets = component.get("v.assetCount") - totalAssetsOnPage;
                    component.set("v.assetCount", noOfAssets);
                }
                component.set("v.loadDataOnDelete", true); 
            }
            else if(state == "ERROR"){
				helper.hideSpinner(component, event, helper);
                let errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    }
})