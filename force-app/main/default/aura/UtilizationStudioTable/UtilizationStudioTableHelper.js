({
    // Show the lightning Spinner
    showSpinner: function (component, event) {
        component.set("v.showSpinner", true);
    },

    // Hide the lightning Spinner
    hideSpinner: function (component, event) {
        component.set("v.showSpinner", false);
    },
    setTableStructure: function (component, event, helper, reset) {
        helper.showSpinner(component, event, helper);
        var self = this;
        var header = ["Billing", "Invoice"];
        setTimeout(function () {
            if (header.length > 0) {
                for (var k = 0; k < header.length; k++) {
                    var innerHeader = header[k];
                    var hideSection = document.querySelectorAll("." + innerHeader);
                    for (var i = 0; i < hideSection.length; i++) {
                        hideSection[i].classList.add("slds-hide");
                    }

                    var topHeader = document.querySelectorAll("." + innerHeader + "_topHeader");
                    for (var j = 0; j < topHeader.length; j++) {
                        topHeader[j].colSpan = "2";
                    }

                    var backwardSpan = document.querySelectorAll("." + innerHeader + "_backward");
                    var forwardSpan = document.querySelectorAll("." + innerHeader + "_forward");
                    for (var j = 0; j < topHeader.length; j++) {
                        backwardSpan[j].classList.add("slds-hide");
                        forwardSpan[j].classList.remove("slds-hide");
                    }
                }
            }
        }, 0);

        setTimeout(function () {
            if (reset == 'resetChild') {
                var showBorderRight = document.querySelectorAll(".showBorderBilling");
                console.log('Reset: ', showBorderRight);
                for (var i = 0; i < showBorderRight.length; i++) {
                    showBorderRight[i].classList.add("borderRight");
                }
                component.set("v.closeBilling", true);
                component.set("v.closeInvoice", true);
            }
        }, 0);

        

    },
    setRowsInTable: function (component, event, helper) {
        var searchData = component.get("v.searchData");
        var allDataList = searchData.operatorData;
        var OperatorAssetCountMap = component.get("v.operatorCountMap");
        var assetCount = component.get("v.assetCount");
        if (OperatorAssetCountMap != null && OperatorAssetCountMap != '') {
            for (let key in allDataList) {
                if (allDataList[key].Operator.Name != 'No Operator' && allDataList[key].numberOfAssets != undefined)
                    allDataList[key].numberOfAssets = OperatorAssetCountMap[allDataList[key].Operator.Id];
                for (let key1 in allDataList[key].assetData) {
                    if (allDataList[key].assetData[key1].showpin) {
                        allDataList[key].assetData[key1].assetCount = assetCount + 1;
                        assetCount++;
                    }
                }
            }
        }
        component.set("v.assetCount", assetCount);
        setTimeout(function () {
            helper.hideSpinner(component, event, helper);
        }, 500);
        console.log("allDataList: ", allDataList);
    },
    handleHide: function (component, event, helper) {
        var value = event.getSource().get("v.name");
        if (value == "Billing") {
            component.set("v.closeBilling", true);
        } else {
            component.set("v.closeInvoice", true);
        }
        var arr = event.getSource().get("v.class").split("_");
        var headerInd = arr[0];
        var showBorderRight = document.querySelectorAll(".showBorder" + headerInd);
        for (var i = 0; i < showBorderRight.length; i++) {
            showBorderRight[i].classList.add("borderRight");
        }

        if (component.get("v.isCreateNew") && !headerInd.includes('Invoice')) {
            var billingEmpty = document.querySelectorAll(".BillingEmpty");
            for (var j = 0; j < billingEmpty.length; j++) {
                billingEmpty[j].colSpan = "3";
                billingEmpty[j].classList.add("borderRight");
            }

            var hideTableData = document.querySelectorAll(".hideTableData");
            for (var i = 0; i < hideTableData.length; i++) {
                hideTableData[i].classList.add("slds-hide");
            }
        }

        if (component.get("v.isCreateNew") && headerInd.includes('Invoice')) {
            var InvoiceEmpty = document.querySelectorAll(".InvoiceEmpty");
            for (var j = 0; j < InvoiceEmpty.length; j++) {
                InvoiceEmpty[j].colSpan = "3";
            }
        }

        var hideSection = document.querySelectorAll("." + headerInd);
        for (var i = 0; i < hideSection.length; i++) {
            hideSection[i].classList.add("slds-hide");
        }

        var topHeader = document.querySelectorAll("." + headerInd + "_topHeader");
        for (var j = 0; j < topHeader.length; j++) {
            topHeader[j].colSpan = "2";
        }

        var backwardSpan = document.querySelectorAll("." + headerInd + "_backward");
        var forwardSpan = document.querySelectorAll("." + headerInd + "_forward");
        for (var j = 0; j < topHeader.length; j++) {
            backwardSpan[j].classList.add("slds-hide");
            forwardSpan[j].classList.remove("slds-hide");
        }
    },
    calculateRatio: function (component, event, helper, searchData) {
        console.log('Calculate ratio');
        var searchData = component.get("v.searchData");
        //var operatorData = searchData.operatorData;
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var trueUpIndex = result.split('-')[2];

        var assemblyWrapperToSet = JSON.parse(component.get("v.assemblyWrapper"));
        console.log('assemblyWrapperToSet: ', assemblyWrapperToSet);
        var assemblyWrapper;
        var assetFHM;
        var assetFC;
        var utilizationWrapper;

        if (trueUpIndex === undefined || trueUpIndex === '') {
            assemblyWrapper = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper;
            utilizationWrapper = assemblyWrapperToSet.operatorData[opIndex].assetData[astIndex].lstComponentWrapper;
            assetFHM = searchData.operatorData[opIndex].assetData[astIndex].totalFHM;
            if(searchData.operatorData[opIndex] && searchData.operatorData[opIndex].assetData[astIndex]
               && searchData.operatorData[opIndex].assetData[astIndex].totalFC){
            assetFC = Math.round(searchData.operatorData[opIndex].assetData[astIndex].totalFC);
            }
        } else {
            assemblyWrapper = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper;
            assetFHM = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFHM;
            if(searchData.operatorData[opIndex] && searchData.operatorData[opIndex].assetData[astIndex] &&
               searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex] &&
               searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFC){
                assetFC = Math.round(searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFC);
            }
            utilizationWrapper = assemblyWrapperToSet.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper;
            for (let key in utilizationWrapper) {
                for (let key1 in searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper) {
                    if (utilizationWrapper[key].assembly != undefined && utilizationWrapper[key].assembly != "APU Values") {
                        if (utilizationWrapper[key].assembly === searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[key1].assembly) {
                            let flightH = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[key1].flightH;
                            let flightC = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[key1].flightC;
                            if (flightH != undefined && flightH != '' && flightH != null)
                                utilizationWrapper[key].tsn = utilizationWrapper[key].tsn - flightH;
                            if (flightC != undefined && flightC != '' && flightC != null)
                                utilizationWrapper[key].csn = utilizationWrapper[key].csn - flightC;
                            break;
                        }
                    }
                }
            }
        }

        console.log('AssetFHM: ', assetFHM);

        var assemblyComponent = [];

        var utilization = [];
        if (utilizationWrapper.length > 0) {
            for (var i = 0; i < utilizationWrapper.length; i++) {
                var assembly = new Object;
                assembly.assemblyName = utilizationWrapper[i].assembly;
                assembly.tsn = utilizationWrapper[i].tsn;
                assembly.csn = utilizationWrapper[i].csn;
                utilization.push(assembly);
            }
        }

        var assemblyVal = utilization;
        var assetFH;
        var assetFC;

        if (trueUpIndex == undefined || trueUpIndex == '') {
            assetFH = searchData.operatorData[opIndex].assetData[astIndex].totalFH;
            if(searchData.operatorData[opIndex] && searchData.operatorData[opIndex].assetData[astIndex] &&
              searchData.operatorData[opIndex].assetData[astIndex].totalFC){
                assetFC = Math.round(searchData.operatorData[opIndex].assetData[astIndex].totalFC);}
        } else {
            assetFH = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFH;
            if(searchData.operatorData[opIndex] && searchData.operatorData[opIndex].assetData[astIndex] &&
              searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex] &&
               searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFC){
                assetFC = Math.round(searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalFC);}
        }

        var totalTSN = 0.0;
        var totalCSN = 0.0;

        if (assemblyWrapper.length > 0) {
            for (var i = 0; i < assemblyWrapper.length; i++) {
                var assemblyName = assemblyWrapper[i].assembly;
                if (assemblyName != 'APU Values') {

                    if (assemblyWrapper[i].flightHM != undefined && assemblyWrapper[i].flightHM != '') {
                        if (assemblyWrapper[i].flightHM.indexOf(':') != -1) {
                            assemblyWrapper[i].flightH = (parseInt(assemblyWrapper[i].flightHM.split(':')[0]) + ((parseInt(assemblyWrapper[i].flightHM.split(':')[1])) / 60)).toFixed(2);
                        } else {
                            assemblyWrapper[i].flightH = parseFloat(assemblyWrapper[i].flightHM);
                        }
                    }

                    if (assemblyWrapper[i].flightH == 0 && assemblyWrapper[i].flightC == 0) {
                        assemblyWrapper[i].ratio = 0
                    } else {
                        if (assemblyWrapper[i].flightH != undefined && assemblyWrapper[i].flightH.toString().includes(':')) {
                            assemblyWrapper[i].ratio = (parseInt(assemblyWrapper[i].flightH.split(':')[0]) + ((parseInt(assemblyWrapper[i].flightH.split(':')[1])) / 60)).toFixed(2) / assemblyWrapper[i].flightC;
                        } else if ((assemblyWrapper[i].flightH == null || assemblyWrapper[i].flightH == undefined || assemblyWrapper[i].flightH == '') && (assemblyWrapper[i].flightC == null || assemblyWrapper[i].flightC == undefined || assemblyWrapper[i].flightC == '')) {
                            assemblyWrapper[i].ratio = 0;
                        } else {
                            assemblyWrapper[i].ratio = assemblyWrapper[i].flightH / assemblyWrapper[i].flightC;
                        }
                    }
                    if (assemblyVal.length > 0) {
                        for (var j = 0; j < assemblyVal.length; j++) {
                            if (assemblyVal[j].assemblyName == assemblyName && assemblyWrapper[i].flightC != undefined) {
                                assemblyWrapper[i].csn = parseInt(assemblyVal[j].csn) + parseInt(assemblyWrapper[i].flightC);
                            } else if (assemblyVal[j].assemblyName == assemblyName && (assemblyWrapper[i].flightC == undefined)) {
                                assemblyWrapper[i].csn = parseInt(assemblyVal[j].csn);
                            }

                            if (assemblyVal[j].assemblyName == assemblyName && assemblyWrapper[i].flightH != undefined && assemblyWrapper[i].flightH != null && assemblyWrapper[i].flightH != '') {
                                if (assemblyWrapper[i].flightH.toString().includes(':')) {
                                    assemblyWrapper[i].tsn = parseFloat(assemblyVal[j].tsn) + parseFloat(assemblyWrapper[i].flightH.split(':')[0]) + parseFloat(assemblyWrapper[i].flightH.split(':')[1]) / 60;
                                } else {
                                    assemblyWrapper[i].tsn = parseFloat(assemblyVal[j].tsn) + parseFloat(assemblyWrapper[i].flightH);
                                }
                            } else if (assemblyVal[j].assemblyName == assemblyName && (assemblyWrapper[i].flightH == undefined || assemblyWrapper[i].flightH == null || assemblyWrapper[i].flightH == '')) {
                                assemblyWrapper[i].tsn = parseFloat(assemblyVal[j].tsn);
                            }
                        }
                    }

                    if (assemblyName == 'Airframe') {
                        totalTSN += assemblyWrapper[i].tsn;
                        totalCSN += assemblyWrapper[i].csn;
                    }
                }
                assemblyComponent.push(assemblyWrapper[i]);
            }

        }
        console.log('Assemblies Values: ', assemblyComponent);
        console.log(assetFH, assetFC);
        if (trueUpIndex == undefined || trueUpIndex == '') {
            if ((assetFH == 0 && assetFC == 0) || (assetFH == undefined || assetFC == undefined)) {
                searchData.operatorData[opIndex].assetData[astIndex].totalRatio = 0;
            } else {
                if (assetFH.toString().includes(':')) {
                    assetFH = parseFloat(assetFH.split(':')[0]) + parseFloat(assetFH.split(':')[1]) / 60;
                    searchData.operatorData[opIndex].assetData[astIndex].totalRatio = assetFH / assetFC;
                } else {
                    searchData.operatorData[opIndex].assetData[astIndex].totalRatio = assetFH / assetFC;
                }
            }
        } else {
            if ((assetFH == 0 && assetFC == 0) || (assetFH == undefined || assetFC == undefined)) {
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalRatio = 0;
            } else {
                if (assetFH.toString().includes(':')) {
                    assetFH = parseFloat(assetFH.split(':')[0]) + parseFloat(assetFH.split(':')[1]) / 60;
                    searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalRatio = assetFH / assetFC;
                } else {
                    searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalRatio = assetFH / assetFC;
                }
            }
        }
        if (trueUpIndex == undefined || trueUpIndex == '') {
            searchData.operatorData[opIndex].assetData[astIndex].totalTSN = totalTSN;
            searchData.operatorData[opIndex].assetData[astIndex].totalCSN = totalCSN;
            searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper = assemblyComponent;
        } else {
            searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalTSN = totalTSN;
            searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].totalCSN = totalCSN;
            searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper = assemblyComponent;
        }

        component.set("v.searchData", searchData);
    },
    handleFHUnknownValues: function (component, event, helper) {
        console.log('handleFHUnknownValues: ');
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var trueUpIndex = parseInt(result.split('-')[2]);
        var asset;
        var assemblyWrapper;
        var assetFHM;
        if (trueUpIndex != undefined && trueUpIndex != '') {
            asset = searchData.operatorData[parseInt(opIndex)].assetData[parseInt(astIndex)];
        } else {
            asset = searchData.operatorData[parseInt(opIndex)].assetData[parseInt(astIndex)].trueUpWrapperList[parseInt(trueUpIndex)];
        }
        assemblyWrapper = asset.lstComponentWrapper;
        assetFHM = asset.totalFHM;

        console.log('assetFHM: ', assetFHM);
        for (let key in assemblyWrapper) {
            if (assemblyWrapper[key].assocAssetIcon === undefined || assemblyWrapper[key].assocAssetIcon === '') {
                if (assetFHM == '' || assetFHM == undefined) {
                    assemblyWrapper[key].flightHM = null;
                    assemblyWrapper[key].flightH = null;
                    assemblyWrapper[key].fHUnknown = true;
                    event.getSource().set("v.class", "inputFeilds asset placeHChange");
                } else {
                    assemblyWrapper[key].fHUnknown = false;
                    event.getSource().set("v.class", "inputFeilds asset");
                }
            }
        }
        console.log('assemblyWrapper FH: ', assemblyWrapper);
        this.calculateRatio(component, event, helper, searchData);

    },
    handleFCUnknownValues: function (component, event, helper) {
        console.log('handleFCUnknownValues: ');
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var trueUpIndex = parseInt(result.split('-')[2]);

        var asset;
        if (trueUpIndex != undefined && trueUpIndex != '') {
            asset = searchData.operatorData[parseInt(opIndex)].assetData[parseInt(astIndex)];
        } else {
            asset = searchData.operatorData[parseInt(opIndex)].assetData[parseInt(astIndex)].trueUpWrapperList[parseInt(trueUpIndex)];
        }
        var assemblyWrapper = asset.lstComponentWrapper;
        var assetFC;
        if(asset.totalFC){
        assetFC = Math.round(asset.totalFC);
        }
        for (let key in assemblyWrapper) {
            if (assemblyWrapper[key].assocAssetIcon === undefined || assemblyWrapper[key].assocAssetIcon === '') {
                if (assetFC == '' || assetFC == undefined) {
                    assemblyWrapper[key].flightC = null;
                    assemblyWrapper[key].fCUnknown = true;
                    event.getSource().set("v.class", "inputFeilds asset placeHChange");
                } else {
                    assemblyWrapper[key].flightC = assetFC;
                    assemblyWrapper[key].fCUnknown = false;
                    event.getSource().set("v.class", "inputFeilds asset");
                }
            }
        }
        console.log('assemblyWrapper FC: ', assemblyWrapper);

        this.calculateRatio(component, event, helper, searchData);

    },
    
    roundoffValues: function(component, event, helper,value) {
        console.log('roundoffValues'+value);
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var trueUpIndex = parseInt(result.split('-')[2]);
        var asset;
        if (trueUpIndex != undefined && trueUpIndex != '') {
            asset = searchData.operatorData[parseInt(opIndex)].assetData[parseInt(astIndex)];
        } else {
            asset = searchData.operatorData[parseInt(opIndex)].assetData[parseInt(astIndex)].trueUpWrapperList[parseInt(trueUpIndex)];
        }
        if(value){
            asset.totalFC = Math.round(value);
        }
        console.log('asset.totalFC',asset.totalFC);
        
    },
    handleFHValues: function (Id, searchData, record, hasEmptyValues, assemblyRecord, helper) {
        for (let key in searchData.operatorData) {
            for (let key1 in searchData.operatorData[key].assetData) {
                if (searchData.operatorData[key].assetData[key1].utilization.Id == Id) {
                    let totalFH = searchData.operatorData[key].assetData[key1].totalFH;
                    if (totalFH != undefined && totalFH.toString().includes(':')) {
                        searchData.operatorData[key].assetData[key1].totalFHM = totalFH;
                        searchData.operatorData[key].assetData[key1].totalFH = 12; // Give value 1 just to pass the validation
                        searchData.operatorData[key].assetData[key1].totalRatio = 0;
                    }
                    record = searchData.operatorData[key].assetData[key1];
                    console.log(record.totalFHM, record.totalFC);

                    console.log(record);
                    for (let key2 in searchData.operatorData[key].assetData[key1].lstComponentWrapper) {
                        if (!searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].showAPUrow) {
                            let flightH = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].flightH;
                            if (flightH != undefined && flightH.toString().includes(':')) {
                                let fHInDecimal = parseInt(flightH.split(':')[0]) + (parseInt(flightH.split(':')[1]) / 60);
                                searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].flightHM = flightH;
                                searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].flightH = fHInDecimal;
                            }
                        }
                    }
                }
            }
        }
        return record;
    },

    handleTrueUpFHValues: function (Id, searchData, record, hasEmptyValues, assemblyRecord, helper) {
        for (let key in searchData.operatorData) {
            for (let key1 in searchData.operatorData[key].assetData) {
                for (let key2 in searchData.operatorData[key].assetData[key1].trueUpWrapperList) {
                    if (searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization.Id == Id) {
                        let totalFH = searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalFH;
                        if (totalFH.toString().includes(':')) {
                            searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalFHM = totalFH;
                            searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalFH = 12; // Give value 1 just to pass the validation
                            searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalRatio = 0;
                        }
                        record = searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2];
                        console.log(record.totalFHM, record.totalFC);

                        console.log(record);
                        for (let key3 in searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper) {
                            if (!searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].showAPUrow) {
                                let flightHM = searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].flightHM;
                                if (flightHM != undefined && flightHM != null && flightHM.toString().includes(':')) {
                                    let fHInDecimal = parseInt(flightHM.split(':')[0]) + (parseInt(flightHM.split(':')[1]) / 60);
                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].flightHM = flightHM;
                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].flightH = fHInDecimal;
                                }
                            }
                        }
                    }
                }
            }
        }
        return record;
    },
	convertDateToString: function(date) {
        let day = date.getUTCDate();
        let month = date.getUTCMonth() + 1;
        let year = date.getUTCFullYear();
        let yyyy_MM_dd = year + "-" + month + "-" + day; 
        return yyyy_MM_dd;
    },
    saveUtilizationRecord: function (component, helper, Id, searchData, assetIndex, record) {
        component.set("v.loadInOpenMode", false);
        //component.set("v.showBilling", true);
        //component.set("v.showInvoice", true);
        var redirectToOpen = false;
        var UtilId = Id;
        var astIndex = assetIndex;
        var status = record.utilization.Status__c;
        let monthEnding = record.utilization.Month_Ending__c;
        if(monthEnding != undefined && monthEnding != '' && monthEnding != null){
         	record.utilization.Month_Ending__c = this.convertDateToString(new Date(monthEnding)); 
            console.log('Month Ending: ',record.utilization.Month_Ending__c);
        }
        
        helper.showSpinner(component, event, helper);
        console.log('saveUtilizationRecord :'+JSON.stringify(record));
        var action = component.get("c.saveFHdata");
        action.setParams({
            recordsToSave: JSON.stringify(record)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                component.set("v.showTableRendering", false);
                var result = response.getReturnValue();
                console.log('result:', result);
                if (result != undefined && result != '' && result != null) {
                    for (let key in searchData.operatorData) {
                        for (let key1 in searchData.operatorData[key].assetData) {
                            let checkCondition = false;
                            if(status === 'New'){
                                if (result.Asset.Id === searchData.operatorData[key].assetData[key1].Asset.Id) {
                                    console.log('Asset Matched');
                                    checkCondition = true;
                                }
                            }
                            else if(status === 'Open'){
                                if(result.utilization.Id === searchData.operatorData[key].assetData[key1].utilization.Id){
                                    console.log('Utilization Matched');
                                    checkCondition = true;
                                }
                            }
                            if (checkCondition) {
                                searchData.operatorData[key].assetData[key1] = result;
                                searchData.operatorData[key].assetData[key1].firstUtilization = false;
                                searchData.operatorData[key].assetData[key1].ischanged = false;
                                if (component.get("v.isCreateNew")) {
                                    searchData.operatorData[key].assetData[key1].lockrecord = true;
                                }
                                var apukey = 0;
                                var apukey1 = 0;
                                var apukey2 = 0;
                                var hasapu = false;
                                var apuFHM;
                                var apuFH;
                                var apuFC;
                                var apuTSN;
                                var apuCSN;
                                var hasapuCounterRow = false;
                                var count = 0;
                                var apuValueList = searchData.operatorData[key].assetData[key1].apuValues.split(';');
                                for (let key2 in searchData.operatorData[key].assetData[key1].lstComponentWrapper) {
                                    if(searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].asmName == 'APU'){
                                        if(!searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].apuCounterRow){
                                            hasapuCounterRow = true;
                                        }
                                        else{
                                            count++;
                                        }
                                    }
                                    if (hasapu && searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].assembly == undefined) {
                                        apukey2++;
                                    }
                                    var assemblyData = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2];
                                    if (assemblyData.flightHM === "" || assemblyData.flightHM === undefined) {
                                        assemblyData.flightHM = null;
                                        assemblyData.fHUnknown = true;
                                    }
                                    if (assemblyData.flightC === "" || assemblyData.flightC === undefined) {
                                        assemblyData.flightC = null;
                                        assemblyData.fCUnknown = true;
                                    }
                                    console.log('assemblyData: ', assemblyData.assembly);
                                    if (assemblyData.assembly != undefined && assemblyData.assembly === 'APU') {
                                        apuFHM = assemblyData.apuFHM;
                                        apuFH = assemblyData.apuFH;
                                        apuFC = assemblyData.apuFC;
                                        apuTSN = assemblyData.apuTSN;
                                        apuCSN = assemblyData.apuCSN;
                                        hasapu = true;
                                        apukey = key;
                                        apukey1 = key1;
                                        apukey2 = parseInt(key2);
                                        var derateRowObj = {
                                            'assembly': 'APU Values',
                                            'showAPUrow': true,
                                            'apuFH': null,
                                            'apuFHM': '',
                                            'apuFC': null
                                        }
                                    }

                                }
                                if (hasapu) {
                                    searchData.operatorData[apukey].assetData[apukey1].lstComponentWrapper.splice(parseInt(apukey2) + parseInt(1), 0, derateRowObj);
                                    searchData.operatorData[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM = apuFHM;
                                    searchData.operatorData[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC = apuFC;
                                    searchData.operatorData[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH = apuFH;
                                    searchData.operatorData[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuTSN = apuTSN;
                                    searchData.operatorData[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuCSN = apuCSN;
                                    if(searchData.operatorData[apukey].assetData[apukey1].apuValues !='')
                                        helper.handleAPUEvents(searchData.operatorData[apukey].assetData[apukey1].lstComponentWrapper,apukey2,hasapuCounterRow,count,apuValueList);
                                }
                                if (component.get("v.removeRowEnabled")) {
                                    if (component.get("v.isCreateNew")) {
                                        if (UtilId == '') {
                                            searchData.operatorData[key].assetData.splice(key1, 1);
                                        }
                                        if (searchData.operatorData[key].assetData.length == 0) {
                                            searchData.operatorData.splice(key, 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (let key in searchData.operatorData) {
                        for (let key1 in searchData.operatorData[key].assetData) {
                            if (searchData.operatorData[key].assetData[key1].utilization.Status__c == 'New') {
                                redirectToOpen = true;
                            }
                        }
                    }
                    if (!redirectToOpen && component.get("v.isCreateNew")) {
                        component.set("v.loadInOpenMode", true);
                    }
                    if (component.get("v.removeRowEnabled")) {
                        if (component.get("v.isCreateNew")) {
                            var assetCount = 0;
                            for (let key in searchData.operatorData) {
                                for (let key1 in searchData.operatorData[key].assetData) {
                                    searchData.operatorData[key].assetData[key1].assetCount = assetCount + 1;
                                    assetCount++;
                                }
                            }
                            component.set("v.assetCount", assetCount);
                        }
                    }

                    //searchData.operatorData[parseInt(opkey)].assetData[parseInt(astkey)] = result;
                    component.set("v.searchData", searchData);
                    component.set("v.showTableRendering", true);
                    console.log('SearchData at End', component.get("v.searchData"));
                    this.setTableStructure(component, event, helper, 'resetChild');
                    helper.hideSpinner(component, event, helper);
                }
            } else if (state === "ERROR") {
                helper.hideSpinner(component, event, helper);
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        })
        $A.enqueueAction(action);
    },
    handleAPUEvents: function(lstComponentWrapper,apukey2,hasapuCounterRow,count,apuValueList){
        try{
            console.log('IN handleAPUEvents', count);
            if(count == 1){                
                for(let key in lstComponentWrapper){
                    if(lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].apuCounterRow){
                        let apuFHM = lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM; 
                        //let apuFH = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH);
                        let apuFC = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC);
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)] = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].showAPUrow = true;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].serialNumber = '';
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM = apuFHM;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC = apuFC;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].assembly = 'APU Values';
                        break;
                    }
                }
               
               console.log('hasapuCounterRow: ',hasapuCounterRow); 
               if(hasapuCounterRow){
                    let assemblyRow = false;
                    for(let key in lstComponentWrapper){
                        if((!lstComponentWrapper[key].apuCounterRow) && lstComponentWrapper[key].assembly == 'APU'){
                            assemblyRow = true;
                            break;
                        }
                    }
             
                    if(assemblyRow){
                        for(let key in lstComponentWrapper){
                            if( lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].assembly === undefined){
                                console.log('Remove Row: ',lstComponentWrapper[key]);
                                lstComponentWrapper.splice(key,1);
                            }
                        }
                    }
                    else{
                        // Get MR Event for APU H or APU C row
                        console.log('WRAPPER: ',lstComponentWrapper);
                        let apuRow = {};
                        for(let key in lstComponentWrapper){
                            if( lstComponentWrapper[key].asmName == 'APU' && (!lstComponentWrapper[key].apuCounterRow)){
                                apuRow = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                                break;
                            }
                        }
                        console.log('APU ROW: ',apuRow);
                        for(let key in lstComponentWrapper){
                            if( lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].assembly === 'APU'){
                                let FHM = lstComponentWrapper[key].flightHM;
                                let FC = lstComponentWrapper[key].flightC;
                                lstComponentWrapper[key] = apuRow;
                                lstComponentWrapper[key].assembly = 'APU';
                                lstComponentWrapper[key].hideFH = false;
                                lstComponentWrapper[key].flightHM = FHM;
                                lstComponentWrapper[key].flightC = FC;
                                if(lstComponentWrapper[key].flightHM != undefined){
                                    lstComponentWrapper[key].fHUnknown = false;
                                } 
                                if(lstComponentWrapper[key].flightC != undefined){
                                    lstComponentWrapper[key].fCUnknown = false;
                                }
                                lstComponentWrapper[parseInt(apukey2) + parseInt(1)].assembly = 'APU Values';
                                break;
                            }
                        }
                        for(let key in lstComponentWrapper){
                            if( lstComponentWrapper[key].asmName == 'APU' && (!lstComponentWrapper[key].apuCounterRow) && lstComponentWrapper[key].assembly === undefined){
                                console.log('Remove Row: ',lstComponentWrapper[key]);
                                lstComponentWrapper.splice(key,1);
                                break;
                            }
                        }
                    }
                }
                else{
                    for(let key in lstComponentWrapper){
                        let asm = lstComponentWrapper[key];
                        if(asm.asmName == 'APU' && asm.assembly === 'APU'){
                            asm.mrEvent = '';
                            asm.rate = null;
                            asm.rateBasis = '';
                            asm.tsn = null;
                            asm.csn = null;
                            asm.billed = null;
                            asm.balance = null;
                            asm.units = null;
                            asm.paid = null;
                            asm.hideCommentField = false;
                        }
                    }
                }
            }
            else if(count == 2){
                for(let key in lstComponentWrapper){
                    if(lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].apuCounterRow){
                        lstComponentWrapper[key].firstCounterRow = true;
                        let apuFHM = lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM;
                        //let apuFH = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH);
                        let apuFC = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC);
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)] = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].showAPUrow = true;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].serialNumber = '';
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].assembly = 'APU Values';
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM = apuFHM;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC = apuFC;
                        //lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH = Number(apuFH);
                        break;
                    }
                }
                var apuRow = {};
                for(let key in lstComponentWrapper){
                    if(lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].apuCounterRow && (!lstComponentWrapper[key].firstCounterRow)){
                        apuRow = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                        apuRow.showAPUrow = false;
                        apuRow.serialNumber = '';
                        apuRow.assembly = '';
                        apuRow.hideAPUInputs = true;
                        break;
                    }
                }
                
                lstComponentWrapper.splice(parseInt(apukey2) + parseInt(2), 0, apuRow);
                for(let key in lstComponentWrapper){
                    let asm = lstComponentWrapper[key];
                        if(asm.asmName == 'APU' && asm.assembly === 'APU'){
                            asm.mrEvent = '';
                            asm.rate = null;
                            asm.rateBasis = '';
                            asm.tsn = null;
                            asm.csn = null;
                            asm.billed = null;
                            asm.balance = null;
                            asm.units = null;
                            asm.paid = null;
                            asm.hideCommentField = false;
                        }
                        console.log(lstComponentWrapper[key].asmName);
                        console.log(lstComponentWrapper[key].assembly);
                    if(lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].assembly === undefined){
                        console.log('Delete: ',lstComponentWrapper[key]);
                        lstComponentWrapper.splice(key,1);
                    }
                }
            }
        }
        catch(e){
            console.log(e);
        }
    },
    saveTrueUpUtilizationRecord: function (component, helper, Id, searchData, assetIndex, trueUpIndex, record) {
        //component.set("v.showBilling", true);
        //component.set("v.showInvoice", true);
        var redirectToOpen = false;
        var UtilId = Id;
        var astIndex = assetIndex;
        var trueUpIndex = trueUpIndex;
        helper.showSpinner(component, event, helper);
        var action = component.get("c.saveFHdata");
        action.setParams({
            recordsToSave: JSON.stringify(record)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                let breakLoop = false;
                console.log('result:', result);
                if (result != undefined && result != '' && result != null) {
                    for (let key in searchData.operatorData) {
                        for (let key1 in searchData.operatorData[key].assetData) {
                            if (searchData.operatorData[key].assetData[key1].trueUpWrapperList != undefined) {
                                var length = searchData.operatorData[key].assetData[key1].trueUpWrapperList.length - 1;
                                for (let key2 in searchData.operatorData[key].assetData[key1].trueUpWrapperList) {
                                    //console.log(record.utilization.Id, searchData.operatorData[key].assetData[key1].Asset.Id);
                                    if (record.utilization.Id != undefined && record.utilization.Id != null) {
                                        if (result.Asset.Id == searchData.operatorData[key].assetData[key1].Asset.Id) {
                                            if (record.utilization.Id != undefined && record.utilization.Id != null) {
                                                if (result.utilization.Id == searchData.operatorData[key].assetData[key1].utilization.Id) {
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList.splice(parseInt(length), 1, result);
                                                    for(let key3 in searchData.operatorData[key].assetData[key1].trueUpWrapperList[length].lstComponentWrapper){
                                                        let cw = searchData.operatorData[key].assetData[key1].trueUpWrapperList[length].lstComponentWrapper[key3];
                                                        if(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBilled === undefined){
                                                            searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBilled = 0;
                                                        }
                                                        if(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBalance === undefined){
                                                            searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBalance = 0;
                                                        }
                                                        if(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalPaid === undefined){
                                                            searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalPaid = 0;
                                                        }
                                                        searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBilled += cw.billed === undefined ? 0 : parseInt(cw.billed);
                                                        searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBalance += parseInt(cw.balance === undefined ? 0 : parseInt(cw.balance));
                                                        searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalPaid += parseInt(cw.paid === undefined ? 0 : parseInt(cw.paid));                                                
                                                        
                                                    }
                                                    this.handleAPUOnApprove(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)]);
                                                    breakLoop = true;
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        if (result.Asset.Id == searchData.operatorData[key].assetData[key1].Asset.Id) {
                                            console.log('In Else Block');
                                            searchData.operatorData[key].assetData[key1].trueUpWrapperList.splice(parseInt(length), 1, result);
                                            for(let key3 in searchData.operatorData[key].assetData[key1].trueUpWrapperList[length].lstComponentWrapper){
                                                let cw = searchData.operatorData[key].assetData[key1].trueUpWrapperList[length].lstComponentWrapper[key3];
                                                if(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBilled === undefined){
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBilled = 0;
                                                }
                                                if(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBalance === undefined){
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBalance = 0;
                                                }
                                                if(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalPaid === undefined){
                                                    searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalPaid = 0;
                                                }
                                                searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBilled += cw.billed === undefined ? 0 : parseInt(cw.billed);
                                                searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalBalance += parseInt(cw.balance === undefined ? 0 : parseInt(cw.balance));
                                                searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)].totalPaid += parseInt(cw.paid === undefined ? 0 : parseInt(cw.paid));                                                
                                                
                                            }
                                            this.handleAPUOnApprove(searchData.operatorData[key].assetData[key1].trueUpWrapperList[parseInt(length)]);
                                            breakLoop = true;
                                            break;
                                        }
                                    }
                                }
                                if (breakLoop) {
                                    break;
                                }
                            }
                        }
                        if (breakLoop) {
                            break;
                        }
                    }
                    //searchData.operatorData = this.handleAPUValues(searchData.operatorData);
                    component.set("v.searchData", searchData);
                    console.log('SearchData at End', component.get("v.searchData"));
                 /* this.setTableStructure(component, event, helper, 'resetChild');*/
                    setTimeout(function () {
                        component.set("v.showTableRendering", false);
                        component.set("v.showTableRendering", true);
                        helper.hideSpinner(component, event, helper);
                    }, 500);
                }
            } else if (state === "ERROR") {
                helper.hideSpinner(component, event, helper);
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        })
        $A.enqueueAction(action);
    },
    handleAPUValues: function (allDataList) {
        for (let key in allDataList) {
            for (let key1 in allDataList[key].assetData) {
                console.log('TrueUp List: ', allDataList[key].assetData[key1].trueUpWrapperList);
                if (allDataList[key].assetData[key1].trueUpWrapperList != []) {
                    for (let key2 in allDataList[key].assetData[key1].trueUpWrapperList) {
                        var apukey = 0;
                        var apukey1 = 0;
                        var apukey2 = 0;
                        var apukey3 = 0;
                        var hasapu = false;
                        var apuFHM;
                        var apuFC;
                        var apuFH;
                        var nextassembly = false;
                        var apuTSN;
                        var finalapukey;
                        var apuCSN;
                        var hasapuCounterRow = false;
                        var count = 0;
                        var apuValueList = allDataList[key].assetData[key1].trueUpWrapperList[key2].apuValues.split(';');
                        for (let key3 in allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper) {
                            if(allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].asmName == 'APU'){
                                if(!allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].apuCounterRow){
                                    hasapuCounterRow = true;
                                }
                                else{
                                    count++;
                                }
                            }
                            if (hasapu && allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].assembly == undefined) {
                                apukey3++;
                                console.log('apu key3', apukey3);
                            }
                            var assemblyData = allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3];
                            console.log('assemblyData: ', assemblyData);
                            if (nextassembly && assemblyData.assembly != undefined) {
                                finalapukey = parseInt(apukey3);
                                console.log('finalapukey: ', finalapukey);
                                nextassembly = false;
                            }
                            if (assemblyData.assembly != undefined && assemblyData.assembly === 'APU') {
                                nextassembly = true;
                                apuFHM = assemblyData.apuFHM;
                                apuFC = assemblyData.apuFC;
                                //apuFH = assemblyData.apuFH;
                                if (apuFHM == '' || apuFHM == undefined) {
                                    apuFHM = assemblyData.apuFH;
                                }
                                apuTSN = assemblyData.apuTSN;
                                apuCSN = assemblyData.apuCSN;
                                console.log(assemblyData.apuFHM, assemblyData.apuFC, assemblyData.apuTSN, assemblyData.apuCSN);
                                hasapu = true;
                                apukey = key;
                                apukey1 = key1;
                                apukey2 = parseInt(key2);
                                apukey3 = parseInt(key3);
                                var derateRowObj = {
                                    'assembly': 'APU Values',
                                    'showAPUrow': true,
                                    'apuFH': null,
                                    'apuFHM': '',
                                    'apuFC': null
                                }
                            }
                        }
                        if (hasapu) {
                            console.log('apu key2', apukey2);
                            allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper.splice(parseInt(finalapukey) + parseInt(1), 0, derateRowObj);
                            allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuFHM = apuFHM;
                            allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuFC = apuFC;
                            //allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3)+parseInt(1)].apuFH = apuFH;
                            allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuTSN = apuTSN;
                            allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuCSN = apuCSN;
                            //console.log('TRUE UP VALUES: ',allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2]);
                            if(allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].apuValues !='')
                                this.handleAPUEvents(allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper,apukey3,hasapuCounterRow,count,apuValueList);
                        }
                        console.log('Updated Values: ', allDataList);
                    }
                }
            }
        }

        return allDataList;
    },
    getBulkButtonStatus: function (component, helper, searchData) {
        var allDataList = searchData.operatorData;
        component.set("v.showSaveAll", false);
        let openButtonCheck = false;
        let submitButtonCheck = false;
        let approveButtonCheck = false;
        for (let key in allDataList) {
            for (let key1 in allDataList[key].assetData) {
                if (allDataList[key].assetData[key1].utilization.Status__c == 'Open' || allDataList[key].assetData[key1].utilization.Status__c == 'New') {
                    openButtonCheck = true;
                    component.set("v.showSaveAll", true);
                    component.set("v.approveAllUtilization", false);
                    component.set("v.sendInvoices", false);
                    if (component.get("v.isCreateNew")) {
                        component.set("v.submitForApproval", false);
                    } else {
                        component.set("v.submitForApproval", true);
                    }
                } else {
                    if (!openButtonCheck) {
                        if (allDataList[key].assetData[key1].utilization.Status__c == 'Submitted To Lessor') {
                            if (!openButtonCheck) {
                                submitButtonCheck = true;
                                component.set("v.submitForApproval", false);
                                component.set("v.sendInvoices", false);
                                component.set("v.approveAllUtilization", true);
                            }
                        } else {
                            if (allDataList[key].assetData[key1].utilization.Status__c == 'Approved By Lessor') {
                                if (!submitButtonCheck) {
                                    approveButtonCheck = true;
                                    component.set("v.approveAllUtilization", false);
                                    component.set("v.submitForApproval", false);
                                    component.set("v.sendInvoices", true);
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    validateTrueUpValues: function(component, event, helper){
        try{
            var result = '';
            if(event.currentTarget == null || event.currentTarget == undefined){
                result = component.get("v.trueUpAttributes");
            }
            else{
                result = event.currentTarget.value;
            }
            component.set("v.showReconciliationModal", false);
        
        var Id = result.split('-')[0];
        var operatorIndex = result.split('-')[1];
        var assetIndex = result.split('-')[2];
        var trueUpIndex = result.split('-')[3];
        
        console.log(Id, operatorIndex, assetIndex,trueUpIndex);
        var searchData = component.get("v.searchData");
        var record = {};
        var assemblyRecord;
        var hasEmptyValues = true;
       // var checkcondition = false;
        if (Id == '') {
            record = searchData.operatorData[operatorIndex].assetData[assetIndex].trueUpWrapperList[trueUpIndex];
            console.log('Record: ',record);
            for (let key2 in searchData.operatorData[operatorIndex].assetData[assetIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper) {
                assemblyRecord = searchData.operatorData[operatorIndex].assetData[assetIndex].trueUpWrapperList[trueUpIndex].lstComponentWrapper[key2];
                console.log('Assembly Record: ',assemblyRecord);
                if(assemblyRecord.assembly != "APU Values"){
                    if(assemblyRecord.flightHM != null || assemblyRecord.flightC != null) {
                        hasEmptyValues = false;
                    }
                }
            }
            if (record.totalFHM === '' && record.totalFC === undefined) {
                hasEmptyValues = true;
                for (let key2 in record.lstComponentWrapper) {
                    assemblyRecord = record.lstComponentWrapper[key2];
                    console.log(assemblyRecord.flightHM, assemblyRecord.flightC);
                    if(assemblyRec.assembly != "APU Values"){
                        if((assemblyRecord.flightHM != null || assemblyRecord.flightC != null) && assemblyRecord.flightHM != '') {
                            hasEmptyValues = false;
                        }
                    }
                }
            }
        }
        else {
            record = helper.handleTrueUpFHValues(Id, searchData, record, hasEmptyValues, assemblyRecord, helper);
            console.log('Record: ',record);
            if (record.totalFHM === '' && record.totalFC === undefined) {
                hasEmptyValues = true;
                for (let key2 in record.lstComponentWrapper) {
                    assemblyRecord = record.lstComponentWrapper[key2];
                    if(assemblyRecord.assembly == "APU Values"){
                        if(assemblyRecord.apuFC == "" || assemblyRecord.apuFC == null || assemblyRecord.apuFC == undefined){
                           assemblyRecord.apuFC = 0; 
                        }
                    }
                    console.log(assemblyRecord.flightHM, assemblyRecord.flightC);
                    if(assemblyRecord.assembly != "APU Values" && assemblyRecord.assembly != undefined){
                        if((assemblyRecord.flightHM != null || assemblyRecord.flightC != null) && assemblyRecord.flightHM != '') {
                            hasEmptyValues = false;
                        }
                    }
                }
            }
            else{
                for (let key2 in record.lstComponentWrapper) {
                    assemblyRecord = record.lstComponentWrapper[key2];
                    if(assemblyRecord.assembly == "APU Values"){
                        if(assemblyRecord.apuFC == "" || assemblyRecord.apuFC == null || assemblyRecord.apuFC == undefined){
                           assemblyRecord.apuFC = 0; 
                        }
                    }
                    console.log(assemblyRecord.flightHM, assemblyRecord.flightC);
                    if(assemblyRecord.assembly != "APU Values" && assemblyRecord.assembly != undefined){
                        if((assemblyRecord.flightHM != null || assemblyRecord.flightC != null) && assemblyRecord.flightHM != '') {
                            hasEmptyValues = false;
                        }
                    }
                }
            }
        }

        if (hasEmptyValues) {
            helper.handleErrors('Please enter FH/FC values for the primary Assembly: Airframe for the Aircraft and Engine 1 for the Engine Asset. Other Assemblies may remain unknown, but confirm Unknown by clicking on the field');
        }
        else {
            if(record.totalFH === ''){
                record.totalFH = null;
            }
            if(record.totalFC === ''){
                record.totalFC = null;
            }
            for(let key in record.lstComponentWrapper){
                var assemblyRec = record.lstComponentWrapper[key];
                if(assemblyRec.assembly == "APU Values"){
                    if(assemblyRec.apuFC == "" || assemblyRec.apuFC == null || assemblyRec.apuFC == undefined){
                        assemblyRec.apuFC = 0; 
                    }
                }
                if(assemblyRec.assembly != "APU Values" && assemblyRec.assembly != undefined){
                    if(assemblyRec.flightHM === ''){
                        hasEmptyValues =true;
                    }
                    if(assemblyRec.flightC === undefined){
                        hasEmptyValues =true;
                    }
                } 
            }
            if (hasEmptyValues) {
                helper.handleErrors('Please enter FH/FC values for the primary Assembly: Airframe for the Aircraft and Engine 1 for the Engine Asset. Other Assemblies may remain unknown, but confirm Unknown by clicking on the field');
            }
            else{
                console.log('record : ', record);
                if(record.invoice != undefined && record.invoice != '' && record.invoice != null){
                    record.invoice = null;   
                }
                // Save record if all FH/FC values are filled.
                helper.saveTrueUpUtilizationRecord(component, helper, Id, searchData, assetIndex,trueUpIndex, record);
            }
        }  
        }
        catch(e){
            console.log(e);
        }
    },
    //Show the error toast when any error occured
    handleErrors: function (errors, type) {
        this.handleErrorMessage(errors, type);
    },
    handleErrorMessage: function (errormsg, type) {
        var _self = this;
        console.log('handleErrorMessage');
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errormsg === 'object') {
            errormsg.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    error.message = _self.parseErrorMessage(error.message);
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });
                    };
                }
            });
        } else if (typeof errormsg === "string" && errormsg.length != 0) {
            toastParams.message = errormsg;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    },
    parseErrorMessage: function (errormsg) {
        var errormessage;
        var finalErrorMsg;
        var msgList;
        // Error handler if error is coming from trigger
        if (errormsg.includes("FIELD_CUSTOM_VALIDATION_EXCEPTION")) {
            var message = errormsg;
            msgList = message.split('FIELD_CUSTOM_VALIDATION_EXCEPTION');
            /*
		Output: msgList[length-1] = , You cannot modify MR Rates once the Lease is Approved.: [Lease__c]
	
										Class.URLITriggerHandler.updateMRInfos: line 355, column 1
										Class.URLITriggerHandler.afterInsert: line 286, column 1
										Class.TriggerFactory.execute: line 73, column 1
										Class.TriggerFactory.createHandler: line 27, column 1
										Trigger.URLITrigger: line 3, column 1: []: []
		*/

            errormsg = this.getErrorType(msgList[msgList.length - 1], finalErrorMsg, errormessage, true);
            console.log('Error: ', errormsg);
        } else {
            errormsg = this.getErrorType(errormsg, finalErrorMsg, errormessage, false);;
        }
        return errormsg;
    },
    getErrorType: function (errorMsg, finalErrorMsg, errormessage, hasFieldCustom) {
        if (errorMsg.includes('LeaseWareException')) {
            finalErrorMsg = this.getErrors(errorMsg, 'LeaseWareException', errormessage);
        } else {
            if (errorMsg.includes('QueryException')) {
                finalErrorMsg = this.getErrors(errorMsg, 'QueryException', errormessage);
            } else {
                if (errorMsg.includes("NullPointerException")) {
                    finalErrorMsg = this.getErrors(errorMsg, 'NullPointerException', errormessage);
                } else {
                    if (errorMsg.includes("StringException")) {
                        finalErrorMsg = this.getErrors(errorMsg, 'StringException', errormessage);
                    } else {
                        if (errorMsg.includes("MathException")) {
                            finalErrorMsg = this.getErrors(errorMsg, 'MathException', errormessage);
                        } else {
                            if (errorMsg.includes("ListException")) {
                                finalErrorMsg = this.getErrors(errorMsg, 'ListException', errormessage);
                            } else {
                                if (hasFieldCustom) {
                                    if (errorMsg.includes('Class.')) {
                                        errormessage = errorMsg.substr(0, errorMsg.indexOf('Class.')); // Output: , You cannot modify MR Rates once the Lease is Approved. [Lease__c]
                                    } else {
                                        errormessage = errorMsg;
                                    }
                                    finalErrorMsg = errormessage.substr(0, errormessage.indexOf(']') + 1).substr(1); // Output: You cannot modify MR Rates once the Lease is Approved.: [Lease__c]
                                } else {
                                    finalErrorMsg = errorMsg;
                                }
                            }
                        }
                    }
                }
            }
        }
        return finalErrorMsg;
    },
    getErrors: function (message, exceptionType, errormessage) {
        var messagesList;
        var classNameWithMsg;
        if (message.includes('Class.')) {
            messagesList = message.split('Class.');
            classNameWithMsg = messagesList[1];
            errormessage = message.substr(0, message.indexOf('Class.'));
            if (exceptionType == 'NullPointerException') {
                errormessage = errormessage.substr(errormessage.indexOf(exceptionType)) + classNameWithMsg + '. Please contact LeaseWorks Support.';
            } else {
                errormessage = errormessage.substr(errormessage.indexOf(exceptionType));
            }
        } else {
            errormessage = message.substr(message.indexOf(exceptionType));
        }
        return errormessage;
    },
    handleAPUOnApprove : function(approvalRecord){
        var apukey2 = 0;
        var hasapu = false;
        var apuFHM;
        var apuFH;
        var apuFC;
        var apuTSN;
        var apuCSN;
        var hasapuCounterRow = false;
        var count = 0;
        var apuValueList = approvalRecord.apuValues.split(';');
        for (let key2 in approvalRecord.lstComponentWrapper) {
            if(approvalRecord.lstComponentWrapper[key2].asmName == 'APU'){
                if(!approvalRecord.lstComponentWrapper[key2].apuCounterRow){
                    hasapuCounterRow = true;
                }
                else{
                    count++;
                }
            }
            if (hasapu && approvalRecord.lstComponentWrapper[key2].assembly == undefined) {
                apukey2++;
            }
            var assemblyData = approvalRecord.lstComponentWrapper[key2];
            if (assemblyData.flightHM === "" || assemblyData.flightHM === undefined) {
                assemblyData.flightHM = null;
                assemblyData.fHUnknown = true;
            }
            if (assemblyData.flightC === "" || assemblyData.flightC === undefined) {
                assemblyData.flightC = null;
                assemblyData.fCUnknown = true;
            }
            console.log('assemblyData: ', assemblyData.assembly);
            if (assemblyData.assembly != undefined && assemblyData.assembly === 'APU') {
                apuFHM = assemblyData.apuFHM;
                apuFH = assemblyData.apuFH;
                apuFC = assemblyData.apuFC;
                apuTSN = assemblyData.apuTSN;
                apuCSN = assemblyData.apuCSN;
                hasapu = true;
                apukey2 = parseInt(key2);
                var derateRowObj = {
                    'assembly': 'APU Values',
                    'showAPUrow': true,
                    'apuFH': null,
                    'apuFHM': '',
                    'apuFC': null
                }
                }
            
        }
        if (hasapu) {
            approvalRecord.lstComponentWrapper.splice(parseInt(apukey2) + parseInt(1), 0, derateRowObj);
            approvalRecord.lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM = apuFHM;
            approvalRecord.lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC = apuFC;
            approvalRecord.lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH = apuFH;
            approvalRecord.lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuTSN = apuTSN;
            approvalRecord.lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuCSN = apuCSN;
            if(approvalRecord.apuValues !='')
                this.handleAPUEvents(approvalRecord.lstComponentWrapper,apukey2,hasapuCounterRow,count,apuValueList);
        }
    }
})