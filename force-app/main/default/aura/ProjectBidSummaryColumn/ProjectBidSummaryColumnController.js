({
	doInit : function(component, event, helper) {
        
        let record = component.get("v.record");
        let field = component.get("v.field");
        
        if(!$A.util.isEmpty(record) && !$A.util.isEmpty(field)){
            let cmp ;
        	let attributes = new Object();
        	let fieldAPIName = field.apiName;
            
            if(field.type === 'REFERENCE'){
                
                var relationShipName = '';
                if(fieldAPIName.indexOf('__c') == -1) {
                    relationShipName = fieldAPIName.substring(0, fieldAPIName.indexOf('Id'));
                }
                else {
                    relationShipName = fieldAPIName.substring(0, fieldAPIName.indexOf('__c')) + '__r';
                }
                
                if(record[relationShipName] != undefined){
                	attributes['label']  = record[relationShipName].Name;    
                    attributes['title']  = record[relationShipName].Name;  
                    attributes['value'] = '/' + record[fieldAPIName];
                }

            } else{
                attributes['value'] = record[fieldAPIName]; 
                attributes['title'] = record[fieldAPIName];
            }

            
            //create component
            
            if(field.type == 'DATE' && attributes['value'] != undefined){
                cmp = 'lightning:formattedDateTime';
                attributes['year'] = "numeric";
                attributes['month'] = "numeric";
                attributes['day'] = "numeric";
            }else if(field.type == 'DATETIME' && attributes['value'] != undefined){
                cmp = 'lightning:formattedDateTime';
                attributes['hour'] = "2-digit";
                attributes['minute'] = "2-digit";
                attributes['year'] = "numeric";
                attributes['month'] = "numeric";
                attributes['day'] = "numeric";
            }else if(field.type == 'CURRENCY' && attributes['value'] != undefined){
                cmp = 'lightning:formattedNumber';
                attributes['style'] = "currency";
                attributes['maximumFractionDigits'] = field.scale;
                attributes['minimumFractionDigits'] = 0;
            }else if(field.type == 'DOUBLE' && attributes['value'] != undefined){
                cmp = 'lightning:formattedNumber';
                attributes['maximumFractionDigits'] = field.scale;
                attributes['minimumFractionDigits'] = 0;
            }else if(field.type == 'PERCENT' && attributes['value'] != undefined){
                cmp = 'lightning:formattedNumber';
                attributes['style'] = "percent";
                attributes['value'] = attributes['value']/100;
                attributes['maximumFractionDigits'] = field.scale;
                attributes['minimumFractionDigits'] = 0;
            }else if(field.type == 'BOOLEAN'){
                cmp = 'ui:outputCheckbox';
            }else if(field.type === 'REFERENCE' && attributes['value'] != undefined){
                cmp = 'ui:outputURL';
                attributes['target'] = "_blank";
            }else{
                cmp = 'lightning:formattedText';
            }
             $A.createComponent(
                cmp,
                attributes,
                function(outoupField, status, errorMessage){
                    //Add the new button to the body array
                    if (status === "SUCCESS") {
                        let body = component.get("v.body");
                        body.push(outoupField);
                        component.set("v.body", body);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.")
                    }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                    }
                }
            );
        }
    }
})