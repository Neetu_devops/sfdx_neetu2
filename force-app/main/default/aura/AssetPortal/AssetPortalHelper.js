({
    getNameSpacePref : function(component) {
        var self = this;
        var action = component.get('c.getNameSpacePrefix');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getNameSpacePref : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getNameSpacePref values " +allValues);
                component.set("v.namespace", allValues);
                self.getAirtypeVariantPicklist(component);
            }
            else if (state === "ERROR") {    
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getNameSpacePref errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getNameSpacePref unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
        
        });
        $A.enqueueAction(action);
    },
  
	getRequest : function(component){
        console.log('getRequest ');
        var self = this;
        let data = component.get("v.requestData");
        let wrapperData = {};
        if(data && data.Asset_Variant__c && data.Asset_Variant__c == '--None--') {
            data.Asset_Variant__c = null;
        }
        wrapperData.assetRequest = data;
        console.log("requestWrapper: "+JSON.stringify(wrapperData));
        
        var action = component.get('c.getAssetRequest');
        action.setParams({
        	searchRequest: JSON.stringify(wrapperData)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getRequest : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getRequest values " +JSON.stringify(allValues));
                if(allValues && allValues.length > 0) {
                    component.set("v.assetRequestData", allValues);
                    //component.set("v.assetRequestFilteredData", allValues);
                    self.excludeConflictEngine(component);
                    component.set("v.showRequestResult", true);
                }
                else {
                    //show no data
                    self.showErrorMsg(component, "There are no Matching Assets with the requested Asset Type on the requested dates. Please modify the Date Start and Date Finish with Lease Term Tolerance or change the Asset Type");
                }
            }
            else if (state === "ERROR") {    
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getRequest errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getRequest unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action); 
    },
   
    saveRequest : function(component, selectedList){
        console.log('saveRequest '+JSON.stringify(selectedList));
        
        let data = component.get("v.requestData");
        let wrapperData = {};
        wrapperData.assetRequest = data;
        //console.log("requestWrapper: "+JSON.stringify(wrapperData));
        
        var self = this;
        var action = component.get('c.saveRequestData');
        action.setParams({
            saveRequest: JSON.stringify(selectedList),
            requestData: JSON.stringify(wrapperData)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("saveRequest : state "+state);
            component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null) {
                    console.log("saveRequest response "+JSON.stringify(allValues));
                    let msg = allValues.message;
                    if(msg) {
                        let code = allValues.code;
                        if(code && code == 200) {
                            self.showSuccessMsg(component, msg);
                            let id = allValues.Id;
                            if(id) {
                                self.launchRecordPage(component, id);
                            }
                        }
                        if(code && code == 400) {
                            self.showErrorMsg(component, msg);
                        }
                    }
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("saveRequest errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("saveRequest unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action); 
    },

    
    excludeConflictEngine: function(component) {
        let originalDataSet = component.get("v.assetRequestData");
        console.log('excludeConflictEngine originalDataSet ', originalDataSet);
        if(originalDataSet) {
            let filterData = [], j = 0;
            for(let i = 0; i < originalDataSet.length; i++) {
                if(originalDataSet[i].IsPreRequested == false) {
                    filterData[j] = originalDataSet[i];
                    j++;
                }
            }
            component.set("v.assetRequestDataExcludedConfEng", filterData);
            component.set("v.assetRequestFilteredData", filterData);
            console.log('excludeConflictEngine filterData ', filterData);
        }
    },
    
    launchRecordPage : function(component, recordId) {
        var navLink = component.find("navLink");
        var pageRef = {
            type: 'standard__recordPage',
            attributes: {
                actionName: 'view',
                objectApiName: 'Portal_Asset_Request__c',
                recordId : recordId 
            },
        };
        navLink.navigate(pageRef, true);
    },

    isFromDateValid: function(component) {
        var fromDate = component.get("v.requestData.Date_Start__c");
        var fromDateObj = new Date(fromDate).setHours(0,0,0,0);
        var today = new Date();
        today.setHours(0,0,0,0);
        if ( fromDateObj < today) { 
            this.showErrorMsg(component, "Date Start should be later than today");
            return false;
        }
            return true;
    },

    isToleranceValid: function(component) {
        let tolerance = component.get("v.requestData.Lease_Term_Tolerance__c");
        if(tolerance && (tolerance < 0 )) { 
            this.showErrorMsg(component, "Please check, lease term tolerance value should be positive number");
            return false;
        }
        return true;
    },

    getRentPerPicklist: function(component) {
        component.set("v.isLoading", true);
        console.log('getRentPerPicklist');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Portal_Asset_Request__c',
            "fld": 'Rent_Up_To__c'
        });
        action.setCallback(this, function(response) {
            console.log('getRentPerPicklist state: ',response.getState() );
            var state = response.getState();
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                var opts = [];
                for(var i=0;i< allValues.length; i++) {
                    if(i == 0)
                        component.set("v.requestData.Rent_Up_To__c", allValues[0]);
                    opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                }
                component.find("rentPerId").set("v.options", opts);
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getRentPerPicklist errors "+errors);
                if(errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getRentPerPicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            this.getSecurityTypePicklist(component); 
        });
        $A.enqueueAction(action); 
    },
    
    getSecurityTypePicklist: function(component) {
        component.set("v.isLoading", true);
        console.log('getSecurityTypePicklist');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Portal_Asset_Request__c',
            "fld": 'Security_Deposit_Type__c'
        });
        action.setCallback(this, function(response) {
            console.log('getSecurityTypePicklist state: ',response.getState() );
            var state = response.getState();
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                var opts = [];
                for(var i=0;i< allValues.length; i++) {
                	if( i == 0) {
                        component.set("v.requestData.Security_Deposit_Type__c", allValues[0]);
                        if(allValues[i] == 'Cash') 
                    	    component.set("v.disableSecurityAmt", false);
                    }
                    opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                }
                component.find("securityDeposit").set("v.options", opts);
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getSecurityTypePicklist errors "+errors);
                if(errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getSecurityTypePicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            this.getMaintancePicklist(component);
        });
        $A.enqueueAction(action); 
    },
    
    getMaintancePicklist: function(component) {
        component.set("v.isLoading", true);
        console.log('getMaintancePicklist');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Portal_Asset_Request__c',
            "fld": 'Maintenance__c'
        });
        action.setCallback(this, function(response) {
            console.log('getMaintancePicklist state: ',response.getState() );
            var state = response.getState();
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                var opts = [];
                for(var i=0;i< allValues.length; i++) {
                    if(i == 0)
                        component.set("v.requestData.Maintenance__c", allValues[0]);
                    opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                }
                component.find("maintenance").set("v.options", opts);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getMaintancePicklist errors "+errors);
                if(errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getMaintancePicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action); 
    },
    
    
    getAirtypeVariantPicklist: function(component) {
        console.log('getAirtypeVariantPicklist');
        var controllerField = component.get("v.controllingFieldAPI");
        var dependentField = component.get("v.dependingFieldAPI");
        var objDetails = component.get("v.objDetail");
        var self = this;
        var action = component.get("c.getDependentMap");
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField 
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set("v.fieldDependencMap",StoreResponse);
                //console.log('getAirtypeVariantPicklist '+JSON.stringify(StoreResponse));
            } else if (state === "ERROR") {    
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getAirtypeVariantPicklist errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getAirtypeVariantPicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            self.getVariantPicklist(component);
        });
        $A.enqueueAction(action);
    },
    
    getVariantPicklist: function(component) {
        console.log('getVariantPicklist');
        
        var dependentPicklistValues=[];
        component.set("v.dependentPicklistValues",dependentPicklistValues);
        component.set("v.requestData.Asset_Type__c", null);
        
        var self = this;
        var action = component.get("c.getPicklistBasedOnRecordType");
        action.setParams({
            "objectName": "Aircraft__c",
            "fieldName": "Aircraft_Variant__c",
            "recordTypeName": component.get("v.recordType")
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getVariantPicklist state: ',response.getState() );
            var state = response.getState();
            if (response.getState() == "SUCCESS") {
                component.set("v.dependentPicklistValuesCache", response.getReturnValue());
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getVariantPicklist errors "+errors);
                if(errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getVariantPicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            self.getAssetTypePicklist(component);
        });
        $A.enqueueAction(action); 
    },
    
    getAssetTypePicklist: function(component) {
        console.log('getAssetTypePicklist');
        var self = this;
        var action = component.get("c.getPicklistBasedOnRecordType");
        action.setParams({
            "objectName": "Aircraft__c",
            "fieldName": "Aircraft_Type__c",
            "recordTypeName": component.get("v.recordType")
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getAssetTypePicklist state: ',response.getState() );
            var state = response.getState();
            if (response.getState() == "SUCCESS") {
                var data = response.getReturnValue();
                component.set("v.controllingPicklistValues", data);
                if(data) {
                    component.set("v.requestData.Asset_Type__c", data[0]);
                }
                self.populateDependentPicklist(component);
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getAssetTypePicklist errors "+errors);
                if(errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getAssetTypePicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            self.getRentPerPicklist(component);
        });
        $A.enqueueAction(action); 
    },
    
    populateDependentPicklist: function(component) {
        console.log('populateDependentPicklist' );
        
        var availablePicklistValuesCacheSet = [];
        availablePicklistValuesCacheSet = component.get("v.dependentPicklistValuesCache");
        var dependencyMap = component.get("v.fieldDependencMap");
        var dependentPicklistValues = [];
        var controllingValue = component.get("v.requestData.Asset_Type__c");
        let isAdded = false;
        if(dependencyMap && controllingValue) {
            var availablePicklistValuesMapSet = dependencyMap[controllingValue];
            if(availablePicklistValuesCacheSet && availablePicklistValuesCacheSet.length>0 && availablePicklistValuesMapSet) {
                for(var i=0; i<availablePicklistValuesCacheSet.length; i++) { 
                    if(availablePicklistValuesMapSet.includes(availablePicklistValuesCacheSet[i])) {
                        if(isAdded == false) {
                            dependentPicklistValues.push('--None--');
                            isAdded = true;
                        }
                        dependentPicklistValues.push(availablePicklistValuesCacheSet[i]);
                    }
                }
            }
        }
        /* if(availablePicklistValuesCacheSet && availablePicklistValuesCacheSet.length > 0) {
            component.set("v.requestData.Asset_Variant__c", availablePicklistValuesCacheSet[0]);
        }
        else {
            component.set("v.requestData.Asset_Variant__c", null);
        } */
        component.set("v.requestData.Asset_Variant__c", null);
        //console.log("populateDependentPicklist "+dependentPicklistValues);
        component.set("v.dependentPicklistValues",dependentPicklistValues);
    },
    

    calculateLeaseTerm: function (component) {
        var fromDate = component.get("v.requestData.Date_Start__c");
        var toDate = component.get("v.requestData.Date_End__c");
        var tolerance = component.get("v.requestData.Lease_Term_Tolerance__c");
        console.log("calculateLeaseTerm " + fromDate + " - "+ toDate + " - "+ tolerance);
        if(fromDate) {
            var flag = this.isFromDateValid(component);
            if(!flag)
                return;
        }
        if(fromDate && toDate) {
            var fromTime = new Date(fromDate).getTime();
            var toTime = new Date(toDate).getTime(); 
            
            if(fromTime > toTime) {
                this.showErrorMsg(component, "Date Finish should be later than Date Start");
				return;
            }
            var difftime = toTime - fromTime; 
            const oneDay = 24 * 60 * 60 * 1000; 
            var numOfDays = difftime / oneDay; 
            console.log('calculateLeaseTerm numOfDays: '+numOfDays);
            var leaseTerm = (numOfDays ) / 30;
            component.set("v.leaseTerm" , leaseTerm);
        }
    },
    
    calculateTotalAmt: function(component) {
        var rentPer = component.get("v.requestData.Rent_Up_To__c");
        var rent = component.get("v.requestData.Rent_Up_To_Amount__c");
        var monthlyFH = component.get("v.requestData.Assumed_Monthly_FH__c");
        var monthlyFC = component.get("v.requestData.Assumed_Monthly_FC__c");
        var leaseTerm = component.get("v.leaseTerm");
        var contractAmt = 0;
        console.log('calculateTotalAmt: '+rentPer+ ' - '+rent+ ' - '+monthlyFH + ' - '+monthlyFC + ' - '+leaseTerm);
        component.set("v.totalContractAmt" , 0);

        if(rentPer == 'FH' && !monthlyFH) {
            this.showInfo(component, "Please enter Assumed Monthly Flight Hours to calculate the Total Rental Amount");
            return;
        }
        else if(rentPer == 'FC' && !monthlyFC) {
            this.showInfo(component, "Please enter Assumed Monthly Flight Cycles to calculate the Total Rental Amount");
            return;
        }
        if(leaseTerm && !rent) {
            this.showInfo(component, "Please enter Rent Up To for Total Rental Amount calculation");
            return;
        }
        
        var fromDate = component.get("v.requestData.Date_Start__c");
        var toDate = component.get("v.requestData.Date_End__c");
        if(!fromDate && !toDate) {
            this.showInfo(component, "Please enter Date Start and Date Finish to calculate the Total Rental Amount");
            return;
        }

        if(leaseTerm && rent) {
            leaseTerm = leaseTerm.toFixed(1);
            rent = rent.toFixed(2);
            if(rentPer == 'MO') {
                contractAmt = leaseTerm * rent;
            }
            else if(rentPer == 'FH') {
                if(monthlyFH) {
                    monthlyFH = monthlyFH.toFixed(0);
                    contractAmt = leaseTerm * rent * monthlyFH;
                }
                else{
                    return;
                }
            }
            else if(rentPer == 'FC') {
                if(monthlyFC) {
                    monthlyFC = monthlyFC.toFixed(0);
                    contractAmt = leaseTerm * rent * monthlyFC;
                }
                else{
                    return;
                }
            }
            component.set("v.totalContractAmt" , contractAmt);
        }
    },

    showInfo : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Info',
            message: msg,
            duration:' 5000',
            key: 'info_alt',
            type: 'info',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    showSuccessMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Success!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('error '+ toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('error '+ toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('error '+ toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
})