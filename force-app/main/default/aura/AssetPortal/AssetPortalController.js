({
    doInit : function(component, event, helper) {
        component.set("v.isLoading", true);
        let requestObj = new Object();
        component.set("v.requestData", requestObj);
        helper.getNameSpacePref(component);
        //    helper.getRequest(component);
    },
    
    onSearchClick: function(component, event, helper) { 
        
        let requestData = component.get("v.requestData");
        console.log("onSearchClick requestData: "+JSON.stringify(requestData));

        if(requestData.Asset_Type__c && requestData.Date_Start__c && requestData.Date_End__c) {
            var fromTime = new Date(requestData.Date_Start__c).getTime();
            var toTime = new Date(requestData.Date_End__c).getTime(); 
            
            var flag = helper.isFromDateValid(component);
            if(!flag)
                return;
            
            if(fromTime > toTime) {
                helper.showErrorMsg(component, "Date Finish should be later than Date Start");
				return;
            }
            if(requestData.Rent_Up_To__c == 'FH') {
                if(!requestData.Assumed_Monthly_FH__c) {
                    helper.showErrorMsg(component, "Please enter Assumed Monthly Flight Hours to calculate the Total Rental Amount");
                    return;
                }
            }
            else if(requestData.Rent_Up_To__c == 'FC') {
                if(!requestData.Assumed_Monthly_FC__c) {
                    helper.showErrorMsg(component, "Please enter Assumed Monthly Flight Cycles to calculate the Total Rental Amount");
                    return;
                }
            }
            if(helper.isToleranceValid(component) == false) {
                return;
            }
            component.set("v.isLoading", true);
            helper.getRequest(component);
        }
        else {
            helper.showErrorMsg(component, "Please check the required fields completeness.");
        }
    },
    
    onSecurityTypeChange: function(component, event, helper) {     
    	var type = component.get("v.requestData.Security_Deposit_Type__c");
        console.log("onSecurityTypeChange: "+type);
        if(type == 'Cash') {
            component.set("v.disableSecurityAmt", false);
        }
        else {
            component.set("v.disableSecurityAmt", true);
        }
    },
    
    onAircraftTypeChange: function(component, event, helper) {     
        var controllerValueKey = component.get("v.requestData.Asset_Type__c");
        console.log("onAircraftTypeChange "+controllerValueKey );
        helper.populateDependentPicklist(component);
        
    },
    
    onDataChange: function(component, event, helper) {
        helper.calculateLeaseTerm(component);
        helper.calculateTotalAmt(component);
    },

    onToleranceChange : function(component, event, helper) {
        helper.isToleranceValid(component);
    },

    onRequestFilter: function(component, event, helper) {
        var isSpecSheet = component.get("v.isSpecSheet");
        var isAfterSV = component.get("v.isAfterSV");
        var isLifeRemaining = component.get("v.isLifeRemaining");
        var isPreRequest = component.get("v.isPreRequest");

        console.log("onRequestFilter "+isSpecSheet+ " - "+isAfterSV+ " - "+isLifeRemaining + " - "+isPreRequest);
        let assetData = component.get("v.assetRequestData");
        let assetRequestDataExcludedConfEng = component.get("v.assetRequestDataExcludedConfEng");
        if(!assetData)
            return;
        //when no filter is selected show all the data without conflict engine
        if(!isSpecSheet && !isAfterSV && !isLifeRemaining && !isPreRequest) {
            component.set("v.assetRequestFilteredData", assetRequestDataExcludedConfEng);
        }
        else {
            //when only show conflict selected show all the data with conflict engine
            if(isPreRequest && !isSpecSheet && !isAfterSV && !isLifeRemaining) {
                component.set("v.assetRequestFilteredData", assetData);
                return;
            }
            if(!isPreRequest) { //filter is off so exclude the conflict engine
                assetData = assetRequestDataExcludedConfEng;
            }
            let filtercount = 0;
            if(isSpecSheet)
                filtercount++;
            if(isAfterSV)
                filtercount++;
            if(isLifeRemaining)
                filtercount++;
            console.log('Total filter selected ',filtercount);
            let filterData = [];
            let j = 0;
            for(let i = 0; i < assetData.length; i++) {
                let filtercountLocal = filtercount;
                //console.log("onRequestFilter assetData: "+JSON.stringify(assetData[i]));
                if(isSpecSheet == true && assetData[i].HasSpecSheet == true) {
                    filtercountLocal--;
                }
                if(isAfterSV == true && assetData[i].IsAfterSV == true) {
                    filtercountLocal--;
                }
                if(isLifeRemaining == true && assetData[i].HasMoreThanHalfLifeFC == true) {
                    filtercountLocal--;
                }
                if(filtercountLocal == 0) {
                    filterData[j] = assetData[i];
                    j++;
                }
            }
            console.log("filterData "+JSON.stringify(filterData));
            component.set("v.assetRequestFilteredData", filterData);
        }
    },

    onRequestClick: function(component, event, helper) {
        var data = component.get("v.assetRequestFilteredData");   
        component.set("v.isLoading", true);
        console.log("onRequestClick data: "+JSON.stringify(data));
        if(!data)
            return;
        let index = 0;
        var selectedList = [];
        for(let i = 0; i < data.length; i++) {
            if(data[i].isSelected && data[i].isSelected == true) {
                selectedList[index] = data[i];
                index++;
            }
        }
        if(index == 0) {
            component.set("v.isLoading", false);     
            helper.showErrorMsg(component, "Please select record to add the request");
        } 
        else {
            helper.saveRequest(component, selectedList);
        }

    },

    onCancel: function(component, event, helper) {
        var navLink = component.find("navLink");
        var pageRef = {
            type: 'standard__objectPage',
            attributes: {
                actionName: 'list',
                objectApiName: 'Portal_Asset_Request__c'
            },
        };
        navLink.navigate(pageRef, true);

    }
    
})