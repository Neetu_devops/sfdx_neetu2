({
    
    init: function(component, event, helper) {
        
        component.set("v.showSpinner", true);
        
        var action = component.get("c.getData");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data =  response.getReturnValue();
                component.set("v.data", data);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                   helper.raiseErrors(errors);
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
        
    },
 
    //created separate methods because child click event fires and it return Id as null.
    proceedWithBilling : function(component, event, helper) { 
		component.set("v.type", 'Billing');
    },
    
	proceedWithFleetPlanning : function(component, event, helper) {
		component.set("v.type", 'FleetPlanning');
	},
    
    handleErrors : function(component, event, helper) {
        var errors = event.getParam("errors");
        helper.raiseErrors(errors);
    },
})