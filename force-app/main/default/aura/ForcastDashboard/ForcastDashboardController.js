({
    doInit: function (component, event, helper) {
        helper.InitializeData(component, event, helper);
    },
    reloadData: function (component, event, helper) {
        $A.util.removeClass(component.find("reloadConfirmationBox"), "slds-hide");
    },
    reload: function (component, event, helper) {
        var addressableContext;
        var result;
        component.set("v.leaseIdOnLoad", "");
        const urlString = window.location.href;
        var url = new URL(urlString);
        var context = url.searchParams.get("inContextOfRef");
        
        if (context != null && context != undefined && context != '') {
            // For some reason, the string starts with "1.", if somebody knows why,
            // this solution could be better generalized.
            if (context.startsWith("1\.")) {
                context = context.substring(2);
            }
            addressableContext = JSON.parse(window.atob(context));
            result = addressableContext.attributes.recordId;
        }
        console.log('context: ',addressableContext);
        console.log('result: ',result);
        if (result != undefined && result != null && result != '') {
            if (addressableContext.attributes.objectApiName === 'Lease__c' || addressableContext.attributes.relationshipApiName === 'Lease_CashFlow__r') {
                helper.resetValues(component, event);
                component.set("v.leaseIdOnLoad", addressableContext.attributes.recordId);
                helper.getAircraftId(component, event, addressableContext.attributes.recordId);
            }
            else if (addressableContext.attributes.objectApiName === 'Aircraft__c' || addressableContext.attributes.relationshipApiName === "Scenario_Input__r") {
                helper.resetValues(component, event);
                component.set("v.aircraftId", addressableContext.attributes.recordId);
            }
        }
    },
    updateReturnConditionValues: function (component, event, helper) {
        console.log(event.getSource().get("v.name"));
        var technicalSectionRecords = component.get("v.technicalSectionRecords");
        var indexToUpdate = event.getSource().get("v.name");
        let record = technicalSectionRecords[indexToUpdate.split('#')[2]].assemblyEventInput[indexToUpdate.split('#')[3]];
        if(record.rc != undefined && record.rc.Return_Percentage__c != undefined && record.rc.Return_Percentage__c){
            if(indexToUpdate.split('#')[4] == 'FH'){
             	if(record.rc.RC_FH_Per__c != undefined && record.rc.RC_FH_Per__c !='' && record.rc.RC_FH_Per__c != null)
            		record.assemblyEventInp.RC_FH__c = (event.getSource().get("v.value") * (record.rc.RC_FH_Per__c/100)).toFixed(2);   
            }
            if(indexToUpdate.split('#')[4] == 'FC'){
             	if(record.rc.RC_FC_Per__c != undefined && record.rc.RC_FC_Per__c !='' && record.rc.RC_FC_Per__c != null)
            		record.assemblyEventInp.RC_FC__c = (event.getSource().get("v.value") * (record.rc.RC_FC_Per__c/100)).toFixed(2);   
            }
            if(indexToUpdate.split('#')[4] == 'Month'){
             	if(record.rc.RC_Months_Per__c != undefined && record.rc.RC_Months_Per__c !='' && record.rc.RC_Months_Per__c != null)
            		record.assemblyEventInp.RC_Months__c = (event.getSource().get("v.value") * (record.rc.RC_Months_Per__c/100)).toFixed(2);   
            }
        }
        technicalSectionRecords[indexToUpdate.split('#')[2]].assemblyEventInput[indexToUpdate.split('#')[3]] = record;
        component.set("v.technicalSectionRecords",technicalSectionRecords);
    },
    proceedToRestoreData : function(component, event, helper) {
        $A.util.addClass(component.find("reloadConfirmationBox"), "slds-hide");
        helper.showSpinner(component);
        var action = component.get("c.restoreAssemblyEventInputData");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            
            helper.hideSpinner(component);
            
            var state = response.getState();
            if(component.isValid && state === 'SUCCESS') {
                helper.generateForecast(component, event, helper);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    closeReloadConfirmationModal : function(component, event, helper) {
        $A.util.addClass(component.find("reloadConfirmationBox"), "slds-hide");
    },
    goBackToRecord: function(component, event, helper) {
        let recId = '';
        if(component.get("v.recId") != undefined && component.get("v.recId") != ''){
            recId = component.get("v.recId");
        }
        if(component.get("v.scenarioInputRecord").Id != undefined && component.get("v.scenarioInputRecord").Id != ''){
            recId = component.get("v.scenarioInputRecord").Id;
        }
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId,
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    onPicklistChangeRcType: function(component, event, helper) {
        // get the value of select option
        var selectedRcTypeValue = component.find("rcType").get("v.value");
        var scenarioInp = component.get("v.scenarioInputRecord");
        scenarioInp.RC_Type__c = selectedRcTypeValue;
        component.set("v.scenarioInputRecord",scenarioInp);
    },
    onPicklistChangeType: function(component, event, helper) {
        // get the value of select option
        var reserveType = component.find("reserveType").get("v.value");
        var scenarioInp = component.get("v.scenarioInputRecord");
        scenarioInp.Ext_Reserve_Type__c = reserveType;
        component.set("v.scenarioInputRecord",scenarioInp);
    },
    onPicklistChangeCost : function(component, event, helper){
        var scenarioInp = component.get("v.scenarioInputRecord");
        var costAssumption = component.find("costAssumption").get("v.value");
        //console.log('costAssumption'+costAssumption);
        if(costAssumption != undefined || costAssumption != ''){ 
            scenarioInp.Cost_Assumption__c = costAssumption;
            component.set("v.scenarioInputRecord",scenarioInp);    
        }
    },
    aircraftChange: function(component, evt, helper) {
        //console.log("aircraftId has changed!!!");
        //console.log("old value: " + evt.getParam("oldValue"));
        //console.log("current value: " + evt.getParam("value"));
        if( evt.getParam("oldValue") == '' && evt.getParam("value") != null && evt.getParam("value") != '' ) {
            helper.showSpinner(component);
            var scenarioRecId = component.get("v.scenarioInputRecord").Id;
            helper.removeSectionValues(component, event);
            component.set("v.scenarioInputRecord", {
                'sobjectType': 'Scenario_Input__c',
                'Asset__c': '{}',
                Number_of_Months_Of_History_To_Use__c: -12,
                Number_Of_Months_For_Projection__c: 12,
                Operating_Environment_Impacts_Airframe__c: false,
                RC_Type__c: '',
                Cost_Assumption__c: ''
            });
            //console.log('>>>>>>>>>>>>> scenario >>>>>>>>> ',component.get("v.scenarioInputRecord"));
            helper.updateRecordsForSelectedAircraft(component,event);
        }
        else {
            helper.updateRecordsForSelectedAircraft(component,event);    
        }
    },
     leaseChange: function (component, evt, helper) {
        let leaseId = component.get("v.leaseId");
        console.log('SCINP RECORD: ',component.get("v.scenarioInputRecord"));
        if (component.get("v.scenarioInputRecord.Asset__c") === undefined) {
            component.set("v.aircraftId", undefined);
        }
        if (leaseId != undefined && leaseId != '' && leaseId != null) {
            let scInpRecord = component.get("v.scenarioInputRecord");
            let aircraft = scInpRecord.Asset__c;
            console.log('LEASE CHANGE: ', aircraft);
            if (aircraft === undefined || aircraft === null || aircraft === '' || aircraft === '{}') {
                console.log('Return Aircraft Id: ', scInpRecord.Lease__r.Aircraft__c);
                component.set("v.aircraftId", scInpRecord.Lease__r.Aircraft__c);
                scInpRecord.Lease__c = leaseId;
                console.log('scInpRecord: ', JSON.parse(JSON.stringify(scInpRecord)));
                component.set("v.scenarioInputRecord", scInpRecord);
            }
            else {
                scInpRecord.Lease__c = leaseId;
                component.set("v.scenarioInputRecord", scInpRecord);
            }
            
        }
    },

    getLookUpValues: function (component, event, helper) {
        var id = component.get("v.aircraftId");
        let fieldName = event.getParam("fieldName");
        let val = event.getParam("recordByEvent");
        let objectAPIName = event.getParam("objectAPIName");
        console.log('Obj API Name: ', objectAPIName);
        if (objectAPIName == 'Aircraft__c') {
            if (val.Id === undefined || val.Id === '' || val.Id === null) {
                component.set("v.scenarioInputRecord.Asset__r", undefined);
                component.set("v.scenarioInputRecord.Asset__c", undefined);
                component.set("v.scenarioInputRecord.Lease__r", undefined);
                component.set("v.scenarioInputRecord.Lease__c", undefined);
                helper.removeSectionValues(component, event);
            }
            var aircrafrtId = val.Id;
            component.set("v.aircraftId", aircrafrtId);
        } else if (objectAPIName == 'Ratio_Table__c') {
            console.log('Selected Ratio_Table__c', JSON.stringify(val));
        } else if (objectAPIName == 'Lease__c') {
            console.log('Value: ', val);
            if (val.Id === undefined || val.Id === '' || val.Id === null) {
                component.set("v.scenarioInputRecord.Lease__r", undefined);
                component.set("v.scenarioInputRecord.Lease__c", undefined);
                helper.removeSectionValues(component, event);
                //component.set("v.scenarioInputRecord.Asset__c", undefined);
                //component.set("v.scenarioInputRecord.Asset__r", undefined);
            }
            else {
                component.set("v.scenarioInputRecord.Lease__r", val);
                if (component.get("v.scenarioInputRecord.Asset__c") != '{}' && component.get("v.scenarioInputRecord.Asset__c") != undefined) {
                    helper.createSectionFlyingHours(component, event, component.get("v.scenarioInputRecord.Asset__c"), val.Id);
                    helper.loadSectionValues(component, event);
                }
            }
            component.set("v.leaseId", val.Id);
        }
    },
    openModal : function(component, event, helper) {
        var asset = component.get("v.scenarioInputRecord").Asset__c;
        //helper.llpcheckbox(component, event, helper);
        let lease = component.get("v.scenarioInputRecord").Lease__c;
        if (lease === undefined || lease === '' || lease === null || lease === '{}') {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please Select Lease."
            });
            toastEvent.fire();
        }else{
            if(event.target.dataset.id != undefined ) {
                var targetId = event.target.dataset.id.split("_");
                if(event.target.dataset.id.length > 0){
                    if(component.get("v."+targetId[1]) == true){
                        $A.util.removeClass(component.find(targetId[0] + "Modal"), 'hideDiv');
                        $A.util.removeClass(component.find("backdrop"), 'hideDiv');
                    }
                    if(targetId[2] == "reset"){
                        component.set("v."+targetId[1], false);
                        $A.util.addClass(component.find(targetId[0]), 'boxDiv');
                        if(targetId[0] == "FHCO"){
                            $A.util.removeClass(component.find(targetId[0]), 'FHCOAfterSave');
                            
                        }else{
                            $A.util.removeClass(component.find(targetId[0]), 'boxDivAfterSave');
                            $A.util.addClass(component.find(targetId[0]+"_BoxTitle"), 'boxTitle');
                            $A.util.removeClass(component.find(targetId[0]+"_BoxTitle"), 'boxTitleAfterSave');
                        }
                    } else if(component.get("v."+targetId[1]) == false){  
                        $A.util.removeClass(component.find(targetId[0] + "Modal"), 'hideDiv');
                        $A.util.removeClass(component.find("backdrop"), 'hideDiv');
                    }
                }
                if(targetId[0] == 'TimeSpan'){
                    helper.setScenarioForProjection(component,event);
                }else{
                    helper.setScenarioValue(component,event); 
                } 
            }
        }
        helper.storeCloneAssemblyRecords(component, event, helper);
    },
    closeModal : function(component, event, helper) {
        let ScenarioInp = component.get("v.scenarioInputRecord").Id;
        let modalIdArray = (event.getSource().getLocalId()).split("-");
        console.log('modalIdArray: ', modalIdArray);
        let modalId = modalIdArray[0];
        console.log('ModalId: ',modalId);
        
        $A.util.addClass(component.find(modalId), 'hideDiv');
        $A.util.addClass(component.find("backdrop"), 'hideDiv');
        if(modalId == 'CustomErrorModal'){
            component.set("v.assemblyWarningList",[]);
            component.set("v.assemblyErrorList",[]);
            if(component.get("v.hasAtleastOneError") == false && component.get("v.hasAtleastOneWarning") == true){
                if(modalIdArray[2] != undefined && modalIdArray[2] == 'CrossButton'){
                    //Do not redirect. Case when user click on cross button.
                }else if(modalIdArray[2] != undefined && modalIdArray[2] == 'OK'){
                    //Redirect. Case when user click on OK button.
                	helper.updateFields(component,event,ScenarioInp);    
                }
                
            }
            component.set("v.hasAtleastOneError",false);            
            component.set("v.hasAtleastOneWarning",false);
        }
    },
    modalSave : function(component, event, helper){
        if(component.get("v.isStartDateCorrect")){
            var targetString = event.getSource().getLocalId().split("-");
            if(targetString[0] != 'OperationalEnvironment'){
                $A.util.addClass(component.find(targetString[0]+"Modal"), 'hideDiv');
                $A.util.addClass(component.find("backdrop"), 'hideDiv');
                $A.util.removeClass(component.find(targetString[0]), 'boxDiv');
            }
            
            if(targetString[0] == 'FHCO'){
                $A.util.addClass(component.find(targetString[0]), 'FHCOAfterSave');
            }else{
                $A.util.addClass(component.find(targetString[0]), 'boxDivAfterSave');
                $A.util.addClass(component.find(targetString[0]+"_BoxTitle"), 'boxTitleAfterSave');
                $A.util.removeClass(component.find(targetString[0]+"_BoxTitle"), 'boxTitle');
            }
            component.set("v."+targetString[1], true);
            helper.saveData(component, targetString[0]);    
        } else {
            console.log('component.get("v.isStartDateCorrect")>>>>>>>>',component.get("v.isStartDateCorrect"));
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message":  "Start date for reserve type should be within ("+ component.get("v.minStartDateForReserveType")+ ' - ' + component.get("v.maxStartDateForReserveType")+")",
                type: 'error',
            });
            toastEvent.fire();
        }
        
    },
    resetModalValue: function(component, event, helper){
        if (event.getSource().getLocalId() != undefined && event.getSource().getLocalId().length > 0){
            var targetId = event.getSource().getLocalId().split("-");
            helper.resetValue(component,event,targetId);
        }
    },
    handleClick: function (cmp, event, helper) {
        let lease = cmp.get("v.scenarioInputRecord.Lease__r");
        let asset = cmp.get("v.scenarioInputRecord.Asset__r");
        if (lease === undefined || lease === '' || lease === null || asset === undefined || asset === '' || asset === null) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select Lease and Aircraft to proceed."
            });
            toastEvent.fire();
        }
        else {
            if (cmp.get("v.alreadyGenerated")) {
                $A.util.removeClass(cmp.find("confirmationBox"), "slds-hide");
            } else {
                helper.generateForecast(cmp, event, helper);
            }
        }
    },
    addShopVisit: function(component, event, helper) {
        let button = event.getSource();
        button.set('v.disabled',true);
        var index = event.target.dataset.index;
        var parentid = index.split("-");
        
        component.set("v.svIndex", parentid[4]);
        var assemblyId = '';
        var svNo =0;
        if(parentid[0] == 'Landing Gear'){
            assemblyId = parentid[3];
            svNo = (parentid[6] == '' || parentid[6] == null || parentid[6] == undefined) ? 0 : parentid[6];
        }else{
            assemblyId = parentid[2];
            svNo = (parentid[5] == '' || parentid[5] == null || parentid[5] == undefined) ? 0 : parentid[5];
        }
        var action = component.get("c.addShopVisitRow");
        action.setParams({
            "assemblyEventInputId": assemblyId,
            "svNumber": svNo
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                var addSV = true;
                var scenarioCmpList = component.get("v.technicalSectionRecords");
                for(var i=0; i<scenarioCmpList.length; i++ ){
                    var scenarioCmp = scenarioCmpList[i];
                    if(parentid != undefined && parentid[0] == scenarioCmp.scenarioComponentInp.Name){
                        for(var j = 0; j < scenarioCmp.assemblyEventInput.length; j++){
                            var assemblyEventInput = scenarioCmp.assemblyEventInput[j];
                            var assemblyName = '';
                            if(parentid[0] == 'Landing Gear'){
                                if(parentid[1] != undefined)
                                    assemblyName = parentid[1]+'-'+parentid[2];
                            }else{
                                if(parentid[1] != undefined)
                                    assemblyName = parentid[1];
                            }
                            if(assemblyName != '' && assemblyName==assemblyEventInput.assemblyEventInp.Name){
                                if(addSV == true){
                                    res.svRec.Name = 'SV - '+assemblyEventInput.assemblyEventInp.Name;
                                    assemblyEventInput.shopVisit.lstShopVistWrapper.push(res);
                                    var noOfShopVisitForAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper.length;
                                    var SVname = 'SV - '+assemblyEventInput.assemblyEventInp.Name;
                                    var SVListAsPerAssembly =[];
                                    
                                    //List of all SV Overide of Scenario Component.
                                    for(var k = 0; k < noOfShopVisitForAssembly; k++){
                                        var SvRecAsPerAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper[k];
                                        if (SvRecAsPerAssembly.svRec.Name == SVname) {
                                            SVListAsPerAssembly.push({
                                                svName: SvRecAsPerAssembly.svRec.Name,
                                                svNumber: SvRecAsPerAssembly.svRec.SV_Number__c,
                                                svRecord: SvRecAsPerAssembly
                                            });
                                        }
                                    }
                                    var lengthofSV = SVListAsPerAssembly.length; 
                                    //List of Particular Elements of Assembly Input
                                    for(var k = 0; k < lengthofSV; k++){
                                        if(lengthofSV-1 == k ){
                                            if(SVListAsPerAssembly[k].svRecord != undefined){
                                                SVListAsPerAssembly[k].svRecord.isfirstShopVisit = true;
                                                SVListAsPerAssembly[k].svRecord.isLastShopVisit = true;
                                            }
                                        }else{
                                            if(SVListAsPerAssembly[k].svRecord != undefined){
                                                SVListAsPerAssembly[k].svRecord.isfirstShopVisit = false;
                                                SVListAsPerAssembly[k].svRecord.isLastShopVisit = false;
                                            }
                                        }
                                    }
                                    //Pushing back to the array all the changes 
                                    for(var k = 0; k < noOfShopVisitForAssembly; k++){
                                        var SvRecAsPerAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper[k];
                                        for(var m = 0; m < lengthofSV; m++){
                                            if(SvRecAsPerAssembly.svRec.Name  == SVListAsPerAssembly[m].svName && SvRecAsPerAssembly.svRec  == SVListAsPerAssembly[m].svRecord){
                                                SVListAsPerAssembly[m].svRec.SV_Number__c = SVListAsPerAssembly[m].svRec.SV_Number__c +1;
                                            }
                                        }
                                    }
                                }else{
                                    addSV = false;
                                }
                            }
                        }
                    }
                }
                
                component.set("v.technicalSectionRecords",scenarioCmpList);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    removeShopVisit: function(component, event, helper) { 
        var eventType = event.getSource().get("v.name");
        component.set("v.loadLLPReplacementFor", eventType);
        eventType = eventType.split("####");
        component.set("v.svIndex", eventType[2]);
        
        let indexToRemove = component.get("v.svIndex");
        var index = event.target.dataset.index;
        var parentid = index.split("-");
        var addSV = true;
        var scenarioCmpList = component.get("v.technicalSectionRecords");
        
        for(var i=0; i<scenarioCmpList.length; i++){
            var scenarioCmp = scenarioCmpList[i];
            if(parentid != undefined && parentid[0] == scenarioCmp.scenarioComponentInp.Name){
                for(var j = 0; j < scenarioCmp.assemblyEventInput.length; j++){
                    var assemblyEventInput = scenarioCmp.assemblyEventInput[j];
                    var assemblyName = '';
                    if(parentid[0] == 'Landing Gear'){
                        if(parentid[1] != undefined)
                            assemblyName = parentid[1]+'-'+parentid[2];
                    }else{
                        if(parentid[1] != undefined)
                            assemblyName = parentid[1];
                    }
                    
                    if(assemblyName != '' && assemblyName==assemblyEventInput.assemblyEventInp.Name){
                        if(addSV == true){
                            if(indexToRemove == 0 && assemblyEventInput.shopVisit.lstShopVistWrapper.length == 1) {
                                assemblyEventInput.shopVisit.lstShopVistWrapper[0].svRec.Override_Event_Date__c = undefined;
                                assemblyEventInput.shopVisit.lstShopVistWrapper[0].svRec.Override_Event_Cost__c = undefined;
                            }else{
                                assemblyEventInput.shopVisit.lstShopVistWrapper.pop();    
                            }
                                
                            
                            var noOfShopVisitForAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper.length;
                            var SVname = 'SV - '+assemblyEventInput.assemblyEventInp.Name;
                            var SVListAsPerAssembly =[];
                            
                            //List of all SV Overide of Scenario Component.
                            for(var k = 0; k < noOfShopVisitForAssembly; k++){
                                var SvRecAsPerAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper[k];
                                if (SvRecAsPerAssembly.svRec.Name == SVname) {
                                    SVListAsPerAssembly.push({
                                        svName: SvRecAsPerAssembly.svRec.Name,
                                        svNumber: SvRecAsPerAssembly.svRec.SV_Number__c,
                                        svRecord: SvRecAsPerAssembly
                                    });
                                }
                            }
                            var lengthofSV = SVListAsPerAssembly.length; 
                            if(lengthofSV == 1){
                                if(SVListAsPerAssembly[0].svRecord != undefined){
                                    SVListAsPerAssembly[0].svRecord.isfirstShopVisit = false; 
                                    SVListAsPerAssembly[0].svRecord.isLastShopVisit = true;
                                }   
                            }else{
                                for(var k = 0; k < lengthofSV; k++){
                                    if(lengthofSV-1 == k ){
                                        if(SVListAsPerAssembly[k].svRecord != undefined){
                                            SVListAsPerAssembly[k].svRecord.isfirstShopVisit = true;
                                            SVListAsPerAssembly[k].svRecord.isLastShopVisit = true;
                                        }
                                    }else{
                                        if(SVListAsPerAssembly[k].svRecord != undefined){
                                            SVListAsPerAssembly[k].svRecord.isfirstShopVisit = false;
                                            SVListAsPerAssembly[k].svRecord.isLastShopVisit = false;
                                        }
                                    }
                                }
                            }
                            //List of Particular Elements of Assembly Input
                            
                            //Pushing back to the array all the changes 
                            for(var k = 0; k < noOfShopVisitForAssembly; k++){
                                var SvRecAsPerAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper[k];
                                for(var m = 0; m < lengthofSV; m++){
                                    if(SvRecAsPerAssembly.svRec.Name  == SVListAsPerAssembly[m].svName && SvRecAsPerAssembly.svRec  == SVListAsPerAssembly[m].svRecord){
                                        assemblyEventInput.shopVisit.lstShopVistWrapper[k] = SVListAsPerAssembly[m].svRecord;
                                    }
                                }
                            }
                        }else{
                            addSV = false;
                        }
                    }
                }
            }
        }
        component.set("v.technicalSectionRecords",scenarioCmpList);
    },
    deleteShopVisit: function(component, event, helper) { 
        var eventType = event.getSource().get("v.name");
        component.set("v.loadLLPReplacementFor", eventType);
        eventType = eventType.split("####");
        component.set("v.svIndex", eventType[2]);
        let indexToRemove = component.get("v.svIndex");
        var index = event.target.dataset.index;
        var parentid = index.split("-");
        var svId = '';
        var assemblyId = '';
        var index = '';
        if(parentid[0] == 'Landing Gear'){
            svId = parentid[4] ;
            index= parentid[5] ;
            assemblyId = parentid[3] ;
        }else{
            svId = parentid[3];
            index= parentid[4] ;
            assemblyId = parentid[2] ;
        }
        
        var action = component.get("c.deleteShopVisitRec");
        action.setParams({
            "svId": svId,  
            "assemblyId":assemblyId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                
                if(res == 'Success'){
                    
                    var addSV = true;
                    var scenarioCmpList = component.get("v.technicalSectionRecords");
                    for(var i=0; i<scenarioCmpList.length; i++ ){
                        var scenarioCmp = scenarioCmpList[i];
                        if(parentid != undefined && parentid[0] == scenarioCmp.scenarioComponentInp.Name){
                            for(var j = 0; j < scenarioCmp.assemblyEventInput.length; j++){
                                var assemblyEventInput = scenarioCmp.assemblyEventInput[j];
                                var assemblyName = '';
                                if(parentid[0] == 'Landing Gear'){
                                    if(parentid[1] != undefined)
                                        assemblyName = parentid[1]+'-'+parentid[2];
                                }else{
                                    if(parentid[1] != undefined)
                                        assemblyName = parentid[1];
                                }
                                if(assemblyName != '' && assemblyName==assemblyEventInput.assemblyEventInp.Name){
                                    var noOfShopVisitForAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper.length;

                                    if(indexToRemove > 0 && indexToRemove == noOfShopVisitForAssembly-1){
                                    	assemblyEventInput.shopVisit.lstShopVistWrapper[indexToRemove - 1].isLastShopVisit = true;    
                                    }
                                    var SVname = 'SV - '+assemblyEventInput.assemblyEventInp.Name;
                                    var SVListAsPerAssembly =[];
                                    //List of all SV Overide of Scenario Component.
                                    for(var k = 0; k < noOfShopVisitForAssembly; k++){
                                        if(k == index){
                                            assemblyEventInput.shopVisit.lstShopVistWrapper.splice(k, 1); 
                                        }
                                    } 
                                }
                            }
                        }
                    } 
                    component.set("v.technicalSectionRecords",scenarioCmpList);
                }else{
                    var data = JSON.parse(res);
                    var addSV = true;
                    var scenarioCmpList = component.get("v.technicalSectionRecords");
                    for(var i=0; i<scenarioCmpList.length; i++ ){
                        var scenarioCmp = scenarioCmpList[i];
                        if(parentid != undefined && parentid[0] == scenarioCmp.scenarioComponentInp.Name){
                            for(var j = 0; j < scenarioCmp.assemblyEventInput.length; j++){
                                var assemblyEventInput = scenarioCmp.assemblyEventInput[j];
                                var assemblyName = '';
                                if(parentid[0] == 'Landing Gear'){
                                    if(parentid[1] != undefined)
                                        assemblyName = parentid[1]+'-'+parentid[2];
                                }else{
                                    if(parentid[1] != undefined)
                                        assemblyName = parentid[1];
                                }
                                if(assemblyName != '' && assemblyName==assemblyEventInput.assemblyEventInp.Name){
                                    var noOfShopVisitForAssembly = assemblyEventInput.shopVisit.lstShopVistWrapper.length;
                                    
                                    var SVname = 'SV - '+assemblyEventInput.assemblyEventInp.Name;
                                    var SVListAsPerAssembly =[];
                                    //List of all SV Overide of Scenario Component.
                                    for(var k = 0; k < noOfShopVisitForAssembly; k++){
                                        if(k == index){
                                            assemblyEventInput.shopVisit.lstShopVistWrapper.splice(k, 1); 
                                        }
                                    }
                                    data.svRec.Name = 'SV - '+assemblyEventInput.assemblyEventInp.Name;
                                    assemblyEventInput.shopVisit.lstShopVistWrapper.push(data);
                                }
                            }
                        }
                    } 
                    component.set("v.technicalSectionRecords",scenarioCmpList); 
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    llpReplacementHandler: function(component, event, helper){
        var eventType = event.getSource().get("v.name");
        component.set("v.loadLLPReplacementFor", eventType);
        eventType = eventType.split("####");
        component.set("v.svIndex", eventType[2]);
        
        let lstSCInput = component.get("v.technicalSectionRecords");
        //component.set("v.total_LLP_Cost",0);
        for(let key1 in lstSCInput){
            let scInput = lstSCInput[key1];
            for(let key2 in scInput.assemblyEventInput){
                let aeInput = scInput.assemblyEventInput[key2];
                if(aeInput.assemblyEventInp.Name === eventType[0]){
                    var index = 0;
                    for(let key3 in aeInput.shopVisit.lstShopVistWrapper){
                        let svRecord = aeInput.shopVisit.lstShopVistWrapper[key3];
                        if(index == component.get("v.svIndex")){
                            if(svRecord.svRec.LLP_Replacement_Ids__c == undefined || svRecord.svRec.LLP_Replacement_Ids__c == undefined){
                                component.set("v.total_LLP_Cost",0);
                                component.set("v.selectedLLPs", []);
                            }
                            else{
                                component.set("v.total_LLP_Cost", svRecord.svRec.Override_Event_Cost__c);
                             	component.set("v.selectedLLPs", svRecord.svRec.LLP_Replacement_Ids__c);
                            }
                        }
                        index++;
                    }
                }
            }
        }
                
        if(eventType[0] === "Engine 1 LLP Replacement"){ eventType = 'Engine 1'; }
        else if (eventType[0] === "Engine 2 LLP Replacement"){ eventType = 'Engine 2'; }
        
        let frontDrop = component.find("LLPReplacementTableFrontdrop");
        let backDrop = component.find("LLPReplacementTableBackdrop");
        $A.util.removeClass(frontDrop, "slds-hide");
        $A.util.removeClass(backDrop, "slds-hide");
        
        let LLPReplacement = component.find("LLPReplacement");
        if(LLPReplacement){
            LLPReplacement.destroy();
        }
        
        $A.createComponent(
            "c:LLPReplacementTable",
            {
                "aura:id": "LLPReplacement",
                "recordId": component.get("v.scenarioInputRecord.Asset__c"),
                "engineType" : eventType,
                "selectedLLPs": component.getReference("v.selectedLLPs"),
                "total_LLP_Cost": component.getReference("v.total_LLP_Cost"),
                "closeLLPReplacementTableModal" : component.getReference("v.closeLLPReplacementTableModal")

            },
            function(elemt, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('LLPReplacementTableDiv');
                    var body = targetCmp.get("v.body");
                    body.push(elemt);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },
    closePopup: function(component, event, helper) {
        let frontDrop = component.find("LLPReplacementTableFrontdrop");
        let backDrop = component.find("LLPReplacementTableBackdrop");
        $A.util.addClass(frontDrop, "slds-hide");
        $A.util.addClass(backDrop, "slds-hide");
    },
    closeConfirmationModal : function(component, event, helper) { 
        var box = component.find("confirmationBox");
        $A.util.addClass(box, "slds-hide");
    },
    proceedToGenerateForecast : function(component, event, helper) { 
        $A.util.addClass(component.find("confirmationBox"), "slds-hide");
        helper.generateForecast(component, event, helper);
    },
    handleExtensionChange: function(component, event, helper) {
        let scenarioInputRecord = component.get("v.scenarioInputRecord");
        let technicalSectionRecords = component.get("v.technicalSectionRecords");
        for(let key in technicalSectionRecords){
            if(technicalSectionRecords[key].cloneScenarioComponentInp != undefined && technicalSectionRecords[key].cloneScenarioComponentInp != ''){
                technicalSectionRecords[key].cloneScenarioComponentInp.New_Extension_Assumptions__c = scenarioInputRecord.Extension_Assumption__c;
            }
        }
        let RentExtensionInputRecords = component.get("v.RentExtensionInputRecords");
        var scenarioInpRecord = component.get("v.scenarioInputRecord").Extension_Assumption__c;
        if(scenarioInpRecord == true &&  RentExtensionInputRecords.length == 0){
            RentExtensionInputRecords.push({
                From_Date__c: null,
                Rent_Amount__c: null,
                To_Date__c: null
            });
            component.set("v.RentExtensionInputRecords", RentExtensionInputRecords);
        }
        console.log(JSON.parse(JSON.stringify(scenarioInputRecord)),JSON.parse(JSON.stringify(technicalSectionRecords)));
        component.set("v.technicalSectionRecords",technicalSectionRecords);
        component.set("v.scenarioInputRecord",scenarioInputRecord);
    },
    handleLLPChange : function(component, event, helper) {
        let frontDrop = component.find("LLPReplacementTableFrontdrop");
        let backDrop = component.find("LLPReplacementTableBackdrop");
        $A.util.addClass(frontDrop, "slds-hide");
        $A.util.addClass(backDrop, "slds-hide");
        
        let selectedLLPs = component.get("v.selectedLLPs");
        let eventType = component.get("v.loadLLPReplacementFor").split("####");
        let lstSCInput = component.get("v.technicalSectionRecords");
        
        if(selectedLLPs != undefined && selectedLLPs != ''){
            for(let key1 in lstSCInput){
                let scInput = lstSCInput[key1];
                for(let key2 in scInput.assemblyEventInput){
                    let aeInput = scInput.assemblyEventInput[key2];
                    if(aeInput.assemblyEventInp.Name === eventType[0]){
                        var index = 0;
                        for(let key3 in aeInput.shopVisit.lstShopVistWrapper){
                            let svRecord = aeInput.shopVisit.lstShopVistWrapper[key3];
                            if(component.get("v.svIndex")  == index){
                                svRecord.svRec['LLP_Replacement_Ids__c'] = selectedLLPs;
                                svRecord.svRec['Override_Event_Cost__c'] = component.get("v.total_LLP_Cost");
                            }
                            index++;
                        }
                    }
                }
            }
        }
        component.set("v.technicalSectionRecords",lstSCInput);
    },
    addNewRowHandler: function(component, event, helper){
        var assemblyName = event.getSource().get("v.name");
        let lstSCInput = component.get("v.technicalSectionRecords");
        let cloneAEInputRecords = component.get("v.cloneAEInputRecords");
        
        for(let key1 in lstSCInput){
            let scInput = lstSCInput[key1];
            if(scInput.scenarioComponentInp.Name === assemblyName){
                for(let key2 in cloneAEInputRecords){
                    if(cloneAEInputRecords[key2].assemblyName === assemblyName){
                        for(let key3 in cloneAEInputRecords[key2].caeInput){
                            let caeInput = cloneAEInputRecords[key2].caeInput[key3];
                            scInput.cloneAssemblyEventInput.push({
                                Fx_Assembly_Input__c: caeInput.Fx_Assembly_Input__c,
                                Fx_Assembly_Input__r: caeInput.Fx_Assembly_Input__r,
                                Event_Type__c: caeInput.Event_Type__c,
                                attributes: caeInput.attributes,
                                Name: caeInput.Name,
                                Assembly__c: caeInput.Assembly__c
                            });
                        }
                    }
                }
            }
        }
        component.set("v.technicalSectionRecords", lstSCInput);
    },
    addNewRentRowHandler: function(component, event, helper){
        let RentExtensionInputRecords = component.get("v.RentExtensionInputRecords");
        if(RentExtensionInputRecords.length > 0){
        for(var i=1;i<RentExtensionInputRecords.length;i++){
            var rentToDate1 = RentExtensionInputRecords[i-1].To_Date__c;
            var rentFromDate1 = RentExtensionInputRecords[i].From_Date__c;
            if(new Date(rentFromDate1) < new Date(rentToDate1) ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": 'From Date must be greater than previous To Date ',
                    type: 'error'
                });
                toastEvent.fire();
                return;
            }
        }
        var rentFromDate = RentExtensionInputRecords[RentExtensionInputRecords.length - 1].From_Date__c;
        var rentToDate = RentExtensionInputRecords[RentExtensionInputRecords.length - 1].To_Date__c;
        if(new Date(rentFromDate) > new Date(rentToDate)){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": 'To Date must be greater than From Date',
                type: 'error'
            });
            toastEvent.fire();
        }
        else{
            var date = new Date(rentToDate);
            var date2 = $A.localizationService.formatDate(date.setDate(date.getDate() + 1), "yyyy-MM-DD");
            if(rentToDate != undefined){
                RentExtensionInputRecords.push({
                    From_Date__c: date2,
                    Rent_Amount__c: null,
                    To_Date__c: null
                });
                component.set("v.RentExtensionInputRecords", RentExtensionInputRecords);
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": 'To Date is Required',
                    type: 'error'
                });
                toastEvent.fire();
            }
        }
        }
        else{
            RentExtensionInputRecords.push({
                    From_Date__c: null,
                    Rent_Amount__c: null,
                    To_Date__c: null
                });
                component.set("v.RentExtensionInputRecords", RentExtensionInputRecords);
        }
    },
    removeRow: function(component, event, helper) {
        var RentList = component.get("v.RentExtensionInputRecords");
        var prerentExtToDelete = component.get("v.RentExtToDelete");
        //Get the target object
        var index = event.getSource().get("v.value");
        if("Id" in RentList[index]){
            var rentExtToDelete = RentList[index];
            prerentExtToDelete.push(RentList[index]);
            component.set("v.RentExtToDelete",prerentExtToDelete);
        }
        RentList.splice(index, 1);
        component.set("v.RentExtensionInputRecords", RentList);
    },
    removeRowAirframe : function(component, event, helper){
        var eventType = event.getSource().get("v.value");
        eventType = eventType.split("####");
        
        var technicalSectionRecords = component.get("v.technicalSectionRecords");
        var tsrindex = 0;
        for(let key1 in technicalSectionRecords){
            let tsr = technicalSectionRecords[key1];
            var caeindex = 0;
            for(let key2 in tsr.cloneAssemblyEventInput){
                if(tsrindex == eventType[0] && caeindex == eventType[1]){
                    var AEInputRecordsToDelete = component.get("v.AEInputRecordsToDelete");
                    if("Id" in tsr.cloneAssemblyEventInput[caeindex]){
                        AEInputRecordsToDelete.push(tsr.cloneAssemblyEventInput[caeindex]);
                        component.set("v.AEInputRecordsToDelete",AEInputRecordsToDelete);
                    }
                    tsr.cloneAssemblyEventInput.splice(caeindex, 1);    
                }
                caeindex++;
            } 
            tsrindex++;
        }
        component.set("v.technicalSectionRecords",technicalSectionRecords);
    },
    validateStartDate : function(component, event, helper){
        component.set("v.isStartDateCorrect",true);
        let startDateForReserveType = new Date(event.getParam("value"));
        let lastUtilizationDate;
        if(component.get("v.scenarioInputRecord.Asset__r.Utilization_Current_As_Of__c") != undefined) {
              lastUtilizationDate = new Date(component.get("v.scenarioInputRecord").Asset__r.Utilization_Current_As_Of__c);
          } else {
              lastUtilizationDate = new Date(component.get("v.scenarioInputRecord.Lease__r.Lease_Start_Date_New__c"));
          }
        let noOfMonthForProjection = component.get("v.scenarioInputRecord").Number_Of_Months_For_Projection__c;
        let maxDateRange = new Date(component.get("v.scenarioInputRecord.Lease__r.Lease_End_Date_New__c"));
        maxDateRange.setMonth(maxDateRange.getMonth() + noOfMonthForProjection);
        
        if((startDateForReserveType < lastUtilizationDate || startDateForReserveType > maxDateRange)) {
            component.set("v.isStartDateCorrect",false);
            component.set("v.minStartDateForReserveType",$A.localizationService.formatDate(lastUtilizationDate, "MM/DD/YYYY"));
            component.set("v.maxStartDateForReserveType", $A.localizationService.formatDate(maxDateRange, "MM/DD/YYYY"));
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Start date for reserve type should be within ("+$A.localizationService.formatDate(lastUtilizationDate, "MM/DD/YYYY") + ' - ' + $A.localizationService.formatDate(maxDateRange, "MM/DD/YYYY")+")",
                type: 'error',
            });
            toastEvent.fire();
        }
    },
    closeModalLLP : function(component, event, helper){
        console.log('closeModalLLP');
    	let frontDrop = component.find("LLPReplacementTableFrontdrop");
        let backDrop = component.find("LLPReplacementTableBackdrop");
        $A.util.addClass(frontDrop, "slds-hide");
        $A.util.addClass(backDrop, "slds-hide");   
        component.set("v.closeLLPReplacementTableModal", false);
    },
    navigateToRecord : function(component, event, helper){
        let recordId = event.getSource().get("v.id");
        console.log('recordId: ',recordId);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})