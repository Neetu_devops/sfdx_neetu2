({
    InitializeData : function(component, event, helper) {
        var recID = component.get("v.recordId");
        if(recID == undefined){
            //console.log('recID'+recID);
            //To get asset ID
            var pageRef = component.get("v.pageReference");
            var state = pageRef.state; // state holds any query params
            var base64Context = state.inContextOfRef;
            //console.log('base64Context'+base64Context);
            if(base64Context != undefined){
                // For some reason, the string starts with "1.", if somebody knows why,
                // this solution could be better generalized.
                if (base64Context.startsWith("1\.")) {
                    base64Context = base64Context.substring(2);
                }
                var addressableContext = JSON.parse(window.atob(base64Context));
                console.log('addressableContext: ', addressableContext);
                if (addressableContext.attributes.objectApiName === 'Lease__c' || addressableContext.attributes.relationshipApiName === 'Lease_CashFlow__r') {
                    component.set("v.leaseIdOnLoad", addressableContext.attributes.recordId);
                    this.getAircraftId(component, event, addressableContext.attributes.recordId);
                }
                else if (addressableContext.attributes.objectApiName === 'Aircraft__c' || addressableContext.attributes.relationshipApiName === "Scenario_Input__r") {
                    component.set("v.aircraftId", addressableContext.attributes.recordId);
                }
            }
        } else {
            //to check if forcase already created or not.
            var action = component.get("c.isForcastAlreadyGenerated");
            action.setParams({
                "forcastId": recID
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid && state === 'SUCCESS') {
                    component.set("v.alreadyGenerated", response.getReturnValue());
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        this.handleErrors(component, errors);
                    }
                }
            });
            $A.enqueueAction(action);
        }
        
        document.title = "Forecast Dashboard";
        //get field labels 
        var action = component.get("c.getAllFieldLabels");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid && state === 'SUCCESS') {
                component.set("v.fieldLabels", JSON.parse(response.getReturnValue()));
                this.getRecordsOnLoad(component, event );
            }
            else if (state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    getAircraftId: function (component, event, recordId) {
        var action = component.get("c.getAircraft");
        action.setParams({
            leaseId: recordId
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                component.set("v.aircraftId", response.getReturnValue());
            }
            else if (state == 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveData: function (component, modalId) {
        component.set("v.openedModalId", modalId);
        this.redirectToSection(component);
    },
    getRecordsOnLoad : function(component, event){
        //to fetch id from url
        var recordId = this.getParamValue('Forecast_Dashboard', 'id');
        var recID = recordId;
        var scenarioId;
        var scenarioInpID = component.get("v.scenarioInputRecord").Id;
        
        if(component.get("v.recId") != undefined && component.get("v.recId") != ''){
            recordId = component.get("v.recId");
        }
        else{
            if((recordId === undefined  || recordId == '' ) && (scenarioInpID === undefined || scenarioInpID === '') ){
                recordId = component.get("v.recordId");
                recID = recordId;
            }else if((recordId === undefined  || recordId == '' ) && (scenarioInpID !== undefined || scenarioInpID !== '')){
                recordId = scenarioInpID;
            }
        }
        
        var isClone = this.getParamValue('Forecast_Dashboard', 'isClone');
        if(isClone === undefined || isClone === '' || isClone === null){ isClone = false; }
        
        // To make Asset field Read only
        if( recID !== undefined &&  recID != ''){
            //component.set("v.proceed",true);
        }
        component.set('v.recId', recID);
        if( recordId != undefined && recordId != null && recordId != ''){ 
            var action = component.get("c.getScenarioInput");
            action.setParams({
                "recordId": recordId
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid && state === 'SUCCESS') {
                    var data = response.getReturnValue();
                    //Display toast if there is no Utilization record with Status= 'Approved By Lessor'
                    var utilizationWarning = data.utilizationWarning;
                    if(utilizationWarning != undefined && utilizationWarning != ''){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Warning!",
                            "message": utilizationWarning,
                            type: 'warning'
                        });
                        toastEvent.fire();
                    }

                    var scId = this.getParamValue('Forecast_Dashboard', 'id');
                    if(isClone && scId === data.scenarioInp.Id.slice(0,15)){
                        this.updateDataForCloneRecord(component, event, data);
                    }
                    
                    var scenarioInp = data.scenarioInp;
                    var base = 0.00;
                    if(scenarioInp.Base_Rent__c > 0){
                    	base = scenarioInp.Base_Rent__c/1000;
                    }
                    component.set("v.baseRent",base );
                    component.set("v.scenarioInputRecord", scenarioInp);
                    component.set("v.technicalSectionRecords",data.lstScenarioComponentInp);
                    if(data.lstRentExtensionInp.length < 1){
                        data.lstRentExtensionInp.push({
                            From_Date__c: null,
                            Rent_Amount__c: null,
                            To_Date__c: null
                        });
                    }
                    component.set("v.RentExtensionInputRecords",data.lstRentExtensionInp);
                    
                    this.getFlightHoursAndCycles(component,event);
                    
                    component.set("v.TSFilled", true);
                    component.set("v.OEFilled", true);
                    component.set("v.VIFilled", true);
                    component.set("v.FHCOFilled", true);
                    $A.util.addClass(component.find('FHCO'), 'FHCOAfterSave');
                    
                    var targetString = ['TimeSpan','OperationalEnvironment','ValuationInputs'];
                    for(var i = 0; i<targetString.length;i++){
                        $A.util.addClass(component.find(targetString[i]), 'boxDivAfterSave');
                    }
                    //console.log('test111' +component.get("v.technicalSectionRecords"));
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        this.handleErrors(component, errors);
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    updateDataForCloneRecord: function(component, event, data){
        data.scenarioInp.Name = 'Clone From '+data.scenarioInp.Name;
        data.scenarioInp.Forecast_Status__c = 'Draft';
        data.scenarioInp.Cloned_From__c = data.scenarioInp.Id;
        delete data.scenarioInp.Id;
        for(let key1 in data.lstScenarioComponentInp){
            let scInput = data.lstScenarioComponentInp[key1];
            
            //Remove Id & references from Assembly event Input And Shop Visit Records.
            for(let key2 in scInput.assemblyEventInput){
                let aeInput = scInput.assemblyEventInput[key2];
                delete aeInput.assemblyEventInp.Id;
                delete aeInput.assemblyEventInp.Fx_Assembly_Input__c;
                delete aeInput.assemblyEventInp.Fx_Assembly_Input__r;

                for(let key3 in aeInput.shopVisit.lstShopVistWrapper){
                    let svRecord = aeInput.shopVisit.lstShopVistWrapper[key3];
                    delete svRecord.svRec.Id;
                    delete svRecord.svRec.Fx_Assembly_Event_Input__c;
                }
            }
            
            //Remove Id & reference from Clone Assembly Event input records.
            for(let key2 in scInput.cloneAssemblyEventInput){
                let cloneAEInput = scInput.cloneAssemblyEventInput[key2];
                delete cloneAEInput.Id;
                delete cloneAEInput.Fx_Assembly_Input__c;
                delete cloneAEInput.Fx_Assembly_Input__r;
            }
            
            //Remove Id & references from Scenario Component input Record With Forecast type current Lease
            delete scInput.scenarioComponentInp.Id;
            delete scInput.scenarioComponentInp.Scenario_Input__c;
            delete scInput.scenarioComponentInp.Scenario_Input__r;

            //Remove Id & references from Scenario Component input Record With Forecast type Lease Extension
            delete scInput.cloneScenarioComponentInp.Id;
            delete scInput.cloneScenarioComponentInp.Scenario_Input__c;
            delete scInput.cloneScenarioComponentInp.Scenario_Input__r;
        }
        return data;
    },
    redirectToSection: function(component){
        var lastOpenedMoadalId = component.get("v.openedModalId") + "Modal";
        //console.log("lastOpenedMoadalId >>>> " + lastOpenedMoadalId );
        var nextModalId = '';
        switch(lastOpenedMoadalId) {
            case "TimeSpanModal":
                this.saveScenarioInp(component,event);
                break;
            case "FHCOModal":
                this.saveScenarioCmpInp(component,event);
                this.getFlightHoursAndCycles(component,event);
                break; 
            case "ValuationInputsModal":
                this.saveScenarioCmpInp(component,event);
                break;
        }
        
        if(lastOpenedMoadalId == "OperationalEnvironmentModal"){
            this.validateAssemblyEventRecords(component);
            //this.saveScenarioCmpInp(component,event);
        }
    },
    validateAssemblyEventRecords: function(component, event){
        var ExtReserveType = component.get("v.scenarioInputRecord").Ext_Reserve_Type__c;
        var Eola = component.get("v.scenarioInputRecord").EOLA__c;
        var ReserveType = '';
        var targetString = component.get("v.openedModalId");
        if(Eola){
            ReserveType = 'EOLA';
        }
        else {
            ReserveType = 'Monthly MR';
        }
        if( ExtReserveType != undefined && !ReserveType.includes(ExtReserveType) && ExtReserveType != '' ){
          var reserveDate = component.find('requiredField');
          var value = reserveDate.get('v.value');
            if(!value || value == '' || value.trim().length === 0){
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Start Date For Reserve Type is Required",
                    type: 'error',
                });
                toastEvent.fire();
            }
            else{
                this.saveScenarioCmpInp(component,event);
                $A.util.addClass(component.find(targetString+"Modal"), 'hideDiv');
                $A.util.addClass(component.find("backdrop"), 'hideDiv');
                $A.util.removeClass(component.find(targetString[0]), 'boxDiv');
            }
        }
        else{
                this.saveScenarioCmpInp(component,event);
                $A.util.addClass(component.find(targetString+"Modal"), 'hideDiv');
                $A.util.addClass(component.find("backdrop"), 'hideDiv');
                $A.util.removeClass(component.find(targetString[0]), 'boxDiv');
            }
    },
    saveScenarioInp:function(component, event, isDirectGenerateForecast){
        this.showSpinner(component);
        var records = component.get("v.technicalSectionRecords");
        var scenarioInp = component.get("v.scenarioInputRecord");
        var rentExtensionInp = component.get("v.RentExtensionInputRecords");
        var scenarioCmpInp = JSON.stringify(records);
        if(scenarioInp.Lease__r != undefined){
            scenarioInp.Lease__r = null;
        }
        if(scenarioInp.Asset__r != undefined){
            scenarioInp.Asset__r = null;
        }
        var action = component.get("c.saveScenarioCmpInp"); //saveTimeSpan
        action.setParams({
            "scenarioInpJSON": JSON.stringify(scenarioInp),
            "scenarioCmpInp": scenarioCmpInp,
            "rentExtensionInp": JSON.stringify(rentExtensionInp),
            "AEInputRecordsToDelete": ''
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                this.hideSpinner(component);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Record has been created Successfully",
                    type: 'success',
                });
                toastEvent.fire();
                var res = JSON.parse(response.getReturnValue());
                component.set("v.scenarioInputRecord", res.scenarioInp);
                component.set("v.technicalSectionRecords", res.lstScenarioComponentInp);
                if(res.lstRentExtensionInp.length < 1){
                    res.lstRentExtensionInp.push({
                        From_Date__c: null,
                        Rent_Amount__c: null,
                        To_Date__c: null
                    });
                }
                component.set("v.RentExtensionInputRecords",res.lstRentExtensionInp);
                component.set("v.recId", res.scenarioInp.Id);
                if(isDirectGenerateForecast){
                    var scenarioInp = component.get("v.scenarioInputRecord").Id; 
                    this.createMonthlyScenarioData(component, event, scenarioInp);
                }
                else{
                	this.getRecordsOnLoad(component,event);   
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveScenarioCmpInp:function(component,event){
        this.showSpinner(component);
        var scenarioInp = component.get("v.scenarioInputRecord");
        var residualValue = 0.0;
        var count = 0;
        if(scenarioInp.IBA__c != undefined && scenarioInp.IBA__c != '' && scenarioInp.IBA__c != 0){
            count = count + 1;
            residualValue = residualValue + Number.parseFloat(scenarioInp.IBA__c);
        }
  
        if(scenarioInp.Other__c != undefined && scenarioInp.Other__c != '' && scenarioInp.Other__c != 0){
            count = count + 1;
        	residualValue = residualValue + Number.parseFloat(scenarioInp.Other__c);
        }
        
        if(scenarioInp.Avitas__c != undefined && scenarioInp.Avitas__c != '' && scenarioInp.Avitas__c != 0){
            count = count + 1;
        	residualValue = residualValue + Number.parseFloat(scenarioInp.Avitas__c);
        }
        if(scenarioInp.Ascend__c != undefined && scenarioInp.Ascend__c != '' && scenarioInp.Ascend__c != 0){
             count = count + 1;
             residualValue = residualValue + Number.parseFloat(scenarioInp.Ascend__c);
        }
        
        if(residualValue != 0 || count != 0)
            residualValue = residualValue/count;
        residualValue = residualValue/1000000;
        
        if(residualValue != 'NaN' || residualValue !== undefined){
            component.set("v.residualValue",residualValue.toFixed(2));
            scenarioInp.Estimated_Residual_Value__c = residualValue.toFixed(2);
        }
        var lstScenarioCompInput = [];
        var records = component.get("v.technicalSectionRecords");
        var scenarioCmpInp = JSON.stringify(records);
        var rentExtensionInp = component.get("v.RentExtensionInputRecords");
        var action = component.get("c.saveScenarioCmpInp");
        
        action.setParams({
            "scenarioInpJSON": JSON.stringify(scenarioInp),
            "scenarioCmpInp": scenarioCmpInp,
            "rentExtensionInp": JSON.stringify(rentExtensionInp),
            "AEInputRecordsToDelete": JSON.stringify(component.get("v.AEInputRecordsToDelete"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if (component.isValid() && state === "SUCCESS") {
                this.hideSpinner(component);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Record has been created Successfully",
                    type: 'success',
                });
                toastEvent.fire();
                var res = JSON.parse(response.getReturnValue());
                component.set("v.scenarioInputRecord", res.scenarioInp);
                component.set("v.technicalSectionRecords", res.lstScenarioComponentInp);
                if(res.lstRentExtensionInp.length < 1){
                    res.lstRentExtensionInp.push({
                        From_Date__c: null,
                        Rent_Amount__c: null,
                        To_Date__c: null
                    });
                }
                component.set("v.RentExtensionInputRecords",res.lstRentExtensionInp);
                component.set("v.recId", res.scenarioInp.Id);
                this.deleteRentExt(component,event);
                this.getRecordsOnLoad(component,event);

            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    deleteRentExt : function(component, event){
        var action = component.get("c.RentExtToDelete");
        action.setParams({
            "RentExtToDelete": JSON.stringify(component.get("v.RentExtToDelete"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(!response.getReturnValue() == 'Success'){
                     var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": response.getReturnValue(),
                    type: 'error',
                });
                toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    createSectionFlyingHours: function (component, event, aircraftId, leaseId) {
        this.showSpinner(component);
        var action = component.get("c.getConstituentAssembly");
        let leaseOnLoad = component.get("v.leaseIdOnLoad");
        if(leaseOnLoad != undefined && leaseOnLoad != '' && leaseOnLoad != null){
            if(leaseId === undefined || leaseId === '' || leaseId === undefined){
                leaseId = leaseOnLoad;
            	component.set("v.leaseOnLoad","");
            }
        }
        action.setParams({
            "aircraftId": aircraftId,
            "forecastType": 'Forecast',
            "directSave": false,
            "leaseId": leaseId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                this.hideSpinner(component);
                var responseVal = response.getReturnValue();
                //Display toast if there is no Utilization record with Status= 'Approved By Lessor'
                var utilizationWarning = responseVal.utilizationWarning;
                if (!this.isUndefined(utilizationWarning) && !this.isUndefined(responseVal.scenarioInp)) {
                    if (!this.isUndefined(component.get("v.scenarioInputRecord.Lease__r")) || !this.isUndefined(responseVal.scenarioInp.Lease__c)) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Warning!",
                            "message": utilizationWarning,
                            type: 'warning'
                        });
                        toastEvent.fire();
                    }
                }
                if( response.getReturnValue().error === undefined || response.getReturnValue().error == '') {
                    var scenarioInp = responseVal.scenarioInp;
                    console.log('ScenarioInput: ', scenarioInp);
                    var wrapper = responseVal.lstScenarioComponentInp;
                    //console.log('Scenario Input: ',JSON.stringify(JSON.parse(scenarioInp)));
                    console.log('---->', component.get("v.scenarioInputRecord.Lease__r"));
                    if (component.get("v.scenarioInputRecord.Lease__r") !== undefined && component.get("v.scenarioInputRecord.Lease__r") !== null) {
                        console.log('Lease Data: ', component.get("v.scenarioInputRecord.Lease__r"));
                        scenarioInp.Lease__r = component.get("v.scenarioInputRecord.Lease__r");
                        scenarioInp.Lease__c = scenarioInp.Lease__r.Id;
                        if (scenarioInp.Asset__r !== undefined && scenarioInp.Asset__r.Lease__r !== undefined) {
                            scenarioInp.Asset__r.Lease__r = component.get("v.scenarioInputRecord.Lease__r");
                            scenarioInp.Asset__r.Lease__c = scenarioInp.Asset__r.Lease__r.Id;
                        }
                        component.set("v.scenarioInputRecord", scenarioInp);
                        console.log('1: ', JSON.stringify(scenarioInp));
                    }
                    else {
                        //this.handleErrors('No Active Lease Exists with this Asset');
                        if (scenarioInp.Lease__r === undefined || scenarioInp.Lease__r === null) {
                            this.removeSectionValues(component, event);
                        }
                        component.set("v.scenarioInputRecord", scenarioInp);
                    }
                    component.set("v.technicalSectionRecords", wrapper);
                    console.table(wrapper);
                    this.getFlightHoursAndCycles(component,event);
                    this.getRecordsOnLoad(component,event); 
                } 
                else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": response.getReturnValue().error,
                        type: 'error'
                    });
                    toastEvent.fire();    
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    setScenarioForProjection : function(component,event){
        var data = component.get("v.scenarioInputRecord");
        var ScenarioInp = new Object();
        ScenarioInp.Number_of_Months_Of_History_To_Use__c = data['Number_of_Months_Of_History_To_Use__c'];
        ScenarioInp.Number_Of_Months_For_Projection__c = data['Number_Of_Months_For_Projection__c'];
        component.set("v.scenarioInpCancel",ScenarioInp);
    },
    //To set the assembly input and other values to a attribute on modal open
    setScenarioValue : function (component, event){
        var data = component.get("v.scenarioInputRecord");
        var ScenarioInp = new Object();
        ScenarioInp.Ext_Reserve_Type__c = data['Ext_Reserve_Type__c'];  
        ScenarioInp.Cost_Assumption__c = data['Cost_Assumption__c'];  
        ScenarioInp.Balloon__c = data['Balloon__c'] ;
        ScenarioInp.Debt__c = data['Debt__c'] ;
        ScenarioInp.Interest_Rate__c = data['Interest_Rate__c'];
        ScenarioInp.UF_Fee__c = data['UF_Fee__c'];
        ScenarioInp.Estimated_Residual_Value__c = data['Estimated_Residual_Value__c'];
        ScenarioInp.IBA__c = data['IBA__c'];
        ScenarioInp.Other__c = data['Other__c'] ;
        ScenarioInp.Ext_Base_Rent__c = data['Ext_Base_Rent__c'];
        ScenarioInp.Extension_Assumption__c = data['Extension_Assumption__c'];
        ScenarioInp.Ascend__c = data['Ascend__c'];
        ScenarioInp.Avitas__c = data['Avitas__c'];
        ScenarioInp.Investment_Required_Purchase_Price__c = data['Investment_Required_Purchase_Price__c'];
        ScenarioInp.Re_Delivery_FC__c = data['Re_Delivery_FC__c'];
        ScenarioInp.Re_Delivery_FH__c = data['Re_Delivery_FH__c'];
        var scenarioCmp = component.get("v.technicalSectionRecords");
        var scenarioCmpCancel = [] ;
        for(var i=0; i<scenarioCmp.length; i++){
            var ScenarioComp = new Object();
            var ae = scenarioCmp[i].assemblyEventInput;
            var assemblyEventCancel = [];
            var shopVisitCancel = [];
            var cae = scenarioCmp[i].cloneAssemblyEventInput;
            var cloneAssemblyEventInputCancel = [];

            for(var j = 0; j < ae.length; j++){
                var assemblyEnt = new Object;
                var assemblyEvent = ae[j].assemblyEventInp;
                assemblyEnt.Name =  assemblyEvent['Name']; 
                assemblyEnt.Event_Cost__c = assemblyEvent['Event_Cost__c'];
                assemblyEnt.Current_MRR__c = assemblyEvent['Current_MRR__c'];
                assemblyEnt.Annual_Escalation__c = assemblyEvent['Annual_Escalation__c'];
                assemblyEnt.Annual_Escalation__c = assemblyEvent['Annual_Escalation__c'];
                assemblyEnt.Last_Escalation_Date__c = assemblyEvent['Last_Escalation_Date__c'];
                assemblyEnt.Interval_1_Hours__c = assemblyEvent['Interval_1_Hours__c'];
                assemblyEnt.Interval_2_Hours__c = assemblyEvent['Interval_2_Hours__c'];
                assemblyEnt.Interval_1_Cycles__c = assemblyEvent['Interval_1_Cycles__c'];
                assemblyEnt.Interval_2_Cycles__c = assemblyEvent['Interval_2_Cycles__c'];
                assemblyEnt.Interval_1_Months__c = assemblyEvent['Interval_1_Months__c'];
                assemblyEnt.Interval_2_Months__c = assemblyEvent['Interval_2_Months__c'];
                assemblyEnt.RC_FC__c = assemblyEvent['RC_FC__c'];
                assemblyEnt.RC_FH__c = assemblyEvent['RC_FH__c'];
                assemblyEnt.RC_Months__c = assemblyEvent['RC_Months__c'];
                assemblyEventCancel.push(assemblyEnt);
                
                for(let key3 in ae[j].shopVisit.lstShopVistWrapper){
                    let svRecord = ae[j].shopVisit.lstShopVistWrapper[key3];
                    var svOverride = new Object;
                    svOverride.Id = svRecord.svRec['Id'];
                    svOverride.Name = svRecord.svRec['Name'];
                    svOverride.Fx_Assembly_Event_Input__c = svRecord.svRec['Fx_Assembly_Event_Input__c'];
                    svOverride.LLP_Replacement_Ids__c = svRecord.svRec['LLP_Replacement_Ids__c'];
                    svOverride.Override_Event_Date__c = svRecord.svRec['Override_Event_Date__c'];
                    //svOverride.LLP_Replacement__c = svRecord.svRec['LLP_Replacement__c'];
                    svOverride.Override_Event_Cost__c = svRecord.svRec['Override_Event_Cost__c'];
                    //svOverride.Override_LLP_Event_Cost__c = svRecord.svRec['Override_LLP_Event_Cost__c'];
                    svOverride.SV_Number__c = svRecord.svRec['SV_Number__c'];
                    shopVisitCancel.push(svOverride);
                }
            }

            for(var j = 0; j < cae.length; j++){
                var cloneassemblyEnt = new Object;
                var cloneassemblyEvent = cae[j];
                cloneassemblyEnt.Name =  cloneassemblyEvent['Name']; 
                cloneassemblyEnt.Event_Cost__c = cloneassemblyEvent['Event_Cost__c'];
                cloneassemblyEnt.Base_MRR__c = cloneassemblyEvent['Base_MRR__c'];
                cloneassemblyEnt.Annual_Escalation__c = cloneassemblyEvent['Annual_Escalation__c'];
                cloneassemblyEnt.Last_Escalation_Date__c = cloneassemblyEvent['Last_Escalation_Date__c'];
                cloneassemblyEnt.From_Date__c = cloneassemblyEvent['From_Date__c'];
                cloneassemblyEnt.To_Date__c = cloneassemblyEvent['To_Date__c'];
                cloneAssemblyEventInputCancel.push(cloneassemblyEnt);
            }
            ScenarioComp.Name = scenarioCmp[i].scenarioComponentInp.Name;
            ScenarioComp.User_Defined_Utilization_FH__c = scenarioCmp[i].scenarioComponentInp.User_Defined_Utilization_FH__c;
            ScenarioComp.FH__c = scenarioCmp[i].scenarioComponentInp.FH__c ;
            ScenarioComp.FC__c = scenarioCmp[i].scenarioComponentInp.FC__c ;
            ScenarioComp.Operating_Environment_Impact__c =  scenarioCmp[i].scenarioComponentInp.Operating_Environment_Impact__c;
            ScenarioComp.Merge_Event__c = scenarioCmp[i].scenarioComponentInp.Merge_Event__c ;
            ScenarioComp.Merge_Span__c = scenarioCmp[i].scenarioComponentInp.Merge_Span__c ;
            ScenarioComp.assemblyEvents = assemblyEventCancel;
            ScenarioComp.shopVisits = shopVisitCancel;
            ScenarioComp.cloneAEInput = cloneAssemblyEventInputCancel;
            scenarioCmpCancel.push(ScenarioComp);
        }
        ScenarioInp.scenarioComp = scenarioCmpCancel;
        component.set("v.scenarioInpCancel",ScenarioInp);

        var rentExtensionInputCancel = component.get("v.RentExtensionInputRecords");
        component.set("v.rentExtensionInputCancel",JSON.stringify(rentExtensionInputCancel));
    },
    //To get the record id
    getParamValue : function( tabName, paramName ) {
        var url = window.location.href;
        var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&');
        var paramValue = '';
        for(var i=0; i<allParams.length; i++) {
            if(allParams[i].split('=')[0] == paramName)
                paramValue = allParams[i].split('=')[1];
        }
        return paramValue;
    },
    //To get the flight hours for preview page
    getFlightHoursAndCycles : function(component, event){
        var lstScenarioCmp =  component.get("v.technicalSectionRecords");
        var ScenarioCmp = new Object();
        
        if(lstScenarioCmp!== undefined && lstScenarioCmp.length > 0){
            for(var i = 0; i < lstScenarioCmp.length ; i++){
                if(lstScenarioCmp[i].lstAssemblyWrapper.length > 0 /*&& lstScenarioCmp[i].lstAssemblyWrapper[0].assembly != undefined*/){
                    if(lstScenarioCmp[i].scenarioComponentInp.Name == 'APU'){
                         if(lstScenarioCmp[i].scenarioComponentInp.User_Defined_Utilization_FH__c != true){
                            ScenarioCmp.APU =  lstScenarioCmp[i].lstAssemblyWrapper[0].avgCSN;
                        }else{
                             ScenarioCmp.APU = lstScenarioCmp[i].scenarioComponentInp.FC__c;
                        }
                    }
                    else if(lstScenarioCmp[i].scenarioComponentInp.Name == 'Engine'){
                        
                        if( lstScenarioCmp[i].scenarioComponentInp.User_Defined_Utilization_FH__c != true){

                            
                            //ScenarioCmp.EngineFH = (lstScenarioCmp[i].lstAssemblyWrapper[0].avgCSN);
                            //ScenarioCmp.EngineFC = (lstScenarioCmp[i].lstAssemblyWrapper[0].avgTSN);
                            
                            
                            //get default FC and FH for engine 1 and engine 2

                            lstScenarioCmp[i].lstAssemblyWrapper.forEach(function(item){
                                if(item.assembly.Type__c == 'Engine 1'){
                                    ScenarioCmp.Engine1FC = (item.avgCSN);
                                    ScenarioCmp.Engine1FH = (item.avgTSN);
                                }else if(item.assembly.Type__c == 'Engine 2'){
                                    ScenarioCmp.Engine2FC = (item.avgCSN);
                                    ScenarioCmp.Engine2FH = (item.avgTSN); 
                                }
                            });
                        }else{

                            /* ScenarioCmp.EngineFH = (lstScenarioCmp[i].scenarioComponentInp.FC__c);
                            ScenarioCmp.EngineFC = (lstScenarioCmp[i].scenarioComponentInp.FH__c);
                            */
                            // APRIL 10, 2019
                            ScenarioCmp.Engine1FC = (lstScenarioCmp[i].scenarioComponentInp.FC__c);
                            ScenarioCmp.Engine2FC = (lstScenarioCmp[i].scenarioComponentInp.FC__c);

                            ScenarioCmp.Engine1FH = (lstScenarioCmp[i].scenarioComponentInp.FH__c);
                            ScenarioCmp.Engine2FH = (lstScenarioCmp[i].scenarioComponentInp.FH__c);
                            
                            if(lstScenarioCmp[i].assemblyEventInput != undefined && 
                               lstScenarioCmp[i].assemblyEventInput.length > 0){
                                
                                lstScenarioCmp[i].assemblyEventInput.forEach(function(item){
                                    
                                    if(item.assemblyEventInp.Name == 'Engine 1 Performance Restoration'){
                                        
                                        if(item.assemblyEventInp.FH__c != null && item.assemblyEventInp.FH__c != undefined){
                                        	ScenarioCmp.Engine1FH = item.assemblyEventInp.FH__c;
                                        } 
                                    
                                     	if(item.assemblyEventInp.FC__c != null && item.assemblyEventInp.FC__c != undefined){
                                        	ScenarioCmp.Engine1FC = item.assemblyEventInp.FC__c;
                                        } 
                                    }
                                    
                                    if(item.assemblyEventInp.Name == 'Engine 2 Performance Restoration'){
                                    
                                        if(item.assemblyEventInp.FH__c != null && item.assemblyEventInp.FH__c != undefined){
                                            
                                            ScenarioCmp.Engine2FH = item.assemblyEventInp.FH__c;
                                        } 
                                        
                                        if(item.assemblyEventInp.FC__c != null && item.assemblyEventInp.FC__c != undefined){
                                            
                                            ScenarioCmp.Engine2FC = item.assemblyEventInp.FC__c;
                                        } 
                                    }
                                });
                            } 
                            
                        }
                    }
                        else if(lstScenarioCmp[i].scenarioComponentInp.Name == 'Landing Gear'){
                            if( lstScenarioCmp[i].scenarioComponentInp.User_Defined_Utilization_FH__c != true){
                            	 ScenarioCmp.LandingGear =(lstScenarioCmp[i].lstAssemblyWrapper[0].avgCSN);
                            }else{
                                ScenarioCmp.LandingGear = lstScenarioCmp[i].scenarioComponentInp.FC__c;
                            }
                        }
                }
            }
            component.set("v.FlightHours",ScenarioCmp);
        }
    },
    resetValue : function(component,event,targetId){
        var data = component.get("v.scenarioInputRecord");
        if(targetId[0] == "TimeSpan"){
            var data = component.get("v.scenarioInputRecord");
            var scenarioInpCancel = component.get("v.scenarioInpCancel");
            if(scenarioInpCancel != null ){
                data['Number_of_Months_Of_History_To_Use__c'] = scenarioInpCancel.Number_of_Months_Of_History_To_Use__c;
                data['Number_Of_Months_For_Projection__c']= scenarioInpCancel.Number_Of_Months_For_Projection__c;
                component.set("v.scenarioInputRecord",data);
            }else{
                data['Number_of_Months_Of_History_To_Use__c'] = -12;
                data['Number_Of_Months_For_Projection__c']= 12;
                component.set("v.scenarioInputRecord",data);
            }
            $A.util.addClass(component.find(targetId[0]+"Modal"), 'hideDiv');
            $A.util.addClass(component.find("backdrop"), 'hideDiv');
        }
        else if(targetId[0] == "OperationalEnvironment"){
            var data = component.get("v.scenarioInputRecord");
            var scenarioCmp = component.get("v.technicalSectionRecords");
            var scenarioInpCancel = component.get("v.scenarioInpCancel");
            
            if(scenarioInpCancel != null && scenarioInpCancel !== undefined && scenarioInpCancel!= ''){
                data['Cost_Assumption__c'] = scenarioInpCancel.Cost_Assumption__c;
                data['Ext_Base_Rent__c'] = scenarioInpCancel.Ext_Base_Rent__c;
                data['Ext_Reserve_Type__c'] = scenarioInpCancel.Ext_Reserve_Type__c;
                data['Extension_Assumption__c'] = scenarioInpCancel.Extension_Assumption__c;
                component.set("v.scenarioInputRecord",data);
                if(scenarioInpCancel.scenarioComp != '' && scenarioInpCancel.scenarioComp !== undefined){
                    for(var i=0; i<scenarioCmp.length; i++){
                        if(scenarioInpCancel.scenarioComp[i].Name == scenarioCmp[i].scenarioComponentInp.Name){
                            var ae = scenarioCmp[i].assemblyEventInput;
                            for(var j = 0; j<ae.length; j++){
                                var assemblyEventcancel = scenarioInpCancel.scenarioComp[i].assemblyEvents[j];
                                var assemblyEvent = ae[j];
                                if(assemblyEventcancel.Name == assemblyEvent.Name){
                                    assemblyEvent['Event_Cost__c'] = assemblyEventcancel.Event_Cost__c;
                                    assemblyEvent['Current_MRR__c'] = assemblyEventcancel.Current_MRR__c;
                                    assemblyEvent['Starting_MR_Balance__c'] = assemblyEventcancel.Starting_MR_Balance__c;
                                    assemblyEvent['Annual_Escalation__c'] = assemblyEventcancel.Annual_Escalation__c;
                                    assemblyEvent['Last_Escalation_Date__c'] = assemblyEventcancel.Last_Escalation_Date__c;
                                    scenarioCmp[i].assemblyEventInput[j] = assemblyEvent;  
                                } 
                            }
                            
                            var cae = scenarioCmp[i].cloneAssemblyEventInput;
                            var count = 0;
                            for(var j = 0; j<cae.length; j++){
                                if(j < scenarioInpCancel.scenarioComp[i].cloneAEInput.length){
                                    var cloneAEInputCancel = scenarioInpCancel.scenarioComp[i].cloneAEInput[j];
                                    var cloneAssemblyEvent = cae[j];
                                    if(cloneAEInputCancel.Name == cloneAssemblyEvent.Name){
                                        cloneAssemblyEvent['Event_Cost__c'] = cloneAEInputCancel.Event_Cost__c;
                                        cloneAssemblyEvent['Base_MRR__c'] = cloneAEInputCancel.Base_MRR__c;
                                        cloneAssemblyEvent['Annual_Escalation__c'] = cloneAEInputCancel.Annual_Escalation__c;
                                        cloneAssemblyEvent['Last_Escalation_Date__c'] = cloneAEInputCancel.Last_Escalation_Date__c;
                                        cloneAssemblyEvent['From_Date__c'] = cloneAEInputCancel.From_Date__c;
                                        cloneAssemblyEvent['To_Date__c'] = cloneAEInputCancel.To_Date__c;
                                        scenarioCmp[i].cloneAssemblyEventInput[j] = cloneAssemblyEvent;  
                                    }
                                }
                                else{
                                    count++;
                                }
                            }
                            if(count != 0){
                                scenarioCmp[i].cloneAssemblyEventInput.splice(scenarioInpCancel.scenarioComp[i].cloneAEInput.length,count);
                            }
                        }
                    }
                    component.set("v.technicalSectionRecords",scenarioCmp);
                }
            }
            else{
                data['Cost_Assumption__c'] = '--None--';
                var scenarioCmp = component.get("v.technicalSectionRecords");
                for(var i=0; i<scenarioCmp.length; i++){
                    var ae = scenarioCmp[i].assemblyEventInput;
                    for(var j = 0; j<ae.length; j++){
                        var assemblyEvent = ae[j];
                        assemblyEvent['Event_Cost__c'] = '';
                        assemblyEvent['Current_MRR__c'] = '';
                        assemblyEvent['Starting_MR_Balance__c'] = '';
                        assemblyEvent['Annual_Escalation__c'] = '';
                        assemblyEvent['Last_Escalation_Date__c'] = '';

                        scenarioCmp[i].assemblyEventInput[j] = assemblyEvent;
                    } 
                }
                component.set("v.scenarioInputRecord",data);
                component.set("v.technicalSectionRecords",scenarioCmp);
            }
            
            var rentExtensionInputCancel = component.get("v.rentExtensionInputCancel");
            component.set("v.RentExtensionInputRecords", JSON.parse(rentExtensionInputCancel));

            $A.util.addClass(component.find(targetId[0]+"Modal"), 'hideDiv');
            $A.util.addClass(component.find("backdrop"), 'hideDiv');
        }
        else if(targetId[0] == "ValuationInputs"){
            var data = component.get("v.scenarioInputRecord");
            var scenarioInpCancel = component.get("v.scenarioInpCancel");
            if(scenarioInpCancel !== null){
                data['Balloon__c'] = scenarioInpCancel.Balloon__c;
                data['Debt__c'] = scenarioInpCancel.Debt__c;
                data['Interest_Rate__c'] = scenarioInpCancel.Interest_Rate__c;
                data['UF_Fee__c'] = scenarioInpCancel.UF_Fee__c;
                data['Estimated_Residual_Value__c'] = scenarioInpCancel.Estimated_Residual_Value__c;
                data['IBA__c'] = scenarioInpCancel.IBA__c;
                data['Other__c'] = scenarioInpCancel.Other__c;
                data['Ascend__c'] = scenarioInpCancel.Ascend__c;
                data['Avitas__c'] = scenarioInpCancel.Avitas__c;
                data['Investment_Required_Purchase_Price__c']=scenarioInpCancel.Investment_Required_Purchase_Price__c;
                component.set("v.scenarioInputRecord",data);
            }
            else{
                data['Balloon__c'] = ''; 
                data['Debt__c'] = '';
                data['Interest_Rate__c'] = '';
                data['UF_Fee__c'] = '';
                data['Estimated_Residual_Value__c'] = '';
                data['IBA__c'] = '';
                data['Other__c'] = '';
                data['Ascend__c'] = '';
                data['Avitas__c'] = '';
                data['Investment_Required_Purchase_Price__c']='';
                component.set("v.scenarioInputRecord",data); 
            }
            $A.util.addClass(component.find(targetId[0]+"Modal"), 'hideDiv');
            $A.util.addClass(component.find("backdrop"), 'hideDiv');
        }
        else if(targetId[0] == 'FHCO'){
            var scenarioInpCancel = component.get("v.scenarioInpCancel");
            if(scenarioInpCancel !== null ){
                var data = component.get("v.scenarioInputRecord");
                if(scenarioInpCancel['RC_Type__c'] != data['RC_Type__c']){
                	data['RC_Type__c'] = scenarioInpCancel['RC_Type__c'];
                }
                if(scenarioInpCancel['Re_Delivery_FC__c'] != data['Re_Delivery_FC__c']){
                    data['Re_Delivery_FC__c'] = scenarioInpCancel['Re_Delivery_FC__c'];
                }
                if(scenarioInpCancel['Re_Delivery_FH__c'] != data['Re_Delivery_FH__c']){
                    data['Re_Delivery_FH__c'] = scenarioInpCancel['Re_Delivery_FH__c'];
                }
                
                var scenarioCmp = component.get("v.technicalSectionRecords");
                if(scenarioInpCancel.scenarioComp != '' && scenarioInpCancel.scenarioComp !== undefined){
                    for(var i=0; i<scenarioCmp.length; i++){
                        if(scenarioInpCancel.scenarioComp[i].Name == scenarioCmp[i].scenarioComponentInp.Name){
                            var ae = scenarioCmp[i].assemblyEventInput;
                            for(var j = 0; j<ae.length; j++){
                                var assemblyEvent = ae[j].assemblyEventInp;
                                var assemblyEventcancel = scenarioInpCancel.scenarioComp[i].assemblyEvents[j];
                                
                                if(assemblyEventcancel.Name === assemblyEvent.Name){
                                    assemblyEvent['Interval_1_Hours__c'] = assemblyEventcancel.Interval_1_Hours__c;
                                    assemblyEvent['Interval_2_Hours__c'] = assemblyEventcancel.Interval_2_Hours__c;
                                    assemblyEvent['Interval_1_Cycles__c'] = assemblyEventcancel.Interval_1_Cycles__c;
                                    assemblyEvent['Interval_2_Cycles__c'] = assemblyEventcancel.Interval_2_Cycles__c;
                                    assemblyEvent['Interval_1_Months__c'] = assemblyEventcancel.Interval_1_Months__c;
                                    assemblyEvent['Interval_2_Months__c'] = assemblyEventcancel.Interval_2_Months__c;
                                    assemblyEvent['RC_FC__c'] = assemblyEventcancel.RC_FC__c;
                                    assemblyEvent['RC_FH__c'] = assemblyEventcancel.RC_FH__c;
                                    assemblyEvent['RC_Months__c'] = assemblyEventcancel.RC_Months__c;
                                    scenarioCmp[i].assemblyEventInput[j].assemblyEventInp = assemblyEvent;
                                }
                                
                                var shopVisitCancel = scenarioInpCancel.scenarioComp[i].shopVisits[j];
                                for(let key1 in ae[j].shopVisit.lstShopVistWrapper){
                                    let shopVisit = ae[j].shopVisit.lstShopVistWrapper[key1];
                                    if(shopVisitCancel.SV_Number__c == shopVisit.svRec.SV_Number__c){
                                        shopVisit.svRec.Override_Event_Date__c = shopVisitCancel.Override_Event_Date__c;
                                        //shopVisit.svRec.LLP_Replacement__c = shopVisitCancel.LLP_Replacement__c;
                                        shopVisit.svRec.Override_Event_Cost__c = shopVisitCancel.Override_Event_Cost__c;
                                        //shopVisit.svRec.Override_LLP_Event_Cost__c = shopVisitCancel.Override_LLP_Event_Cost__c;
                                        shopVisit.svRec.LLP_Replacement_Ids__c = shopVisitCancel.LLP_Replacement_Ids__c;
                                    }
                                }
                            }
                            scenarioCmp[i].scenarioComponentInp.User_Defined_Utilization_FH__c = scenarioInpCancel.scenarioComp[i].User_Defined_Utilization_FH__c;
                            scenarioCmp[i].scenarioComponentInp.FH__c = scenarioInpCancel.scenarioComp[i].FH__c;
                            scenarioCmp[i].scenarioComponentInp.FC__c = scenarioInpCancel.scenarioComp[i].FC__c;
                            scenarioCmp[i].scenarioComponentInp.Operating_Environment_Impact__c = scenarioInpCancel.scenarioComp[i].Operating_Environment_Impact__c;
                            scenarioCmp[i].scenarioComponentInp.Merge_Event__c = scenarioInpCancel.scenarioComp[i].Merge_Event__c;
                            scenarioCmp[i].scenarioComponentInp.Merge_Span__c = scenarioInpCancel.scenarioComp[i].Merge_Span__c;
                        }
                    }
                }
                component.set("v.technicalSectionRecords",scenarioCmp);
                component.set("v.scenarioInputRecord",data);
            }
            else{
                var data = component.get("v.scenarioInputRecord");
                data['RC_Type__c'] = '--None--';
                var scenarioCmp = component.get("v.technicalSectionRecords");
                if(scenarioCmp != undefined){
                    for(var i=0; i<scenarioCmp.length; i++){
                        if( scenarioCmp[i].assemblyEventInput != undefined){
                            var ae = scenarioCmp[i].assemblyEventInput;
                            for(var j = 0; j<ae.length; j++){
                                var assemblyEvent = ae[j];
                                assemblyEvent['Interval_1_Hours__c'] = '';
                                assemblyEvent['Interval_2_Hours__c'] = '';
                                assemblyEvent['Interval_1_Cycles__c'] = '';
                                assemblyEvent['Interval_2_Cycles__c'] = '';
                                assemblyEvent['Interval_1_Months__c'] = '';
                                assemblyEvent['Interval_2_Months__c'] = '';
                                assemblyEvent['RC_FC__c'] = '';
                                assemblyEvent['RC_FH__c'] = '';
                                assemblyEvent['RC_Months__c'] = '';
                                scenarioCmp[i].assemblyEventInput[j] = assemblyEvent;
                            }
                        }                  
                        scenarioCmp[i].scenarioComponentInp.User_Defined_Utilization_FH__c = false;
                        scenarioCmp[i].scenarioComponentInp.FH__c ='';
                        scenarioCmp[i].scenarioComponentInp.FC__c ='';
                        scenarioCmp[i].scenarioComponentInp.Operating_Environment_Impact__c = false;
                        scenarioCmp[i].scenarioComponentInp.Merge_Event__c = '';
                        scenarioCmp[i].scenarioComponentInp.Merge_Span__c = 0;
                    }
                    component.set("v.technicalSectionRecords",scenarioCmp);
                    component.set("v.scenarioInputRecord",data);  
                }
            }
            $A.util.addClass(component.find(targetId[0]+"Modal"), 'hideDiv');
            $A.util.addClass(component.find("backdrop"), 'hideDiv');
        }
    },
    //Update the records for selected aircraft
    updateRecordsForSelectedAircraft : function(component, event){
        var aircraftId = component.get("v.aircraftId");
        if (aircraftId != undefined && aircraftId != null && aircraftId != '') {
            this.createSectionFlyingHours(component, event, aircraftId, '');
            this.loadSectionValues(component, event);
            let obj = component.get("v.scenarioInputRecord");

            obj['Asset__c'] = aircraftId;
            component.set("v.scenarioInputRecord",obj);  
        }
    },
    loadSectionValues : function(component,event){
        $A.util.removeClass(component.find("TimeSpan"), 'boxDiv');
        $A.util.addClass(component.find("TimeSpan"), 'boxDivAfterSave');
        $A.util.addClass(component.find("TimeSpan_BoxTitle"), 'boxTitleAfterSave');
        
        $A.util.removeClass(component.find("OperationalEnvironment"), 'boxDiv');
        $A.util.addClass(component.find("OperationalEnvironment"), 'boxDivAfterSave');
        $A.util.addClass(component.find("OperationalEnvironment_BoxTitle"), 'boxTitleAfterSave');
        
        $A.util.removeClass(component.find("ValuationInputs"), 'boxDiv');
        $A.util.addClass(component.find("ValuationInputs"), 'boxDivAfterSave');
        $A.util.addClass(component.find("ValuationInputs_BoxTitle"), 'boxTitleAfterSave');
        
        $A.util.removeClass(component.find("FHCO"), 'boxDiv');
        $A.util.addClass(component.find("FHCO"), 'FHCOAfterSave');
        $A.util.addClass(component.find("FHCO_BoxTitle"), 'boxTitleAfterSave');
        
        component.set("v.FHCOFilled", true);
        component.set("v.TSFilled", true);
        component.set("v.OEFilled", true);
    },
    removeSectionValues : function(component,event){
        $A.util.addClass(component.find("TimeSpan"), 'boxDiv');
        $A.util.removeClass(component.find("TimeSpan"), 'boxDivAfterSave');
        $A.util.removeClass(component.find("TimeSpan_BoxTitle"), 'boxTitleAfterSave');
        
        $A.util.addClass(component.find("ValuationInputs"), 'boxDiv');
        $A.util.removeClass(component.find("ValuationInputs"), 'boxDivAfterSave');
        $A.util.removeClass(component.find("ValuationInputs_BoxTitle"), 'boxTitleAfterSave');
        
        $A.util.addClass(component.find("OperationalEnvironment"), 'boxDiv');
        $A.util.removeClass(component.find("OperationalEnvironment"), 'boxDivAfterSave');
        $A.util.removeClass(component.find("OperationalEnvironment_BoxTitle"), 'boxTitleAfterSave');
        
        $A.util.addClass(component.find("FHCO"), 'boxDiv');
        $A.util.removeClass(component.find("FHCO"), 'FHCOAfterSave');
        $A.util.removeClass(component.find("FHCO_BoxTitle"), 'boxTitleAfterSave');
        
        $A.util.addClass(component.find("ValuationInputs"), 'boxDiv');
        component.set("v.VIFilled", false);
        component.set("v.FHCOFilled", false);
        component.set("v.TSFilled", false);
        component.set("v.OEFilled", false);
    },
    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    generateForecast :function(component, event, helper){
        var scenarioInp = component.get("v.scenarioInputRecord").Id; 
        if(scenarioInp == undefined){
            this.saveScenarioInp(component, event, true);
        }
        else{
            this.createMonthlyScenarioData(component, event, scenarioInp);
        }
    },
    //To create Component Output and their Monthly Sceanrio
    createMonthlyScenarioData :function (component, event, scenarioInp) { 
        var sceanrioInpName = component.get("v.scenarioInputRecord").Name;
        var action2 = component.get("c.createMonthlyDataForAssembly");
        action2.setParams({
            "ForecastId": scenarioInp
        });
        action2.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid && state === 'SUCCESS') {
                //Functionality with batch class
                this.hideSpinner(component);
                let message = response.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "message": message,
                    "type": 'success',
                });
                toastEvent.fire();
            }else{
                console.log('Error occurred');
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(component, errors);
                }
            }
        });
        $A.enqueueAction(action2);
    },
    handleErrors: function (component, errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errors === 'object') {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );
                        });
                    };
                }
            });
        }
        else if (typeof errors === "string" && errors.length != 0) {
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    },
    storeCloneAssemblyRecords: function(component, event, helper){
        let cAEInput = [];
        let templist = [];
        let lstSCInput = component.get("v.technicalSectionRecords");
        for(let key1 in lstSCInput){
            let scInput = lstSCInput[key1];
            if(scInput.cloneAssemblyEventInput.length != 0){
                cAEInput.push({
                    assemblyName : scInput.scenarioComponentInp.Name,
                    caeInput: []
                });
                for(let key2 in scInput.cloneAssemblyEventInput){
                    if(!templist.includes(scInput.cloneAssemblyEventInput[key2].Name)){
                        templist.push(scInput.cloneAssemblyEventInput[key2].Name);
                        for(let key3 in cAEInput){
                            if(cAEInput[key3].assemblyName === scInput.scenarioComponentInp.Name){
                                cAEInput[key3].caeInput.push(scInput.cloneAssemblyEventInput[key2])
                            }
                        }
                    }
                }
            }
        }
        component.set("v.cloneAEInputRecords", cAEInput);
    },
    resetValues: function(component, event, helper){
        this.removeSectionValues(component, event);
        component.set("v.scenarioInputRecord.Asset__r", undefined);
        component.set("v.scenarioInputRecord.Asset__c", undefined);
        component.set("v.scenarioInputRecord.Lease__r", undefined);
        component.set("v.scenarioInputRecord.Lease__c", undefined);
    },
    isUndefined: function (input) {
        if (input != undefined && input != '' && input != null) { return false; }
        return true;
    }
})