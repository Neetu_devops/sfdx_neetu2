({
    SliderFunction : function (minR,maxR, className) {
        $('.'+className).jRange({
            from:minR,
            to: maxR,
            step: 1,
            format: '%s',
            theme: 'theme-blue',
            showScale: false,
            width: '100%',
            showLabels: true,
            isRange : true
        });
        $('.'+className).jRange('setValue',minR.toString()+','+maxR.toString());
    },
    
    
    selectedRecordsUpdate : function(component) {    
        var records = component.get("v.operatorRecordsList"); 
        var selectedOperators = component.get("v.selectedOperators");
        
        if(selectedOperators.length > 0) {
            for(var i=0; i<selectedOperators.length; i++) {
                for(var j=0; j<records.length; j++) {
                    if(records[j].operator.Id == selectedOperators[i].Id) {
                        records[j].isOperatorExists = true;
                        
                        break;
                    }
                }
            }
        }
        
        component.set("v.operatorRecordsList", records); 
        
        if(records.length > 0) {
            this.operatorSelection(component);     
        }        
    },
    
    
    operatorSelection : function(component) {    
        var operatorList = component.get("v.operatorRecordsList");
        var selectedOperators = [];
        var checkedCount = 0;
        var disabledCount = 0;
        
        for(var i=0; i<operatorList.length; i++) {
            if(operatorList[i].isDisabled) {
                disabledCount++;
            }
            else {
                if(operatorList[i].isOperatorExists) {
                    checkedCount++;
                    
                    selectedOperators.push(operatorList[i].operator);
                }
            }            
        }
        
        if(checkedCount > 0) {
            component.set("v.checkboxList", true);
        }
        else {
            component.set("v.checkboxList", false);
        }
        
        if((checkedCount + disabledCount) == operatorList.length) {
            component.find("headerCheckBox").set("v.checked", true);
        }
        else {
            component.find("headerCheckBox").set("v.checked", false);
        }
        
        component.set("v.selectedOperators", selectedOperators);
    },
    
    
    handleErrors : function(component,errors) {
        if (errors) {
            errors.forEach( function (error){
                if (error.message){
                    console.log('error.message'+error.message);
                    let message = error.message;
                    component.set("v.message",message);
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        let message = pageError.message;
                        console.log('message',message);
                        component.set("v.message",message);
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            let message = errorList.message;
                            console.log( "Field Error on " + fieldName + " : " + message );
                            component.set("v.message",message);
                        });  
                    };
                }
            });
            
            console.log('---'+component.get("v.message"));
            $A.util.removeClass(component.find("toastError"), "slds-hide");
        }
    }
})