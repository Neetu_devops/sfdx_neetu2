({
    doInit : function(component, event, helper) {
        let ele = document.getElementById("sliderDiv");
        if(ele) {
            let width = ele.offsetWidth - 40;
            console.log("calcualted width "+width);
            component.set("v.sliderLeftValue", width);
        }
        var $chkboxes = document.getElementsByClassName('chkbox');
        var lastChecked = null;
        console.log('doinit record id  ' +component.get("v.recordId"));
        var recId = component.get("v.recordId");
        if($A.util.isEmpty(recId) || $A.util.isUndefined(recId) || recId == null || recId == '') {
            component.set('v.enableSistershipSearch', false);
        }
        var action = component.get("c.loadDefaultData");
        action.setParams({
            "marketCampaignId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            $A.util.addClass(component.find("busyIndicator"), "slds-hide");
            var state = response.getState();
            
            if(component.isValid && state === 'SUCCESS') {
                console.table(response.getReturnValue());
                component.set("v.searchFieldsData", response.getReturnValue());
                console.log('doinit searchFieldsData ' +JSON.stringify(response.getReturnValue()));
                //To set None value in Aircraft Type
                var aType=[];
                aType.push('-- None --');
                var aircraftType = component.get("v.searchFieldsData").airCraftType;
                
				if(aircraftType != undefined){
					for(var i = 0 ;i <aircraftType.length; i++ ){
						aType.push(aircraftType[i]);
					}
                }
				
                component.set("v.searchFieldsData.airCraftType",aType);                                
                component.set("v.recordName",response.getReturnValue().Name);
                component.set("v.aircraftInCampLst",component.get("v.searchFieldsData.aircraftInDealList"));
                component.set("v.vintageSelVal", component.get("v.searchFieldsData.vintageMinRange"));
                component.set("v.columns", response.getReturnValue().colList);
                
                //added by Priyanka C: 
                //if either of below 6 fields are blank or no world fleet record present,then defaulting to 1-10 ,as no records will be fetched
                var vintageMin = component.get("v.searchFieldsData.vintageMinRange");
                var vintageMax = component.get("v.searchFieldsData.vintageMaxRange");
                if(vintageMin == vintageMax)
                {vintageMin = 0;
                 vintageMax = 1;
                }
                var mtowMin = component.get("v.searchFieldsData.mtowMinRange");
                var mtowMax = component.get("v.searchFieldsData.mtowMaxRange");
                if(mtowMin == mtowMax ){
                mtowMin = 0;
                mtowMax = 1; 
                }
                var seatMin = component.get("v.searchFieldsData.seatMinRange");
                var seatMax = component.get("v.searchFieldsData.seatMaxRange");
                if(seatMin == seatMax){
                seatMin =0;
                seatMax = 1;
                }
                console.log('vintageMin :'+vintageMin+ ' vintageMax :'+vintageMax);
                console.log('mtowMin :'+mtowMin+ ' mtowMax :'+mtowMax);
                console.log('seatMin :'+seatMin+ ' seatMax :'+seatMax);
                
                if(vintageMin == null || vintageMin == undefined || vintageMax == null || vintageMax == undefined){
                    console.log(" if V");
                    helper.SliderFunction( '1','10','range-slider');}
                else{
                    console.log(" else V ",vintageMin+' ',vintageMax);
                    helper.SliderFunction( vintageMin,vintageMax,'range-slider');
                }
                
                 if(mtowMin == null || mtowMin == undefined || mtowMax == null || mtowMax == undefined){
                    console.log(" if M");
                    helper.SliderFunction( '1','10','range-slider-mtow');}
                else{
                    console.log(" else M");
                    helper.SliderFunction( mtowMin,mtowMax,'range-slider-mtow');
                }
                
                if(seatMin == null || seatMin == undefined ||  seatMax == null || seatMax == undefined){
                    console.log(" if S");
                    helper.SliderFunction( '1','10','range-slider-seat');}
                else{
                    console.log(" else S");
                    helper.SliderFunction( seatMin,seatMax,'range-slider-seat');
                }
                
                
            }else{
                helper.handleErrors(component,response.getError()); 
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
    aircraftChange: function (component, event) { 
        if(component.find("actype").get("v.value") != ''){
            $A.util.removeClass(component.find("busyIndicator"), "slds-hide");
            
            var action = component.get("c.onAircraftTypeLoad");
            action.setParams({
                "airCraftType" : component.find("actype").get("v.value"),
            });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid && state === 'SUCCESS') { 
                    console.log(JSON.stringify(response.getReturnValue())+'****'+response.getReturnValue().lstEngineType);
                    component.set("v.searchFieldsData.engineType",response.getReturnValue().lstEngineType);
                    component.set("v.searchFieldsData.variant",response.getReturnValue().lstAircraftVariant);
                    component.set("v.searchFieldsData.engineVariant",''); 
                } 
                else {
                    console.log('Error'+response.getError());   
                } 
                
                $A.util.addClass(component.find("busyIndicator"), "slds-hide");
            });
            
            $A.enqueueAction(action);  
        }
    },
    
    engineTypeChange: function (component, event) { 
        if(component.find("actype").get("v.value") != '' && component.find("engineType").get("v.value") != ''){
            console.log('test2');
            $A.util.removeClass(component.find("busyIndicator"), "slds-hide");
            
            var action = component.get("c.getEngineVariant");
            action.setParams({
                "airCraftType" : component.find("actype").get("v.value"),
                "engineType" : component.find("engineType").get("v.value"),
            });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                $A.util.addClass(component.find("busyIndicator"), "slds-hide");
                
                if(component.isValid && state === 'SUCCESS') {
                    console.log(JSON.stringify(response.getReturnValue()));
                    component.set("v.searchFieldsData.engineVariant",response.getReturnValue());
                    
                } else{
                    helper.handleErrors(component, response.getError());
                    console.log('Error'+response.getError());   
                }
                
                
            });
            $A.enqueueAction(action); 
        } 
    },
    
    handleRangeChange: function (component, event) {
        var detail = component.set("v.value", event.getParam("value"));
    },
    
    cancel : function(component, event, helper) {
        window.open('/'+component.get("v.recordId"),'_self');
    },
    
    searchProspect : function(component, event, helper) {
        if(component.find("actype").get("v.value") == undefined 
           || (component.find("actype").get("v.value")).indexOf('None') != -1) {
            $A.util.removeClass(component.find("toastWarning"), "slds-hide");
            component.set("v.operatorRecordsList", undefined);
        }
        else { 
            
            if(component.get("v.enableSistershipSearch") && component.find("msn").get("v.value") != '') {
                
                $A.util.addClass(component.find("errorMsg"), "slds-hide");
                
                $A.util.removeClass(component.find("busyIndicator"), "slds-hide");
                
                component.set("v.msnSearch",true);
                
                var action = component.get("c.searchSistersships");
                var aircraftdealRecords = component.get("v.aircraftInCampLst");
                var acMSN = component.find("msn").get("v.value");
                
                var acType = '';
                var acVintage = '';
                
                for(var i=0; i< aircraftdealRecords.length; i++) {
                    if(acMSN == aircraftdealRecords[i].MSN__c) {
                        acType = aircraftdealRecords[i].Aircraft__r.Aircraft_Type__c;
                        acVintage = (aircraftdealRecords[i].Aircraft__r.Vintage__c).split('-')[0];
                        console.log('acType'+acType+'acVintage'+acVintage);
                    }
                }
                action.setParams({
                    "aircraftMSN" : acMSN,
                    "aircraftType" : acType,
                    "aircraftVintage" : acVintage,
                    "marketingCloudId" : component.get("v.recordId")
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    $A.util.addClass(component.find("busyIndicator"), "slds-hide");
                    
                    if(component.isValid && state === 'SUCCESS') {
                        component.set("v.operatorRecordsList", response.getReturnValue());
                        helper.selectedRecordsUpdate(component);                         
                    }
                    else{
                        helper.handleErrors(component, response.getError());
                    }
                });       
                
                $A.enqueueAction(action);
                
                $A.util.removeClass(document.getElementById("blankOperatorList"), "slds-hide");
                $A.util.addClass(document.getElementById("blankOperatorList"), "slds-show");
            }else{
                $A.util.addClass(component.find("errorMsg"), "slds-hide");              
                $A.util.removeClass(component.find("busyIndicator"), "slds-hide");
                
                var flag = 0;
                var vMin = document.getElementsByClassName("pointer-label low")[0].innerHTML;
                var vMax = document.getElementsByClassName("pointer-label high")[0].innerHTML;
                var mMin = document.getElementsByClassName("pointer-label low")[1].innerHTML;
                var mMax = document.getElementsByClassName("pointer-label high")[1].innerHTML;
                var seatMin = document.getElementsByClassName("pointer-label low")[2].innerHTML;
                var seatMax = document.getElementsByClassName("pointer-label high")[2].innerHTML;
                
                if(component.find('actype').get("v.value") != ''){
                    console.log('*******1*******');
                    console.log('vMin '+': '+vMin + ', vMax: '+vMax+ ', mMin:'+mMin +', mMax:'+mMax+ ', seatMin:'+seatMin+ ', seatMax:'+seatMax);
                    var action = component.get("c.searchProspects");
                    action.setParams({
                        "aircraftType" : component.find("actype").get("v.value"),
                        "variant" : component.find("variant").get("v.value"),
                        "vMin" : vMin,
                        "vMax" : vMax,
                        "mMin" : mMin,
                        "mMax" : mMax,
                        "seatMin" : seatMin,
                        "seatMax" : seatMax,
                        "operatorCountry" : component.find("country").get("v.value"),
                        "operatorRegion" : component.find("region").get("v.value"),
                        "engineType" : component.find("engineType").get("v.value"),      
                        "engineVariant" : component.find("engVariant").get("v.value"),
                        "marketCampaignId" : component.get("v.recordId")
                    });                   
                    action.setCallback(this, function(response){                        
                        $A.util.addClass(component.find("busyIndicator"), "slds-hide");
                        
                        var state = response.getState();
                        component.set("v.msnSearch",false);
                        
                        if(component.isValid && state === 'SUCCESS') {
                            component.set("v.operatorRecordsList", response.getReturnValue()); 
                            helper.selectedRecordsUpdate(component); 
                        }
                        else {
                            helper.handleErrors(component, response.getError()); 
                        }
                    });    
                    
                    $A.enqueueAction(action);
                    
                    component.set("v.recordSel", false);
                    $A.util.removeClass(document.getElementById("blankOperatorList"), "slds-hide");
                    $A.util.addClass(document.getElementById("blankOperatorList"), "slds-show");
                }
                else {
                    $A.util.removeClass(component.find("errorMsg"), "slds-hide");
                }
            }
        }        
    },
    
    
    addProspect : function(component, event, helper) {     
        $A.util.removeClass(component.find("busyIndicator"), "slds-hide");
        
        var selectedOperators = component.get("v.operatorRecordsList");
        
        var action = component.get("c.addProspects");
        console.log('Selected operator'+JSON.stringify(selectedOperators));
        action.setParams({
            "operatorRecords" : JSON.stringify(selectedOperators),
            "marketCampaignId" : component.get("v.recordId")
        });
        
        action.setCallback(this, function(response){
            $A.util.addClass(component.find("busyIndicator"), "slds-hide");
            
            var state = response.getState();
            
            if(component.isValid && state === 'SUCCESS') {
                console.log('SUCCESS');
                $A.util.removeClass(component.find("toast"), "slds-hide");
            } 
            else{
                console.log('Error'+response.getError());
                helper.handleErrors(component,response.getError());
                
            }                   
        });
        $A.enqueueAction(action);
    },
    
    close : function(component, event, helper) {
        $A.util.addClass(component.find("toast"), "slds-hide");
        $A.util.addClass(component.find("toastWarning"), "slds-hide");
        $A.util.addClass(component.find("toastError"), "slds-hide");
    },
    
    selectAll : function(component, event, helper) {
        var operatorList = component.get("v.operatorRecordsList");
        var maincheck = component.find("headerCheckBox").get("v.checked");
        var selectedOperators = [];
        
        if(maincheck) {
            for(var i=0; i<operatorList.length; i++) {
                if(!operatorList[i].isDisabled) {
                    operatorList[i].isOperatorExists = true;
                    selectedOperators.push(operatorList[i].operator);
                }   
            }
        }
        else {
            for(var i=0; i<operatorList.length; i++) {
                if(!operatorList[i].isDisabled) {
                    operatorList[i].isOperatorExists = false;
                }    
            }
        }
        
        component.set("v.operatorRecordsList", operatorList);
        component.set("v.checkboxList", maincheck);
        component.set("v.selectedOperators", selectedOperators);
    },
    
    checkboxChecked : function(component, event, helper) {
        helper.operatorSelection(component);       
    }
})