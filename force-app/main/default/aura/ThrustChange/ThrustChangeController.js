({
	onPageReferenceChange : function(component, event, helper) {
		helper.onPageReferenceChange(component, event);
	},
    
    cancel : function(component, event, helper) {
		helper.cancel(component, event);
	},
    
    save : function(component, event, helper) {
        if(helper.check(component)){ 
        	var modal = component.find("confirmationModal");
        	$A.util.removeClass(modal, 'slds-hide'); 
        }else{
            helper.showToast('Error','Please fill all fields!','ERROR');
        }
    },
    
    proceedToSave : function(component, event, helper) {
        helper.save(component, event);
    },
    
    closeConfirmationModal : function(component, event, helper) {
    	helper.closeModal(component, event, helper);
    },
    
    
    onDateChange: function(component, event, helper) {
        if(component.get("v.prevAssetTSN") == component.get("v.data.assetTSN") && 
          component.get("v.prevAssetCSN") == component.get("v.data.assetCSN") &&
          component.get("v.prevAssemblyTSN") == component.get("v.data.assemblyTSN") &&
          component.get("v.prevAssemblyCSN") == component.get("v.data.assemblyCSN"))
        	helper.onDateChange(component, event);
    }
})