({
    closeModal : function(component, event, helper){
        var modal = component.find("confirmationModal");
        $A.util.addClass(modal, 'slds-hide');
    },
	onPageReferenceChange : function(component, event) {
        
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        
        var myPageRef = component.get("v.pageReference");
        var recordId = myPageRef.state.c__recordId;
        component.set("v.recordId", recordId);
        
		var action = component.get('c.getAssemblyData');
        action.setParams({
            'recordId' : component.get('v.recordId')
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.assetId != null) {
                    component.set("v.isDisplay",true);
                    var errorBlock = component.find('errorBlock');
                    $A.util.addClass(errorBlock,"hideBlock");
                } else {
                    component.set("v.errorMessage","Assembly is not installed to any Asset, please install first!");
                    var errorBlock = component.find('errorBlock');
                    $A.util.removeClass(errorBlock,"hideBlock");
					 component.set("v.isDisplay",false);
                }
                component.set("v.data", result);
                component.set("v.selectedDate",today);
                component.set("v.todaydate", today);
				if(component.find("approvalStatus") != undefined){
					component.find("approvalStatus").set("v.value",result.approvalStatus[0]);
				}	
                component.set("v.prevAssetTSN", result.assetTSN);
                component.set("v.prevAssetCSN", result.assetCSN);
                component.set("v.prevAssemblyTSN", result.assemblyTSN);
                component.set("v.prevAssemblyCSN", result.assemblyCSN);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action); 	
	},
    
    //to cancel the form
    cancel : function(component, event) {
        
        var recordId = component.get('v.recordId');
        
        if(recordId != null) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
        
        component.set("v.data",[]);
        component.set("v.prevAssetTSN", null);
        component.set("v.prevAssetCSN", null);
        component.set("v.prevAssemblyTSN", null);
        component.set("v.prevAssemblyCSN", null);
		if(component.find("selectedThrust") != undefined){
        	component.find("selectedThrust").set("v.value", null);
        }  
		component.set("v.isDisplay",false);
    },
    
    //to save the record
    save : function(component, event) {
        component.set("v.data.currentTS",component.find("selectedThrust").get("v.value"));
        component.set("v.data.selectedApprovalStatus",component.find("approvalStatus").get("v.value"));
        var assemblyData = component.get('v.data');
        var action = component.get("c.saveData");
        action.setParams({
            assemblyData : JSON.stringify(assemblyData),
            userSelectedDate : component.get('v.selectedDate')
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
            	this.showToast("Success", "Record created Successfully!","SUCCESS");
                var recordId = response.getReturnValue();
                if(recordId != null) {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId
                    });
                    navEvt.fire();
                    setTimeout(function(){
                        $A.get('e.force:refreshView').fire();
                    },100);
                }
            } 
            else if(state === 'ERROR') {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
       
    },
    
    //to show spinner while the data is loading
    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    //to hide spinner after the data is loaded
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    //to show the toast message
    showToast : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
    check : function(component) {
        var tsn = component.get("v.data.assemblyTSN");
        var csn = component.get("v.data.assemblyCSN");
        var acTsn = component.get("v.data.assetTSN");
        var acCsn = component.get("v.data.assetCSN");
        var thrust = component.find("selectedThrust").get("v.value");

        if(component.get("v.selectedDate") && (tsn === 0 || tsn) && (csn === 0 || csn) &&
          (acTsn === 0 || acTsn) && (acCsn === 0 || acCsn) && thrust)
            return true;
        else
            return false;
    },
    
    onDateChange : function(component, event) {
        
        var assemblyData = component.get("v.data");
        var action = component.get("c.getTsnCsnValuesOnDateChange");
        this.showSpinner(component);
        action.setParams({
            assemblyData : JSON.stringify(assemblyData),
            userSelectedDate : component.get('v.selectedDate')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                component.set("v.data", result);
                component.set("v.prevAssetTSN", result.assetTSN);
                component.set("v.prevAssetCSN", result.assetCSN);
                component.set("v.prevAssemblyTSN", result.assemblyTSN);
                component.set("v.prevAssemblyCSN", result.assemblyCSN);
            } 
            else if(state === 'ERROR') {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    }
})