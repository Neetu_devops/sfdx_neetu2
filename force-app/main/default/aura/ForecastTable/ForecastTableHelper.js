({
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    applyStyleOnTable : function(component, event){
        var tblList = $('.forecast_DataTable');
        for(let i = 0 ; i < tblList.length; i++){
            this.applyFinalStyle(component, event, tblList[i]);
        }
    },
    applyFinalStyle:function(component, event, tbl){
        var parentWidth = 0;
        if($(tbl).parent().width() !== undefined){
            parentWidth = $(tbl).parent().width();
        }
        if(parentWidth == 0){
            parentWidth = screen.width; 
        }
        
        var pageUrl = location.href;
        var docHeight;
        if (pageUrl.indexOf('forecastsummary') != -1){
            var tabHeight = 40; // Tab height on the vf page
            var headerHeight = 55;
            docHeight = $(document).height() - tabHeight;
            $(".main_TableClass").css("height",docHeight);
            $('#tbltbody').css('height', docHeight - tabHeight - headerHeight);
        }
        else{
            if(localStorage.getItem('salesforceHeaderHeight') == undefined){
                localStorage.setItem('salesforceHeaderHeight', document.getElementById('main_TableID').offsetHeight + 10);
            }
            docHeight = $(document).height() - localStorage.getItem('salesforceHeaderHeight');
            $(tbl).find('#tbltbody').css('height', docHeight - 30);
        }
        
        $(tbl).css('width',parentWidth-10);
        $(tbl).find("#tblthead").css('width',parentWidth);
        $(tbl).find("#tbltbody").css('width',parentWidth);
        
        $(tbl).find("#tbltbody").scroll(function(e) { //detect a scroll event on the tbody
            //console.log('scrolling...');
            var scrollL = $(tbl).find("#tbltbody").scrollLeft();
            $(tbl).find('#tblthead').css("left", -scrollL);
            $(tbl).find('.firstCol').css("left", scrollL);
        });
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error",
            duration: '5000'
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    },
})