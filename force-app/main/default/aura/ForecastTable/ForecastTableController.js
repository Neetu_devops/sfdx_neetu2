({
    doInit:function(cmp,event,helper){
        var action = cmp.get("c.isForecastGenerated");
        action.setParams({
            "recordId": cmp.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.isForecastGenerated",response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    scriptsLoaded : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        var action = component.get("c.getForecastData");
        action.setParams({
            "recordId": component.get("v.recordId"),
            "loadHData": component.get("v.loadHData")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(component.isValid && state === 'SUCCESS') {
                var result = response.getReturnValue(); 
                component.set("v.forecast", result);
                console.log('result : ',result);
                var lstVal = component.get("v.forecast.lstComponentWrapper");
                component.set("v.monthListSize", lstVal.length);
                if (lstVal.length > 0){
                    component.set("v.lastMonth",lstVal[lstVal.length-1].startDate);
                    //console.log('test',component.get("v.lastMonth"));
                    helper.applyStyleOnTable(component, event);
                    var self = this;
                    setTimeout(function(){
                        if(result.lstAssemblyWrapper.length > 0){
                            for(var i = 0 ;i < result.lstAssemblyWrapper.length; i++){
                                if(result.lstAssemblyWrapper[i].lstAssemblyColumnsWrapper.length){
                                    for(var j = 1 ; j < result.lstAssemblyWrapper[i].lstAssemblyColumnsWrapper.length; j++){
                                        var innerHeader = result.lstAssemblyWrapper[i].assemblyType;
                                        //console.log('innerHeader'+innerHeader);
                                        $("."+innerHeader).addClass("slds-hide");
                                        $("."+innerHeader+"_topHeader").attr('colspan',0);
                                        $("."+innerHeader+"_forward").removeClass("slds-hide");
                                        $("."+innerHeader+"_backward").addClass("slds-hide");
                                    }
                                }
                            }
                        }
                    }, 3000);
                }
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
            helper.hideSpinner(component, event, helper);
        });
        $A.enqueueAction(action);
    },
    navigateToMyComponent : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:ForcastDashboard",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        evt.fire();
    },
    showHistoricaldata : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        var data = component.find("ShowButton");
        if(data.get("v.label") == "Hide Historical Data"){
            data.set("v.label","Show Historical Data");
            component.set("v.loadHData",false);
        }
        else{
            data.set("v.label","Hide Historical Data");
            component.set("v.loadHData",true);
        }
        
        var action = component.get("c.getForecastData");//fetchHistoricalData
        action.setParams({
            "recordId": component.get("v.recordId"),
            "loadHData": component.get("v.loadHData")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid && state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('result',result);
                component.set("v.forecast", result);
                var lstVal = component.get("v.forecast.lstComponentWrapper");
                if (lstVal.length > 0){
                    component.set("v.lastMonth",lstVal[lstVal.length-1].startDate);
                    
                    var self = this;
                    setTimeout(function(){
                        if(result.lstAssemblyWrapper.length > 0){
                            for(var i = 0 ;i < result.lstAssemblyWrapper.length; i++){
                                if(result.lstAssemblyWrapper[i].lstAssemblyColumnsWrapper.length){
                                    for(var j = 1 ; j < result.lstAssemblyWrapper[i].lstAssemblyColumnsWrapper.length; j++){
                                        var innerHeader = result.lstAssemblyWrapper[i].assemblyType;
                                        $("."+innerHeader).addClass("slds-hide");
                                        $("."+innerHeader+"_topHeader").attr('colspan',0);
                                        $("."+innerHeader+"_forward").removeClass("slds-hide");
                                        $("."+innerHeader+"_backward").addClass("slds-hide");
                                    }
                                }
                            }
                        }
                    }, 100);
                }
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
            helper.hideSpinner(component, event, helper);
        });
        $A.enqueueAction(action);
    },
    navigateToLLPTable : function(component, event, helper) {
        var llpRecordId = event.currentTarget.dataset.recid;
        component.set("v.monthValue","");
        
        let frontDrop = component.find("ShowLLPForecastTableFrontdrop");
        let backDrop = component.find("ShowLLPForecastTableBackdrop");
        $A.util.removeClass(frontDrop, "slds-hide");
        $A.util.removeClass(backDrop, "slds-hide");
        
        let ShowLLPForecast = component.find("ShowLLPForecast");
        if(ShowLLPForecast){
            ShowLLPForecast.destroy();
        }
        $A.createComponent(
            "c:ShowLLPForecastTable",
            {
                "aura:id": "ShowLLPForecast",
                "recordId": component.get("v.recordId"),
                "fxMonthlyOutputId": llpRecordId,
                "monthValue" : component.getReference("v.monthValue")
            },
            function(elemt, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('ShowLLPForecastTableDiv');
                    var body = targetCmp.get("v.body");
                    body.push(elemt);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },
    navigateToEOLATable : function(component, event, helper) {
        var EOLARecordId = event.currentTarget.dataset.recid;
        component.set("v.monthValue","");
        
        let frontDrop = component.find("ShowEOLAForecastTableFrontdrop");
        let backDrop = component.find("ShowEOLAForecastTableBackdrop");
        $A.util.removeClass(frontDrop, "slds-hide");
        $A.util.removeClass(backDrop, "slds-hide");
        
        let ShowEOLAForecast = component.find("ShowEOLAForecast");
        if(ShowEOLAForecast){
            ShowEOLAForecast.destroy();
        }
        $A.createComponent(
            "c:ShowEOLAForecastTable",
            {
                "aura:id": "ShowEOLAForecast",
                "recordId": component.get("v.recordId"),
                "fxMonthlyOutputId": EOLARecordId,
                "monthValue" : component.getReference("v.monthValue")
            },
            function(elemt, status, errorMessage){
                if (status === "SUCCESS") {
                    var targetCmp = component.find('ShowEOLAForecastTableDiv');
                    var body = targetCmp.get("v.body");
                    body.push(elemt);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },
    closePopup: function(component, event, helper) {
        let frontDrop = component.find("ShowLLPForecastTableFrontdrop");
        let backDrop = component.find("ShowLLPForecastTableBackdrop");
        $A.util.addClass(frontDrop, "slds-hide");
        $A.util.addClass(backDrop, "slds-hide");
    },
    closeEOLAPopup: function(component, event, helper) {
        let frontDrop = component.find("ShowEOLAForecastTableFrontdrop");
        let backDrop = component.find("ShowEOLAForecastTableBackdrop");
        $A.util.addClass(frontDrop, "slds-hide");
        $A.util.addClass(backDrop, "slds-hide");
    },
    handleClickShow : function(component, event, helper) {
        var arr = event.getSource().get("v.class").split(" ");
        var headerInd = arr[0];
        
        $("."+headerInd).removeClass("slds-hide");
        
        var reserve = component.get("v.forecast.reserveType");
        if(reserve == undefined)
            extType = '';
        if(reserve == 'MR'){
            if($("."+headerInd).hasClass("slds-hide")){
                $("."+headerInd+"_topHeader").attr('colspan',0);
            }else{
                $("."+headerInd+"_topHeader").attr('colspan',6);
            } 
        }else if(reserve == 'EOLA'){
            if($("."+headerInd).hasClass("slds-hide")){
                $("."+headerInd+"_topHeader").attr('colspan',0);
            }else{
                $("."+headerInd+"_topHeader").attr('colspan',4);
            } 
        }
            else{
                if($("."+headerInd).hasClass("slds-hide")){
                    $("."+headerInd+"_topHeader").attr('colspan',0);
                }else{
                    $("."+headerInd+"_topHeader").attr('colspan',6);
                }  
            }
        
        $("."+headerInd+"_forward").addClass("slds-hide");
        $("."+headerInd+"_backward").removeClass("slds-hide");
    },
    handleClickHide : function(component, event, helper) {
        var arr = event.getSource().get("v.class").split(" ");
        var headerInd = arr[0];
        console.log(arr);
        $("."+headerInd).addClass("slds-hide");
        if($("."+headerInd).hasClass("slds-hide")){
            $("."+headerInd+"_topHeader").attr('colspan',0);
        }else{
            $("."+headerInd+"_topHeader").attr('colspan',5);
        }
        
        $("."+headerInd+"_forward").removeClass("slds-hide");
        $("."+headerInd+"_backward").addClass("slds-hide");
    },
    openMSOAPopup: function(component, event, helper) {
        let frontDrop = component.find("ShowMSOForecastTableFrontdrop");
        let backDrop = component.find("ShowMSOForecastTableBackdrop");
        $A.util.removeClass(frontDrop, "slds-hide");
        $A.util.removeClass(backDrop, "slds-hide");
        var msRecordId = event.target.getAttribute("data-recId");
        console.log(JSON.stringify(msRecordId));
        console.log(component.get('v.forecast'));
    },
    closeMSOPopup: function(component, event, helper) {
        let frontDrop = component.find("ShowMSOForecastTableFrontdrop");
        let backDrop = component.find("ShowMSOForecastTableBackdrop");
        $A.util.addClass(frontDrop, "slds-hide");
        $A.util.addClass(backDrop, "slds-hide");
    },
    handleMouseLeave : function(component, event, helper) {
        //console.log('Mouse Leave');
        let helptext = event.currentTarget.dataset.recordtypid;
        let indexID = helptext.substring(helptext.indexOf('-') + 1,helptext.length);
        $('.'+helptext).removeClass("slds-rise-from-ground");
        $('.'+helptext).addClass("slds-fall-into-ground");
        //$(".forecast_DataTable").css("overflow",'hidden');
        component.set("v.isTootipLoaded",false);
    },
    handleMouseEnter : function(component, event, helper) {
        //console.log('Mouse Enter');
        let helptext = event.currentTarget.dataset.recordtypid;
        //console.log(helptext);
        let indexID = helptext.substring(helptext.indexOf('-') + 1,helptext.length);
        
        let columnIndexAssemblyType = helptext.split('-');
        let column = columnIndexAssemblyType[0];
        let rowIndex = columnIndexAssemblyType[1];
        let columnIndex = columnIndexAssemblyType[2];
		

        
        let lastRowIndex = component.get("v.forecast.lstComponentWrapper").length; 
        let lastColumnIndex = component.get("v.forecast.lstAssemblyWrapper").length;
        if(column == 'MSO'){
            if(columnIndex >= lastColumnIndex-2){
            	if(rowIndex == 0){
                    $('.'+helptext).addClass("slds-nubbin_top-right");  
                    $('.'+helptext).addClass("lastColumnTopRowsTooltip");
                } else if(rowIndex >= lastRowIndex-6){
                    $('.'+helptext).addClass("slds-nubbin_right-bottom"); 
                    $('.'+helptext).addClass("lastColumnBottomRowsTooltip");
                } else{
                    $('.'+helptext).addClass("slds-nubbin_right-top");
                    $('.'+helptext).addClass("lastColumnMiddleRowsTooltip");
                }     
            }else{
                if(rowIndex == 0){
                    $('.'+helptext).addClass("slds-nubbin_top-left");  
                    $('.'+helptext).addClass("topRowsTooltip");
                } else if(rowIndex >= lastRowIndex-5){
                    $('.'+helptext).addClass("slds-nubbin_left-bottom"); 
                    $('.'+helptext).addClass("bottomRowsTooltip");
                } else{
                    $('.'+helptext).addClass("slds-nubbin_left-top");
                    $('.'+helptext).addClass("middleRowsTooltip");
                }    
            }
                
        }
        
        
        $('.'+helptext).removeClass("slds-fall-into-ground");
        $('.'+helptext).addClass("slds-rise-from-ground");
        //$(".forecast_DataTable").css("overflow",'unset');
        component.set("v.isTootipLoaded",true);
        if(indexID >4){
        var calculatedTop = $('#child'+helptext).height()-20;
        $('#child'+helptext).attr('style','min-height:36px;left:90px;top:'+'-'+calculatedTop+'px;');
       }
    }
})