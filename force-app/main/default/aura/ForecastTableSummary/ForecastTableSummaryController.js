({
    doInit : function(component, event, helper) {
        helper.deliveryReturnCondition(component, event);
        helper.getIsForecastGenerated(component, event);
    },
    navigateToMyComponent : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:ForcastDashboard",
            componentAttributes: {
                recordId : component.get("v.recordId")
            }
        });
        evt.fire();
    }
})