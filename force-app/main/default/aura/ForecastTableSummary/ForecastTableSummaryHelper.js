({
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    deliveryReturnCondition : function(component, event){
        var action = component.get("c.getReserveType");
        var recordId = component.get("v.recordId");
        action.setParams({
            "scenarioInp": recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                if(dataObj != undefined){
                    console.log(dataObj);
                    if(dataObj != undefined){
                        component.set("v.checkEOLA",dataObj.isEOLA);
                        component.set("v.extType",dataObj.extType);
                        component.set("v.isLessor",dataObj.lessor);
                        component.set("v.reserveType",dataObj.reserveType);
                    }
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    getIsForecastGenerated : function(component, event){
        var action = component.get("c.isForecastGenerated");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isForecastGenerated",response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error",
            duration: '5000'
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });  
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){  
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );  
                        });  
                    };
                }
            });
        }
    },
})