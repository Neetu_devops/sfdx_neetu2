({
    doInit : function(component, event, helper) {
        helper.doInit(component, event, undefined);
    },
    
    reloadData : function(component, event, helper) {
        var nonMR = component.get("v.excessUtilizationData.nonMR");
        var type;
        
        if(nonMR){
            type = 'Assembly';
        }else{
            type = 'SR';
        }
        component.set("v.excessUtilizationData.excessUtilizationtableData", []);
        helper.doInit(component, event, type);
    },
    
    save : function(component, event, helper) {
        var invoiceDate = component.get('v.invoiceDate');
        if(invoiceDate == undefined || invoiceDate == ''){
            helper.showToast("Error", "Please select a Invoice Date.","Error");
            
        }else{
            helper.save(component, event);
        }
        
    },
   
    calculateTotalAmt : function(component, event, helper) {
        var indx = event.currentTarget.getAttribute("data-index");
        
        var excessUtilizationData = component.get('v.excessUtilizationData');
        var tableValues = excessUtilizationData.excessUtilizationtableData;
        var totalAmountDue = 0;
        
        
        if(tableValues[indx].maxUnits != undefined && tableValues[indx].maxUnits != ''
           && tableValues[indx].excessUnitBase != '--None--'){
            
            if(tableValues[indx].excessUnitBase == 'Hours' && tableValues[indx].maxUnits <= tableValues[indx].totalFH){
                
                tableValues[indx].excessUnits = tableValues[indx].totalFH - tableValues[indx].maxUnits;
                
            }else if(tableValues[indx].excessUnitBase == 'Cycles' && tableValues[indx].maxUnits <= tableValues[indx].totalFC){
                
                tableValues[indx].excessUnits = tableValues[indx].totalFC - tableValues[indx].maxUnits;
                
            }else{
                tableValues[indx].excessUnits = 0;
            }      
        }else{
            tableValues[indx].excessUnits = undefined;
        }
        
        
        if(tableValues[indx].excessRate != undefined && tableValues[indx].excessRate != '' 
           && tableValues[indx].excessUnits != undefined){
            
            tableValues[indx].totalAmount = tableValues[indx].excessUnits * tableValues[indx].excessRate;
            
            tableValues[indx].totalAmount = Math.round(tableValues[indx].totalAmount * 100) / 100;
                
        }else{
            tableValues[indx].totalAmount = undefined;
        }
        
        for(var i = 0; i < tableValues.length; i++){
            if(tableValues[i].totalAmount != undefined && tableValues[i].selectedAssembly){
                totalAmountDue += tableValues[i].totalAmount;
            }
        }
        
        excessUtilizationData.totalAmountDue = totalAmountDue;
        excessUtilizationData.excessUtilizationtableData = tableValues;
        component.set('v.excessUtilizationData',excessUtilizationData);
    }
})