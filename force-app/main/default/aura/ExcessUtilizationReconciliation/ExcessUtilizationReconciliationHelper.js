({
    doInit: function(component, event, type) {
        this.showSpinner(component);
        var action = component.get("c.getData");
        action.setParams({
            "recordId": component.get("v.reconciliationId"),
            "type" : type
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (response.getState() === "SUCCESS") {
                
                var allData =  response.getReturnValue();
                
                if(type == undefined){
                    
                component.set("v.excessUtilizationData", allData);
                var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
                component.set('v.invoiceDate', today);
                
                var screenHeight = screen.availHeight;
                screenHeight = (screenHeight * 65) / 100;
                component.set("v.screenHeight", screenHeight);
                    
                }else{
                    
                	component.set("v.excessUtilizationData.excessUtilizationtableData",
                                  allData.excessUtilizationtableData);  
                }

            }
            else if(state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinner",true);
    },
    
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinner",false);
    },
    
    //to show the toast message
    showToast : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    }, 
    
    save : function(component, event) {
       
        var excessUtilizationData = component.get("v.excessUtilizationData");
        
        var selectedExcessUtilizationData = [];
        var tableValues = excessUtilizationData.excessUtilizationtableData;
        var flag = false;
        
        for(var i = 0; i < tableValues.length; i++){
            if(tableValues[i].selectedAssembly){
                if(tableValues[i].totalAmount != undefined ){
                    selectedExcessUtilizationData.push(tableValues[i]);
                }else{
                    flag = true;
                    break;
                }
            }
        }
        if(flag){
            
            this.showToast("Error", "Please enter Required Fields.","Error");
            
        }else if(selectedExcessUtilizationData.length > 0){
            
            excessUtilizationData.excessUtilizationtableData = selectedExcessUtilizationData;
          
            if(excessUtilizationData.totalAmountDue == undefined || excessUtilizationData.totalAmountDue == 0){
                this.showToast("Error", "Invoice cannot be created with 0 amount.","Error");
            }else{
                
                this.showSpinner(component);
                var action = component.get("c.saveData");
                action.setParams({
                    data : JSON.stringify(excessUtilizationData),
                    invoiceDate : component.get("v.invoiceDate"),
                    paymentDate : component.get("v.paymentDate")
                });
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === 'SUCCESS') {
                        this.showToast("Success", "Invoice created successfully!","SUCCESS");
                        var recordId = response.getReturnValue();
                        if(recordId != null) {
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                                "recordId": recordId
                            });
                            navEvt.fire();
                        }     
                    }
                    else if(state === 'ERROR') {
                        var errors = response.getError();
                        this.handleErrors(errors);
                    }
                    this.hideSpinner(component);
                });
                $A.enqueueAction(action);
            }
        }else{
            this.showToast("Error", "Please select a row to generate Invoice.","Error");
        }
    },
    handleErrors: function (errors, type) {
        _jsLibrary.handleErrors(errors, type); // Calling the JS Library function to handle the error
    },
})