({
    summaryInfo : function(component, event) { 
        var recordId = component.get("v.recordId");
        if(recordId) {
            var action = component.get('c.getSavedData');
            action.setParams({
                recordId : recordId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var tableData = response.getReturnValue();
                    component.set("v.sourceAssemblyInfo", tableData.sourceObjectRecords);
                    this.displayInfo(component);
                }
            });
            $A.enqueueAction(action);
        } else {
            this.displayInfo(component);
        }
        
    },
    
    displayInfo : function(component) {
        var changedList = component.get("v.sourceAssemblyInfo");
        if(changedList.length > 0) {
            var displayData = [];
            var displayRightData = [];
            var displayLeftData = [];
            var checkLease = 0;
            var finalMap = {};
            var count = 0;
            
            for(var i=0; i<changedList.length; i++) {
                if(changedList[i].targetAmountAfterTransfer != null) {
                    var sourceId = changedList[i].sourceRecord.recordId;
                    var targetId = changedList[i].targetRecord.recordId;
                    
                    var sourceLeaseId = changedList[i].sourceLeaseId;
                    var targetLeaseId = changedList[i].targetLeaseId;
                    
                    var targetAmount = finalMap[targetId];
                    var sourceAmount = finalMap[sourceId];
                    
                    
                    if(!sourceAmount) {
                        count++;
                        var blankList = [];
                        var beforeBal = changedList[i].sourceRecord.availableAmount;
                        var afterBal = changedList[i].sourceAmountAfterTransfer;
                        var change = afterBal - beforeBal;
                        blankList.push({
                            record : 'source',
                            leaseName : changedList[i].sourceLeaseName,
                            name : changedList[i].sourceRecord.recordName+" (SN "+changedList[i].sourceRecord.serialNumber+")",
                            change : change,
                            afterBalance : afterBal
                        });
                        finalMap[sourceId] = blankList;
                    }
                    else {
                        var beforeBal = changedList[i].sourceRecord.availableAmount;
                        var afterBal = changedList[i].sourceAmountAfterTransfer;
                        var change = beforeBal - afterBal;
                        var beforeBal = finalMap[sourceId][0].afterBalance;
                        var afterBal = beforeBal - change;
                        finalMap[sourceId][0].afterBalance = afterBal
                        var beforeChange = finalMap[sourceId][0].change;
                        change = beforeChange - change;
                        finalMap[sourceId][0].change = change;
                        console.log("sourceBal: "+finalMap[sourceId][0].afterBalance+" "+i);
                    } 
                    
                    if(!targetAmount) {
                        count++;
                        var blankList = [];
                        var beforeBal = changedList[i].targetRecord.availableAmount;
                        var afterBal = changedList[i].targetAmountAfterTransfer;
                        var change = afterBal - beforeBal;
                        blankList.push({
                            record : 'target',
                            leaseName : changedList[i].targetLeaseName,
                            name : changedList[i].targetRecord.recordName+" (SN "+changedList[i].targetRecord.serialNumber+")",
                            change : change,
                            afterBalance : afterBal
                        });
                        finalMap[targetId] = blankList;
                    }
                    else {
                        var beforeBal = changedList[i].targetRecord.availableAmount;
                        var afterBal = changedList[i].targetAmountAfterTransfer;
                        var change = afterBal - beforeBal;
                        var beforeBal = finalMap[targetId][0].afterBalance;
                        var afterBal = beforeBal + change;
                        finalMap[targetId][0].afterBalance = afterBal
                        var beforeChange = finalMap[targetId][0].change;
                        change = change + beforeChange;
                        finalMap[targetId][0].change = change;
                    } 
                    
                    if(sourceLeaseId == targetLeaseId) {
                        //if source and terget lease is same
                        checkLease = 1;
                    }else checkLease = 2;
                }
            }
            
            if(checkLease == 1 && count > 0) {
                for(var key in finalMap) {
                    displayLeftData.push({
                        value : finalMap[key]
                    });
                }
                
            } else if(checkLease == 2 && count > 0) {
                for(var key in finalMap) {
                    if(finalMap[key][0].record == 'source'){
                        displayLeftData.push({
                            value : finalMap[key]
                        });
                    } else if(finalMap[key][0].record == 'target') {
                        displayRightData.push({
                            value : finalMap[key]
                        });
                    }
                }
            }
            
            if(displayLeftData.length > 0) {
                if(displayRightData.length > 0) {
                    for(var i=0; i<displayRightData.length; i++) {
                        displayData.push({
                            showRight : true,
                            leftValue : displayLeftData[i].value[0],
                            rightValue : displayRightData[i].value[0]
                        });
                    }
                    if(displayLeftData.length > displayRightData.length){
                        for(var i=displayRightData.length; i<displayLeftData.length; i++) {
                            displayData.push({
                                showRight : true,
                                leftValue : displayLeftData[i].value[0],
                                rightValue : []
                            });
                        }
                    }
                } else {
                    for(var i=0; i<displayLeftData.length; i++) {
                        displayData.push({
                            showRight : false,
                            leftValue : displayLeftData[i].value[0],
                            rightValue : []
                        });
                    }
                }
            }
            
            component.set("v.previewAssemblyInfo", displayData);
        }
    }
})