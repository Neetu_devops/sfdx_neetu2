({
    doInit: function (cmp, event, helper) {
        helper.loadInitData(cmp, event, helper);
    },
    handleValidateAction: function (cmp, event, helper) {
        let prePaymentData = cmp.get("v.prePaymentData");
        if (helper.checkUndefined(prePaymentData.prePaymentRec) || helper.checkUndefined(prePaymentData.prePaymentRec.Id)) {
            helper.handleMessage("Please select a Prepayment to be applied to the invoice.", "Warning");
        }
        else if (cmp.get("v.hasError")) {
            helper.handleMessage("Please review the 'Amount to be Applied' field.");
        }
        else {
            helper.handleSaveAction(cmp, event, helper);
        }
    },
    handleCancel: function (cmp, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    handlePrePaymentRecChange: function (cmp, event, helper) {
        let prePaymentData = cmp.get("v.prePaymentData");
        let prePaymentAmountApplied = cmp.get("v.prePaymentAmountApplied");
        var updateAmount = false;

        if (helper.checkUndefined(prePaymentAmountApplied)) { prePaymentAmountApplied = 0.0; }

        if (!helper.checkUndefined(prePaymentData.prePaymentRec) && !helper.checkUndefined(prePaymentData.prePaymentRec.Id) && !helper.checkUndefined(prePaymentData.prePaymentRec.Available_Balance_Cur__c)) {
            if (!helper.checkUndefined(prePaymentData.invoiceRec.Balance_Due__c) && prePaymentData.invoiceRec.Balance_Due__c > 0) {
                if (prePaymentData.prePaymentRec.Available_Balance_Cur__c > prePaymentData.invoiceRec.Balance_Due__c) {
                    prePaymentAmountApplied = prePaymentData.invoiceRec.Balance_Due__c;
                    updateAmount = true;
                }
                else {
                    prePaymentAmountApplied = prePaymentData.prePaymentRec.Available_Balance_Cur__c;
                    updateAmount = true;
                }
            }
        }
        else if (prePaymentAmountApplied !== prePaymentData.invoiceRec.Balance_Due__c) {
            prePaymentAmountApplied = prePaymentData.invoiceRec.Balance_Due__c;
            updateAmount = true;
        }
        if (updateAmount) {
            cmp.set("v.hasError", false);
            cmp.set("v.prePaymentAmountApplied", prePaymentAmountApplied);
        }
    },
    onAppliedAmountChange: function (cmp, event, helper) {
        let prePaymentData = cmp.get("v.prePaymentData");
        let prePaymentAmountApplied = cmp.get("v.prePaymentAmountApplied");

        if (helper.checkUndefined(prePaymentAmountApplied)) { prePaymentAmountApplied = 0.0; }

        if (prePaymentAmountApplied > 0 && !helper.checkUndefined(prePaymentData.invoiceRec.Balance_Due__c) && prePaymentData.invoiceRec.Balance_Due__c < prePaymentAmountApplied) {
            cmp.set("v.hasError", true);
        }
        else if (prePaymentAmountApplied > 0 && !helper.checkUndefined(prePaymentData.prePaymentRec.Available_Balance_Cur__c) && prePaymentData.prePaymentRec.Available_Balance_Cur__c < prePaymentAmountApplied) {
            cmp.set("v.hasError", true);
        }
        else {
            cmp.set("v.hasError", false);
        }
    }
})