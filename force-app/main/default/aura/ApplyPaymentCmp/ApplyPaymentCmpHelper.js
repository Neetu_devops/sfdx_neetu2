({
    loadInitData: function (component, event, helper) {
        this.showSpinner(component);
        let action = component.get("c.fetchInitData");
        action.setParams({
            "invoiceId": component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log('response', response.getReturnValue());
                component.set("v.prePaymentData", response.getReturnValue());
            } else {
                var errors = response.getError();
                this.handleMessage(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    handleSaveAction: function (component, event, helper) {
        this.showSpinner(component);
        let action = component.get("c.applyPrePayment");
        action.setParams({
            "prePaymentData": JSON.stringify(component.get("v.prePaymentData")),
            "prePaymentAmountApplied": component.get("v.prePaymentAmountApplied")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                this.handleMessage('Prepayment applied successfully', 'Success');
                $A.get("e.force:closeQuickAction").fire();
            } else {
                var errors = response.getError();
                this.handleMessage(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    checkUndefined: function (input) {
        if (input !== undefined && input !== '' && input !== null) {
            return false;
        }
        return true;
    },

    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    handleMessage: function (errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errors === 'object') {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message.replace(/&quot;/g, '"');
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message.replace(/&quot;/g, '"');
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message.replace(/&quot;/g, '"');
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
        else if (typeof errors === "string" && errors.length != 0) {
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})