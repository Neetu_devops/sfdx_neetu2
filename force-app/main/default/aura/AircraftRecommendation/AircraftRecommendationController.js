({
    doInit: function(component, event, helper) {
        var action = component.get('c.getDealType');
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            component.set('v.isMA', false);
            if(!$A.util.isEmpty(data) && !$A.util.isUndefined(data) && data == 'MA') 
                component.set('v.isMA', true);
        });
        $A.enqueueAction(action);

    },
    
    openPop : function(component, event, helper) {
        console.log("openPop ");
        var whichOne = event.target.style;
        var title = event.target.innerHTML;
        component.set("v.aircraftTitle", title);
        
        console.log("title "+title);
        
        var divId = event.target.id;
        var res = divId.split("aircraftId");
        var operatorId = res[1];
       // console.log("openPop id "+divId + " "+res +" operator id "+ res[1]);
        
        var aircraftPropList = component.get("v.aircraftRecomd");
        //console.log("openpop aircraftPropList "+JSON.stringify(aircraftPropList));
        var aircraftProp = aircraftPropList[operatorId];
       // component.set("v.aircraft", aircraftProp);
        //console.log("openpop aircraftProp "+JSON.stringify(aircraftProp));
        var params = JSON.stringify(aircraftProp);
       // console.log("openpop params "+params);
        component.set('v.showMore', false);
        component.set('v.moreLabel','More');
            
        var action = component.get('c.getAircraftCriteria');
        action.setParams({ 
            aircraft : params
        });        
        action.setCallback(this, function(response) {
            var aircraftData = response.getReturnValue();
          //  console.log('openPop airline '+aircraftData);
            if(aircraftData != null) {
                var leases = aircraftData.leasesList;
                var optionsList = [];
                if(!$A.util.isEmpty(leases) && !$A.util.isUndefined(leases)) {
                    for (var key in leases) {
                        if (leases.hasOwnProperty(key)) {
                            optionsList.push({value: key, label: leases[key]});
                        }
                    };
                    aircraftData.leasesList = optionsList;
                }
                var portfolio = aircraftData.leasesVsOwned;
                if(!$A.util.isEmpty(portfolio) && !$A.util.isUndefined(portfolio)) {
                    var recordLmt = component.get("v.recordLimit");
                    if(portfolio.length > recordLmt) 
                        component.set('v.moreData', true);
                } else{
                    component.set('v.moreData', false);
                }
            }
            component.set("v.aircraft", aircraftData);
       		helper.checkDealCreateStatus(component);
        });
        $A.enqueueAction(action);
        
        var cmpTarget = component.find('pop');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        var popup = component.find("pop").getElement();
        if(popup) {
            var offset = component.find('parentDiv').getElement().getBoundingClientRect().top + 
                document.documentElement.scrollTop ;
            var topPadding = (event.clientY + document.documentElement.scrollTop) - offset + 10;
            var leftOffset = component.find('parentDiv').getElement().getBoundingClientRect().left;
            var documentWidth = document.documentElement.clientWidth - 40;
            var left = event.clientX - leftOffset;
            
            popup.style.top = (topPadding + 25)+'px';
            //popup.style.left = -(leftOffset - 10)+'px';
            popup.style.left = (left- 600 )+'px';
            popup.style.display = "inline";
            popup.style.position = "absolute";
            //popup.style.width = documentWidth+ "px";
            component.set("v.isDetailPopVisible", true);
        }
        
    }, 
    
    onItemClick: function(component,event,helper){
        var aircraft = component.get("v.aircraft");
        console.log("onItemClick fleetId:" +aircraft.fleetId);
        window.open('/' + aircraft.fleetId);  
    },
    
    onDealClick: function(component,event,helper){
        var deal = component.get("v.dealId");
        console.log("ondealclick deal:" +deal);
        window.open('/' + deal);  
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        helper.closeDetailsPopup(component);
        component.set("v.isDetailPopVisible", false);
    }, 
    
    createMarketingActivity : function(component, event, helper) {
        console.log("createMarketingActivity ");
        
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        
        var operatorId = component.get("v.recordId");
        var msn = component.get("v.aircraftTitle");
        console.log("createMarketingActivity "+operatorId + " msn "+msn);
        var aircraft = component.get("v.aircraft");
        var action = component.get('c.newMarkActivity');
        action.setParams({
            operatorId : operatorId,
            msn : msn,
            fleetId : aircraft.fleetId
        });
        action.setCallback(this, function(response) {
            var airlineCriteria = response.getReturnValue();
            console.log('airline criteria response '+airlineCriteria);
            if(airlineCriteria != null)
                helper.showToast(component, event, helper, airlineCriteria);
            else
            	helper.showToast(component, event, helper, null);
        });
        $A.enqueueAction(action); 
    },
    
    onControllerFieldChange: function(component, event, helper) {     
        var controllerValueKey = event.getSource().get("v.value"); // get selected controller field value
        var depnedentFieldMap = component.get("v.depnedentFieldMap");
        var dependentEngineFieldMap = component.get("v.dependentEngineFieldMap");
        
        if (controllerValueKey != '--- ALL ---') {
            component.set("v.bDisabledDependent" , false); 
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            var ListOfDependentEngineFields = dependentEngineFieldMap[controllerValueKey];
            
            // for variant
            if(ListOfDependentFields.length > 0){
                component.set("v.bDisabledDependentFld" , false);  
                helper.fetchDepValues(component, ListOfDependentFields, 'Variant', false);    
            }else{
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['--- None ---']);
            }
            
            // for engine
            if(ListOfDependentEngineFields.length > 0){
                component.set("v.bDisabledDependentEngineFld" , false);  
                helper.fetchDepValues(component, ListOfDependentEngineFields, 'Engine', false);    
            }else{
                component.set("v.bDisabledDependentEngineFld" , true); 
                component.set("v.listDependingEngineValues", ['--- None ---']);
            }
            
        } else {
            component.set("v.listDependingValues", ['--- None ---']);
            component.set("v.listDependingEngineValues", ['--- None ---']);
            component.set("v.bDisabledDependentFld" , true);
            component.set("v.bDisabledDependentEngineFld" , true); 
            component.set("v.bDisabledDependent" , true); 
        }
    },
    
    onFilterClick : function(component,event,helper){
        var operator = component.get("v.recordId"); //component.find('operator').get("v.value");
    	var aircraftType = component.find('aircraftType').get("v.value");
        var variant = component.find('variant').get("v.value");
        var engine = component.find('engine').get("v.value");
        var mtow = component.find('mtow').get("v.value");
        var mtowRange = component.find('mtowRange').get("v.value");
        var vintage = component.find("vintageValue").get("v.value");
        var vintageRange = component.find("vintageRange").get("v.value");
        
        console.log('onFilterClick operator:'+operator+' type:'+aircraftType + ' variant:'+variant+' engine:'+engine);
        console.log('onFilterClick mtow:'+mtow + ' mtowRange:'+mtowRange+' vintage:'+vintage+ ' vintageRange:'+vintageRange);
        
        component.set("v.isLoading", true);
        
        if(aircraftType == '--- ALL ---' || aircraftType.includes('Aircraft_Type__c')) {
            aircraftType = null;
            variant = null;            
            engine = null;
            component.set("v.isBasedOnType" , false);
        }
        else{
            component.set("v.isBasedOnType" , true);
            if(variant == '--- None ---' || $A.util.isUndefined(variant) || variant.includes('Aircraft_Variant__c')) 
                variant = null;            
            if(engine == '--- None ---' || $A.util.isUndefined(engine) || engine.includes('Engine_Type__c')) 
                engine = null;
        }
        if (vintage == '-None-') 
            vintage = 0;
        if($A.util.isUndefined(vintage) || $A.util.isEmpty(vintage))
            vintage = 0;
        if($A.util.isUndefined(vintageRange) || $A.util.isEmpty(vintageRange))
            vintageRange = 0 ;
        if($A.util.isUndefined(mtow) || $A.util.isEmpty(mtow))
            mtow = 0;
        if($A.util.isUndefined(mtowRange) || $A.util.isEmpty(mtowRange))
            mtowRange = 0;
        console.log('onFilterClick inside type:'+aircraftType + ' variant:'+variant+' engine:'+engine);
        console.log('onFilterClick inside mtow:'+mtow + ' mtowRange:'+mtowRange+' vintage:'+vintage+ ' vintageRange:'+vintageRange);
        
            
        helper.displayAircraftPyramid(component, helper, operator, aircraftType, variant ,
                                      engine ,vintage , vintageRange, mtow ,mtowRange);
        
        
	},
    
    onClickMore: function(component, event, helper) {
        var flag = component.get("v.showMore");
        if(flag == true) 
            component.set("v.moreLabel", "More");
        else
            component.set("v.moreLabel", "Less");
        component.set("v.showMore", !flag);
    },
    
    onHoverOver: function(component, event, helper) {
       // console.log('onHoverOver ' + component.get("v.isPopVisible"));
        if(component.get("v.isDetailPopVisible") == true || component.get("v.isPopVisible") == true)
            return;
        component.set("v.isPopVisible", true);
        var title = event.target.innerHTML;
        component.set("v.aircraftTitle", title);
       
        var divId = event.target.id;
        var res = divId.split("aircraftId");
        var operatorId = res[1];
        
        var aircraftPropList = component.get("v.aircraftRecomd");
        var aircraftProp = aircraftPropList[operatorId];
       // console.log("onHoverOver aircraftProp "+JSON.stringify(aircraftProp));
        component.set('v.aircraftDetails', aircraftProp);
        
        
        var cmpTarget = component.find('aircraftDetailsPopup');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        var popup = component.find("aircraftDetailsPopup").getElement();
        if(popup) {
            var offset = component.find('parentDiv').getElement().getBoundingClientRect().top + 
                document.documentElement.scrollTop ;
            var topPadding = (event.clientY + document.documentElement.scrollTop) - offset + 10;
            var leftOffset = component.find('parentDiv').getElement().getBoundingClientRect().left;
            var documentWidth = document.documentElement.clientWidth - 40;
            var left = event.clientX - leftOffset;
            
            popup.style.top = (topPadding + 25)+'px';
            //popup.style.left = -(leftOffset - 10)+'px';
            popup.style.left = (left- 300 )+'px';
            popup.style.display = "inline";
            popup.style.position = "absolute";
            //popup.style.width = documentWidth+ "px";
        }
    },
    
    onHoverOut: function(component, event, helper) {
        helper.closeDetailsPopup(component);
    },
    
})