({
    checkComponentVisibility : function(component){
        var operatorId = component.get("v.recordId");
        var self = this;
        var action = component.get('c.getOperatorId');
        action.setParams({
            objectId: operatorId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("checkComponentVisibility : state "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("checkComponentVisibility : response "+result);
                if(result != null) {
                    component.set("v.recordId", result);
                    component.set('v.showComponent', true);
                    self.getNameSpacePrefix(component);
                    self.displayAircraftPyramid(component, self, result, null, null,null, 0, 0,0,0,0);
                }
                else{
                    component.set('v.showComponent', false);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("checkComponentVisibility errors "+errors);
                if (errors) {
                    self.handleErrors(errors);
                } 
            }
        });
        $A.enqueueAction(action);
    },
    
    
    getNameSpacePrefix : function(component){
        var action = component.get('c.getNameSpacePrefix');
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            component.set('v.namespace', namespace);
            console.log("getNameSpacePrefix  "+namespace);
            this.populateFilterValues(component, this);
        });
        $A.enqueueAction(action);
    },
    
    
    displayAircraftPyramid: function(component, helper,operatorId1, aircraftType1, variant1, engine1, 
                                     vintage1, vintageRange1, mtow1, mtowRange1) {
        console.log("displayAircraftPyramid: sending request");
        var action = component.get('c.getAircraftsRecommended');
        console.log('displayAircraftPyramid operatorId:'+operatorId1+' type:'+aircraftType1 + ' variant:'+variant1+' engine:'+engine1);
        console.log('displayAircraftPyramid mtow:'+mtow1 + ' mtowRange:'+mtowRange1+' vintage:'+vintage1+ ' vintageRange:'+vintageRange1);
        
        action.setParams({ 
            "operatorId" : operatorId1,
            "aircraftType" : aircraftType1,
            "variant" : variant1,
            "engine" : engine1,
            "vintage" : vintage1,
            "vintageRange" : vintageRange1,
            "mtow" : mtow1,
            "mtowRange" : mtowRange1
        });
        
        action.setCallback(this, function(response) {
            var aircraftRecomdList = response.getReturnValue();
            var previousData = component.get("v.aircraftRecomd");
            component.set("v.aircraftRecomd", aircraftRecomdList);
            var aircraftPropList = component.get("v.aircraftRecomd");
            if(aircraftPropList != null) {
                helper.removeNode();
                console.log("displayAircraftPyramid result length "+aircraftPropList.length);
                var rankColumn = new Map();
                rankColumn.set(1,3);
                rankColumn.set(2,4);
                rankColumn.set(3,5);
                rankColumn.set(4,6);
                rankColumn.set(5,7);
                rankColumn.set(6,8); 
                var colCount = 0;
                var rowIndex = 1;
                var previousRank = 1;
                for(var i = 0; i < aircraftPropList.length; i++) {
                    var aircraftProp = aircraftPropList[i];
                    var text = aircraftProp.aircraft ;
                    if(colCount == rankColumn.get(aircraftProp.rank) || previousRank != aircraftProp.rank) {
                        //go to next row 
                        if(colCount != rankColumn.get(aircraftProp.rank))
                            this.arrangeData(i,colCount,rankColumn.get(aircraftProp.rank - 1), component);
                        rowIndex++;
                        colCount = 0;
                        if(previousRank != aircraftProp.rank)
                        	previousRank = aircraftProp.rank;
                    }
                    colCount++;
                    var containerId = "col"+rowIndex;
                    //add div for rowIndex 
                    this.addDiv(aircraftProp,rankColumn.get(aircraftProp.rank) , i, containerId, component, aircraftProp.rank);
               
                }        
                if(colCount != rankColumn.get(aircraftProp.rank))
                    this.arrangeData(i,colCount,rankColumn.get(aircraftProp.rank), component);   
                component.set("v.isLoading", false);
            }
            else {
                console.log("displayAircraftPyramid result "+aircraftPropList);
                helper.removeNode();
                component.set("v.isLoading", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    removeNode : function(){
       for(var i = 1; i < 12; i++) {
            var id = "col"+i;
            var myNode = document.getElementById(id);
            //console.log("removeNode "+myNode);
            if(myNode != null) {
                while (myNode.firstChild) {
                    myNode.removeChild(myNode.firstChild);
                }
            }
        } 
    },
    
    arrangeData: function(index, childCount, ColCount, component) {
       // console.log("arrangeData "+index +" "+childCount+" "+ColCount);
        index--;
        for(var i = 0; i < childCount ; i++) {
            var cmpTarget = component.find("aircraftId" + index);
            if(cmpTarget.length != undefined)
                cmpTarget = component.find("aircraftId" + index)[cmpTarget.length - 1];
            var removeColClass = "slds-size_1-of-"+ColCount;
            var addColClass = "slds-size_1-of-"+childCount;
          //  console.log("arrangeData "+cmpTarget.length+ " removeColClass: "+removeColClass +" addColClass:"+addColClass);
         	$A.util.addClass(cmpTarget, addColClass);
            $A.util.removeClass(cmpTarget, removeColClass);
            index--;
        }
    },
    
    
    addDiv : function(aircraftProp, numOfItem, itemId, containerId, component, rank){
        var divCss = "col"+rank;
        var rankCss = "rank"+rank;
        var gridClass = "slds-col textLimit gridCustom "+divCss +" "+rankCss+" slds-size_1-of-"+numOfItem;
       // console.log("addDiv id dynamic "+containerId + " class "+gridClass);
        var type = aircraftProp.aircraftType;
        if($A.util.isEmpty(type) || $A.util.isUndefined(type)) 
            type = '-';
        var dom = aircraftProp.aircraftDom;
        if($A.util.isEmpty(dom) || $A.util.isUndefined(dom)) 
            dom = '-';
        
        var aircraftDetails = type + ' / '+ dom;
        var newComponents = [];
        newComponents.push(
            ["aura:html", 
             {  
                "aura:id":  "aircraftId"+itemId,
                tag: "div",
                HTMLAttributes:{
                    "id": "aircraftId"+itemId,
                    "class": gridClass,
                    "style" : "text-align:center;",
                    "onmouseout": component.getReference("c.onHoverOut")
                }
             }
            ]);
        newComponents.push(
            [
                "aura:html", 
                {  
                    tag: "div",
                    "body" :  aircraftProp.aircraft,
                    HTMLAttributes:{ 
                        "id": "aircraftId"+itemId,
                        "onmouseover": component.getReference("c.onHoverOver"),
                        "onclick": component.getReference("c.openPop"),
                        "value" :  aircraftProp.aircraft,
                        "style" : "text-align:center; font-size:28px; display:inline-block;pointer:cursor;"
                    }
                }
            ]);
        newComponents.push(
            [
                "aura:html", 
                {  
                    tag: "div",
                    HTMLAttributes:{ 
                        "style" : "text-align:center; font-size:12px;color:#666666;"
                    }
                }
            ]);
        newComponents.push(
            [
                "ui:outputText", { 
                    "id": "aircraftId"+itemId,
                    "value" : aircraftDetails
                    
                }
            ]);
     
        $A.createComponents(
            newComponents,
            function (components, status, errorMessage) {
                if (status === "SUCCESS") {
                    var container = component.find(containerId);
                    if (!$A.util.isUndefined(container) && container.isValid()) {
                        var body = container.get("v.body");
                        
                        var divMain = components[0];
                        var divMainBody = divMain.get("v.body");
                        var divChild = components[1];
                        var divChild2 = components[2];
                        var divChild2Body = divChild2.get("v.body");
                        var textType = components[3];
                        divChild2Body.push(textType);
                        divChild2.set("v.body", divChild2Body);
                        
                        divMainBody.push(divChild);
                        divMainBody.push(divChild2);
                        divMain.set("v.body", divMainBody);
                        body.push(divMain);
                        
                        container.set("v.body", body);
                    }
                }
            }
        );
    },
    
    showToast : function(component, event, helper, recordId) {
        var toastEvent = $A.get("e.force:showToast");
        
        if(recordId != null) {
            toastEvent.setParams({
                title: "Success!",
                message: "The record has been created successfully.",
                messageTemplate: '{1} has been created successfully.',
                messageTemplateData: ['Salesforce', {
                    url: '/'+recordId,
                    label: 'Deal',
                }],
                duration:'5000',
                key: 'info_alt',
                type: 'success',
                mode: 'dismissible'
            });
            
        }
        else{
            toastEvent.setParams({
                "title": "Failed!",
                "message": "Error in creating record",
                duration:'5000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
        }
        toastEvent.fire();
	},
    
    getVintagePicklist: function(component, elementId) {
        var action = component.get('c.getVintagePicklist');
        var self = this;
        var opts = [];
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            opts.push({"class": "optionClass", label: '-None-', value: 0});
            for(var i=0;i< response.getReturnValue().length; i++) {
                opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
        	}
            //console.log('getVintagePicklist '+component.find("vintageValue") + " "+elementId);
            component.find("vintageValue").set("v.options", opts); 
        });
        $A.enqueueAction(action);
    },
    
    fetchPicklistValues: function(component,objDetails,controllerField, dependentField, field) {
        var self = this;
        var action = component.get("c.getDependentMap");
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField 
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                if(field == 'Variant')
                	component.set("v.depnedentFieldMap",StoreResponse);
                else if(field == 'Engine')
                    component.set("v.dependentEngineFieldMap",StoreResponse);
                
                var listOfkeys = []; 
                var ControllerField = []; 
                
                for (var singlekey in StoreResponse) 
                    listOfkeys.push(singlekey);
                
                if (listOfkeys != undefined && listOfkeys.length > 0)  {
                    ControllerField.push({value: '--- ALL ---', selected: false});
                    for (var i = 0; i < listOfkeys.length; i++) 
                        ControllerField.push({value: listOfkeys[i], selected: false});
                }
                component.set("v.listControllingValues", ControllerField);
            }else{
                console.log('response.getState() == ERROR fetchPicklistValues');
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields, field, isFirstLoad) {
        if(field == 'Variant') {
            var dependentFields = [];
            dependentFields.push({value: '--- None ---', selected: false});
            for (var i = 0; i < ListOfDependentFields.length; i++) {
                if(isFirstLoad && variant != null && variant == ListOfDependentFields[i]) {
                    dependentFields.push({value: ListOfDependentFields[i], selected: true});
                    component.find('variant').set("v.value", ListOfDependentFields[i]);
                }
                else
                    dependentFields.push({value: ListOfDependentFields[i], selected: false});
            }
            component.set("v.listDependingValues", dependentFields);
        }
        else if(field == 'Engine') {
            var dependentFields = [];
            dependentFields.push({value: '--- None ---', selected: false});
            for (var i = 0; i < ListOfDependentFields.length; i++) {
                if(isFirstLoad && engine != null && engine == ListOfDependentFields[i]) {
                    dependentFields.push({value: ListOfDependentFields[i], selected: true});
                    component.find('engine').set("v.value", ListOfDependentFields[i]);
                }
                else
                    dependentFields.push({value: ListOfDependentFields[i], selected: false});
            }
        	component.set("v.listDependingEngineValues", dependentFields);
        }
    },
    
    populateFilterValues : function(component,  helper) {
        var controllingFieldAPI = component.get("v.controllingFieldAPI");
        var dependingFieldAPI = component.get("v.dependingFieldAPI");
        var dependingEngineFieldAPI = component.get("v.dependingEngineFieldAPI");
        var objDetails = component.get("v.objDetail");
        helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingFieldAPI, 'Variant');
        helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingEngineFieldAPI, 'Engine');
        
        helper.getVintagePicklist(component, "vintageValue");
    },
    
    checkDealCreateStatus : function(component){
        var action = component.get("c.isDealAlreadyCreated");
        var aircraft = component.get("v.aircraft");
        console.log("checkDealCreateStatus "+aircraft.fleetId +" fleetId: "+component.get("v.recordId"));
        action.setParams({
            operatorId : component.get("v.recordId"),
            assetId : aircraft.fleetId
        });
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            console.log("checkDealCreateStatus data: "+data);
            if(data != null) {
                component.set("v.isDealExist", true);
                component.set("v.dealId", data);
            }
            else
                component.set("v.isDealExist", false);
        });
      	$A.enqueueAction(action);  
    },
    
    closeDetailsPopup: function(component){
        var cmpTarget = component.find('aircraftDetailsPopup');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        component.set("v.isPopVisible", false);
      //  console.log('onHoverOut ' + component.get("v.isPopVisible"));
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})