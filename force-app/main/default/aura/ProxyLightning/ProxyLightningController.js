({
	doInit : function(component, event, helper) {
		
		// conditional creation of component
		// map of attributes
		// addd to the facet             
     	var compName = component.get("v.componentTypeName");
        var compAttribute = component.get("v.componentAttribute");
        console.log("compName====>" + compName);
        console.log("compAttribute====>" ,compAttribute);
        //compAttribute["press"] = component.get("c.handlePress");
        console.log("Attributes=====>",compAttribute);
        $A.createComponent(
            compName,
            compAttribute,
            function(newComp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var facet1 = component.get("v.facet");
                    console.log("facet===>",facet1);
                    facet1.push(newComp);
                    component.set("v.facet", facet1);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    }

	
})