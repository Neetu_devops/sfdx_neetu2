({
    getEditModeAttributes: function (component, event, helper) {
        var action = component.get("c.getEditCondition");
        action.setParams({
            snapshotId: component.get("v.snapshotId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Response: ', response.getReturnValue());
                component.set("v.editWrapper", response.getReturnValue());
            }
            else if (state === "ERROR") {
                this.handleErrors(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    getAssemblyBasic: function (component) {
        component.set("v.showSpinner", true);

        var action = component.get("c.getAssemblyDetails");
        action.setParams({
            recordId: component.get("v.recordId"),
            snapshotId: component.get("v.snapshotId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.assemblyBasicDetails", allValues);
               
                    if(allValues.TSN != undefined && allValues.TSN != '' && allValues.TSN != null){
                    	allValues.TSN = Number(allValues.TSN).toFixed(2); 
                	}
                    if(allValues.CSN != undefined && allValues.CSN != '' && allValues.CSN != null){
                    	allValues.CSN = Number(allValues.CSN).toFixed(2); 
                	}
                	if(allValues.LLPWrapper.length > 0){
                        allValues.LLPWrapper.map(llp => {
                            if(llp.TSN != undefined && llp.TSN != '' && llp.TSN != null){
                                llp.TSN = Number(llp.TSN).toFixed(2); 
                            }
                            if(llp.CSN != undefined && llp.CSN != '' && llp.CSN != null){
                                llp.CSN = Number(llp.CSN).toFixed(2); 
                            }
                      	})
                  	 } 
                
                if (allValues.LLPWrapper.length == 0) {
                    var thrustList = component.get("v.ThrustUnqList");
                    thrustList.push({ "ThrustId": "test", "ThrustName": "Test " });
                    component.set("v.ThrustUnqList", thrustList);

                    component.set("v.ThrustPicklist", allValues.ThrustUniqueWrapper);
                    var thrustPicklist = component.get("v.ThrustPicklist");

                    var thrust = [];
                    thrust.push({
                        Id: null,
                        Name: null,
                        ThrustId: thrustPicklist[0].ThrustId,
                        ThrustName: thrustPicklist[0].ThrustId,
                        Cycles: null,
                        CycleLimit: null,
                        RemainingCycle: null
                    });
                    allValues.LLPWrapper.push({
                        Id: null,
                        Name: null,
                        PartNumber: null,
                        SerialNumber: null,
                        TSN: null,
                        CSN: null,
                        SortOrder: null,
                        CurrentThrust: null,
                        Thrust: thrust
                    });
                    component.set("v.llpWrapper", allValues.LLPWrapper);
                    component.set("v.showSpinner", false);
                }
                else {
                    component.set("v.llpWrapper", allValues.LLPWrapper);
                    component.set("v.dupllpWrapper", JSON.stringify(allValues.LLPWrapper));

                    var thrusts = [];
                    if (allValues.LLPWrapper.length > 0) {
                        component.set("v.childList", allValues.LLPWrapper[0].Thrust);
                    }
                    component.set("v.namespace", allValues.namespacePrefix);
                    component.set("v.ThrustPicklist", allValues.ThrustUniqueWrapper);
                    component.set("v.InitialThrustPicklist", allValues.ThrustUniqueWrapper);
                    component.set("v.ThrustUnqList", allValues.ThrustCurrentWrapper);

                    if (component.get("v.loadedFromSnapshot")) {
                        for (let key in component.get('v.ThrustUnqList')) {
                            thrusts.push(component.get('v.ThrustUnqList')[key].ThrustName);
                        }
                        let finalList = [];
                        for (let key in allValues.ThrustUniqueWrapper) {
                            if (!thrusts.includes(allValues.ThrustUniqueWrapper[key].ThrustName))
                                finalList.push(allValues.ThrustUniqueWrapper[key]);
                        }
                        component.set("v.ThrustPicklist", finalList);
                    }

                    component.set("v.showSpinner", false);
                    if (allValues != null && allValues.ThrustCurrentWrapper != null && allValues.ThrustCurrentWrapper.length > 0) {
                        component.set("v.currentThrustName", allValues.ThrustCurrentWrapper[0].ThrustName);
                        component.set("v.ThrustCurrent", allValues.ThrustCurrentWrapper[0].Id);
                        console.log('ThrustCurrent: ',allValues.ThrustCurrentWrapper[0]);
                    }
                }
            }
            else if (state === "ERROR") {
                this.handleErrors(response.getError());
                component.set("v.showSpinner", false);
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    getThrustList: function (component) {
        var self = this;
        var assBasDet = component.get("v.assemblyBasicDetails");
        var thrustId = null, thrustName = null;
        if (!$A.util.isEmpty(assBasDet) && !$A.util.isUndefined(assBasDet)) {
            thrustId = assBasDet.ThrustId;
            thrustName = assBasDet.ThrustName;
        }
        var action = component.get("c.getThrust");
        action.setParams({
            assemblyId: component.get("v.recordId"),
            currentThrustId: thrustId,
            currentThrustName: thrustName
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.ThrustUnqList", allValues);
                if (allValues != null)
                    component.set("v.ThrustCurrent", allValues[0].ThrustId);
            }
            else if (state === "ERROR") {
                this.handleErrors(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    saveLLPRecords: function (component) {
        var action = component.get("c.saveLLPList");
        var llpWrapper = component.get("v.llpWrapper");
        var assemblyId = component.get("v.recordId");
        action.setParams({
            'llpWrapper': JSON.stringify(llpWrapper),
            'assemblyId': assemblyId,
            'currentThrustId': component.get("v.assemblyBasicDetails").ThrustId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isEdit", false);
                this.getAssemblyBasic(component);
            }
            else if (state === "ERROR") {
                this.handleErrors(response.getError());
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    deleteLLPData: function (component) {
        var llpWrapper = component.get("v.llpWrapper");
        var deleteIndex = component.get("v.deleteIndex");
        var recordId;
        if (component.get("v.loadedFromLLP") === true) {
            recordId = llpWrapper[deleteIndex].LLSnapId;
        }
        else {
            recordId = llpWrapper[deleteIndex].Id;
        }
        var action = component.get("c.deleteLLP");
        var self = this;
        action.setParams({
            llpId: recordId,
            isSnapshotComp: component.get("v.loadedFromLLP")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                llpWrapper.splice(deleteIndex, 1);
                component.set("v.llpWrapper", llpWrapper);
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                this.handleErrors(response.getError());
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    resetEdit: function (component) {
        component.set("v.showSpinner", true);
        component.set("v.rowIndex", 0);
        component.set("v.isEdit", false);
        component.set("v.newThrustCount", 0);
        component.set("v.newThrustForExistingLLP", []);
        component.set("v.showThrustAddIcon", true);
        component.set("v.showLLPAddIcon", true);
        setTimeout(function () { component.set("v.showSpinner", false); }, 2000);
    },

    saveSorting: function (component) {
        var Table = document.getElementById('tblLocations');
        //gets rows of table
        var rowLength = document.getElementById("tblLocations").rows.length;
        var LLPDataList = component.get("v.llpWrapper");
        //loops through rows
        var index = 0;
        var newLLPList = [];
        var ThrustUnqList = component.get("v.ThrustUnqList");

        for (var i = 3; i < rowLength; i++) {
            var rowId = Table.rows.item(i).id;
            if (rowId.length == 0 || rowId == null || rowId == '') { //new record
                index = index + 1;
                var row = Table.rows.item(i);
                var newLLP = {};
                var thrustList = [];
                var thrustIndex = 0;
                var thrust = {};
                var thrustUnqIndex = 0;

                for (var j = 2, col; col = row.cells[j]; j++) {
                    //LLP Data
                    var data = '';
                    if ($A.util.isEmpty(data) || $A.util.isUndefined(data))
                        data = null;
                    if (j == 2)
                        newLLP.IIN = data;
                    if (j == 3)
                        newLLP.Name = data;
                    if (j == 4)
                        newLLP.PartNumber = data;
                    if (j == 5)
                        newLLP.SerialNumber = data;
                    if (j == 6)
                        newLLP.TSN = data;
                    if (j == 7)
                        newLLP.CSN = data;

                    if (j > 7) {
                        if (thrustIndex == 0) {
                            thrust.Cycles = data;
                            thrust.ThrustId = ThrustUnqList[thrustUnqIndex].ThrustId;
                        }
                        if (thrustIndex == 1)
                            thrust.CycleLimit = data;
                        if (thrustIndex == 2)
                            thrust.RemainingCycle = data;
                        thrustIndex++;
                        if (thrustIndex == 3) {
                            thrustList.push(thrust);
                            thrust = {};
                            thrustIndex = 0;
                            thrustUnqIndex++;
                        }
                    }
                }
                newLLP.Thrust = thrustList;
                newLLP.SortOrder = index;
                newLLPList.push(newLLP);
            }
            if (!$A.util.isEmpty(rowId) && !$A.util.isUndefined(rowId) && rowId != 'header') {
                index = index + 1;
                if (!$A.util.isEmpty(LLPDataList) && !$A.util.isUndefined(LLPDataList)) {
                    for (var j = 0; j < LLPDataList.length; j++) {
                        if (LLPDataList[j].Id == rowId) {
                            LLPDataList[j].SortOrder = index;
                        }
                    }
                }
            }
        }
        if (!$A.util.isEmpty(LLPDataList) && !$A.util.isUndefined(LLPDataList)) {
            component.set("v.newLLPList", newLLPList);
        }
        if (!$A.util.isEmpty(LLPDataList) && !$A.util.isUndefined(LLPDataList)) {
            component.set("v.llpWrapper", LLPDataList);
        }
    },

    disableTableDrag: function (component, event, helper) {
        jQuery("document").ready(function () {
            var $sortableTable = $("#tblLocations tbody");
            $sortableTable.sortable("disable");
        });
    },

    enableTableDrag: function (component, event, helper) {
        jQuery("document").ready(function () {
            var $sortableTable = $("#tblLocations tbody");
            $sortableTable.sortable("enable");
        });
    },

    showErrorMsg: function (component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration: '5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    changeRowHighlight: function (component, rowId) {
        if (!$A.util.isEmpty(rowId) && !$A.util.isUndefined(rowId))
            component.set("v.highlightedRow", rowId);
        var table = document.getElementById('tblLocations');
        for (var i = 0, row; row = table.rows[i]; i++) {
            if (i == rowId) {
                table.rows[i].style = "background-color:#f3f2f2;";
            }
            else {
                table.rows[i].style = "background-color:;";
            }

        }
    },

    addNewThrustColumn: function (component) {
        if (component.get("v.ThrustPicklist").length > 0) {
            var thrustList = component.get("v.ThrustUnqList");
            thrustList.push({ "ThrustId": "test", "ThrustName": "testName" });
            component.set("v.ThrustUnqList", thrustList);

            var thrustPicklist = component.get("v.ThrustPicklist");
            var llpWrapper = component.get("v.llpWrapper");
            for (var i = 0; i < llpWrapper.length; i++) {
                llpWrapper[i].Thrust.push({
                    Id: null,
                    Name: thrustPicklist[0].ThrustName.trim(),
                    ThrustId: thrustPicklist[0].ThrustId,
                    ThrustName: thrustPicklist[0].ThrustName.trim(),
                    isNewThrust: true,
                    Cycles: null,
                    CycleLimit: null,
                    RemainingCycle: null
                });
            }

            component.set("v.llpWrapper", llpWrapper);
            if (llpWrapper.length > 0) {
                component.set("v.childList", llpWrapper[0].Thrust);
            }
            this.top(component, event);
        }
        else {
            this.showErrorMsg(component, "You can not add more thrusts");
        }
    },

    top: function (component, event) {
        let top = document.getElementById('tblLocations').lastElementChild;
        top.scrollIntoView({ behavior: "smooth", block: "center", inline: "end" });
    },

    addNewLLPRow: function (component) {
        var llpWrapper = component.get("v.llpWrapper");
        var thrustList = component.get("v.ThrustUnqList");
        var assBasDet = component.get("v.assemblyBasicDetails");
        var thrust = [];

        for (var i = 0; i < thrustList.length; i++) {
            thrust.push({
                Id: null,
                Name: thrustList[i].ThrustName,
                ThrustId: thrustList[i].ThrustId,
                ThrustName: thrustList[i].ThrustName,
                Cycles: null,
                CycleLimit: null,
                RemainingCycle: null
            });
        }

        llpWrapper.push({
            Id: null,
            Name: null,
            PartNumber: null,
            SerialNumber: null,
            TSN: null,
            CSN: null,
            SortOrder: null,
            CurrentThrust: null,
            Thrust: thrust
        });
        component.set("v.llpWrapper", llpWrapper);
    },

    validateInput: function (component, event, helper, saveLLPSnap) {
        var llpWrapper = component.get("v.llpWrapper");
        var thrustPicklist = component.get("v.ThrustUnqList");
        var errorList = [];
        for (var i = 0; i < llpWrapper.length; i++) {
            for (var j = 0; j < thrustPicklist.length; j++) {
                var Cycles = llpWrapper[i].Thrust[j].Cycles;
                var CycleLimit = llpWrapper[i].Thrust[j].CycleLimit;
                if (!$A.util.isEmpty(Cycles) && !$A.util.isUndefined(Cycles)) {
                    if (CycleLimit == null || CycleLimit == undefined || CycleLimit == '' || CycleLimit <= 0 || Cycles < 0) {
                        if (component.get("v.loadedFromSnapshot") && component.get("v.loadedFromLLP"))
                            errorList.push(llpWrapper[i].Thrust[j].ThrustName);
                        else
                            errorList.push(llpWrapper[i].Thrust[j].Id);
                    }
                }
                else {
                    if (Cycles == null || Cycles == undefined || Cycles == '' || Cycles <= 0) {
                        if (component.get("v.loadedFromSnapshot") && component.get("v.loadedFromLLP"))
                            errorList.push(llpWrapper[i].Thrust[j].ThrustName);
                        else
                            errorList.push(llpWrapper[i].Thrust[j].Id);
                    }
                }
            }
        }
        var uniqueThrusts = [];
        var duplicatesThrusts = [];
        for (let key in llpWrapper[0].Thrust) {
            if (uniqueThrusts.includes(llpWrapper[0].Thrust[key].ThrustName)) {
                duplicatesThrusts.push(llpWrapper[0].Thrust[key].ThrustName);
            }
            else {
                uniqueThrusts.push(llpWrapper[0].Thrust[key].ThrustName);
            }
        }
        if (errorList.length == 0 && duplicatesThrusts.length == 0) {
            if (saveLLPSnap)
                this.saveLLPSnap(component);
            else
                this.saveLLPRecords(component);
        } else {
            if (errorList.length > 0)
                this.showErrorMsg(component, "Limit/Cycles cannot be null or 0 or negative value when LLP has usage on this Thrust Setting");
            else if (duplicatesThrusts.length > 0) {
                this.showErrorMsg(component, "You cannot add duplicate thrusts, Please use a different thrust.");
            }
            component.set("v.isLoading", false);
        }
    },

    saveLLPSnap: function (component) {
        component.set("v.showSpinner", true);
        var llpWrapper = component.get("v.llpWrapper");
        let thrustPicklist = component.get("v.InitialThrustPicklist");
        let currentThrustName = component.get("v.currentThrustName");
        let thrustId = '';
        for (let key in thrustPicklist) {
            if (thrustPicklist[key].ThrustName === currentThrustName) {
                thrustId = thrustPicklist[key].ThrustId;
                break;
            }
        }
        var action = component.get("c.saveUpdateLLPSnapshotDetails");
        action.setParams({
            wrapperDataList: JSON.stringify(llpWrapper),
            assemblyUtilId: component.get("v.snapshotId"),
            updateToAssembly: component.get("v.saveAndUpdate"),
            currentThrustId: thrustId
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isEdit", false);
                component.set("v.loadedFromLLP", true);
                component.set("v.updateLLPIcon", true);
                if (component.get("v.saveAndUpdate")) {
                    this.handleErrors('Assembly LLPs were successfully updated', 'Success');
                    component.set("v.isLLPUpdated", true);
                    component.set("v.saveAndUpdate", true);
                }
                this.getAssemblyBasic(component);
            }
            else if (state === "ERROR") {
                this.handleErrors(response.getError());
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    
    handleErrors: function (errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errors === 'object') {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });
                    };
                }
            });
        }
        else if (typeof errors === "string" && errors.length != 0) {
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})