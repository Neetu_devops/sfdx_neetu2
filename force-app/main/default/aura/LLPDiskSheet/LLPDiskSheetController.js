({
    doInit: function (component, event, helper) {
        if (component.get("v.assemblyUtilId") == component.get("v.snapshotId")) {
            component.set("v.isLLPUpdated", true);
            component.set("v.saveAndUpdate", true);
        }
        component.set("v.showSpinner", true);
        let snapId = component.get("v.snapshotId");
        console.log('Snapshot Id: ', component.get("v.snapshotId"));
        if (snapId != undefined && snapId != '' && snapId != null) {
            helper.getEditModeAttributes(component, event, helper);
        }
        helper.getAssemblyBasic(component);
    },

    cancelDialog: function (component, event, helper) {
        component.set("v.showModal", false);
        component.set("v.reloadCmp", true);
    },

    changeTextColor: function (component, event, helper) {
        var id = event.getSource().get("v.name");
        var fieldName = id.split('-')[0];
        let llpWrapper = component.get("v.llpWrapper");
        if (fieldName == 'tsn') {
            let index = id.split('-')[1];
            llpWrapper[index].isTSNchanged = true;
        }
        if (fieldName == 'cyc') {
            let cycIndex = id.split('-')[1];
            let index = id.split('-')[2];
            llpWrapper[index].Thrust[cycIndex].isCyclesChanged = true;
        }
        if (fieldName == 'lim') {
            let limIndex = id.split('-')[1];
            let index = id.split('-')[2];
            llpWrapper[index].Thrust[limIndex].isLimitChanged = true;
        }
        document.getElementById(id).classList.add('textcolor');
    },

    handleCYC: function (component, event, helper) {
        var cyc = event.getSource().get("v.name");
        var cycval = event.getSource().get("v.value");
        var originalWrapper = component.get("v.llpWrapper");
        var llpWrapper = JSON.parse(component.get("v.dupllpWrapper"));
        for (let key in originalWrapper) {
            if (originalWrapper[key].Thrust != undefined && originalWrapper[key].Thrust.length > 0) {
                if (cycval != undefined && cycval != '' && cycval != null) {
                    if (llpWrapper[key] != undefined && llpWrapper[key] != null) {
                        if (llpWrapper[key].Thrust[cyc] == undefined) {
                            if (!originalWrapper[key].Thrust[cyc].isCyclesChanged)
                                originalWrapper[key].Thrust[cyc].Cycles = parseFloat(cycval);
                        } else {
                            if (!originalWrapper[key].Thrust[cyc].isCyclesChanged)
                                originalWrapper[key].Thrust[cyc].Cycles = parseFloat(llpWrapper[key].Thrust[cyc].Cycles) + parseFloat(cycval);
                        }
                    } else {
                        if (llpWrapper[key] == undefined) {
                            originalWrapper[key].Thrust[cyc].Cycles = parseFloat(cycval);
                        }
                    }
                } else {
                    if (llpWrapper[key] != undefined && llpWrapper[key] != null) {
                        if (llpWrapper[key].Thrust[cyc] != undefined) {
                            if (!originalWrapper[key].Thrust[cyc].isCyclesChanged)
                                originalWrapper[key].Thrust[cyc].Cycles = parseFloat(llpWrapper[key].Thrust[cyc].Cycles);
                        } else {
                            if (!originalWrapper[key].Thrust[cyc].isCyclesChanged)
                                originalWrapper[key].Thrust[cyc].Cycles = 0;
                        }
                    }
                    if (llpWrapper[key] == undefined) {
                        originalWrapper[key].Thrust[cyc].Cycles = 0;
                    }
                }
            }
        }
        component.set("v.llpWrapper", originalWrapper);
    },

    handleLIM: function (component, event, helper) {
        var lim = event.getSource().get("v.name");
        var limval = event.getSource().get("v.value");
        var originalWrapper = component.get("v.llpWrapper");
        var llpWrapper = JSON.parse(component.get("v.dupllpWrapper"));
        for (let key in originalWrapper) {
            if (originalWrapper[key].Thrust != undefined && originalWrapper[key].Thrust.length > 0) {
                if (limval != undefined && limval != '' && limval != null) {
                    if (llpWrapper[key] != undefined && llpWrapper[key] != null) {
                        if (llpWrapper[key].Thrust[lim] == undefined) {
                            if (!originalWrapper[key].Thrust[lim].isLimitChanged)
                                originalWrapper[key].Thrust[lim].CycleLimit = parseFloat(limval);
                        } else {
                            if (!originalWrapper[key].Thrust[lim].isLimitChanged)
                                originalWrapper[key].Thrust[lim].CycleLimit = parseFloat(llpWrapper[key].Thrust[lim].CycleLimit) + parseFloat(limval);
                        }
                    } else {
                        if (llpWrapper[key] == undefined) {
                            originalWrapper[key].Thrust[lim].CycleLimit = parseFloat(limval);
                        }
                    }
                } else {
                    if (llpWrapper[key] != undefined && llpWrapper[key] != null) {
                        if (llpWrapper[key].Thrust[lim] != undefined) {
                            if (!originalWrapper[key].Thrust[lim].isLimitChanged)
                                originalWrapper[key].Thrust[lim].CycleLimit = parseFloat(llpWrapper[key].Thrust[lim].CycleLimit);
                        } else {
                            if (!originalWrapper[key].Thrust[lim].isLimitChanged)
                                originalWrapper[key].Thrust[lim].CycleLimit = 0;
                        }
                    }
                    if (llpWrapper[key] == undefined) {
                        originalWrapper[key].Thrust[lim].CycleLimit = 0;
                    }
                }
            }
        }
        component.set("v.llpWrapper", originalWrapper);
    },
    updateTSN: function (component, event, helper) {
        var updatedTSN = event.getSource().get("v.value");
        var originalWrapper = component.get("v.llpWrapper");
        var llpWrapper = JSON.parse(component.get("v.dupllpWrapper"));
        for (let key in originalWrapper) {
            if (!originalWrapper[key].isTSNchanged) {
                if (updatedTSN != undefined && updatedTSN != '' && updatedTSN != null) {
                    if (llpWrapper[key] != undefined) {
                        if (llpWrapper[key].TSN != undefined && llpWrapper[key].TSN != null)
                            originalWrapper[key].TSN = parseFloat(llpWrapper[key].TSN) + parseFloat(updatedTSN);
                        else
                            originalWrapper[key].TSN = parseFloat(updatedTSN);
                    }
                    else {
                        originalWrapper[key].TSN = updatedTSN;
                    }
                } else {
                    if (llpWrapper[key] != undefined) {
                        if (llpWrapper[key].TSN != undefined && llpWrapper[key].TSN != null) {
                            originalWrapper[key].TSN = parseFloat(llpWrapper[key].TSN);
                        }
                        else {
                            originalWrapper[key].TSN = 0;
                        }
                    }
                    else {
                        originalWrapper[key].TSN = 0;
                    }
                }
            }
        }
        component.set("v.llpWrapper", originalWrapper);
    },

    refreshData: function (component, event, helper) {
        component.set("v.isLoading", true);
        helper.getTemplateData(component);
    },

    disableDrag: function (component, event, helper) {
        helper.disableTableDrag(component, event, helper);
    },

    enableDrag: function (component, event, helper) {
        helper.enableTableDrag(component, event, helper);
    },

    loadJs: function (component, event, helper) {
        jQuery("document").ready(function () {
            var $sortableTable = $("#tblLocations tbody");
            $sortableTable.sortable({
                items: 'tr:not(.noSort)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                }
            });
            $sortableTable.sortable("disable");

            var maxWidth = 0;
            $('#tblLocations td:nth-child(7)').each(function () {
                if (maxWidth < $(this).width())
                    maxWidth = $(this).width();
            });
            $('#tblLocations td:nth-child(7)').css('width', maxWidth);
            $('#tblLocations td:nth-child(6)').css('width', maxWidth);
            $('#tblLocations td:nth-child(5)').css('width', maxWidth);
            $('#tblLocations td:nth-child(4)').css('width', maxWidth);
            $('#tblLocations td:nth-child(3)').css('width', maxWidth);
            $('#tblLocations td:nth-child(2)').css('width', maxWidth);
            $('#tblLocations td:nth-child(1)').css('width', maxWidth);
        });
    },

    addThrustColumn: function (component, event, helper) {
        component.set("v.showLLPAddIcon", false);
        helper.addNewThrustColumn(component);
    },

    changeSaveButton: function (component, event, helper) {
        var disableCheckbox = component.get("v.saveAndUpdate");
    },

    saveLLPSnapshots: function (component, event, helper) {
        helper.validateInput(component, event, helper, true);
    },
    updateThrust: function (component, event, helper) {
        var index = event.target.dataset.id;
        var llpWrapper = component.get("v.llpWrapper");
        var thrustPicklist = component.get("v.ThrustPicklist");
        var thrustUnique = component.get("v.ThrustUnqList");
        var thrustId = thrustUnique[index].SelectedVal;
        var thrustName;
        var llpWrapper = component.get("v.llpWrapper");
        var uniqueThrusts = [];
        var duplicatesThrusts = [];

        for (var i = 0; i < thrustPicklist.length; i++) {
            if (thrustPicklist[i].ThrustId == thrustId) {
                thrustName = thrustPicklist[i].ThrustName;
                break;
            }
        }
        for (var i = 0; i < llpWrapper.length; i++) {
            llpWrapper[i].Thrust[index].ThrustId = thrustId;
            llpWrapper[i].Thrust[index].ThrustName = thrustName;
            llpWrapper[i].Thrust[index].Name = thrustName;
        }
        component.set("v.llpWrapper", llpWrapper);

        for (let key in llpWrapper[0].Thrust) {
            if (uniqueThrusts.includes(llpWrapper[0].Thrust[key].ThrustName)) {
                duplicatesThrusts.push(llpWrapper[0].Thrust[key].ThrustName);
            } else {
                uniqueThrusts.push(llpWrapper[0].Thrust[key].ThrustName);
            }
        }

        if (duplicatesThrusts.length > 0) {
            helper.showErrorMsg(component, "You cannot add duplicate thrusts, Please use a different thrust.");
        }
    },

    addLLPRow: function (component, event, helper) {
        component.set("v.showThrustAddIcon", false);
        helper.addNewLLPRow(component);
    },

    editRow: function (component, event, helper) {
        var isEdit = component.get("v.isEdit");
        helper.resetEdit(component);
        component.set("v.isEdit", true);
        helper.enableTableDrag(component, event, helper);
    },

    saveRow: function (component, event, helper) {
        var isEdit = component.get("v.isEdit");
        component.set("v.isLoading", true);
        helper.disableTableDrag(component, event, helper);
        helper.saveSorting(component, event, helper);
        helper.validateInput(component, event, helper, false);
        component.set("v.limitRequired", false);
    },

    cancelRow: function (component, event, helper) {
        helper.disableTableDrag(component, event, helper);
        helper.getAssemblyBasic(component);
        component.set("v.isEdit", false);
    },

    deleteRow: function (component, event, helper) {
        var index = event.target.dataset.id;
        var llpWrapper = component.get("v.llpWrapper");
        var recordId = llpWrapper[index].Id;

        if (recordId == undefined) {
            llpWrapper.splice(index, 1);
            component.set("v.llpWrapper", llpWrapper);
        } else {
            component.set("v.isOpen", true);
            component.set("v.deleteIndex", index);
        }
    },

    closeModel: function (component, event, helper) {
        component.set("v.isOpen", false);
    },

    confirmDelete: function (component, event, helper) {
        component.set("v.isOpen", false);
        component.set("v.isLoading", true);
        helper.deleteLLPData(component);
    },

    showChildRecord: function (component, event, helper) {
        var recordId = event.currentTarget.id;
        var isEdit = component.get("v.isEdit");
        if (isEdit == false) {
            component.set("v.llpTempId", recordId);
            var target = event.currentTarget;
            var rowId = target.rowIndex;
            component.set('v.rowId', rowId);
            helper.changeRowHighlight(component, rowId);
        }
    },

    showRecord: function (component, event, helper) {
        var recordId = event.currentTarget.id;
        window.open('/' + recordId);
    },

    // ## function call on Click on the "Download As CSV" Button. 
    downloadCsv: function (component, event, helper) {
        // get the Records [contact] list from 'ListOfContact' attribute 
        var stockData = component.get("v.llpWrapper");
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.convertArrayOfObjectsToCSV(component, stockData);
        if (csv == null) {
            return;
        }

        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'ExportData.csv'; // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file
    },

    deleteLLPSnapshots: function (component, event, helper) {
        var action = component.get("c.resetLLPSnapshot");
        action.setParams({
            assemblyUtilId: component.get("v.snapshotId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isEdit", false);
                component.set("v.loadedFromLLP", false);
                component.set("v.isLLPUpdated", false);
                component.set("v.saveAndUpdate", false);
                helper.getAssemblyBasic(component);
            }
            else if (state === "ERROR") {
                helper.handleErrors(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    closeThrustModel: function (component) {
        component.set("v.openThrustModal", false);
    },

    updateCurrentThrustValue: function (component, event, helper) {
        let thrustPicklist = component.get("v.InitialThrustPicklist");
        let currentThrustName = component.get("v.currentThrustName");
        let thrustId = '';
        for (let key in thrustPicklist) {
            if (thrustPicklist[key].ThrustName === currentThrustName) {
                thrustId = thrustPicklist[key].ThrustId;
                break;
            }
        }

        var llpWrapper;
        if (!component.get("v.loadedFromLLP")) {
            llpWrapper = '';
        } else {
            llpWrapper = component.get("v.llpWrapper");
            llpWrapper.forEach(function (data) {
                data.Thrust.forEach(function (d) {
                    if (d.ThrustName.trim() === currentThrustName) {
                        d['isCurrentThrust'] = true;
                        data.CurrentThrust = currentThrustName;
                    }
                });
            });
            llpWrapper = JSON.stringify(llpWrapper);
        }

        var action = component.get("c.updateCurrentThrust");
        action.setParams({
            wrapperDataList: llpWrapper,
            assemblyId: component.get("v.recordId"),
            currentThrustId: thrustId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isEdit", false);
                helper.getAssemblyBasic(component);
                component.set("v.openThrustModal", false);
            }
            else if (state === "ERROR") {
                helper.handleErrors(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    openThrustModal: function (component, event, helper) {
        if (component.get("v.isEdit")) {
            var thrustName = event.target.getAttribute("data-recId").trim();
            component.set("v.currentThrustName", thrustName);
            component.set("v.openThrustModal", true);
        }
    }
})