({
    doInit : function(component, event, helper) {
        $A.util.addClass(component.find("confirmSection"), 'slds-fade-in-open');
    },
    
    
    confirmDeleteRecord : function(component, event, helper) {
        component.set("v.confirmDeleteSection", false);  
        $A.util.removeClass(component.find("confirmSection"), 'slds-fade-in-open'); 
        
        var deleteEvt = component.getEvent('deleteRecord');
        deleteEvt.fire();
    },
    
    
    closeSection : function(component, event, helper) {
        $A.util.removeClass(component.find("confirmSection"), 'slds-fade-in-open');
        component.set("v.confirmDeleteSection", false); 
    }
})