({
    helperCloseMethod: function (cmp, canvasIdVar) {
        if ((typeof sforce != 'undefined') && (sforce != null)) {
            // do your SF1 mobile button stuff
            //sforce.one.back(true);
            if (canvasIdVar !== null) {
                sforce.one.navigateToSObject(canvasIdVar);
            }
            else {
                sforce.one.back(true);
            }

        }
        else {
            // do your desktop stuff
            //window.c 	lose();
            let nameSpacePrefix = cmp.get("v.nameSpacePrefix");
            if (nameSpacePrefix == undefined || nameSpacePrefix == '' || nameSpacePrefix == null) { nameSpacePrefix = ''; }

            if (canvasIdVar !== null) {
                var finalURL = window.location.protocol + "//" + window.location.hostname + "/apex/" + nameSpacePrefix + "RedirectVF?paramId=" + canvasIdVar;
                window.open(finalURL, "_self");
            }
            else {
                window.history.back();
            }
        }
    },
    throwErrorForKicks: function (cmp, message) {
        message = "Review all error messages below to correct your data.\n" +message;
        this.showErrorMsg(cmp,message);
        
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    helperhandleAssetTypeValueChange: function (component) {
        console.log("helperhandleAssetTypeValueChange+==>");
        var selected = component.get("v.assetType");
        var opts;
        console.log('selected=' + selected);
        if (selected == "Aircraft") {
            opts = [
                { class: "optionClass", label: "MSN", value: "MSN", selected: "true" },
                { class: "optionClass", label: "Asset Type", value: "Asset Type" }
            ];
        }
        else {
            opts = [
                { class: "optionClass", label: "ESN", value: "ESN", selected: "true" },
                { class: "optionClass", label: "Asset Type", value: "Asset Type" }
            ];
        }
        var InputSelectMsn = component.find("InputSelectMsnATId");
        InputSelectMsn.set("v.options", opts);
        InputSelectMsn.render();
        console.log("helperhandleAssetTypeValueChange-==>");
    },
    fetchNameSpacePrefix: function (cmp, event) {
        const action = cmp.get("c.getNameSpacePrefix");
        action.setCallback(this, function (response) {
            const state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("result: ", result);
                if (result != undefined && result != '') {
                    cmp.set("v.nameSpacePrefix", result);
                }
            }
            else if (state === 'ERORR') {
                return new Error('Error: ' + response.error[0].message);
            }
        });
        $A.enqueueAction(action);
    },
})