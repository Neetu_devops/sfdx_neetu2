({
    initialize: function (component, event, helper) {
        console.log("CanvassingExampleController initialize(+)==>");
        var today = new Date();
        //component.set("v.dealDate",todayDate);
        component.set('v.dealDate', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
        
        var opts = [
            { class: "optionClass", label: "MSN/ESN", value: "MSN", selected: "true" },
            { class: "optionClass", label: "World Fleet", value: "World_Fleet" },
            { class: "optionClass", label: "Aircraft/Engine Type", value: "Asset_Type" }
        ];
        // component.find("InputSelectMsnATId").set("v.options", opts);        
        
        // fetch NameSpacePrefix
        helper.fetchNameSpacePrefix(component);

        console.log("CanvassingExampleController initialize(-)==>");
    },


    callSave: function (component, event, helper) {
        console.log("AddmultipleContacts callSave(+)==>");
        var errorMessage = "";
        var emptyString1 = ",";

        //1.
        var CampaignNameVar = component.get('v.CampaignName');
        console.log("CampaignNameVar===================================================" + CampaignNameVar);

        //2.
        var CampaignTypeVar = component.get('v.CampaignType');
        console.log("CampaignTypeVar=" + CampaignTypeVar);

        //3.
        var CatagoryVar = component.get('v.Catagory');
        console.log("CatagoryVar=" + CatagoryVar);

        //4.
        var MarketingStatusVar = component.get('v.MarketingStatus');
        console.log("MarketingStatusVar=" + MarketingStatusVar);

        //5. 6.
        var AircraftArray = component.get('v.AircraftArray');
        console.log("AircraftArray: ", AircraftArray);
        var AircraftIdArrayVar = component.get('v.AircraftIdArray');
        var AircraftNameArrayVar = component.get('v.AircraftNameArray');
        for (var key in AircraftArray) {
            if (AircraftArray[key] != undefined) {
                AircraftIdArrayVar.push(AircraftArray[key].Id);
                AircraftNameArrayVar.push(AircraftArray[key].Name);
            }
        }
        component.set("v.AircraftIdArray", AircraftIdArrayVar);
        component.set("v.AircraftNameArray", AircraftNameArrayVar);
        console.log("AircraftIdArrayVar=" + AircraftIdArrayVar);
        console.log("AircraftNameArrayVar=" + AircraftNameArrayVar);

        //7. 8.
        var AircraftITArray = component.get('v.AircraftITArray');
        console.log("AircraftITArray: ", AircraftITArray);
        var AircraftITIdArrayVar = component.get('v.AircraftITIdArray');
        var AircraftITNameArrayVar = component.get('v.AircraftITNameArray');
        for (var key in AircraftITArray) {
            if (AircraftITArray[key] != undefined) {
                AircraftITIdArrayVar.push(AircraftITArray[key].Id);
                AircraftITNameArrayVar.push(AircraftITArray[key].Name);
            }
        }
        component.set("v.AircraftITIdArray", AircraftITIdArrayVar);
        component.set("v.AircraftITNameArray", AircraftITNameArrayVar);
        console.log("AircraftITIdArrayVar=" + AircraftITIdArrayVar);
        console.log("AircraftITNameArrayVar=" + AircraftITNameArrayVar);

        //9.
        var PlacementPriorityTypeVar = component.get('v.PlacementPriorityType');
        console.log("PlacementPriorityTypeVar=" + PlacementPriorityTypeVar);

        var customerArray = component.get('v.customerArray');
        console.log("customerArray: ", customerArray);
        var customeridArrayVar = component.get('v.customerIdArray');
        var customerNameArrayVar = component.get('v.customerNameArray');
        for (var key in customerArray) {
            if (customerArray[key] != undefined) {
                customeridArrayVar.push(customerArray[key].Id);
                customerNameArrayVar.push(customerArray[key].Name);
            }
        }
        component.set("v.customerIdArray", customeridArrayVar);
        component.set("v.customerNameArray", customerNameArrayVar);
        console.log("customeridArrayVar=" + customeridArrayVar);
        console.log("customerNameArrayVar=" + customerNameArrayVar);

        var lessorIdArrayVar = component.get('v.lessorIdArray');
        console.log("lessorIdArrayVar=" + lessorIdArrayVar);
        var lessorNameArrayVar = component.get('v.lessorNameArray');
        console.log("lessorNameArrayVar=" + lessorNameArrayVar);


        //if((customeridArrayVar[0]==null || customeridArrayVar[0]=="")) errorMessage += ' Please choose at least one valid Prospect Customer.\n';

        if ((CampaignTypeVar == 'Current Fleet') && (AircraftIdArrayVar[0] == null)) errorMessage += 'Please enter the valid Aircraft.\n';
        if ((CampaignTypeVar == 'Aircraft New Order') && (AircraftITIdArrayVar[0] == null)) errorMessage += 'Please enter the valid Aircraft In Transaction.\n';
        if ((CampaignTypeVar == 'Transaction') && (AircraftITIdArrayVar[0] == null)) errorMessage += 'Please enter the valid Aircraft In Transaction.\n';


        if (errorMessage != null && errorMessage != "") {
            helper.throwErrorForKicks(component, errorMessage);
            //alert("Review all error messages below to correct your data.\n" + errorMessage);
        }
        else {
            //if (confirm('Are you sure you want to save the changes?')) 
            //{
            // Save it!
            var canvasIdVar;
            var action = component.get("c.callSaveApexCampaign");
            action.setParams({
                CampaignName_p: CampaignNameVar
                , CampaignType_p: CampaignTypeVar
                , Catagory_p: CatagoryVar
                , MarketingStatus_p: MarketingStatusVar
                , AircraftIdArray_p: AircraftIdArrayVar
                , AircraftNameArray_p: AircraftNameArrayVar
                , AircraftITIdArray_p: AircraftITIdArrayVar
                , AircraftITNameArray_p: AircraftITNameArrayVar
                , PlacementPriorityType_p: PlacementPriorityTypeVar
                , customeridArray_p: customeridArrayVar
                , customerNameArray_p: customerNameArrayVar
                , lessorIdArray_p: lessorIdArrayVar
                , lessorNameArray_p: lessorNameArrayVar
            });
            action.setCallback(this, function (a) {
                if (a.error && a.error.length) {
                    //return $A.error('Unexpected error callSave: '+a.error[0].message);
                    return new Error('Unexpected error callSave: ' + a.error[0].message);
                }
                console.log("success");
                var canvasMap = a.getReturnValue();
                console.log("canvasIdVar=", canvasMap);
                canvasIdVar = canvasMap['ID'];
                console.log("canvasIdVar=" + canvasIdVar);
                if (canvasMap["ERRORID"] == "1") {
                    helper.throwErrorForKicks(component, canvasMap["ERROR"]);
                }
                else {
                    var errorDiv = component.find("errorDiv1");
                    $A.util.addClass(errorDiv, "slds-hide");
                    //alert("Records created successfully. Canvas Report Name:"+canvasIdVar);
                    helper.helperCloseMethod(component, canvasIdVar);
                }
            });
            $A.enqueueAction(action);
            //} 
        }
        console.log("AddmultipleContacts callSave(-)==>");
    },
    callCancel: function (component, event, helper) {
        console.log("callCancel======================>");
        helper.helperCloseMethod(component, null);
    },
    handleAssetTypeValueChange: function (component, event, helper) {
        console.log("handleAssetTypeValueChange======================>");
        helper.helperhandleAssetTypeValueChange(component);
    }

})