({
    doInit: function (component, event, helper) {
        var action = component.get("c.getNameSpacePrefix");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var namespace = response.getReturnValue();
                let myPageRef = component.get("v.pageReference");
                var result;
                if (namespace === "") {
                    result = decodeURIComponent((new RegExp('[?|&]c__id' + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
                }
                else {
                    result = decodeURIComponent((new RegExp('[?|&]' + namespace + 'id=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
                }
                component.set("v.recordId", result);
				component.set("v.flag",true);
            }
            else {
                console.log('ERROR: ', response.getError());
            }
        });
        $A.enqueueAction(action);
    },
	 
	locationChange : function(component, event, helper){
    	component.set("v.flag",false);
    }
})