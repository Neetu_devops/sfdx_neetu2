({
    viewSnapshotCall : function(component) {
        var recordIdL = component.get("v.recordId");
        var snapshotIdL = component.get("v.snapshotId");
        console.log('viewSnapshotCall recordId: '+recordIdL + ' snapshotIdL: '+snapshotIdL);
        var self = this;
        if($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            component.set("v.isLoading", false);
            component.set("v.showViewerFromChild",false);
            return;
        }
        if($A.util.isEmpty(snapshotIdL) || $A.util.isUndefined(snapshotIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            component.set("v.isLoading", false);
            component.set("v.showViewerFromChild",false);
            return;
        }
        var action = component.get('c.getSnapshot');
        action.setParams({
            snapshotId : snapshotIdL,
            recordId: recordIdL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("viewSnapshotCall : state "+state);
            component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                //console.log("getChildObjectCall : response "+allValues);
                if(allValues != null && allValues.length > 0) {
                    //console.log("viewSnapshotCall : response "+JSON.stringify(allValues));
                    component.set('v.SnapshotData', allValues[0]);
                    component.set('v.items', allValues[0].items);
                    component.set('v.originalItems', allValues[0].items);
                    
                    /*if(allValues[0].items != null && allValues[0].items.length > 0 && 
                       allValues[0].items[0] != null && 
                       allValues[0].items[0].items != null && allValues[0].items[0].items.length > 0 ) {
                       self.getRecordDetailsFromServer(component, allValues[0].items[0].items[0].Id);
                    } */
                    
                    self.getRecordDetailsFromServer(component,recordIdL);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("viewSnapshotCall errors "+errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("viewSnapshotCall Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    getRecordDetailsFromServer : function(component, recordIdL) {
        component.set("v.isLoading", true);
        var self= this;
        var snapshotIdL = component.get("v.snapshotId");
        var action = component.get('c.getSnapshotRecordDetail');
        action.setParams({
            snapshotId : snapshotIdL,
            recordId: recordIdL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getRecordDetailsFromServer : state "+state);
            component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                //console.log("getRecordDetailsFromServer : response "+allValues);
                if(allValues != null && allValues.length > 0) {
                   // console.log("getRecordDetailsFromServer : response "+JSON.stringify(allValues));
                    component.set("v.RecordData", allValues);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getRecordDetailsFromServer errors "+errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getRecordDetailsFromServer Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getRecordDetails: function(component, RecordId) {
        var items = component.get("v.originalItems");
       // console.log('getRecordDetails items '+JSON.stringify(items));
        console.log('getRecordDetails RecordId '+RecordId);
        if(!$A.util.isEmpty(items) && !$A.util.isUndefined(items)) {
        	this.searchItem(component, items, RecordId);
            component.set("v.isLoading", false);
            console.log('getRecordDetails RecordData: '+JSON.stringify(component.get("v.RecordData")));
        }
    },
    
    searchItem : function(component, items, RecordId) {
        //console.log('searchItem items '+JSON.stringify(items));
        if(!$A.util.isEmpty(items) && !$A.util.isUndefined(items)) {
            for(var i = 0; i < items.length; i++) {
                if(items[i].name == RecordId) {
                    console.log('searchItem snapshotJsonList: ' + JSON.stringify(items[i]));
                    component.set("v.RecordData", items[i].snapshotJsonList);
                    break;
                }
                else
                    this.searchItem(component, items[i].items, RecordId);
            }
        }
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})