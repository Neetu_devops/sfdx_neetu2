({
	doInit : function(component, event, helper) {
		var showViewer = component.get("v.showViewer");
        console.log("loadSnapshot doInit"+showViewer);
        
	}, 
    
    onClickViewSnapshot: function(component, event, helper) {
        component.set("v.isLoading", true);
		helper.viewSnapshotCall(component);
	}, 
    
    loadSnapshot: function (component, event, helper) {
        var showViewer = component.get("v.showViewer");
        console.log("loadSnapshot showViewer"+showViewer);
        if(showViewer == true) {
            component.set("v.isLoading", true);
            helper.viewSnapshotCall(component);
        }
    },
    
    onItemClick: function(component, event, helper) {
        console.log('onItemClick data params: '+JSON.stringify(event.getParams()));
        var recordId = event.getParam('name');
        console.log('onItemClick recordId: '+recordId);
        if($A.util.isEmpty(recordId) || $A.util.isUndefined(recordId)) 
            return;
        component.set("v.isLoading", true);
        helper.getRecordDetailsFromServer(component, recordId);
    },
    
    closePop : function(component, event, helper) {
        component.set("v.showViewerFromChild",false);
        console.log('closePop showViewerFromChild');
    },
    
    openRecord: function(component,event,helper) {
        var record = event.currentTarget.id;
        console.log("openRecord "+record);
        window.open('/' + record);  
    },

    
})