({
    doInit : function(component, event, helper) {
        helper.doInit(component, event);
    },
    
    editInterpolation : function(component, event, helper) {
        component.set("v.editRecord",true);
        
    },
    disableEditing: function(component, event, helper) {
        component.set("v.editRecord",false);
    },
    saveChanges : function(component, event, helper) {
        var newList = component.get("v.interpolationDataContainer.interpolationTableValues");
        var oldList = component.get("v.oldInterpolationRecordList");
        
        var ListToUpdate = [];
        
        for(var i=0; i < newList.length; i++){
            for(var j=0; j < newList[i].interpolationDataList.length; j++){
                if(newList[i].interpolationDataList[j].id != undefined && newList[i].interpolationDataList[j].rate != oldList[i].interpolationDataList[j].rate ){
                    ListToUpdate.push(newList[i].interpolationDataList[j]);
                }
            }
        }
        component.set("v.editRecord",false);
        if(ListToUpdate.length > 0){
            helper.showSpinner(component);
            var action = component.get("c.saveInterpolationRateData");
            
            action.setParams({ 
                'interpolationDataList' : JSON.stringify(ListToUpdate)
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    component.set("v.oldInterpolationRecordList",JSON.parse(JSON.stringify(newList)));
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success',
                        message: 'Record updated successfully',
                        duration:' 500',
                        type: 'success',
                    });
                    
                    toastEvent.fire();

                    $A.get('e.force:refreshView').fire();
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
                helper.hideSpinner(component);
            });
            $A.enqueueAction(action);
        }
    },
   
})