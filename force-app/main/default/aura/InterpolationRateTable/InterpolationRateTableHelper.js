({
    doInit : function(component, event) {
        
        this.showSpinner(component);
        var action = component.get("c.loadData");
        action.setParams({ 
            'recordId' : component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var value = response.getReturnValue();
                if(value != null && value != undefined && value != ''){
                    
                    component.set("v.interpolationDataContainer",value);
                    var oldList = value.interpolationTableValues;
                    component.set("v.oldInterpolationRecordList",JSON.parse(JSON.stringify(oldList)));
                }else{
                    component.set("v.isRecord",false);
                }
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
	showSpinner: function (component) {
       component.set("v.showSpinner",true);
    },
    
    hideSpinner: function (component) {
        component.set("v.showSpinner",false);
    },
    
    handleErrors : function(errors) {
        
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    },
})