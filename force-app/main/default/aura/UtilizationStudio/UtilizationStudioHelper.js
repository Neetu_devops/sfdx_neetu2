({
    init: function (component, event, helper, reload) {
        component.set("v.pageNumber", 1);
        component.set("v.assetCount", 0);

        var recordId = component.get("v.recordId");
        var nameSpace = component.get("v.nameSpacePrefix");
        var param;
        var result;
        var base64Context;
        var pageRef = component.get("v.pageReference");
        if (pageRef != null && pageRef != undefined && pageRef != '') {
            let state = pageRef.state; // state holds any query params
            base64Context = state.inContextOfRef;
            console.log('base64Context: ', base64Context);
        }
        if (reload) {
            component.set("v.pageReference", '');
        }
        if (base64Context != undefined) {
            // For some reason, the string starts with "1.", if somebody knows why,
            // this solution could be better generalized.
            if (base64Context.startsWith("1\.")) {
                base64Context = base64Context.substring(2);
            }
            var addressableContext = JSON.parse(window.atob(base64Context));
            result = addressableContext.attributes.recordId;
        } else {
            if (nameSpace != '' && nameSpace != null && nameSpace != undefined) {
                param = 'leaseworks__Id';
            } else {
                param = 'c__Id';
            }
            result = decodeURIComponent((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        }
        console.log('IdToProcess: ' + result);
        if (result != null && result != '') {
            recordId = result;
        }
        if (recordId != undefined && recordId != '' && recordId != null) {
            var loadedFromUtilization = false;
            var action = component.get("c.getRecordsonLoad")
            action.setParams({
                recordId: recordId
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log('result: ', result);
                    if (result != undefined && result != '' && result != null) {
                        if (result.objName == result.nameSpacePrefix + 'Aircraft__c') {
                            component.set("v.assetRecord", result.objRecord);
                            if (result.loadInCreateNew) {
                                var searchFilters = component.get("v.searchFilters");
                                searchFilters.Status = 'Create New';
                                component.set("v.isCreateNew", true);
                                component.set("v.searchFilters", searchFilters);
                            }
                        }
                        if (result.objName == result.nameSpacePrefix + 'Operator__c') {
                            component.set("v.operatorRecord", result.objRecord);
                        }
                        if (result.objName == result.nameSpacePrefix + 'Utilization_Report__c') {
                            loadedFromUtilization = true;
                            component.set("v.isDateDisabled", false);
                            component.set("v.searchFilters.DateOption", "On Date");
                            component.set("v.searchFilters.Status", result.utilRecord.Status__c);
                            component.set("v.searchFilters.InputDate", result.utilRecord.Month_Ending__c);
                            component.set("v.assetRecord", result.objRecord);
                        }
                        component.set("v.showLookup", true);
                        if (loadedFromUtilization) {
                            helper.searchDataonLoad(component, event, helper, '', recordId);
                        } else {
                            helper.searchDataonLoad(component, event, helper);
                        }
                    }
                } else if (state == "ERROR") {
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
            });
            $A.enqueueAction(action);
        }
    },
    setSearchFilters: function (component, event, helper) {
        var stopLoad = false;
        var searchData = false;
        component.set("v.pageNumber", 1);
        component.set("v.assetCount", 0);
        component.set("v.hideTable", false);
        var searchFilters = component.get("v.searchFilters");
        var status = searchFilters.Status;
        console.log(status);
        if (status == 'Create New') {
            var searchData = component.get("v.searchData");
            var showSearchModal = false;
            if (searchData != undefined) {
                for (let key in searchData.operatorData) {
                    for (let key1 in searchData.operatorData[key].assetData) {
                        if (searchData.operatorData[key].assetData[key1].ischanged) {
                            showSearchModal = true;
                        }
                    }
                }
            }
            else {
                showSearchModal = false;
            }
            console.log('showSearchModal: ', showSearchModal);

            if (showSearchModal) {
                component.set("v.showSearchModal", true);
                stopLoad = true;
            }
            else {
                if (component.get("v.isSearchPopup")) {
                    component.set("v.searchFilters.Status", 'All');
                    component.set("v.isCreateNew", false);
                    component.set('v.ignoreIdsList', []);
                    searchData = true;
                }
                else {
                    component.set("v.approveAllUtilization", false);
                    component.set("v.submitForApproval", false);
                    component.set("v.sendInvoices", false);
                    component.set("v.ignoreIdsList", []);
                    component.set("v.previousList", []);
                    component.set("v.isCreateNew", true);
                    stopLoad = true;
                    helper.searchDataonLoad(component, event, helper);
                }

            }

        }
        if (status != 'Create New') {
            component.set("v.isCreateNew", false);
        }
        var operatorRecord = component.get("v.operatorRecord");
        var assetRecord = component.get("v.assetRecord");
        if ((assetRecord != null && assetRecord != '' && assetRecord != undefined && component.get("v.pinnedIds").length > 0) || (operatorRecord != null && operatorRecord != '' && operatorRecord != undefined && component.get("v.pinnedIds").length > 0)) {
            component.set("v.isInitialLoad", false);
            component.set("v.assetOrOperatorPreviousSelected", true);
            if (status != 'Create New') {
                helper.searchDataonLoad(component, event, helper);
            }
        }
        else {
            if (component.get("v.assetOrOperatorPreviousSelected")) {
                component.set("v.ignoreIdsList", []);
            }
            else {
                if (component.get("v.pinnedIds").length > 0) {
                    component.set("v.isInitialLoad", false);
                    component.set("v.ignoreIdsList", []);
                    component.set("v.previousList", []);
                }
                else {
                    component.set("v.ignoreIdsList", []);
                    component.set("v.previousList", []);
                    component.set("v.pinnedIds", []);
                    component.set("v.isInitialLoad", true);
                }
            }
            if ((status != 'Create New' || searchData) && !stopLoad) {
                helper.searchDataonLoad(component, event, helper);
            }
        }
    },
    
    
    getAPUCounterState: function (component, event, helper) {
        var action = component.get("c.getAPUCounterStatus");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result != '' && result != null && result != undefined) {
                    component.set("v.showAPUCounter", result);
                }
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    
    getNameSpace: function (component, event, helper) {
        var action = component.get("c.getNameSpacePrefix");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result != '' && result != null && result != undefined) {
                    component.set("v.nameSpacePrefix", result);
                }
                this.init(component, event, helper);
            }
            else if (state == "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    searchDataonLoad: function (component, event, helper, loadingObject, recordId, redirectToOpen, addTrueUpRow, checkRecordsOnCurrentPage, loadinNewStatus) {
        helper.showSpinner(component, event, helper);
        component.set("v.showTable", false);
        component.set("v.showSaveAll", false);
        var loadTable = false;
        var hideError = false;
        if (component.get("v.pageNumber") == 1) {
            component.set("v.isPreviousDisabled", true);
        }
        var inputDate = component.get("v.searchFilters.InputDate");
        if (inputDate != undefined && inputDate != null && inputDate != '') {
            let formattedDate = $A.localizationService.formatDate(inputDate, "YYYY-MM-DD");
            component.set("v.searchFilters.InputDate", formattedDate);
        }
        console.log('Filters: ', component.get("v.searchFilters.InputDate"));
        var action = component.get("c.fetchUtilizationData");
        action.setParams({
            asset: JSON.stringify(component.get("v.assetRecord")),
            operator: JSON.stringify(component.get("v.operatorRecord")),
            searchFilters: JSON.stringify(component.get("v.searchFilters")),
            ignoreIdsList: component.get("v.ignoreIdsList"),
            isInitialLoad: component.get("v.isInitialLoad"),
            previousRecordsList: component.get("v.previousList"),
            pinnedIdsList: component.get("v.pinnedIds"),
            loadingObject: loadingObject,
            recordId: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                helper.hideSpinner(component, event, helper);
                var result = response.getReturnValue();
                console.log('Response from server: ', result);
                if (result.operatorData.length == 0 && !component.get('v.pinnedIds').length > 0) {
                    if (checkRecordsOnCurrentPage) {
                        console.log('Prev Ignore List : ', component.get("v.ignoreIdsList"));
                        let ignoreList = component.get("v.ignoreIdsList");
                        ignoreList.splice(ignoreList.length - 10, 10);
                        console.log('ignoreList: ', ignoreList);
                        component.set("v.ignoreIdsList", ignoreList);
                        component.set("v.assetCount", component.get("v.assetCount") - 11);
                        component.set("v.pageNumber", component.get("v.pageNumber") - 1);
                        this.searchDataonLoad(component, event, helper);
                    }
                    else {
                        if (loadinNewStatus) {
                            component.set("v.searchFilters.Status", "Create New");
                            component.set("v.isCreateNew", true);
                            component.set("v.assetCount", component.get("v.assetCount") - 1);
                            hideError = true;
                            this.searchDataonLoad(component, event, helper);
                        }
                        else {
                            if (redirectToOpen == 'redirect') {
                                component.set("v.isCreateNew", false);
                                component.set("v.searchFilters.Status", "Open");
                                this.searchDataonLoad(component, event, helper);
                            }
                            else {
                                component.set("v.noRecordsToDisplay", true);
                                loadTable = true;
                            }
                        }
                    }
                }
                else {
                    if (checkRecordsOnCurrentPage) {
                        component.set("v.assetCount", component.get("v.assetCount") - 1);
                    }
                    console.log('Status: ', component.get("v.searchFilters.Status"));
                    if (component.get("v.searchFilters.Status") != 'Create New') {
                        if ((result.errorMsg == '' && !result.errorMsg.includes('Pinned'))) {
                            if (addTrueUpRow === "addTrueUpRow")
                                helper.fillAssemblyWrapper(component, event, helper, result, addTrueUpRow);
                            else {
                                let operatorRecord = component.get("v.operatorRecord");
                                if (operatorRecord != undefined && operatorRecord != null) {
                                    helper.fillAssemblyWrapper(component, event, helper, result, addTrueUpRow);
                                }
                                else {
                                    helper.fetchAdditionalUtilization(component, event, helper, addTrueUpRow, result);
                                }
                            }
                        }
                    }
                    else {
                        helper.getAssembliesData(component, event, helper, result);
                        //loadTable = true;
                    }
                    component.set("v.noRecordsToDisplay", false);
                }
                if (result.errorMsg != undefined && result.errorMsg != '' && !result.errorMsg.includes('Pinned') && !hideError) {
                    helper.handleErrors(result.errorMsg);
                    loadTable = false;
                    component.set("v.showTable", false);
                    helper.hideSpinner(component, event);
                }
                else {
                    if (!hideError) {
                        console.log('Final Result: ', result);
                        if (result.errorMsg.includes('Pinned')) {
                            var msg = result.errorMsg.split('-');
                            helper.handleErrors(msg[0]);
                        }
                        var allDataList = result.operatorData;
                        // Handling assets to show on UI
                        if (component.get('v.pinnedIds').length > 0)
                            helper.handleAssetsToShow(component, allDataList, result, helper);

                        var totalAssetsOnCurrentPage = 0;
                        for (let key in allDataList) {
                            totalAssetsOnCurrentPage += allDataList[key].assetData.length;
                        }
                        component.set("v.totalAssetsOnCurrentPage", totalAssetsOnCurrentPage);
                        if (totalAssetsOnCurrentPage < 10) {
                            component.set("v.disableNext", true);
                        }
                        else {
                            component.set("v.disableNext", false);
                        }
                        //if (component.get("v.searchFilters.Status") == "Create New") {
                        //   console.log('In New');
                        //   this.searchNextRecordsonLoad(component, event, helper);
                        //}
                        component.set("v.searchData", result);
                        var ids = component.get("v.ignoreIdsList");
                        var searchData = component.get("v.searchData");
                        var allDataList = searchData.operatorData;
                        var operatorMap = searchData.operatorAssetCountMap;
                        var isEmpty = true;

                        if (typeof operatorMap == 'object') {
                            for (let key in operatorMap) {
                                if (operatorMap[key] != undefined && operatorMap[key] != null && operatorMap[key] != '') {
                                    isEmpty = false;
                                }
                            }
                        }
                        if (!isEmpty) {
                            component.set("v.operatorCountMap", operatorMap);
                        }

                        for (let key in allDataList) {
                            for (let key2 in allDataList[key].assetData) {
                                ids.push(allDataList[key].assetData[key2].Asset.Id);
                            }
                        }
                        let openButtonCheck = false;
                        let submitButtonCheck = false;
                        let approveButtonCheck = false;
                        component.set("v.showSaveAll", false);
                        for (let key in allDataList) {
                            for (let key1 in allDataList[key].assetData) {
                                if (allDataList[key].assetData[key1].utilization.Status__c == 'Open' || allDataList[key].assetData[key1].utilization.Status__c == 'New') {
                                    openButtonCheck = true;
                                    component.set("v.showSaveAll", true);
                                    component.set("v.approveAllUtilization", false);
                                    component.set("v.sendInvoices", false);
                                    if (component.get("v.isCreateNew")) {
                                        component.set("v.submitForApproval", false);
                                    }
                                    else {
                                        component.set("v.submitForApproval", true);
                                    }
                                }
                                else {
                                    if (!openButtonCheck) {
                                        if (allDataList[key].assetData[key1].utilization.Status__c == 'Submitted To Lessor') {
                                            if (!openButtonCheck) {
                                                submitButtonCheck = true;
                                                component.set("v.submitForApproval", false);
                                                component.set("v.sendInvoices", false);
                                                component.set("v.approveAllUtilization", true);
                                            }
                                        }
                                        else {
                                            if (allDataList[key].assetData[key1].utilization.Status__c == 'Approved By Lessor') {
                                                if (!submitButtonCheck) {
                                                    approveButtonCheck = true;
                                                    component.set("v.approveAllUtilization", false);
                                                    component.set("v.submitForApproval", false);
                                                    component.set("v.sendInvoices", true);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        console.log('New List: ', allDataList);
                        //component.set("v.searchData.operatorData", allDataList);

                        if (!component.get("v.isInitialLoad")) {
                            var pinnedignoreIdsList = component.get("v.pinnedIds");
                            var pinnedIds = [];
                            if (pinnedignoreIdsList.length > 0) {
                                var finalignoreIdsList = [];
                                for (let key in pinnedignoreIdsList) {
                                    pinnedIds.push(pinnedignoreIdsList[key]);
                                }
                                for (let key in allDataList) {
                                    for (let key1 in allDataList[key].assetData) {
                                        for (let key2 in pinnedIds) {
                                            if (allDataList[key].assetData[key1].Asset.Id == pinnedIds[key2]) {
                                                allDataList[key].assetData[key1].ispinned = true;
                                            }
                                        }
                                    }
                                }
                                /* Swapping list items */
                                var tempList = [];
                                for (let key in allDataList) {
                                    for (var i = 0; i < allDataList[key].assetData.length; i++) {
                                        for (var k = parseInt(i) + parseInt(1); k < allDataList[key].assetData.length; k++) {
                                            if (!allDataList[key].assetData[i].ispinned && allDataList[key].assetData[k].ispinned) {
                                                tempList = allDataList[key].assetData[i];
                                                allDataList[key].assetData[i] = allDataList[key].assetData[k];
                                                allDataList[key].assetData[k] = tempList;
                                            }
                                        }
                                    }
                                }
                                component.set("v.searchData.operatorData", allDataList);

                                //component.set("v.searchData.operatorData", operatorData);
                                for (let key in ids) {
                                    var isIdExist = false;
                                    for (let key1 in pinnedIds) {
                                        if (ids[key] === pinnedIds[key1]) {
                                            isIdExist = true;
                                            break;
                                        }
                                    }
                                    if (!isIdExist) {
                                        finalignoreIdsList.push(ids[key]);
                                    }
                                }
                                let finalUniqueIgnoreIdsList = Array.from(new Set(finalignoreIdsList));
                                component.set('v.ignoreIdsList', finalUniqueIgnoreIdsList);
                            }
                            else {
                                component.set('v.ignoreIdsList', ids);
                            }
                        }
                        else {
                            component.set('v.ignoreIdsList', ids);
                        }
                    }
                }
                if (loadTable) {
                    component.set("v.showTable", true);
                }
                console.log('Final Ignore List: ', component.get("v.ignoreIdsList"));
            }
            else if (state === "ERROR") {
                helper.hideSpinner(component, event, helper);
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        })
        $A.enqueueAction(action);
    },
    getAssembliesData: function (component, event, helper, result) {
        let assetIds = [];
        var count = 0;
        for (let key in result.operatorData) {
            for (let key1 in result.operatorData[key].assetData) {
                assetIds.push(result.operatorData[key].assetData[key1].Asset.Id);
            }
        }
        for (var i = 0; i < assetIds.length; i++) {
            var action = component.get("c.getAssemblyData");
            action.setParams({
                assetId: assetIds[i]
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == 'SUCCESS') {
                    count++;
                    var asmData = response.getReturnValue();
                    console.log('Asm Data: ', asmData);
                    try {
                        for (let key in result.operatorData) {
                            for (let key1 in result.operatorData[key].assetData) {
                                if (result.operatorData[key].assetData[key1].Asset.Id == asmData.assetId) {
                                    result.operatorData[key].assetData[key1].lstComponentWrapper = asmData.cwList;

                                    for (let key2 in result.operatorData[key].assetData[key1].lstComponentWrapper) {
                                        var assemblyData = result.operatorData[key].assetData[key1].lstComponentWrapper[key2];
                                        console.log('assemblyData: ', assemblyData.assembly);
                                        if (assemblyData.assembly != undefined && assemblyData.assembly === 'APU') {

                                            var derateRowObj = {
                                                'assembly': 'APU Values',
                                                'showAPUrow': true,
                                                'apuFH': null,
                                                'apuFC': null,
                                                'apuFHM': ''
                                            }
                                            result.operatorData[key].assetData[key1].lstComponentWrapper.splice(parseInt(key2) + parseInt(1), 0, derateRowObj);
                                        }
                                    }


                                    let adw = result.operatorData[key].assetData[key1];
                                    if (adw.Asset.RecordType.Name == 'Aircraft') {
                                        for (let key in asmData.cwList) {
                                            var assembly = asmData.cwList[key];
                                            if (assembly.assembly.includes('Airframe')) {
                                                adw.totalFH = assembly.flightH == undefined ? 0 : assembly.flightH;
                                                if (assembly.flightH != undefined) {
                                                    var min = parseInt((assembly.flightH - Math.floor(assembly.flightH)) * 60);
                                                    adw.totalFHM = Math.floor(assembly.flightH) + ':' + (min > 10 ? min : 0) + min;
                                                }
                                                else {
                                                    adw.totalFHM = '';
                                                }

                                                adw.totalFC = assembly.flightC == undefined ? null : assembly.flightC;
                                                adw.totalRatio = assembly.ratio == undefined ? 0 : assembly.ratio;
                                                adw.totalTSN = assembly.tsn == undefined ? 0 : assembly.tsn;
                                                adw.totalCSN = assembly.csn == undefined ? 0 : assembly.csn;
                                            }
                                        }

                                    }
                                    else {
                                        if (adw.Asset.RecordType.Name == 'Engine') {
                                            for (let key in asmData.cwList) {
                                                var assembly = asmData.cwList[key];
                                                if (assembly.assembly.includes('Engine 1')) {
                                                    adw.totalFH = assembly.flightH == null ? 0 : assembly.flightH;

                                                    if (assembly.flightH != null) {
                                                        var min = parseInt((assembly.flightH - Math.floor(assembly.flightH)) * 60);
                                                        adw.totalFHM = Math.floor(assembly.flightH) + ':' + (min > 10 ? min : 0) + min;
                                                    }
                                                    else {
                                                        adw.totalFHM = '';
                                                    }

                                                    adw.totalFC = assembly.flightC == undefined ? null : assembly.flightC;
                                                    adw.totalRatio = assembly.ratio == undefined ? 0 : assembly.ratio;
                                                    adw.totalTSN = assembly.tsn == undefined ? 0 : assembly.tsn;
                                                    adw.totalCSN = assembly.csn == undefined ? 0 : assembly.csn;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (count == assetIds.length) {
                            component.set("v.showTable", true);
                        }
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
                else if (state == 'ERROR') {
                    count++;
                    if (count == assetIds.length) {
                        component.set("v.showTable", true);
                    }
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
            });
            $A.enqueueAction(action);
        }
    },
    fetchAdditionalUtilization: function (component, event, helper, addTrueUpRow, result) {
        try {
            let assetIds = [];
            let utilIds = [];
            let monthEndings = [];
            var count = 0;
            for (let key in result.operatorData) {
                for (let key1 in result.operatorData[key].assetData) {
                    assetIds.push(result.operatorData[key].assetData[key1].Asset.Id);
                    utilIds.push(result.operatorData[key].assetData[key1].utilization.Id);
                    monthEndings.push(result.operatorData[key].assetData[key1].utilization.Month_Ending__c);
                }
            }
            console.log('ASSET IDS: ', assetIds);
            console.log('UTIL IDS: ', utilIds);
            console.log('MONTH ENDINGS: ', monthEndings);
            for (var i = 0; i < assetIds.length; i++) {
                helper.showSpinner(component, event);
                var action = component.get("c.getAdditionalUtilization");
                action.setParams({
                    assetId: assetIds[i],
                    utilizationId: utilIds[i],
                    monthEnding: monthEndings[i]
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state == 'SUCCESS') {
                        count++;
                        var res = response.getReturnValue();
                        if (res.length > 0) {
                            var astindex = 0;
                            var opindex = 0;
                            var newWrapper = {};
                            console.log("UTILIZATION RESULT: ", res);
                            for (let key in res) {
                                let breakloop = false;
                                for (let key1 in result.operatorData) {
                                    for (let key2 in result.operatorData[key1].assetData) {
                                        if (result.operatorData[key1].assetData[key2].Asset.Id == res[key].Aircraft__c) {
                                            let adw = JSON.stringify(result.operatorData[key1].assetData[key2]);
                                            newWrapper = JSON.parse(adw);
                                            newWrapper.showpin = false;
                                            newWrapper.isTrueUp = true;
                                            newWrapper.utilization = res[key];
                                            opindex = key1;
                                            astindex = key2;
                                            //result.operatorData[key1].assetData.push(newWrapper);
                                            breakloop = true;
                                            break;
                                        }
                                    }
                                    if (breakloop) {
                                        console.log('OpIndex-AstIndex: ', opindex, astindex);
                                        console.log('Result: ', result.operatorData[parseInt(opindex)].assetData[parseInt(astindex) + parseInt(1)]);
                                        result.operatorData[parseInt(opindex)].assetData.splice(parseInt(astindex) + parseInt(1), 0, newWrapper);
                                        break;
                                    }
                                }
                            }
                        }
                        if (count == assetIds.length) {
                            helper.fillAssemblyWrapper(component, event, helper, result, addTrueUpRow);
                        }
                    }
                });
                $A.enqueueAction(action);
            }
        }
        catch (e) {
            console.log('ERROR: ', e);
        }
    },
    convertDateToString: function (date) {
        let day = date.getUTCDate();
        let month = date.getUTCMonth() + 1;
        let year = date.getUTCFullYear();
        let yyyy_MM_dd = year + "-" + month + "-" + day;
        return yyyy_MM_dd;
    },
    searchNextRecordsonLoad: function (component, event, helper, loadingObject, recordId, redirectToOpen) {
        helper.showSpinner(component, event, helper);

        var action = component.get("c.fetchUtilizationData");
        action.setParams({
            asset: JSON.stringify(component.get("v.assetRecord")),
            operator: JSON.stringify(component.get("v.operatorRecord")),
            searchFilters: JSON.stringify(component.get("v.searchFilters")),
            ignoreIdsList: component.get("v.ignoreIdsList"),
            isInitialLoad: component.get("v.isInitialLoad"),
            previousRecordsList: component.get("v.previousList"),
            pinnedIdsList: component.get("v.pinnedIds"),
            loadingObject: loadingObject,
            recordId: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                helper.hideSpinner(component, event, helper);
                var result = response.getReturnValue();
                console.log('Response from server: ', result);
                if (result.operatorData.length == 0 && !component.get('v.pinnedIds').length > 0) {
                    console.log('Hide Button');
                    component.set("v.disableNext", true);
                }
                else {
                    component.set("v.disableNext", false);
                    console.log('Show Button');
                }

            }
            else if (state === "ERROR") {
                helper.hideSpinner(component, event, helper);
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        })
        $A.enqueueAction(action);
    },
    fillAssemblyWrapper: function (component, event, helper, result, addTrueUpRow) {
        var assetIds = [];
        var utilIds = [];
        var count = 0;
        component.set("v.noRecordsToDisplay", false);
        for (let key in result.operatorData) {
            for (let key1 in result.operatorData[key].assetData) {
                if (result.operatorData[key].assetData[key1].Asset != undefined && result.operatorData[key].assetData[key1].utilization != undefined) {
                    assetIds.push(result.operatorData[key].assetData[key1].Asset.Id);
                    utilIds.push(result.operatorData[key].assetData[key1].utilization.Id);
                }
            }
        }
        for (var i = 0; i < assetIds.length; i++) {
            helper.showSpinner(component, event);
            var action = component.get("c.getUtilizationRelatedData");
            action.setParams({
                assetId: assetIds[i],
                utilizationId: utilIds[i]
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == 'SUCCESS') {
                    for (let key in result.operatorData) {
                        for (let key1 in result.operatorData[key].assetData) {
                            if (result.operatorData[key].assetData[key1].utilization.Id == response.getReturnValue().utilizationId) {
                                var assembly;
                                count++;
                                result.operatorData[key].assetData[key1].lstComponentWrapper = response.getReturnValue().cwList;
                                if (result.operatorData[key].assetData[key1].Asset.RecordType.Name === 'Engine') {
                                    assembly = 'Engine 1';
                                }
                                if (result.operatorData[key].assetData[key1].Asset.RecordType.Name === 'Aircraft') {
                                    assembly = 'Airframe';
                                }
                                for (let key2 in result.operatorData[key].assetData[key1].lstComponentWrapper) {
                                    let asm = result.operatorData[key].assetData[key1].lstComponentWrapper[key2];
                                    if (asm.asmName === "APU" && asm.rateBasis != undefined && asm.rateBasis != '' && asm.rateBasis != null && (asm.rateBasis == 'APU Hour' || asm.rateBasis == 'APU Cycle')) {
                                        if (result.operatorData[key].assetData[key1].apuValues == '') {
                                            result.operatorData[key].assetData[key1].apuValues = asm.mrEvent + ',' + (asm.rate == undefined ? 0 : asm.rate) + '/' + asm.rateBasis;
                                        }
                                        else {
                                            result.operatorData[key].assetData[key1].apuValues += ';' + asm.mrEvent + ',' + (asm.rate == undefined ? 0 : asm.rate) + '/' + asm.rateBasis;
                                        }
                                    }
                                    if (asm.flightHM === "" || asm.flightHM === undefined) {
                                        asm.flightHM = null;
                                        asm.fHUnknown = true;
                                    }
                                    if (asm.flightC === "" || asm.flightC === undefined) {
                                        asm.flightC = null;
                                        asm.fCUnknown = true;
                                    }
                                    if (asm.assemblyUtilizationRecord != undefined && asm.assemblyUtilizationRecord.Constituent_Assembly__r != undefined && asm.assembly === assembly) {
                                        var cw = result.operatorData[key].assetData[key1].lstComponentWrapper[key2];
                                        if (result.operatorData[key].assetData[key1].lstComponentWrapper[key2].assemblyUtilizationRecord.Constituent_Assembly__r.Type__c === assembly) {
                                            console.log('FlightH: ', cw.flightH);
                                            result.operatorData[key].assetData[key1].totalFH = cw.flightH == undefined ? null : cw.flightH;
                                            if (cw.flightHM != '' && cw.flightHM != undefined) {
                                                result.operatorData[key].assetData[key1].totalFHM = cw.flightHM;
                                            }
                                            else {
                                                result.operatorData[key].assetData[key1].totalFHM = cw.flightH == undefined ? null : cw.flightH;
                                            }
                                            result.operatorData[key].assetData[key1].totalFC = cw.flightC == undefined ? null : cw.flightC;
                                            result.operatorData[key].assetData[key1].totalRatio = cw.ratio == undefined ? null : cw.ratio;
                                            result.operatorData[key].assetData[key1].totalTSN = cw.tsn;
                                            result.operatorData[key].assetData[key1].totalCSN = cw.csn;
                                        }
                                    }
                                    result.operatorData[key].assetData[key1].totalBilled += parseInt(asm.billed === undefined ? 0 : parseInt(asm.billed));
                                    result.operatorData[key].assetData[key1].totalBalance += parseInt(asm.balance === undefined ? 0 : parseInt(asm.balance));
                                    result.operatorData[key].assetData[key1].totalPaid += parseInt(asm.paid === undefined ? 0 : parseInt(asm.paid));
                                }
                                console.log(count, assetIds.length);
                                if (count == assetIds.length) {
                                    component.set("v.showBulkButtons", true);
                                    result.operatorData = this.handleAPUValues(result.operatorData);
                                    if (component.get("v.totalAssetsOnCurrentPage") >= 10) {
                                        this.searchNextRecordsonLoad(component, event, helper);
                                    }
                                    // component.set("v.showTable", true);
                                    helper.getTrueUpData(component, event, helper, result, addTrueUpRow);
                                    component.set("v.searchData", result);
                                }
                                //helper.hideSpinner(component,event);
                            }
                        }
                    }
                }
                else if (state == 'ERROR') {
                    helper.hideSpinner(component, event);
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
            });
            $A.enqueueAction(action);
        }
    },
    getTrueUpData: function (component, event, helper, result, addTrueUpRow) {
        var trueUpRecords = [];
        for (let key in result.operatorData) {
            for (let key1 in result.operatorData[key].assetData) {
                if (result.operatorData[key].assetData[key1].utilization.Status__c == "Approved By Lessor") {
                    trueUpRecords.push(result.operatorData[key].assetData[key1]);
                }
            }
        }
        console.log('True Up Records: ', trueUpRecords);
        var count = 0;
        if (trueUpRecords.length > 0) {
            for (var i = 0; i < trueUpRecords.length; i++) {
                var assetId = trueUpRecords[i].Asset.Id;
                var utilizationId = trueUpRecords[i].utilization.Id;
                console.log(assetId, utilizationId);
                var action = component.get("c.getTrueUpData");
                action.setParams({
                    assetId: assetId,
                    utilizationId: utilizationId
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state == 'SUCCESS') {
                        console.log('True Up Result: ', response.getReturnValue());
                        for (let key in result.operatorData) {
                            for (let key1 in result.operatorData[key].assetData) {
                                console.log('TRUE UP', result.operatorData[key].assetData[key1].utilization.Id);
                                if (result.operatorData[key].assetData[key1].utilization.Id == response.getReturnValue().utilizationId) {
                                    count++;
                                    console.log('Added');
                                    result.operatorData[key].assetData[key1].trueUpWrapperList = response.getReturnValue().assetDataWrapperList;
                                    break;
                                }
                            }
                            console.log(count, trueUpRecords.length);
                            if (count === trueUpRecords.length) {
                                helper.fillTrueUpAssemblyWrapper(component, event, helper, result, addTrueUpRow);
                                break;
                            }
                        }
                    }
                    else if (state == 'ERROR') {
                        helper.hideSpinner(component, event);
                        var errors = response.getError();
                        helper.handleErrors(errors);
                    }
                });
                $A.enqueueAction(action);
            }
        }
        else {
            helper.hideSpinner(component, event);
            component.set("v.showTable", true);
        }
    },
    fillTrueUpAssemblyWrapper: function (component, event, helper, result, addTrueUpRow) {
        console.log('fillTrueUpAssemblyWrapper');
        var assetIds = [];
        var utilIds = [];
        var count = 0;
        var addTrueUp = true;
        for (let key in result.operatorData) {
            for (let key1 in result.operatorData[key].assetData) {
                for (let key2 in result.operatorData[key].assetData[key1].trueUpWrapperList) {
                    if (result.operatorData[key].assetData[key1].trueUpWrapperList.length > 0) {
                        if (result.operatorData[key].assetData[key1].trueUpWrapperList[key2].Asset != undefined && result.operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization != undefined) {
                            assetIds.push(result.operatorData[key].assetData[key1].trueUpWrapperList[key2].Asset.Id);
                            utilIds.push(result.operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization.Id);
                        }
                    }
                }
            }
        }
        if (assetIds.length > 0) {
            for (var i = 0; i < assetIds.length; i++) {

                var action = component.get("c.getUtilizationRelatedData");
                action.setParams({
                    assetId: assetIds[i],
                    utilizationId: utilIds[i]
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state == 'SUCCESS') {
                        for (let key in result.operatorData) {
                            for (let key1 in result.operatorData[key].assetData) {
                                for (let key2 in result.operatorData[key].assetData[key1].trueUpWrapperList) {
                                    console.log('In True Up Wrapper List');
                                    if (result.operatorData[key].assetData[key1].trueUpWrapperList[key2].utilization.Id == response.getReturnValue().utilizationId) {
                                        var assembly;
                                        count++;
                                        result.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper = response.getReturnValue().cwList;
                                        if (result.operatorData[key].assetData[key1].trueUpWrapperList[key2].Asset.RecordType.Name === 'Engine') {
                                            assembly = 'Engine 1';
                                        }
                                        if (result.operatorData[key].assetData[key1].trueUpWrapperList[key2].Asset.RecordType.Name === 'Aircraft') {
                                            assembly = 'Airframe';
                                        }
                                        for (let key3 in result.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper) {
                                            let asm = result.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3];
                                            if (asm.asmName === "APU" && asm.rateBasis != undefined && asm.rateBasis != '' && asm.rateBasis != null && (asm.rateBasis == 'APU Hour' || asm.rateBasis == 'APU Cycle')) {
                                                if (result.operatorData[key].assetData[key1].trueUpWrapperList[key2].apuValues == '') {
                                                    result.operatorData[key].assetData[key1].trueUpWrapperList[key2].apuValues = asm.mrEvent + ',' + (asm.rate == undefined ? 0 : asm.rate) + '/' + asm.rateBasis;
                                                }
                                                else {
                                                    result.operatorData[key].assetData[key1].trueUpWrapperList[key2].apuValues += ';' + asm.mrEvent + ',' + (asm.rate == undefined ? 0 : asm.rate) + '/' + asm.rateBasis;
                                                }
                                            }
                                            if (asm.assemblyUtilizationRecord != undefined && asm.assemblyUtilizationRecord.Constituent_Assembly__r != undefined && asm.assembly === assembly) {
                                                var cw = result.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3];
                                                if (result.operatorData[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].assemblyUtilizationRecord.Constituent_Assembly__r.Type__c === assembly) {
                                                    console.log('FlightH: ', cw.flightH);
                                                    result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalFH = cw.flightH == undefined ? 0 : cw.flightH;
                                                    if (cw.flightHM != '' && cw.flightHM != undefined) {
                                                        result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalFHM = cw.flightHM;
                                                    }
                                                    else {
                                                        result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalFHM = cw.flightH == undefined ? null : cw.flightH;
                                                    }
                                                    result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalFC = cw.flightC == undefined ? null : cw.flightC;
                                                    result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalRatio = cw.ratio == undefined ? 0 : cw.ratio;
                                                    result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalTSN = cw.tsn;
                                                    result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalCSN = cw.csn;
                                                }

                                            }
                                            result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalBilled += parseInt(parseInt(cw.billed) == undefined ? 0 : parseInt(cw.billed));
                                            result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalBalance += parseInt(parseInt(cw.balance) == undefined ? 0 : parseInt(cw.balance));
                                            result.operatorData[key].assetData[key1].trueUpWrapperList[key2].totalPaid += parseInt(parseInt(cw.paid) == undefined ? 0 : parseInt(cw.paid));
                                        }
                                        console.log(count, assetIds.length);
                                        if (count == assetIds.length) {
                                            result.operatorData = this.handleTrueUpAPUValues(result.operatorData);
                                            if (addTrueUpRow === "addTrueUpRow") {
                                                var asset = {};
                                                var assemblyList = [];
                                                var assetRecord = component.get("v.assetObj");
                                                var finalAsset;

                                                for (let key in result.operatorData[0].assetData[0].trueUpWrapperList) {
                                                    var status = result.operatorData[0].assetData[0].trueUpWrapperList[key].utilization.Status__c;
                                                    if (status == "Open" || status == "Submitted To Lessor" || status == "Rejected By Lessor") {
                                                        addTrueUp = false;
                                                    }
                                                }
                                                if (addTrueUp) {
                                                    helper.addTrueUpRow(component, result, assemblyList, asset, assetRecord);
                                                }
                                            }
                                            console.log("Result----", result);
                                            component.set("v.showTable", true);
                                            component.set("v.searchData", result);
                                        }
                                        helper.hideSpinner(component, event, helper);
                                    }
                                }
                            }
                        }
                    }
                    else if (state == 'ERROR') {
                        var errors = response.getError();
                        helper.handleErrors(errors);
                    }
                });
                $A.enqueueAction(action);
            }
        }
        else {
            console.log('Result----->', result, addTrueUpRow);
            if (addTrueUpRow === "addTrueUpRow") {
                var asset = {};
                var assemblyList = [];
                var assetRecord;
                helper.addTrueUpRow(component, result, assemblyList, asset, assetRecord);
            }
            component.set("v.showTable", true);
            helper.hideSpinner(component, event, helper);
        }
    },
    addTrueUpRow: function (component, result, assemblyList, asset, assetRecord) {
        console.log('IN IF OF TRUE UP');
        let finalAsset;
        console.log('RESULT: ', result);
        if (result.operatorData[0].assetData[0].trueUpWrapperList.length == 0) {
            if (result.operatorData[0].assetData[0].utilization.Type__c === "Actual") {
                console.log('In IF CONDITION');
                assetRecord = JSON.stringify(result.operatorData[0].assetData[0]);
                asset = JSON.parse(assetRecord);
                asset.utilization.Aircraft__c = result.operatorData[0].assetData[0].Asset.Id;
                asset.trueUpWrapperList = [];
                asset.utilization.Last_Actual_Monthly_Utilization__c = asset.utilization.Id;
                asset.utilization.Original_Monthly_Utilization__c = asset.utilization.Id;
                asset.utilization.Id = null;
                asset.utilization.Status__c = "New";
                asset.utilization.Type__c = "True Up Gross";
                asset.isNewUtilization = true;
                asset.totalBilled = null;
                asset.totalBalance = null;
                asset.totalPaid = null;
                asset.isTrueUp = false;
                asset.color = '#5D8EC9';
                asset.isInvoicePresent = false;
                console.log('AssetToPush: ', asset);
                for (let key in asset.lstComponentWrapper) {
                    var assembly = asset.lstComponentWrapper[key];
                    assembly.billed = null;
                    assembly.balance = null;
                    assembly.units = null;
                    assembly.paid = null;
                    if (asset.lstComponentWrapper[key].assembly != undefined) {
                        if (assembly.associatedAircraft == undefined && assembly.attachedAircraft != undefined) {
                            asset.lstComponentWrapper[key].assocAssetIcon = 'utility:change_record_type';
                        }
                        else {
                            if (assembly.associatedAircraft != assembly.attachedAircraft) {
                                asset.lstComponentWrapper[key].assocAssetIcon = 'utility:change_record_type';
                            }
                        }
                        asset.lstComponentWrapper[key].rate = 0;
                        asset.lstComponentWrapper[key].mrEvent = "";
                        assemblyList.push(asset.lstComponentWrapper[key]);
                    }
                }
                asset.lstComponentWrapper = assemblyList;
                component.set("v.assetObj", JSON.stringify(asset));
                console.log("assetObj: ", JSON.parse(component.get("v.assetObj")));
                result.operatorData[0].assetData[0].trueUpWrapperList.push(JSON.parse(component.get("v.assetObj")));
                this.checkReconciliationCondition(component);
            }
        }
        else {
            let length = result.operatorData[0].assetData[0].trueUpWrapperList.length;
            console.log('In ELSE CONDITION', length);
            console.log(result.operatorData[0].assetData[0].trueUpWrapperList[parseInt(length) - 1].utilization.Type__c);
            if (result.operatorData[0].assetData[0].trueUpWrapperList[parseInt(length) - 1].utilization.Type__c === "True Up Gross") {
                assetRecord = JSON.stringify(result.operatorData[0].assetData[0].trueUpWrapperList[parseInt(length) - 1]);
                asset = JSON.parse(assetRecord);
                asset.utilization.Aircraft__c = result.operatorData[0].assetData[0].trueUpWrapperList[parseInt(length) - 1].Asset.Id;
                asset.trueUpWrapperList = [];
                asset.utilization.Last_Actual_Monthly_Utilization__c = asset.utilization.Id;
                asset.utilization.Id = null;
                asset.utilization.Status__c = "New";
                asset.utilization.Type__c = "True Up Gross";
                asset.isNewUtilization = true;
                asset.totalBilled = null;
                asset.totalBalance = null;
                asset.totalPaid = null;
                asset.isTrueUp = false;
                asset.color = '#5D8EC9';
                asset.isInvoicePresent = false;
                console.log('AssetToPush: ', asset);
                for (let key in asset.lstComponentWrapper) {
                    var assembly = asset.lstComponentWrapper[key];
                    assembly.billed = null;
                    assembly.balance = null;
                    assembly.units = null;
                    assembly.paid = null;
                    if (asset.lstComponentWrapper[key].assembly != undefined) {
                        if (assembly.associatedAircraft == undefined && assembly.attachedAircraft != undefined) {
                            asset.lstComponentWrapper[key].assocAssetIcon = 'utility:change_record_type';
                        }
                        else {
                            if (assembly.associatedAircraft != assembly.attachedAircraft) {
                                asset.lstComponentWrapper[key].assocAssetIcon = 'utility:change_record_type';
                            }
                        }
                        asset.lstComponentWrapper[key].rate = 0;
                        asset.lstComponentWrapper[key].engineThrust = null;
                        asset.lstComponentWrapper[key].mrEvent = "";
                        assemblyList.push(asset.lstComponentWrapper[key]);
                    }
                }
                asset.lstComponentWrapper = assemblyList;
                component.set("v.assetObj", JSON.stringify(asset));
                console.log("assetObj: ", JSON.parse(component.get("v.assetObj")));
                result.operatorData[0].assetData[0].trueUpWrapperList.push(JSON.parse(component.get("v.assetObj")));
                this.checkReconciliationCondition(component);
            }
        }
    },
    checkReconciliationCondition: function (component) {
        let searchData = component.get("v.searchData");
        console.log('Data to check', searchData);

        if (searchData.operatorData[0].assetData[0].utilization.Y_hidden_Lease__c != undefined) {
            let utilizationId = searchData.operatorData[0].assetData[0].utilization.Id;
            let leaseId = searchData.operatorData[0].assetData[0].utilization.Y_hidden_Lease__c;
            let action = component.get("c.getReconciliationCondition");
            action.setParams({
                utilizationId: utilizationId,
                leaseId: leaseId
            });
            action.setCallback(this, function (response) {
                let state = response.getState();
                if (state == "SUCCESS") {
                    console.log('Reconciliation Schedule: ', response.getReturnValue());
                    let hasReconciliation = response.getReturnValue().hasReconciliationSchedule;
                    if (hasReconciliation) {
			let url = window.location.href.substr(0, window.location.href.indexOf('/lightning')) + '/' + response.getReturnValue().reconschedules.Id;
                        if (response.getReturnValue().reconschedules.Status__c === 'Complete') {
                            this.showToastWithHyperlink(component, url, 'This utilization is part of the reconciliation done for the period {0}. Please redo the reconciliation for the differential invoice to reflect the true-up amount.', response.getReturnValue().reconschedules.Name, 'warning');
                        }
                        else {
                            this.showToastWithHyperlink(component, url, 'This utilization is part of the reconciliation done for the period {0}. The reconciled invoice is approved and hence users will have to raise a manual adjustment invoice to include the true up amount.', response.getReturnValue().reconschedules.Name, 'warning');
                        }
                    }
                }
                else if (state == "ERROR") {
                    console.log('ERROR');
                    this.handleErrors(response.getError());
                }
            });
            $A.enqueueAction(action);
        }

    },
    showToastWithHyperlink: function (component, url, msg, name, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Warning!",
            mode: 'dismissible',
            message: 'This is a required message',
            messageTemplate: msg,
            messageTemplateData: [{
                url: url,
                label: name,
            }
            ],
            type: type,
            duration: '5000'
        });
        toastEvent.fire();
    },
    handleAPUValues: function (allDataList) {
        for (let key in allDataList) {
            for (let key1 in allDataList[key].assetData) {
                var apukey = 0;
                var apukey1 = 0;
                var apukey2 = 0;
                var hasapu = false;
                var apuFHM;
                var apuFC;
                var apuFH;
                var nextassembly = false;
                var apuTSN;
                var finalapukey;
                var apuCSN;
                var hasapuCounterRow = false;
                var count = 0;
                var apuValueList = allDataList[key].assetData[key1].apuValues.split(';');
                for (let key2 in allDataList[key].assetData[key1].lstComponentWrapper) {
                    if (allDataList[key].assetData[key1].lstComponentWrapper[key2].asmName == 'APU') {
                        if (!allDataList[key].assetData[key1].lstComponentWrapper[key2].apuCounterRow) {
                            hasapuCounterRow = true;
                        }
                        else {
                            count++;
                        }
                    }
                    if (hasapu && allDataList[key].assetData[key1].lstComponentWrapper[key2].assembly == undefined) {
                        apukey2++;
                        console.log('apu key1', apukey2);
                    }
                    var assemblyData = allDataList[key].assetData[key1].lstComponentWrapper[key2];
                    console.log('assemblyData: ', assemblyData);
                    if (nextassembly && assemblyData.assembly != undefined) {
                        finalapukey = parseInt(apukey2);
                        console.log('finalapukey: ', finalapukey);
                        nextassembly = false;
                    }
                    if (assemblyData.assembly != undefined && assemblyData.assembly === 'APU') {
                        console.log('FINAL APU KEY: ', finalapukey);
                        if (finalapukey === undefined) {
                            finalapukey = parseInt(key2) + 1;
                        }
                        nextassembly = true;
                        apuFHM = assemblyData.apuFHM;
                        apuFC = assemblyData.apuFC;
                        if (apuFHM == '') {
                            apuFHM = assemblyData.apuFH;
                        }
                        apuTSN = assemblyData.apuTSN;
                        apuCSN = assemblyData.apuCSN;
                        console.log(assemblyData.apuFHM, assemblyData.apuFC, assemblyData.apuTSN, assemblyData.apuCSN);
                        hasapu = true;
                        apukey = key;
                        apukey1 = key1;
                        apukey2 = parseInt(key2);
                        var derateRowObj = {
                            'assembly': 'APU Values',
                            'showAPUrow': true,
                            'apuFH': null,
                            'apuFHM': '',
                            'apuFC': null
                        }
                    }
                }
                if (hasapu) {
                    console.log('apu key2', apukey2);
                    allDataList[apukey].assetData[apukey1].lstComponentWrapper.splice(parseInt(finalapukey) + parseInt(1), 0, derateRowObj);
                    allDataList[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM = apuFHM;
                    allDataList[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC = apuFC;
                    //allDataList[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2)+parseInt(1)].apuFH = apuFH;
                    allDataList[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuTSN = apuTSN;
                    allDataList[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuCSN = apuCSN;
                    //console.log('IN APU BLOCK: ',allDataList[apukey].assetData[apukey1].apuValues);
                    if (allDataList[apukey].assetData[apukey1].apuValues != '')
                        this.handleAPUEvents(allDataList[apukey].assetData[apukey1].lstComponentWrapper, apukey2, hasapuCounterRow, count, apuValueList);
                }
                console.log('Updated Values: ', allDataList);
            }
        }
        return allDataList;
    },
    handleAPUEvents: function (lstComponentWrapper, apukey2, hasapuCounterRow, count, apuValueList) {
        try {
            console.log('IN handleAPUEvents', count);
            if (count == 1) {
                for (let key in lstComponentWrapper) {
                    if (lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].apuCounterRow) {
                        let apuFHM = lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM;
                        //let apuFH = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH);
                        let apuFC = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC);
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)] = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].showAPUrow = true;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].serialNumber = '';
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM = apuFHM;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC = apuFC;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].assembly = 'APU Values';
                        break;
                    }
                }

                console.log('hasapuCounterRow: ', hasapuCounterRow);
                if (hasapuCounterRow) {
                    let assemblyRow = false;
                    for (let key in lstComponentWrapper) {
                        if ((!lstComponentWrapper[key].apuCounterRow) && lstComponentWrapper[key].assembly == 'APU') {
                            assemblyRow = true;
                            break;
                        }
                    }

                    if (assemblyRow) {
                        for (let key in lstComponentWrapper) {
                            if (lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].assembly === undefined) {
                                console.log('Remove Row: ', lstComponentWrapper[key]);
                                lstComponentWrapper.splice(key, 1);
                            }
                        }
                    }
                    else {
                        // Get MR Event for APU H or APU C row
                        let apuRow = {};
                        for (let key in lstComponentWrapper) {
                            if (lstComponentWrapper[key].asmName == 'APU' && (!lstComponentWrapper[key].apuCounterRow)) {

                                apuRow = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                                break;
                            }
                        }
                        console.log('APU ROW: ', apuRow);
                        for (let key in lstComponentWrapper) {
                            if (lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].assembly === 'APU') {
                                let FHM = lstComponentWrapper[key].flightHM;
                                let FC = lstComponentWrapper[key].flightC;
                                lstComponentWrapper[key] = apuRow;
                                lstComponentWrapper[key].assembly = 'APU';
                                lstComponentWrapper[key].hideFH = false;
                                lstComponentWrapper[key].flightHM = FHM;
                                lstComponentWrapper[key].flightC = FC;
                                if (lstComponentWrapper[key].flightHM != undefined) {
                                    lstComponentWrapper[key].fHUnknown = false;
                                }
                                if (lstComponentWrapper[key].flightC != undefined) {
                                    lstComponentWrapper[key].fCUnknown = false;
                                }
                                lstComponentWrapper[parseInt(apukey2) + parseInt(1)].assembly = 'APU Values';
                                break;
                            }
                        }
                        for (let key in lstComponentWrapper) {
                            if (lstComponentWrapper[key].asmName == 'APU' && (!lstComponentWrapper[key].apuCounterRow) && lstComponentWrapper[key].assembly === undefined) {
                                console.log('Remove Row: ', lstComponentWrapper[key]);
                                lstComponentWrapper.splice(key, 1);
                                break;
                            }
                        }
                    }
                }
                else {
                    for (let key in lstComponentWrapper) {
                        let asm = lstComponentWrapper[key];
                        if (asm.asmName == 'APU' && asm.assembly === 'APU') {
                            asm.mrEvent = '';
                            asm.rate = null;
                            asm.rateBasis = '';
                            asm.tsn = null;
                            asm.csn = null;
                            asm.billed = null;
                            asm.balance = null;
                            asm.units = null;
                            asm.paid = null;
                            asm.hideCommentField = false;
                        }
                    }
                }
            }
            else if (count == 2) {
                for (let key in lstComponentWrapper) {
                    if (lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].apuCounterRow) {
                        lstComponentWrapper[key].firstCounterRow = true;
                        let apuFHM = lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM;
                        //let apuFH = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH);
                        let apuFC = parseInt(lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC);
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)] = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].showAPUrow = true;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].serialNumber = '';
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].assembly = 'APU Values';
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFHM = apuFHM;
                        lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFC = apuFC;
                        //lstComponentWrapper[parseInt(apukey2) + parseInt(1)].apuFH = Number(apuFH);
                        break;
                    }
                }
                var apuRow = {};
                for (let key in lstComponentWrapper) {
                    if (lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].apuCounterRow && (!lstComponentWrapper[key].firstCounterRow)) {
                        apuRow = JSON.parse(JSON.stringify(lstComponentWrapper[key]));
                        apuRow.showAPUrow = false;
                        apuRow.serialNumber = '';
                        apuRow.assembly = '';
                        apuRow.hideAPUInputs = true;
                        break;
                    }
                }

                lstComponentWrapper.splice(parseInt(apukey2) + parseInt(2), 0, apuRow);
                for (let key in lstComponentWrapper) {
                    let asm = lstComponentWrapper[key];
                    if (asm.asmName == 'APU' && asm.assembly === 'APU') {
                        asm.mrEvent = '';
                        asm.rate = null;
                        asm.rateBasis = '';
                        asm.tsn = null;
                        asm.csn = null;
                        asm.billed = null;
                        asm.balance = null;
                        asm.units = null;
                        asm.paid = null;
                        asm.hideCommentField = false;
                    }
                    console.log(lstComponentWrapper[key].asmName);
                    console.log(lstComponentWrapper[key].assembly);
                    if (lstComponentWrapper[key].asmName == 'APU' && lstComponentWrapper[key].assembly === undefined) {
                        console.log('Delete: ', lstComponentWrapper[key]);
                        lstComponentWrapper.splice(key, 1);
                    }
                }
            }
        }
        catch (e) {
            console.log(e);
        }
    },
    handleTrueUpAPUValues: function (allDataList) {
        for (let key in allDataList) {
            for (let key1 in allDataList[key].assetData) {
                for (let key2 in allDataList[key].assetData[key1].trueUpWrapperList) {
                    var apukey = 0;
                    var apukey1 = 0;
                    var apukey2 = 0;
                    var apukey3 = 0;
                    var hasapu = false;
                    var apuFHM;
                    var apuFC;
                    var apuFH;
                    var nextassembly = false;
                    var apuTSN;
                    var finalapukey;
                    var apuCSN;
                    var hasapuCounterRow = false;
                    var count = 0;
                    var apuValueList = allDataList[key].assetData[key1].trueUpWrapperList[key2].apuValues.split(';');
                    for (let key3 in allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper) {
                        if (allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].asmName == 'APU') {
                            if (!allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].apuCounterRow) {
                                hasapuCounterRow = true;
                            }
                            else {
                                count++;
                            }
                        }
                        if (hasapu && allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3].assembly == undefined) {
                            apukey3++;
                            console.log('apu key1', apukey2);
                        }
                        var assemblyData = allDataList[key].assetData[key1].trueUpWrapperList[key2].lstComponentWrapper[key3];
                        console.log('assemblyData: ', assemblyData);
                        if (nextassembly && assemblyData.assembly != undefined) {
                            finalapukey = parseInt(apukey3);
                            console.log('finalapukey: ', finalapukey);
                            nextassembly = false;
                        }
                        if (assemblyData.assembly != undefined && assemblyData.assembly === 'APU') {
                            if (finalapukey === undefined) {
                                finalapukey = parseInt(key2) + 1;
                            }
                            nextassembly = true;
                            apuFHM = assemblyData.apuFHM;
                            apuFC = assemblyData.apuFC;
                            if (apuFHM == '') {
                                apuFHM = assemblyData.apuFH;
                            }
                            apuTSN = assemblyData.apuTSN;
                            apuCSN = assemblyData.apuCSN;
                            console.log(assemblyData.apuFHM, assemblyData.apuFC, assemblyData.apuTSN, assemblyData.apuCSN);
                            hasapu = true;
                            apukey = key;
                            apukey1 = key1;
                            apukey2 = key2;
                            apukey3 = parseInt(key3);
                            var derateRowObj = {
                                'assembly': 'APU Values',
                                'showAPUrow': true,
                                'apuFH': null,
                                'apuFHM': '',
                                'apuFC': null
                            }
                        }
                    }
                    if (hasapu) {
                        console.log('apukey3', apukey3);
                        allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper.splice(parseInt(finalapukey) + parseInt(1), 0, derateRowObj);
                        allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuFHM = apuFHM;
                        allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuFC = apuFC;
                        //allDataList[apukey].assetData[apukey1].lstComponentWrapper[parseInt(apukey2)+parseInt(1)].apuFH = apuFH;
                        allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuTSN = apuTSN;
                        allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper[parseInt(apukey3) + parseInt(1)].apuCSN = apuCSN;
                        if (allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].apuValues != '')
                            this.handleAPUEvents(allDataList[apukey].assetData[apukey1].trueUpWrapperList[apukey2].lstComponentWrapper, apukey3, hasapuCounterRow, count, apuValueList);
                    }
                    console.log('Updated Values: ', allDataList);
                }
            }
        }
        return allDataList;
    },

    handleAssetsToShow: function (component, allDataList, result, helper) {
        console.log('Operator Data: ', result.operatorData);
        var notIdsPresentList = [];
        var isExists = false;
        var pinnedIdsList = component.get('v.pinnedIds');
        var existingData = component.get('v.searchData');
        var assetExists = false;
        var pinnedIdwithStatusMap = new Map();
        for (let key in pinnedIdsList) {
            assetExists = false;
            for (let key1 in existingData.operatorData) {
                for (let key2 in existingData.operatorData[key1].assetData) {
                    if (existingData.operatorData[key1].assetData[key2].Asset.Id == pinnedIdsList[key]) {
                        assetExists = true;
                        pinnedIdwithStatusMap.set(pinnedIdsList[key], existingData.operatorData[key1].assetData[key2].utilization.Status__c);
                        break;
                    }
                }
                if (assetExists) {
                    break;
                }
            }
        }
        if (component.get('v.pinnedIds').length > 0) {
            for (let key in pinnedIdsList) {
                isExists = false;
                for (let key1 in allDataList) {
                    for (let key2 in allDataList[key1].assetData) {
                        if (allDataList[key1].assetData[key2].Asset.Id == pinnedIdsList[key] && pinnedIdwithStatusMap.get(pinnedIdsList[key]) == allDataList[key1].assetData[key2].utilization.Status__c) {
                            isExists = true;
                            break;
                        }
                    }
                    if (isExists) {
                        break;
                    }
                }
                if (!isExists) {
                    notIdsPresentList.push(pinnedIdsList[key]);
                }
            }
        }
        console.log('notIdsPresentList: ', notIdsPresentList);
        var listToPush = [];
        var finallistToPush = [];
        var operatorIdsAdded = [];
        if (notIdsPresentList.length > 0) {
            var searchData = component.get('v.searchData');
            var assetsToInclude = [];
            for (let key in searchData.operatorData) {
                for (let key2 in searchData.operatorData[key].assetData) {
                    if (notIdsPresentList.includes(searchData.operatorData[key].assetData[key2].Asset.Id) && searchData.operatorData[key].assetData[key2].ispinned) {
                        if (!operatorIdsAdded.includes(searchData.operatorData[key].Operator.Id)) {
                            listToPush.push(searchData.operatorData[key]);
                            operatorIdsAdded.push(searchData.operatorData[key].Operator.Id);
                        }
                        assetsToInclude.push(searchData.operatorData[key].assetData[key2].Asset.Id);
                    }
                }
            }
            console.log('assetsToInclude: ', assetsToInclude);
            var recordsToRemove = [];
            finallistToPush = Array.from(new Set(listToPush));

            var operatorRecord = component.get("v.operatorRecord");
            var assetRecord = component.get("v.assetRecord");
            if ((assetRecord != null && assetRecord != '' && assetRecord != undefined) || (operatorRecord != null && operatorRecord != '' && operatorRecord != undefined) || (notIdsPresentList.length == assetsToInclude.length)) {
                for (let key in finallistToPush) {
                    var i = finallistToPush[key].assetData.length;
                    while (i--) {
                        if (!assetsToInclude.includes(finallistToPush[key].assetData[i].Asset.Id)) {
                            finallistToPush[key].assetData.splice(i, 1);
                        }
                    }
                }
            }
        }
        console.log('finallistToPush[key]: ', finallistToPush);
        console.log('result.operatorData1:', result.operatorData);
        if (finallistToPush.length > 0) {
            for (let key in finallistToPush) {
                result.operatorData.push(finallistToPush[key]);
            }
        }
        console.log('result.operatorData2:', result.operatorData);
        var opIds = [];
        var removeDuplicatesList = [];
        for (let key in result.operatorData) {
            if (!opIds.includes(result.operatorData[key].Operator.Id)) {
                opIds.push(result.operatorData[key].Operator.Id);
                removeDuplicatesList.push(result.operatorData[key]);
            }
            else {
                for (let key1 in removeDuplicatesList) {
                    if (result.operatorData[key].Operator.Id == removeDuplicatesList[key1].Operator.Id) {
                        for (let key2 in result.operatorData[key].assetData) {
                            removeDuplicatesList[key1].assetData.push(result.operatorData[key].assetData[key2]);
                        }
                    }
                }
            }
        }
        result.operatorData = removeDuplicatesList;
        console.log('removeDuplicatesList: ', result.operatorData);
        var numberOfAssets = 0;
        for (let key in result.operatorData) {
            numberOfAssets += result.operatorData[key].assetData.length;
        }
        if (numberOfAssets > 10) {
            var recordsToRemove = numberOfAssets - 10;
            var count = 0;
            var breakloop = false;
            for (let key in result.operatorData) {
                var i = result.operatorData[key].assetData.length;
                while (i--) {
                    if (!component.get('v.pinnedIds').includes(result.operatorData[key].assetData[i].Asset.Id) && result.operatorData[key].assetData.length > 1) {
                        if (count == recordsToRemove) {
                            breakloop = true;
                            break;
                        }
                        result.operatorData[key].assetData.splice(i, 1);
                        if (result.operatorData[key].assetData.length == 0) {
                            result.operatorData.splice(key, 1);
                        }
                        count++;
                    }
                    if (breakloop) {
                        break;
                    }
                }
            }
        }
        for (let key in result.operatorData) {
            if (result.operatorData[key].Operator.Name != 'No Operator') {
                result.operatorData[key].numberOfAssets = result.operatorAssetCountMap[result.operatorData[key].Operator.Id];
            }
            else {
                result.operatorData[key].numberOfAssets = result.operatorData[key].assetData.length;
            }
            result.operatorData[key].assetsToShow = result.operatorData[key].assetData.length;
        }
    },
    // Show the lightning Spinner
    showSpinner: function (component, event) {
        component.set("v.showSpinner", true);
    },

    // Hide the lightning Spinner
    hideSpinner: function (component, event) {
        component.set("v.showSpinner", false);
    },
    searchNextRecords: function (component, event, helper) {
        var pageNumber = component.get("v.pageNumber");
        pageNumber += 1;
        component.set("v.pageNumber", pageNumber);
        component.set("v.previousList", []);
        component.set("v.isPreviousDisabled", false);
        component.set("v.isInitialLoad", false);
        var ignoreIdsList = component.get("v.ignoreIdsList");
        helper.searchDataonLoad(component, event, helper);
    },
    searchPreviousRecords: function (component, event, helper) {
        var pageNumber = component.get("v.pageNumber");
        pageNumber -= 1;
        component.set("v.pageNumber", pageNumber);
        var assetcount = component.get("v.assetCount");
        assetcount = assetcount - (component.get("v.totalAssetsOnCurrentPage") + parseInt(10));
        component.set("v.assetCount", assetcount);
        component.set("v.isInitialLoad", false);
        var ignoreIdsList = component.get("v.ignoreIdsList");
        var pinnedList = component.get("v.pinnedIds")
        var currentpagelength = component.get("v.totalAssetsOnCurrentPage");
        console.log('----->', pinnedList.length, ignoreIdsList.length);
        if (pinnedList.length == 0 && ignoreIdsList.length >= 10) {
            if (ignoreIdsList.length - (10 + currentpagelength) < 0) {
                ignoreIdsList.length = 0;
            }
            else {
                ignoreIdsList.length = ignoreIdsList.length - (10 + currentpagelength);
            }
            console.log('List: ', ignoreIdsList);
            component.set("v.ignoreIdsList", ignoreIdsList);
        }
        else {
            var listToSend = [];
            for (let key in pinnedList) {
                listToSend.push(pinnedList[key]);
            }
            console.log('Before: ', ignoreIdsList);
            ignoreIdsList.length = ignoreIdsList.length - (currentpagelength - listToSend.length);
            console.log('After: ', ignoreIdsList);
            var newList = [];
            console.log(ignoreIdsList.length);
            var recordstoFill = 10 - listToSend.length;
            console.log(recordstoFill);
            var i = 0;
            for (var j = ignoreIdsList.length - 1; j >= 0; j--) {
                listToSend.push(ignoreIdsList[j]);
                i++;
                if (i == recordstoFill) {
                    break;
                }
            }
            for (let key in ignoreIdsList) {
                var isIdExist = false;
                for (let key1 in listToSend) {
                    if (ignoreIdsList[key] == listToSend[key1]) {
                        isIdExist = true;
                        break;
                    }
                }
                if (!isIdExist) {
                    newList.push(ignoreIdsList[key]);
                }
            }
            if (newList.length == 10) {
                component.set("v.isPreviousDisabled", true);
            }
            component.set("v.ignoreIdsList", newList);
            console.log('New Id List: ', newList);
            console.log('Previous pinnedList: ', pinnedList);
            console.log('listToSend: ', listToSend);
            component.set("v.previousList", listToSend);
        }
        helper.searchDataonLoad(component, event, helper);
    },
    saveRecords: function (component, event, helper, recordsToSave) {
        helper.showSpinner(component, event, helper);
        var index = 0;

        var searchData = component.get("v.searchData");
        for (var i = 0; i < recordsToSave.length; i++) {
            var action = component.get("c.updateStatus");
            action.setParams({
                utilizationRecord: JSON.stringify(recordsToSave[i]),
                email: '',
                invoiceRecord: '',
                requestType: ''
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log('result:', result);
                    if (result != undefined && result != '' && result != null) {
                        if (result.ErrorMsg != '' && result.ErrorMsg != undefined && result.ErrorMsg != '') {
                            helper.handleErrors(result.ErrorMsg);
                            helper.hideSpinner(component, event, helper);
                        }
                        else {
                            for (let key in searchData.operatorData) {
                                for (let key1 in searchData.operatorData[key].assetData) {
                                    if (searchData.operatorData[key].assetData[key1].utilization.Id == result.utilRecord.Id) {
                                        index++;
                                        console.log('index: ', index);
                                        searchData.operatorData[key].assetData[key1].utilization.Status__c = result.status;
                                        if (result.status == "Submitted To Lessor") {
                                            searchData.operatorData[key].assetData[key1].color = "#F5CA48";
                                            searchData.operatorData[key].assetData[key1].rowIcon = "approval";
                                        }
                                        if (result.status == "Rejected By Lessor") {
                                            searchData.operatorData[key].assetData[key1].color = "#FF8F7F";
                                            searchData.operatorData[key].assetData[key1].rowIcon = "save";
                                        }
                                        if (result.status == "Open") {
                                            searchData.operatorData[key].assetData[key1].color = "#5D8EC9";
                                            searchData.operatorData[key].assetData[key1].rowIcon = "send";
                                        }
                                        if (result.status == "Approved By Lessor") {
                                            searchData.operatorData[key].assetData[key1].invoice = result.invoiceList;
                                            if (result.invoiceList.length > 0 && result.invoiceList[0].Invoice_Line_Items__r != undefined) {
                                                if (result.invoiceList[0].Invoice_Line_Items__r.length > 0) {
                                                    for (let key2 in result.invoiceList[0].Invoice_Line_Items__r) {
                                                        var invoiceLineItem = result.invoiceList[0].Invoice_Line_Items__r[key2];
                                                        //console.log('Invoice Line Item: ',invoiceLineItem);
                                                        for (let key3 in searchData.operatorData[key].assetData[key1].lstComponentWrapper) {
                                                            let assembly = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key3];
                                                            //console.log('Assembly: ',assembly,assembly.assemblyUtilizationRecord);
                                                            if (assembly.assemblyUtilizationRecord != undefined && assembly.assemblyUtilizationRecord != null && invoiceLineItem != undefined) {
                                                                if (assembly.assemblyUtilizationRecord.Id == invoiceLineItem.Assembly_Utilization__c) {
                                                                    searchData.operatorData[key].assetData[key1].lstComponentWrapper[key3].balance = invoiceLineItem.Assembly_Utilization__r.Total_Maintenance_Reserve__c;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    for (let key4 in searchData.operatorData[key].assetData[key1].lstComponentWrapper) {
                                                        let bal = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key4].balance;
                                                        if (bal != undefined)
                                                            searchData.operatorData[key].assetData[key1].totalBalance += bal;
                                                    }
                                                }
                                            }
                                            searchData.operatorData[key].assetData[key1].color = "#1B9F88";
                                            searchData.operatorData[key].assetData[key1].rowIcon = "description";
                                        }
                                        if (index == recordsToSave.length) {
                                            helper.hideSpinner(component, event, helper);
                                            var allDataList = searchData.operatorData;
                                            let openButtonCheck = false;
                                            let submitButtonCheck = false;
                                            let approveButtonCheck = false;
                                            component.set("v.showSaveAll", false);
                                            for (let key in allDataList) {
                                                for (let key1 in allDataList[key].assetData) {
                                                    if (allDataList[key].assetData[key1].utilization.Status__c == 'Open') {
                                                        openButtonCheck = true;
                                                        component.set("v.showSaveAll", true);
                                                        component.set("v.approveAllUtilization", false);
                                                        component.set("v.sendInvoices", false);
                                                        component.set("v.submitForApproval", true);
                                                    }
                                                    else {
                                                        if (allDataList[key].assetData[key1].utilization.Status__c == 'Submitted To Lessor') {
                                                            if (!openButtonCheck) {
                                                                submitButtonCheck = true;
                                                                component.set("v.submitForApproval", false);
                                                                component.set("v.sendInvoices", false);
                                                                component.set("v.approveAllUtilization", true);
                                                            }
                                                        }
                                                        else {
                                                            if (allDataList[key].assetData[key1].utilization.Status__c == 'Approved By Lessor') {
                                                                if (!submitButtonCheck) {
                                                                    approveButtonCheck = true;
                                                                    component.set("v.approveAllUtilization", false);
                                                                    component.set("v.submitForApproval", false);
                                                                    component.set("v.sendInvoices", true);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            component.set("v.searchData", searchData);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    console.log('Response: ', response.getReturnValue());
                }
                else if (state === "ERROR") {
                    helper.hideSpinner(component, event, helper);
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
            })
            $A.enqueueAction(action);
        }
    },
    clearFilters: function (component, event, helper) {
        component.set("v.approveAllUtilization", false);
        component.set("v.sendInvoices", false);
        component.set("v.submitForApproval", false);
        component.set("v.showTable", false);
        component.set("v.assetRecord", null);
        component.set("v.operatorRecord", null);
        var searchFilters = component.get("v.searchFilters");
        searchFilters.DateOption = 'All';
        searchFilters.InputDate = '';
        searchFilters.AssetType = 'All';
        searchFilters.Status = 'All';
        component.set("v.searchFilters", searchFilters);
        var searchData = component.get("v.searchData");
        if (searchData != null && searchData != '' && searchData != undefined) {
            if (!searchData.operatorData.length == 0) {
                for (let key in searchData.operatorData) {
                    for (let key1 in searchData.operatorData[key].assetData) {
                        if (searchData.operatorData[key].assetData[key1].ispinned) {
                            searchData.operatorData[key].assetData[key1].ispinned = false;
                        }
                    }
                }
                component.set("v.searchData", searchData);
                var pinnedList = component.get("v.pinnedIds");
                if (pinnedList.length > 0) {
                    pinnedList = [];
                    component.set("v.pinnedIds", pinnedList);
                }
            }
        }
    },
    //Show the error toast when any error occured
    handleErrors: function (errors, type, assetName) {
        this.handleErrorMessage(errors, type, assetName);
    },
    handleErrorMessage: function (errormsg, type, assetName) {
        var _self = this;
        console.log('handleErrorMessage');
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errormsg === 'object') {
            errormsg.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    error.message = _self.parseErrorMessage(error.message);
                    if (assetName != undefined && assetName != '' && assetName != null) {
                        error.message = assetName + ': ' + error.message;
                    }
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });
                    };
                }
            });
        } else if (typeof errormsg === "string" && errormsg.length != 0) {
            toastParams.message = errormsg;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    },
    parseErrorMessage: function (errormsg) {
        var errormessage;
        var finalErrorMsg;
        var msgList;
        // Error handler if error is coming from trigger
        if (errormsg.includes("FIELD_CUSTOM_VALIDATION_EXCEPTION")) {
            var message = errormsg;
            msgList = message.split('FIELD_CUSTOM_VALIDATION_EXCEPTION');
            /*
        Output: msgList[length-1] = , You cannot modify MR Rates once the Lease is Approved.: [Lease__c]
	
                                        Class.URLITriggerHandler.updateMRInfos: line 355, column 1
                                        Class.URLITriggerHandler.afterInsert: line 286, column 1
                                        Class.TriggerFactory.execute: line 73, column 1
                                        Class.TriggerFactory.createHandler: line 27, column 1
                                        Trigger.URLITrigger: line 3, column 1: []: []
        */

            errormsg = this.getErrorType(msgList[msgList.length - 1], finalErrorMsg, errormessage, true);
            console.log('Error: ', errormsg);
        }
        else {
            errormsg = this.getErrorType(errormsg, finalErrorMsg, errormessage, false);;
        }
        return errormsg;
    },
    getErrorType: function (errorMsg, finalErrorMsg, errormessage, hasFieldCustom) {
        if (errorMsg.includes('LeaseWareException')) {
            finalErrorMsg = this.getErrors(errorMsg, 'LeaseWareException', errormessage);
        }
        else {
            if (errorMsg.includes('QueryException')) {
                finalErrorMsg = this.getErrors(errorMsg, 'QueryException', errormessage);
            }
            else {
                if (errorMsg.includes("NullPointerException")) {
                    finalErrorMsg = this.getErrors(errorMsg, 'NullPointerException', errormessage);
                }
                else {
                    if (errorMsg.includes("StringException")) {
                        finalErrorMsg = this.getErrors(errorMsg, 'StringException', errormessage);
                    }
                    else {
                        if (errorMsg.includes("MathException")) {
                            finalErrorMsg = this.getErrors(errorMsg, 'MathException', errormessage);
                        }
                        else {
                            if (errorMsg.includes("ListException")) {
                                finalErrorMsg = this.getErrors(errorMsg, 'ListException', errormessage);
                            }
                            else {
                                if (hasFieldCustom) {
                                    if (errorMsg.includes('Class.')) {
                                        errormessage = errorMsg.substr(0, errorMsg.indexOf('Class.')); // Output: , You cannot modify MR Rates once the Lease is Approved. [Lease__c]
                                    }
                                    else {
                                        errormessage = errorMsg;
                                    }
                                    finalErrorMsg = errormessage.substr(0, errormessage.indexOf(']') + 1).substr(1); // Output: You cannot modify MR Rates once the Lease is Approved.: [Lease__c]
                                }
                                else {
                                    finalErrorMsg = errorMsg;
                                }
                            }
                        }
                    }
                }
            }
        }
        return finalErrorMsg;
    },
    getErrors: function (message, exceptionType, errormessage) {
        var messagesList;
        var classNameWithMsg;
        if (message.includes('Class.')) {
            messagesList = message.split('Class.');
            classNameWithMsg = messagesList[1];
            errormessage = message.substr(0, message.indexOf('Class.'));
            if (exceptionType == 'NullPointerException') {
                errormessage = errormessage.substr(errormessage.indexOf(exceptionType)) + classNameWithMsg + '. Please contact LeaseWorks Support.';
            }
            else {
                errormessage = errormessage.substr(errormessage.indexOf(exceptionType));
            }
        }
        else {
            errormessage = message.substr(message.indexOf(exceptionType));
        }
        return errormessage;
    },
})