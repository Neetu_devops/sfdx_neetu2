({
   
    scriptsLoaded : function(component, event, helper) {
        console.log('JS Library Loaded');
        component.set("v.isScriptLoaded", true);
        var showShortVersion = component.get("v.showShortVersion");
        if(!showShortVersion){
        	helper.getNameSpace(component, event, helper);
        }
        helper.getAPUCounterState(component);
    },
    createNew : function(component, event, helper) {
        component.set("v.isSearchPopup",false);
        component.set("v.isCreateNew", true);
        var searchData= component.get("v.searchData");
        if(searchData != undefined){
            for(let key in searchData.operatorData){
                for(let key1 in searchData.operatorData[key].assetData){
                    searchData.operatorData[key].assetData[key1].ischanged = false;
                    if(searchData.operatorData[key].assetData[key1].utilization.Status__c != "New"){
                        if(searchData.operatorData[key].assetData[key1].ispinned){
                            component.set("v.pinnedIds",[]);
                            searchData.operatorData[key].assetData[key1].ispinned = false;
                        }
                    }
                } 
            }
        }
        component.set("v.searchFilters.Status", "Create New");
        helper.setSearchFilters(component, event, helper);
    },
    // This method is used to reload component whenever user switch from one record page to another.
    reload: function(component, event, helper){
        var showShortVersion = component.get("v.showShortVersion");
        if(!showShortVersion){
            var nameSpace = component.get("v.nameSpacePrefix");
            var param;
            if(nameSpace != '' && nameSpace != null && nameSpace != undefined){
                param = 'leaseworks__Id';
            }
            else{
                param = 'c__Id';
            }
            var result = decodeURIComponent((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
            console.log('Code: '+result);
            if(result == undefined || result == null || result == ''){
                var base64Context;
                var pageRef = component.get("v.pageReference");
                if (pageRef != null && pageRef != undefined && pageRef != '') {
                    let state = pageRef.state; // state holds any query params
                    base64Context = state.inContextOfRef;
                    console.log('base64Context: ', base64Context);
                }
                if (base64Context != undefined) {
                    // For some reason, the string starts with "1.", if somebody knows why,
                    // this solution could be better generalized.
                    if (base64Context.startsWith("1\.")) {
                        base64Context = base64Context.substring(2);
                    }
                    var addressableContext = JSON.parse(window.atob(base64Context));
                    result = addressableContext.attributes.recordId;
                }
            }
            if(result!=undefined && result!=null && result!=''){
                helper.clearFilters(component, event, helper);
                helper.init(component, event, helper, true);
            }
        }
    },
    //doinit: function(component, event, helper) {
        
    //},
    // This method is used when value of Asset is changed on UI
    handleAssetChange : function(component, event, helper) {
        var assetRecord = component.get("v.assetRecord");
        if(assetRecord != null && assetRecord != '' && assetRecord != undefined ){
            component.set("v.changeDateOptions", true);
            component.set("v.assetSelected", true);
            var searchFilters = component.get("v.searchFilters");
            //searchFilters.DateOption = 'All';
           // searchFilters.InputDate = '';
            searchFilters.AssetType = 'All';
            component.set("v.searchFilters",searchFilters);
        }
        else{
            component.set("v.changeDateOptions", false);
            component.set("v.assetSelected", false);
        }
    },
    // This method is used when value of Operator is changed on UI
    handleOperatorChange: function(component, event, helper) {
        var operatorRecord = component.get("v.operatorRecord");
        if(operatorRecord != null && operatorRecord != '' && operatorRecord != undefined){
            component.set("v.operatorSelected", true);
        }
        else{
            component.set("v.operatorSelected", false);
        }
    },
    discardAndSearch: function(component, event, helper){
        
        if(component.get("v.isSearchPopup")){
            component.set("v.searchFilters.Status", "All");
        }
        helper.setSearchFilters(component, event, helper);
        component.set("v.showSearchModal",false);
    },
    saveAndSearch: function(component, event, helper){
        var action = component.get('c.handleFHSave');
        $A.enqueueAction(action);
        component.set("v.showSearchModal",false);
    },
    // This method executes when user click on search button on UI
    search : function(component, event, helper){
        component.set("v.isSearchPopup",true);
        component.set("v.changeButtonColor", true);
        var searchButtonValue = event.getSource().get("v.value");
        console.log(searchButtonValue);
        helper.setSearchFilters(component, event, helper,false,searchButtonValue);
    },
    
    
    handleComponentEvent : function(component, event, helper){
        var asset = event.getParam("recordByEvent");
        if(asset.Id == null || asset.Id == undefined || asset.Id == ''){
            component.set("v.assetRecord", null);
            component.set("v.operatorRecord", null);
            component.set("v.assetSelected", false);    
        }
    },
    
    // This function executes when a utilizations in "New" Status are to be saved
    handleFHSave: function(component, event, helper){
        var recordsToSave=[];
        var errorList=[];
        var searchData = component.get("v.searchData");
        var operatorData = searchData.operatorData;
        var status;
        var record;
        var assemblyRecord;
        var hasEmptyValues = true;
        for(let key in operatorData){
            for(let key1 in operatorData[key].assetData){
                record = operatorData[key].assetData[key1];
                if (record.totalFHM === '' && record.totalFC === undefined) {
                    hasEmptyValues = true;
                    for (let key2 in record.lstComponentWrapper) {
                        assemblyRecord = record.lstComponentWrapper[key2];
                        if(assemblyRecord.assembly != "APU Values" && assemblyRecord.assembly != undefined){
                            if(assemblyRecord.flightHM != null || assemblyRecord.flightC != null) {
                                hasEmptyValues = false;
                            }
                        }
                    }
                    for (let key2 in record.lstComponentWrapper) {
                        assemblyRecord = record.lstComponentWrapper[key2];
                        if(assemblyRecord.assembly != "APU Values" && assemblyRecord.assembly != undefined){
                            if(assemblyRecord.flightHM === '' || assemblyRecord.flightC === undefined){
                                hasEmptyValues = true;
                            }
                        }
                    }
                }
                else{
                    for (let key2 in record.lstComponentWrapper) {
                        assemblyRecord = record.lstComponentWrapper[key2];
                        if(assemblyRecord.assembly != "APU Values" && assemblyRecord.assembly != undefined){
                            if(assemblyRecord.flightHM != null || assemblyRecord.flightC != null) {
                                hasEmptyValues = false;
                            }
                        }
                    }
                    for (let key2 in record.lstComponentWrapper) {
                        assemblyRecord = record.lstComponentWrapper[key2];
                        if(assemblyRecord.assembly != "APU Values" && assemblyRecord.assembly != undefined){
                            if(assemblyRecord.flightHM === '' || assemblyRecord.flightC === undefined){
                                hasEmptyValues = true;
                            }
                        }
                    }
                }
                if(record.totalFH == undefined || record.totalFH == ''){
                    record.totalFH = null;
                }
                if(record.totalFC == undefined || record.totalFC == ''){
                    record.totalFC = null;
                }
                for(let key2 in operatorData[key].assetData[key1].lstComponentWrapper){
                    assemblyRecord = record.lstComponentWrapper[key2];
                    if(assemblyRecord.assembly == "APU Values"){
                        if(assemblyRecord.apuFC == "" || assemblyRecord.apuFC == null || assemblyRecord.apuFC == undefined){
                            assemblyRecord.apuFC = 0; 
                        }
                    }
                    let flightHM = operatorData[key].assetData[key1].lstComponentWrapper[key2].flightHM;
                    if(flightHM != undefined){
                        if(flightHM.toString().includes(':')){
                            let fHInDecimal = parseInt(flightHM.split(':')[0]) + (parseInt(flightHM.split(':')[1])/60);
                            operatorData[key].assetData[key1].lstComponentWrapper[key2].flightHM = flightHM;
                            operatorData[key].assetData[key1].lstComponentWrapper[key2].flightH = fHInDecimal;
                        } 
                    }
                }
            }
        }
        if(hasEmptyValues){
            helper.handleErrors('Please enter FH/FC values for at least one of the Assemblies for each asset');
        }
        else{
            for(let key in operatorData){
                for(let key1 in operatorData[key].assetData){
                    status = operatorData[key].assetData[key1].utilization.Status__c
                    if(component.get("v.searchFilters.Status") == 'Create New'){
                        if(status == 'New'){
                            recordsToSave.push(operatorData[key].assetData[key1]);
                        }    
                    }
                    else{
                        if(status == 'Open'){
                            recordsToSave.push(operatorData[key].assetData[key1]);
                        }
                    }
                }
            } 
            
            console.log(recordsToSave);
            if(recordsToSave != '' && recordsToSave != null && recordsToSave != undefined){
                var assetName;
                helper.showSpinner(component, event, helper);
                var index = 0;
                var errorIndex = 0;
                for(var i=0; i<recordsToSave.length;i++){
                    let monthEnding = recordsToSave[i].utilization.Month_Ending__c;
                    if(monthEnding != undefined && monthEnding != '' && monthEnding != null){
                        recordsToSave[i].utilization.Month_Ending__c = helper.convertDateToString(new Date(monthEnding)); 
                        //console.log('Month Ending: ',recordsToSave[i].utilization.Month_Ending__c);
                    }
                    var action = component.get("c.saveFHdata");
                    action.setParams({
                        recordsToSave :JSON.stringify(recordsToSave[i])
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if(state == "SUCCESS"){
                            errorIndex++;
                            console.log("Success");
                            var result = response.getReturnValue();
                            console.log('result:',result);
                            if(result != undefined && result != '' && result != null){
                                index++;
                            } 
                            if(component.get("v.searchFilters.Status") == 'Create New'){
                                if(index == recordsToSave.length){
                                    helper.hideSpinner(component, event, helper);
                                    component.set("v.assetCount",0);
                                    helper.searchDataonLoad(component,event,helper,',','','redirect');
                                }   
                            }
                            else{
                                if(index == recordsToSave.length){
                                    helper.hideSpinner(component, event, helper);
                                }
                            }
                            if(errorIndex == recordsToSave.length){
                                if(errorList.length > 0){
                                    console.log(errorList);
                                    component.set("v.showErrorModal", true);
                                    component.set("v.errorList",errorList);
                                }
                            }
                        }
                        else if (state === "ERROR") {
                            errorIndex++;
                            assetName = recordsToSave[parseInt(errorIndex) - parseInt(1)].Asset.Name;
                            helper.hideSpinner(component, event, helper);
                            var errormsg = response.getError();
                            if (typeof errormsg === 'object') {
                                errormsg.forEach(function (error) {
                                    //top-level error. There can be only one
                                    if (error.message) {
                                        error.message = helper.parseErrorMessage(error.message);
                                        if(assetName != undefined && assetName != '' && assetName != null){
                                            error.message = assetName+' error : '+error.message;
                                        }
                                        errorList.push(error.message);
                                    }
                                    //page-level errors (validation rules, etc)
                                    if (error.pageErrors) {
                                        let arrErr = [];
                                        error.pageErrors.forEach(function (pageError) {
                                            if(assetName != undefined && assetName != '' && assetName != null){
                                                pageError.message = assetName+' error : '+pageError.message;
                                            }
                                            errorList.push(pageError.message);
                                        });
                                    }
                                    if (error.fieldErrors) {
                                        //field specific errors--we'll say what the field is
                                        for (var fieldName in error.fieldErrors) {
                                            //each field could have multiple errors
                                            error.fieldErrors[fieldName].forEach(function (errorsList) {
                                                if(assetName != undefined && assetName != '' && assetName != null){
                                                    errorsList.message = assetName+' error : '+errorsList.message;
                                                }
                                                errorList.push(pageError.message);                     
                                            });
                                        };
                                    }
                                });
                            }
                            if(errorIndex == recordsToSave.length){
                                if(errorList.length > 0){
                                    console.log(errorList);
                                    component.set("v.showErrorModal", true);
                                    component.set("v.errorList",errorList);
                                }
                            } 
                            //helper.handleErrors(errors,'Error',assetName);
                        }
                    })
                    $A.enqueueAction(action); 
                }
            }
        }
    },
    closeErrorModal: function(component, event, helper){
        component.set("v.showErrorModal", false);
    },
    handleSend : function(component, event, helper){
        var recordsToSave=[];
        var searchData = component.get("v.searchData");
        var operatorData = searchData.operatorData;
        var status;
        var utilizationId;
        var recordExists= true;
        var hasAccess = true;
        for(let key in operatorData){
            for(let key1 in operatorData[key].assetData){
                utilizationId = operatorData[key].assetData[key1].utilization.Id;
                if(utilizationId == undefined){
                    recordExists = false;
                }
                status = operatorData[key].assetData[key1].utilization.Status__c;
                if(!operatorData[key].assetData[key1].userExistsinGroup){
                    hasAccess = false;
                }
                if( status == 'Open' && utilizationId != undefined){
                    recordsToSave.push(operatorData[key].assetData[key1].utilization);
                }
            }
        }
        if(!recordExists){
            helper.handleErrors('Please create all utilization records, then you can send them for approval');
        }
        else{
            if(!hasAccess){
                helper.handleErrors('You do not have access to approve the records. Please contact your system admin.');
            }
            else{
                console.log('Records: ',recordsToSave);
                if(recordsToSave != '' && recordsToSave != null && recordsToSave != undefined){
                    helper.saveRecords(component, event, helper,recordsToSave);
                }
            }
        }
    },
    
    handleUtilizationApproval : function(component, event, helper){
        var recordsToSave=[];
        var searchData = component.get("v.searchData");
        var operatorData = searchData.operatorData;
        var status;
        var hasAccess= true;
        for(let key in operatorData){
            for(let key1 in operatorData[key].assetData){
                status = operatorData[key].assetData[key1].utilization.Status__c
                if(!operatorData[key].assetData[key1].userExistsinQueue){
                    hasAccess = false;
                }
                if(status == 'Submitted To Lessor'){
                    recordsToSave.push(operatorData[key].assetData[key1].utilization);
                }
            }
        }
        console.log(recordsToSave);
        if(!hasAccess){
            helper.handleErrors('You do not have permission to approve all Records. Please contact your system administrator.');
        }
        else{
            if(recordsToSave != '' && recordsToSave != null && recordsToSave != undefined){
                helper.saveRecords(component, event, helper,recordsToSave);
            }
        }
    },
    handleInvoices: function(component, event, helper){
        helper.showSpinner(component, event, helper);
        var recordsToSave=[];
        var searchData = component.get("v.searchData");
        var operatorData = searchData.operatorData;
        var status;
        var invRecord;
        for(let key in operatorData){
            for(let key1 in operatorData[key].assetData){
                status = operatorData[key].assetData[key1].utilization.Status__c
                if( status == 'Approved By Lessor'){
                    if(typeof operatorData[key].assetData[key1].invoice == 'object'){
                        invRecord = operatorData[key].assetData[key1].invoice;
                        if(typeof invRecord.Invoice_Line_Items__r == 'object')
                        	invRecord.Invoice_Line_Items__r = null;
                        recordsToSave.push(invRecord);    
                    } 
                }
            }
        }
        console.log('Records: ',recordsToSave);
        
        if(recordsToSave != '' && recordsToSave != null && recordsToSave != undefined){
            for(var i=0; i<recordsToSave.length;i++){
                var action = component.get("c.sendInvoices");
                action.setParams({
                    utilizationRecord :JSON.stringify(recordsToSave[i]),
                    Status : '',
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state == "SUCCESS"){
                        helper.hideSpinner(component, event, helper);
                        var result = response.getReturnValue();
                        if(result !='' && result !=undefined && result !=null){
                            helper.handleErrors(result);
                        }
                        else{
                            helper.handleErrors('The process to send Invoice has been initiated, Mails will be delivered Shortly.','Success');
                        }
                    }
                    else if (state === "ERROR") {
                        helper.hideSpinner(component, event, helper);
                        var errors = response.getError();
                        helper.handleErrors(errors);
                    }
                })
                $A.enqueueAction(action); 
            }
        }
        else{
            helper.hideSpinner(component, event, helper);
        }
    },
    reloadInOpenMode : function(component, event, helper){
        if(component.get("v.loadInOpenMode")){
            component.set("v.searchFilters.DateOption","All");
            component.set("v.searchFilters.Status","Open");
            component.set("v.isCreateNew", false);
            helper.setSearchFilters(component, event, helper);
        } 
    },
    onDateOptionChange : function(component, event, helper){
        var dateValue = component.get("v.searchFilters.DateOption");
        console.log("dateValue: ",dateValue);
        if(dateValue == "All" || dateValue == "Previous Month"){
            component.set("v.isDateDisabled", true);
        }
        else{
            if(dateValue == 'On Date'){
                component.set("v.searchFilters.Status",'Approved By Lessor');
            }
            component.set("v.isDateDisabled", false);
        }
    },
    
    clear: function(component, event, helper){
        helper.clearFilters(component, event, helper);
    },
    
    showLastMonthUtilizations : function(component, event, helper){
        component.set("v.assetCount",0);
        component.set("v.pinnedIds", []);
        component.set("v.ignoreIdsList",[]);
        var searchFilters = component.get("v.searchFilters");
        component.set("v.assetRecord", null);
        component.set("v.operatorRecord", null);
        searchFilters.DateOption = 'Previous Month';
        searchFilters.Status = 'Create New';
        searchFilters.InputDate = '';
        searchFilters.AssetType = 'All'; 
        component.set("v.searchFilters",searchFilters);
        helper.searchDataonLoad(component, event, helper);
        
    },
    showPendingInvoices : function(component, event, helper){
        component.set("v.assetCount",0);
        var searchFilters = component.get("v.searchFilters");
        searchFilters.Status = 'Submitted To Lessor';
        searchFilters.DateOption = 'All';
        searchFilters.InputDate = '';
        searchFilters.AssetType = 'All';
        component.set("v.ignoreIdsList",[]);
        component.set("v.previousList",[]);
        component.set("v.searchFilters",searchFilters);
        helper.searchDataonLoad(component, event, helper);
    },
    searchPrevious : function(component, event, helper){
        var searchData= component.get("v.searchData");
        var showNext = false;
        for(let key in searchData.operatorData){
            for(let key1 in searchData.operatorData[key].assetData){
                if(searchData.operatorData[key].assetData[key1].ischanged){
                    showNext = true;
                }
            } 
        }
        if(showNext){
            component.set("v.showSaveModal", true);
        }
        else{
            helper.searchPreviousRecords(component, event, helper);    
        }
        component.set("v.previousSave", true);	
    }, 
    closeModel: function(component, event, helper){
        component.set("v.showSaveModal", false); 
        component.set("v.showSearchModal",false);
    },
    saveAndNext : function(component, event, helper){
        helper.showSpinner(component, event, helper);
        component.set("v.showSaveModal", false);
        var recordsToSave=[];
        var searchData = component.get("v.searchData");
        for(let key in searchData.operatorData){
            for(let key1 in searchData.operatorData[key].assetData){
                if(searchData.operatorData[key].assetData[key1].ischanged){
                    recordsToSave.push(searchData.operatorData[key].assetData[key1]);
                }
            }   
        }
        
        if(recordsToSave != '' && recordsToSave != null && recordsToSave != undefined){
            var index;
            for(var i=0; i<recordsToSave.length;i++){
                index= i;
                var action = component.get("c.saveFHdata");
                action.setParams({
                    recordsToSave :JSON.stringify(recordsToSave[i])
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state == "SUCCESS"){
                        helper.hideSpinner(component, event, helper);
                        var result = response.getReturnValue();
                        console.log('result:',result);
                        if(result != undefined && result != '' && result != null){
                            for(let key in searchData.operatorData){
                                for(let key1 in searchData.operatorData[key].assetData){
                                    if(searchData.operatorData[key].assetData[key1].Asset.Id == result.Asset.Id){
                                        searchData.operatorData[key].assetData[key1].ischanged = false;
                                    }
                                } 
                            }
                            recordsToSave.splice(index,1);
                        }
                        if(recordsToSave.length == 0){
                            if(component.get("v.previousSave")){
                                helper.searchPreviousRecords(component, event, helper);    
                            }
                            else{
                                helper.searchNextRecords(component, event, helper);
                            }
                        }
                    }
                    else if (state === "ERROR") {
                        helper.hideSpinner(component, event, helper);
                        var errors = response.getError();
                        helper.handleErrors(errors);
                    }
                })
                $A.enqueueAction(action); 
            }   
        }
        else{
            helper.hideSpinner(component, event, helper);
            if(component.get("v.previousSave")){
                helper.searchPreviousRecords(component, event, helper);    
            }
            else{
                helper.searchNextRecords(component, event, helper);
            }
        }
    },
    goToNext :  function(component, event, helper){
        component.set("v.showSaveModal", false);
        if(component.get("v.previousSave")){
            helper.searchPreviousRecords(component, event, helper);    
        }
        else{
         	helper.searchNextRecords(component, event, helper);   
        }
    },
    searchNext: function(component, event, helper){
        var searchData= component.get("v.searchData");
        var showNext = false;
        for(let key in searchData.operatorData){
            for(let key1 in searchData.operatorData[key].assetData){
                if(searchData.operatorData[key].assetData[key1].ischanged){
                    showNext = true;
                }
            } 
        }
        component.set("v.previousSave", false);
        component.set("v.nextSave", true);
        if(showNext){
            component.set("v.showSaveModal", true);
        }
        else{
            helper.searchNextRecords(component, event, helper);
        }
    }, 
    loadTrueUpData: function(component, event, helper){ 
        console.log('Parent Function Called');
        if(component.get("v.loadTrueUpData")){
            var searchFilters = component.get("v.searchFilters");
            searchFilters.DateOption = 'All';
            searchFilters.InputDate = '';
            searchFilters.AssetType = 'All';
            searchFilters.Status = 'All';
            component.set("v.searchFilters", searchFilters);
            component.set("v.operatorRecord", null);
            component.set("v.changeButtonColor", false);
            helper.searchDataonLoad(component, event, helper,'Utilization',component.get("v.trueUpUtilizationId"),false,"addTrueUpRow");
        }
    },
    loadSearchDataOnDelete : function(component, event, helper){
        console.log('Parent Load SearchData OnDelete');
        let checkRecordsOnCurrentPage = false;
        let loadinNewStatus = false;
        console.log('Values: ',component.get("v.loadDataOnDelete"));
        if(component.get("v.assetCount") % 10 == 1 && component.get("v.assetCount") > 10){
            checkRecordsOnCurrentPage = true;
        }
        else{
            if(component.get("v.assetRecord") != undefined && component.get("v.assetRecord") != null){
                loadinNewStatus = true;
            }
        }
        if(component.get("v.loadDataOnDelete")){
            helper.searchDataonLoad(component, event, helper, '','',false,false,checkRecordsOnCurrentPage,loadinNewStatus);
        }
    }
})