({
    
    doInit : function(component, event, helper) {

        var prospectWrapperList = component.get("v.prospectList");
        var prospectId = component.get("v.prospectId");
        var projectStatusOptionsMap = component.get("v.projectStatusOptionsMap");
        
        if(prospectWrapperList != null && prospectWrapperList !== undefined){
            
            for(var i=0; i < prospectWrapperList.length; i++){
                
                var assetTermProspectId = prospectWrapperList[i].assetTerm.Marketing_Activity__r.Prospect_Id__c ;
                
                if(assetTermProspectId == prospectId){
                    //getting picklist option's label instead of API name.
                    if(projectStatusOptionsMap != undefined && prospectWrapperList[i].assetTerm.Project_Status__c != undefined &&
                       projectStatusOptionsMap.has(prospectWrapperList[i].assetTerm.Project_Status__c)){
                        prospectWrapperList[i].assetTerm.Project_Status__c = projectStatusOptionsMap.get(prospectWrapperList[i].assetTerm.Project_Status__c);
                    }
                    
                    component.set("v.assetTerm",prospectWrapperList[i]);
                    break;
                }
            }
        }
        
    },
    
    editSale: function(component, event, helper) {        
        
       if(component.get("v.assetTerm") != undefined && component.get("v.assetTerm").assetTerm != undefined && component.get("v.assetTerm").assetTerm.Id != undefined){
             
           if(!component.get("v.isActive")){
               
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": 'Access Denied',
                    "type" : 'warning',
                    "message": 'This Campaign is not active. Please change the status of the Campaign to edit any part of it.'
                });
                toastEvent.fire();
               
           }else{
           
               //fire event to open modal to edit asset in deal. 
               var cmpEvent = component.getEvent("openModalToEditAD");
               
               var info = new Object();
               info.assetDetailId = component.get("v.assetTerm").assetTerm.Id;
               info.prospect = component.get("v.assetTerm").assetTerm.Prospect__c;
               info.msn = component.get("v.assetTerm").assetTerm.Aircraft__r.MSN_Number__c;
               info.prospectId = component.get("v.assetTerm").assetTerm.Marketing_Activity__r.Prospect_Id__c;
               info.recordTypeId = component.get("v.assetTerm").assetTerm.RecordTypeId;
              // console.log(component.get("v.assetTerm").assetTerm.Marketing_Activity__r.Prospect_Id__c);
               
               cmpEvent.setParams({"assetDetailInfo" : info });
               cmpEvent.fire();
           }
           
           helper.hideIcon(component);
           
        }
    },

    showPencilIcon : function(component, event, helper) {
        var cmpTarget = component.find('iconSpan');
        $A.util.removeClass(cmpTarget, 'slds-hide');
    },
    
    hidePencilIcon : function(component, event, helper) {
    	helper.hideIcon(component);
    }
    
})