({
	onPageReferenceChange : function(component, event, helper) {
		helper.onPageReferenceChange(component, event);
	},
    
    cancel : function(component, event, helper) {
		helper.cancel(component, event);
	},
    
    save : function(component, event, helper) {
        if(helper.check(component)) 
        	helper.save(component, event);
        else
            helper.showToast('Error','Please fill all fields!','ERROR');
	},
    proceed: function(component, event, helper){
        component.set("v.showWarningModal", false);
      	helper.proceedFurther(component, event);  
    },
    closeWarningModal: function(component, event, helper){
        helper.hideSpinner(component);
      component.set("v.showWarningModal", false);  
    },
    onDateChange: function(component, event, helper) {
        if(component.get("v.prevAssetTSN") == component.get("v.data.assetTSN") && 
          component.get("v.prevAssetCSN") == component.get("v.data.assetCSN") &&
          component.get("v.prevAssemblyTSN") == component.get("v.data.assemblyTSN") &&
          component.get("v.prevAssemblyCSN") == component.get("v.data.assemblyCSN"))
        	helper.onDateChange(component, event);
    }
})