({
    init: function(component, event, helper) {
        
        component.set("v.showSpinner", true);
        window.setTimeout(
            $A.getCallback(function() {
                helper.loadData(component, event, helper);
            }), 200
        );
    },
 
    goBack: function(cmp, event, helper) {
        cmp.set("v.type", undefined);
        //unset Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title" : ""});
        appEvent.fire();
    },
	
    /*contactUs: function(cmp, event, helper) {
        
        //Create component dynamically        
        $A.createComponent(
            "c:Com_Cmty_Contact_Us",{},
            function(msgBox){                
                if (cmp.isValid()) {
                    var targetCmp = cmp.find('contact-us');
                    var body = targetCmp.get("v.body");
                    body.push(msgBox);
                    targetCmp.set("v.body", body); 
                }
            }
        );
    }*/
    
});