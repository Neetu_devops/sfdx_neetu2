({
    loadData: function (component, event, helper) {
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title": "Billing Home" });
        appEvent.fire();

        helper.setColumns(component);

        helper.showSpinner(component);
        var action = component.get("c.getBillingData");

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.hideSpinner(component);
                var data = response.getReturnValue();
                component.set("v.data", data);

                if (data.billsOverview != undefined) {
                    component.set("v.totalOutstanding", data.billsOverview[data.billsOverview.length - 1].totalOutstanding);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var cmpEvent = component.getEvent("ErrorsEvent");
                    cmpEvent.setParams({ "errors": errors });
                    cmpEvent.fire();
                } else {
                    console.log("Unknown error");
                }
                helper.hideSpinner(component);
            }
            component.set("v.isDataLoaded", true);
        });

        $A.enqueueAction(action);
    },
    setColumns: function (cmp) {

        var outstandingColumns = [
            
            {
                	label: 'View Invoice',
                    type: 'button-icon',
                    typeAttributes: {
                        iconName: 'utility:preview',
                        name: 'view_invoice', 
                        title: 'Work in progress',
                        pointerEvents: 'none',
                        variant: 'border-filled',
                        disabled: false,
                        alternativeText: 'View Invoice',
                        class: 'scaled-down'
                    }
                },

            { label: 'Invoice', fieldName: 'Name', sortable: true },
            { label: 'Type', fieldName: 'Invoice_Type__c', sortable: true },
            { label: 'MSN/ESN', fieldName: 'MSN_ESN__c', sortable: true },
            { label: 'Date', fieldName: 'Issue_Date__c', type: 'date', sortable: true },
            { label: 'Period From', fieldName: 'Period_From__c', type : 'date', sortable: true },
            { label: 'Period To', fieldName: 'Period_To__c', type : 'date', sortable: true },
            { label: 'Invoice Amount', fieldName: 'Invoice_Amount__c', type: 'currency', typeAttributes: { currencyCode: 'USD'}, sortable: true },
            { label: 'Paid Amount', fieldName: 'Paid_Amount__c', type: 'currency', typeAttributes: { currencyCode: 'USD'}, sortable: true }
        ];
        
        var paidColumns = outstandingColumns.slice();
        
        outstandingColumns.push({ label: 'Outstanding Amount', fieldName: 'Outstanding_Amount__c', type: 'currency', typeAttributes: { currencyCode: 'USD'}, sortable: true });
        paidColumns.push({ label: 'Closed Date', fieldName: 'Paid_Date__c',  type : 'date', sortable: true });
        
        cmp.set('v.outstandingColumns', outstandingColumns);
        
		// to rename Date column as Posted Date.
        paidColumns[4] = { label: 'Posted Date', fieldName: 'Issue_Date__c', type: 'date', sortable: true };
        
        //to remove paid amount column.
        paidColumns.splice(8, 1);
        cmp.set('v.paidColumns', paidColumns);
        
        var overviewColumns = [
            { label: 'MSN/ESN', fieldName: 'url', type: 'url',  typeAttributes: {target: '_blank', label: { fieldName: 'msn' }}}, 
            { label: 'Deposit', fieldName: 'deposit',  type: 'currency', typeAttributes: { currencyCode: 'USD'}},
            { label: 'Deposit Type', fieldName: 'depositType'},
            { label: 'Total Outstanding', fieldName: 'totalOutstanding', type: 'currency', typeAttributes: { currencyCode: 'USD'}},
            { label: '1-15 Days', fieldName: 'firstPeriod', type: 'currency', typeAttributes: { currencyCode: 'USD'}},
            { label: '16-30 Days', fieldName: 'secondPeriod', type: 'currency', typeAttributes: { currencyCode: 'USD'}},
            { label: '31-60 Days', fieldName: 'thirdPeriod', type: 'currency', typeAttributes: { currencyCode: 'USD'}},
            { label: '61-90 Days', fieldName: 'forthPeriod', type: 'currency', typeAttributes: { currencyCode: 'USD'}},
            { label: '90+ Days', fieldName: 'fifthPeriod', type: 'currency', typeAttributes: { currencyCode: 'USD'}}
       	];
        
        cmp.set('v.overviewColumns', overviewColumns);
    },
    showSpinner: function (component) {
        var spinner = component.find("spinner");
        console.log('spinner' + spinner);
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (component) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },

})