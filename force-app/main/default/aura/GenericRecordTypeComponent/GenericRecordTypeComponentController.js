({
	closeComponent : function(component, event, helper) {
		component.set("v.recordTypeSection", false);
	},
    
    
    chooseRecordType : function(component, event, helper) {
		component.set("v.recordTypeSection", false);
        component.set("v.createNewRecord", true);
	},
})