({   
    init: function(component, event, helper) {
		let panel = document.getElementsByClassName("cCenterPanel");
        console.log('Panel: ',panel); 
        //panel.classList.remove("slds-m-top--x-large");
        var action = component.get("c.getCommunityPrefix");
        var link = '/s/available-assets';
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var prefix =  response.getReturnValue();
                component.set("v.prefix", prefix);
                if(prefix != undefined && prefix != ''){
                  link = prefix + link;  
                }
            }
            
            component.set("v.availableAssetsURL", link);
        });
        
        $A.enqueueAction(action); 
    },
    
	setTitle : function(component, event, helper) {
        var title = event.getParam("title");
		component.set("v.title", title);
        console.log('Event Captured by Header');
	},
    
    logOut: function(component, event, helper){
        let prefix = component.get("v.prefix");
        console.log(prefix);
        let redirectURL = '';
        if(prefix != undefined && prefix != '' && prefix != null){
            redirectURL = prefix+"/secur/logout.jsp?retUrl="+prefix+"/login";
        }
        else{
            redirectURL = "/secur/logout.jsp?retUrl=%2Flogin";
        }   
        console.log(redirectURL);
    	window.location.replace(redirectURL);
	},
    
    showMenu: function(component, event, helper){
    	var navList = document.getElementById("nav-lists");
    	navList.classList.add("_Menus-show");
    },
    
	hideMenu : function(component, event, helper){
    	helper.hide();
    },
    
    contactUs: function(cmp, event, helper) {
        
        helper.hide();
        
        //Create component dynamically        
        $A.createComponent(
            "c:Com_Cmty_Contact_Us",{},
            function(msgBox){                
                if (cmp.isValid()) {
                    var targetCmp = cmp.find('contact-us');
                    var body = targetCmp.get("v.body");
                    body.push(msgBox);
                    targetCmp.set("v.body", body); 
                }
            }
        );
    }
    
})