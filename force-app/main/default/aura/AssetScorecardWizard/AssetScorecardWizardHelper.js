({
  //get asset risk data if it is saved already
  getAssetAsssessmentData: function (component) {
    component.set("v.isLoading", true);
    var recordIdL = component.get("v.recordId");
    console.log("getAssetAsssessmentData recordId: " + recordIdL);
    var self = this;
    if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
      self.showErrorMsg(component, "Invalid record Id");
      return;
    }
    var action = component.get("c.getAssetAssessmentDetails");
    action.setParams({
      recordId: recordIdL
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      console.log("getAssetAsssessmentData : state " + state);
      component.set("v.isLoading", false);
      if (state === "SUCCESS") {
        var allValues = response.getReturnValue();
        console.log("getAssetAsssessmentData values " + allValues);
        if (allValues != null) {
          console.log("getAssetAsssessmentData values " + JSON.stringify(allValues));
          
          component.set("v.assetAssessmentData", allValues);
          if(allValues.scoreWrapper != null) {
            component.set("v.assetTypeScore", allValues.scoreWrapper.assetTypeScore);
            component.set("v.leaseTermRemainingScore",allValues.scoreWrapper.leaseTermRemainingScore);
            component.set("v.assetSpecScore", allValues.scoreWrapper.assetSpecScore);
            component.set("v.MREOLAScore", allValues.scoreWrapper.MREOLAScore);
          }

          if (allValues.leaseTermMap != null) {
            var optionsList = [];
            for (var key in allValues.leaseTermMap) {
              optionsList.push({
                value: allValues.leaseTermMap[key],
                key: key
              });
            }
            component.set("v.leaseTerm", optionsList);
          }
          if (allValues.thrustWeightMap != null) {
            var thrustWeightList = [];
            var assetSpecScoreOption = [];
            var thrustValues = [];
            for (var key in allValues.thrustWeightMap) {
              thrustWeightList.push({
                value: allValues.thrustWeightMap[key],
                key: key
              });
              thrustValues.push(allValues.thrustWeightMap[key]);
            }
            if(thrustValues != null) {
                var uniquethrustValues = Array.from(new Set(thrustValues)).sort();
                assetSpecScoreOption.push({ class: "optionClass", label: '-', value: '0'});
                for(var i = 0 ; i < uniquethrustValues.length; i++) {
                    assetSpecScoreOption.push({ class: "optionClass", label: uniquethrustValues[i], value: uniquethrustValues[i]});
                }
            }
            component.set("v.assetSpecScoreOptValues", assetSpecScoreOption);
            component.set("v.thrustWeight", thrustWeightList);
            if (allValues.thrustKeySet != null) {
              component.set("v.thrustSize", allValues.thrustKeySet.length);
            }
            if (allValues.weightKeySet != null) {
              component.set("v.weightSize", allValues.weightKeySet.length);
            }
          }
        } else {
          self.showErrorMsg(component, "Asset data not found!");
        }
      } else if (state === "ERROR") {
        var errors = response.getError();
        console.log("getAssetAsssessmentData errors " + errors);
        if (errors) {
          self.handleErrors(errors);
        } else {
          console.log("getAssetAsssessmentData unknown error");
          self.showErrorMsg(component, "Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  //Update the score factor and calculate the overall score based on the input
  updateAssetAsssessmentData: function (component) {
    component.set("v.isLoading", true);
    var assetAssessmentData = component.get("v.assetAssessmentData");
    console.log( "updateAssetAsssessmentData assetAssessmentData: " + assetAssessmentData);
    var self = this;
    if(assetAssessmentData == null || assetAssessmentData.scoreWrapper == null) {
        return;
    }
    var updateData = assetAssessmentData.scoreWrapper;
    updateData.assetTypeScore = component.get("v.assetTypeScore");
    updateData.leaseTermRemainingScore = component.get("v.leaseTermRemainingScore");
    updateData.assetSpecScore = component.get("v.assetSpecScore");
    updateData.MREOLAScore = component.get("v.MREOLAScore");
    updateData.assetId = component.get("v.recordId");

    var params = JSON.stringify(updateData);
    console.log('updateAssetAsssessmentData params: '+params);
    var action = component.get("c.updateAsssessmentDetails");
    action.setParams({
        "assessmentDataString": params
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      console.log("updateAssetAsssessmentData : state " + state);
      component.set("v.isLoading", false);
      if (state === "SUCCESS") {
        var allValues = response.getReturnValue();
        console.log("updateAssetAsssessmentData values " + allValues);
        self.showSucessMsg(component, 'Saved successfully');
        //self.getAssetAsssessmentData(component);
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire(); 
      } else if (state === "ERROR") {
        var errors = response.getError();
        console.log("updateAssetAsssessmentData errors " + errors);
        if (errors) {
          self.handleErrors(errors);
        } else {
          console.log("updateAssetAsssessmentData unknown error");
          self.showErrorMsg(component, "Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  //setting all picklist values
  setPicklistOption: function (component) {
    var currentStep = component.get("v.currentStep");
    if (currentStep == 1) {
      var opts = [
        { class: "optionClass", label: "-", value: "0" },
        { class: "optionClass", label: "1", value: "1" },
        { class: "optionClass", label: "2", value: "2" },
        { class: "optionClass", label: "3", value: "3" },
        { class: "optionClass", label: "4", value: "4" },
        { class: "optionClass", label: "5", value: "5" },
        { class: "optionClass", label: "6", value: "6" }
      ];
      component.find("assetTypeScoreOption").set("v.options", opts);
    }
    else if (currentStep == 2) {
        var leaseTerm = component.get("v.leaseTerm");
        var leaseTermMonthOpt = [];
        if(leaseTerm != null) {
          leaseTermMonthOpt.push({ class: "optionClass", label: '-', value: '0'});
            for(var i = 1; i <= leaseTerm.length; i++) {
                leaseTermMonthOpt.push({ class: "optionClass", label: i, value: i});
            }
        }
        component.find("leaseTermMonthOption").set("v.options", leaseTermMonthOpt);
    }
    else if (currentStep == 3) {
        var assetSpecScoreOptValues = component.get('v.assetSpecScoreOptValues');
        component.find("assetSpecScoreOption").set("v.options", assetSpecScoreOptValues);
    }
    else if (currentStep == 4) {
        var MREOLAScoreOption = [
            { class: "optionClass", label: "-", value: "0" },
            { class: "optionClass", label: "1", value: "1"},
            { class: "optionClass", label: "2", value: "2"},
            { class: "optionClass", label: "3", value: "3"},
            { class: "optionClass", label: "4", value: "4"},
            { class: "optionClass", label: "5", value: "5"},
            { class: "optionClass", label: "6", value: "6" }
        ];
        component.find("MREOLAScoreOption").set("v.options", MREOLAScoreOption);
    }
  },

  showSucessMsg : function(component, msg) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        title: "",
        message: msg,
        duration:'5000',
        key: 'info_alt',
        type: 'success',
        mode: 'dismissible'
    });
    toastEvent.fire();
  },

  showErrorMsg: function (component, msg) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title: "Error!",
      message: msg,
      duration: "5000",
      key: "info_alt",
      type: "error",
      mode: "dismissible"
    });
    toastEvent.fire();
  },

  //to handle errors
  handleErrors: function (errors) {
    // Configure error toast
    let toastParams = {
      title: "Error",
      message: "Unknown error",
      type: "error"
    };
    if (errors) {
      errors.forEach(function (error) {
        //top-level error. There can be only one
        if (error.message) {
          toastParams.message = error.message;
          console.log("error " + toastParams.message);
          let toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams(toastParams);
          toastEvent.fire();
        }
        //page-level errors (validation rules, etc)
        if (error.pageErrors) {
          let arrErr = [];
          error.pageErrors.forEach(function (pageError) {
            toastParams.message = pageError.message;
            console.log("error " + toastParams.message);
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
          });
        }
        if (error.fieldErrors) {
          //field specific errors--we'll say what the field is
          for (var fieldName in error.fieldErrors) {
            //each field could have multiple errors
            error.fieldErrors[fieldName].forEach(function (errorList) {
              toastParams.message = errorList.message;
              console.log("error " + toastParams.message);
              let toastEvent = $A.get("e.force:showToast");
              toastEvent.setParams(toastParams);
              toastEvent.fire();
            });
          }
        }
      });
    }
  }
});