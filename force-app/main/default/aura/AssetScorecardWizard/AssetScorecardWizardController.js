({
	doInit : function(component, event, helper) {
        helper.setPicklistOption(component);
		helper.getAssetAsssessmentData(component);
	},
    
    onCancel : function(component, event, helper) {
        var allValues = component.get("v.assetAssessmentData");
        if(allValues != null && allValues.scoreWrapper != null) {
            component.set("v.assetTypeScore", allValues.scoreWrapper.assetTypeScore);
            component.set("v.leaseTermRemainingScore",allValues.scoreWrapper.leaseTermRemainingScore);
            component.set("v.assetSpecScore", allValues.scoreWrapper.assetSpecScore);
            component.set("v.MREOLAScore", allValues.scoreWrapper.MREOLAScore);
        }
        else{
            component.set("v.assetTypeScore", '');
            component.set("v.leaseTermRemainingScore", '');
            component.set("v.assetSpecScore", '');
            component.set("v.MREOLAScore", '');
        }
	},
    
    onPrevious : function(component, event, helper) {
		var currentStep = component.get("v.currentStep");
        if(currentStep != 1) {
            currentStep = currentStep - 1;
            component.set("v.currentStep", currentStep);
        }   
        helper.setPicklistOption(component);
	},
    
    onNext : function(component, event, helper) {
		var currentStep = component.get("v.currentStep");
        if(currentStep != 4) {
            currentStep = currentStep + 1;
            component.set("v.currentStep", currentStep);
        }
        helper.setPicklistOption(component);
	},
    
    onSave : function(component, event, helper) {
        helper.updateAssetAsssessmentData(component);
    },
    
    showRecord : function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log('showRecord recordId '+recordId);
        window.open('/' + recordId);  
    },
})