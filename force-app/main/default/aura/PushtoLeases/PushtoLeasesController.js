({
    doInit: function (component, event, helper) {
        helper.getParamValue('PushtoLeases', 'c__MPEId', component);
    },

    closeModel: function (component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
        helper.mpePagePreview(component);
    },

    submitData: function (component, event, helper) {
        // Set isModalOpen attribute to false
        //Add your code to call apex method or do some processing
        helper.pushtoLeases(component, event, helper);
        component.set("v.isModalOpen", false);
    },
})