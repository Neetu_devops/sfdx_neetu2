({
    getParamValue: function (tabName, paramName, component) {
        var url = window.location.href;
        var allParams = url.substr(url.indexOf(tabName) + tabName.length + 1).split('&amp;amp;amp;');
        var paramValue = '';

        if (allParams != null && allParams != undefined) {
            for (var i = 0; i < allParams.length; i++) {
                if (allParams[i].split('=')[0] == paramName)
                    paramValue = allParams[i].split('=')[1];
            }
            component.set('v.recordId', paramValue);
        }
        if (component.get("v.recordId") != undefined && component.get("v.recordId") != '') { component.set("v.isModalOpen", true); }
        else { this.handleMessage("Do not call this component from App Launcher."); }
    },

    mpePagePreview: function (component, event, helper) {
        var navService = component.find("navService");
        var pageRef = {
            type: 'standard__recordPage',
            attributes: {
                actionName: 'view',
                objectApiName: 'Maintenance_Program_Event__c',
                recordId: component.get("v.recordId")
            },
        };
        navService.navigate(pageRef, true);
        window.setTimeout($A.getCallback(function () { $A.get('e.force:refreshView').fire(); }), 10);
    },

    pushtoLeases: function (component, event, helper) {
        var action = component.get("c.PushToLeases");
        action.setParams({
            MPERecID: component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.handleMessage("All latest Event Requirements were successfully copied to all associated Leases/Supplemental Rents", "SUCCESS");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) { this.handleMessage(errors); }
                else { this.handleMessage("Error in creating record"); }
            }
            this.mpePagePreview(component);
        });
        $A.enqueueAction(action);
    },

    //Show the error toast when any error occured
    handleMessage: function (errors, type, duration) {
        //duration = "10000"; // time to display the Toast message. (default: 7000, min: 5000)
        _jsLibrary.handleErrors(errors, type, duration); // Calling the JS Library function to handle the error
    }
})