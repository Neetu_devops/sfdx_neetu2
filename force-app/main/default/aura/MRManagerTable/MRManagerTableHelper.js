({
    getLoadData: function (component, event, helper) {
        this.showSpinner(component);
        var recordId = component.get("v.recordId");
        console.log('recordId: ', recordId, 'aircraftId: ', component.get("v.aircraftId"));

        var action = component.get("c.getForecastData");
        action.setParams({
            "mrId": recordId,
            "aircraftId": component.get("v.aircraftId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('result: ', response.getReturnValue());
                var result = response.getReturnValue();
                component.set("v.isMRGenerated", true);
                component.set("v.forecast", result);
                component.set("v.cashFlowId", result.cashflowId);

                this.getCashflowStatus(component, event, helper, result);
            } else {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(component, errors);
                }
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    getCashflowStatus: function (component, event, helper, result) {
        this.showSpinner(component);

        var action = component.get("c.isForecastGenerated");
        action.setParams({
            "recordId": component.get("v.cashFlowId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('response: ', response.getReturnValue());

                let status = response.getReturnValue();
                if (status != undefined && status != '') {
                    component.set("v.isForecastGenerated", status);
                    if (component.get("v.isForecastGenerated") == 'Completed') {
                        var self = this;
                        setTimeout($A.getCallback(function () {
                            helper.applyStyleOnTable(component, event);
                            if (result.lstAssemblyWrapper.length > 0) {
                                for (var i = 0; i < result.lstAssemblyWrapper.length; i++) {
                                    var innerHeader = result.lstAssemblyWrapper[i].assemblyType;
                                    $("." + innerHeader).addClass("slds-hide");
                                    $("." + innerHeader + "_topHeader").attr('colspan', 0);
                                    $("." + innerHeader + "_forward").removeClass("slds-hide");
                                    $("." + innerHeader + "_backward").addClass("slds-hide");
                                }
                            }
                        }), 1000);
                        window.addEventListener('resize', $A.getCallback(function () {
                            if (component.isValid()) {
                                console.log('Resizing Window');
                                helper.applyStyleOnTable(component, event);
                            }
                        }));
                    }
                    else {
                        helper.getReloadStatus(component, event, helper);
                    }
                }
                else {
                    helper.getReloadStatus(component, event, helper);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    saveMRRecord: function (component, event, helper) {
        this.showSpinner(component);
        var cashFlowId = component.get("v.cashFlowId");
        if (cashFlowId != undefined) {
            var action = component.get("c.generateMRProfile");
            action.setParams({
                "forecastId": cashFlowId
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    console.log('message:', response.getReturnValue());
                    let message = response.getReturnValue();
                    if (message != undefined && message != '') {
                        this.handleErrors(message, 'Success');

                        component.set("v.isForecastGenerated", 'In Progress');
                        setTimeout($A.getCallback(() => this.getReloadStatus(component, event, helper)), 10000);
                    }
                } else {
                    var errors = response.getError();
                    if (errors) {
                        this.handleErrors(component, errors);
                    }
                }
                this.hideSpinner(component);
            });
            $A.enqueueAction(action);
        }
    },

    applyStyleOnTable: function (component, event) {
        $(".mTable").css("height", window.innerHeight - ($('#oneHeader').height() + 100));
        $('#tbltbody').css('height', $('.mTable').height() - 60);

        $(window).scroll(function () {
            var scrollL = $(window).scrollLeft();
            if (scrollL > 15) {
                scrollL = scrollL - 15;
            }
            $('.mr-manager-table-header').find('thead th:nth-child(1)').css("left", scrollL);
            $('.mr-manager-table-body').find('tbody td:nth-child(1)').css("left", scrollL);
        });
    },

    getReloadStatus: function (component, event, helper) {
        //console.log('cashFlowId: ', component.get("v.cashFlowId"));
        var action = component.get("c.isForecastGenerated");
        action.setParams({
            "recordId": component.get("v.cashFlowId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let status = response.getReturnValue();
                console.log('status: ', status);
                if (status != undefined && status != '') {
                    if (status == 'Completed') {
                        component.set("v.isForecastGenerated", status);
                        //window.location.reload();
                        this.getLoadData(component, event, helper);
                    }
                    else {
                        component.set("v.isForecastGenerated", status);
                        setTimeout($A.getCallback(() => this.getReloadStatus(component, event, helper)), 10000);
                    }
                }
                else {
                    setTimeout($A.getCallback(() => this.getReloadStatus(component, event, helper)), 10000);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },

    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    handleErrors: function (errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errors === 'object') {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
        else if (typeof errors === "string" && errors.length != 0) {
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})