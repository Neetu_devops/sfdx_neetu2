({
    scriptsLoaded: function (component, event, helper) {
        helper.getLoadData(component, event, helper);
    },

    handleClickShow: function (component, event, helper) {
        var arr = event.getSource().get("v.class").split(" ");
        var headerInd = arr[0];

        $("." + headerInd).removeClass("slds-hide");

        if ($("." + headerInd).hasClass("slds-hide")) {
            $("." + headerInd + "_topHeader").attr('colspan', 0);
        } else {
            $("." + headerInd + "_topHeader").attr('colspan', 7);
        }

        $("." + headerInd + "_forward").addClass("slds-hide");
        $("." + headerInd + "_backward").removeClass("slds-hide");
    },

    handleClickHide: function (component, event, helper) {
        var arr = event.getSource().get("v.class").split(" ");
        var headerInd = arr[0];

        $("." + headerInd).addClass("slds-hide");

        if ($("." + headerInd).hasClass("slds-hide")) {
            $("." + headerInd + "_topHeader").attr('colspan', 0);
        } else {
            $("." + headerInd + "_topHeader").attr('colspan', 5);
        }

        $("." + headerInd + "_forward").removeClass("slds-hide");
        $("." + headerInd + "_backward").addClass("slds-hide");
    },

    refreshTable: function (component, event, helper) {
        helper.saveMRRecord(component, event, helper);
    }
})