({
	onPageReferenceChange : function(component, event, helper) {
		helper.onPageReferenceChange(component, event);
	},
    
    assemblyChange : function(component, event, helper) {
        if(event.getParam("value").Id && component.get("v.prevPrevTSN") == component.get("v.data.prevTSN") && 
           component.get("v.prevPrevCSN") == component.get("v.data.prevCSN") &&
           component.get("v.prevNewTSN") == component.get("v.data.newTSN") &&
           component.get("v.prevNewCSN") == component.get("v.data.newCSN"))
            helper.assemblyChange(component, event);
        else if(component.get("v.prevPrevTSN") == component.get("v.data.prevTSN") && 
                component.get("v.prevPrevCSN") == component.get("v.data.prevCSN") &&
                component.get("v.prevNewTSN") == component.get("v.data.newTSN") &&
                component.get("v.prevNewCSN") == component.get("v.data.newCSN"))
            helper.clearFields(component, event);
	},
    
    cancel : function(component, event, helper) {
		helper.cancel(component, event);
	},
    
    save : function(component, event, helper) {
        if(helper.check(component)) 
        	helper.save(component, event);
        else
            helper.showToast('Error','Please fill all fields!','ERROR');
	},
    
     onDateChange: function(component, event, helper) {
        if(component.get("v.prevPrevTSN") == component.get("v.data.prevTSN") && 
          component.get("v.prevPrevCSN") == component.get("v.data.prevCSN") &&
          component.get("v.prevNewTSN") == component.get("v.data.newTSN") &&
          component.get("v.prevNewCSN") == component.get("v.data.newCSN"))
        	helper.onDateChange(component, event);
    }
})