({
	onPageReferenceChange : function(component, event) {
        
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        
        var myPageRef = component.get("v.pageReference");
        var recordId = myPageRef.state.c__recordId;
        component.set("v.recordId", recordId);
        
		var action = component.get('c.getLeaseData');
        action.setParams({
            'recordId' : component.get('v.recordId')
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.data", result);
                component.set("v.selectedDate",today);
                component.set("v.todaydate", today);
                component.find("approvalStatus").set("v.value",result.approvalStatus[0]);
            }
        });
        $A.enqueueAction(action); 	
	},
    
    assemblyChange : function(component, event) {
        
        var prevAssemblyId = component.get("v.prevAssembly.Id");
        var newAssemblyId = component.get("v.newAssembly.Id");
        
        if(!prevAssemblyId) 
            prevAssemblyId = null;
        else if(!newAssemblyId)
            newAssemblyId = null;
        
        var leaseData = component.get("v.data");
        var action = component.get("c.getTsnCsnValues");
        this.showSpinner(component);
        action.setParams({
            leaseData : JSON.stringify(leaseData),
            prevAssemblyId : prevAssemblyId,
            newAssemblyId : newAssemblyId,
            userSelectedDate : component.get("v.selectedDate")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
            	var result = response.getReturnValue();
                component.set("v.data", result);
                component.set("v.prevPrevTSN",result.prevTSN);
                component.set("v.prevPrevCSN",result.prevCSN);
                component.set("v.prevNewTSN",result.newTSN);
                component.set("v.prevNewCSN",result.newCSN);
                if(component.get("v.assemblyType") == '')
                	component.set("v.assemblyType", result.assemblyType);
            } 
            else if(state === 'ERROR') {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    clearFields : function(component, event) {
        var prevAssemblyId = component.get("v.prevAssembly.Id");
        var newAssemblyId = component.get("v.newAssembly.Id");
        if(prevAssemblyId == '') {
            component.set("v.prevAssembly",[]);
            component.set("v.data.prevTSN", null);
            component.set("v.data.prevCSN", null);
            component.set("v.prevPrevTSN", null);
            component.set("v.prevPrevCSN", null);
        }
        if(newAssemblyId == '') {
            component.set("v.newAssembly",[]);
            component.set("v.data.newTSN",null);
            component.set("v.data.newCSN",null);
            component.set("v.prevNewTSN", null);
            component.set("v.prevNewCSN", null);
        }
        if(!prevAssemblyId && !newAssemblyId) {
            component.set("v.assemblyType",'');
        }
    },
    
    //to show spinner while the data is loading
    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    //to hide spinner after the data is loaded
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    //to cancel the form
    cancel : function(component, event) {
        
        if(component.find("prevAssembly1") != undefined){
        	component.find("prevAssembly1").clearPill();    
        }
        
        if(component.find("prevAssembly2") != undefined){
        	component.find("prevAssembly2").clearPill();    
        }
        
        if(component.find("newAssembly1") != undefined){
        	  component.find("newAssembly1").clearPill();    
        }
        
        if(component.find("newAssembly2") != undefined){
        	  component.find("newAssembly2").clearPill();    
        }
        
        component.set("v.data",undefined);
        
        component.set("v.prevPrevTSN", null);
        component.set("v.prevPrevCSN", null);
        component.set("v.prevNewTSN", null);
        component.set("v.prevNewCSN", null);
        
        
        var recordId = component.get('v.recordId');
   
        if(recordId != null) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
    },
    
    //to save the record
    save : function(component, event) {
        component.set("v.data.selectedApprovalStatus",component.find("approvalStatus").get("v.value"));
        var leaseData = component.get('v.data');
        var action = component.get("c.saveData");
        this.showSpinner(component);
        action.setParams({
            leaseData : JSON.stringify(leaseData),
            prevAssemblyId : component.get("v.prevAssembly.Id"),
            newAssemblyId : component.get("v.newAssembly.Id"),
            prevAssemblyName : component.get("v.prevAssembly.Name"),
            newAssemblyName : component.get("v.newAssembly.Name"),
            userSelectedDate : component.get("v.selectedDate")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
            	this.showToast("Success", "Record created Successfully!","SUCCESS");
                var recordId = response.getReturnValue();
                if(recordId != null) {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId
                    });
                    navEvt.fire();
                    setTimeout(function(){
                        $A.get('e.force:refreshView').fire();
                    },100);
                }
            } 
            else if(state === 'ERROR') {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
       
    },
    //to show the toast message
    showToast : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
    check : function(component) {
        
        var prevTsn = component.get("v.data.prevTSN");
        var prevCsn = component.get("v.data.prevCSN");
        var newTsn = component.get("v.data.newTSN");
        var newCsn = component.get("v.data.newCSN");
        if(component.get("v.selectedDate") && 
           component.get("v.prevAssembly.Id") && component.get("v.newAssembly.Id") &&
          (prevTsn === 0 || prevTsn) && (prevCsn === 0 || prevCsn) &&
          (newTsn === 0 || newTsn) && (newCsn === 0 || newCsn))
            return true;
        else
            return false;
    },
    
    onDateChange : function(component, event) {
        var prevAssemblyId = component.get("v.prevAssembly.Id");
        var newAssemblyId = component.get("v.newAssembly.Id");
        
        if(!prevAssemblyId) 
            prevAssemblyId = null;
        else if(!newAssemblyId)
            newAssemblyId = null;
        
        if(prevAssemblyId || newAssemblyId) {
            var leaseData = component.get("v.data");
            var action = component.get("c.getTsnCsnValuesOnDateChange");
            this.showSpinner(component);
            action.setParams({
                leaseData : JSON.stringify(leaseData),
                prevAssemblyId : prevAssemblyId,
                newAssemblyId : newAssemblyId,
                userSelectedDate : component.get("v.selectedDate")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var result = response.getReturnValue();
                    component.set("v.data", result);
                    component.set("v.prevPrevTSN", result.prevTSN);
                    component.set("v.prevPrevCSN", result.prevCSN);
                    component.set("v.prevNewTSN", result.newTSN);
                    component.set("v.prevNewCSN", result.newCSN);
                } 
                else if(state === 'ERROR') {
                    var errors = response.getError();
                    this.handleErrors(errors);
                }
                this.hideSpinner(component);
            });
            $A.enqueueAction(action);
        }
    }
})