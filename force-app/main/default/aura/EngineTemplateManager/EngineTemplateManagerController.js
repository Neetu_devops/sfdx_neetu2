({
    doInit : function(component, event, helper) {
      //  component.set("v.recordId", "a1r4P00000BvS4OQAV");
        
        component.set("v.isLoading", true);
        helper.getNamespacePrefix(component);
        helper.getTemplateBasic(component);
       // helper.getTemplateData(component);
        helper.getModulePickList(component); 
    }, 
    
    
    onCatalogYearChange: function(component, event, helper) {
        var value = event.getSource().get('v.value'); 
        console.log("onCatalogYearChange value: "+value);
        console.log("onCatalogYearChange currentYear: "+(component.get("v.currentYear")));
        
    },
    
    refreshData: function(component, event, helper) {
        console.log("Event refresh ");
        component.set("v.isLoading", true);
        helper.getTemplateData(component);
    },
    
    onModuleChange: function(component, event, helper) {
        var value = event.getSource().get('v.value');  
       // console.log("onModuleChange value: "+value);
    },
    
    disableDrag : function(component, event, helper) {
        //console.log("disableDrag");
        helper.disableTableDrag(component, event, helper);
    },
    
    enableDrag: function(component, event, helper) {
        //console.log("enableDrag");
        helper.enableTableDrag(component, event, helper);
    },
    
    loadJs : function(component, event, helper) {
        //console.log('loaded');
        jQuery("document").ready(function(){            
          //  console.log('loadJs');
            var $sortableTable  = $("#tblLocations tbody");
            $sortableTable.sortable({
                items: 'tr:not(tr:first-child)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                    
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                    $(this).find("tr").each(function (index) {
                       // if (index > 0) {
                         //   $(this).find("td").eq(6).html(index);
                        //}
                    });
//                    $sortableTable.sortable( "disable" );   
                }
            }); 
            $sortableTable.sortable( "disable" );
            var maxWidth = 0;
            $('#tblLocations td:nth-child(5)').each(function(){
                if(maxWidth < $(this).width())
                    maxWidth = $(this).width();
            });
            
            $('#tblLocations td:nth-child(5)').css('width',maxWidth);
            $('#tblLocations td:nth-child(4)').css('width',maxWidth);
            $('#tblLocations td:nth-child(3)').css('width',maxWidth);
            $('#tblLocations td:nth-child(2)').css('width',maxWidth);
            $('#tblLocations td:nth-child(1)').css('width',maxWidth);

        }); 
    }, 
    
    addRow: function(component, event, helper) {
        helper.addDynamicRow(component);
    },
    
    editRow : function(component, event, helper) {
        var isEdit = component.get("v.isEdit");
        console.log("editRow "+isEdit);
        
        helper.resetEdit(component);
        
        component.set("v.isEdit", true);
        helper.enableTableDrag(component, event, helper);
    },
    
    saveRow : function(component, event, helper) {
        var isEdit = component.get("v.isEdit");
        console.log("saveRow "+isEdit);
        
        //component.set("v.isEdit", false);
        component.set("v.isLoading", true);
       
        helper.disableTableDrag(component, event, helper);
        
        helper.saveSorting(component);
        helper.updateLLDdata(component);
        
        
    },
    
    cancelRow : function(component, event, helper) {
        var isEdit = component.get("v.isEdit");
        console.log("cancelRow "+isEdit);
        component.set("v.isLoading", true);
        helper.resetEdit(component);
        helper.disableTableDrag(component, event, helper);
        helper.getTemplateData(component);
    },
    
    deleteRow : function(component, event, helper) {
        component.set("v.deleteRecordId", "");
        component.set("v.deleteRowIndex", "");
        
        var recordId = event.currentTarget.id;
        console.log("deleteRow "+recordId);
        var target = event.currentTarget;
        
        var rowIndex = target.parentNode.parentNode.rowIndex;
        console.log("deleteRow Row No : " + rowIndex + ' target:');
       
        if(recordId.includes("dateDivIndex")) {
            var dynamicRowTotalCount = component.get("v.dynamicRowTotalCount");
        	console.log("Deleting dynamically created row "+dynamicRowTotalCount);
            if(dynamicRowTotalCount == 1) {
                component.set("v.rowIndex", "0");
                component.set("v.body", []);
                component.set("v.newLLPData", []);
                component.set("v.dynamicRowTotalCount", 0);
                console.log("Clearing data "+dynamicRowTotalCount);
            }
            else{
                var indexArr = recordId.split('dateDivIndex_');
                console.log("deleteRow indexArr "+indexArr);
                var index = indexArr[1];
                
                var newData = component.get("v.newLLPData");
                
                console.log("deleteRow newData bf "+JSON.stringify(newData));
                var mapp = {};
                delete newData[index];
                console.log("deleteRow newData af "+JSON.stringify(newData));
                
                component.set("v.newLLPData", []);
                component.set("v.newLLPData", newData);
                console.log("deleteRow newData ================"+JSON.stringify(component.get("v.newLLPData")));
                
                document.getElementById("tblLocations").deleteRow(rowIndex);
                
                
            }
            dynamicRowTotalCount = dynamicRowTotalCount - 1;
            console.log("deleteRow Deleting dynamically created row after "+dynamicRowTotalCount);
            component.set("v.dynamicRowTotalCount", dynamicRowTotalCount);
        }
        else{
            console.log("Deleting data in db");
            component.set("v.deleteRecordId", recordId);
            component.set("v.deleteRowIndex", rowIndex);
            component.set("v.isOpen", true);
            //helper.deleteLLPData(component, recordId, rowIndex);
            
        }
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    confirmDelete: function(component, event, helper) {
        component.set("v.isOpen", false);
        console.log("Deleting data in db");
        var recordId = component.get("v.deleteRecordId");
        var rowIndex = component.get("v.deleteRowIndex");
        component.set("v.isLoading", true);
        helper.deleteLLPData(component, recordId, rowIndex);
    },
    
    showChildRecord: function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log("showChildRecord LLPRecord "+recordId);
        var isEdit = component.get("v.isEdit");
        var isLLPCEdit = component.get("v.isLLPCEdit");
        var isLLPLEdit = component.get("v.isLLPLEdit");
        console.log("showChildRecord LLPRecord isEdit: "+isEdit+ " isLLPCEdit: "+isLLPCEdit +" isLLPLEdit: "+isLLPLEdit);
        if(isEdit == false && isLLPCEdit == false && isLLPLEdit == false) {
            component.set("v.llpTempId", recordId);
            var target = event.currentTarget;
            var rowId = target.rowIndex;
            component.set('v.rowId', rowId);
            helper.changeRowHighlight(component, rowId);
            helper.getCatalogYear(component);
        }
    },
    
    showRecord : function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord " + recordId);
        window.open('/' + recordId);  
    },
    
    
})