({
    getNamespacePrefix : function(component) {
        console.log('getNamespacePrefix');
        var self = this;
        var action = component.get("c.getNamespacePrefix");
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            console.log("getNamespacePrefix response: "+namespace);
            component.set("v.namespace",namespace);
        });
        $A.enqueueAction(action);                
    },
  
    getCatalogYear: function(component) {
        var self = this;
        var action = component.get("c.getLLPCatalogYearTemplate");
        action.setParams({
            llpTemplateId : component.get("v.llpTempId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('getLLPCatalogYear state: '+ state);
            if (state === "SUCCESS") {
                var ctYr = response.getReturnValue();    
                console.log('getLLPCatalogYear allValues '+ctYr);
                
                var currentYr = $A.localizationService.formatDate(new Date(), "YYYY");
                var templateBasicDetails = component.get("v.templateBasicDetails");
                if(!$A.util.isEmpty(templateBasicDetails) && !$A.util.isUndefined(templateBasicDetails)) {
                    currentYr = templateBasicDetails.CurrentCatalogYear;
                }
                console.log('getCatalogYear currentYr '+currentYr);
                component.set("v.currentYear", currentYr);
                
                component.set("v.catalogYearList", ctYr);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getLLPCatalogYear errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("getLLPCatalogYear: Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getModulePickList: function(component) {
        console.log('getModulePickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'LLP_Template__c',
            "fld": 'Module_N__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getModulePickList state: ',response.getState() );
            var state = response.getState();
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
               // console.log('getModulePickList  result:'+allValues);
                component.set('v.modulePicklist', allValues);
                
                var opts = [];
                for(var i=0;i< allValues.length; i++){
                    opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                }
                component.set("v.moduleOptions", opts);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getTemplateBasic errors "+errors);
                if(errors)
                    console.log("getTemplateBasic errors "+JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
    },
    
    getTemplateBasic: function(component) {
        var self = this;
        var action = component.get("c.getTemplateBasicDetails");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('getTemplateBasic state: '+ state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();    
               // console.log('getTemplateBasic allValues '+allValues);
                component.set("v.templateBasicDetails", allValues);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getTemplateBasic errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("getTemplateBasic: Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            self.getTemplateData(component);
        });
        $A.enqueueAction(action);
    },
    
    getTemplateData : function(component) {
        console.log('getTemplateData ');
        var action = component.get("c.getTempData");
        var self = this;
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('getTemplateData state: '+ state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();    
               // console.log('getTemplateData allValues '+JSON.stringify(allValues));
               
                component.set("v.templateData", allValues);
                var totalCost = 0;
                if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues) && allValues.length > 0) {
                    for(var i = 0; i < allValues.length; i++){
                        totalCost = totalCost + parseFloat(allValues[i].CostPerCycle);
                    }
                    console.log("getTemplateData total cost "+ totalCost);
                    var rowId = component.get('v.rowId');
                    var llpIndex = 0;
                    console.log('getTemplateData rowId '+rowId);
                    if($A.util.isEmpty(rowId) || $A.util.isUndefined(rowId)) {
                        rowId = 2;
                    }
                    else{
                        llpIndex = rowId - 2;
                    }
                    console.log('getTemplateData llpIndex: '+llpIndex);
                    if(allValues.length < llpIndex)
                        llpIndex = 0;
                    var idStr = allValues[llpIndex].Id;
                    component.set("v.llpTempId", idStr);
                    setTimeout(function() {
                        var rowId = component.get('v.rowId');
                        if($A.util.isEmpty(rowId) || $A.util.isUndefined(rowId)) 
                            rowId = 2;
                        self.changeRowHighlight(component, rowId);
                     }, 1000);
                    self.getCatalogYear(component);
                }
                component.set("v.totalCost", totalCost.toFixed(2));
                component.set("v.isLoading", false);
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getTemplateData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    
    updateLLDdata : function(component) {
        var data = component.get("v.templateData");
        var self = this;
        console.log("updateLLDdata data: "+data);
        
        var newData = component.get("v.newLLPData");
        var newDataJsonStr = null;
       // console.log('updateLLDdata newData: ' +newData);
        
        if(!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData)) {
           // console.log('updateLLDdata data:dtr '+ JSON.stringify(newData));  
            var nonNullData = [];
            var j = 0;
            for(var i = 0; i < newData.length; i++) {
                var mapp = {};
                mapp = newData[i];
                console.log("updateLLDdata "+JSON.stringify(mapp));
                
                var isValid = false;
                if(!$A.util.isEmpty(mapp.DefaultPn) || 
                   !$A.util.isEmpty(mapp.IIN) || 
                   !$A.util.isEmpty(mapp.LifeLimit) || 
                   !$A.util.isEmpty(mapp.LLPDesc) 
                  ) {
                    isValid = true;
                }
                console.log("updateLLDdata isValid "+isValid);
                
                if(!$A.util.isEmpty(newData[i]) && !$A.util.isUndefined(newData[i]) && isValid == true) {
                    nonNullData[j] = newData[i];
                	j++;
                }
            }
            //console.log('updateLLDdata data:nonNullData '+ JSON.stringify(nonNullData));  
            newDataJsonStr = JSON.stringify(nonNullData);
        }
        if((!$A.util.isEmpty(data) && !$A.util.isUndefined(data)) ||
        (!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData))) {
            console.log("updateLLDdata data:str "+JSON.stringify(data));
            var dataJsonStr = JSON.stringify(data);
            var action = component.get("c.updateLLDData");
            action.setParams({
                dataStr : dataJsonStr,
                newDataStr : newDataJsonStr,
                recordId : component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('updateLLDdata state: '+ state);
                if (state === "SUCCESS") {
                    var allValues = response.getReturnValue();    
                //    console.log('updateLLDdata allValues '+allValues);
                    self.getTemplateData(component);
                    self.resetEdit(component);
                }
                else if (state === "ERROR") {
                    component.set("v.isLoading", false);
                    var errors = response.getError();
                    console.log("errors "+errors);
                    component.set("v.isEdit", true);
                    if (errors) {
                        console.log("updateLLDdata errors "+JSON.stringify(errors));
                        console.log("Error message: err " + JSON.stringify(errors[0]));
                        if (errors[0] && errors[0].pageErrors) {
                            console.log("Error message: " + errors[0].pageErrors[0].message);
                            self.showErrorMsg(component, errors[0].pageErrors[0].message);
                        }
                        else if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            self.showErrorMsg(component, errors[0].message);
                        }
                        else {
                            console.log("Unknown error");
                            self.showErrorMsg(component, "Error in creating record "+JSON.stringify(errors));
                        }
                    } else {
                        console.log("Unknown error");
                        self.showErrorMsg(component, "Error in creating record ");
                    }
                }
            });
            $A.enqueueAction(action);
        } 
    },
    
    resetEdit : function(component) {
        component.set("v.rowIndex", "0");
        component.set("v.isEdit", false);
        component.set("v.body", []);
        component.set("v.newLLPData", []);
        component.set("v.dynamicRowTotalCount", 0);
    },
    
    deleteLLPData : function(component, llpRecord, rowIndex) {
        //console.log('rowIndex '+rowIndex + 'dele '+document.getElementById("tblLocations").deleteRow(rowIndex));
        var action = component.get("c.deleteLLP");
        var self = this;
        action.setParams({
            llpId : llpRecord
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('getTemplateData state: '+ state);
            if (state === "SUCCESS") {
                console.log('rowIndex '+rowIndex + 'dele '+document.getElementById("tblLocations"));
                self.resetEdit(component);
                self.getTemplateData(component);
                document.getElementById("tblLocations").deleteRow(rowIndex); 
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getTemplateData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    saveSorting : function(component) {
        var oTable = document.getElementById('tblLocations');
        //gets rows of table
        var rowLength = oTable.rows.length;
        var tempData = component.get("v.templateData");
        var newData = component.get("v.newLLPData");
        console.log("before sorting newData: "+JSON.stringify(newData));
        //loops through rows    
        var index = 0;
        for (var i = 0; i < rowLength; i++){
            var rowId = oTable.rows.item(i).id;
            console.log("row id "+ rowId);
            if(!$A.util.isEmpty(rowId) && !$A.util.isUndefined(rowId))  {
                index = index + 1;
                
                console.log("row id "+ rowId + ' i:'+i +' index:' +index);
                if(rowId.includes('index_')){
                    var res = rowId.split("index_");
                    //console.log('split '+res[1]);
                    if(!$A.util.isEmpty(newData[res[1]]) && !$A.util.isUndefined(newData[res[1]])) 
                    	newData[res[1]].Sorting = index;
                }
                else if(!$A.util.isEmpty(tempData) && !$A.util.isUndefined(tempData)) {
                    for(var j =0; j< tempData.length; j++) {
                        //console.log("equals out "+tempData[j].Id+ ' rowId: '+rowId);
                        if(tempData[j].Id == rowId) {
                            //console.log("equals "+tempData[j].Id+ ' '+rowId);
                            tempData[j].Sorting = index;
                        }
                    }
                }
            }
        }
        if(!$A.util.isEmpty(tempData) && !$A.util.isUndefined(tempData))  {
            console.log("after sorting tempData: "+JSON.stringify(tempData));
            component.set("v.templateData", tempData);
        }
        
        if(!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData))  {
            console.log("after sorting newData: "+JSON.stringify(newData));
            component.set("v.newLLPData", newData);    
        }
            
    },
    
    addDynamicRow : function(component){
        var index = component.get("v.rowIndex");
        var self = this;
        var resLoc =   $A.get('$Resource.MRCompsIcons') + '/MRCompsIcons/LLP.svg';
        console.log('addDynamicRow resLoc: '+resLoc);
        // Array of components to create
        console.log('addDynamicRow '+ index);
        var opts = component.get("v.moduleOptions");
        var newComponents = [];
        newComponents.push(["aura:html", { //1
            "tag": "tr",
            "HTMLAttributes": {
                "id": "index_"+index
            }
        }]);
        
        
        newComponents.push(["aura:html", { //2
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", { //3
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle mouseStyle",
                "id" : "dateDivIndex_"+index,
                "onclick" : component.getReference("c.deleteRow")
            }
        }]);
        newComponents.push(["lightning:icon", {  //7
            				"iconName" : "utility:clear",
            				"size" :"xx-small"
        					}
                           ]);
        
        
        newComponents.push(["aura:html", { //2
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", { //3
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle mouseScrollStyle",
                "id" : "dateDivIndex_"+index
            }
        }]);
        newComponents.push(["lightning:icon", {  //7
            				"iconName" : "utility:sort",
            				"size" :"xx-small"
        					}
                           ]);
        
        /*newComponents.push(["aura:html", { //4
            "tag": "img",
            "HTMLAttributes": {
                "class": "imgStyle",
                "src": resLoc
            }
        }]);
        */
        
        //2nd column
        newComponents.push(["aura:html", { //5
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //6
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //7
            				"value" : component.getReference("v.newLLPData["+index+"].IIN")
        					}
                           ]);
        
        //3rd column
        newComponents.push(["aura:html", {  //8
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //9
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //10
            				"value" : component.getReference("v.newLLPData["+index+"].LLPDesc")
        					}
                           ]);
        
        //4th column
        newComponents.push(["aura:html", {  //11
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //12
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputSelect", {  //13
            				"value" : component.getReference("v.newLLPData["+index+"].Module")
        					}
                           ]);
        
        //5th column
        newComponents.push(["aura:html", {  //14
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //15
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //16
            				"value" : component.getReference("v.newLLPData["+index+"].DefaultPn")
        					}
                           ]);
        
        //6th column
        newComponents.push(["aura:html", {  //17
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //18
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle alignRight",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputNumber", {  //19
            				"value" : component.getReference("v.newLLPData["+index+"].LifeLimit"),
        					}
                           ]);
        
        //6th column
        newComponents.push(["aura:html", {  //17
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //18
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle alignRight",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:outputNumber", {  //19
            				"value" : "0",
            				"format" : "$#,###.00"
        					}
                           ]);
        
        
        $A.createComponents(newComponents,
                            function (components, status, errorMessage) {
                                if (status === "SUCCESS") {
                                    var pageBody = component.get("v.body");
                                    var tr = components[0];
                                    var trBody = tr.get("v.body");
                                    
                                    for (var i = 1; i < components.length; i += 3) {                                           
                                        var tdDt = components[i];
                                        var tdDtBody = tdDt.get("v.body");
                                        var divDt = components[i+1];
                                        var divDtBody = divDt.get("v.body");
                                        var ip = components[i+2];
                                        if((i+2) == 15) {
                                            ip.set("v.options", opts);
                                        }
                                        divDtBody.push(ip);
                                        divDt.set("v.body", divDtBody);
                                        tdDtBody.push(divDt);
                                        tdDt.set("v.body", tdDtBody);
                                        trBody.push(tdDt);
                                    }
                                    tr.set("v.body", trBody);
                                    pageBody.push(tr);
                                    component.set("v.body", pageBody);
                                    index++;
                                    component.set("v.rowIndex", index);
                                    
                                    var dyCount = component.get("v.dynamicRowTotalCount");
                                    dyCount++;
                                    component.set("v.dynamicRowTotalCount" , dyCount);
                                    
                                    
                                }
                                else // Report any error
                                {
                                    console.log("Error", "Failed to create list components.");
                                }
                            }
                           );
        
    },
        
    disableTableDrag : function(component, event, helper) {
       // console.log("disableDrag");
        jQuery("document").ready(function(){
         //   console.log("disableDrag inside");
            var $sortableTable  = $("#tblLocations tbody");
            $sortableTable.sortable( "disable" );
        });
    },
    
    enableTableDrag: function(component, event, helper) {
        //console.log("enableDrag");
        jQuery("document").ready(function(){
          //  console.log("enableDrag inside");
            var $sortableTable  = $("#tblLocations tbody");
            $sortableTable.sortable( "enable" );
        });
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    changeRowHighlight : function(component, rowId) { 
        console.log("changeRowHighlight "+rowId);
        if(!$A.util.isEmpty(rowId) && !$A.util.isUndefined(rowId))
            component.set("v.highlightedRow" , rowId);
        var table = document.getElementById('tblLocations');
        for (var i = 0, row; row = table.rows[i]; i++) {
            if(i == rowId) {
                table.rows[i].style = "background-color:#f3f2f2;";
                //console.log("changeRowHighlight matched "+rowId);
            }
            else {
                //console.log("changeRowHighlight else "+i);
                table.rows[i].style = "background-color:;";
            }
            
        }
    },
    
})