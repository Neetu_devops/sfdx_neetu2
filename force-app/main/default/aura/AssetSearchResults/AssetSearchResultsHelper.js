({
    addAsset: function(component, row) {
        var selectedItems = component.get("v.selectedRows");
        var selectedMSN = component.get("v.selectedMSN");
        var tableData = component.get("v.tableData");
        var rowIndex = tableData.indexOf(row);
        
        selectedItems.push(row.Id);
        selectedMSN.push({
            Id: row.Id, 
            Name: row.Name,
            Next_Lessee__c: row.Next_Lessee__c,
            Assigned_To__c: row.Assigned_To__c
        });
        
        tableData[rowIndex].iconName = 'utility:close';
        tableData[rowIndex].className = 'removeAssets';
        tableData[rowIndex].isSelected = true;
        tableData[rowIndex].title = 'Click to Remove';
        
        if(selectedItems.length == tableData.length) {
            component.set("v.allSelected", true);
        }
        else {
            component.set("v.allSelected", false);
        }
        
        component.set("v.tableData", tableData);
        component.set("v.selectedRows", selectedItems);
        component.set("v.selectedRowsCount", selectedItems.length);
        component.set("v.selectedMSN", selectedMSN);
    },
    
    
    removeAsset : function(component, row) {
        var selectedItems = component.get("v.selectedRows");
        var selectedMSN = component.get("v.selectedMSN");
        var tableData = component.get("v.tableData");
        var rowIndex = tableData.indexOf(row);
        var selectedIndex = selectedItems.indexOf(row.Id);
        
        tableData[rowIndex].iconName = 'utility:add';
        tableData[rowIndex].className = 'addAssets';
        tableData[rowIndex].isSelected = false;
        tableData[rowIndex].title = 'Click to Add';
        
        selectedItems.splice(selectedIndex, 1);
        selectedMSN.splice(selectedIndex, 1);        
        
        if(selectedItems.length == tableData.length) {
            component.set("v.allSelected", true);
        }
        else {
            component.set("v.allSelected", false);
        }
        
        component.set("v.tableData", tableData);
        component.set("v.selectedRows", selectedItems);
        component.set("v.selectedRowsCount", selectedItems.length);
        component.set("v.selectedMSN", selectedMSN);
    },
    
    
    addAssets : function(component, rows) {
        var selectedItems = [];
        var selectedMSN = [];
        
        rows.forEach(function(item) {
            item.isSelected = true;
            item.iconName = 'utility:close';
            item.className = 'removeAssets';
            item.title = 'Click to Remove';
            
            selectedItems.push(item.Id);
            selectedMSN.push({
                Id: item.Id, 
                Name: item.Name,
                Next_Lessee__c: item.Next_Lessee__c,
                Assigned_To__c: item.Assigned_To__c
            });
        });
        
        component.set("v.tableData", rows);
        component.set("v.selectedRows", selectedItems);
        component.set("v.selectedRowsCount", selectedItems.length);
        component.set("v.selectedMSN", selectedMSN);
    },
    
    
    removeAssets : function(component, rows) {
        rows.forEach(function(item) {
            item.isSelected = false;
            item.iconName = 'utility:add';
            item.className = 'addAssets';
            item.title = 'Click to Add';
        });
        
        component.set("v.tableData", rows);
        component.set("v.selectedRows", []);
        component.set("v.selectedRowsCount", 0);
        component.set("v.selectedMSN", []);
    }
})