({
    doInit : function (component, event, helper) {
        setTimeout(function() {
            var tableBody = document.getElementById('assetFinderTr');       
            var tableHead = document.getElementById('assetFinderTh');
            
            if(tableHead != undefined) {
                //document.getElementById('assetFinderTh').style.width = tableBody.clientWidth + 'px';
            }                         
        }, 50);        
    },
    
    
    addRemoveAssets : function(component, event, helper) {
        var index = event.target.dataset.id;
        var rows = component.get('v.tableData');
        var row = rows[index];
        
        if(row.isSelected) {
            helper.removeAsset(component, row);
        }
        else {
            helper.addAsset(component, row);
        }
    },
    
    
    updateSelectedCount : function(component, event, helper) {
        component.set("v.selectedRowsCount", 0);
        component.set("v.selectedRows", []);
        component.set("v.selectedMSN", []);
        component.set("v.allSelected", false);
    },
    
    
    selectAllAssets : function(component, event, helper) {
        var tableData = component.get("v.tableData");
        var allSelected = component.get("v.allSelected");
        
        if(allSelected) {
            component.set("v.allSelected", false);
            helper.removeAssets(component, tableData);
        }
        else {
            component.set("v.allSelected", true);
            helper.addAssets(component, tableData);
        } 
    }
})