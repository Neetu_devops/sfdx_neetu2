({
	doInit: function(cmp, event, helper) {
        
        helper.showSpinner(cmp);
        
        var action = cmp.get("c.getContacts");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var contactList =  response.getReturnValue();
                cmp.set("v.contactList", contactList);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var cmpEvent = cmp.getEvent("ErrorsEvent");
                    cmpEvent.setParams( { "errors" : errors } );
                    cmpEvent.fire();
                } else {
                    console.log("Unknown error");
                }
            }
            helper.hideSpinner(cmp);

        });
        
        $A.enqueueAction(action);
    },
    
    close : function(cmp, event, helper) {
         cmp.destroy() ;
    }
})