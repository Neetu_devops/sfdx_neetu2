({
    doInit : function(component, event) {
       
        this.showSpinner(component);
        var recdId;
        var base64Context;
        if(component.get("v.recordId") != undefined && component.get("v.recordId") != '' &&  component.get("v.recordId") != null){
            recdId = component.get("v.recordId");
        }else{
            var myPageRef = component.get("v.pageReference");
            if (myPageRef != null && myPageRef != undefined && myPageRef != '') {
                let state = myPageRef.state; // state holds any query params
                base64Context = state.inContextOfRef;
            }
            if (base64Context != undefined) {
                if (base64Context.startsWith("1\.")) {
                    base64Context = base64Context.substring(2);
                }
                var addressableContext = JSON.parse(window.atob(base64Context));
                recdId = addressableContext.attributes.recordId;
            }
            else{
                recdId = myPageRef.state.c__recordId;
            }
        }
        var action = component.get("c.getSnapshotUtilizationInfo");
        action.setParams({
            'recordId' : recdId,
            'snapshotDate' : component.get("v.selectedDate")
        });
     
        action.setCallback(this, function(response) {
            var state = response.getState();
           
            if (state === "SUCCESS") {
               
                var value = response.getReturnValue();
                console.log('value--- ',value);
                var SnapshotContainer = value;
               
                if(SnapshotContainer && SnapshotContainer.childRecords) {
                for(var i = 0; i < SnapshotContainer.childRecords.length; i++){
                    if(SnapshotContainer.childRecords[i].snapshotTSN != undefined && SnapshotContainer.childRecords[i].snapshotTSN != ''){
                        SnapshotContainer.childRecords[i].offsetTSNAtStart = (SnapshotContainer.childRecords[i].TSNAtStart != undefined && SnapshotContainer.childRecords[i].TSNAtStart != '' ) ? parseFloat(Math.round((SnapshotContainer.childRecords[i].snapshotTSN - SnapshotContainer.childRecords[i].TSNAtStart) * 100) / 100).toFixed(2) : '';
                        SnapshotContainer.childRecords[i].offsetTSNAtEnd = (SnapshotContainer.childRecords[i].TSNAtEnd != undefined && SnapshotContainer.childRecords[i].TSNAtStart != '' ) ? parseFloat(Math.round((SnapshotContainer.childRecords[i].TSNAtEnd - SnapshotContainer.childRecords[i].snapshotTSN) * 100) / 100).toFixed(2) : '';
                   
                        if(Math.sign(SnapshotContainer.childRecords[i].offsetTSNAtStart) == -1){
                            SnapshotContainer.childRecords[i].offsetTSNAtStartBGColor = 'red';
                            component.set("v.isNegativeOffset",true);
                        }else{
                            SnapshotContainer.childRecords[i].offsetTSNAtStartBGColor = '';
                        }
                       
                        if(Math.sign(SnapshotContainer.childRecords[i].offsetTSNAtEnd) == -1){
                            SnapshotContainer.childRecords[i].offsetTSNAtEndBGColor = 'red';
                            component.set("v.isNegativeOffset",true);
                        }else{
                            SnapshotContainer.childRecords[i].offsetTSNAtEndBGColor = '';
                        }
                       
                    }
                    if(SnapshotContainer.childRecords[i].snapshotCSN != undefined && SnapshotContainer.childRecords[i].snapshotCSN != '' ){
                        SnapshotContainer.childRecords[i].offsetCSNAtStart = (SnapshotContainer.childRecords[i].CSNAtStart != undefined && SnapshotContainer.childRecords[i].CSNAtStart != '' ) ? Math.round((SnapshotContainer.childRecords[i].snapshotCSN - SnapshotContainer.childRecords[i].CSNAtStart) * 100) / 100 : '';
                        SnapshotContainer.childRecords[i].offsetCSNAtEnd = (SnapshotContainer.childRecords[i].CSNAtEnd != undefined && SnapshotContainer.childRecords[i].CSNAtEnd != '' ) ? Math.round((SnapshotContainer.childRecords[i].CSNAtEnd - SnapshotContainer.childRecords[i].snapshotCSN) * 100) / 100 : '';
                       
                        if(Math.sign(SnapshotContainer.childRecords[i].offsetCSNAtStart) == -1){
                            SnapshotContainer.childRecords[i].offsetCSNAtStartBGColor = 'red';
                            component.set("v.isNegativeOffset",true);
                        }else{
                            SnapshotContainer.childRecords[i].offsetCSNAtStartBGColor = '';
                        }
                       
                        if(Math.sign(SnapshotContainer.childRecords[i].offsetCSNAtEnd) == -1){
                            SnapshotContainer.childRecords[i].offsetCSNAtEndBGColor = 'red';
                            component.set("v.isNegativeOffset",true);
                        }else{
                            SnapshotContainer.childRecords[i].offsetCSNAtEndBGColor = '';
                        }
                    }
                }
                }
                else {
                    this.showErrorMsg(component, 'Cannot create TSN/CSN snapshot as this Asset does not have any Associated Assemblies. Create Associated Assemblies first');
                }

                component.set("v.SnapshotContainer",SnapshotContainer);
               
            }else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },

    showErrorMsg: function (component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration: '5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
   
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinner",true);
        // var spinner = component.find("mySpinner");
        // $A.util.removeClass(spinner, "slds-hide");
    },
   
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinner",false);
        //var spinner = component.find("mySpinner");
        // $A.util.addClass(spinner, "slds-hide");
    },
   
    navigateTo: function(component, recId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId
        });
        navEvt.fire();
        $A.get('e.force:refreshView').fire();
    },
   
    handleErrors : function(errors) {
       
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );
                        });  
                    };
                }
            });
        }
    },
})
