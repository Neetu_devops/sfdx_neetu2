({
    doInit : function(component, event, helper) {
        component.set("v.userId",$A.get("$SObjectType.CurrentUser.Id"));
        if(component.get("v.recordId") != undefined && component.get("v.recordId") != ''){
            helper.doInit(component, event);
        }
     },
     reloadComponent: function(component, event, helper) {
        if(component.get("v.reloadCmp") === true){
            if(component.get("v.recordId") != undefined && component.get("v.recordId") != ''){
                helper.doInit(component, event);
            }
        }
     },
     updateIcon: function(component, event, helper){
        let snapshotContainer = component.get("v.SnapshotContainer");
        console.log(snapshotContainer);
        for(let key in snapshotContainer.childRecords){
            if(snapshotContainer.childRecords[key].assemblyId == component.get("v.assemblyId")){
                snapshotContainer.childRecords[key].hasLLPSnapshots = true;
                break;
            }
        }
        component.set("v.SnapshotContainer",snapshotContainer);
     },
     loadLLPDiskCmp: function(component, event, helper){
         console.log(event.target.getAttribute("data-recId1"));
         console.log(event.target.getAttribute("data-recId2"));
         console.log(event.target.getAttribute("data-recId3"));
         console.log(event.target.getAttribute("data-recId4"));
         component.set("v.assemblyId",event.target.getAttribute("data-recId1"));
         component.set("v.snapshotId",event.target.getAttribute("data-recId2"));
         component.set("v.loadedFromLLPSnapshot", event.target.getAttribute("data-recId3"));
         component.set("v.assemblyUtilId", event.target.getAttribute("data-recId4"));
         component.set("v.showModal", true);
     },
     closeModal :  function(component, event, helper){
        component.set("v.showModal", false);
    },
    afterDateSelected : function(component, event, helper) {
        if(component.get("v.selectedDate") != undefined && component.get("v.selectedDate") != ''){
            helper.doInit(component, event);
        }else{
            component.set("v.SnapshotContainer",undefined);
        }
    },
   
    keyCheck : function(component, event, helper){
        var indx = event.currentTarget.getAttribute("data-index");
        var row = event.currentTarget.getAttribute("data-row");
       
        var data = component.get("v.SnapshotContainer");
        if(row == 'TSN' && !isNaN(data.childRecords[indx].snapshotTSN)){
           
            data.childRecords[indx].offsetTSNAtStart = (data.childRecords[indx].TSNAtStart != undefined && data.childRecords[indx].TSNAtStart != '' && data.childRecords[indx].snapshotTSN != '') ? parseFloat(Math.round((data.childRecords[indx].snapshotTSN - data.childRecords[indx].TSNAtStart) * 100) / 100).toFixed(2) : '';
            data.childRecords[indx].offsetTSNAtEnd = (data.childRecords[indx].TSNAtEnd != undefined && data.childRecords[indx].TSNAtStart != '' && data.childRecords[indx].snapshotTSN != '') ? parseFloat(Math.round((data.childRecords[indx].TSNAtEnd - data.childRecords[indx].snapshotTSN) * 100) / 100).toFixed(2) : '';
            if(Math.sign(data.childRecords[indx].offsetTSNAtStart) == -1){
                data.childRecords[indx].offsetTSNAtStartBGColor = 'red';
            }else{
                data.childRecords[indx].offsetTSNAtStartBGColor = '';
            }
           
            if(Math.sign(data.childRecords[indx].offsetTSNAtEnd) == -1){
                data.childRecords[indx].offsetTSNAtEndBGColor = 'red';
            }else{
                data.childRecords[indx].offsetTSNAtEndBGColor = '';
            }
        }else if(row == 'CSN' && !isNaN(data.childRecords[indx].snapshotCSN)){
            data.childRecords[indx].offsetCSNAtStart = (data.childRecords[indx].CSNAtStart != undefined && data.childRecords[indx].CSNAtStart != '' && data.childRecords[indx].snapshotCSN != '') ? Math.round((data.childRecords[indx].snapshotCSN - data.childRecords[indx].CSNAtStart) * 100) / 100 : '';
            data.childRecords[indx].offsetCSNAtEnd = (data.childRecords[indx].CSNAtEnd != undefined && data.childRecords[indx].CSNAtEnd != '' && data.childRecords[indx].snapshotCSN != '') ?  Math.round((data.childRecords[indx].CSNAtEnd - data.childRecords[indx].snapshotCSN) * 100) / 100 : '';
           
            if(Math.sign(data.childRecords[indx].offsetCSNAtStart) == -1){
                data.childRecords[indx].offsetCSNAtStartBGColor = 'red';
            }else{
                data.childRecords[indx].offsetCSNAtStartBGColor = '';
            }
           
            if(Math.sign(data.childRecords[indx].offsetCSNAtEnd) == -1){
                data.childRecords[indx].offsetCSNAtEndBGColor = 'red';
            }else{
                data.childRecords[indx].offsetCSNAtEndBGColor = '';
            }
        }
        component.set("v.SnapshotContainer",data);
    },
   
   saveSnapshotRecord: function(component, event, helper) {
       component.set("v.disableSaveButton", true);
       helper.showSpinner(component);
       
       var SnapshotContainer = component.get("v.SnapshotContainer");
       var flag = false;
	   var showError = true;
	   if(SnapshotContainer != undefined){
        if(SnapshotContainer && SnapshotContainer.childRecords) {
		   for(var i = 0; i < SnapshotContainer.childRecords.length; i++){
			   if(!flag && ((SnapshotContainer.childRecords[i].snapshotTSN != undefined && SnapshotContainer.childRecords[i].snapshotTSN != '')
								  || (SnapshotContainer.childRecords[i].snapshotCSN != undefined && SnapshotContainer.childRecords[i].snapshotCSN != ''))){
				   //snapshotList.push(SnapshotContainer.childRecords[i]);
				   flag = true;
			   }
			   if(SnapshotContainer.childRecords[i].snapshotTSN == ''){
				   SnapshotContainer.childRecords[i].snapshotTSN = null;
			   }
			   if(SnapshotContainer.childRecords[i].snapshotCSN == ''){
				   SnapshotContainer.childRecords[i].snapshotCSN = null;
			   }
		   }
        }
        else {
            showError = false;
            helper.showErrorMsg(component, 'Cannot create TSN/CSN snapshot as this Asset does not have any Associated Assemblies. Create Associated Assemblies first');
            helper.hideSpinner(component);
        }
	   }   
       if(flag){
           var action = component.get("c.saveSnapshotDetails");
           action.setParams({
               'wrapperData' : JSON.stringify(component.get("v.SnapshotContainer"))
           });
           action.setCallback(this, function(response) {
               var state = response.getState();
               
               if (state === "SUCCESS") {
                   helper.hideSpinner(component);
                   component.set("v.disableSaveButton", false);
                   var value = response.getReturnValue();
                   console.log('SaveValue--- ',value);
                   var toastEvent = $A.get("e.force:showToast");
                   toastEvent.setParams({
                       title : 'Success',
                       message: 'Record saved successfully',
                       duration:' 500',
                       type: 'success',
                   });
                   
                   toastEvent.fire();
                   
                   helper.navigateTo(component, value.Id);
                   
               }else if (state === "ERROR") {
                   component.set("v.disableSaveButton", false);
                   helper.hideSpinner(component);
                   var errors = response.getError();
                   helper.handleErrors(errors);
               }
               
           });
           $A.enqueueAction(action);
       }else if(showError == true){
           helper.hideSpinner(component);
           component.set("v.disableSaveButton", false);
            var toastEvent = $A.get("e.force:showToast");
                   toastEvent.setParams({
                       title : 'Error',
                       message: 'Provide Snapshot for atleast one row to save.',
                       duration:' 500',
                       type: 'error',
                   });
                   
                   toastEvent.fire();
       }
    },
   
    checkNegativeSnapshot : function(component, event, helper) {
       
        var SnapshotContainer = component.get("v.SnapshotContainer");
        var flag = false;
        for(var i = 0; i < SnapshotContainer.childRecords.length; i++){
            if(SnapshotContainer.childRecords[i].offsetTSNAtStart < 0 ||
               SnapshotContainer.childRecords[i].offsetTSNAtEnd < 0 ||
               SnapshotContainer.childRecords[i].offsetCSNAtStart < 0 ||
               SnapshotContainer.childRecords[i].offsetCSNAtEnd < 0){
                flag = true;
                break;
            }
        }
        if(flag){
            component.set("v.isNegativeOffset",true);
        }else{
            component.set("v.isNegativeOffset",false);
        }
    },
   
    clearSnapshotRecord : function(component, event, helper) {
        var SnapshotContainer = component.get("v.SnapshotContainer");
        for(var i = 0; i < SnapshotContainer.childRecords.length; i++){
            SnapshotContainer.childRecords[i].snapshotTSN = undefined;
            SnapshotContainer.childRecords[i].offsetTSNAtStart = undefined;
            SnapshotContainer.childRecords[i].offsetTSNAtEnd = undefined;
            SnapshotContainer.childRecords[i].snapshotCSN = undefined;
            SnapshotContainer.childRecords[i].offsetCSNAtStart = undefined;
            SnapshotContainer.childRecords[i].offsetCSNAtEnd = undefined;
        }
        component.set("v.isNegativeOffset",false);
        component.set("v.SnapshotContainer",SnapshotContainer);
    },
   
    cancelDialog: function(component, event, helper) {
        var base64Context;
        var myPageRef = component.get("v.pageReference");
        if (myPageRef != null && myPageRef != undefined && myPageRef != '') {
            let state = myPageRef.state; // state holds any query params
            base64Context = state.inContextOfRef;
        }
        var recdId;
        if (base64Context != undefined) {
            if (base64Context.startsWith("1\.")) {
                base64Context = base64Context.substring(2);
            }
            var addressableContext = JSON.parse(window.atob(base64Context));
            recdId = addressableContext.attributes.recordId;
        }
        else{
            if(myPageRef.state.c__recordId != undefined && myPageRef.state.c__recordId != '' &&  myPageRef.state.c__recordId != null){
                recdId = myPageRef.state.c__recordId;
            }else{
                recdId = component.get("v.recordId");
            }
        }
        console.log(recdId);
        helper.navigateTo(component, recdId);
    }
})
