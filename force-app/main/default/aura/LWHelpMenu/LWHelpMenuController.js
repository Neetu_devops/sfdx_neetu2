({
    
	scriptsLoaded : function(component, event, helper) {
		console.log('javaScript files loaded successful'); 
	},
    
      doInit: function(cmp, event, helper) {
	
        	
          var sObj;
		  var IframeURL='';	
	      var sParam = 'sParam';
          var x = window.location.href;
          console.log('---x--'+x);
          var ParamValue = helper.getParameterByName(cmp ,sParam);  //getting the URL parameter value
          console.log('---ParamValue--'+ParamValue);
          cmp.set('v.sourceParamValue', ParamValue);
          
          
          //call apex class method
          var action = cmp.get('c.getHelpVideo');
          action.setParams({ vParameter : ParamValue });
          action.setCallback(this, function(response) {
          //store state of response
          var state = response.getState();
          if (state === "SUCCESS") {
          //set response value in ListOfHelpVideo attribute on component.
          cmp.set('v.ListOfHelpVideo', response.getReturnValue());
		   	  
              
              
              sObj=JSON.stringify(response.getReturnValue());
              console.log(' sObj is:' + sObj);
              
              var arr = sObj;
    
            for (var i = 0; i < arr.length; i++){
              console.log("array index: " + i);
              var obj = arr[i];
              for (var key in obj){
                var value = obj[key];
                console.log("Json Data" + key + ": " + value);
              }
            }
				            
           }
          });
          
          $A.enqueueAction(action); 
          
    }
    
})