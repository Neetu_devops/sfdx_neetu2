({
	doAction : function(component, event, helper) { 
        var recordIdVar = component.get("v.recordId");
        console.log(" recordIdVar+" + recordIdVar);
		var finalURL = window.location.protocol +"//"+window.location.hostname+"/" + recordIdVar ;
        window.open(finalURL ,"_self");                
        
    },
    
	doInit : function(component, event, helper) {
        
        var recordIdVar = component.get("v.recordId");
        console.log("recordIdVar="+recordIdVar);
        var action = component.get("c.runMR_Recon_apex");
        //action.setParams({ leaseId_p : recordIdVar});
        action.setCallback(this,function(a){
        	console.log("after runMR_Recon_apex");
        	console.log(a.getState());
            if(a.getState()=="SUCCESS")	{
            	console.log("after runMR_Recon_apex Success");
                var messageReturn = a.getReturnValue();	
                var res = messageReturn.split(";");
                component.set("v.retMessage",res[1]);
            }
        });
        $A.enqueueAction(action); 
        
	}
})