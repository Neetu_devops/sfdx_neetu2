({
    fetchEventTypeList : function(assemblyType, component, event, helper) {
        console.log('------'+component.get("v.mrId")+assemblyType);
        var lstMR = component.get("v.listMRShare");
        var lstAddedMRShare = [];
        
        //creating list for Added MR Share
        if(lstMR != undefined && lstMR.length > 0) {
            for(var i=0; i<lstMR.length; i++) {
                var addedMRShare;
				var mrShareWrapper = lstMR[i];  
                
                if((mrShareWrapper.mrShareRecord.Assembly__c == undefined || 
                    mrShareWrapper.mrShareRecord.Assembly__c == '--None--' || 
                    mrShareWrapper.mrShareRecord.Assembly__c=='')) {
                    addedMRShare = mrShareWrapper.mrShareRecord.Assembly__c;
                    
                    if(mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c == undefined || 
                       mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c == '--None--' || 
                       mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c=='') {
                        addedMRShare += mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c;
                    }
                    
                    lstAddedMRShare.push(addedMRShare);
                }
            }
        }
        
        console.log('lstAddedMRShare', lstAddedMRShare);
        
        var action = component.get("c.fetchEventType");
        action.setParams({
            "assemblyMRId": component.get("v.mrId"),
            "assemblyType": assemblyType,
            'lstAddedMRShare_p': lstAddedMRShare
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(component.isValid() && state === "SUCCESS") {
                helper.hideSpinner(component, event, helper);
                var res = response.getReturnValue();
                
                if(res !== undefined){
                    console.log(res);                   
                    var lstMRType = component.get("v.listMRShare"); 
                    
                    for(var i=0; i<lstMRType.length; i++ ){
                        if(assemblyType == lstMRType[i].mrShareRecord.Assembly__c && lstMRType[i].mrShareRecord.Maintenance_Event_Name__c != undefined && lstMRType[i].mrShareRecord.Maintenance_Event_Name__c != '--None--'){
                            var index = res.indexOf(lstMRType[i].mrShareRecord.Maintenance_Event_Name__c);
                            res.splice(index,1); 
                        }	
                    }
                    console.log(res);
                    component.set("v.listAssemblyEvent", res);
                    component.set("v.mrShare.isDisable",false);
                }
            }
            else{
                var error = response.getError();
                $A.get("e.force:closeQuickAction").fire();
                helper.handleErrors(error);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error",
            duration: 5000
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    }
})