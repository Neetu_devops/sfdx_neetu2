({
    doInit: function(component, event, helper) {
        console.log('***',component.get("v.rowIndex"));
        console.log('****----*',component.get("v.mrShare"));
    },
    
    onAssemblyChange : function(component, event, helper) {        
        var assemblyType = component.find("assemblyType").get("v.value");
        
        helper.showSpinner(component, event, helper);
        component.set("v.mrShare.mrShareRecord.Maintenance_Event_Name__c",undefined);
        helper.fetchEventTypeList(assemblyType, component, event, helper);
    },
    
    onAssemblyEventChange : function(component, event, helper) {
        var assemblyEventType = component.find("assemblyEventType").get("v.value");
    },
    
    addRow: function(component, event, helper) {
        var listMRShare = component.get("v.listMRShare");
        var lstAddedMRShare = [];
        var sNo = listMRShare.length;
        
        //creating list for Added MR Share
        if(listMRShare != undefined && listMRShare.length > 0) {
            console.log('listMRShare[i]', listMRShare[0]);
            for(var i=0; i<listMRShare.length; i++) {
                var addedMRShare;
                var mrShareWrapper = listMRShare[i];  
                
                if((mrShareWrapper.mrShareRecord.Assembly__c != undefined && 
                    mrShareWrapper.mrShareRecord.Assembly__c != '--None--' && 
                    mrShareWrapper.mrShareRecord.Assembly__c != '')) {
                    addedMRShare = mrShareWrapper.mrShareRecord.Assembly__c;
                    
                    if(mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c != undefined && 
                       mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c != '--None--' && 
                       mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c != '') {
                        addedMRShare += mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c;
                    }
                    
                    lstAddedMRShare.push(addedMRShare);
                }
            }
        }
        
        console.log('lstAddedMRShare', lstAddedMRShare);
        
        var action = component.get("c.fetchAssemblyType");
        action.setParams({
            "assemblyMRId": component.get("v.mrId"),
            "mrShareId": component.get("v.mrShareId"),
            'lstAddedMRShare_p': lstAddedMRShare
        });
        action.setCallback(this, function(response){
            helper.hideSpinner(component, event, helper);
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var res = response.getReturnValue();
                
                if(res !== undefined) {                                     
                    res.sNo = sNo;
                    listMRShare.push(res);
                    
                    component.set("v.assemblyType", res.lstAssemblyType);                    
                    component.set("v.listMRShare", listMRShare);
                    console.table(component.get("v.listMRShare"));
                }                
            }
            else{
                var error = response.getError();
                $A.get("e.force:closeQuickAction").fire();
                helper.handleErrors(error);
            }
        });
        
        $A.enqueueAction(action); 
    },
    
    removeRow: function(component, event, helper) {
        var listMRShare = component.get("v.listMRShare");
        if(listMRShare.length == 2) { 
            listMRShare.sNo = 1;
        }
        listMRShare.pop(listMRShare.length -1);
        
        component.set("v.listMRShare",listMRShare);
        console.table(component.get("v.listMRShare"));
    }
})