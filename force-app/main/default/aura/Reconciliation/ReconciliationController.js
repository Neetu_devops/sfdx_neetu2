({
	doInit : function(component, event, helper) {
        helper.doInit(component, event);
    },
    
    validateReconciliation : function(component, event, helper){
        helper.showSpinner(component);
        
        var reconciliationObj = component.get("v.reconciliationRec");
        
        if(reconciliationObj.reconciliationType == undefined){
            helper.showToast(component, 'Error','error','Please select Reconciliation Type.');
        }else{
            
            var action = component.get("c.validateReconciliationRecord");
            action.setParams({
                "reconciliationData": JSON.stringify(component.get("v.reconciliationRec")),
                "recordId": component.get("v.recordId")
            }); 
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                
                if (response.getState() === "SUCCESS") {
                    var value =  response.getReturnValue();
                    
                    component.set('v.confirmationMsg',value.confirmationMsg);
                    component.set('v.displayConfirmationFooter',value.displayConfirmationFooter);
                    var modal = component.find("confirmationModal");
                    $A.util.removeClass(modal, 'slds-hide');
                }
                else if(state === "ERROR") {
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
                helper.hideSpinner(component);
            });
            $A.enqueueAction(action);
        }
    },
 
    proceedNext : function(component, event, helper) {
        var reconciliationRec = component.get('v.reconciliationRec');
        helper.closeModal(component, event, helper);
        component.set('v.reconciliationType',reconciliationRec.reconciliationType);
     },
    
    saveReconciliation : function(component, event, helper) {
        console.log("saveReconciliation on 1st page");
        if(component.get('v.reconciliationType') == 'Excess Utilization'){
            var childComp = component.find('excessReconciliationCmp');
            childComp.saveReconciliation();
        }else{
            if(component.get("v.page1")){
                console.log("save parent on 1st page");
              component.set("v.page1",false); 
              component.set("v.page2",true); 
            }
            
            var childComp = component.find('reconciliationCmp');
            childComp.saveReconciliation();
        }
        
    },
    cancelDialog : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        
        if(recordId != null) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
    },
    
    closeConfirmationModal : function(component, event, helper) {
    	helper.closeModal(component, event, helper);
    },
    recallPreviousCmp: function(component, event, helper) {
        console.log("on click of back "+component.get("v.page2"));
        if(component.get("v.page2") && component.get("v.reconciliationType")== 'Assumed Ratio'){
            component.set("v.page1",true);
            component.set("v.page2",false);
            
            var childComp = component.find('reconciliationCmp');
            childComp.callPage1();
            
        }else{
        component.set('v.reconciliationType',undefined);
        }
    },
})