({
	doInit: function(component, event) {
        this.showSpinner(component);
        var action = component.get("c.getReconciliationRecordData");
        action.setParams({
            "recordId": component.get("v.recordId"),
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (response.getState() === "SUCCESS") {
                
                var allData =  response.getReturnValue();
                component.set("v.reconciliationRec", allData);
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    closeModal : function(component, event, helper){
        var modal = component.find("confirmationModal");
        $A.util.addClass(modal, 'slds-hide');
    },
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinner",true);
    },
    
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinner",false);
    },
    
    showToast: function(component, title, type, message){

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : type,
            "message": message
    	});
    	toastEvent.fire();
        this.hideSpinner(component);
    },
    
    handleErrors: function (errors, type) {
        _jsLibrary.handleErrors(errors, type); // Calling the JS Library function to handle the error
    },
})