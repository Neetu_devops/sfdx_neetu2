({
    doInit : function(component, event, helper) {
        console.log("doInit: " + component.get('v.recordId'));
        helper.getParamValue('MidTermLease','c__LeaseId', component);
        helper.getLeaseData(component);
        helper.getTypePickList(component);
        console.log('isExtensionStep3 :'+component.get("v.isExtensionStep3"));
        console.log('isExtensionStep1 :'+component.get("v.isExtensionStep1"));
        var dtUpdate = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set("v.dateofUpdate",dtUpdate);
        console.log('dateofUpdate :'+dtUpdate);
        
        if(component.get("v.isExtensionStep3") == true){
            component.set("v.isModalOpen",false);
        }
        console.log("istoLE : MidTerm :"+component.get("v.istoLE"));
        if(component.get("v.istoLE") == true){
            component.set("v.isModalOpen",false);
            component.set("v.isExtensionStep2",true);
        }
        console.log('isModalOpen :'+component.get("v.isModalOpen"));
        
    },
    
    onTypeChange : function(component, event, helper){
        console.log('onTypeChange');
        helper.getTypePickList(component);
    },
    
    openRecord: function(component,event,helper) {
        var record = event.currentTarget.id;
        console.log("openRecord "+record);
        window.open('/' + record);  
    },
    
    openModel: function(component, event, helper) {
        // Set isModalOpen attribute to true
        component.set("v.isModalOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
        helper.leasePagePreview(component);
    },
    
    //Submit RecordType for the record Step 1 :& on Save if RecordType == Lease Extension then we populate data for Lease Extension Step 1 
    submitType: function(component,event,helper){
        component.set("v.isModalOpen", false);
        console.log('submitType');
        var dateofUpdate = component.find('dateId').get("v.value");
        component.set("v.dateofUpdate",dateofUpdate);
        
        if($A.util.isEmpty(dateofUpdate) || $A.util.isUndefined(dateofUpdate)) {
            component.set("v.isDateErrorMsg",True); 
            component.set("v.isModalOpen", True);
            component.set("v.isExtensionStep1",False); 
        }else{				
            
            var type = component.find('typeId').get("v.value");
            component.set("v.typeValue",type);
            
            console.log('submitType type:'+type);
            if(type == 'Lease Extension'){
                component.set("v.isModalOpen", false);
                component.set("v.isExtensionStep1",true); 
            }
            else if(type == 'Lease Amendment'){
                component.set("v.isModalOpen", false);
                component.set("v.isAmendment",true); 
                
            }
                else if(type == 'Side Letter'){
                    component.set("v.isModalOpen", false);
                    component.set("v.isSideLetter",true); 
                }
                    else if(type == 'Novation'){
                        component.set("v.isModalOpen", false);
                        component.set("v.isNovationStep1",true); 
                    }
            
            var isExtensionStep1 = component.get("v.isExtensionStep1");
            
            if(isExtensionStep1 == true){
                var startDate = component.get("v.leaseStartDate");
                var endDate = component.get("v.leaseEndDate");
                helper.getExtensionStartDate(component,endDate);
                }
        } 
    },
    
    
    //Lease Extension Step 3 : on Save
    fetchLEStep3:function(component,event,helper){
        
        console.log('fetchLEStep3 :');
        var leComments = component.find('commentId').get("v.value");
        component.set("v.comments",leComments);
        console.log('fetchLEStep3 :',leComments);
        
        helper.createLE(component);
        console.log(' go on to upload file');
        component.set("v.isModalOpen", false);
    },
    
    fetchLA: function(component,event,helper){
        
        console.log('fetchLA');
        var amendName = component.find('amendmentNameId').get("v.value");
        if($A.util.isEmpty(amendName) || $A.util.isUndefined(amendName)){
            component.set("v.isAmndNameErrorMsg",true);
        }else{
            component.set("v.amendmentName",amendName);
            component.set("v.isAmndNameErrorMsg",false);    
            console.log('fetchLA amendName :',component.get("v.amendmentName"));
        }
        
        var amendDate = component.find('amendmentDateId').get("v.value");
        if($A.util.isEmpty(amendDate) || $A.util.isUndefined(amendDate)){
            component.set("v.isAmndDateErrorMsg",true);
        }else{
            component.set("v.amendmentDate",amendDate);
            component.set("v.isAmndDateErrorMsg",false);    
            console.log('fetchLA :',component.get("v.amendmentDate"));
        }
        
        var amendComments = component.find('comment2Id').get("v.value");
        component.set("v.comments",amendComments);
        console.log('fetchLA :',amendComments);
        
        var iserrorName = component.get("v.isAmndNameErrorMsg");
        var iserrorDate = component.get("v.isAmndDateErrorMsg");
        console.log('errorName :'+iserrorName+'errorDate :'+iserrorDate);
        if(iserrorName == false && iserrorDate == false){
            helper.createLA(component);
        }
        component.set("v.isModalOpen", false);
    },
    
    fetchSL: function(component,event,helper){
        
        console.log('fetchSL');
        var sideLettrName = component.find('sideLetterNameId').get("v.value");
        if($A.util.isEmpty(sideLettrName) || $A.util.isUndefined(sideLettrName)){
            component.set("v.isSideLetterNameErrorMsg",true);
        }else{
            component.set("v.sideLetterName",sideLettrName);
            component.set("v.isSideLetterNameErrorMsg",false);    
            console.log('fetchSL SLName :',component.get("v.sideLetterName"));
        }
        
        var sideLettrDate = component.find('sideLetterDateId').get("v.value");
        if($A.util.isEmpty(sideLettrDate) || $A.util.isUndefined(sideLettrDate)){
            component.set("v.isSideLetterDateErrorMsg",true);
        }else{
            component.set("v.sideLetterDate",sideLettrDate);
            component.set("v.isSideLetterDateErrorMsg",false);    
            console.log('fetchSL SLDate :',component.get("v.sideLetterDate"));
        }
        
        
        var slComments = component.find('comment3Id').get("v.value");
        component.set("v.comments",slComments);
        console.log('fetchSL slComments :',component.get("v.comments"));
        
        var iserrorName = component.get("v.isSideLetterNameErrorMsg");
        var iserrorDate = component.get("v.isSideLetterDateErrorMsg");
        console.log('errorName :'+iserrorName+'errorDate :'+iserrorDate);
        if(iserrorName == false && iserrorDate == false){
            helper.createSL(component);
        }
        component.set("v.isModalOpen", false);
    },
    
    clickNovNext: function(component,event,helper){
       
        console.log('clickNovNext');
        
        console.log('isNovationStep1 :'+component.get("v.isNovationStep1"));
        if(component.get("v.isNovationStep1")){
            var novName = component.find('novationNameId').get("v.value");
            if($A.util.isEmpty(novName) || $A.util.isUndefined(novName)){
                component.set("v.isNovNameErrorMsg",true);
            }else{
                component.set("v.novationName",novName);
                component.set("v.isNovNameErrorMsg",false);    
                console.log('clickNovNext NovName :',component.get("v.novationName"));
            }
            
            var novDate = component.find('novationDateId').get("v.value");
            if($A.util.isEmpty(novDate) || $A.util.isUndefined(novDate)){
                component.set("v.isNovDateErrorMsg",true);
            }else{
                component.set("v.novationDate",novDate);
                component.set("v.isNovDateErrorMsg",false);    
                console.log('clickNovNext NovDate :',component.get("v.novationDate"));
            }
            
            var novComments = component.find('comment4Id').get("v.value");
            component.set("v.comments",novComments);
            console.log('clickNovNext novComments :',component.get("v.comments"));
        }
        
       // var nocfile = component.find("fileId4").get("v.files");
       // component.set("v.fileToBeUploaded",nocfile);
        
        console.log('nov file :',component.get("v.fileToBeUploaded"));
        var iserrorName = component.get("v.isNovNameErrorMsg");
        var iserrorDate = component.get("v.isNovDateErrorMsg");
        console.log('errorName :'+iserrorName+'errorDate :'+iserrorDate);
        if(iserrorName == false && iserrorDate == false){
            
            component.set("v.isNovationStep1",false);
            component.set("v.isNovationStep2",true);
            
        }
        component.set("v.isModalOpen", false);
    },
    
    fetchNov: function(component,event,helper){
        console.log('fetchNov');
        component.set("v.isModalOpen", false);
        helper.createNov(component);
    },
    
    onLessorChanges:function(component,event,helper){
        console.log('onLessorChanges');
        var lessorVal = component.get("v.lessorLookUpRecord");
        console.log('onLessorChanges'+lessorVal.Id);
        
    },
    
    onGuarantorChanges:function(component,event,helper){
        console.log('onGuarantorChanges');
        var guarantorVal = component.get("v.guarantorLookUpRecord");
        console.log('onGuarantorChanges'+guarantorVal.Id);
        
    },
    
    onSTChanges:function(component,event,helper){
        console.log('onSTChanges');
        var securtyVal = component.get("v.securityTrusteeLookUpRecord");
        console.log('onSTChanges'+securtyVal.Id);
     
    },
    
    onFinancierChanges:function(component,event,helper){
        console.log('onFinancierChanges');
        var financirVal = component.get("v.financierLookUpRecord");
        console.log('onFinancierChanges'+financirVal.Id);
       
    },
    
    onLessorMRChanges:function(component,event,helper){
        console.log('onLessorMRChanges');
        var lessorMR = component.get("v.lessorMRLookUpRecord");
        console.log('onLessorMRChanges'+lessorMR.Id);
      
    },
    
    onLessorRentChanges:function(component,event,helper){
        console.log('onLessorRentChanges');
        var lessoRent = component.get("v.lessorRentLookUpRecord");
        console.log('onLessorRentChanges'+lessoRent.Id);
      
    },
    
    //Lease Extension Step 2 : on Save
    gotoRentStudio: function(component,event,helper){
        console.log('gotoRentStudio');
        console.log('gotoRentStudio :'+component.get('v.recordId'));
        
        var extStartDate = component.get("v.extensionStartValue");
        console.log('gotoRentStudio extStartDate :'+extStartDate);
        
        var extEndDate = component.get("v.extensionEndValue");
        console.log('gotoRentStudio extEndDate'+extEndDate);
        
        var updateRent = component.get("v.updateSchedule");
            component.set("v.updateSchedule",updateRent);
            console.log('fetchLE updateRent :'+updateRent); 
            
            component.set("v.isFromLE",true);
            
            component.set("v.isExtensionStep1",false);
            var updateSchedule = component.get("v.updateSchedule");
        if(updateSchedule){
            component.set("v.isExtensionStep2",true);
        }
        else{
            component.set("v.isExtensionStep3",true);
        }
        
        if(extEndDate < extStartDate){
            console.log('gotoRentStudio if dates');
            component.set("v.isExEndErrorMsg",true);
            component.set("v.ExEndErrorMsg","The Extension Period End Date cannot be earlier than the Extension Period Start Date");
            component.set("v.isExtensionStep1",true);
            component.set("v.isExtensionStep3",false);
            component.set("v.isExtensionStep2",false);
        }
        if(extEndDate != null){
                component.set("v.extensionEndValue",extEndDate); 
                var term = component.get("v.extTermValue");
                console.log('gotoRentStudio extEndDate :'+extEndDate+' term:'+term);
            }
        if(extEndDate == null){
            component.set("v.isExEndErrorMsg",true);
            component.set("v.ExEndErrorMsg","Please Enter the Extension Period End Date ");
            component.set("v.isExtensionStep1",true);
            component.set("v.isExtensionStep3",false);
            component.set("v.isExtensionStep2",false);
            }
        component.set("v.isModalOpen", false);
    },
    
    //on blur of Extension End Date, we calculate Term
    getTerm : function(component,event,helper){
        console.log('getTerm');
        component.set("v.isExEndErrorMsg",false);
        var extensionStartDate = component.get("v.extensionStartValue");
        console.log('getTerm extensionStartDate :'+extensionStartDate);
        var extensionEndDate = component.get("v.extensionEndValue");
        
        console.log('getTerm extensionEndDate :'+extensionEndDate);
        if(extensionEndDate != undefined && extensionEndDate != '' && extensionEndDate != null){
            const d1 = new Date(extensionStartDate);
            const d2 = new Date(extensionEndDate);
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();
           if( months <= 0)
               months = 0
               
            console.log('diffDays' +months);
            component.set("v.extTermValue",months );
        }
        
    },
    
   
    handleFilesChange: function(component, event, helper) {
        console.log('handleFilesChange :');
        var fileName = "No File Selected..";
        var files='';
        
        var type = component.get("v.typeValue");
        if(type == 'Lease Extension'){
        var fileCount=component.find("fileId1").get("v.files").length;
        if (fileCount > 0) {
            for (var i = 0; i < fileCount; i++) 
            {
                fileName = component.find("fileId1").get("v.files")[i]["name"];
                files=files+' '+fileName;
            }
        }
        }
         if(type == 'Lease Amendment'){
        var fileCount=component.find("fileId2").get("v.files").length;
        if (fileCount > 0) {
            for (var i = 0; i < fileCount; i++) 
            {
                fileName = component.find("fileId2").get("v.files")[i]["name"];
                files=files+' '+fileName;
            }
        }
        }
         if(type == 'Side Letter'){
        var fileCount=component.find("fileId3").get("v.files").length;
        if (fileCount > 0) {
            for (var i = 0; i < fileCount; i++) 
            {
                fileName = component.find("fileId3").get("v.files")[i]["name"];
                files=files+' '+fileName;
            }
        }
        }
         if(type == 'Novation'){
        var fileCount=component.find("fileIdnov").get("v.files").length;
        if (fileCount > 0) {
            for (var i = 0; i < fileCount; i++) 
            {   
                var filesNov = component.get("v.fileToBeUploaded");
                if (filesNov && filesNov.length > 0) {
                var file = filesNov[0][0];
                fileName = file.name;
                files=files+' '+fileName;
                console.log('handleFilesChange files :'+files+' fileName : '+fileName);
                }
                
            }
        }
        }
        component.set("v.fileName", files);
    },
    
    refreshAll : function(component, event, helper){
       console.log('refreshAll'); 
    window.location.reload();
    },
    
    
})