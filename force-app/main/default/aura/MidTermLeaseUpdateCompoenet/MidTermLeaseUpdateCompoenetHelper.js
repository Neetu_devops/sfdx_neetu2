({
    getParamValue : function( tabName, paramName,component ) {
        
        var url = window.location.href;
        var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&amp;amp;amp;');
        var paramValue = '';
        console.log("getParamValue url "+url);
        console.log("getParamValue allParams "+allParams);
        if(allParams != null && allParams != undefined){
            for(var i=0; i < allParams.length; i++) {
                if(allParams[i].split('=')[0] == paramName)
                    paramValue = allParams[i].split('=')[1];
            }
            console.log("getParamValue: " + paramValue);
            component.set('v.recordId', paramValue);
             this.getNamespacePrefix(component);
        }
    }, 
    
    getNamespacePrefix : function(component) {
        console.log('getNamespacePrefix');
        var self = this;
        var action = component.get("c.getNamespacePrefix");
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            if(namespace != null){
            console.log("getNamespacePrefix response: "+namespace);
            component.set("v.namespace",namespace);
            }
        });
        $A.enqueueAction(action);                
    },
    
    leasePagePreview : function(component, event, helper) {
        console.log('leasePagePreview');
        var leaseRec = component.get("v.recordId");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": leaseRec,
            "slideDevName": "related"
        });
        navEvt.fire();
        $A.get('e.force:refreshView').fire();
        
    },
    
    getLeaseData : function(component, event, helper) {
        console.log('getLeaseData');
        var action = component.get("c.getLeaseInfo");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
             var state = response.getState();
            console.log("fetchLeaseData response-state: "+state);
            if (state === "SUCCESS") {
            var leaseInfo = response.getReturnValue();
            console.log("getLeaseData response: "+JSON.stringify(leaseInfo));
                if(leaseInfo != null){
            component.set("v.lease",leaseInfo.Name);
            component.set("v.leaseId",leaseInfo.Id);
            component.set("v.asset",leaseInfo.Aircraft__r.MSN_Number__c);
            component.set("v.assetId",leaseInfo.Aircraft__c);
            component.set("v.leaseStartDate",leaseInfo.Lease_Start_Date_New__c);
            component.set("v.leaseEndDate",leaseInfo.Lease_End_Date_New__c);
                }
           }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("fetchLeaseData errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("fetchLeaseData Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
            
        });
        $A.enqueueAction(action); 
    },

    getExtensionStartDate : function(component,endDate){
        console.log("getExtensionStartDate");
      var action = component.get("c.getExtensionStartDate");
        action.setParams({
            endDate : endDate
        });
        action.setCallback(this, function(response) {
             var state = response.getState();
            console.log("getExtensionStartDate response-state: "+state);
            if (state === "SUCCESS") {
                var exStartDate = response.getReturnValue();
                console.log("getExtensionStartDate response: "+exStartDate);
                if(exStartDate != null){
                    component.set("v.extensionStartValue",exStartDate);
                    
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getExtensionStartDate errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("getExtensionStartDate Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
            
        });
        $A.enqueueAction(action);   
    },
    
    
    createLE : function(component, event, helper) {
        console.log('createLE');
        
        console.log('recordId :'+component.get("v.recordId"));
        console.log('type:'+component.get("v.typeValue"));
        console.log('dateOfUpdate: '+component.get("v.dateofUpdate"));
        console.log('exStartDAte: '+component.get("v.extensionStartValue"));
        console.log('exEndDate: '+component.get("v.extensionEndValue"));
        console.log('term: '+component.get("v.extTermValue"));
        console.log('isUpdateRent: ' +component.get("v.updateSchedule"));
        console.log('comments: '+component.get("v.comments"));
        var action = component.get("c.createLeaseExtension");
        action.setParams({
            recordId : component.get("v.recordId"),
            etype: component.get("v.typeValue"),
            dateOfUpdate: component.get("v.dateofUpdate"),
            exStartDAte: component.get("v.extensionStartValue"),
            exEndDate: component.get("v.extensionEndValue"),
            term: component.get("v.extTermValue"),
            isUpdateRent: component.get("v.updateSchedule"),
            comments:component.get("v.comments")
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("createLE response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result != null){
                console.log("createLE response: "+JSON.stringify(result));
                console.log("createLE response: "+result.Id);    
                component.set("v.recId",result.Id); 
                console.log("createLE rec Id :"+component.get("v.recId"));    
                if(component.find("fileId1").get("v.files")==undefined)
                {
                    console.log('if1');
                    this.leasePagePreview(component);
                }
                else if (component.find("fileId1").get("v.files").length > 0) {
                    console.log('if2');
                    var s = component.get("v.FilesUploaded");
                    var fileName = "";
                    var fileType = "";
                    var fileCount=component.find("fileId1").get("v.files").length;
                    if (fileCount > 0) {
                        for (var i = 0; i < fileCount; i++) 
                        {
                            this.uploadHelper(component, event,component.find("fileId1").get("v.files")[i]);
                                                    }
                    }
                } 
                else {
                    this.showMessage("Please Select a Valid File",false);
                }
                }
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("createLA errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("createLA Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
            
        });
        $A.enqueueAction(action);    
    },
    
    createLA : function(component, event, helper) {
        console.log('createLA');
        var aName = component.get("v.amendmentName");
        var aDate = component.get("v.amendmentDate");
        var aComments = component.get("v.comments");
        var type = component.get("v.typeValue");
        console.log('createLA : aName '+aName+ 'aDate: '+aDate+ 'aComments: '+aComments+'type: '+type);
        var action = component.get("c.createLeaseAmendment");
        action.setParams({
            recordId : component.get("v.recordId"),
            aType: component.get("v.typeValue"),
            dateOfUpdate: component.get("v.dateofUpdate"),
            aName: component.get("v.amendmentName"),
            aDate: component.get("v.amendmentDate"),
            comments:component.get("v.comments")
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("createLA response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result != null){
                console.log("createLA response: "+JSON.stringify(result));
                console.log("createLA response: "+result.Id);    
                component.set("v.recId",result.Id); 
                console.log("createLA rec Id :"+component.get("v.recId"));    
                if(component.find("fileId2").get("v.files")==undefined)
                {
                    console.log('if1');
                    this.leasePagePreview(component);
                }
                if (component.find("fileId2").get("v.files").length > 0) {
                    console.log('if2');
                    var s = component.get("v.FilesUploaded");
                    var fileName = "";
                    var fileType = "";
                    var fileCount=component.find("fileId2").get("v.files").length;
                    if (fileCount > 0) {
                        for (var i = 0; i < fileCount; i++) 
                        {
                            this.uploadHelper(component, event,component.find("fileId2").get("v.files")[i]);
                                                    }
                    }
                } 
                else {
                    this.showMessage("Please Select a Valid File",false);
                }
                }
                
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("createLA errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("createLA Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);    
    },
    
    createSL :function(component, event, helper) {
        console.log('createSL');
        var sName = component.get("v.sideLetterName");
        var sDate = component.get("v.sideLetterDate");
        var sComments = component.get("v.comments");
        var stype = component.get("v.typeValue");
        console.log('createSL : sName '+sName+ 'sDate: '+sDate+ 'sComments: '+sComments+'stype: '+stype);
        var action = component.get("c.createSideLetter");
        action.setParams({
            recordId : component.get("v.recordId"),
            sType: component.get("v.typeValue"),
            dateOfUpdate: component.get("v.dateofUpdate"),
            sName: component.get("v.sideLetterName"),
            sDate: component.get("v.sideLetterDate"),
            comments:component.get("v.comments")
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("createSL response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result != null){
                console.log("createSL response: "+JSON.stringify(result));
                console.log("createSL response: "+result.Id);    
                component.set("v.recId",result.Id); 
                console.log("createSL rec Id :"+component.get("v.recId"));    
                if(component.find("fileId3").get("v.files")==undefined)
                {
                    console.log('if1');
                    this.leasePagePreview(component);
                }
                if (component.find("fileId3").get("v.files").length > 0) {
                    console.log('if2');
                    var s = component.get("v.FilesUploaded");
                    var fileName = "";
                    var fileType = "";
                    var fileCount=component.find("fileId3").get("v.files").length;
                    if (fileCount > 0) {
                        for (var i = 0; i < fileCount; i++) 
                        {
                            this.uploadHelper(component, event,component.find("fileId3").get("v.files")[i]);
                                                    }
                    }
                } 
                else {
                    this.showMessage("Please Select a Valid File",false);
                }
                }
                                
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("createSL errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("createSL Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
            
        });
        $A.enqueueAction(action);    
    },
    
    createNov :function(component, event, helper) {
        console.log('createNov');
        var lessorId;
        var guarantorId;
        var financirId;
        var securtyId;
        var lessorMRId;
        var lessorRentId;
        var managingCompany = component.find('companyNameId').get("v.value");
        component.set("v.companyName",managingCompany);
        if(component.get("v.lessorLookUpRecord") != null){
        lessorId = component.get("v.lessorLookUpRecord").Id;
            console.log('fetchNov: '+lessorId);}
        if(component.get("v.guarantorLookUpRecord") != null){
        guarantorId = component.get("v.guarantorLookUpRecord").Id;
            console.log('fetchNov: '+guarantorId);}
        if(component.get("v.securityTrusteeLookUpRecord") != null){
        financirId = component.get("v.securityTrusteeLookUpRecord").Id;
            console.log('fetchNov: '+financirId);}
        if(component.get("v.financierLookUpRecord") != null){
        securtyId = component.get("v.financierLookUpRecord").Id;
            console.log('fetchNov: '+securtyId);}
        if(component.get("v.lessorMRLookUpRecord") != null){
        lessorMRId = component.get("v.lessorMRLookUpRecord").Id;
            console.log('fetchNov: '+lessorMRId);}
        if(component.get("v.lessorRentLookUpRecord") != null){
        lessorRentId = component.get("v.lessorRentLookUpRecord").Id;
            console.log('fetchNov: '+lessorRentId);}
        
        
        var action = component.get("c.createNovation");
        action.setParams({
            recordId : component.get("v.recordId"),
            nType: component.get("v.typeValue"),
            dateOfUpdate: component.get("v.dateofUpdate"),
            nName: component.get("v.novationName"),
            nDate: component.get("v.novationDate"),
            comments: component.get("v.comments"),
            lessor: lessorId,
            managingCompany :managingCompany,
            guarantor: guarantorId,
            financir: financirId,
            securty: securtyId,
            lessorMR: lessorMRId,
            lessorRent: lessorRentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("createNov response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result != null){
                console.log("createNov response: "+JSON.stringify(result));
                console.log("createNov response: "+result.Id);
                
                component.set("v.recId",result.Id); 
                console.log("createNov rec Id :"+component.get("v.recId")); 
                 
                var novFile = component.get("v.fileToBeUploaded");
                if(novFile == null)
                {
                    console.log('if1');
                    this.leasePagePreview(component);
                }
                else if (novFile != null) {
                    console.log('if2');
                    var s = component.get("v.FilesUploaded");
                    var fileName = "";
                    var fileType = "";
                    var fileCount=novFile.length;
                    if (fileCount > 0) {
                        for (var i = 0; i < fileCount; i++) 
                        {
                            this.uploadHelper(component, event,novFile[i]);
                                                    }
                    }
                } 
                
                } 
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("createNov errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("createNov Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
            
        });
        $A.enqueueAction(action);     
    },
    
    getTypePickList: function(component) {
        console.log('getTypePickList');
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Lease_Extension_History__c',
            "fld": 'Type__c'
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('getTypePickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getTypePickList result:'+allValues);
                //component.set('v.typePickListValue', allValues);
                var typeList = [];
                var j=0;
                for(var i=0;i< response.getReturnValue().length; i++) {
                    var data = response.getReturnValue()[i];
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    typeList[j] = response.getReturnValue()[i];
                    j++;
                    
                }
                console.log('getTypePickList opts :',opts);
                var Type = component.find('typeId');
                if(Type != undefined){
                    Type.set("v.options", opts); 
                }
                component.set('v.typePickListValue', typeList);
                console.log('getTypePickList typeList :',typeList);
            }
            var fieldV = component.get("v.typeValue");
            console.log('getTypePickList fieldV :',fieldV);
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousTypePickList(component);
        });
        $A.enqueueAction(action);
    },
    
    showPreviousTypePickList: function(component) {
        var opts = component.find("typeId").get("v.options");
        var fieldV = component.get("v.typeValue");
        var valueList = component.get("v.typePickListValue");
        console.log('showPreviousTypePickList valueList '+valueList);
        if(!$A.util.isEmpty(valueList) && !$A.util.isUndefined(valueList)) {
            for(var i=0;i< valueList.length; i++) {
              if(valueList[i] == fieldV) 
                    opts[i].selected = true;
                else
                    opts[i].selected = false;
            }
            var rentType = component.find('typeId');
            if(rentType != undefined){
                rentType.set("v.options", opts); 
            }
        }
        console.log('showPreviousTypePickList fieldV: '+fieldV+" "+valueList);
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000, //Chunk Max size 750Kb 
    
    uploadHelper: function(component, event,f) {
        console.log('uploadHelper');
        component.set("v.showLoadingSpinner", true);
        var type = component.get("v.typeValue");
        if (type == 'Novation'){
        var novFile = component.get("v.fileToBeUploaded");
        var file = novFile[0][0];
            console.log('uploadHelper Novation file : '+file.Name);
        }
        else{
          var file = f;  
        }
        var self = this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function  
        if (file.length > self.MAX_FILE_SIZE) {
            component.set("v.showLoadingSpinner", false);
            component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
            return;
        }
        
        // Convert file content in Base64
        var objFileReader = new FileReader();
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;
            fileContents = fileContents.substring(dataStart);
            self.uploadProcess(component, file, fileContents);
        });
        
        objFileReader.readAsDataURL(file);
    },
    
    uploadProcess: function(component, file, fileContents) {
        console.log('uploadProcess');
        // set a default size or startpostiton as 0 
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value   
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        
        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },
    
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        console.log('uploadInChunk');
        // call the apex method 'saveFile'
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.saveFile");
        console.log("uploadInChunk: ",component.get("v.recId"));
        action.setParams({
            // Take current object's opened record. You can set dynamic values here as well
            parentId: component.get("v.recId"), 
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });
        
        // set call back 
        action.setCallback(this, function(response) {
            attachId = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                // update the start position with end postion
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                    this.leasePagePreview(component);

                } else {
                    this.showMessage('your File is uploaded successfully',true);
                    component.set("v.showLoadingSpinner", false);
                    this.leasePagePreview(component);

                }
                // handel the response errors        
            } else if (state === "INCOMPLETE") {
                this.showMessage("From server: " + response.getReturnValue(),false);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    showMessage : function(message,isSuccess) {
        console.log('showMessage');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": isSuccess?"Success!":"Error!",
            "type":isSuccess?"success":"error",
            "message": message
        });
        toastEvent.fire();
    },
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
})