({
    getColumns : function(component , event, isChildUpdate) {
        console.log("Table cmp getColumns "+isChildUpdate);
        var self = this;
        var action = component.get('c.getColumns');
        var objectName = component.get('v.parentObject');
        var fields = component.get('v.parentFields');
        action.setParams({
            objectName : objectName,
            fieldSetName : fields
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
           // console.log("getColumns parent values " +JSON.stringify(allValues));
            component.set("v.columns", allValues); 
            var cols = [];
            for(var i = 0; i < allValues.length; i++) {
             //   console.log("allValues[i].apiName.includes('status') "+allValues[i].apiName+" : "+allValues[i].apiName.includes('status'));
                if(allValues[i].apiName == 'Name') {
                    cols.push({label: allValues[i].label, fieldName: allValues[i].apiName, 
                               type: 'button', initialWidth: 100,
                               typeAttributes: {label: { fieldName: allValues[i].apiName }}});
				}
                else if(allValues[i].apiName.includes('Status')) {
                    cols.push({label :allValues[i].label, fieldName: allValues[i].apiName,initialWidth: 80, });
                }
                else if(allValues[i].apiName == 'Id') {}
                else if(allValues[i].apiName == 'Records_Item_Description__c') {
                    cols.push({label :allValues[i].label, fieldName: allValues[i].apiName , 
                               initialWidth: 400,
                               type : allValues[i].fieldType, sortable: true});
                }
                else
                	cols.push({label :allValues[i].label, fieldName: allValues[i].apiName , 
                               initialWidth: 60,
                               type : allValues[i].fieldType, sortable: true});
            }
            component.set("v.columns", cols);
            self.getRows(component, event, isChildUpdate);
        });
        $A.enqueueAction(action);
    },
    
    getRows : function(component , event,isChildUpdate) {
        var action = component.get('c.getRows');
        action.setParams({
            objectName : component.get('v.parentObject'),
            mappingCond : component.get('v.parentCond'),
            recordId : component.get("v.assetReviewRecordId"),
            fieldSetName : component.get('v.parentFields'),
            parentReviewType : component.get('v.parentRecordType'),
            sectionField: component.get('v.parentZone'),
            section: component.get('v.sectionName')
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getRows table helper class values " +JSON.stringify(allValues));
            component.set("v.data", allValues); 
            component.set("v.currentCount", component.get("v.initialRows"));
            if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues)) {
                if(isChildUpdate)
                    this.removeAllChildforRefresh(component);
                this.addTableRows(component, allValues, isChildUpdate);
            }
        });
        $A.enqueueAction(action);
    },
    
    removeAllChildforRefresh : function(component){
        try{
            var tableId = "table-"+component.get("v.sectionName")+"-"+component.get("v.parentRecordType");
            console.log("removeAllChildforRefresh values " +tableId);
            var node = document.getElementById(tableId);
            console.log("removeAllChildforRefresh node " + node);
            while (node.hasChildNodes()) {
                node.removeChild(node.lastChild);
            }
            console.log("removeAllChildforRefresh haschild: " +node.hasChildNodes());
        }
        catch(e){ console.log("removeAllChildforRefresh exception : "+e);}
    },
    
    addTableRows: function(component,retRecords, isChildUpdate){
        var self = this;
        var fields = component.get("v.columns");  
        var tableId = "table-"+component.get("v.sectionName")+"-"+component.get("v.parentRecordType");
        console.log("getRows values " +tableId);
        var rowCount = 0;
        retRecords.forEach(function(s) {
            rowCount++;
            var tableRow = document.createElement('tr');
            
            //creating an sequence number 
            var tableData = document.createElement('td');
            var tableDataNode = document.createTextNode(rowCount);
            tableData.appendChild(tableDataNode);
            tableData.style="color:#000000; font-size:13px; text-align:left; width:30px";
            tableData.classList.add('topBorderOnly');
            tableData.classList.add('bottomBorderOnly');
            tableRow.style="height:80px;"
            tableRow.appendChild(tableData);
            //-----------------------------
                
            fields.forEach(function(field){ 
                var tableData = document.createElement('td');
                var tableDataNode = document.createTextNode(s[field.fieldName]);
                var colorTd = "000000;";
                if(field.fieldName == "Name") {
                    colorTd = "005fb2;";
                    var link = document.createElement("a");
                    link.setAttribute("href", "/"+s["Id"]);
                    link.setAttribute("target","_blank");
                    link.appendChild(tableDataNode);
                    tableData.appendChild(link);
                }
                else
                    tableData.appendChild(tableDataNode);
                tableRow.id = s["Id"];
               // console.log("table idd "+tableRow.id);
                tableRow.onclick = self.createClickHandler(tableRow,s,rowCount,component,isChildUpdate);
                tableData.style="color:#"+colorTd+"; font-size:13px; text-align:left; ";
                //tableData.classList.add('slds-truncate');
                tableData.classList.add('wrap-text');
                tableData.classList.add('topBorderOnly');
                tableData.classList.add('bottomBorderOnly');
                tableRow.style="height:60px;"
                
                tableRow.onmouseover = function() {
                   // console.log("bg color===== " + this.style.backgroundColor);
                    var rgb = this.style.backgroundColor;
                    if(!$A.util.isEmpty(rgb) && !$A.util.isUndefined(rgb) && 
                       (self.colorToHex(this.style.backgroundColor) == '#ccf2ff')) 
                        this.style.backgroundColor = "#ccf2ff";
                    else
                    	this.style.backgroundColor = "#e8e8e3";
                };
                tableRow.onmouseout =  function() {
                    //console.log("bg color===== " + this.style.backgroundColor);
                    if((self.colorToHex(this.style.backgroundColor) == '#ccf2ff'))
                        this.style.backgroundColor = "#ccf2ff";
                    else
                    	this.style.backgroundColor = "";
                };
                console.log("onclick selctedRow: " + component.get("v.selectedRow") + " rowId: "+ tableRow.id);
                //if(isChildUpdate && (component.get("v.selectedRow") == tableRow.id))
                if(isChildUpdate && (component.get("v.selectedRow") == tableRow.id)) {
                    console.log("onclick selctedRow: equals " + component.get("v.selectedRow") + " rowId: "+ tableRow.id);
                 //   tableRow.style = "background-color:#ccf2ff;height:60px;";
                }
                
                tableRow.appendChild(tableData);
                
            });
            setTimeout(function() {
                document.getElementById(tableId).appendChild(tableRow);
            }, 1000);
        });
    },
    
    colorToHex: function (color) {
        if (color.substr(0, 1) === '#') {
            return color;
        }
        var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);
        
        var red = parseInt(digits[2]);
        var green = parseInt(digits[3]);
        var blue = parseInt(digits[4]);
        
        var rgb = blue | (green << 8) | (red << 16);
        return digits[1] + '#' + rgb.toString(16).padStart(6, '0');
    },

    
    createClickHandler : function(selectedRow, rowData, rowCount, cmp, isChildUpdate) {
        var self=this;
        return function() {
            console.log('createClickHandler');
            cmp.set("v.selectedRow", selectedRow.id);
            var id = rowData["Id"];
            var name = cmp.get("v.sectionName") + "" + (rowCount) + ": "+rowData["Name"];
            var onItemChange = cmp.getEvent("onItemChange");
            onItemChange.setParams({
                "itemId": id,
                "name": name,
                "index" : cmp.get("v.sectionIndex"),
                "childIndex" : rowCount-1
            });
            onItemChange.fire();
            self.removeSelectedItem(cmp);
            
            var tableId = "table-"+cmp.get("v.sectionName")+"-"+cmp.get("v.parentRecordType");
            //console.log("tableIDdd:"+tableId);
            var table = document.getElementById(tableId);
            console.log("onclick " +tableId + " "+table.rows);
            for (var i = 0, row; row = table.rows[i]; i++) {
                console.log("onclick table.rows[i].id: " +i+" : "+table.rows[i].id +" selectedRow.id"+selectedRow.id);
                if(table.rows[i].id == selectedRow.id) {
                    table.rows[i].style = "background-color:#ccf2ff;height:60px;";
                }
                else {
                    table.rows[i].style = "background-color:;height:60px;";
                }
                
            }
        };
    },
    
    removeSelectedItem: function(component) {
        var secList =component.get("v.sectionList");
        //console.log("removeSelectedItem "+secList.length);
        secList.forEach(function(sec) {
            var id = sec;
            var tableId = "table-"+id+"-"+component.get("v.parentRecordType");
            try{
                var table = document.getElementById(tableId);
                for (var i = 0, row; row = table.rows[i]; i++) {
                    table.rows[i].style = "background-color:;height:60px;";
                }  
            }
            catch(e){console.log("removeSelectedItem exception:"+e)}
        });
      
    }, 

    fireSuccessToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'title' : 'Success', 
            'message' : 'Record updated sucessfully.' ,
            'type':'success'
        }); 
        toastEvent.fire(); 
    },
    
    fireFailureToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'title' : 'Failed', 
            'message' : 'An error occurred.',
            'type':'error'
        }); 
        toastEvent.fire(); 
    },
    
    fireRefreshEvt : function(component) {
        var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent){
            console.log("fireRefreshEvt");
            refreshEvent.fire();
        }
    }
})