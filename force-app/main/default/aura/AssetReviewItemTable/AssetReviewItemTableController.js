({
	doInit : function(component, event, helper) {
		helper.getColumns(component, event, false);
        //helper.getTotalCount(component);
	},
    
    refreshCmp : function(component,event,helper) {
        helper.getColumns(component, event, true);
    },
    
    handleRowAction : function(cmp,event,helper){
        var row = event.getParam('row');
        var data = cmp.get("v.data");
        var rowIndex = data.indexOf(row);
        //console.log("handleRowAction " + rowIndex);
        var name = cmp.get("v.sectionName") + "" + (rowIndex+1) + ": "+row.Name;
		var onItemChange = cmp.getEvent("onItemChange");
        onItemChange.setParams({
			"itemId": row.Id,
			"name": name,
            "index" : cmp.get("v.sectionIndex"),
            "childIndex" : rowIndex
		});
		onItemChange.fire();
    },
    
    handleSaveEdition : function (component, event, helper) {
        var draft = event.getParam('draftValues');
		//console.log("handleSaveEdition draftValues "+JSON.stringify(draft));
        var action = component.get('c.updateRrdItem');
        action.setParams({ 
            objectItem: draft
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.fireSuccessToast(component);  
                helper.fireRefreshEvt(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(JSON.stringify(errors));
                helper.fireFailureToast(component);  
            }
        });
        $A.enqueueAction(action);
    },
    
})