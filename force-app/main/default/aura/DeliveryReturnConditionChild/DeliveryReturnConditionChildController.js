({
    doInit : function(component, event, helper) {
        var action = component.get("c.getDeliveryReturnData");
        var recordId = component.get("v.recordId");
        action.setParams({
            "scenarioInp": recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log('deliveryReturnCondition',dataObj);
                if(dataObj != undefined){
                  component.set("v.rcChildAttribute",dataObj);
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    helper.handleErrors(errors);
                }
            }
        });
        $A.enqueueAction(action);
    },
})