({
    doInit : function(cmp, event, helper){
    },
    afterScriptsLoaded : function(component, event, helper)
    {
        helper.DisplayAssetbyLocation(component,event,helper);
    },    
    handleSelectorchange : function(component, event, helper)
    {
        helper.HandleKpiChange(component, event, helper);
    },

//Deals Filter generated as a pop up 
navigateToDeals: function(component, event, helper){
    var modalBody;
    var kpi= component.get("v.kpisel");
        $A.createComponent("c:PortfolioAnalyzerFilters", {
            "cmpEvent": component.getReference("c.cmpEvent"),
            "SelectedKPI": kpi
        },
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   component.find('overlayLib').showCustomModal({
                       header: "Deals Filter",
                       body: modalBody,
                       showCloseButton: true,
                       cssClass: "mymodal",
                       closeCallback: function() {
                          // alert('You closed the alert!');
                       }
                   })
               }
           });
    },
    cmpEvent:function(component, event, helper){
        var msg=event.getParam("message");
        var dealt = event.getParam("dealType");
        var dealsSelected=event.getParam("ActiveSelectedDeals");
        var kpi= component.get("v.kpisel");
        console.log("kpi selected is---"+kpi);
        if(dealt){
        component.set("v.projectedData",dealt);
        component.set("v.flagForChartTitle",true);
        helper.PieChartProjected(component, event, helper);
        component.set("v.dealfilterdata",dealt);
        console.log("messgae is----"+msg);
        console.log("deal type and data ----"+dealt);
        }
        else{
            component.set("v.projectedData",dealsSelected);
        component.set("v.flagForChartTitle",false);
        helper.PieChartProjected(component, event, helper);
        component.set("v.SelectedDealsPlusCurrent",dealsSelected);
        }
       
        
    }

})
