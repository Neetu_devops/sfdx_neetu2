({
    DisplayAssetbyLocation : function(component, event, helper)
    {
        var assetByLocCurr = component.get("c.GetAssetByLocation");
        assetByLocCurr.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                component.set("v.data",dataObj);
                component.set("v.flagForChartTitle",false);
                helper.PieChartCurrent(component,event,helper);
               
            }
        });
        $A.enqueueAction(assetByLocCurr);
        var getDeals = component.get("c.GetProjectedAssetByLocation");
        getDeals.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dealresult= response.getReturnValue();
                component.set("v.projectedData",dealresult);
                component.set("v.flagForChartTitle",false);
                helper.PieChartProjected(component,event,helper);
            }
        });
        $A.enqueueAction(getDeals);

    },

    HandleKpiChange : function(component,event,helper){
        var kpiSelected = component.get("v.kpisel");
        if(kpiSelected==='Operator'){
            helper.DisplayOperatorChart(component,event,helper);
        }
        else if(kpiSelected === 'Country'){
            helper.DisplayAssetbyLocation(component,event,helper);
        }
    },

    DisplayOperatorChart : function(component,event,helper){
        var getOperators = component.get("c.GetCurrentOperators");
        getOperators.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var OperatorResult= response.getReturnValue();
                component.set("v.data",OperatorResult);
                component.set("v.flagForChartTitle",true);
                helper.PieChartCurrent(component,event,helper);
            }
        });
        $A.enqueueAction(getOperators);
        var getProjectedOperators = component.get("c.GetProjectedOperators");
        getProjectedOperators.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var OperatorResult= response.getReturnValue();
                component.set("v.projectedData",OperatorResult);
                component.set("v.flagForChartTitle",true);
                helper.PieChartProjected(component,event,helper);
            }
        });
        $A.enqueueAction(getProjectedOperators);
    },

    PieChartCurrent : function(component,event,helper) {
        
        var jsonData = component.get("v.data");
        var flagForOperator= component.get("v.flagForChartTitle");
        var dataObj = JSON.parse(jsonData);
        var chartTitle='';
        var chartSubTitle='';
        if(flagForOperator){
            chartTitle = 'Aircraft By Operator',
            chartSubTitle='Aircraft distribution by Operator'
        }
        else{
             chartTitle ='Aircraft By Location',
             chartSubTitle = 'Aircraft distribution by Country'
             
        }
        var groupSmallData = function(series, number) {
            var groupValue = 0,
              smallGroup = [];
            Highcharts.each(series.data, function(refSmall) {
              if (refSmall.y < number) {
                groupValue += refSmall.y;
              } else {
                smallGroup.push([refSmall.name, refSmall.y])
              }
            });
            smallGroup.push(['others', groupValue]);
            series.setData(smallGroup)
          }
        new Highcharts.Chart({
            colors: ['#09857C','#10B552','#6CF5A3','#F5E7A1','#F7B360', '#DAA985', '#DA858C', '#2B5469',
        '#479287', '#479261', '#85DAA8', '#E35D6D', '#443A63','#C6947C','#FAD3A2','#FDF3AF','#067532','#10B552','#5DAB74','#5DAB9B','#8BD3F7','#E6EEFC'],
            chart: {
                plotBackgroundColor: null,
                plotShadow: false,
                renderTo: component.find("currentPieChart").getElement(),
                type: 'pie',
                style: {
                    fontFamily: 'Salesforce Sans, Arial, sans-serif'
                },
            },
            
            title: {
                text: chartTitle + ' (Current)',
                style: {
                    color: 'Black',
                    fontFamily: 'Salesforce Sans,Arial,sans-serif',
                    fontSize: '15px'
                }
            },
            subtitle: {
                style: {
                    color: '#393a3b',

                },
                text: chartSubTitle
            },
            xAxis: {
                categories: component.get("v.xAxisCategories"),
                crosshair: true,
                gridLineColor: '#707073',
                labels: {
                style: {
                    color: '#E0E0E3'
                }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                title: {
                    style: {
                        color: '#A0A0A3'

                    }
                }
            },
            yAxis: {
                min: 0,
                title:
                {
                    text: component.get("v.yAxisParameter")
                },
                gridLineColor: '#707073',
        labels: {
            style: {
                color: '#E0E0E3'
            }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
            style: {
                color: '#A0A0A3'
            }
        }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
               
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '65%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ',
                    }

                }
            },
            
            series: [{
                name:'Aircraft concentration',
                data:dataObj,
            }]
 
        },function(chart) {
            var series = chart.series[0];
            groupSmallData(series, 3);
          });
 
    },
 
    
    PieChartProjected : function(component,event,helper){
        var jsonData = component.get("v.projectedData");
        var flagForOperator= component.get("v.flagForChartTitle");
        var  chartTitle ='';
        var chartSubTitle='';
        if(flagForOperator){
              chartTitle = 'Aircraft By Operator',
              chartSubTitle='Aircraft distribution by Operator'
          }
          else{
               chartTitle ='Aircraft By Location',
               chartSubTitle = 'Aircraft distribution by Country'
          }
        var dealResult = JSON.parse(jsonData);
        console.log('inside PieChartProjected and the data is : '+dealResult);
        var groupSmallData = function(series, number) {
            var groupValue = 0,
              smallGroup = [];
            Highcharts.each(series.data, function(refSmall) {
              if (refSmall.y < number) {
                groupValue += refSmall.y;
              } else {
                smallGroup.push([refSmall.name, refSmall.y])
              }
            });
            smallGroup.push(['others', groupValue]);
            series.setData(smallGroup)
          }
       new Highcharts.Chart({
        colors: ['#09857C','#10B552','#6CF5A3','#F5E7A1','#F7B360', '#DAA985', '#DA858C', '#2B5469',
        '#479287', '#479261', '#85DAA8', '#E35D6D', '#443A63','#C6947C','#FAD3A2','#FDF3AF','#067532','#10B552','#5DAB74','#5DAB9B','#8BD3F7','#E6EEFC'],
            chart: {
                plotBackgroundColor: null,
                plotShadow: false,
                renderTo: component.find("projectedPieChart").getElement(),
                type: 'pie',
                style:{
                    fontFamily: 'Salesforce Sans,Arial,sans-serif'
                }
            },
            title: {
                text: chartTitle+ ' (Projected)',
                style: {
                    color: 'Black',
                    fontFamily: 'Salesforce Sans,Arial,sans-serif',
                    fontSize: '15px'
                }
            },
            subtitle: {
                text: chartSubTitle
            },
            xAxis: {
                categories: component.get("v.xAxisCategories"),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title:
                {
                    text: component.get("v.yAxisParameter")
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '65%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y} ',
                    }

                }
            },
            series: [{
                name:'Aircraft concentration',
                data:dealResult
            }]
 
        },function(chart) {
            var series = chart.series[0];
            groupSmallData(series, 3);
          });
    }
})
