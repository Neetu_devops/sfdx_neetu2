({
    doInit : function(component, event, helper) {
        var opts = [];
        opts.push({"class": "optionClass", label: "10", value: "10",  selected: true});
        opts.push({"class": "optionClass", label: "15", value: "15"});
        opts.push({"class": "optionClass", label: "20", value: "20"});
        var pages = component.find(pageSize);
        if(pages != undefined){
            pages.set("v.options", opts); 
        }
        helper.getTitle(component);
        
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.find("pageSize").get("v.value"); 
        component.set("v.isLoading", true);
        helper.viewAllSnapshotCall(component, pageNumber, pageSize);
        
    }, 
    
    showSnapshotComment: function(component, event, helper) {
        component.set("v.showComment",true);
    },
    
    createSnapshot: function(component, event, helper) {
        var cmt =  component.get("v.comments");
        if($A.util.isEmpty(cmt) || $A.util.isUndefined(cmt)) 
            return;
        component.set("v.showComment",false);
        component.set("v.isLoading", true);
        helper.createSnapshotCall(component);
        
    }, 
    
    showRecord: function(component,event,helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord "+recordId);
        window.open('/' + recordId);  
    },
    
    showChildRecord: function(component,event,helper) {
        var snapshotId = event.currentTarget.id;
        console.log("showChildRecord snapshotId: "+snapshotId);
        component.set("v.snapshotId" ,snapshotId);
    },
    
    openPop : function(component,event, helper) {
        var indexStr = event.currentTarget.id;
        console.log('openPop indexStr: '+indexStr);
        
        component.set("v.showViewer",true );
        component.set("v.snapshotId",indexStr );
        
        var cmpTarget = component.find('pop');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        
     },
    
    closePop : function(component, event, helper) {
        console.log('closePop showViewerFromChild');
        component.set("v.showViewer",false);
        component.set("v.showViewerFromChild",true);
        component.set("v.snapshotId",'');
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
    
    handleCancel: function (component, event, helper) {
        component.set("v.showComment",false);
        $A.get("e.force:closeQuickAction").fire();
    },
    
    handleNext: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.find("pageSize").get("v.value");
        pageNumber++;
        component.set("v.isLoading", true);
        helper.viewAllSnapshotCall(component, pageNumber, pageSize);
    },
     
    handlePrev: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.find("pageSize").get("v.value");
        pageNumber--;
        component.set("v.isLoading", true);
        helper.viewAllSnapshotCall(component, pageNumber, pageSize);
    },
     
    handleFirst: function(component, event, helper) {
        var pageSize = component.find("pageSize").get("v.value");
        var pageNumber = 1;
        component.set("v.isLoading", true);
        helper.viewAllSnapshotCall(component, pageNumber, pageSize);
    },
    
    handleLast: function(component, event, helper) {
        var totalRecords = component.get("v.TotalRecords");  
        var pageSize = component.find("pageSize").get("v.value");
        var pageNumber = Math.ceil(totalRecords / pageSize);
        component.set("v.isLoading", true);
        helper.viewAllSnapshotCall(component, pageNumber, pageSize);
    },
    
    onSelectChange: function(component, event, helper) {
        var page = 1
        var pageSize = component.find("pageSize").get("v.value");
        component.set("v.isLoading", true);
        helper.viewAllSnapshotCall(component, page, pageSize);
    },
    
})