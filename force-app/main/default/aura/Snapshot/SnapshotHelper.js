({
    getTitle : function(component) {
        var recordIdL = component.get("v.recordId");
        console.log('getTitle recordId: '+recordIdL );
        var self = this;
        if($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getObjectName');
        action.setParams({
            recordId: recordIdL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getTitle : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getTitle : response "+allValues);
                if(allValues != null) 
                    component.set('v.title', allValues); 
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getTitle errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getTitle Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
	viewAllSnapshotCall : function(component, pageNumberL, pageSizeL) {
        var recordIdL = component.get("v.recordId");
        console.log('viewAllSnapshotCall recordId: '+recordIdL);
        var self = this;
        if($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getAllSnapshots');
        action.setParams({
            recordId: recordIdL,
            pageNumber: pageNumberL,
            pageSize: pageSizeL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("viewAllSnapshotCall : state "+state);
            component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("viewAllSnapshotCall : response "+allValues);
                if(allValues != null) {
                   // console.log("viewSnapshotCall : response af items "+JSON.stringify(allValues));
                    component.set('v.Snapshots', allValues.SnapshotItems);
                    component.set("v.PageNumber", allValues.pageNumber);
                    component.set("v.TotalRecords", allValues.totalRecords);
                    component.set("v.RecordStart", allValues.recordStart);
                    component.set("v.RecordEnd", allValues.recordEnd);
                    component.set("v.TotalPages", Math.ceil(allValues.totalRecords / pageSizeL));
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("viewAllSnapshotCall errors "+errors);
                if (errors) {
                    console.log("viewAllSnapshotCall errors "+errors);
                    self.handleErrors(errors);   
                } else {
                    console.log("viewAllSnapshotCall Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    createSnapshotCall : function(component) {
        var recordIdL = component.get("v.recordId");
        var commentL = component.get("v.comments");
        console.log('createSnapshotCall recordId: '+recordIdL + ' comments:'+commentL);
        var self = this;
        if($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.createSnapshotForRecord');
        action.setParams({
            recordId: recordIdL,
            comment: commentL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("createSnapshotCall : state "+state);
            component.set("v.isLoading", false);
            component.set("v.comments", "");
            if (state === "SUCCESS") { 
                var allValues = response.getReturnValue();
                console.log("createSnapshotCall : response "+allValues);
                component.set("v.isLoading", true);
                var pageNumber = component.get("v.PageNumber");  
                var pageSize = component.find("pageSize").get("v.value"); 
                self.viewAllSnapshotCall(component, pageNumber, pageSize);
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("createSnapshotCall errors "+JSON.stringify(errors));
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("createSnapshotCall Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})