({
    doInit : function(component, event, helper) {
        if(component.get("v.wrapperObj.record.Id") != undefined && component.get("v.wrapperObj.record.Id") != ''){
            component.set("v.invoiceObj.Lease__c",component.get("v.wrapperObj.record.Id")); 
            helper.populateMRInfo(component,component.get("v.wrapperObj.record.Id"));
        } 
    },
    leaseChanged : function(component, event, helper) {
        
        var val = event.getParam("recordByEvent");
        var leaseId = val.Id.trim();
        component.set("v.invoiceObj.Lease__c",leaseId);
        
        if(!$A.util.isEmpty(leaseId)){
            helper.populateMRInfo(component,leaseId);
        }else{
            component.set("v.assemblyMRInfoList", new Array());
            component.set("v.totalAmount",0);
        }
    },
    removeSelected : function(component, event, helper) {
        var rows = component.get("v.assemblyMRInfoList");
        var tempAssmblyMRInfoList = [];
        rows.forEach(function(item){
            if(!item.flag){
                tempAssmblyMRInfoList.push(item);
            }
        })
        component.set("v.assemblyMRInfoList", tempAssmblyMRInfoList);
        helper.calculateTotalAmount(component, event, helper);
    },
    updateAmount : function(component, event, helper) {
        var rowIndex = event.getSource().get("v.class").split('-')[1];
        var recordList = component.get("v.assemblyMRInfoList");
        var record = recordList[rowIndex]; 
        var utilizationValue = 0;
        var rateValue = 0;
        if(record.utilization != null && record.utilization != undefined && record.utilization != ''){
            utilizationValue = record.utilization;            
        }
        if(record.rate != null && record.rate != undefined && record.rate != ''){
            rateValue = record.rate;            
        }
        record.amount = utilizationValue * rateValue;
        recordList[rowIndex] = record;
        component.set("v.assemblyMRInfoList", recordList);
        helper.calculateTotalAmount(component, event, helper);
    },
    updateTotalAmount : function(component, event, helper) {
        helper.calculateTotalAmount(component, event, helper);
    },
    handleSaveEvent : function(component, event, helper) {
        
        var invoiceObj = component.get("v.invoiceObj");

		//validate required fields.        
        if($A.util.isEmpty(invoiceObj.Lease__c) || $A.util.isEmpty(invoiceObj.Invoice_Date__c)){
            helper.showToast("Error","error","Please fill required fields.");
            return;
        }    
        
        //validate amount.    
        var totalAmount = component.get("v.totalAmount");
        	
        var assemblyMRInfoList = component.get('v.assemblyMRInfoList');
        var isAmountBlank = false;
		var requiredFieldMissing = false;
        
        assemblyMRInfoList.forEach(function(item){
            
            if(!requiredFieldMissing && $A.util.isEmpty(item.MRInfoRecordId)){
                requiredFieldMissing = true;
            }
            
            if(!isAmountBlank && ($A.util.isEmpty(item.amount) || item.amount == 0)){
                isAmountBlank = true;
            }
        });
        
        if(requiredFieldMissing){
            helper.showToast("Error","error","Please select Supplemental Rent.");
            return;
        }else{
            
            if(isAmountBlank){
                var modal = component.find("confirmationModal");
                $A.util.removeClass(modal, 'slds-hide');
            }else{
                helper.saveInvoice(component, event, helper);
            }
        }
    },
    
    closeConfirmationModal : function(component, event, helper) {
    	helper.closeModal(component, event, helper);
    },
    
    proceedToSave : function(component, event, helper) {
        helper.closeModal(component, event, helper);
   		helper.saveInvoice(component, event, helper);
    },
    
    addRow : function(component, event, helper) {
        var mrInfoList = component.get("v.assemblyMRInfoList");
        var obj = new Object();
        obj.isNew = true;
        mrInfoList.push(obj);
        component.set("v.assemblyMRInfoList",mrInfoList);
    },
    
    MRInfoSelected : function(component, event, helper){
        
        var selectedId = event.getSource().get("v.value");
        var rowIndex = event.getSource().get("v.class").split('-')[1];
        var recordList = component.get("v.assemblyMRInfoList");
        
        if(!$A.util.isEmpty(selectedId)){
            var selectedRecord = component.get("v.MRInfoMap").get(selectedId);
            //copy each field value otherwise it copies reference of the object.
            recordList[rowIndex].MRInfoRecordId = selectedRecord.MRInfoRecordId;
            recordList[rowIndex].name = selectedRecord.name;
            recordList[rowIndex].eventType = selectedRecord.eventType;
            recordList[rowIndex].serialNo = selectedRecord.serialNo ;
            recordList[rowIndex].rateBasis = selectedRecord.rateBasis ;
            recordList[rowIndex].rate = selectedRecord.rate ;
            recordList[rowIndex].utilization = selectedRecord.utilization ;
            recordList[rowIndex].amount = selectedRecord.amount ;
            recordList[rowIndex].comments = selectedRecord.comments ;
            recordList[rowIndex].flag = selectedRecord.flag ;
            recordList[rowIndex].assemblyType = selectedRecord.assemblyType ;
            component.set("v.assemblyMRInfoList",recordList);
        }else{
            var obj = new Object();
            obj.isNew = true;
            recordList[rowIndex] = obj;
            component.set("v.assemblyMRInfoList",recordList);
            helper.calculateTotalAmount(component, event, helper);
        }
	}
    
})