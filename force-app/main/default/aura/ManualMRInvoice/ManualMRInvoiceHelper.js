({
    closeModal : function(component, event, helper){
        var modal = component.find("confirmationModal");
        $A.util.addClass(modal, 'slds-hide');
    },
    
    saveInvoice : function(component, event, helper){
        
        var invoiceObj = component.get("v.invoiceObj");
        
        component.set("v.showSpinner", true);
        
        // call the apex class method 
        let action = component.get("c.saveInvoice"); 
        var MRInfoList = component.get('v.assemblyMRInfoList');
        
        MRInfoList.forEach(function(item){
            if($A.util.isEmpty(item.utilization)){
                item.utilization = 0;
            }
            if($A.util.isEmpty(item.rate)){
                item.rate = 0;
            }
            if($A.util.isEmpty(item.amount)){
                item.amount = 0;
            }
        });
        
        action.setParams({'invoiceObj': invoiceObj, 'recordTypeDeveloperName': component.get('v.recordType'),
                          'assemblyMRInfoList' : JSON.stringify(MRInfoList)});
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.goToInvoice(response.getReturnValue().Id);
            }else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            component.set("v.showSpinner", false);
        });
        // enqueue the Action  
        $A.enqueueAction(action); 
    },
    
    showToast : function(title,type, msg){
        let toastParams = {
            title: title,
            message: msg, 
            type: type
        };
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },
    
    goToInvoice : function (recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    calculateTotalAmount : function(component, event, helper) {
        var recordList = component.get("v.assemblyMRInfoList");
        var totalAmount = 0;
        recordList.forEach(function(record){
            if(record.amount != null && record.amount != undefined && record.amount != ''){
                totalAmount = totalAmount + Number(record.amount);
            }
        })
        component.set("v.totalAmount", totalAmount);
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    },
    populateMRInfo :  function(component, leaseId) {
        
        component.set("v.showSpinner", true);
        // call the apex class method 
        let action = component.get("c.getAssemblyMRInfo");
        // set param to method  
        action.setParams({'leaseId': leaseId});
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var result = response.getReturnValue();
                component.set("v.assemblyMRInfoList", result);
                
                //create map to get record info for the newly added rows.
                if(result != undefined){
                    var MRInfoMap = new Map();
                    var allMRInfo = [];
                    result.forEach(function(item){
                        MRInfoMap.set(item.MRInfoRecordId,item);
                        allMRInfo.push({name : item.name, id : item.MRInfoRecordId});
                    });
  					component.set("v.allMRInfo",allMRInfo);
                    component.set("v.MRInfoMap",MRInfoMap);
                }
                
                
                /*if(result != undefined){
                    var MRInfoList = new Array();
                    for(var key in result){
                    	MRInfoList.push(result[key]);
                    }
                    component.set("v.assemblyMRInfoList", MRInfoList);
                    component.set("v.MRInfoMap",result);
                }    */            
                //component.set("v.MRInfoOptionList", response.getReturnValue());
                

            }else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            component.set("v.showSpinner", false);
        });
        // enqueue the Action  
        $A.enqueueAction(action); 
    }
})