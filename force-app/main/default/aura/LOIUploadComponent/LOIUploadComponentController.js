({
    doinit : function(component, event, helper){
        helper.init(component, event, helper);
    },
    handleLoad : function(component, event, helper){
        var fieldValue = component.get('v.recordId');
        component.find('input_field').set('v.value', fieldValue);
    },
    handleSubmit : function(component, event, helper){  
        event.preventDefault(); // stop form submission
        var eventFields = event.getParam("fields");
        eventFields["Y_Hidden_Department__c"] = component.get('v.Department');
        component.find('myform').submit(eventFields);
    },
    handleUploadFinished : function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        console.log('Uploaded Files: ',uploadedFiles);
        component.set('v.showForm', true);
        component.set('v.uploadedFilesList',uploadedFiles); 
    },
    handleUploadRow: function(component, event, helper) {        
        var uploadedFiles = event.getParam("files");
        console.log('Uploaded Files: ',uploadedFiles);
        var documentId = uploadedFiles[0].documentId;
        var action = component.get('c.updateContentVersion');
        action.setParams({ 
            uploadId: uploadedFiles[0].documentId,
            recordId : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Id: ',response.getReturnValue());
                component.set('v.documentLinkParent',response.getReturnValue());
                component.set('v.showCommentModal', true);
                window.setTimeout(
                    $A.getCallback(function() { 
                        helper.init(component,helper, event);
                    }), 1000
                )
            }
            else if(state == "ERROR"){
                var errors = response.getError();
                console.log('Errors: ',errors);
                if(errors[0].message.includes("INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY")){
                    helper.handleErrors('You do not have permission to upload a revision on this file. Please contact your system administrator.');
                    helper.deleteUploadFile(component, event, helper,documentId);
                }
                else{
                    helper.handleErrors(errors);
                }
            } 
            
        });
        $A.enqueueAction(action)
    },
    handleSuccess : function(component, event, helper) {
        var payload = event.getParams().response;
        console.log('Id: ',payload.id);
        component.set('v.showForm', false);
        var uploadDocumentList = component.get('v.uploadedFilesList');
        var uploadDoumentIds = [];
        console.log(uploadDocumentList);	
        if(uploadDocumentList != null && uploadDocumentList != undefined && uploadDocumentList.length > 0){
            for(var documentIndex in uploadDocumentList){
                uploadDoumentIds.push(uploadDocumentList[documentIndex].documentId);
            }  
        }
        
        var action = component.get("c.saveLoiRecord");
        console.log(payload.id);
        console.log(uploadDoumentIds);
        action.setParams({ 
            loi: payload.id,
            uploadList: uploadDoumentIds
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('LOI record saved successfully');
                window.setTimeout(
                    $A.getCallback(function() {
                        helper.init(component,helper, event);
                    }), 1000
                )	
            }
            else if(state == "ERROR"){
                var errors = response.getError();
                console.log('Errors: ',errors);
                helper.handleErrors(errors);
            } 
        });
        $A.enqueueAction(action)
    },
    showAllLoi : function(component,event, helper){
        component.set('v.showAllLoi',true);
        component.set('v.showView',false);
        helper.init(component,helper, event);
    },
    closeModel : function(component, event, helper){
        component.set('v.showForm', false);
        component.set('v.showCommentModal', false);
        console.log('Value: ',component.get('v.comments'));
        helper.saveContentVersionComment(component, event, helper);
    },
    closeLoiModel : function(component, event, helper){
        component.set('v.showForm', false);
        component.set('v.showCommentModal', false);
        var contentDocumentId = component.get('v.uploadedFilesList');
        console.log('documentId:',contentDocumentId);
        helper.deleteUploadFile(component, event, helper,contentDocumentId[0].documentId);
    },
    handleMenuSelect: function(component, event, helper) {
        console.log('handleMenuSelect function called');
        var menuValue = event.detail.menuItem.get('v.accesskey');
        console.log('menuValue = '+menuValue);
        switch(menuValue) {
            case "1": helper.doEdit(component, event, helper); break;
            case "2": helper.doDelete(component, event, helper); break;
        }
    },
    saveComment : function(component, event, helper){
        console.log('Value: ',component.get('v.comments'));
        helper.saveContentVersionComment(component, event, helper);
    }
    
})