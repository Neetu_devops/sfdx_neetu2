({
    init : function(component,event,helper){
        component.set("v.showSpinner", true);
        console.log('refresh');
        var action1 = component.get("c.getLoiRevisionData");
        action1.setParams({ 
            recordId: component.get('v.recordId'),
            showAllLoi : component.get('v.showAllLoi')
        });
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                if(result.error){
                    component.set('v.haserror',true);
                    component.set("v.showSpinner", false);
                }
                else{
                    console.log(result.scdw);
                    component.set('v.totalDisplayRecords',result.totalDisplayRecords);
                    component.set('v.libraryId', result.recordID);
                    component.set('v.totalrecords', result.totalrecords);
                    component.set('v.Department', result.department);
                    component.set("v.showSpinner", false);
                    console.log('Viewer',result.isViewer);
                    if(result.isViewer){
                        component.set('v.isViewer', false);
                    }
                    for(let key in result.scdw){
                        console.log('Loop Result',result.scdw[key].cvData);
                        if(result.scdw[key].cvData!=null && result.scdw[key].cvData!=undefined && result.scdw[key].cvData!=''){
                            result.scdw[key].lastModifiedDate = $A.localizationService.formatDate(result.scdw[key].cvData.LastModifiedDate) +'   '+ $A.localizationService.formatTime(result.scdw[key].cvData.LastModifiedDate);
                            console.log($A.localizationService.formatDate(result.scdw[key].cvData.LastModifiedDate));
                        }
                    }
                    component.set('v.wrapperDataList', result.scdw);
                    console.log('Result:', result);	
                }
            }
            else if (state === "ERROR") {
                component.set("v.showSpinner", false);
                var errors = response.getError();
                console.log('Errors: ',errors);
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action1);
    },
    
    changeuploadtext: function(component, event, helper){
        document.getElementById("abc").innerHTML = 'Drop New Files Here';
    },
    doEdit: function(component, event, helper) {
        console.log('Do edit called');
        var selectedMenuItemValue = event.getParam('value');
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            recordId: selectedMenuItemValue
        });
        editRecordEvent.fire();
        helper.init(component, event);
    },
    doDelete: function(component,event,helper) {
        console.log('Do delete called');
        var selectedMenuItemValue = event.getParam('value');
        console.log(selectedMenuItemValue);
        var loiId = selectedMenuItemValue;
        var action = component.get('c.deleteLoiById');
        console.log(action);
        action.setParams({
            loi : loiId,
        });
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS'){
                console.log('Record Deleted Successfully');
                window.setTimeout(
                    $A.getCallback(function() {
                        helper.init(component, event);
                    }), 1000
                )
            }
            else if(state == "ERROR"){
                var errors = response.getError();
                console.log('Errors: ',errors);
                helper.handleErrors(errors);
            } 
        });
        $A.enqueueAction(action);
    },
    saveContentVersionComment :  function(component,event,helper) {
      var action = component.get("c.saveComments");
        action.setParams({ 
            loi: component.get('v.documentLinkParent'),
            comment: component.get('v.comments')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Comment Updated Successfully');
                component.set('v.showCommentModal', false);
                component.set('v.comments','');
                window.setTimeout(
                    $A.getCallback(function() {
                        helper.init(component,helper, event);
                    }), 1000
                )
            }
            else if(state == "ERROR"){
                var errors = response.getError();
                console.log('Errors: ',errors);
                helper.handleErrors(errors);
            } 
        });
        $A.enqueueAction(action);  
    },
    handleErrors : function(errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error": type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error": type,
            duration: '7000'
        };
        if(typeof errors === 'object') {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });  
                    };
                }
            });
        }
        else if(typeof errors === "string" && errors.length != 0){
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    },
    deleteUploadFile: function(component,event,helper,documentId){
    var action = component.get("c.deleteUploadedRecord");
        action.setParams({ 
            uploadId: documentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Content Document deleted successfully');	
            }
            else if(state == "ERROR"){
                var errors = response.getError();
                console.log('Errors: ',errors);
                helper.handleErrors(errors);
            } 
        });
        $A.enqueueAction(action)
    }
})