({
    doInit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set("v.reloadTable", false);
        
        var objectName = 'Global_Fleet__c';        
        var action = component.get("c.getResultColumns");
        action.setParams({
            'objectName' : objectName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                component.set('v.columns', response.getReturnValue());   
                component.set("v.showTable", false);
                component.set("v.tableData", []);
                component.set("v.selectedRows", []);
            }
            else {
                helper.handleErrors(response.getError());
            }
            
            component.set('v.showSpinner', false);
        });
        
        $A.enqueueAction(action);      
    },
    
    
    handleSubmit : function(component, event, helper) {
        component.set("v.showSpinner", true);
        component.set("v.reloadTable", false); 
        
        var controllingField = component.get("v.controllingSelectedVal"); 
        var dependentEField = component.get("v.selectedEDependentValue");
        var dependentField = component.get("v.selectedDependentValue"); 
        
        if(controllingField == undefined || controllingField.indexOf('None') != -1) {
            component.set("v.showSpinner", false);
            
            helper.showMessage("Please complete the required fields on the form.", "error");                
        }
        else {
            var domYear = component.get("v.domYear");
            var domLength;
            
            if(domYear != undefined) {
                domLength= domYear.toString().length;
            }
            
            if(domLength != undefined && domLength != '' && domLength != 4) {
                component.set("v.showSpinner", false);
                
                helper.showMessage("Please enter a valid year for Year of Manufacture.", "error");                   
            }
            else {
                var worldFleetRecord = component.get("v.worldFleetRecord");
                worldFleetRecord.AircraftType__c = controllingField;
                
                if(dependentField.indexOf('None') != -1) {
                    worldFleetRecord.AircraftVariant__c = '';
                }
                else {
                    worldFleetRecord.AircraftVariant__c = dependentField;
                }
                
                if(dependentEField.indexOf('None') != -1) {
                    worldFleetRecord.EngineType__c = '';
                }
                else {
                    worldFleetRecord.EngineType__c = dependentEField;
                }
                
                component.set("v.worldFleetRecord", worldFleetRecord);
                
                helper.searchWorldFleets(component, event);
            }            
        }
    },
    
    
    addSelected : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.addSelectedAssets(component);
    }
})