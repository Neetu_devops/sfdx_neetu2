({
    myAction: function (component, event, helper) {
    },
    initialize: function (component, event, helper) {
        console.log("CanvassingExampleController initialize(+)==>");
        var today = new Date();
        //component.set("v.dealDate",todayDate);
        component.set('v.dealDate', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
        var opts = [{
            class: "optionClass",
            label: "MSN",
            value: "MSN",
            selected: "true"
        }, {
            class: "optionClass",
            label: "World Fleet",
            value: "World_Fleet"
        }];
        console.log('component.get("v.showWorldFleet")', component.get("v.showWorldFleet"));
        if (component.get("v.transRecType") === 'Acquisition' && component.get("v.showWorldFleet") === 'YES') {
            console.log('transaction type = Acquisition & showWorldFleet = yes');
            component.find("InputSelectMsnATId").set("v.options", opts);
            console.log(component.find("InputSelectMsnATId").get("v.options"), opts);
        }
        // fetch NameSpacePrefix
        helper.fetchNameSpacePrefix(component);

        console.log("CanvassingExampleController initialize(-)==>");
    },
    onSelectChangeMsnATId: function (component, event, helper) {
        console.log("CanvassingExampleController.onSelectChangeMsnATId(+)==>");
        var selected = component.find("InputSelectMsnATId").get("v.value");
        console.log("selected==>" + selected);
        component.set("v.msnATId", selected);
        var stringIdArray = [];
        for (var i = 0; i < 10; i++) {
            stringIdArray[i] = null;
        }

        component.set('v.MSNNameText', stringIdArray);
        component.set('v.msnWFIdArray', stringIdArray);

        console.log("CanvassingExampleController.onSelectChangeMsnATId(-)==>");
    },
    callSave: function (component, event, helper) {
        console.log("AddmultipleContacts callSave(+)==>");
        var errorMessage = "";
        var emptyString1 = ",";

        //var dealDateVar = component.get('v.dealDate');
        //console.log("dealDateVar="+dealDateVar);  

        //1
        var transactionNameVar = component.get('v.TransactionName');
        console.log("transactionNameVar=" + transactionNameVar);

        //2
        var transactionTypeVar = component.get('v.transRecType');
        console.log("transactionTypeVar=" + transactionTypeVar);

        //3
        var transactionStatusVar = component.get('v.TransactionStatus');
        console.log("transactionStatusVar=" + transactionStatusVar);

        //4
        var acquisitionTypeVar = component.get('v.AcquisitionType');
        console.log("acquisitionTypeVar=" + acquisitionTypeVar);

        //5
        var sellerTypeVar = component.get('v.SellerType');
        console.log("sellerTypeVar=" + sellerTypeVar);

        //5
        var OtherSellerTypeVar = component.get('v.OthersSellerType');
        console.log("Other Seller typeVar=" + OtherSellerTypeVar);

        //6
        var msnATIdVar = component.get('v.msnATId');
        console.log("msnATId TextVar=" + msnATIdVar);

        //6.1
        var MSNNameString = component.get("v.MSNNameString");
        var MSNNameTextVar = component.get('v.MSNNameText');
        if (MSNNameString != undefined && MSNNameString != null && MSNNameString != '') {
            MSNNameTextVar = MSNNameString.split(',');
        }
        console.log("MSN Name TextVar=" + MSNNameTextVar);

        //6.2
        var msnWFArray = component.get('v.msnWFArray');
        console.log("msnWFArray: ", msnWFArray);
        var msnWFIdArrayVar = component.get('v.msnWFIdArray');
        var msnWFNameArrayVar = component.get('v.msnWFNameArray');
        for (var key in msnWFArray) {
            if (msnWFArray[key] != undefined) {
                msnWFIdArrayVar.push(msnWFArray[key].Id);
                msnWFNameArrayVar.push(msnWFArray[key].Name);
            }
        }
        component.set("v.msnWFIdArray", msnWFIdArrayVar);
        component.set("v.msnWFNameArray", msnWFNameArrayVar);
        console.log("msnWFIdArrayVar=" + msnWFIdArrayVar);
        console.log("msnWFNameArrayVar=" + msnWFNameArrayVar);

        //7
        var AircraftTypeVar = component.get('v.AircraftTypeforAIT');
        console.log("AircraftTypeVar=" + AircraftTypeVar);

        //8
        var VarientVar = component.get('v.VariantforAIT');
        console.log("VarientVar=" + VarientVar);

        //9
        var EngineVar = component.get('v.EngineforAIT');
        console.log("EngineVar=" + EngineVar);

        //10
        var EngineVariantVar = component.get('v.EngineVariantForAIT');
        console.log("EngineVariantVar=" + EngineVariantVar);

        var customerArray = component.get('v.customerArray');
        console.log("customerArray: ", customerArray);
        var customeridArrayVar = component.get('v.customerIdArray');
        var customerNameArrayVar = component.get('v.customerNameArray');

        if (transactionTypeVar === 'Acquisition' && sellerTypeVar === 'Airline') {
            if (customerArray[0] != undefined) {
                customeridArrayVar.push(customerArray[0].Id);
                customerNameArrayVar.push(customerArray[0].Name);
            }
        }
        else if (transactionTypeVar === 'Disposal') {
            for (var key in customerArray) {
                if (customerArray[key] != undefined && !customeridArrayVar.includes(customerArray[key].Id)) {
                    customeridArrayVar.push(customerArray[key].Id);
                    customerNameArrayVar.push(customerArray[key].Name);
                }
            }
        }
        component.set("v.customerIdArray", customeridArrayVar);
        component.set("v.customerNameArray", customerNameArrayVar);
        console.log("customeridArrayVar=" + customeridArrayVar);
        console.log("customerNameArrayVar=" + customerNameArrayVar);

        var lessorArray = component.get('v.lessorArray');
        console.log("lessorArray: ", lessorArray);
        var lessorIdArrayVar = component.get('v.lessorIdArray');
        var lessorNameArrayVar = component.get('v.lessorNameArray');

        if (transactionTypeVar === 'Acquisition' && sellerTypeVar === 'Lessor') {
            if (lessorArray[0] != undefined) {
                lessorIdArrayVar.push(lessorArray[0].Id);
                lessorNameArrayVar.push(lessorArray[0].Name);
            }
        }
        else if (transactionTypeVar === 'Disposal') {
            for (var key in lessorArray) {
                if (lessorArray[key] != undefined && !lessorIdArrayVar.includes(lessorArray[key].Id)) {
                    lessorIdArrayVar.push(lessorArray[key].Id);
                    lessorNameArrayVar.push(lessorArray[key].Name);
                }
            }
        }
        component.set("v.lessorIdArray", lessorIdArrayVar);
        component.set("v.lessorNameArray", lessorNameArrayVar);
        console.log("lessorIdArrayVar=" + lessorIdArrayVar);
        console.log("lessorNameArrayVar=" + lessorNameArrayVar);

        var msnArray = component.get('v.msnArray');
        console.log("msnArray: ", msnArray);
        var msnIdArrayVar = component.get('v.msnIdArray');
        var msnNameArrayVar = component.get('v.msnNameArray');
        for (var key in msnArray) {
            if (msnArray[key] != undefined) {
                if (!msnIdArrayVar.includes(msnArray[key].Id)) {
                    msnIdArrayVar.push(msnArray[key].Id);
                    msnNameArrayVar.push(msnArray[key].Name);
                }
            }
        }
        component.set("v.msnIdArray", msnIdArrayVar);
        component.set("v.msnNameArray", msnNameArrayVar);
        console.log("msnIdArrayVar=" + msnIdArrayVar);
        console.log("msnNameArrayVar=" + msnNameArrayVar);

        if (transactionNameVar == null || transactionNameVar == "") errorMessage += 'Transaction Name is a required field.\n';
        if (transactionTypeVar == 'Acquisition' && sellerTypeVar == 'Airline' && (customeridArrayVar[0] == null || customeridArrayVar[0] == "")) errorMessage += 'Please select the appropriate seller for the selected Seller Type (Airline).\n';
        if (transactionTypeVar == 'Acquisition' && sellerTypeVar == 'Lessor' && (lessorIdArrayVar[0] == null || lessorIdArrayVar[0] == "")) errorMessage += 'Please select the appropriate seller for the selected Seller Type (Lessor).\n';
        if (transactionTypeVar == 'Acquisition' && sellerTypeVar == 'Other' && (OtherSellerTypeVar[0] == null || OtherSellerTypeVar[0] == "")) errorMessage += 'Please select the appropriate seller for the selected Seller Type (Other).\n';

        if ((MSNNameTextVar[0] == null || MSNNameTextVar[0] == "") && (msnATIdVar == "MSN") && (transactionTypeVar == 'Acquisition')) errorMessage += 'MSN is a required field.\n';
        if ((msnATIdVar == "World_Fleet") && (msnWFIdArrayVar[0] == null || msnWFIdArrayVar[0] == "") && (transactionTypeVar == 'Acquisition')) errorMessage += 'World Fleet Assignment is a required field.\n';
        if ((customeridArrayVar[0] == null) && (lessorIdArrayVar[0] == null) && (transactionTypeVar == 'Disposal')) errorMessage += 'Please enter either Airline or Lessor.\n';

        if (errorMessage != null && errorMessage != "") {
            helper.throwErrorForKicks(component, errorMessage);
        } else {
            //{
            // Save it!
            var canvasIdVar;
            var action = component.get("c.callSaveApexTrans");
            action.setParams({
                transactionName_p: transactionNameVar,
                transactionType_p: transactionTypeVar,
                transactionStatus_p: transactionStatusVar,
                acquisitionType_p: acquisitionTypeVar,
                sellerType_p: sellerTypeVar,
                OtherSellerType_p: OtherSellerTypeVar,
                msnATId_p: msnATIdVar,
                MSNNameText_p: MSNNameTextVar,
                msnWFIdArray_p: msnWFIdArrayVar,
                msnWFNameArray_p: msnWFNameArrayVar,
                AircraftType_p: AircraftTypeVar,
                Varient_p: VarientVar,
                Engine_p: EngineVar,
                EngineVariant_p: EngineVariantVar,
                msnArray_p: msnIdArrayVar,
                msnNameArray_p: msnNameArrayVar,
                customeridArray_p: customeridArrayVar,
                customerNameArray_p: customerNameArrayVar,
                lessorIdArray_p: lessorIdArrayVar,
                lessorNameArray_p: lessorNameArrayVar
            });
            action.setCallback(this, function (a) {
                if (a.error && a.error.length) {
                    //return $A.error('Unexpected error callSave: '+a.error[0].message);
                    return new Error('Unexpected error callSave: ' + a.error[0].message);
                }
                console.log("success");
                var canvasMap = a.getReturnValue();
                console.log("canvasIdVar=", canvasMap);
                canvasIdVar = canvasMap['ID'];
                console.log("canvasIdVar=" + canvasIdVar);
                if (canvasMap["ERRORID"] == "1") {
                    helper.throwErrorForKicks(component, canvasMap["ERROR"]);
                } else {
                    var errorDiv = component.find("errorDiv1");
                    $A.util.addClass(errorDiv, "slds-hide");
                    //alert("Records created successfully. Canvas Report Name:"+canvasIdVar);
                    helper.helperCloseMethod(component, canvasIdVar);
                }
            });
            $A.enqueueAction(action);
            //}
        }
        console.log("AddmultipleContacts callSave(-)==>");
    },
    callCancel: function (component, event, helper) {
        console.log("callCancel======================>");
        helper.helperCloseMethod(component, null);
    },
    handleAssetTypeValueChange: function (component, event, helper) {
        console.log("handleAssetTypeValueChange======================>");
        helper.helperhandleAssetTypeValueChange(component);
    },
    openModel: function (component, event, helper) {
        let nameSpacePrefix = component.get("v.nameSpacePrefix");
        if (nameSpacePrefix == undefined || nameSpacePrefix == '' || nameSpacePrefix == null) { nameSpacePrefix = 'c'; }
        else { nameSpacePrefix = nameSpacePrefix.slice(0, -2); }

        let searchMSN = component.find("searchMSN");
        if (searchMSN) {
            searchMSN.destroy();
        }
        $A.createComponent(
            "" + nameSpacePrefix + ":MSNSearchandAdd",
            {
                "aura:id": "searchMSN",
                "proccessMSN": component.getReference("v.proccessMSN"),
                "selectedMSN": component.getReference("v.msnListArray"),
                "assetType": component.getReference("v.assetType")
            },
            function (elemt, status, errorMessage) {
                if (status === "SUCCESS") {
                    var targetCmp = component.find('searchMSNDiv');
                    var body = targetCmp.get("v.body");
                    body.push(elemt);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },
    handleMSNChange: function (component, event, helper) {
        var proccessMSN = component.get("v.proccessMSN");
        if (proccessMSN) {
            var msnArray = component.get("v.msnArray");
            var msnListArray = component.get("v.msnListArray");
            var msnIdArrayVar = component.get("v.msnIdArray");
            var msnNameArrayVar = component.get("v.msnNameArray");
            for (var key in msnListArray) {
                if (msnListArray[key] != undefined) {
                    if (msnArray.length === 0) {
                        msnIdArrayVar.push(msnListArray[key].Id);
                        msnNameArrayVar.push(msnListArray[key].Name);
                        msnArray.push(msnListArray[key]);
                    }
                    else {
                        var addValue = true;
                        for (var key2 in msnArray) {
                            if (msnArray[key2].Id === msnListArray[key].Id) {
                                addValue = false;
                            }
                        }
                        if (addValue) {
                            msnIdArrayVar.push(msnListArray[key].Id);
                            msnNameArrayVar.push(msnListArray[key].Name);
                            msnArray.push(msnListArray[key]);
                        }
                    }
                }
            }
            component.set("v.msnArray", msnArray);
            component.set("v.msnIdArray", msnIdArrayVar);
            component.set("v.msnNameArray", msnNameArrayVar);
            component.set("v.proccessMSN", false);
            var searchMSN = component.find("searchMSN");
            if (searchMSN) {
                searchMSN.destroy();
            }
        }
    },
})