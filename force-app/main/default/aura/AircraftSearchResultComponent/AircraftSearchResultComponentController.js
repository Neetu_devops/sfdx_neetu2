({
    init : function(cmp, event, helper) {
        console.log('inside init');
        console.log(cmp.get("v.aircraftType"));
        console.log(cmp.get("v.aircraftVariant"));
        console.log(cmp.get("v.engineType"));
        console.log(cmp.get("v.vintage"));
        console.log(cmp.get("v.vintageRange"));
        console.log(cmp.get("v.searchTransaction"));
        console.log(cmp.get("v.leaseDateTo"));
        console.log(cmp.get("v.leaseDateFrom"));
        
        var action = cmp.get("c.searchAircraft");
        action.setParams({'aircraftType':cmp.get("v.aircraftType"),
                          'aircraftVariant' : cmp.get("v.aircraftVariant"),
                          'engineType' : cmp.get("v.engineType"),
                          'engineVariant' : cmp.get("v.engineVariant"),
                          'vintage' : cmp.get("v.vintage"),
                          'vintageRange' : cmp.get("v.vintageRange"),
                          'searchTransaction' : cmp.get("v.searchTransaction"),
                          'leaseDateTo' : cmp.get("v.leaseDateTo"),
                          'leaseDateFrom' : cmp.get("v.leaseDateFrom")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                cmp.set("v.listOfAircrafts",result.listOfAircrafts);
                console.log(result.listOfAircrafts);
                cmp.set("v.listOfAircraftsInTransaction",result.listOfAircraftInTransactions);
                cmp.set("v.totalResults",(result.listOfAircrafts.length + result.listOfAircraftInTransactions.length));
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action); 
    },
    navigateToDetail : function(cmp,event){
        /* var evt = $A.get("e.c:AircraftSearchEvent");
        evt.setParams({'recId':$(event.target).closest('.box').attr('data-id'),
                       'objName' : $(event.target).closest('.box').attr('data-objName'),
                       'evtName' : 'showRecordDetail'
                      });
        evt.fire(); */
        
        var recId = $(event.target).closest('.box').attr('data-id');
        var context = cmp.get("v.userContext");
        console.log(context);
        if(context != undefined) {
            if(context == 'Theme4t' || context == 'Theme4d') {
                sforce.one.navigateToSObject(recId);
            } else {
                window.location.href="/"+recId;
            }
        } else {
            var event = $A.get("e.force:navigateToSObject");
            event.setParams({"recordId": recId});
            event.fire();
        }
        
    },
    goBack: function(cmp,event){
        var evt = $A.get("e.c:AircraftSearchEvent");
        evt.setParams({'evtName':'backToSearch',
                      });
        evt.fire();
    },
    goToLeasePage : function(cmp,event){
        event.stopPropagation();
        var leaseId = $(event.target).attr('data-leaseid');
        var context = cmp.get("v.userContext");
        console.log(context);
        if(context != undefined) {
            if(context == 'Theme4t' || context == 'Theme4d') {
                sforce.one.navigateToSObject(leaseId);
            } else {
                console.log(2);
                window.location.href="/"+leaseId;
            }
        } else {
            var event = $A.get("e.force:navigateToSObject");
            event.setParams({"recordId": leaseId});
            event.fire();
        }
    }
    
    
})