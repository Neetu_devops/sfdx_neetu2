({
    confirmDateUpdate : function(component, event, helper) {
        component.set("v.showModal", false);  
        component.set("v.confirmDateChange", true);  
	},
    
    
    closeSection : function(component, event, helper) {
        component.set("v.showModal", false); 
        component.set("v.confirmDateChange", false);  
    }
})