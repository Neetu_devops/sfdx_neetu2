({
    checkDivVisibility: function(component, data ){
        console.log("checkLoad1");
        var ele = component.find('barChart').getElement();
        var self = this;
        if(ele == null) {
            window.setTimeout(
                $A.getCallback(function() {
                    console.log("checkLoad timeout ");
                    self.createGraph(component, data); 
                }), 3000);  
        }
        else {
            self.createGraph(component, data);
        }
    },
    
    createGraph : function(cmp, data) {
        console.log("createGraph data "+data.chartData);
        var graphEle = cmp.find('barChart').getElement();
        console.log('createGraph graphEle '+graphEle);
        cmp.set("v.isLoading", false);
        if(graphEle == null) {
            cmp.set("v.showComponent" , false);
            return;
        }
        var ctx = graphEle.getContext('2d');
        var self = this;
        if(data.netAmount > 0)
        	cmp.set("v.net", "$"+data.netAmount + "k");
        else
            cmp.set("v.net", "0");
      	var newData = data.chartData;
        var bucket = ["0-15", "16-30", "31-60", "61-90", "91-120", "+120"];
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: bucket,
                datasets: [
                    {
                        label: "Amount Outstanding ($k)",
                        backgroundColor: "#1874CD",
                        data: newData
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            fontColor: "#1874CD"
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Days Outstanding",
                            fontColor: "#1874CD"
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            fontColor: "#1874CD",
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "($k)",
                            fontColor: "#1874CD"
                        }
                    }]
                },
                onClick: function(evt, activeElements) {
                    console.log("onClick "+activeElements +" evt "+activeElements[0]);
                    if(!$A.util.isEmpty(activeElements[0]) && !$A.util.isUndefined(activeElements[0])) {
                        var elementIndex = activeElements[0]._index;
                        console.log('Chart onclick elementIndex: '+elementIndex);
                        self.openReport(cmp,evt,elementIndex);
                        //this.update();
                    }
                },
            }
        });
        
        cmp.set("v.totalBalance", data.totalAmount + "k" );
    },
    
    getReport : function(component) {
        var action = component.get("c.getReport");
        action.setParams({
            reportName : component.get("v.reportName")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getReport : state "+state);
            if (state === "SUCCESS") {
                var report = response.getReturnValue();
                
                console.log("getReport "+report);
                console.log("getReport Name " + reportDisplayName);
                
                if(report != null)
                	component.set("v.reportId",report.Id);
                
                var reportDisplayName = component.get("v.reportDisplayName");
                if($A.util.isEmpty(reportDisplayName) || $A.util.isUndefined(reportDisplayName)) {
                    if(report != null)
                    	component.set("v.reportDisplayName",report.Name);
                }else{
                    component.set("v.reportDisplayName",reportDisplayName);
                }   
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getReport errors "+errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getReport Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
        });
        $A.enqueueAction(action);                
    },
  	
    getInvoiceReportId : function(component) {
        var reportName = "Leases_with_Invoices_Report_JtA";
        var action = component.get("c.getReport");
        action.setParams({
            reportName : reportName
        });
        action.setCallback(this, function(response) {
            var report = response.getReturnValue();
            console.log("getReportRecordId "+report);
            if(report != null)
            	component.set("v.invoiceReportId", report.Id);
        });
        $A.enqueueAction(action);                
    },
    
    openReport : function(component,event, index) {
        console.log("openReport "+index);
        var hostname = "https://"+window.location.hostname;
        console.log('report hostname '+hostname);
      
        var leaseId = component.get("v.recordId");
        var reportId =component.get("v.invoiceReportId");// "00O6F00000FXN1PUAX";
        console.log('openReport reportId: '+reportId);
        if(!$A.util.isEmpty(reportId) && !$A.util.isUndefined(reportId)){
            var baseUrl = hostname+'/lightning/r/Report/'+reportId+'/view';
            
            var param1 = "";
            var param2 = ""
            if(index == 0 ) {
                param1 = "";
                param2 = "15";
            }
            else if(index == 1 ) {
                param1 = "15";
                param2 = "30";
            }
            else if(index == 2 ) {
                param1 = "30";
                param2 = "60";
            }
            else if(index == 3 ) {
                param1 = "60";
                param2 = "90";
            }
			else if(index == 4 ) {
                param1 = "90";
                param2 = "120";
            }
            else if(index == 5 ) {
                param1 = "120";
                param2 = "";
            }
            
            var url = baseUrl + '?fv0='+leaseId+'&fv1='+param1 + '&fv2=' + param2;
            console.log('url '+url);
            window.open(url); 
            
            //  var reportId = 'https://leaseworks-traning-dev-ed.lightning.force.com/lightning/r/Report/00O6F00000FXMypUAH/view?fv0=a2M6F000007mvr9UAA';
        }
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
})