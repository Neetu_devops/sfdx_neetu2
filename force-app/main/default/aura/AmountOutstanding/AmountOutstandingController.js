({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        console.log("Record Id "+recordId);
        //helper.getOutstandingAmount(component);
        helper.getReport(component);
        helper.getInvoiceReportId(component);
    },
    
    drawChart : function(cmp, event, helper) {
        var temp = [];
        var recordId = cmp.get("v.recordId");
        var action = cmp.get("c.getAmount");
        action.setParams({recordId : recordId});
        action.setCallback(this, function(response){
            console.log("drawChart "+response.getReturnValue() + " "+response.getState());
            if(response.getState() === 'SUCCESS' && response.getReturnValue()){
                temp = response.getReturnValue();
                console.log("drawChart "+temp);
                cmp.set("v.amtList" , temp);
                if(!$A.util.isEmpty(temp) && !$A.util.isUndefined(temp) && temp != null) {
                    console.log("drawChart1 "+JSON.stringify(temp));
                    cmp.set("v.isLoading", true);
                    cmp.set("v.showComponent", true);
                    window.setTimeout(
                        $A.getCallback(function() {
                            helper.checkDivVisibility(cmp, temp);
                        }), 2000
                    );
                }
            }
        });    
        $A.enqueueAction(action);	
    },
    
    onclickReport : function(component, event, helper) {
        console.log("onclickInvoice");
        var reportId = component.get("v.reportId");
        console.log('onclickReport id ='+reportId);
        window.open('/' + reportId); 
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
    
})