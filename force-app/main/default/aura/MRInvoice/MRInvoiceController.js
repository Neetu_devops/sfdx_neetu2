({
    doInit : function(component, event, helper) {
         if(component.get("v.wrapperObj.record.Id") != undefined && component.get("v.wrapperObj.record.Id") != ''){
             if(component.get("v.wrapperObj.objectType") == 'Utilization_Report__c'){
                 component.set("v.invoiceObj.Utilization_Report__c",component.get("v.wrapperObj.record.Id"));
             }    
         } 
    },
    utilizationSelected : function(component, event, helper) {
   
        var val = event.getParam("recordByEvent");
        var utilizationId = val.Id.trim();
        component.set("v.invoiceObj.Utilization_Report__c",utilizationId);
    },
    
    handleSaveEvent : function(component, event, helper) {

        var invoiceObj = component.get("v.invoiceObj");
        
        if($A.util.isEmpty(invoiceObj.Utilization_Report__c) || $A.util.isEmpty(invoiceObj.Invoice_Date__c)){
            
            let toastParams = {
                    title: "Error",
                    message: "Please fill required fields.", // Default error message
                    type: "error"
                };
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
                return;
        }   
        
        component.set("v.showSpinner", true);
		// call the apex class method 
        let action = component.get("c.saveMRInvoice"); 
        
        action.setParams({'invoiceObj': invoiceObj, 'recordTypeDeveloperName': component.get('v.recordType')});
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": response.getReturnValue().Id,
                    "slideDevName": "detail"
                });
                navEvt.fire();
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
            component.set("v.showSpinner", false);
        });
        // enqueue the Action  
        $A.enqueueAction(action); 	
    }
})