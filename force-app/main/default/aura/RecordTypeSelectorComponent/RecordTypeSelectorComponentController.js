({
	doInit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        
        var action = component.get("c.getRecordTypes");
        action.setParams({
            'objectName' : component.get("v.objectName")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === "SUCCESS") {
                component.set("v.defaultRTId", response.getReturnValue().defaultRecordType);  
                component.set("v.recordTypes", response.getReturnValue().recordTypeList); 
                component.set("v.selectedRTId", response.getReturnValue().defaultRecordType);
                component.set('v.showSpinner', false);
            }
        });   
        
        $A.enqueueAction(action);               
    },
    
    recordTypeChange : function(component, event, helper) {
        var selRecordType;
        var rtButtons = document.getElementsByClassName('recordType');        
        for(var i=0; i<rtButtons.length; i++) {
            if(rtButtons[i].checked) {
                selRecordType = rtButtons[i].id;
            }
        }
        
        component.set("v.selectedRTId", selRecordType);
    }
})