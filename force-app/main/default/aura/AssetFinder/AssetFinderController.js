({
    doInit : function(component, event, helper) {
        component.set("v.showSpinner", true);
        
        var action = component.get("c.getDefaultValues");
        action.setParams({
            'aircraftTypeField' : component.get("v.aircraftTypField"),
            'engineTypeField' : component.get("v.engineTypeField"),
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {                   
            var state = response.getState();
            
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                
                component.set("v.dealModal", result.dealType);
                component.set("v.aircraftDefaultType", result.aircraftType);
                component.set("v.engineDefaultType", result.engineType);
                
                if((result.recordObjectName != undefined 
                   && result.recordObjectName.indexOf('Deal__c') == -1 
                   && result.recordObjectName.indexOf('Aircraft_In_Deal__c') == -1 
                   && result.recordObjectName.indexOf('Marketing_Activity__c') == -1 
                   && result.recordObjectName.indexOf('Aircraft_Proposal__c') == -1 
                   && result.recordObjectName.indexOf('Pricing_Run_New__c') == -1) 
                  || component.get("v.recordId") == undefined) {
                    component.set("v.standAloneVersion", true);
                }
                else {
                    component.set("v.standAloneVersion", false);
                }
                component.set("v.lessorFleet", true);
                component.set("v.worldFleet", false);
                
                if(component.get("v.defaultAssetClass") === ''){
                    if(result.recordTypeName != undefined 
                       && result.recordTypeName != '' 
                       && result.recordTypeName.indexOf('engine') != -1) {
                        component.set("v.engines", true);
                        component.set("v.aircraft", false);
                    }
                    else {
                        component.set("v.aircraft", true);
                        component.set("v.engines", false);
                    }
                }
                else if (component.get("v.defaultAssetClass") === 'Aircraft'){
                    component.set("v.aircraft", true);
                    component.set("v.engines", false);
                }
                else if (component.get("v.defaultAssetClass") === 'Engine'){
                    component.set("v.engines", true);
                    component.set("v.aircraft", false);
                }
                
                component.set("v.hideSearch", false);
                component.set("v.reloadTable", false);
            }
            else {
                helper.handleErrors(response.getError());
            }      
            
            component.set("v.showSpinner", false);    
        });
        
        $A.enqueueAction(action);        
    },
    
    
    setMSNType : function(component, event, helper) {
        var msnType = event.getSource().getLocalId();
        
        if(msnType == 'world') {
            component.set("v.worldFleet", true);
            component.set("v.lessorFleet", false);
        }
        else {
            component.set("v.lessorFleet", true);
            component.set("v.worldFleet", false);
        }
    },
    
    
    setAircraftType : function(component, event, helper) {
        var aircraftType = event.getSource().getLocalId();
        
        if(aircraftType == 'engines') {
            component.set("v.engines", true);
            component.set("v.aircraft", false);            
            component.set("v.lessorFleet", true);
            component.set("v.worldFleet", false);
        }
        else {
            component.set("v.aircraft", true);
            component.set("v.engines", false);
        }
    },
    
    
    showSearchSection : function(component, event, helper) {
        if(component.get("v.hideSearch")) {
            component.set("v.hideSearch", false);
        }
        else {
    		component.set("v.hideSearch", true);
        }
    },
    
    
    refreshAssetFinder : function(component, event, helper) {
        component.set("v.lessorFleet", false);
        component.set("v.worldFleet", false);
        component.set("v.aircraft", false);
        component.set("v.engines", false);
        component.refresh();     
    }
})