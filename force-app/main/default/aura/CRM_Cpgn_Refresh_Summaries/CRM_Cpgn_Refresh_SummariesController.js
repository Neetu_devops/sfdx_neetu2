({
	doInit: function(component, event, helper) {
        
    	var recordId = component.get("v.recordId");
        var action = component.get("c.refreshSummaries");
        action.setParams({
            "campaignId": recordId,
        }); 
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();

            if (response.getState() === "SUCCESS") {
               var div = component.find("message-div");
               $A.util.removeClass(div, "slds-hide");
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
})