({
    
    loadData:function(cmp,event,helper){

        //set Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title" : "Aircraft Asset Detail"});
        appEvent.fire();
       
        
        //read parameter from URL.
        var recordId = helper.getURLParameterValue().recordId;
        console.log('recordId ---->' + recordId);
        if(recordId != undefined){
        
            helper.showSpinner(cmp);
            
            var action = cmp.get("c.getData");
            action.setParams({"assetId" : recordId});
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var data =  response.getReturnValue();
                    console.log('Data:', data);
                    cmp.set("v.data", data);
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        helper.handleErrors(errors);
                    } else {
                        console.log("Unknown error");
                    }
                }
                helper.hideSpinner(cmp);
            });
            
            $A.enqueueAction(action);
        }  
    },
	getURLParameterValue: function() {
 
        var querystring = location.search.substr(1);
        var paramValue = {};
        querystring.split("&").forEach(function(part) {
            var param = part.split("=");
            paramValue[param[0]] = decodeURIComponent(param[1]);
        });
 
        console.log('PARAM VALUE --->' + paramValue);
        
        return paramValue;
    },
    
    showSpinner: function (component) {
        var spinner = component.find("spinner");
        console.log('spinner' + spinner);
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    handleErrors : function(errors) {
        
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    }
})