({ 
    
    doInit: function(cmp, event, helper) {
           
        window.setTimeout(
            $A.getCallback(function() {
                helper.loadData(cmp, event, helper);
            }), 200);
    },
    
    handleErrors : function(component, event, helper) {
        
        var errors = event.getParam("errors");
        helper.handleErrors(errors);
    },
    
   /* contactUs: function(cmp, event, helper) {
        
        //Create component dynamically        
        $A.createComponent(
            "c:Com_Cmty_Contact_Us",{},
            function(msgBox){                
                if (cmp.isValid()) {
                    var targetCmp = cmp.find('contact-us');
                    var body = targetCmp.get("v.body");
                    body.push(msgBox);
                    targetCmp.set("v.body", body); 
                }
            }
        );
    }*/
    
})