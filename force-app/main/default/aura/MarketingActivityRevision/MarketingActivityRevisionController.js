({
    doInit: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        var action = component.get("c.getFileRecords");
        action.setParams({
            "maractId": component.get("v.recordId")
        });

        // Configure response handler
        action.setCallback(this, function (response) {
            helper.hideSpinner(component, event, helper);
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();

                component.set("v.Marketing_Activity", res.mActivity);
                var versionNumber = res.vNumber;
                var staticLabel = 'Comments for Revision ' + versionNumber + ' of this Deal:';
                //console.log("staticLabel"+staticLabel);
                var cmt = component.find("verCmt");
                cmt.set("v.label", staticLabel);

                component.set("v.hideFilesSection", res.hideFilesSection);
                component.set("v.listOfFiles", res.mActivityRevisionList);
            } else {
                var errors = response.getError();
                helper.handleErrors(component, errors);
            }
        });
        $A.enqueueAction(action);
    },

    handleSaveArchive: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        var cmt = component.get("v.comments");
        var saveArchiveAction = component.get("c.saveArchive");
        if (cmt != undefined && cmt != '') {
            saveArchiveAction.setParams({
                "recodID": component.get("v.recordId"),
                "VerCmnt": cmt,
                "listFiles": JSON.stringify(component.get("v.listOfFiles"))
            });

            saveArchiveAction.setCallback(this, function (response) {
                helper.hideSpinner(component, event, helper);
                var state = response.getState();
                console.log(state);
                if (state === "SUCCESS") {
                    console.log('Success');
                    $A.get('e.force:refreshView').fire();
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "Title": "Record Archived",
                        "type": "success",
                        "message": "Success: New Deal Revision created"
                    });

                    // Update the UI: close panel, show toast, refresh Marketing_Activity__c page
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();

                } else {
                    var errors = response.getError();
                    helper.handleErrors(component, errors);
                }
            });

            // Send the request to create the new contact
            $A.enqueueAction(saveArchiveAction);
        }
        else {
            helper.hideSpinner(component, event, helper);
        }
    },

    handleCancel: function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

})