({
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    handleErrors: function (cmp, errors) {
        // Configure error toast
        let errorVal = '';
        let errorPopup = cmp.find("errorM");
        $A.util.removeClass(errorPopup, "slds-hide");
        /*let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };*/
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    errorVal = error.message;
                    /*let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();*/
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        errorVal = pageError.message;
                        /*let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();*/
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            errorVal = errorList.message;
                            /*let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();*/
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });
                    };
                }
            });
        }

        cmp.set("v.errorMsg", errorVal);
    },
})