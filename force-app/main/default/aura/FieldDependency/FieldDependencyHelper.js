({
    fetchPicklistValues: function(component,event, helper) {
        // call the server side function  
        var action = component.get("c.getPicklistValues");
        // pass paramerters [object API Name , Dependent Field API ,Record Type Name] -
        // to server side function 
        action.setParams({
            'objectName' : component.get("v.objectAPI"),
            'fieldName': component.get("v.dependentFieldAPI"),
            'recordTypeName': component.get("v.recordTypeName") 
        });
        //set callback   
        action.setCallback(this, function(response) {                    
            if(response.getState() == "SUCCESS") {                
                //store the returned response from server (Map<String,List<String>>)  
                var res = response.getReturnValue();
                
                // once set #res to mapDependentField attribute 
                if(component.get("v.dependentFieldAPI") == 'Engine_Type__c'){
                    component.set("v.mapEDependentField",res);
                    console.log('Engine DOINIT>>>',component.get("v.mapEDependentField"));
                }else{
                    component.set("v.mapDependentField",res);
                }
                
                
                // create a empty array for store map keys(@@--->which is controller picklist values) 
                var listOfkeys = []; // for store all map keys (controller picklist values)
                var ControllerField = []; // for store controller picklist value to set on lightning:select. 
                
                // play a for loop on Return map 
                // and fill the all map key on listOfkeys variable.
                for (var singlekey in res) {
                    listOfkeys.push(singlekey);
                }
                
                //set the controller field value for lightning:select
                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push('--- None ---');
                }
                
                for (var i = 0; i < listOfkeys.length; i++) {
                    ControllerField.push(listOfkeys[i]);
                }  
                // set the ControllerField variable values to country(controller picklist field)
                component.set("v.listControllingValues", ControllerField); 
                
                if(component.get("v.selectedControllingValue") != undefined) {
                    this.onControllingFieldChange(component, event, helper);
                }                
            }
            else {
                console.log('response.getState() == ERROR fetchPicklistValues');
            }                        
        });
        
        $A.enqueueAction(action);
    },
    
    fetchDependentValues: function(component, ListOfDependentFields,isEngineType) {
        // create a empty array var for store dependent picklist values for controller field 
        if(isEngineType == true){
            var dependentFields = [];
            dependentFields.push('--- None ---');
            for (var i = 0; i < ListOfDependentFields.length; i++) {
                dependentFields.push(ListOfDependentFields[i]);
            }
            // set the dependentFields variable values to store(dependent picklist field) on lightning:select
            component.set("v.listEDependingValues", dependentFields);
            component.set("v.selectedEDependentValue", '');            
        } 
        else {
            var dependentFields = [];
            dependentFields.push('--- None ---');
            
            for (var i = 0; i < ListOfDependentFields.length; i++) {
                dependentFields.push(ListOfDependentFields[i]);
            }
            
            component.set("v.listDependingValues", dependentFields);
            component.set("v.selectedDependentValue",'');                
        }   
    },
    
    onControllingFieldChange: function(component, event, helper) {
        var controllerValueKey = component.get("v.selectedControllingValue");;
        var mControllingField =  component.get("v.selectedMultiselectValue");
        if(mControllingField != undefined && mControllingField != ''){
            controllerValueKey = mControllingField;
            console.log('controllerValueKey>>',controllerValueKey);
        }
        
        component.set("v.selectedControllingValue", controllerValueKey);
        let mapDependentField = component.get("v.mapDependentField");
        let mapEDependentField = component.get("v.mapEDependentField");      

        if(mapEDependentField != undefined) {
            console.log("Engine>>>", mapEDependentField);
            if(controllerValueKey != '--- None ---' && controllerValueKey != 'None' &&  controllerValueKey != undefined) {
                var ListOfEDependentFields = mapEDependentField[controllerValueKey];
                if(ListOfEDependentFields != undefined && ListOfEDependentFields.length > 0) {
                    component.set("v.disableEDependentField" , false); 
                    helper.fetchDependentValues(component, ListOfEDependentFields, true);    
                }
                else{
                    component.set("v.disableEDependentField" , true); 
                    component.set("v.listEDependingValues", ['']);
                    component.set("v.selectedEDependentValue", '');                    
                }  
            } 
            else {
                component.set("v.listEDependingValues", ['']);
                component.set("v.disableEDependentField" , true);
                component.set("v.selectedEDependentValue", '');
            }
        }
        
        if(mapDependentField != undefined ){
            console.log("Asset mapDependentField>>>",controllerValueKey);
            if (controllerValueKey != '--- None ---' && controllerValueKey != 'None' &&  controllerValueKey != undefined) {
                var ListOfDependentFields = mapDependentField[controllerValueKey];
                if(ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
                    component.set("v.disableDependentField" , false);  
                    helper.fetchDependentValues(component, ListOfDependentFields, false);    
                }
                else{
                    component.set("v.disableDependentField" , true); 
                    component.set("v.listDependingValues", ['']);
                    component.set("v.selectedDependentValue",'');
                }  
                
            } 
            else {
                component.set("v.listDependingValues", ['']);
                component.set("v.disableDependentField" , true);
                component.set("v.selectedDependentValue",'');
            }
        }
    }
})