({
    doInit : function(component, event, helper) {
        // call the helper function
        helper.fetchPicklistValues(component,event, helper);
    },
    
    onControllerFieldChange: function(component, event, helper) {    
        helper.onControllingFieldChange(component,event, helper);
    },
    
    onDependentFieldChange: function(component, event, helper) {        
        var dependentVal = event.getSource().get("v.value"); // get selected dependent field value        
        
        if( component.get("v.isMultiSelect")){
            var selectedList = component.get("v.listSelectedDependentValues");
            var dependentValuesList = component.get("v.listDependingValues");
            var indexVal = dependentValuesList.findIndex(d=>d==dependentVal);
            dependentValuesList.splice(indexVal,1);
            
            component.set("v.listDependingValues",dependentValuesList);
            console.log("v.listDependingValues",dependentValuesList);
            
            selectedList.push(dependentVal);
            component.set("v.listSelectedDependentValues",selectedList);
            console.log("v.listSelectedDependentValues",selectedList);
            
            component.set("v.selectedDependentValue",selectedList.join(";"));
            console.log( component.get("v.selectedDependentValue"));
            event.getSource().set("v.value", "");
        }else{
            component.set("v.selectedDependentValue",dependentVal);
        }
        
    },
    
    
    handleRemoveSelectedVal: function(component, event, helper) {
        var dependentVal = event.getParam("name");
        
        //remove Selected Items 
        var selectedList = component.get("v.listSelectedDependentValues");
        var indexVal = selectedList.indexOf(dependentVal);
        selectedList.splice(indexVal,1);
        component.set("v.listSelectedDependentValues",selectedList);
        console.log('selectedList',selectedList);
        
        //add elements to exisiting list
        var dependentValuesList = component.get("v.listDependingValues"); 
        dependentValuesList.push(dependentVal);
        component.set("v.listDependingValues",dependentValuesList);
        console.log('dependentValuesList',dependentValuesList);
    },
    
    onMultiSelectControllerFieldChange: function(component, event, helper) {
        console.log('MultiSelect');
        //component.set("v.listSelectedDependentValues",[]);
        helper.onControllingFieldChange(component,event, helper);
    },
    
    onEDependentFieldChange: function(component, event, helper) {         
        var dependentEngVal = event.getSource().get("v.value"); // get selected dependent field value
        component.set("v.selectedEDependentValue", dependentEngVal);
    },
})