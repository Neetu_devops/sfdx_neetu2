({
    //Rent Manager initial load
    doInit : function(component, event, helper) {
        console.log("doInit RentManager: ");
        
        component.set("v.showRentList", true);
        component.set("v.showTypeWizard", false);
        component.set("v.showFixedTypeWizard", false);
        component.set("v.showVariableTypeWizard", false);
        component.set("v.showFloatingTypeWizard", false);
	    component.set("v.showPBHTypeWizard", false);
        component.set("v.isEdit", false);
        component.set("v.isRefresh", false);
        component.set("v.isNewType", false);
        component.set("v.isView", false);
        component.set("v.viewPreview", false);
        var isFromLeaseEx = component.get("v.isFromLE");
        if(isFromLeaseEx == true)
        {
           component.set("v.isLeaseExtension",true); 
            helper.getNamespacePrefix(component);
        }else{
          helper.getParamValue('RentScheduler', 'c__LeaseId', component);
          
        }
    },
    
    //used to open the record page on a new window for which user clicked
    openRecord: function(component,event,helper) {
        var record = event.currentTarget.id;
        console.log("openRecord "+record);
        window.open('/' + record);  
    },
    
    //used to show the list of schedules for the clicked multirent record
    showRecord: function(component,event,helper) {
        var record = event.currentTarget.id;
        console.log("showRecord RentManager"+record);
        
        var rentList = component.get("v.rentList");
        //console.log('showRecord rentList :',rentList);
        var recordSel = rentList[record];
        console.log('showRecord recordSel :',recordSel);
        
        component.set("v.multiRentSch", recordSel);
        component.set("v.isView", true);
        
        component.set("v.showRentList", false);
        component.set("v.showTypeWizard", true);
        console.log('showRecord RentType :',recordSel.RentType);
        component.set("v.typeValue",recordSel.RentType);
        console.log('showRecord typeValue :', component.get("v.typeValue"));
        console.log('showRecord RentPeriod :',recordSel.RentPeriod);
        component.set("v.periodValue",recordSel.RentPeriod);
        console.log('showRecord periodValue :', component.get("v.periodValue"));
        component.set("v.nameValue",recordSel.RentName);
        component.set("v.commentValue",recordSel.Rentcomment);
        component.set("v.includeComment",recordSel.RentIncludeComment);
        component.set("v.isExtended",recordSel.isExtendedRentPeriod);
        console.log("showRecord : "+component.get("v.isExtended"));
        helper.showPreviewData(component);
    },
    
    //when a user clicks on the New Schedule button,we set following data
    onNewScheduleClick: function(component, event, helper) {
        console.log("onNewScheduleClick ");
        component.set("v.showRentList", false);
        component.set("v.showTypeWizard", true);
        component.set("v.isNewType", true);
        if(component.get("v.isView") == false){
            component.set("v.typeValue","");
            component.set("v.periodValue","");
            component.set("v.nameValue","");
            component.set("v.commentValue","");
            component.set("v.includeComment",false);
            component.set("v.isExtended",false);
        }
        helper.getPicklistValues(component);
        
        
        //refresh data in child component 
        console.log("onNewScheduleClick refresh child component "+component.get("v.isView"));
        var onRefreshButtonClicked = component.getEvent("onRefreshButton");
        onRefreshButtonClicked.setParams({
            "refresh" : true
        });
        onRefreshButtonClicked.fire();
    }, 
    
    refreshRentList: function(component,event,helper) {
        console.log("refreshRentList");
        var leaseId = event.getParam("leaseId");
        var showTypeWizard = event.getParam("showTypeWizard");
        var isEdit = event.getParam("isEdit");
        
        console.log("refreshRentList recordId: "+leaseId + " showTypeWizard: "+showTypeWizard + " isEdit:"+isEdit);
        component.set("v.isEdit", isEdit);
        
        if(showTypeWizard == true) {
            component.set("v.showRentList", false);
            component.set("v.showTypeWizard", true);
            component.set("v.showFixedTypeWizard", false);
            component.set("v.showVariableTypeWizard", false);
            component.set("v.showFloatingTypeWizard", false);
            component.set("v.showPBHTypeWizard", false);             
            var val = component.get('v.typePickListValue');
            if($A.util.isEmpty(val) || $A.util.isUndefined(val)) 
                helper.getTypePickList(component);
            else
                helper.showPreviousTypePickList(component); 
            
            var valP = component.get('v.periodPickListValue');
            if($A.util.isEmpty(valP) || $A.util.isUndefined(valP)) 
                helper.getPeriodPickList(component);
            else
                helper.showPreviousPeriodPickList(component); 
            
            component.set("v.isView", false);
        }
        else{
            component.set("v.showRentList", true);
            component.set("v.showTypeWizard", false);
            component.set("v.showFixedTypeWizard", false);
            component.set("v.showVariableTypeWizard", false);
            component.set("v.showFloatingTypeWizard", false);
	    component.set("v.showPBHTypeWizard", false); 
            component.set("v.isLoading", true);
            component.set("v.isView", false);
            helper.getRentScheduleData(component);
        }
    },
    
    //next button from Page 1 to Page 2
    onTypeNextClick: function(component, event, helper) {
        console.log('onTypeNextClick Rent Manager');
        component.set("v.isNameErrorMsg", false);
        console.log('onTypeNextClick');
        var name = component.find('nameId').get("v.value");
        var type = component.find('typeId').get("v.value");
        var period = component.find('periodId').get("v.value");
        var includeComment = component.find('includeId').get("v.value");
        var isExtendedRent = component.get("v.isExtended");
        console.log('onTypeNextClick type: '+type+ ' period:'+period+ 'includeComment :'+includeComment+'isExtendedRent :'+isExtendedRent);
        
        component.set("v.periodValue",period);
        component.set("v.includeComment",includeComment);
        component.set("v.isExtended",isExtendedRent);
        component.set("v.typeValue",type);
        
        if($A.util.isEmpty(name) || $A.util.isUndefined(name)) {
            component.set("v.isNameErrorMsg", true);
        }
        else{
            console.log("onTypeNextClick isNewType "+component.get("v.isNewType"));
            
            if(type == "Fixed Rent") {
                //show fixed wizard
                component.set("v.showRentList", false);
                component.set("v.showTypeWizard", false);
                component.set("v.showFixedTypeWizard", true);
                component.set("v.showVariableTypeWizard", false);
                component.set("v.showFloatingTypeWizard", false);
		        component.set("v.showPBHTypeWizard", false);
                component.set("v.isRefresh", component.get("v.isNewType"));
                component.set("v.showSlr",true);
                console.log('validSLR :'+component.get("v.showSlr"));
            }
            else if(type == "Seasonal Rent") {
                //show variable wizard
                component.set("v.showRentList", false);
                component.set("v.showTypeWizard", false);
                component.set("v.showFixedTypeWizard", false);
                component.set("v.showVariableTypeWizard", true);
                component.set("v.showFloatingTypeWizard", false);
		 		component.set("v.showPBHTypeWizard", false);
                component.set("v.isRefresh", component.get("v.isNewType"));
                component.set("v.showSlr",true);  
                console.log('validSLR :'+component.get("v.showSlr"));
            }
            else if(type == "Floating Rent") {
                component.set("v.showRentList", false);
                component.set("v.showTypeWizard", false);
                component.set("v.showFixedTypeWizard", false);
                component.set("v.showVariableTypeWizard", false);
                component.set("v.showFloatingTypeWizard", true);
		        component.set("v.showPBHTypeWizard", false);
                component.set("v.isRefresh", component.get("v.isNewType"));
                component.set("v.showSlr",false); 
                console.log('validSLR :'+component.get("v.showSlr"));
            }
	    else if(type == "Power By The Hour") {
                component.set("v.showRentList", false);
                component.set("v.showTypeWizard", false);
                component.set("v.showFixedTypeWizard", false);
                component.set("v.showVariableTypeWizard", false);
                component.set("v.showFloatingTypeWizard", false);
                component.set("v.showPBHTypeWizard", true);
		        component.set("v.showSlr",false); 
                component.set("v.isRefresh", component.get("v.isNewType"));
            }
        }
    },
    
    //when rent type picklist changes
    onTypeChange: function(component, event, helper) {
        console.log("onTypeChange :");
        helper.getPeriodPicklistValues(component);
    },
    
    //used for Cancel button on Page1 or Back to Rent Schedule button clicked
    onCancelClick: function(component, event, helper) {
        console.log("onCancelClick :");
        component.set("v.showRentList", true);
        component.set("v.showTypeWizard", false);
        component.set("v.showFixedTypeWizard", false);
        component.set("v.showVariableTypeWizard", false);
        component.set("v.showFloatingTypeWizard", false);
        component.set("v.showPBHTypeWizard", false);
        component.set("v.isEdit", false);
        component.set("v.isRefresh", false);
        component.set("v.isNewType", false);
        component.set("v.isView", false);
        component.set("v.viewPreview",false);
        helper.getRentScheduleData(component);
    },
    
    //used to delete multirent record
    onDeleteClick: function(component, event, helper) {
        var record = event.currentTarget.id;
            console.log("onDeleteClick "+record);
            
            var rentList = component.get("v.rentList");
            var isValid = true;
            if(record){
                var recordSel = rentList[record];
                if(!$A.util.isEmpty(rentList[record]) && !$A.util.isUndefined(rentList[record])) {
                    var status = rentList[record].Status;
                    if(status == "InActive")
                        isValid = false;
                    console.log('onEditData '+status);
                }
                if(isValid){
                    component.set("v.isLoading", true);
                    helper.deleteMultiRentSch(component, recordSel);
                    component.set("v.isView", false);
                }else{
                    helper.showWarningMsg(component, "InActive", "The rent schedule is unable to delete because the schedule is ticked as Inactive.");
                }
            }
    },
    
    //used when user clicks on View button to display all the parameters of the given multirent schedule
    onViewClick: function(component, event, helper) {
        console.log('onViewClick');
        var record = event.currentTarget.id;
        var rentList = component.get("v.rentList");
        var recordSel = rentList[record];
        console.log("onViewClick recordSel: "+JSON.stringify(recordSel));
        component.set("v.viewPreview",true);
        component.set("v.showRentList",false);
        
        console.log('onViewClick :'+component.get('v.viewPreview'));
        console.log('onViewClick record:'+recordSel);
        helper.viewRentScheduleData(component,recordSel);
    },
    
    //used for MidTerm lease, to move from Step 2 to Step 3 for LeaseExtension
    showLEstep3: function(component, event, helper){
        console.log('showLEstep3');
        
        component.set("v.isExtensionStep2",false);
        component.set("v.isExtensionStep3",true);
    },
      
})