({
    //called from do-init: used to set the url parameters
	getParamValue : function( tabName, paramName,component ) {
 		var url = window.location.href;
        var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&amp;amp;amp;');
        var paramValue = '';
        console.log("getParamValue url "+url);
        console.log("getParamValue allParams "+allParams);
        for(var i=0; i < allParams.length; i++) {
            if(allParams[i].split('=')[0] == paramName)
                paramValue = allParams[i].split('=')[1];
        }
        console.log("getParamValue: " + paramValue);
        component.set('v.recordId', paramValue);
        this.getNamespacePrefix(component);
        }, 
    
    //called from getParamValue: used to get the namespace prefix for the packaged org
    getNamespacePrefix : function(component) {
        console.log('getNamespacePrefix');
        var self = this;
        var action = component.get("c.getNamespacePrefix");
        action.setCallback(this, function(response) {
            var namespace = response.getReturnValue();
            console.log("getNamespacePrefix response: "+namespace);
            component.set("v.namespace",namespace);
            self.getRentScheduleData(component);
            self.getLeaseData(component);
        });
        $A.enqueueAction(action);                
    },
    
    //called from getNamespacePrefix: used to fetch the lease parameters to show on the wizard
    getLeaseData : function(component) {
        console.log('getLeaseData');
         var action = component.get("c.getLeaseInfo");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var leaseInfo = response.getReturnValue();
            console.log("getLeaseData response: "+JSON.stringify(leaseInfo));
            component.set("v.leaseInfo",leaseInfo);
            component.set("v.currentRent",leaseInfo.CurrentRent);
            console.log("getLeaseData currentRent",component.get("v.currentRent"));
            var leaseType = leaseInfo.LeaseType;
            console.log("getLeaseData leaseType",leaseType);
            if(leaseType == 'Finance Lease' ){
            component.set("v.validLeaseType",false);
            }
            else{
               component.set("v.validLeaseType",true); 
            }
        });
        $A.enqueueAction(action);                
    },
    
    //called from getNamespacePrefix: used to fetch multirent schedule details
    getRentScheduleData : function(component) {
        console.log('getRentScheduleData');
        var action = component.get("c.getRentSchedulesForLease");
        action.setParams({
            recordId : component.get("v.recordId"),
            multirentId : null
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getRentScheduleData state: "+state);
            if (state === "SUCCESS") {
                var rentList = response.getReturnValue();
                console.log("getRentScheduleData response: "+JSON.stringify(rentList));
                component.set("v.rentList",rentList);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getRentScheduleData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        this.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    this.showErrorMsg(component, "Error in creating record");
                }
            }
			component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);                
    },
    
    //called from onViewClick: used to set the multirentSchedule record parameters
    viewRentScheduleData : function(component,multiRentData) {
        console.log('viewRentScheduleData ',JSON.stringify(multiRentData));
         if(multiRentData != null){
            component.set("v.typeValue",multiRentData.RentType);
            component.set("v.periodValue",multiRentData.RentPeriod);
            component.set("v.nameValue",multiRentData.RentName);
            component.set("v.commentValue",multiRentData.Rentcomment);
            component.set("v.includeComment",multiRentData.RentIncludeComment);
            component.set("v.isExtended",multiRentData.isExtendedRentPeriod);
             if(component.get("v.isExtended")){
             component.set("v.extendedRentType",multiRentData.ExtendedRentType);
             
             if(component.get("v.extendedRentType") == 'Extended at Holdover Rent Rate'){
                    component.set("v.isHoldOverType",true);
                }else{
                    component.set("v.isHoldOverType",false);
                }
             }
            component.set("v.rentStartValue",multiRentData.StartDate);
            component.set("v.rentEndValue", multiRentData.EndDate);
            component.set("v.differentStartDate", multiRentData.DifferentStartDate);
            component.set("v.secondRentStartValue", multiRentData.SecondStartDate);    
            component.set("v.baseRentValue",multiRentData.BaseRent);
            component.set("v.offWingRentValue",multiRentData.OffWingRent);
            component.set("v.rentEscalationValue", multiRentData.Escalation);
            component.set("v.rentEscalationMonthValue", multiRentData.EscalationMonth);
            component.set("v.invoiceDayValue", multiRentData.InvoiceGenerationDay);
            component.set("v.periodEndDayValue", multiRentData.RentPeriodEndDay);
            component.set("v.rentDueTypeValue", multiRentData.RentDueType);
            component.set("v.rentDueSettingValue", multiRentData.RentDueSetting);
            component.set("v.rentDueDayValue", multiRentData.RentDueDay);
            component.set("v.prorataValue", multiRentData.ProrataNumberOfDays);
            component.set("v.dueDateCorrectionValue", multiRentData.DueDayCorrection);
            if(multiRentData.RentType == 'Seasonal Rent'){
                console.log('if Seasonal :'+multiRentData.RentType);
                component.set("v.lowSeasonRentValue" , multiRentData.LowSeasonRent);
                component.set("v.lowStartMOnthValue", multiRentData.LowSeasonStartMonth);
                component.set("v.lowStartYearValue", multiRentData.LowSeasonStartYear);
                component.set("v.lowEndYearValue", multiRentData.LowSeasonEndYear);
                component.set("v.lowEndMOnthValue", multiRentData.LowSeasonEndMonth);
                component.set("v.lowSeasonPeriodValue", multiRentData.LowSeasonPeriod);
            }
            if(multiRentData.RentType == 'Fixed rent' || multiRentData.RentType == 'Seasonal Rent'){
                if('for slr :'+multiRentData.RentType);
                component.set("v.slrCheck", multiRentData.SLRGenerateCheck);
                component.set("v.slrOpeningBalance", multiRentData.SLROpeningBalance);
                component.set("v.slrStartDateValue", multiRentData.SLRStartDate);
                component.set("v.slrEndDateValue", multiRentData.SLREndDate);
            }
             if(multiRentData.RentType == 'Floating Rent'){
                 console.log('if Floating :');
                 component.set("v.assInterestRateValue", multiRentData.AssumedInterestRate);
                 component.set("v.rentCreditDebitValue", multiRentData.RentalCreditDebit);
                 component.set("v.costFundValue", multiRentData.CostofFunds);
                 component.set("v.rateResetFrequency", multiRentData.RateResetFrequency);
                 component.set("v.firstResetDateValue", multiRentData.FirstResetDateValue);
                 component.set("v.businessDaysPrior", multiRentData.BusinessDaysPrior);
                 component.set("v.adjustmentFactorValue",multiRentData.AdjustmentFactor);
                 component.set("v.manuEscFactValue",multiRentData.ManufactureFactor);
                 var InterestRateIndex = new Object();
                 InterestRateIndex.Id = multiRentData.InterestRateIndexId;
                 InterestRateIndex.Name = multiRentData.InterestRateIndexName;
                 component.set("v.interestRateIndexLookupRecord", InterestRateIndex);
                 console.log('InterestRateIndex : ',component.get("v.interestRateIndexLookupRecord"));
                 component.set("v.rentEscalationValue", multiRentData.Escalation);
                 component.set("v.rentEscalationMonthValue", multiRentData.EscalationMonth);
             }
            if(multiRentData.RentType == 'Fixed Rent'){
                console.log('if Fixed :'+multiRentData.RentType);
                console.log('multirentId :'+multiRentData.RentId);
                var action = component.get("c.getSteppedMultiData");
                action.setParams({
                    multiRentId : multiRentData.RentId
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state == "SUCCESS") {
                        var stRentList = response.getReturnValue();
                        console.log("showPreviewScreen response:stRentList "+JSON.stringify(stRentList));
                        if(stRentList != null) {
                            var steppedObjectArr = [];
                            for(var i = 0; i < stRentList.length; i++){
                                
                                var dateV = stRentList[i].SteppedRentStartDate;
                                var numV = stRentList[i].RentPercent/100;
                                var amtV = stRentList[i].RentAmount;
                                var amtCheckV = stRentList[i].AmtIncDec;
                                var id = stRentList[i].Id;
                                steppedObjectArr.push({stData : dateV , stPer: numV, stAmtCheck:amtCheckV, stAmt: amtV, stId:id});
                                
                            }
                            component.set("v.steppedObjectArr" ,steppedObjectArr );
                        }
                        console.log("showPreviewScreen obj steppedObjectArr: "+steppedObjectArr);
                        if(!$A.util.isEmpty(steppedObjectArr) && !$A.util.isUndefined(steppedObjectArr)) 
                            console.log("showPreviewScreen json: steppedObjectArr"+JSON.stringify(steppedObjectArr));
                    }
                    else{
                        var errors = response.getError();
                        console.log("showPreviewScreen errors "+JSON.stringify(errors));
                        component.set("v.isLoading", false);
                    }
                });
                $A.enqueueAction(action);
            }
            if(multiRentData.RentType == 'Power By The Hour'){
               
                this.getPbhVal(component,multiRentData);
                this.getPBHRentData(component,multiRentData);
                
                
            }
            
        }       
        
        
        
    },
    

    //called from viewRentScheduleData: used to set the pbh multirentSchedule record parameters
    getPbhVal : function(component,multiRentData){
      var action = component.get("c.getMultiRentScheduleRecord");
       action.setParams({
            multiRentScheduleId : multiRentData.RentId,
            leaseId: component.get("v.leaseId")
            
        }); 
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getPbhVal allValues',JSON.stringify(allValues));
                component.set("v.minTotalValue",allValues.TotalMinAmount);
                component.set("v.maxTotalValue",allValues.TotalMaxAmount);
                component.set("v.minPBHValue",allValues.PbhminAmount);
                component.set("v.maxPBHValue",allValues.PbhmaxAmount);
            }
            else if (response.getState() === "ERROR") {
                var errors = response.getError();
                console.log("getPBHRentData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    
    //called from viewRentScheduleData: used to set the pbh multirentSchedule record parameters
    getPBHRentData : function(component,multiRentData) {
        console.log("getPBHRentData");
        console.log("getPBHRentData leaseId"+component.get("v.leaseId"));
        var action = component.get("c.getPBHRentData");
        action.setParams({
            multiRentId : multiRentData.RentId,
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getPBHRentData state: "+state);
            if (state === "SUCCESS") {
                var pbhrentList = response.getReturnValue();
                console.log("getPBHRentData response: "+JSON.stringify(pbhrentList));
                component.set("v.assemblyList",pbhrentList);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getPBHRentData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            component.set("v.isLoading", false);
            console.log("getPBHRentData loading "+component.get("v.isLoading"));
        });
        $A.enqueueAction(action); 
    },
    
    //called from viewRentScheduleData: used to set the pbh multirentSchedule record parameters
    fetchAssemblyList : function(component){
        console.log('fetchAssemblyList');
        console.log('LeaseId :',component.get("v.recordId"));
        var action = component.get("c.getAssemblyList");
        console.log('call');
        action.setParams({
            recordId : component.get("v.recordId")
        });
        
         action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("fetchAssemblyList state: "+state);
            if (state === "SUCCESS") {
                var assemblyList = response.getReturnValue();
                console.log("fetchAssemblyList response: "+JSON.stringify(assemblyList));
                component.set("v.assemblyList",assemblyList); 
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("fetchAssemblyList errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            component.set("v.isLoading", false);
            console.log("fetchAssemblyList loading "+component.get("v.isLoading"));
        });
        
        $A.enqueueAction(action);  
    },
    
    //called from onNewScheduleClick: used to fetch the picklist values
    getPicklistValues : function(component) {
        this.getTypePickList(component);
        this.getPeriodPickList(component);
        
        var isExtended = component.get("v.isExtended");
        console.log("isExtended :"+isExtended);
        if(isExtended){
        this.getExtendedRentTypePickList(component);
        }
    },
    
    //called from getPicklistValues: used to fetch the Rent Type picklist value
    getTypePickList : function(component) {
        console.log('getTypePickList');
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Type__c'
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('getTypePickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getTypePickList result:'+allValues);
                //component.set('v.typePickListValue', allValues);
                var typeList = [];
                var j=0;
                for(var i=0;i< response.getReturnValue().length; i++) {
                    var data = response.getReturnValue()[i];
                    if(data.includes('Fixed') || data.includes('Floating') || data.includes('Seasonal') || data.includes('Power By The Hour')) {
                    	opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                        typeList[j] = response.getReturnValue()[i];
                        j++;
                    }
            	}
            	console.log('getTypePickList opts :',opts);
                var rentType = component.find('typeId');
                if(rentType != undefined){
                    rentType.set("v.options", opts); 
                }
                component.set('v.typePickListValue', typeList);
                console.log('getTypePickList typeList :',typeList);
            }
            var fieldV = component.get("v.typeValue");
            console.log('getTypePickList fieldV :',fieldV);
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
            	self.showPreviousTypePickList(component);
        });
        $A.enqueueAction(action);
    },

    //called from getPicklistValues: used to fetch the Rent Period picklist values
    getPeriodPickList : function(component) {
        console.log('getPeriodPickList' );
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Period__c'
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('getPeriodPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('getPeriodPickList result:'+allValues);
                component.set('v.periodPickListValue', allValues);
                var rentType = component.get("v.typeValue");
                console.log('getPeriodPickList rentType :'+rentType);
                
                if(rentType == "Power By The Hour"){
                    opts.push({"class": "optionClass", label: "Monthly", value: "Monthly", selected:true});
                    opts.push({"class": "optionClass", label: "Quarterly", value: "Quarterly"});
                }
                else if(rentType == "Seasonal Rent"){
                   opts.push({"class": "optionClass", label: "Monthly", value: "Monthly", selected:true});
                     
                }
                else{
                for(var i=0;i< response.getReturnValue().length; i++) {
                    if(response.getReturnValue()[i] == 'Monthly')
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected:true});
                    else
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
            	}
                
                    var periodId = component.find('periodId');
                if(periodId != undefined){
                    periodId.set("v.options", opts); 
                }
            	console.log('getPeriodPickList opts :',opts );
            }
            var fieldV = component.get("v.periodValue");
            console.log('getPeriodPickList fieldV :',fieldV );
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
            	self.showPreviousPeriodPickList(component);
            }     
        });
        $A.enqueueAction(action);
    },
     
    //called from onTypeChange: used to fetch Rent Period picklist values when Rent type is changed
    getPeriodPicklistValues : function(component){
        console.log('getPeriodPicklistValues');
        var rentType = component.get("v.typeValue");
        console.log('getPeriodPicklistValues rentType ',rentType);
        var opts = [];
      if(rentType == "Power By The Hour"){
          opts.push({"class": "optionClass", label: "Monthly", value: "Monthly", selected:true});
          opts.push({"class": "optionClass", label: "Quarterly", value: "Quarterly"});
      }
      else if(rentType == "Seasonal Rent"){
          opts.push({"class": "optionClass", label: "Monthly", value: "Monthly", selected:true});
          
      }
          else{
              this.getPeriodPickList(component);
          }   
        
        var periodId = component.find('periodId');
                if(periodId != undefined){
                    periodId.set("v.options", opts); 
                }
      console.log('getPeriodPickList opts :',opts );
            
            var fieldV = component.get("v.periodValue");
            console.log('getPeriodPickList fieldV :',fieldV );
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
            	this.showPreviousPeriodPickList(component);
                 
    },
    
    showPreviousTypePickList : function(component){
        var opts = component.find("typeId").get("v.options");
        var fieldV = component.get("v.typeValue");
        var valueList = component.get("v.typePickListValue");
        console.log('showPreviousTypePickList valueList '+valueList);
        if(!$A.util.isEmpty(valueList) && !$A.util.isUndefined(valueList)) {
            for(var i=0;i< valueList.length; i++) {
                /*if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            */
                if(valueList[i] == fieldV) 
                    opts[i].selected = true;
                else
                    opts[i].selected = false;
            }
            var rentType = component.find('typeId');
                if(rentType != undefined){
                    rentType.set("v.options", opts); 
                }
        }
        console.log('showPreviousTypePickList fieldV: '+fieldV+" "+valueList);
    },
    
    showPreviousPeriodPickList : function(component){
        var opts = [];
        var fieldV = component.get("v.periodValue");
        
        var type = component.get("v.typeValue");
        if(type == "Power By The Hour"){
            var valueList = ["Monthly","Quarterly"];
       
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
         else if(type == "Seasonal Rent"){
            var valueList = ["Monthly"]; 
             for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
            
        else{
            var valueList = component.get("v.periodPickListValue");
            for(var i=0;i< valueList.length; i++) {
                if(valueList[i] == fieldV) 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
            }
        }
        var periodId = component.find('periodId');
        if(periodId != undefined){
            periodId.set("v.options", opts); 
        }
    },
    
    //called from showRecord: decides which type multiRent schedule to be displayed
    showPreviewData : function(component) {
        var type = component.get("v.typeValue");
        console.log('showPreviewData type ' +type);
        if(type == "Fixed Rent") {
            //show fixed wizard
            component.set("v.showRentList", false);
            component.set("v.showTypeWizard", false);
            component.set("v.showFixedTypeWizard", true);
            component.set("v.showVariableTypeWizard", false);
            component.set("v.showFloatingTypeWizard", false);
            component.set("v.showPBHTypeWizard", false);
        }
        else if(type == "Seasonal Rent") {
            //show variable wizard
            component.set("v.showRentList", false);
            component.set("v.showTypeWizard", false);
            component.set("v.showFixedTypeWizard", false);
            component.set("v.showVariableTypeWizard", true);
            component.set("v.showFloatingTypeWizard", false);
            component.set("v.showPBHTypeWizard", false);
        }
		else if(type == "Floating Rent") {
            component.set("v.showRentList", false);
            component.set("v.showTypeWizard", false);
            component.set("v.showFixedTypeWizard", false);
            component.set("v.showVariableTypeWizard", false);
            component.set("v.showFloatingTypeWizard", true);
            component.set("v.showPBHTypeWizard", false);
        }
        else if(type == "Power By The Hour") {
            component.set("v.showRentList", false);
            component.set("v.showTypeWizard", false);
            component.set("v.showFixedTypeWizard", false);
            component.set("v.showVariableTypeWizard", false);
            component.set("v.showFloatingTypeWizard", false);
            component.set("v.showPBHTypeWizard", true);
        }
    },
    
    //called from onDeleteClick: used to delete multirent record from the backend
    deleteMultiRentSch : function(component, multiRentData) {
        //var multiRentData = component.get("v.multiRent");
        var action = component.get("c.deleteMultiRentRecord");
        var self= this;
        action.setParams({
            steppedRentId : multiRentData.RentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("deleteMultiRentSch response-state: "+state);
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
           		console.log("deleteMultiRentSch response: "+res);
                if($A.util.isEmpty(res) || $A.util.isUndefined(res) || res == null)
                    self.showDeleteMsg(component, 'Success');
                else
                	self.showErrorMsg(component, res);
                self.getRentScheduleData(component);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("deleteMultiRentSch errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
                component.set("v.isLoading", false);
            }
            
        });
        $A.enqueueAction(action);
    },
    
    //general function to display error messages
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
	},
    
    //used to display the warning message
    showWarningMsg : function(component, title, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'warning',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    
    //function to display error messages when multirent record is deleted
    showDeleteMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Successfully Deleted!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
	},
})