({
	doInit : function(component, event, helper) {
		var action = component.get("c.getXIRRInput");
        console.log('recordId: ', component.get("v.recordId"));
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            let state = response.getState();
            if(state === 'SUCCESS'){
                let xirrWrapStr = response.getReturnValue();
                let xirrWrap = JSON.parse(xirrWrapStr);
                
                //We need to call this every time to calculate the updated value of XIRR
                helper.formatXIRRInput(component,xirrWrap.xirrInput);    
                
            } else{
                //helper.handleErrors(component, errors);
            }
        });
        $A.enqueueAction(action);
	}
})