({
	scriptsLoaded : function(component, event, helper) {
		// Added By AKG for Testing of SparklineChartComponent in Managed Package on 21 April 2020
        console.log('JS Library Loaded');
		if(component.get("v.loadSparklineData")){
			var sampleData = [10,20,30,20,15,40,40,20,10,35];
			component.set("v.sampleData", sampleData);
			helper.getUtilizationData(component, event, helper);
			component.set("v.isScriptLoaded", true);
		}
    }, 
})