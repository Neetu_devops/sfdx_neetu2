({
	getUtilizationData: function(component, event, helper){
	// Added By AKG for Testing of SparklineChartComponent in Managed Package on 21 April 2020 
		var action = component.get("c.getUtilizationRelatedData");
            action.setParams({
                assetId : component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state=='SUCCESS'){
					console.log('Result: ', response.getReturnValue());
					component.set("v.assemblyWrapper", response.getReturnValue());
				}
			});
			$A.enqueueAction(action);
	}
})