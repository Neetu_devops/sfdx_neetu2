({
    doInit: function (component, event, helper) {
        var rec = component.get("v.fileRec");
        if (rec.isCopy) {
            component.find("copyBtn").set("v.variant", "brand");
            component.find("btn").set("v.variant", "neutral");
            component.find("moveBtn").set("v.variant", "neutral");
        }
        else if (rec.isMove) {
            component.find("moveBtn").set("v.variant", "brand");
            component.find("btn").set("v.variant", "neutral");
            component.find("copyBtn").set("v.variant", "neutral");
        }
            else {
                component.find("btn").set("v.variant", "brand");
                component.find("moveBtn").set("v.variant", "neutral");
                component.find("copyBtn").set("v.variant", "neutral");
            }
    },
    copyfile: function (component, event, helper) {
        component.find("copyBtn").set("v.variant", "brand");
        component.find("btn").set("v.variant", "neutral");
        component.find("moveBtn").set("v.variant", "neutral");
        var rec = component.get("v.fileRec");
        rec.isCopy = true;
        rec.isMove = false;
        rec.isNone = false;
        component.set("v.fileRec", rec);
    },
    
    handleAction: function (component, event, helper) {
        component.find("btn").set("v.variant", "brand");
        component.find("moveBtn").set("v.variant", "neutral");
        component.find("copyBtn").set("v.variant", "neutral");
        var rec = component.get("v.fileRec");
        rec.isCopy = false;
        rec.isMove = false;
        rec.isNone = true;
        component.set("v.fileRec", rec);
    },
    
    movefile: function (component, event, helper) {
        component.find("moveBtn").set("v.variant", "brand");
        component.find("btn").set("v.variant", "neutral");
        component.find("copyBtn").set("v.variant", "neutral");
        var rec = component.get("v.fileRec");
        rec.isCopy = false;
        rec.isMove = true;
        rec.isNone = false;
        component.set("v.fileRec", rec);
    },
})