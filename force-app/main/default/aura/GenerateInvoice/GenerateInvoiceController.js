({
    recordTypeSelected : function(component, event, helper) {
        component.set("v.childNode",undefined);     
        if(component.get("v.parentNode") == 'Security Deposit'){
            component.set("v.childNode","Security_Deposit");
        }else if(component.get("v.parentNode") == 'Rent'){
            component.set("v.childNode","Rent");
        }else if(component.get("v.parentNode") == 'MR'){
            component.set("v.childNode","Monthly_Utilization");
        }
    },
    
    cancel : function(component, event, helper){

        if(component.get("v.recordId") != undefined && component.get("v.recordId") != ''){
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.recordId"),
                "slideDevName": "detail"
            });
            navEvt.fire();
        }else{
            var homeEvent = $A.get("e.force:navigateToObjectHome");
            homeEvent.setParams({
                "scope": "Invoice__c"
            });
            homeEvent.fire();
        }      
    },
    
    goNext :  function(component, event, helper) {
        
        if(component.get("v.childNode") != undefined){
            if(component.get("v.childNode") == 'Manual_MR_Invoice' || 
              	component.get("v.childNode") == 'Manual_MR_Credit_Note'){
                component.set("v.modalClass", 'slds-modal_large');
            }
            
            //if its lease then RENT, MR and SD won't require Lease Lookup.
            var record = component.get("v.record");
            var parentNode = component.get("v.parentNode");

			if(record.objectType == 'Lease__c' && (parentNode == 'Rent' || parentNode == 'MR' ||
                                                   parentNode == 'Security Deposit')){
            	
               component.set("v.record", {});
            }
            component.set("v.isRCSelected", true);
            
            //get the header title.
            var recordTypeMap = component.get("v.recordTypeMap");
            var title = recordTypeMap.get(parentNode).get(component.get("v.childNode"));
            
            if(title.endsWith('Invoice')){
                title = 'Create ' + title;
            }else{
            	title = 'Create ' + title + ' Invoice'; 
            }
            component.set("v.modalTitle", title);
            
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select a invoice type.",
                "type" : "Error"
            });
            toastEvent.fire();
        }
    },
    
    save : function(component,event,helper){
        var appEvent = $A.get("e.c:InvoiceSaveEvent");
        appEvent.fire();  
    },
    
    doInit : function(component, event, helper) {

             // call the apex class method 
        let action = component.get("c.getData"); 
            component.set("v.showSpinner", true);
        
        var recId = component.get("v.recordId");
        
        action.setParams({'recordId': recId == '' ? null : recId});
            // set a callBack    
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    
                    var result = response.getReturnValue();

                if(result.objectRecord != undefined && result.objectRecord != ''){
                    
                    component.set("v.record",result.objectRecord);
                    
                    if(result.objectRecord.objectType == 'Rent__c'){
                        component.set("v.childNode",'Rent');
                        component.set("v.isRCSelected", true);
                        component.set("v.modalTitle", 'Create Rent Invoice');
                    }else if(result.objectRecord.objectType == 'Utilization_Report__c'){
                        component.set("v.childNode",'Monthly_Utilization');
                        component.set("v.isRCSelected", true);
                        component.set("v.modalTitle", 'Create MR Invoice');
                    }else if(result.objectRecord.objectType == 'Cash_Security_Deposit__c'){
                        component.set("v.childNode",'Security_Deposit');
                        component.set("v.isRCSelected", true);
                        component.set("v.modalTitle", 'Create Security Deposit Invoice');
                    }
                    
        }else{
           component.set("v.record", {});
        }

        var recordTypeMap = new Map();
        var recordTypes = [];
                var recordTypeDynamicMap = result.recordTypeMap;
        
        var grp1 = new Map();
                //grp1.put('Rent', recordTypeDynamicMap['Rent']);
               // grp1.put('Credit_Note_Rent', recordTypeDynamicMap['Credit_Note_Rent']);
        
        recordTypeMap.put('Rent', grp1);
        
        var grp2 = new Map();
                //grp2.put('Monthly_Utilization', recordTypeDynamicMap['Monthly_Utilization']);
              //  grp2.put('Credit_Note_MR', recordTypeDynamicMap['Credit_Note_MR']);
        
        recordTypeMap.put('MR',grp2);
        
        var grp3 = new Map();
               // grp3.put('Security_Deposit', recordTypeDynamicMap['Security_Deposit']);
        
        recordTypeMap.put('Security Deposit', grp3);
        
        var grp4 = new Map();
                    grp4.put('Manual_MR_Invoice', recordTypeDynamicMap['Manual_MR_Invoice']);
                    //june release 2020.
                    if(result.is_Manual_MR_Credit_Note_Accessible){
                    	grp4.put('Manual_MR_Credit_Note', recordTypeDynamicMap['Manual_MR_Credit_Note']);
                    }
        grp4.put('Other1', 'Manual SD');
        grp4.put('Other2', 'Manual Other');
        
        recordTypeMap.put('Manual', grp4);
        

        for(var key of recordTypeMap.keys()) {    

            var grp = {};
            grp.parentNode = [{'label' : key ,'value' : key}];
            grp.childNodes = [];
            
            for(var childKey of recordTypeMap.get(key).keys()){

				grp.childNodes.push({'label' : recordTypeMap.get(key).get(childKey), 'value' : childKey});
            }
            recordTypes.push(grp); 
        }

  
        component.set("v.recordTypeMap", recordTypeMap);
        component.set("v.recordTypes",recordTypes);
        
                
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
            component.set("v.showSpinner", false);
        });
        // enqueue the Action  
        $A.enqueueAction(action); 	
               
    }        
})