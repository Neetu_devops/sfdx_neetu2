({
	 callBatch: function (component,event) {
        var self = this;
         console.log('test---');
            var action = component.get('c.initBatch');
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("callBatch : state " + state);
            if (state === "SUCCESS") {
                 self.showMsg(component, "Batch started successfully, an email will be sent on completion", 'success', "Success");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("callBatch errors ", errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("callBatch unknown error");
                    self.showMsg(component, "Unknown error", 'error', "Error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    

    showMsg: function (component, msg, type, title) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: msg,
            duration: '5000',
            key: 'info_alt',
            type: type,
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    //to handle errors
    handleErrors: function (errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error",
            type: "error"
        };
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    console.log('error ' + toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        console.log('error ' + toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            console.log('error ' + toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
    }
})