({
	doinit : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
		var recordId = component.get("v.recordId");
        var action = component.get("c.getUtilizationData");
        action.setParams({
            recordId : recordId          
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                helper.hideSpinner(component, event, helper);
                var result = response.getReturnValue();
                if(result.errorMsg != ''){
                    helper.handleErrors(result.errorMsg);
                }
                else{
                    component.set("v.UtilizationWrapper", result);   
                }
            }
            else{
                helper.hideSpinner(component, event, helper);
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
	}
})