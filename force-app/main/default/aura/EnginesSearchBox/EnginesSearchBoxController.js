({
    doInit : function(component, event, helper) {        
        component.set('v.showSpinner', true);
        component.set("v.reloadTable", false);
        
        var objectName = 'Aircraft__c';
        var action = component.get("c.getResultColumns");
        action.setParams({
            'objectName' : objectName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                component.set('v.columns', response.getReturnValue());
                component.set("v.showTable", false);
                component.set("v.tableData", []);
                component.set("v.selectedRows", []);
            }
            else {
                helper.handleErrors(response.getError());
            }
            
            component.set('v.showSpinner', false);
        });
        
        $A.enqueueAction(action);      
    },
    
    
    handleSubmit : function(component, event, helper) {
        component.set("v.showSpinner", true);
        component.set("v.reloadTable", false); 
        
        var controllingField = component.get("v.controllingSelectedVal"); 
        var dependentEField = component.get("v.selectedEDependentValue");
        var dependentField = component.get("v.selectedDependentValue");        
        
        if(controllingField == undefined || controllingField.indexOf('None') != -1) {
            component.set("v.showSpinner", false);
            
            helper.showMessage("Please complete the required fields on the form.", "error"); 
        }
        else {
            var aircraftRecord = component.get("v.aircraftRecord");
            aircraftRecord.Aircraft_Type__c = controllingField;
            
            if(dependentField.indexOf('None') != -1) {
                aircraftRecord.Aircraft_Variant__c = '';
            }
            else {
                aircraftRecord.Aircraft_Variant__c = dependentField;
            }
            
            component.set("v.aircraftRecord", aircraftRecord);
            
            helper.searchEngines(component);
        }
    },
    
    
    addSelected : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.addSelectedAssets(component);
    }
})