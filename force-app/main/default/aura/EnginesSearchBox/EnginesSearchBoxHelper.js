({
    showMessage : function(message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: message,
            type: type
        });
        toastEvent.fire();     
    },
    
    
    searchEngines : function(component) {
        var hours = component.get("v.hours"); 
        var months = component.get("v.months");   
        var cycles = component.get("v.cycles");  
        var age = component.get("v.age");   
        var ageRange = component.get("v.ageRange");   
        var leaseRemainingMonth = component.get("v.leaseRemainingMonths");   
        var leaseMonthRange = component.get("v.leaseMonthsRange");
        
        if(months === undefined || months === '') {
            months = 0;
        }
        
        if(cycles === undefined || cycles === '') {
            cycles = 0;
        }
        
        if(hours === undefined || hours === '') {
            hours = 0;
        }
        
        if(ageRange === undefined || ageRange === '') {
            ageRange = 0;
        }

		if(age === undefined || age === '') {
            age = 0;
        }

		if(leaseMonthRange === undefined || leaseMonthRange === '') {
            leaseMonthRange = 0;
        }

		if(leaseRemainingMonth === undefined || leaseRemainingMonth === '') {
            leaseRemainingMonth = 0;
        }        
        
        var action = component.get("c.searchEngines");
        action.setParams({
            "aircraftRecord" : JSON.stringify(component.get("v.aircraftRecord")),
            "hours" : hours,
            "months" : months,
            "cycles" : cycles,
            "age" : age,
            "ageRange" : ageRange,
            "leaseRemainingMonth" : leaseRemainingMonth,
            "leaseMonthRange" : leaseMonthRange
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                console.log("SUCCESS ");
                
                var columns = component.get('v.columns');    
                var returnedValues = response.getReturnValue();
                var tableData = [];
                if(returnedValues == null) {
                    component.set("v.showSpinner", false);
                    return;
                }
                let wrapperList= returnedValues.engineWrapperList;
                wrapperList.forEach(function(item) {
                    var obj = new Object();                    
                    obj.Id = item.Id;
                    obj.iconName = 'utility:add';
                    obj.className = 'addAssets';
                    obj.title = 'Click to Add';
                    obj.isSelected = false;
                    
                    for(var i=0; i<columns.length; i++) {
                        obj[columns[i].fieldName] = item[columns[i].fieldName];
                    }
                    
                    tableData.push(obj);
                });
                
                component.set("v.reloadTable", true); 
                component.set("v.tableData",tableData);
                component.set("v.selectedRows", []);
                component.set("v.selectedRowsCount", 0);
                component.set("v.showTable", true); 
                if(returnedValues.totalRecord != null && returnedValues.totalRecord > component.get("v.recordLimit")) {
                    this.showToastMsg(component, "Too many records meet your search criteria. Only 100 records are displayed. Please refine the search criteria to find different results.", "warning", "Warning");
                }
            } 
            else {
                this.handleErrors(response.getError());
            }
            
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    
    
    addSelectedAssets : function(component) {
        var recordId;
        
        if(component.get("v.standAloneVersion")) {
            if(component.get("v.selectedRecordId") != undefined) {
                recordId = component.get("v.selectedRecordId").Id;
            }
        }
        else {
            recordId = component.get("v.recordId");
        }
        
        if(recordId == undefined) {
            this.showMessage("Please specify the deal / campaign that you would like to add these assets to", "error");
            
            component.set("v.showSpinner", false);
        }
        else {
            var action = component.get("c.addSelectedAssetsToRecord");
            action.setParams({
                'recordId' : recordId,
                'selectedAssets' : component.get("v.selectedRows"),
                'objectName' : 'Aircraft__c'
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(response.getReturnValue());
                
                if(state === 'SUCCESS' && response.getReturnValue() == 'Success') {
                    this.showMessage("Selected assets added successfully", "success"); 
                    
                    if(component.get("v.recordId") != undefined) {
                        //var url = '/' + component.get("v.recordId");
                        var url = '/';
                        if (component.get("v.recordId") === $A.get("$SObjectType.CurrentUser.Id")) {
                            url += recordId;
                        }
                        else {
                            url += component.get("v.recordId");
                        }
                        window.open(url, '_self');
                    }
                    else {
                        var tableData = component.get("v.tableData");
                        
                        for(var i=0; i<tableData.length; i++) {
                            tableData[i].iconName = 'utility:add';
                            tableData[i].className = 'addAssets';
                            tableData[i].title = 'Click to Add';
                            tableData[i].isSelected = false;
                        }
                        
                        component.set("v.tableData", tableData);
                        component.set("v.selectedRows", []);
                        component.set("v.selectedRowsCount", 0);
                    }
                }
                else {
                    this.handleErrors(response.getError());
                }
                
                component.set("v.showSpinner", false);
            });
            
            $A.enqueueAction(action);   
        }
    },
    
    showToastMsg: function (component, msg, type, title) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: msg,
            duration: '5000',
            key: 'info_alt',
            type: type,
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    handleErrors: function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        
        if(errors) {
            errors.forEach(function(error) {
                //top-level error. There can be only one
                if(error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                
                //page-level errors (validation rules, etc)
                if(error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                
                if(error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function(errorList) { 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    }
})