({
    onRowSelectionHandler: function (cmp, event, helper) {
        console.log('In ROW SELECTION HANDLER');
        let gridData = cmp.get("v.gridData");
        let sRows = event.getParam("selectedRows");
        let oldSelectedRows = cmp.get("v.selectedRows");
        let currentSelectedRows = new Set();
        let changedRows = new Set();
        let mySet = new Set();
        let finalSelectedRows = new Array();
        let ignoreDel = false;
        let isToggleCalled = cmp.get("v.isToggleCalled");
        let onToggle = cmp.get("v.onToggle");
        let getSelectedRows = cmp.find("mytree").getSelectedRows();

        if (!isToggleCalled) {
            if (onToggle) {
                getSelectedRows.forEach(function (data) {
                    if (data.hasChildren != undefined) {
                        if (data.hasChildren) {
                            gridData.forEach(function (row) {
                                if (data.name == row.name && row._children != undefined) {
                                    var notAllChild = false;
                                    row._children.forEach(function (child) {
                                        if (!data.isExpanded) {
                                            mySet.add(child.name);
                                        } else {
                                            if (Object.values(getSelectedRows).indexOf(child.name) > -1) {
                                                mySet.add(child.name);
                                                notAllChild = false;
                                            } else {
                                                notAllChild = true;
                                            }
                                            return false;
                                        }
                                    });
                                    if (notAllChild && data.isExpandeds) {
                                        mySet.delete(data.name);
                                    } else {
                                        mySet.add(data.name);
                                    }
                                }
                            });
                        } else {
                            mySet.add(data.name);
                        }
                    } else {
                        mySet.add(data.name);
                    }
                });
            }
            if (onToggle) {
                mySet.forEach(function (mySetData) {
                    currentSelectedRows.add(mySetData);
                });
            } else {
                sRows.forEach(function (data) {
                    currentSelectedRows.add(data.name);
                });
            }

            if (currentSelectedRows != undefined && currentSelectedRows.size > 0) {
                if (oldSelectedRows != undefined && oldSelectedRows.length > 0) {
                    oldSelectedRows.forEach(function (data) {
                        if (!currentSelectedRows.has(data)) {
                            changedRows.add(data);
                        }
                    });
                    if (oldSelectedRows.length < currentSelectedRows.size) {
                        currentSelectedRows.forEach(function (data) {
                            if (!oldSelectedRows.includes(data)) {
                                ignoreDel = true;
                                changedRows.add(data);
                            }
                        });
                    }
                }
                else {
                    currentSelectedRows.forEach(function (data) {
                        ignoreDel = true;
                        changedRows.add(data);
                    });
                }

                var copyOfCurrentSelectedRows = new Set();
                currentSelectedRows.forEach(function (row) {
                    copyOfCurrentSelectedRows.add(row);
                });

                if (changedRows != undefined && changedRows.size > 0) {
                    gridData.forEach(function (row) {
                        changedRows.forEach(function (data) {
                            if (data == row.name && row._children != undefined) {
                                if (copyOfCurrentSelectedRows.has(row.name)) {
                                    row._children.forEach(function (child) {
                                        if (copyOfCurrentSelectedRows.has(child.name) && !ignoreDel) {
                                            currentSelectedRows.delete(child.name);
                                        }
                                        else {
                                            currentSelectedRows.add(child.name);
                                        }
                                    });
                                } else {
                                    row._children.forEach(function (child) {
                                        currentSelectedRows.delete(child.name);
                                    });
                                }
                            } else {
                                if (row._children != undefined) {
                                    row._children.forEach(function (child) {
                                        if (data == child.name) {
                                            if (currentSelectedRows.has(row.name)) {
                                                currentSelectedRows.delete(row.name);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    });
                }

                gridData.forEach(function (data) {
                    let addParentRow = true;
                    if (data._children != undefined) {
                        data._children.forEach(function (row) {
                            if (!currentSelectedRows.has(row.name)) {
                                addParentRow = false;
                            }
                        });
                        if (addParentRow) {
                            currentSelectedRows.add(data.name);
                        }
                    }
                });
            }

            currentSelectedRows.forEach(function (data) {
                finalSelectedRows.push(data);
            });

            cmp.set("v.mainList", finalSelectedRows);
            cmp.set("v.selectedRows", finalSelectedRows);
            cmp.set("v.isToggleCalled", false);
            cmp.set("v.onToggle", true);
        } else {
            let mainList = cmp.get("v.mainList");
            let tfinalSelectedRows = [];
            cmp.set("v.selectedRows", []);
            mainList.forEach(function (data) {
                tfinalSelectedRows.push(data);
            });
            cmp.set("v.selectedRows", tfinalSelectedRows);
            cmp.set("v.onToggle", true);
            cmp.set("v.isToggleCalled", false);
        }
    },
    processInvoiceRecords: function (cmp, event, helper, selectedRows) {
        cmp.set('v.gridExpandedRows', []);
        var fromDate = cmp.get("v.fromDate");
        var toDate = cmp.get("v.toDate");
        if ((fromDate === undefined || fromDate === null || fromDate === '') || (toDate === undefined || toDate === null || toDate === '')) {
            helper.handleErrors("Please select From and To date to filter the search result.");
        }
        else {
            this.showSpinner(cmp);
            fromDate = helper.dateToParseHelper(cmp, event, helper, fromDate);
            toDate = helper.dateToParseHelper(cmp, event, helper, toDate);

            var serialNumber = cmp.get("v.serialNumberFilter");
            if (serialNumber != undefined && serialNumber != '' && serialNumber.Id != undefined && serialNumber.Id != '') {
                serialNumber = serialNumber.Id;
            }
            else {
                serialNumber = '';
            }

            var action = cmp.get("c.processInvoices");
            action.setParams({
                selectedRecords: selectedRows,
                fromDate: fromDate,
                toDate: toDate,
                invoiceType: cmp.get("v.invoiceType"),
                invoiceStatus: cmp.get("v.invoiceStatus"),
                serialNumber: serialNumber,
                viewSentInvoice: cmp.get("v.viewSentInvoice")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //console.log('response',response.getReturnValue());
                    let result = response.getReturnValue();
                    if (result != undefined && result != '' && result.processStatus != undefined && result.processStatus != '') {
                        this.handleErrors(result.processStatus, result.status);
                    }
                    else {
                        if (result.gridData != undefined && result.gridData != "" && result.gridData != null && result.recordsProcessed > 0) {
                            if (result.exceededLimit) {
                                helper.handleErrors("Too many records meet your search criteria. Only 100 records are displayed. Please refine search criteria to find different results.", "Warning");
                            }
                            var showInvoiceActionButtons = false;

                            for (let key in result.gridData) {
                                if (result.gridData[key] != undefined && result.gridData[key] != '' && result.gridData[key]._children != undefined && result.gridData[key]._children != '' && result.gridData[key]._children.length != 0) {
                                    showInvoiceActionButtons = true;
                                }
                            }
                            cmp.set('v.gridData', result.gridData);
                            cmp.set('v.gridExpandedRows', result.gridExpandedRows);
                            cmp.set("v.selectedRows", new Array());
                        }
                        else {
                            cmp.set("v.gridData", []);
                            helper.handleErrors("No record meet your search criteria. Please refine search criteria to find different results.", "Warning");
                        }

                        if (cmp.get("v.invoiceType") === 'All' || cmp.get("v.invoiceStatus") === 'All') {
                            cmp.set("v.displayActions", false);
                        }
                        else {
                            cmp.set("v.displayActions", showInvoiceActionButtons);
                        }
                        this.handleScreenSize(cmp, event);
                    }
                }
                else if (state === "ERROR") {
                    this.handleErrors(response.getError());
                }
                this.hideSpinner(cmp);
            });
            $A.enqueueAction(action);
        }
    },
    handleScreenSize: function (cmp, event) {
        document.getElementById('dataGridTable').style.height = "" + 83 + "%";
    },
    //LW - AKG - 18-03-2020
    dateToParseHelper: function (cmp, event, helper, dateToParse) {
        var dateToFormat = new Date(dateToParse);
        var formattedDate = dateToFormat.getUTCFullYear() + '-' + (dateToFormat.getUTCMonth() + 1) + '-' + dateToFormat.getUTCDate(); // yyyy-mm-dd
        //console.log('formattedDate: ', formattedDate);
        return formattedDate;
    },
    //LW - AKG - 18-03-2020
    showSpinner: function (cmp) {
        const spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function (cmp) {
        const spinner = cmp.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    handleErrors: function (errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "Error" : type,
            duration: '7000'
        };
        if (typeof errors === 'object') {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );
                        });
                    };
                }
            });
        }
        else if (typeof errors === "string" && errors.length != 0) {
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})