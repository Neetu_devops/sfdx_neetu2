({
    loadInit: function (cmp, event, helper) {
        let date = new Date;
        cmp.set("v.fromDate", $A.localizationService.formatDate(new Date(date.getFullYear(), date.getMonth(), 1), "YYYY-MM-DD"));
        cmp.set("v.toDate", $A.localizationService.formatDate(new Date(date.getFullYear(), date.getMonth() + 1, 0), "YYYY-MM-DD"));

        var columns = [
            { type: "url", fieldName: "lesseeRecordLink", label: "Lessee", initialWidth: 250, typeAttributes: { label: { fieldName: "lessee" } } },
            { type: "text", fieldName: "serialNumber", label: "Serial Number" },
            { type: "text", fieldName: "invoiceType", label: "Lease Type" },
            { type: "text", fieldName: "invoiceType", label: "Invoice Type" },
            { type: "dateTime", fieldName: "forPeriodEnding", label: "For Period Ending" },
            { type: "url", fieldName: "reference", label: "Reference", typeAttributes: { label: { fieldName: "invoiceName" } } },
            { type: "dateTime", fieldName: "datePaymentDue", label: "Date Payment Due" },
            { type: "currency", fieldName: "Amount", label: "Invoiced Amount" },
            { type: "text", fieldName: "invoiceStatus", label: "Invoice Status" },
            { type: "email", fieldName: "emailAddress", label: "Email" },
            { type: "boolean", fieldName: "sentStatus", label: "Sent Status" },
            { type: "dateTime", fieldName: "invoiceSentDate", label: "Date Invoice Sent" }
        ];
        cmp.set("v.gridColumns", columns);
    },
    fetchInvoices: function (cmp, event, helper) {
        cmp.set("v.gridExpandedRows", []);

        var fromDate = cmp.get("v.fromDate");
        var toDate = cmp.get("v.toDate");
        if ((fromDate === undefined || fromDate === null || fromDate === "") || (toDate === undefined || toDate === null || toDate === "")) {
            helper.handleErrors("Please select From and To date to filter the search result.");
        }
        else {
            helper.showSpinner(cmp);
            fromDate = helper.dateToParseHelper(cmp, event, helper, fromDate);
            toDate = helper.dateToParseHelper(cmp, event, helper, toDate);
            var invoiceType = cmp.get("v.invoiceType");
            var invoiceStatus = cmp.get("v.invoiceStatus");
            var viewSentInvoice = cmp.get("v.viewSentInvoice");
            var serialNumber = cmp.get("v.serialNumberFilter");

            if (serialNumber != undefined && serialNumber != "" && serialNumber.Id != undefined && serialNumber.Id != "") {
                serialNumber = serialNumber.Id;
            }
            else {
                serialNumber = "";
            }

            var action = cmp.get("c.fetchInvoice");
            action.setParams({
                fromDate: fromDate,
                toDate: toDate,
                invoiceType: invoiceType,
                invoiceStatus: invoiceStatus,
                serialNumber: serialNumber,
                viewSentInvoice: viewSentInvoice
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //console.log("response", response.getReturnValue());
                    let result = response.getReturnValue();

                    if (result.gridData != undefined && result.gridData != "" && result.gridData != null && result.recordsProcessed > 0) {
                        if (result.exceededLimit) {
                            helper.handleErrors("Too many records meet your search criteria. Only 100 records are displayed. Please refine search criteria to find different results.", "Warning");
                        }
                        cmp.set("v.gridData", result.gridData);
                        cmp.set("v.gridExpandedRows", result.gridExpandedRows);
                        cmp.set("v.selectedRows", new Array());
                        
                        var showInvoiceActionButtons = false;
                        for (let key in result.gridData) {
                            if (result.gridData[key] != undefined && result.gridData[key] != "" && result.gridData[key]._children != undefined && result.gridData[key]._children != "" && result.gridData[key]._children.length != 0) {
                                showInvoiceActionButtons = true;                                
                            }
                        }
                    }
                    else {
                        cmp.set("v.gridData", []);
                        helper.handleErrors("No record meet your search criteria. Please refine search criteria to find different results.", "Warning");
                    }

                    if (cmp.get("v.invoiceStatus") === 'All') {
                        cmp.set("v.displayActions", false);
                    }
                    else {
                        cmp.set("v.displayActions", showInvoiceActionButtons);
                    }
                    helper.handleScreenSize(cmp, event);
                }
                else if (state === "ERROR") {
                    helper.handleErrors(response.getError());
                }
                helper.hideSpinner(cmp);
            });
            $A.enqueueAction(action);
        }
    },
    onRowSelectionHandler: function (cmp, event, helper) {
        helper.onRowSelectionHandler(cmp, event, helper);
    },
    onToggleHandler: function (cmp, event, helper) {
        var bool = false;
        event.preventDefault();
        let treeGridCmp = cmp.find('mytree');
        cmp.set("v.gridExpandedRows", treeGridCmp.getCurrentExpandedRows());

        if (cmp.get("v.onToggle") && !cmp.get("v.isToggleCalled")) {
            bool = true;
        }

        if (cmp.get("v.onToggle")) {
            let mainList = cmp.get("v.mainList");
            let finalSelectedRows = [];

            cmp.set("v.selectedRows", []);
            mainList.forEach(function (data) {
                finalSelectedRows.push(data);
            });
            cmp.set("v.selectedRows", finalSelectedRows);

            if (bool) {
                cmp.set("v.isToggleCalled", true);
                cmp.set("v.onToggle", true);
            }
        }
        if (event.getParam('isExpanded')) {
            helper.onRowSelectionHandler(cmp, event, helper);
        }
    },
    invoiceProcessHandler: function (cmp, event, helper) {
        let sRows = cmp.find("mytree").getSelectedRows();
        let invoiceStatus = cmp.get("v.invoiceStatus");
        let selectedRows = [];

        if (sRows.length != 0) {
            for (let key in sRows) {
                if (invoiceStatus != "No Invoice") {
                    if (sRows[key].name != undefined && sRows[key].name != "" && sRows[key].invoiceStatus === invoiceStatus) {
                        selectedRows.push(sRows[key].name);
                    }
                }
                else {
                    if (sRows[key].rentId != undefined && sRows[key].rentId != "") {
                        selectedRows.push(sRows[key].rentId);
                    }
                    if (sRows[key].utilizationId != undefined && sRows[key].utilizationId != "") {
                        selectedRows.push(sRows[key].utilizationId);
                    }
                }
            }
            if (selectedRows.length != 0) {
                helper.processInvoiceRecords(cmp, event, helper, selectedRows);
            }
        }
        else {
            helper.handleErrors("Please select atleast one record to continue.", "Warning");
        }
    },
    toggleDisplayActions: function (cmp, event, helper) {
        let viewSentInvoice = cmp.get("v.viewSentInvoice");
        if (viewSentInvoice) {
            cmp.set("v.displayActions", !viewSentInvoice);
        }
        else {
            cmp.set("v.displayActions", !viewSentInvoice);
        }
    }
})