({
    searchHelper: function (component, event, getInputkeyWord) {
        // call the apex class method
        let action = component.get("c.fetchLookUpValues");
        // set param to method
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName': component.get("v.objectAPIName"),
            'whereClause': component.get("v.whereClause"),
            'additionalFields': component.get("v.aditionalFields"),
            'whereClause2': component.get("v.whereClause2")
        });
        // set a callBack
        action.setCallback(this, function (response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                this.applyStyle(component); // This function handle the load of the list within a table based on elementId
                const storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.}
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
            else {
                console.log(response.getError());
            }
        });
        // enqueue the Action
        $A.enqueueAction(action);
    },
    applyStyle: function (component) {
        var elementId = component.get("v.elementId");
        var windowHeight = window.innerHeight;
        var ulList = component.find('ulList');
        if (elementId !== undefined && elementId !== ''){
            var elem = document.getElementById('' + elementId).getBoundingClientRect();
            if (elem != undefined && ulList != undefined){
                var applyClass = ((windowHeight - elem.bottom) > 190);
                if (applyClass) {
                    $A.util.removeClass(ulList, "reverse");
                } else {
                    $A.util.addClass(ulList, "reverse");
                }
            }
        }
    }
})