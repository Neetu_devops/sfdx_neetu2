({
    showMessage : function(message, type, mode) {
        var toastEvent = $A.get("e.force:showToast");              
        toastEvent.setParams({  
            mode: mode,
            message: message,                   
            type: type
        }); 
        toastEvent.fire(); 
    },
    
    
    sortData : function(component, helper, fieldName, sortDirection) {
        console.log('fieldName:', fieldName);
        var columns = component.get("v.columns");
        var data = component.get("v.commercialTermData");
        
        //function to return the value stored in the field
        var key = function(a) { 
            return a.objRecord[fieldName];
        }
        var reverse = sortDirection == 'asc' ? 1 : -1;
        
        // to handel number/currency type fields 
        for(var i=0; i<columns.length; i++) {
            if(fieldName == columns[i].fieldName) {
                if(columns[i].type.toLowerCase() == 'integer' || columns[i].type.toLowerCase() == 'percent' || columns[i].type.toLowerCase() == 'currency') {                    
                    data.sort(function(a,b){
                        var a = key(a) ? key(a) : '';
                        var b = key(b) ? key(b) : '';
                        return reverse * ((a>b) - (b>a));
                    }); 
                }
                else {
                    // to handel text type fields 
                    data.sort(function(a,b) { 
                        //To handle null values , uppercase records during sorting
                        var a = key(a) ? key(a).toLowerCase() : '';
                        var b = key(b) ? key(b).toLowerCase() : '';
                        return reverse * ((a>b) - (b>a));
                    });    
                }
                
                //set sorted data to dataForPagination attribute
                component.set("v.commercialTermData", data);
                break;
            }
        }
        
        component.set("v.showSpinner", false);
    },

    
    //Show the error toast when any error occured
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        
        if(errors) {
            errors.forEach( function (error) {
                //top-level error. There can be only one
                if(error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                
                //page-level errors (validation rules, etc)
                if(error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    }); 
                }
                
                if(error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for(var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList) {    
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    }
                }
            });
        }
    }
})