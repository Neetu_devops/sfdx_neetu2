<aura:component controller="CommercialTermRelatedListController" access="global">
    <!-- Attributes -->
    <aura:attribute name="selectedRecordId" type="String" />
    <aura:attribute name="dealId" type="String" />   
    <aura:attribute name="recordName" type="String" />
    <aura:attribute name="actionType" type="String" />
    <aura:attribute name="relatedListLabel" type="String" />
    <aura:attribute name="dealTypeField" type="String" />
    <aura:attribute name="commercialTermTypeField" type="String" />
    <aura:attribute name="ctListSize" type="Integer" /> 
    <aura:attribute name="recordIndex" type="Integer" /> 
    <aura:attribute name="columns" type="List" />
    <aura:attribute name="commercialTermData" type="List" />
    <aura:attribute name="items" type="List" />
    <aura:attribute name="mslData" type="sObject[]" />
    <aura:attribute name="eaList" type="List" />
    <aura:attribute name="showSpinner" type="Boolean" />
    <aura:attribute name="createNewRecord" type="Boolean" default="false" />    
    <aura:attribute name="confirmDeleteSection" type="Boolean" default="false" />
    <aura:attribute name="showAssets" type="Boolean" /> 
    <aura:attribute name="showManualSerialNumber" type="Boolean" /> 
    <aura:attribute name="showAssetsInDealSelector" type="Boolean"/>
    <aura:attribute name="openRecordPage" type="Boolean" access="global"/>
    <aura:attribute name="deleteAIDOnCTDeletion" type="Boolean" access="global" />
    
    <!-- Handlers -->
    <aura:handler name="init" value="{!this}" action="{!c.doInit}" />
    <aura:handler name="deleteRecord" event="c:RecordDeleteEvent" action="{!c.deleteSelectedRecord}" />
    <aura:registerEvent name="refreshRelatedList" type="c:RelatedListRefreshEvent" />
    
    <!-- Custom style -->
    <aura:html tag="style">
        .dataTable .slds-button:focus {
        box-shadow: none !important;
        }
    </aura:html>
    
    <!-- Body -->
    <table class="slds-table slds-table_cell-buffer slds-no-row-hover slds-table_fixed-layout slds-table_bordered dataTable">
        <thead id="tableTh">
            <tr>                   
                <aura:iteration items="{!v.columns}" var="header">
                    <th class="slds-text-title_caps" scope="col">
                        <div class="slds-grid slds-has-flexi-truncate slds-p-top_xxx-small slds-p-bottom_xxx-small">
                            <span class="slds-truncate" title="{!header.label}">{!header.label}</span>
                            <span class="sortIcons" data-id="{!header.fieldName}">
                                <lightning:buttonIcon iconName="{!'utility:' + header.sortIcon}" 
                                                      alternativeText="ascending" 
                                                      variant="bare" 
                                                      size="small" 
                                                      class="slds-m-left_xx-small" 
                                                      onclick="{!c.sortColumns}" /> 
                            </span>
                        </div>
                    </th>
                </aura:iteration>
            </tr>
        </thead>
        <tbody id="tableTr">
            <aura:iteration items="{!v.commercialTermData}" var="data" indexVar="index">
                <tr>
                    <aura:iteration items="{!v.columns}" var="fieldName">
                        <td scope="col">
                            <c:RelatedListDataTableRow field="{!fieldName}" 
                                                       rowData="{!data.objRecord}"  />                             
                        </td>
                    </aura:iteration> 
                </tr>
                
                <tr class="assets">
                    <td>
                        <div class="slds-p-bottom_xx-small" data-id="{!index}">
                            <lightning:buttonIcon iconName="utility:edit" 
                                                  alternativeText="Edit" 
                                                  variant="bare" 
                                                  size="small" 
                                                  onclick="{!c.editSelectedRecord}" />
                            <lightning:buttonIcon iconName="utility:copy" 
                                                  alternativeText="Clone" 
                                                  variant="bare" 
                                                  size="small" 
                                                  onclick="{!c.cloneSelectedRecord}" />
                            <lightning:buttonIcon iconName="utility:delete" 
                                                  alternativeText="Delete" 
                                                  variant="bare" 
                                                  size="small" 
                                                  onclick="{!c.confirmDelete}" />                                
                        </div>
                    </td>
                    <aura:if isTrue="{!or(data.mslList.length > 0, data.msnList.length > 0)}">
                        <td colspan="{!v.columns.length - 1}">
                            <span class="slds-text-title_caps slds-truncate assetsTitle" title="Assets included in this Commercial Terms">Assets</span>
                            
                            <div class="wrapText" data-id="{!index}">
                                <aura:if isTrue="{!data.mslList.length > 0}">
                                    <aura:iteration items="{!data.mslList}" var="mslRecord" indexVar="mslIndex">
                                        <aura:if isTrue="{!mslIndex == 0}">
                                            {!mslRecord.Name}
                                            <aura:set attribute="else">
                                                {! ', ' + mslRecord.Name}
                                            </aura:set>
                                        </aura:if>
                                    </aura:iteration>
                                    
                                    <aura:set attribute="else">
                                        <aura:iteration items="{!data.msnList}" var="msnRecord" indexVar="msnIndex">
                                            <aura:if isTrue="{!msnIndex == 0}">
                                                {!msnRecord.MSN_E__c}
                                                <aura:set attribute="else">
                                                    {! ', ' + msnRecord.MSN_E__c}
                                                </aura:set>
                                            </aura:if>
                                        </aura:iteration>                                            
                                    </aura:set>
                                </aura:if>
                                
                                <aura:if isTrue="{!and(data.mslList.length > 0, data.msnList.length > 0)}">
                                    <aura:iteration items="{!data.msnList}" var="msnRecord">
                                        {! ', ' + msnRecord.MSN_E__c}
                                    </aura:iteration>
                                </aura:if>
                            </div>
                        </td>                  
                    </aura:if>
                </tr>
            </aura:iteration>          
        </tbody>
    </table>
    
    <aura:if isTrue="{!v.createNewRecord}">
        <c:CreateCommercialTerm recordId="{!v.dealId}" 
                                selectedRecordId="{!v.selectedRecordId}"
                                createNewRecord="{!v.createNewRecord}" 
                                recordName="{!v.recordName}" 
                                eaList="{!v.eaList}" 
                                lstSelectedRecords="{!v.mslData}" 
                                relatedListLabel="{!v.relatedListLabel}" 
                                actionType="{!v.actionType}" 
                                items="{!v.items}" 
                                dealTypeField="{!v.dealTypeField}"
                                commercialTermTypeField="{!v.commercialTermTypeField}"
                                showAssets="{!v.showAssets}"
                                showManualSerialNumber="{!v.showManualSerialNumber}"
                                showAssetsInDealSelector="{!v.showAssetsInDealSelector}"
                                openRecordPage="{!v.openRecordPage}" />
    </aura:if>
    
    <aura:if isTrue="{!v.confirmDeleteSection}">
        <c:RecordConfirmDeleteComponent confirmDeleteSection="{!v.confirmDeleteSection}" 
                                        relatedListLabel="{!v.relatedListLabel}" />
    </aura:if>
</aura:component>