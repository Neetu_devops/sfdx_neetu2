({
    doInit : function (component, event, helper) {
        setTimeout(function() {
            var tableBody = document.getElementById('tableTr'); 
            let tableTh = document.getElementById('tableTh');
            if(tableBody && tableTh) {
                tableTh.style.width = tableBody.clientWidth + 'px';             
            }
        }, 250);
    },
    
    
    sortColumns : function (component, event, helper) {
        var fieldName = event.target.dataset.id; 
        var headers = component.get("v.columns");
        var sortDirection;
        
        headers.map(function(data, index) {
            if(data.fieldName == fieldName) {
                if(data.sortIcon == 'arrowdown') {
                    sortDirection = 'asc';
                    Object.assign(data, {"sortIcon" : 'arrowup'});
                }
                else {
                    sortDirection = 'desc';
                    Object.assign(data, {"sortIcon" : 'arrowdown'});
                }
            }
        });                      
        
        component.set("v.columns", headers);
        component.set('v.showSpinner', true);
        helper.sortData(component, helper, fieldName, sortDirection);
    },
    
    
    editSelectedRecord : function(component, event, helper) { 
        var recordIndex = event.target.dataset.id;
        var rows = component.get('v.commercialTermData');
        var recordId = rows[recordIndex].objRecord.Id;
        var recordName = rows[recordIndex].objRecord.Name;
        var mslList = rows[recordIndex].mslList;
        var msnList = rows[recordIndex].msnList;
        var eaList = rows[recordIndex].eaList;
        var msnData = [];
        
        for(var i=0; i<msnList.length; i++) {
            msnData.push({
                type: 'icon',
                label: msnList[i].Name,
                name: msnList[i].Id,
                iconName: 'custom:custom30',
            });
        }
        
        component.set("v.selectedRecordId", recordId);
        component.set("v.recordName", recordName);  
        component.set("v.eaList", eaList); 
        component.set("v.mslData", mslList);  
        component.set("v.items", msnData);  
        component.set("v.actionType", 'Edit');
        component.set("v.createNewRecord", true);
    },
    
    
    cloneSelectedRecord : function(component, event, helper) {   
        var recordIndex = event.target.dataset.id;
        var rows = component.get('v.commercialTermData');
        var recordId = rows[recordIndex].objRecord.Id;
        var mslList = rows[recordIndex].mslList;
        var msnList = rows[recordIndex].msnList;
        var msnData = [];
        
        for(var i=0; i<msnList.length; i++) {
            msnData.push({
                type: 'icon',
                label: msnList[i].Name,
                name: msnList[i].Id,
                iconName: 'custom:custom30',
            });
        }
        
        component.set("v.selectedRecordId", recordId);
        component.set("v.mslData", mslList);  
        component.set("v.items", msnData);  
        component.set("v.actionType", "Clone");
        component.set("v.createNewRecord", true);
    },
    
    
    confirmDelete : function(component, event, helper) {
        component.set("v.confirmDeleteSection", true);   
        component.set("v.recordIndex", event.target.dataset.id);
    },
    
    
    deleteSelectedRecord : function(component, event, helper) {
        var recordIndex = component.get("v.recordIndex");       
        var rows = component.get('v.commercialTermData');
        var recordId = rows[recordIndex].objRecord.Id;
        var recordName = rows[recordIndex].objRecord.Name;
        
        component.set('v.showSpinner', true);
        
        var action = component.get("c.deleteCommercialTerm");
        action.setParams({
            'commercialTermId' : recordId,
            'showAssetsInDealSelector': component.get("v.showAssetsInDealSelector"),
            'deleteAIDOnCTDeletion': component.get("v.deleteAIDOnCTDeletion")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                rows.splice(recordIndex, 1);
                
                component.set('v.commercialTermData', rows);    
                component.set('v.ctListSize', rows.length); 
                component.set('v.showSpinner', false);
                
                helper.showMessage(component.get("v.relatedListLabel") + ' "' + recordName + '" was deleted', 'success');
                
                var refreshEvt = $A.get("e.c:CommercialTermListRefreshEvent");
            	refreshEvt.fire();

                $A.get('e.force:refreshView').fire();
            }
            else if(state == "ERROR"){
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        
        $A.enqueueAction(action);                  
    }
})