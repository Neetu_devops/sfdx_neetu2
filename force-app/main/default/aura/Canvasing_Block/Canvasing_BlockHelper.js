({
  
    getMarketingType : function(component) {
        var self = this;
        var action = component.get('c.getMarketingRepType');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getMarketingRepType : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getMarketingRepType values " +allValues);
                if(allValues != null) {
                	component.set("v.marketingRepTypePicklist" , allValues);
                    if(allValues != 'Picklist') {
                        this.getCurrentUserInfo(component);
                    }
                }
            }
            self.getShowListState(component);
        });
        $A.enqueueAction(action);
    },
    
    isCmpLoadonPhone: function() {
        let deviceType = $A.get("$Browser.formFactor");
        console.log('deviceType : '+deviceType);
        if(deviceType === 'DESKTOP'){
            return false;
        }
        return true;
    },
    
    getLeasesForOperator: function(component,oprId,recordType) {
        console.log('c.getLeasesForOperator-------' + oprId);

        var action = component.get("c.getLeasesForOperator");
                action.setParams({
                    "operatorId" :oprId,
                    "recordType":recordType
                });
       
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log("getLeasesForOperator : state "+state);
                    component.set("v.isLoading", false);
                    if (state === "SUCCESS") {
                        var allValues = response.getReturnValue();
                        console.log("getLeasesForOperator values " + allValues);
                        if(allValues != null) {
                           // console.log("getLeasesForOperator values " +JSON.stringify(allValues));
                            component.set("v.leaseArray", allValues);
                            var leaseArray= component.get("v.leaseArray");
                            var leaseIdArray = component.get("v.leaseIdArray");
                            for ( var i in allValues) {
                                leaseIdArray.push(i.Id);
                            }
                            console.log('leaseIdArray-----'+ leaseIdArray);
                            component.set('v.leaseIdArray' , leaseIdArray);
                        }
                    }
                            
                });
                $A.enqueueAction(action);

    },
    getCurrentUserInfo : function(component) {
        var self = this;
        var action = component.get('c.getCurrentUser');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getCurrentUserInfo : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getCurrentUserInfo values " +response.getReturnValue());
                if(allValues != null) {
                	component.set("v.marketingRep" , allValues);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getShowListState : function(component) {
        var self = this;
        var action = component.get('c.getShowListVisibility');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getShowListState : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getShowListState values " +allValues);
                component.set("v.showListState" , allValues);
            }
            self.getDealGroupState(component);
        });
        $A.enqueueAction(action);
    },
    
    getDealGroupState : function(component) {
        var self = this;
        var action = component.get('c.getDealGroupVisibility');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getDealGroupState : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getDealGroupState values " +allValues);
                component.set("v.showDealGroup" , allValues);
            }
            self.fetchNameSpacePrifix(component);
        });
        $A.enqueueAction(action);
    },

    getILData : function(component, event, helper) {
        //this.showSpinner(component);
        var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
        var namespace = component.get("v.nameSpacePrifix");
        var param;
        
        // Check for namespaceprifix if namespace is present use leaseworks__ else c__
        if(namespace != '' && namespace != null && namespace != undefined){ param = namespace+'ILid'; }
        else{ param = 'c__ILid'; }
        
        var result = decodeURIComponent((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        console.log('param: '+result);
		    console.log('currentUserId:', currentUserId);
        console.log('namespace:'+ namespace + ' result:'+result);
        
        if(result != null && result != ''){
            component.set("v.ILid", result);
            component.set("v.isILPage", true);
            

            var action1 = component.get("c.getILData"); 
            action1.setParams({
                recordId : result,
                userId : currentUserId
            })
            action1.setCallback(this,function(response){
                if(response.getState()=="SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('Final Result: ', JSON.parse(JSON.stringify(result)));

                    if(result.copySummaryField){
                        if (result.descriptionValue != null && result.descriptionValue != '' && result.descriptionValue != undefined){
                            component.set('v.description', result.descriptionValue);
                        }
                    }
                    // Store the result in a variable and use that variable instead of response.getReturnValue().
                    var result1 = response.getReturnValue().IL;
                    if(result1 != undefined && result1 != '' && result1 != null){
                        if(result1.Related_To_Aircraft__c != undefined && result1.Related_To_Aircraft__c !=''&& result1.Related_To_Aircraft__c!=null){
                            component.set('v.AssetRecordType',result1.Related_To_Aircraft__r.RecordType.Name);
                            component.set('v.AircraftSelectedRecords', result1.Related_To_Aircraft__r);
                        }
                        
                        var result2 = response.getReturnValue().lookupList;
                        if(result2 != undefined && result2 != ''){
                            component.set('v.lstSelectedRecords', result2);
                        } 
                        
                        if(result1.Related_To_Operator__c != null && result1.Related_To_Operator__c != '' && result1.Related_To_Operator__c != undefined){
                            component.set('v.ProspectType', 'Airline');
                            component.set('v.customerArray', result2);
                        }
                        else{
                            if(result1.Counterparty__c != null && result1.Counterparty__c != '' && result1.Counterparty__c != undefined){
                                component.set('v.ProspectType', 'Lessor');
                                component.set('v.lessorArray', result2);
                            }
                            else if(result1.Related_To_Counterparty__c != null && result1.Related_To_Counterparty__c != '' && result1.Related_To_Counterparty__c != undefined){
                                component.set('v.ProspectType', 'Counterparty');
                                component.set('v.counterPartyArray', result2);
                            }
                        }
                        if(result1.Deal_Type_Discussed__c != null && result1.Deal_Type_Discussed__c != '' && result1.Deal_Type_Discussed__c != undefined){
                            component.set("v.dealTypeDiscussed", result1.Deal_Type_Discussed__c);
                        }
                        if(result1.Summary__c != null && result1.Summary__c != '' && result1.Summary__c != undefined){
                            component.set("v.weeklySummary", result1.Summary__c);
                        }
                    }
                }
                else{
                    console.log('ERROR: '+response.getError());
                }
                //this.hideSpinner(component);
            });
            $A.enqueueAction(action1); 
        }
        else
            component.set('v.ProspectType', 'Airline');
    },
    
	helperCloseMethod : function(cmp, canvasIdVar,navigateVal) {
          console.log('helperCloseMethod : '+canvasIdVar+ ' : '+navigateVal);
          var isMobile = cmp.get("v.loadedInPhone");
          if(isMobile){
           if(canvasIdVar){
               if(navigateVal == '1'){
                   var navEvt = $A.get("e.force:navigateToSObject");
                   navEvt.setParams({
                       "recordId": canvasIdVar,
                       "slideDevName": "related"
                   });
                   navEvt.fire();
               }
               else if(navigateVal == '2'){
                   
                   var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": canvasIdVar
                    });
                    urlEvent.fire();
                }
                   else{
                       var homeEvent = $A.get("e.force:navigateToObjectHome");
                       homeEvent.setParams({
                           "scope": "Marketing_Activity__c"
                       });
                       homeEvent.fire(); 
                   }
           }    
        }
        else {
            
            let nameSpacePrifix = cmp.get("v.nameSpacePrifix");
            if(nameSpacePrifix == undefined || nameSpacePrifix == '' || nameSpacePrifix == null) { nameSpacePrifix = ''; }
            
            if(canvasIdVar!==null){
            	var finalURL = window.location.protocol +"//"+window.location.hostname+"/apex/"+nameSpacePrifix+"RedirectVF?paramId="+ canvasIdVar ;
            	window.open(finalURL ,"_self");
            }
            else{
                window.history.back();
            }
        }
    },
    
    throwErrorForKicks: function(cmp,message) {
        message = "Review all error messages below to correct your data.\n" +message;
        this.showErrorMsg(cmp,message);
        
    },

    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    setAssetArrays : function(component){
        var assetTypeArray = new Array();
        assetTypeArray.push({assetType:"", assetQuantity:1});
        component.set("v.engineTypeArray", assetTypeArray); // Assign Default Array
        component.set("v.aircraftTypeArray", assetTypeArray); // Assign Default Array
    },
    
    fetchNameSpacePrifix: function(cmp, event){
     //   this.showSpinner(cmp);
        const action = cmp.get("c.getNameSpacePrifix");
        action.setCallback(this,function(response){
            const state = response.getState();
            if(state === 'SUCCESS'){
                var result = response.getReturnValue();
                console.log("result: ", result);
                
                if(result != undefined && result != ''){
                    cmp.set("v.nameSpacePrifix",result.nameSpacePrefix);
                    cmp.set("v.showWeeklySummary",result.showWeeklySummary);
                    cmp.set("v.showMarketingRep", result.showMarketingRep);
                    cmp.set("v.showDescription", result.showDescription);
                                       
                    if(result.defaultAssetType != undefined && result.defaultAssetType != '') {
                        cmp.set('v.AssetRecordType', result.defaultAssetType);
                        cmp.set('v.assetType', result.defaultAssetType);
                    }
                    else {
                        if(cmp.get('v.AssetRecordType') == '' || cmp.get('v.AssetRecordType')== null || cmp.get('v.AssetRecordType')== undefined){
                            cmp.set('v.AssetRecordType', 'Aircraft');
                            cmp.set('v.assetType', 'Aircraft');
                        } 
						//commentted by sagar on March 23, 2020
                       /* else{
                            cmp.set('v.assetType', 'Engine');
                        }*/
                    }
                    
                    cmp.set('v.defaultRecordType', result.defaultRecordTypeName);
                    cmp.set('v.dealType', result.defaultRecordTypeName);
                    this.getILData(cmp, event);
                }
            }
            else if(state === 'ERORR'){
                return new Error('Error: '+response.error[0].message);
            }
            //this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
    },
    //LW-AKG - 17-03-2020
    dateToParseHelper : function (component, event, helper, dateToParse) {
        var dateToFormat = new Date(dateToParse); 
        var formattedDate = dateToFormat.getUTCFullYear() + '-' + (dateToFormat.getUTCMonth() + 1) + '-' + dateToFormat.getUTCDate(); // yyyy-mm-dd
        //console.log('formattedDate: ', formattedDate);
        return formattedDate;
    },
    //LW-AKG - 17-03-2020
    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
})