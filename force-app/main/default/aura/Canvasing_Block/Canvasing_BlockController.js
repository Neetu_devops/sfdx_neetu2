({
	initialize : function(component, event, helper) {
        console.log("CanvassingExampleController initialize(+)==>");
        helper.getMarketingType(component);
        component.set("v.loadedInPhone", helper.isCmpLoadonPhone());
        
        var today = new Date();
        //component.set("v.dealDate",todayDate);
		component.set('v.dealDate', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
        
        helper.setAssetArrays(component);
        component.set("v.otherType",""); // Assign Default Array
        
        console.log("CanvassingExampleController initialize(-)==>");
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.initialLoad", false);
                helper.hideSpinner(component);
            }), 7000
        );
    },
    
    handleassetTypeObjChange: function(component, event, helper) {
        var changeEventField = event.getParam('response');
        let msnATIdVar = component.get('v.msnATId');
        let assetTypeVar = component.get('v.assetType');
        let assetTypeArray = new Array();
        console.log("changeEventField", changeEventField);

     
        /**********************************************************************************/
        if(changeEventField == 'Asset_Type__c' || changeEventField == 'Engine_Type_new__c') {
            //Code to handle the Asset Quantity And Type Value selection and removal on User interaction.
            if(msnATIdVar == 'By Quantity and Type'){
                if(assetTypeVar === 'Aircraft') {
                    assetTypeArray = component.get("v.aircraftTypeArray");
                }
                else if(assetTypeVar === 'Engine') {
                    assetTypeArray = component.get("v.engineTypeArray");
                }
            }
            
            if(assetTypeArray.length > 0){
                for(let key in assetTypeArray){
                    if(key == assetTypeArray.length-1){
                        if(assetTypeArray[key].assetType != undefined && assetTypeArray[key].assetType != ''){
                            assetTypeArray.push({assetType:"", assetQuantity:1});
                        }
                    }
                    else{
                        if(assetTypeArray[key].assetType == undefined || assetTypeArray[key].assetType == ''){
                            assetTypeArray.splice(key,1);
                        }
                    }
                }
            }
            if(msnATIdVar == 'By Quantity and Type'){
                if(assetTypeVar === 'Aircraft') {
                    component.set("v.aircraftTypeArray",assetTypeArray);
                    assetTypeArray = new Array();
                    assetTypeArray.push({assetType:"", assetQuantity:1});
                    component.set('v.engineTypeArray',assetTypeArray);
                }
                else if(assetTypeVar === 'Engine') {
                    component.set("v.engineTypeArray",assetTypeArray);
                    assetTypeArray = new Array();
                    assetTypeArray.push({assetType:"", assetQuantity:1, assetVariant:""});
                    component.set("v.aircraftTypeArray",assetTypeArray);
                }
            }  
        }
        else {
            if(assetTypeVar != 'Aircraft' && assetTypeVar != 'Engine') {
                helper.setAssetArrays(component);
            }
            else if(msnATIdVar != 'By Quantity and Type'){
            	helper.setAssetArrays(component);
            }
        }
        
        if(msnATIdVar !== 'By Type' && (assetTypeVar !== 'Aircraft' || assetTypeVar !== 'Engine')){
            component.set("v.otherType","");// Changed on 23/4/2019
        }
    },
    
    onSelectChangeMsnATId : function(component, event, helper) {
        console.log("CanvassingExampleController.onSelectChangeMsnATId(+)==>");
    	var selected = component.find("InputSelectMsnATId").get("v.value");
        console.log("selected==>"+selected);
    	component.set("v.msnATId",selected);

    	var stringIdArray = [];
		for (var i = 0; i < 10; i++) {
			stringIdArray[i]=null;
        }
        component.set('v.msnIdArray',stringIdArray);
    	component.set('v.esnIdArray',stringIdArray);
		
        helper.setAssetArrays(component);
        component.set("v.otherType","");// Changed on 23/4/2019
        
        console.log("CanvassingExampleController.onSelectChangeMsnATId(-)==>");
    },
    
    handleAssetTypeChange: function(component, event, helper) {
    	helper.showSpinner(component);
    },
    
    handleMSNChange: function(component, event, helper) {
    	helper.hideSpinner(component);
    },
    
    
    handleDealStatusChange : function(component, event, helper) {
        component.set("v.isDealStatusLoaded", false);
        console.log("handleDealStatusChange isDealStatusLoaded: "+component.get("v.isDealStatusLoaded")+ " isDeliveryTypeLoaded: "+ component.get("v.isDeliveryTypeLoaded")+ " isProspectTypeLoaded: " +component.get("v.isProspectTypeLoaded"));
        if(component.get("v.initialLoad") == false && 
           component.get("v.isDealStatusLoaded") == false && 
           component.get("v.isDeliveryTypeLoaded") == false &&
           component.get("v.isProspectTypeLoaded") == false)
            helper.hideSpinner(component);  
        console.log("handleDealStatusChange ");
    },
    
    handleDeliveryTypeChange : function(component, event, helper) {
        component.set("v.isDeliveryTypeLoaded", false);
        
        console.log("handleDeliveryTypeChange isDealStatusLoaded: "+component.get("v.isDealStatusLoaded")+ " isDeliveryTypeLoaded: "+ component.get("v.isDeliveryTypeLoaded")+ " isProspectTypeLoaded: " +component.get("v.isProspectTypeLoaded"));
        
        if(component.get("v.initialLoad") == false && 
           component.get("v.isDealStatusLoaded") == false && 
           component.get("v.isDeliveryTypeLoaded") == false &&
           component.get("v.isProspectTypeLoaded") == false)
            helper.hideSpinner(component);  
        console.log("handleDeliveryTypeChange "); 
    },
    
    handleProspectTypeChange : function(component, event, helper) {
        var assetType = component.get("v.ProspectType");
        console.log('assetType: ',assetType);
        component.set("v.isProspectTypeLoaded", false);
        
        console.log("handleProspectTypeChange isDealStatusLoaded: "+component.get("v.isDealStatusLoaded")+ " isDeliveryTypeLoaded: "+ component.get("v.isDeliveryTypeLoaded")+ " isProspectTypeLoaded: " +component.get("v.isProspectTypeLoaded"));
        
        if(component.get("v.initialLoad") == false && 
           component.get("v.isDealStatusLoaded") == false && 
           component.get("v.isDeliveryTypeLoaded") == false &&
           component.get("v.isProspectTypeLoaded") == false)
            helper.hideSpinner(component);   
        console.log("handleProspectTypeChange "); 
        var arr = [];
        if(assetType == 'Airline'){
            component.set("v.lessorArray",arr);
            component.set("v.lessorIdArray",arr);
            component.set("v.lessorNameArray",arr);
            component.set("v.counterPartyArray",arr);
            component.set("v.counterPartyIdArray",arr);
            component.set("v.counterPartyNameArray",arr);
        }else if(assetType == 'Lessor'){
            component.set("v.customerArray",arr);
            component.set("v.customerIdArray",arr);
            component.set("v.customerNameArray",arr);
            component.set("v.counterPartyArray",arr);
            component.set("v.counterPartyIdArray",arr);
            component.set("v.counterPartyNameArray",arr);
        }else{
            component.set("v.customerArray",arr);
            component.set("v.customerIdArray",arr);
            component.set("v.customerNameArray",arr);
            component.set("v.lessorArray",arr);
            component.set("v.lessorIdArray",arr);
            component.set("v.lessorNameArray",arr);
        }
    },
    
    handleDealTypeChange : function(component, event, helper) {
        var dealType = component.get("v.dealType");
        
        var arr =[];
        
        
        if(dealType == 'Lease Extension'){
            component.set("v.lessorArray",arr);
            component.set("v.lessorIdArray",arr);
            component.set("v.lessorNameArray",arr);
            
            component.set("v.counterPartyArray",arr);
            component.set("v.counterPartyIdArray",arr);
            component.set("v.counterPartyNameArray",arr);
            
            component.set("v.customerArray",arr);
            component.set("v.customerIdArray",arr);
            component.set("v.customerNameArray",arr);
        }
        
        if(component.get("v.initialLoad") == false)
             helper.showSpinner(component);
        component.set("v.isDealStatusLoaded", true);
        component.set("v.isDeliveryTypeLoaded", true);
        console.log("isILPage: "+component.get("v.isILPage"));
        console.log('component.get("v.dealType")----------'+ component.get("v.dealType"));
        if((component.get("v.dealType") != 'Payment Deferral Request' &&  component.get("v.dealType") != 'Lease Restructuring') && component.get("v.isILPage") == false)
            component.set("v.isProspectTypeLoaded", true);
        else
            component.set("v.isProspectTypeLoaded", false);

            console.log('component.set("v.isProspectTypeLoaded".....'  +component.get("v.isProspectTypeLoaded"));
        
    },
    
    onCheckCreateProject : function(component, event, helper) {
        console.log("CanvassingExampleController.onCheckCreateProject(+)==>");
        var checkBox = component.find("createProjectFlagId").get("v.value");
		console.log("checkBox=="+checkBox);
        if (checkBox){
        	component.set('v.createProjectFlag','TRUE');
        } else {
            component.set('v.createProjectFlag','FALSE');
			console.log("checkBox=false");
        }
		console.log("CanvassingExampleController.onCheckCreateProject(-)==>");
    },
    
	callSave : function(component, event, helper) {
        helper.showSpinner(component);
        console.log("AddmultipleContacts callSave(+)==>");
        var errorMessage="";
        var emptyString1= ",";
        var dealDateVar = component.get('v.dealDate');
        console.log("dealDateVar="+dealDateVar);
        var ILid = component.get("v.ILid");
        var marketingRepVar = component.get('v.marketingRep');
        if(component.get("v.showMarketingRep") == 'OFF') {
            marketingRepVar = '';
        }
        console.log("marketingRepVar="+marketingRepVar);
        var dealTypeVar = component.get('v.dealType');
        console.log("dealTypeVar="+dealTypeVar);
        
        var dealGroupArray = component.get('v.dealGroup');
        console.log("dealGroupVar=",dealGroupArray);
        var dealGroupIdArrayVar = new Array();
        var dealGroupNameArrayVar = new Array();
        for (var key in dealGroupArray ){
            if(dealGroupArray[key] != undefined){
                if(!dealGroupIdArrayVar.includes(dealGroupArray[key].Id)){
                    dealGroupIdArrayVar.push(dealGroupArray[key].Id);
                    dealGroupNameArrayVar.push(dealGroupArray[key].Name);
                }
            }
        }
        component.set("v.dealGroupIdArrayVar",dealGroupIdArrayVar);
        component.set("v.dealGroupNameArrayVar",dealGroupNameArrayVar);
        console.log('dealgroupId array'+dealGroupIdArrayVar);
        console.log('dealgroupName array'+dealGroupNameArrayVar);

        var dealStatusVar = component.get('v.dealStatus');
        console.log("dealStatusVar="+dealStatusVar);
        
        var deliveryTypeVar = component.get('v.deliveryType');
        console.log("deliveryTypeVar="+deliveryTypeVar);
        var assetTypeVar = component.get('v.assetType');
        console.log("assetTypeVar="+assetTypeVar);
        
        var customerArray = component.get('v.customerArray');
        console.log("customerArray: ",customerArray);
        var customeridArrayVar = new Array();
        var customerNameArrayVar = new Array();
        for(var key in customerArray){
            if(customerArray[key] != undefined){
                if(!customeridArrayVar.includes(customerArray[key].Id)){
                    customeridArrayVar.push(customerArray[key].Id);
                    customerNameArrayVar.push(customerArray[key].Name);
                }
            }
        }
        component.set("v.customerIdArray",customeridArrayVar);
        component.set("v.customerNameArray",customerNameArrayVar);
        console.log("customeridArrayVar="+customeridArrayVar);
        console.log("customerNameArrayVar="+customerNameArrayVar);


        
        var operatorIdVar = component.get('v.operatorId');
        console.log("operatorIdVar="+operatorIdVar);

       
        var rentDeferralObj = component.get('v.rentDeferral');
        var rentDeferralIdVar ='';
        if(rentDeferralObj != undefined) {
        console.log('rentDeferralObj------'+ rentDeferralObj);
         rentDeferralIdVar = rentDeferralObj.Id;
        console.log("rentDeferralIdVar="+rentDeferralIdVar);
        }
        var lessorArray = component.get('v.lessorArray');
        console.log("lessorArray: ",lessorArray);
        var lessorIdArrayVar = new Array();
        var lessorNameArrayVar = new Array();
        for(var key in lessorArray){
            if(lessorArray[key] != undefined){
                if(!lessorIdArrayVar.includes(lessorArray[key].Id)){
                    lessorIdArrayVar.push(lessorArray[key].Id);
                    lessorNameArrayVar.push(lessorArray[key].Name);
                }
            }
        }
        component.set("v.lessorIdArray",lessorIdArrayVar);
        component.set("v.lessorNameArray",lessorNameArrayVar);
        console.log("lessorIdArrayVar="+lessorIdArrayVar);
        console.log("lessorNameArrayVar="+lessorNameArrayVar);

        var counterPartyArray = component.get('v.counterPartyArray');
        console.log("counterPartyArray: ",counterPartyArray);
        var counterPartyIdArrayVar = new Array();
        var counterPartyNameArrayVar = new Array();
        for(var key in counterPartyArray){
            if(counterPartyArray[key] != undefined){
                if(!counterPartyIdArrayVar.includes(counterPartyArray[key].Id)){
                    counterPartyIdArrayVar.push(counterPartyArray[key].Id);
                    counterPartyNameArrayVar.push(counterPartyArray[key].Name);
                }
            }
        }
        component.set("v.counterPartyIdArray",counterPartyIdArrayVar);
        component.set("v.counterPartyNameArray",counterPartyNameArrayVar);
        console.log("counterPartyIdArrayVar="+counterPartyIdArrayVar);
        console.log("counterPartyNameArrayVar="+counterPartyNameArrayVar);
        
        var msnATIdVar = component.get('v.msnATId');
        console.log("msnATIdVar="+msnATIdVar);
        
        var msnArray = component.get('v.AircraftSelectedRecords');
        console.log("msnArray: ",msnArray);
        var msnIdArrayVar = new Array();
        var msnNameArrayVar = new Array();
        for(var key in msnArray){
            if(msnArray[key] != undefined){
                if(!msnIdArrayVar.includes(msnArray[key].Id)){
                    msnIdArrayVar.push(msnArray[key].Id);
                    msnNameArrayVar.push(msnArray[key].Name);
                }
            }
        }
        component.set("v.msnIdArray",msnIdArrayVar);
        component.set("v.msnNameArray",msnNameArrayVar);
        console.log("msnIdArrayVar="+msnIdArrayVar);
        console.log("msnNameArrayVar="+msnNameArrayVar);
        
        var msnWFArray = component.get('v.msnWFArray');
        console.log("msnWFArray: ",msnWFArray);
        var msnWFIdArrayVar = new Array();
        var msnWFNameArrayVar = new Array();
        for(var key in msnWFArray){
            if(msnWFArray[key] != undefined){
                if(!msnWFIdArrayVar.includes(msnWFArray[key].Id)){
                    msnWFIdArrayVar.push(msnWFArray[key].Id);
                    msnWFNameArrayVar.push(msnWFArray[key].Name);
                }
            }
        }
        component.set("v.msnWFIdArray",msnWFIdArrayVar);
        component.set("v.msnWFNameArray",msnWFNameArrayVar);
        console.log("msnWFIdArrayVar="+msnWFIdArrayVar);
        console.log("msnWFNameArrayVar="+msnWFNameArrayVar);
        
        var esnArray = component.get('v.esnArray');
        console.log("esnArray: ",esnArray);
        var esnIdArrayVar = new Array();
        var esnNameArrayVar = new Array();
        for(var key in esnArray){
            if(esnArray[key] != undefined){
                if(!esnIdArrayVar.includes(esnArray[key].Id)){
                    esnIdArrayVar.push(esnArray[key].Id);
                    esnNameArrayVar.push(esnArray[key].Name);
                }
            }
        }
        component.set("v.esnIdArray",esnIdArrayVar);
        component.set("v.esnNameArray",esnNameArrayVar);
        console.log("esnIdArrayVar="+esnIdArrayVar);  
        console.log("esnNameArrayVar="+esnNameArrayVar); 


        var leaseArray = component.get('v.leaseArray');
        console.log("leaseArray: ",leaseArray);
        var leaseIdArrayVar = new Array();
       
        for(var key in leaseArray){
            if(leaseArray[key] != undefined){
                if(!leaseIdArrayVar.includes(leaseArray[key].Id)){
                    leaseIdArrayVar.push(leaseArray[key].Id);
                    
                }
            }
        }
        component.set("v.leaseIdArray",leaseIdArrayVar);
        console.log("leaseIdArrayVar="+leaseIdArrayVar);  
        

       
        
    
        var aircraftTypeArrayVar = component.get('v.aircraftTypeArray');
        // Code to filter out null values
        aircraftTypeArrayVar = aircraftTypeArrayVar.filter(function (data, index) {
            if(data.assetType != undefined && data.assetType != ''){ return data; }
        });
        console.log("aircraftTypeArrayVar=",JSON.parse(JSON.stringify(aircraftTypeArrayVar)));
        
        var engineTypeArrayVar = component.get('v.engineTypeArray');
        // Code to filter out null values
        engineTypeArrayVar = engineTypeArrayVar.filter(function (data, index) {
            if(data.assetType != undefined && data.assetType != ''){ return data; }
        });
        console.log("engineTypeArrayVar=",JSON.parse(JSON.stringify(engineTypeArrayVar)));
        
        var projectRecord = component.get('v.projectRecord');
        console.log("projectRecord: ",projectRecord);
        var projectIDVar = component.get('v.projectID');
        var projectNAMEVar = component.get('v.projectNAME');
        if(projectRecord != null && projectRecord != undefined && projectRecord != ''){
            projectIDVar = projectRecord.Id;
            projectNAMEVar = projectRecord.Name;
        }
        component.set("v.projectID",projectIDVar);
        component.set("v.projectNAME",projectNAMEVar);
        console.log("projectIDVar="+projectIDVar);  
        console.log("projectNAMEVar="+projectNAMEVar);

        var createProjectFlagVar = component.get('v.createProjectFlag');
        console.log("createProjectFlagVar="+createProjectFlagVar);
        var projectStatusVar = component.get('v.projectStatus');
        console.log("projectStatusVar="+projectStatusVar);

        var currentStatusVar = component.get('v.description');
        console.log("currentStatusVar="+currentStatusVar);
        var weeklySummaryVar = component.get('v.weeklySummary');
        console.log("weeklySummaryVar="+weeklySummaryVar);
        var serialNumberVar = component.get("v.serialNumber");
        console.log("serialNumberVar="+serialNumberVar);
        
        var otherType = component.get("v.otherType");
        console.log("otherType="+otherType);
        
        if(component.get("v.showMarketingRep") == 'REQUIRED' && (marketingRepVar==null || marketingRepVar=="")) errorMessage += 'Marketing Rep is a required field.\n';
        if(dealTypeVar==null || dealTypeVar=="") errorMessage += 'Deal Type is a required field.\n';
        if(dealStatusVar==null || dealStatusVar=="") errorMessage += 'Deal Status is a required field.\n';
        if(deliveryTypeVar==null || deliveryTypeVar=="") errorMessage += 'Delivery Type is a required field.\n';
        if(assetTypeVar==null || assetTypeVar=="") errorMessage += 'Asset Type is a required field.\n';
        console.log('dealTypeVar----'+dealTypeVar);
        if((dealTypeVar && dealTypeVar != 'Payment Deferral Request' && dealTypeVar!='Lease Restructuring' && dealTypeVar != 'Lease Extension') && (customeridArrayVar[0]==null || customeridArrayVar[0]=="") && (lessorIdArrayVar[0]==null || lessorIdArrayVar[0]=="") && (counterPartyIdArrayVar[0]==null || counterPartyIdArrayVar[0]=="" )) errorMessage += ' Please choose at least one valid Airline/Lessor/Counterparty.\n';
        if((customeridArrayVar[0]!=null) && (lessorIdArrayVar[0]!=null) && (counterPartyIdArrayVar[0]!=null) ) errorMessage += 'Please add the Prospect for this Marketing Activity by populating either Lessor, Airline or Counterparty.\n';
        if((dealTypeVar =='Payment Deferral Request' || dealTypeVar=='Lease Restructuring')  && (operatorIdVar==null ||operatorIdVar=="") )  errorMessage += 'Please select the Operator.\n';
        if((dealTypeVar =='Payment Deferral Request'||  dealTypeVar=='Lease Restructuring') && (leaseIdArrayVar==null || leaseIdArrayVar==""))   errorMessage += 'Please select the Lease.\n';
        if((dealTypeVar!='Payment Deferral Request' && dealTypeVar!='Lease Restructuring') && msnATIdVar=='From My Fleet' && (msnIdArrayVar[0]==null || msnIdArrayVar[0]=="" ) ) errorMessage += " Please choose at least one valid From My Fleet.\n" ;
        else if(msnATIdVar=='Attached Assemblies' && assetTypeVar=='Engine' && (esnIdArrayVar[0]==null || esnIdArrayVar[0]=="" ) ) errorMessage += " Please choose at least one valid Attached Assembly.\n" ;
        else if(msnATIdVar=='By Manual Serial Number' && (serialNumberVar==null || serialNumberVar=="" ) ) errorMessage += "By Manual Serial Number is a required field.\n" ;
        else if(msnATIdVar=='From World Fleet' && assetTypeVar=='Aircraft' && (msnWFIdArrayVar[0]==null || msnWFIdArrayVar[0]=="" ) ) errorMessage += " Please choose at least one valid From World Fleet.\n" ;
        else if(msnATIdVar=='From World Fleet' && assetTypeVar!='Aircraft' ) errorMessage += " Field MSN/ESN or Type? : Please select either ESN or Engine Type.\n" ;
        else if(msnATIdVar=='By Quantity and Type' && assetTypeVar=='Aircraft' && (aircraftTypeArrayVar[0]==null || aircraftTypeArrayVar[0]=="" ) ) errorMessage += " Please choose at least one valid Aircraft Quantity and Type.\n" ;
        else if(msnATIdVar=='By Quantity and Type' && assetTypeVar=='Engine' && (engineTypeArrayVar[0]==null || engineTypeArrayVar[0]=="" ) ) errorMessage += " Please choose at least one Engine Quantity and Type.\n" ;
        else if(msnATIdVar=='By Type' && (assetTypeVar!='Engine' && assetTypeVar!='Aircraft') && (otherType==null || otherType=="" ) ) errorMessage += " Please choose valid Type.\n" ;
        
        //if((msnIdArrayVar[0]==null || msnIdArrayVar[0]=="") && (aircraftTypeArrayVar[0]==null || aircraftTypeArrayVar[0]=="" ) && (engineTypeArrayVar[0]==null || engineTypeArrayVar[0]=="" )) errorMessage += msnATIdVar + " is a required field.\n" ;
        if(component.get("v.showDescription") == 'REQUIRED' && (currentStatusVar==null || currentStatusVar=="")) errorMessage += 'Description is a required field.\n';
        if(component.get("v.showWeeklySummary") == 'REQUIRED' && (weeklySummaryVar==null || weeklySummaryVar=="")) errorMessage += 'Weekly Summary Notes is a required field.\n';
		
        if(createProjectFlagVar=='FALSE' && (projectIDVar==null || projectIDVar=='') && !(projectNAMEVar==null || projectNAMEVar=='')) errorMessage += 'No matching project found. Either enable \'Create New Project\' checkbox or set it blank.\n';
		
        //Code to validate the Qunatity fields on the form - START
        var fields = component.find("fieldCheck");
        var allValid = true;
        if(fields !== undefined && fields.length === undefined){
            var value = fields.get("v.value");
            if(value == undefined || value == null || value == ''){ allValid = false; }
        }
        else{
            for(var key in fields){
                var inputCmp = fields[key];
                var value = inputCmp.get("v.value");
                if(value == undefined || value == null || value == ''){ allValid = false; }
            }
        }
        if(!allValid){ errorMessage += 'Please complete the required fields on the form.\n'; }
        //Code to validate the Qunatity fields on the form - END

        if(errorMessage!=null && errorMessage!=""){
            helper.throwErrorForKicks(component, errorMessage);
            helper.hideSpinner(component);
        }
        else{
            // Save it!
            
            var canvasIdVar;
            var showDealList =false;
            //LW-AKG - 17-03-2020
            
            //LW-AKG - 17-03-2020
           
            console.log('Save now.......');
           
            var marketingType = component.get("v.marketingRepTypePicklist");
            var marketingValue = '';
            if(marketingType == 'Picklist')
            	marketingValue = marketingRepVar;
            else {
                if(marketingRepVar != null && marketingRepVar != '' && marketingRepVar != undefined){
                    marketingValue = marketingRepVar.Id;
                }
            }
            
            console.log('marketingType '+marketingType + ' marketingValue: '+marketingValue);     
            
          /**  var leaseRentDefIdsVar =[ {'leaseArray_p': leaseIdArrayVar
                ,'rentDeferralArray_p':rentDeferralIdArrayVar }] ;  **/
        
            var leaseRentDefIdsVar =[ {'leaseArray_p': leaseIdArrayVar
                ,'rentDeferralId':rentDeferralIdVar}] ;
            console.log('leaseRentDefIdsVar-----', leaseRentDefIdsVar);

            var action = component.get("c.callSaveApex");
            
            action.setParams({ 
                dealDate_p : dealDateVar 	//LW-AKG - 17-03-2020
                ,marketingRep_p:marketingValue
                ,dealType_p:dealTypeVar
                ,dealGroup_p:dealGroupIdArrayVar
                ,dealGroupNameArray_p:dealGroupNameArrayVar
                ,dealStatus_p:dealStatusVar
                ,deliveryType_p:deliveryTypeVar
                ,assetType_p:assetTypeVar
                ,customerArray_p:customeridArrayVar
                ,customerNameArray_p:customerNameArrayVar
                ,lessorArray_p:lessorIdArrayVar
                ,lessorNameArray_p:lessorNameArrayVar
                ,counterpartyArray_p:counterPartyIdArrayVar
                ,counterpartyNameArray_p:counterPartyNameArrayVar
                ,msn_at_p:msnATIdVar
                ,msnArray_p:msnIdArrayVar
                ,msnNameArray_p:msnNameArrayVar
                ,esnArray_p:esnIdArrayVar
                ,esnNameArray_p:esnNameArrayVar
                ,msnWFArray_p:msnWFIdArrayVar 
                ,msnWFNameArray_p:msnWFNameArrayVar
                ,atArray_p:JSON.stringify(aircraftTypeArrayVar)
                ,engineArray_p:JSON.stringify(engineTypeArrayVar)
                ,projectPK_p:projectNAMEVar
                ,projectID_p:projectIDVar
                ,projectStatus_p:projectStatusVar                                  
                ,currentStatus_p:currentStatusVar
                ,weeklySummary_p:weeklySummaryVar
                ,serialNumber_p:serialNumberVar
                ,otherType_p:otherType
                ,ILid : ILid
                ,leaseRentDefIds:JSON.stringify(leaseRentDefIdsVar) 
                
              
            });
            action.setCallback(this,function(a){
                
                console.log('*******',a.getState());
                if(a.getState() === "ERROR"){
                    var errors = a.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "type": 'error',
                                "duration":' 5000',
                                "message": errors[0].message
                            });
                            toastEvent.fire();
                        }
                    }
					helper.hideSpinner(component);
                }
                console.log("success");
                var canvasMap = a.getReturnValue();
                var navigateVal;
                console.log("canvasIdVar=",canvasMap);
                if(canvasMap == null || canvasMap == undefined) {
                    helper.hideSpinner(component);
                    helper.showErrorMsg(component, "Unexpected error occured, please check with your system administrator.")
                    return;
                }
                if(canvasMap['ID'] != undefined || canvasMap['ID'] != null)
                { canvasIdVar = canvasMap['ID'];
                 navigateVal = '1';}
                if(canvasMap['MA_ID'] != undefined || canvasMap['MA_ID'] != null){
                    {  canvasIdVar = canvasMap['MA_ID'];
                     navigateVal = '1';}
                }else if(canvasMap['MA_List'] != undefined || canvasMap['MA_List'] != null ){
                    canvasIdVar = canvasMap['MA_List'];
                    navigateVal = '2';
                    console.log('canvasIdVar>>'+canvasIdVar);
                    showDealList =true;
                    var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": canvasIdVar
                        });
                        urlEvent.fire();
                   
                }
                console.log("canvasIdVar=" +canvasIdVar);
                console.log("navigateVal=" +navigateVal);
                if(canvasMap["ERRORID"]==="1"){
                    helper.throwErrorForKicks(component, canvasMap["ERROR"]);
                    helper.hideSpinner(component);
                }
                else if(!showDealList){
                    var errorDiv = component.find("errorDiv1");
                    $A.util.addClass(errorDiv, "slds-hide");
                    helper.helperCloseMethod(component,canvasIdVar,navigateVal);
                }
            });
            $A.enqueueAction(action);
        }
        console.log("AddmultipleContacts callSave(-)==>");
	},
    
	callCancel : function(component, event, helper) {
        console.log("callCancel=>");
        helper.helperCloseMethod(component,null);
        $A.get('e.force:refreshView').fire();
    },
    
    openOperatorModel : function(component, event, helper) {        
        var nameSpacePrifix = component.get("v.nameSpacePrifix");
        var searchOperators = component.find("searchOperators");
        component.set("v.customerListArrayCopy", component.get("v.customerListArray"));
        
        if(nameSpacePrifix == undefined || nameSpacePrifix == '' || nameSpacePrifix == null) { 
            nameSpacePrifix = 'c'; 
        }
        else { 
            nameSpacePrifix = nameSpacePrifix.slice(0,-2); 
        }
        
        if(searchOperators && !component.get("v.loadedInPhone")){
            searchOperators.destroy();
        }
        
        $A.createComponent(
            ""+nameSpacePrifix+":OperatorFinder", {
                "aura:id": "searchOperators",
                "proccessOperator": component.getReference("v.proccessOperator"),
                "selectedOperators": component.getReference("v.customerListArray"),
                "loadedInPhone": component.getReference("v.loadedInPhone"),
                "isCanvass": true
            }, 
            function(element, status, errorMessage) {
                if (status === "SUCCESS") {
                    if(!component.get("v.loadedInPhone")){ //Check if not loaaded on Mobile device
                        var targetCmp = component.find('searchMSNDiv');
                        var body = targetCmp.get("v.body");
                        body.push(element);
                        targetCmp.set("v.body", body);
                    }
                    else { 
                        component.find("overlayLib").showCustomModal({
                            body: element,
                            showCloseButton: false,
                            cssClass: "my-modal,my-custom-class,my-other-class, cCanvasing_Block",
                            closeCallback: function() {
                                $A.get("e.force:closeQuickAction").fire();
                            }
                        });
                    }
                }
            }
        );        
    },
    
    openModel: function(component, event, helper) {
        component.set("v.msnListArray", []);
        let nameSpacePrifix = component.get("v.nameSpacePrifix");
        if(nameSpacePrifix == undefined || nameSpacePrifix == '' || nameSpacePrifix == null) { nameSpacePrifix = 'c'; }
        else{ nameSpacePrifix = nameSpacePrifix.slice(0,-2); }
        
        let searchMSN = component.find("searchMSN");
        if(searchMSN && !component.get("v.loadedInPhone")){
            searchMSN.destroy();
        }
        $A.createComponent(
            ""+nameSpacePrifix+":MSNSearchandAdd",
            {
                "aura:id": "searchMSN",
                "proccessMSN": component.getReference("v.proccessMSN"),
                "selectedMSN": component.getReference("v.msnListArray"),
                "assetType" : component.getReference("v.assetType"),
                "loadedInPhone": component.getReference("v.loadedInPhone")
            },
            function(elemt, status, errorMessage){
                if (status === "SUCCESS") {
                    if(!component.get("v.loadedInPhone")){ //Check if not loaaded on Mobile device
                        var targetCmp = component.find('searchMSNDiv');
                        var body = targetCmp.get("v.body");
                        body.push(elemt);
                        targetCmp.set("v.body", body);
                    }
                    else { 
                        component.find("overlayLib").showCustomModal({
                            body: elemt,
                            showCloseButton: false,
                            cssClass: "my-modal,my-custom-class,my-other-class, cCanvasing_Block",
                            closeCallback: function() {
                                $A.get("e.force:closeQuickAction").fire();
                            }
                        });
                    }
                }
            }
        ); 
    },
    
    
    getLeaseIDArray:function(component, event, helper) {
       
        console.log('getLeaseIDArray'); 
        
        var leaseArray= component.get("v.leaseArray");
        var leaseIdArray = component.get("v.leaseIdArray");
       
        console.log('leaseArray----'+ leaseArray);
        if(leaseArray==''){
            var arr =[];
            component.set("v.leaseIdArray", arr);
            component.set("v.rentDeferral", arr);
           
        }else{
           
            for(var i=0; i<leaseArray.length; i++) {
                leaseIdArray.push('\'' + leaseArray[i].Id + '\'');
            }
            console.log('leaseIdArray----'+ leaseIdArray);
            component.set("v.leaseIdArray", leaseIdArray);
        }
        
        
      
    },

    getOperatorId : function(component, event, helper) {
       
           console.log('getOperatorId'); 
           var arr = [];
            var operatorObj = component.get("v.operator");
            var assetType = component.get("v.assetType");
            console.log('operatorObj---'+operatorObj);
            component.set("v.operatorId", operatorObj.Id);
            console.log('operatorObj.Id----'+ operatorObj.Id);
            if(operatorObj.Id==''){
                component.set("v.leaseArray", arr);
                component.set("v.leaseIdArray", arr);
                component.set("v.rentDeferral", arr);
            }else{
                    
                   helper.getLeasesForOperator(component,operatorObj.Id,assetType);            
            }
           
          
    },
    handleOperatorChange : function(component, event, helper) {
        var proccessOperator = component.get("v.proccessOperator");
        
        if(proccessOperator) {
            var searchOperators = component.find("searchOperators");
            var customerArray = component.get("v.customerArray");
            var customerListArray = component.get("v.customerListArray");
            var customerListArrayCopy = component.get("v.customerListArrayCopy");
            var customerIdArray = component.get("v.customerIdArray");
            var customerNameArray = component.get("v.customerNameArray");
            console.log('handleOperatorChange-----prospect'+ customerIdArray);
            var leaseArray = component.get("v.leaseArray");
            
            for(var i=0; i<customerListArrayCopy.length; i++) {
                var index = customerArray.indexOf(customerListArrayCopy[i]);
                if(index != -1) {
                    customerArray.splice(index, 1);
                }
            }
                
            for(var key in customerListArray) {
                if(customerListArray[key] != undefined) {
                    if(customerArray.length === 0) {
                        customerIdArray.push(customerListArray[key].Id);
                        customerNameArray.push(customerListArray[key].Name);
                        customerArray.push(customerListArray[key]);
                    }
                    else {
                        var addValue = true;
                        
                        for(var key2 in customerArray) {                            
                            if(customerArray[key2].Id === customerListArray[key].Id){
                                addValue = false;
                            }
                        }
                        
                        if(addValue) {
                            customerIdArray.push(customerListArray[key].Id);
                            customerNameArray.push(customerListArray[key].Name);
                            customerArray.push(customerListArray[key]);
                        }
                    }
                }
            }
            
            component.set("v.customerArray", customerArray);
            component.set("v.customerIdArray", customerIdArray);
            component.set("v.customerNameArray", customerNameArray);
            component.set("v.proccessOperator", false);
            
            if(searchOperators && !component.get("v.loadedInPhone")){
                searchOperators.destroy();
            }
        }
    },
    
    handleMSNChange: function(component, event, helper) {
        var proccessMSN = component.get("v.proccessMSN");
        if(proccessMSN){
            var msnArray = component.get("v.AircraftSelectedRecords");
            var msnListArray = component.get("v.msnListArray");
            var msnIdArrayVar = component.get("v.msnIdArray");
            var msnNameArrayVar = component.get("v.msnNameArray");
            for(var key in msnListArray){
                if(msnListArray[key] != undefined){
                    if(msnArray.length === 0){
                        msnIdArrayVar.push(msnListArray[key].Id);
                        msnNameArrayVar.push(msnListArray[key].Name);
                        msnArray.push(msnListArray[key]);
                    }
                    else{
                        var addValue = true;
                        for(var key2 in msnArray){
                            if(msnArray[key2].Id === msnListArray[key].Id){
                                addValue = false;
                            }
                        }
                        if(addValue){
                            msnIdArrayVar.push(msnListArray[key].Id);
                            msnNameArrayVar.push(msnListArray[key].Name);
                            msnArray.push(msnListArray[key]);
                        }
                    }
                }
            }
            component.set("v.AircraftSelectedRecords",msnArray);
            component.set("v.msnIdArray",msnIdArrayVar);
            component.set("v.msnNameArray",msnNameArrayVar);
            component.set("v.proccessMSN",false);
            var searchMSN = component.find("searchMSN");
            if(searchMSN && !component.get("v.loadedInPhone")){
                searchMSN.destroy();
            }
        }
    },
    handleDealGroupChange: function(component, event, helper) {
        var dealGroupInput = component.get("v.dealGroupChange");
        console.log('value input from end user for deal group is----'+dealGroupInput);
    }
    
     
})