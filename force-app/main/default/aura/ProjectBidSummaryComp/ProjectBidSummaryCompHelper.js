({
    convertArrayOfObjectsToCSV: function (component) {

        var aircraftData = component.get('v.aircraftData');
        var headerValues = aircraftData.fieldList;
        var csvStringResult;

        var propspectWithAircraftId = {};

        if (aircraftData.aircraftList != null && aircraftData.aircraftList != undefined && aircraftData.aircraftList.length > 0) {

            this.showSpinner(component);

            var counter, keys, columnDivider, lineDivider;
            csvStringResult = '';

            columnDivider = ',';
            lineDivider = '\n';

            csvStringResult += '"' + 'MSN' + '"';
            csvStringResult += columnDivider;
            aircraftData.aircraftList.forEach(function (record) {
                if (record.aircraft['MSN_Number__c'] != null && record.aircraft['MSN_Number__c'] != undefined) {
                    csvStringResult += '"' + '' + record.aircraft['MSN_Number__c'] + '"';
                } else {
                    csvStringResult += '"' + '' + '"';
                }
                csvStringResult += columnDivider;
            })
            csvStringResult += lineDivider;
            headerValues.forEach(function (headerItem) {
                csvStringResult += '"' + headerItem.label + '"';
                csvStringResult += columnDivider;
                aircraftData.aircraftList.forEach(function (record) {
                    if (record.aircraft[headerItem.apiName] != null && record.aircraft[headerItem.apiName] != undefined) {
                        if (headerItem.type == 'DATE') {
                            csvStringResult += '"' + $A.localizationService.formatDate(record.aircraft[headerItem.apiName], "MM/dd/yyyy") + '"';
                        } else if (headerItem.type == 'DATETIME') {
                            csvStringResult += '"' + $A.localizationService.formatDate(record.aircraft[headerItem.apiName], "MM/dd/yyyy, hh:mm a") + '"';
                        } else if (headerItem.type == 'REFERENCE') {

                            var relationShipName = '';
                            if (headerItem.apiName.indexOf('__c') == -1) {
                                relationShipName = headerItem.apiName.substring(0, headerItem.apiName.indexOf('Id'));
                            }
                            else {
                                relationShipName = headerItem.apiName.substring(0, headerItem.apiName.indexOf('__c')) + '__r';
                            }

                            csvStringResult += '"' + record.aircraft[relationShipName].Name + '"';

                        } else {
                            csvStringResult += '"' + record.aircraft[headerItem.apiName] + '"';
                        }
                    } else {
                        csvStringResult += '"' + '' + '"';
                    }
                    csvStringResult += columnDivider;
                })
                csvStringResult += lineDivider;
            })

            csvStringResult += '"' + 'Total Number of Bids' + '"';
            csvStringResult += columnDivider;
            aircraftData.aircraftList.forEach(function (record) {
                if (record.totalBids != null && record.totalBids != undefined) {
                    csvStringResult += '"' + record.totalBids + '"';
                } else {
                    csvStringResult += '"' + 0 + '"';
                }

                csvStringResult += columnDivider;
            });
            csvStringResult += lineDivider;


            var count = 0;
            var countBidAmount = 0;
            var countBidderName = 0;

            aircraftData.labelsForMaxBidColumns.forEach(function (record) {
                csvStringResult += '"' + record + '"';
                csvStringResult += columnDivider;

                if (count % 2 == 0) {

                    aircraftData.aircraftList.forEach(function (record1) {
                        if (countBidAmount < record1.maxBidList.length) {
                            if (record1.maxBidList[countBidAmount].bidAmount != null && record1.maxBidList[countBidAmount].bidAmount != undefined) {
                                csvStringResult += '"' + record1.maxBidList[countBidAmount].bidAmount + '"';
                            } else {
                                csvStringResult += '"' + 0 + '"';
                            }
                            csvStringResult += columnDivider;
                        }
                    });
                    countBidAmount++;
                }else {

                    aircraftData.aircraftList.forEach(function (record1) {
                        if (countBidderName < record1.maxBidList.length) {
                            if (record1.maxBidList[countBidderName].bidderName != null && record1.maxBidList[countBidderName].bidderName != undefined) {
                                csvStringResult += '"' + record1.maxBidList[countBidderName].bidderName + '"';
                            } else {
                                csvStringResult += '"' + 0 + '"';
                            }
                            csvStringResult += columnDivider;
                        }
                    });
                    countBidderName++;
                }
                count++;
                csvStringResult += lineDivider;
            });


            /*            
                        csvStringResult += '"'+ 'Max Bid' +'"'; 
                        csvStringResult += columnDivider; 
                        aircraftData.aircraftList.forEach(function (record){
                            if(record.maxBidList[0].bidAmount != null && record.maxBidList[0].bidAmount != undefined){
                                csvStringResult += '"'+ record.maxBidList[0].bidAmount+'"';
                            }else{
                                csvStringResult += '"'+ 0+'"';
                            }
                            csvStringResult += columnDivider; 
                        });
                        csvStringResult += lineDivider;
                        
                        csvStringResult += '"'+ 'Max Bidder' +'"'; 
                        csvStringResult += columnDivider; 
                        aircraftData.aircraftList.forEach(function (record){
                            if(record.maxBidder != null && record.maxBidder != undefined){
                                csvStringResult += '"'+ record.maxBidder+'"';
                            }else{
                                csvStringResult += '"'+ ''+'"';
                            }
                            csvStringResult += columnDivider; 
                        });
                        csvStringResult += lineDivider;
                        
                        csvStringResult += '"'+ 'Second Bid' +'"'; 
                        csvStringResult += columnDivider; 
                        aircraftData.aircraftList.forEach(function (record){
                            if(record.secondMaxBid != null && record.secondMaxBid != undefined){
                                csvStringResult += '"'+ record.secondMaxBid+'"';
                            }else{
                                csvStringResult += '"'+ 0+'"';
                            }
                            csvStringResult += columnDivider; 
                        });
                        csvStringResult += lineDivider;
                        
                        csvStringResult += '"'+ 'Second Bidder' +'"'; 
                        csvStringResult += columnDivider; 
                        aircraftData.aircraftList.forEach(function (record){
                            if(record.secondMaxBidder != null && record.secondMaxBidder != undefined){
                                csvStringResult += '"'+ record.secondMaxBidder+'"';
                                
                            }else{
                                csvStringResult += '"'+ ''+'"';
                            }
                            csvStringResult += columnDivider; 
                        });
                        
                        csvStringResult += lineDivider;*/

            if (component.get('v.aircraftData.showLossGain')) {
                csvStringResult += '"' + 'Gain / (Loss) on Sale' + '"';
                csvStringResult += columnDivider;
                aircraftData.aircraftList.forEach(function (record) {

                    csvStringResult += '"' + (record.maxBidList[0].bidAmount - record.bookValue) + '"';
                    csvStringResult += columnDivider;
                });
                csvStringResult += lineDivider;
            }

            if (aircraftData.ProspectWrapperList != null && aircraftData.ProspectWrapperList != undefined && aircraftData.ProspectWrapperList.length > 0) {

                aircraftData.aircraftList.forEach(function (aircraftRecordWrapObj) {

                    if (aircraftRecordWrapObj.summary != null && aircraftRecordWrapObj.summary != undefined) {

                        aircraftRecordWrapObj.summary.forEach(function (summaryobj) {
                            if (summaryobj.assetTerm != undefined) {

                                //var assetTermProspectId = summaryobj.assetTerm.Marketing_Activity__r.Prospect_Id__c ;

                                var assetTermProspectId;

                                if (summaryobj.assetTerm.Marketing_Activity__r.Prospect__c != undefined) {
                                    assetTermProspectId = summaryobj.assetTerm.Marketing_Activity__r.Prospect__c;
                                } else if (summaryobj.assetTerm.Marketing_Activity__r.Counterparty__c != undefined) {
                                    assetTermProspectId = summaryobj.assetTerm.Marketing_Activity__r.Counterparty__c;
                                } else {
                                    assetTermProspectId = summaryobj.assetTerm.Marketing_Activity__r.Lessor__c;
                                }

                                propspectWithAircraftId[assetTermProspectId + aircraftRecordWrapObj.aircraft.Id] = summaryobj;
                            }
                        });
                    }
                });

                aircraftData.ProspectWrapperList.forEach(function (propspectRecordWrapObj) {
                    csvStringResult += '"' + propspectRecordWrapObj.prospect + '"';
                    csvStringResult += columnDivider;
                    aircraftData.aircraftList.forEach(function (aircraftRecordWrapObj) {
                        var result = propspectWithAircraftId[propspectRecordWrapObj.prospectId + aircraftRecordWrapObj.aircraft.Id];
                        if (result != null && result != undefined) {
                            if (result.isFiltered) {
                                var valueOfProspect = result.value;
                                csvStringResult += '"' + valueOfProspect + '"';
                            } else {

                                if (result.assetTerm.Project_Status__c != undefined && result.assetTerm.Project_Status__c != null && result.assetTerm.Project_Status__c != '') {
                                    csvStringResult += '"' + 'Status:[' + result.assetTerm.Project_Status__c + ']' + '"';
                                } else {
                                    csvStringResult += '"' + 'Status:[]' + '"';
                                }
                            }
                        } else {
                            csvStringResult += '"' + '-' + '"';
                        }
                        csvStringResult += columnDivider;
                    });
                    csvStringResult += lineDivider;
                });
                console.log('csvStringResult :'+csvStringResult);
            }
            // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvStringResult);
            hiddenElement.target = '_self'; // 
            hiddenElement.download = 'BidMatrix.csv';  // CSV file Name* you can change it.[only name not .csv] 
            document.body.appendChild(hiddenElement); // Required for FireFox browser
            hiddenElement.click(); // using click() js function to download csv file             

            this.hideSpinner(component);

        }else {
            var errorMsg = [{ 'message': 'No data found.' }]
            this.handleErrors(errorMsg);
        }
    },

    loadData: function (component) {
        console.log('loadData');
        this.showSpinner(component);
        var action = component.get("c.getAircraftData");
        action.setParams({
            'projectId': component.get('v.recordId')
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('loadData state: ',state);
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                console.log('value : '+JSON.stringify(value));
                var projectStatusOptionsMap = new Map();

                if (value != undefined) {
                    if (value.projectStatusOptions != undefined) {
                        var selectedValues = new Array();

                        if (value.selectedFilterStatus != undefined) {
                            selectedValues = value.selectedFilterStatus.split(',');
                        }

                        value.projectStatusOptions.forEach(function (option) {
                            if (selectedValues.includes(option.value)) {
                                option.selected = true;
                            }

                            projectStatusOptionsMap.set(option.value, option.label);

                        });
                    }
                }

                component.set("v.projectStatusList", value.projectStatusOptions);
                component.set("v.projectStatusOptionsMap", projectStatusOptionsMap);
				
                this.updateAircraftList(component, value);
                //$A.get('e.force:refreshView').fire();
                this.hideSpinner(component);
            } else if (state === "ERROR") {
                this.hideSpinner(component);
                var errors = response.getError();
                console.log('errors'+JSON.stringify(errors[0].message));
                this.handleErrors(errors);
            }

			this.calculateHeight(component);
            //to reproduce collapsed view.
            this.updateView(component);
        });
        $A.enqueueAction(action);
    },
    
    /**
     * Method to calculate the height of the page and accordingly setting the max height of the container
     * this is to avoid extra space at the bottom in the higher screen size
     * https://app.asana.com/0/1181547498580321/1200023707304873/f
     * */
    calculateHeight: function(component) {
        var cId = document.getElementById("containerId");
        console.log('calculateHeight cId: '+cId);
        if(cId != undefined) {
            var rect = cId.getBoundingClientRect();
            let heightMatrix = window.innerHeight.toFixed(0) - rect.top.toFixed(0) - 30;
            component.set("v.matrixMaxHeight", heightMatrix);
        }
    },
    
    updateAircraftList : function(component, aircraftData) { 
        let bgColorMap = new Map();
        bgColorMap[0] = '#95e095';
        bgColorMap[1] = '#eac37b';
        bgColorMap[2] = '#c38c9d';
        bgColorMap[3] = '#b4a1dc';
        bgColorMap[4] = '#fff987';
        
        let aircraftList = aircraftData.aircraftList;
        for (var i = 0; i < aircraftList.length; i++) {
            let summary = aircraftList[i].summary;
            
            for (var j = 0; j < summary.length; j++) {
                if (summary[j].value != undefined) {
                    var valueOfProspect = summary[j].value;
                    let maxBidList = aircraftList[i].maxBidList;
                    //  var valueAfterConversion = valueOfProspect.replace(/[^a-zA-Z0-9\s]/g, "", "");
                    
                    for (var k = 0; k < maxBidList.length; k++) {
                        //  console.log(value.aircraftList[i].maxBidList[k].bidAmount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                        maxBidList[k].bidValue = maxBidList[k].bidAmount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        
                        if (maxBidList[k].bidAmount != 0 && maxBidList[k].bidValue == valueOfProspect) {
                            summary[j].bgColor = bgColorMap[k];
                            break;
                        }
                    }
                }
            }
        }
		
        component.set("v.aircraftData", aircraftData);
    },
    
    
    
    handleErrors: function (errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    if(error.message.includes('No such column') && error.message.includes('If you are attempting to use a custom field, be sure to append the \'__c\' after the custom field name. Please reference your WSDL or the describe call for the appropriate names') ){
                    toastParams.message = error.message.replace('If you are attempting to use a custom field, be sure to append the \'__c\' after the custom field name. Please reference your WSDL or the describe call for the appropriate names','Please check if the field api details added for Gain/Loss calculation. You can add only currency fields');
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                    }
                    else{
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                    }
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });
                    };
                }
            });
        }
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'10000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    updateView: function (component) {

        //if the view was already collapsed and the data is being refreshed while fitleration or
        //saving the record, reproduce the view as collapsed.

        /*var isExpanded = component.get("v.expanded");
        
        if(!isExpanded){
            
            //var elements = component.find("toggle");
            var defaultHeader = component.find("defaultHeader");
            
            if(Array.isArray(defaultHeader)){
                defaultHeader.forEach(function(header){
                    $A.util.removeClass(header, "defaultHeader");
                }); 
                
            }else{
                $A.util.removeClass(defaultHeader, "defaultHeader");
            } 
        }*/
        if (component.get("v.hideProspects")) {
            this.hideProspects(component);
        }
    },

    hideProspects: function (component) {

        var aircraftData = component.get("v.aircraftData");

        if (aircraftData != undefined && aircraftData != null) {

            var hideProspects = component.get("v.hideProspects");

            var prospectData = aircraftData.ProspectWrapperList;
            var indexListToHide = [];

            for (var index in prospectData) {
                if (prospectData[index].totalBids == '0') {
                    indexListToHide.push(index);
                    prospectData[index].isHiddenRow = hideProspects;

                }
            }
            aircraftData.ProspectWrapperList = prospectData;

            var aircraftList = aircraftData.aircraftList;

            for (var index in aircraftList) {
                for (var index1 in aircraftList[index].summary) {
                    if (indexListToHide.includes(index1)) {
                        aircraftList[index].summary[index1].isHiddenRow = hideProspects;
                    }
                }
            }

            aircraftData.aircraftList = aircraftList;
            component.set("v.aircraftData", aircraftData);
        }
    },

    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    closeBoxes: function (component, e, event, helper) {

        if (!$A.util.hasClass(component.find("projectStatusConfigDiv"), 'slds-hide')) {

            var div = document.getElementById('projectStatusConfigDiv');
            var target = e.target;
            var statusFilterButton = document.getElementById('statusFilterBtn');
            if (statusFilterButton != target && !div.contains(target)) {

                helper.closeProjectStatusBox(component);
            }
        }
    },

    success: function (component, helper, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: 'Success',
            message: message,
            duration: ' 500',
            type: 'success',
        });

        toastEvent.fire();

        //var appEvent = $A.get("e.c:ProjectStatusEvent");
        //appEvent.fire();
        // to resolve spinner issue while refresh 24 sep, 2019.
       /* window.setTimeout(
            $A.getCallback(function () {
                helper.loadData(component);
            }), 500
        );*/
    },

    closeProjectStatusBox: function (component) {

        $A.util.addClass(component.find("projectStatusConfigDiv"), 'slds-hide');
    },
    
    openProjectStatusBox: function (component) {

        $A.util.removeClass(component.find("projectStatusConfigDiv"), 'slds-hide');

    }
})