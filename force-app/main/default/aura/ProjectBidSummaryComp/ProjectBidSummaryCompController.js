({

    downloadDataAsCSV: function (component, event, helper) {

        helper.convertArrayOfObjectsToCSV(component);
    },

    refreshData: function (component, event, helper) {
        if (component != event.getSource()) {
            helper.loadData(component);
        }
    },

    doInit: function (component, event, helper) {
        window.addEventListener('resize', $A.getCallback(function(){
            helper.calculateHeight(component);
        }));
        helper.loadData(component);
    },

    editMarketingActivity: function (component, event, helper) {

        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.recordid;
        var name = selectedItem.dataset.marketingactivityname;
        var recordTypeId = selectedItem.dataset.recordtypid;

        if (recordId != undefined) {

            if (component.get("v.aircraftData.marketingActivityFieldList").length > 0) {

                var obj = new Object();
                obj.recordId = recordId;
                obj.name = name;
                obj.recordTypeId = recordTypeId;

                component.set("v.selectedMarketingActivityInfo", obj);
                component.set("v.openModalForMA", true);
                helper.showSpinner(component);

            } else {

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error',
                    message: 'Field set does not contain any field',
                    duration: ' 1000',
                    type: 'error',
                });

                toastEvent.fire();
            }
        }
    },

    editAircraft: function (component, event, helper) {
        var selectedItem = event.currentTarget.dataset.record;
        var recordId = selectedItem.Id;
        var msnNumber = selectedItem.MSN_Number__c;
        var recordTypeId = selectedItem.RecordTypeId;

        if (recordId != undefined) {

            if (component.get("v.aircraftData.aircraftFieldList").length > 0) {

                var obj = new Object();
                obj.recordId = recordId;
                obj.msnNumber = msnNumber;
                obj.recordTypeId = recordTypeId;

                component.set("v.selectedAircraftInfo", obj);
                component.set("v.openModalForAircraft", true);
                helper.showSpinner(component);

            } else {

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error',
                    message: 'Field set does not contain any field',
                    duration: ' 1000',
                    type: 'error',
                });

                toastEvent.fire();
            }
        }
    },

    expandCollapseView: function (component, event, helper) {

        // var elements = component.find("toggle");
        var collapseButton = component.find("collapseButton");
        var expandButton = component.find("expandButton");
        //  var defaultHeader = component.find("defaultHeader");

        component.set("v.expanded", !component.get("v.expanded"));


        $A.util.toggleClass(collapseButton, "slds-hide");
        $A.util.toggleClass(expandButton, "slds-hide");

        /* if(Array.isArray(defaultHeader)){
             defaultHeader.forEach(function(header){
                 $A.util.toggleClass(header, "defaultHeader");
             }); 
         }else{
             $A.util.toggleClass(defaultHeader, "defaultHeader");
         } */
    },

    openChooseFieldBox: function (component, event, helper) {
        $A.util.removeClass(component.find("bidFieldConfigDiv"), 'slds-hide');

    },
    closeChooseFieldBox: function (component, event, helper) {
        $A.util.addClass(component.find("bidFieldConfigDiv"), 'slds-hide');
    },
    saveChanges: function (component, event, helper) {

        helper.showSpinner(component);

        var selectedBidField = component.get("v.aircraftData.selectedBidField");
        var action = component.get("c.updateBidPreferredField");

        action.setParams({
            'projectId': component.get("v.recordId"),
            'fieldApiName': selectedBidField,
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            helper.hideSpinner(component);
            if (component.isValid && state === 'SUCCESS') {
                helper.loadData(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });

        $A.enqueueAction(action);

        $A.util.addClass(component.find("bidFieldConfigDiv"), 'slds-hide');
    },

    closeChooseProjecStatusBox: function (component, event, helper) {
        helper.closeProjectStatusBox(component);
    },
    openChooseProjectStatusBox: function (component, event, helper) {
        helper.openProjectStatusBox(component)
    },

    onLoad: function (component, event, helper) {
        /*setTimeout(function () {
            helper.hideSpinner(component);
            //$A.get('e.force:refreshView').fire();
        }, 300);*/

    },
    cancelAction: function (component, event, helper) {
        helper.hideSpinner(component);
        component.set("v.openModalForEdit", false);
    },

    submitForm: function (component, event, helper) {

        helper.showSpinner(component);
    },

    saveAssetDetailValues: function (component, event, helper) {
        helper.showSpinner(component);
        event.preventDefault(); //Prevent default submit

        var eventFields = event.getParam("fields");
        var allFields = component.get("v.aircraftData.assetDetailFieldList");
        let salePrice = 0;
        //var bidField = component.get("v.aircraftData.selectedBidField");
        var targetObj = new Object();
        var fieldsToBeCopied = new Array();

        //check if anything to copy on other assets or not and prepare target object's data.
        allFields.forEach(function (field) {
            if (field.selected) {
                fieldsToBeCopied.push(field.apiName);
            }

            targetObj[field.apiName] = eventFields[field.apiName];
            if(field.apiName == 'Sale_Price__c') {
                salePrice = eventFields[field.apiName];
            }
        });
        
        let aircraftData = component.get("v.aircraftData");
        let assetIndex = component.get("v.assetDetailInfo.assetIndex");
        let prospectIndex = component.get("v.assetDetailInfo.prospectIndex");
        aircraftData.aircraftList[assetIndex].summary[prospectIndex].value = salePrice;
        component.set("v.aircraftData", aircraftData);
        
        //targetObj[bidField] = eventFields[bidField];
        targetObj['Id'] = component.get("v.assetDetailInfo.assetDetailId");

        //log(targetObj);

        //if nothing to copy on all assets then use simply standard save.
        if (fieldsToBeCopied.length == 0) {
            component.find("assetDetailForm").submit(eventFields);
        } else {
            //server call to update all the Asset in Deal records.
            var action = component.get("c.updateAssetDetailRecords");
            action.setParams({
                'assetDetail': targetObj,
                'prospectId': component.get("v.assetDetailInfo.prospectId"),
                'projectId': component.get('v.recordId'),
                'fieldsToBeCopied': fieldsToBeCopied
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    if(fieldsToBeCopied.indexOf('Sale_Price__c') != -1) {
                        aircraftData.aircraftList.forEach(function(obj) {
                            obj.summary[prospectIndex].value = salePrice;
                            return obj;
                        });
                        component.set("v.aircraftData", aircraftData);     			
                    }
                    component.set("v.openModalForEdit", false);                   
                    helper.success(component, helper, 'Bid records updated successfully. Please refresh the page to see the updates.');
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    helper.handleErrors(errors);
                }
                
                helper.hideSpinner(component);
            });

            $A.enqueueAction(action);
        }
    },
    onError: function (component, event, helper) {
        helper.hideSpinner(component);
    },

    closeMAModal: function (component, event, helper) {
        helper.hideSpinner(component);
        component.set("v.openModalForMA", false);
    },

    closeAircraftModal: function (component, event, helper) {
        helper.hideSpinner(component);
        component.set("v.openModalForAircraft", false);
    },

    onSuccess: function (component, event, helper) {
        helper.hideSpinner(component);
        component.set("v.openModalForEdit", false);
        helper.success(component, helper, 'Bid record updated successfully');
    },

    onSuccessAircraft: function (component, event, helper) {
        component.set("v.openModalForAircraft", false);
        helper.success(component, helper, 'Asset record updated successfully');
    },

    applyStatusFilter: function (component, event, helper) {
        var options = component.get("v.projectStatusList");
        var filterStatus = '';
        if (options != undefined && options.length > 0) {
            options.forEach(function (option) {
                if (option.selected)
                    filterStatus += option.value + ',';
            });
            filterStatus = filterStatus.slice(0, -1);
        }

        $A.util.addClass(component.find("projectStatusConfigDiv"), 'slds-hide');

        //save the filter status.
        var action = component.get("c.updateFilterStatus");

        helper.showSpinner(component);

        action.setParams({
            'projectId': component.get('v.recordId'),
            'filters': filterStatus
        });

        action.setCallback(this, function (response) {

            helper.hideSpinner(component);

            var state = response.getState();

            if (state === "SUCCESS") {
                helper.loadData(component);
                var appEvent = $A.get("e.c:ProjectStatusEvent");
                appEvent.fire();

            } else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }

        });
        $A.enqueueAction(action);
    },

    editCell: function (component, event, helper) {
        var assetIndex = event.currentTarget.dataset.assetindex;
        var prospectIndex = event.currentTarget.dataset.prospectindex;
        var aircraftData = component.get("v.aircraftData");
        if (aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm != undefined) {

            if (!aircraftData.isActive) {

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": 'Access Denied',
                    "type": 'warning',
                    "message": 'This Campaign is not active. Please change the status of the Campaign to edit any part of it.'
                });
                toastEvent.fire();

            } else {

                helper.showSpinner(component);
                var action = component.get("c.validateTradingProfile");

                action.setParams({
                    "assetId": aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm.Aircraft__c,
                    "prospectId": aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm.Marketing_Activity__r.Prospect_Id__c

                });

                action.setCallback(this, function (response) {
                    var state = response.getState();

                    if (response.getState() === "SUCCESS") {
                        var Assetdetails = response.getReturnValue();

                        var info = new Object();

                        info.details = Assetdetails;
                        info.assetDetailId = aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm.Id;
                        info.prospect = aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm.Prospect__c;
                        info.msn = aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm.Aircraft__r.MSN_Number__c;
                        info.prospectId = aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm.Marketing_Activity__r.Prospect_Id__c;
                        info.recordTypeId = aircraftData.aircraftList[assetIndex].summary[prospectIndex].assetTerm.RecordTypeId;
						info.assetIndex = assetIndex;
                        info.prospectIndex = prospectIndex;
                        
                        component.set("v.assetDetailInfo", info);
                        component.set("v.openModalForEdit", true);
						helper.hideSpinner(component);
                    } else if (state === "ERROR") {

                        var errors = response.getError();
                        helper.handleErrors(errors);
                        helper.hideSpinner(component);
                    }

                });
                $A.enqueueAction(action);
            }
        }
    },

    hideProspectWithNoBids: function (component, event, helper) {
        helper.showSpinner(component);
        component.set("v.hideProspects", !component.get("v.hideProspects"));
        helper.hideProspects(component);

        var appEvent = $A.get("e.c:ToggleInactiveProspectsEvent");
        appEvent.setParams({ "hideInactiveProspects": component.get("v.hideProspects") });

        appEvent.fire();

        window.setTimeout(
            $A.getCallback(function () {
                helper.hideSpinner(component);
            }), 1000
        );
    },

    toggleInactiveProspects: function (component, event, helper) {
        if (component != event.getSource()) {
            component.set("v.hideProspects", event.getParam("hideInactiveProspects"));
            helper.hideProspects(component);
        }
    },

    saveMAAndAIDRecords: function (component, event, helper) {
        helper.showSpinner(component);
        event.preventDefault(); //Prevent default submit

        let eventFields = event.getParam("fields");
        let allFields = component.get("v.aircraftData.marketingActivityFieldList");
        let targetObj = new Object();

        targetObj['Id'] = component.get("v.selectedMarketingActivityInfo.recordId");
        allFields.forEach(function (field) {
            targetObj[field] = eventFields[field];
        });

        let aidDataForMA = component.get("v.aidDataForMA");
        console.log('aidDataForMA: ', aidDataForMA);

        let aidObjList = [];
        for (let key in aidDataForMA) {
            let fields = aidDataForMA[key].fields;
            let aidObj = {};
            aidObj.Id = aidDataForMA[key].aidId;
            if (!$A.util.isEmpty(fields) && !$A.util.isUndefined(fields)) {
                for (let key2 in fields) {
                    aidObj[fields[key2].apiName] = fields[key2].value;
                }
                aidObjList.push(aidObj);
            }
        }
        console.log('aidObjList: ', aidObjList);

        let action = component.get("c.updateMAAndAIDRecords");
        action.setParams({
            'maRecord': targetObj,
            'aidObjList': aidObjList
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.openModalForMA", false);
                helper.success(component, helper, 'Bid records updated successfully. Please refresh the page to see the updates.');
            } else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    }
})