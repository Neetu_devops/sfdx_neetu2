({
	sendHelper: function(component) {
        var action = component.get("c.sendMailMethod");   
        var self = this;
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('sendHelper response '+state);
            if (state === "SUCCESS") 
                self.showToast(true);
            else
            	self.showToast(false);
        });
        $A.enqueueAction(action);
    },
    
    sendEmailToMany : function(component){
        var recordIdL = component.get("v.recordId");
        console.log('sendEmailToMany recordId: '+recordIdL );
        var self = this;
        if($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        
        var userData = component.get("v.userList");
        var addtionalEmail = component.get("v.emailList"); 
        var action = component.get('c.sendMailToMany');
        action.setParams({
            recordId : recordIdL,
            userList: userData,
            emailAddress: addtionalEmail,
            userType: component.get("v.userType")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("sendEmailToMany : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("sendEmailToMany values " +allValues);
                if(allValues != null && allValues == 'Success') {
                    self.showToast(true);
                    self.reset(component);
                }
                else {
                    component.set("v.isLoading", false);
                    self.showErrorMsg(component, "Error in sending email");
                }
            }
            else if (state === "ERROR") {    
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("sendEmailToMany errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("sendEmailToMany unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    
    
    reset : function(component) {
        console.log('reset');
        component.set('v.showEmailPopup', false);
        component.set("v.userList", new Array());
        component.set("v.emailList", null);
    },
    
    isEmailValid: function( emailAddress) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailAddress) == false) 
            return false;
        return true;
    },
    
    
    showToast : function(state) {
        var toastEvent = $A.get("e.force:showToast");
        if(state == true) {
            toastEvent.setParams({
                title: "Success!",
                message: "Email sent successfully.",
                duration:'5000',
                key: 'info_alt',
                type: 'success',
                mode: 'dismissible'
            });
        }
        else{
            toastEvent.setParams({
                "title": "Failed!",
                "message": "Error in sending email",
                duration:'5000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
        }
        toastEvent.fire();
	},
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
            
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('error '+ toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('error '+ toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('error '+ toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
})