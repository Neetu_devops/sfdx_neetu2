({
	doInit : function(component, event, helper) {
		
	},
    
    sendemailmsg:  function(component, event, helper) {
		helper.sendHelper(component);
	},
    
    openEmailPopup:  function(component, event, helper) {
		component.set("v.showEmailPopup", true);
	},
     
    onCancel : function(component, event, helper) {
        console.log('Cancel');
        helper.reset(component);
    },
    
    onChangeType : function(component, event, helper) {
        console.log("onChangeType");
        component.set("v.userList", new Array());
    },
    
    onSave : function(component, event, helper) {
        console.log("onSave");
        var userData = component.get("v.userList");
        console.log("onSave userData "+JSON.stringify(userData));
        var addtionalEmail = component.get("v.emailList");
        console.log("onSave addtionalEmail "+addtionalEmail);
        var hasAdditionalMail = false, hasUserData = false;
        if(!$A.util.isEmpty(addtionalEmail) && !$A.util.isUndefined(addtionalEmail)) {
            var inValidList=[];
        	var emailList= addtionalEmail.split(',');
            console.log("onSave emailList: " + emailList);
            for(var i =0 ; i < emailList.length; i++) {
                var isValid = helper.isEmailValid(emailList[i]);
                if(isValid == false)
                    inValidList.push(emailList[i]);
            }
            console.log("onSave inValidList"+inValidList);
            if($A.util.isEmpty(inValidList) || $A.util.isUndefined(inValidList) || inValidList == null) {
                hasAdditionalMail = true;
            }
            else {
                canSend = false;
                //show invalid error message
                console.log("onSave invalid email "+ inValidList);
                var msg = "Invalid email: " + inValidList;
                helper.showErrorMsg(component, msg);
            }
        }
        else
            component.set("v.emailList", null);
        
        if(!$A.util.isEmpty(userData) && !$A.util.isUndefined(userData) && userData != null && userData != '') 
        	hasUserData = true;
        
            
        console.log("onSave hasAdditionalMail: "+hasAdditionalMail + " hasUserData: "+hasUserData);
        if(hasAdditionalMail == true || hasUserData == true) {
            //send email
            component.set("v.isLoading", true);
            helper.sendEmailToMany(component);
        }
        else{
            //show toast to enter email id 
            helper.showErrorMsg(component, 'Please enter email address');
        }
    },
    
})