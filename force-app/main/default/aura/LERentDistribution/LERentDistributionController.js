({
	 doInit : function(component, event, helper) {
       
        console.log("doInit inside parent: " + component.get('v.leaseId'));
        helper.getParamValue('RentDistribution', 'leaseworks__id', component);
    },
    save : function(component, event,helper){
        var cmpEvent = component.getEvent("saveHierarchy");
        var legalEntities = component.get("v.legalEntityList");
        cmpEvent.setParams({
            "heirarchyList" : legalEntities
        });
        cmpEvent.fire();
    },

    saveHierarchyList: function(component, event,helper){
        helper.saveLeaseHierarcgy(component);
        window.history.back();
        return false;
    },
    Cancel:function(component, event,helper){
        	/*var url = window.location.href; 
            var value = url.substr(0,url.lastIndexOf('/') + 1);*/
            $A.get('e.force:refreshView').fire();
            window.history.back();
        	helper.getLegalEntityData(component);
            return false;
    }
    

})