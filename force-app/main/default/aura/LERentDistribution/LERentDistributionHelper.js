({
    getParamValue : function( tabName, paramName,component ) {
       var url = window.location.href;
       console.log("index of hierarchy::"+url.indexOf(tabName));    
       var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&amp;amp;amp;');
       var paramValue = '';
       console.log("getParamValue url "+url);
       console.log("getParamValue allParams "+allParams);
       for(var i=0; i < allParams.length; i++) {
           if(allParams[i].split('=')[0] == paramName)
               paramValue = allParams[i].split('=')[1];
       }
       console.log("getParamValue: " + paramValue);
       component.set('v.leaseId', paramValue);
       this.getNamespacePrefix(component);
   }, 
   
   getNamespacePrefix : function(component) {
       console.log('getNamespacePrefix');
       var self = this;
       var action = component.get("c.getNamespacePrefix");
       action.setCallback(this, function(response) {
           var namespace = response.getReturnValue();
           console.log("getNamespacePrefix response: "+namespace);
           component.set("v.namespace",namespace);
           self.getLegalEntityData(component);
       });
       $A.enqueueAction(action);                
   },
     
    getLegalEntityData : function(component) {
        console.log('getLegalEntityData');
        var action = component.get("c.getLegalEntityData");
        console.log('Lease Id:::'+component.get("v.leaseId"))
        action.setParams({
            leaseId : component.get("v.leaseId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getLegalEntityData state: "+state);
            if (state === "SUCCESS") {
                var legalEntities = response.getReturnValue();
                console.log("getLegalEntityData response: "+JSON.stringify(legalEntities));
                component.set("v.legalEntityList",legalEntities);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getLegalEntityData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
            }
        });
        $A.enqueueAction(action);        
    },
    saveLeaseHierarcgy : function(component){
        var self = this;
        var action = component.get("c.saveLeaseHierarchy");
        var entitylist = component.get("v.legalEntityList");
        var entityString=JSON.stringify(entitylist);
        action.setParams({
            legalEntityString : entityString,
            leaseId : component.get("v.leaseId")

        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getLegalEntityData state: "+state);
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if(!($A.util.isEmpty(res) || $A.util.isUndefined(res) || res == null))
                    self.showErrorMsg(component, res);
                else
                    self.showSucessMsg(component, 'Data Saved Successfully');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getLegalEntityData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        //self.showErrorMsg(component, errors[0].message);
                    }
                } 
            }
        });
        $A.enqueueAction(action);       

    },
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'10000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
	},
    
    showSucessMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
	}
})