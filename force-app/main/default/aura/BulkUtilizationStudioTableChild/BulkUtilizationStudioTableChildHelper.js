({
	// Show the lightning Spinner
    showSpinner: function (component, event) {
        component.set("v.showSpinner", true);
    },
    
    // Hide the lightning Spinner
    hideSpinner: function (component, event) {
        component.set("v.showSpinner", false);
    }, 
    
    saveCommentsOnItemsRecord: function(component, event, recordToUpdate,objectAPIName) {
        console.log(recordToUpdate);
        var action = component.get("c.saveCommentOnListItems");
        action.setParams({
            recordToUpdate : JSON.stringify(recordToUpdate),
            objectAPIName : objectAPIName
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log("SUCCESS");
            }else{
                console.log("ERROR");
            }
        });
        $A.enqueueAction(action);
    },
    calculateRatio : function(component, event, helper) {
         var searchData = component.get("v.searchData");
        //var operatorData = searchData.operatorData;
        var result = event.getSource().get("v.name");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        
        var assemblyWrapper = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper;
        var assemblyComponent = [];
        
        var assemblyComponent = [];
        var utilization = [];
        if(assemblyWrapper.length > 0){
            for(var i = 0 ; i < assemblyWrapper.length; i++){
                var assembly = new Object; 
                assembly.assemblyName = assemblyWrapper[i].assembly;
                assembly.tsn = assemblyWrapper[i].tsn;
                assembly.csn = assemblyWrapper[i].csn;
                utilization.push(assembly);
            }
        }
        
        var assemblyVal = utilization;      
        var assetFH = searchData.operatorData[opIndex].assetData[astIndex].totalFH;
        var assetFC = searchData.operatorData[opIndex].assetData[astIndex].totalFC;
        var totalTSN = 0.0;
        var totalCSN = 0.0;
        
        
       
        
        
        if(assemblyWrapper.length > 0) {
            for(var i=0; i<assemblyWrapper.length; i++) {
                var assemblyName = assemblyWrapper[i].assembly;    
                
                if(assemblyWrapper[i].flightHM != undefined && assemblyWrapper[i].flightHM != '') {
                    if(assemblyWrapper[i].flightHM.indexOf(':') != -1) {
                        assemblyWrapper[i].flightH = (parseInt(assemblyWrapper[i].flightHM.split(':')[0]) + ((parseInt(assemblyWrapper[i].flightHM.split(':')[1]))/60)).toFixed(2);
                    }
                    else {
                        assemblyWrapper[i].flightH = parseFloat(assemblyWrapper[i].flightHM);
                    }                   
                }       
                
                if(assemblyWrapper[i].flightH == 0 && assemblyWrapper[i].flightC == 0) {
                    assemblyWrapper[i].ratio = 0
                }
                else {
                	assemblyWrapper[i].ratio = assemblyWrapper[i].flightH/assemblyWrapper[i].flightC;
                }
                
                if(assemblyVal.length > 0) {
                    for(var j=0; j<assemblyVal.length; j++) {
                        if(assemblyVal[j].assemblyName == assemblyName && assemblyWrapper[i].flightC != undefined) {
                            assemblyWrapper[i].csn = parseInt(assemblyVal[j].csn) +  parseInt(assemblyWrapper[i].flightC);
                        }
                        else if(assemblyVal[j].assemblyName == assemblyName && (assemblyWrapper[i].flightC == undefined)){
                            assemblyWrapper[i].csn = parseInt(assemblyVal[j].csn);
                        }
                        
                        if(assemblyVal[j].assemblyName == assemblyName && assemblyWrapper[i].flightH != undefined) {                            
                            assemblyWrapper[i].tsn = parseFloat(assemblyVal[j].tsn) +  parseFloat(assemblyWrapper[i].flightH);
                        }
                        else if(assemblyVal[j].assemblyName == assemblyName && (assemblyWrapper[i].flightH == undefined)) {
                            assemblyWrapper[i].tsn = parseFloat(assemblyVal[j].tsn);
                        }                                               
                    }
                }
                
                if(assemblyName == 'Airframe') {
                    totalTSN += assemblyWrapper[i].tsn;
                    totalCSN += assemblyWrapper[i].csn;
                }
                
                assemblyComponent.push(assemblyWrapper[i]);
            }
        }
        
        if(assetFH == 0 && assetFC == 0) {
            searchData.operatorData[opIndex].assetData[astIndex].totalRatio = 0;
            component.set("v.searchData", searchData);
        }
        else {
            component.set("v.searchData", assetFH/assetFC);
        }
        
        searchData.operatorData[opIndex].assetData[astIndex].totalTSN = totalTSN;
        searchData.operatorData[opIndex].assetData[astIndex].totalCSN = totalCSN;
        searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper =assemblyComponent;
        
        component.set("v.searchData", searchData);
    }
      
})