({
    doinit : function(component, event, helper){
        console.log('child do init ');
        console.log(component.get("v.closeBilling"), component.get("v.closeInvoice"));
        helper.showSpinner(component, event, helper);
        if(component.get("v.closeBilling") && component.get("v.closeInvoice")){
            var self = this;
            var header =["Billing","Invoice"];
            setTimeout(function() {
                if(header.length > 0){
                    for(var k = 0 ;k < header.length; k++){
                        var innerHeader = header[k]; 
                        var hideSection = document.querySelectorAll("."+innerHeader); 
                        for(var i =0; i< hideSection.length; i++){
                            hideSection[i].classList.add("slds-hide");
                        }
                        
                        var topHeader= document.querySelectorAll("."+innerHeader+"_topHeader");
                        for(var j =0; j< topHeader.length; j++){
                            topHeader[j].colSpan ="2";
                        }
                        
                        var backwardSpan = document.querySelectorAll("."+innerHeader+"_backward"); 
                        var forwardSpan = document.querySelectorAll("."+innerHeader+"_forward");
                        for(var j =0; j< topHeader.length; j++){
                            backwardSpan[j].classList.add("slds-hide");
                            forwardSpan[j].classList.remove("slds-hide");
                        }
                    }
                }  
            }, 0);
        }
        else{
            if(!component.get("v.closeBilling") && component.get("v.closeInvoice")){
                var self = this;
                var header =["Invoice"];
                setTimeout(function() {
                    if(header.length > 0){
                        for(var k = 0 ;k < header.length; k++){
                            var innerHeader = header[k]; 
                            var hideSection = document.querySelectorAll("."+innerHeader); 
                            for(var i =0; i< hideSection.length; i++){
                                hideSection[i].classList.add("slds-hide");
                            }
                        }
                        
                        var showBorderRight = document.querySelectorAll(".showBorderBilling"); 
                        for(var i =0; i< showBorderRight.length; i++){
                            showBorderRight[i].classList.remove("borderRight");
                        }
                    }
                }, 0);
            }
            else if(component.get("v.closeBilling") && !component.get("v.closeInvoice")){
                var self = this;
                var header =["Billing"];
                setTimeout(function() {
                    if(header.length > 0){
                        for(var k = 0 ;k < header.length; k++){
                            var innerHeader = header[k]; 
                            var hideSection = document.querySelectorAll("."+innerHeader); 
                            for(var i =0; i< hideSection.length; i++){
                                hideSection[i].classList.add("slds-hide");
                            }
                        }
                        var showBorderRight = document.querySelectorAll(".showBorderInvoice"); 
                        console.log('showBorderRight>>>'+showBorderRight);
                        for(var i =0; i< showBorderRight.length; i++){
                            showBorderRight[i].classList.remove("borderRight");
                        }
                    }
                }, 0);
            }
                else{
                    var header =["Billing","Invoice"];
                    setTimeout(function() {
                        if(header.length > 0){
                            for(var k = 0 ;k < header.length; k++){
                                var innerHeader = header[k]; 
                                var showBorderRight = document.querySelectorAll(".showBorder"+innerHeader);
                                for(var i =0; i< showBorderRight.length; i++){
                                    showBorderRight[i].classList.remove("borderRight");
                                }
                            }
                        }
                    },0);
                }
        }
        
        setTimeout(function(){helper.hideSpinner(component, event, helper); }, 500);
    },
    getIndex : function(component, event, helper) {
        console.log(event.getSource().get("v.value"));
        var result= event.getSource().get("v.value");
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        
        
        var initData = component.get("v.searchData");
        if(initData.operatorData[opIndex].assetData != null){
            var showDiv = initData.operatorData[opIndex].assetData[astIndex].showDiv;
            var isOpen = initData.operatorData[opIndex].assetData[astIndex].isOpen;
            if(showDiv){
                showDiv = false;
            }
            else{
                showDiv = true;
            }
            if(isOpen){
                isOpen = false;
            }
            else{
                isOpen = true;    
            }
            initData.operatorData[opIndex].assetData[astIndex].showDiv = showDiv;
            initData.operatorData[opIndex].assetData[astIndex].isOpen = isOpen;
            component.set("v.searchData", initData); 
        }
    },
    handleClickShow : function(component, event, helper) {
        var arr = event.getSource().get("v.class").split("_");
        var headerInd = arr[0];
        var showBorderRight = document.querySelectorAll(".showBorder"+headerInd); 
        console.log('showBorderRight>>>'+showBorderRight);
        for(var i =0; i< showBorderRight.length; i++){
            showBorderRight[i].classList.remove("borderRight");
        }
        
        var showSection = document.querySelectorAll("."+headerInd); 
        for(var i =0; i< showSection.length; i++){
            showSection[i].classList.remove("slds-hide");
        }
        
        var topHeader= document.querySelectorAll("."+headerInd+"_topHeader");
        for(var j =0; j< topHeader.length; j++){
            if(headerInd.includes('Invoice'))
                topHeader[j].colSpan ="3";
            else
                topHeader[j].colSpan ="7";
        }
        
        var backwardSpan = document.querySelectorAll("."+headerInd+"_backward"); 
        var forwardSpan = document.querySelectorAll("."+headerInd+"_forward");
        for(var j =0; j< topHeader.length; j++){
            backwardSpan[j].classList.remove("slds-hide");
            forwardSpan[j].classList.add("slds-hide");
        }
        
    },
    
    handleClickHide : function(component, event, helper) {
        
        var arr = event.getSource().get("v.class").split("_");
        var headerInd = arr[0];
        var showBorderRight = document.querySelectorAll(".showBorder"+headerInd); 
        for(var i =0; i< showBorderRight.length; i++){
            showBorderRight[i].classList.add("borderRight");
        }
        
        var hideSection = document.querySelectorAll("."+headerInd); 
        for(var i =0; i< hideSection.length; i++){
            hideSection[i].classList.add("slds-hide");
        }
        
        var topHeader= document.querySelectorAll("."+headerInd+"_topHeader"); 
        for(var j =0; j< topHeader.length; j++){
            topHeader[j].colSpan ="2";
        }
        
        var backwardSpan = document.querySelectorAll("."+headerInd+"_backward"); 
        var forwardSpan = document.querySelectorAll("."+headerInd+"_forward");
        for(var j =0; j< topHeader.length; j++){
            backwardSpan[j].classList.add("slds-hide");
            forwardSpan[j].classList.remove("slds-hide");
        }
        
    },
    saveComment: function(component, event, helper) {
        var recId = event.currentTarget.dataset.recid;
        var invoiceId = recId.split(" ");
        console.log('InvoiceId: ',invoiceId);
        var searchData = component.get("v.searchData");
        var recordToUpdate;
        var objectAPIName;
        var invoiceRec;
        var assemblyUtilizationRec;;
        if(invoiceId[1] == "invoice"){
            for(let key in searchData.operatorData){
                for(let key1 in searchData.operatorData[key].assetData){
                    for(let key2 in searchData.operatorData[key].assetData[key1].lstComponentWrapper){
                        invoiceRec = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].invoiceRec;
                        if(invoiceRec != undefined && invoiceRec !='' && invoiceRec != null){
                            if(invoiceRec.Id == invoiceId[0]){
                                recordToUpdate = invoiceRec;
                                console.log('recordToUpdate: ',recordToUpdate);
                                objectAPIName = 'Invoice_Line_Item__c';
                                break;
                            }
                        }
                    }
                }
            }
        }
        if(invoiceId[1] == "assemblyUtilization"){
            for(let key in searchData.operatorData){
                for(let key1 in searchData.operatorData[key].assetData){
                    for(let key2 in searchData.operatorData[key].assetData[key1].lstComponentWrapper){
                        assemblyUtilizationRec = searchData.operatorData[key].assetData[key1].lstComponentWrapper[key2].assemblyUtilizationRecord;
                        if(assemblyUtilizationRec.Id == invoiceId[0]){
                            recordToUpdate = assemblyUtilizationRec;
                            objectAPIName='Utilization_Report_List_Item__c';
                            break;
                        }
                    }
                }
            }
        }
        if(recordToUpdate != undefined && recordToUpdate != '' && recordToUpdate != null )
            helper.saveCommentsOnItemsRecord(component, event, recordToUpdate,objectAPIName);
    },  
    changeFHValue: function(component, event, helper) {
        var searchData = component.get("v.searchData");
        var indexes = event.getSource().get("v.name");
        console.log(indexes);
        var opIndex = parseInt(indexes.split('-')[0]);
        var astIndex = parseInt(indexes.split('-')[1]);
        var asmIndex = parseInt(indexes.split('-')[2]);
        var trueUpIndex = component.get("v.trueUpIndex");
        console.log("true Up Index",trueUpIndex);
        var assetFHM;
        
        if(trueUpIndex != undefined){
            assetFHM = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].flightHM;
            if (assetFHM == '' || assetFHM == undefined) {
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].flightHM = null;
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].flightH = null;
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].fHUnknown = true;
                event.getSource().set("v.class", "inputFeilds asset placeHChange");
            }
            else{
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].fHUnknown = false;
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].flightHM = assetFHM;
                event.getSource().set("v.class", "inputFeilds asset");
            }
        }
        else{
            assetFHM = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].flightHM;
            if (assetFHM == '' || assetFHM == undefined) {
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].flightHM = null;
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].flightH = null;
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].fHUnknown = true;
                event.getSource().set("v.class", "inputFeilds asset placeHChange");
            }
            else{
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].fHUnknown = false;
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].flightHM = assetFHM;
                event.getSource().set("v.class", "inputFeilds asset");
            }
        }
    },
    changeFCValue : function(component, event, helper) { 
        var APUcounters = event.getSource().get("v.value");
        var APUcountersRoundOFF;
        if(APUcounters){
         APUcountersRoundOFF = Math.round(APUcounters);
        }
        event.getSource().set("v.value",APUcountersRoundOFF); 
        
        var searchData = component.get("v.searchData");
        var result = event.getSource().get("v.name");
        console.log(result);
        var opIndex = parseInt(result.split('-')[0]);
        var astIndex = parseInt(result.split('-')[1]);
        var asmIndex = parseInt(result.split('-')[2]);
        var trueUpIndex = component.get("v.trueUpIndex");
        console.log("true Up Index",trueUpIndex);
        var assetFC;
        if(trueUpIndex != undefined){
            assetFC = searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].flightC;
            console.log('assetFc:',assetFC);
            if (assetFC == '' || assetFC == undefined) {
                event.getSource().set("v.class", "inputFeilds asset placeHChange");
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].flightC = null;
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].fCUnknown = true;
            }
            else{
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].fCUnknown = false;
                searchData.operatorData[opIndex].assetData[astIndex].trueUpWrapperList[parseInt(trueUpIndex)].lstComponentWrapper[asmIndex].flightC = assetFC;
                event.getSource().set("v.class", "inputFeilds asset");
            }
        }
        else{
            assetFC = searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].flightC;
            console.log('assetFc:',assetFC);
            if (assetFC == '' || assetFC == undefined) {
                event.getSource().set("v.class", "inputFeilds asset placeHChange");
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].flightC = null;
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].fCUnknown = true;
            }
            else{
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].fCUnknown = false;
                searchData.operatorData[opIndex].assetData[astIndex].lstComponentWrapper[asmIndex].flightC = assetFC;
                event.getSource().set("v.class", "inputFeilds asset");
            }
        }
    },
    calcRatio: function(component, event, helper) {
        helper.calculateRatio(component,event, helper);       
    },
})