({
    doInit: function (component, event, helper) {
        this.showSpinner(component);
        var action = component.get("c.getDrawnDownAmountRecords");
        action.setParams({ recordId: component.get("v.recordId") });
        action.setCallback(this, function (res) {
            if (res.getState() == "SUCCESS") {
                var response = res.getReturnValue();
                for (var i = 0; i < response.drawnDownAmountRecords.length; i++) {
                    if (response.drawnDownAmountRecords[i].locked) {
                        response.remainingAmount = response.remainingAmount - response.drawnDownAmountRecords[i].amount;
                    }
                }
                if ((response.drawnDownAmountRecords.length) == 3 && response.drawnDownAmountRecords[(response.drawnDownAmountRecords.length) - 1].locked) {
                    component.set("v.editMode", false);
                    component.set("v.saveMode", false);
                } console.log('resp-- ', response);
                component.set("v.wrapperObj", response);
                this.hideSpinner(component);
            }
            else if (res.getState() == "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors, component);
            }
        });
        $A.enqueueAction(action);
    },
    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    //to hide spinner after the data is loaded
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    handleErrors: function (errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error" : type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error" : type,
            duration: '7000'
        };
        if (typeof errors === 'object') {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
        else if (typeof errors === "string" && errors.length != 0) {
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    },
    showToast: function (titleofToast, messageOfToast, typeOfToast) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: titleofToast,
            message: messageOfToast,
            type: typeOfToast
        });
        toastEvent.fire();
    }
})