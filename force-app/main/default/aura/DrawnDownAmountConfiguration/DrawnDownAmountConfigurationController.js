({
    doInit: function (component, event, helper) {
        helper.doInit(component, event, helper);
    },
    handleSave: function (component, event, helper) {
        helper.showSpinner(component);
        component.set('v.spinner', true);
        var containerWrapperObj = component.get("v.wrapperObj");
        var amount = containerWrapperObj.drawnDownAmountRecords[(containerWrapperObj.drawnDownAmountRecords.length) - 1].amount;
        if (containerWrapperObj.remainingAmount == null) {
            helper.hideSpinner(component);
            var messageOfToast = 'LoC amount is not specified on this Letter of credit.';
            helper.showToast('Error', messageOfToast, 'error');
        }
        else if (amount == 0) {
            helper.hideSpinner(component);
            helper.showToast('Error', 'Amount field is required', 'error');
        }
        else if (amount < 0) {
            helper.hideSpinner(component);
            var messageOfToast = 'The drawdown amount entered is negative. Please enter a positive amount.';
            helper.showToast('Error', messageOfToast, 'error');
        }
        else if (amount <= containerWrapperObj.remainingAmount) {
            var action = component.get("c.saveDrawnDownAmountRecord");
            action.setParams({
                locdrawndownId: containerWrapperObj.drawnDownAmountRecords[(containerWrapperObj.drawnDownAmountRecords.length) - 1].id, name: containerWrapperObj.drawnDownAmountRecords[(containerWrapperObj.drawnDownAmountRecords.length) - 1].name,
                locrecordId: component.get("v.recordId"),
                amount: amount
            });
            action.setCallback(this, function (res) {
                var response = res.getReturnValue();
                if (res.getState() == "SUCCESS") {

                    // $A.get('e.force:refreshView').fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success",
                        "message": "The record has been inserted successfully.",
                        "type": "Success"
                    });
                    toastEvent.fire();
                    //this.doInit(component,event,helper);
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get("v.recordId"),
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                    helper.hideSpinner(component);
                }
                else if (res.getState() == "ERROR") {
                    helper.hideSpinner(component);
                    var errors = res.getError();
                    helper.handleErrors(errors);
                }
            });
            $A.enqueueAction(action);
            $A.get('e.force:refreshView').fire();
        }
        else {
            helper.hideSpinner(component);
            var messageOfToast = 'The drawdown amount entered is greater than the amount available to draw on the LoC. Please enter a valid amount.';
            helper.showToast('Error', messageOfToast, 'error');
        }
    },
    handleEdit: function (component, event, helper) {
        component.set("v.readMode", true);
        component.set("v.editMode", false);
        component.set("v.saveMode", true);
    },
    handleCancel: function (component, event, helper) {
        component.set("v.readMode", false);
        component.set("v.editMode", true);
        component.set("v.saveMode", false);
        helper.doInit(component, event, helper);
    },
})