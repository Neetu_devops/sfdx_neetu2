({
	doInit : function(component, event, helper) {
		helper.CummulativeLinechart(component,event,helper);
	},
    closeCummlativeModal : function(component, event, helper){
        var element = document.getElementById("cummulativeModal");
        element.classList.remove("slds-show");
        element.classList.add("slds-hide");
        
    }
})