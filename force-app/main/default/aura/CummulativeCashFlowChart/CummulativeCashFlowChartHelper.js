({
    //Cummulative Cash Flow
    CummulativeLinechart : function(component,event,helper) {
        this.showSpinner(component,event,helper);
        var recordId =component.get("v.recordId");
        var action2 = component.get("c.cummulativeCashFlowData");
        action2.setParams({
            "recordId": recordId
        });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                console.log('State--->>'+state);
                var data = response.getReturnValue();
                var dataObj= JSON.parse(response.getReturnValue()); 
                var flag = false
                component.set("v.cummlativeData",dataObj);
                var monthValue = [];
                for(var i = 0 ;i<dataObj.length;i++){
                    if(dataObj[i].y>0 && flag == false) {
                        component.set("v.breakpoint",dataObj[i].month);
                        console.log('berakpoint', component.get("v.breakpoint"));
                        flag =true;
                    }
                    /* Converting the format of date to UTC */
                    let dateValue = new Date(dataObj[i].month);
                    dataObj[i].month = $A.localizationService.formatDateUTC(dateValue);
                    monthValue.push(dataObj[i].month);
                }
                console.log('monthValue',monthValue);
                component.set("v.xAxisCategories",monthValue);
                this.CummulativeCashLinechart(component,event,helper);
            }
            this.hideSpinner(component,event,helper);
        });
        $A.enqueueAction(action2);
    },
    
    CummulativeCashLinechart : function(component,event,helper) {
        var dataObj  = component.get("v.cummlativeData");
        console.log(dataObj);
        var lstYAxisData = [];
        for(let data of dataObj ){
            lstYAxisData.push( data.y );	    
        }
        lstYAxisData.sort();
        if(component.find("cummulativelinechart") != undefined ){
            new Highcharts.Chart({
                chart: {
                    zoomType: 'xy',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    renderTo: component.find("cummulativelinechart").getElement(),
                    type: 'line'
                },
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 700
                        },
                        chartOptions: {
                            legend: {
                                enabled: false
                            }
                        }
                    }]
                },
                title: {
                    text: 'Cummulative Cash Flow Chart'
                },
                
                xAxis: {
                    categories: component.get("v.xAxisCategories"),
                    crosshair: true, 
                },
                yAxis: {
                    gridLineColor: '#197F07',
                    gridLineWidth: 0,
                    lineWidth:1,
                    plotLines: [{
                        color: '#dcdbdb',
                        width: 1,
                        value: 0
                    }],
                    offset: 10,
                    labels: {
                        formatter: function () {
                            return (this.value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) + 'K';
                        }
                    },
                    title: {
                        text: 'Cash Flow',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                    
                },
                tooltip: {
                    formatter: function() {
                        return '<b>'+ 'Cash Flow' +'</b><br/>'+
                            this.x +': $'+ (this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) +'K';
                        	//(this.x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) +': $'+ (this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) +'K';
                        
                    }
                },
                plotOptions: {
                    "line": {
                        marker: {
                            radius: 4,
                            lineColor: '#666666',
                            lineWidth: 1
                        }
                    },
                    
                    //To show table on point click
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: $A.getCallback(function (evt) {
                                    let accCashFlow = this.y;
                                   	component.set("v.yData",accCashFlow);
                                    component.set("v.month",this.month);
                                    var recordId =component.get("v.recordId");
                                    var action2 = component.get("c.fetchCummulativeDataForTable");
                                    action2.setParams({
                                        "recordId": recordId,
                                        "startDate": evt.point.month
                                    });
                                    action2.setCallback(this, function(response) {
                                        var state = response.getState();
                                        if (state === "SUCCESS") { 
                                            var dataObj= response.getReturnValue();  
                                            if(dataObj !== undefined){
                                                var element = document.getElementById("cummulativeModal");
                                                element.classList.remove("slds-hide");
                                                element.classList.add("slds-show");
                                                component.set("v.cummulativeTableData",dataObj); 
                                            }
                                        }
                                    });
                                    $A.enqueueAction(action2);
                                })
                            }
                        }
                    }
                },
                
                
                series: [{
                    name:'Time Duration',
                    data:dataObj
                }]
                
            });  
        }  
    },
    
     showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
  
})