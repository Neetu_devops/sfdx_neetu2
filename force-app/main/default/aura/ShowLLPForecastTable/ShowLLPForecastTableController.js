({
	doInit : function(cmp, event, helper) {
		helper.fetchInitData(cmp, event);
	},
    handleMouseLeave : function(component, event, helper) {
        let index = event.currentTarget.dataset.index;
        let element = document.getElementById('LLPForecastTable-'+index);
        $A.util.removeClass(element, 'slds-rise-from-ground');
        $A.util.addClass(element, 'slds-fall-into-ground');
    },
    handleMouseEnter : function(component, event, helper) {
   		let index = event.currentTarget.dataset.index;
        let element = document.getElementById('LLPForecastTable-'+index);
        $A.util.addClass(element, 'slds-rise-from-ground');
        $A.util.removeClass(element, 'slds-fall-into-ground');
    },
    navigateToRecord : function(component, event, helper){
        let recordId = event.getSource().get("v.id");
        console.log('recordId: ',recordId);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})