({
	fetchInitData : function(cmp,event) {
		this.showSpinner(cmp);
        let action = cmp.get("c.loadInitData");
        action.setParams({
            recordId: cmp.get("v.recordId"),
            fxMonthlyOutputId: cmp.get("v.fxMonthlyOutputId")
        });
        action.setCallback(this,function(response){
            const state = response.getState();
            if(state == "SUCCESS"){
                let result = response.getReturnValue();
                console.log('result: ',result);
                cmp.set("v.tableData",result.msRecordList);
                cmp.set("v.monthValue",result.msMonth);
               
                cmp.set('v.CSN',this.formatDecimalValues(result.CSN));
                cmp.set('v.TSN',this.formatDecimalValues(result.TSN));
                
                cmp.set('v.buildStandard',this.formatDecimalValues(result.buildStandard));
                cmp.set('v.FH',this.formatDecimalValues(result.FH));
                cmp.set('v.FC',this.formatDecimalValues(result.FC));
                cmp.set('v.mrRate',result.mrRate);
                cmp.set('v.FHDivideFC',result.FHDivideFC);
                
                cmp.set('v.serialNo',result.serialno);
                cmp.set('v.engineType',result.engineType);
                var ShortFallValue = result.msRecordList;
                var count = 0;
                var Key;
                for(Key in ShortFallValue) {
                    if(ShortFallValue[Key].Shortfall_FC != null && ShortFallValue[Key].Shortfall_FC != undefined) {
                        count++;
                    }
                }
         cmp.set("v.ShortFallCount",count);
            }else if(state == "ERROR"){
                this.handleErrors(response.getError());
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
	},
    
    formatDecimalValues : function(decimalValue){
        let formattedString = '';
        if(decimalValue != null && decimalValue != undefined){
            formattedString = decimalValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } 
        return formattedString;
    },
    
	showSpinner: function(cmp){
        const spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function(cmp){
        const spinner = cmp.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
	handleErrors: function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });  
                    };
                }
            });
        }
    }
})