({
    
	fetchData : function(component,event, helper) {
        console.log('Inside Helper');
        var action = component.get("c.getAssetInDealData");
        console.log('After Helper ');
        action.setParams({
            'parentId' : component.get("v.recordId"),
            'parentField' : component.get("v.parentFieldName"),
            'relatedObjectName' : component.get("v.relatedListObject"),
            'fieldSetName' :component.get("v.fieldSetName"),
            'sortOrderField': component.get("v.defaultSortOrder")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                console.log('result:', response.getReturnValue());
                var result = response.getReturnValue();
                var headers = result.headers;
                console.log('headers :',headers);
                
                //Aligning list of columns so that Name should always come as a first column
                headers = this.alignColumns(headers);   
                console.log('headers with Name as start :',headers);
                //Making First column 'Name' hyperlink to show detail page when clicked
                component.set("v.showSpinner", false);  
                console.log('showSpinner :',component.get("v.showSpinner"));
                
                component.set("v.columns", headers);
                console.log('columns :',component.get("v.columns"));
                
                component.set("v.listSize", result.recordList.length);
                console.log('listSize :',component.get("v.listSize"));
                
                component.set("v.relatedListData", result.recordList);  
                console.log('relatedListData :',component.get("v.relatedListData"));
                
                if (component.get("v.titleComponent") != '' && component.get("v.titleComponent") != undefined) {
                    console.log('if titleComponent !- null:',component.get("v.titleComponent"));
                	component.set("v.relatedListLabel", component.get("v.titleComponent")); 
                }
                else {
                    console.log('else titleComponent== null:');
                    component.set("v.relatedListLabel", result.relatedListName); 
                }
                
                if(result.iconName != undefined) {
                    component.set("v.relatedListIcon", result.iconName);  
                }
                else {
                    component.set("v.relatedListIcon", 'custom:custom12');
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    alignColumns : function(headers) {
        var columns = [];
        
        for(var i=0; i<headers.length; i++) {
            if(headers[i].fieldName.toLowerCase() == 'name') {
                headers[i].type = headers[i].type.toLowerCase();
                
                columns.push(headers[i]);
                break;
            }			                              
        }
        
        for(var i=0; i<headers.length; i++) {
            if(headers[i].fieldName.toLowerCase() != 'name') {
                headers[i].type = headers[i].type.toLowerCase();
                
                columns.push(headers[i]);
            }			                              
        }
        
        return columns;
    },


    fetchMatchingRecordType : function(component, event, helper) {
        var action = component.get("c.fetchMatchingRTId");
        action.setParams({
            'parentId' : component.get("v.recordId"),
            'relatedObjectName' : component.get("v.relatedListObject")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                if(response.getReturnValue() != undefined && response.getReturnValue() != '' && response.getReturnValue() != 'Error'){
                    component.set("v.selectedRTId", response.getReturnValue());
                    component.set("v.createNewRecord", true);
                }
                else{
                    this.handleErrors('No matching RecordType found with the parent RecordType Name.');
                }
            }
            else{
                this.handleErrors(response.getError());
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },


    handleErrors : function(errors, type) {
        // Configure error toast
        let toastParams = {
            title: type == undefined ? "Error": type,
            message: "Unknown error", // Default error message
            type: type == undefined ? "error": type,
            duration: '7000'
        };
        if(typeof errors === 'object') {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });  
                    };
                }
            });
        }
        else if(typeof errors === "string" && errors.length != 0){
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    }
})