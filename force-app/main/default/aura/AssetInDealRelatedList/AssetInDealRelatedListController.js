({
	doInit : function(component, event, helper) {
        component.set("v.showSpinner", true);
        console.log('Inside Controller');
        helper.fetchData(component, helper);
    },
    addNewRecord : function (component, event, helper) {        
        var action = component.get("c.getObjectRecTypeId");
        action.setParams({
            'objectName' : component.get("v.relatedListObject")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();

            if(state === 'SUCCESS') {
                if(response.getReturnValue().indexOf("Master") != -1) {
                    component.set("v.createNewRecord", true);
                }
                else {
                    var useParentRecordType = component.get("v.useParentRecordType");
                    if(useParentRecordType != undefined && useParentRecordType != '' && useParentRecordType === 'Use Parent'){
                        component.set("v.showSpinner", true);
                        helper.fetchMatchingRecordType(component, event, helper);
                    }
                    else if(useParentRecordType != undefined && useParentRecordType != '' && useParentRecordType === 'Show Types'){
                        component.set("v.recordTypeSection", true);
                    }
                    else{
                        component.set("v.createNewRecord", true);
                    }
                }
            }
        }); 
        
        $A.enqueueAction(action);
    },
    
    refreshData : function (component, event, helper) {  
        component.set("v.showSpinner", true);
        helper.fetchData(component, helper);
    }
    
    
})