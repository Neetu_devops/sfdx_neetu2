({
    init : function(component, event) {
        console.log('init  :');
        this.showSpinner(component);
        console.log(' SR List : '+component.get("v.listSRitems"));
        var action = component.get("c.loadData");
        action.setParams({
            reconciliationScheduleId : component.get("v.reconciliationId"),
            reconciliationType : component.get("v.reconciliationType"),
            srIds : component.get("v.listSRitems")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('init state :'+state);
            if(state === 'SUCCESS') {
                var containerData = response.getReturnValue();
                var data = containerData.MRReconciliationInvoiceWrapperList;
                var nestedData = [];
                var expandedRows = [];   
                var firstRow;
                
                component.set("v.data", containerData);
                console.log('containerData :'+JSON.stringify(containerData));
                for(var i=0; i<data.length; i++) {
                    if(i==0){
                        firstRow = data[i].title;
                    }
                    var childData = [];
                    if(data[i].childItems) {
                        for(var j=0; j<data[i].childItems.length; j++) {
                            var child = new Object();
                            child.name =  data[i].childItems[j].title;
                            child.PERIODENDING = data[i].childItems[j].title;
                            
                            if(component.get("v.reconciliationType") && component.get("v.reconciliationType") == 'Assumed Ratio'){
                                
                                child.FH = JSON.stringify(data[i].childItems[j].FH);
                            	child.FC = JSON.stringify(data[i].childItems[j].FC);
                                child.ASSUMEDFHFC = JSON.stringify(data[i].childItems[j].assumedFHFC);
                                child.ACTUALFHFC = JSON.stringify(data[i].childItems[j].actualFHFC);
                                child.ASSUMEDRATE = JSON.stringify(data[i].childItems[j].assumedRate);
                                child.ACTUALRATE = JSON.stringify(data[i].childItems[j].actualRate);
                            }    
                            
                            child.ORIGINALAMT = JSON.stringify(data[i].childItems[j].originalAmount);
                            child.RECONCILEDAMT = JSON.stringify(data[i].childItems[j].reconciledAmount);
                            child.DIFFERENCEAMT = JSON.stringify(data[i].childItems[j].differenceAmount);
                            childData.push(child);
                        }
                        
                        var parent = new Object(); 
                        parent.name = data[i].title,
                            parent.PERIODENDING = data[i].title;
                        
                        if(component.get("v.reconciliationType") && component.get("v.reconciliationType") == 'Assumed Ratio'){
                            parent.FH = JSON.stringify(data[i].FH);
                            parent.FC = JSON.stringify(data[i].FC);
                            parent.ASSUMEDFHFC = JSON.stringify(data[i].assumedFHFC);
                            parent.ACTUALFHFC = JSON.stringify(data[i].actualFHFC);
                            parent.ASSUMEDRATE = JSON.stringify(data[i].assumedRate);
                            parent.ACTUALRATE = JSON.stringify(data[i].actualRate);
                        }    
                        
                        parent.ORIGINALAMT = JSON.stringify(data[i].originalAmount);
                        parent.RECONCILEDAMT = JSON.stringify(data[i].reconciledAmount);
                        parent.DIFFERENCEAMT = JSON.stringify(data[i].differenceAmount);
                        parent._children = childData
                        
                        nestedData.push(parent);
                    }
                    else {
                        var parent = new Object();
                        parent.name = data[i].title;
                        parent.PERIODENDING = data[i].title;
                        
                        if(component.get("v.reconciliationType") && component.get("v.reconciliationType") == 'Assumed Ratio'){
                            parent.FH = JSON.stringify(data[i].FH);
                         	parent.FC = JSON.stringify(data[i].FC);
                            parent.ASSUMEDFHFC = JSON.stringify(data[i].assumedFHFC);
                            parent.ACTUALFHFC = JSON.stringify(data[i].actualFHFC);
                            parent.ASSUMEDRATE = JSON.stringify(data[i].assumedRate);
                            parent.ACTUALRATE = JSON.stringify(data[i].actualRate);
                        }
                        
                        parent.RECONCILEDAMT = JSON.stringify(data[i].reconciledAmount);
                        parent.ORIGINALAMT = JSON.stringify(data[i].originalAmount);
                        parent.DIFFERENCEAMT = JSON.stringify(data[i].differenceAmount);
                        
                        nestedData.push(parent);
                        if(data[i].title == "Total") {
                            component.set("v.invoiceAmount", JSON.stringify(data[i].differenceAmount));
                            component.set("v.mrOriginalLabel", data[i].mrOriginalLabel);
                            component.set("v.mrReconcileLabel", data[i].mrReconcileLabel);
                        }
                    }
                } 
                component.set('v.gridData', nestedData);
                
                var columns = [];
                
                columns.push(
                    
                    { type: 'text',
                     fieldName: 'PERIODENDING',
                     label: 'Period Ending',
                     initialWidth: 200
                    }
                );
                
                if(component.get("v.reconciliationType") && component.get("v.reconciliationType") == 'Assumed Ratio'){  
                    
                    columns.push(
                        
                        { type: 'number',
                         fieldName: 'FH',
                         label: 'FH'
                        },
                        
                        { type: 'number',
                         fieldName: 'FC',
                         label: 'FC'
                        },
                        
                        { type: 'number',
                         fieldName: 'ASSUMEDFHFC',
                         label: 'Assumed FH:FC',
                        },
                        
                        { type: 'number',
                         fieldName: 'ACTUALFHFC',
                         label: 'Actual FH:FC',
                        },
                        
                        { type: 'currency',
                         fieldName: 'ASSUMEDRATE',
                         label: 'Assumed Rate',
                        },
                        
                        { type: 'currency',
                         fieldName: 'ACTUALRATE',
                         label: 'Actual Rate',
                        }
                    );
                }       
                
                
                columns.push(
                    
                    {
                        type: 'currency',
                        fieldName: 'ORIGINALAMT',
                        label: component.get('v.mrOriginalLabel')
                    },
                    {
                        type: 'currency',
                        fieldName: 'RECONCILEDAMT',
                        label: component.get('v.mrReconcileLabel')
                    },
                    {
                        type: 'currency',
                        fieldName: 'DIFFERENCEAMT',
                        label: 'Difference Amount'
                    }
                );
                
                
                component.set('v.gridColumns', columns);
                
                var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
                component.set('v.invoiceDate', today);
                
                var screenHeight = screen.availHeight;
                screenHeight = (screenHeight * 65) / 100;
                component.set("v.screenHeight", screenHeight);
                
                screenHeight = (screenHeight * 75) / 100; 
                component.set("v.tableGridHeight", screenHeight);
            }
            else if(state === 'ERROR') {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action); 
    },
    
    //fetch SR data
    fetchSRData: function(component, event, type) {
        this.showSpinner(component);
        var action = component.get("c.getSRdetails");
        action.setParams({
            "recordId": component.get("v.reconciliationId"),
            
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (response.getState() === "SUCCESS") {
                
                var allData =  response.getReturnValue();
                component.set("v.mrData", allData);
                
                if(component.get("v.reconciliationType") && component.get("v.reconciliationType ") == 'Assumed FH and FC'){
                var allDataValues = allData.srDetails;
        
                var srList = [];
                 for(var i = 0; i < allDataValues.length; i++){
                    srList.push(allDataValues[i].srId);
                 }
                console.log('srList :'+srList);
                component.set('v.listSRitems',srList);
                    this.init(component, event, helper);
                }
                
                if(component.get("v.reconciliationType") && component.get("v.reconciliationType ") == 'Assumed Ratio'){
                var allDataValues = allData.srDetails;
        
                var srList = [];
                 for(var i = 0; i < allDataValues.length; i++){
                     if(allDataValues[i].isSelected){
                    srList.push(allDataValues[i].srId);
                     }
                 }
                console.log('srList :'+srList);
                    if(srList.length > 0){
                component.set('v.listSRitems',srList);
                    }
                }
                var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
                component.set('v.invoiceDate', today);
                
                var screenHeight = screen.availHeight;
                screenHeight = (screenHeight * 65) / 100;
                component.set("v.screenHeight", screenHeight);
                    
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    //to show spinner while the data is loading
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinner",true);
    },
    
    //to hide spinner after the data is loaded
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinner",false);
    },
    
    //to show the toast message
    showToast : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
    
    //to handle errors
    handleErrors: function (errors, type) {
        _jsLibrary.handleErrors(errors, type); // Calling the JS Library function to handle the error
    },
    
    save : function(component, event) {
        
        if(component.get("v.savePage2")){
        var action = component.get("c.saveData");
        action.setParams({
            data : JSON.stringify(component.get('v.data')),
            invoiceDate : component.get("v.invoiceDate"),
            paymentDate : component.get("v.paymentDate")
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                this.showToast("Success", "Invoice created successfully!","SUCCESS");
                var recordId = response.getReturnValue();
                if(recordId != null) {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId
                    });
                    navEvt.fire();
                }     
            }
            else if(state === 'ERROR') {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
        }
    },
   
})