({ 
    
    // eslint-disable-line
    init : function (component,event,helper) {
        helper.fetchSRData(component,event);
        
    },
    
    back : function(component,event,helper){
        console.log("back");
        component.set("v.page1",true);
            component.set("v.page2",false);
            
    },
    save : function(component, event, helper) {
        var invoiceDate = component.get('v.invoiceDate');
        if(invoiceDate == undefined || invoiceDate == ''){
            helper.showToast("Error", "Please select a Invoice Date.","Error");
           
        }else{
            if(component.get("v.page1")){
                console.log('if');
                helper.init(component, event);
                if(component.get("v.listSRitems").length > 0){
                component.set("v.page1",false);
                component.set("v.page2",true);
                
                }else{
                    helper.showToast("Error", "Please select a row to generate Invoice.","Error");
                }
            }
            else{
                console.log('else');
                component.set("v.savePage2",true);
                component.set("v.page1",false);
                component.set("v.page2",true);
                
                helper.save(component, event);
            }
        }
        
    },
    
     fetchSelectedSR : function(component, event, helper) {
        var indx = event.currentTarget.getAttribute("data-index");
         console.log('indx :'+indx);
        var mrUtilizationData = component.get('v.mrData');
        var tableValues = mrUtilizationData.srDetails;
        
        var srList = [];
         for(var i = 0; i < tableValues.length; i++){
             console.log('tableValues[i].isSelected :'+tableValues[i].isSelected);
            if(tableValues[i].isSelected){
                srList.push(tableValues[i].srId);
            }
        }
         console.log('srList :'+srList);
        component.set('v.listSRitems',srList);
        mrUtilizationData.srDetails = tableValues;
        component.set('v.mrData',mrUtilizationData);
        
    }
});