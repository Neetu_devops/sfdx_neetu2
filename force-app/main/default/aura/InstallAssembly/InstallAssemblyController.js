({
	onPageReferenceChange : function(component, event, helper) {
		helper.onPageReferenceChange(component, event);
	},
    assetChange : function(component, event, helper) {
		
        if(event.getParam("value").Id && component.get("v.prevAssetTSN") == component.get("v.data.assetTSN") 
           && component.get("v.prevAssetCSN") == component.get("v.data.assetCSN")){
            helper.assetChange(component, event);
        }    
        else if(component.get("v.prevAssetTSN") == component.get("v.data.assetTSN") && 
                component.get("v.prevAssetCSN") == component.get("v.data.assetCSN")){
            helper.clearFields(component, event);
        }
        else if(event.getParam("value").Id && component.get("v.prevAssetTSN") != null 
           && component.get("v.prevAssetCSN") != null) {
            var data = component.get("v.data");
            data.assetId = event.getParam("value").Id;
            data.leaseId = null;
            component.set("v.data",data);
        }
        
    },
    cancel : function(component, event, helper) {
		helper.cancel(component, event);
	},
    proceed: function(component, event, helper){
        component.set("v.showWarningModal", false);
      	helper.proceedFurther(component, event);  
    },
    closeWarningModal: function(component, event, helper){
        helper.hideSpinner(component);
      component.set("v.showWarningModal", false);  
    },
    save : function(component, event, helper) {
        
        if(helper.check(component)) 
        	helper.save(component, event);
        else
            helper.showToast('Error','Please fill all fields!','ERROR');
	},
    
    onDateChange: function(component, event, helper) {
        if(component.get("v.prevAssetTSN") == component.get("v.data.assetTSN") && 
          component.get("v.prevAssetCSN") == component.get("v.data.assetCSN"))
        	helper.onDateChange(component, event);
    }
})