({
	Barchart : function(component,event,helper) {
        var leasorData  = component.get("v.data");
        
        var mrData  = component.get("v.mrData");
        var leaseeData  = component.get("v.leaseeData");
        var engineData  = component.get("v.engine");
        var airframeData  = component.get("v.airframe");
        
        var str ='',strAirframe ='';
        if(engineData != undefined){
        	str = JSON.stringify(engineData,['month','y']);
        }if(airframeData != undefined){
        	strAirframe = JSON.stringify(airframeData,['month','y']);
        }
        
        if(str != '' && str != undefined){
            var data = str.replace(/"y":/g, "");
            var data2 = data.replace(/{/g, "[");
            var data3 = data2.replace(/}/g, "]");
            var data4 = data3.replace(/"month":/g, "");
            var engineValue = JSON.parse(data4);
        }
        
        if(strAirframe != '' && str != undefined){
            var data = strAirframe.replace(/"y":/g, "");
            var data2 = data.replace(/{/g, "[");
            var data3 = data2.replace(/}/g, "]");
            var data4 = data3.replace(/"month":/g, "");
            var airframeValue = JSON.parse(data4);
        }
        
        var marker  = component.get("v.marker");
       /* var jsonData2  = component.get("v.data2"); 
        var dataObj2  = JSON.parse(jsonData2);*/
        
        new Highcharts.Chart({
            chart: {
                marginRight: 80,
                type: 'column',
                zoomType: 'x',
                renderTo: component.find("Barchart").getElement()
            }, title: {
                text: 'Maintenance Details'
            },
            xAxis: [{
                categories:  component.get("v.xAxisCategories")
            }],yAxis: {
                gridLineColor: '#197F07',
                gridLineWidth: 0,
                lineWidth:1,
                plotLines: [{
                    color: '#dcdbdb',
                    width: 1,
                    value: 0
                }],
                offset: 20,
                labels: {
                    formatter: function () {
                        return (this.value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) + 'K';
                    }
                },  title:
                {
                    text: component.get("v.yAxisParameter")
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                },series: {
                    borderWidth: 0, 
                    //To show table on point click
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: $A.getCallback(function (evt) {
                               component.set("v.month",evt.point.month);
                               var recordId =component.get("v.recId");
                               var action = component.get("c.getCashFlowBarTableData");
                                action.setParams({
                                    "recordId": recordId,
                                    "startDate": evt.point.month
                                });
                                action.setCallback(this, function(response) {
                                    var state = response.getState();
                                    if(state == "SUCCESS") { 
                                        console.log("Maintenace Success",response.getReturnValue());
                                        var dataObj= response.getReturnValue(); 
                                        if(dataObj !== undefined){
                                            var element = document.getElementById("modalCashFlowMain");
                                            element.classList.remove("slds-hide");
                                            element.classList.add("slds-show");
                                            component.set("v.cashFlowMainTableData",dataObj); 
                                            
                                            let width = 100/(dataObj.length + 1);
                                            component.set("v.cellWidth",width);
                                            
                                        }
                                    }
                                });
                                $A.enqueueAction(action); 
                            })
                        }
                    }
                } 
            }, 
            tooltip: {
                formatter: function() {
                    if(this.series.name == 'marker' ){
                        return false ;
                        // to disable the tooltip at a point return false 
                    }else {
                        //pointFormat: '<span style="color:{point.color}">{series.name}</span>: <b>{point.y:.2f} </b> <br/>'
                        return '<b>'+ this.series.name +'</b><br/>'+
                            this.x +': '+  (this.y).toLocaleString( {style: "currency", currency: "USD", minimumFractionDigits: 0})+'K'+ '<br/>' + 'Total : ' + (this.point.stackTotal).toLocaleString( {style: "currency", currency: "USD", minimumFractionDigits: 0})+'K';
                    }
                }   
            }, 
            series: [{
                    name:'Lessor Contribution ',
                    data:leasorData ,
                    //data: [null,-3,null,-9,null,-4,null,-4,null,-1,null,-3],
                    stack:component.get("v.stack"),
                    color:'#de7575',
                },{
                    name:'MR Contribution',
                    //data: [null,-3,null,-9,null,-4,null,-4,null,-2,null,-1],
                    stack:component.get("v.stack"),
                    data:mrData,
                    color:'#7cb5ec'
                },{
                    name:'Lessee Contribution ',
                    //data: [null,-3,null,-9,null,-4,null,-4,null,-8,null,-10],
                    data:leaseeData,
                    color:'#60c760',
                    
                },{
                    name : 'marker',
                    groupPadding: 2,
                    showInLegend:false,
                    data: marker,
                    stack:component.get("v.stack"),
                    type: 'scatter',  
                },
            ], 
       });
   },
    
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
})