({
    scriptsLoaded : function(component, event, helper){
        helper.showSpinner(component, event, helper);
        var action = component.get("c.getCashFlowBarData");
        var recordId =component.get("v.recId");
        console.log('******',recordId);
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                console.log("Values----------->>>"+dataObj);
                if(dataObj != undefined)
                    var leasor = JSON.parse(dataObj);
                if(leasor.leasordata != undefined)
                    var leasordata = leasor.leasordata;
                if(leasor.mrdata != undefined)
                    var mrdata = leasor.mrdata;
                if(leasor.leaseedata != undefined)
                    var leaseedata = leasor.leaseedata;
                if(leasor.stackVal != undefined)
                    var stackVal = leasor.stack;
                if(leasor.marker != undefined)
                    var marker = leasor.marker;
                if(leasor.Engine != undefined)
                    var enginedata = leasor.Engine;
                if(leasor.airframe != undefined)
                    var airframeData = leasor.airframe;
                component.set("v.data",leasordata);
                component.set("v.mrData",mrdata);
                component.set("v.leaseeData",leaseedata);
                component.set("v.engine",enginedata);
                component.set("v.airframe",airframeData);
                component.set("v.marker",marker);
                component.set("v.stack",stackVal);
                
                var monthValue = [];
                if(leasordata.length != undefined){
                    for(var i = 0 ;i<leasordata.length;i++){
                        monthValue.push(leasordata[i].month);
                    } 
                }
                component.set('v.xAxisCategories',monthValue);
                helper.Barchart(component,event,helper);
            }else{
                console.log("error");
            }
            helper.hideSpinner(component, event, helper);
        });        
        $A.enqueueAction(action);
    }, 
    closeModal : function(component, event, helper){
        var element = document.getElementById("modalCashFlowMain");
        element.classList.remove("slds-show");
        element.classList.add("slds-hide");
        
    },
})