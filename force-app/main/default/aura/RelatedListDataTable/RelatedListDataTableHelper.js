({
    showMessage : function(message, type, mode) {
        var toastEvent = $A.get("e.force:showToast");              
        toastEvent.setParams({  
            mode: mode,
            message: message,                   
            type: type
        }); 
        toastEvent.fire(); 
    },
    
    
    sortData : function(component, helper, fieldName, sortDirection) {
        console.log('fieldName:', fieldName);
        var columns = component.get("v.columns");
        var data = component.get("v.relatedListData");
        
        //function to return the value stored in the field
        var key = function(a) { 
            return a[fieldName];
        }
        var reverse = sortDirection == 'asc' ? 1 : -1;
        
        // to handel number/currency type fields 
        for(var i=0; i<columns.length; i++) {
            if(fieldName == columns[i].fieldName) {
                if(columns[i].type.toLowerCase() == 'integer' || columns[i].type.toLowerCase() == 'percent' || columns[i].type.toLowerCase() == 'currency') {                    
                    data.sort(function(a,b){
                        var a = key(a) ? key(a) : '';
                        var b = key(b) ? key(b) : '';
                        return reverse * ((a>b) - (b>a));
                    }); 
                }
                else {
                    // to handel text type fields 
                    data.sort(function(a,b) { 
                        //To handle null values , uppercase records during sorting
                        var a = key(a) ? key(a).toLowerCase() : '';
                        var b = key(b) ? key(b).toLowerCase() : '';
                        return reverse * ((a>b) - (b>a));
                    });    
                }
                
                //set sorted data to dataForPagination attribute
                component.set("v.relatedListData", data);
                break;
            }
        }
        
        component.set("v.showSpinner", false);
    }
})