({
    doInit : function (component, event, helper) {
      
    },

    
    sortColumns : function (component, event, helper) {
        var fieldName = event.target.dataset.id; 
        var headers = component.get("v.columns");
        var sortDirection;
        
        headers.map(function(data, index) {
            if(data.fieldName == fieldName) {
                if(data.sortIcon == 'arrowdown') {
                    sortDirection = 'asc';
                    Object.assign(data, {"sortIcon" : 'arrowup'});
                }
                else {
                    sortDirection = 'desc';
                    Object.assign(data, {"sortIcon" : 'arrowdown'});
                }
            }
        });                      
        
        component.set("v.columns", headers);
        component.set('v.showSpinner', true);
        helper.sortData(component, helper, fieldName, sortDirection);
    },
    
    
    editSelectedRecord : function(component, event, helper) { 
        var recordIndex = event.target.dataset.id;
        var rows = component.get('v.relatedListData');
        var recordId = rows[recordIndex].Id;
        var recordName = rows[recordIndex].Name;
                
        component.set("v.selectedRecordId", recordId);
        component.set("v.recordName", recordName);      
        component.set("v.actionType", 'Edit');    
        component.set("v.createNewRecord", true);
    },
    
    
    cloneSelectedRecord : function(component, event, helper) {   
        var recordIndex = event.target.dataset.id;
        var rows = component.get('v.relatedListData');
        var recordId = rows[recordIndex].Id;
        var recordName = rows[recordIndex].Name;
                
        component.set("v.selectedRecordId", recordId);
        component.set("v.recordName", recordName);      
        component.set("v.actionType", 'Clone'); 
        component.set("v.createNewRecord", true);
    },
    
    
    confirmDelete : function(component, event, helper) {
        component.set("v.confirmDeleteSection", true);   
        component.set("v.recordIndex", event.target.dataset.id);
    },
    
    
    deleteSelectedRecord : function(component, event, helper) {
        var recordIndex = component.get("v.recordIndex");
        var rows = component.get('v.relatedListData');
        var recordId = rows[recordIndex].Id;
        var recordName = rows[recordIndex].Name;
        var objectLabel = component.get('v.relatedListLabel');
        
        component.set('v.showSpinner', true);
        
        var action = component.get("c.deleteRecord");
        action.setParams({
            'delRecordId' : recordId,
            'objectName' : component.get("v.relatedListObject")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                if(response.getReturnValue() == 'Success') {
                    rows.splice(recordIndex, 1);
                    
                    component.set('v.relatedListData', rows);   
                    component.set('v.ctListSize', rows.length); 
                    component.set('v.showSpinner', false);
                    helper.showMessage(objectLabel + ' "' + recordName + '" was deleted', 'success'); 
                	
                    var refreshEvt = $A.get("e.c:CommercialTermListRefreshEvent");
                	refreshEvt.fire();

                    $A.get('e.force:refreshView').fire();
                }
                else {
                    component.set('v.showSpinner', false);
                    helper.showMessage(response.getReturnValue(), 'error', 'sticky'); 
                } 
            }
        });
        
        $A.enqueueAction(action);                
    },
})