({
	fetchInitData : function(cmp,event) {
		this.showSpinner(cmp);
        let engineNo = cmp.get("v.filtertype").split('#')[1];
        console.log('engineNo: ', engineNo);
        cmp.set("v.engineNo",engineNo);
        let action = cmp.get("c.loadInitData");
        action.setParams({
            recordId: cmp.get("v.recordId"),
            filtertype: cmp.get("v.filtertype")
        });
        action.setCallback(this,function(response){
            const state = response.getState();
            if(state == "SUCCESS"){
                cmp.set("v.tableData",response.getReturnValue());
            }else if(state == "ERROR"){
                this.handleErrors(response.getError());
            }
            this.hideSpinner(cmp);
        });
        $A.enqueueAction(action);
	},
	showSpinner: function(cmp){
        const spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner: function(cmp){
        const spinner = cmp.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
	handleErrors: function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });  
                    };
                }
            });
        }
    }
})