({
    doInit : function(cmp, event, helper) {
        helper.fetchInitData(cmp, event);
    },
    handleMouseLeave : function(component, event, helper) {
        let index = event.currentTarget.dataset.index;
        console.log('index: ', index);
        let element = document.getElementById('LLPForecastTable-'+index);
        console.log('element: ',element);
        $A.util.removeClass(element, 'slds-rise-from-ground');
        $A.util.addClass(element, 'slds-fall-into-ground');
    },
    handleMouseEnter : function(component, event, helper) {
   		let index = event.currentTarget.dataset.index;
        console.log('index: ', index);
        let element = document.getElementById('LLPForecastTable-'+index);
        console.log('element: ',element);
        $A.util.addClass(element, 'slds-rise-from-ground');
        $A.util.removeClass(element, 'slds-fall-into-ground');
    }
})