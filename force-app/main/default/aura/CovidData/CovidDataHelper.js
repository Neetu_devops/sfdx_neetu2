({
    //To get the covid data for particular record id.
    getCovidData : function(component) {
        component.set("v.isLoading", true);
        var recordIdL = component.get("v.recordId");
        console.log('getCovidData recordId: '+recordIdL );
        var self = this;
        if($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getPandamicDetails');
        action.setParams({
            recordId : recordIdL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.isLoading", false);
            console.log("getCovidData : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getCovidData values " +allValues);
                if(allValues != null) {
                    console.log("getCovidData values " +JSON.stringify(allValues));
                    component.set("v.covidData", allValues);
                    var red = "#FF0000";
                    var green = "#228B22";
                    var orange = "#FFA500";
                    if(allValues.recoveryRate != null && allValues.regionRecoveryRate != null) {
                        allValues.recoveryRate = parseFloat(allValues.recoveryRate);
                        allValues.regionRecoveryRate = parseFloat(allValues.regionRecoveryRate);
                        if(allValues.recoveryRate <= allValues.regionRecoveryRate) {
                            component.set("v.recoveryColor", red);
                        }
                        else if(allValues.recoveryRate > allValues.regionRecoveryRate) {
                            component.set("v.recoveryColor", green);
                        }
                    }
                    if(allValues.fatalityRate != null && allValues.regionFatalityRate != null) {
                        allValues.fatalityRate = parseFloat(allValues.fatalityRate);
                        allValues.regionFatalityRate = parseFloat(allValues.regionFatalityRate);
                        if(allValues.fatalityRate >= allValues.regionFatalityRate) {
                            component.set("v.fatalityColor", red);  
                        }
                        else if(allValues.fatalityRate < allValues.regionFatalityRate ) {
                            component.set("v.fatalityColor", green);  
                        }
                    }
                }
                else {
                    component.set("v.isLoading", false);
                    self.showErrorMsg(component, "Covid Data not found!");
                }
            }
            else if (state === "ERROR") {    
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getCovidData errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getCovidData unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('error '+ toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('error '+ toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('error '+ toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
})