({
	doInit : function(component, event, helper) {
		helper.getCovidData(component);
	},

	onExpandClick : function(component, event, helper) {
		var isExpand = component.get("v.isExpand");
		
		if(isExpand == true)
			component.set("v.expandLabel", 'More');
		else
			component.set("v.expandLabel", 'Less');
		component.set("v.isExpand", !isExpand);
	},
	
    showRecord : function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log('showRecord recordId '+recordId);
        window.open('/' + recordId);  
    },

})