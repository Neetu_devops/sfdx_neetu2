({
    //set source lookup
    setSourceId : function(component, event) { 
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.selectedDate', today);

        if(component.get("v.recordId") != undefined && component.get("v.recordId") != '') {
             // call the apex class method 
            let action = component.get("c.getLeaseRecord"); 
            this.showSpinner(component);
            action.setParams({'recordId': component.get("v.recordId")});
            // set a callBack    
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    
                    var result = response.getReturnValue();
                    component.set("v.sourceLease",result);
                    component.set("v.showLookups",true);
                 
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    this.handleErrors(errors);
                    component.set("v.showLookups",true);
                }
                this.hideSpinner(component);
                
            });
            $A.enqueueAction(action); 	
        } else {
            component.set("v.showLookups",true);
        }
    },
    
    //to validate the lookups and fill the table
    validateFields : function(component, event) { 
        
        component.set('v.mappingRecCount', 0);
        var chkSource = component.get('v.sourceLease.Id');
        var chkTarget = component.get('v.targetLease.Id');
        var selDate = component.get('v.selectedDate');
        
        if(!chkSource || !chkTarget || !selDate) {
            
            this.showToast("Error!","Please fill the required fields.","ERROR");
        }
        else {
            this.showSpinner(component);
            var action = component.get('c.getRecordData');
            action.setParams({
                sourceId : chkSource,
                targetId : chkTarget,
                sourceLeaseName : component.get('v.sourceLease.Name'),
                targetLeaseName : component.get('v.targetLease.Name'),
                selectedDate : component.get('v.selectedDate')
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var tableData = response.getReturnValue();
                    
                    if(tableData.sourceObjectRecords != undefined) {
                        
                        if(tableData.mapTargetObjectRecord == undefined) {
                            this.showToast("Error", "No Records to display in Target Lease!","ERROR");
                            
                        } else {
                            for (var i = 0; i < tableData.sourceObjectRecords.length; i++) {
                                var obj = new Object();
                                tableData.sourceObjectRecords[i].targetRecord = obj;
                                tableData.sourceObjectRecords[i].sourceLeaseId = chkSource;
                                tableData.sourceObjectRecords[i].targetLeaseId = chkTarget;
                            }
                            
                            var targetData = [];
                            var map = tableData.mapTargetObjectRecord;
                            for (var key in map) {
                                targetData.push({
                                    value : map[key]
                                    //serializeValue : JSON.stringify(map[key])
                                });
                            }
                            
                            component.set('v.sourceAssemblyInfo', tableData.sourceObjectRecords);
                            component.set('v.targetAssemblyInfo', targetData);
                            component.set('v.roundUpDown', targetData.roundUpOrDown);
                            
                        }
                    }
                    else {
                        this.showToast("Error", "No Records to display in Source Lease!","ERROR");
                    }
                    
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    this.handleErrors(errors);
            	}
                this.hideSpinner(component);
            });
            $A.enqueueAction(action);
            
        }
    },
    
    //on edit amout or percent value
	editFunction : function(component, event) {
        var sectionId = component.get('v.sectionId');
        if(sectionId) {
            var box = document.getElementById(sectionId);
            $A.util.addClass(box, "toggle");
        }
        var clickedElement = event.getSource().get('v.value');
        console.log('editFunction clickedElement : ',clickedElement);
        
        var substr = clickedElement.split("-");
        console.log('editFunction substr : ',substr);
        console.log('editFunction substr.length : ',substr[0].length);
        
        if(substr[0].length == 1){
       	var index = clickedElement.substring(0,1);
        console.log('editFunction index if : ',index);
        }
        else{
        var index = clickedElement.substring(0,substr[0].length);
        console.log('editFunction index else : ',index); 
        }
        var action = clickedElement.slice(substr[0].length+1);
        console.log('editFunction action : ',action);
       	
        var inputId = document.getElementById(clickedElement+'-input');
        console.log('editFunction inputId : ',inputId);
       	
        if(action=="EditAmount") {
            var val = component.get('v.sourceAssemblyInfo')[index].transferAmount;
            inputId.value = val;
        } else {
            var val = component.get('v.sourceAssemblyInfo')[index].amountPercent;
            inputId.value = val;
        }
        
		var box = document.getElementById(clickedElement);
        
        $A.util.removeClass(box, "toggle");
        component.set('v.sectionId', clickedElement);
        setTimeout(function(){ 
            inputId.focus();},10);
	},
    
    //to set edited amount or percent to fields
    clickInput : function(component, event) { 
        var sectionId = component.get('v.sectionId');
        console.log("clickInput sectionId : ",sectionId);
        var val = document.getElementById(sectionId+'-input').value;
        
        var substr = sectionId.split("-");
        console.log('editFunction substr : ',substr);
        console.log('editFunction substr.length : ',substr[0].length);
        
        if(substr[0].length == 1){
       	var index = sectionId.substring(0,1);
        console.log('editFunction index if : ',index);
        }
        else{
        var index = sectionId.substring(0,substr[0].length);
        console.log('editFunction index else : ',index); 
        }
        var action = sectionId.slice(substr[0].length+1);
        console.log('editFunction action : ',action);
       	
        
        /*var index = sectionId.substring(0,1);
        console.log("clickInput index : ",index);
        var action = sectionId.slice(2);
        console.log("clickInput action : ",action);*/
        var map = component.get('v.sourceAssemblyInfo');
        if(val=='') val=0;
        if(action=="EditAmount") {
            
            var sAmount = map[index].sourceRecord.availableAmount;
            var tAmount = parseFloat(val);
            var roundUporDown = component.get('v.roundUpDown');
            tAmount =  Math.round(tAmount * 100)/100;
            
            
            if(tAmount > sAmount) {
                tAmount = 0;
                this.showToast("Error!","Insufficient Balance in Source.","ERROR");
            }
            else if(tAmount < 0) {
                tAmount = 0;
                this.showToast("Error!","Amount cannot be Negative.","ERROR");
            }
            else {
                map[index].transferAmount = tAmount
                component.set('v.sourceAssemblyInfo',map);
                
                this.afterTransferCalculation(index,action,tAmount,component);
            }
            
        } else {
            
            var tPercent = parseFloat(val);
            console.log("clickInput tPercent : ",tPercent);
            if(tPercent > 100)  {
                console.log("clickInput if1");
               	tPercent = 0;
               	this.showToast("Error!","Percentage cannot be greater than 100.","ERROR");
        	}
        	else if(tPercent < 0) {
                console.log("clickInput if2");
            	tPercent = 0;
               	this.showToast("Error!","Percentage cannot be less than 0.","ERROR");
        	}
            else { 
        		this.afterTransferCalculation(index,action,tPercent,component); 
            }
        }
        
        var box = document.getElementById(sectionId);
        $A.util.addClass(box, "toggle");
    },
    
    //on click dropdown of target lookup
    changeData : function(component, event) { 
        
        var clickedElement = event.target.id;
        var index = event.target.getAttribute("data-selected-Index");
        var val = document.getElementById(clickedElement).value;
        var count = component.get('v.mappingRecCount');
        var map = component.get("v.targetAssemblyInfo");
        var mapTargetRecord = component.get('v.sourceAssemblyInfo');
        
        if(val) {
            
           	var perviousVal = mapTargetRecord[index].targetAmountAfterTransfer;
            if(!perviousVal) {
                count = count + 1;
            	component.set('v.mappingRecCount', count);
            }
            //mapTargetRecord[index].targetRecord = JSON.parse(map[val].serializeValue);
            mapTargetRecord[index].targetRecord.recordId = map[val].value.recordId;
            mapTargetRecord[index].targetRecord.recordName = map[val].value.recordName;
            mapTargetRecord[index].targetRecord.availableAmount = map[val].value.availableAmount;
            mapTargetRecord[index].targetRecord.leaseId = map[val].value.leaseId;
            mapTargetRecord[index].targetRecord.serialNumber = map[val].value.serialNumber;
            
            mapTargetRecord[index].targetAmountAfterTransfer = map[val].value.availableAmount;
            
            component.set('v.sourceAssemblyInfo', mapTargetRecord);
            
            this.afterTransferCalculation(index,'','',component);
        }
        else {
            var obj = new Object();
            mapTargetRecord[index].targetRecord = obj;
            mapTargetRecord[index].targetAmountAfterTransfer = null;
            mapTargetRecord[index].amountPercent = 0;
            mapTargetRecord[index].transferAmount = 0;
            mapTargetRecord[index].sourceAmountAfterTransfer = mapTargetRecord[index].sourceRecord.availableAmount;
            component.set('v.sourceAssemblyInfo', mapTargetRecord);
            
            if(count > 0) count = count - 1;
            else count = 0;
            component.set('v.mappingRecCount', count);
        }
    },
    
    //on click add all
    addAllClick : function(component, event) {
        
        var index = event.getSource().get("v.value");
        this.afterTransferCalculation(index,'addAll',100,component);
    },
    
    //after transfer the balance final calculation
    afterTransferCalculation : function(index,action,value,component) {
        var map = component.get('v.sourceAssemblyInfo');
        var availSourceBal = map[index].sourceRecord.availableAmount;
        var availDestinationBal = map[index].targetRecord.availableAmount;
        var transferBal = map[index].transferAmount;
        var tPer = document.getElementById(index+'-tPercent');
        var roundUporDown = component.get('v.roundUpDown');
        
        if(action && (value || value == 0)) {
            if(action == "EditAmount" && availSourceBal > 0) {
                var tpercent = ((transferBal/availSourceBal) * 100);
                tpercent =  Math.round(tpercent * 100)/100;

                
                map[index].transferAmount = value;
                map[index].amountPercent = tpercent;
                tPer.innerHTML = tpercent + '%';
            }
            else if(action == "EditAmount" && availSourceBal == 0) {
                var tpercent = 0;
                tpercent = parseFloat(Math.round(tpercent * 100) / 100).toFixed(2);
                map[index].transferAmount = 0;
                map[index].amountPercent = tpercent;
                tPer.innerHTML = tpercent + '%';
            }
            else if(action == "EditPercent") {
                transferBal = (availSourceBal * (value/100));
                transferBal =  Math.round(transferBal * 100)/100;
                
                map[index].transferAmount = transferBal;
                var tpercent = parseFloat(Math.round(value * 100) / 100).toFixed(2);
                map[index].amountPercent = tpercent;
                tPer.innerHTML = tpercent + '%';
            }
            else {
                if(availSourceBal < 0) {
                    this.showToast("Error!","Source Balance is negative.","ERROR");
                } else {
                    tPer.innerHTML = 100.00 + '%';
                    transferBal = availSourceBal;
                    map[index].amountPercent = 100.00;
                    map[index].transferAmount = availSourceBal;
                }
            }
        }
        
        map[index].sourceAmountAfterTransfer = availSourceBal - transferBal;
        if(availDestinationBal != null) map[index].targetAmountAfterTransfer = availDestinationBal + transferBal;
        
        component.set('v.sourceAssemblyInfo', map);
    },
    
    //to preview the final data
    previewData : function(component, event) { 
        
        $A.util.removeClass(component.find("displayPreview"), "toggle");
        var summary = component.find("summaryInfo");
        summary.getSummaryInfoMethod();
        
    },
    
    //to close the preview dialog
    clickClose : function(component, event) { 
        $A.util.addClass(component.find("displayPreview"), "toggle");
    },
    
    //to save the final transaction data 
    clickSave : function(component, event) { 
        
        var action = component.get("c.saveData");
        var wrapperList = component.get("v.sourceAssemblyInfo");
        
        var jsonData = JSON.stringify(wrapperList);
        
        this.showSpinner(component);
        action.setParams({
            recordToUpdate : jsonData,
            transferDate : component.get("v.selectedDate")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                this.showToast("Success", "Record created Successfully!","SUCCESS");
                var recordId = response.getReturnValue();
                if(recordId != null) {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId
                    });
                    navEvt.fire();
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
            $A.util.addClass(component.find("displayPreview"), "toggle");
        });
        $A.enqueueAction(action);
    },
    
    //to show the toast message
    showToast : function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
    
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    
    //to show spinner while the data is loading
    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    //to hide spinner after the data is loaded
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    //to cancel the MR fund transfer form
    onCancel : function(component, event) {
        
        var recordId = component.get('v.recordId');
        
        if(recordId != null) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
    },
    
    //to open the record
    onRecordClick : function (component, event) {
        var recordId = event.target.id;
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId
        });
        navEvt.fire();
    },
    
    //on changing the lookup field value
    onLookupFieldChange : function (component, event) { 
        var tableData = [];
        component.set('v.sourceAssemblyInfo', tableData);
        component.set('v.mappingRecCount', 0);
        component.set("v.changedDate", null);
    },
    
    //to show the confirmation modal
    handleComponentEvent : function (component, event) { 
    	var param = event.getParam("identifier");
        component.set("v.identifier", param);
        var modal = component.find('confirmationModal');
        $A.util.removeClass(modal, "slds-hide");
        
    },
    
    //to check condition on change selected date
    dateChange :function(component, event, helper) {
        component.set("v.changedDate",event.getParam("value"));
        if(component.get("v.mappingRecCount") > 0) {
            var modal = component.find('confirmationModal');
            $A.util.removeClass(modal, "slds-hide");
            component.set("v.selectedDate", event.getParam("oldValue"));
            component.set("v.identifier", '');
        }
        else {
            this.onLookupFieldChange(component, event);
        }
    },
    
    //to reset the values of removing lookup field
    proceedToContinue : function (component, event) { 
    	
        var eventName = component.get("v.identifier");
        if(eventName == "target") {
            var target = component.find("targetLookUp");
            target.clearPill();
        } else if(eventName == "source") {
            var source = component.find("sourceLookUp");
            source.clearPill();
        } else {
            component.set("v.selectedDate", component.get("v.changedDate"));
            component.set("v.changedDate", null);
        }
        
        var modal = component.find('confirmationModal');
        $A.util.addClass(modal, "slds-hide");
    },
    
    //to close the confirmation modal
    closeConfirmationModal : function (component, event) {
        
        var modal = component.find('confirmationModal');
        $A.util.addClass(modal, "slds-hide");
    }
})