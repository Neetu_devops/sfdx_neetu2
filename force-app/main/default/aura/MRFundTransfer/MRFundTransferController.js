({
    doInit : function(component, event, helper) {
		var myPageRef = component.get("v.pageReference");
        var recordId = myPageRef.state.c__recordId;
        component.set("v.recordId", recordId);
        helper.setSourceId(component, event);
        component.set("v.targetLease",'');

	},
    
	handleClick : function(component, event, helper) {
		
        helper.editFunction(component, event);
	},
    
    changeInput : function(component, event, helper) {
		
        helper.clickInput(component, event);
	},
    
    checkFields : function(component, event, helper) {
		
        helper.validateFields(component, event);
	},
    
    onSelectChange : function(component, event, helper) {
		
        helper.changeData(component, event);
        
        if(component.get("v.mappingRecCount") > 0) component.set("v.changePreventEvent",true);
        else component.set("v.changePreventEvent",false);
	},
    
    clickAddAll : function(component, event, helper) {
		
        helper.addAllClick(component, event);
	},
    
    preview : function(component, event, helper) {
		
        helper.previewData(component, event);
	},
    close : function(component, event, helper) {
		
        helper.clickClose(component, event);
	},
    save : function(component, event, helper) {
		
        helper.clickSave(component, event);
	},
    cancel : function(component, event, helper) {
		
        helper.onCancel(component, event);
	},
    recordLink : function(component, event, helper) {
		
        helper.onRecordClick(component, event);
	},
    sourceChange : function(component, event, helper) {
        
        helper.onLookupFieldChange(component, event);
	},
    
    targetChange : function(component, event, helper) {
        
       helper.onLookupFieldChange(component, event);
	},
    
    dateChange : function(component, event, helper) {
        if(event.getParam("value") != null && component.get("v.changedDate") != event.getParam("oldValue")) {
            helper.dateChange(component, event);
        }
        else if(event.getParam("value") == null) helper.onLookupFieldChange(component, event);
    },
    
    handleComponentEvent : function(component, event, helper) {
        
        helper.handleComponentEvent(component, event);
        
    },
    
    proceedToContinue : function(component, event, helper) { 
        component.set("v.changePreventEvent",false);
        helper.onLookupFieldChange(component, event);
        
        helper.proceedToContinue(component, event);
        
    },
    
    closeConfirmationModal : function(component, event, helper) { 
        
        helper.closeConfirmationModal(component, event);
        
    }
    
})