({
    costPerFH : function(component, event){
        this.showSpinner(component);
        if(component.get("v.recordId") != undefined){
            var action = component.get("c.calculateCostPerFH");
            action.setParams({
                "scenarioInp": component.get("v.recordId"),
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('response: ',response.getReturnValue());
                    var dataObj= response.getReturnValue();
                    if(dataObj != undefined){
                        component.set("v.costPerFH",dataObj);
                        var eolaReserveType = '';
                        if(dataObj.reserveType != undefined){
                            eolaReserveType = dataObj.reserveType;
                        }
                        component.set("v.reserveType",eolaReserveType);  
                        
                        var subHeaderLst = [];
                        var subHeader = [];
                        var noOFSV = 0;
                        if(dataObj.noOfSV != undefined){
                            noOFSV = dataObj.noOfSV;
                        }

                        var SVoddorEven = dataObj.noOfSV%2;
                        var ClassNme = '',ClassNme2 ='',ClassNme3='';
                        if(eolaReserveType == 'BOTH'){
                            if(SVoddorEven == 0 ){
                                ClassNme = 'odd';
                                ClassNme3 = 'even';
                                ClassNme2 = 'odd';
                            }else{
                                ClassNme = 'even';
                                ClassNme3 = 'odd';
                                ClassNme2 = 'even';
                            }
                        }else{
                            if(SVoddorEven == 0 ){
                                ClassNme = 'odd';
                                console.log(dataObj.noOfSV+SVoddorEven+'ClassNme'+ClassNme);
                                ClassNme2 = 'even';
                            }else{
                                ClassNme = 'even';
                                console.log('data-->>'+dataObj.noOfSV+'SV__'+SVoddorEven+'ClassNme'+ClassNme);
                                ClassNme2 = 'odd';
                            }
                        }
                        component.set("v.classNmeTostaticColumn3",ClassNme3);
                        component.set("v.classNmeTostaticColumn1",ClassNme);
                        component.set("v.classNmeTostaticColumn2",ClassNme2);
                        
                        for(var i = 1; i<=noOFSV; i++){
                            subHeaderLst.push('PR Date','PR'+i+' Cost','FH at PR'+i,'PR rate ($/FH)');
                            subHeader.push({classNme:"", val:subHeaderLst});
                        }

                        for(let key in dataObj.lstTableData){
                            if(dataObj.lstTableData[key].rowVal != undefined && dataObj.lstTableData[key].rowVal != ''){
                                for(let key2 in dataObj.lstTableData[key].rowVal){
                                    if(dataObj.lstTableData[key].rowVal[key2] != undefined && dataObj.lstTableData[key].rowVal[key2] != ''){
                                        dataObj.lstTableData[key].rowVal[key2].index = this.changeIndexToString(dataObj.lstTableData[key].rowVal[key2].index);
                                    }
                                }
                            }
                        }
                        component.set("v.subHeaderLst",subHeaderLst);
                        component.set("v.tableBodyLst",dataObj.lstTableData);
                    }
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        this.handleErrors(errors);
                    }
                }
                this.hideSpinner(component);
            });
            $A.enqueueAction(action);
        }
    },
    changeIndexToString: function(index){
        var special = ['zeroth','first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth'];
        var deca = ['twent', 'thirt', 'fort', 'fift', 'sixt', 'sevent', 'eight', 'ninet'];

        if (index < 20) return special[index]+' SV';
        if (index%10 === 0) return deca[Math.floor(index/10)-2] + 'ieth SV';
        return deca[Math.floor(index/10)-2] + 'y-' + special[index%10] + ' SV';
    },
    // Show the lightning Spinner
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    // Hide the lightning Spinner
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    //Show the success toast when record is saved successfully
    showToast : function(component, event) {
        var sMsg = 'Saved Successfully';
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: sMsg,
            type : 'success'
        });
        toastEvent.fire();
    },
    //Show the error toast when any error occured
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Cost Per FH Table Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    }); 
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message ); 
                        });  
                    };
                }
            });
        }
    }
})