({
    getDealGroups : function(component,event,helper) {
        var action = component.get('c.getDealGroups');
        action.setParams({
            "DealId": component.get("v.recordId")
        });
        console.log("getDealGroups recordId "+component.get("v.recordId"));
        action.setCallback(this, function(response) {
            var Group = response.getReturnValue();
            if(!$A.util.isEmpty(Group)){
            console.log("getDealGroups  " +JSON.stringify (Group));
            component.set("v.dealsList", Group);
            
        }
        else{
            var errors = response.getError();
            console.log('Return value is empty..error!!!');
            console.error(JSON.stringify(errors));
            this.fireFailureToast(component); 
        }
        });
        $A.enqueueAction(action);
    },

    redirectToDeal: function(component, event, helper){
    var ctarget = event.currentTarget;
    var dealid_str = ctarget.dataset.value;
    window.open('/' + dealid_str);
    },
    fireFailureToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'message' : 'No deal is currently assigned to this group.',
            'type':'info',
            'duration':'1500',
            'mode': 'dismissible'
        }); 
        toastEvent.fire(); 
    }
})
