({
    doInit: function(component, event, helper) {
        var action = component.get("c.fetchDealGroupsDetails");
        action.setParams({"DealId": component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state="+state);
            if (state === "SUCCESS"){
                var responseDealGroupRecord = response.getReturnValue();
                component.set("v.DealGroup", responseDealGroupRecord);
            }
            else if (state === "ERROR") {
                //Error message display logic.
                var errors = response.getError();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "ERROR!",
                    "message": errors[0].message
                });
                toastEvent.fire();
        }
    });
    $A.enqueueAction(action);
    helper.getDealGroups(component,event,helper);
      
    },
    onDealClick : function(component, event, helper)
    {
        helper.redirectToDeal(component, event, helper);
    },
    saveComment : function(component, event, helper){
        var action = component.get("c.updateComment");
        action.setParams({"comment": component.get("v.DealGroup")});
        action.setCallback(this, function(response){
            var state = response.getState();
           var res= response.getReturnValue();
            console.log("state="+state);
            if (state === "SUCCESS"){
                //Success message display logic.
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "Success",
                    "message": "Deal Group record has been updated successfully."
                });
                toastEvent.fire();
            }
                else if (state === "ERROR") {
                    //Error message display logic.
                    var errors = response.getError();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "ERROR!",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                }
        
    });
    $A.enqueueAction(action);
},
    setOutput : function(component, event, helper) {
    	var cmpMsg = component.find("msg");
    	$A.util.removeClass(cmpMsg, 'hide');
        var comments = component.find("comments").get("v.value");
        var oTextarea = component.find("oTextarea");
        oTextarea.set("v.value", comments);
    }
})
