({
    //lease related data
    getLeaseData : function(component) {
        console.log('getLeaseData RentManagerPBHWizard');
        var action = component.get("c.getLeaseInfo");
        action.setParams({
            recordId : component.get("v.leaseId")
        });
        action.setCallback(this, function(response) {
            var leaseInfo = response.getReturnValue();
            if(leaseInfo != null){
            console.log("getLeaseData response: "+JSON.stringify(leaseInfo));
            component.set("v.leaseInfo",leaseInfo);
            component.set("v.currentRent",leaseInfo.CurrentRent);
            console.log("getLeaseData leaseInfo",leaseInfo);
            console.log("getLeaseData leaseInfo",leaseInfo.CurrentRent);
            
            if(!$A.util.isEmpty(leaseInfo) && !$A.util.isUndefined(leaseInfo)) {
                 if(component.get("v.isExtendedRent")){
                    //Case 1 : Rent Schedules present 
                    if(!$A.util.isEmpty(leaseInfo.RentScheduleLastDate) && !$A.util.isUndefined(leaseInfo.RentScheduleLastDate)){
                      console.log('getLeaseData if1 Extend: '+leaseInfo.RentScheduleLastDate);
                    this.getLeaseStartDate(component,leaseInfo.RentScheduleLastDate);
                  }
                    //Case 2 : Rent Schedules not present
                    else if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                    console.log('getLeaseData else1 Extend: '+leaseInfo.LeaseEndDate);
                        this.getLeaseStartDate(component,leaseInfo.LeaseEndDate);
                   }
                     component.set("v.rentEndValue",null);
                }else{
                if(!$A.util.isEmpty(leaseInfo.RentScheduleLastDate) && !$A.util.isUndefined(leaseInfo.RentScheduleLastDate)){
                    console.log('if1');
                    var leaseStartDt = new Date(leaseInfo.RentScheduleLastDate);
                    component.set("v.rentStartValue",leaseInfo.RentScheduleLastDate);
                }
                else if(!$A.util.isEmpty(leaseInfo.LeaseStartDate) && !$A.util.isUndefined(leaseInfo.LeaseStartDate)){
                    console.log('else1');
                    var leaseStartDt = new Date(leaseInfo.LeaseStartDate);
                    component.set("v.rentStartValue",leaseInfo.LeaseStartDate);
                }
                if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                    console.log('if2');
                    var leaseEndDt = new Date(leaseInfo.LeaseEndDate);
                    component.set("v.rentEndValue",leaseInfo.LeaseEndDate);
                }
                if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                    if(!$A.util.isEmpty(leaseInfo.RentScheduleLastDate) && !$A.util.isUndefined(leaseInfo.RentScheduleLastDate)){
                    var leaseEndDt = new Date(leaseInfo.LeaseEndDate);
                    var ScheduleEndDt = new Date(leaseInfo.RentScheduleLastDate); 
                        console.log("leaseEndDt :"+leaseEndDt+ "ScheduleEndDt :"+ScheduleEndDt);
                        if(leaseEndDt <= ScheduleEndDt){
                            console.log('if3');
                    component.set("v.rentEndValue","");
                    component.set("v.rentStartValue","");        
                        }
                }
                }      
                }
            }
            }
        });
        $A.enqueueAction(action);                
    },
    
    //calculate Lease Start Date
    getLeaseStartDate: function(component,startDate) {
        console.log("getLeaseStartDate");
      var action = component.get("c.getLeaseStartDate");
        action.setParams({
            startDate : startDate
        });
        action.setCallback(this, function(response) {
             var state = response.getState();
            console.log("getLeaseStartDate response-state: "+state);
            if (state === "SUCCESS") {
                var leaseStartDate = response.getReturnValue();
                console.log("getLeaseStartDate response: "+leaseStartDate);
                if(leaseStartDate != null){
                    component.set("v.rentStartValue",leaseStartDate);
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getLeaseStartDate errors "+JSON.stringify(errors));
                if (errors) {
                    this.handleErrors(errors);   
                } else {
                    console.log("getLeaseStartDate Unknown error");
                    this.showErrorMsg(component, "Error in fetching the lease start date");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    
    checkLeaseApprovalStatus: function(component) {

        console.log('checkLeaseApprovalStatus');

        var self = this;

        var action = component.get("c.getLeaseApprovalStatus");

        action.setParams({

            leaseId : component.get("v.leaseId")

        });

        action.setCallback(this, function(response) {

            var state = response.getState();

            console.log("checkLeaseApprovalStatus response-state: "+state);

            if (state === "SUCCESS") {

                var result = response.getReturnValue();

                console.log("checkLeaseApprovalStatus"+result);

                if(result) {

                    component.set("v.isLeaseApproved", result);

                    this.generateData(component);

                    console.log("checkLeaseApprovalStatus"+component.get("v.isLeaseApproved"));

                }

                else{

                    component.set("v.isLeaseApproved", result);

                    component.set("v.isLoading", false);

                    this.showErrorMsg(component, "Rent Schedules can only be created after the Lease is approved via the Approval Process");

                }

            }

            else if (state === "ERROR") {    

                var errors = response.getError();

                console.log("checkLeaseApprovalStatus errors "+JSON.stringify(errors));

                if (errors) {

                    this.handleErrors(errors);   

                } else {

                    console.log("checkLeaseApprovalStatus Unknown error");

                    this.showErrorMsg(component, "Error in fetching record");

                }

            }

        });

        $A.enqueueAction(action); 

    },
    //fetch escalation month picklist values
    getMonthPickList: function(component) {
        console.log('getMonthPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Escalation_Month__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getMonthPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                    console.log('getMonthPickList  result:'+allValues);
                    component.set('v.rentEscalationMonthPickListValue', allValues);
                    opts.push({class: "optionClass",label: "--- None ---",value: ""});
                    for(var i=0;i< response.getReturnValue().length; i++) {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    }
                }
                var escalationMonthId = component.find('escalationMonthId');
                if(escalationMonthId != undefined){
                    escalationMonthId.set("v.options", opts); 
                }
                
            }
            var fieldV = component.get("v.rentEscalationMonthValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousEscPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch rent period end day picklist values
    getRentPeriodDayPickList: function(component) {
        console.log('getRentPeriodDayPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Period_End_Day__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRentPeriodDayPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getRentPeriodDayPickList  result:'+allValues);
                component.set('v.periodEndDayPickListValue', allValues);
                for(var i=0;i< response.getReturnValue().length; i++) {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                }
                var rentPeriodEndDayId = component.find('rentPeriodEndDayId');
                if(rentPeriodEndDayId != undefined){
                    rentPeriodEndDayId.set("v.options", opts); 
                }
            }
            var fieldV = component.get("v.periodEndDayValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousRentDueDayPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch invoice generation day picklist values
    getInvoiceDayPickList: function(component) {
        console.log('getInvoiceDayPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Invoice_Generation_Day__c'
        });
        var opts = [];
        console.log("before res");
        action.setCallback(this, function(response) {
            console.log("after res");
            console.log('getInvoiceDayPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getInvoiceDayPickList  result:'+allValues);
                component.set('v.invoiceDayPickListValue', allValues);
                for(var i=0;i< response.getReturnValue().length; i++) {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                }
                var invoiceDayId = component.find('invoiceDayId');
                if(invoiceDayId != undefined){
                    invoiceDayId.set("v.options", opts); 
                }
            }
            var fieldV = component.get("v.invoiceDayValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousInvoiceDayPickList(component);
        });
        $A.enqueueAction(action);
    },
 
    //fetch extended rent type picklist values
    getExtendedRentTypePickList: function(component) {
        console.log('getExtendedRentTypePickList');
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Extended_Rent_Type__c'
        });
        var opts = [];
        var self = this;
        action.setCallback(this, function(response) {
            console.log('getExtendedRentTypePickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getExtendedRentTypePickList result:'+allValues);
                component.set("v.extendRentTypePickListValue",allValues);
               for(var i=0;i< response.getReturnValue().length; i++) {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                }
                var extendedRentTypeId = component.find('extendedRentTypeId');
                if(extendedRentTypeId != undefined){
                    extendedRentTypeId.set("v.options", opts); 
                }
            }
            var fieldV = component.get("v.extendedRentType");
            console.log('getTypePickList fieldV :',fieldV);
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
            	self.showPreviousExtendedRentTypePickList(component);
        });
        $A.enqueueAction(action);
    },
     
    //fetch rent due type picklist values
    getRentDueTypePickList: function(component) {
        console.log('getRentDueTypePickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Due_Type__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRentDueTypePickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getRentDueTypePickList  result:'+allValues);
                component.set('v.rentDueTypePickListValue', allValues);
                for(var i=0;i< response.getReturnValue().length; i++) {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                }
                var rentDueTypeId = component.find('rentDueTypeId');
                if(rentDueTypeId != undefined){
                    rentDueTypeId.set("v.options", opts); 
                }
            }
            var fieldV = component.get("v.rentDueTypeValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousRentDueTypePickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch rent due day picklist values for Fixed due type
    getRentDuedayFixedVal: function(component) {
        console.log('getRentDuedayFixedVal');
        var opts = [];
        var duetype = component.find("rentDueTypeId").get("v.value");
        var dueday = component.get('v.rentDueDayValue');
        var isEdit = component.get("v.isEdit");
        console.log("duetype",duetype);    
        
        if(duetype == 'Fixed' || duetype == undefined){
            if(isEdit){
               var dueday = component.get('v.rentDueDayValue'); 
                for(var i=1;i<=31; i++){
                    if(i== dueday){
                        opts.push({"class": "optionClass", label: i, value:i,selected: true}); 
                    }
                    else{
                        opts.push({"class": "optionClass", label: i, value:i}); 
                    }
                }
                component.set("v.isFixedDueType",true);
            }
            else{
                for(var i=1;i<=31; i++) {
                    opts.push({"class": "optionClass", label: i, value:i});
                } 
            }
                var rentDueDayId1 = component.find('rentDueDayId1');
                if(rentDueDayId1 != undefined){
                    rentDueDayId1.set("v.options", opts); 
                } 
            component.set("v.isFixedDueType",true);
            
        }else{
                component.set("v.isFixedDueType",false); 
            }
    },
    
    //fetch rent due day setting picklist values
    getRentDueSettingPickList: function(component) {
        console.log('getRentDueSettingPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Rent_Due_Day_Setting__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getRentDueSettingPickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getRentDueSettingPickList  result:'+allValues);
                component.set('v.rentDueSettingPickListValue', allValues);
                for(var i=0;i< response.getReturnValue().length; i++) {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                }
                var isValidSetting = component.get("v.isValidSetting");
                if(isValidSetting){ 
                    console.log('if');
                    var rentDueSetting = component.find('settingId3'); 
                    if(rentDueSetting != undefined){
                        rentDueSetting.set("v.options", opts); 
                    }
                    
                }
             }
            var fieldV = component.get("v.rentDueSettingValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousRentDueSettingPickList(component);
        });
        $A.enqueueAction(action);
    },
        
    //fetch pbh rate basis picklist values
    getPBHrateBasisPickList: function(component) {
        console.log('getPBHrateBasisPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'PBH_Rent_Rate__c',
            "fld": 'Rate_Basis__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getPBHrateBasisPickList state: ',response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getPBHrateBasisPickList  result:'+allValues);
                component.set('v.rateBasisPicklist', allValues);
                for(var i=0;i< allValues.length; i++) {
                    opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                }
                component.set("v.rateBasisOptions", opts);
                }
            }
            var fieldV = component.get("v.pbhRateBasisValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                console.log('hyh');
            //    self.showPreviousRentDueTypePickList(component);
        });
        $A.enqueueAction(action);
    },
    
    //fetch rent due day correction picklist values
    getDueCorrectionPickList: function(component) {
        console.log('getDueCorrectionPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Due_Day_Correction__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getDueCorrectionPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getDueCorrectionPickList  result:'+allValues);
                component.set('v.dueDateCorrectionPickListValue', allValues);
                //opts.push({class: "optionClass",label: "--- None ---",value: ""});
                for(var i=0;i< response.getReturnValue().length; i++) {
                    if(response.getReturnValue()[i] == "None")
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: ""});
                    else 
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                }
                var dueDayCorrectionId = component.find('dueDayCorrectionId');
                if(dueDayCorrectionId != undefined){
                    dueDayCorrectionId.set("v.options", opts); 
                }
            }
            var fieldV = component.get("v.dueDateCorrectionValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousDueCorrectionPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    getPbhVal: function(component){
        console.log('getPbhVal');
       var multiRentData = component.get("v.multiRent");
        console.log('getPbhVal multiRentData Id',multiRentData.RentId);
        console.log('getPbhVal lease Id',component.get("v.leaseId"));
       var action = component.get("c.getMultiRentScheduleRecord");
        
        action.setParams({
            multiRentScheduleId : multiRentData.RentId,
            leaseId: component.get("v.leaseId")
            
        }); 
        action.setCallback(this, function(response) {
            console.log('getPbhVal',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getPbhVal allValues',JSON.stringify(allValues));
                component.set("v.minTotalValue",allValues.TotalMinAmount);
                component.set("v.maxTotalValue",allValues.TotalMaxAmount);
                component.set("v.minPBHValue",allValues.PbhminAmount);
                component.set("v.maxPBHValue",allValues.PbhmaxAmount);
                }
            }
            else if (response.getState() === "ERROR") {
                var errors = response.getError();
                console.log("getPBHRentData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in fetching PBH record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    
    //fetch pro-rata number of days picklist values
    getProrataPickList: function(component) {
        console.log('getProrataPickList');
        var self = this;
        var action = component.get("c.getPickListValues");
        action.setParams({
            "objectName": 'Stepped_Rent__c',
            "fld": 'Pro_rata_Number_Of_Days__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log('getProrataPickList state: ',response.getState() );
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues != null){
                console.log('getProrataPickList  result:'+allValues);
                component.set('v.prorataPickListValue', allValues);
                //opts.push({class: "optionClass",label: "--- None ---",value: ""});
                for(var i=0;i< response.getReturnValue().length; i++) {
                    if(response.getReturnValue()[i] == "None")
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: ""});
                    else 
                        opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                }
                var prorataId = component.find('prorataId');
                if(prorataId != undefined){
                    prorataId.set("v.options", opts); 
                }

            }
            var fieldV = component.get("v.prorataValue");
            if(!$A.util.isEmpty(fieldV) && !$A.util.isUndefined(fieldV)) 
                self.showPreviousProrataPickList(component);
        });
        $A.enqueueAction(action);
    },
    
    showPreviousEscPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.rentEscalationMonthValue");
        var valueList = component.get("v.rentEscalationMonthPickListValue");
        opts.push({class: "optionClass",label: "--- None ---",value: ""});
        if(valueList != null){
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var escalationMonthId = component.find('escalationMonthId');
                if(escalationMonthId != undefined){
                    escalationMonthId.set("v.options", opts); 
                }
        
        console.log('onPreviousClick fieldV: '+fieldV+" "+valueList);
    },
    
    showPreviousRentDueDayPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.periodEndDayValue");
        var valueList = component.get("v.periodEndDayPickListValue");
        if(valueList != null){
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var rentPeriodEndDayId = component.find('rentPeriodEndDayId');
                if(rentPeriodEndDayId != undefined){
                    rentPeriodEndDayId.set("v.options", opts); 
                }
    },
    
    showPreviousRentDueSettingPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.rentDueSettingValue");
        var valueList = component.get("v.rentDueSettingPickListValue");
        if(valueList != null){
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var isValidSetting = component.get("v.isValidSetting");
                if(isValidSetting){ 
                    var rentDueSetting = component.find('settingId3'); 
                    if(rentDueSetting != undefined){
                        rentDueSetting.set("v.options", opts); 
                    }
                    
                }
                else{
                    var rentDueSetting = component.find('rentDueSettingId'); 
                    if(rentDueSetting != undefined){
                        rentDueSetting.set("v.options", opts); 
                    }
                }
                
        
    },
    
    showPreviousInvoiceDayPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.invoiceDayValue");
        var valueList = component.get("v.invoiceDayPickListValue");
        if(valueList != null){
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var invoiceDayId = component.find('invoiceDayId');
                if(invoiceDayId != undefined){
                    invoiceDayId.set("v.options", opts); 
                }
         
    },
    
    showPreviousExtendedRentTypePickList: function(component){
     var opts = [];
        var fieldV = component.get("v.extendedRentType");
        var valueList = component.get("v.extendRentTypePickListValue");
        if(valueList != null){
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var extendedRentTypeId = component.find('extendedRentTypeId');
                if(extendedRentTypeId != undefined){
                    extendedRentTypeId.set("v.options", opts); 
                }
        
        console.log('onPreviousClick fieldV: '+fieldV+" "+valueList);
       
    },
    
    showPreviousRentDueTypePickList: function(component){
        var opts = [];
        var fieldV = component.get("v.rentDueTypeValue");
        console.log('showPreviousRentDueTypePickList fieldV ',fieldV);
        if(fieldV == 'Fixed'){
            component.set("v.isValidSetting",true);   
            component.set("v.isFixedDueType",true); 
        }
        else{
            
            component.set("v.isValidSetting",false);   
            component.set("v.isFixedDueType",false); 
        
        }
        var valueList = component.get("v.rentDueTypePickListValue");
        if(valueList != null){
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var rentDueTypeId = component.find('rentDueTypeId');
                if(rentDueTypeId != undefined){
                    rentDueTypeId.set("v.options", opts); 
                } 
        
    },
    
    showPreviousDueCorrectionPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.dueDateCorrectionValue");
        var valueList = component.get("v.dueDateCorrectionPickListValue");
        if(valueList != null){
        // opts.push({class: "optionClass",label: "--- None ---",value: ""});
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else if(valueList[i] == "None")
                opts.push({"class": "optionClass", label: valueList[i], value: ""});
                else
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var dueDayCorrectionId = component.find('dueDayCorrectionId');
                if(dueDayCorrectionId != undefined){
                    dueDayCorrectionId.set("v.options", opts); 
                }
        
        
    },
    
    showPreviousProrataPickList: function(component){
        var opts = [];
        var fieldV = component.get("v.prorataValue");
        var valueList = component.get("v.prorataPickListValue");
        if(valueList != null){
        // opts.push({class: "optionClass",label: "--- None ---",value: ""});
        for(var i=0;i< valueList.length; i++) {
            if(valueList[i] == fieldV) 
                opts.push({"class": "optionClass", label: valueList[i], value: valueList[i], selected: true});
            else if(valueList[i] == "None")
                opts.push({"class": "optionClass", label: valueList[i], value: ""});
                else 
                    opts.push({"class": "optionClass", label: valueList[i], value: valueList[i]});
        }
        }
        var prorataId = component.find('prorataId');
                if(prorataId != undefined){
                    prorataId.set("v.options", opts); 
                }
        
        
    },
    
    //generate PBH Rent MultiRent Schedule
    generateData: function(component) {
        console.log('generateData');
        var self = this;
        
        var isEdit = component.get("v.isEdit");
        console.log("generateData isEdit: "+ isEdit);
        
        var stMp = component.get("v.assemblyList");
        var stMpStr = null;
        console.log("generateData stMp "+stMp);
        
        if(!$A.util.isEmpty(stMp) && !$A.util.isUndefined(stMp)) {
            console.log("generateData json stMp "+JSON.stringify(stMp));
            stMpStr = JSON.stringify(stMp);
        }
        var multiRentData = component.get("v.multiRent");
        var multiRtId = null;
        
        if(isEdit && !$A.util.isEmpty(multiRentData) && !$A.util.isUndefined(multiRentData))
            multiRtId = multiRentData.RentId;
        
        var params = this.getParameters(component,stMpStr,multiRtId);
        console.log("params :"+JSON.stringify(params));
        
        var action = component.get("c.generatePBHRentSchedule");
        action.setParams({jsonString : JSON.stringify(params) });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("generateData response-state: "+state);
            if (state === "SUCCESS") {
                var rentList = response.getReturnValue();
                if(rentList != null){
                console.log("generateData response: "+JSON.stringify(rentList));
                component.set("v.multiRent",rentList); 
                self.showPreviewScreen(component);
                //data saved Successfully
                //Fire the refresh view event to update Account detail view
                //$A.get('e.force:refreshView').fire();
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("generateData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in generating multirent record");
                }
                component.set("v.showOutput", false);
                component.set("v.showRentAmount", false);
                component.set("v.showRentPeriod", false);
                component.set("v.showRentDue", false);
                component.set("v.showPreview", true);
                
                component.set("v.isLoading", false);
            }
        });
        $A.enqueueAction(action);                
    },
    
    //show all multirent data parameters
    showPreviewScreen: function(component) {
        console.log('showPreviewScreen');
        var self = this;
        component.set("v.showRentAmount", false);
        component.set("v.showRentPeriod", false);
        component.set("v.showRentDue", false);
        component.set("v.showPreview", false);
        
        component.set("v.showOutput", true);
        
        var multiRentData = component.get("v.multiRent");
        if(multiRentData != null){
        console.log("showPreviewScreen multiRentData "+JSON.stringify(multiRentData));
        component.set("v.baseRentValue",multiRentData.BaseRent);
        if(multiRentData.OffWingRent > 0 )
            component.set("v.offWingRentValue",multiRentData.OffWingRent);
        else
            component.set("v.offWingRentValue","");
        if(multiRentData.Escalation > 0)
            component.set("v.rentEscalationValue", multiRentData.Escalation);
        else
            component.set("v.rentEscalationValue", "");
        
        component.set("v.rentEscalationMonthValue", multiRentData.EscalationMonth);
        var stDate = multiRentData.StartDate;
        component.set("v.rentStartValue",stDate);
        component.set("v.rentEndValue", multiRentData.EndDate);
        component.set("v.differentStartDate", multiRentData.DifferentStartDate);
        component.set("v.secondRentStartValue", multiRentData.SecondStartDate);
        component.set("v.invoiceDayValue", multiRentData.InvoiceGenerationDay);
        component.set("v.rentDueTypeValue", multiRentData.RentDueType);
        component.set("v.rentDueSettingValue", multiRentData.RentDueSetting);
        component.set("v.rentDueDayValue", multiRentData.RentDueDay);
        component.set("v.prorataValue", multiRentData.ProrataNumberOfDays);
        component.set("v.dueDateCorrectionValue", multiRentData.DueDayCorrection);
        component.set("v.isExtendedRent", multiRentData.isExtendedRentPeriod);
        component.set("v.extendedRentType", multiRentData.ExtendedRentType);
        if(multiRentData.Status == 'InActive'){
            component.set("v.isActive",false);}
        else{
            component.set("v.isActive",true);}
        }        
        console.log('showPreviewScreen');
        
        var action = component.get("c.getSteppedMultiData");
        action.setParams({
            multiRentId : component.get("v.multiRent").RentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS") {
                var stRentList = response.getReturnValue();
                console.log("showPreviewScreen response:stRentList "+JSON.stringify(stRentList));
                }
            else{
                var errors = response.getError();
                console.log("showPreviewScreen errors "+JSON.stringify(errors));
                component.set("v.isLoading", false);
            }
            self.getRentScheduleData(component, multiRentData);
        });
        $A.enqueueAction(action);  
    },
    
    getPBHRentData: function(component) {
        console.log("getPBHRentData");
        var action = component.get("c.getPBHRentData");
        console.log('getPBHRentData multirentId :',component.get("v.multiRent").RentId);
        action.setParams({
            multiRentId : component.get("v.multiRent").RentId,
            recordId : component.get("v.leaseId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getPBHRentData state: "+state);
            if (state === "SUCCESS") {
                var pbhrentList = response.getReturnValue();
                if(pbhrentList != null){
                console.log("getPBHRentData response: "+JSON.stringify(pbhrentList));
                component.set("v.assemblyList",pbhrentList);
                component.set("v.pbhDataList",pbhrentList);
                } 
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getPBHRentData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in fetching pbh rent record");
                }
            }
            component.set("v.isLoading", false);
            console.log("getPBHRentData loading "+component.get("v.isLoading"));
        });
        $A.enqueueAction(action); 
    },
    
    //fetch multirent data parameters
    getRentScheduleData : function(component, multiRentData) {
        var action = component.get("c.getOutputData");
        action.setParams({
            steppedRentId : multiRentData.RentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getRentScheduleData state: "+state);
            if (state === "SUCCESS") {
                var rentList = response.getReturnValue();
                if(rentList != null){
                console.log("getRentScheduleData response: "+JSON.stringify(rentList));
                component.set("v.rentGeneratedList",rentList);
                } 
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getRentScheduleData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in fetching rent schedule record");
                }
            }
            component.set("v.isLoading", false);
            console.log("getRentScheduleData loading "+component.get("v.isLoading"));
        });
        $A.enqueueAction(action);  
    },
    
    //fetch list of Assemblies for an Asset
    fetchAssemblyList : function(component){
        console.log('fetchAssemblyList');
        console.log('LeaseId :',component.get("v.leaseId"));
        var action = component.get("c.getAssemblyList");
        console.log('call');
        action.setParams({
            recordId : component.get("v.leaseId")
        });
        
         action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("fetchAssemblyList state: "+state);
            if (state === "SUCCESS") {
                var assemblyList = response.getReturnValue();
                if(assemblyList != null){
                console.log("fetchAssemblyList response: "+JSON.stringify(assemblyList));
                component.set("v.assemblyList",assemblyList); 
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("fetchAssemblyList errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in fetching Assembly data record");
                }
            }
            component.set("v.isLoading", false);
            console.log("fetchAssemblyList loading "+component.get("v.isLoading"));
        });
        
        $A.enqueueAction(action);  
    },
    
    //delete multirent schedule record
    deleteMultiRentSch : function(component) {
        var multiRentData = component.get("v.multiRent");
        var action = component.get("c.deleteMultiRent");
        var self= this;
        action.setParams({
            steppedRentId : multiRentData.RentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("deleteMultiRentSch response-state: "+state);
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if(res != null){
                console.log("deleteMultiRentSch response: "+res);
                if($A.util.isEmpty(res) || $A.util.isUndefined(res) || res == null)
                    self.showDeleteMsg(component, 'Success');
                else
                    self.showErrorMsg(component, res);
                self.getRentScheduleData(component, multiRentData);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("deleteMultiRentSch errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in deleting multirent record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    
    //used to display the warning message
    showWarningMsg : function(component, title, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'warning',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    //used to display the error message
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'10000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
    //used to display message when user deletes multirent record
    showDeleteMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Successfully Deleted!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    //change multiRent status
    changeMultiRentStatus : function(component) {
        var st = component.get("v.isActive");
        var multiRentData = component.get("v.multiRent");
        console.log("changeMultiRentStatus isActive "+st + "multiRentData "+multiRentData);
        if(multiRentData===null)return;
        var action = component.get("c.updateMultiRentStatus");
        action.setParams({
            multiRentId : multiRentData.RentId,
            status : st
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("changeMultiRentStatus response-state: "+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("changeMultiRentStatus response: "+result);
                if(st == true)
                    multiRentData.Status = "Active";
                else
                    multiRentData.Status = "Inactive";
                component.set("v.multiRent", multiRentData);
                console.log("changeMultiRentStatus after update: "+JSON.stringify(multiRentData));
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("changeMultiRentStatus errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in changing the status of multirent record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);    
    },
    
    getSecondRentStartDate : function(component) {
                var edit = component.get("v.isEdit");
                console.log("getSecondRentStartDate Edit : "+edit); 
                
        if(edit){
            console.log("getSecondRentStartDate Edit if: "+edit); 
                var differentStartFlag = component.get("v.differentStartDate");
                console.log("getSecondRentStartDate differentStartFlag :"+differentStartFlag);
                var resultcmp1 = component.find("differentStart");
                resultcmp1.set("v.value",differentStartFlag);
                console.log("getSecondRentStartDate recordId :"+component.find("differentStart").get("v.value")); 
        
                var secondStartDate = component.get("v.secondRentStartValue");
                console.log("getSecondRentStartDate secondRentStartValue :"+secondStartDate);
                
            if(component.get("v.differentStartDate")){
                var resultcmp2 = component.find("secondRentStartId2");
                resultcmp2.set("v.value",secondStartDate);
                console.log("getSecondRentStartDate recordId :"+component.find("secondRentStartId2").get("v.value"));
            }
            else{
               var resultcmp2 = component.find("secondRentDateValue");
                resultcmp2.set("v.value",secondStartDate);
                console.log("getSecondRentStartDate recordId :"+component.find("secondRentDateValue").get("v.value")); 
            }
            }
        else{
            console.log("getSecondRentStartDate Edit else: "+edit); 
            
            component.set("v.isSecondRentStartErrorMsg", false);
            component.set("v.secondRentStartErrorMsg" ,"");
                
            component.set("v.differentStartDate",false);
            component.set("v.secondRentStartValue","");
           
            var differentStartFlag = component.get("v.differentStartDate");
                console.log("getSecondRentStartDate differentStartFlag :"+differentStartFlag);
            var secondStartDate = component.get("v.secondRentStartValue");
                console.log("getSecondRentStartDate secondRentStartValue :"+secondStartDate);
                
        }
    },
    
    //Json String formation for Rent Schedule generation
    getParameters : function(component,stMpStr,multiRtId){ 
        
        return{
        'nameStr': component.get("v.nameValue"),
        'rentTypeStr': component.get("v.typeValue"),
        'periodStr': component.get("v.periodValue"),
        'commentStr': component.get("v.commentValue"),
        'isRentExtendedStr':component.get("v.isExtendedRent"),
        'extendedRentTypeStr' : component.get("v.extendedRentType"),
        'includeCommentStr': component.get("v.includeComment"),
        'rentStartStr': component.get("v.rentStartValue"),
        'rentEndStr': component.get("v.rentEndValue"),
        'invoiceDayStr': component.get("v.invoiceDayValue"),
        'baseRentStr': component.get("v.baseRentValue"),
        'offWingRentStr': component.get("v.offWingRentValue"),
        'rentEscalationStr': component.get("v.rentEscalationValue"),
        'rentEscalationMonthStr': component.get("v.rentEscalationMonthValue"),
        'minTotalStr': component.get("v.minTotalValue"),
        'maxTotalStr': component.get("v.maxTotalValue"),
        'minPBHStr': component.get("v.minPBHValue"),
        'maxPBHStr': component.get("v.maxPBHValue"),
        'rentDueTypeStr': component.get("v.rentDueTypeValue"),
        'rentDueSettingStr': component.get("v.rentDueSettingValue"),
        'rentDueDayStr': component.get("v.rentDueDayValue"),
        'prorataStr': component.get("v.prorataValue"),
        'dueDateCorrectionStr': component.get("v.dueDateCorrectionValue"), 
        'assemblyMapListStr': stMpStr,
        'leaseIdStr': component.get('v.leaseId'),
        'multiRentIdStr': multiRtId,
        'isFromLE' : component.get("v.isFromLE")};
    },  
})