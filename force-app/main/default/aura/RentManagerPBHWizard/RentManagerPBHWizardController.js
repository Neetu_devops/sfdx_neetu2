({
    //on component load if user selects Fixed Rent : checks if multiRent is Active or Inactive
    doInit : function(component, event, helper) {
        console.log("doInit: ");
        var multiRent = component.get("v.multiRent");
        if(!$A.util.isEmpty(multiRent) && !$A.util.isUndefined(multiRent)) {
            var status = multiRent.Status;
            if(status.toUpperCase() == "Active".toUpperCase())
                component.set("v.isActive", true);
            else if(status.toUpperCase() == "InActive".toUpperCase())
                component.set("v.isActive", false);
        }
        
    },
    
    //on load: calls previewScreen and for edit, it fetches the all the picklist values for Page1
    reload :  function(component, event, helper){
        console.log("reload:showComponent "+ component.get("v.showComponent"));
        if(component.get("v.showComponent") == true) {
            if(component.get("v.isViewMode") == true) {
                component.set("v.isLoading", true);
                helper.showPreviewScreen(component);
            }
            else{
                component.set("v.showOutput", false);
                component.set("v.showRentPeriod", true);
                
                if(component.get("v.isExtendedRent")){
                var extendedRentType = component.get('v.extendRentTypePickListValue');
                console.log('reload extendedRentType: '+extendedRentType);
                if($A.util.isEmpty(extendedRentType) || $A.util.isUndefined(extendedRentType)) 
                    helper.getExtendedRentTypePickList(component);
                else
                    helper.showPreviousExtendedRentTypePickList(component);
                }
                else{
                    component.set("v.extendedRentType",null);
                }
                
                var invoiceVal = component.get('v.invoiceDayPickListValue');
                console.log('reload invoiceVal: '+invoiceVal);
                if($A.util.isEmpty(invoiceVal) || $A.util.isUndefined(invoiceVal)) 
                    helper.getInvoiceDayPickList(component);
                else
                    helper.showPreviousInvoiceDayPickList(component);
                if(component.get("v.isEdit") == false)
                    helper.getLeaseData(component);
                
                   
            }
        }
        
        
    },
    
    //on refresh: resets all the component parameters
    refreshWizard :  function(component, event, helper){
        console.log("refreshWizard:refreshData "+component.get("v.isRefresh"));
        if(component.get("v.isRefresh") == true) {
            component.set("v.baseRentValue", "");
            component.set("v.offWingRentValue","");
            component.set("v.startDateValue", "");
            component.set("v.incDecPer","");
            component.set("v.amtIncDec","");
            component.set("v.rentAmountValue", ""); 
            component.set("v.rentEscalationValue","");
            component.set("v.rentEscalationMonthValue", "");
            component.set("v.rentStartValue","");
            component.set("v.rentEndValue", "");
            component.set("v.invoiceDayValue", "");
            component.set("v.differentStartDate",false);
            component.set("v.secondRentStartValue","");
            component.set("v.periodEndDayValue", "");
            component.set("v.rentDueTypeValue", "");
            component.set("v.rentDueSettingValue", "");
            component.set("v.rentDueDayValue", "");
            component.set("v.prorataValue", "");
            component.set("v.dueDateCorrectionValue", "");
            component.set("v.steppedObjectArr", []);
            component.set("v.inputDateVal" , "");
            component.set("v.inputNumberVal" , "");
            component.set("v.inputCheckVal" , "");
            component.set("v.inputCurrencyVal" , "");
            component.set("v.steppedTotalCount" , 0);
            component.set("v.rowIndex" , 0);
            component.set("v.minTotalValue" , "");
            component.set("v.maxTotalValue" , "");
            component.set("v.minPBHValue" , "");
            component.set("v.maxPBHValue" , "");
            var body = component.get("v.body");
            component.set("v.body",[]);
            console.log("refreshWizard body: "+body);
            helper.getLeaseData(component);
        } 
    },
    
    //on Cancel button click
    onCancelClick: function(component, event, helper){
        console.log("onCancelClick ");
        component.set("v.showRentAmount", false);
        component.set("v.showRentPeriod", false);
        component.set("v.showRentDue", false);
        component.set("v.showPreview", false);
        component.set("v.showOutput", false);
        component.set("v.isEdit", false);
        
        var leaseId = component.get("v.leaseId");
        console.log("onCancelClick PBHWizard "+leaseId);
        var onCancelSchedule = component.getEvent("onWizardCancelButton");
        console.log("onCancelSchedule PBHWizard "+onCancelSchedule);
        onCancelSchedule.setParams({
            "leaseId" : leaseId,
            "isEdit" : component.get("v.isEdit")
        });
        helper.changeMultiRentStatus(component);
        
        onCancelSchedule.fire();
        
    },
    
    //on Previous button click for all pages
    onPreviousClick: function(component, event, helper){
        console.log('onPreviousClick');
        var showRentAmount = component.get("v.showRentAmount");
        var showRentPeriod = component.get("v.showRentPeriod");
        var showRentDue = component.get("v.showRentDue");
        var showPreview = component.get("v.showPreview");
        var showPbhRent = component.get("v.showPbhRent");
        //page 2 => 1
        if(showRentPeriod == true){
            console.log('showRentPeriod',showRentPeriod);
            component.set("v.showComponent", false);
            component.set("v.showRentAmount", false);
            component.set("v.showRentPeriod", false);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            component.set("v.showPbhRent",false);
            
            //need to fire event 
            var leaseId = component.get("v.leaseId");
            console.log("onPreviousClick "+leaseId);
            var onTypeWizardSchedule = component.getEvent("onWizardCancelButton");
            onTypeWizardSchedule.setParams({
                "leaseId" : leaseId,
                "showTypeWizard" : true,
                "isEdit" : component.get("v.isEdit")
            });
            onTypeWizardSchedule.fire();
        }
        //page 3 => 2
        else if(showRentAmount == true) {
             console.log('showRentAmount',showRentAmount);
            component.set("v.showRentPeriod", true);
            component.set("v.showRentAmount", false);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            component.set("v.showPbhRent", false);
            
            helper.showPreviousEscPickList(component);
        }
        //page 4 => 3
        else if(showPbhRent == true) {
             console.log('showPbhRent',showPbhRent);
            component.set("v.showRentPeriod", false);
            component.set("v.showRentAmount", true);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            component.set("v.showPbhRent", false);
            component.set("v.isSameSession", true);
           // helper.showPreviousRentDueDayPickList(component);
           // helper.showPreviousInvoiceDayPickList(component);
        } 
        //page 5 => 4
        else if(showRentDue == true) {
                console.log('showRentDue',showRentDue);
                component.set("v.showRentPeriod", false);
                component.set("v.showRentAmount", false);
                component.set("v.showRentDue", false);
                component.set("v.showPreview", false);
                component.set("v.showPbhRent", true);
            
            helper.showPreviousRentDueSettingPickList(component);
            helper.showPreviousRentDueDayPickList(component);
            helper.showPreviousInvoiceDayPickList(component);
                
            }
        //page 6 =>5
                else if(showPreview == true){
                    console.log('showPreview',showPreview);
                    component.set("v.showRentAmount", false);
                    component.set("v.showRentPeriod", false);
                    component.set("v.showRentDue", true);
                    component.set("v.showPreview", false);
                    component.set("v.showPbhRent", false);
                    
                    helper.showPreviousRentDueTypePickList(component);
                    helper.showPreviousDueCorrectionPickList(component);
                    helper.showPreviousProrataPickList(component);
                }
    },
   
    //on Next click button for all pages
    onNextClick: function(component, event, helper) {
        console.log('onNextClick source is from ShowPreview: '+component.get("v.showPreview"));
        console.log('onNextClick source is from IsEdit : '+component.get("v.isEdit"));
        
        var showRentAmount = component.get("v.showRentAmount");
        console.log('onNextClick showRentAmount fetch: '+showRentAmount);
        
        var showRentPeriod = component.get("v.showRentPeriod");
        console.log('onNextClick showRentPeriod fetch: '+showRentPeriod);
        
        var showRentDue = component.get("v.showRentDue");
        console.log('onNextClick showRentDue fetch: '+showRentDue);
        
        var showPreview = component.get("v.showPreview");
        console.log('onNextClick showPreview fetch: '+showPreview);
        
        var showPbhRent = component.get("v.showPbhRent");
        console.log('onNextClick showPbhRent : '+showPbhRent);
        
        component.set("v.isRentErrorMsg1", false);
        component.set("v.isRentErrorMsg2", false);
        component.set("v.isRentStartErrorMsg", false);
        component.set("v.isRentEndErrorMsg", false);
        component.set("v.isPBHrateErrorMsg",false);
        component.set("v.isInvoiceDayErrorMsg", false);
        component.set("v.isRentAmtErrorMsg", false);
        component.set("v.isRentDueDayErrorMsg", false);
        component.set("v.isSecondRentStartCheckMsg", false);
        component.set("v.isSecondRentStartErrorMsg", false);
        
        console.log("onNextClick showRentAmount " +showRentAmount );
        if(showRentPeriod == true) {
            console.log("showRentPeriod :"+showRentPeriod);
                
            console.log("start");
            var rentStart = component.find('rentStartId').get("v.value");
            console.log("rentStart",rentStart);
            var rentEnd = component.find('rentEndId').get("v.value");
            console.log("rentEnd",rentEnd);
            var invoiceDay = component.find('invoiceDayId').get("v.value");
            console.log("invoiceDay",invoiceDay);
            var periodicity = component.get("v.periodValue");
            console.log("periodicity",periodicity);
            component.set("v.invoiceDayValue",invoiceDay);
            
            if(component.get("v.isExtendedRent")){
            var extendedRentType = component.find('extendedRentTypeId').get("v.value");
                component.set("v.extendedRentType",extendedRentType);
                console.log("extendedRentType",extendedRentType);
                if(component.get("v.extendedRentType") == 'Extended at Holdover Rent Rate'){
                    component.set("v.isHoldOverType",true);
                }else{
                    component.set("v.isHoldOverType",false);
                }
            }else{
                component.set("v.extendedRentType",null);
            }
            
            console.log('onNextClick : rentStart: '+rentStart+'rentEnd: '+rentEnd+'invoiceDay: '+invoiceDay);
            console.log('onNextClick : periodicity: '+periodicity);
            var isValid = true;
            var leaseInfo = component.get("v.leaseInfo");
            
            //if Rent Start Date is empty	
            if($A.util.isEmpty(rentStart) || $A.util.isUndefined(rentStart)){
                console.log('Error 1');
                component.set("v.isRentStartErrorMsg", true);
                component.set("v.rentStartErrorMsg" ,"Please Enter Rent Start Date");
                isValid = false;
            }
            
            //if Rent Start Date is before Lease Start Date
            else if(!$A.util.isEmpty(leaseInfo.LeaseStartDate) && !$A.util.isUndefined(leaseInfo.LeaseStartDate)){
                console.log('Error 2');
               console.log('rentStart '+rentStart + ' leaseInfo: '+leaseInfo.LeaseStartDate);
             
                if(rentStart < leaseInfo.LeaseStartDate) {
                    console.log('Error 2 if');
                    isValid = false;
                    component.set("v.rentStartErrorMsg" ,"Cannot be before Lease Start Date");
                    component.set("v.isRentStartErrorMsg", true);
                }
            }
            
            //if Rent End Date is empty
            if($A.util.isEmpty(rentEnd) || $A.util.isUndefined(rentEnd)) {
                console.log('Error 3');
                component.set("v.isRentEndErrorMsg", true);
                component.set("v.rentEndErrorMsg" ,"Please Enter Rent End Date");
                isValid = false;
            }
            
            //if Lease Extended using MidTermLeaseUpdateWizard
            else if(component.get("v.isFromLE")){
                var newLeaseEnd = component.get("v.leaseEndDate");
                // if rent Start date not in between Lease Start Date and Extended LEase End Date	
                if(rentStart < leaseInfo.LeaseStartDate || rentStart >newLeaseEnd ){	
                    console.log('Error 4 ');	
                    isValid = false;	
                    component.set("v.rentStartErrorMsg" ,"Rent Start Date should be in between Lease Start Date and Extension Period End Date ");	
                    component.set("v.isRentStartErrorMsg", true);	
                }	
                //if rent End Date is after Extended Lease End Date	
                if(rentEnd > newLeaseEnd){
                    console.log('Error 4 if');
                    isValid = false;
                    component.set("v.rentEndErrorMsg" ,"Cannot be after the extended Lease End Date");
                    component.set("v.isRentEndErrorMsg", true);
                }
            }else if(!$A.util.isEmpty(leaseInfo.LeaseEndDate) && !$A.util.isUndefined(leaseInfo.LeaseEndDate)){
                console.log('Error 4');
                if(!component.get("v.isExtendedRent")){
              if(rentEnd > leaseInfo.LeaseEndDate) {
                    console.log('Error 4 if');
                    isValid = false;
                    component.set("v.rentEndErrorMsg" ,"Cannot be after Lease End Date");
                    component.set("v.isRentEndErrorMsg", true);
                }
            }   
            }
            
            
            //Extended rent: If Rent Start or Rent End Date > LeaseRedeliveryDate
            if(component.get("v.isExtendedRent")){
                if(!$A.util.isEmpty(leaseInfo.LeaseRedeliveryDate) && !$A.util.isUndefined(leaseInfo.LeaseRedeliveryDate)){
                    console.log('Error E');
                    if(component.get("v.iscontinue") == false){
                    if(rentEnd > leaseInfo.LeaseRedeliveryDate || rentStart > leaseInfo.LeaseRedeliveryDate ) {
                        console.log('Error 4.1 if');
                        isValid = false;
                        component.set("v.isRedelivery", true);
                    }
                    }else
                    {
                       component.set("v.iscontinue",false);
                        isValid = true;
                    }
                }
            }
            
            //if Rent Start Date is greater than the Rent End Date
            if(rentStart >= rentEnd ){
                console.log('Error 5');
                
               component.set("v.isRentStartErrorMsg", true);
                component.set("v.rentStartErrorMsg" ,"Rent Start Date cannot be later than Rent End Date");
                isValid = false; 
            }
            
            if(isValid == true) {
                component.set("v.showRentPeriod", false);
                component.set("v.showPbhRent", false);
                component.set("v.showRentAmount", true);
                component.set("v.showRentDue", false);
                component.set("v.showPreview", false);
                console.log('showRentAmount set to true :'+component.get("v.showRentAmount"))
                var val = component.get('v.rentEscalationMonthPickListValue');
                if($A.util.isEmpty(val) || $A.util.isUndefined(val)) 
                    helper.getMonthPickList(component);
                else
                    helper.showPreviousEscPickList(component); 
                var isEdit = component.get("v.isEdit");
                if(isEdit)
                helper.getPbhVal(component);
                console.log(component.get("v.minTotalValue"));
                console.log(component.get("v.maxTotalValue"));
                console.log(component.get("v.minPBHValue"));
                console.log(component.get("v.maxPBHValue"));
                
            }
        }
         if(showRentAmount == true){
            
            console.log("inside showRentAmount",component.get("v.rentEscalationValue"));
             var escalationval = component.find('escalationId').get("v.value");
             if(escalationval != null){
                 component.set("v.rentEscalationValue",escalationval);
             }
             else{
                 component.set("v.rentEscalationValue",null);
             }
            
            var escalationMonth = component.find('escalationMonthId').get("v.value");
            component.set("v.rentEscalationMonthValue",escalationMonth);
            
            var minTotalValue = component.find('minTotalId').get("v.value");
            component.set("v.minTotalValue",minTotalValue);
            
            var maxTotalValue = component.find('maxTotalId').get("v.value");
            component.set("v.maxTotalValue",maxTotalValue);
            
             var minPBHValue = component.find('minPBHId').get("v.value");
            component.set("v.minPBHValue",minPBHValue);
             
             var maxPBHValue = component.find('maxPBHId').get("v.value");
            component.set("v.maxPBHValue",maxPBHValue);
             
              
            var offWingRentValue = component.get("v.offWingRentValue ");
              if(!component.get("v.isEdit")){
                 component.set("v.rentDueTypeValue","Fixed");
                 var rentDueType = component.get("v.rentDueTypeValue");
                 if(rentDueType == 'Fixed'){
                     component.set("v.isFixedDueType",true);
                     component.set("v.isValidSetting",true);
                     
                 }else{
                     component.set("v.isFixedDueType",false);
                     component.set("v.isValidSetting",false);
                 }
                 }
            console.log('onNextClick escalationMonth'+escalationMonth + ' amtIncDec: ' +" v.offWingRentValue :"+offWingRentValue );
             console.log('onNextClick minTotalValue : '+minTotalValue + ' maxTotalValue: ' +maxTotalValue+ ' minPBHValue : ' +minPBHValue+ 'maxPBHValue : '+maxPBHValue );
            
            var isValid = true;
            var rent = component.find('baseRentId').get("v.value");
            if($A.util.isEmpty(rent) || $A.util.isUndefined(rent)) {
                component.set("v.isRentErrorMsg1", true);
                isValid = false;
            }
             var leaseInfo = component.get("v.leaseInfo");
             var currentRent = leaseInfo.CurrentRent;
             console.log('rent :'+rent);
             console.log('currentRent :'+currentRent);
             if(component.get("v.isHoldOverType")){
                 if(rent < currentRent){
                     component.set("v.isRentErrorMsg2", true);
                isValid = false;
                 }
             }
            
            console.log('onNext click isValid '+isValid);
            if(isValid == true) {
                
                component.set("v.showRentAmount", false);
                component.set("v.showRentPeriod", false);
                component.set("v.showRentDue", false);
                component.set("v.showPreview", false);
                component.set("v.showPbhRent", true);
                
               var rentVal = component.get('v.rentDueTypePickListValue');
                if($A.util.isEmpty(rentVal) || $A.util.isUndefined(rentVal)) 
                    helper.getRentDueTypePickList(component);
                else
                    helper.showPreviousRentDueTypePickList(component);
                helper.getRentDuedayFixedVal(component);
                
                var rentSetting = component.get('v.rentDueSettingPickListValue');
                if($A.util.isEmpty(rentSetting) || $A.util.isUndefined(rentSetting)) 
                    helper.getRentDueSettingPickList(component);
                else
                    helper.showPreviousRentDueSettingPickList(component);
                
                var corVal = component.get('v.dueDateCorrectionPickListValue');
                if($A.util.isEmpty(corVal) || $A.util.isUndefined(corVal)) 
                    helper.getDueCorrectionPickList(component);
                else
                    helper.showPreviousDueCorrectionPickList(component);
                
                var prorataVal = component.get('v.prorataPickListValue');
                if($A.util.isEmpty(prorataVal) || $A.util.isUndefined(prorataVal)) 
                    helper.getProrataPickList(component);
                else
                    helper.showPreviousProrataPickList(component);
                
            }
            var isSameSession = component.get("v.isSameSession");
             console.log('isSameSession',isSameSession);
             if(isSameSession == false){
                 console.log('isSameSession if');
             helper.fetchAssemblyList(component);
             helper.getPBHrateBasisPickList(component);
             if(component.get("v.isEdit")){
                 helper.getPBHRentData(component);
                 console.log("assembly list: ",component.get("v.assemblyList"));
                 console.log("assembly list: ",component.get("v.pbhDataList"));
             }
             }else{
                 console.log('isSameSession else');
               component.set("v.isSameSession",false);
             }
        }
        if(showPbhRent == true){
            var isValid= false;
            console.log('showPbhRent');
            var assmblyList = component.get("v.assemblyList");
            console.log("fetchAssemblyList "+JSON.stringify(assmblyList));
            console.log("assmblyList.length", assmblyList.length);  
            
            
            for(var i=0; i<assmblyList.length ;i++){
                var pbhRate = assmblyList[i].PBHrate;
               // console.log("pbhDataList pbhRate", assmblyList[i].PBHrate);
               // console.log("pbhDataList selection", assmblyList[i].Selection);
                
                if(assmblyList[i].Selection){
                    if($A.util.isEmpty(pbhRate) || $A.util.isUndefined(pbhRate)) {
                        console.log('Error pbhrate');
                        component.set("v.isPBHrateErrorMsg", true);
                        //component.set("v.PBHrateErrorMsg" ,"Please Enter PBH Rate Value");
                        console.log('error done',component.get("v.isPBHrateErrorMsg"));
                        isValid = false;
                        console.log('isValid',isValid);
                    }else{
                        console.log('else',isValid);
                        isValid = true;
                     }
                }
                
            if(isValid == true) {
                console.log('if',isValid);
                component.set("v.showRentAmount", false);
                component.set("v.showRentPeriod", false);
                component.set("v.showRentDue", true);
                component.set("v.showPreview", false);
                component.set("v.showPbhRent", false);
            }
            }
            if(isValid == false){
               console.log('if outside for',isValid); 
                var msg ="Cannot save Rent Schedule without at least one Assembly selected for PBH Rate.";
                var title = "Save Error!";
                            helper.showWarningMsg(component, title, msg);
             }
            
        }
             if(showRentDue == true){
                var rentDueType = component.find('rentDueTypeId').get("v.value");
                var dueCorr = component.find('dueDayCorrectionId').get("v.value");
                var prorata = component.find('prorataId').get("v.value");
                 
                
                component.set("v.rentDueTypeValue",rentDueType);
                component.set("v.dueDateCorrectionValue",dueCorr);
                component.set("v.prorataValue",prorata);
                
                 console.log('showRentDue rentDueType',rentDueType);
                 if(rentDueType == 'Fixed'){
                     component.set("v.isFixedDueType",true);
                     component.set("v.isValidSetting",true);
                     
                 }else{
                     component.set("v.isFixedDueType",false);
                     component.set("v.isValidSetting",false);
                 }
                 
                 var isFixedDueType = component.get("v.isFixedDueType");
                 var isEdit = component.get("v.isEdit");
                 console.log('showRentDue isFixedDueType',isFixedDueType);
                 console.log('showRentDue isEdit',isEdit);
                 
                 var rentDueDay = null;
                 
                     if(isFixedDueType){
                         console.log(" if isFixedDueType");
                         rentDueDay = component.find('rentDueDayId1').get("v.value");}
                     else{
                         console.log("else 2 isFixedDueType");
                         rentDueDay = component.find('rentDueDayId').get("v.value");
                     }
                 
                 component.set("v.rentDueDayValue",rentDueDay);
                 var isValid = true;
                if($A.util.isEmpty(rentDueDay) || $A.util.isUndefined(rentDueDay))  {
                    component.set("v.isRentDueDayErrorMsg", true);
                    component.set("v.rentDueDayErrorMsg", "Please Enter Rent Due Day");
                    isValid = false;
                }
                else{
                    if(rentDueType.includes('Fixed')) {
                        if(rentDueDay > 31) {
                            isValid = false;
                            component.set("v.isRentDueDayErrorMsg", true);
                            component.set("v.rentDueDayErrorMsg", "Rent Due Type 'Fixed' has been selected, therefore value in Rent Due Day field must be between 1 and 31 (days in the calendar month).");
                        }
                    }    
                }
                 
                 var isValidSetting = component.get("v.isValidSetting");
                 var rentDueSetting = null;
                 console.log('isValidSetting',isValidSetting);
                 if(isValidSetting){ 
                 rentDueSetting = component.find('settingId3').get("v.value"); 
                 }
                 else{
                  rentDueSetting = component.find('rentDueSettingId').get("v.value"); 
                 }
                 component.set("v.rentDueSettingValue",rentDueSetting); 
                 console.log('showRentDue rentDueSetting',rentDueSetting);
                 
                
            
                if(isValid == true) {
                    console.log("isValid :",isValid);
                    component.set("v.showRentAmount", false);
                    component.set("v.showRentPeriod", false);
                    component.set("v.showRentDue", false);
                    component.set("v.showPreview", true);
                    component.set("v.showPbhRent", false);
                    
                    
                }
            }
                 if(showPreview == true) {
                     console.log("showPreview :",showPreview);
                     console.log('assembly list at 5',component.get("v.assemblyList"));
                    component.set("v.isLoading", true);
                    helper.checkLeaseApprovalStatus(component);
                }
    },
    
    //On Edit button click
    onEditData: function(component, event, helper){
        console.log('onEditData');
        
        var multiRent = component.get("v.multiRent");
        console.log('multirentdata on edit :'+multiRent);
        console.log(component.get('v.commentValue'));
        console.log(component.get('v.includeComment'));
        
        var isValid = true;
        if(!$A.util.isEmpty(multiRent) && !$A.util.isUndefined(multiRent)) {
            var status = multiRent.Status;
            if(status.toUpperCase() == "Active".toUpperCase())
                component.set("v.isActive", true);
            else if(status.toUpperCase() == "InActive".toUpperCase()){
                component.set("v.isActive", false);
                isValid = false;
            } 
            console.log('onEditData '+status+'isValid'+isValid);
        }
        
        if(isValid) {
            component.set("v.showComponent", false);
            component.set("v.showRentAmount", false);
            component.set("v.showRentPeriod", false);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            component.set("v.isEdit", true);
            component.set("v.isViewMode", false);
            
            
            //need to fire event 
            var leaseId = component.get("v.leaseId");
            console.log("onEditData "+leaseId);
            var onTypeWizardSchedule = component.getEvent("onWizardCancelButton");
            onTypeWizardSchedule.setParams({
                "leaseId" : leaseId,
                "showTypeWizard" : true, 
                "isEdit" : component.get("v.isEdit")
            });
            onTypeWizardSchedule.fire();
        }
        else{
            helper.showWarningMsg(component, "InActive", "The rent schedule is unable to edit because the schedule is ticked as Inactive.");
        }
    },
    
    //onDelete multiRent record
    onDeleteRecord: function(component, event, helper){
        console.log('onDeleteRecord');
        
        var multiRent = component.get("v.multiRent");
        var isValid = true;
        if(!$A.util.isEmpty(multiRent) && !$A.util.isUndefined(multiRent)) {
            var status = multiRent.Status;
            if(status.toUpperCase() == "InActive".toUpperCase())
                isValid = false;
            console.log('onEditData '+status);
        }
        
        if(isValid) {
            component.set("v.isLoading", true);
            helper.deleteMultiRentSch(component);
            //this.refreshWizard(component);
        }
        else{
            helper.showWarningMsg(component, "InActive", "The rent schedule is unable to delete because the schedule is ticked as Inactive.");
        }
    },
    
    //used to show the list of schedules for the clicked multirent record
    showRecord: function(component,event,helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord Fixed wizard "+recordId);
        window.open('/' + recordId);  
    },
    
    toggleChange: function(component, event, helper){
        console.log("toggleChange");
        console.log("toggleChange isActive "+component.get("v.isActive") +' loading:'+component.get("v.isLoading"));
    },
    
    //used when checkbox is true or false,to hide the second rent Date field
    secondRentPeriod : function(component, event, helper) {
        let checkBoxState = event.getSource().get('v.value');
        
        //for label when checkbox true
        var changeElement = component.find("secondRentStartId1");
        $A.util.toggleClass(changeElement, "slds-hide");
        
        //for date field when checkbox true
        var changeElement1 = component.find("secondRentStartId2");
        $A.util.toggleClass(changeElement1, "slds-hide");
        
        //for label and datefield together wheck check box is false
        var changeElement2 = component.find("secondRentStartId3");
        $A.util.toggleClass(changeElement2, "slds-hide");
        
        

    },
    
    //used when checkbox is true or false,to hide the rent due type picklist field
    showSetting : function(component, event, helper) {
        console.log("showSetting");
        var duetype = component.find('rentDueTypeId').get("v.value");
         console.log('showSetting',duetype);
        
        if(duetype == 'Fixed'){
            component.set("v.isValidSetting",true);   
            component.set("v.isFixedDueType",true);            
            var changeElement = component.find("settingId2");
            console.log('changeElement',changeElement);
            $A.util.toggleClass(changeElement, "slds-hide");
            helper.getRentDueSettingPickList(component);
            helper.getRentDuedayFixedVal(component);
        }
        else{
            component.set("v.isValidSetting",false); 
            component.set("v.isFixedDueType",false);
            var changeElement1 = component.find("settingId1");
            console.log('changeElement1');
            $A.util.toggleClass(changeElement1, "slds-hide"); 
            
            var changeElement3 = component.find("settingId3");
            console.log('changeElement3');
            $A.util.toggleClass(changeElement3, "slds-hide"); 
            
        }
        },
    
    //used for isRedelivery pop-up model close 
    closeModel : function(component,event,helper){
        console.log('closeModel');
         if(component.get("v.showRentPeriod") == true){
            component.set("v.showComponent", false);
            component.set("v.showRentAmount", false);
            component.set("v.showRentPeriod", false);
            component.set("v.showRentDue", false);
            component.set("v.showPreview", false);
            
            //need to fire event 
            var leaseId = component.get("v.leaseId");
            console.log("onPreviousClick "+leaseId);
            var onTypeWizardSchedule = component.getEvent("onWizardCancelButton");
            onTypeWizardSchedule.setParams({
                "leaseId" : leaseId,
                "showTypeWizard" : true,
                "isEdit" : component.get("v.isEdit")
            });
            onTypeWizardSchedule.fire();
        }
        component.set("v.isRedelivery",false);
       
    },
    
    //used for isRedelivery pop-up model: when pressed Ok, then continue to Step3
    ProceedStep3 : function(component,event,helper){
        console.log('ProceedStep3');
        component.set("v.isRedelivery",false);
        component.set("v.iscontinue",true);
    },
       
})