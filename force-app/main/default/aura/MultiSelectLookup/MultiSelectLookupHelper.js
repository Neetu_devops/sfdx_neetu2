({
    searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        var action = component.get("c.fetchLookUpValues");
        // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName"),
            'ExcludeitemsList' : component.get("v.lstSelectedRecords"),
            'whereClause' :  component.get("v.whereClause")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                var message = component.get('v.noRecordsMessage');
                // if storeResponse size is equal 0 ,display No Records Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set('v.Message',message );
                } else {
                    component.set("v.Message", '');
                    // set searchResult list with return value from server.
                }
                
                storeResponse.forEach(function(response) {
                    if(component.get("v.objectAPIName") == 'Aircraft__c' && component.get("v.canvass")) {
                       
                        var nextLesse = '';
                        var assignedTo = '';
                        
                        if(response.Next_Lessee__c != undefined){
                            nextLesse = (response.Next_Lessee__c.split('">')[1]).split('<')[0];
                        }   
                        if(response.Assigned_To__c != undefined){
                            assignedTo = (response.Assigned_To__c.split('">')[1]).split('<')[0];
                        }    
                        response.Next_Lessee__c = nextLesse == ' ' ? '' : nextLesse;
                        response.Assigned_To__c = assignedTo == ' ' ? '' : assignedTo;
                    }
                    
					return response;
                });
                console.log('storeResponse:', storeResponse);
                component.set("v.listOfSearchRecords", storeResponse); 
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },
})