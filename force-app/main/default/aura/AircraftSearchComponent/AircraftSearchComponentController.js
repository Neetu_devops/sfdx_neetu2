({
    init : function(cmp, event, helper) {
        var action = cmp.get("c.retrievePicklistValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            // This callback doesn’t reference cmp. If it did,
            // you should run an isValid() check
            //if (cmp.isValid() && state === "SUCCESS") {
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                var result = response.getReturnValue();
                cmp.set("v.aircraftTypeValues",result.aircraftTypeValues);
                cmp.set("v.vintageRange",result.vintageRange);
                cmp.set("v.aircraftPicklists",result);
                console.log(result);
                var sliderLabels = [];
                var maxLabels = (result.maxVintage - result.minVintage)/5;
                var label = result.minVintage;
                for(var x=0; x<maxLabels; x++){
                    sliderLabels.push(label);
                    label += 5;
                }
                if(sliderLabels[sliderLabels.length-1] < result.maxVintage)
                    sliderLabels.push(result.maxVintage);
                
                $('.slider-input').jRange({
                    from: result.minVintage,
                    to: result.maxVintage,
                    step: 1,
                    scale: sliderLabels,
                    format: '%s',
                    width: 900,
                    showLabels: true,
                    isRange : true
                });
                var currentYear = new Date().getFullYear();
                var selectedVal = currentYear-3 +','+(currentYear+3);
                $('.slider-input').jRange('setValue', selectedVal);
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            //else if (cmp.isValid() && state === "INCOMPLETE") {
            else if (state === "INCOMPLETE") {
                // do something
            }
            //else if (cmp.isValid() && state === "ERROR") {
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    //change the dependent picklist values
    changePicklistValues :function(cmp,event){
        var target = event.target;
        var changedPicklistId = $(target).attr('id');
        var setOptions = false;
        var allPicklistValues = cmp.get("v.aircraftPicklists");
        if(changedPicklistId == 'aircraftTypeValues'){
            var _picklistValMap = allPicklistValues['aircraftVariantValues'];
            var _picklistVal = _picklistValMap[$(target).val()];
            cmp.set("v.aircraftVariantValues",_picklistVal);
            
            _picklistValMap = allPicklistValues['engineTypeValues'];
            _picklistVal = _picklistValMap[$(target).val()];
            cmp.set("v.engineTypeValues",_picklistVal);
            
            var emptyArray = [];
            cmp.set("v.engineVariantValues",emptyArray);
        } else if(changedPicklistId == 'engineTypeValues'){
            var _picklistValMap = allPicklistValues['engineVariantValues'];
            var _picklistVal = _picklistValMap[$(target).val()];
            cmp.set("v.engineVariantValues",_picklistVal);
        }
        
    },
    
    goToSearchResultPage : function(cmp,event){
        var aircraftType = $('#aircraftTypeValues').val();
        var aircraftVariant = $('#aircraftVariantValues').val();
        var engineType = $('#engineTypeValues').val();
        var searchTransaction = $('#isTransaction').is(':checked');
        var engineVariant = $('#engineVariantValues').val();
        var aircraftVintageRange = ($(".slider-input").val().split(','))[1];
        var aircraftVintage = ($(".slider-input").val().split(','))[0];
        var leaseDateTo = $("#leaseDateTo").val();
        var leaseDateFrom = $("#leaseDateFrom").val();
        var evt = $A.get("e.c:AircraftSearchEvent");
        evt.setParams({'aircraftType':aircraftType,
                       'aircraftVariant' : aircraftVariant,
                       'engineType' : engineType,
                       'engineVariant' : engineVariant,
                       'vintage' : aircraftVintage,
                       'vintageRange' : aircraftVintageRange,
                       'searchTransaction' : searchTransaction,
                       'leaseDateTo' : leaseDateTo,
                       'leaseDateFrom' : leaseDateFrom,
                       'evtName' : 'showSearchResults'
                      });
        evt.fire();
    },
    initialize : function(cmp){
        
    }
})