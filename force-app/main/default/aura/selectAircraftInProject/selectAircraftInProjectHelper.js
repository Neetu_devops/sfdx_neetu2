({
    
    loadData: function(component, event) {
        var recordId = component.get("v.recordId");
        
        this.showSpinner(component);
        var action = component.get("c.getData");
        action.setParams({
            "projectId": recordId,
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            
            if (response.getState() === "SUCCESS") {
                
                var allData =  response.getReturnValue();
                
                component.set("v.showListState" , allData.showOperatorFinderLink);
                
                component.set("v.layoutSectionList", allData.layoutSectionList);
                component.set("v.selectedAircraftRecords", allData.aircrafts);
                component.set("v.selectedAirlineRecords", allData.airlines);
                component.set("v.selectedLessorRecords", allData.lessors);
                component.set("v.selectedCounterpartyRecords", allData.counterparties);
                if(allData.tradingProfileFeature != null){
                    component.set("v.showCheckProfileButton", allData.tradingProfileFeature);
                }
                
                if(allData.selectedProspect != null){
                    
                    var campaignConfigList = allData.selectedProspect.split(";");
                    
                    if(campaignConfigList.includes("Airlines/Operators")){
                        component.set("v.includeAirlines", true);
                    }
                    if(campaignConfigList.includes("Lessors")){
                        component.set("v.includeLessors", true);
                    }
                    if(campaignConfigList.includes("Counterparties")){
                        component.set("v.includeCounterparties", true);
                    }
                }
				
                //capture old data.
                this.captureOldAircraftsData(component,allData.aircrafts);
                if(component.get("v.includeAirlines")){
                    this.captureOldProspectsData(component,allData.airlines);
                }
                if(component.get("v.includeLessors")){
                    this.captureOldProspectsData(component,allData.lessors);
                }
                if(component.get("v.includeCounterparties")){
                    this.captureOldProspectsData(component,allData.counterparties);
                }
            }
            else if(state === "ERROR") {
        
                var errors = response.getError();
                this.handleErrors(errors);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    gotoProject : function (component, event, SuccessRecord) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": SuccessRecord,
            "slideDevName": "detail"
        });
        navEvt.fire();
        $A.get('e.force:refreshView').fire();
    },
    
    captureOldAircraftsData : function (component, items) {
        var aircraftIds = component.get("v.oldAircraftIds");
        if(items != undefined && items.length > 0){
            
            for(var i=0; i < items.length ; i++){
                aircraftIds.push(items[i].Id);
            }
            component.set("v.oldAircraftIds",aircraftIds);
        }
    }, 
    
    captureOldProspectsData : function (component, items) {
        var prospectIds = component.get("v.oldProspectIds");
        if(items != undefined && items.length > 0){
            for(var i=0; i < items.length ; i++){
                prospectIds.push(items[i].Id);
            }
            component.set("v.oldProspectIds",prospectIds);
        }
    },
    
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    },
    
    createChildRecords : function (component, helper, event, obj, answer){
        
        var prospects = [];
        
        if(obj.selectedAirlines.length>0){
        	obj.selectedAirlines.forEach(function(item) {
            	prospects.push(item);
            });   
        }
                                         
        if(obj.selectedLessors.length>0){
			obj.selectedLessors.forEach(function(item) {
           		prospects.push(item);
            });    
        }
        	
        if(obj.selectedCounterparties.length>0){
            obj.selectedCounterparties.forEach(function(item) {
                prospects.push(item);
            });
        }
        var prospectCount = component.get("v.prospectsLength");
        var batchCount = Math.ceil(prospects.length/prospectCount) ;
        var increment =  100 / batchCount;
        component.set('v.progress', 0);
        $A.util.removeClass(component.find('progressBar'), "slds-hide");
        component.set("v.existingDeals", 0);
        component.set("v.newDeals", 0);
        this.showSpinner(component);
        this.saveRecords(component, helper, obj.selectedAssets, prospects, answer, increment, []);     

    },

    saveRecords : function (component, helper, assets, allProspects, answer, increment, associatedDeals){
        
        var action = component.get("c.saveProject");
        var prospects = [];
        var prospectsCount = component.get("v.prospectsLength");
        console.log('prospectsCount : '+prospectsCount);
        var noOfElements = allProspects.length > prospectsCount  ? prospectsCount : allProspects.length ;
        console.log('noOfElements : '+noOfElements);
		prospects = allProspects.splice(0, noOfElements);

        console.log('allProspects--> ' + allProspects.length);	

        action.setParams({
            "projectId": component.get("v.recordId"),
            "lstAircraft" : assets,
            "prospects" : prospects
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (response.getState() == "SUCCESS") {
                
                var result = response.getReturnValue();  
                
                if(result.status == 'Success'){ 
                    
                    component.set("v.existingDeals", component.get("v.existingDeals") + result.existingDeals);
                    component.set("v.newDeals", component.get("v.newDeals") + result.newDeals);
                    
                    result.associatedDeals.forEach(function(item) {
                        associatedDeals.push(item);
                });
                

                    var progress = component.get('v.progress');
            		component.set('v.progress', progress + increment);
                    
                    if(allProspects.length>0){
                        this.saveRecords(component, helper, assets, allProspects, answer, increment, associatedDeals);
                    }else{
                    	this.detachDeals(component, event, helper, answer, associatedDeals);
                    }
                }else{
                    //To initiate summary calculation future call and show error.
                    //detachDeals is not called in case of any failure.
					this.calculateSummary(component, helper, result);
                }
            }
            else if(state === "ERROR") {
                
                this.hideSpinner(component);
                
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                } else {
                    alert("Unknown error");
                }
                $A.util.addClass(component.find('progressBar'), "slds-hide");
            }
            
        });
        $A.enqueueAction(action); 
    },
	
    calculateSummary : function(component, helper, result){
        
        var action = component.get("c.calculateSummary");
        action.setParams({
            "campaignId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) {
            this.hideSpinner(component);
            this.showToast(result.status,result.status,result.message);
            $A.util.addClass(component.find('progressBar'), "slds-hide");
        });
        $A.enqueueAction(action);
    },
	
    detachDeals: function (component, event, helper, answer, associatedDeals){

        console.log('delete called');
        console.log(associatedDeals);
        
        var action = component.get("c.detachDeals");
        action.setParams({
            "campaignId": component.get("v.recordId"),
            "associatedDeals" : associatedDeals,
            "answer" : answer
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (response.getState() == "SUCCESS") {
                      
                var msg =  component.get("v.newDeals")  + ' deals created, ' +  component.get("v.existingDeals")   + ' existing deals associated to the campaign.';
                this.showToast('Success','Success', msg);
                
                window.setTimeout(
                    $A.getCallback(function() {
                        helper.gotoProject(component,event,component.get("v.recordId"));
                    }), 1000
                );
                 
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                } else {
                    alert("Unknown error");
                }
                 $A.util.addClass(component.find('progressBar'), "slds-hide");
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    
    getLookupValues : function (component, event){
        
        var obj = new Object();
        
        obj.selectedAssets = [];
        obj.selectedAirlines = [];
        obj.selectedLessors = [];
        obj.selectedCounterparties = [];
        
        component.get("v.selectedAircraftRecords").forEach(function(entry) {
            obj.selectedAssets.push(entry.Id);
        });
        
        component.get("v.selectedAirlineRecords").forEach(function(entry) {
            obj.selectedAirlines.push(entry.Id);
        });
        
        component.get("v.selectedLessorRecords").forEach(function(entry) {
            obj.selectedLessors.push(entry.Id); 
        });
        
        component.get("v.selectedCounterpartyRecords").forEach(function(entry) {
            obj.selectedCounterparties.push(entry.Id); 
        });
        
        return obj;
    },
    
    showSpinner: function (component, event, helper) {
        component.set("v.showSpinner",true);
    },
    
    hideSpinner: function (component, event, helper) {
        component.set("v.showSpinner",false);
    },
    
    showToast: function(title, type, message){

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : type,
            "message": message
    	});
    	toastEvent.fire();
    }
})