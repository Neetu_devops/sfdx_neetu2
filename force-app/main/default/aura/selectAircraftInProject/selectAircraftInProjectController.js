({
   
    navHome : function (component, event, helper) {
       
        if(component.get("v.recordId") != undefined){
            helper.gotoProject(component,event,component.get("v.recordId"));
        }else{
           
            var homeEvent = $A.get("e.force:navigateToObjectHome");
            homeEvent.setParams({
                "scope": "Project__c"
            });
            homeEvent.fire();
        }
    },
   
    doInit: function(component, event, helper) {
        helper.loadData(component, event);
    },
   
    onRecordSubmit : function(component, event, helper){
       
        event.preventDefault();
        var eventFields = event.getParam("fields");
        if('Name' in eventFields){
            if(eventFields["Name"] == undefined || eventFields["Name"] == ''){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Required",
                    "type" : "error",
                    "message": "Please enter campaign name."
                });
                toastEvent.fire();
                return;
            }  
        }
       
        //check that if project is inactive then don't allow to add/ delete aircraft/ prospect.
        if('Inactive__c' in eventFields && eventFields['Inactive__c']){

            var obj = helper.getLookupValues(component,event);
            var oldAircrafts = Object.values(component.get("v.oldAircraftIds"));  
           
            var oldProspects = Object.values(component.get("v.oldProspectIds"));
            var newProspects = [];
 
            if(obj.selectedAirlines.length>0){
                for(var i = 0; i < obj.selectedAirlines.length; i++){
                    newProspects.push(obj.selectedAirlines[i]);
                }
            }

            if(obj.selectedLessors.length>0){
                for(var i = 0; i < obj.selectedLessors.length; i++){
                    newProspects.push(obj.selectedLessors[i]);
                }
            }

            if(obj.selectedCounterparties.length>0){
                for(var i = 0; i < obj.selectedCounterparties.length; i++){
                    newProspects.push(obj.selectedCounterparties[i]);
                }
            }
           
            var newAircrafts = obj.selectedAssets;
           
            if((newAircrafts.sort().join(',') !== oldAircrafts.sort().join(',')) ||
               (newProspects.sort().join(',') !== oldProspects.sort().join(','))){
           
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Access Denied",
                    "type" : "warning",
                    "message": "This Campaign is not active. Please change the status of the Campaign to add/remove Assets or Prospects."
                });
                toastEvent.fire();
                return;
            }
        }

        if(component.get("v.selectedAirlineRecords").length == 0 && component.get("v.selectedLessorRecords").length  == 0 &&
         component.get("v.selectedCounterpartyRecords").length == 0 ){
           
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Alert",
                "type" : "error",
                "message": "Please add the Prospect for this Deal by populating either Lessor, Counterparty or/and Airline."
            });
            toastEvent.fire();
            return;
        }  

        helper.showSpinner(component);
        component.find('projectForm').submit(eventFields);
    },
    onError : function(component, event, helper){
        helper.hideSpinner(component);
    },
   
    createAssetDetails :  function(component, event, helper) {
       
        var payload = event.getParams().response;
       
        //getting selected airlines, aircrafts and lessors.
        var obj = helper.getLookupValues(component,event);
       
        //it if is edit case.
        if(component.get("v.recordId") != undefined){

            //check if any prospect or airline deleted or not.
            var prospectsCount = 0;
            var assetsCount = 0;
            var oldProspects = component.get("v.oldProspectIds");
            var oldAssets = component.get("v.oldAircraftIds");
            
            if(oldProspects != undefined && oldProspects.length > 0){
               
                for(var i = 0; i < oldProspects.length; i++){
                    if(!obj.selectedAirlines.includes(oldProspects[i]) && !obj.selectedLessors.includes(oldProspects[i])
                      && !obj.selectedCounterparties.includes(oldProspects[i])){
                        prospectsCount++;
                    }
                }
            }
            
            if(oldAssets != undefined && oldAssets.length > 0){
                for(var i = 0; i < oldAssets.length; i++){
                    if(!obj.selectedAssets.includes(oldAssets[i])){
                        assetsCount++;
                    }
                }
            }
            
            
            if(prospectsCount > 0 || assetsCount > 0){
                  
                var warning = 'Are you sure you want to remove ';
               
                if(prospectsCount > 0 && assetsCount > 0){
                    warning +=  assetsCount + ' asset(s) and ' + prospectsCount + ' prospect(s)'; 
                }else if(prospectsCount > 0){
                    warning += prospectsCount + ' prospect(s)';
                }else{
                    warning += assetsCount + ' asset(s)';
                }
                warning += ' from Campaign?';

                component.set("v.warning", warning);

                helper.hideSpinner(component);
                component.set("v.showConfirmationBox", true);
                return;
                
            }else{
               
                helper.createChildRecords(component,helper,event,obj,undefined);
            }
        }else{
            component.set("v.recordId", payload.id);
            helper.createChildRecords(component,helper, event,obj,undefined);
        }    
    },
   
    saveChilds : function(component, event, helper) {
        var answer = event.currentTarget.dataset.answer;
        console.log('answer '+answer);
        if(answer != 'Cancel'){
            //getting selected airlines, aircrafts and lessors.
            var obj = helper.getLookupValues(component,event);
           
            helper.createChildRecords(component, helper, event,obj,answer);
        }
        component.set("v.showConfirmationBox", false);
        component.set("v.warning", "");
       
    },
   
    bulkupload : function(component, event, helper) {
        component.set("v.openModal", true);
    },
    openOperatorModal: function(component, event, helper) {
        component.set("v.openFilterModal", true);
    },
   
    handleOperatorChange : function(component, event, helper) {
       
        var proccessOperator = component.get("v.proccessOperator");
        if(proccessOperator){
            var selectedAirlineRecords = component.get("v.selectedAirlineRecords");
            var filteredAircraftRecords = component.get("v.filteredAircraftRecords");
           
            var oldAirlineIds = [];
            var addAirlines = [];
            for(var i = 0; i < selectedAirlineRecords.length; i++){
                oldAirlineIds.push(selectedAirlineRecords[i].Id);
                addAirlines.push(selectedAirlineRecords[i]);
            }
           
            if(oldAirlineIds.length > 0){
               
                for(var i = 0; i < filteredAircraftRecords.length; i++){
                    if(!oldAirlineIds.includes(filteredAircraftRecords[i].Id)){
                        addAirlines.push(filteredAircraftRecords[i]);
                    }
                }
                component.set("v.selectedAirlineRecords", addAirlines);
               
            }else{
            component.set("v.selectedAirlineRecords", filteredAircraftRecords);
            }
            component.set("v.filteredAircraftRecords", []);
            component.set("v.proccessOperator", false);
       
		}	
    }
})