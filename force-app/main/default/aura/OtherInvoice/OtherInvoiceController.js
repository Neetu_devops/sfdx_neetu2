({
    doInit : function(component, event, helper) {
        if(component.get("v.wrapperObj.record.Id") != undefined && component.get("v.wrapperObj.record.Id") != ''){
            component.set("v.invoiceObj.Lease__c",component.get("v.wrapperObj.record.Id"));  
        } 
    },
    leaseSelected : function(component, event, helper) {
        
        var val = event.getParam("recordByEvent");
        var leaseId = val.Id.trim();
        component.set("v.invoiceObj.Lease__c",leaseId);
    },
    
    handleSaveEvent : function(component, event, helper) {

        var invoiceObj = component.get("v.invoiceObj");
        
        if($A.util.isEmpty(invoiceObj.Lease__c) || $A.util.isEmpty(invoiceObj.Invoice_Date__c)){
            
            let toastParams = {
                    title: "Error",
                    message: "Please fill required fields.", // Default error message
                    type: "error"
                };
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
                return;
        }   
        
        component.set("v.showSpinner", true);
		// call the apex class method 
        let action = component.get("c.saveInvoice"); 
        
        action.setParams({'invoiceObj': invoiceObj, 'recordTypeDeveloperName': component.get('v.recordType'),
                          'assemblyMRInfoList' : ''});
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": response.getReturnValue().Id,
                    "slideDevName": "detail"
                });
                navEvt.fire();
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
            component.set("v.showSpinner", false);
        });
        // enqueue the Action  
        $A.enqueueAction(action); 	
    }
})