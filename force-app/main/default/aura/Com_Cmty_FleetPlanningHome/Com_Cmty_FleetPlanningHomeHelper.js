({
    loadData: function (cmp, event, helper) {
        helper.setColumns(cmp);

        //set Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title": "Fleet Planning Home" });
        appEvent.fire();

        helper.showSpinner(cmp);
        var action = cmp.get("c.getData");

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                cmp.set("v.data", data);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var cmpEvent = cmp.getEvent("ErrorsEvent");
                    cmpEvent.setParams({ "errors": errors });
                    cmpEvent.fire();
                } else {
                    console.log("Unknown error");
                }
            }
            helper.hideSpinner(cmp);
        });

        $A.enqueueAction(action);
    },
    setColumns: function (cmp) {

        var leaseOverviewColumns = [
            { label: 'Lease', fieldName: 'title', sortable: true },
            { label: 'MSN/ESN', fieldName: 'msnURL', type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'msn' } }, sortable: true },
            { label: 'Type', fieldName: 'type', sortable: true },
            { label: 'Operator Name', fieldName: 'operatorName', sortable: true },
            { label: 'Status', fieldName: 'status', sortable: true },
            { label: 'Lease Start', fieldName: 'leaseStart', type: 'date', sortable: true },
            { label: 'Lease End', fieldName: 'leaseEnd', type: 'date', sortable: true },
            { label: 'GT Lease', fieldName: 'isGTLease', sortable: true }, 
            { label: 'Months Remain', fieldName: 'monthsRemaining', type: 'number', sortable: true },
            { label: 'Base Rent', fieldName: 'baseRent', type: 'currency', typeAttributes: { currencyCode: 'USD'}, sortable: true },
            { label: 'Action', fieldName: 'submitUR_URL', type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'submitURTitle'}}}
        ];
         
        cmp.set('v.leaseOverviewColumns', leaseOverviewColumns);
         
        //             { label: 'Aircraft Type', fieldName: 'aircraftType', sortable: true }, 
        var aircraftsColumns = [
            { label: 'MSN', fieldName: 'msnURL', type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'msn' } }, sortable: true },
            { label: 'Aircraft Type', fieldName: 'aircraftType'},
            //{ label: 'Asset Variant', fieldName: 'assetVariant', sortable: true },
            { label: 'Vintage', fieldName: 'vintage', sortable: true },
            { label: 'Aircraft Engine Type', fieldName: 'aircraftEngineType', sortable: true },
            { label: 'Operator Name', fieldName: 'operatorName', sortable: true },
            { label: 'TSN', fieldName: 'tsn', sortable: true, type: 'number' },
            { label: 'CSN', fieldName: 'csn', sortable: true, type: 'number' },
        ];
         
        cmp.set('v.aircraftsOnLeaseColumns', aircraftsColumns);
            

        var enginesColumns = [
            { label: 'ESN', fieldName: 'esnURL', type: 'url', typeAttributes: {target: '_blank', label: { fieldName: 'esn' } }, sortable: true },
            { label: 'Asset Series', fieldName: 'assetSeries', sortable: true },
            { label: 'Configuration', fieldName: 'currentThrust', sortable: true },
            { label: 'Operator Name', fieldName: 'operatorName', sortable: true },
            { label: 'TSO', fieldName: 'tso', sortable: true, type: 'number' },
            { label: 'CSO', fieldName: 'cso', sortable: true, type: 'number' },
            { label: 'Limiter', fieldName: 'limiter', type: 'number', sortable: true }
        ];

        cmp.set('v.enginesOnLeaseColumns', enginesColumns);
    },
    showSpinner: function (cmp) {
        var spinner = cmp.find("spinner");
        console.log('spinner' + spinner);
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function (cmp) {
        var spinner = cmp.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },
})