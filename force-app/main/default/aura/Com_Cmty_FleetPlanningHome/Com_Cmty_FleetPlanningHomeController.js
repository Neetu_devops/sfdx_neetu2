({
    init: function(cmp, event, helper) {
        window.setTimeout(
            $A.getCallback(function() {
                helper.loadData(cmp, event, helper);
            }), 200
        );
    },

    goBack: function(cmp, event, helper) {
        cmp.set("v.type", undefined);
        //unset Title on the header.
        var appEvent = $A.get("e.c:HeaderTitleEvent");
        appEvent.setParams({ "title" : ""});
        appEvent.fire();
    },
    
    viewAllAssets : function(cmp, event, helper) {
   		cmp.set("v.showAllAssets", true);  
    }
    
});