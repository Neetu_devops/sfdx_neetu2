({   
    showMessage : function(message, type) {
        var toastEvent = $A.get("e.force:showToast");              
        toastEvent.setParams({  
            message: message,                   
            type: type
        }); 
        toastEvent.fire(); 
    },
    

    fetchAssetsInDeal: function(component, event, helper) {
        component.set('v.showSpinner', true);
        
        var selectedRecordId = component.get("v.selectedRecordId");
        if(selectedRecordId != undefined && selectedRecordId != '' && selectedRecordId != null){
            var action = component.get("c.getAssetsInDeal");
            action.setParams({
                recId : selectedRecordId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS") {
                    //console.log('response: ',response.getReturnValue());
                    component.set("v.lstAssetsinDeal", response.getReturnValue());
                    this.fetchPageLayout(component, event, helper);
                }
                else if(state == "ERROR"){
                    var errors = response.getError();
                    this.handleErrors(errors);
                }
                component.set('v.showSpinner', false);
            });
            
            $A.enqueueAction(action);
        }
        else{
            this.fetchPageLayout(component, event, helper);
        }
    },


    //To get the Page Layout fields
    fetchPageLayout: function(component, event) {
        var recordId;
        var actionType = component.get("v.actionType");
        
        if(component.get("v.selectedRecordId") != undefined && component.get("v.selectedRecordId") != '') {
            recordId = component.get("v.selectedRecordId"); 
        }
        else {
            recordId = component.get("v.recordId"); 
        }

        var dealTypeField = '';
        if(component.get("v.dealTypeField") != undefined && component.get("v.dealTypeField") != ''){
            dealTypeField = component.get("v.dealTypeField");
        }
        
        var action = component.get("c.getPageLayoutFields");
        action.setParams({
            'recordId' : recordId,
            'dealTypeField' : dealTypeField
        });        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state: ', state);
            if (state === "SUCCESS") {
                var result =  response.getReturnValue();
                console.log('ResultCommercial: ',result);
                if(result.errorMsg == true){
                    component.set('v.showSection',false);
                    this.showMessage('Could not retrieve layout for this record type','error');
                    component.set('v.showSpinner', false);
                }
                else{
                var lstSections = result.lstSections;
                
                console.log(result, lstSections, actionType);
                
                if(actionType == 'Clone') {
                    for(var key in lstSections){
                        if(lstSections[key].lstFields != undefined && lstSections[key].lstFields != ''){
                            for(let key2 in lstSections[key].lstFields){
                                var field = lstSections[key].lstFields[key2];
                                console.log(field , ' ---- ', result.selectedRecord[''+field.fieldName]);
                                field["value"] = result.selectedRecord[''+field.fieldName];
                            }
                        }
                    }
                    
                    component.set("v.selectedRecordId", ''); 
                } 
                
                component.set("v.layoutSections", lstSections);
                component.set("v.commercialTermType", result.commercialTermType); 
                
                var recform = component.find("myform");
                recform.set("v.recordTypeId", result.recordTypeId);               
            }
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
            component.set("v.showSpinner", false);
        }); 
        
        $A.enqueueAction(action);  
    },
    

    saveRespectiveRecords : function(component, event, helper){
        component.set('v.showSpinner', true);

        var actionType = component.get("v.actionType");
        var showAssetsInDealSelector = component.get("v.showAssetsInDealSelector");
        var openRecordPage = component.get("v.openRecordPage");

        //console.log(component.get("v.lstSelectedRecords"));
        var assetRecord = component.get("v.lstSelectedRecords");
        var assetList = [];

        var items = component.get('v.items');
        var msnList = [];

        var assetInDealRecords = component.get("v.lstAssetsinDeal");
        var assetInDealList = [];
        
        if(showAssetsInDealSelector){
            for(var i = 0; i < assetInDealRecords.length; i++) {
                assetInDealList.push({
                    'Id': assetInDealRecords[i].Id,
                    'Name': assetInDealRecords[i].Name
                });
            }
        }
        else{
            for(let key in assetRecord) {
                assetList.push({ 'Id': assetRecord[key].Id, 'Name': assetRecord[key].Name });
            }
            
            for(let key in items) {
                msnList.push(items[key].name);
            }
        }
        console.log(assetInDealList, assetList, msnList);

        var action = component.get("c.processRelatedRecords");
        action.setParams({
            'commercialTermId' : component.get("v.selectedRecordId"),
            'dealId' : component.get("v.recordId"),
            'assetsList' : assetList,
            'serialNumber' : component.get("v.serialNumber"),
            'msnList' : msnList,
            'actionType' : component.get("v.actionType"),
            'assetInDealList' : assetInDealList,
            'isAISSelected' : showAssetsInDealSelector
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === 'SUCCESS') {
                console.log('response',response.getReturnValue());
                if(actionType == 'Edit') {
                    this.showMessage(component.get("v.relatedListLabel") + ' "' + component.get("v.recordName") + '" was saved', 'success');              
                }   
                else {
                    this.showMessage(component.get("v.relatedListLabel") + ' "' + component.get("v.recordName") + '" was created', 'success');
                    if(openRecordPage){
                        this.navigateToRecordPage(component, event, component.get("v.selectedRecordId"));
                    }
                }
                component.set("v.createNewRecord", false);
                component.set('v.showSpinner', false);
                $A.get('e.force:refreshView').fire();
                var refreshEvt = $A.get("e.c:CommercialTermListRefreshEvent");
                refreshEvt.fire();
               //  window.location.reload();
            }    
            else {
                component.set('v.showSpinner', false);
                var errors = response.getError();
                this.handleErrors(errors);
            }
        }); 
        
        $A.enqueueAction(action);
    },


    navigateToRecordPage : function(component, event, recordId){
        if(recordId != undefined && recordId != '' && recordId != null){
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId,
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
    },

    
    //Show the error toast when any error occured
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        
        if(errors) {
            errors.forEach( function (error) {
                //top-level error. There can be only one
                if(error.message) {
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                
                //page-level errors (validation rules, etc)
                if(error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                
                if(error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for(var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList) {	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    }
                }
            });
        }
    }
})