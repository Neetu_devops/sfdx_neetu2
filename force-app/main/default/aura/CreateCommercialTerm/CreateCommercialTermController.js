({
    doInit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        helper.fetchAssetsInDeal(component, event, helper); 
    },
    
    
    chooseRecordType : function(component, event, helper) { 
        component.set('v.recordTypeSection', false);
        component.set('v.showSpinner', true);
        $A.util.addClass(component.find("recordModal"), "slds-modal_medium");
        helper.fetchPageLayout(component, event);
    },
    
    
    handleLoad : function(component, event, helper) {
        setTimeout(function(){ 
            var lookupSection = document.getElementById('msLookup');
            if(lookupSection != undefined && lookupSection != '' && lookupSection != null){
                lookupSection.scrollIntoView(true);
            }
            
        }, 100);
    },
    
    
    handleSubmit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        
        var recordName = event.getParam("fields").Name;
        if(recordName == undefined || recordName == '' ||recordName == null) {
            event.preventDefault();
            
            var errorDiv = document.getElementById("error");
            if(errorDiv != undefined && errorDiv != '' && errorDiv != null){
                errorDiv.style.display = 'block';
                errorDiv.innerHTML = 'Name is required to create ' +  component.get("v.relatedListLabel");
                errorDiv.scrollIntoView(true);
            }
            component.set('v.showSpinner', false);
        }
        else{
            var errorDiv = document.getElementById("error");
            if(errorDiv != undefined && errorDiv != '' && errorDiv != null){
                errorDiv.style.display = 'none';
            }
        }

        component.set("v.recordName", recordName);
    },
    
    
    handleError : function(component, event, helper) { 
        component.set('v.showSpinner', false);
        var errorDiv = document.getElementById("errorMsg");
        if(errorDiv != undefined && errorDiv != '' && errorDiv != null){
            errorDiv.style.display = 'block';
            errorDiv.scrollIntoView(true);
        }
    },
    
    
    handleSuccess : function(component, event, helper) {
        var errorDiv = document.getElementById("errorMsg");
        if(errorDiv != undefined && errorDiv != '' && errorDiv != null){
            errorDiv.style.display = 'none';
        }
        
        if(component.get("v.actionType") != 'Edit') {
            component.set("v.selectedRecordId", event.getParams().response.id);
        }
        
        helper.saveRespectiveRecords(component, event);
    },
    
    
    closeComponent : function(component, event, helper) {
        component.set("v.createNewRecord", false);
    },
    
    removeMSN : function(component, event, helper) {
        var name = event.getParam("item").name;
        var item = event.getParam("index");
        var items = component.get('v.items');

        items.splice(item, 1);
        component.set('v.items', items);
    },
})