({
    doInit : function(component, event, helper) {
        var action = component.get("c.getScenarioInput");
        action.setParams({ 
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.scenarioInputRecord",response.getReturnValue());   
            }else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    saveRecord : function(component, event, helper) {
        var action = component.get("c.updateScenarioInput");
        action.setParams({ 
            scenarioInput: component.get("v.scenarioInputRecord")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var toastParams = {
                    title: "Success",
                    message: "Record updated successfully.", // Default error message
                    type: "Success"
                };
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }else if (state === "ERROR") {
                var errors = response.getError();
                helper.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    }
})