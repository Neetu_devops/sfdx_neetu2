({
    getTooltipData: function (component, event, helper){
        var forecast = component.get("v.forecastData");
        var startDate = component.get("v.startDate");
        var assemblyName = component.get("v.assemblyDisplayName");
        
        if(assemblyName.includes('Engine')){
            component.set("v.isEngineAssembly",true);
        } else if(assemblyName.includes('LG')){
            component.set("v.isLGAssembly",true);
        }
        
        var displayName = component.get("v.assemblyDisplayName").replace(/(_)/g," ");
        if(displayName == '4C 6Y'){
            displayName = '4C/6Y';
        }else if(displayName == '8C 12Y'){
            displayName = '8C/12Y'
        } 
        component.set("v.displayName",displayName);
        
        forecast.lstComponentWrapper.forEach(function(componentObj) {
            if(componentObj.eventClass == 'hasevent' && componentObj.startDate == startDate){
                componentObj.lstMSW.forEach(function(componentObj2){
                    if(componentObj2.assemblyType == assemblyName && (componentObj2.ms != null && componentObj2.ms != undefined)){
                        if(componentObj2.ms.Fx_Component_Output__r.Assembly_Display_Name__c  == displayName){
                            component.set("v.serialNumber",componentObj2.ms.Fx_Component_Output__r.Assembly__r.Serial_Number__c);
                        }
                    }
                });                         
            }
        });
        
        forecast.lstAssemblyWrapper.forEach(function(AssemblyWrapper){
            if(AssemblyWrapper.assemblyName == displayName){
                if(AssemblyWrapper.AVGFC != null && AssemblyWrapper.AVGFC != undefined){
                    component.set("v.AVGFC",AssemblyWrapper.AVGFC.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));    
                }
                if(AssemblyWrapper.AVGFH != null && AssemblyWrapper.AVGFH != undefined){
                    component.set("v.AVGFH",AssemblyWrapper.AVGFH.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));   
                }
                component.set("v.eventType",AssemblyWrapper.eventType);
                component.set("v.lastEventDate",AssemblyWrapper.lastEventDate);
            }
        });
        
        forecast.lstComponentWrapper.forEach(function(ComponentWrapper){
            if(ComponentWrapper.startDate == startDate){
                ComponentWrapper.lstMSW.forEach(function(ComponentWrapper2){
                    if(ComponentWrapper2.assemblyType == assemblyName && (ComponentWrapper2.ms != null && ComponentWrapper2.ms != undefined)){
                        console.log('ComponentWrapper2:', ComponentWrapper2.ms);
                        if(ComponentWrapper2.ms.Fx_Component_Output__r.First_Event__c){
                            if(ComponentWrapper2.ms.RateBasis_Label__c=='MSO'){
                                if(ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Months__c != null && ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Months__c != undefined){
                            component.set("v.interval_1_Months",ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Months__c.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        } 
                            } 
                            else if(ComponentWrapper2.ms.RateBasis_Label__c=='CSO'){
                               if(ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c != null && ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c != undefined){
                            component.set("v.interval_1_Cycles",ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        }
 
                            }
                                else if(ComponentWrapper2.ms.RateBasis_Label__c=='TSO'){
                                  if(ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Hours__c != null && ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Hours__c != undefined){
                            component.set("v.interval_1_Hours",ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_1_Hours__c.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        }   
                                }
                        }
                        else{
                            if(ComponentWrapper2.ms.RateBasis_Label__c=='MSO'){
                         
                                                if(ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Months__c != null && ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Months__c != undefined){
                            component.set("v.interval_2_Months",ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Months__c.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        }       
                            } 
                            else if(ComponentWrapper2.ms.RateBasis_Label__c=='CSO'){
                               
                        if(ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c != null && ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c != undefined){
                            component.set("v.interval_2_Cycles",ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")); 
                        } 
                            }
                                else if(ComponentWrapper2.ms.RateBasis_Label__c=='TSO'){
                                 if(ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c != null && ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c != undefined){
                            component.set("v.interval_2_Hours",ComponentWrapper2.ms.Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        }     
                                }
                        }                                
                    } 
                });
            }
        });
    },
    
    handleErrors : function(errors, type) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: type == undefined ? "error": type,
            duration: '7000'
        };
        if(typeof errors === 'object') {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });             
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){ 
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );                         
                        });  
                    };
                }
            });
        }
        else if(typeof errors === "string" && errors.length != 0){
            toastParams.message = errors;
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    },
})