({
    claimMR: function (component, event, helper) {
        component.set("v.availableAmount", 0);
        component.set("v.claimDate", undefined);

        if (component.get("v.selectedRecord") != undefined) {
            component.set("v.showSpinner", true);
            helper.findMRClaim(component);
        }
    },


    reselecthighlightRow: function (component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            var rowId = params.rowId;
            component.set("v.clickedRowId", rowId)
            helper.updateRows(component);
        }
    },


    highlightRow: function (component, event, helper) {
        var rowId = event.currentTarget.dataset.id;
        var rows = component.get("v.mrClaimList");

        component.set("v.confirmDateChange", false);

        if (rows[rowId].isDefaultMRClaim && component.get("v.invoiceListSize") == 0) {
            component.set("v.clickedRowId", rowId);
            helper.updateRows(component);
        }
        else if (!rows[rowId].highlightRow && component.get("v.invoiceListSize") == 0
            && (rows[rowId].type == 'INDUCTION'
                || rows[rowId].type == 'RELEASE'
                || rows[rowId].type == 'INSTALLATION'
                || rows[rowId].type == 'REMOVAL')) {
            component.set("v.clickedRowId", rowId);
            component.set("v.showModal", true);
        }
    },


    updateHighlightedRow: function (component, event, helper) {
        if (component.get("v.confirmDateChange")) {
            helper.updateRows(component);
        }
    }
})