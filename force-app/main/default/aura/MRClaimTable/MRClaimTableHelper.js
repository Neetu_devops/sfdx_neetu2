({
    findMRClaim: function (component) {
        var action = component.get("c.findMRClaimAvailable");
        action.setParams({
            'mrInfo': component.get("v.selectedRecord"),
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var result = response.getReturnValue();

                let selectedMRShare = component.get("v.selectedMRShare");
                if (selectedMRShare != undefined && selectedMRShare != '') {
                    result = this.updateAvailableAmount(selectedMRShare, result);
                }

                var flag = false;
                for (var i = 0; i < result.length; i++) {
                    if (result[i].highlightRow) {
                        flag = true;

                        component.set("v.availableAmount", result[i].available);
                        component.set("v.claimDate", result[i].mrDate);
                        component.set("v.clickedRowId", i);
                        break;
                    }
                }

                if (flag) {
                    component.set("v.generateInvoice", false);
                }
                else {
                    component.set("v.generateInvoice", true);
                }

                component.set("v.mrClaimList", result);
                component.set("v.mrClaimListSize", result.length);
            }
            else {
                var errors = response.getError();

                if (errors) {
                    this.handleErrors(errors);
                }
            }

            component.set("v.showSpinner", false);
        });

        $A.enqueueAction(action);
    },


    updateRows: function (component) {
        var rowIndex = component.get("v.clickedRowId");
        var rows = component.get("v.mrClaimList");

        for (var i = 0; i < rows.length; i++) {
            if (i == rowIndex) {
                rows[i].highlightRow = true;

                var oldValue = component.get("v.availableAmount");
                component.set("v.availableAmount", rows[i].available);

                if (oldValue == rows[i].available) {
                    component.set("v.updateAmount", true);
                }

                component.set("v.claimDate", rows[i].mrDate);
                component.set("v.claimRefType", rows[i].type);
            }
            else {
                rows[i].highlightRow = false;
            }
        }

        component.set("v.mrClaimList", rows);
    },


    updateAvailableAmount: function (selectedShareRecord, result) {
        let availableAmount;

        if (selectedShareRecord != undefined) {
            if (selectedShareRecord.Max_Cap_in_Cash__c != null && selectedShareRecord.Max_Cap_in_Cash__c != 0
                && selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != null && selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != 0) {
                if (selectedShareRecord.Max_Cap_in_Cash__c < selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c) {
                    availableAmount = selectedShareRecord.Max_Cap_in_Cash__c;
                }
                else {
                    availableAmount = selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c;
                }
            }
            else {
                if (selectedShareRecord.Max_Cap_in_Cash__c != null
                    && selectedShareRecord.Max_Cap_in_Cash__c != 0) {
                    availableAmount = selectedShareRecord.Max_Cap_in_Cash__c;
                }
                else if (selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != null
                    && selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c != 0) {
                    availableAmount = selectedShareRecord.Y_Hidden_Max_Cap_PerAmt__c;
                }
            }
        }

        for (let key in result) {
            if (result[key].clickableRow) {
                result[key].available = availableAmount;
            }
            if (result[key].type == 'STARTING BALANCE') {
                break;
            }
        }
        return result;
    },


    //Show the error toast when any error occured
    handleErrors: function (errors, type) {
        _jsLibrary.handleErrors(errors, type); // Calling the JS Library function to handle the error
    }
})