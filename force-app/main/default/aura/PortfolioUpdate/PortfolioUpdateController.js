({
    doInit: function (component, event, helper) {
        //component.set("v.recordId", 'a3T4T0000008jSkUAI'); //a3T4T0000008lfiUAA  a3T4T0000008jSkUAI
        helper.initialize(component);

        
    },

    closeModal: function(component, event, helper){
        helper.closeModalHelper();
    },
 
    hideSpinner : function(component, event, helper) {
        var spinLoaderCount = component.get("v.spinLoaderCount");
        var data = component.get("v.assetInDealData");
        if (data != undefined || data != null) {
            //console.log("hideSpinner data.length "+data.length + ' spinLoaderCount ' + spinLoaderCount); 
            if (spinLoaderCount < (data.length - 1)) {
                spinLoaderCount++;
                component.set("v.spinLoaderCount", spinLoaderCount);
            } else {
                component.set("v.isLoading", false);
            }
        }
        else {
            component.set("v.isLoading", false);
        }


        console.log("hideSpinner");
    },

    onAllChange: function (component, event, helper) {
        var isAll = component.get("v.isAll");
        console.log('onAllChange isAll: ' + isAll);
        if ($A.util.isEmpty(isAll) || $A.util.isUndefined(isAll))
            return;

        var AIDData = component.get("v.assetInDealData");
        console.log("onAllChange values " + AIDData);
        if ($A.util.isEmpty(AIDData) || $A.util.isUndefined(AIDData) || AIDData == null)
            return;

        //console.log("onAllChange AIDData " +JSON.stringify(AIDData));
        for (var i = 0; i < AIDData.length; i++) {
            AIDData[i].isSelect = isAll;
        }
        component.set("v.assetInDealData", AIDData);
    },
    
    onCheckboxChange: function (component, event, helper) {
        var AIDData = component.get("v.assetInDealData");
        console.log("onAllChange values " + AIDData);
        if ($A.util.isEmpty(AIDData) || $A.util.isUndefined(AIDData) || AIDData == null)
            return;

        //console.log("onAllChange AIDData " +JSON.stringify(AIDData));
        var isAllSelected = true;
        for (var i = 0; i < AIDData.length; i++) {
            if (AIDData[i].isSelect == false)
                isAllSelected = false;
        }
        component.set("v.isAll", isAllSelected);
    },

    onSave: function (component, event, helper) {
        helper.updatePortfolio(component);
    },
    
})
