({
    initialize: function(component) {
        component.set("v.isLoading", true);
        this.checkDealValidation(component);
    },
    
    checkDealValidation: function (component) {
        var recordIdL = component.get("v.recordId");
        console.log('checkDealValidation recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.checkValidDealForUpdatePort');
        action.setParams({
            dealId: recordIdL
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("checkDealValidation : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("checkDealValidation values " + allValues);
                if (allValues != null && allValues) {
                    component.set("v.isLoading", false);
                    //self.showErrorMsg(component, allValues);
                    component.set("v.showError", true);
                    component.set("v.errorMsg", allValues);
                }
                else {
                    self.getDealTypeForRecord(component);
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("checkDealValidation errors ", errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("checkDealValidation unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getDealTypeForRecord: function (component) {
        var recordIdL = component.get("v.recordId");
        console.log('getDealTypeForRecord recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getAIDDealType');
        action.setParams({
            parentId: recordIdL
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getDealTypeForRecord : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getDealTypeForRecord values " + allValues);
                if (allValues != null) {
                    component.set("v.dealType", allValues);
                    if(allValues == 'Sale' || allValues == 'SLB') {
                        component.set("v.showLease", true);
                        component.set("v.showAsset", true);
                    }
                    else if(allValues == 'Purchase') {
                        component.set("v.showLease", false);
                        component.set("v.showAsset", true);
                    }
                    else if(allValues == 'Lease' || allValues == 'Lease Extension') {
                        component.set("v.showLease", true);
                        component.set("v.showAsset", false);
                    }
                    var fs = component.get('v.fieldSet');
                    if ($A.util.isEmpty(fs) || $A.util.isUndefined(fs) || fs == '' || fs == null)
                        self.getFieldsetName(component);
                    else
                        self.getFields(component);
                }
                else {
                    component.set("v.isLoading", false);
                    self.showErrorMsg(component, "Deal type not found!");
                    this.closeModalHelper();
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getDealTypeForRecord errors " ,errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getDealTypeForRecord unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getFieldsetName: function (component) {
        var recordIdL = component.get("v.recordId");
        console.log('getFieldsetName recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getFieldSetBasedOnType');
        action.setParams({
            recordId: recordIdL
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getFieldsetName : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getFieldsetName values " + allValues);
                if (allValues != null) {
                    component.set("v.fieldSetBasedOnType", allValues);
                    console.log("getFields fieldSetBasedOnType " + component.get("v.fieldSetBasedOnType"));
                    self.getFields(component);
                }
                else {
                    component.set("v.isLoading", false);
                    self.showErrorMsg(component, "Incorrect configuration: cannot find field set");
                    this.closeModalHelper();
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getFieldsetName errors " , errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getFieldsetName unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getFields: function (component) {
        var self = this;
        var action = component.get('c.getColumns');
        action.setParams({
            recordId: component.get('v.recordId'),
            fieldSetName: component.get('v.fieldSet')
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getFields : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getFields values " + allValues);
                if (allValues != null) {
                    var resObj = JSON.parse(allValues);
                    console.log("getFields values " ,resObj);
                    component.set("v.selectedFields", resObj);

                    console.log("getFields allValues.length " + resObj.length);
                    self.getAIDs(component);
                }
                else {
                    component.set("v.isLoading", false);
                    var fieldSetDefault = component.get("v.fieldSetBasedOnType");
                    console.log("getFields fieldSetBasedOnType " + fieldSetDefault);
                    self.showErrorMsg(component, "Incorrect configuration: cannot find field set " + fieldSetDefault);
                    this.closeModalHelper();
                }
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("getFields errors " + errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getFields Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }

        });
        $A.enqueueAction(action);
    },

    getAIDs: function (component) {
        var recordIdL = component.get("v.recordId");
        console.log('getAIDs recordId: ' + recordIdL);
        var self = this;
        if ($A.util.isEmpty(recordIdL) || $A.util.isUndefined(recordIdL)) {
            self.showErrorMsg(component, 'Invalid record Id');
            return;
        }
        var action = component.get('c.getAIDList');
        action.setParams({
            recordId: recordIdL,
            fieldSetName: component.get('v.fieldSet')
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("getAIDs : state " + state);
            component.set("v.isLoading", false);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getAIDs values " ,allValues);
                if (allValues != null) {
                    var resObj = JSON.parse(allValues);
                    console.log("getAIDs values " +JSON.stringify(resObj));
                    if (allValues.length > 0) {
                        component.set("v.aidDummyId", resObj[0].aidId);
                    }
                    component.set("v.assetInDealData", resObj);
                }
                else {
                    component.set("v.assetInDealData", null);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.isLoading", false);
                console.log("getAIDs errors " , errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("getAIDs Unknown error");
                    self.showErrorMsg(component, "Error in fetching record");
                }
            }
           
        });
        $A.enqueueAction(action);
    },
  
    updatePortfolio: function(component) {
        var aidData = component.get("v.assetInDealData");
        console.log("updatePortfolio values " , aidData);
        if ($A.util.isEmpty(aidData) || $A.util.isUndefined(aidData) || aidData == null)
            return;
        //console.log("onCopyToSelected aidData " +JSON.stringify(aidData));
        
        let resultWrapper = [];
        let dealType = component.get("v.dealType");
        let isSelected = false;
        let cnt = 0;
        for (var i = 0; i < aidData.length; i++) {
            let wrapper = new Object();
            if (aidData[i].isSelect == true) {
                cnt++;
                wrapper.aidId = aidData[i].aidId;
                for(let j =0 ; j < aidData[i].fields.length; j++) {
                    if(aidData[i].fields[j].apiName == 'Name'){
                        wrapper.aidName = aidData[i].fields[j].value;
                    }
                }
                wrapper.status = 'Processing..';
                resultWrapper.push(wrapper);
                component.set('v.resultWrapper', resultWrapper);
                if(isSelected == false) {
                    component.set("v.isLoading",true);
                    component.set("v.showResult",true);
                }
                isSelected = true;
                this.sendRequestToServer(component, aidData[i].aidId, dealType);
                component.set('v.loadCount', cnt);
            }
        }
        
        if(isSelected == false) {
            this.showErrorMsg(component, 'Please select a record to proceed');
        }
        
    },

    sendRequestToServer: function(component, Id, type) {
        console.log('sendRequestToServer recordId: ' + Id + ', type:'+type);
        var self = this;
        var action = component.get('c.savePortfolio');
        action.setParams({
            recordTypeName: type,
            aid: Id
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log("sendRequestToServer : state " + state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("sendRequestToServer values " ,allValues);
                if (allValues != null) {
                    self.updateStatus(component,allValues);
                }
                else {
                    self.updateCount(component);
                    self.showErrorMsg(component, "Error in updating the record");
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                //component.set("v.isLoading", false);
                self.updateCount(component);
                console.log("sendRequestToServer errors " , errors);
                if (errors) {
                    self.handleErrors(errors);
                } else {
                    console.log("sendRequestToServer Unknown error");
                    self.showErrorMsg(component, "Error in updating the record");
                }
            }
        });
        $A.enqueueAction(action);
    },

    updateStatus: function(component, data) {
        let resultWrapper = component.get('v.resultWrapper');
        console.log('updateStatus data '+JSON.stringify(data));
        console.log('updateStatus data '+JSON.stringify(resultWrapper));
        for(let i =0; i < resultWrapper.length; i++) {
            if(resultWrapper[i].aidId == data.aidId) {
                resultWrapper[i] = data;
            }
        }
        component.set("v.resultWrapper",resultWrapper);
        this.updateCount(component);
    },

    updateCount: function(component) {
        let count = component.get('v.loadCount');
        console.log('updateCount count: '+count);
        count--;
        component.set('v.loadCount', count);
        if(count == 0) {
            component.set("v.isLoading", false);
        }
    },

    closeModalHelper: function(){
        $A.get("e.force:closeQuickAction").fire();
    },

    showErrorMsg: function (component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration: '5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

    //to handle errors
    handleErrors: function (errors) {
        this.closeModalHelper();
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error",
            type: "error"
        };
        if (errors) {
            errors.forEach(function (error) {
                //top-level error. There can be only one
                if (error.message) {
                    toastParams.message = error.message;
                    console.log('error ' + toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors) {
                    let arrErr = [];
                    error.pageErrors.forEach(function (pageError) {
                        toastParams.message = pageError.message;
                        console.log('error ' + toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });
                }
                if (error.fieldErrors) {
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach(function (errorList) {
                            toastParams.message = errorList.message;
                            console.log('error ' + toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });
                    };
                }
            });
        }
    }
  
})
