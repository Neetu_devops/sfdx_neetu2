({
    doInit : function(component, event, helper) {
        var currentYr = $A.localizationService.formatDate(new Date(), "YYYY");
        console.log('doInit currentYr '+currentYr);
        //component.set("v.currentYear", currentYr);
       // component.set("v.isLoading", true);
   //     helper.getLLPCatalogYear(component);
      //  helper.getLLPCatalogs(component, currentYr);
        
    }, 
    
    refreshData : function(component, event, helper) {
        component.set("v.isLoading", true);
        console.log('refreshData '+component.get("v.LLPTemplateId"));
        //var currentYr = $A.localizationService.formatDate(new Date(), "YYYY");
        var currentYr = component.get("v.currentYear");
        console.log('refreshData currentYr '+currentYr);
        
        var llpTemplId = component.get("v.LLPTemplateId");
        console.log("refreshData llpTemplId: "+llpTemplId);
        
        if(!$A.util.isEmpty(llpTemplId) && !$A.util.isUndefined(llpTemplId) && 
           !$A.util.isEmpty(currentYr) && !$A.util.isUndefined(currentYr)){
            
       		// helper.getLLPCatalogYear(component);
        	helper.getLLPCatalogs(component, currentYr);
        }
        
    }, 
    
    onCatalogYearChange: function(component, event, helper) {
       // var value = event.getSource().get('v.value'); 
        var value = component.get("v.currentYear");
        
        console.log("onCatalogYearChange value: "+value);
        var llpTemplId = component.get("v.LLPTemplateId");
        console.log("onCatalogYearChange llpTemplId: "+llpTemplId);
        if(!$A.util.isEmpty(llpTemplId) && !$A.util.isUndefined(llpTemplId) && 
           !$A.util.isEmpty(value) && !$A.util.isUndefined(value)){
            helper.resetEdit(component);
            helper.disableTableDrag(component, event, helper);
            helper.getLLPCatalogs(component, value);
        }
        
    },
    
    onDefaultChange : function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log("onDefaultChange "+recordId);
        var isEdit = component.get("v.isLLPCEdit");
        if(isEdit == false)
            return;
        var catalogData = component.get("v.LLPCatalogData");
        console.log("onDefaultChange "+recordId);
        
        if(!$A.util.isEmpty(recordId) && !$A.util.isUndefined(recordId) && recordId.includes('dynamicField'))  {
            var res = recordId.split("dynamicField_");
            var selectedIndex = res[1];
            console.log('split selectedIndex: '+selectedIndex);
            var newLLPCatalogData = component.get("v.newLLPCatalogData");
            console.log("onDefaultChange newLLPCatalogData "+ JSON.stringify(newLLPCatalogData));
            
            if(!$A.util.isEmpty(newLLPCatalogData) && !$A.util.isUndefined(newLLPCatalogData)) {
                for(var j =0; j< newLLPCatalogData.length; j++) {
                    if(!$A.util.isEmpty(newLLPCatalogData[j]) && !$A.util.isUndefined(newLLPCatalogData[j]))  {
                        if(!$A.util.isEmpty(newLLPCatalogData[j].IsDefaultPrice) && !$A.util.isUndefined(newLLPCatalogData[j].IsDefaultPrice)) {
                            console.log("onDefaultChange newLLPCatalogData IsDefaultPrice: "+ JSON.stringify(newLLPCatalogData[j].IsDefaultPrice));
                            console.log("onDefaultChange newLLPCatalogData IsDefaultPrice: j "+ j + ' , '+selectedIndex);
                            if(selectedIndex == j) {
                                newLLPCatalogData[j].IsDefaultPrice == true;
                            }
                            else {
                                newLLPCatalogData[j].IsDefaultPrice == false;
                            }
                        }
                    }
                }
                 component.set("v.newLLPCatalogData", newLLPCatalogData);
            }
            if(!$A.util.isEmpty(catalogData) && !$A.util.isUndefined(catalogData)) {
                for(var i= 0; i < catalogData.length; i++) {
                    catalogData[i].IsDefaultPrice = false;
                }
                component.set("v.LLPCatalogData", catalogData);
            }
        }
        else{
            if(!$A.util.isEmpty(catalogData) && !$A.util.isUndefined(catalogData)) {
                for(var i= 0; i < catalogData.length; i++) {
                    if(catalogData[i].Id == recordId)
                        catalogData[i].IsDefaultPrice = true;
                    else
                        catalogData[i].IsDefaultPrice = false;
                }
                component.set("v.LLPCatalogData", catalogData);
            }
            var newLLPCatalogData = component.get("v.newLLPCatalogData");
            if(!$A.util.isEmpty(newLLPCatalogData) && !$A.util.isUndefined(newLLPCatalogData)) {
                for(var j =0; j< newLLPCatalogData.length; j++) {
                    if(!$A.util.isEmpty(newLLPCatalogData[j]) && !$A.util.isUndefined(newLLPCatalogData[j]))  {
                        if(!$A.util.isEmpty(newLLPCatalogData[j].IsDefaultPrice) && !$A.util.isUndefined(newLLPCatalogData[j].IsDefaultPrice)) {
                            newLLPCatalogData[j].IsDefaultPrice == false;
                        }
                    }
                }
                component.set("v.newLLPCatalogData", newLLPCatalogData); 
            }
        }
    },
    
    addRow: function(component, event, helper) {
        helper.addDynamicRow(component);
    },
    
    editRow : function(component, event, helper) {
        var isEdit = component.get("v.isLLPCEdit");
        console.log("editRow "+isEdit);
        
        helper.resetEdit(component);
        
        component.set("v.isLLPCEdit", true);
        helper.enableTableDrag(component, event, helper);
    },
    
    saveRow : function(component, event, helper) {
        var isEdit = component.get("v.isLLPCEdit");
        console.log("saveRow "+isEdit);
        
        //component.set("v.isEdit", false);
        
        helper.disableTableDrag(component, event, helper);
        
      //  helper.saveSorting(component);
        helper.updateLLPCdata(component);
        
        
    },
    
    cancelRow : function(component, event, helper) {
        var isEdit = component.get("v.isLLPCEdit");
        console.log("cancelRow "+isEdit);
        helper.resetEdit(component);
        helper.disableTableDrag(component, event, helper);
        //var currentYr = component.set("v.currentYear", currentYr);
        var currentYr = component.get("v.currentYear");
        component.set("v.isLoading", true);
        helper.getLLPCatalogs(component, currentYr);
    },
    
    deleteRow : function(component, event, helper) {
        component.set("v.deleteLLPCRecordId", "");
        component.set("v.deleteLLPCRowIndex", "");
        
        var recordId = event.currentTarget.id;
        console.log("deleteRow "+recordId);
        var target = event.currentTarget;
        
        var rowIndex = target.parentNode.parentNode.rowIndex;
        console.log("Row No : " + rowIndex + ' target:'+target);
        
        if(recordId.includes("dateDivIndex")) {
            var dynamicRowTotalCount = component.get("v.llpcDynamicRowTotalCount");
            console.log("Deleting dynamically created row "+dynamicRowTotalCount);
            if(dynamicRowTotalCount == 1) {
                component.set("v.llpcRowIndex", "0");
                component.set("v.body", []);
                component.set("v.newLLPCatalogData", []);
                component.set("v.llpcDynamicRowTotalCount", 0);
            }
            else{
                document.getElementById("llpctblLocations").deleteRow(rowIndex);
            }
            dynamicRowTotalCount = dynamicRowTotalCount - 1;
            console.log("Deleting dynamically created row after "+dynamicRowTotalCount);
            component.set("v.llpcDynamicRowTotalCount", dynamicRowTotalCount);
        }
        else{
            console.log("Deleting data in db");
            component.set("v.deleteLLPCRecordId", recordId);
            component.set("v.deleteLLPCRowIndex", rowIndex);
            component.set("v.isllpcOpen", true);
            //helper.deleteLLPData(component, recordId, rowIndex);
            
        }
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isllpcOpen", false);
    },
    
    showRecord : function(component, event, helper) {
        var recordId = event.currentTarget.id;
        console.log("showRecord " + recordId);
        window.open('/' + recordId);  
    },
    
    confirmDelete: function(component, event, helper) {
        component.set("v.isllpcOpen", false);
        console.log("Deleting data in db");
        var recordId = component.get("v.deleteLLPCRecordId");
        var rowIndex = component.get("v.deleteLLPCRowIndex");
        component.set("v.isLoading", true);
        helper.deleteLLPCData(component, recordId, rowIndex);
    },
    
    
    disableDrag : function(component, event, helper) {
        //console.log("disableDrag");
        helper.disableTableDrag(component, event, helper);
    },
    
    enableDrag: function(component, event, helper) {
        //console.log("enableDrag");
        helper.enableTableDrag(component, event, helper);
    },
    
    loadJs : function(component, event, helper) {
        //console.log('loaded');
       /* jQuery("document").ready(function(){            
            //  console.log('loadJs');
            var $sortableTable  = $("#llpctblLocations tbody");
            $sortableTable.sortable({
                items: 'tr:not(tr:first-child)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                    
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                    $(this).find("tr").each(function (index) {
                        if (index > 0) {
                            $(this).find("td").eq(6).html(index);
                        }
                    });
                    //                    $sortableTable.sortable( "disable" );   
                }
            }); 
            $sortableTable.sortable( "disable" );
            var maxWidth = 0;
            $('#tblLocations td:nth-child(5)').each(function(){
                if(maxWidth < $(this).width())
                    maxWidth = $(this).width();
            });
            
            $('#tblLocations td:nth-child(5)').css('width',maxWidth);
            $('#tblLocations td:nth-child(4)').css('width',maxWidth);
            $('#tblLocations td:nth-child(3)').css('width',maxWidth);
            $('#tblLocations td:nth-child(2)').css('width',maxWidth);
            $('#tblLocations td:nth-child(1)').css('width',maxWidth);
            
        }); */
    }, 
    
    
})