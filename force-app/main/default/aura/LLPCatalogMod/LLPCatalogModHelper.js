({
    getLLPCatalogs: function(component, currentYr) {
       // console.log("getLLPCatalogs year " +component.get("v.currentYear") + " Id: "+component.get("v.LLPTemplateId"));
        var self = this;
        var action = component.get("c.getLLPCatalogData");
        action.setParams({
            llpTemplateId : component.get("v.LLPTemplateId"),
            currentYear : component.get("v.currentYear")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
          //  console.log('getLLPCatalogs state: '+ state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();    
           //     console.log('getLLPCatalogs allValues '+ allValues);
             //   if(!$A.util.isEmpty(allValues) && !$A.util.isUndefined(allValues)) 
               // 	console.log('getLLPCatalogs allValues '+JSON.stringify(allValues));
                component.set("v.LLPCatalogData", allValues);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("getLLPCatalogs errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("getLLPCatalogs: Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    
    
    updateLLPCdata : function(component) {
        var data = component.get("v.LLPCatalogData");
        var self = this;
     //   console.log("updateLLPCdata data: "+data);
        
        var newData = component.get("v.newLLPCatalogData");
        var newDataJsonStr = null;
      //  console.log('updateLLPCdata newData: ' +newData);
        
        if(!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData)) {
         //   console.log('updateLLPCdata data:dtr '+ JSON.stringify(newData));  
            var nonNullData = [];
            var j = 0;
            for(var i = 0; i < newData.length; i++) {
                if(!$A.util.isEmpty(newData[i]) && !$A.util.isUndefined(newData[i])) {
                    nonNullData[j] = newData[i];
                	j++;
                }
            }
        //    console.log('updateLLPCdata data:nonNullData '+ JSON.stringify(nonNullData));  
            newDataJsonStr = JSON.stringify(nonNullData);
        }
        if((!$A.util.isEmpty(data) && !$A.util.isUndefined(data)) || 
        (!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData))){
            component.set("v.isLoading", true);
            console.log("updateLLPCdata data:str "+JSON.stringify(data));
            var dataJsonStr = JSON.stringify(data);
            var yr = component.get("v.currentYear");
            var action = component.get("c.updateLLDCatalogData");
            console.log('updateLLPCdata ctYear: '+yr);
            action.setParams({
                dataStr : dataJsonStr,
                newDataStr : newDataJsonStr,
                LLPTemplateId : component.get("v.LLPTemplateId"),
                ctYear : yr
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('updateLLPCdata state: '+ state);
                if (state === "SUCCESS") {
                    var allValues = response.getReturnValue();    
                    self.getLLPCatalogs(component, yr);
                    self.resetEdit(component);
                    
                    //refresh parent data
                    var evt = component.getEvent("RefreshEvent");
                    evt.setParams({ "isRefresh": true});
                    evt.fire();
                    
                }
                else if (state === "ERROR") {
                    component.set("v.isLoading", false);
                    var errors = response.getError();
                    console.log("errors "+errors);
                    component.set("v.isLLPCEdit", true);
                    if (errors) {
                        console.log("updateLLPCdata errors "+JSON.stringify(errors));
                        console.log("Error message: err " + JSON.stringify(errors[0]));
                        if (errors[0] && errors[0].pageErrors) {
                            console.log("Error message: " + errors[0].pageErrors[0].message);
                            self.showErrorMsg(component, errors[0].pageErrors[0].message);
                        }
                        else if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            if(errors[0].message.includes('Duplicate part number')) {
                                self.showErrorMsg(component, 'Duplicate Part number is not allowed!');
                                this.resetEdit(component);
                            }
                            else
                                self.showErrorMsg(component, errors[0].message);
                        }
                        else {
                            console.log("Unknown error");
                            self.showErrorMsg(component, "Error in creating record "+JSON.stringify(errors));
                        }
                    } else {
                        console.log("Unknown error");
                        self.showErrorMsg(component, "Error in creating record ");
                    }
                }
            });
            $A.enqueueAction(action);
        } 
    },
    
    resetEdit : function(component) {
        component.set("v.llpcRowIndex", "0");
        component.set("v.isLLPCEdit", false);
        component.set("v.body", []);
        component.set("v.newLLPCatalogData", []);
        component.set("v.llpcDynamicRowTotalCount", 0);
    },
    
    deleteLLPCData : function(component, llpRecord, rowIndex) {
        var action = component.get("c.deleteLLPCatalog");
        var self = this;
        action.setParams({
            llpcId : llpRecord
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
       //     console.log('deleteLLPCData state: '+ state);
            if (state === "SUCCESS") {
             //   console.log('rowIndex '+rowIndex + 'dele '+document.getElementById("llpctblLocations"));
                self.resetEdit(component);
                self.getLLPCatalogs(component);
                document.getElementById("llpctblLocations").deleteRow(rowIndex);
            }
            else if (state === "ERROR") {
                component.set("v.isLoading", false);
                var errors = response.getError();
                console.log("deleteLLPCData errors "+JSON.stringify(errors));
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        self.showErrorMsg(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    self.showErrorMsg(component, "Error in creating record");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    
    saveSorting : function(component) {
        var oTable = document.getElementById('llpctblLocations');
        //gets rows of table
        var rowLength = oTable.rows.length;
        var tempData = component.get("v.LLPCatalogData");
        var newData = component.get("v.newLLPCatalogData");
        
        //loops through rows    
        var index = 0;
        for (var i = 0; i < rowLength; i++){
            var rowId = oTable.rows.item(i).id;
          //  console.log("row id "+ rowId);
            if(!$A.util.isEmpty(rowId) && !$A.util.isUndefined(rowId))  {
                index = index + 1;
                
             //   console.log("row id "+ rowId + ' i:'+i +' index:' +index);
                if(rowId.includes('index_')){
                    var res = rowId.split("index_");
             //       console.log('split '+res[1]);
              //      console.log('split newData[res[1]]: '+newData[res[1]]);
                    if(!$A.util.isEmpty(newData[res[1]]) && !$A.util.isUndefined(newData[res[1]])) 
                    	newData[res[1]].Sorting = index;
                }
                else if(!$A.util.isEmpty(tempData) && !$A.util.isUndefined(tempData)) {
                    for(var j =0; j< tempData.length; j++) {
                        //console.log("equals out "+tempData[j].Id+ ' rowId: '+rowId);
                        if(tempData[j].Id == rowId) {
                            //console.log("equals "+tempData[j].Id+ ' '+rowId);
                            tempData[j].Sorting = index;
                        }
                    }
                }
            }
        }
        if(!$A.util.isEmpty(tempData) && !$A.util.isUndefined(tempData))  {
         //   console.log("after sorting tempData: "+JSON.stringify(tempData));
            component.set("v.LLPCatalogData", tempData);
        }
        
        if(!$A.util.isEmpty(newData) && !$A.util.isUndefined(newData))  {
           // console.log("after sorting newData: "+JSON.stringify(newData));
            component.set("v.newLLPCatalogData", newData);    
        }
            
    },
    
    addDynamicRow : function(component){
        var index = component.get("v.llpcRowIndex");
        var self = this;
        var resLoc =   $A.get('$Resource.MRCompsIcons') + '/MRCompsIcons/LLP.svg';
        console.log('addDynamicRow resLoc: '+resLoc);
        // Array of components to create
        console.log('addDynamicRow '+ index);
        
        var newComponents = [];
        newComponents.push(["aura:html", { //1
            "tag": "tr",
            "HTMLAttributes": {
                "id": "index_"+index
            }
        }]);
        
        newComponents.push(["aura:html", { //2
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", { //3
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle mouseStyle",
                "id" : "dateDivIndex_"+index,
                "onclick" : component.getReference("c.deleteRow")
            }
        }]);
        newComponents.push(["lightning:icon", {  //7
            				"iconName" : "utility:clear",
            				"size" :"xx-small"
        					}
                           ]);
        /*newComponents.push(["aura:html", { //4
            "tag": "img",
            "HTMLAttributes": {
                "class": "imgStyle",
                "src": resLoc
            }
        }]); */
        
        
        //2nd column
        newComponents.push(["aura:html", { //5
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //6
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //7
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].PartNumber")
        					}
                           ]);
        
        //3rd column
        newComponents.push(["aura:html", {  //8
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //9
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputCurrency", {  //10
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].CatalogPrice"),
            				"format" : "$#,###.00"
        					}
                           ]);
        //4th column
        newComponents.push(["aura:html", {  //14
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //15
            "tag": "div",
            "HTMLAttributes": {
                "class": "tdTextStyle",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag")
            }
        }]);
        newComponents.push(["ui:inputText", {  //16
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].LeadTimeDays")
        					}
                           ]);
        
        //5th column
        newComponents.push(["aura:html", {  //11
            "tag": "td"
        }]);
        
        newComponents.push(["aura:html", {  //12
            "tag": "div",
            "HTMLAttributes": {
                "id": "dynamicField_"+index,
                "class": "tdTextStyle",
                "style" : "text-align:center",
                "onmouseover" : component.getReference("c.disableDrag"),
                "onmouseout" : component.getReference("c.enableDrag"),
                "onclick": component.getReference("c.onDefaultChange")
            }
        }]);
        newComponents.push(["ui:inputCheckbox", {  //13
            				"value" : component.getReference("v.newLLPCatalogData["+index+"].IsDefaultPrice")
        					}
                           ]);
        
        $A.createComponents(newComponents,
                            function (components, status, errorMessage) {
                                if (status === "SUCCESS") {
                                    var pageBody = component.get("v.body");
                                    var tr = components[0];
                                    var trBody = tr.get("v.body");
                                    
                                    for (var i = 1; i < components.length; i += 3) {                                           
                                        var tdDt = components[i];
                                        var tdDtBody = tdDt.get("v.body");
                                        var divDt = components[i+1];
                                        var divDtBody = divDt.get("v.body");
                                        var ip = components[i+2];
                                        divDtBody.push(ip);
                                        divDt.set("v.body", divDtBody);
                                        tdDtBody.push(divDt);
                                        tdDt.set("v.body", tdDtBody);
                                        trBody.push(tdDt);
                                    }
                                    tr.set("v.body", trBody);
                                    pageBody.push(tr);
                                    component.set("v.body", pageBody);
                                    index++;
                                    component.set("v.llpcRowIndex", index);
                                    
                                    var dyCount = component.get("v.llpcDynamicRowTotalCount");
                                    dyCount++;
                                    component.set("v.llpcDynamicRowTotalCount" , dyCount);
                                    
                                    console.log("Component has been added ");
                                }
                                else // Report any error
                                {
                                    console.log("Error", "Failed to create list components.");
                                }
                            }
                           );
        
    },
        
    disableTableDrag : function(component, event, helper) {
       // console.log("disableDrag");
      /*  jQuery("document").ready(function(){
         //   console.log("disableDrag inside");
            var $sortableTable  = $("#llpctblLocations tbody");
            $sortableTable.sortable( "disable" );
        }); */
    },
    
    enableTableDrag: function(component, event, helper) {
        //console.log("enableDrag");
      /*  jQuery("document").ready(function(){
          //  console.log("enableDrag inside");
            var $sortableTable  = $("#llpctblLocations tbody");
            $sortableTable.sortable( "enable" );
        }); */
    },
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
})