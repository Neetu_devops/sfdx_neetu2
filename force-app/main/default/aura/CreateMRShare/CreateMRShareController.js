({
    doInit : function(component, event, helper) {           
        var modal =  component.find("mrShareModal");
        var backdrp = component.find("backdrop");
        var mrId = component.get("v.recordId");
        var lstAddedMRShare = [];
        
        helper.showSpinner(component, event, helper);
        $A.util.addClass(modal, 'slds-fade-in-open');            
        $A.util.addClass(backdrp, 'slds-backdrop');
        $A.util.addClass(backdrp, 'slds-backdrop_open');
                
        if(mrId == undefined) {
            var pageRef = component.get("v.pageReference");
            var state = pageRef.state; // state holds any query params
            var base64Context = state.inContextOfRef;
            //console.log('base64Context'+base64Context);
            if(base64Context != undefined){
                // For some reason, the string starts with "1.", if somebody knows why,
                // this solution could be better generalized.
                if (base64Context.startsWith("1\.")) {
                    base64Context = base64Context.substring(2);
                }
                var addressableContext = JSON.parse(window.atob(base64Context));
                mrId=addressableContext.attributes.recordId;
                component.set("v.mrId", mrId);
            } 
        }
        else {
            mrId = undefined;
        }
                        
        var action = component.get("c.fetchAssemblyType");
        action.setParams({
            "assemblyMRId": mrId,
            "mrShareId": component.get("v.recordId"),
            'lstAddedMRShare_p': lstAddedMRShare
        });
        action.setCallback(this, function(response){
            helper.hideSpinner(component, event, helper);
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var res = response.getReturnValue();
                
                if(res !== undefined){
                    component.set("v.MRShareWrapper", res);
                    component.set("v.assemblyType", res.lstAssemblyType);
                    
                    if( component.get("v.recordId") != undefined){
                        component.set("v.mrId", res.mrShareRecord.Assembly_MR_Info__c);
                    }
                }
                
                helper.fillMRShareList(component, event, helper);
            }
            else{
                var error = response.getError();
                $A.get("e.force:closeQuickAction").fire();
                helper.handleErrors(error);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    SaveRecord: function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        var action = component.get("c.saveMRShare");
        var lstMR = component.get("v.listMRShare");
        var allVaild =true;
        
        for(var i =0 ;i< lstMR.length; i++) {
            var mrShareWrapper = lstMR[i];
            console.log('****',mrShareWrapper.mrShareRecord.Assembly__c,mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c);
            if((mrShareWrapper.mrShareRecord.Assembly__c == undefined || mrShareWrapper.mrShareRecord.Assembly__c == '--None--' || mrShareWrapper.mrShareRecord.Assembly__c=='') ||
               (mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c == undefined || mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c == '--None--' || mrShareWrapper.mrShareRecord.Maintenance_Event_Name__c=='')){
                allVaild = false;
                break;
            }
            else {
                allVaild =true; 
            }
        }
        
        if(!allVaild){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning!",
                "type": "warning",
                "message": "Please fill the Assembly Name/Event Type field."
            });
            toastEvent.fire();
        }
        else{
            action.setParams({
                "stringMRShareWrapper": JSON.stringify(lstMR)
            });
            action.setCallback(this, function(response){
                helper.hideSpinner(component, event, helper);
                var state = response.getState();
                
                if (component.isValid() && state === "SUCCESS") {
                    console.log("SUCCESS");
                    var res = response.getReturnValue();
                    
                    if(res !== undefined && res != ''){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type": "error",
                            "message": res
                        });
                        toastEvent.fire();
                    }
                    else{
                        helper.navigateToURL(component, event, helper);
                    }
                }
                else{
                    var error = response.getError();
                    $A.get("e.force:closeQuickAction").fire();
                    helper.handleErrors(error);
                }
            });
            
            $A.enqueueAction(action);
        }
        
    },
    
    closeModal : function(component, event, helper) {             
        var modal =  component.find("mrShareModal");
        var backdrp = component.find("backdrop");
        
        $A.util.removeClass(modal, 'slds-fade-in-open');           
        $A.util.removeClass(backdrp, 'slds-backdrop');
        $A.util.removeClass(backdrp, 'slds-backdrop_open');
        helper.navigateToURL(component, event, helper);
    }
})