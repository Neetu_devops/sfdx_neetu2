({
    init: function (component, event, helper) {

        var action = component.get("c.getCommunityPrefix");
        var link = '/s/available-assets';
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                let prefix = result.communityPrefix;
                component.set("v.prefix", prefix);
                if (prefix != undefined && prefix != '') {
                    link = prefix + link;
                }
                console.log(link);
                component.set("v.availableAssetURL", link);
                //component.set("v.data", result.data);
            }

        });

        $A.enqueueAction(action);
    },

})
