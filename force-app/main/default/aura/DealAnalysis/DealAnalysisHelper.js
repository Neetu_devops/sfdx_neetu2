({
    getFinanicalPicklist : function(component){
        var self = this;
        if(component.get("v.showFinancial") == false) {
            return;
        }
        var action = component.get('c.getPickListOptions');
        action.setParams({
            sobj : 'Credit_Score__c',
            fieldname: 'Financial_Risk_Profile__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getFinanicalPicklist : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getFinanicalPicklist values " +allValues);
                if(allValues != null) {
                    var opts = [];
                    var financialRiskDefault = component.get("v.financialRiskDefault");
                    for(var i=0;i< allValues.length; i++) {
                        if(financialRiskDefault == allValues[i]) {
                            opts.push({"class": "optionClass", label: allValues[i], value: allValues[i], selected: true});
                        }
                        else {
                            opts.push({"class": "optionClass", label: allValues[i], value: allValues[i]});
                        }
                    }
                    
                    var riskPicklistId = component.find('financialRisk');
                    console.log("getFinanicalPicklist riskPicklistId: "+riskPicklistId);
                    if(riskPicklistId != undefined){
                        riskPicklistId.set("v.options", opts); 
                    }
                }
                else {
                    self.showErrorMsg(component, "Picklist not found!");
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getFinanicalPicklist errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getFinanicalPicklist unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
        
    getPicklistRecord : function(component){
        var self = this;
        var action = component.get('c.getPicklistForRecords');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getPicklistRecord : state "+state);
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("getPicklistRecord values " +JSON.stringify(allValues));
                if(allValues != null) {
                	component.set("v.recordTypePicklist",allValues);
                }
                else {
                    self.showErrorMsg(component, "Picklist not found!");
                }
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getPicklistRecord errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getPicklistRecord unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    
    getRecordType: function(component, aircraftType) {
        var recordPicklistMap = component.get("v.recordTypePicklist");
        if(!recordPicklistMap) {
            component.set("v.recordType","Aircraft");
            return;
        }
        console.log('getRecordType aircraftType ' + aircraftType);
        var keys = Object.keys(recordPicklistMap);
        var mapValues = recordPicklistMap[keys[0]];
        //console.log("mapValues : " +JSON.stringify(mapValues));
        for (var key in mapValues) {
            if (mapValues.hasOwnProperty(key)) {
                for(var item in mapValues[key]) {
                    if(mapValues[key][item] == aircraftType) {
                        component.set("v.recordType", key);     
                        break;
                    }
                }
            }
        };
        console.log("getRecordType recordType After "+component.get("v.recordType"));
    },
    
    getPageInfo: function(component,event,helper){
        var self = this;
        console.log('getPageInfo');
        var action = component.get('c.getPageType');
        action.setParams({ 
            recordId : component.get("v.recordId"),
            sObjName : component.get("v.sobjecttype")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("getPageInfo : state "+state);
            if (state === "SUCCESS") {
                var value = response.getReturnValue();
                console.log('getPageInfo : value '+value);
                component.set("v.showpicklist", value);
                console.log('getPageInfo : component set value '+component.get("v.showpicklist"));
                if(value == false) {
                	this.getPicklistRecord(component);    
                }
                this.getPreDefinedValues(component);
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("getPageInfo errors "+errors);
                if (errors) {
                    self.handleErrors(errors);   
                } else {
                    console.log("getPageInfo unknown error");
                    self.showErrorMsg(component, "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getPreDefinedValues : function(component) {
        console.log('getPreDefinedValues'); 
        console.log('getPreDefinedValues : sObj :'+component.get("v.sobjecttype"));    
        var action = component.get('c.getPresetValues');
        console.log('getPreDefinedValues : getPresetValues :'+action);
        var self = this;
        action.setParams({ 
            recordId : component.get("v.recordId"),
            sObjName : component.get("v.sobjecttype")
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log('getPreDefinedValues :allValues :'+JSON.stringify(allValues));
            component.set("v.presetData", allValues);
            var financialRiskDefault = '';
            if(allValues != null) {
                component.set("v.aircraftType", allValues.AircraftType);
                console.log('getPreDefinedValues : allValues.Aircrafttype :'+allValues.AircraftType);
                var type = component.get("v.aircraftType");
             	financialRiskDefault = allValues.FinancialRisk;
                component.set("v.financialRiskDefault", financialRiskDefault);
                console.log('getPreDefinedValues : type :'+type);
                component.set("v.recordType", allValues.RecordType);
                //this.fetchRecordType(component,type);
                //var recordType = component.get("v.recordType");
                //console.log('getPreDefinedValues : recordType :'+recordType);
                var data = "for "+type;
                console.log('getPreDefinedValues : data :'+data);
                var variant = allValues.Variant;
                console.log('getPreDefinedValues : variant :'+variant);
                component.set("v.assetVariant",variant);
                if(variant)
                    data = "for "+type+"-"+variant;
                component.set("v.aircraftData",data);
                console.log('getPreDefinedValues : data for variant :'+data);
                var date = allValues.DateOfManf;
                console.log('getPreDefinedValues : date'+date);
                if(date == null)
                    date = new Date();
             
                if(date != null) {
                    date = new Date(date);
                    console.log("getPreDefinedValues : getDateOfManufacture date is " + date);
                    var monthNames = 
                        ["Jan", "Feb", "Mar",
                         "Apr", "May", "Jun", "July",
                         "Aug", "Sep", "Oct",
                         "Nov", "Dec"
                        ]; 
                    var day = date.getUTCDate();
                    var monthIndex = date.getUTCMonth();
                    var year = date.getUTCFullYear();
                    
                    console.log("getPreDefinedValues : Date given: "+day + ' ' + monthNames[monthIndex] + ' ' + year);
                    var dateFormat = monthNames[monthIndex] +' '+day+ ', ' +year;
                    //var dateFormat = (monthIndex+1) +' '+year;
                    component.set("v.dom", date); 
                    console.log('dateFormat v.dom'+dateFormat);
                    component.set("v.aircraftDom", dateFormat);
                }
                this.getDataList(component);
             }
            if(component.get("v.showpicklist") == false) {
                // get the fields API name and pass it to helper function  
                var controllingFieldAPI = component.get("v.controllingFieldAPI");
                var dependingFieldAPI = component.get("v.dependingFieldAPI");
                var objDetails = component.get("v.objDetail");
                // call the helper function
                this.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingFieldAPI);
            }
            this.getFinanicalPicklist(component);
            this.getSourcePicklist(component, 'source');
        });
        $A.enqueueAction(action);
    },
    
    getSourcePicklist: function(component, elementId) {
        console.log('getSourcePicklist');
        var action = component.get('c.getSourcePicklist');
        var self = this;
        var opts = [];
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getSourcePicklist : response for getSourcePicklist "+allValues);
            for(var i=0;i< response.getReturnValue().length; i++) {
                if(response.getReturnValue()[i] == ('Portfolio Acquisitions') && 
                   component.get('v.Portfolio_Acquisitions') == true)
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                else if(response.getReturnValue()[i] == ('Closed Deals') && 
                   component.get('v.Closed_Deals') == true)
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                else if(response.getReturnValue()[i] == ('All Deals') && 
                   component.get('v.All_Deals') == true)
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                else if(response.getReturnValue()[i] == ('Current Leases') && 
                   component.get('v.Current_Leases') == true)
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i],  selected: true});
                else if(response.getReturnValue()[i] == ('Market Intel') && 
                   component.get('v.Market_Intel') == true)
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                else if(response.getReturnValue()[i] == ('World Fleet') && 
                   component.get('v.World_Fleet') == true)
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                
             /*   if(response.getReturnValue()[i] == "Current Leases") 
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i], selected: true});
                else
                    opts.push({"class": "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                    */
            }
            var sourcepicklistId = component.find(elementId);
            if(sourcepicklistId != undefined){
                sourcepicklistId.set("v.options", opts); 
            }    
        });
        $A.enqueueAction(action);
    },
    
    getDataList: function(component) {
        console.log('getDataList');
        //var action = component.get('c.getData');
        var action = component.get('c.getDataSources');
        
        var dom = 3;
        var criteriaCred = ">=";
        var rating = "Any";
        var src = "Current Leases";
        component.set("v.popupTitle", src);
        component.set("v.isLoading", true);
        
        let aircraftData = component.get("v.aircraftData");
        let variant;
        let aircraftVariant = [];
        console.log('--------- 1',component.get("v.assetVariant"));
        
        console.log('AIRCRAFT DATA: ',aircraftData);
        var aircraftType = component.get('v.aircraftType');
        var yearOfManufacture = component.get("v.aircraftDom"); 
        var recordId = component.get('v.recordId');
        var yrOfManu = new Date().getUTCFullYear();
        var rating = '';
        if(component.get("v.showFinancial") == true)
        	rating = component.find('financialRisk').get("v.value");
        
        
        if(yearOfManufacture != null)
        	yrOfManu = new Date(yearOfManufacture).getUTCFullYear();
        console.log("getDataList :Type:" + aircraftType + " DOM:"+yearOfManufacture+" id:"+recordId +" yrOfManu:"+yrOfManu);
        console.log('getDataList : Rating selected '+rating+ ' dom '+dom + ' source '+src);
        var presetData = component.get("v.presetData");
        var recordType = component.get("v.recordType");
        if(!$A.util.isEmpty(presetData) && !$A.util.isUndefined(presetData))
            recordType = presetData.RecordType;
        var leaseDateRange = component.get("v.leaseDateRange");
        console.log('getDataList : leaseDateRange: '+leaseDateRange + ' presetData: '+presetData +'recordType'+recordType);
        
        action.setParams({
            VintageRange : dom,
            FinancialRisk : rating,
            Source : src,
            Variant : component.get("v.assetVariant"),
            AircraftType: aircraftType,
            YearOfManufacture : yrOfManu,
            AssetDetailId : recordId,
            leaseDateRange : leaseDateRange,
            recordType : recordType
        });
        action.setCallback(this, function(response) {
            console.log('getDataList : getReturnValue '+JSON.stringify(response.getReturnValue()));
            //component.set('v.assetDeal', response.getReturnValue());
            var arrayMapKeys = [];
            for(var key in response.getReturnValue()){
                arrayMapKeys.push({key: key, value: response.getReturnValue()[key]});
            }
            component.set("v.assetDeal", arrayMapKeys);
            component.set("v.isLoading", false);
        });
        if(component.get('v.Current_Leases') == true)
        	$A.enqueueAction(action);
        else
            component.set("v.isLoading", false);
    },
    
    onClosePopup: function(component,event,helper) {
        console.log('onClosePopup');
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
    
    fetchPicklistValues: function(component,objDetails,controllerField, dependentField) {
        console.log('fetchPicklistValues');
        // call the server side function  
        var action = component.get("c.getDependentMap");
        // pass paramerters [object definition , contrller field name ,dependent field name] -to server side function 
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField 
        });
        //set callback   
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var StoreResponse = response.getReturnValue();
                
                // once set #StoreResponse to depnedentFieldMap attribute 
                component.set("v.depnedentFieldMap",StoreResponse);
                
                // create a empty array for store map keys(@@--->which is controller picklist values) 
                var listOfkeys = []; // for store all map keys (controller picklist values)
                var ControllerField = []; // for store controller picklist value to set on lightning:select. 
                
                // play a for loop on Return map and fill the all map key on listOfkeys variable.
                for (var singlekey in StoreResponse) {
                    listOfkeys.push(singlekey);
                }
                
                //set the controller field value for lightning:select
                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push('--- None ---');
                }
                for (var i = 0; i < listOfkeys.length; i++) {
                    ControllerField.push(listOfkeys[i]);
                }  
                // set the ControllerField variable values to country(controller picklist field)
                component.set("v.listControllingValues", ControllerField);
                console.log('fetchPicklistValues : Aircraft type values '+ControllerField);
            } else{
                alert('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields) {
        console.log('fetchDepValues');
        // create a empty array var for store dependent picklist values for controller field  
        var dependentFields = [];
        //dependentFields.push('--- None ---');
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push(ListOfDependentFields[i]);
        }
        // set the dependentFields variable values to store(dependent picklist field) on lightning:select
        component.set("v.listDependingValues", dependentFields);
        console.log('fetchDepValues : Engine type values'+dependentFields);
    }, 
    
    showErrorMsg : function(component, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Error!",
            message: msg,
            duration:'5000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

      
    //to handle errors
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", 
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    console.log('error '+ toastParams.message);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        console.log('error '+ toastParams.message);
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            console.log('error '+ toastParams.message);
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        });  
                    };
                }
            });
        }
    },
})