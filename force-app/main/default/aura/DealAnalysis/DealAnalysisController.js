({    
    applyFilter: function(component, event, helper) {
        console.log('applyFilter');
        var showpicklist = component.get("v.showpicklist");
        console.log('applyFilter :showpicklist '+showpicklist);
        var rating = '';
        if(component.get("v.showFinancial") == true)
            rating = component.find('financialRisk').get("v.value");
        
        if(showpicklist == false) {
            var type = component.get("v.aircraftType");
            console.log('applyFilter : type :'+type);
            var data = "for "+type;
            console.log('applyFilter : data :'+data);
            var variant = component.get("v.assetVariant");
            console.log('applyFilter : variant :'+variant);
            if(variant)
                data = "for "+type+"-"+variant;
            component.set("v.aircraftData",data);
        }
        
        helper.onClosePopup(component, event, helper);
        
        var dom = component.find('domId').get("v.value");
        var src = component.find('source').get("v.value");
        var leaseDateRange = component.find("leaseDateRangeId").get("v.value");
        var aircraftType = component.get('v.aircraftType');
        var yearOfManufacture = component.get("v.aircraftDom");
        var recordId = component.get('v.recordId');
        var yrOfManu = new Date(yearOfManufacture).getUTCFullYear();
        if($A.util.isEmpty(dom) || $A.util.isUndefined(dom))
            dom = 0;
        
        if($A.util.isEmpty(dom) || $A.util.isUndefined(dom))
            dom = 0;
        component.set("v.isLoading", true);
        var presetData = component.get("v.presetData");
        var recordType = component.get('v.recordType');
        
        console.log("applyFilter : Type:" + aircraftType + " DOM:"+yearOfManufacture+" id:"+recordId + "yrOfManu:"+yrOfManu);
        console.log('applyFilter : Rating selected '+rating+ ' dom '+dom + ' source '+src + ' recordType: '+recordType);
        console.log("applyFilter : leaseDateRange "+leaseDateRange+ ' presetData: '+presetData);
        var action = component.get('c.getDataSources');
        
        action.setParams({
            VintageRange : dom,
            FinancialRisk : rating,
            Source : src,
            Variant : component.get("v.assetVariant"),
            AircraftType: aircraftType,
            YearOfManufacture : yrOfManu,
            AssetDetailId : recordId,
            leaseDateRange : leaseDateRange,
            recordType : recordType
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("applyFilter : state "+state);
            if (state === "SUCCESS") {
                console.log('applyFilter getReturnValue '+response.getReturnValue());
                var arrayMapKeys = [];
                var data = response.getReturnValue();
                for(var key in data){
                    arrayMapKeys.push({key: key, value: data[key]});
                }
                component.set("v.assetDeal", arrayMapKeys);
            }
            else if (state === "ERROR") {    
                var errors = response.getError();
                console.log("applyFilter errors "+errors);
                if (errors) {
                    helper.handleErrors(errors);   
                } else {
                    console.log("applyFilter unknown error");
                    helper.showErrorMsg(component, "Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
        
    },
    
    loadChildData: function(component, event, helper) {
        console.log('loadChildData');
        var aircraftType = component.get('v.simpleRecord.Aircraft_Type__c');
        var variant = component.get('v.simpleRecord.Aircraft__r.Aircraft_Variant__c');
        var data = "for "+aircraftType+"-"+variant;
        console.log("loadChildData : data " +data) ; 
        component.set("v.aircraftData",data);
        helper.getDataList(component);
    },
    
    onOpenPopup: function(component,event,helper) {
        console.log('onOpenPopup');
        var isPopup = component.get("v.isPopup");
        console.log("onOpenPopup : onMouseOver isPopup "+isPopup);
        if(isPopup == true)
            return;
        component.set("v.isPopup", true);
        var indexStr = event.currentTarget.id;
        console.log("onOpenPopup : onMouseOver index "+indexStr);
        var splitStr = indexStr.split("#");
        console.log("onOpenPopup : onMouseOver "+splitStr);
        var index = splitStr[0];
        var category = splitStr[1];
        var subIndex = splitStr[2];
        var highLow = splitStr[3];
        
        console.log("onOpenPopup : onMouseOver "+index +" "+category);
        var assetList = component.get("v.assetDeal");
        var asset = assetList[index].value[subIndex];
        component.set("v.popupTitle", asset.Source);
        console.log("onOpenPopup : selected Deal "+ JSON.stringify(asset));
        
        if(asset.Source == 'Portfolio Acquisitions') 
            component.set('v.showTerm', false);
        else
        	component.set('v.showTerm', true);
        
        if(asset.Source == 'Portfolio Acquisitions') 
            component.set('v.displayTitle', 'Deal');
        else if(asset.Source == 'Closed Deals') 
            component.set('v.displayTitle', 'Deal');
        else if(asset.Source == 'All Deals') 
            component.set('v.displayTitle', 'Asset in Deal');
        else if(asset.Source == 'Current Leases') 
            component.set('v.displayTitle', 'Lease');
        else if(asset.Source == 'Market Intel') 
            component.set('v.displayTitle', 'Market Intel');
        else if(asset.Source == 'World Fleet') 
            component.set('v.displayTitle', 'Fleet');
        
        var popupWidth = 750;
        
        if(asset.Source == 'All Deals' || asset.Source == 'Portfolio Acquisitions' || asset.Source == 'Closed Deals') 
            component.set("v.typeTitle", 'MSN');
        else
            component.set("v.typeTitle", 'Aircraft Type/Variant');
        
        console.log('popup title src '+ asset.Source);
        console.log('popup title  ' +component.get("v.typeTitle"));
        
        component.set("v.popupWidth", popupWidth);
        
        var recs = asset.Recs;
        if(recs.length > 0){
          recs = recs.sort((a, b) => parseFloat(b.Rent) - parseFloat(a.Rent));
        }
        var data =[];
        if(recs.length > 0 ){
            for(var i =0 ; i < recs.length; i++) {
                var recItem = recs[i];
                if(highLow == 'low') {
                    if(!$A.util.isEmpty(recItem.IsLowest) && !$A.util.isUndefined(recItem.IsLowest)) {
                        data = recItem;
                    }
                }
                else if(highLow == 'high') {
                    if(!$A.util.isEmpty(recItem.IsHighest) && !$A.util.isUndefined(recItem.IsHighest)) {
                        data = recItem;
                    }
                }else {
                    data.push(recItem);
                }
            }
        }
        else {
            data = null;
        }
        //console.log("onOpenPopup : onclick data "+JSON.stringify(data));
        component.set("v.assetRecord", data);
        if(data != null) {
            var cmpTarget = component.find('pop');
            $A.util.removeClass(cmpTarget, 'slds-hide');
            $A.util.addClass(cmpTarget, 'slds-show');
            $A.util.removeClass(cmpTarget, 'slds-hide');
            var popup = component.find("pop").getElement();
            if(popup) {
                var offset = component.find('parentDiv').getElement().getBoundingClientRect().top + 
                    document.documentElement.scrollTop ;
                var topPadding = (event.clientY + document.documentElement.scrollTop) - offset + 10;
                var leftOffset = component.find('parentDiv').getElement().getBoundingClientRect().left;
                var documentWidth = document.documentElement.clientWidth - 40;
                var left = event.clientX - leftOffset;
                
                popup.style.top = (topPadding + 25)+'px';
                popup.style.left = (left- 770 )+'px';
                popup.style.display = "inline";
                popup.style.position = "absolute";
                //popup.style.width = documentWidth+ "px";
            }
        }
    },
    
    onClosePopup: function(component,event,helper) {
       // console.log('onClosePopup');
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        component.set("v.isPopup", false);
       // console.log('onClosePopup : component set '+component.get("v.isPopup"));
    },
    
    showRecord: function(component,event,helper) {
        console.log('showRecord');
        var recordId = event.currentTarget.id;
        console.log("showRecord : recordId "+recordId);
        window.open('/' + recordId);  
    },
    
    onControllerFieldChange: function(component, event, helper) {  
        console.log('onControllerFieldChange');
        var controllerValueKey = event.getSource().get("v.value"); // get selected controller field value
        var depnedentFieldMap = component.get("v.depnedentFieldMap");
        console.log('onControllerFieldChange : selected contrller value'+controllerValueKey);
        console.log('onControllerFieldChange : selected dependent value'+depnedentFieldMap);
        if(controllerValueKey != '--- None ---') {
            helper.getRecordType(component, controllerValueKey);
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            component.set("v.aircraftType", controllerValueKey);
            component.set("v.assetVariant", '');
            console.log('onControllerFieldChange : selected value'+controllerValueKey);
            if(ListOfDependentFields.length > 0){
                component.set("v.bDisabledDependentFld" , false);
                helper.fetchDepValues(component, ListOfDependentFields);    
            }
            else{
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['--- None ---']);
            }  
        } 
        else {
            component.set("v.listDependingValues", ['--- None ---']);
            component.set("v.bDisabledDependentFld" , true);
        }
    },
    
    onDependentFieldChange : function(component, event, helper) {  
        console.log('onDependentFieldChange');
        var dependentValueKey = event.getSource().get("v.value"); // get selected controller field value
        console.log('onDependentFieldChange : selected dependent value'+dependentValueKey);
        if(dependentValueKey != '--- None ---') {
            component.set("v.assetVariant", dependentValueKey);
        } 
        else {
            component.set("v.assetVariant",'');
            
        }
    },
    
})