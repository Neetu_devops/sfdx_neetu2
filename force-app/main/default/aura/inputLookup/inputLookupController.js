({
    
    /*
     * Executes the search server-side action when the c:InputLookupEvt is thrown
     */
    handleInputLookupEvt: function(component, event, helper){
        console.log("inputLookupController.handleInputLookupEvt(+)==>");
		helper.searchAction(component, event.getParam('searchString')); 
        console.log("inputLookupController.handleInputLookupEvt(-)==>");
    },
    
    /*
    	Loads the typeahead component after JS libraries are loaded
    */
    initTypeahead : function(component, event, helper){
        //first load the current value of the lookup field and then
        //creates the typeahead component
        console.log("inputLookupController.initTypeahead(+)==>");
        helper.loadFirstValue(component);
        console.log("inputLookupController.initTypeahead(-)==>");
    }
})