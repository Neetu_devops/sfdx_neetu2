({
    doInit : function(component, event) {

        this.showSpinner(component);
        var action = component.get("c.processUtilization");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result != '' && result != null) {
                    component.set("v.message",result);
                    var  messageBlock = component.find('messageBlock');
                    $A.util.removeClass(messageBlock,"failureMessage");
                    $A.util.addClass(messageBlock,"successMessage");
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors, component);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
        
    },
    
    //to show spinner while the data is loading
    showSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    //to hide spinner after the data is loaded
    hideSpinner: function (component) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    //to handle errors
    handleErrors : function(errors, component) {
        let errorMessage = '';
        
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    errorMessage += error.message;
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    
                    error.pageErrors.forEach( function(pageError) {
                        errorMessage += pageError.message;
                    });
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){
                            errorMessage += errorList.message;
                        });  
                    };
                }
            });
            component.set("v.message",errorMessage);
            var messageBlock = component.find('messageBlock');
            $A.util.removeClass(messageBlock,"successMessage");
            $A.util.addClass(messageBlock,"failureMessage");
        }
    }
})