({
    
    doInit : function(component, event, helper) {
        
        helper.doInit(component, event);
    },
    closeModel : function(cmp , event ,helper)
    {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
           homeEvent.setParams({
               "scope": "Utilization_Report_Staging__c"
           });
           homeEvent.fire();
    }
   
 })