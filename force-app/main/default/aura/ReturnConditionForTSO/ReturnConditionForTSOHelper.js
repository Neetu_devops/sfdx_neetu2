({
    fetchDeliveryReturnCondition : function(component, event){
        this.showSpinner(component);
        var action = component.get("c.fetchReturnCondition");
        var recId = component.get("v.recordId");
        action.setParams({
            "ScenarioId": recId,
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                if(dataObj.length > 0 ){ 
                    console.log('******',dataObj);
                    if(dataObj[1] != undefined)
                        component.set("v.returnConditionApplicability",dataObj[1].returnConditionApplicability);
                    component.set("v.lstAssemblyWrapper",dataObj);
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    this.handleErrors(errors);
                }
            }
            this.hideSpinner(component);
        });
        
        $A.enqueueAction(action);
    },
    
    // Show the lightning Spinner
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    // Hide the lightning Spinner
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    //Show the error toast when any error occured
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Return Contition Table Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        if (errors) {
            errors.forEach( function (error){
                //top-level error. There can be only one
                if (error.message){
                    toastParams.message = error.message;
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
                //page-level errors (validation rules, etc)
                if (error.pageErrors){
                    let arrErr = [];
                    error.pageErrors.forEach( function(pageError) {
                        toastParams.message = pageError.message;
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    });	
                }
                if (error.fieldErrors){
                    //field specific errors--we'll say what the field is
                    for (var fieldName in error.fieldErrors) {
                        //each field could have multiple errors
                        error.fieldErrors[fieldName].forEach( function (errorList){	
                            toastParams.message = errorList.message;
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                            //console.log( "Field Error on " + fieldName + " : " + errorList.message );	
                        });  
                    };
                }
            });
        }
    }
})