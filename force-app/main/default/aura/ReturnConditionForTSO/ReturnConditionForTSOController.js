({
	doInit : function(component, event, helper) {
		helper.fetchDeliveryReturnCondition(component,event);
	},
    navigateToLLPTable: function(component, event, helper) {
        var keyValue = event.currentTarget.dataset.recid;
        console.log('keyValue',keyValue);
        component.set("v.assemblyType",keyValue.split('#')[2]);
        var filtertype = keyValue.split('#')[0]+'#'+keyValue.split('#')[1];
        console.log('filtertype',filtertype);
        
        let frontDrop = component.find("ShowSummaryFrontdrop");
        let backDrop = component.find("ShowSummaryBackdrop");
        $A.util.removeClass(frontDrop, "slds-hide");
        $A.util.removeClass(backDrop, "slds-hide");
		
        let ShowLLPSummary = component.find("ShowSummaryTable");
        if(ShowLLPSummary){
            ShowLLPSummary.destroy();
        }
        $A.createComponent(
            "c:ShowLLPSummaryTable",
            {
                "aura:id": "ShowSummaryTable",
                "recordId": component.get("v.recordId"), 
                "filtertype" : filtertype
            },
            function(elemt, status, errorMessage){
                if (status === "SUCCESS") {
                    console.log(status);
                    var targetCmp = component.find('ShowSummaryTableDiv');
                    var body = targetCmp.get("v.body");
                    body.push(elemt);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },
    
    closePopup: function(component, event, helper) {
        let frontDrop = component.find("ShowSummaryFrontdrop");
        let backDrop = component.find("ShowSummaryBackdrop");
        $A.util.addClass(frontDrop, "slds-hide");
        $A.util.addClass(backDrop, "slds-hide");
    },
})