({
    switchComponents : function(cmp, event, helper) {
        var evtName = event.getParam("evtName");
        console.log('switng');
        if(evtName == 'showSearchResults'){
            $('#searchContainer').hide();
            var userContext = cmp.get("v.uiTheme");
            $A.createComponent(
                "c:AircraftSearchResultComponent",
                {"userContext" : userContext,
                 "aircraftType" : event.getParam("aircraftType"),
                 "aircraftVariant" : event.getParam("aircraftVariant"),
                 "engineType" : event.getParam("engineType"),
                 "vintage" : event.getParam("vintage"),
                 "vintageRange" : event.getParam("vintageRange"),
                 "leaseDateTo" : event.getParam("leaseDateTo"),
                 "leaseDateFrom" : event.getParam("leaseDateFrom"),
                 "searchTransaction" : event.getParam("searchTransaction"),
                },
                function(newComp, status, errorMessage){
                    if (status === "SUCCESS") {
                        var content = cmp.find("searchResultContainer");
                        content.set("v.body", newComp);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("Error while creating new Component")
                        // Show offline error
                    }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                            // Show error message
                        }
                }
            );
        }
        else if(evtName == 'showRecordDetail'){
            $('#searchResultContainer').hide();
            
            $A.createComponent(
                "c:AircraftSearchResultDetailComponent",
                {"objName" : event.getParam("objName"),
                 "recId" : event.getParam("recId")
                },
                function(newComp, status, errorMessage){
                    if (status === "SUCCESS") {
                        var content = cmp.find("searchDetailContainer");
                        content.set("v.body", newComp);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("Error while creating new Component")
                        // Show offline error
                    }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                            // Show error message
                        }
                }
            );
        }
        else if(evtName == 'backToSearch'){
            $('#searchContainer').show();
            var content = cmp.find("searchResultContainer");
            content.set("v.body", []);
        }
        else if(evtName == 'backToResult'){
                $('#searchResultContainer').show();
                var content = cmp.find("searchDetailContainer");
                content.set("v.body", []);
            }
    }
})