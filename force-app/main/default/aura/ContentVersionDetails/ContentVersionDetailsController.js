({
    doinit : function(component, event, helper){
        var viewall = false;
        helper.init(component, event, helper,viewall);
    },
    showAllVersions: function(component, event, helper){
        component.set('v.showView',false);
        var viewall = true;
        helper.init(component, event, helper,viewall);
    }
})