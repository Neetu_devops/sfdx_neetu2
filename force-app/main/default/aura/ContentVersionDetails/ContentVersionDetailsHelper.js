({
    init : function(component, event, helper, viewall) {
        var action = component.get('c.getContentVersionDetails');
        action.setParams({ 
            recordId : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.ListSize',response.getReturnValue().length);
                console.log('result.length:',result.length,viewall);
                if(result.length>=5 && !viewall ){
                    var contentList = [];
                    for(var i=0 ; i<5 ;i++){
                        contentList.push(result[i]);
                    }
                    component.set('v.ContentVersionList',contentList);
                }
                else{
                    if(viewall){
                        component.set('v.showView',false);    
                    }
                    component.set('v.ContentVersionList',result);
                }
            }
            else if(state === "ERROR"){
                alert('Error');
            }
        });
        $A.enqueueAction(action)
    }
})