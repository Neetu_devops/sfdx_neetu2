({
    
  /*  getSectionsBasednRecordType : function(component, event, index, childIndex, showSection) {
        var objectAPIName = component.get("v.parentObject");
        var fieldAPIName = component.get("v.parentZone");
        var recordTypeDeveloperName =  component.get("v.parentRecordType");
        
        var action = component.get("c.getSectionsRecordType");
        action.setParams({
            objectAPIName : objectAPIName,
            fieldAPIName : fieldAPIName,
            recordTypeDeveloperName : recordTypeDeveloperName
        });
        
        action.setCallback(this, function(response){
            var allValues = response.getReturnValue();
            console.log("getSections output: " +JSON.stringify(allValues));
            component.set("v.sectionList", allValues);
            if(!$A.util.isUndefined(allValues) && !$A.util.isEmpty(allValues)) {
               // this.getReviewStatus(component,event);
                this.getParentRows(component,event, index, childIndex,showSection);
            }
        });
        $A.enqueueAction(action);
        
	}, */
    
    refreshReviewStatus :function(component, event) {
        var action = component.get('c.getReviewStatus');
        action.setParams({
            sections : component.get("v.sectionList"),
            recordType : component.get("v.parentRecordType"),
            recordId: component.get("v.assetReviewRecordId")
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("refreshReviewStatus output: " +JSON.stringify(allValues));
            var secRList = [];
            for(var i =0; i < allValues.length; i++) {
                secRList.push(allValues[i].reviewStatus);
            }
            component.set("v.sectionReviewList", secRList);
        });
        $A.enqueueAction(action);    
    },
    
    getSections : function(component, event, index, childIndex, showSection) {
        var action = component.get('c.getSections');
        action.setParams({
            recordType : component.get("v.parentRecordType"),
            recordId: component.get("v.assetReviewRecordId")
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getSections output: " +JSON.stringify(allValues));
            component.set("v.sectionObj", allValues);
            if(!$A.util.isUndefined(allValues) && !$A.util.isEmpty(allValues)) {
                var secList = [];
                for(var i =0; i < allValues.length; i++) {
                    secList.push(allValues[i].name);
                }
                component.set("v.sectionList", secList);
                this.getParentRows(component,event, index, childIndex,showSection); 
            }
            else
                component.set("v.sectionList", null);
        });
        $A.enqueueAction(action);    
    },
    
    /*
    getReview : function(component) {
        var action = component.get('c.getReviewStatus');
        action.setParams({
            sections: component.get("v.sectionList"),
            recordId: component.get("v.assetReviewRecordId"),
            recordType : component.get("v.parentRecordType")
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            
        });
        $A.enqueueAction(action);
        
    }, */
   
    getParentRows : function(component , event, index, childIndex,showSection) {
        var sectionName;
        var secList = component.get("v.sectionList");
        console.log("getParentRows secList "+secList);
        if(secList != null && secList.length > 0)
            sectionName = secList[index];
        console.log("getParentRows sectionName "+sectionName);
        var action = component.get('c.getRows');
        action.setParams({
            objectName : component.get('v.parentObject'),
            mappingCond : component.get('v.parentCond'),
            recordId : component.get("v.assetReviewRecordId"),
            fieldSetName : component.get('v.parentFields'),
            parentReviewType : component.get('v.parentReviewType'),
            sectionField: component.get('v.parentZone'),
            section: sectionName
        });
        action.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log("getParentRows parent values " +JSON.stringify(allValues));
            var onItemChange = component.getEvent("onItemChange");
            console.log("getParentRows before childIndex "+childIndex);
            if($A.util.isUndefined(childIndex) || $A.util.isEmpty(childIndex))
                childIndex = 0;
            console.log("getParentRows after childIndex "+childIndex);
            try{
                onItemChange.setParams({
                    "itemId": allValues[childIndex].Id,
                    "name": secList[index]+' '+(childIndex+1) +': '+allValues[childIndex].Name,
                    "desc" : allValues[childIndex].Item_Description__c,
                    "index" : index,
                    "childIndex" : childIndex 
                });
                onItemChange.fire();
            }
            catch(e){console.log("getParentRows child id not found "+childIndex);}
            if(showSection){
            	//component.find("accordion").set('v.activeSectionName', secList[index]);
                //$A.get('e.force:refreshView').fire();
                this.fireRefreshEvt(component);
            }
        });
        $A.enqueueAction(action);
    },
    
    fireSuccessToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'title' : 'Success', 
            'message' : 'Record updated sucessfully.' ,
            'type':'success'
        }); 
        toastEvent.fire(); 
    },
    
    fireFailureToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ 
            'title' : 'Failed', 
            'message' : 'An error occurred.',
            'type':'error'
        }); 
        toastEvent.fire(); 
    },
    
    fireRefreshEvt : function(component) {
        var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent){
            console.log("fireRefreshEvt");
            refreshEvent.fire();
        }
    },
    
})