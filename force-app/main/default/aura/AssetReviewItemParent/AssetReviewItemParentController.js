({
	doInit : function(component, event, helper) {
        //helper.getSectionsBasednRecordType(component, event, 0, 0, false);
		helper.getSections(component, event, 0, 0, false);
        
	},
    
    refreshSection : function(component, event, helper) {
        console.log("refreshSection parent ");
        var params = event.getParam('arguments');
        if (params) {
            var index = params.refreshIndex;
            var childIndex = params.childIndex;
            component.set("v.sectionIndex", index);
            
            //helper.refreshReviewStatus(component, event);
            //helper.getParentRows(component, event, index, childIndex, true);
            
            helper.getParentRows(component, event, index, childIndex, true);
            console.log("refreshSection parent: "+index +" childIndex "+childIndex);
        }
	},
        
    onclickSection : function (component,event,helper) {
        var index = event.target.id;
        console.log("onclickSection " + index);
        
        var sectionList  = component.get("v.sectionList");
        var section = sectionList[index];
        console.log("onclickSection section "+JSON.stringify(section));
        if(section != undefined){
            console.log("onclickSection " + index);
        	component.set("v.sectionIndex", index);
            helper.getParentRows(component,event,index, 0, false);
        }
    },
    
    /*
    refreshCmp : function(component,event,helper) {
        helper.getColumns(component, event, false);
    }, */
    
    handleRowAction : function(cmp,event,helper){
        var row = event.getParam('row');
        var onItemChange = cmp.getEvent("onItemChange");
		onItemChange.setParams({
			"itemId": row.Id,
			"name": row.Name
		});
		onItemChange.fire();
    },
    
    mainSectionClick : function(component, event, helper) {
        var isSectionExpand = component.get("v.isSectionExpand");
        console.log("mainSectionClick isSectionExpand "+isSectionExpand);
        if(isSectionExpand) {
            component.set("v.openSection", "");
        }
        else {
            var sectionList = component.get("v.sectionList");
            var secNameList = [];
            if(!$A.util.isUndefined(sectionList) && !$A.util.isEmpty(sectionList)) {
                sectionList.forEach(function(record){
                    secNameList.push(record);
                });
                component.set("v.openSection", secNameList);
            }
            console.log("mainSectionClick secNameList "+secNameList);   
        }
        var acc = component.find("sec");
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
            $A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
        component.set("v.isSectionExpand", !isSectionExpand);
    }, 
    
    
})