public with sharing class PDPPaymentTriggerHandler {

    public void doPaymentsCalculations( List<PDP_Payment_Schedule__c> tiggerList ) {

        for (PDP_Payment_Schedule__c sched: tiggerList) {
            Decimal percent = sched.Percentage_of_PDPRP__c;
            Decimal totalPayment = sched.Total_Payment_Due_mm__c;
            Decimal PDPRP = sched.PDPRP__c;
            Decimal monthBeforeDeli = sched.Payment_Months_Before_Delivery__c;
            Date schedDeliveryDate = sched.Delivery_Date__c;
            Date schedDate = sched.Date__c;

            if ( percent != null && totalPayment !=  null && PDPRP != null ) {
                sched.Total_Payment_Due_mm__c += PDPRP * sched.Percentage_of_PDPRP__c/100;
            } else if ( percent == null && PDPRP != null && PDPRP > 0) {
                sched.Percentage_of_PDPRP__c = totalPayment / PDPRP * 100;
            }

            if(monthBeforeDeli != null && schedDeliveryDate != null) {
                sched.Date__c = schedDeliveryDate.addMonths(-integer.ValueOf(monthBeforeDeli));
            } else if (monthBeforeDeli == null && schedDate != null  && schedDeliveryDate != null) {
                sched.Payment_Months_Before_Delivery__c = schedDate.monthsBetween(schedDeliveryDate);
            }
        }
    }
}