public class ILOperatorTriggerHandler implements ITrigger{
	
    // Constructor
    private final string  triggerBefore = 'ILOperatorTriggerHandlerBefore';
    private final string  triggerAfter = 'ILOperatorTriggerHandlerAfter';
   
   
	public void bulkBefore() {}
    public void bulkAfter(){}
    public void beforeInsert(){
        system.debug('ILOperatorTriggerHandler.beforeInsert(+)');
        //Validate If the operator already added for IL 
        list<IL_Operator__c> newList = (list<IL_Operator__c>)trigger.new;
        set<id> ILIdSet = new set<id>();
        for(IL_Operator__c curRec : newList){
            ILIdSet.add(curRec.Interaction_Log__c);
        }
        map<Id,List<ID>> mapILOprs = getMapOfOperatorJOsForIL(ILIdSet);
        for(IL_Operator__c curRec: newList){
            if(mapILOprs.containsKey(curRec.Interaction_Log__c)){
                List<Id> oprIdList = mapILOprs.get(curRec.Interaction_Log__c);
                if(oprIdList.contains(curRec.Operator__c)){
                    curRec.addError('This Interaction log is already related to this Operator');
                }
            }
        }
       
        system.debug('ILOperatorTriggerHandler.beforeInsert(-)');
    }
    public void beforeUpdate() {}
   
    
    public void afterInsert(){
        system.debug('ILOperatorTriggerHandler.afterInsert(+)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        try{
            UpdateIL();
            UpdateLastILonOperator((list<IL_Operator__c>) trigger.new);//updates the Operator, Last Date of Interaction and Summary field to the latest vaules
        }
        catch(DMLexception ex){
            string errorMessage='';
            for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                // Process exception here
                errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
            }
            system.debug('errorMessage=='+errorMessage);
            for(IL_Operator__c il : (list<IL_Operator__c>) trigger.new){
                il.addError(errorMessage);
            }
        }
        system.debug('ILOperatorTriggerHandler.afterInsert(-)');
    }
   
    public void afterUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
	    UpdateIL();
    }
    public void beforeDelete()
    {
        system.debug('ILOperatorTriggerHandler.beforeDelete(+)');
        system.debug('ILOperatorTriggerHandler.beforeDelete(-)');
    }
    public void afterDelete()
    {
        system.debug('ILOperatorTriggerHandler.afterDelete(+)');
        UpdateIL();
        UpdateLastILonOperator((list<IL_Operator__c>) trigger.old);
        system.debug('ILOperatorTriggerHandler.afterDelete(-)');
    }
    public void afterUnDelete()
    {
    }
    public void andFinally()
    {
	}
    
     /**************************************************************************************************** */
    /* This method updates the Operator Look ups on IL and also Hidden Field Operator List
    Called From : After Insert/ After Update / After Delete
    /**************************************************************************************************** */
    private void UpdateIL(){
        //On After Insert/Update/Delete of Operator , get the details of corresponding IL and reset the values
        list<IL_Operator__c> newList = (list<IL_Operator__c>)(trigger.isDelete?trigger.old:trigger.new);

        set<id> ILIdSet = new set<id>();
        
        for(IL_Operator__c curRec : newList){
            ILIdSet.add(curRec.Interaction_Log__c);
        }
       
        map<id,Customer_Interaction_Log__c> cLogMap =new map<Id,Customer_Interaction_Log__c>([select Id, Name,Related_To_Operator__c,Related_To_Operator_2__c,Related_To_Operator_3__c,
                                           Related_To_Operator_4__c,Related_To_Operator_5__c,Related_To_Operator_6__c,(select Id,Operator__c,Operator__r.Name from ILOperators__r order by CreatedDate) from Customer_Interaction_Log__c where ID IN : ILIdSet]);
        
        String oprNames;
        for(Customer_Interaction_Log__c curRec: cLogMap.values()){
              oprNames='';
              clearOperators(curRec);
              List<IL_Operator__c> oprList = curRec.ILOperators__r;
              for(Integer i=0;i<oprList.size();i++){
                  switch on i{
                      when 0{
                        curRec.Related_To_Operator__c = oprList[0].Operator__c; 
                        oprNames +=oprList[0].Operator__r.Name;
                      }when 1{
                        curRec.Related_To_Operator_2__c = oprList[1].Operator__c;
                        oprNames +=', ' + oprList[1].Operator__r.Name;
                      }when 2{
                        curRec.Related_To_Operator_3__c = oprList[2].Operator__c;
                        oprNames += ', ' + oprList[2].Operator__r.Name;
                      }when 3{
                        curRec.Related_To_Operator_4__c = oprList[3].Operator__c;
                        oprNames += ', ' + oprList[3].Operator__r.Name;
                      }when 4{
                        curRec.Related_To_Operator_5__c = oprList[4].Operator__c;
                        oprNames += ', ' + oprList[4].Operator__r.Name;
                      }when 5{
                        curRec.Related_To_Operator_6__c = oprList[5].Operator__c;
                        oprNames += ', ' + oprList[5].Operator__r.Name;
                      }when else{
                        oprNames += ', ' + oprList[i].Operator__r.Name;
                      }
                  }

              }
              curRec.Y_Hidden_Operator_List__c = oprNames;
        }
        try{
            LeasewareUtils.TriggerDisabledFlag = true;
            update cLogMap.values();
            LeasewareUtils.TriggerDisabledFlag = false;
        }catch(DmlException e){
            String exMessage = e.getMessage();
             	String customValidationString = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
             	if(exMessage.contains(customValidationString)){
                    	Integer index1 = exMessage.lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                    	Integer index2 = exMessage.indexOf(': [');
            			exMessage= exMessage.substring(index1+customValidationString.length()+1,index2);
                        system.debug('exMessage---'+exMessage);
                       
            			system.debug('Filtered Error Message---'+ exMessage);
                 }
            		newList[0].addError(exMessage);
        }
       
        
            
                        
    }
   
    /***************************************************************************************** */
    /* This method is to get the List of OperatorIds for the IL
    /********************************************************************************************** */
    private  map<Id,List<Id>> getMapOfOperatorJOsForIL(Set<Id> ILIdSet){
        map<Id,List<Id>> mapILOpr = new map<Id,List<Id>>();
        List<Id> oprIdList=null;
        List<IL_Operator__c> oprJOList = [select Id, Interaction_Log__c,Operator__c,Operator__r.Name from IL_Operator__c where Interaction_Log__c IN :ILIdSet order by Id];
        for(IL_Operator__c curRec : oprJOList){
            if(mapILOpr.containsKey(curRec.Interaction_Log__c)){
                oprIdList = mapILOpr.get(curRec.Interaction_Log__c);
               
            }else{
                oprIdList = new List<id>();
            }
             oprIdList.add(curRec.Operator__c);
             
             mapILOpr.put(curRec.Interaction_Log__c,oprIdList);
        }
        return mapILOpr;
    }
    private void clearOperators(Customer_Interaction_Log__c curRec){
        curRec.Related_To_Operator__c = null;
        curRec.Related_To_Operator_2__c =null;
        curRec.Related_To_Operator_3__c =null;
        curRec.Related_To_Operator_4__c = null;
        curRec.Related_To_Operator_5__c = null;
        curRec.Related_To_Operator_6__c = null;
    }

     /******
     * updates the Operator's Last Date of Interaction and Summary field to the latest vaules
     * ********/
    public static void UpdateLastILonOperator(list<IL_Operator__c> newList){
        system.debug('UpdateLastILonOperator(+)');
        set<id> setOpIds = new set<id>();   
        list<Operator__c> operatorsToUpdate = new list<Operator__c>();
        for(IL_Operator__c ilOp : newList){
            setOpIds.add(ilOp.operator__c);
        }
        map<id, Operator__c> mapOps = new map<id, Operator__c>([select Id,Name,Date_Of_Last_Interaction__c,Y_Hidden_Date_Of_Last_Interaction__c,Summary__c,(select interaction_log__c,interaction_log__r.name,interaction_log__r.date_of_call__c,interaction_log__r.summary__C from ILOperators__r order by interaction_log__r.Date_of_Call__c desc,CreatedDate desc limit 1)  from Operator__c where id IN :setOpIds]);
        for (Operator__c opr: mapOps.values()) {
            if(opr.ILOperators__r.size()>0){
                il_operator__c clog = opr.ILOperators__r;
                if(opr.Date_Of_Last_Interaction__c!= clog.interaction_log__r.date_of_call__c ||  opr.Summary__c != clog.interaction_log__r.summary__C){
                    opr.Y_Hidden_Date_Of_Last_Interaction__c = clog.interaction_log__r.date_of_call__c;
                    opr.Summary__c = clog.interaction_log__r.summary__C;
                    operatorsToUpdate.add(opr);
                } 
            }
            else{
                if(opr.Y_Hidden_Date_Of_Last_Interaction__c != null ||opr.Summary__c != null ){
                    opr.Y_Hidden_Date_Of_Last_Interaction__c = null;
                    opr.Summary__c = null;
                    operatorsToUpdate.add(opr);
                }
            }                                      
        }
        try{
            LeaseWareUtils.TriggerDisabledFlag = true;    
            if(operatorsToUpdate.size()>0){database.update(operatorsToUpdate, true);}// Changed as per this task https://app.asana.com/0/1134042593512586/1189867984417285
            LeaseWareUtils.TriggerDisabledFlag = false;
        }catch(DMLexception e){
            system.debug('Exception ' + e.getMessage());
            leasewareutils.createExceptionLog(e, 'Exception while updating Operator UpdateLastInteraction_Call() ', null, null,null,true);
            throw e;
        }
        system.debug('UpdateLastILonOperator(-)');
    }

}