public with sharing class CummulativeCashFlowController {
    @AuraEnabled
    public static ReturnConditionWrapper.CummulativePerMonthWrapper fetchCummulativeDataForTable(Id recordId, String startDate){
        ReturnConditionWrapper.CummulativePerMonthWrapper cummulativeWrapper = new ReturnConditionWrapper.CummulativePerMonthWrapper();
        List<Monthly_Scenario__c> lstMonthlyScenario = ForecastSOQLServices.getMonthlyScenarioAsPerDate(recordId,startDate);
        if(lstMonthlyScenario.size()>0){
            cummulativeWrapper.investmentDate = (lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c).addMonths(-1);
            cummulativeWrapper.investment = lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c;
            cummulativeWrapper.irr = lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.IRR__c;
            cummulativeWrapper.vpn = lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.NPV__c;
        }
        
        return cummulativeWrapper;
    }
    
    //2nd Line Chart for Cummulative Cash Flow 
    @AuraEnabled
    public static String cummulativeCashFlowData(Id recordId) {
        Scenario_Input__c scenarioInp = ForecastSOQLServices.getScenarioInput(recordId);
        
        List<Scenario_Component__c> lstSecnarioCmp =  ForecastSOQLServices.getScenarioOutput(scenarioInp.Id);
        List<Monthly_Scenario__c> lstMonthlyScenario = ForecastSOQLServices.getMonthlyScenario(lstSecnarioCmp);
        
        StaticResource static_resource = [SELECT Id, Name, SystemModStamp
                                          FROM StaticResource 
                                          WHERE Name = 'APU'
                                          LIMIT 1];
        String org_Url = String.valueOF(System.URL.getSalesforceBaseUrl().toExternalForm());
        
        String url_file_ref = '/resource/'
            + String.valueOf(((DateTime)static_resource.get('SystemModStamp')).getTime())
            + '/' 
            + leasewareutils.getNamespacePrefix()+static_resource.get('Name')
            ;
        org_Url += url_file_ref;
        //Map<String, Decimal> mapMonthlyScenario = CashFlowChartController.calculateTotalAsPerType(lstMonthlyScenario, true);
        Map<Date, Decimal> mapMonthlyScenario = calculateTotalAsPerType(lstMonthlyScenario, true);
        
        Date min, max;
        Date leaseDate;
        if(lstMonthlyScenario.size()>0)
            leaseDate= lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c.addMonths(-1);
        
        for(Monthly_Scenario__c ms : lstMonthlyScenario){
            if(min == null){
                min = leaseDate;
            }
            max =ms.Start_Date__c;
        }
        String markerJSON =  'url' + '(' + org_Url + ')' ;
        List<CashFlowChartController.CashFlowData> data = new List<CashFlowChartController.CashFlowData>();
        Map<Date, Decimal> mapCummulativeCashFlow = new  Map<Date, Decimal>();
        
        /*if(lstMonthlyScenario.size()>0){
	mapCummulativeCashFlow.put(leaseDate,lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c);
    	}*/
    
        Decimal temp =0 ;
        for(Date key : mapMonthlyScenario.keySet()){
            temp = temp + mapMonthlyScenario.get(key);
            mapCummulativeCashFlow.put(key,temp);
        }
        //System.debug('mapCummulativeCashFlow'+JSON.serializePretty(mapCummulativeCashFlow));
        for(Date key : mapCummulativeCashFlow.keySet()){
            if(key == min || key == max){
                data.add(new CashFlowChartController.CashFlowData(String.valueOf(key),markerJSON,Math.round(mapCummulativeCashFlow.get(key)/1000)));  
            }else{
                data.add(new CashFlowChartController.CashFlowData(String.valueOf(key),'',Math.round(mapCummulativeCashFlow.get(key)/1000)));
            }
        }
        return System.Json.serialize(data);    
    }
    
    /* Already exist in CashFlowChartController.cls. This method is created, with minor changes with data types, in order to
     * do not disturb the existing functionality */
    public static Map<Date,Decimal> calculateTotalAsPerType(List<Monthly_Scenario__c> lstMonthlyScenario, Boolean isInvestmentRequired){
        Map<Date, Decimal> mapMonthlyScenario = new Map<Date, Decimal>();
        Date leaseDate;
        Decimal investmentReq =0;
        leaseDate= lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c.addMonths(-1);
        if(isInvestmentRequired && leaseDate != null && lstMonthlyScenario.size()>0 && lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c != null){
            investmentReq = lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c > 0 ? lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c * (-1) : lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c;
            mapMonthlyScenario.put(leaseDate,investmentReq);
        }
        
        
        //System.debug('lstMonthlyScenario'+lstMonthlyScenario.size());
        for(Monthly_Scenario__c ms : lstMonthlyScenario){
            if(ms.Fx_Component_Output__r.Type__c == 'General'){
                mapMonthlyScenario.put(ms.Start_Date__c,ms.MR_Amount__c);
            }
        }
        return mapMonthlyScenario;
    }
}