public with sharing class PBHRentAssemblyTriggerHandler implements ITrigger {
    
    
    private final string triggerBefore = 'PBHRentAssemblyTriggerHandlerBefore';
    private final string triggerAfter = 'PBHRentAssemblyTriggerHandlerAfter';
    public PBHRentAssemblyTriggerHandler() {

    }
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public  void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){
        LeaseWareUtils.setFromTrigger(triggerBefore); 
        system.debug('PBHRentAssemblyTriggerHandler.beforeInsert(+)');
        
        system.debug('PBHRentAssemblyTriggerHandler.beforeInsert(-)');
    }
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('PBHRentAssemblyTriggerHandler.beforeUpdate(+)');
        system.debug('PBHRentAssemblyTriggerHandler.beforeUpdate(-)');
    }
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);       
        system.debug('PBHRentAssemblyTriggerHandler.beforeDelete(+)');
          
        system.debug('PBHRentAssemblyTriggerHandler.beforeDelete(-)');
    }
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        LeaseWareUtils.setFromTrigger(triggerAfter); 
        system.debug('PBHRentAssemblyTriggerHandler.afterInsert(+)');
        
        system.debug('PBHRentAssemblyTriggerHandler.afterInsert(-)');
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        system.debug('PBHRentAssemblyTriggerHandler.afterUpdate(+)');
        system.debug('PBHRentAssemblyTriggerHandler.afterUpdate(-)');
    }
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(){}
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterUnDelete(){}    
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){}

    
}