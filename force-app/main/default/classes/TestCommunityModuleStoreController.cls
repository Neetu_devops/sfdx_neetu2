/*****************************************************************************************************************
*	    Date:		    20 April 2021
*       Author:         Denis Karpenko – Leaseworks
*       Description:    Test Class for CommunityModuleStoreController Class
******************************************************************************************************************/
@isTest
public with sharing class TestCommunityModuleStoreController {
    
    @istest
    public static void testingQueries() {
        
        Test.startTest();
        
        List<String> assetClassList= new List<String>();
        Schema.DescribeFieldResult assetClassfieldResult = Portal_Asset_Template__c.Asset_Class__c.getDescribe();
        List<Schema.PicklistEntry> assetClassValuesList = assetClassfieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : assetClassValuesList){
            String picklistStringVal = pickListVal.getLabel();
            assetClassList.add(picklistStringVal);
        }     
        
        List<String> assetTypeList= new List<String>();
        Schema.DescribeFieldResult assetTypefieldResult = Portal_Asset_Template__c.Asset_Type__c.getDescribe();
        List<Schema.PicklistEntry> assetTypeValuesList = assetTypefieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : assetTypeValuesList){
            String picklistStringVal = pickListVal.getLabel();
            assetTypeList.add(picklistStringVal);
        }     
        
        List<String> assetVariantList= new List<String>();
        Schema.DescribeFieldResult assetVariantfieldResult = Portal_Asset_Template__c.Asset_Variant__c.getDescribe();
        List<Schema.PicklistEntry> assetVariantValuesList = assetVariantfieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : assetVariantValuesList){
            String picklistStringVal = pickListVal.getLabel();
            assetVariantList.add(picklistStringVal);
        } 
        
        Portal_Asset_Template__c testtemplate1 = new Portal_Asset_Template__c(Name = 'Test Template 1', Asset_Class__c = assetClassList[0], Asset_Type__c = assetTypeList[0], Asset_Variant__c =assetVariantList[0], Remaining_Cycles_applicable__c = true);
        Portal_Asset_Template__c testtemplate2 = new Portal_Asset_Template__c(Name = 'Test Template 2', Asset_Class__c = assetClassList[0], Asset_Type__c = assetTypeList[0], Asset_Variant__c = assetVariantList[0], Remaining_Cycles_applicable__c = true);
        insert testtemplate1;
        insert testtemplate2;
        
        List<Portal_Asset_Template__c> templateList = CommunityModuleStoreController.getTemplateInfo(assetClassList[0], assetTypeList, assetVariantList);
        System.assertEquals(2, templateList.size(), 'Templates should have been retrieved');
        
        Date todaysDate = CommunityModuleStoreController.getTodaysDate();
        System.assertEquals(Date.today(), todaysDate, 'Todays date should have been retrieved');

        String datatableColumns = CommunityModuleStoreController.getDatatableColumns('FakeAssetName');
        System.assertNotEquals(null, datatableColumns, 'Datatable Columns should have been retrieved');

        List<CommunityModuleStoreController.DynamicAssetFilter> dynamicFiltersList = CommunityModuleStoreController.getDatatableFilters('FakeAssetName');
        System.assertNotEquals(0, dynamicFiltersList.size(), 'Datatable Filters should have been retrieved');
 
        Portal_Asset__c testAsset1 = new Portal_Asset__c(Name = 'Test Asset 1', Asset_Class__c = assetClassList[0], Asset_Type__c = assetTypeList[0], Asset_Variant__c = assetVariantList[0], PMA_DER__c = false,  Portal_Asset_Template__c = testtemplate1.Id, Remaining_Cycles__c = 10000, EGT_Margin__C = 100);
        Portal_Asset__c testAsset2 = new Portal_Asset__c(Name = 'Test Asset 2', Asset_Class__c = assetClassList[0], Asset_Type__c = assetTypeList[0], Asset_Variant__c = assetVariantList[0], PMA_DER__c = true,  Portal_Asset_Template__c = testtemplate1.Id, Remaining_Cycles__c = 10000, EGT_Margin__C = 100);
        insert testAsset1;
        insert testAsset2;

        CommunityModuleStoreController.AssetPicklistWrapper picklistWrapper = CommunityModuleStoreController.getAssetPicklistValues(assetClassList[0]);
        System.assertEquals(assetTypeList[0], picklistWrapper.assetTypeValues[0].picklistFieldLabel, 'Picklist values for Asset Type were not retrieved');
        System.assertEquals(assetVariantList[0], picklistWrapper.assetVariantValues[0].picklistFieldLabel, 'Picklist values for Asset Variant were not retrieved');
        
        List<CommunityModuleStoreController.AssetFilter> filterList = new List<CommunityModuleStoreController.AssetFilter>();
        CommunityModuleStoreController.AssetFilter assetFilter1 = new CommunityModuleStoreController.AssetFilter();
        assetFilter1.templateId = testtemplate1.Id;
        assetFilter1.limiterRange = 9000;
        assetFilter1.egtMargin = 90;
        assetFilter1.limiterField = 'Remaining_Cycles__c';
        CommunityModuleStoreController.AssetFilter assetFilter2 = new CommunityModuleStoreController.AssetFilter();
        assetFilter2.templateId = testtemplate1.Id;
        assetFilter2.limiterRange = 11000;
        assetFilter2.egtMargin = 200;
        assetFilter2.limiterField = 'Remaining_Cycles__c';
        filterList.add(assetFilter1);
        filterList.add(assetFilter2);

        List<CommunityModuleStoreController.DynamicAssetFilter> updatedDynamicFiltersList = new List<CommunityModuleStoreController.DynamicAssetFilter>();
        for (CommunityModuleStoreController.DynamicAssetFilter dynFilter : dynamicFiltersList) {
            if (dynFilter.fieldType == 'number') {
                CommunityModuleStoreController.DynamicAssetFilter updDynFilter = new CommunityModuleStoreController.DynamicAssetFilter();
                updDynFilter.fieldLabel = dynFilter.fieldLabel;
                updDynFilter.fieldName = dynFilter.fieldName;
                updDynFilter.fieldType = dynFilter.fieldType;
                updDynFilter.fieldValue = '0';
                updDynFilter.comparisonSign = 'greater';
                updatedDynamicFiltersList.add(updDynFilter);
            }
        }
        
        String assetListJson = CommunityModuleStoreController.getPortalAssetData(assetClassList[0], assetTypeList, assetVariantList, JSON.serialize(filterList), JSON.serialize(updatedDynamicFiltersList));
        System.assertNotEquals(null, assetListJson, 'Assets should have been retrieved');
        
        List<String> templateIdList = new List<String>();
        templateIdList.add(String.valueOf(testtemplate1.Id));
        templateIdList.add(String.valueOf(testtemplate2.Id));
        Portal_Asset_Template_Parts__c testPart1 = new Portal_Asset_Template_Parts__c(Name = 'Test Asset 1', Part_Name__c = 'Test Part 1', Portal_Asset_Template__c = testtemplate1.Id, Remaining_Cycles_applicable__c = true);
        insert testPart1;
        
        List<CommunityModuleStoreController.AssetTemplateWithParts> partsWrapperList = CommunityModuleStoreController.getPortalAssetPartsData(templateIdList);
        System.assertEquals(2, partsWrapperList.size(), 'Parts Wrappers should have been retrieved');
        System.assertEquals(1, partsWrapperList[0].partRecordsList.size(), 'Parts Template should have been retrieved');
        
        List<CommunityModuleStoreController.RequestDetailPart> partdetailList = new List<CommunityModuleStoreController.RequestDetailPart>();
        CommunityModuleStoreController.RequestDetailPart partDetail = new CommunityModuleStoreController.RequestDetailPart();
        partDetail.name = 'Test Part Detail';
        partdetailList.add(partDetail);
        
        List<CommunityModuleStoreController.RequestDetail> exchdetailList = new List<CommunityModuleStoreController.RequestDetail>();
        List<CommunityModuleStoreController.RequestDetail> detailList = new List<CommunityModuleStoreController.RequestDetail>();
        
        CommunityModuleStoreController.RequestDetail exchdetail = new CommunityModuleStoreController.RequestDetail();
        exchdetail.assetId = testAsset1.Id;
        exchdetail.assetType = testAsset1.Asset_Type__c;
        exchdetail.assetVariant = testAsset1.Asset_Variant__c;
        exchdetail.parts = partdetailList;
        exchdetail.pmaDer = true;
        CommunityModuleStoreController.RequestDetail nonexchdetail = new CommunityModuleStoreController.RequestDetail();
        nonexchdetail.assetId = testAsset1.Id;
        nonexchdetail.assetType = testAsset1.Asset_Type__c;
        nonexchdetail.assetVariant = testAsset1.Asset_Variant__c;
        nonexchdetail.parts = partdetailList;
        nonexchdetail.pmaDer = true;
        
        exchdetailList.add(exchdetail);
        detailList.add(nonexchdetail);
        
        CommunityModuleStoreController.Request req = new CommunityModuleStoreController.Request();
        req.assetClass = assetClassList[0];
        req.requestType = 'Exchange';
        req.dateStart = String.valueOf(todaysDate); 
        req.serviceType = 'Full'; 
        req.additionalRequest = 'None';
        req.requestedLocation = 'New Haven';
        req.exchangeDetails = exchdetailList;
        req.details = detailList;
        
        LW_Setup__c setting = new LW_Setup__c();
        setting.Name = 'Module Store Email Recipients';
        setting.Value__c = 'test@test.com';
        insert setting;
        
        CommunityModuleStoreController.generateRequest(JSON.serialize(req));
        
        List<Portal_Asset_Request__c> requestList = [SELECT Id, Name FROM Portal_Asset_Request__c];
        List<Asset_Request_Details__c> requestdetailsList = [SELECT Id, Name FROM Asset_Request_Details__c];
        List<Portal_Asset_Parts__c> partsList = [SELECT Id, Name FROM Portal_Asset_Parts__c];
        
        System.assertEquals(1, requestList.size(), 'Request should have been created');
        System.assertEquals(2, requestdetailsList.size(),'Request Details should have been created');
        System.assertEquals(1, partsList.size(), 'Part should have been created');
        
        Integer invocations = Limits.getEmailInvocations();
        System.assertEquals(2, invocations, 'Emails should have been sent');
        
        Test.stopTest();
        
    }
}