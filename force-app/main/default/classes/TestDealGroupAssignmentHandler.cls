@isTest
public class TestDealGroupAssignmentHandler {
    @isTest
    static void loadTestCases() {
        
        Country__c country =new Country__c(Name='United States');
        insert country;
        
  
        Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
     // opr.Country__c = 'United States';
        opr.Country_Lookup__c = country.id;
        opr.Region__c = 'Asia';
        insert opr;
        
        Marketing_Activity__c myMA = new Marketing_Activity__c(
            Name = 'My Deal',
            Prospect__c =opr.Id,
            Deal_Type__c='Sale', 
            Deal_Status__c = 'Market Intel');
        insert myMA;
        
 		Date d1 = Date.parse('03/15/2009');
        Deal_Group__c TestDealGroup1 = new Deal_Group__c(
            Name = 'Deal Group Asia ',                   
            Target_Completion_Date__c = d1,                  
            Comment__c = 'All deals in Asia'
            
        );
        insert TestDealGroup1;
        
        Deal_And_Group_Assignment__c dg = new Deal_And_Group_Assignment__c();
        dg.Deals__c = myMA.Id;
        dg.Deal_Group__c = TestDealGroup1.Id;
        insert dg;
        LeaseWareUtils.clearFromTrigger();
        
        update dg;
        
        try{
        Deal_And_Group_Assignment__c dg1 = new Deal_And_Group_Assignment__c();
        dg1.Deals__c = myMA.Id;
        dg1.Deal_Group__c = TestDealGroup1.Id;
        insert dg1;    
        }
        catch(Exception e){}
        delete dg;
    }
    
}