/*
 * This class is the controller behind the New Transaction
 * wizard. The new wizard is comprised of many pages, each of
 * which utilizes the same instance of this controller.
 */
 public class NewTransactionWizController {
 
      public NewTransactionWizController(ApexPages.StandardController controller) {
      }

      public NewTransactionWizController() {
      }


   Id leaseId = [select id from lessor__c limit 1].id; 
   Transaction__c tnx;
   Aircraft_In_Transaction__c aircraftInTnx;

   public Transaction__c getTnx() {
      if(tnx == null) tnx = new Transaction__c();
      return tnx;
   }
   
   public Aircraft_In_Transaction__c getAircraftInTnx() {
      if(aircraftInTnx == null) aircraftInTnx = new Aircraft_In_Transaction__c();
      return aircraftInTnx;
   }


   public PageReference step1() {
   	  //return new PageReference('leaseworks__TransactionWiz1');
      return Page.TransactionWiz1;
   }

   public PageReference step2() {
   	  //return new PageReference('leaseworks__TransactionWiz2');
      return Page.TransactionWiz2;
   }

   public PageReference step3() {
   		//return new PageReference('leaseworks__TransactionWiz3');
      return Page.TransactionWiz3;
   }
   
    public PageReference step4() {
    	//return new PageReference('leaseworks__TransactionWiz4');
      return Page.TransactionWiz4;
   }
   
   public PageReference step5() {
      return Page.TransactionWiz5;
  }
  
  public PageReference step6() {
      return Page.TransactionWiz6;   
      
   }


   // This method cancels the wizard, and returns the user to the 
   // Setup tab
    public PageReference cancel() {
            PageReference tnxPage = new PageReference('/' + leaseId);
            tnxPage.setRedirect(true);
            return tnxPage; 
    }

   // This method performs the final save for the two objects, and
   // then navigates the user to the detail page for the new
   // Transaction.
   	public PageReference save() {

		Savepoint spWizard = Database.setSavePoint(); 
      

      try{ 
      	  tnx.Transaction_Type__c = 'Acquisition';
      	  insert tnx;

     
      	  	aircraftInTnx.Transaction__c = tnx.id;
      	  	insert aircraftInTnx;

     

          	PageReference transactionPage = new PageReference('/' +tnx.id);
      	  	transactionPage.setRedirect(true);

          	return transactionPage;
		}catch(exception e){
        	Database.rollback(spWizard);
        
        	tnx=tnx.clone(false);
        	aircraftInTnx=aircraftInTnx.clone(false);
        
        	ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,  e.getMessage()));
        	return ApexPages.currentPage();
     	}

   }

}