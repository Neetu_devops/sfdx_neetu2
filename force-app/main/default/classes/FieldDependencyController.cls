public class FieldDependencyController {
    @AuraEnabled 
  	public static Map<String,List<String>> getPicklistValues(String objectName, string fieldName, string recordTypeName) {
        String prefix = leasewareutils.getNamespacePrefix();        
        fieldName = fieldName.replace(prefix, '');
        objectName = objectName.replace(prefix, '');
        System.debug('getPicklistValues : fieldName :'+fieldName+ 'objectName :'+objectName);
        return UIAPIServices.fetchPicklistValues(objectName,fieldName,recordTypeName);
    }
}