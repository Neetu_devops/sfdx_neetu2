public class IssuanceWithInvestorTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'IssuanceWithInvestorTriggerHandlerBefore';
    private final string triggerAfter = 'IssuanceWithInvestorTriggerHandlerAfter';
    public IssuanceWithInvestorTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('IssuanceWithInvestorTriggerHandler.beforeInsert(+)');
		

	    system.debug('IssuanceWithInvestorTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('IssuanceWithInvestorTriggerHandler.beforeUpdate(+)');
    	
    	
    	system.debug('IssuanceWithInvestorTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('IssuanceWithInvestorTriggerHandler.beforeDelete(+)');
    	
    	UpdateCounterParty();
    	system.debug('IssuanceWithInvestorTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceWithInvestorTriggerHandler.afterInsert(+)');
    	UpdateCounterParty();
    	system.debug('IssuanceWithInvestorTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceWithInvestorTriggerHandler.afterUpdate(+)');
    	UpdateCounterParty();
    	
    	system.debug('IssuanceWithInvestorTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceWithInvestorTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('IssuanceWithInvestorTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceWithInvestorTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('IssuanceWithInvestorTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	private void UpdateCounterParty(){
		
		list<List_of_Investors__c> listNew = (list<List_of_Investors__c>)(trigger.IsDelete?trigger.old:trigger.new);
		set<id> setCPIds = new set<id>();
		set<id> setDelds = new set<id>();
		for(List_of_Investors__c currec :listNew){
			setCPIds.add(currec.Counterparty__c);
			if(trigger.IsDelete)setDelds.add(currec.Id);
		}
		AggregateResult[] arList = [select Counterparty__c cparty,sum(Allocation_mm__c) sumAl
				from List_of_Investors__c 
				where (Counterparty__c in :setCPIds) and (id not in :setDelds)
				group by Counterparty__c];
		map<string,Decimal> mapSumBasedOnCP = new map<string,Decimal>();		
		for(AggregateResult ar:arList){
			mapSumBasedOnCP.put(String.valueOf(ar.get('cparty')),(Decimal)ar.get('sumAl'));
		}		
		Bank__c[] CPList = [select id from Bank__c where id in :setCPIds
							];
		for(Bank__c curRec:CPList){
			curRec.Total_Value__c = mapSumBasedOnCP.get(curRec.Id);
		}		
		update CPList;			
	}

}