public with sharing class ConsttAssemblyClaimFormController {
  
    private final Constt_Assembly_Eligible_Event_Invoice__c Aee_In;
    private final Assembly_Eligible_Event__c Aee;
    private final Constituent_Assembly__c ConstAssemb;
    private final Aircraft__c parentAC; 
     
    public ID AeeIn_Id {get;set;}
    public ID Aee_Id;
    private ID parentAC_ID;
    private ID CA_ID;
    
    public string Logo_URL;
    public String email {get;set;}
    public String sub {get;set;}
    public String ebody {get;set;}

 
    public ConsttAssemblyClaimFormController() {
    
        Logo_URL =  LeaseWareUtils.getLogoURL();
        
         List<Constt_Assembly_Eligible_Event_Invoice__c>   AeeIn_list = [select id, Constt_Assembly_Eligible_Event__c, 
                                                                        Invoice_Amount__c,Invoice_Date__c,Name
                                                                        from Constt_Assembly_Eligible_Event_Invoice__c where 
                                                                             id =  :ApexPages.currentPage().getParameters().get('id')];
        if (AeeIn_list.size() > 0)
            {
                Aee_In = AeeIn_list.get(0);
                
                AeeIn_Id = Aee_In.id;
                
                Aee_Id = Aee_In.Constt_Assembly_Eligible_Event__c;
                
            }
            
        List<Assembly_Eligible_Event__c>   Aee_list = [select id, Constituent_Assembly__c, 
                                                    Balance_Amount__c, Claim_Amount__c, 
                                                    CSN__c, Invoiced_Amount__c, 
                                                    Start_Date__c,TSN__c, 
                                                    Type_P__c, End_Date__c,Name
                                                    from Assembly_Eligible_Event__c where id =:Aee_Id];
       
       if (Aee_list.size() > 0)
            {
                Aee = Aee_list.get(0);
                
                Aee_Id = Aee.id;
                CA_ID = Aee.Constituent_Assembly__c;
                
            }
            
      List<Constituent_Assembly__c> ConstAssemblist = [select id,name, TSN__c, TSLV__c, CSN__c,CSLV__c, Type__c, Attached_Aircraft__c 
                                                      from Constituent_Assembly__c where id = :CA_ID];
                                                          
      if (ConstAssemblist.size() > 0)
            {
                ConstAssemb = ConstAssemblist.get(0);
                
               parentAC_ID =  ConstAssemb.Attached_Aircraft__c;
                
            }
            
      List<Aircraft__c>   Ac_spec = [select id, Logo__c, Aircraft_Type__c, Assigned_To__c, Country_Of_Registration__c,MSN_Number__c, 
                                                  CSN__c, Lease__c,Approach_Landing_Cat__c, 
                                                  Number_of_engines__c, Registration_Number__c,ETOPS__c,Line_Num__c, 
                                                  Time_Remaining_To_Check__c,TSN__c
                                             from Aircraft__c where id =  :parentAC_ID];
                                                                                                          
       if (Ac_spec.size() > 0)
            {
                parentAC = Ac_spec.get(0);
            }
         
      
  }
  
  public Constt_Assembly_Eligible_Event_Invoice__c getAee_In() {
        return Aee_In;
    }
    
    public Assembly_Eligible_Event__c getAee() {
        return Aee;
    }
    public Aircraft__c getparentAC() {
        return parentAC;
    }
    
     public string getLogo_URL()
    {
      
        return Logo_URL;
    }
    
    
	static testMethod void testMRInvController() {
    	map<String,Object> mapInOut = new map<String,Object>();
    	mapInOut.put('Maintenance_Program__c.Historical_Utilization_Months__c',10) ;
    	TestLeaseworkUtil.createMP(mapInOut); 
		Id MPId = (Id)mapInOut.get('Maintenance_Program__c.Id');
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Engine') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
    	TestLeaseworkUtil.createMPDetail(mapInOut); 

		LeaseWareUtils.unsetFromTrigger();
	    LeaseWareUtils.clearFromTrigger();    	
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','APU') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
    	TestLeaseworkUtil.createMPDetail(mapInOut); 
    	    	
		LeaseWareUtils.unsetFromTrigger();
	    LeaseWareUtils.clearFromTrigger();    	
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c',LeaseWareUtils.C_LG_M) ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
    	TestLeaseworkUtil.createMPDetail(mapInOut);   
		LeaseWareUtils.unsetFromTrigger();
	    LeaseWareUtils.clearFromTrigger();    	
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c',LeaseWareUtils.C_LG_W) ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
    	TestLeaseworkUtil.createMPDetail(mapInOut);  
		LeaseWareUtils.unsetFromTrigger();
	    LeaseWareUtils.clearFromTrigger();    	
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c',LeaseWareUtils.C_LG_NOSE) ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
    	TestLeaseworkUtil.createMPDetail(mapInOut);      	    	  	
		LeaseWareUtils.unsetFromTrigger();
	    LeaseWareUtils.clearFromTrigger();      	
	        	
    	TestLeaseworkUtil.createLessor(mapInOut); 
    	TestLeaseworkUtil.createTSLookup(mapInOut);        
        // Set up the Operator record.
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        LeaseWareUtils.clearFromTrigger();insert operator;
        // Set up the Lease record
        //Date dateToday = Date.parse('01/30/2009');
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10',Lease_Start_Date_New__c=Date.parse('01/01/2010'),Lease_Start_Date__c=Date.parse('01/01/2010'), Lease_End_Date__c=Date.parse('12/31/2012'), Lease_End_Date_New__c=Date.parse('12/31/2012'),Rent_Date__c=Date.parse('01/01/2010'), Rent_End_Date__c=Date.parse('12/31/2012'));
        LeaseWareUtils.clearFromTrigger();insert lease;
        // Set up the Aircraft record.
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(Maintenance_Program__c = 	MPId,MSN_Number__c=aircraftMSN,Date_of_Manufacture__c = System.today(), Aircraft_Type__c='737', Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000,  Status__c='Available');
        LeaseWareUtils.clearFromTrigger();insert a;
        
        lease.aircraft__c=a.id;
        LeaseWareUtils.clearFromTrigger();update lease;
    
        
        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');    
        Aircraft_Eligible_Event__c assEE= new Aircraft_Eligible_Event__c(Name= 'ca test', 
        	Aircraft__c=a.Id, Start_Date__c=dateStart, End_Date__c=dateEnd, InitClaimAmt_MR_Engine_1__c=1000000,TSN_N__c = 100, CSN__c = 100,Event_Type__c='Heavy Maintenance 1');
                   LeaseWareUtils.clearFromTrigger();insert assEE;
       Aircraft_Eligible_Event_Invoice__c invoice = new Aircraft_Eligible_Event_Invoice__c(Invoice_Date__c=Date.parse('02/12/2009'),  Aircraft_Eligible_Event__c = assEE.Id);
                LeaseWareUtils.clearFromTrigger();insert invoice;
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= a.Id, Description__c ='test description', TSN__c=0, CSN__c=0, TSLV__c=0, CSLV__c= 0, Type__c ='Other', Serial_Number__c='123');
                            LeaseWareUtils.clearFromTrigger();insert consttAssembly;
        
    
       Assembly_Eligible_Event__c aEE= new Assembly_Eligible_Event__c(
           Event_Type__c='Performance Restoration',
        	Constituent_Assembly__c=consttAssembly.Id, Start_Date__c=dateStart, End_Date__c=dateEnd,  Claim_Amount__c=1000000,TSN__c = 100, CSN__c = 100);
                LeaseWareUtils.clearFromTrigger();insert aEE;
              
        Constt_Assembly_Eligible_Event_Invoice__c caInvoice = new Constt_Assembly_Eligible_Event_Invoice__c(Invoice_Date__c=Date.parse('02/12/2009'),
                                             Assembly_Amount__c=1000000,Status__c='Approved',Constt_Assembly_Eligible_Event__c=aEE.Id);
                        LeaseWareUtils.clearFromTrigger();
		try{    
            insert caInvoice;
        }
        catch(DmlException e) {
            System.debug('The Exception/Error occured and this is expected because of new functionality MR Share added when we clamming Event Invoice. This testcase should be get pass thats why added try/catch block.');
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
        System.debug('*************************************** aEE is '+aEE);
        ApexPages.currentPage().getParameters().put('id', caInvoice.Id);
        ConsttAssemblyClaimFormController controller = new ConsttAssemblyClaimFormController();
        System.debug('*************************************** attaching mail');
        controller.getAee_In();
        controller.getAee();
        controller.getparentAC();
        controller.getLogo_URL();
        controller.email='';
        try{
        System.assertEquals(controller.sendPdf(), null);
        }catch(exception e){
            System.debug('Expected.. Ignore');
        }    

          
    }
    
    
    public PageReference sendPdf() {
     
    PageReference pdf = Page.ConsttAssembClaimFormPDF;
    // add parent id to the parameters for standardcontroller
    pdf.getParameters().put('id',Aee_In.id);
 
    // the contents of the attachment from the pdf
    Blob body;
 
    // returns the output of the page as a PDF
    body = Test.isRunningTest()?Blob.ValueOf('dummy text'):pdf.getContentAsPDF();
 
    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
    attach.setContentType('application/pdf');
    attach.setFileName('CA_ClaimFormPDF.pdf');
    attach.setInline(false);
    attach.Body = body;
 
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setUseSignature(false);

    if(Test.isRunningTest()){
        mail.setToAddresses(new String[] {'a@lw.com'});
    }else{
        mail.setToAddresses(new String[] { email });
    }

    mail.setSubject(sub);
    mail.setplainTextBody(ebody);
    mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
 
    // Send the email
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with Constituent Assembly Claim Report sent to '+email));
 
    return null;
 
  }
 
}