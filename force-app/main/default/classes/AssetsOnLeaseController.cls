public without sharing class AssetsOnLeaseController {

    @AuraEnabled
    public static ContainerWrapper getData(Boolean availableAssets){
        
        ContainerWrapper container = new ContainerWrapper();
        String communityPrefix = UtilityWithoutSharingController.getCommunityPrefix();
        List<Operator__c> operators = UtilityWithSharingController.getOperators();
        
        String qry = 'select id, recordType.DeveloperName, Current_Location__c, Aircraft_Variant__c, Aircraft_Type__c,';
        	  qry += 'Registration_Number__c, Lease__r.Lease_Id_External__c,';
        	  qry += 'Vintage__c, MSN_Number__c, ';
        	  qry += 'Engine_Type__c, Lease__r.Operator__r.Name,';
              qry += 'Aircraft_Series__c,Lease__r.Lessee__r.Name,';
              qry += 'Lease__r.Lease_Start_Date_New__c, CSN__c,';
              qry += 'Lease__r.Lease_End_Date_New__c, TSN__c,';
              qry += 'Type_Variant__c,';
              qry += '(select id, Type__c, Serial_Number__c,';
              qry += 'Current_TS__r.Name, TSO_External__c, CSO_External__c,';
              qry += 'Lowest_LLP_Limiter_Current_TS__c, Lowest_Limiter_LLP__r.Name ';
              qry += 'from Constituent_Assemblies__r where Replaced_External__c = false), ';

         	  qry += '(select id, Type__c, Serial_Number__c,';
              qry += 'Current_TS__r.Name, TSO_External__c, CSO_External__c,';
              qry += 'Lowest_LLP_Limiter_Current_TS__c, Lowest_Limiter_LLP__r.Name ';
              qry += 'from Assemblies__r where Replaced_External__c = false) ';
              qry += 'from Aircraft__c ';
        
        if(availableAssets){
            qry += 'where Asset_Status_External__c = \'Available\' ';
            qry += 'and (Lease__c = null or Lease__r.Lease_Status_External__c != \'Active\')';
        }else{
             qry += 'where Status__c != \'Sold\' '; 
             qry += 'and Lease__c != null and Lease__r.Lease_Status_External__c = \'Active\' ';
             qry += 'and Lease__r.lessee__c in : operators';
        }
        
        for(Aircraft__c asset : Database.query(qry)){
            
            if(asset.recordType.DeveloperName == 'Aircraft'){ 
                
                String engine1, engine2, engine3, engine4;
                String msnURL = communityPrefix + '/s/aircraft-details?recordId=' + UtilityWithoutSharingController.encodeData(asset.Id); 
                
                for(Constituent_Assembly__c assembly : asset.Assemblies__r){
                    
                    if(assembly.Type__c == 'Engine 1'){
                        engine1 = assembly.Serial_Number__c; 
                    }else if(assembly.Type__c == 'Engine 2'){
                        engine2 = assembly.Serial_Number__c;
                    }else if(assembly.Type__c == 'Engine 3'){
                        engine3 = assembly.Serial_Number__c;
                    }else if(assembly.Type__c == 'Engine 4'){
                        engine4 = assembly.Serial_Number__c;
                    }
                }
               
                container.aircraftList.add(new AircraftWrapper(asset.MSN_Number__c, msnURL, asset.Type_Variant__c,
                                                         asset.Aircraft_Variant__c, asset.Vintage__c, 
                                                         asset.Registration_Number__c,asset.Engine_Type__c,
                                                         engine1, engine2, engine3, engine4,
                                                    	 asset.Lease__r.Lease_Id_External__c,
                                                         asset.Lease__r.Lessee__r.Name, 
                                                         asset.Lease__r.Operator__r.Name,
                                                         asset.Lease__r.Lease_Start_Date_New__c,
                                                         asset.Lease__r.Lease_End_Date_New__c)); 
            }else{
                
                String currentThrust, limitingPart;
                Decimal tso, cso, limiter;
                String esnURL = communityPrefix + '/s/engine-details?recordId=' + UtilityWithoutSharingController.encodeData(asset.Id);
                
                if(asset.Constituent_Assemblies__r.size()>0){
                	currentThrust = asset.Constituent_Assemblies__r[0].Current_TS__r.Name;
                    tso = asset.Constituent_Assemblies__r[0].TSO_External__c;
                    cso = asset.Constituent_Assemblies__r[0].CSO_External__c;
                    limiter = asset.Constituent_Assemblies__r[0].Lowest_LLP_Limiter_Current_TS__c;
                    limitingPart = asset.Constituent_Assemblies__r[0].Lowest_Limiter_LLP__r.Name;
                }
                
                container.engineList.add(new EngineWrapper(asset.MSN_Number__c, esnURL, asset.Engine_Type__c,
                                                           asset.Aircraft_Series__c, 
                                                           currentThrust, asset.TSN__c,asset.CSN__c,
                                                           tso, cso, limiter, limitingPart, asset.Lease__r.Lease_Id_External__c,
														   asset.Lease__r.Lessee__r.Name, 
                                                           asset.Lease__r.Operator__r.Name,
                                                           asset.Lease__r.Lease_Start_Date_New__c,
                                                           asset.Lease__r.Lease_End_Date_New__c, asset.Current_Location__c));
                
            }    

        }

        return container;
    } 
    
    public class ContainerWrapper{
        @AuraEnabled public List<AircraftWrapper> aircraftList;
        @AuraEnabled public List<EngineWrapper> engineList;
        
        public ContainerWrapper(){
            aircraftList = new List<AircraftWrapper>();
            engineList =  new List<EngineWrapper>();
        }
    }
    
    public class AircraftWrapper{
        
        @AuraEnabled public String msn;
        @AuraEnabled public String msnURL;
        @AuraEnabled public String aircraftType;
        @AuraEnabled public String assetVariant;
        @AuraEnabled public String vintage;
        @AuraEnabled public String registration;
        @AuraEnabled public String aircraftEngineType;   
        @AuraEnabled public String esn1;
        @AuraEnabled public String esn2;
        @AuraEnabled public String esn3;
        @AuraEnabled public String esn4;
 		@AuraEnabled public String leaseNo;
 		@AuraEnabled public String lesseeName;
 		@AuraEnabled public String operatorName; 
 		@AuraEnabled public Date leaseStart;
 		@AuraEnabled public Date leaseEnd;
        
        public AircraftWrapper(String msn, String msnURL, String aircraftType, String assetVariant, String vintage, String registration, String aircraftEngineType,
                               String esn1, String esn2, String esn3, String esn4, String leaseNo, String lesseeName,
                               String operatorName, Date leaseStart, Date leaseEnd){
                               
        	this.msn = msn;
            this.msnURL = msnURL;
            this.aircraftType = aircraftType;
            this.assetVariant = assetVariant;
            this.vintage = (vintage != null ? Vintage.left(4) : null);
            this.registration = registration;          
            this.aircraftEngineType = aircraftEngineType;
            this.esn1 = esn1 == null ? 'N/A' : esn1;
            this.esn2 = esn2 == null ? 'N/A' : esn2;
            this.esn3 = esn3 == null ? 'N/A' : esn3;
            this.esn4 = esn4 == null ? 'N/A' : esn4;
            this.leaseNo = leaseNo;
            this.lesseeName = lesseeName;
            this.operatorName = operatorName;  
            this.leaseStart = leaseStart; 
            this.leaseEnd =  (leaseEnd == Date.newInstance(2200, 01, 01) ? null : leaseEnd);
        }
    }
    
     public class EngineWrapper{
        
        @AuraEnabled public String esn;
        @AuraEnabled public String esnURL;
        @AuraEnabled public String engineType;
        @AuraEnabled public String assetSeries ;
        @AuraEnabled public String currentThrust ;
        @AuraEnabled public Decimal tsn ;
        @AuraEnabled public Decimal csn;   
        @AuraEnabled public Decimal tso ;
        @AuraEnabled public Decimal cso;
        @AuraEnabled public Decimal limiter ;
        @AuraEnabled public String limitingPart;
 		@AuraEnabled public String leaseNo;
 		@AuraEnabled public String lesseeName;
 		@AuraEnabled public String operatorName; 
 		@AuraEnabled public Date leaseStart;
         @AuraEnabled public Date leaseEnd;
         @AuraEnabled public String currentLocation;
        
        public EngineWrapper(String esn, String esnURL, String engineType, String assetSeries, String currentThrust, Decimal tsn, Decimal csn,
                               Decimal tso, Decimal cso, Decimal limiter, String limitingPart, String leaseNo, String lesseeName,
                               String operatorName, Date leaseStart, Date leaseEnd, String currentLocation){
                               
        	this.esn = esn;
            this.esnURL = esnURL;  
            this.engineType = engineType;
            this.assetSeries = assetSeries;
            this.currentThrust = currentThrust;
            this.tsn = tsn;          
            this.csn = csn;
            this.tso = tso;
            this.cso = cso;
            this.limiter = limiter;
            this.limitingPart = limitingPart;
            this.leaseNo = leaseNo;
            this.lesseeName = lesseeName;
            this.operatorName = operatorName;  
            this.leaseStart = leaseStart; 
            this.leaseEnd =  (leaseEnd == Date.newInstance(2200, 01, 01) ? null : leaseEnd);
            this.currentLocation = currentLocation;
        }
    }
    
}