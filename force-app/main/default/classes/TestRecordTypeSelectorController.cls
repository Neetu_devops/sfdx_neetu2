/**
*******************************************************************************
* Class: RecordTypeSelectorController
* @author Created by Bhavna, Lease-Works, 
* @date  6/12/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to find Record Types of a given Object
*
* History:
* - VERSION   DEVELOPER NAME    	DATE         	DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestRecordTypeSelectorController {    
    /**
    * @description null.
    *  	 
    * @return null
    */
    @testSetup 
    static void setup(){
        
    }
    
    
    
    /**
    * @description This method gives the code coverage of getRecordTypes method of RecordTypeSelectorController.G
    * 
    * @return null
    */
    @isTest static void testRecordTypeSelectorController() {        
        RecordTypeSelectorController.RecordTypeWrapper rtWrapper =  RecordTypeSelectorController.getRecordTypes(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        system.assertNotEquals(null, rtWrapper);
        
    }
}