@isTest
private class TestPDPCostRevisionHandler {

	@testSetup static void setUp() {

		List<ECI_Index__c> indexes = createIndex('ECI336411W', 1);
		indexes.addAll(createIndex('WPU03THRU15', 1));
		indexes.addAll(createIndex('WPU10', 1));
		indexes.addAll(createIndex('CUUR0000SA0', 1));
		indexes.addAll(createIndex('CIU2013000000000I', 1));

		insert indexes;

	}
			

	@isTest static void testAirbusCostRevision() {
		
		Date dte = Date.newInstance(2018, 2, 1);
		Date oneMonthBefore = dte.addMonths(-1);
		Date oneMonthLater = dte.addMonths(1);

		List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'Airbus', 
			'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
		
		update crf;

		List<Revision_Factor__c> rfs = [ SELECT Id, Revision_Month__c, Cost_Revision__c, Name, Revision_Factor__c FROM Revision_Factor__c WHERE Cost_Revision__c =: crf[0].Id ];
		System.assertEquals(rfs[0].Revision_Factor__c, 1);
	}
	
	@isTest static void testAirbusCreditCostRevision() {
		
		Date dte = Date.newInstance(2018, 2, 1);
		Date oneMonthBefore = dte.addMonths(-1);
		Date oneMonthLater = dte.addMonths(1);
		String itype = 'Airbus (Credit)';

		List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => itype, 
			'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});

		update crf;

		List<Revision_Factor__c> rfs = [ SELECT Id, Revision_Month__c, Cost_Revision__c, Name, Revision_Factor__c FROM Revision_Factor__c WHERE Cost_Revision__c =: crf[0].Id ];
		System.assertEquals(rfs[0].Revision_Factor__c, 1);
	}

	@isTest static void testBoeingCostRevision() {
		
		Date dte = Date.newInstance(2018, 2, 1);
		Date oneMonthBefore = dte.addMonths(-1);
		Date oneMonthLater = dte.addMonths(1);

		List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'Boeing', 
			'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
		
		update crf;

		List<Revision_Factor__c> rfs = [ SELECT Id, Revision_Month__c, Cost_Revision__c, Name, Revision_Factor__c FROM Revision_Factor__c WHERE Cost_Revision__c =: crf[0].Id ];
		System.assertEquals(rfs[0].Revision_Factor__c, 1);
	}

	@isTest static void testRollsRoyceCostRevision() {
		
		Date dte = Date.newInstance(2018, 2, 1);
		Date oneMonthBefore = dte.addMonths(-1);
		Date oneMonthLater = dte.addMonths(1);

		List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'Rolls-Royce', 
			'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
		
		update crf;

		List<Revision_Factor__c> rfs = [ SELECT Id, Revision_Month__c, Cost_Revision__c, Name, Revision_Factor__c FROM Revision_Factor__c WHERE Cost_Revision__c =: crf[0].Id ];
		System.assertEquals(rfs[0].Revision_Factor__c, 1);
	}

	@isTest static void testCFMCostRevision() {
		
		Date dte = Date.newInstance(2018, 2, 1);
		Date oneMonthBefore = dte.addMonths(-1);
		Date oneMonthLater = dte.addMonths(1);

		List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'CFM', 
			'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
		
		update crf;

		List<Revision_Factor__c> rfs = [ SELECT Id, Revision_Month__c, Cost_Revision__c, Name, Revision_Factor__c FROM Revision_Factor__c WHERE Cost_Revision__c =: crf[0].Id ];
		System.assertEquals(rfs[0].Revision_Factor__c, 0.008);
	}

	@isTest static void testPWCostRevision() {
		
		Date dte = Date.newInstance(2018, 2, 1);
		Date oneMonthBefore = dte.addMonths(-1);
		Date oneMonthLater = dte.addMonths(1);

		List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'PW', 
			'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
		
		update crf;

		List<Revision_Factor__c> rfs = [ SELECT Id, Revision_Month__c, Cost_Revision__c, Name, Revision_Factor__c FROM Revision_Factor__c WHERE Cost_Revision__c =: crf[0].Id ];
		System.assertEquals(rfs[0].Revision_Factor__c, 1);
	}


	private static List<ECI_Index__c> createIndex(String itype, Decimal indx) {

		List<ECI_Index__c> lstIndxs = new List<ECI_Index__c>();

		Map<Integer,String> allMonths = new Map<Integer,String>{1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May',
        6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'};

        for ( Integer i : allMonths.keyset() ) {
        	for ( Integer year = 16; year < 19; year++ ) {
        		String mth = allMonths.get(i) + '-' + year;
        		ECI_Index__c index = new ECI_Index__c();
				index.Type__c = itype;
				index.Index__c = indx;
				index.Month__c = mth;
				lstIndxs.add(index);
        	}
        }

		return lstIndxs;
	}
}