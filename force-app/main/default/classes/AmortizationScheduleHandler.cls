public class AmortizationScheduleHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string  triggerBefore = 'AmortizationScheduleHandlerBefore';
    private final string  triggerAfter = 'AmortizationScheduleHandlerAfter';
    public AmortizationScheduleHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('AmortizationScheduleHandler.beforeInsert(+)');
    	Amortization_Schedule__c[] listNew = (list<Amortization_Schedule__c>)trigger.new ;
    	for(Amortization_Schedule__c curRec:listNew){
    		if(!LeaseWareUtils.isFromTrigger('InsertedFromFinancingHandler')) {
    			curRec.addError('You are not allowed to create Amortization records.');
    		}
			
    	}    	
    	
		
	    system.debug('AmortizationScheduleHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AmortizationScheduleHandler.beforeUpdate(+)');
    	Amortization_Schedule__c[] listNew = (list<Amortization_Schedule__c>)trigger.new ;
    	for(Amortization_Schedule__c curRec:listNew){
    			curRec.addError('You are not allowed to modify Amortization records.');
    	}     	
    	
    	system.debug('AmortizationScheduleHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AmortizationScheduleHandler.beforeDelete(+)');
    	Amortization_Schedule__c[] listNew = (list<Amortization_Schedule__c>)trigger.old ;
    	for(Amortization_Schedule__c curRec:listNew){
    		if(!LeaseWareUtils.isFromTrigger('DeletedFromFinancingHandler')) {
    			curRec.addError('You are not allowed to delete Amortization records.');
    		}
			
    	}      	
    	
    	system.debug('AmortizationScheduleHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AmortizationScheduleHandler.afterInsert(+)');
    	
    	system.debug('AmortizationScheduleHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AmortizationScheduleHandler.afterUpdate(+)');
    	
    	
    	system.debug('AmortizationScheduleHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AmortizationScheduleHandler.afterDelete(+)');
    	
    	
    	system.debug('AmortizationScheduleHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AmortizationScheduleHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('AmortizationScheduleHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


}