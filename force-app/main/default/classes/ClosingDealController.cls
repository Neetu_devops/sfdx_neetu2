/**
*******************************************************************************
* Class: ClosingDealController
* @author Created by Bhavna, Lease-Works, 
* @date 9/13/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic when deal is made inactive.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*
********************************************************************************
*/
public class ClosingDealController {
    /**
    * @description Get the columns list and records of Asset in Deals to create a datatable for UI
    *
    * @return cdw - ClosingDealWrapper, list of ClosingDealWrapper records
    */
    @AuraEnabled
    public static ClosingDealWrapper getAssetInDeals(String fieldSetName, String editableFields, String recordId) {
        String query = 'Select Id, Name';
        
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Aircraft_Proposal__c');
        Schema.FieldSet assetFieldSet = RLUtility.describeFieldSet(objectDescribe, fieldSetName);
    	
        if(assetFieldSet!=null){
        	for(Schema.FieldSetMember fsm : assetFieldSet.getFields()) {
            	if(query.indexOf(fsm.getFieldPath()) == -1) {
                	query += ', ' + fsm.getFieldPath();
            	}            
        	}
        }
        
        query += ' from ' + leasewareutils.getNamespacePrefix() + 'Aircraft_Proposal__c where ' + leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c = :recordId order by CreatedDate';
        
        ClosingDealWrapper cdw = new ClosingDealWrapper();        
        cdw.colList = getResultColumns(fieldSetName, editableFields);
        cdw.assetDealList = Database.query(query);
        cdw.dealName = [Select Name from Marketing_Activity__c where Id = :recordId].Name;
        
    /*  if(cdw.assetDealList.size() > 0) {
            Marketing_Activity__c deal = new Marketing_Activity__c(Id = recordId, 
                                                                   CloseDealConfirmed__c = false, 
                                                                   Inactive_MA__c = false);
            update deal;
        }  */        
        
        return cdw;
    }
    
    
	/**
    * @description Retreives all the fields given in the fieldset mentioned and creates a header for search results
    *
    * @return colList - List<ColumnWrapper>, list of ColumnWrapper records
    */
    private static List<ColumnWrapper> getResultColumns(String fieldSetName, String editableFields) {
        List<ColumnWrapper> colList = new List<ColumnWrapper>();
        List<String> editableFieldList = editableFields.split(',');
        Set<String> editableFieldSet = new Set<String>();
        
        //Creating set of editable fields
        for(String fieldName : editableFieldList) {
            editableFieldSet.add(fieldName.trim());
        }        
        
		//Retrieving Asset in Deal fieldset
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Aircraft_Proposal__c');
        Schema.FieldSet assetFieldSet = RLUtility.describeFieldSet(objectDescribe, fieldSetName);
    	
        ColumnWrapper cwName = new ColumnWrapper('Name', 'Name', 'text', false);            
        colList.add(cwName);
        
        if(assetFieldSet!=null){
            for(Schema.FieldSetMember fsm : assetFieldSet.getFields()) {
                Boolean editable = false;
            
                 //creating columnwrapper list for Result Table headers
                if(fsm.getFieldPath() != 'Name') {
                    if(editableFieldSet.contains(fsm.getFieldPath())) {
                            editable = true;
                    }
                
                    ColumnWrapper cw = new ColumnWrapper(fsm.getLabel(), fsm.getFieldPath(), String.valueOf(fsm.getType()).toLowerCase(), editable);            
            	    colList.add(cw);
                }
            } 
        }
        
        return colList;
    }
    
    
    /**
    * @description Save all the Asset in Deals and related Deal with required updated values sent by the Component
    *
    * @return String, success message
    */
    @AuraEnabled
    public static String saveAssetInDeals(List<Aircraft_Proposal__c> assetList, Boolean confirmed, Boolean updatedOffer, 
                                          String reasonForClosing, String closingComments, String recordId) {
        if(assetList.size() > 0) {
            for(Aircraft_Proposal__c asset : assetList) {
                asset.Confirmed__c = confirmed;
                asset.Updated_Offer__c = updatedOffer;
            }
            
            update assetList;
        }        
        
        if(recordId != null) {
            Marketing_Activity__c deal = new Marketing_Activity__c(Id = recordId, 
                                                                   CloseDealConfirmed__c = true, 
                                                                   Inactive_MA__c = true,
                                                                   Reason_For_Closing_Deal__c = reasonForClosing,
                                                                   Deal_Closing_Comments__c = closingComments);
            update deal;
        }        
        
        return 'Success';
    }
    
    
    /**
    * @description Update Deal to active again if popup is closed
    *
    * @return null
    */
    @AuraEnabled
    public static void updateDeal(String recordId) {
        if(recordId != null) {
            Marketing_Activity__c deal = new Marketing_Activity__c(Id = recordId, 
                                                                   CloseDealConfirmed__c = false, 
                                                                   Inactive_MA__c = false);
            update deal;
        } 
    }
    
    
    /**
    * @description ClosingDealWrapper to hold details related to headers and Asset in Deal data for ClosingDealComponent
    *
    */
    public class ClosingDealWrapper {
        @AuraEnabled public List<Aircraft_Proposal__c> assetDealList {get; set;}
        @AuraEnabled public List<ColumnWrapper> colList {get; set;}
        @AuraEnabled public String dealName {get; set;}
        
        public ClosingDealWrapper() {
            this.assetDealList = new List<Aircraft_Proposal__c>();
            this.colList = new List<ColumnWrapper>();
        }
    }
    
        
    /**
    * @description ColumnWrapper to hold details related to headers of the Operator Data
    *
    */
    public class ColumnWrapper {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public Boolean editable {get; set;}
        
        public ColumnWrapper(String label, String fieldName, String type, Boolean editable) {
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
            this.editable = editable;
        }
    }
}