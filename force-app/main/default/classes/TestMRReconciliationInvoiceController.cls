@isTest
public class TestMRReconciliationInvoiceController {
    @TestSetup
    static Void testSetup(){
        
        Date lastDayOfMonth = System.today().toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                  
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,        
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                        
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = minStartDate,
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = minStartDate,
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        
        insert testAircraft;
        
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.Id; 
        update leaseRecord;
        
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                          Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                                          Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
        LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
        Constituent_Assembly__c consituentAssemblyRecord = new Constituent_Assembly__c();
        consituentAssemblyRecord.Serial_Number__c = '2';
        //consituentAssemblyRecord.CSLV__c = 2;
        consituentAssemblyRecord.CSN__c = 3;
        //consituentAssemblyRecord.TSLV__c = 4;
        consituentAssemblyRecord.TSN__c = 5;
        consituentAssemblyRecord.Last_CSN_Utilization__c = 3;
        consituentAssemblyRecord.Last_TSN_Utilization__c = 5;
        consituentAssemblyRecord.Attached_Aircraft__c = testAircraft.Id;
        consituentAssemblyRecord.Asset__c = testAircraft.Id;
        consituentAssemblyRecord.Name ='Testing';
        consituentAssemblyRecord.Type__c ='Airframe'; 
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert consituentAssemblyRecord ;
        LWGlobalUtils.setTriggersflagOff();
        
        Ratio_Table__c testRatioTable = new Ratio_Table__c(Name = 'Test Ratio Table');
        insert testRatioTable;
        
        Assembly_Event_Info__c assemblyEventInfoObj=new Assembly_Event_Info__c(
            Name='Airframe-Airframe-Heavy 1',
            Constituent_Assembly__c = consituentAssemblyRecord.Id
        );
        insert assemblyEventInfoObj;
        
        Assembly_MR_Rate__c assemblyMrRecord = new Assembly_MR_Rate__c();
        assemblyMrRecord.Name ='TestMrRecord';
        assemblyMrRecord.Lease__c =leaseRecord.Id;
        assemblyMrRecord.Base_MRR__c =200;
        assemblyMrrecord.Escalation_Type__c='Advanced';
        assemblyMrRecord.Rate_Basis__c='Monthly';
        assemblyMrRecord.Assembly_Type__c ='Engine 3';
        assemblyMrRecord.Assembly_Lkp__c = consituentAssemblyRecord.Id;
        assemblyMrRecord.Assembly_Event_Info__c = assemblyEventInfoObj.Id;
        assemblyMrRecord.Ratio_Table__c = testRatioTable.Id;
        assemblyMrRecord.FH_FC_Ratio__c = 1.07;
        assemblyMrRecord.Annual_Escalation__c = 1;
       
        
        insert assemblyMrRecord;  
        
         Utilization_Report__c firstUtilizationRecord = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = minStartDate.toStartOfMonth().addMonths(1).addDays(-1),
            Status__c='Approved By Lessor'
        );
        insert firstUtilizationRecord;
        
        Id recordTypeId = Schema.SObjectType.Reconciliation_Schedule__c.getRecordTypeInfosByDeveloperName().get('Default').getRecordTypeId();
        
        Date startDate = Date.newInstance(lastDayOfMonth.year() - 2, lastDayOfMonth.month() + 1, 1);
        Date endDate = Date.newInstance(lastDayOfMonth.year() - 1, lastDayOfMonth.month(), date.daysInMonth(lastDayOfMonth.year() - 1, lastDayOfMonth.month()));
        
        Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = endDate,
            Status__c='Approved By Lessor'
        );
        insert utilizationRecord1;
        
        Utilization_Report__c utilizationRecordMRReonciliation = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'MR Reconciliation',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = endDate,
            Status__c='Approved By Lessor'
        );
        insert utilizationRecordMRReonciliation;
        
        Reconciliation_Schedule__c rsObjSecond = new Reconciliation_Schedule__c();
        rsObjSecond.Name = startDate.year()+' '+ datetime.newInstance(startDate.year(), startDate.month(),startDate.day()).format('MMM')+' to '+ endDate.addDays(-1).year() +' '+datetime.newInstance(endDate.addDays(-1).year(), endDate.addDays(-1).month(),endDate.addDays(-1).day()).format('MMM');
        rsObjSecond.From_Date__c = startDate;
        rsObjSecond.To_Date__c = endDate;
        rsObjSecond.Status__c = 'Complete';
        rsObjSecond.RecordTypeId = recordTypeId;
        rsObjSecond.Reconciliation_Type__c = 'Assumed Ratio';
        rsObjSecond.Disable_Invoice_Auto_Generation__c = false;
        rsObjSecond.Lease__c = leaseRecord.Id;
        rsObjSecond.Asset__c = testAircraft.Id;
        rsObjSecond.Reconciliation_Period__c = '12';
        
        insert rsObjSecond;        
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = lastDayOfMonth,
            Status__c='Approved By Lessor'
        );
        insert utilizationRecord;
       
        Utilization_Report_List_Item__c utilizationReportListItemObj=new Utilization_Report_List_Item__c(
            Constituent_Assembly__c=consituentAssemblyRecord.Id,
            Utilization_Report__c=utilizationRecord.Id,
            //Event_Name__c=assemblyEventInfoObj.Id,
            Cycles_During_Month__c=1,
            Running_Hours_During_Month__c=2,
            Assembly_MR_Info__c=assemblyMrRecord.Id,
            Y_Hidden_MR__c = 15000
        );
        
        insert utilizationReportListItemObj;
        
         Utilization_Report__c utilizationRecordAssumed = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Assumed/Estimated',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = endDate,
            Status__c='Approved By Lessor'
        );
        insert utilizationRecordAssumed;
        
        startDate = Date.newInstance(lastDayOfMonth.year() - 1, lastDayOfMonth.month() + 1, 1);
        endDate = lastDayOfMonth;
        
        Reconciliation_Schedule__c rsObj = new Reconciliation_Schedule__c();
        rsObj.Name = startDate.year()+' '+ datetime.newInstance(startDate.year(), startDate.month(),startDate.day()).format('MMM')+' to '+ endDate.addDays(-1).year() +' '+datetime.newInstance(endDate.addDays(-1).year(), endDate.addDays(-1).month(),endDate.addDays(-1).day()).format('MMM');
        rsObj.From_Date__c = startDate;
        rsObj.To_Date__c = endDate;
        rsObj.Status__c = 'Open';
        rsObj.RecordTypeId = recordTypeId;
        rsObj.Reconciliation_Type__c = 'Assumed FH and FC';
        rsObj.Disable_Invoice_Auto_Generation__c = false;
        rsObj.Lease__c = leaseRecord.Id;
        rsObj.Asset__c = testAircraft.Id;
        rsObj.Reconciliation_Period__c = '12';
        
        insert rsObj;
    }
    
    @isTest
    static  void testMethod1(){
        Test.startTest();
      
        Reconciliation_Schedule__c reconciliationScheduleRecord = [Select Id, Reconciliation_Type__c From Reconciliation_Schedule__c Where Reconciliation_Type__c = 'Assumed Ratio'  Limit 1];
        MRReconciliationInvoiceController.ReconSRDetailsWrapper reconSRDetailsWrapper=  MRReconciliationInvoiceController.getSRdetails(reconciliationScheduleRecord.id);
        list<id> srIDs = new list<id>();
        for(MRReconciliationInvoiceController.SRDetailsWrapper sr : reconSRDetailsWrapper.srDetails){
            srIDs.add(sr.srId);
        }
       
        MRReconciliationInvoiceController.MRReconciliationInvoiceContainer wrapperObj = MRReconciliationInvoiceController.loadData(reconciliationScheduleRecord.Id, reconciliationScheduleRecord.Reconciliation_Type__c,srIDs);
       
        MRReconciliationInvoiceController.saveData(JSON.serialize(wrapperObj), System.today(),System.today()); 
      
        MRReconciliationInvoiceController.monthName(1,2019);
        MRReconciliationInvoiceController.monthName(2,2019);
        MRReconciliationInvoiceController.monthName(3,2019);
        MRReconciliationInvoiceController.monthName(4,2019);
        MRReconciliationInvoiceController.monthName(5,2019);
        MRReconciliationInvoiceController.monthName(6,2019);
        MRReconciliationInvoiceController.monthName(7,2019);
        MRReconciliationInvoiceController.monthName(8,2019);
        MRReconciliationInvoiceController.monthName(9,2019);
        MRReconciliationInvoiceController.monthName(10,2019);
        MRReconciliationInvoiceController.monthName(11,2019);
        MRReconciliationInvoiceController.monthName(12,2019);
        MRReconciliationInvoiceController.monthName(13,2019);
        list<invoice__c> invs = [select id,(select id,Amount_Due__c from Invoice_Line_Items__r )from invoice__c];
        system.assertEquals(1,invs.size());
        Test.stopTest();
    }
    
     @isTest
    static  void testMethod2(){
        Test.startTest();
        Reconciliation_Schedule__c reconciliationScheduleRecord = [Select Id, Reconciliation_Type__c From Reconciliation_Schedule__c Where Reconciliation_Type__c = 'Assumed FH and FC'  Limit 1];
        MRReconciliationInvoiceController.ReconSRDetailsWrapper reconSRDetailsWrapper =  MRReconciliationInvoiceController.getSRdetails(reconciliationScheduleRecord.id);
        list<id> srIDs = new list<id>();
        for(MRReconciliationInvoiceController.SRDetailsWrapper sr : reconSRDetailsWrapper.srDetails){
            srIDs.add(sr.srId);
        }
        MRReconciliationInvoiceController.MRReconciliationInvoiceContainer wrapperObj = MRReconciliationInvoiceController.loadData(reconciliationScheduleRecord.Id, reconciliationScheduleRecord.Reconciliation_Type__c,srIDs);
        
        MRReconciliationInvoiceController.saveData(JSON.serialize(wrapperObj), System.today(),System.today()); 
        list<invoice__c> invs = [select id,(select id,Amount_Due__c from Invoice_Line_Items__r )from invoice__c];
        system.assertEquals(1,invs.size());
        Test.stopTest();
    }
}