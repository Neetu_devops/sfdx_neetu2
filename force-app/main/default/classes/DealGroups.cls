public with sharing class DealGroups { 
    @AuraEnabled
    public static List<Deal_And_Group_Assignment__c> getDealGroups(Id DealId){
       List<Deal_And_Group_Assignment__c> dealsByDealGroup= [SELECT Deals__r.Name,Deals__r.Id,Deal_Group__r.Name,Deal_Group__r.Owner.Name,CreatedBy.name,
            Deal_Group__r.Target_Completion_Date__c,Deal_Group__r.Comment__c
            from Deal_And_Group_Assignment__c where Deal_Group__r.Id=:DealId];
         system.debug('dealbydealgroups-----'+dealsByDealGroup);
        return dealsByDealGroup;
    }
    @AuraEnabled
    public static Deal_Group__c fetchDealGroupsDetails(Id DealId){
        List<Deal_Group__c> listDealGroups = new List<Deal_Group__c>();
        if(DealId!=null)
        listDealGroups = [select Comment__c,Name,Target_Completion_Date__c from Deal_Group__c where Id =: DealId];
        if(!listDealGroups.isEmpty())
        return listDealGroups.get(0);
        return null;
    }
    @AuraEnabled
    public static void updateComment (Deal_Group__c comment){
        
        system.debug('------- the comment coming in----'+JSON.serializePretty(comment));
        UPDATE comment;
    }

}