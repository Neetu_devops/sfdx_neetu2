public class PaymentLineItemHandler implements ITrigger{
  private final string triggerBefore = 'PaymentLineItemHandlerBefore';
  private final string triggerAfter = 'PaymentLineItemHandlerAfter';

  public PaymentLineItemHandler()
  {
  }

  public void bulkBefore()
  {
  }
   
  public void bulkAfter()
  {
  }
       
  public void beforeInsert()
  {
      if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
      LeaseWareUtils.setFromTrigger(triggerBefore);
      
      system.debug('PaymentLineItemHandler.beforeInsert(+)');
      doNotAllowManualPmtLICreation();
      system.debug('PaymentLineItemHandler.beforeInsert(-)');     
  }
   
  public void beforeUpdate()
  {
      if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
      LeaseWareUtils.setFromTrigger(triggerBefore);
      system.debug('PaymentLineItemHandler.beforeUpdate(+)');  
      
      if(LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter')) return;
      for(payment_line_item__c pyLI : (list<payment_line_item__c>)trigger.new){          
        if(pyLI.status__c.equals('Cancelled')||pyLI.status__c.equals('Cancelled-Pending')){
          pyLI.addError('Payment and related Line Items in Cancelled-Pending and Cancelled status cannot be modified.');
          return;
        }
      }      
      OverrideChkboxUpdOnPmtLI();
      system.debug('PaymentLineItemHandler.beforeUpdate(-)');
  }
   
  public void beforeDelete()
  {  
    if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
    LeaseWareUtils.setFromTrigger(triggerBefore);
    system.debug('PaymentLineItemHandler.beforeDelete(+)');
    UpdateTotalOnPayment();      
    system.debug('PaymentLineItemHandler.beforeDelete(-)');
  }
   
  public void afterInsert()
  {
      if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
      LeaseWareUtils.setFromTrigger(triggerAfter);
      system.debug('PaymentLineItemHandler.afterInsert(+)');

  UpdateTotalOnPayment();

      system.debug('PaymentLineItemHandler.afterInsert(-)');      
  }
   
  public void afterUpdate()
  {
      if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
      LeaseWareUtils.setFromTrigger(triggerAfter);
      system.debug('PaymentLineItemHandler.afterUpdate(+)');

  UpdateTotalOnPayment();

      system.debug('PaymentLineItemHandler.afterUpdate(-)');      
  }
   
  public void afterDelete()
  {
      system.debug('PaymentLineItemHandler.afterDelete(+)');
      
      
      system.debug('PaymentLineItemHandler.afterDelete(-)');      
  }

  public void afterUnDelete()
  {
      system.debug('PaymentLineItemHandler.afterUnDelete(+)');
      
      system.debug('PaymentLineItemHandler.afterUnDelete(-)');        
  }
   
  public void andFinally()
  {
  }
  //Hint: Shuold not allow to create Payment Line Item Manually.
  public void doNotAllowManualPmtLICreation() {    
    system.debug('PaymentLineItemHandler.doNotAllowManualPmtLICreation(+)');
    list<Payment_Line_Item__c> listNew = (list<Payment_Line_Item__c>)trigger.New;
    if(!(LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerBefore')|| LeaseWareUtils.isFromTrigger('CREATE_PY_LINE_ITEM_CREDIT_MEMO'))){
      for(Payment_Line_Item__c curPmtLI: listNew){
        curPmtLI.adderror('You are not allowed to create line items on a payment manually.');
      }
    }
    system.debug('PaymentLineItemHandler.doNotAllowManualPmtLICreation(-)');
  }

  //Hint- Auto-check when user is changing the Payment Line Item Amount to different value or User manualy check or Uncheck Override
  //Before Update
  public void OverrideChkboxUpdOnPmtLI() {    
    system.debug('PaymentLineItemHandler.OverrideChkboxUpdOnPmtLI(+)');
    if(LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter')) return;
    if(LeaseWareUtils.isFromTrigger(triggerAfter)) return; 
    decimal dOldPmtLIAmt;    
    list<Payment_Line_Item__c> listNew = (list<Payment_Line_Item__c>)trigger.New;
    for(Payment_Line_Item__c curPmtLI: listNew){
      system.debug('Before Update Case- Payment LI='+curPmtLI.id);
      Payment_Line_Item__c oldPmtLI = (Payment_Line_Item__c)(trigger.oldMap.get(curPmtLI.Id));
      dOldPmtLIAmt = leaseWareUtils.zeroIfNull(OldPmtLI.Amount_Paid__c).setscale(2,System.RoundingMode.HALF_UP);
      if(dOldPmtLIAmt != leaseWareUtils.zeroIfNull(curPmtLI.Amount_Paid__c).setscale(2,System.RoundingMode.HALF_UP))
      {
        if(curPmtLI.Invoice_Type__c == 'Aircraft MR' || curPmtLI.Invoice_Type__c == 'Non MR')//Override checkbox applicable only for MR, MR Reconciliation payments
        {
          if(!curPmtLI.Override__c) curPmtLI.Override__c=True; // Auto-check when user is changing the Payment Line Item Amount to different value
        }        
      }
    }
    system.debug('PaymentLineItemHandler.OverrideChkboxUpdOnPmtLI(-)');       
  }

  //after insert,after update,before delete
  set<Payment_Line_Item__c> setPaymtLIAmtChnge = new set<Payment_Line_Item__c>();
  map<id, Assembly_MR_Rate__c> mapAssemblyMRInfo;
  boolean bErr = false;
  private void UpdateTotalOnPayment() {
    system.debug('PaymentLineItemHandler.UpdateTotalOnPayment(+)');
    list<Payment_Line_Item__c> listNew = (list<Payment_Line_Item__c>)(trigger.isDelete?trigger.old:trigger.New);

    //Hint: Shuold not allow to delete Payment LI manually.
    for(Payment_Line_Item__c curPmtLI: listNew){
      system.debug('=='+LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerBefore'));
      if(trigger.isDelete && !LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerBefore')){
        curPmtLI.adderror('Deletion of line items are not allowed. Please mark the payment amount 0 in case you do not wish to make a payment for a particular line');
      }
    }

    set<id> setPmtIds = new set<Id>();
    set<id> setInvLIIds = new set<Id>();
    map<id, Invoice_Line_Item__c> mapInvLineItems;
    map<id, Invoice_Line_Item__c> mapUpdInvLineItems= new map<id, Invoice_Line_Item__c>();
    set<id> setLeaseIds = new set<id>();
    list<Assembly_MR_Rate__c> listAsmblyMRInfosToUpd = new list<Assembly_MR_Rate__c>();
    boolean isCreditMemo = false;
    
    for(Payment_Line_Item__c curPmtLI: listNew){
      setInvLIIds.add(curPmtLI.Invoice_Line_Item__c);
    }
    system.debug('size of setInvLIIds='+setInvLIIds.size());
    //Need Invoice LI record to calculate total amount paid for that and based on that update Balance amount field
    mapInvLineItems  =new map<id, Invoice_Line_Item__c>([select id, Total_Amount_Paid__c, Total_Amount_Credited__c, Total_Line_Amount__c,Invoice__r.Lease__c, Invoice__r.Rent__r.Rent_Type__c,balance_due__c
                                                        from Invoice_Line_Item__c where id in :setInvLIIds]); 
    system.debug('size of mapInvLineItems='+mapInvLineItems.size());
    for(Invoice_Line_Item__c curInvLI : mapInvLineItems.values()){
      setLeaseIds.add(curInvLI.Invoice__r.Lease__c);
    }
    mapAssemblyMRInfo = new map<id,Assembly_MR_Rate__c>([select id, Lease__c, Assembly_Lkp__r.Type__c, Assembly_Event_Info__r.Event_Type__c,
    Transactions_Total_Cash__c,  Snapshot_Event_Record_ID__c, Date_Of_Snapshot_Event__c
      from Assembly_MR_Rate__c where Lease__c in :setLeaseIds]);
    
    Invoice_Line_Item__c curInvLIRec;
    List<Payment_Line_Item__c> listPaymentLItoUpd=new List<Payment_Line_Item__c>();
    for(Payment_Line_Item__c curPmtLI: listNew){
      if(('Credit Memo').equals(curPmtLI.Payment_Credit_Memo__c))isCreditMemo = true;
      if(!mapInvLineItems.containskey(curPmtLI.Invoice_Line_Item__c)){
        LeasewareUtils.createExceptionLog(null, 'There is some issue with Invoice Line Item for this Payment line item', 'Payment_Line_Item__c',curPmtLI.id,null,false);
        continue;
      }
      curInvLIRec = mapInvLineItems.get(curPmtLI.Invoice_Line_Item__c);
      decimal dNewPmtLIAmt = leaseWareUtils.zeroIfNull(curPmtLI.Amount_Paid__c).setscale(2,System.RoundingMode.HALF_UP);
      decimal dNetChange = 0; //, dNetChangeLLP = 0;
      if(trigger.isInsert){
        system.debug('Insert Case');
        dNetChange = dNewPmtLIAmt;
        
        if(curPmtLI.status__c.equals('Approved')){
          if(isCreditMemo)
          {
            curInvLIRec.Total_Amount_Credited__c= (LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Credited__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
          }
          else
          {
            curInvLIRec.Total_Amount_Paid__c= (LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Paid__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
          }
          
          mapUpdInvLineItems.put(curInvLIRec.id, curInvLIRec);
        }
      }else if(trigger.isUpdate){
        system.debug('Update Case='+curPmtLI.Name);
        Payment_Line_Item__c oldPmtLI = (Payment_Line_Item__c)(trigger.oldMap.get(curPmtLI.Id));
        decimal dOldPmtLIAmt = leaseWareUtils.zeroIfNull(OldPmtLI.Amount_Paid__c).setscale(2,System.RoundingMode.HALF_UP);
        if(dOldPmtLIAmt != dNewPmtLIAmt ||(curPmtLI.status__c.equals('Approved'))){
          //when py amount is changed and the status of py is also change together. Invoice line item is updated only for approved py 
          if('Approved'.equals(curPmtLI.status__c) && (LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter'))){dNetChange = dNewPmtLIAmt;}
          else if(dOldPmtLIAmt != dNewPmtLIAmt && curPmtLI.status__c.equals('Approved') ) {dNetChange = dNewPmtLIAmt - dOldPmtLIAmt;}
          else if(dOldPmtLIAmt != dNewPmtLIAmt && curPmtLI.status__c.equals('Pending') ) {dNetChange = dNewPmtLIAmt;}
          system.debug('Dnet Change::'+dNetChange);
          if(isCreditMemo)
          {
            curInvLIRec.Total_Amount_Credited__c= (LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Credited__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
          }
          else
          {
            curInvLIRec.Total_Amount_Paid__c=(LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Paid__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
          }
                    
          if((curInvLIRec.Total_Line_Amount__c - (LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Paid__c) + LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Credited__c))) < 0 ){
            curPmtLI.adderror('Payment line allocation cannot exceed Balance Due amount for the specific Invoice line.');
            bErr = true;
            continue;
          }  
          if(curInvLIRec.Invoice__r.Rent__r.Rent_Type__c == 'Power By The Hour' && !LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter')) 
          {
              //Donot allow user to override the payment line item amount in case of PBH Rent
              curPmtLI.adderror('Payment line amount cannot be edited for PBH Rent Invoice.');
              bErr = true;
              continue;
          } 

          if(curPmtLI.status__c.equals('Approved')){
            system.debug('Adding to mapUpdInvLineItems');
            mapUpdInvLineItems.put(curInvLIRec.id, curInvLIRec);
          }
          
          setPmtIds.add(curPmtLI.Payment__c); // When Amount Paid get changed by End User
          setPaymtLIAmtChnge.add(curPmtLI);
        }  
        else if((curPmtLI.Invoice_Type__c == 'Aircraft MR' || curPmtLI.Invoice_Type__c =='Non MR') && 
          oldPmtLI.Override__c!= curPmtLI.Override__c && !curPmtLI.Override__c) {
          system.debug('End User has Unchecked Override checkbox then recalcualte all unlocked PaymentLI Amount');
          setPmtIds.add(curPmtLI.Payment__c); // When Override checkbox get changed (either checked or Unchecked) by End User
        }
        else if(LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter')){
          //When Payment Amount get updated from Payment Header side
          system.debug('called from PaymentTriggerHandlerAfter');
          setPmtIds.add(curPmtLI.Payment__c);
          //setPaymtLIAmtChnge.add(curPmtLI); // Case when Payment header amount get update and due to that Payment LI get overpaid
        }
      }
      else{//Delete
        dNetChange = -dNewPmtLIAmt;
        //dNetChangeLLP = -dNewPmtLIAmtLLP;          
        //curPmt.Amount__c = (curPmt.Amount__c==null?0:curPmt.Amount__c) + dNewPmtLIAmt;
        //setPmtIds.add(curPmt.Id);
        //mapPmts.put(curPmt.Id, curPmt);
        system.debug('=dNetChange='+ dNetChange);
        if(curPmtLI.status__c.equals('Approved')){
          if(isCreditMemo)
          {
            curInvLIRec.Total_Amount_Credited__c= (LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Credited__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
          }
          else
          {
            curInvLIRec.Total_Amount_Paid__c=(LeaseWareUtils.zeroIfNull(curInvLIRec.Total_Amount_Paid__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
          }
          mapUpdInvLineItems.put(curInvLIRec.id, curInvLIRec);
        }
      }
      if(!bErr && curPmtLI.status__c.equals('Approved')){
        if(trigger.isUpdate &&  LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter') )continue;//on update this method is called in the updPaymentLIAdjAmtCal method
        UpdationOnSuppRent(curPmtLI,dNetChange,isCreditMemo); // Update Supplement Rent -Balance Accumulation and other fields
      }      
    }   
    system.debug('size of setPmtIds='+setPmtIds.size());
    system.debug('size of setPaymtLIAmtChnge='+setPaymtLIAmtChnge.size());
    //At the time of Update- When User change Amount Paid or override checkbox, then Calculate and Adjust Amount in Payment LI
    
    if(setPmtIds.size()>0 && !bErr){
      listPaymentLItoUpd = updPaymentLIAdjAmtCal(setPmtIds,isCreditMemo);
    }
    system.debug('size of listPaymentLItoUpd='+ listPaymentLItoUpd.size());
    if(!bErr){
      set<id> setInvLIIdAutoUpd= new set<id>();    
      if(listPaymentLItoUpd.size()>0){
        try{
          update listPaymentLItoUpd;
        }
        catch(DmlException ex){
          throw new LeaseWareException(ex.getMessage());
        }
        catch(exception ex){
          throw new LeaseWareException(ex.getMessage());
        }
        for(Payment_Line_Item__c curPayLI: listPaymentLItoUpd){
          if(curPayLI.status__c.equals('Approved')){
            setInvLIIdAutoUpd.add(curPayLI.Invoice_Line_Item__c); // Finding all InvLI which has to update due PaymentLI Amt adjustment
          }
        }
      }
      map<id, Invoice_Line_Item__c> mapInvLIUpd =new map<id, Invoice_Line_Item__c>([select id, Total_Amount_Paid__c, Total_Amount_Credited__c 
                                                                                    from Invoice_Line_Item__c where id in:setInvLIIdAutoUpd]);
      system.debug('size of mapInvLIUpd='+ mapInvLIUpd.size());
      system.debug('size of mapUpdInvLineItems='+ mapUpdInvLineItems.size());

      Invoice_Line_Item__c invLIRec;
      if(listPaymentLItoUpd.size()>0){
        for(Payment_Line_Item__c curPayLI: listPaymentLItoUpd){
          if(mapInvLIUpd.containskey(curPayLI.Invoice_Line_Item__c)){
            invLIRec=mapInvLIUpd.get(curPayLI.Invoice_Line_Item__c);
            system.debug('this is the total Paid amout value for this Inv LI='+LeaseWareUtils.zeroIfNull(curPayLI.Invoice_Line_Item__r.Total_Amount_Paid__c).setscale(2,System.RoundingMode.HALF_UP));
            system.debug('this is the total Credited amout value for this Inv LI='+LeaseWareUtils.zeroIfNull(curPayLI.Invoice_Line_Item__r.Total_Amount_Credited__c).setscale(2,System.RoundingMode.HALF_UP));
            if(!('Credit Memo').equals(curPayLI.Payment_Credit_Memo__c))
            {
              invLIRec.Total_Amount_Paid__c=LeaseWareUtils.zeroIfNull(curPayLI.Invoice_Line_Item__r.Total_Amount_Paid__c).setscale(2,System.RoundingMode.HALF_UP); //Using this Updating Balance Amount(Formula field) at Inv LI 
            }
            else
            {
              invLIRec.Total_Amount_Credited__c = LeaseWareUtils.zeroIfNull(curPayLI.Invoice_Line_Item__r.Total_Amount_Credited__c).setscale(2,System.RoundingMode.HALF_UP);
            }
            
            mapUpdInvLineItems.put(invLIRec.id, invLIRec);
          }
        }
      }
      system.debug('size of mapUpdInvLineItems='+ mapUpdInvLineItems.size());
      
      if(mapUpdInvLineItems.size()>0) {
        try{
          LeaseWareUtils.TriggerDisabledFlag=true;
          update mapUpdInvLineItems.values();  //Only updating 'Line Amount Paid'(Total_Amount_Paid__c) at Inv Line Item
          LeaseWareUtils.TriggerDisabledFlag=false;
        }
        catch(DmlException ex){
          throw new LeaseWareException(ex.getMessage());
        }
        catch(exception ex){
          throw new LeaseWareException(ex.getMessage());
        }
      }    
      
      if(mapAssemblyMRInfo.size()>0){
        for(id curId : mapAssemblyMRInfo.keyset()){
          listAsmblyMRInfosToUpd.add(mapAssemblyMRInfo.get(curId));
        }
        //LeaseWareUtils.TriggerDisabledFlag = true; Can't switch off. Snapshot creation depends on MR Assembly Trigger.
        LeaseWareUtils.setFromTrigger('SKIP_LEASE_LOCK_VALIDATION'); // this will skip checking Lease approval check
        update listAsmblyMRInfosToUpd;
        LeaseWareUtils.unsetTriggers('SKIP_LEASE_LOCK_VALIDATION');
        //LeaseWareUtils.TriggerDisabledFlag = false;
      }
    }
  }

  private void UpdationOnSuppRent(Payment_Line_Item__c curPmtLI,decimal dNetChange,boolean isCreditMemo ){
    ID SuppRentID;// = curPmt.Lease__c + '-' + curPmtLI.Report_Item_Type__c + '-' + curPmtLI.Report_Item_Subtype__c;

    //June 13, 2019 (Invoice Module)
    if(curPmtLI.Assembly_MR_Info__c != null){
      SuppRentID = curPmtLI.Assembly_MR_Info__c;
    }
            
    system.debug('Getting Key ' + SuppRentID);
    Assembly_MR_Rate__c thisAsmblyMRInf = mapAssemblyMRInfo.get(SuppRentID);
    
    //added on Aug 26, 2019
    if(thisAsmblyMRInf!= null){
      thisAsmblyMRInf.Snapshot_Event_Record_ID__c = 'P' + curPmtLI.Id;
      thisAsmblyMRInf.Date_Of_Snapshot_Event__c = curPmtLI.Payment_Date__c;
      mapAssemblyMRInfo.put(SuppRentID, thisAsmblyMRInf);
    }
  }

  //Hint-At the time of Update- When User change Amount Paid or override checkbox, then re-calculate and adjust amount in Payment LI
  //after update
  private list<Payment_Line_Item__c> updPaymentLIAdjAmtCal(set<id> setPmtIds,boolean isCreditMemo) {
    decimal sumLockPaymntLIAmtPaid, sumUnLockInvLIBalDue, TotalPaymntAmtUnlock, dOldPmtLIAmt, dNewPmtLIAmt, dNetChange=0, sumUnLockPaymntLIAmtPaid,sumPaymntLI,amount=0;
    integer countOfLI,counter;
    map<id,Payment_Line_Item__c> mapPaymentLINotLocked = new map<id,Payment_Line_Item__c>();
    map<id,decimal> mapPaymentLIPrevBalDue = new map<id,decimal>(); //map<PaymentLIID,Previous Balance due>
    List<Payment_Line_Item__c> listPaymentLItoUpd=new List<Payment_Line_Item__c>();
    String payLINonMRRecTypeId = Schema.SObjectType.Payment_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Non_MR').getRecordTypeId();

    //Find all Payment LI for that payment where Paid Amount Recalcuate and adjust amount in Payment LI
    map<id, Payment__c> mapPmts = new map<id, Payment__c>([select id, Amount__c,
                                      (SELECT id,name, Override__c,Amount_Paid__c,Invoice_Line_Item__c,Invoice_Line_Item__r.Total_Line_Amount__c,RecordTypeId,Invoice_Type__c,Payment_Credit_Memo__c,status__c,Invoice_Line_Item__r.balance_due__c,
                                              Invoice_Line_Item__r.Total_Amount_Paid__c,Invoice_Line_Item__r.Total_Amount_Credited__c, Assembly_MR_Info__c,Payment_Date__c FROM Payment_Line_Items__r order by Name)
                                      from Payment__c
                                      where id in :setPmtIds]);
		for( Payment__c curPmt : mapPmts.values()){
      system.debug('enter in payment loop='+ curPmt.Payment_Line_Items__r.size());
      sumLockPaymntLIAmtPaid=0;sumUnLockInvLIBalDue=0;TotalPaymntAmtUnlock=0;sumUnLockPaymntLIAmtPaid=0;
      //Finding sum of locked and unlock Payment LI- Amount paid
      for(Payment_Line_Item__c curPaymntLI : curPmt.Payment_Line_Items__r){
        //Sum of Locked PaymentLI- Amount Paid
        if(('Credit Memo').equals(curPaymntLI.Payment_Credit_Memo__c)){isCreditMemo = true;}
        else isCreditMemo = false;
        if(curPaymntLI.Override__c || (curPaymntLI.RecordTypeId == payLINonMRRecTypeId && curPaymntLI.Invoice_Type__c != 'Non MR'))
        {
          system.debug('Amount paid for Paymnt LI='+ curPaymntLI.Amount_Paid__c.setscale(2,System.RoundingMode.HALF_UP));
          sumLockPaymntLIAmtPaid+= curPaymntLI.Amount_Paid__c.setscale(2,System.RoundingMode.HALF_UP);          
          if(trigger.isUpdate &&  LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter') && curPaymntLI.status__c.equals('Approved') ){
            UpdationOnSuppRent(curPaymntLI,curPaymntLI.Amount_Paid__c,isCreditMemo);
          }        
        }
        else{ 
          //Below re-calculation logic applicable when PaymentLI getting update for those which is not locked
          if('Pending'.equals(curPaymntLI.status__c)){//as payment amount is not added to invoice line item directly taking invoice line item balance due
            system.debug('Inside pending and balance due::'+leaseWareUtils.zeroIfNull(curPaymntLI.Invoice_Line_Item__r.balance_due__c));
            amount = (leaseWareUtils.zeroIfNull(curPaymntLI.Invoice_Line_Item__r.balance_due__c)).setscale(2,System.RoundingMode.HALF_UP);
          }
          else{
            system.debug('=else part- Recalculate and allocate='+ (curPaymntLI.Invoice_Line_Item__r.Total_Line_Amount__c - (Leasewareutils.zeroIfNull(curPaymntLI.Invoice_Line_Item__r.Total_Amount_Paid__c) + LeaseWareUtils.zeroIfNull(curPaymntLI.Invoice_Line_Item__r.Total_Amount_Credited__c))) +'==='+curPaymntLI.Amount_Paid__c);
            amount = ((leaseWareUtils.zeroIfNull(curPaymntLI.Invoice_Line_Item__r.Total_Line_Amount__c) - (leaseWareUtils.zeroIfNull(curPaymntLI.Invoice_Line_Item__r.Total_Amount_Paid__c) + leasewareutils.zeroIfNull(curPaymntLI.Invoice_Line_Item__r.Total_Amount_Credited__c)))+ leaseWareUtils.zeroIfNull(curPaymntLI.Amount_Paid__c)).setscale(2,System.RoundingMode.HALF_UP); //Finding pervious balance Due before allocation and doing all sums 
          }
          sumUnLockInvLIBalDue+=amount;
          mapPaymentLINotLocked.put(curPaymntLI.id,curPaymntLI);
          mapPaymentLIPrevBalDue.put(curPaymntLI.id,amount); 
        }
      }
      TotalPaymntAmtUnlock=(curPmt.Amount__c - sumLockPaymntLIAmtPaid).setscale(2,System.RoundingMode.HALF_UP); // Total payment Amout which is unlocked
      system.debug('sumLockPaymntLIAmtPaid='+sumLockPaymntLIAmtPaid+'=sumUnLockInvLIBalDue='+sumUnLockInvLIBalDue+'=TotalPaymntAmtUnlock='+TotalPaymntAmtUnlock);
      system.debug('size of mapPaymentLINotLocked ='+ mapPaymentLINotLocked.size());
      
      /* Adjusting rest of unlock amount paid to all unlocked Payment LI-Amount Paid OR when Payment Header Amount get update
        based on the calculation Invoice line item balance due * Payment amount / Total Inv Balance due */

      sumPaymntLI=0;counter=0;
      countOfLI=mapPaymentLINotLocked.size();
      for(Payment_Line_Item__c paymentLIUpdRec : mapPaymentLINotLocked.values()){
        if(('Credit Memo').equals(paymentLIUpdRec.Payment_Credit_Memo__c)){isCreditMemo = true;}
        else isCreditMemo = false;
        counter=counter+1;
        dOldPmtLIAmt = leaseWareUtils.zeroIfNull(paymentLIUpdRec.Amount_Paid__c).setscale(2,System.RoundingMode.HALF_UP);
        if(counter!=countOfLI){
          dNewPmtLIAmt = sumUnLockInvLIBalDue>0? (LeaseWareUtils.zeroIfNull(mapPaymentLIPrevBalDue.get(paymentLIUpdRec.id))*TotalPaymntAmtUnlock/sumUnLockInvLIBalDue).setscale(2,System.RoundingMode.HALF_UP):0;
        }
        else{
          dNewPmtLIAmt=TotalPaymntAmtUnlock-sumPaymntLI; // Last Line Amount Calculation
        }
        system.debug('=Invoice Balance Due for PaymntLI='+paymentLIUpdRec.Name+'='+mapPaymentLIPrevBalDue.get(paymentLIUpdRec.id)+ '=dNewPmtLIAmt='+dNewPmtLIAmt);
        system.debug('paymentLIUpdRec.Invoice_Line_Item__r.balance_due__c::'+paymentLIUpdRec.Invoice_Line_Item__r.balance_due__c);
        //The changes made to this line item can lead the allocation on other line items to go below $0. Please adjust the line allocations to be equal to or less than the total payment amount.
        for(Payment_Line_Item__c curPLIRec: setPaymtLIAmtChnge){
          if(dNewPmtLIAmt < 0){
            curPLIRec.adderror('The changes made to this line item can lead the allocation on other line items to go below $0. Please adjust the line allocations to be equal to or less than the total payment amount.');
            bErr = true;
            continue;
          }
          else if(dNewPmtLIAmt > mapPaymentLIPrevBalDue.get(paymentLIUpdRec.id).setscale(2,System.RoundingMode.HALF_UP)){
              curPLIRec.adderror('The changes made to this line item will cause the allocation on other line items to exceed the total payment amount.  Please adjust the line allocations to be equal to or less than the total payment amount.');
              bErr = true;
              continue;
          }
        }      
        if(!bErr){
          paymentLIUpdRec.Amount_Paid__c = dNewPmtLIAmt;
          sumUnLockPaymntLIAmtPaid+=paymentLIUpdRec.Amount_Paid__c;
          //Should be update Invoice LI Balance Amount due to changes Amount Paid in Payment LI
          if(dOldPmtLIAmt != dNewPmtLIAmt ||'Approved'.equals(paymentLIUpdRec.status__c) ){
            //amount change on approval
            if('Approved'.equals(paymentLIUpdRec.status__c) && (LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerAfter')))dNetChange = dNewPmtLIAmt;
            else if(dOldPmtLIAmt != dNewPmtLIAmt)dNetChange = dNewPmtLIAmt - dOldPmtLIAmt;
            //else dNetChange = dNewPmtLIAmt;
            system.debug('dNetChange='+dNetChange);
            if(!('Credit Memo').equals(paymentLIUpdRec.Payment_Credit_Memo__c))
            {
              paymentLIUpdRec.Invoice_Line_Item__r.Total_Amount_Paid__c=(LeaseWareUtils.zeroIfNull(paymentLIUpdRec.Invoice_Line_Item__r.Total_Amount_Paid__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
            }
            else
            {
              paymentLIUpdRec.Invoice_Line_Item__r.Total_Amount_Credited__c=(LeaseWareUtils.zeroIfNull(paymentLIUpdRec.Invoice_Line_Item__r.Total_Amount_Credited__c) + dNetChange).setscale(2,System.RoundingMode.HALF_UP);
            }            
          }
          system.debug('Name='+paymentLIUpdRec.Name+'=Amount Paid='+paymentLIUpdRec.Amount_Paid__c);
          system.debug('Final InvLI Paid Amt='+paymentLIUpdRec.Invoice_Line_Item__r.Total_Amount_Paid__c);
          system.debug('Final InvLI Credited Amt='+paymentLIUpdRec.Invoice_Line_Item__r.Total_Amount_Credited__c);
          listPaymentLItoUpd.add(paymentLIUpdRec);
          if(dNetChange!=0 && paymentLIUpdRec.status__c.equals('Approved')){
             UpdationOnSuppRent(paymentLIUpdRec,dNetChange,isCreditMemo);}
        }
        sumPaymntLI+=dNewPmtLIAmt; 
      } //end of Payment LI calculation
      if(!bErr){
        system.debug('sumUnLockPaymntLIAmtPaid='+sumUnLockPaymntLIAmtPaid+'=sumLockPaymntLIAmtPaid='+sumLockPaymntLIAmtPaid);
        if(curPmt.Amount__c.setscale(2,System.RoundingMode.HALF_UP)< (sumUnLockPaymntLIAmtPaid+sumLockPaymntLIAmtPaid).setscale(2,System.RoundingMode.HALF_UP)){
          for(Payment_Line_Item__c curPLIRec: setPaymtLIAmtChnge){
            curPLIRec.Amount_Paid__c.adderror('Sum of Line Items cannot exceed Payment Amount. Please adjust the line allocations.');
            bErr = true;
            continue;
          }
        }
        else if(curPmt.Amount__c.setscale(2,System.RoundingMode.HALF_UP) > (sumUnLockPaymntLIAmtPaid+sumLockPaymntLIAmtPaid).setscale(2,System.RoundingMode.HALF_UP)){
          for(Payment_Line_Item__c curPLIRec: setPaymtLIAmtChnge){
            curPLIRec.Amount_Paid__c.adderror('Sum of Line Items cannot be lower than Payment Amount. Please adjust the line allocations.');
            bErr = true;
            continue;
          }
        }
      } 
    } //end of Payment header
    System.debug('listPaymentLItoUpd:' + listPaymentLItoUpd.size());
    return listPaymentLItoUpd;
  }
}