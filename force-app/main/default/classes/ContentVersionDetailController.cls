public class ContentVersionDetailController {
	@AuraEnabled
    public static List<contentwrapper> getContentVersionDetails(String recordId){
        List<contentwrapper> cwList = new List<contentwrapper>();
       List<ContentVersion> cvList = [Select Id, VersionNumber, Description, LastModifiedBy.Name from ContentVersion where ContentDocumentId=:recordId order by VersionNumber asc];
        for(ContentVersion cv : cvList){
            contentwrapper cw = new contentwrapper();
	    if(cv.Description != null && cv.Description != ''){
           	cv.Description = cv.Description.replaceAll('<[/a-zAZ0-9]*>','');
		}
            if(cv.Description!=null && cv.Description.length()>40){
                cw.Description = cv.Description.substring(0,40) + '...';
                cw.cvData = cv;
            }
            else{
                cw.cvData=cv;
                cw.Description = cv.Description;
            }
            cwList.add(cw);
        }
        return cwList; 
    }
    public class contentwrapper{
        @AuraEnabled public ContentVersion cvData{get;set;}
        @AuraEnabled public String Description{get;set;}
    }
}