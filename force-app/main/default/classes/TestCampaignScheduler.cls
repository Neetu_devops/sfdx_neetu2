@isTest(SeeAllData=true)
private class TestCampaignScheduler {

    static testMethod void TestCampaignScheduler() {
        
	        Test.startTest();
	        try{
		        
		        CampaignCreationScheduler CampaignCheck = new  CampaignCreationScheduler();
	        	ID batchprocessid = Database.executeBatch(CampaignCheck);
	        }catch(System.DmlException e){
	        	system.debug('Expected - Ignore');
	        }

        Deal__c myDeal = new Deal__c(Name = 'Test Deal');
        insert myDeal;	        
	        
	    Lease__c lease1 = [select id,name from Lease__c where name like 'TestSeededMSN1%' limit 1];    
	    lease1.Anticipated_Remarketing_Status__c = 'Placement';
	    update lease1;
	        
	        LWGlobalUtils.CreateCampaigns(lease1.id);
	        
	        Test.stopTest();        
        
    }

}