//Safiya 28/04/2021 Reused for converting updatelease on UpdateRentOnLeaseScheduler to a batch process, due to the Apex CPU time limit

global class UpdateBalanceDueOnLeaseScheduler implements Schedulable, Database.Batchable<SObject> {

    
    string exceptionMessage='';
    
    public static void callUpdateBalanceDueOnLeaseBatch() {
        Database.executeBatch(new UpdateBalanceDueOnLeaseScheduler(),100);
    }        
  
    global void execute(SchedulableContext ctx)
    {
      
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
      
        date todaysdate= system.today(); 
       
        return Database.getQueryLocator(
        
        [select id,name,Base_Rent__c,Rent_Date__c,Rent_End_Date__c,Lease_Start_Date_New__c,Lease_End_Date_New__c
                   ,Applicable_Rent__c
                   ,Rent__c,Lessee_Account_Name__c,Y_Hidden_MRSchModifyDt__c //Using this Label -"Y_Hidden MultiRentSch ID"(API- Lessee_Account_Name__c) [earlier marked earlier as Z_Unused field, using this field due to shortage of new field creation on lease] field to store currently used MultiRentid from Multi Rent schedule object
                   ,aircraft__r.Lease__c,aircraft__c
                   ,(select id,name,Lease__c,rent_type__c, rent_period__c,rent_comment__c,Interest_Rate_Index__c,
                        start_date__c,rent_end_date__c,invoice_generation_day__c,rent_period_end_day__c,off_wing_rent__c,
                        rent_due_type__c, rent_due_day__c, due_day_correction__c, pro_rata_number_of_days__c, 
                        assumed_interest_rate__c,rental_credit_debit__c, cost_of_funds__c,
                        base_rent__c, LastModifiedDate 
                     from Stepped_Rents__r
                     where Status__c=true
                     order by start_date__c,createddate
                    )
                   from lease__c 
                   where Lease_End_Date_New__c>=:todaysdate and Lease_Start_Date_New__c<=:todaysdate
                   order by Lease_Start_Date_New__c asc]);
      
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> listLeaseToMultiRentRec){ 
       
        system.debug('Batch-UpdateBalanceDueOnLeaseScheduler Execution Start');
        try {

            CopyFieldMRSchToLease(listLeaseToMultiRentRec,'Schedule');
            FindMultiRentSch(listLeaseToMultiRentRec);

        } catch (Exception ex) {
            
            exceptionMessage += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
            system.debug('Exception : ' + ex +  + '===' +exceptionMessage);
        }
        system.debug('Batch-UpdateBalanceDueOnLeaseScheduler Execution End');
     
    }      
    
    global void finish(Database.BatchableContext BC){
         // execute any post-processing operations like sending email
       if(exceptionMessage!=''){
          //sending email
          string scriptMethodName = 'UpdateBalanceDueOnLeaseScheduler';
          string subjectName = 'Unexpected error on batch Script UpdateBalanceDueOnLeaseScheduler: on Date '+ system.today().format();
          string emailBody = 'Please find the attachment.';
          list<Document> documents = new list<Document>();
         //log file
       documents.add(LeaseWareStreamUtils.createDoc(exceptionMessage,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
    
       LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),subjectName ,emailBody,documents);
      }

       
    }
 
  // To Update Actual interest rate on Rent object
    //Scheduler	: Only for Floating Rent
	public static void FindMultiRentSch(list<lease__c> listLeaseToMultiRentRec){
        system.debug('UpdateBalanceDueOnLeaseScheduler.FindMultiRentSch(+)');
		set<id> setMultiRentSchID = new set<id>();
        set<id> setIntRateIndxIds= new set<id>();
        Date todaysdate= system.today();
		map<string,decimal> mapGetActIntRate = new map<string,decimal>();
		for( lease__c curleaserec: listLeaseToMultiRentRec){
			for(stepped_rent__c curmultirentrec: curleaserec.Stepped_Rents__r){
				if('Floating Rent'.equalsIgnoreCase(curmultirentrec.Rent_Type__c) && curmultirentrec.start_date__c<= todaysdate && curmultirentrec.rent_end_date__c >=todaysdate && curmultirentrec.Interest_Rate_Index__c!=Null){
					setMultiRentSchID.add(curmultirentrec.id);
					setIntRateIndxIds.add(curmultirentrec.Interest_Rate_Index__c);
				}
			}
		}
		system.debug('size of setMultiRentSchID='+setMultiRentSchID.size());
		system.debug('size of setIntRateIndxIds='+setIntRateIndxIds.size());
		if(setIntRateIndxIds.size()>0){	
			for(Interest_Rate_Index_Daily__c curIRID:[select id,name , Interest_Rate_Date__c,Interest_Rate_Index__c,Interest_Rate_Prcntg__c from Interest_Rate_Index_Daily__c where Interest_Rate_Index__c =:setIntRateIndxIds]){
				mapGetActIntRate.put(curIRID.Interest_Rate_Index__c+'-'+curIRID.Interest_Rate_Date__c,curIRID.Interest_Rate_Prcntg__c);
			}
		}
		system.debug('size of mapGetActIntRate='+mapGetActIntRate.size());
        FetchRentRecToUpd(setMultiRentSchID,mapGetActIntRate,'Scheduler');
        system.debug('UpdateBalanceDueOnLeaseScheduler.FindMultiRentSch(-)');
	}

    /* If In the Lease have multiple MultiRentSch records, then it check which MultiRent Record is active (Today's date falls under which Multi Rent start and end date),
       and only copy once from multirent to Lease used field "Y_Hidden MultiRentSch ID", while condition checknign Today date fall under Rent start and End date and 
       Y_Hidden MultiRentSch ID and Current multi RentId shuld not match, if match (after 1st time copied) won't condition match. */
	
    public static void CopyFieldMRSchToLease(list<lease__c> listLeaseToMultiRentRec,String TypeOfCall) {
        Date todaysdate= system.today(); 
        map < id, lease__c > mapUpdLeaseRec = new map < id, lease__c > ();
        //Note = 'Floating Rent'.equalsIgnoreCase(curmultirentrec.rent_type__c) => this below logic applied for phase1. in phase1 Floating Rent isthere on Lease page.
        // May this logic get change for phase 2.
        map<string,list<SteppedMultiRent__c>> mapSteppedRent = new map<string,list<SteppedMultiRent__c>>();
        list<SteppedMultiRent__c> tempList;
        for(SteppedMultiRent__c curRec:[select Multi_Rent_Schedule__c,Stepped_Rent_Start_Date__c,Rent_Amount__c 
                                            from SteppedMultiRent__c 
                                            where Multi_Rent_Schedule__r.Lease__c = :listLeaseToMultiRentRec
                                                order by Multi_Rent_Schedule__c,Stepped_Rent_Start_Date__c]){
            if(mapSteppedRent.containskey(curRec.Multi_Rent_Schedule__c)){
                tempList = mapSteppedRent.get(curRec.Multi_Rent_Schedule__c);
            }else{
                tempList = new list<SteppedMultiRent__c>();
            }
            tempList.add(curRec);
            mapSteppedRent.put(curRec.Multi_Rent_Schedule__c,tempList);
        }
         


        string newApplicableRentStr,EndDatestr;
        Date RentStartDate,RentEndDate;
        List<date> listMRSStartDate= new List<date>();
        List<date> listMRSEndDate= new List<date>();
        Decimal BaseRent,CurrentRent;
        for( lease__c curleaserec: listLeaseToMultiRentRec){
            //initialization
            listMRSStartDate.clear();
            listMRSEndDate.clear();
            newApplicableRentStr=' ';
            CurrentRent = null; 
            BaseRent = null;
            RentStartDate = curleaserec.Rent_Date__c==null? curleaserec.Lease_Start_Date_New__c : curleaserec.Rent_Date__c;
            RentEndDate = curleaserec.Rent_End_Date__c==null? curleaserec.Lease_End_Date_New__c : curleaserec.Rent_End_Date__c;
            

            for(stepped_rent__c curmultirentrec: curleaserec.Stepped_Rents__r){
                listMRSStartDate.add(curmultirentrec.start_date__c);
                listMRSEndDate.add(curmultirentrec.rent_end_date__c);
                BaseRent = curmultirentrec.Base_Rent__c==null?0:curmultirentrec.Base_Rent__c;
                if(mapSteppedRent.containsKey(curmultirentrec.id)){

                    //you have stepped rent records
                    tempList = mapSteppedRent.get(curmultirentrec.id);
                    if(curmultirentrec.start_date__c<= todaysdate && curmultirentrec.rent_end_date__c >=todaysdate){
                        // if multirent scedule in range of today then only
                        CurrentRent = BaseRent;
                        for(SteppedMultiRent__c  curstepRent:tempList){
                            if(todaysdate>=curstepRent.Stepped_Rent_Start_Date__c){
                                CurrentRent = curstepRent.Rent_Amount__c;
                            }
                        }
                    }

                    newApplicableRentStr += ' From '+
                        curmultirentrec.Start_Date__c.format() +' To '+ tempList[0].Stepped_Rent_Start_Date__c.adddays(-1).format() + ': $'+ BaseRent.format() + '\n'+' ';
    
                    EndDatestr = null;
                        for(integer i=0; i<tempList.size(); i++){
                            if( (i+1) >= tempList.size() ){
                                EndDatestr=curmultirentrec.Rent_End_Date__c.format();
                            }else{
                                EndDatestr=tempList[i+1].Stepped_Rent_Start_Date__c.adddays(-1).format();
                            }
                            // system.debug('++++++ newApplicableRentStr' +newApplicableRentStr);
                            // system.debug('++++++ Stepped_Rent_Start_Date__c' +tempList[i].Stepped_Rent_Start_Date__c);
                            // system.debug('++++++ EndDatestr ' +EndDatestr);
                            // system.debug('++++++ Rent_Amount__c' +tempList[i].Rent_Amount__c);
                            try{
                                newApplicableRentStr += 'From '+  
                                    tempList[i].Stepped_Rent_Start_Date__c.format() + ' To '+ EndDatestr +': $'+tempList[i].Rent_Amount__c.format() + '\n'+' ';
                            }catch(exception e){	
                                system.debug('Ignore (old existing records) - exception e ' + e);
                            }
                        }
                }else{
                    //when you do not have stepped rent records
                    newApplicableRentStr += ' From '+
                        curmultirentrec.Start_Date__c.format() +' To '+ curmultirentrec.rent_end_date__c.format() + ': $'+ BaseRent.format() + '\n'+' ';
                    
                    if(curmultirentrec.start_date__c<todaysdate &&    curmultirentrec.Rent_End_date__c >=todaysdate){
                        // if multirent scedule in range of today then only
                        CurrentRent = BaseRent;
                    }                    
                }    
                

                // Update only those Multi rent schedule which is applicable as of today.
                if(!(curmultirentrec.start_date__c <= todaysdate && curmultirentrec.rent_end_date__c >= todaysdate)) continue;

                if( 'Floating Rent'.equalsIgnoreCase(curmultirentrec.rent_type__c)||(curleaserec.Lessee_Account_Name__c!=curmultirentrec.id) || (curleaserec.Y_Hidden_MRSchModifyDt__c==Null) || (curleaserec.Y_Hidden_MRSchModifyDt__c < curmultirentrec.LastModifiedDate)) {
                    curLeaseRec.Lessee_Account_Name__c=curmultirentrec.id;
                    //--------CRM-----------
                    curleaserec.base_rent__c = curmultirentrec.base_rent__c;
                    curleaserec.rent_type__c = curmultirentrec.rent_type__c ;
                    //-----------------------                   
                    curleaserec.Y_Hidden_MRSchModifyDt__c= curmultirentrec.LastModifiedDate;
                    //Based on Anjani's disussion: Not removing dependency of below two fields as of now. Will create Engg Supp ticket to discuss with Diane.
                    curleaserec.due_date_correction__c = curmultirentrec.due_day_correction__c;
                    curleaserec.pro_rata_number_of_days__c = curmultirentrec.pro_rata_number_of_days__c;
                    mapUpdLeaseRec.put(curleaserec.id, curleaserec);
                }
            }// Loop of Multi rent
            
            if(listMRSStartDate.size()>0) {
                listMRSStartDate.sort();
                system.debug('Lowest MultiRentSch Start Date='+listMRSStartDate[0]);
                //---------Asset/CRM------
                if(curleaserec.rent_date__c!=listMRSStartDate[0]){
                    curleaserec.rent_date__c = listMRSStartDate[0];
                    mapUpdLeaseRec.put(curleaserec.id, curleaserec);
                }
            }
            if(listMRSEndDate.size()>0){
                listMRSEndDate.sort();
                system.debug('Max MultiRentSch End Date='+listMRSEndDate[listMRSEndDate.size()-1]);
                //---------Asset/CRM------
                if(curleaserec.rent_end_date__c!=listMRSEndDate[listMRSEndDate.size()-1]){
                    curleaserec.rent_end_date__c = listMRSEndDate[listMRSEndDate.size()-1] ;
                    mapUpdLeaseRec.put(curleaserec.id, curleaserec);
                }
            }
            // applicable rent get changed , please update the lease
            if(curleaserec.Applicable_Rent__c != newApplicableRentStr){
                curleaserec.Applicable_Rent__c = newApplicableRentStr;
                mapUpdLeaseRec.put(curleaserec.id, curleaserec);
            }
            // current rent get changed , please update the lease
            if(curleaserec.Rent__c != CurrentRent){
                curleaserec.Rent__c = CurrentRent;
                mapUpdLeaseRec.put(curleaserec.id, curleaserec);
            }


        }
        system.debug('=Size of mapUpdLeaseRec= ' + mapUpdLeaseRec.size());
        
        if (mapUpdLeaseRec.values().size() > 0) {
            LeaseWareUtils.TriggerDisabledFlag=true; // trigger disabled
            database.SaveResult[] res = database.update( mapUpdLeaseRec.values(), false);
            for(database.SaveResult sr : res){
                if (!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()) {
                        system.debug('UpdateBalanceDueOnLeaseScheduler : Lease could not be updated due to exception '+ + err.getStatusCode() + ': ' + err.getMessage() + err.getFields());
                        LeaseWareUtils.createExceptionLog(null,' UpdateBalanceDueOnLeaseScheduler : Lease could not be updated due to exception' + err.getStatusCode() + ': ' + err.getMessage() + err.getFields() ,null,'','Update',false);
                    }                    
                }
            }     
	      	// if any error
            LeaseWareUtils.insertExceptionLogs();  
                               
            LeaseWareUtils.TriggerDisabledFlag=false; // trigger enabled
        }
    }
    //Quick Action Button: Only for Floating Rent
	public static void FetchRentRecToUpd(set<id> setMultiRentSchID, map<string,decimal> mapGetActIntRate, string strCalledFrom){
        system.debug('UpdateBalanceDueOnLeaseScheduler.FetchRentRecToUpd(+)');
		Date todaysdate= system.today();
		set<id> setIntRateIndxIds= new set<id>();
		map<id,List<Rent__c>> mapMRSchIDToRent= new map<id,List<Rent__c>>();

		list<Rent__c> listRentRec=[select id, name,Start_Date__c,Rent_Reset_Date__c,Actual_Interest_Rate__c, Stepped_Rent__c,Stepped_Rent__r.Rent_Type__c, Stepped_Rent__r.Interest_Rate_Index__c,Cost_Of_Funds__c,
									Rental_Adjustment_Factor__c,Escalation_Factor__c,Assumed_Interest_Rate__c,Rent_Due_Amount__c,Rent_Amount_Due_After_Prorate__c,Rental_Credit_Debit__c,
									(select id,Status__c from Invoices__r order by createddate desc) 
                                    from Rent__c
                                    where Stepped_Rent__c =:setMultiRentSchID
                                    and Actual_Interest_Rate__c=Null
									and Rent_Reset_Date__c!=Null
									and Rent_Reset_Date__c<=:todaysdate //Less than Today's date
                                    order by Start_Date__c desc
                                    ];
									
        for(Rent__c curRentRec: listRentRec){
			if(!mapMRSchIDToRent.containsKey(curRentRec.Stepped_Rent__c)){
				mapMRSchIDToRent.put(curRentRec.Stepped_Rent__c,new List<Rent__c>());
			}
			mapMRSchIDToRent.get(curRentRec.Stepped_Rent__c).add(curRentRec);
		}
		
		if(!'Scheduler'.equals(strCalledFrom)){
            mapGetActIntRate= new map<string,decimal>();
			for(stepped_rent__c curmultirentrec : [select id,Interest_Rate_Index__c from stepped_rent__c where id=:setMultiRentSchID]){
				setIntRateIndxIds.add(curmultirentrec.Interest_Rate_Index__c);
			}
			if(setIntRateIndxIds.size()>0){	
				for(Interest_Rate_Index_Daily__c curIRID:[select id,name , Interest_Rate_Date__c,Interest_Rate_Index__c,Interest_Rate_Prcntg__c from Interest_Rate_Index_Daily__c where Interest_Rate_Index__c =:setIntRateIndxIds]){
					mapGetActIntRate.put(curIRID.Interest_Rate_Index__c+'-'+curIRID.Interest_Rate_Date__c,curIRID.Interest_Rate_Prcntg__c);
				}
			}
        }
        system.debug('size of mapMRSchIDToRent='+mapMRSchIDToRent.size());
		system.debug('size of mapGetActIntRate='+mapGetActIntRate.size());
        checkingInvRentRec(mapMRSchIDToRent,mapGetActIntRate);
        system.debug('UpdateBalanceDueOnLeaseScheduler.FetchRentRecToUpd(-)');
		
    }
    /*Using this method doing below actions
    1. Finding all latest floating rent slip which has no invoice or Declined or Credited  
    2. after finding all list of floating rent slip, using Interest ratre Index and reset date will check Actual interest rate paresent in the Interest Rate Index Daily object
    3. if record found then fetch actual interest rate and update in the rent else send email to lease owner */
	public static void checkingInvRentRec(map<id,List<Rent__c>> mapMRSchIDToRent,map<string,decimal> mapGetActIntRate){
        system.debug('UpdateBalanceDueOnLeaseScheduler.checkingInvRentRec(+)');

		if(mapMRSchIDToRent.size()>0) {
            list<rent__c> listUpdFloatingRentDontInv = new list<rent__c>();
            list<rent__c> updListRent = new list<rent__c>();
            set<id> setMRSchId= new set<id>();
            Integer InvFlag=1; // if InvFlag =1 means there is no invoice for that rent. if InvFlag=0 means invoice generated and can't delete that rent record
            for(id MRSchID: mapMRSchIDToRent.keySet()){ //for multiple MultiRentSchedule records, once rent has active invoice then break and jump for another multirent record
				for(Rent__C curFloatingRentRec: mapMRSchIDToRent.get(MRSchID)){
					system.debug('=Rent_Reset_Date='+curFloatingRentRec.Rent_Reset_Date__c+'=='+curFloatingRentRec.Stepped_Rent__r.Interest_Rate_Index__c);
					if(curFloatingRentRec.Invoices__r.size()==0){
						listUpdFloatingRentDontInv.add(curFloatingRentRec);
					}
					else{
						for(Invoice__c curInv: curFloatingRentRec.Invoices__r){
							if(InvFlag!=0 && ('Pending'.equals(curInv.Status__c) || 'Approved'.equals(curInv.Status__c) || 'Cancelled-Pending'.equals(curInv.Status__c)||'Paid'.equals(curInv.Status__c)||'Partially Paid'.equals(curInv.Status__c))) {
								InvFlag=0; // used to skip those rent id who has invoice
								break;
							}
						}
						if(InvFlag==1){ // For case: Declined or Credited 
							listUpdFloatingRentDontInv.add(curFloatingRentRec);
						}
					}
					if(InvFlag==0) break; // Exit completly from Rent loop if found any active Invoice.
				}// End of Rent loop
			}// End of MultiRentSchedule loop
			decimal Final_Interest_Rate, Rental_Adjustment_Amount,getActIntRate;
			for(Rent__C curRentRec: listUpdFloatingRentDontInv){
                getActIntRate=mapGetActIntRate.get(curRentRec.Stepped_Rent__r.Interest_Rate_Index__c+'-'+curRentRec.Rent_Reset_Date__c);                
                if(getActIntRate==Null){
                    //below set will to send email aler for missing Actual interest rate
                    setMRSchId.add(curRentRec.Stepped_Rent__c);
                    continue; 
                }
                else if(curRentRec.Actual_Interest_Rate__c==getActIntRate){
                    continue; 
                }
                Final_Interest_Rate=getActIntRate + leasewareUtils.zeroIfNull(curRentRec.Cost_Of_Funds__c) - leasewareUtils.zeroIfNull(curRentRec.Assumed_Interest_Rate__c);
                curRentRec.Rent_Due_Amount__c = curRentRec.Rent_Amount_Due_After_Prorate__c * curRentRec.Escalation_Factor__c;
                curRentRec.Actual_Interest_Rate__c=getActIntRate;               
                Rental_Adjustment_Amount= leasewareUtils.zeroIfNull(curRentRec.Rental_Adjustment_Factor__c) * leasewareUtils.zeroIfNull(Final_Interest_Rate) * curRentRec.Escalation_Factor__c;
				system.debug('=Rent_Due_Amount__c='+curRentRec.Rent_Due_Amount__c+'=Rental_Adjustment_Amount='+Rental_Adjustment_Amount);
				if(curRentRec.Rent_Due_Amount__c!=Null && Rental_Adjustment_Amount != NULL ) {
				    curRentRec.Rent_Due_Amount__c = curRentRec.Rent_Due_Amount__c + Rental_Adjustment_Amount; //curRent.Rental_Adjustment_Amount__c;
				}
				if(curRentRec.Rent_Due_Amount__c!=Null && curRentRec.Rental_Credit_Debit__c != NULL) {
				    curRentRec.Rent_Due_Amount__c = curRentRec.Rent_Due_Amount__c +curRentRec.Rental_Credit_Debit__c;
				}
				updListRent.add(curRentRec);				
			}
		  	system.debug('size of updListRent='+updListRent.size());
			LeaseWareUtils.TriggerDisabledFlag=true; // trigger disabled
            if(updListRent.size()>0){
				update updListRent;  
            }
            //To Send email alret
           if(setMRSchId.size()>0) {
                try{
                    UpdateBalanceDueOnLeaseScheduler.sendEmailAlert(setMRSchId);
                }
                catch(exception ex){
                    LeaseWareUtils.createExceptionLog(ex,'Floating Rent :Send Email failed'+ex.getMessage() ,null,null,null,false);
                    system.debug('Error Message: '+ ex);
                }
            }
               

            LeaseWareUtils.TriggerDisabledFlag=false; // trigger enabled
            system.debug('UpdateBalanceDueOnLeaseScheduler.checkingInvRentRec(-)');
		}
    }
    
    //(start_date__c<=todaysdate) and (rent_end_date__c>= todaysdate)
    public static void CreatListLeaseToMultiRentRec(string LeaseID){
        date todaysdate= system.today();
        system.debug('creatListLeaseToMultiRentRec(+)');
        list<lease__c> listLeaseToMultiRentRec = [ 
                select id,name,Base_Rent__c,Rent_Date__c,Rent_End_Date__c,Lease_Start_Date_New__c,Lease_End_Date_New__c
                ,Applicable_Rent__c
                ,Rent__c,Lessee_Account_Name__c,Y_Hidden_MRSchModifyDt__c, //Using this Label -"Y_Hidden MultiRentSch ID"(API- Lessee_Account_Name__c) [earlier marked earlier as Z_Unused field, using this field due to shortage of new field creation on lease] field to store currently used MultiRentid from Multi Rent schedule object
                (select id,name,Lease__c,rent_type__c, rent_period__c,rent_comment__c,
                    start_date__c,rent_end_date__c,invoice_generation_day__c,rent_period_end_day__c,off_wing_rent__c,
                    rent_due_type__c, rent_due_day__c, due_day_correction__c, pro_rata_number_of_days__c, 
                    assumed_interest_rate__c,rental_credit_debit__c, cost_of_funds__c,
                    base_rent__c, LastModifiedDate 
                 from Stepped_Rents__r
                 where Status__c=true
                 order by start_date__c,createddate
                )
                from lease__c
                where id=:LeaseID ];
            system.debug('=size of listLeaseToMultiRentRec= ' + listLeaseToMultiRentRec.size());
            
            UpdateBalanceDueOnLeaseScheduler.CopyFieldMRSchToLease(listLeaseToMultiRentRec,'RentStudio');
        system.debug('creatListLeaseToMultiRentRec(-)');
    }
    
    public static void sendEmailAlert(set<id> setMRSchID){
        system.debug('UpdateBalanceDueOnLeaseScheduler.sendEmailAlert(+)');
		List<Stepped_Rent__c> listMRSchRec=[select id,name,lease__c,lease__r.name,lease__r.Owner.Email, CreatedBy.Email 
                             from Stepped_Rent__c where id=:setMRSchID];
        system.debug('listMRSchRec='+listMRSchRec.size());
        String prefix = leasewareutils.getNamespacePrefix();
        for(Stepped_Rent__c curMRSchRec: listMRSchRec){
            String emailBody ='';
            String URLLink = URL.getSalesforceBaseUrl().toExternalForm();
            emailBody+=('Dear User,');
            emailBody+=('<br></br>The Floating Rent for this lease needs to be calculated. Please enter any missing Actual Interest Rate(s) in the Interest Rate Index (Daily),'
                +'<br></br>and then go to the Lease page > Rent Studio > Rent Schedule and click the quick action button <q>Generate Pending Periods.</q>'
                +'<br></br><b>Reference Link:</b>');
            emailBody+=('<br></br>Lease Name: <a href="' + URLLink+ '/' + curMRSchRec.lease__c + '">' + curMRSchRec.lease__r.name + '</a>');
            URLLink += '/lightning/o/'+prefix+'Interest_Rate_Index__c/list?filterName=All';
            emailBody+=('<br></br>Interest Rate Index Tab: <a href="'  + URLLink + '">' + 'Interest Rate Index' + '</a>');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setHtmlBody(emailBody);
            mail.setToAddresses( new String[] { curMRSchRec.lease__r.Owner.Email,curMRSchRec.CreatedBy.Email} );
            mail.setSubject('Alert: A Floating Rent Interest Rate is overdue');
            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
        }
        system.debug('UpdateBalanceDueOnLeaseScheduler.sendEmailAlert(-)');
    }

}