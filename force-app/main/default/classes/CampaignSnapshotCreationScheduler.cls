public with sharing class CampaignSnapshotCreationScheduler implements Schedulable, Database.Batchable<SObject>{

	// Date, Campaign Name, Prospect, Weekly Summary, Action Item, Category, Rep Assignment and link to the Prospect Record.
	// Date : Creation date 
	//  Rep Assignment : will add later not there in latest code
	
	String query = 'select Deal__r.name,Id,Overall_Status__c,Action__c,Category__c ,Marketing_Representative__r.name,Deal__c,Name from Deal_Proposal__c ' 
					+ ' where Action__c != null OR Overall_Status__c != null ';
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new CampaignSnapshotCreationScheduler());
    }
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }
    public void execute(Database.BatchableContext bc, List<Deal_Proposal__c> listProspects) {
        System.debug('CampaignSnapshotCreationScheduler.execute : Start()');
        if(!listProspects.isEmpty()){
        	list<Campaign_Snapshot__c> listCS = new list<Campaign_Snapshot__c>();
        	Date todayDate = System.today();
        	for(Deal_Proposal__c curRec:listProspects){
        		listCS.add(new Campaign_Snapshot__c(
        			Name =curRec.Deal__r.name 
        			,Prospect__c =  curRec.Id
        			,Weekly_Summary__c = curRec.Overall_Status__c
        			,Action_Item__c = curRec.Action__c
        			,Category__c = curRec.Category__c
        			,Marketing_Representative__c = curRec.Marketing_Representative__r.name
        			,Date__c = todayDate
        			,Campaign__c = curRec.Deal__c
        			,Prospect_Name__c = curRec.Name
        			)
        		);
        	}
        	if(!listCS.isEmpty()){
        		System.debug('CampaignSnapshotCreationScheduler.execute : NoOfInsertedRecord() :' + listCS.size());
        		insert listCS;
        	}
        	
        }
        // purge case 
        
        Lessor__c[] fetchedSetup = [select Id,Purge_Campaign_Snapshots_Older_than_Mos__c from Lessor__c limit 1];
        Decimal PurgeNoOfMonths = -999999 ;
        if(fetchedSetup.size() ==1 && fetchedSetup[0].Purge_Campaign_Snapshots_Older_than_Mos__c != null ){
        	 PurgeNoOfMonths = fetchedSetup[0].Purge_Campaign_Snapshots_Older_than_Mos__c ;
        }
        System.debug('CampaignSnapshotCreationScheduler.execute : PurgeNoOfMonths() :' + PurgeNoOfMonths);
        if(PurgeNoOfMonths != -999999){
        	Date todayDate = System.today().addMonths(-1*PurgeNoOfMonths.intValue());
        	delete new List<Campaign_Snapshot__c>([select Id from Campaign_Snapshot__c where Date__c < = :todayDate ]);
        }
        System.debug('CampaignSnapshotCreationScheduler.execute : End()');
    }
    public void finish(Database.BatchableContext bc) {
        System.debug('CampaignSnapshotCreationScheduler.finish ');
        // No email or log send
    }





}