public class CapitalizedCostHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'CapitalizedCostHandlerBefore';
    private final string triggerAfter = 'CapitalizedCostHandlerAfter';
    public CapitalizedCostHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('CapitalizedCostHandler.beforeInsert(+)');

	    system.debug('CapitalizedCostHandler.beforeInsert(-)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('CapitalizedCostHandler.beforeUpdate(+)');

		Capitalized_Cost__c[] newACs = (list<Capitalized_Cost__c>)trigger.new;
 	
	   	for(Capitalized_Cost__c newAC : newACs){
	        if(newAC.Y_Hidden_Generate_Depreciation_Schedule__c){
	        	newAC.Y_Hidden_Generate_Depreciation_Schedule__c=false;
	        	generateEquipDS(newAC);
	        }
	    }    	    	
    	
    	system.debug('CapitalizedCostHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('CapitalizedCostHandler.beforeDelete(+)');
    	
		Capitalized_Cost__c[] newACs = (list<Capitalized_Cost__c>)trigger.old;
		
 		LeaseWareUtils.setFromTrigger('DeletedFromAircraftTriggerHandler');
 		//LeaseWareUtils.setFromTrigger('UpdatedFromAircraftTriggerHandler');
 		
        delete [select Id from Equipment_Depreciation_Schedule__c where Capitalized_Cost__c in :newACs];
      	LeaseWareUtils.unsetTriggers('DeletedFromAircraftTriggerHandler');
      	//LeaseWareUtils.unsetTriggers('UpdatedFromAircraftTriggerHandler');
      	
    	system.debug('CapitalizedCostHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CapitalizedCostHandler.afterInsert(+)');
  	
    	system.debug('CapitalizedCostHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CapitalizedCostHandler.afterUpdate(+)');
    	
  		

    	system.debug('CapitalizedCostHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CapitalizedCostHandler.afterDelete(+)');
    	
    	
    	system.debug('CapitalizedCostHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CapitalizedCostHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('CapitalizedCostHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
	

	private static void createEquipDS(list<Aircraft_Depreciation_Schedule__c> insertAircraftDS,Capitalized_Cost__c CCRec){

		Integer lastCounter= 1,startParentPointer;
		Decimal aircraftCost;
		Decimal comulativeDepreciation =0,comulativeDepreciationAmount=0,DepreciationAmountMonthly=0,tempDepreciationAmountMonthly=0,carryoverLastYearPartialAmount=0;
		Boolean partialMonthFlag=false;
		Depreciation_schedule__c listEquipDR;
		Date tempDate;
		
		Id DSID;
		Decimal EquipCost,LastDepreciatedAmt=0;
		Date DepStartDate;
		Decimal Depreciation_Rate_local=0;
		boolean DepreciatToResidualFlag=false;
		Decimal ResidualValue = LeasewareUtils.zeroIfNull(CCRec.Residual_Value__c) ; 
		Decimal SetResidualvalue = 0;
		//Residual_Value__c
		
		list<Equipment_Depreciation_Schedule__c> insertEquiptDS = new list<Equipment_Depreciation_Schedule__c>();

			DSID = CCRec.Book_Depreciation_Schedule_F__c ;
			EquipCost = CCRec.Book_Value__c ;
			DepStartDate = CCRec.Start_Date__c ;
	
			if(DSID!=null)
				listEquipDR = [select id,Number_of_Years__c ,Depreciation_Method__c,Depreciate_Till_Residual_Value__c
								,(select Id,Depreciation_Rate__c,Period_Sequence__c from Depreciation_Rate__r order by Period_Sequence__c )
								from Depreciation_schedule__c  where Id = :DSID ];
	
	
			if(EquipCost!=null && DSID!=null){
				//find the pointer of parent
				Integer j=0;
				for(Aircraft_Depreciation_Schedule__c curADSRec:insertAircraftDS){
					if(curADSRec.name==LeaseWareUtils.getDatetoString(DepStartDate,'YYYY-MM')) break;
					j++;
				}
				startParentPointer = j;
				system.debug('startParentPointer='+startParentPointer);
				tempDate = DepStartDate;
				lastCounter= 1;
				comulativeDepreciation =0;comulativeDepreciationAmount=0;DepreciationAmountMonthly=0;tempDepreciationAmountMonthly=0;carryoverLastYearPartialAmount=0;
				partialMonthFlag=false;
				Decimal DepreciationAmountAnnually =0;
				Decimal NetDepYearly,DepYearly;
				Decimal NetDepSL,DepSL,DepDB,NetDepDB ;// only for Hybrid case
				for(Integer iter=0;iter<listEquipDR.Number_of_Years__c ;iter++){
					
					//Straight Line,MACRS,Double Declining Balance,Hybrid - DDB And Straight Line,150% Declining Balance,Other
					if(listEquipDR.Depreciation_Method__c == 'Straight Line'){
						
						if(iter==0) {
							aircraftCost = EquipCost - ResidualValue;
							Depreciation_Rate_local = 100/listEquipDR.Number_of_Years__c;
							SetResidualvalue = ResidualValue;
						}
						DepreciationAmountAnnually = aircraftCost * 	Depreciation_Rate_local/(100.00);
					}
					else if(listEquipDR.Depreciation_Method__c == 'Double Declining Balance'){
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepYearly = EquipCost;	 
							Depreciation_Rate_local = 200/listEquipDR.Number_of_Years__c;	// New rate for cur Year
						}	
						
						LastDepreciatedAmt = NetDepYearly;
						DepYearly = NetDepYearly * 	Depreciation_Rate_local/(100.00);
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						
						NetDepYearly = NetDepYearly - DepYearly;
						
						DepreciationAmountAnnually =DepYearly;
						
					}else if(listEquipDR.Depreciation_Method__c == '150% Declining Balance'){
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepYearly = EquipCost;	 
							Depreciation_Rate_local = 150/listEquipDR.Number_of_Years__c;	// New rate for cur Year
						}	
						
						LastDepreciatedAmt = NetDepYearly;
						DepYearly = NetDepYearly * 	Depreciation_Rate_local/(100.00);
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						
						NetDepYearly = NetDepYearly - DepYearly;
						
						DepreciationAmountAnnually =DepYearly;
						
					}else if(listEquipDR.Depreciation_Method__c == 'Hybrid - DDB And Straight Line'){// TODO Hybrid - DDB And Straight Line
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepSL = EquipCost;
							NetDepDB = EquipCost;	 
							Depreciation_Rate_local = 200/listEquipDR.Number_of_Years__c;	// New rate for cur Year
							DepSL = NetDepSL * 	(100/listEquipDR.Number_of_Years__c)/(100.00);
							NetDepYearly = NetDepDB;
						}	
						
						LastDepreciatedAmt = NetDepDB;
						DepDB = NetDepDB * 	Depreciation_Rate_local/(100.00);
						NetDepDB = NetDepDB - DepDB;
						
						DepYearly = DepDB ;
						if(DepYearly<DepSL) DepYearly = DepSL;
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						NetDepYearly = NetDepYearly - DepYearly;
						
						
						DepreciationAmountAnnually =DepYearly;					
	
					}
					else if(listEquipDR.Depreciation_Method__c == 'Hybrid - 150% DB And Straight Line'){// TODO Hybrid - DDB And Straight Line
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepSL = EquipCost;
							NetDepDB = EquipCost;	 
							Depreciation_Rate_local = 150/listEquipDR.Number_of_Years__c;	// New rate for cur Year
							DepSL = NetDepSL * 	(100/listEquipDR.Number_of_Years__c)/(100.00);
							NetDepYearly = NetDepDB;
						}	
						
						LastDepreciatedAmt = NetDepDB;
						DepDB = NetDepDB * 	Depreciation_Rate_local/(100.00);
						NetDepDB = NetDepDB - DepDB;
						
						DepYearly = DepDB ;
						if(DepYearly<DepSL) DepYearly = DepSL;
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						NetDepYearly = NetDepYearly - DepYearly;
						
						
						DepreciationAmountAnnually =DepYearly;	
					}				
					else{
						if(iter==0) {
							aircraftCost = EquipCost - ResidualValue;
							SetResidualvalue = ResidualValue;
						}
						Depreciation_Rate_local = listEquipDR.Depreciation_Rate__r[iter].Depreciation_Rate__c;
						DepreciationAmountAnnually = aircraftCost * 	Depreciation_Rate_local/(100.00);
			
					}
					
						//lastCounter = lastCounter +1;
						comulativeDepreciation =comulativeDepreciation+ Depreciation_Rate_local/100;
						// Depricate till Residual value at the end
	
						DepreciationAmountAnnually = DepreciationAmountAnnually<=0?0:DepreciationAmountAnnually; 
						
						DepreciationAmountMonthly= DepreciationAmountAnnually/12.00;
						system.debug('comulativeDepreciation'+comulativeDepreciation);
						system.debug('DepreciationAmountMonthly'+DepreciationAmountMonthly);
						for(Integer i=1;i<=12;i++){
							tempDepreciationAmountMonthly = DepreciationAmountMonthly ;
							if(i==1 && tempDate.day()!=1){ 
								partialMonthFlag=true; 
								tempDepreciationAmountMonthly = carryoverLastYearPartialAmount + DepreciationAmountMonthly * (Date.daysInMonth(tempDate.year(), tempDate.month())- tempDate.day() +1)/(365.0/12);
								carryoverLastYearPartialAmount = DepreciationAmountMonthly -  DepreciationAmountMonthly * (Date.daysInMonth(tempDate.year(), tempDate.month())- tempDate.day() +1)/(365.0/12);
							}                        // prorate
							
							comulativeDepreciationAmount = comulativeDepreciationAmount + tempDepreciationAmountMonthly;
							system.debug('comulativeDepreciationAmount'+comulativeDepreciationAmount);
							
							system.debug('insertAircraftDS.size()='+insertAircraftDS.size());
							system.debug('startParentPointer+lastCounter='+(startParentPointer+lastCounter));
							if(insertAircraftDS.size()>=(startParentPointer+lastCounter)){
								system.debug('value i='+i);
								insertEquiptDS.add(new Equipment_Depreciation_Schedule__c(name = LeaseWareUtils.getDatetoString(tempDate,'YYYY-MM')
									,Aircraft_Depreciation_Schedule__c = 	insertAircraftDS[startParentPointer+lastCounter-1].Id
									,Depreciation_Percentage__c = Depreciation_Rate_local
									,Depreciation__c =   tempDepreciationAmountMonthly
									,Period_Sequence__c = lastCounter ++
									,Salvage_Value__c = aircraftCost- comulativeDepreciationAmount
									,Depreciation_Date__c =tempDate
									,Depreciation_to_Date__c = comulativeDepreciationAmount
									,Capitalized_Cost__c =CCRec.Id 
									));
							}
							tempDate = 	DepStartDate.addMonths(lastCounter-1);	
						}
						// e.g if 2nd year counter 13
						//if( partialMonthFlag)  carryoverLastYearPartialAmount =  DepreciationAmountMonthly * (tempDate.day()-1)/(365.0/12);                          // prorate
						if(partialMonthFlag && lastCounter>=listEquipDR.Number_of_Years__c*12){
							tempDepreciationAmountMonthly = carryoverLastYearPartialAmount;
							comulativeDepreciationAmount = comulativeDepreciationAmount + tempDepreciationAmountMonthly;
							if(insertAircraftDS.size()>=(startParentPointer+lastCounter)){
								insertEquiptDS.add(new Equipment_Depreciation_Schedule__c(name = LeaseWareUtils.getDatetoString(tempDate,'YYYY-MM')
									,Aircraft_Depreciation_Schedule__c = 	insertAircraftDS[startParentPointer+lastCounter-1].Id
									,Depreciation_Percentage__c = Depreciation_Rate_local
									,Depreciation__c =   tempDepreciationAmountMonthly
									,Period_Sequence__c = lastCounter ++
									,Salvage_Value__c = aircraftCost- comulativeDepreciationAmount
									,Depreciation_Date__c =tempDate
									,Depreciation_to_Date__c = comulativeDepreciationAmount
									,Capitalized_Cost__c =CCRec.Id 
									));
	
								lastCounter ++;
							}
					
						}
						
					}
			}// end if If Book DS

			// Start for Statutory DS
			DSID = CCRec.Statutory_Depreciation_Schedule_F__c;
			EquipCost = CCRec.Statutory_Value__c ;
			DepStartDate = CCRec.Start_Date__c ;
			DepreciatToResidualFlag=false;
			Depreciation_Rate_local=0;
			LastDepreciatedAmt=0;	
			if(DSID!=null)
				listEquipDR = [select id,Number_of_Years__c ,Depreciation_Method__c,Depreciate_Till_Residual_Value__c
								,(select Id,Depreciation_Rate__c,Period_Sequence__c from Depreciation_Rate__r order by Period_Sequence__c )
								from Depreciation_schedule__c  where Id = :DSID ];
			
			if(EquipCost!=null && DSID!=null){
				//find the pointer of parent
				Integer j=0;
				for(Aircraft_Depreciation_Schedule__c curADSRec:insertAircraftDS){
					if(curADSRec.name==LeaseWareUtils.getDatetoString(DepStartDate,'YYYY-MM')) break;
					j++;
				}
				startParentPointer = j;
				system.debug('startParentPointer='+startParentPointer);
				tempDate = DepStartDate;
				lastCounter= 1;
				comulativeDepreciation =0;comulativeDepreciationAmount=0;DepreciationAmountMonthly=0;tempDepreciationAmountMonthly=0;carryoverLastYearPartialAmount=0;
				partialMonthFlag=false;
			Decimal DepreciationAmountAnnually =0;
			Decimal NetDepYearly,DepYearly;
			Decimal NetDepSL,DepSL,DepDB,NetDepDB ;// only for Hybrid case
				
				for(Integer iter=0;iter<listEquipDR.Number_of_Years__c ;iter++){
					
					//Straight Line,MACRS,Double Declining Balance,Hybrid - DDB And Straight Line,150% Declining Balance,Other
					if(listEquipDR.Depreciation_Method__c == 'Straight Line'){
						
						if(iter==0) {
							aircraftCost = EquipCost - ResidualValue;
							Depreciation_Rate_local = 100/listEquipDR.Number_of_Years__c;
							SetResidualvalue = ResidualValue;
						}
						DepreciationAmountAnnually = aircraftCost * 	Depreciation_Rate_local/(100.00);
					}
					else if(listEquipDR.Depreciation_Method__c == 'Double Declining Balance'){
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepYearly = EquipCost;	 
							Depreciation_Rate_local = 200/listEquipDR.Number_of_Years__c;	// New rate for cur Year
						}	
						
						LastDepreciatedAmt = NetDepYearly;
						DepYearly = NetDepYearly * 	Depreciation_Rate_local/(100.00);
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						
						NetDepYearly = NetDepYearly - DepYearly;
						
						DepreciationAmountAnnually =DepYearly;
						
					}else if(listEquipDR.Depreciation_Method__c == '150% Declining Balance'){
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepYearly = EquipCost;	 
							Depreciation_Rate_local = 150/listEquipDR.Number_of_Years__c;	// New rate for cur Year
						}	
						
						LastDepreciatedAmt = NetDepYearly;
						DepYearly = NetDepYearly * 	Depreciation_Rate_local/(100.00);
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						
						NetDepYearly = NetDepYearly - DepYearly;
						
						DepreciationAmountAnnually =DepYearly;
						
					}else if(listEquipDR.Depreciation_Method__c == 'Hybrid - DDB And Straight Line'){// TODO Hybrid - DDB And Straight Line
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepSL = EquipCost;
							NetDepDB = EquipCost;	 
							Depreciation_Rate_local = 200/listEquipDR.Number_of_Years__c;	// New rate for cur Year
							DepSL = NetDepSL * 	(100/listEquipDR.Number_of_Years__c)/(100.00);
							NetDepYearly = NetDepDB;
						}	
						
						LastDepreciatedAmt = NetDepDB;
						DepDB = NetDepDB * 	Depreciation_Rate_local/(100.00);
						NetDepDB = NetDepDB - DepDB;
						
						DepYearly = DepDB ;
						if(DepYearly<DepSL) DepYearly = DepSL;
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						NetDepYearly = NetDepYearly - DepYearly;
						
						
						DepreciationAmountAnnually =DepYearly;					
	
					}
					else if(listEquipDR.Depreciation_Method__c == 'Hybrid - 150% DB And Straight Line'){// TODO Hybrid - DDB And Straight Line
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepSL = EquipCost;
							NetDepDB = EquipCost;	 
							Depreciation_Rate_local = 150/listEquipDR.Number_of_Years__c;	// New rate for cur Year
							DepSL = NetDepSL * 	(100/listEquipDR.Number_of_Years__c)/(100.00);
							NetDepYearly = NetDepDB;
						}	
						
						LastDepreciatedAmt = NetDepDB;
						DepDB = NetDepDB * 	Depreciation_Rate_local/(100.00);
						NetDepDB = NetDepDB - DepDB;
						
						DepYearly = DepDB ;
						if(DepYearly<DepSL) DepYearly = DepSL;
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						NetDepYearly = NetDepYearly - DepYearly;
						
						
						DepreciationAmountAnnually =DepYearly;	
					}				
					else{
						if(iter==0) {
							aircraftCost = EquipCost - ResidualValue;
							SetResidualvalue = ResidualValue;
						}
						Depreciation_Rate_local = listEquipDR.Depreciation_Rate__r[iter].Depreciation_Rate__c;
						DepreciationAmountAnnually = aircraftCost * 	Depreciation_Rate_local/(100.00);
			
					}
					
						//lastCounter = lastCounter +1;
						comulativeDepreciation =comulativeDepreciation+ Depreciation_Rate_local/100;
						// Depricate till Residual value at the end
	
						DepreciationAmountAnnually = DepreciationAmountAnnually<=0?0:DepreciationAmountAnnually; 
						
						DepreciationAmountMonthly= DepreciationAmountAnnually/12.00;
						system.debug('comulativeDepreciation'+comulativeDepreciation);
						system.debug('DepreciationAmountMonthly'+DepreciationAmountMonthly);
						for(Integer i=1;i<=12;i++){
							tempDepreciationAmountMonthly = DepreciationAmountMonthly ;
							if(i==1 && tempDate.day()!=1){ 
								partialMonthFlag=true; 
								tempDepreciationAmountMonthly = carryoverLastYearPartialAmount + DepreciationAmountMonthly * (Date.daysInMonth(tempDate.year(), tempDate.month())- tempDate.day() +1)/(365.0/12);
								carryoverLastYearPartialAmount = DepreciationAmountMonthly -  DepreciationAmountMonthly * (Date.daysInMonth(tempDate.year(), tempDate.month())- tempDate.day() +1)/(365.0/12);
							}                        // prorate
							
							comulativeDepreciationAmount = comulativeDepreciationAmount + tempDepreciationAmountMonthly;
							system.debug('comulativeDepreciationAmount'+comulativeDepreciationAmount);
							
							system.debug('insertAircraftDS.size()='+insertAircraftDS.size());
							system.debug('startParentPointer+lastCounter='+(startParentPointer+lastCounter));
							if(insertAircraftDS.size()>=(startParentPointer+lastCounter)){
								system.debug('value i='+i);
								if(insertEquiptDS.size()>=lastCounter){
									insertEquiptDS[lastCounter-1].Statutory_Depreciation_Percentage__c =Depreciation_Rate_local;
									insertEquiptDS[lastCounter-1].Statutory_Depreciation__c = tempDepreciationAmountMonthly;
									insertEquiptDS[lastCounter-1].Statutory_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount;
									insertEquiptDS[lastCounter-1].Statutory_Depr_to_Date__c = comulativeDepreciationAmount;
									lastCounter ++;
								}
								else{								
									insertEquiptDS.add(new Equipment_Depreciation_Schedule__c(name = LeaseWareUtils.getDatetoString(tempDate,'YYYY-MM')
										,Aircraft_Depreciation_Schedule__c = 	insertAircraftDS[startParentPointer+lastCounter-1].Id
										,Statutory_Depreciation_Percentage__c = Depreciation_Rate_local
										,Statutory_Depreciation__c =   tempDepreciationAmountMonthly
										,Period_Sequence__c = lastCounter ++
										,Statutory_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount
										,Depreciation_Date__c =tempDate
										,Statutory_Depr_to_Date__c = comulativeDepreciationAmount
										,Capitalized_Cost__c =CCRec.Id 
										));
								}
							}
							tempDate = 	DepStartDate.addMonths(lastCounter-1);	
						}
						// e.g if 2nd year counter 13
						//if( partialMonthFlag)  carryoverLastYearPartialAmount =  DepreciationAmountMonthly * (tempDate.day()-1)/(365.0/12);                          // prorate
						if(partialMonthFlag && lastCounter>=listEquipDR.Number_of_Years__c*12){
							tempDepreciationAmountMonthly = carryoverLastYearPartialAmount;
							comulativeDepreciationAmount = comulativeDepreciationAmount + tempDepreciationAmountMonthly;
							if(insertAircraftDS.size()>=(startParentPointer+lastCounter)){
								if(insertEquiptDS.size()>=lastCounter){
									insertEquiptDS[lastCounter-1].Statutory_Depreciation_Percentage__c = Depreciation_Rate_local;
									insertEquiptDS[lastCounter-1].Statutory_Depreciation__c = tempDepreciationAmountMonthly;
									insertEquiptDS[lastCounter-1].Statutory_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount;
									insertEquiptDS[lastCounter-1].Statutory_Depr_to_Date__c = comulativeDepreciationAmount;
									//lastCounter ++;
								}
								else{									
									insertEquiptDS.add(new Equipment_Depreciation_Schedule__c(name = LeaseWareUtils.getDatetoString(tempDate,'YYYY-MM')
										,Aircraft_Depreciation_Schedule__c = 	insertAircraftDS[startParentPointer+lastCounter-1].Id
										,Statutory_Depreciation_Percentage__c = Depreciation_Rate_local
										,Statutory_Depreciation__c =   tempDepreciationAmountMonthly
										,Period_Sequence__c = lastCounter ++
										,Statutory_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount
										,Depreciation_Date__c =tempDate
										,Statutory_Depr_to_Date__c = comulativeDepreciationAmount
										,Capitalized_Cost__c =CCRec.Id 
										));
	
								}
							}
					
						}
						
					}
			}// end if If Statutory DS

			// Start for Tax DS
			DSID = CCRec.Tax_Depreciation_Schedule_F__c;
			EquipCost = CCRec.Tax_Value__c ;
			DepStartDate = CCRec.Start_Date__c ;
			DepreciatToResidualFlag=false;
			Depreciation_Rate_local=0;
			LastDepreciatedAmt=0;
			if(DSID!=null)
				listEquipDR = [select id,Number_of_Years__c ,Depreciation_Method__c,Depreciate_Till_Residual_Value__c
								,(select Id,Depreciation_Rate__c,Period_Sequence__c from Depreciation_Rate__r order by Period_Sequence__c )
								from Depreciation_schedule__c  where Id = :DSID ];
			
			if(EquipCost!=null && DSID!=null){
				//find the pointer of parent
				Integer j=0;
				for(Aircraft_Depreciation_Schedule__c curADSRec:insertAircraftDS){
					if(curADSRec.name==LeaseWareUtils.getDatetoString(DepStartDate,'YYYY-MM')) break;
					j++;
				}
				startParentPointer = j;
				system.debug('startParentPointer='+startParentPointer);
				tempDate = DepStartDate;
				lastCounter= 1;
				comulativeDepreciation =0;comulativeDepreciationAmount=0;DepreciationAmountMonthly=0;tempDepreciationAmountMonthly=0;carryoverLastYearPartialAmount=0;
				partialMonthFlag=false;
			Decimal DepreciationAmountAnnually =0;
			Decimal NetDepYearly,DepYearly;
			Decimal NetDepSL,DepSL,DepDB,NetDepDB ;// only for Hybrid case
				for(Integer iter=0;iter<listEquipDR.Number_of_Years__c ;iter++){
					
					//Straight Line,MACRS,Double Declining Balance,Hybrid - DDB And Straight Line,150% Declining Balance,Other
					if(listEquipDR.Depreciation_Method__c == 'Straight Line'){
						
						if(iter==0) {
							aircraftCost = EquipCost - ResidualValue;
							Depreciation_Rate_local = 100/listEquipDR.Number_of_Years__c;
							SetResidualvalue = ResidualValue;
						}
						DepreciationAmountAnnually = aircraftCost * 	Depreciation_Rate_local/(100.00);
					}
					else if(listEquipDR.Depreciation_Method__c == 'Double Declining Balance'){
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepYearly = EquipCost;	 
							Depreciation_Rate_local = 200/listEquipDR.Number_of_Years__c;	// New rate for cur Year
						}	
						
						LastDepreciatedAmt = NetDepYearly;
						DepYearly = NetDepYearly * 	Depreciation_Rate_local/(100.00);
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						
						NetDepYearly = NetDepYearly - DepYearly;
						
						DepreciationAmountAnnually =DepYearly;
						
					}else if(listEquipDR.Depreciation_Method__c == '150% Declining Balance'){
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepYearly = EquipCost;	 
							Depreciation_Rate_local = 150/listEquipDR.Number_of_Years__c;	// New rate for cur Year
						}	
						
						LastDepreciatedAmt = NetDepYearly;
						DepYearly = NetDepYearly * 	Depreciation_Rate_local/(100.00);
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						
						NetDepYearly = NetDepYearly - DepYearly;
						
						DepreciationAmountAnnually =DepYearly;
						
					}else if(listEquipDR.Depreciation_Method__c == 'Hybrid - DDB And Straight Line'){// TODO Hybrid - DDB And Straight Line
						
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepSL = EquipCost;
							NetDepDB = EquipCost;	 
							Depreciation_Rate_local = 200/listEquipDR.Number_of_Years__c;	// New rate for cur Year
							DepSL = NetDepSL * 	(100/listEquipDR.Number_of_Years__c)/(100.00);
							NetDepYearly = NetDepDB;
						}	
						
						LastDepreciatedAmt = NetDepDB;
						DepDB = NetDepDB * 	Depreciation_Rate_local/(100.00);
						NetDepDB = NetDepDB - DepDB;
						
						DepYearly = DepDB ;
						if(DepYearly<DepSL) DepYearly = DepSL;
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						NetDepYearly = NetDepYearly - DepYearly;
						
						
						DepreciationAmountAnnually =DepYearly;					
	
					}
					else if(listEquipDR.Depreciation_Method__c == 'Hybrid - 150% DB And Straight Line'){// TODO Hybrid - DDB And Straight Line
						if(iter==0){
							aircraftCost = EquipCost ;
							NetDepSL = EquipCost;
							NetDepDB = EquipCost;	 
							Depreciation_Rate_local = 150/listEquipDR.Number_of_Years__c;	// New rate for cur Year
							DepSL = NetDepSL * 	(100/listEquipDR.Number_of_Years__c)/(100.00);
							NetDepYearly = NetDepDB;
						}	
						
						LastDepreciatedAmt = NetDepDB;
						DepDB = NetDepDB * 	Depreciation_Rate_local/(100.00);
						NetDepDB = NetDepDB - DepDB;
						
						DepYearly = DepDB ;
						if(DepYearly<DepSL) DepYearly = DepSL;
						
						if((NetDepYearly -DepYearly )<=ResidualValue ) DepYearly = NetDepYearly - ResidualValue;
						if(listEquipDR.Depreciate_Till_Residual_Value__c && iter==(listEquipDR.Number_of_Years__c-1)) DepYearly = NetDepYearly - ResidualValue;
						NetDepYearly = NetDepYearly - DepYearly;
						
						
						DepreciationAmountAnnually =DepYearly;	
					}				
					else{
						if(iter==0) {
							aircraftCost = EquipCost - ResidualValue;
							SetResidualvalue = ResidualValue;
						}
						Depreciation_Rate_local = listEquipDR.Depreciation_Rate__r[iter].Depreciation_Rate__c;
						DepreciationAmountAnnually = aircraftCost * 	Depreciation_Rate_local/(100.00);
			
					}
					
						//lastCounter = lastCounter +1;
						comulativeDepreciation =comulativeDepreciation+ Depreciation_Rate_local/100;
						// Depricate till Residual value at the end
	
						DepreciationAmountAnnually = DepreciationAmountAnnually<=0?0:DepreciationAmountAnnually; 
						
						DepreciationAmountMonthly= DepreciationAmountAnnually/12.00;
						system.debug('comulativeDepreciation'+comulativeDepreciation);
						system.debug('DepreciationAmountMonthly'+DepreciationAmountMonthly);
						for(Integer i=1;i<=12;i++){
							tempDepreciationAmountMonthly = DepreciationAmountMonthly ;
							if(i==1 && tempDate.day()!=1){ 
								partialMonthFlag=true; 
								tempDepreciationAmountMonthly = carryoverLastYearPartialAmount + DepreciationAmountMonthly * (Date.daysInMonth(tempDate.year(), tempDate.month())- tempDate.day() +1)/(365.0/12);
								carryoverLastYearPartialAmount = DepreciationAmountMonthly -  DepreciationAmountMonthly * (Date.daysInMonth(tempDate.year(), tempDate.month())- tempDate.day() +1)/(365.0/12);
							}                        // prorate
							
							comulativeDepreciationAmount = comulativeDepreciationAmount + tempDepreciationAmountMonthly;
							system.debug('comulativeDepreciationAmount'+comulativeDepreciationAmount);
							
							system.debug('insertAircraftDS.size()='+insertAircraftDS.size());
							system.debug('startParentPointer+lastCounter='+(startParentPointer+lastCounter));
							if(insertAircraftDS.size()>=(startParentPointer+lastCounter)){
								system.debug('value i='+i);
								if(insertEquiptDS.size()>=lastCounter){
									insertEquiptDS[lastCounter-1].Tax_Depreciation_Percentage__c =Depreciation_Rate_local;
									insertEquiptDS[lastCounter-1].Tax_Depreciation__c = tempDepreciationAmountMonthly;
									insertEquiptDS[lastCounter-1].Tax_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount;
									insertEquiptDS[lastCounter-1].Tax_Depr_to_Date__c = comulativeDepreciationAmount;
									lastCounter ++;
								}
								else{								
									insertEquiptDS.add(new Equipment_Depreciation_Schedule__c(name = LeaseWareUtils.getDatetoString(tempDate,'YYYY-MM')
										,Aircraft_Depreciation_Schedule__c = 	insertAircraftDS[startParentPointer+lastCounter-1].Id
										,Tax_Depreciation_Percentage__c = Depreciation_Rate_local
										,Tax_Depreciation__c =   tempDepreciationAmountMonthly
										,Period_Sequence__c = lastCounter ++
										,Tax_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount
										,Depreciation_Date__c =tempDate
										,Tax_Depr_to_Date__c = comulativeDepreciationAmount
										,Capitalized_Cost__c =CCRec.Id 
										));
								}
							}
							tempDate = 	DepStartDate.addMonths(lastCounter-1);	
						}
						// e.g if 2nd year counter 13
						//if( partialMonthFlag)  carryoverLastYearPartialAmount =  DepreciationAmountMonthly * (tempDate.day()-1)/(365.0/12);                          // prorate
						if(partialMonthFlag && lastCounter>=listEquipDR.Number_of_Years__c*12){
							tempDepreciationAmountMonthly = carryoverLastYearPartialAmount;
							comulativeDepreciationAmount = comulativeDepreciationAmount + tempDepreciationAmountMonthly;
							if(insertAircraftDS.size()>=(startParentPointer+lastCounter)){
								if(insertEquiptDS.size()>=lastCounter){
									insertEquiptDS[lastCounter-1].Tax_Depreciation_Percentage__c = Depreciation_Rate_local;
									insertEquiptDS[lastCounter-1].Tax_Depreciation__c = tempDepreciationAmountMonthly;
									insertEquiptDS[lastCounter-1].Tax_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount;
									insertEquiptDS[lastCounter-1].Tax_Depr_to_Date__c = comulativeDepreciationAmount;
									//lastCounter ++;
								}
								else{									
									insertEquiptDS.add(new Equipment_Depreciation_Schedule__c(name = LeaseWareUtils.getDatetoString(tempDate,'YYYY-MM')
										,Aircraft_Depreciation_Schedule__c = 	insertAircraftDS[startParentPointer+lastCounter-1].Id
										,Tax_Depreciation_Percentage__c = Depreciation_Rate_local
										,Tax_Depreciation__c =   tempDepreciationAmountMonthly
										,Period_Sequence__c = lastCounter ++
										,Tax_Salvage_Value__c = aircraftCost- comulativeDepreciationAmount
										,Depreciation_Date__c =tempDate
										,Tax_Depr_to_Date__c = comulativeDepreciationAmount
										,Capitalized_Cost__c =CCRec.Id 
										));
	
								}
							}
					
						}
						
					}
			}// end if If Tax DS
		// insert Equip DS : set trigger to insert from trigger internally.
		LeaseWareUtils.setFromTrigger('InsertedFromAircraftTriggerHandler');		
		if(insertEquiptDS.size()>0){
			for(Equipment_Depreciation_Schedule__c curADS:insertEquiptDS){
				curADS.Salvage_Value__c = curADS.Salvage_Value__c!=null?Math.round(curADS.Salvage_Value__c):null;
				curADS.Depreciation__c = curADS.Depreciation__c!=null?Math.round(curADS.Depreciation__c):null;
				curADS.Depreciation_to_Date__c = curADS.Depreciation_to_Date__c!=null?Math.round(curADS.Depreciation_to_Date__c):null;
				
				curADS.Statutory_Salvage_Value__c = curADS.Statutory_Salvage_Value__c!=null?Math.round(curADS.Statutory_Salvage_Value__c):null;
				curADS.Statutory_Depreciation__c = curADS.Statutory_Depreciation__c!=null?Math.round(curADS.Statutory_Depreciation__c):null;
				curADS.Statutory_Depr_to_Date__c = curADS.Statutory_Depr_to_Date__c!=null?Math.round(curADS.Statutory_Depr_to_Date__c):null;
				
				curADS.Tax_Salvage_Value__c = curADS.Tax_Salvage_Value__c!=null?Math.round(curADS.Tax_Salvage_Value__c):null;
				curADS.Tax_Depreciation__c = curADS.Tax_Depreciation__c!=null?Math.round(curADS.Tax_Depreciation__c):null;
				curADS.Tax_Depr_to_Date__c = curADS.Tax_Depr_to_Date__c!=null?Math.round(curADS.Tax_Depr_to_Date__c):null;				
				
			}
			insert insertEquiptDS;
		}
		LeaseWareUtils.unsetTriggers('InsertedFromAircraftTriggerHandler');		
		
	}
	
	
	private void generateEquipDS(Capitalized_Cost__c paramRec){
		
		LeaseWareUtils.setFromTrigger('DeletedFromAircraftTriggerHandler');
		Delete [select Id from Equipment_Depreciation_Schedule__c where Capitalized_Cost__c = :paramRec.Id];
		LeaseWareUtils.unsetTriggers('DeletedFromAircraftTriggerHandler');	
		
		//insertADS();
		Aircraft__c ACRec = [select Id,Name,Date_Acquired__c,Equipment_Cost__c,Residual_Value__c
						,Lease__c,Y_Hidden_Lease_Start_Date__c,Y_Hidden_Lease_End_Date__c
						,Depreciation_Schedule__c,Statutory_Depreciation_Schedule__c,Tax_Depreciation_Schedule__c
						,Book_Value__c,Statutory_Value__c,Tax_Value__c
						,(select Id,Depreciation_Schedule__c,Statutory_Depreciation_Schedule__c,Tax_Depreciation_Schedule__c
							,Book_Value__c,Statutory_Value__c,Tax_Value__c,Start_Date__c from Capitalized_Costs__r)
					from Aircraft__c where id =:paramRec.Aircraft__c];		

		map<string,Object> returnMap = AircraftTriggerHandler.getNumberOfParentRecord(ACRec);
		Integer numberofParentRec = (Integer)returnMap.get('NumberOfPeriodSequence');
		Date startDate = (Date)returnMap.get('Date');		
		
		system.debug('numberofParentRec' +numberofParentRec);	
		system.debug('startDate' +startDate);

		Aircraft_Depreciation_Schedule__c[] existRec = [select Id,Depreciation_Date__c from Aircraft_Depreciation_Schedule__c where Aircraft__c = :paramRec.Aircraft__c order by Period_Sequence__c];
		list<Aircraft_Depreciation_Schedule__c> updateADS = new list<Aircraft_Depreciation_Schedule__c>(); 
		list<Aircraft_Depreciation_Schedule__c> insertAircraftDS = new list<Aircraft_Depreciation_Schedule__c>(); 
		
		for(Integer i=1;i<=numberofParentRec;i++){
			if(existRec.size() >0 && existRec[0].Depreciation_Date__c == startDate.addMonths(i-1)){
				if(i==1) {
					// if the first record is already present
					i= existRec.size();
					
				}
				else{
					for(Aircraft_Depreciation_Schedule__c curRec:existRec){
						curRec.Period_Sequence__c = i;
						curRec.Depreciation_Date__c =startDate.addMonths(i-1);
						updateADS.add(curRec);
						i++;
					}
				}
			}
			else{
				// insert new ADS
				insertAircraftDS.add(new Aircraft_Depreciation_Schedule__c(name = LeaseWareUtils.getDatetoString(startDate.addMonths(i-1),'YYYY-MM')
							,Aircraft__c = 	paramRec.Aircraft__c
							,Period_Sequence__c = i
							,Depreciation_Date__c =startDate.addMonths(i-1)
							));					
				
			
			}

		}	
		
		LeaseWareUtils.setFromTrigger('AircraftDepreciationScheduleHandlerBefore');	
		if(updateADS.size()>0) update updateADS;	
		if(insertAircraftDS.size()>0)insert insertAircraftDS;
		LeaseWareUtils.unsetTriggers('AircraftDepreciationScheduleHandlerBefore');				
		//insertEDS();
		existRec = [select Id,Depreciation_Date__c,Name from Aircraft_Depreciation_Schedule__c where Aircraft__c = :paramRec.Aircraft__c order by Period_Sequence__c];

		createEquipDS( existRec,paramRec) ;
		
		
	}
	
    
    
}