@isTest
private class TestForecastSOQLServices {
    @testSetup
    static void createSetup() {
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Assembly_RC_Info__c returnInfo = new Assembly_RC_Info__c();
        returnInfo.Return_Condition__c = rec.Id;
        returnInfo.Assembly_Type__c = 'Engine 1';
        returnInfo.Event_Type__c = 'Performance Restoration';
        returnInfo.RC_FC__c=100;
        insert returnInfo;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January'
           );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
         
        lease.Aircraft__c = aircraft.ID;
        update lease;
       
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Scenario_Component_Input__c scenarioComponentInp = new Scenario_Component_Input__c();
        scenarioComponentInp.Name = 'Test Scenario Component Input';
        scenarioComponentInp.FH__c = 20;
        scenarioComponentInp.FC__c =21;
        scenarioComponentInp.Forecast_Type__c = 'Current Lease';
        scenarioComponentInp.Type__c ='Engine';
        scenarioComponentInp.User_Defined_Utilization_FH__c =True;
        scenarioComponentInp.Scenario_Input__c = scenarioInp.Id;
        scenarioComponentInp.Operating_Environment_Impact__c = True;
        insert scenarioComponentInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        insert constitutentAssembly;
    }
    
    
    @isTest
    static void testForecastSOQLMethods() {
        Aircraft__c asset = [Select Id from Aircraft__c];
        Scenario_Input__c scenarioInp = [Select Id from Scenario_Input__c];
        List<Scenario_Component_Input__c> scenarioCmpList = [Select Id from Scenario_Component_Input__c];
        Lease__c lease = [Select Id from Lease__c];
        Constituent_Assembly__c constitutentAssembly = [Select Id from Constituent_Assembly__c];
        Component_Return_Condition__c rec = [Select Id from Component_Return_Condition__c];
        List<Scenario_Component__c> scenarioCmp = new List<Scenario_Component__c>();
        Set<Id> assemblyIds = new Set<Id>();
        
                
        assemblyIds.add(constitutentAssembly.Id);
        
        ForecastSOQLServices.getAsset(asset.Id);
        ForecastSOQLServices.getScenarioInput(scenarioInp.Id);
        ForecastSOQLServices.getScenarioCmpInput(scenarioInp.Id);
        ForecastSOQLServices.getAssemblyEventInput(scenarioCmpList);
        ForecastSOQLServices.getConstituentAssembly(asset.Id);
        ForecastSOQLServices.getLease(lease.Id);
        ForecastSOQLServices.getAssemblyEventInfo(assemblyIds);
        ForecastSOQLServices.getAssemblyMRrates(lease.Id);
        ForecastSOQLServices.getReturnCondition(rec.Id);
        ForecastSOQLServices.getScenarioOutput(scenarioInp.Id);
        ForecastSOQLServices.getMonthlyScenario(scenarioCmp);        
        ForecastSOQLServices.getMonthlyScenarioRecords(scenarioInp.Id);
        ForecastSOQLServices.getMonthlyScenarioForBarChart(scenarioInp.Id);
        try {
            ForecastSOQLServices.getMonthlyScenarioAsPerDate(scenarioInp.Id, String.valueOf(System.Today()));
        }
        catch(Exception e) {
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
        
        try {
        	ForecastSOQLServices.getMonthlyScenarioForBarChartTable(scenarioInp.Id, String.valueOf(System.Today()));
        }
        catch(Exception e) {
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
}