@isTest
public class AircraftUtilizationControllerTest {
    @TestSetup
    public static void testSetup(){
         
        
        Operator__c operator = new Operator__c(Name='TestThisAirline', Status__c='Approved');
        insert operator;
        
        // Set up the Aircraft record.
        Aircraft__c a = new Aircraft__c(MSN_Number__c='12345', Aircraft_Type__c='737', Date_of_Manufacture__c = System.today(),
                                        Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, 
                                        Threshold_for_C_Check__c=1000, Status__c='Available',Model_Code__c='A320');
        
        insert a;
        
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10', 
                                      Lease_Start_Date__c=Date.parse('01/01/2010'),Lease_End_Date__c=Date.parse('12/31/2012'), 
                                      Lease_Start_Date_New__c=Date.parse('01/01/2010'), Lease_End_Date_New__c=Date.parse('12/31/2012'), 
                                      Rent_Date__c=Date.parse('01/01/2010'),Rent_End_Date__c=Date.parse('12/31/2012'));
        lease.aircraft__c=a.id;
        insert lease;
        
        Custom_Lookup__c customLookup = new Custom_Lookup__c(Name='Default',Lookup_Type__c='Engine',active__c=true);
        insert customLookup;
        
        Constituent_Assembly__c consttAssembly = 
            new Constituent_Assembly__c(Name='Name', 
                                        Aircraft_Type__c='737-300', 
                                        Aircraft_Engine__c='737-300 @ CFM56', 
                                        Engine_Model__c ='CFM56', 
                                        Attached_Aircraft__c= a.Id, 
                                        Description__c ='Desc', 
                                        TSN__c=0, CSN__c=0, 
                                        TSLV__c=0, CSLV__c= 0, 
                                        Type__c ='Engine 1', 
                                        Serial_Number__c='123', 
                                        Engine_Thrust__c='-3B1');
        insert consttAssembly;
        
        Lessor__c lessor = new Lessor__c(Name='test Lessor');
        insert lessor;
        
        
        
    }
    
    @isTest
    public static void startTest(){
        AircraftUtilizationController.getAirlines();
        List<String> airlinesIds = new List<String>();
        
        airlinesIds.add('kdjgkfdjg');
        AircraftUtilizationController.getAircraftUtilization('2018-08-30', airlinesIds);
        
        List<AircraftUtilizationController.AirlineWrapper> awList = AircraftUtilizationController.getAircraftUtilization('2018-08-30', null);
        for(AircraftUtilizationController.AirlineWrapper airlineWrapper : awList){
            for(AircraftUtilizationController.AircraftModelCodeWrapper amcw : airlineWrapper.modelCodeWrapperList){
                for(AircraftUtilizationController.AircraftAssemblyWrapper aaw : amcw.aircraftAssemblyList){
                        for(AircraftUtilizationController.AssemblyWrapper aw : aaw.assemblyList){
                            if(aw.assembly != null  ){
                                aw.assemblyUtilization.Running_Hours_During_Month__c = 30;
                                aw.assemblyUtilization.Cycles_During_Month__c = 30;
                            }
                        }
                }
            }
        }
        try{
            AircraftUtilizationController.saveAssemblyUtilization(JSON.serialize(awList), '2018-08-30');
        }catch(Exception e){
			    system.debug('Error handled');        
        }
        
        
        awList = AircraftUtilizationController.getAircraftUtilization('2018-08-30', null);
        AircraftUtilizationController.submitUtilizationForApproval(JSON.serialize(awList));
    }
}