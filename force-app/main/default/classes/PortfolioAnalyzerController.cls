public with sharing class PortfolioAnalyzerController {
    @AuraEnabled
    public static String GetAssetByLocation(){
        
        List<AggregateResult> assetList = [
        select Country_Of_Registration__c country ,count(Id) assetByCountryCount
        FROM Aircraft__c 
        WHERE Marked_For_Sale__c=false AND Country_Of_Registration__c != null
        group by Country_Of_Registration__c
        order by count(Id) desc ];
        Map<String,Integer> mapAssetLocationCur = new Map<String,Integer>();
    
        for(AggregateResult assetCur : assetList)
        {
            mapAssetLocationCur.put((String)assetCur.get('country'),(Integer)assetCur.get('assetByCountryCount'));
            
        }
        list<AssetDealWrapper> AssetDealData = new list<AssetDealWrapper>();
 
        for(String key : mapAssetLocationCur.keySet())
        {
            AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=key;
            adw.y=mapAssetLocationCur.get(key);
            AssetDealData.add(adw);
        }
        return System.json.serialize(AssetDealData);

} 

    @AuraEnabled
    public static String GetDealsByCountry(){
    List<Marketing_Activity__c> dealsFilterByCountry = [select Asset_Type__c,Country__c FROM Marketing_Activity__c
     WHERE Inactive_MA__c=false AND (Deal_Type__c='Lease' OR Deal_Type__c='SLB') 
     AND Country__c != null ];
    Map<String,Integer> mapProjectedData = new Map<String,Integer>();
    for(Marketing_Activity__c dealsByRegion : dealsFilterByCountry){
            if(mapProjectedData.containsKey(dealsByRegion.Country__c))
            {
                mapProjectedData.put(dealsByRegion.Country__c, mapProjectedData.get(dealsByRegion.Country__c)+1) ;
            }else{
                mapProjectedData.put(dealsByRegion.Country__c, 1) ;
            }
    }
    list<AssetDealWrapper> assetDealData = new list<AssetDealWrapper>();
    for(String key : mapProjectedData.keySet())
    {
        AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=key;
            adw.y=mapProjectedData.get(key);
            assetDealData.add(adw);
    }
    return System.json.serialize(assetDealData);
}

//code for deals filter starts from here- maybe move it to a new/diffrent apex class 

    @AuraEnabled
    public static String GetDealsByCountryTrial2(List<String> DealTypes){
        system.debug('dealt in controller is:'+DealTypes);
    List<Marketing_Activity__c> dealsFilterByCountry = [select Asset_Type__c,Country__c FROM Marketing_Activity__c
     WHERE Inactive_MA__c=false AND Deal_Type__c IN: DealTypes
     AND Country__c != null ];
     String Assets = GetAssetByLocation();
     system.debug('dealsfilterbyregion------!!! after refactoring working ok?'+dealsFilterByCountry);
     List<AssetDealWrapper> assetsCurrent = (List<AssetDealWrapper>) JSON.deserialize(Assets, List<AssetDealWrapper>.class);
     String ActiveDeal=GetDealsCountry(assetsCurrent,dealsFilterByCountry);
     return ActiveDeal;  
}

@AuraEnabled
public static string GetSelectedDealByCountry(List<String> DealsSelected){
    //Code to handle projected chart data for specific deal
    String Assets = GetAssetByLocation();
    List<Marketing_Activity__c> Deals = [select Asset_Type__c,Country__c FROM Marketing_Activity__c
     WHERE Inactive_MA__c=false AND Id IN: DealsSelected
     AND Country__c != null];
     List<AssetDealWrapper> assetsCurrent = (List<AssetDealWrapper>) JSON.deserialize(Assets, List<AssetDealWrapper>.class);
     String ActiveDeal = GetDealsCountry(assetsCurrent,Deals);
     return ActiveDeal;
    
}

@AuraEnabled
public static string GetDealsCountry(List<AssetDealWrapper> Asset,List<Marketing_Activity__c> selectDeal){
    Map<String,Integer> mapProjectedData = new Map<String,Integer>();
    Map<String,List<Integer>> projectedMap = new Map<String,List<Integer>>();
    List<AssetDealWrapper> currentAndProjectedAssetByLoc = new List<AssetDealWrapper>();
    for(Marketing_Activity__c dealsByRegion : selectDeal){
            if(mapProjectedData.containsKey(dealsByRegion.Country__c))
            {
                mapProjectedData.put(dealsByRegion.Country__c, mapProjectedData.get(dealsByRegion.Country__c)+1) ;
            }else{
                mapProjectedData.put(dealsByRegion.Country__c, 1) ;
            }
    }
    list<AssetDealWrapper> assetDealData = new list<AssetDealWrapper>();
    for(String key : mapProjectedData.keySet())
    {
        AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=key;
            adw.y=mapProjectedData.get(key);
            assetDealData.add(adw);
    }
    //return System.json.serialize(assetDealData);

    //combining the deals and current based on the deal type filter passed from the UI
    for(integer i=0;i<assetDealData.size();i++){
        if(!projectedMap.containsKey(assetDealData[i].name))
            projectedMap.put(assetDealData[i].name,new List<Integer>{assetDealData[i].y});
        else
            projectedMap.get(assetDealData[i].name).add(assetDealData[i].y);
    }
    for(integer i=0;i<Asset.size();i++){
        if(!projectedMap.containsKey(Asset[i].name))
            projectedMap.put(Asset[i].name,new List<Integer>{Asset[i].y});
        else
            projectedMap.get(Asset[i].name).add(Asset[i].y);
    }
    Set<String> mapkeys = projectedMap.keySet();
    for(String country : mapkeys){
        List<integer> count =projectedMap.get(country);
        integer total=0;
        if(count.size()>1){
            for(integer i : count){
                total += i;
            }
            AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=country;
            adw.y=total;
            currentAndProjectedAssetByLoc.add(adw);
        }
        else if(count.size()==1){
            AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=country;
            adw.y=count[0];
            currentAndProjectedAssetByLoc.add(adw);
        }
        system.debug('corresponding key is : '+projectedMap.get(country));
    }
    return System.json.serialize(CurrentAndProjectedAssetByLoc);
}

@AuraEnabled
public static string GetDealsOperator(List<AssetDealWrapper> CurrentOP, List<Marketing_Activity__c> SelectedDealList){
    Map<String,Integer> DealsData = new Map<String,Integer>();
    Map<String,List<Integer>> MapKeyset = new Map<String,List<Integer>>();
    List<AssetDealWrapper> currentAndProjectedOperator = new List<AssetDealWrapper>();
    for(Marketing_Activity__c dealsByOp : SelectedDealList){
            if(DealsData.containsKey(dealsByOp.Prospect_Name__c))
            {
                DealsData.put(dealsByOp.Prospect_Name__c, DealsData.get(dealsByOp.Prospect_Name__c)+1) ;
            }else{
                DealsData.put(dealsByOp.Prospect_Name__c, 1) ;
            }
    }
    list<AssetDealWrapper> assetDealData = new list<AssetDealWrapper>();
    for(String key : DealsData.keySet())
    {
        AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=key;
            adw.y=DealsData.get(key);
            assetDealData.add(adw);
    }

    //combine deal(selected or specific deal) and current aircrafts by operator
   for(integer i=0;i<CurrentOP.size();i++){
        if(!MapKeyset.containsKey(CurrentOP[i].name))
            MapKeyset.put(CurrentOP[i].name,new List<Integer>{CurrentOP[i].y});
        else
            MapKeyset.get(CurrentOP[i].name).add(CurrentOP[i].y);
    }
    for(integer i=0;i<assetDealData.size();i++){
        if(!MapKeyset.containsKey(assetDealData[i].name))
            MapKeyset.put(assetDealData[i].name,new List<Integer>{assetDealData[i].y});
        else
            MapKeyset.get(assetDealData[i].name).add(assetDealData[i].y);
    } 
    Set<String> mapkeys = MapKeyset.keySet();
    for(String op : mapkeys){
        List<integer> count =MapKeyset.get(op);
        integer total=0;
        if(count.size()>1){
            for(integer i : count){
                total += i;
            }
        AssetDealWrapper adw = new AssetDealWrapper();
        adw.name=op;
        adw.y=total;
        currentAndProjectedOperator.add(adw);
        }
        else if(count.size()==1){
        AssetDealWrapper adw = new AssetDealWrapper();
        adw.name=op;
        adw.y=count[0];
        currentAndProjectedOperator.add(adw);
        }
    }
    system.debug('the last list is ---------'+currentAndProjectedOperator);
    return System.json.serialize(currentAndProjectedOperator);
    
}

@AuraEnabled
    public static List<Marketing_Activity__c> GetActiveDeals(){
        List<Marketing_Activity__c> ActiveDeals = [Select Name, Id FROM Marketing_Activity__c WHERE Inactive_MA__c=false];
        system.debug('Active deals listed------'+ActiveDeals);
        system.debug('the size is----'+ActiveDeals.size());
        return ActiveDeals;
    }
@AuraEnabled
public static string GetSelectedDealByOperator(List<String> DealsSelected){
    Map<String,Integer> DealsData = new Map<String,Integer>();
    Map<String,List<Integer>> MapKeyset = new Map<String,List<Integer>>();
    List<AssetDealWrapper> currentAndProjectedOperator = new List<AssetDealWrapper>();
    String currentOperator= GetCurrentOperators();
    List<AssetDealWrapper> CurrentOP = (List<AssetDealWrapper>) JSON.deserialize(currentOperator, List<AssetDealWrapper>.class);
    List<Marketing_Activity__c> SelectedDealList = [select Prospect__c,Prospect_Name__c,Name,Id,Asset_Type__c FROM Marketing_Activity__c 
        WHERE Inactive_MA__c=false AND Asset_Type__c='Aircraft' AND Id IN :DealsSelected];
    system.debug('specific deals selected------'+SelectedDealList);
   String ActiveDeal= GetDealsOperator(CurrentOP,SelectedDealList);
   return ActiveDeal;

}

@AuraEnabled
public static string OperatorByDealType(List<String> DealTypes){
     String currentOps = GetCurrentOperators();
     Map<String,Integer> DealsData = new Map<String,Integer>();
    Map<String,List<Integer>> MapKeyset = new Map<String,List<Integer>>();
    List<AssetDealWrapper> currentAndProjectedOperator = new List<AssetDealWrapper>();
     List<AssetDealWrapper> CurrentOP = (List<AssetDealWrapper>) JSON.deserialize(currentOps, List<AssetDealWrapper>.class);
    List<Marketing_Activity__c> SelectedDealList=[select Prospect__c,Prospect_Name__c,Name,Id,Asset_Type__c FROM Marketing_Activity__c 
        WHERE Inactive_MA__c=false AND Asset_Type__c='Aircraft' AND Deal_Type__c IN: DealTypes];
        String ActiveDeal= GetDealsOperator(CurrentOP,SelectedDealList);
   return ActiveDeal;
}

    @AuraEnabled
    public static string GetProjectedAssetByLocation(){
    String deals= GetDealsByCountry();
    String Assets = GetAssetByLocation();
    Map<String,List<Integer>> projectedMap = new Map<String,List<Integer>>();
    List<AssetDealWrapper> currentAndProjectedAssetByLoc = new List<AssetDealWrapper>();
    List<AssetDealWrapper> dealsProjected = (List<AssetDealWrapper>) JSON.deserialize(deals, List<AssetDealWrapper>.class);
    List<AssetDealWrapper> assetsCurrent = (List<AssetDealWrapper>) JSON.deserialize(Assets, List<AssetDealWrapper>.class);
    for(integer i=0;i<dealsProjected.size();i++){
        if(!projectedMap.containsKey(dealsProjected[i].name))
            projectedMap.put(dealsProjected[i].name,new List<Integer>{dealsProjected[i].y});
        else
            projectedMap.get(dealsProjected[i].name).add(dealsProjected[i].y);
    }
    for(integer i=0;i<assetsCurrent.size();i++){
        if(!projectedMap.containsKey(assetsCurrent[i].name))
            projectedMap.put(assetsCurrent[i].name,new List<Integer>{assetsCurrent[i].y});
        else
            projectedMap.get(assetsCurrent[i].name).add(assetsCurrent[i].y);
    }
    Set<String> mapkeys = projectedMap.keySet();
    for(String country : mapkeys){
        List<integer> count =projectedMap.get(country);
        integer total=0;
        if(count.size()>1){
            for(integer i : count){
                total += i;
            }
            AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=country;
            adw.y=total;
            currentAndProjectedAssetByLoc.add(adw);
        }
        else if(count.size()==1){
            AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=country;
            adw.y=count[0];
            currentAndProjectedAssetByLoc.add(adw);
        }
        system.debug('corresponding key is : '+projectedMap.get(country));
    }
    return System.json.serialize(CurrentAndProjectedAssetByLoc);       
}
    
    @AuraEnabled
    public static string GetCurrentOperators(){
    List <Aircraft__c> aircraftList = [select 	Aircraft_ID__c, Assigned_To__c 
    FROM Aircraft__c WHERE Status__c ='Assigned'];
    System.debug('aircraft list ------------'+aircraftList);
    Map<String,Integer> mapcurrentData = new Map<String,Integer>();
        for(Aircraft__c assetOp : aircraftList){
            String assignedTo = (assetOp.Assigned_To__c.split('">')[1]).split('<')[0];
            if(String.isNotBlank(assignedTo)){
            if(mapcurrentData.containsKey(assetOp.Assigned_To__c))
            {
                mapcurrentData.put(assetOp.Assigned_To__c, mapcurrentData.get(assetOp.Assigned_To__c)+1) ;
            }else{
                mapcurrentData.put(assetOp.Assigned_To__c, 1) ;
            }
            }
        }

    list<AssetDealWrapper> AssetDealData = new list<AssetDealWrapper>();
    system.debug('mapcurrentdata----'+mapcurrentData);
    for(String key : mapcurrentData.keySet())
    {
        AssetDealWrapper adw = new AssetDealWrapper();
        adw.name=(key.split('">')[1]).split('<')[0];
        adw.y=mapcurrentData.get(key);
        AssetDealData.add(adw);
    }
    return System.json.serialize(AssetDealData);        
}
    
    @AuraEnabled
    public static string GetProjectedOperators(){
        String currentOps = GetCurrentOperators();
        String Projectedops = ProjectedOperators();
    System.debug('Operators in deals : '+Projectedops);
    system.debug('current operators'+currentOps);
    List<AssetDealWrapper> currentProjectedAssetOp = new List<AssetDealWrapper>();
    Map<String,List<Integer>> MapKeyset = new Map<String,List<Integer>>();
    List<AssetDealWrapper> dealCurrentOperatorList = (List<AssetDealWrapper>) JSON.deserialize(currentOps, List<AssetDealWrapper>.class);
    List<AssetDealWrapper> dealProjectedOperatorList = (List<AssetDealWrapper>) JSON.deserialize(Projectedops, List<AssetDealWrapper>.class);
    for(integer i=0;i<dealCurrentOperatorList.size();i++){
        if(!MapKeyset.containsKey(dealCurrentOperatorList[i].name))
            MapKeyset.put(dealCurrentOperatorList[i].name,new List<Integer>{dealCurrentOperatorList[i].y});
        else
            MapKeyset.get(dealCurrentOperatorList[i].name).add(dealCurrentOperatorList[i].y);
    }
    for(integer i=0;i<dealProjectedOperatorList.size();i++){
        if(!MapKeyset.containsKey(dealProjectedOperatorList[i].name))
            MapKeyset.put(dealProjectedOperatorList[i].name,new List<Integer>{dealProjectedOperatorList[i].y});
        else
            MapKeyset.get(dealProjectedOperatorList[i].name).add(dealProjectedOperatorList[i].y);
    }
    Set<String> mapkeys = MapKeyset.keySet();
    for(String op : mapkeys){
        List<integer> count =MapKeyset.get(op);
        integer total=0;
        if(count.size()>1){
            for(integer i : count){
                total += i;
            }
        AssetDealWrapper adw = new AssetDealWrapper();
        adw.name=op;
        adw.y=total;
        currentProjectedAssetOp.add(adw);
        }
        else if(count.size()==1){
        AssetDealWrapper adw = new AssetDealWrapper();
        adw.name=op;
        adw.y=count[0];
        currentProjectedAssetOp.add(adw);
        }
    }
    system.debug('the last list is ---------'+currentProjectedAssetOp);
    return System.json.serialize(currentProjectedAssetOp);
}
    @AuraEnabled
    public static string ProjectedOperators(){
        List<Marketing_Activity__c> dealsByOperator = [select Prospect__c,Prospect_Name__c,Name,Id,Asset_Type__c FROM Marketing_Activity__c 
        WHERE Inactive_MA__c=false AND Asset_Type__c='Aircraft' AND (Deal_Type__c='Lease' OR Deal_Type__c='SLB')];
        Map<String,Integer> mapPorjectedData = new Map<String,Integer>();
            for(Marketing_Activity__c md : dealsByOperator){
                if(mapPorjectedData.containsKey(md.Prospect_Name__c))
                {
                    mapPorjectedData.put(md.Prospect_Name__c, mapPorjectedData.get(md.Prospect_Name__c)+1) ;
                }else{
                mapPorjectedData.put(md.Prospect_Name__c, 1) ;
                }
            }

        list<AssetDealWrapper> AssetDealData = new list<AssetDealWrapper>();
        for(String key : mapPorjectedData.keySet())
        {
            AssetDealWrapper adw = new AssetDealWrapper();
            adw.name=key;
            adw.y=mapPorjectedData.get(key);
            AssetDealData.add(adw);
        }
        return System.json.serialize(AssetDealData);

    }
    
     @AuraEnabled
    Public static List<String> getPickListValues(String objectName, String fld) {
        String namespace = LeaseWareUtils.getNamespacePrefix();
        objectName = namespace + objectName;
        objectName = objectName.replace(namespace + namespace , namespace);
        System.debug('objectName '+objectName);
        
        if(!fld.equals('Name')) {
            fld = namespace + fld;
            fld = fld.replace(namespace + namespace , namespace);
        }
        
        system.debug('objObject --->' + objectName);
        system.debug('fld --->' + fld);
        List <String> allOpts = new list <String>();
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        return allOpts;
    }
    
    
/**
* Wrapper class to serialize as JSON as return Value
* */
    public Class AssetDealWrapper{
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public integer y {get; set;} 
    }
    
}