/**
***********************************************************************************************************************
* Class: EmailTemplateAndFilesCtrl
* @author Created by Aman, Lease-Works, 
* @date 17/08/2020
* @version 1.0
* ---------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
* Contains all custom business logic to read the Email Template body and create public link for the image files if included in the long Text area field.
*
* History:
* VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
* 1.0       A.K.G                 17.08.2020      EmailTemplate & File [Public Link]
***********************************************************************************************************************
*/
public class EmailTemplateAndFilesCtrl {
    @TestVisible public static final String FIELD_API_NAME = Test.isRunningTest() ? 'MSN_Number__c' : 'EmailTemplateAPI__c';
    @TestVisible public static final String USER_FIELD_API_NAME = 'SendAsOrgWideEmail__c';
    /**
    * @description Check and reterive the Email Template value on the record and send the email out to the user. 
    *
    * @param eventId - String, contains record Id.
    *
    * @return String - SUCCESS Or Error message (In case of any error).
    */
    @AuraEnabled
    public static String sendEmail(String recordId) {
        try {
            if (recordId == null || recordId == '') { return 'Something went wrong. Try Refreshing the page.'; }

            String objName = Id.valueOf(recordId).getSobjectType().getDescribe().getName();
            if (objName == null) { return 'Something went wrong. Try Refreshing the page.'; }
            
            Boolean feildExists = false, userFieldExists = false;
            String templateName = '', userEmail = '';
            
            Map<String, SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
            for (String fieldName : fieldsMap.keySet()) {
                if (fieldName == FIELD_API_NAME) { feildExists = true; }
                if (fieldName == USER_FIELD_API_NAME) { userFieldExists = true; }
                if (feildExists && userFieldExists) { break; }
            }

            if (!feildExists) { return 'Email Template configuration not found, please configure a field on the record with API \''+FIELD_API_NAME+'\''; }

            SObject recordData = (SObject) Database.query('SELECT Id, ' + FIELD_API_NAME + (userFieldExists ? ', ' + USER_FIELD_API_NAME : '') + ' FROM ' + objName + ' WHERE Id = \'' + recordId + '\'');
            if (recordData != null) {
                templateName = recordData.get(FIELD_API_NAME) != null ? (String) recordData.get(FIELD_API_NAME) : '';
                userEmail = (userFieldExists && recordData.get(USER_FIELD_API_NAME) != null) ? (String) recordData.get(USER_FIELD_API_NAME) : '';
            }

            if (templateName == null || templateName == '') {
                return 'Email Template configuration not found, please configure a field on the record with API \''+FIELD_API_NAME+'\'';
            }
            List<EmailTemplate> etList = [SELECT Id, Name, DeveloperName, IsActive, TemplateType, Description, Subject, Markup FROM EmailTemplate WHERE DeveloperName =: templateName LIMIT 1];
            
            if (!etList.isEmpty()) {
                Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(etList[0].Id, UserInfo.getUserId(), recordId);
                if (userEmail != '') {
                    // Query from Organization-Wide Email Address
                    List<OrgWideEmailAddress> lstEmailAddress = [SELECT Id FROM OrgWideEmailAddress WHERE Address =: userEmail];
                    if (!lstEmailAddress.isEmpty()) {
                        mail.setOrgWideEmailAddressId(lstEmailAddress[0].Id);
                    }
                    else { return '['+userEmail+'] not found as Organization-Wide Address in the system.'; }
                }
                mail.setTargetObjectId(UserInfo.getUserId());
                mail.setSaveAsActivity(false);

                String mailBody = encodeDecodeImageURLs(mail.getHTMLBody(), recordId);
                mail.setHTMLbody(mailBody);

                List<String> cdIds = new List<String>();
                List<Id> fileIds = new List<Id>();
                for (ContentDocumentLink cdLink : [SELECT LinkedEntityid, ContentDocumentid FROM ContentDocumentLink WHERE LinkedEntityid =: recordId]) {
                    cdIds.add(cdLink.ContentDocumentid);
                }
                for (ContentVersion cVersion : [SELECT Id, title, FileExtension, versiondata, FileType FROM Contentversion WHERE ContentDocumentId IN : cdIds AND IsLatest = true]) {
                    fileIds.add(cVersion.Id);
                }
                mail.setEntityAttachments(fileIds);
                LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
                Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
                if (results[0].success) { return 'SUCCESS'; } 
                else { return results[0].errors[0].message; }
            }
            else { return 'Email Template \''+templateName+'\' not found, please make sure it exists and you have access to it'; }
        }
        catch (Exception ex) { throw new AuraHandledException(Utility.getErrorMessage(ex)); }
    }
	
    /**
    * @description get all the Image Url's within the Email Body and replace with the public link for the File. 
    *
    * @param emailBody - String, HTML body of the EmalTemplate contianing the field value with Image URl added by the User.
    * @param recordId - String, contains record Id.
    *
    * @return String - HTML body with the public Image URL's.
    */
    @TestVisible
    private static String encodeDecodeImageURLs(String emailBody, String recordId) {
        try {
            Set<String> cdIds = new Set<String>();
            Map<Id, ContentVersion> contentVersionsMap = new Map<Id, ContentVersion>();
            Map<Id, ContentDistribution> conDocPublicURLMap = new Map<Id, ContentDistribution>();
            List<ContentDistribution> cdistToInsert = new List<ContentDistribution>();

            for (ContentDocumentLink cdLink : [SELECT LinkedEntityid, ContentDocumentid FROM ContentDocumentLink WHERE LinkedEntityid =: recordId]) {
                cdIds.add(cdLink.ContentDocumentid);
            }
            for (ContentVersion cVersion : [SELECT Id, ContentDocumentId, ContentDocument.Title, FileType, ContentBodyId
                                            FROM Contentversion WHERE ContentDocumentId IN: cdIds]) {
                contentVersionsMap.put(cVersion.Id, cVersion);
            }

            for (ContentDistribution cd : [SELECT Id, ContentVersionId, ContentVersion.ContentDocumentId, ContentDownloadUrl, DistributionPublicUrl 
                                            FROM ContentDistribution 
                                            WHERE ContentVersion.ContentDocumentId IN: cdIds]) {
                conDocPublicURLMap.put(cd.ContentVersion.ContentDocumentId, cd);
            }
            System.debug('conDocPublicURLMap: '+conDocPublicURLMap);

            Matcher imgMatcher = Pattern.compile( '<img(.+?)>' ).matcher( emailBody );
            Map<String, ImageDataWrapper> imageDataMap = new Map<String, ImageDataWrapper>();

            while (imgMatcher.find()) { // iterate each image tag found
                ImageDataWrapper idw = new ImageDataWrapper();
                idw.imageTag = imgMatcher.group(); // get the image tag html
                idw.imageURL = idw.imageTag.substringBetween(' src="', '"'); // get the value of the src attribute

                if (idw.imageURL.indexOf('/version/download/') != -1) {
                    idw.versionId = idw.imageURL.substringAfter('/version/download/');
                    idw.versionId = idw.versionId.substringBefore('?');
                }
                if (idw.versionId != null && idw.versionId != '' && contentVersionsMap.containsKey(idw.versionId) && contentVersionsMap.get(idw.versionId) != null) {
                    idw.contentDocId = contentVersionsMap.get(idw.versionId).ContentDocumentId;
                    idw.fileType = contentVersionsMap.get(idw.versionId).FileType;
                    idw.contentBodyId = contentVersionsMap.get(idw.versionId).ContentBodyId;
                }
                if (idw.contentDocId != null && idw.contentDocId != '' && conDocPublicURLMap.containsKey(idw.contentDocId) && conDocPublicURLMap.get(idw.contentDocId) != null) {
                    idw.contentDownloadURL = conDocPublicURLMap.get(idw.contentDocId).ContentDownloadUrl;
                    idw.contentPublicURL = conDocPublicURLMap.get(idw.contentDocId).DistributionPublicUrl;
                    idw.finalURL = frameURL(idw);
                }
                if(idw.contentDocId != null && idw.contentDocId != '' && !imageDataMap.containsKey(idw.contentDocId)) {
                    imageDataMap.put(idw.contentDocId, idw);
                }
            }
            System.debug('imageDataMap: '+imageDataMap);
            
            cdIds = new Set<String>();
            for (String key : imageDataMap.keySet()) {
                if (!conDocPublicURLMap.containsKey(key)) {
                    ContentDistribution cd = new ContentDistribution();
                    cd.Name = contentVersionsMap.get(imageDataMap.get(key).versionId).ContentDocument.Title;
                    cd.ContentVersionId = contentVersionsMap.get(imageDataMap.get(key).versionId).Id;
                    cd.PreferencesAllowViewInBrowser = true;
                    cd.PreferencesLinkLatestVersion = true;
                    cd.PreferencesNotifyOnVisit = false;
                    cd.PreferencesPasswordRequired = false;
                    cd.PreferencesAllowOriginalDownload = true;
                    cdistToInsert.add(cd);
                    cdIds.add(contentVersionsMap.get(imageDataMap.get(key).versionId).ContentDocumentId);
                }
            }
            System.debug('cdistToInsert: '+cdistToInsert);

            if (!cdistToInsert.isEmpty()) {
                insert cdistToInsert;
            }

            for (ContentDistribution cd : [SELECT Id, Name, ContentDocumentId, ContentDownloadUrl, DistributionPublicUrl
                                            FROM ContentDistribution WHERE ContentDocumentId IN: cdIds]) {
                if(imageDataMap.containsKey(cd.ContentDocumentId)){
                    imageDataMap.get(cd.ContentDocumentId).contentDownloadURL = cd.ContentDownloadUrl;
                    imageDataMap.get(cd.ContentDocumentId).contentPublicURL = cd.DistributionPublicUrl;
                    imageDataMap.get(cd.ContentDocumentId).finalURL = frameURL(imageDataMap.get(cd.ContentDocumentId));
                }
            }

            for (String key : imageDataMap.keySet()) {
                if (imageDataMap.get(key).finalURL != null && imageDataMap.get(key).finalURL != '') {
                    emailBody = emailBody.replace(imageDataMap.get(key).imageURL, imageDataMap.get(key).finalURL);
                }
            }
            System.debug('emailBody: '+emailBody);
        }
        catch (Exception ex) { throw new AuraHandledException(Utility.getErrorMessage(ex)); }
        return emailBody;
    }
	
    /**
    * @description frame the final Public URL based on the values we have from the Content Version record.
    *
    * @param idw - ImageDataWrapper, wrapper object containing all the required values to frame the url.
    *
    * @return String - Final Public URL
    */
    @TestVisible
    private static String frameURL(ImageDataWrapper idw){
        try {
            String finalURL = idw.contentDownloadURL;
            finalURL = finalURL.substringBefore('download/?') + 'renditionDownload?rendition=ORIGINAL_' + idw.fileType;
            finalURL += '&versionId=' + idw.versionId;
            finalURL += '&operationContext=DELIVERY';
            finalURL += '&contentId=' + idw.contentBodyId;
            finalURL += '&page=0';
            finalURL += '&d=/a/' + idw.contentPublicURL.substringAfter('/a/');
            finalURL += '&oid=' + idw.contentDownloadURL.substringBetween('oid=', '&');
            finalURL += '&dpt=null&viewId=';
            return finalURL;
        }
        catch (Exception ex) { throw new AuraHandledException(Utility.getErrorMessage(ex)); }
    }
	
    /**
    * @description ImageDataWrapper to hold data related to Image URL
    */
    public class ImageDataWrapper {
        public String imageTag;
        public String imageURL;
        public String decodedURL;
        public String finalURL;
        public String versionId;
        public String contentDocId;
        public String contentBodyId;
        public String fileType;
        public String contentDownloadURL;
        public String contentPublicURL;
    }
}