@isTest
public class TestAmountOutstanding {

   // private static Id leaseId ='a0SG000000Anrns'; //Hardcoding to Lease '7773_AirIndia_Lease' 
    //deprecated
    @isTest
    public static void loadTestCases() {
        
      /*  map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);   
        
        Aircraft__c a = new Aircraft__c(MSN_Number__c='12345', Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available');
        
        LeaseWareUtils.clearFromTrigger();
        insert a;
        a = [SELECT Name, MSN_Number__c
             FROM Aircraft__c
             WHERE Id = :a.Id];
        String msn=a.MSN_Number__c;
      
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        
        LeaseWareUtils.clearFromTrigger();
        insert operator;
       
        String LeaseName = 'My Lease';
        Lease__c l = new Lease__c(Aircraft__c=a.Id, Name=LeaseName, 
                    Operator__c=operator.Id, UR_Due_Day__c='10',Lease_Start_Date_New__c=Date.parse('01/01/2010'), 
                    Lease_End_Date_New__c=Date.parse('12/31/2012'));
        
        LeaseWareUtils.clearFromTrigger();
        insert l;
        
        Lease__c lease = [SELECT Name, Lease_End_Date__c
             FROM Lease__c
             WHERE Id = :l.Id];
        String leaseName2=l.Name; 
        System.assertEquals(LeaseName, leaseName2);
        
        
        
        lease.Lease_End_Date__c=lease.Lease_End_Date__c.addMonths(1);
        
        LeaseWareUtils.clearFromTrigger();
        update lease;
  
        Date endDate = l.Lease_Start_Date_New__c;//system.today().addMonths(-3);
        endDate = endDate.addMonths(1).toStartOfMonth().addDays(-1);
        String UrType ='Actual'; 
        Utilization_Report__c testURForCMR2 = new Utilization_Report__c(
            Name= 'thisURForCMR', Aircraft__c=a.Id, FH_String__c='100:35',Type__c = UrType, 
            Y_hidden_Lease__c = lease.id,
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= endDate, 
            		Status__c='Approved By Lessor',
            		Maint_Reserve_Heavy_Maint_1_Airframe__c=100000,
            		Maintenance_Reserve_Engine_1__c = 20000,
            		Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();
        insert testURForCMR2;
        
        endDate = l.Lease_Start_Date_New__c.addMonths(1);
        endDate = endDate.addMonths(1).toStartOfMonth().addDays(-1);
        Utilization_Report__c testURForCMR3 = new Utilization_Report__c(
            Name= 'thisURForCMR3', Aircraft__c=a.Id, FH_String__c='100:35',Type__c = UrType, 
            Y_hidden_Lease__c = lease.id,
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= endDate, 
            		Status__c='Approved By Lessor',
            		Maint_Reserve_Heavy_Maint_1_Airframe__c=100000,
            		Maintenance_Reserve_Engine_1__c = 20000,
            		Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();
        insert testURForCMR3;
        
        
        String monthlyUtilization = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId();
        Test.startTest();
        Invoice__c newInterestInv=new Invoice__c(Lease__c=lease.Id, Name='Test UR Inv', 
                                                 RecordTypeId = monthlyUtilization,
                                                 Date_of_MR_Payment_Due__c = Date.today().addMonths(-3),
                                                 Invoice_date__c=Date.today().addMonths(-5),
                                                 Utilization_Report__c = testURForCMR2.Id,
                                                 Status__c = 'Approved',
                                                 Amount__c = 79990,
                                                 Invoice_Type__c='Aircraft MR', Refresh_Values_From_UR__c=true,
                                                 Adjustment_1__c=1000);
        
        LeaseWareUtils.clearFromTrigger();
        insert newInterestInv;
        
        //String rent = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId();
        Payment__c newPayment3 = new Payment__c(Name='Test', Invoice__c=newInterestInv.id, payment_date__c=date.today(), 
                                                //RecordTypeId=rent,
                                                  Amount__c=200);
        
        LeaseWareUtils.clearFromTrigger();
        insert newPayment3;
        
        
        AmountOutstandingController.getAmount(lease.Id);
		
		AmountOutstandingController.getReport('Aging_By_Customer_By_Asset');
		Test.stopTest();*/
    }
    
    
}