/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestEngineTemplate {

    @isTest(seeAllData=true)
    static  void CRUD_EngineTemplateTest() {
        // TO DO: implement unit test
        Custom_Lookup__c[] listThrust = [select id from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true and name ='Default'];
        Engine_Template__c et1 = new Engine_Template__c(name='RB211',Manufacturer__c ='Rolls Royce'
                            ,Model__c='RB211',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2015');
        
        LeaseWareUtils.clearFromTrigger();insert et1;
        et1.Variant__c = '3B1';
        LeaseWareUtils.clearFromTrigger();update et1;
        LeaseWareUtils.clearFromTrigger();delete et1;
        LeaseWareUtils.clearFromTrigger();Undelete et1;
        
    }
    
    @isTest(seeAllData=true)
    static  void DeleteNegativeTest() {
        // TO DO: implement unit test
        Custom_Lookup__c[] listThrust = [select id from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true and name ='Default'];
        Engine_Template__c et1 = new Engine_Template__c(name='RB211',Manufacturer__c ='Rolls Royce'
                            ,Model__c='RB211',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2015');
        
        LeaseWareUtils.clearFromTrigger();insert et1;

    // 2.1 Add Assembly
        list<Constituent_Assembly__c> listCA = new list<Constituent_Assembly__c>();
        string AssemblyName='CA';
        string CAtype;
        map<String,Object> mapEngineRecTypes = new map<String,Object>();
        TestLeaseworkUtil.setRecordTypeOfEngine(mapEngineRecTypes);
        ID recTypeId=(ID)(mapEngineRecTypes.get('Engine'));         
            
            CAtype = 'Engine 1';
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Engine_Template_CA__c =et1.Id ,  Description__c ='Test', 
                TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today()
                , Type__c = CAtype, Serial_Number__c=AssemblyName
                , RecordTypeId=recTypeId));
        LeaseWareUtils.clearFromTrigger();insert listCA;
        try{
            
            LeaseWareUtils.clearFromTrigger();delete et1;
            System.assertEquals(1,2);// not expected to reach this line
        }catch(Exception ex){
            System.debug('Expected failure : triggerInsertTest   ' + ex);
            System.assertEquals('This Engine DB is already associated with Engine.You are not allowed to delete this record.',ex.getDmlMessage(0));
            
        }
        
        et1.Performance_Limit_Cycles__c = 1000;
        LeaseWareUtils.clearFromTrigger();update et1;        
        
    }    

    @isTest(seeAllData=true)
    static  void CRUD_LLPTemplateTest() {
        // TO DO: implement unit test
        Custom_Lookup__c[] listThrust = [select id from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true and name ='Default'];
        Engine_Template__c et1 = new Engine_Template__c(name='RB211',Manufacturer__c ='Rolls Royce'
                            ,Model__c='RB211',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2015');
        
        LeaseWareUtils.clearFromTrigger();insert et1;

        LLP_Template__c LLP1= new LLP_Template__c(Name='P1',Engine_Template__c=et1.Id ,Part_Name__c='P1'
                                            ,Part_Number__c='P1',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
        LeaseWareUtils.clearFromTrigger();insert LLP1;
        LLP_Template__c LLP2= new LLP_Template__c(Name='P2',Engine_Template__c=et1.Id,Part_Name__c='P2'
                                            ,Part_Number__c='P2',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
        LeaseWareUtils.clearFromTrigger();insert LLP2;
        Engine_Template__c et2 = et1.clone();
        LeaseWareUtils.clearFromTrigger();insert et2;
        
        LeaseWareUtils.clearFromTrigger();update LLP1;
        LeaseWareUtils.clearFromTrigger();delete LLP1;
        LeaseWareUtils.clearFromTrigger();Undelete LLP1;
        
    }    
    
    @isTest(seeAllData=true)
    static  void CRUD_LLPCatalogTest() {
        // TO DO: implement unit test
        Custom_Lookup__c[] listThrust = [select id from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true and name ='Default'];
        Engine_Template__c et1 = new Engine_Template__c(name='RB211',Manufacturer__c ='Rolls Royce'
                            ,Model__c='RB211',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2015');
        
        LeaseWareUtils.clearFromTrigger();insert et1;


		LLP_Template__c LLP1= new LLP_Template__c(Name='P1',Engine_Template__c=et1.Id ,Part_Name__c='P1'
                                        	,Part_Number__c='P1',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
		LeaseWareUtils.clearFromTrigger();insert LLP1;
		LLP_Template__c LLP2= new LLP_Template__c(Name='P2',Engine_Template__c=et1.Id,Part_Name__c='P2'
                                        	,Part_Number__c='P2',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
		LeaseWareUtils.clearFromTrigger();insert LLP2;
		list<LLP_Catalog__c> insLLPCatList = new list<LLP_Catalog__c>();
		insLLPCatList.add(new LLP_Catalog__c(name='2015',LLP_Template__c = LLP1.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P2'));
		insLLPCatList.add(new LLP_Catalog__c(name='2016',LLP_Template__c = LLP1.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P3'));
		insLLPCatList.add(new LLP_Catalog__c(name='2017',LLP_Template__c = LLP1.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P4'));
		insLLPCatList.add(new LLP_Catalog__c(name='2015',LLP_Template__c = LLP2.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P5'));
		insLLPCatList.add(new LLP_Catalog__c(name='2016',LLP_Template__c = LLP2.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P6'));
		insLLPCatList.add(new LLP_Catalog__c(name='2017',LLP_Template__c = LLP2.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P7'));		
		
		LeaseWareUtils.clearFromTrigger();insert insLLPCatList;
        
        Engine_Template__c et2 = new Engine_Template__c(name='RB2112',Manufacturer__c ='Rolls Royce'
        					,Model__c='RB2112',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2018');
        
        LeaseWareUtils.clearFromTrigger();insert et2;
        
  		LLP_Template__c LLP12= new LLP_Template__c(Name='P12',Engine_Template__c=et2.Id ,Part_Name__c='P12'
                                        	,Part_Number__c='P12',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
		LeaseWareUtils.clearFromTrigger();insert LLP12;
		LLP_Template__c LLP22= new LLP_Template__c(Name='P22',Engine_Template__c=et2.Id,Part_Name__c='P22'
                                        	,Part_Number__c='P22',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
		LeaseWareUtils.clearFromTrigger();insert LLP22;
		list<LLP_Catalog__c> insLLPCatList2 = new list<LLP_Catalog__c>();
		insLLPCatList2.add(new LLP_Catalog__c(name='2015',LLP_Template__c = LLP12.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P2'));
		insLLPCatList2.add(new LLP_Catalog__c(name='2016',LLP_Template__c = LLP12.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P3'));
		insLLPCatList2.add(new LLP_Catalog__c(name='2017',LLP_Template__c = LLP12.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P4'));
		insLLPCatList2.add(new LLP_Catalog__c(name='2015',LLP_Template__c = LLP22.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P5'));
		insLLPCatList2.add(new LLP_Catalog__c(name='2016',LLP_Template__c = LLP22.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P6'));
		insLLPCatList2.add(new LLP_Catalog__c(name='2017',LLP_Template__c = LLP22.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P7'));		
		
		LeaseWareUtils.clearFromTrigger();insert insLLPCatList2;
 
        
        LeaseWareUtils.clearFromTrigger();update insLLPCatList[0];
        LeaseWareUtils.clearFromTrigger();delete insLLPCatList[0];
        try{
            LeaseWareUtils.clearFromTrigger();Undelete insLLPCatList[0];
         }catch(Exception ex){
            //expected
        }   
        
    }      
 
    @isTest(seeAllData=true)
    static  void CRUD_LLPCatSumTriggerHandler() {
        // TO DO: implement unit test
        Custom_Lookup__c[] listThrust = [select id from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true and name ='Default'];
        Engine_Template__c et1 = new Engine_Template__c(name='RB211',Manufacturer__c ='Rolls Royce'
                            ,Model__c='RB211',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2015');
        
        LeaseWareUtils.clearFromTrigger();insert et1;

        LLP_Template__c LLP1= new LLP_Template__c(Name='P1',Engine_Template__c=et1.Id ,Part_Name__c='P1'
                                            ,Part_Number__c='P1',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
        LeaseWareUtils.clearFromTrigger();insert LLP1;
        LLP_Template__c LLP2= new LLP_Template__c(Name='P2',Engine_Template__c=et1.Id,Part_Name__c='P2'
                                            ,Part_Number__c='P2',Life_Limit_Cycles__c=10000,Module_N__c='LPC');
        LeaseWareUtils.clearFromTrigger();insert LLP2;
        list<LLP_Catalog__c> insLLPCatList = new list<LLP_Catalog__c>();
        insLLPCatList.add(new LLP_Catalog__c(name='2015',LLP_Template__c = LLP1.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P2'));
        insLLPCatList.add(new LLP_Catalog__c(name='2016',LLP_Template__c = LLP1.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P3'));
        insLLPCatList.add(new LLP_Catalog__c(name='2017',LLP_Template__c = LLP1.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P4'));
        insLLPCatList.add(new LLP_Catalog__c(name='2015',LLP_Template__c = LLP2.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P5'));
        insLLPCatList.add(new LLP_Catalog__c(name='2016',LLP_Template__c = LLP2.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P6'));
        insLLPCatList.add(new LLP_Catalog__c(name='2017',LLP_Template__c = LLP2.Id,Catalog_Price__c=1000, Part_Number_New__c = 'P7'));      
        
        LeaseWareUtils.clearFromTrigger();insert insLLPCatList;
        
        LLP_Catalog_Summary__c[] llList = [select id from LLP_Catalog_Summary__c order by name asc];
        llList[0].Status__c = 'Approved';
        LeaseWareUtils.clearFromTrigger(); update llList[0];
        
    }  
    
}