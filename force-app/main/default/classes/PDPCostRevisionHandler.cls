public with sharing class PDPCostRevisionHandler implements ITrigger {

	
    public void bulkBefore() {

    }
     
    public void bulkAfter() {

    }
     
    public  void beforeInsert() {

    }
     
    public void beforeUpdate() {

    }

    public void beforeDelete() {

    }

    public void afterInsert() {

    }

    public void afterUpdate() {
        insertRows();
    }
 
    public void afterDelete() {

    }
    
    public void afterUnDelete() {

    }
 
    public void andFinally() {
    
    }



    private void insertRows () { 
        List<Revision_Factor__c> rfList = new List<Revision_Factor__c>();
        Map<String, ECI_index__c> eciIndexMap = new Map<String, ECI_index__c>();
        List<Cost_Revision__c> allConversion = (List<Cost_Revision__c>)trigger.new;
        this.doDeleteOfOldIndexes(allConversion);

        for ( ECI_index__c eciIndex : [ SELECT Index__c, Month__c, Type__c FROM ECI_index__c ] ) {
            eciIndexMap.put( eciIndex.Month__c + '-' + eciIndex.Type__c , eciIndex );
        }

        for ( Cost_Revision__c cr : allConversion ) {

            Integer monthsBetween = cr.Revision_Start_Date__c.monthsBetween(cr.Revision_End_Date__c);
            for ( Integer i = 0; i <= monthsBetween; i++ ) {
                Revision_Factor__c rf = new Revision_Factor__c();
                Date revisionDate = cr.Revision_Start_Date__c.addMonths(i);
                revisionDate = revisionDate.toStartOfMonth();
                Date RfMonth = Date.newInstance(revisionDate.year(), revisionDate.month(), 1);
                rf.Revision_Month__c = RfMonth;

                rf.Cost_Revision__c = cr.Id;
                Integer yearBase = cr.Base_Date__c.year();
                rf.Name = returnMonthBase(cr.Revision_Start_Date__c.addMonths(i).month(), 0, cr.Revision_Start_Date__c.addMonths(i));

                Decimal constantA;
                Decimal constantB;
                Decimal constantC;

                switch on cr.IndexType__c {
                    when 'Airbus' {
                        Decimal baseValue = (1 + 0.005*(rf.Revision_Month__c.year() - yearBase));
                        constantA = calculateIndex( eciIndexMap, cr.Base_Date__c, 'ECI336411W'  ).setScale(1,System.RoundingMode.HALF_UP);

                        constantB = calculateIndex( eciIndexMap, cr.Base_Date__c, 'WPU03THRU15' ).setScale(1,System.RoundingMode.HALF_UP);
                        rf.Revision_Factor__c =  ( 0.75 * ( calculateIndex( eciIndexMap, rf.Revision_Month__c, 'ECI336411W' ).setScale(1,System.RoundingMode.HALF_UP) / constantA).setScale(4,System.RoundingMode.HALF_UP));
                        Decimal group1=rf.Revision_Factor__c;
                        rf.Revision_Factor__c += ( 0.25 * ( calculateIndex( eciIndexMap, rf.Revision_Month__c, 'WPU03THRU15').setScale(1,System.RoundingMode.HALF_UP) / constantB).setScale(4,System.RoundingMode.HALF_UP));
                        Decimal group2=rf.Revision_Factor__c;

                        rf.Revision_Factor__c = (rf.Revision_Factor__c.setScale(4,System.RoundingMode.HALF_UP) * baseValue );
                        Decimal group3 = rf.Revision_Factor__c;

                        
                        System.debug(rf.Revision_Month__c);
                        System.debug('baseValue=' + baseValue);
                        System.debug('constantA=' + constantA);
                        System.debug('constantB=' + constantB);
                        System.debug( '0,75 group='+group1);
                        System.debug('0,25 group' + group2);
                        System.debug( 'final group' + group3);

                    }
                    when 'Airbus (Credit)' {
                        Decimal baseValue = (1 + 0.005*(rf.Revision_Month__c.year() - yearBase));
                        constantA = calculateIndex( eciIndexMap, cr.Base_Date__c, 'ECI336411W'  ).setScale(1,System.RoundingMode.HALF_UP);
                        constantB = calculateIndex( eciIndexMap, cr.Base_Date__c, 'WPU03THRU15'  ).setScale(1,System.RoundingMode.HALF_UP);
                        rf.Revision_Factor__c =  ( 0.75 * ( calculateIndex( eciIndexMap, rf.Revision_Month__c, 'ECI336411W'  ).setScale(1,System.RoundingMode.HALF_UP) / constantA).setScale(4,System.RoundingMode.HALF_UP));
                        rf.Revision_Factor__c += ( 0.25 * ( calculateIndex( eciIndexMap, rf.Revision_Month__c, 'WPU03THRU15').setScale(1,System.RoundingMode.HALF_UP) / constantB).setScale(4,System.RoundingMode.HALF_UP));
                        rf.Revision_Factor__c = (rf.Revision_Factor__c * baseValue ).setScale(4,System.RoundingMode.HALF_UP);
                    }
                    when 'Boeing' {
                        constantA = calculateIndex( eciIndexMap, cr.Base_Date__c, 'CIU2013000000000I'  ).setScale(1,System.RoundingMode.HALF_UP);
                        constantB = calculateIndex( eciIndexMap, cr.Base_Date__c, 'CUUR0000SA0' ).setScale(1,System.RoundingMode.HALF_UP);   
                        rf.Revision_Factor__c = ((0.65 *  calculateIndex( eciIndexMap, rf.Revision_Month__c, 'CIU2013000000000I' ).setScale(1,System.RoundingMode.HALF_UP)).divide(constantA,4, RoundingMode.HALF_UP));
                        rf.Revision_Factor__c += ((0.35 *  calculateIndex( eciIndexMap, rf.Revision_Month__c, 'CUUR0000SA0'  ).setScale(1,System.RoundingMode.HALF_UP)).divide(constantB,4, RoundingMode.HALF_UP));
                    }
                    when 'Rolls-Royce' {
                        constantA = calculateIndex( eciIndexMap, cr.Base_Date__c, 'CIU2013000000000I'  ).setScale(1,System.RoundingMode.HALF_UP);
                        constantB = calculateIndex( eciIndexMap, cr.Base_Date__c, 'CUUR0000SA0'  ).setScale(1,System.RoundingMode.HALF_UP);
                        rf.Revision_Factor__c = ((0.65) *  (calculateIndex( eciIndexMap, rf.Revision_Month__c, 'CIU2013000000000I' ).setScale(1,System.RoundingMode.HALF_UP)).divide(constantA,10)).setScale(4, RoundingMode.HALF_UP);
                        rf.Revision_Factor__c += ((0.35) *  (calculateIndex( eciIndexMap, rf.Revision_Month__c, 'CUUR0000SA0'  ).setScale(1,System.RoundingMode.HALF_UP)).divide(constantB,10)).setScale(4, RoundingMode.HALF_UP);
                    }
                    when 'CFM' {
                        Decimal baseValue = (1 + 0.005*(rf.Revision_Month__c.year() - yearBase));
                        constantA = 194.46;
                        rf.Revision_Factor__c  = (0.65 * ( (calculateIndex(eciIndexMap, rf.Revision_Month__c, 'ECI336411W') * 1.777).setScale(1,System.RoundingMode.HALF_UP) ));
                        rf.Revision_Factor__c = (rf.Revision_Factor__c +(0.35 * (calculateIndex( eciIndexMap, rf.Revision_Month__c, 'WPU03THRU15')).setScale(2,System.RoundingMode.HALF_UP))).setScale(2,System.RoundingMode.HALF_UP);
                        rf.Revision_Factor__c  = (rf.Revision_Factor__c.setScale(2,System.RoundingMode.HALF_UP).divide(constantA,10)).setScale(3, RoundingMode.HALF_UP);
                        rf.Revision_Factor__c  =(rf.Revision_Factor__c * baseValue ).setScale(6,System.RoundingMode.HALF_UP);

                    }
                    when 'PW' {
                        Decimal baseValue = (1 + 0.005*(rf.Revision_Month__c.year() - yearBase));

                        constantA = calculateIndex( eciIndexMap, cr.Base_Date__c, 'ECI336411W'  );
                        constantB = calculateIndex( eciIndexMap, cr.Base_Date__c, 'WPU03THRU15'  );
                        constantC = calculateIndex( eciIndexMap, cr.Base_Date__c, 'WPU10'  ).setScale(1,System.RoundingMode.HALF_UP);

                        rf.Revision_Factor__c  = (0.75 * ( (calculateIndex( eciIndexMap, rf.Revision_Month__c, 'ECI336411W' ).setScale(1,System.RoundingMode.HALF_UP)).divide(constantA,4, RoundingMode.HALF_UP)));
                        rf.Revision_Factor__c += (0.15 * ( (calculateIndex( eciIndexMap, rf.Revision_Month__c, 'WPU03THRU15' ).setScale(1,System.RoundingMode.HALF_UP)).divide(constantB,4, RoundingMode.HALF_UP)));
                        rf.Revision_Factor__c += (0.10 * ( (calculateIndex( eciIndexMap, rf.Revision_Month__c, 'WPU10' )).setScale(1,System.RoundingMode.HALF_UP).divide(constantC,4, RoundingMode.HALF_UP)));
                        rf.Revision_Factor__c  = rf.Revision_Factor__c.setScale(4,System.RoundingMode.HALF_UP);
                        rf.Revision_Factor__c  =(rf.Revision_Factor__c * baseValue );
                    }
                }

                rfList.add(rf);
            }
        }

        insert rfList;
    }

    public void doDeleteOfOldIndexes(List<Cost_Revision__c> allIndexes) {

        Set<Id> IdsToDel = new Set<Id>();
        for ( Cost_Revision__c cr : allIndexes ) {
            IdsToDel.add(cr.Id);
        }
        List<Revision_Factor__c>  oldValuesToDel = [ SELECT Id FROM Revision_Factor__c WHERE Cost_Revision__c IN: IdsToDel ];
        if ( oldValuesToDel.size() > 0 ) {
            delete oldValuesToDel;
        }
    }

    private Decimal indexAverage( Decimal val1, Decimal val2, Decimal val3 ) {

  		return (val1 + val2 + val3).divide(3,10);
    }

    private String returnMonth( Integer currentMonth, Integer value, Date currentDate ) {

    	String year = (String.valueOf(currentDate.year() -1 )).substring(2,4);
    	if ( (currentMonth + value) <= 0 ) year = (String.valueOf(currentDate.year() -2)).substring(2,4);
        if ( (currentMonth + value) >= 13 ) year = (String.valueOf(currentDate.year())).substring(2,4);

        Map<Integer,String> allMonths = new Map<Integer,String>{1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May',
        6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'};
    	if ( (currentMonth + value) <= 0 ) return 'Dec-' + year;
        if ( (currentMonth + value) >= 13 ) return 'Jan-' + year;
    	return (allMonths.get(currentMonth + value) + '-' + year);
    }

	private String returnMonthBase( Integer currentMonth, Integer value, Date currentDate ) {

		String year = (String.valueOf(currentDate.year())).substring(2,4);
		if ( (currentMonth + value) <= 0 ) year = (String.valueOf(currentDate.year() -1)).substring(2,4);
        if ( (currentMonth + value) >= 13 ) year = (String.valueOf(currentDate.year() +1)).substring(2,4);

        Map<Integer,String> allMonths = new Map<Integer,String>{1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May',
        6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'};
		if ( (currentMonth + value) <= 0 ) return 'Dec-' + year;
        if ( (currentMonth + value) >= 13 ) return 'Jan-' + year;
		return (allMonths.get(currentMonth + value) + '-' + year);
	}

    private Decimal  calculateIndex( Map<String, ECI_index__c> mapValues, Date monthToCalculate, String indexKey) {

        Decimal val1 = null;
        Decimal val2 = null;
        Decimal val3 = null;

        if ( mapValues.get(returnMonth(monthToCalculate.month() ,-1 , monthToCalculate) + '-' + indexKey) != null ) {
            val1 = mapValues.get(returnMonth(monthToCalculate.month() ,-1 , monthToCalculate) + '-' + indexKey).Index__c;
        }
        if ( mapValues.get(returnMonth(monthToCalculate.month() ,0 , monthToCalculate) + '-' + indexKey) != null ) {
            val2 = mapValues.get(returnMonth(monthToCalculate.month() ,0 , monthToCalculate) + '-' + indexKey).Index__c;
        }
        if ( mapValues.get(returnMonth(monthToCalculate.month() ,1 , monthToCalculate) + '-' + indexKey) != null ) {
            val3 = mapValues.get(returnMonth(monthToCalculate.month() ,1 , monthToCalculate) + '-' + indexKey).Index__c;
        }

        if ( val1 != null && val2 != null && val3 != null) {
            return 	indexAverage(val1,val2,val3);
        }
    	return 9999999;
    }
}