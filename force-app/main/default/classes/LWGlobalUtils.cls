/*
    Purpose: This apex class will get used for global methods. This should not have any buissenss logic but should call other utility methods.
        Example : callGlobalScript() 
        
    Reference document: 
        https://docs.google.com/spreadsheets/d/1ggYKejNX0pazlZYXXF-wBTg2s9ChQLtgGqLKZs91Hyw/edit#gid=1447814031    



*/
global class LWGlobalUtils {
    
    

    WebService static boolean setTriggersOff(){
        try{
            LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
            if(lwSettings==null)lwSettings = new LeaseWorksSettings__c(SetupOwnerId=System.UserInfo.getUserId());
            lwSettings.Disable_All_Triggers__c=true;
            upsert lwSettings;
        }catch(exception e){
            return false;
        }
        return true;
    }

    WebService static boolean setTriggersOn(){
        try{
            LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
            if(lwSettings==null)return true;
            lwSettings.Disable_All_Triggers__c=false;
            update lwSettings;
        }catch(exception e){
            return false;
        }
        return true;
    }


    WebService static boolean setTriggersAndWFsOff(){
        return LeasewareUtils.setTriggersAndWFsOff();
    }

    WebService static boolean setTriggersAndWFsOn(){
        return LeasewareUtils.setTriggersAndWFsOn();
    }


    WebService static boolean setWFsOff(){
       return LeasewareUtils.setWFsOff();
    }

    WebService static boolean setWFsOn(){
        return LeasewareUtils.setWFsOn();
    }

    WebService static String getZ_FieldsWithReadPerm(){        
        return 'deprecated';
    }
    
    WebService static void emailZ_FieldsWithReadPerm(string strEmailId){

        String strBody =  '';
        string Z_UnusedFdls = LWGlobalUtils.getZ_FieldsWithReadPerm();
        if(''.equals(Z_UnusedFdls)){
            strBody += ('<BR>No unused fields found.');
        }else{
            strBody += ('<BR>Following unused fields should be turned off:<BR>'+ Z_UnusedFdls);
        }
        LeaseWareUtils.sendEmail('Z_Unused fields on ' + System.UserInfo.getOrganizationName(), strBody, strEmailId);

    }
    
    WebService static void copyLeaseStartEndDates(){
         /* deprecated */
    }


    WebService static void setRatioInfo(ID URId){
        
		/* deprecated */
        
  	}


    WebService static void CopyLLPDescToName(){
        //deprecated
    }
    
    
    WebService static void UpdateAllB7xTo7x()
    {
        //deprecated
    }
    
    
    WebService static void setBodyTypeAll()
    {
        //deprecated
    }
    
    WebService static void setAircraftID()
    {
        //deprecated
    }

    WebService static void setPlacementByDateOnLeases()
    {
        //deprecated
    }

    WebService static string CreateCampaigns(Id leaseId)
    {
        return LeaseWareUtils.CreateCampaign(leaseId);
    }

    WebService static string PopulateTopup(Id leaseId)
    {
        return 'deprecated';
    }

    WebService static string CopyTopupToAircraft(Id AircraftId)
    {
        return 'deprecated'; 
    }
    
    WebService static string DeleteProspects(Id CampaignId)
    {
        return 'deprecated'; 
    }
    
    WebService static void setExternalIdOnWF()
    {
        //deprecated
    }

    WebService static string setMatchingOpsOnWF(Id OpId)
    {
        return LeasewareUtils.setMatchingOpsOnWF(OpId);
    }

    WebService static string setMatchingOpsOnWFAll()
    {
        return 'deprecated';
    }
    
    WebService static string GenerateSnapshots(Id LeaseId)
    {
        return 'deprecated';
    }

    WebService static void setLowestLimiterOnCA(list<Id> listCAIDs)
    {
        LeaseWareUtils.setLowestLimiterOnCA(new set<id>(listCAIDs), true, true, true,null);
    }

    global static void setLowestLimiterOnCA(list<Id> listCAIDs,boolean executeLowestLLP,boolean executeSetCurrentTS,boolean updateAVG,Date URDate)
    {
        //(setCAIDs, executeLowestLLP, executeSetCurrentTS, updateAVG,CurrentURDate);
        System.debug('=== before LWGlobalUtils.setLowestLimiterOnCA  Number of Query Used: ' + Limits.getQueries());
        LeaseWareUtils.setLowestLimiterOnCA(new set<id>(listCAIDs), executeLowestLLP, executeSetCurrentTS, updateAVG ,URDate);
        System.debug('=== aftter LWGlobalUtils.setLowestLimiterOnCA  Number of Query Used: ' + Limits.getQueries());
    }    

    WebService static void SeedAircraftMaintenanceEventLookup()
    {
        //deprecated
    }

    WebService static void SeedThrustSettingLookup()
    {
        //deprecated
    }

    WebService static void TSUptakeScript()
    {
        //deprecated LeaseWareDataFixScript.TSUptakeScript();
    }

    WebService static void createLWAdminPublicGroup(){
        LeaseWareUtils.createLWAdminPublicGroup();
    }
    WebService static void sendEmailToLWAdmins(String subject, String body)
    {
        LeaseWareUtils.sendEmailToLWAdmins(subject, body);
    }       

    WebService static void MPEAssemblyScript()
    {
        //deprecated : LeaseWareDataFixScript.MPEAssemblyScript();
    }
    
    WebService static Date getNearestWorkDay(Date DayIn, Id OpOrACId)
    {
        //return LeaseWareUtils.getNearestWorkDay(DayIn, OpOrACId);
        return system.today();
    }

    WebService static void UpdateNextEventAC()
    {
        //deprecated LeaseWareDataFixScript.UpdateNextEventAC();
    }   
    WebService static void UpdateMPEValueOnAC()
    {
        //deprecated LeaseWareDataFixScript.UpdateMPEValueOnAC();
    }   
    WebService static void UpdateNextEventCA()
    {
        //deprecated
        //LeaseWareDataFixScript.UpdateNextEventCA();
    }   
    WebService static void MPELGDataScript()
    {
        //deprecated
        //LeaseWareDataFixScript.MPELGDataScript();
    }
	
	WebService static string CreateAssumedUR(Id LeaseId){
		
		//deprecated
		//return LeaseWareUtils.CreateAssumedUR(LeaseId);
		
		return 'deprecated';
	}	

    WebService static void ACEEInvDataScript_UpdateInAmt()
    {
        //deprecated LeaseWareDataFixScript.ACEEInvDataScript_UpdateInAmt();
    }
    WebService static void MRssDataScript_UpdateSeqNum()
    {
        /* Not using after Airfram breaout */
        //LeaseWareDataFixScript.MRssDataScript_UpdateSeqNum();
    }
    WebService static void CAEEInvDataScript_UpdatePaymentDate()
    {
        //deprecated LeaseWareDataFixScript.CAEEInvDataScript_UpdatePaymentDate();
    }   
    WebService static string InsUpdEventForecast(Id AcID)
    {
        return 'Deprecated ';
    }
    WebService static void UR_UpdateType()
    {
        //deprecated LeaseWareDataFixScript.UR_UpdateType();
    }
    WebService static void CAEvent_UpdateType()
    {
        //deprecated LeaseWareDataFixScript.CAEvent_UpdateType();
    }   
    WebService static string createAircraftDS(Id AcID)
    {
        return 'Deprecated ';
    }   
    WebService static void UR_UpdateRunningHoursCycles()
    {
        //deprecated
                   
    }   
    WebService static void UR_UpdateTSN_CSN_MonthStart()
    {
        //deprecated
    }   
    WebService static string UpdateURTsnCsnMonthStart(Id AcID)
    {
        LWGlobalUtils.setTriggersAndWFsOff();
        LeaseWareUtils.TriggerDisabledFlag=true;
        String msg;
        try{
            msg= LeaseWareUtils.UpdateURTsnCsnMonthStart(new set<id>{AcID},false);
		}catch(Exception ex){
        	LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger enabled for all users at apex level
        	LWGlobalUtils.setTriggersAndWFsOn();			
			throw new LeaseWareException('!!! Unexpected Error, please check with System Administrator :' + ex);
		}            
        LeaseWareUtils.TriggerDisabledFlag=false;
        LWGlobalUtils.setTriggersAndWFsOn();
        return msg;
    }   
    WebService static string UpdateAssemblyURTsnCsnMonthStart(Id CAID)
    {
        return  LeaseWareUtils.UpdAssemblyURTsnCsnMonthStartWrap(CAID);

    }      
    WebService static void setTriggersflagOn(){
        LeasewareUtils.TriggerDisabledFlag = true;
    }
    WebService static void setTriggersflagOff(){
        LeasewareUtils.TriggerDisabledFlag = false;
    }   
    WebService static boolean isTriggerDisabled(){
        return LeasewareUtils.isTriggerDisabled();
    }
    
    webservice static void GenerateAssemblyMRRateRecs(list<ID> listLeaseIds, boolean bDelAndGenMRRateRecs){
		//Anjani:  deprecated ....
        
    }
    WebService static string CreateRentSchedule(Id leaseId)
    {

       return 'deprecated';
     }
    
    WebService static string CreateAuditSchedule(Id leaseId)
    {
        //Deprecated:
        /* 
        leasewareUtils.InsertAuditRecs(leaseId);
        return 'Audit Schedule Creation In Progress.';*/
        return null;
    }
    
    
    
    WebService static string  UpdateStackCostPerCycle(Id LLPMRRateID)
    {
      
        return 'Deprecated';
    }

    WebService static string CreateInterestSchedule(Id leaseId)
    {

        return 'Deprecated';
    }


  WebService static string UpdateWorldFleetReferences(){
        return 'Job with Id ' + Database.executeBatch(new BatchUpdateWorldFleet()) + ' submitted.';
  }  

 //This Method will update the LLPs in Assembly Page, if there is any changes in LLP @ SV page under Assembly Maintenace Event Page.

  WebService static string  UpdateLLPsFromAME(Id LLPatSVID){
    
        return 'deprecated';
  }
  
  
    // Generic global method 
    WebService static string  callGlobalMethod(string TypeOfmethod,Id RecId){
        
       if('findHiddenAndUnusedFields'.equals(TypeOfmethod)) return BatchNotifyHiddenFieldsOnLayouts.findHiddenAndUnusedFields();
        
        return 'Type of method not matched.';  
    }

    WebService static void  callUnsetTriggers(string triggerName){
        leasewareUtils.unsetTriggers(triggerName);
    }
    WebService static void  callSetTriggers(string triggerName){
        leasewareUtils.setFromTrigger(triggerName);
        if(triggerName == 'URTriggerHandler.RESET_STATIC')
        {
            URTriggerHandler.ResetStatic();
        }
        else if(triggerName == 'URLITriggerHandler.RESET_STATIC')
        {
            URLITriggerHandler.ResetStatic();
        }
    }  
    
    WebService static void  callcreateURInvoiceOnApproved(list<id> listURids,String InvRecordId){
        System.debug('=== before LWGlobalUtils.callcreateURInvoiceOnApproved  Number of Query Used: ' + Limits.getQueries());
        URTriggerHandler.callcreateURInvoiceOnApproved(new set<id>(listURids),InvRecordId);
        System.debug('=== after LWGlobalUtils.callcreateURInvoiceOnApproved  Number of Query Used: ' + Limits.getQueries());
    }   
    
    WebService static void  createSnapshot(Id leaseId){   
        /*deprecated*/
    }

    // global method
    // Generic global method 
    global static string  callGlobalScript(string TypeOfmethod,Integer BatchSize,boolean updateMode,map<string,string> mapParam_p){
        system.debug('LWGlobalUtils.callGlobalScript Param =' + ' TypeOfmethod ' + TypeOfmethod + ' mapParam_p ' + mapParam_p + ' BatchSize ' + BatchSize + ' updateMode ' + updateMode);

        if(TypeOfmethod.startswith('v-')){
            string[] splitStr = TypeOfmethod.split('v-');
            GenericScriptExecute.callGenericScriptMethod(Integer.ValueOf(splitStr[1]),BatchSize,updateMode,mapParam_p);
        }else if('Z_Objects'==TypeOfmethod){
            string packVersion= '0.1';        
            if(!LeaseWareUtils.isRunningInDevInstance()) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('Z_UnusedRemPermissionOfObject_202003',packVersion,BatchSize,updateMode,mapParam_p,null);
        }else if('Z_Fields'==TypeOfmethod){
            string packVersion= '0.1';
            if(!LeaseWareUtils.isRunningInDevInstance()) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('Z_UnusedRemPermissionOfField_202003',packVersion,BatchSize,updateMode,mapParam_p,null);
        }else if('Dup_F'==TypeOfmethod || 'FindDupFields_202006'==TypeOfmethod){
            string packVersion= '0.1';
            if(!LeaseWareUtils.isRunningInDevInstance()) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('FindDupFields_202006',packVersion,BatchSize,updateMode,mapParam_p,null);
        }else if('ShareFilesWithLib'==TypeOfmethod || 'shareFilesWithLibrary_202006'==TypeOfmethod){
            string packVersion= '0.1';
            if(!LeaseWareUtils.isRunningInDevInstance()) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('shareFilesWithLibrary_202006',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('AddUsersToLib'==TypeOfmethod || 'addUsersIntoLibrary_202006'==TypeOfmethod){
            string packVersion= '0.1';
            if(!LeaseWareUtils.isRunningInDevInstance()) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('addUsersIntoLibrary_202006',packVersion,BatchSize,updateMode,mapParam_p,null);
        }else if('CreateAccountForOprLsrCP_202009'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('CreateAccountForOprLsrCP_202009',packVersion,BatchSize,updateMode,mapParam_p,null);
        }else if('UpdateDealSummary_202012'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('UpdateDealSummary_202012',packVersion,BatchSize,updateMode,mapParam_p,null);
        }else if('updateLatestFileDetailsOnAsset_202012'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('updateLatestFileDetailsOnAsset_202012',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('updateLatestFileDetailsOnLease_202012'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('updateLatestFileDetailsOnLease_202012',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('updateLatestFileDetailsOnDeal_202012'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('updateLatestFileDetailsOnDeal_202012',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('attachAssociatedAirframeToAsset_202012'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('attachAssociatedAirframeToAsset_202012',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('verifyAssociatedAssetAttached_202012'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('verifyAssociatedAssetAttached_202012',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('updateTypeAndPSLIOnPayment_202012'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('updateTypeAndPSLIOnPayment_202012',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('deleteOldDmoRecords_202103'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('deleteOldDmoRecords_202103',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }else if('setAircraftTypeQtyOnDeal_202106'==TypeOfmethod){
            string packVersion= '0.1';
            if(leasewareUtils.getNamespacePrefix()!=null && leasewareUtils.getNamespacePrefix().length()>1) packVersion =  ''+system.requestVersion();
            GenericScriptExecute.callGenericScriptMethod_Z('setAircraftTypeQtyOnDeal_202106',packVersion,BatchSize,updateMode,mapParam_p,GenericScriptExecute.C_QL);
        }
        else{
            GenericScriptExecute.callGenericScriptMethod(TypeOfmethod,BatchSize,updateMode,mapParam_p);
        }
        return TypeOfmethod + ': successfully Executed';  
    }    

    // read only method
    global static string  callGlobalScript(string TypeOfmethod){
        if(TypeOfmethod.startswith('v-')){
            string[] splitStr = TypeOfmethod.split('v-');
            GenericScriptExecute.callScript(Integer.ValueOf(splitStr[1]));
        }else{
            return callGlobalScript(TypeOfmethod,null,false,null);
        }
        return 'successful';
    }          

}