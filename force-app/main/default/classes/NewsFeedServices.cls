//@updated 2016.12.11 :5 PM : Arjun : General News Feed Services to be used with Map.
public class NewsFeedServices
{ 
    public static final String SERVICENAME = 'News_Feed_Aviator';
    private list<string> qSearch = new list<string>();
    private map<String,NewsArticleWrapper> articlesGlobal = new map<String,NewsArticleWrapper>();
    private static string operatorNameFilter = LeaseWareUtils.getLWSetup_CS('OPERATOR_NAME_FILTER') ;
    private list<NewsArticleWrapper> articlesGlobalDel = new list<NewsArticleWrapper>();
    public static final Set<String> SETTOEXCLUDE = new Set<String>{'Inc.','Corp.','Co.','LLC','Ltd.','GmbH','AG','S.A.','SA','plc'}; 
   
    
   
    public string  responsebody                       {get;set;}
    public integer statuscode                          {get;set;} 
    public string  statusMessage                       {get;set;}
    public static string token                                {get;set;}
    public static string endPointUrl                          {get;set;}
    public Operator__c     operator;
    public Counterparty__c lessor;
    private bank__c bank;
    public lessor__c setupObj;
    private final integer REQUESTTIMEOUT         = 60000;    //max request timeout set as 60 seconds
    public string aircraftType;
    private static Integer pageSize ;
    private static Integer pageNumber ;
    private String searchTerm = ''; 
    private string startDate = '' ;
    private String endDate  = '';
    private String region = '';
    private String category = '';
    private integer totalEntries ;
    private string page ;
    private integer totalPages ;
    public string operatorName = '',operatorAlias = '',opeartorLesseee = '',operatorWF = '';
    private static boolean opName = false;
    private static boolean notEM = false;  // this is to decide wether to do exact match or not in API 
    Map<String,NewsArticleWrapper> mapNewsArticle  = new Map<String,NewsArticleWrapper>();
    public List<String> listOpAlias = new List<String> ();

    private final string  Name = 'World Fleet Name/Name';
    public NewsFeedServices( ) 
    {       
        statuscode    = 0;
        statusMessage = '';
        //responseBody = '';
        aircraftType  = '';
    }
    
    //Type Param comes in format ==> AirlineName Type1,Type2,Type3.. Where Type1 could be AircraftType+Variant or AircraftType
    public String  getNewsArticles( Id operatorId , String type , String endPointUrl1,String aviatortoken , 
                                                    Integer customPageNumber, Integer customPageSize,String searchKeywrd,
                                                    String startDt, String endDt,String fieldName,String regionValue, String categoriesValue)
    {                   
        list<NewsArticleWrapper> existingArticles = new list<NewsArticleWrapper>();
       // list<NewsArticleWrapper> existingArticlesTmp = new list<NewsArticleWrapper>();
        list<NewsArticleWrapper> existingArticlesNew = new list<NewsArticleWrapper>();   
        list<String> responseFromAll = new List<String>();
        
        if(String.isNotBlank(aviatortoken)) {token = aviatortoken;}
        else {token = requestAviatorApiToken();}
        if(String.isNotBlank(endPointUrl1)) {endPointUrl = endPointUrl1;}
        else {endPointUrl = requestAviatorUrl();}
        if (customPageSize == null){pageSize = 20;}
        else{pageSize = customPageSize;}
        if(customPageNumber == null){pageNumber = 0;}
        else{pageNumber = customPageNumber;}
        if(String.isNotBlank(searchKeywrd)) {searchTerm = searchKeywrd;}
        if(String.isNotBlank(startDt)) {startDate = startDt;}
        if(String.isNotBlank(endDt)) {endDate = endDt;}
        if(String.isNotBlank(regionValue)) {region = regionValue;}
        if(String.isNotBlank(categoriesValue)) {category = categoriesValue;}

        if(Name.equals(fieldName)) {opName = true;} // if this is true then search first with World Fleet Name, if that is not present then search with operator Name
                                                    // else search with all names.

        if (String.isBlank(token) || String.isBlank(endPointUrl) ){
            return ''; 
        }       
            if(String.isNotBlank(type))
            {   
                type =  type.replaceAll('\n',',');
                
                list<string> acTypesWVariant = new list<string>();
                Set<string> acTypesWOVariant = new Set<string>();
                list<string> acTypesWOVariantList = new list<string>();
                
                acTypesWVariant = type.split(',');
                
                for( string aircrafttypeTmp :  acTypesWVariant )
                {
                    list<string> aircraftTypeInfo = aircrafttypeTmp.split('-'); 
                    if(aircraftTypeInfo[0].startswith('7')) {acTypesWOVariant.add( 'B'+aircraftTypeInfo[0] );}            
                    else {acTypesWOVariant.add( aircraftTypeInfo[0] );}    
                }               
                acTypesWOVariantList.addAll( acTypesWOVariant );
                aircraftType = String.join( acTypesWOVariantList, ' ,' );
            }           
            
            LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
            
            if(operatorId != null) {setPageName(operatorId);}
                if(mapNewsArticle.isEmpty()){
                    System.debug('After 1st call :' + mapNewsArticle.size() +'::'+ mapNewsArticle);

                    List<String> listSearchString = new List<String> ();
                    String  searchString = '';

                    if(String.isNotBlank(searchTerm)) {
                        System.debug('call search coming from Home');
                        if(searchTerm.contains(',')) {
                           for(String str : searchTerm.split(',')){
                                searchString += str+'+';
                           }
                        }
                        else {searchString += searchTerm;}
                        notEm = true;
                    }
 
                    searchString = searchString.removeEnd('+');
                    
                    if(operatorId != null && qSearch.size()>0){
                        if(opName){  // if this is true then search first with World Fleet Name, if that is not present then search with operator Name
                            String name = '';
                            if(qSearch.size()>2 && String.isNotBlank(qSearch[2])){  // this index has WF name
                                name = qSearch[2];
                            }else { //if(String.isBlank(qSearch[2]))
                                name = qSearch[0]; 
                            }
                            String searchString2 = '';
                            if(String.isBlank(searchString)){
                                searchString2 = name;
                            }else{
                                searchString2 = name+'+'+searchString;
                            }
                            System.debug('searchString with WF Name :' + searchString2);
                            responseBody = '';
                            responseBody    = sendRequestToAviator(searchString2,'');	// sending request to aviator server
                            if(responseBody!=null && statusMessage.equals('OK')){
                                responseFromAll.add(responseBody);
                            }else{
                                responseFromAll.add('No Records');
                            }  
                        }else{  // else search with all names
                            if(qSearch.size()>3 && String.isNotBlank(qSearch[3])){ // this index has operator Alias names
                                    System.debug('----------------Searching for Aliases with Operator Name ---------------------');                 
                                    System.debug('searching Alias === ' + qSearch[3]);                  
                                    responseBody = createSearchString(0,searchString,true);
                                    if(responseBody!=null && statusMessage.equals('OK')){
                                        responseFromAll.add(responseBody);
                                    }else{
                                        responseFromAll.add('No Records');
                                    }
                            }else{
                                System.debug('----------------Searching only with Operator Name, No Alias---------------------');
                                System.debug('searching OPerator Name === ' + qSearch[0]);
                                responseBody = createSearchString(0,searchString, false);
                                if(responseBody!=null && statusMessage.equals('OK')){
                                    responseFromAll.add(responseBody);
                                }else{
                                    responseFromAll.add('No Records');
                                }
                            }
                            //first search for WF name and Lesse legal name
                            System.debug('----------------Searching for WF name and Lesse Legal Name in All Nmaes---------------------');
                            for(integer i=1;i<3;i++){
                                if(qSearch.size()>i){
                                    if(String.isBlank(qSearch[i])) {
                                        System.debug('Returning === ' + qSearch[i]);
                                        continue;
                                    }     
                                    System.debug('searching === ' + qSearch[i]);
                                    responseBody = createSearchString(i,searchString, false);
                                    if(responseBody!=null && statusMessage.equals('OK')){
                                        responseFromAll.add(responseBody);
                                }else{
                                    responseFromAll.add('No Records');
                                }  
                            }                  
                        }
                    }
                }else{
                    System.debug('searchString :' + searchString);
                    notEm = true;
                    responseBody    = sendRequestToAviator(searchString,'');	// sending request to aviator server for Home
                    System.debug('Status :' + statusMessage);
                    System.debug('responseBody :' + responseBody);
                    if(responseBody!=null && statusMessage.equals('OK')){
                        responseFromAll.add(responseBody);
                    } 
                } 

                if(responseFromAll !=null){
                    for(String responseBody : responseFromAll){
                        if( String.isNotBlank(responseBody) && !responseBody.equals('No Records'))
                        {
                            existingArticlesNew = parseJson( responseBody,false );	// parsing response to store and display
                            system.debug('existingArticlesNew===='+existingArticlesNew.size());
                            if(existingArticlesNew!=null && !existingArticlesNew.isEmpty()){
                                for(NewsArticleWrapper news:existingArticlesNew){
                                    mapNewsArticle.put(news.articleId,news);
                                }
                            } 
                            System.debug('MAP 1 :' + mapNewsArticle.size() +'::'+ mapNewsArticle);
                        }
                        existingArticlesNew.clear();
                    }  
                }
                System.debug('mapNewsArticle ==== final =====:' + mapNewsArticle.size() +'::'+ mapNewsArticle);
            }
        system.debug('mapNewsArticle==final==='+mapNewsArticle.size());
        return  System.json.serialize(mapNewsArticle.values()); 
    }   

    private String createSearchString(Integer index,String searchString,Boolean includeAlias){
        String searchString2 = '';
        String aliasName = (includeAlias)?qSearch[3]:'';
        if(String.isBlank(searchString)){
            searchString2 = qSearch[index];
        }else{
            searchString2 = qSearch[index]+'+'+searchString;
        }
        System.debug('searchString for Operator :' + searchString2);
        System.debug('aliasName for Operator :' + aliasName);
        responseBody = '';
        responseBody = sendRequestToAviator(searchString2,aliasName);	// sending request to aviator server
         return responsebody;
    }

    private void setPageName(Id recordId){
        Id operatorId = recordId;

        String operatorPrefix = Operator__c.sobjecttype.getDescribe().getKeyPrefix();
        String counterpartyPrefix  = Counterparty__c.sobjecttype.getDescribe().getKeyPrefix();
        String lessorPrefix   = Lessor__c.sobjecttype.getDescribe().getKeyPrefix();
        String bankPrefix = Bank__c.sobjecttype.getDescribe().getKeyPrefix();

        if( operatorId != null && String.valueof(operatorId).substring(0,3) == operatorPrefix )
                {
                    operator    = [ SELECT Id,Name,Alias__c,Lessee_Legal_Name__c,World_Fleet_Operator_Name__c FROM Operator__c WHERE Id =:operatorId ];  
                    if(String.isBlank(aircraftType) ){
                        qSearch.add(operator.Name);                       
                        qSearch.add(operator.Lessee_Legal_Name__c == null ? '' : operator.Lessee_Legal_Name__c);
                        qSearch.add(operator.World_Fleet_Operator_Name__c == null ? '' : operator.World_Fleet_Operator_Name__c );
                       // qSearch.add(operator.Alias__c == null ? '' : operator.Alias__c);
                        
                        if(operator != null && operator.Alias__c !=null) {
                            if(operator.Alias__c.contains(',')){
                                listOpAlias = operator.Alias__c.split(',');
                                String str = '';                       
                                for(String s:listOpAlias){
                                     str += s+';' ;
                                }                  
                                str = str.removeEnd(';');
                                System.debug('FINAL string for operator:' + str);
                                qSearch.add(str.trim()); 
                            }else{
                                qSearch.add(operator.Alias__c == null ? '' : operator.Alias__c);
                            }
                        }
                    }
                    else{
                        for(string att:aircraftType.split(' ,')){
                            qSearch.add(operator.Name +' '+att);                           
                            qSearch.add(operator.Lessee_Legal_Name__c == null ? '' : operator.Lessee_Legal_Name__c +' '+att );
                            qSearch.add(operator.World_Fleet_Operator_Name__c == null ? '' : operator.World_Fleet_Operator_Name__c +' '+att);
                            qSearch.add(operator.Alias__c == null ? '' : operator.Alias__c +' '+att);
                        }
                    }
                }
                else if(  operatorId != null && String.valueof(operatorId).substring(0,3) == counterpartyPrefix )  
                {
                    lessor        = [ SELECT Id,Name FROM Counterparty__c WHERE Id =:operatorId ];  
                    if(String.isBlank(aircraftType) ){
                        qSearch.add(lessor.Name == null ? '' : lessor.Name); //airlineName   = lessor.Name;
                    }
                    else{
                        for(string att:aircraftType.split(' ,')){
                            qSearch.add(lessor.Name == null ? '' : lessor.Name +' '+att);
                        }                	
                    }                   
                }                   
                else  if(  operatorId != null && String.valueof(operatorId).substring(0,3) == lessorPrefix ) 
                {
                    setupObj      = [ SELECT Id,Name FROM lessor__c WHERE Id =:operatorId ];  
                    if(String.isBlank(aircraftType) ){
                        qSearch.add(setupObj.Name == null ? '' : setupObj.Name);//airlineName   = setupObj.Name;
                    }   
                    else{
                        for(string att:aircraftType.split(' ,')){
                            qSearch.add(setupObj.Name == null ? '' : setupObj.Name +' '+att);
                        }                     
                    }
                }else  if(  operatorId != null && String.valueof(operatorId).substring(0,3) == bankPrefix ) 
                {
                    bank      = [ SELECT Id,Name,Alias__c FROM Bank__c WHERE Id =:operatorId ];  
                    if(String.isBlank(aircraftType) ){
                        qSearch.add(bank.Name == null ? '' : bank.Name);
                        qSearch.add('');   // need to do this to maintain index position in list for bank alias name
                        qSearch.add('');
                       // qSearch.add(bank.Alias__c == null ? '' : bank.Alias__c);

                        if(bank != null && bank.Alias__c !=null) {
                            if(bank.Alias__c.contains(',')){
                                listOpAlias = bank.Alias__c.split(',');
                                String str = '';                       
                                for(String s:listOpAlias){
                                     str += s+';' ;
                                }                  
                                str = str.removeEnd(';');
                              
                                qSearch.add(str.trim());
                            }else{
                                qSearch.add(bank.Alias__c == null ? '' : bank.Alias__c);
                            }                       
                        }   
                        System.debug('FINAL string for CP:' + qSearch);                     
                    }    
                    else{
                        for(string att:aircraftType.split(' ,')){
                            qSearch.add(bank.Name == null ? '' : bank.Name +' '+att);
                            qSearch.add('');
                            qSearch.add('');
                            qSearch[3] = bank.Alias__c == null ? '' : bank.Alias__c ;
                        }                     
                    }
                }
                 else{
                    qSearch.add('');
                }
                    operatorName = qSearch[0];
             
                    // if(qSearch.size()>1) {opeartorLesseee =  String.isBlank(qSearch[2]) ? '' : qSearch[1];}
                    // if(qSearch.size()>2) {operatorWF =  String.isBlank(qSearch[3]) ? '' : qSearch[2];}
                    // if(qSearch.size()>3) {operatorAlias = String.isBlank(qSearch[1]) ? '' : qSearch[3];}

                    System.debug('qSearch:' + qSearch);
    }

    public List<String> getOperatorName(){
        System.debug('operator names :' + qSearch);
        return qSearch;
    }
    
    public string sendRequestToAviator(string qSearchStr,String aliasName)
    {        
        HttpResponse response = sendHttpRequest(qSearchStr,aliasName);  
        system.debug( 'response=='+response + '=='+qSearchStr);
        String responsebody = response.getBody();  
        system.debug('responsebody=='+responsebody);
        statusMessage = response.getStatus();  
        statuscode    = response.getStatusCode();       
        system.debug('statuscode=='+statuscode+'  '+statusMessage);
        if(statuscode == 401){
           // LeaseWareUtils.createExceptionLog(null, 'Unauthorized Access: Please check custom settings ', null , null ,'News Article', true);
        }
        return responsebody;
    }
    
    
    public HttpResponse sendHttpRequest(string qSearchStr,String aliasName)
    {   
        system.debug('qSearchStr====='+qSearchStr);   
        HttpRequest request = new HttpRequest();
        Http http = new Http();
        HttpResponse response;
        Decimal regionCode ;
        Decimal categoryCode;
        final PageReference theUrl = new PageReference(endPointUrl);        
        theUrl.getParameters().put( 'key',token );
        system.debug('Token = '+ token);

        theUrl.getParameters().put( 'page', String.valueOf(pageNumber));
        theUrl.getParameters().put( 'per_page',String.valueOf(pageSize));
        theUrl.getParameters().put('ca',aliasName);
        theUrl.getParameters().put( 'q',qSearchStr );
        String url = theUrl.getUrl();

        if (String.isNotBlank(operatorNameFilter) && operatorNameFilter.equalsIgnoreCase('OFF')){
            notEM = true;
        }
        if(!notEM){url += '&em=1';} 
        else{url += '&em=0';}
        if(String.isNotBlank(startDate)){
            url += '&sd='+startDate;
        }
        if(String.isNotBlank(endDate)){
            url += '&ed='+endDate;
        }
        if(String.isNotBlank(region)){
            Map <string, LW_Aviator_News_Regions__c > mapLWAviatorRegions = LW_Aviator_News_Regions__c.getAll();
            System.debug('mapLWAviatorRegions:' + mapLWAviatorRegions);
           
            if(mapLWAviatorRegions.containsKey(region)){
                regionCode = Integer.valueOf(mapLWAviatorRegions.get(region).Region_Code__c);
                System.debug('regionCode:' + regionCode);
            }
            if(regionCode != null){
                url += '&rid='+regionCode;
            }
        }

        if(String.isNotBlank(category)){
            Map <string, LW_Aviator_News_Categories__c > mapLWAviatorCategories = LW_Aviator_News_Categories__c.getAll();
            System.debug('mapLWAviatorCategories:' + mapLWAviatorCategories);
           
            if(mapLWAviatorCategories.containsKey(category)){
                categoryCode =  Integer.valueOf(mapLWAviatorCategories.get(category).Category_Code__c);
                System.debug('categoryCode:' + categoryCode);
            }
            if(categoryCode != null){
                url += '&cid='+categoryCode;
            }
        }
       
        system.debug('final URL   = ' + url);
        request.setEndpoint( url );
        request.setMethod( 'GET' );
        request.setTimeout( REQUESTTIMEOUT );
        request.setHeader( 'content-type','application/json' );   
        request.setHeader( 'accept','application/json' );
        try     {
            system.debug('Response Value before call ====='+response);
            system.debug('Request Value before call ====='+request);
            system.debug('Response Body====='+responseBody); 
            response = http.send( request );  
            system.debug('response from services just after calling servies = ' +response );
            
        }
        catch( System.CalloutException exCall )     {
            system.debug('exception in HTTPResponse call=='+exCall.getMessage()); 
            system.debug('Response body in exception handling catch block====='+responseBody); //RL
            response = http.send( request );
        }
        return response;
    }
    public list<NewsArticleWrapper> parseJson( String response ,boolean showMore ) //Parsing JSOSN Date into respective format.
    {  
         List<Entries> newsTransactionFeeds = new list<Entries> ();
        if(String.isNotBlank(response))
        {
            system.debug('JSON Parser in parsing mechanism  1st= '+response);
            response = response.replaceAll('"date":','"Trans_date":');
            response = response.replaceAll('"category":','"category_p":');
         //   system.debug('JSON Parser in parsing mechanism 2nd= '+response);
         JSONParser parser1 = JSON.createParser(response);    
         while (parser1.nextToken() != null) {
            if(parser1.getCurrentToken() == JSONToken.FIELD_NAME){
                while(parser1.nextValue() != null){
                 if(String.isNotBlank(parser1.getCurrentName()) && parser1.getCurrentName().equals('total_entries')){                   
                     totalEntries = parser1.getIntegerValue();
                 }else if(String.isNotBlank(parser1.getCurrentName()) && parser1.getCurrentName().equals('page')){
                     page = parser1.getText();
                 }else if(String.isNotBlank(parser1.getCurrentName()) && parser1.getCurrentName().equals('total_pages')){
                    totalPages = parser1.getIntegerValue();
                 }
                }
            }
         }
         System.debug('totalEntries ---' + totalEntries +'----page---'+page +'----totalPages---'+ totalPages);  

            
            list<Entries> listInv = new list<Entries>();
            JSONParser parser = JSON.createParser(response);            
            while (parser.nextToken() != null) {                          
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                   // System.debug('TOKEN 3: start array : ' + parser.getCurrentToken() ); 
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to
                        //  find next invoice statement object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                           // System.debug('TOKEN 3:' + parser.getCurrentToken() ); 
                            // Read entire invoice object, including its array of line items.
                            Entries inv = (Entries)parser.readValueAs(Entries.class);
                                listInv.add(inv);
                                newsTransactionFeeds.add(inv);
                      }
                    }
                }//if condition                              
            }// While Loop
            
          //  system.debug('listinvoie.size()=========='+listInv.size()); //ok
          system.debug('=========='+newsTransactionFeeds.size()); 
 
            for(Entries curRec:listInv){
                
                if(curRec.regions != null){
                    for(RegionItem curline:curRec.regions){
                    }
                    for(source_url_Item curline:curRec.source_urls){
                    } 
                }
                
            }
            
        }          
     //   system.debug('newsTransactionFeeds======='+newsTransactionFeeds.size()+'  '+newsTransactionFeeds);
        
        for( Entries newFeed: newsTransactionFeeds )
        {  
            if(newFeed.source_urls != null){		
                
                if(newFeed.source_urls.size()>0){
                    newFeed.source_url_1 = newFeed.source_urls[0].url;
                }
                else{
                    newFeed.source_url_1 = '';
                }

                integer count = newFeed.article.countMatches('*');
                for(integer i= 0; i<count;i++)
                {
                    if(newFeed.article.contains('*') && math.mod(i,2) == 0)
                    {
                        newFeed.article = newFeed.article.replaceFirst('\\*','<B>');
                        continue;
                    }
                    if(newFeed.article.contains('*') && math.mod(i,2) != 0)
                    {
                        newFeed.article = newFeed.article.replaceFirst('\\*','</B>');
                        continue;
                    }
                }   
            }  
            
        }
        system.debug('newsTransactionFeeds===last===='+newsTransactionFeeds.size()+'  '+newsTransactionFeeds);
        list<NewsArticleWrapper> articles         = new list<NewsArticleWrapper>();        
        for ( entries newsFeed: newsTransactionFeeds )
        {
            date transDate = stringToDate( newsFeed.Trans_date );
            
            if(String.isBlank(newsFeed.article)){
                continue ;
            }
                if( String.isBlank(aircraftType)){
                    articles.add( createArticle( newsFeed,operator,lessor,setupObj,null ) );
                }
                else{
                    articles.add( createArticle( newsFeed,operator,lessor,setupObj,aircraftType ) );
                }                
        }      
        if( !articles.isEmpty())
        {
            for(NewsArticleWrapper news: articles){
                articlesGlobal.put(news.articleId,news); 
            }
        }
        System.debug('Articles added:' + articles.size() +'::'+ articles);
        newsTransactionFeeds.clear();
        return articles;
    }
    
    
    public NewsArticleWrapper createArticle( entries newsFeedTmp,Operator__c operatorTmp,Counterparty__c lessortmp , lessor__c setup, String type  )
    {
        NewsArticleWrapper article  = new NewsArticleWrapper();
        if( operatorTmp == null && setup==null && lessortmp != null )   {
            article.name            = lessortmp.Name;
            article.lessor       = lessortmp.Id;
        }
        else if( setup == null && operatorTmp!=null)    {
            article.name            = operatorTmp.Name;
            article.operator     = operatorTmp.Id;
        }
        else if(setup != null)  {
            article.name            = setup.Name;
            article.setup        = setup.Id;
        }else{
            article.name            = 'Home';
        }
        
        if( newsFeedTmp == null )   
            article.articleDate         = System.Today();
        else
        {
            article.articleDate         = stringToDate( newsFeedTmp.Trans_date );
        }
        
        if( newsFeedTmp==null )
        {
           // article.article   = NONEWSFOUNDMSG;
           // article.Tip__c       = NONEWSFOUNDMSG;
        }
        else
        {
            if( newsFeedTmp != null && newsFeedTmp.article != null){ 
                    article.article = newsFeedTmp.article ;
                    article.fullArticle = newsFeedTmp.article;
            }
            //article.Tip__c        = newsFeedTmp.articletip;
            article.link       = newsFeedTmp.source_url_1;
            article.articleId = String.valueOf(newsFeedTmp.id);
            article.newsSource = 'Aviator';
            article.totalEntries = totalEntries;
            article.page = page;
            article.totalPages = totalPages;

            if(pageNumber == totalPages){
                article.isLastPage = true;
            }
        }
        if( type!=null ){article.type = type;}

        return article;
    }
    
    public date stringToDate( string inputDate )
    {
        if(inputDate != null){
           // system.debug('inputDate = '+inputDate);
            string tDate = inputDate;
            String[] dateStr = tDate.split('-');
          // system.debug('dateStr=='+dateStr);
            Date outputDate  = date.newinstance( Integer.valueOf(dateStr[0]) , Integer.valueOf(dateStr[1]), Integer.valueOf(dateStr[2]) );
           // system.debug('outputDate=='+outputDate);
            return outputDate;
        }
        return Date.today();
    }
    
    public static string requestAviatorApiToken()
    {
        ServiceSetting__c lwServiceSetup = ServiceSetting__c.getValues(SERVICENAME); 
        if( lwServiceSetup!=null ){
            token = lwServiceSetup.Token__c;
            System.debug('lwSettings : Token :'+ token );
            return lwServiceSetup.Token__c;
        }  
        return null;
    }

    public static string requestAviatorUrl()
    {
        ServiceSetting__c lwServiceSetup = ServiceSetting__c.getValues(SERVICENAME); 
        if( lwServiceSetup!=null ){
            endPointUrl = lwServiceSetup.EndPointUrl__c;
            System.debug('lwSettings : endPointUrl: '+endPointUrl  +'  '+token );
            return lwServiceSetup.EndPointUrl__c;
        }  
        return null;
    }

    @testVisible
    public class NewsArticleWrapper {
        String name {get;set;}
        Id operator {get;set;}
        Id lessor {get;set;}
        Id setup {get;set;}
        Date articleDate {get;set;}
        String article {get;set;}
        String tag {get;set;}
        String type {get;set;}
        String link {get;set;}
        String title {get;set;}
        @testVisible
        String articleId {get;set;}
        @testVisible
        string newsSource {get;set;}
        string fullArticle {get;set;}
        boolean isLastPage {get;set;}
        integer totalEntries {get;set;}
        string page {get;set;}
        @testVisible
        integer totalPages {get;set;}

        public NewsArticleWrapper (){name = '';
                                operator = null;
                                lessor = null;
                                setup= null;
                                articleDate = null;
                                article = '';
                                tag = '';
                                type = '';
                                link = '';
                                title = '';
                                articleId = '';
                                newsSource = '';
                                fullArticle = '';
                                isLastPage = false;
                        }
    }
    
    @testVisible
    public class Entries
    {
        public String article             {get;set;} //article
        public String Trans_date    {get;set;} //date        
        public string company 			  {get;set;} //company
        public List<source_url_Item> source_urls         {get;set;} //source_urls
        public String source_url_1         {get;set;} //source_urls
        public List<RegionItem> regions         {get;set;} //regions
        public String category_p                {get;set;}//category
        public Long id                 {get;set;} //id        
        public Entries()
        {
            this.source_urls        = new List<source_url_Item>();
            this.regions        = new List<RegionItem>();
        }
    }
    
    public class RegionItem {
        public string name;
    }
    
    public class source_url_Item {
        public string url;
    }  
}