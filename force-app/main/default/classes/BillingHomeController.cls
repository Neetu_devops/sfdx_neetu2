public without sharing class BillingHomeController {

	@AuraEnabled
	public static ContainerWrapper getBillingData(){

		ContainerWrapper container = new ContainerWrapper();
		container.userName = Userinfo.getName();

        BillsOverviewWrapper billsOverviewTotal =  new BillsOverviewWrapper();
        
        List<Operator__c> operators = UtilityWithSharingController.getOperators();
        String communityPrefix = UtilityWithoutSharingController.getCommunityPrefix();
        
		for(Lease__c lease : [select Id, Deposit_Type_External__c, Aircraft__r.MSN_Number__c,
		                                  Aircraft__r.RecordType.DeveloperName, Aircraft__c,
                                          Security_Deposit__c,
		                                  (select Name, MSN_ESN__c, Paid_Date__c,Period_To__c,
		                                   Period_From__c, Outstanding_Amount__c, Paid_Amount__c,
		                                   Invoice_Amount__c,Issue_Date__c, Invoice_Type__c
		                                   from Invoices_Stage__r)
		                                  from Lease__c
		                                  where lessee__c in : operators
                                          and Lease_Status_External__c = 'Active'  and Aircraft__c != null]) {


			BillsOverviewWrapper billsOverview =  new BillsOverviewWrapper();
			billsOverview.msn = lease.Aircraft__r.MSN_Number__c;
			billsOverview.depositType = lease.Deposit_Type_External__c;
                                              
			if(lease.Aircraft__r.RecordType.DeveloperName == 'Aircraft') {
				billsOverview.url =  communityPrefix + '/s/aircraft-details?recordId=';
			}else{
				billsOverview.url =  communityPrefix + '/s/engine-details?recordId=';
			}
			
			billsOverview.url += UtilityWithoutSharingController.encodeData(lease.Aircraft__c);
			                                  
			Decimal outstandingAmount, paidAmount;
                                              
            billsOverview.deposit += lease.Security_Deposit__c == null ? 0 : lease.Security_Deposit__c;
                                              
			for(Invoice_External__c invoice : lease.Invoices_Stage__r) {

				outstandingAmount = invoice.Outstanding_Amount__c == null ? 0.00 : invoice.Outstanding_Amount__c;
				paidAmount = invoice.Paid_Amount__c == null ? 0.00 : invoice.Paid_Amount__c;

//				billsOverview.deposit +=  paidAmount;
				billsOverview.totalOutstanding += outstandingAmount;

				if(invoice.Issue_Date__c != null) {

					Integer days = invoice.Issue_Date__c.daysBetween(System.today());

					if(days <= 15) {
						billsOverview.firstPeriod += outstandingAmount;
					}else if(days > 15 && days <= 30) {
						billsOverview.secondPeriod += outstandingAmount;
					}else if(days > 30 && days <= 60) {
						billsOverview.thirdPeriod += outstandingAmount;
					}else if(days > 60 && days <= 90) {
						billsOverview.forthPeriod += outstandingAmount;
					}else{
						billsOverview.fifthPeriod += outstandingAmount;
					}
				}
			}
            billsOverviewTotal.deposit += billsOverview.deposit;
            billsOverviewTotal.totalOutstanding += billsOverview.totalOutstanding; 
            billsOverviewTotal.firstPeriod += billsOverview.firstPeriod;
            billsOverviewTotal.secondPeriod += billsOverview.secondPeriod;
            billsOverviewTotal.thirdPeriod += billsOverview.thirdPeriod;
            billsOverviewTotal.forthPeriod += billsOverview.forthPeriod;
            billsOverviewTotal.fifthPeriod += billsOverview.fifthPeriod;                            

			container.billsOverview.add(billsOverview); 
		}
        
        billsOverviewTotal.msn = 'Total';
        billsOverviewTotal.url = '#';

        container.billsOverview.add(billsOverviewTotal);
        
        
		container.totalOustandingBills = [select count() from Invoice_External__c where Outstanding_Amount__c > 0
                                          and Lease__r.lessee__c in : operators];
        
		container.totalPaidBills = [select count() from Invoice_External__c where
		                            (Outstanding_Amount__c = 0 or Outstanding_Amount__c = null)
                                    and Lease__r.lessee__c in : operators];

		return container;

	}

	public class ContainerWrapper {
		@AuraEnabled public String userName ;
		@AuraEnabled public Integer totalOustandingBills ;
		@AuraEnabled public Integer totalPaidBills ;
		@AuraEnabled public List<BillsOverviewWrapper> billsOverview ;

		public ContainerWrapper(){
			billsOverview = new List<BillsOverviewWrapper>();
		}
	}

	public class BillsOverviewWrapper {
		@AuraEnabled public String msn ;
		@AuraEnabled public String url ;
		@AuraEnabled public Decimal deposit ;
        @AuraEnabled public String depositType ; 
		@AuraEnabled public Decimal totalOutstanding ;
		@AuraEnabled public Decimal firstPeriod ; // 1-15 Days
		@AuraEnabled public Decimal secondPeriod ; // 16-30 Days
		@AuraEnabled public Decimal thirdPeriod ; // 31-60 Days
		@AuraEnabled public Decimal forthPeriod ; // 61-90 Days
		@AuraEnabled public Decimal fifthPeriod ; // 90+ Days

		public BillsOverviewWrapper(){
			totalOutstanding = 0.00;
			deposit = 0.00;
			firstPeriod = 0.00;
			secondPeriod = 0.00;
			thirdPeriod = 0.00;
			forthPeriod = 0.00;
			fifthPeriod = 0.00;

		}
	}

}