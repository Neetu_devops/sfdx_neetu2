public class GenerateMRMangerController {
    
    //Creating CashFlow record with MR Manager Profile and Scenario Component Output
	@AuraEnabled
    public static MRManagerWrapper fetchMRManager(String aircraftId){
        try{
            MRManagerWrapper mrw = new MRManagerWrapper();
            Aircraft__c aircraftRec = [SELECT Id, (SELECT ID FROM Scenario_Input__r WHERE Type__c = 'MR Profile') FROM Aircraft__c WHERE Id=: aircraftId LIMIT 1]; 
            String ScenarioID = '' ;
            if(aircraftRec.Scenario_Input__r.size()>0){
                ScenarioID = aircraftRec.Scenario_Input__r[0].Id;
            }
            else{
                ForcastDashboardController.ScenarioInputWrapper siw = ForcastDashboardController.getCashFlowRecord(aircraftId, 'MR Profile', true,'');
                ScenarioID = siw.scenarioInp.Id;
            }
            mrw.ScenarioInp = ScenarioID;
            List<Scenario_Component__c> listScenarioComponent = [SELECT Id, Name FROM Scenario_Component__c WHERE Fx_General_Input__c =: ScenarioID AND Type__c != 'General'];
            mrw.isForecastGenerated = listScenarioComponent.size() > 0 ? true : false;
            return mrw;
        }catch(Exception ex){
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    //Method to generate Forecast
    @AuraEnabled
    public static String generateMRProfile(String forecastId) {
        try{
            String message = ForecastBatch.GenerateOutput(forecastId);
            return message;
        }catch(Exception ex){
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    //To get CashFlow ID to hold weather Output is Generated or not
    public class MRManagerWrapper{
        @AuraEnabled public String ScenarioInp{get; set;}
        @AuraEnabled public Boolean isForecastGenerated{get; set;}
    }
}