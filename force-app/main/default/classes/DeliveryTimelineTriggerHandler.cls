public class DeliveryTimelineTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'DeliveryTimelineTriggerHandlerBefore';
    private final string triggerAfter = 'DeliveryTimelineTriggerHandlerAfter';
    public DeliveryTimelineTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('DeliveryTimelineTriggerHandler.beforeInsert(+)');

        
		
            

        system.debug('DeliveryTimelineTriggerHandler.beforeInsert(-)');       
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('DeliveryTimelineTriggerHandler.beforeUpdate(+)');
        
        
        system.debug('DeliveryTimelineTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('DeliveryTimelineTriggerHandler.beforeDelete(+)');
        updateDuration();
        
        system.debug('DeliveryTimelineTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DeliveryTimelineTriggerHandler.afterInsert(+)');
        updateDuration();
        system.debug('DeliveryTimelineTriggerHandler.afterInsert(-)');        
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DeliveryTimelineTriggerHandler.afterUpdate(+)');
        updateDuration();
        
        system.debug('DeliveryTimelineTriggerHandler.afterUpdate(-)');        
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DeliveryTimelineTriggerHandler.afterDelete(+)');
        
        
        system.debug('DeliveryTimelineTriggerHandler.afterDelete(-)');        
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DeliveryTimelineTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('DeliveryTimelineTriggerHandler.afterUnDelete(-)');      
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
 
    // beforeinsert and update
    private void updateDuration(){
        
        list<Delivery_Timeline__c> newList = (list<Delivery_Timeline__c>)(trigger.isDelete?trigger.old:trigger.new);
        set<id> setNoSelectOnDeletion = new set<id>();
        set<id> AssetTermIds = new set<id>();
        
        for(Delivery_Timeline__c  curRec:newList){
            if(!trigger.IsUpdate ) AssetTermIds.add(curRec.Asset_Term__c);
            if(trigger.IsUpdate && curRec.name!=((Delivery_Timeline__c)trigger.oldmap.get(curRec.Id)).name)AssetTermIds.add(curRec.Asset_Term__c);
        }       
        system.debug('AssetTermIds='+AssetTermIds);
        if(!AssetTermIds.isEmpty() && !system.isFuture() && !system.isBatch()){
        	updateDurationFuture(AssetTermIds);
        	system.debug('calling future method');
        }
    }
    
    @future
    public static void updateDurationFuture(set<id> AssetTermIds){
        system.debug('updateDurationFuture+');
        if(!AssetTermIds.isEmpty()){
            list<Aircraft_Proposal__c> assetTermList = [ select id 
                                                                    ,(select id,name from Delivery_Timelines__r order by name)
                                                                    from Aircraft_Proposal__c
                                                                    where id in :AssetTermIds];
            list<Delivery_Timeline__c> updateDT = new list<Delivery_Timeline__c>();
            for(Aircraft_Proposal__c curAt:assetTermList){
                for(Integer i=1;i<curAt.Delivery_Timelines__r.size();i++){
                    curAt.Delivery_Timelines__r[i].Y_Hidden_Prev_Delivery_Timeline__c = curAt.Delivery_Timelines__r[i-1].Id;
                }
                updateDT.addAll(curAt.Delivery_Timelines__r);
            }
            if(!updateDT.IsEmpty())update updateDT;
        }
        system.debug('updateDurationFuture-');
    }
 
}