public class Timeline
{
	//test
	public string callType    			{get;set;}  
    public string color                 {get;set;}  
    public string eventType             {get;set;}
    public string eventTypeAlias        {get;set;}
	public string eventTypeLong         {get;set;}
	public string legendLabel           {get;set;}
    public string quarter               {get;set;}
    public integer amount               {get;set;}
    public Integer month                {get;set;}
    public Integer year                 {get;set;} 
    public Integer day                  {get;set;} 
    public date forcastedEventDateTo    {get;set;}  
    public date lasteventDate 			{get;set;}  
    
    public date DateofManuf    			{get;set;}     					
    public decimal MonthsToNextEventDate{get;set;}  
    public string AvergeUtilization 	{get;set;} 	
    public string Interval				{get;set;} 
    public date LastUtilizationDate	{get;set;}
   	      
    public Timeline()
    {
    	callType  = 'Default';
        color     = 'Green';    
        eventType = eventTypeAlias = eventTypeLong = legendLabel = '';
        amount    = 0;
        lasteventDate	= system.today();
        DateofManuf	= system.today();
        MonthsToNextEventDate	= 0;
        AvergeUtilization	= '';
        Interval	= '';
        LastUtilizationDate = system.today();
        month     = system.now().month();
        year      = system.now().year();
        day       = system.now().day();
        forcastedEventDateTo   = system.today();
        quarter   = fetchQuarter(system.now().month());
    }

    public static string fetchQuarter( Integer month )
    {
        string quarter = '';          
        if( month == 1  || month == 2  || month == 3  ) return  quarter = 'Q1';   else
        if( month == 4  || month == 5  || month == 6  ) return  quarter = 'Q2';   else
        if( month == 7  || month == 8  || month == 9  ) return  quarter = 'Q3';   else
            return quarter = 'Q4';
    }    
}