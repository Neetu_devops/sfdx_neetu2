public class PurchaseAgreementMilestoneTriggerHandler implements ITrigger{

    private final string triggerBefore = 'PurchaseAgreementMilestoneTriggerHandlerBefore';
    private final string triggerAfter = 'PurchaseAgreementMilestoneTriggerHandlerAfter';
    
    public PurchaseAgreementMilestoneTriggerHandler()
    {
    }
    map<id,Order__c> mapPurchaseAgreement = new map<id, Order__c>();
    map<id,Purchase_Agreement_Milestone__c> mapPurchaseAgreementMilestones = new map<id, Purchase_Agreement_Milestone__c>();
    map<id,Batch__c> mapBatch = new map<id, Batch__c>();
    
   	Set<Id> PAIds = new Set<Id>();
    Set<Id> PAMilestoneIds = new Set<Id>();
    Set<Id> batchIds = new Set<Id>();
    private void setSOQLMAPOrList(){
        list<Purchase_Agreement_Milestone__c> pamilestoneList = (list<Purchase_Agreement_Milestone__c>)trigger.new;
        for(Purchase_Agreement_Milestone__c pamilestone :pamilestoneList){
         	PAIds.add(pamilestone.Purchase_Agreement__c);
            PAMilestoneIds.add(pamilestone.Id);
            
       	}
        system.debug('PAIDs---'+PAIds);
      	mapBatch = new map<Id,Batch__c>([select Id, Name,(select Id,Name,Aircraft_Model__c,Aircraft__r.Aircraft_Type__c,Delivery_Date__c,Rank__c 
                                                          from Aircraft__r) 
                                        from Batch__c
                                        where Purchase_Agreement__c IN :PAIds]);
         system.debug('mapBatch-------'+mapBatch);
      	mapPurchaseAgreement= new map<Id, Order__c>([select Id, Name,
                                                    (select Id,Name from Batches__r),
                                                    (select Id,Name,Applicable_AC_Type__c,PA_Milestone_Type__c from Purchase_Agreement_Milesones__r)
                                    				from Order__c
                                                    where Id IN :PAIds]);
       
      
      	system.debug('mapPurchaseAgreement-------'+mapPurchaseAgreement);
        system.debug('mapBatch-------'+mapBatch);
        
        
    }
   
    public void beforeInsert()
    {
        system.debug('PurchaseAgreementMilestoneTriggerHandler Before Inset');
        setSOQLMAPOrList();
        validatePAMilestoneType();
        
    }
    public void afterInsert()
    {
        system.debug('PurchaseAgreementMilestoneTriggerHandler After Insert');
        list<Purchase_Agreement_Milestone__c> currentPAMilestoneList = (list<Purchase_Agreement_Milestone__c>)trigger.new;
       	setSOQLMAPOrList();
        system.debug('After PA Milestones are inserted, insert delivery milestones for the deliveries');
        
        List<Pre_Delivery_Payment__c> deliveriesList = new List<Pre_Delivery_Payment__c>();
        system.debug('mapBatch---'+mapBatch);
        for(Id id :mapBatch.keySet()){
            system.debug('Batch Id------'+mapBatch.get(id));
            Batch__c batch = mapBatch.get(id);
            system.debug('Deliveries--'+ batch.Aircraft__r);
            deliveriesList = batch.Aircraft__r;
         }
        system.debug('deliveriesList.size----'+ deliveriesList.size());
        if(deliveriesList.size()>0) createDeliveryMilestoneFromPAMilestone(currentPAMilestoneList,deliveriesList);
        
    }
    public void beforeUpdate()
    { 
        setSOQLMAPOrList();
        system.debug('PurchaseAgreementMilestoneTriggerHandler beforeUpdate.....');
        list<Purchase_Agreement_Milestone__c> currentPAMilestoneList = (list<Purchase_Agreement_Milestone__c>)trigger.new;
        for(Purchase_Agreement_Milestone__c pamilestone : currentPAMilestoneList){
        Purchase_Agreement_Milestone__c oldpamilestone = (Purchase_Agreement_Milestone__c)Trigger.oldMap.get(pamilestone.Id);
            if(oldpamilestone.PA_Milestone_Type__c != pamilestone.PA_Milestone_Type__c){
                validatePAMilestoneType();
              }
        }
    }
    public void afterUpdate()
    {
    }
    public void beforeDelete()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('PurchaseAgreementMilestoneTriggerHandler beforeDelete.....');
        list<Purchase_Agreement_Milestone__c> currentPAMilestoneList = (list<Purchase_Agreement_Milestone__c>)trigger.old;
        system.debug('currentPAMilestoneList Size To Delete----'+currentPAMilestoneList.size());
       
        for(Purchase_Agreement_Milestone__c pamilestone :currentPAMilestoneList){
         	PAMilestoneIds.add(pamilestone.Id);
       	}
        mapPurchaseAgreementMilestones = new map<Id, Purchase_Agreement_Milestone__c>
           							    ([select Id,Name,
                                        (select Id,Name,Due_Date_Customized__c from Delivery_Milestones__r) 
                                         from Purchase_Agreement_Milestone__c 
        								 where id IN :PAMilestoneIds]);
        deleteDeliveryMilestone(currentPAMilestoneList);
      
    }
    public void afterDelete()
    {
      
    }
    public void bulkBefore()
    {
   	}
    public void bulkAfter()
    {
    }
    public void afterUnDelete()
    {
    }
    public void andFinally()
    {
	}
    
    private void deleteDeliveryMilestone(List<Purchase_Agreement_Milestone__c> currentPAMilestoneList){
        
        List<Delivery_Milestone__c> deliveryMilestoneListToDelete = new List<Delivery_Milestone__c>();
         List<Delivery_Milestone__c> deliveryMilestoneListToUpdate = new List<Delivery_Milestone__c>();
        for(Purchase_Agreement_Milestone__c curPAMilestone :currentPAMilestoneList){
            //Get delivery milestone with pa milestone id
            List<Delivery_Milestone__c> deliveryMilestoneListWithPAMilestoneId =  mapPurchaseAgreementMilestones.get(curPAMilestone.Id).Delivery_Milestones__r;
            for(Delivery_Milestone__c deliveryMilestone:deliveryMilestoneListWithPAMilestoneId){
                
                if(deliveryMilestone.Due_Date_Customized__c){ 
                    deliveryMilestone.Purchase_Agreement_Milestone_ID__c = null;
                    deliveryMilestoneListToUpdate.add(deliveryMilestone);
                }
                else deliveryMilestoneListToDelete.add(deliveryMilestone);
                    
            }
      	}
        system.debug('deliveryMilestoneListToDelete----'+deliveryMilestoneListToDelete.size());
        system.debug('deliveryMilestoneListToUpdate----'+deliveryMilestoneListToUpdate.size());
        if(deliveryMilestoneListToDelete.size()>0) delete deliveryMilestoneListToDelete;
        if(deliveryMilestoneListToUpdate.size()>0) update deliveryMilestoneListToUpdate;
        
    }
    private void createDeliveryMilestoneFromPAMilestone(List<Purchase_Agreement_Milestone__c> currentPAMilestoneList,List<Pre_Delivery_Payment__c> deliveriesList){
        //Get the deliveries for the purchase agreement and then put the condition
        List<Delivery_Milestone__c> deliveryMilestoneList = new List<Delivery_Milestone__c>();
       
        	for(Pre_Delivery_Payment__c delivery:deliveriesList){
              for(Purchase_Agreement_Milestone__c  pa: currentPAMilestoneList){
                system.debug('curPAMilestone.Applicable_AC_Type__c----'+pa.Applicable_AC_Type__c);
                system.debug('delivery.Aircraft_Model__c----'+delivery.Aircraft_Model__c);
               
                String[] applicableACType =pa.Applicable_AC_Type__c==null? new String[]{}: pa.Applicable_AC_Type__c.split(';');
                 
                 if(applicableACType.size()==0){
                    	system.debug('Create Delivery Milestone from pa milestone----'+pa.Name+ ' for Applicability type empty ');
                    	Delivery_Milestone__c deliveryMilestone = new Delivery_Milestone__c();
                		deliveryMilestone.Purchase_Agreement_Milestone_ID__c=pa.Id;
                		deliveryMilestone.Delivery__c=delivery.Id;
                		deliveryMilestone.Asset__c= delivery.Aircraft__c;
                		deliveryMilestoneList.add(deliveryMilestone);
                }else{
                	for(String s : applicableACType){
                   		if(s.equals(delivery.Aircraft_Model__c)){
                    		system.debug('Create Delivery Milestone from pa milestone----'+pa.Name + 'for Aircraft type--'+delivery.Aircraft_Model__c);
                    		Delivery_Milestone__c deliveryMilestone = new Delivery_Milestone__c();
                			deliveryMilestone.Purchase_Agreement_Milestone_ID__c=pa.Id;
                			deliveryMilestone.Delivery__c=delivery.Id;
                			deliveryMilestone.Asset__c= delivery.Aircraft__c;
                			deliveryMilestoneList.add(deliveryMilestone);
                 		}
                	}
                }
                }
            
            
        }
        system.debug('Delivery MilestoneList------'+ deliveryMilestoneList.size());
        if(deliveryMilestoneList.size()>0) insert deliveryMilestoneList;
        
    }
    
    private void validatePAMilestoneType(){
       
        list<Purchase_Agreement_Milestone__c> currentPAMilestoneList = (list<Purchase_Agreement_Milestone__c>)trigger.new;
        for(Purchase_Agreement_Milestone__c curPAMilestone :currentPAMilestoneList){
            curPAMilestone.Y_Hidden_Applicability_Type__c = curPAMilestone.Applicable_AC_Type__c;
            list<Purchase_Agreement_Milestone__c> existingMilestonesForPA = mapPurchaseAgreement.get(curPAMilestone.Purchase_Agreement__c).Purchase_Agreement_Milesones__r;
            system.debug('existingMilestones----'+existingMilestonesForPA.size());
            if(existingMilestonesForPA.size()>0){
            	for(Purchase_Agreement_Milestone__c milest : existingMilestonesForPA){
                    system.debug('milest----'+ milest);
                    
                    if(milest.Applicable_AC_Type__c == null || curPAMilestone.Applicable_AC_Type__c == null){
                        if(milest.PA_Milestone_Type__c == curPAMilestone.PA_Milestone_Type__c){
                              curPAMilestone.addError('PA Milestone Type - '+curPAMilestone.PA_Milestone_Type__c+' cannot be saved as this Milestone with the same type already exists at this Purchase Agreement. Specify new type');
                	    	  break ;
                        }
                    }
                	if(curPAMilestone.PA_Milestone_Type__c !=null && (curPAMilestone.PA_Milestone_Type__c+curPAMilestone.Applicable_AC_Type__c).equals(milest.PA_Milestone_Type__c+milest.Applicable_AC_Type__c)){
                    //Add Validation Error
                    curPAMilestone.PA_Milestone_Type__c.addError('PA Milestone Type - '+curPAMilestone.PA_Milestone_Type__c+' cannot be saved as this Milestone with the '+ milest.Applicable_AC_Type__c+ '  type  already exists at this Purchase Agreement. Specify new type or leave blank');
                	}
            	}
            }
           // Date currentDate = system.today();
           // curPAMilestone.Name = LeasewareUtils.blankIfNull(LeasewareUtils.getDatetoString(curPAMilestone.Milestone_Date__c,'DD MON YYYY'))+'-'+leasewareUtils.blankIfNull(curPAMilestone.Applicable_AC_Type__c);
        }
    }
}