public class PermanentReplacementController {
    
    public class LeaseWrapper {
        @AuraEnabled
        public Id leaseId {get;set;}
        @AuraEnabled
        public Id assetId {get;set;}
        @AuraEnabled
        public Id newAssetId {get;set;}
        @AuraEnabled
        public String leaseName {get;set;}
        @AuraEnabled
        public String assetName {get;set;}
        @AuraEnabled
        public List<String> approvalStatus {get;set;}
        @AuraEnabled
        public String selectedApprovalStatus {get; set;}
        @AuraEnabled
        public Decimal prevTSN {get;set;}
        @AuraEnabled
        public Decimal newTSN {get;set;}
        @AuraEnabled
        public Decimal prevCSN {get;set;}
        @AuraEnabled
        public Decimal newCSN {get;set;}
        @AuraEnabled
        public List<Constituent_Assembly__c> prevAssembly {get;set;}
        @AuraEnabled
        public List<Constituent_Assembly__c> newAssembly {get;set;}
        @AuraEnabled
        public String assemblyType {get; set;}
    }
    
    @AuraEnabled
    public static LeaseWrapper getLeaseData(Id recordId) {
        LeaseWrapper leaseData = new LeaseWrapper();
        
        Lease__c lease = [SELECT Id, Name, Aircraft__c, Aircraft__r.Name FROM Lease__c WHERE ID = :recordId];
        
        leaseData.leaseId = lease.Id;
        leaseData.assetId = lease.Aircraft__c;
        leaseData.leaseName = lease.Name;
        leaseData.assetName = lease.Aircraft__r.Name;
        List<String> approvalStatus = new List<String>{'Approved','Not Required'};
            leaseData.approvalStatus = approvalStatus;
        
        return leaseData;
    }
    
    @AuraEnabled
    public static LeaseWrapper getTsnCsnValues(String leaseData, Id prevAssemblyId, Id newAssemblyId, Date userSelectedDate) {
        
        LeaseWrapper leaseWrapperData = new LeaseWrapper();
        leaseWrapperData = (LeaseWrapper)System.JSON.deserialize(leaseData, LeaseWrapper.class);
        
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
        Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        List<Constituent_Assembly__c> assemblies = [SELECT ID, Asset__c, TSN__c, CSN__c, RecordTypeId
                                                    FROM Constituent_Assembly__c 
                                                    WHERE ID = :prevAssemblyId OR ID = :newAssemblyId];
        
        if(assemblies.size() > 0) {
            
            Decimal oldPrevTSN = null;
            Decimal oldPrevCSN = null;
            Decimal oldNewTSN = null;
            Decimal oldNewCSN = null;
            
            for(Constituent_Assembly__c assembly : assemblies) {
                if(String.isNotBlank(prevAssemblyId) && assembly.Id == prevAssemblyId) {
                    oldPrevTSN = assembly.TSN__c;
                    oldPrevCSN = assembly.CSN__c;
                    leaseWrapperData.assemblyType = assembly.RecordTypeId;
                }
                if(String.isNotBlank(newAssemblyId) && assembly.Id == newAssemblyId) {
                    oldNewTSN = assembly.TSN__c;
                    oldNewCSN = assembly.CSN__c;
                    leaseWrapperData.newAssetId = assembly.Asset__c;
                    leaseWrapperData.assemblyType = assembly.RecordTypeId;
                }
            }
            
            
            List<Assembly_Utilization__c> assemblyUtilizations = [SELECT For_The_Month_Ending__c, Constituent_Assembly__c, 
                                                                          TSN_At_Month_Start__c, TSN_At_Month_End__c, 
                                                                          CSN_At_Month_Start__c, CSN_At_Month_End__c
                                                                          FROM Assembly_Utilization__c 
                                                                          WHERE ((Utilization_Report__r.Aircraft__c = :leaseWrapperData.assetId
                                                                          AND Constituent_Assembly__c = :prevAssemblyId) 
                                                                          OR (Utilization_Report__r.Aircraft__c = :leaseWrapperData.newAssetId
                                                                          AND Constituent_Assembly__c = :newAssemblyId))
                                                                          AND (For_The_Month_Ending__c = :lastDayOfMonth
                                                                          OR For_The_Month_Ending__c = :lastDayOfPreviousMonth)
                                                                          AND Utilization_Report__r.Type__c = 'Actual' order by End_Date_F__c desc];
            
            Boolean isCurrMonthForPrev = false;
            Boolean isCurrMonthForNew = false;
            
            if(assemblyUtilizations.size() > 0) {
                
                for(Assembly_Utilization__c assemblyUtilization : assemblyUtilizations) {
                    
                    if(assemblyUtilization.Constituent_Assembly__c == prevAssemblyId) {
                        if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfMonth && isCurrMonthForPrev == false) {
                            leaseWrapperData.prevTSN = assemblyUtilization.TSN_At_Month_End__c; 
                            leaseWrapperData.prevCSN = assemblyUtilization.CSN_At_Month_End__c;
                            isCurrMonthForPrev = true;
                        }
                        if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfPreviousMonth && isCurrMonthForPrev == false) {
                            leaseWrapperData.prevTSN = assemblyUtilization.TSN_At_Month_End__c; 
                            leaseWrapperData.prevCSN = assemblyUtilization.CSN_At_Month_End__c;
                        }
                    }
                    if(assemblyUtilization.Constituent_Assembly__c == newAssemblyId) {
                        if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfMonth && isCurrMonthForNew == false) {
                            leaseWrapperData.newTSN = assemblyUtilization.TSN_At_Month_End__c; 
                            leaseWrapperData.newCSN = assemblyUtilization.CSN_At_Month_End__c;
                            isCurrMonthForNew = true;
                        }
                        if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfPreviousMonth && isCurrMonthForNew == false) {
                            leaseWrapperData.newTSN = assemblyUtilization.TSN_At_Month_End__c; 
                            leaseWrapperData.newCSN = assemblyUtilization.CSN_At_Month_End__c;
                        }
                    }
                }
            } else {
                
                leaseWrapperData.prevTSN = oldPrevTSN;
                leaseWrapperData.prevCSN = oldPrevCSN;
                leaseWrapperData.newTSN = oldNewTSN;
                leaseWrapperData.newCSN = oldNewCSN;
                
            }
        }
        return leaseWrapperData;
    }
    
    @AuraEnabled
    public static Id saveData(String leaseData, Id prevAssemblyId, Id newAssemblyId, String prevAssemblyName, String newAssemblyName, Date userSelectedDate) {
        
        LeaseWrapper wrapperData = new LeaseWrapper();
        wrapperData = (LeaseWrapper)System.JSON.deserialize(leaseData, LeaseWrapper.class);
        
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
        Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        List<Assembly_Utilization__c> assemblyUtilizations = [SELECT Constituent_Assembly__c, TSN_At_Month_Start__c, TSN_At_Month_End__c, 
                                                                      CSN_At_Month_Start__c, CSN_At_Month_End__c
                                                                      FROM Assembly_Utilization__c 
                                                                      WHERE ((Utilization_Report__r.Aircraft__c = :wrapperData.assetId
                                                                      AND Constituent_Assembly__c = :prevAssemblyId) 
                                                                      OR (Utilization_Report__r.Aircraft__c = :wrapperData.newAssetId
                                                                      AND Constituent_Assembly__c = :newAssemblyId))
                                                                      AND (For_The_Month_Ending__c = :lastDayOfMonth
                                                                      OR For_The_Month_Ending__c = :lastDayOfPreviousMonth)
                                                                      AND Utilization_Report__r.Type__c = 'Actual' order by End_Date_F__c desc];
        
        Boolean isPrevTSNValid = false;
        Boolean isPrevCSNValid = false;
        Boolean isNewTSNValid = false;
        Boolean isNewCSNValid = false;
        
        if(assemblyUtilizations.size() > 0) {
            
            for(Assembly_Utilization__c assemblyUtilization : assemblyUtilizations) {
                
                if(assemblyUtilization.Constituent_Assembly__c == prevAssemblyId) {
                    if(wrapperData.prevTSN <= assemblyUtilization.TSN_At_Month_End__c 
                       && wrapperData.prevTSN >= assemblyUtilization.TSN_At_Month_Start__c) {
                           isPrevTSNValid = true;
                       }
                    if(wrapperData.prevCSN <= assemblyUtilization.CSN_At_Month_End__c
                       && wrapperData.prevCSN >= assemblyUtilization.CSN_At_Month_Start__c) {
                           isPrevCSNValid = true;
                       }
                }
                if(assemblyUtilization.Constituent_Assembly__c == newAssemblyId) {
                    
                    if(wrapperData.newTSN <= assemblyUtilization.TSN_At_Month_End__c 
                       && wrapperData.newTSN >= assemblyUtilization.TSN_At_Month_Start__c) {
                           isNewTSNValid = true;
                       }
                    if(wrapperData.newCSN <= assemblyUtilization.CSN_At_Month_End__c
                       && wrapperData.newCSN >= assemblyUtilization.CSN_At_Month_Start__c) {
                           isNewCSNValid = true;
                       }
                }
            }
            
        } else {
            
            List<Constituent_Assembly__c> assemblies = [SELECT ID, TSN__c, CSN__c
                                                        FROM Constituent_Assembly__c 
                                                        WHERE ID = :prevAssemblyId OR ID = :newAssemblyId];
            
            if(assemblies.size() > 0) {
                for(Constituent_Assembly__c assembly : assemblies) {
                    if(String.isNotBlank(prevAssemblyId) && assembly.Id == prevAssemblyId) {
                        if(wrapperData.prevTSN == assembly.TSN__c) {
                            isPrevTSNValid = true;
                        }
                        if(wrapperData.prevCSN == assembly.CSN__c) {
                            isPrevCSNValid = true;
                        }
                    }
                    if(String.isNotBlank(newAssemblyId) && assembly.Id == newAssemblyId) {
                        if(wrapperData.newTSN == assembly.TSN__c) {
                            isNewTSNValid = true;
                        }
                        if(wrapperData.newCSN == assembly.CSN__c) {
                            isNewCSNValid = true;
                        }
                    }
                }
            }
        }
        
        
        if(!isPrevTSNValid) {
            throw new AuraHandledException('Reported utilization for '+prevAssemblyName+' do not match Previous Assembly Permanent Replacement TSN');  
        }
        if(!isPrevCSNValid) {
            throw new AuraHandledException('Reported utilization for '+prevAssemblyName+' do not match Previous Assembly Permanent Replacement CSN');  
        }
        if(!isNewTSNValid) {
            throw new AuraHandledException('Reported utilization for '+newAssemblyName+' do not match New Assembly Permanent Replacement TSN');  
        }
        if(!isNewCSNValid) {
            throw new AuraHandledException('Reported utilization for '+newAssemblyName+' do not match New Assembly Permanent Replacement CSN');  
        }
        
        AssemblyTriggerHandler CAInst = new AssemblyTriggerHandler();
        
        CAInst.permanentReplacement(wrapperData.leaseId,wrapperData.assetId,userSelectedDate,
                                    wrapperData.selectedApprovalStatus,prevAssemblyId,newAssemblyId,
                                    wrapperData.prevTSN,wrapperData.prevCSN,wrapperData.newTSN,wrapperData.newCSN);
        return wrapperData.leaseId;
    }
    
    @AuraEnabled
    public static LeaseWrapper getTsnCsnValuesOnDateChange(String leaseData, Id prevAssemblyId, Id newAssemblyId, Date userSelectedDate) {
        LeaseWrapper wrapperData = new LeaseWrapper();
        wrapperData = (LeaseWrapper)System.JSON.deserialize(leaseData, LeaseWrapper.class);
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
		Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        Decimal oldPrevTSN = wrapperData.prevTSN;
        Decimal oldPrevCSN = wrapperData.prevCSN;
        Decimal oldNewTSN = wrapperData.newTSN;
        Decimal oldNewCSN = wrapperData.newCSN;
        
        Boolean isCurrMonthForPrev = false;
        Boolean isCurrMonthForNew = false;
        
        wrapperData.prevTSN = null;
        wrapperData.prevCSN = null;
        wrapperData.newTSN = null;
        wrapperData.newCSN = null;
            
        for(Assembly_Utilization__c assemblyUtilization : [SELECT For_The_Month_Ending__c, Constituent_Assembly__c, 
                                                 TSN_At_Month_Start__c, TSN_At_Month_End__c, 
                                                 CSN_At_Month_Start__c, CSN_At_Month_End__c
                                                 FROM Assembly_Utilization__c 
                                                 WHERE ((Utilization_Report__r.Aircraft__c = :wrapperData.assetId
                                                 AND Constituent_Assembly__c = :prevAssemblyId) 
                                                 OR (Utilization_Report__r.Aircraft__c = :wrapperData.newAssetId
                                                 AND Constituent_Assembly__c = :newAssemblyId))
                                                 AND (For_The_Month_Ending__c = :lastDayOfMonth
                                                 OR For_The_Month_Ending__c = :lastDayOfPreviousMonth)
                                                 AND Utilization_Report__r.Type__c = 'Actual' order by End_Date_F__c desc]) 
        {
            
            if(assemblyUtilization.Constituent_Assembly__c == prevAssemblyId) {
                if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfMonth && isCurrMonthForPrev == false) {
                    wrapperData.prevTSN = assemblyUtilization.TSN_At_Month_End__c; 
                    wrapperData.prevCSN = assemblyUtilization.CSN_At_Month_End__c;
                    isCurrMonthForPrev = true;
                }
                if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfPreviousMonth && isCurrMonthForPrev == false) {
                    wrapperData.prevTSN = assemblyUtilization.TSN_At_Month_End__c; 
                    wrapperData.prevCSN = assemblyUtilization.CSN_At_Month_End__c;
                }
            }
            if(assemblyUtilization.Constituent_Assembly__c == newAssemblyId) {
                if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfMonth && isCurrMonthForNew == false) {
                    wrapperData.newTSN = assemblyUtilization.TSN_At_Month_End__c; 
                    wrapperData.newCSN = assemblyUtilization.CSN_At_Month_End__c;
                    isCurrMonthForNew = true;
                }
                if(assemblyUtilization.For_The_Month_Ending__c == lastDayOfPreviousMonth && isCurrMonthForNew == false) {
                    wrapperData.newTSN = assemblyUtilization.TSN_At_Month_End__c; 
                    wrapperData.newCSN = assemblyUtilization.CSN_At_Month_End__c;
                }
            }
            
        }
        if(wrapperData.prevTSN == null || wrapperData.prevCSN == null || wrapperData.newTSN == null || wrapperData.newCSN == null) {
            wrapperData.prevTSN = oldPrevTSN;
            wrapperData.prevCSN = oldPrevCSN;
            wrapperData.newTSN = oldNewTSN;
            wrapperData.newCSN = oldNewCSN;
        }
        
        return wrapperData;
    }
}