public class AircraftNeedHandler implements ITrigger
{


    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'AircraftNeedHandlerBefore';
    private final string triggerAfter = 'AircraftNeedHandlerAfter';

    private list<Consolidated_Intel__c> listATMIs = new list<Consolidated_Intel__c>();
    private set<Id> setMIToDel = new set<Id>();
    private map<Id, Consolidated_Intel__c> mapATMIs = new map<Id, Consolidated_Intel__c>();


    public AircraftNeedHandler()
    {
    }
 

    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    	if(trigger.isUpdate){
    		list<Consolidated_Intel__c> listExATMIs = [select id, Name, Aircraft_Type__c, Aircraft_Need__c 
    			from Consolidated_Intel__c where Aircraft_Need__c in 
    			:(list<Aircraft_Need__c>)trigger.new];
    		for(Consolidated_Intel__c curATMI : listExATMIs){
    			mapATMIs.put(curATMI.Aircraft_Need__c, curATMI);
    		}	
    	}
    }
         
    public void beforeInsert()
    {
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('AircraftNeedHandler.beforeInsert(+)');

	    system.debug('AircraftNeedHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
		
    	system.debug('AircraftNeedHandler.beforeUpdate(+)');
  	
    	system.debug('AircraftNeedHandler.beforeUpdate(-)');
    }
     

    public void beforeDelete()
    {  

    	system.debug('AircraftNeedHandler.beforeDelete(+)');
		CRUDAircraftTypeMarketIntel();
    	system.debug('AircraftNeedHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftNeedHandler.afterInsert(+)');
    	generateMatching();
    	CRUDAircraftTypeMarketIntel();
    	system.debug('AircraftNeedHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftNeedHandler.afterUpdate(+)');
    	generateMatching();
    	CRUDAircraftTypeMarketIntel();
    	system.debug('AircraftNeedHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	system.debug('AircraftNeedHandler.afterDelete(+)');
    	
    	
    	system.debug('AircraftNeedHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	system.debug('AircraftNeedHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('AircraftNeedHandler.afterUnDelete(-)');     	
    }
     

    public void andFinally()
    {
        // insert any audit records

    }

	private void generateMatching(){

    	String SelectClauseforAC = ' select Id, MSN_Number__c, Lease__r.Lease_End_Date_New__c, Status__c   '
			+ '  from Aircraft__c ' 
			+ '   ';

		String WhereClauseforAC = '';

    	String SelectClauseforAIT = ' select Id, Status__c ,Transaction__r.Transaction_Type__c,Name  '
			+ '  from Aircraft_In_Transaction__c ' 
			+ '   ';

		String WhereClauseforAIT = '';
		
		List<Matching_Aircraft__c> listMatches = new List<Matching_Aircraft__c>();		
		
    	Aircraft_Need__c[] listNew = (list<Aircraft_Need__c>)trigger.new ;
    	Date tempDateFrom,tempDateTo ;
    	for(Aircraft_Need__c curRec:listNew){
    		listMatches.clear();
    		if(curRec.Search_Lease_Expirations__c){
    			delete [select Id from Matching_Aircraft__c where Aircraft_Need__c=:curRec.Id];
	    		WhereClauseforAC = ' where ';
	    		// 1. Aircraft_Type__c = :curAcNeed.Aircraft_Type__c
				WhereClauseforAC = WhereClauseforAC + ' Aircraft_Type__c = \''+ curRec.Aircraft_Type__c + '\' ';
	
				// 2. AND (Lease__r.Lease_End_Date__c <= :curAcNeed.Date_Needed__c OR Status__c='Available') 
				WhereClauseforAC = WhereClauseforAC + ' AND Status__c not in (\'Sold\') ';
				if(curRec.Lease_Start_Date__c != null){
					tempDateFrom = curRec.Lease_Start_Date__c;
					WhereClauseforAC = WhereClauseforAC + ' AND Lease__r.Lease_End_Date_New__c >= :tempDateFrom   ';
				}
				if(curRec.Date_Needed__c != null){
					tempDateTo = curRec.Date_Needed__c;
					WhereClauseforAC = WhereClauseforAC + ' AND Lease__r.Lease_End_Date_New__c <= :tempDateTo   ';
				}				
				// 3. AND (Aircraft_Variant__c=null OR Aircraft_Variant__c LIKE :(curAcNeed.Variant__c==null?'%':curAcNeed.Variant__c))
				WhereClauseforAC = WhereClauseforAC + 'AND ( Aircraft_Variant__c = null ';
				if(curRec.Variant__c !=null) WhereClauseforAC = WhereClauseforAC + ' OR Aircraft_Variant__c like \''+ curRec.Variant__c + '\' ';
				else WhereClauseforAC = WhereClauseforAC + ' OR Aircraft_Variant__c like \'%\' ';
				// clsoing bracket
				WhereClauseforAC = WhereClauseforAC + ' ) ' ;
				// 4. AND Engine_Type__c like :tempEngineType
				if(curRec.Engine_Type_List__c != null) WhereClauseforAC = WhereClauseforAC + ' AND Engine_Type__c like \''+ curRec.Engine_Type_List__c + '%\' ';
				
				// 5. AND Vintage + Vintage Range
				Integer vintageFrom,vintageTo,range;
				if(curRec.Vintage_From__c!=null){
					vintageFrom = Integer.valueOf(curRec.Vintage_From__c);
					if(curRec.Vintage_Range__c!=null){
						range = Integer.valueOf(curRec.Vintage_Range__c);
						vintageTo = vintageFrom + range;
						vintageFrom = vintageFrom - range;
						WhereClauseforAC = WhereClauseforAC + ' AND CALENDAR_YEAR(Date_of_Manufacture__c) >= :vintageFrom AND CALENDAR_YEAR(Date_of_Manufacture__c) <= :vintageTo '	;
					}
					else{
						WhereClauseforAC = WhereClauseforAC + ' AND CALENDAR_YEAR(Date_of_Manufacture__c) = :vintageFrom  '	;
					}
					
				}
				// 6. AND (Id in ( select Attached_Aircraft__c from Constituent_Assembly__c where Manufacturer_List__c = :curAcNeed.Engine_Mfr_List__c) )
				if(curRec.Engine_Variant__c != null){
					WhereClauseforAC = WhereClauseforAC + ' AND (Id in ( select Attached_Aircraft__c from Constituent_Assembly__c where Engine_Thrust__c like \''+ curRec.Engine_Variant__c + '%\' )) ';
				}
				system.debug('SelectClauseforAC='+ SelectClauseforAC);
				system.debug('WhereClauseforAC='+ WhereClauseforAC);
				
				List<Aircraft__c> availableACs = Database.query(SelectClauseforAC + WhereClauseforAC);
			
				for(Aircraft__c matchAC:availableACs){
					try{
						date dateAvlbl = matchAC.Lease__r.Lease_End_Date_New__c==null?date.today():matchAC.Lease__r.Lease_End_Date_New__c.addDays(1); 
						listMatches.add(new Matching_Aircraft__c(Aircraft_Need__c=curRec.Id, Aircraft__c=matchAC.Id, 
								Name='MSN ' + matchAC.MSN_Number__c + ' Available '+dateAvlbl.format(), 
								Available_On__c=dateAvlbl));
					}catch(exception e){
						//Skip the problematic match and proceed.
						continue;
					}
				}
				insert listMatches;			
    		} 
			else{
				delete [select Id from Matching_Aircraft__c where Aircraft_Need__c=:curRec.Id];
			}    		



			// Update count using future method
    		//LeaseWareUtils.updateMatchCount(curRec.Id, listMatches.size() +listAITMatches.size() );
    	}// end of for loop	
		
		
	}


	private void CRUDAircraftTypeMarketIntel(){
		list<Aircraft_Need__c> cuMIList = (list<Aircraft_Need__c>)(trigger.isDelete?trigger.old:trigger.new);
		
		try{
	
			Aircraft_Need__c oldMapMI;
			
			for(Aircraft_Need__c curMI : cuMIList){
					
				if(trigger.isInsert){

					if(curMI.Aircraft_Type__c==null)continue;
					Id ACTypeId;
					try{
						ACTypeId = [select id from Aircraft_Type__c where Aircraft_Type__c = :curMI.Aircraft_Type__c 
							AND Aircraft_Variant__c = null AND Engine_Type__c = null AND Engine_Variant__c =null order by 
							CreatedDate desc limit 1].id;
					}catch(exception e){
						Aircraft_Type__c newACType = new Aircraft_Type__c(Name=curMI.Aircraft_Type__c, 
									Aircraft_Type__c = curMI.Aircraft_Type__c);
						insert newACType;
						ACTypeId = newACType.id;
					}
					listATMIs.add(new Consolidated_Intel__c(Name='Dummy', Aircraft_Type__c=ACTypeId, 
								Aircraft_Need__c=curMI.Id));

				}else if(trigger.isUpdate){
					oldMapMI = (Aircraft_Need__c)trigger.oldMap.get(curMI.Id);
					
					if(oldMapMI.Aircraft_Type__c != null && curMI.Aircraft_Type__c == null)
						setMIToDel.add(oldMapMI.Id);
					if(curMI.Aircraft_Type__c==null || oldMapMI.Aircraft_Type__c == curMI.Aircraft_Type__c)continue;

					Id ACTypeId;
					try{
						ACTypeId = [select id from Aircraft_Type__c where Aircraft_Type__c = :curMI.Aircraft_Type__c 
							AND Aircraft_Variant__c = null AND Engine_Type__c = null AND Engine_Variant__c =null order by 
							CreatedDate desc limit 1].id;
					}catch(exception e){
						Aircraft_Type__c newACType = new Aircraft_Type__c(Name=curMI.Aircraft_Type__c, 
									Aircraft_Type__c = curMI.Aircraft_Type__c);
						insert newACType;
						ACTypeId = newACType.id;
					}

					Consolidated_Intel__c exATMI = mapATMIs.get(curMI.Id);
					if(exATMI == null){
						listATMIs.add(new Consolidated_Intel__c(Name='Dummy', Aircraft_Type__c=ACTypeId, 
								Aircraft_Need__c=curMI.Id));
					}else{
						exATMI.Aircraft_Type__c=ACTypeId;
						exATMI.Aircraft_Need__c=curMI.Id;
						listATMIs.add(exATMI);
					}
					
				}else{ //Delete
					if(curMI.Aircraft_Type__c!=null)setMIToDel.add(curMI.Id);
				}
			}
			if(setMIToDel.size()>0)delete [select id from Consolidated_Intel__c where Aircraft_Need__c in :setMIToDel];
			if(listATMIs.size()>0)upsert listATMIs;
	
		}catch(exception e){
			system.debug('CRUD on AircraftTypeMarketIntel failed due to exception '+ e.getMessage() + '\n' + e.getStackTraceString());
		}		
	}


}