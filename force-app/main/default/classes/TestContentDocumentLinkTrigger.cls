@isTest
public class TestContentDocumentLinkTrigger {
	
  
    @isTest
    private static void testFileSharings1(){
            
        //To generate the excpetion.
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;  
    }
    
    @isTest
    private static void testFileSharings2(){
        
     	Account accObj = new Account (Name = 'Test');
        insert accObj;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
    }

    @IsTest
    private static void testLatestFileUploadOnAsset(){
        LW_Setup__c lwSetup = new LW_Setup__c();
		lwSetup.Name = 'UPDATE_LATEST_FILE_FIELDS';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'ON';
		insert lwSetup;


        Aircraft__c aircraft = new Aircraft__c(
            Name = 'TestAircraft',
            MSN_Number__c = '0J54-E190-80972',
            Aircraft_Type__c = '175',
            TSN__c = 100,
             
            CSN__c = 100
            );
        LeasewareUtils.clearFromTrigger();
        insert aircraft;
        Aircraft__c ac = [select Id from Aircraft__c limit 1];
        String yourFilesContent = 'TheBlogReaders.com File upload content';
 
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = 'testing.txt'; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = 'Testing Files'; // Display name of the files
        conVer.VersionData = Blob.valueOf(yourFilesContent); // converting your binary string to Blog
        conVer.FirstPublishLocationId =  ac.Id; 
        system.debug('conVer--------'+ conver);    
        insert conVer;    //Insert ContentVersion
        
        
      
        Aircraft__c updatedAC = [select Id, Latest_File_Updated_Date__c,Latest_File_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Aircraft__c where ID = :ac.Id];
        ContentDocumentLink cdLink = [select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId = : ac.id];
        system.debug('cdLink---'+ cdLink);
        system.debug('updatedAC------'+ updatedAC.Latest_File_Updated_Date__c);
        system.debug('updatedAC------'+ updatedAC.Latest_File_Updated_By__c);
        system.debug('updatedAC------'+ updatedAC.Y_Hidden_Latest_File_Doc_Id__c);
        system.assertEquals(system.today(),updatedAC.Latest_File_Updated_Date__c);
        system.assertEquals(cdLink.ContentDocumentId+'-'+'Testing Files',updatedAC.Y_Hidden_Latest_File_Doc_Id__c);

        ContentVersion conVer1 = new ContentVersion();
        conVer1.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer1.PathOnClient = 'testingNew.txt'; // The files name, extension is very important here which will help the file in preview.
        conVer1.Title = 'Testing New Files'; // Display name of the files
        conVer1.VersionData = Blob.valueOf(yourFilesContent); // converting your binary string to Blog
        conVer1.FirstPublishLocationId =  updatedAC.Id;   
        system.debug('conVer1---------'+ conver1); 
        LeasewareUtils.clearFromTrigger();
        insert conVer1;
        //Insert ContentVersion
        system.debug('Asset shld be updated------');
        List<ContentDocumentLink> cdLink1 = [select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId = : ac.id order by Id asc ];
       
        LeasewareUtils.clearFromTrigger();
        Aircraft__c updatedAC1= [select Id, Latest_File_Updated_Date__c,Latest_File_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Aircraft__c where ID = :ac.Id];
        system.debug('cdLink---'+ cdLink1);
        system.debug('updatedAC1------'+ updatedAC1.Latest_File_Updated_Date__c);
        system.debug('updatedAC1------'+ updatedAC1.Latest_File_Updated_By__c);
        system.debug('updatedAC1------'+ updatedAC1.Y_Hidden_Latest_File_Doc_Id__c);
        system.assertEquals(system.today(),updatedAC1.Latest_File_Updated_Date__c);
        system.assertEquals(cdLink1[1].ContentDocumentId+'-'+'Testing New Files',updatedAC1.Y_Hidden_Latest_File_Doc_Id__c);


        ContentDocument cd= [select Id from ContentDocument where Id =: cdLink1[1].ContentDocumentId];
        delete cd;
        Aircraft__c updatedAC2 = [select Id, Latest_File_Updated_Date__c,Latest_File_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Aircraft__c where ID = :ac.Id];
        ContentDocument cd1 = [select Id from ContentDocument ];
        
        system.assertEquals(system.today(),updatedAC2.Latest_File_Updated_Date__c);
        system.assertEquals(cd1.Id+'-'+'Testing Files',updatedAC2.Y_Hidden_Latest_File_Doc_Id__c);

        delete cd1;
        Aircraft__c updatedAC3 = [select Id, Latest_File_Updated_Date__c,Latest_File_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Aircraft__c where ID = :ac.Id];
        system.assertEquals(null,updatedAC3.Latest_File_Updated_Date__c);
        system.assertEquals(null,updatedAC3.Y_Hidden_Latest_File_Doc_Id__c);

    }
    @IsTest
    private static void testLatestFileUploadOnLease(){
        LW_Setup__c lwSetup = new LW_Setup__c();
		lwSetup.Name = 'UPDATE_LATEST_FILE_FIELDS';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'ON';
		insert lwSetup;


        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c =  system.today().addYears(5)
            
        );
        insert leaseRecord;
        Lease__c lease = [select Id from Lease__c limit 1];
        String yourFilesContent = 'TheBlogReaders.com File upload content';
 
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = 'testing.txt'; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = 'Testing Files'; // Display name of the files
        conVer.VersionData = Blob.valueOf(yourFilesContent); // converting your binary string to Blog
        conVer.FirstPublishLocationId =  lease.Id;    
        insert conVer;    //Insert ContentVersion
        
        
      
        Lease__C updatedlease = [select Id, Latest_Lease_Summary_Updated_Date__c,Latest_Lease_Summary_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Lease__c where ID = :lease.Id];
        ContentDocumentLink cdLink = [select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId = : lease.id];
        system.debug('cdLink---'+ cdLink);
        system.debug('updatedlease------'+ updatedlease.Latest_Lease_Summary_Updated_Date__c);
        system.debug('updatedlease------'+ updatedlease.Latest_Lease_Summary_Updated_By__c);
        system.debug('updatedlease------'+ updatedlease.Y_Hidden_Latest_File_Doc_Id__c);
        system.assertEquals(system.today(),updatedlease.Latest_Lease_Summary_Updated_Date__c);
        system.assertEquals(cdLink.ContentDocumentId+'-'+'Testing Files',updatedlease.Y_Hidden_Latest_File_Doc_Id__c);


        ContentVersion conVer1 = new ContentVersion();
        conVer1.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer1.PathOnClient = 'testingNew.txt'; // The files name, extension is very important here which will help the file in preview.
        conVer1.Title = 'Testing New Files'; // Display name of the files
        conVer1.VersionData = Blob.valueOf(yourFilesContent); // converting your binary string to Blog
        conVer1.FirstPublishLocationId =  updatedlease.Id;   
        system.debug('conVer1---------'+ conver1); 
        LeasewareUtils.clearFromTrigger();
        insert conVer1; 
        List<ContentDocumentLink> cdLink1 = [select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId = :lease.id order by Id asc ];
       
        ContentDocument cd= [select Id from ContentDocument where Id =: cdLink1[1].ContentDocumentId];
        delete cd;
        Lease__c updatedlease2 = [select Id, Latest_Lease_Summary_Updated_Date__c,Latest_Lease_Summary_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Lease__c where ID = :lease.Id];
        ContentDocument cd1 = [select Id from ContentDocument ];
        
        system.assertEquals(system.today(),updatedlease2.Latest_Lease_Summary_Updated_Date__c);
        system.assertEquals(cd1.Id+'-'+'Testing Files',updatedlease2.Y_Hidden_Latest_File_Doc_Id__c);

        delete cd1;
        Lease__c updatedlease3 = [select Id, Latest_Lease_Summary_Updated_Date__c,Latest_Lease_Summary_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Lease__c where ID = :lease.Id];
        system.assertEquals(null,updatedlease3.Latest_Lease_Summary_Updated_Date__c);
        system.assertEquals(null,updatedlease3.Y_Hidden_Latest_File_Doc_Id__c);



    }

   
    @IsTest
    private static void testLatestFileUploadOnDeal(){
        LW_Setup__c lwSetup = new LW_Setup__c();
		lwSetup.Name = 'UPDATE_LATEST_FILE_FIELDS';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'ON';
		insert lwSetup;


        map<String,Object> mapInOut = new map<String,Object>();
         // Create Operator
         mapInOut.put('Operator__c.Name','Air India');
         TestLeaseworkUtil.createOperator(mapInOut);
         Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
         Id idRecordTypeAsset=Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
         
         Aircraft__c aircraft = new Aircraft__c(
             Name= ' Test Asset 1',
             Aircraft_Type__c='3DAero',
             RecordTypeId = idRecordTypeAsset, 
             MSN_Number__c = '111'
         );
         LeaseWareUtils.clearFromTrigger(); 
         insert aircraft;
         Id Aircraft1 = aircraft.Id;  
         
         Aircraft__c aircraft2 = new Aircraft__c(
             Name= ' Test Asset 2',
             Aircraft_Type__c='CFM56',
             RecordTypeId = idRecordTypeAsset,
             MSN_Number__c = '222'
         );
         LeaseWareUtils.clearFromTrigger(); 
         insert aircraft2;
         Id Aircraft2Id = aircraft2.Id; 
         
         Aircraft__c aircraft3 = new Aircraft__c(
             Name= ' Test Asset 3',
             Aircraft_Type__c='CFM56',
             RecordTypeId = idRecordTypeAsset,
             MSN_Number__c = '333'
         );
         LeaseWareUtils.clearFromTrigger(); 
         insert aircraft3;
         Id Aircraft3Id = aircraft3.Id; 
         Date StartDate=  Date.newInstance(2020,1,1);
         Date EndDate=  Date.newInstance(2020,12,31);        
         
         Id idRecordType=Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByDeveloperName().get('Lease').getRecordTypeId();
         system.debug('Record Type ID=-'+ idRecordType);
         system.debug('Insert Deal');
         Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
         Deal_Type__c='Lease' ,
         RecordTypeId = idRecordType, 
         Deal_Status__c = 'Pipeline',
         Deal_Name_Format__c= 'Prospect + MSNs',
         Asset_Type__c='Aircraft');


         LeaseWareUtils.clearFromTrigger();
         insert m1;
        
         Marketing_Activity__c deal = [select Id from Marketing_Activity__c limit 1];
        String yourFilesContent = 'TheBlogReaders.com File upload content';
        
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = 'testing.txt'; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = 'Testing Files'; // Display name of the files
        conVer.VersionData = Blob.valueOf(yourFilesContent); // converting your binary string to Blog
        conVer.FirstPublishLocationId =  deal.Id;    
        insert conVer;    //Insert ContentVersion
        
        
      
        Marketing_Activity__c updatedDeal = [select Id, Y_Hidden_Latest_File_Doc_Id__c,Latest_LOI_Marketing_Summary__c,Latest_LOI_Summary_Updated_By__c,Latest_LOI_Summary_Updated_Date__c from Marketing_Activity__c where ID = :deal.Id];
        ContentDocumentLink cdLink = [select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId = : deal.id];
        system.debug('cdLink---'+ cdLink);
        system.debug('updatedDeal-----'+ updatedDeal.Latest_LOI_Summary_Updated_Date__c);
        system.debug('updatedDeal------'+ updatedDeal.Latest_LOI_Summary_Updated_By__c);
        system.debug('updatedDeal------'+ updatedDeal.Y_Hidden_Latest_File_Doc_Id__c);
        system.assertEquals(system.today(),updatedDeal.Latest_LOI_Summary_Updated_Date__c);
        system.assertEquals(cdLink.ContentDocumentId+'-'+'Testing Files',updatedDeal.Y_Hidden_Latest_File_Doc_Id__c);
		
        ContentVersion conVer1 = new ContentVersion();
        conVer1.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer1.PathOnClient = 'testingNew.txt'; // The files name, extension is very important here which will help the file in preview.
        conVer1.Title = 'Testing New Files'; // Display name of the files
        conVer1.VersionData = Blob.valueOf(yourFilesContent); // converting your binary string to Blog
        conVer1.FirstPublishLocationId =  deal.Id;   
        system.debug('conVer1---------'+ conver1); 
        LeasewareUtils.clearFromTrigger();
        insert conVer1; 
        List<ContentDocumentLink> cdLink1 = [select Id,ContentDocumentId from ContentDocumentLink where LinkedEntityId = :deal.id order by Id asc ];
       
        ContentDocument cd= [select Id from ContentDocument where Id =: cdLink1[1].ContentDocumentId];
        delete cd;
        Marketing_Activity__c updatedDeal2 = [select Id, Y_Hidden_Latest_File_Doc_Id__c,Latest_LOI_Marketing_Summary__c,Latest_LOI_Summary_Updated_By__c,Latest_LOI_Summary_Updated_Date__c from Marketing_Activity__c where ID = :deal.Id];
		ContentDocument cd1 = [select Id from ContentDocument ];
        
        system.assertEquals(system.today(),updatedDeal2.Latest_LOI_Summary_Updated_Date__c);
        system.assertEquals(cd1.Id+'-'+'Testing Files',updatedDeal2.Y_Hidden_Latest_File_Doc_Id__c);

        delete cd1;
        Marketing_Activity__c updatedDeal3 = [select Id, Y_Hidden_Latest_File_Doc_Id__c,Latest_LOI_Marketing_Summary__c,Latest_LOI_Summary_Updated_By__c,Latest_LOI_Summary_Updated_Date__c from Marketing_Activity__c where ID = :deal.Id];
		system.assertEquals(null,updatedDeal3.Latest_LOI_Summary_Updated_Date__c);
        system.assertEquals(null,updatedDeal3.Y_Hidden_Latest_File_Doc_Id__c);



    }
    
    @isTest
    static void testWithCSOFF(){
         LW_Setup__c lwSetup = new LW_Setup__c();
		lwSetup.Name = 'UPDATE_LATEST_FILE_FIELDS';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'OFF';
		insert lwSetup;


        Aircraft__c aircraft = new Aircraft__c(
            Name = 'TestAircraft',
            MSN_Number__c = '0J54-E190-80972',
            Aircraft_Type__c = '175',
            TSN__c = 100,
             
            CSN__c = 100
            );
        LeasewareUtils.clearFromTrigger();
        insert aircraft;
        Aircraft__c ac = [select Id from Aircraft__c limit 1];
        String yourFilesContent = 'TheBlogReaders.com File upload content';
 
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = 'testing.txt'; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = 'Testing Files'; // Display name of the files
        conVer.VersionData = Blob.valueOf(yourFilesContent); // converting your binary string to Blog
        conVer.FirstPublishLocationId =  ac.Id; 
        system.debug('conVer--------'+ conver);    
        insert conVer;    //Insert ContentVersion
        
       	Aircraft__c updatedAC = [select Id, Latest_File_Updated_Date__c,Latest_File_Updated_By__c,Y_Hidden_Latest_File_Doc_Id__c from Aircraft__c where ID = :ac.Id];
		 system.assertEquals(null,updatedAC.Latest_File_Updated_Date__c);
        system.assertEquals(null,updatedAC.Y_Hidden_Latest_File_Doc_Id__c);

    }
}