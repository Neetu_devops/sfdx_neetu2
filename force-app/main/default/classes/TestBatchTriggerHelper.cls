@isTest
private class TestBatchTriggerHelper {

    @TestSetup static void setupMethod() {

        Test.startTest();
            Date dte = Date.newInstance(2018, 9, 21);
            Date oneMonthBefore = dte.addDays(-30);
            Date oneMonthLater = dte.addDays(30);
            List<Order__c> orders = TestLeaseworkUtil.createPurchaseAgreement(1, true, new Map<String, Object>{'Escalation__c' => 5, 'Order_Sign_Date__c' => dte });
            Order__c ord = orders[0];
            List<Structure__c> strucs = TestLeaseworkUtil.createStructures(1, ord.Id, true, new Map<String, Object>());
            Id structLineRTI = Schema.SObjectType.PDP_Structure_Line__c.getRecordTypeInfosByName().get('Variable').getRecordTypeId();
            List<PDP_Structure_Line__c> line1 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 1,
            'Payment__c' => 1, 'Month_Prior_Delivery__c' => 1});
            List<PDP_Structure_Line__c> line2 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 2,
            'Payment__c' => 1, 'Month_Prior_Delivery__c' => 2});
            List<PDP_Structure_Line__c> line3 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 3,
            'Payment__c' => 1, 'Month_Prior_Delivery__c' => 3});

            List<Batch__c> batches = TestLeaseworkUtil.createBatch(1, strucs[0].Id, orders[0].Id, true, new Map<String, Object>());
            Id recordTypeId = Schema.SObjectType.Engine_Base_Price__c.getRecordTypeInfosByName().get('CFM').getRecordTypeId();
            List<Engine_Base_Price__c> ebp = TestLeaseworkUtil.createEBP(1, ord.Id, true, new Map<String, Object>{'RecordTypeId' => recordTypeId, 'Aircraft_Type__c' => 'A300', 'Manufacturer__c' => 'CFM',
            'Engine_Type__c' => 'CFM56-5B4/P', 'Base_Reference_Net_Price__c' => 15, 'Price__c' => 10, 'Allowance__c' => 1});
            Id rti = Schema.SObjectType.Aircraft_Base_Price__c.getRecordTypeInfosByName().get('Airbus').getRecordTypeId();
            List<Aircraft_Base_Price__c> abp = TestLeaseworkUtil.createABP(1, ord.Id, true, new Map<String, Object>{'RecordTypeId' => rti,'Aircraft_Type__c' => 'A300'});

            Assumed_Escalation__c ac = new Assumed_Escalation__c();
            ac.Name = 'Assumed Escalation';
            ac.Percentage__c = 5;
            insert ac;
            EngineManufacturersCap__c Emc = new EngineManufacturersCap__c();
            Emc.Name = 'CFM';
            Emc.AVAssumed__c = 100;
            Emc.Fleet__c = 'A319/A320';
            Emc.InitialCap__c = 3.50;
            Emc.ShareCap__c = 7.50;
            insert Emc;
            EscalationCS__c Ecs = new EscalationCS__c();
            Ecs.Name='Airbus';
            Ecs.Assumed_Escalation__c=4;
            Ecs.AV_Assumed__c=100;
            Ecs.Initial_CAP__c=2.85;
            Ecs.Share_CAP__c=100;
            insert Ecs;

            List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'Airbus',
            'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf;

            String itype = 'Airbus (Credit)';
            List<Cost_Revision__c> crf2 = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => itype,
            'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf2;

            List<Cost_Revision__c> crf3 = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'CFM',
            'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf3;

        Test.stopTest();

        List<Pre_Delivery_Payment__c> pdps = TestLeaseworkUtil.createPDP(1, batches[0].Id, true,
                new Map<String, Object>{'Aircraft_Base_Price__c' => abp[0].Id, 'Engine_Base_Price__c' => ebp[0].Id, 'Aircraft_Model__c' => 'A300',
                'Delivery_Date__c' => dte, 'Rank__c' => 'A', 'Engine_Manufacturer__c' => 'CFM', 'Engine_Type__c' => 'CFM56-5B4/P'});
    }

    @isTest static void deleteChangeStructureTest() {

        Test.startTest();
            Order__c ord = [ SELECT Id FROM Order__c LIMIT 1 ];
            List<Structure__c> strucsAux = TestLeaseworkUtil.createStructures(1, ord.Id, true, new Map<String, Object>());
            Batch__c b = [ SELECT Id FROM  Batch__c LIMIT 1];
            b.PDP_Structure__c = strucsAux[0].Id;
            update b;
        Test.stopTest();
    }

    @isTest static void BatchTriggerHelperITrigger() {
        PDPBatchTriggerHelper bth = new PDPBatchTriggerHelper();
        bth.bulkBefore();
        bth.bulkAfter();
        bth.beforeInsert();
        bth.beforeUpdate();
        bth.beforeDelete();
        bth.afterInsert();
        bth.afterDelete();
        bth.afterUnDelete();
        bth.andFinally();
    }
}