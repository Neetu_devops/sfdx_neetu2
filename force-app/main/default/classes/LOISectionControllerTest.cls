@isTest
private class LOISectionControllerTest {
    
    @testSetup
    static void testsetup(){
        
        Lessor__c setup = new Lessor__c();
        setup.Name= 'Test';
        insert setup;
        
        Operator__c prospect = new Operator__c();
        prospect.Airline_Type_M__c= 'Full Service';
        insert prospect;

        
        Marketing_Activity__c ma = new Marketing_Activity__c(name ='test MA' , prospect__c =prospect.id, Deal_Status__c = 'LOI');
        insert ma;
        
                
        Department_Default__c departmentDefault = new Department_Default__c();
        departmentDefault.Name= 'Test';
        departmentDefault.Recipients_on_Complete__c = 'test@gmail.com';
        departmentDefault.Recipients_on_Incomplete__c = 'testincomplete@gmail.com';
        departmentDefault.Setup__c= setup.Id;
        insert departmentDefault;
        
        Deal_Team__c dteam2 = new Deal_Team__c (name ='test deal team final', Marketing_Activity__c = ma.id,final_notification__c =  true);
        dteam2.complete__c = true;
        dteam2.Complete_Message_Enabled__c = true;
        dteam2.Recipients_on_Complete__c = 'test@test.com';
        dteam2.Email_Message_on_Complete__c = 'test';
        dteam2.InComplete_Message_Enabled__c = true;
        dteam2.Recipients_on_InComplete__c = 'test@test.com';
        dteam2.Email_Message_on_InComplete__c = 'test';
        insert dteam2;
    }
    @isTest
    static void testMethodUpdate(){
        Department_Default__c departmentDefault = [SELECT Id,Recipients_on_Incomplete__c,Recipients_on_Complete__c FROM Department_Default__c LIMIT 1];
        departmentDefault.Recipients_on_Complete__c = 'abc@gmail.com';
        departmentDefault.Recipients_on_Incomplete__c = 'testincomplete@gmail.com,abc@gmail.com';
        update departmentDefault;
        
        Deal_Team__c dteam2 = [SELECT Id,Recipients_on_Incomplete__c,Recipients_on_Complete__c FROM Deal_Team__c LIMIT 1];
        dteam2.Recipients_on_Complete__c = 'test@test.com';
        dteam2.Recipients_on_InComplete__c = 'test@test.com';
        update dteam2;
    }
    
    @isTest
    static void testMethodDelete(){
        Department_Default__c departmentDefault = [SELECT Id,Recipients_on_Incomplete__c,Recipients_on_Complete__c FROM Department_Default__c LIMIT 1];
        delete departmentDefault;
        
        Deal_Team__c dteam2 = [SELECT Id,Recipients_on_Incomplete__c,Recipients_on_Complete__c FROM Deal_Team__c LIMIT 1];
        delete dteam2;
    }
    
    @isTest
    static void testMethod1(){
    
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Lessor__c setup = [SELECT Id FROM Lessor__c LIMIT 1];
        
        Department_Default__c departmentDefault = new Department_Default__c();
        departmentDefault.Name= 'Test';
        departmentDefault.Recipients_on_Complete__c = 'test@gmail.com';
        departmentDefault.Recipients_on_Incomplete__c = 'testincomplete@gmail.com';
        departmentDefault.Setup__c= setup.Id;
        insert departmentDefault;
        
        
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
         // Escalation__c = 43,
         // Escalation_Month__c = 'January'
         	);
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        Operator__c prospect = new Operator__c();
        prospect.Airline_Type_M__c= 'Full Service';
        insert prospect;
        
        Marketing_Activity__c ma = new Marketing_Activity__c(name ='test MA' , prospect__c =prospect.id, Deal_Status__c = 'LOI');
        insert ma;
        
       Pricing_Run_New__c dealAnalysis =new Pricing_Run_New__c (name='1'
                                                                 ,Deal_Type__c = 'Order Position'
                                                                 ,Analysis_Target__c = 'Rent for Given IRR'
                                                                 ,MTOW_lbs__c =12
                                                                 ,Maintenance_Reserves__c = 'Cash'
                                                                 ,Lease_Term__c = '1 Year'
                                                                 ,Rate_Type__c = 'Fixed/Fixed'
                                                                 ,Deposit_Type__c = 'Cash',Deposit__c =1200
                                                                 ,Type_of_MR_If_Cash__c = 'Use Internal Estimates'
                                                                 ,Status__c ='Open'
                                                                 ,IRR__c = 10.9  
                                                                 ,Engine_Type_picklist__c = 'CFM'              
                                                                 ,Marketing_Activity__c = ma.Id,Final__c = True);
        LeaseWareUtils.clearFromTrigger();
        //Pricing_Run_New__c dealAnalysis = new Pricing_Run_New__c(name = 'test PRN', Marketing_Activity__c = ma.Id,Final__c = True);
        insert dealAnalysis;
        
        LOISectionController.getDealId(dealAnalysis.Id);
        
        //LOISectionController loiSection = new LOISectionController();
        
        LOISectionController.getRecordIdPrefix(dealAnalysis.Id);    
        LOISectionController.getPageLayoutFields(ma.Id);
        LOISectionController.getPageLayoutFields(dealAnalysis.Id);
        LOISectionController.getAssetTypeForSelectedDealAnalysis(dealAnalysis.Id);
        
        
        
        Aircraft_Proposal__c assetTrm = new Aircraft_Proposal__c(Marketing_Activity__c = ma.id,Asset_Type__c = '737-900',Deal_Analysis__c = null);
        insert assetTrm;
        
        Aircraft_Proposal__c assetTerm = new Aircraft_Proposal__c(Name='Test',Marketing_Activity__c = ma.id,Asset_Type__c = '737-900',Deal_Analysis__c = dealAnalysis.Id);
        insert assetTerm;
        
        List<Id> lstAssetTerm = new List<Id>();
        lstAssetTerm.add(assetTerm.Id);
        
        LOISectionController.saveEconoicAnalysis(dealAnalysis.Id,'Test',lstAssetTerm);
        
        Pricing_Output_New__c pricingOutput = new Pricing_Output_New__c ();
        pricingOutput.Pricing_Run__c = dealAnalysis.id;
        pricingOutput.Asset_Term__c = assetTerm.id;
        insert pricingOutput ;
        
        dealAnalysis.Final__c = False;
        update dealAnalysis;
        
        LOISectionController.getManagedAssetType(ma.Id);
        LOISectionController.getManagedAssetType(dealAnalysis.Id);
        LOISectionController.saveEconoicAnalysis(dealAnalysis.Id,'Test',lstAssetTerm);
        ShowAssetDetailsController.getAssetTermForMarketingActivity(ma.Id);    
        
        AssetTermOnDealAnalysisController.getDealAnalysisData(dealAnalysis.id);
        
        // create deal team records 
        Deal_Team__c dteam = new Deal_Team__c (name ='test deal team', Marketing_Activity__c = ma.id);
        insert dteam;
        
        //for final notification
        Deal_Team__c dteam2 = new Deal_Team__c (name ='test deal team final', Marketing_Activity__c = ma.id,final_notification__c =  true);
        dteam2.complete__c = true;
        dteam2.Complete_Message_Enabled__c = true;
        dteam2.Recipients_on_Complete__c = 'test@test.com';
        dteam2.Email_Message_on_Complete__c = 'test';
        dteam2.InComplete_Message_Enabled__c = true;
        dteam2.Recipients_on_InComplete__c = 'test@test.com';
        dteam2.Email_Message_on_InComplete__c = 'test';
        insert dteam2;
        
        List<Deal_Team__c> dealteams = DepartmentalFlagsController.getDepartmentValues(ma.Id);
        DepartmentalFlagsController.saveDepartmentStatus(dealteams);
        
         User usr = [Select id from User where Id = :UserInfo.getUserId()];

 

         System.RunAs(usr)
    
         {
            //create email template.
            EmailTemplate e = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test'); 
            insert e;
        
        }    
        
        //Utility Controller.
        dealteams[0].complete__c = true;
        dealteams[0].Complete_Message_Enabled__c = true;
        dealteams[0].Recipients_on_Complete__c = 'test@test.com';
        dealteams[0].Email_Message_on_Complete__c = 'test';
        
        update dealTeams;
         
        dealTeams[0].complete__c= false;
        update dealTeams;  
        
        Utility.formatDate(System.Today());
        Utility.formatYear(System.Today());
        Utility.getFieldLabels(new List<String>{'Account'});
        
    }
    
}