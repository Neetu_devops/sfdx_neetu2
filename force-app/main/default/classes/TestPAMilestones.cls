@isTest
private class TestPAMilestones {
	
    @testSetup static void setUp() {

        Test.startTest();
            Date dte = Date.newInstance(2018, 9, 21);
            Date oneMonthBefore = dte.addDays(-30);
            Date oneMonthLater = dte.addDays(30);
            List<Order__c> orders = TestLeaseworkUtil.createPurchaseAgreement(3, true, new Map<String, Object>{'Escalation__c' => 5, 'Order_Sign_Date__c' => dte });
            Order__c ord = orders[0];
            List<Structure__c> strucs = TestLeaseworkUtil.createStructures(1, ord.id, true, new Map<String, Object>());
            Id structLineRTI = Schema.SObjectType.PDP_Structure_Line__c.getRecordTypeInfosByDeveloperName().get('Variable').getRecordTypeId();
            List<PDP_Structure_Line__c> line1 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 1, 
                'Payment__c' => 1, 'Month_Prior_Delivery__c' => 1});
            List<PDP_Structure_Line__c> line2 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 2, 
                'Payment__c' => 1, 'Month_Prior_Delivery__c' => 2});
            List<PDP_Structure_Line__c> line3 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 3, 
                'Payment__c' => 1, 'Month_Prior_Delivery__c' => 3});

            List<Batch__c> batches = TestLeaseworkUtil.createBatch(1, strucs[0].Id,ord.id, true, new Map<String, Object>());
            Id recordTypeId = Schema.SObjectType.Engine_Base_Price__c.getRecordTypeInfosByDeveloperName().get('CFM').getRecordTypeId();
            List<Engine_Base_Price__c> ebp = TestLeaseworkUtil.createEBP(1, ord.id, true, new Map<String, Object>{'RecordTypeId' => recordTypeId, 'Aircraft_Type__c' => 'A300', 'Manufacturer__c' => 'CFM',
             'Engine_Type__c' => 'CFM56-5B4/P', 'Base_Reference_Net_Price__c' => 15, 'Price__c' => 10, 'Allowance__c' => 1});
            Id rti = Schema.SObjectType.Aircraft_Base_Price__c.getRecordTypeInfosByDeveloperName().get('Airbus').getRecordTypeId();
            List<Aircraft_Base_Price__c> abp = TestLeaseworkUtil.createABP(1, ord.id, true, new Map<String, Object>{'RecordTypeId' => rti,'Aircraft_Type__c' => 'A300'});

            Assumed_Escalation__c ac = new Assumed_Escalation__c();
            ac.Name = 'Assumed Escalation';
            ac.Percentage__c = 5;
            insert ac;
            EngineManufacturersCap__c Emc = new EngineManufacturersCap__c();
            Emc.Name = 'CFM';
            Emc.AVAssumed__c = 100;
            Emc.Fleet__c = 'A319/A320';
            Emc.InitialCap__c = 3.50;
            Emc.ShareCap__c = 7.50;
            insert Emc;
            EscalationCS__c Ecs = new EscalationCS__c();
            Ecs.Name='Airbus';
            Ecs.Assumed_Escalation__c=4;
            Ecs.AV_Assumed__c=100;
            Ecs.Initial_CAP__c=2.85;
            Ecs.Share_CAP__c=100;
            insert Ecs;

            List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'Airbus',
                'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf;

            String itype = 'Airbus (Credit)';
            List<Cost_Revision__c> crf2 = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => itype,
                'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf2;

            List<Cost_Revision__c> crf3 = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'CFM',
                'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf3;

        Test.stopTest();
        List<Pre_Delivery_Payment__c> pdps = TestLeaseworkUtil.createPDP(1, batches[0].Id, true, 
            new Map<String, Object>{'Aircraft_Base_Price__c' => abp[0].Id, 'Engine_Base_Price__c' => ebp[0].Id, 'Aircraft_Model__c' => 'A300',
            'Delivery_Date__c' => dte, 'Rank__c' => 'A', 'Engine_Manufacturer__c' => 'CFM', 'Engine_Type__c' => 'CFM56-5B4/P'});
       
    }

   
    @isTest
    static void testPAMilestoneTypeValidation() {
        String unexpectedEx;
      	Test.startTest();
        try{
        Order__c order = new Order__c(Name='Test Airbus',Order_Sign_Date__c=system.today(),
                                      Escalation__c=4,Defferal_Write__c=2,Flexibility_Date__c=5,
                                      Month_Notification__c=6);
        insert order;
      	Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
         milestone.PA_Milestone_Type__c = 'Model Substitution';
         milestone.Applicable_AC_Type__c= 'A300';
         milestone.Purchase_Agreement__c = order.Id;
         milestone.Description__c ='Test Milestone';
         milestone.Months_to_Delivery__c=10;
         insert milestone;
         Purchase_Agreement_Milestone__c milestone2 = new Purchase_Agreement_Milestone__c();
         milestone2.PA_Milestone_Type__c = 'Model Substitution';
         milestone2.Applicable_AC_Type__c= 'A300';
         milestone2.Purchase_Agreement__c = order.Id;
         milestone2.Description__c ='Test Milestone 2';
         milestone2.Months_to_Delivery__c=15;
         insert milestone2;
      	
        }catch(Exception e){
            system.debug('Exception Message --------'+e.getMessage());
            unexpectedEx = e.getMessage();
        }
 		system.Assert(unexpectedEx.contains('PA Milestone Type - Model Substitution cannot be saved as this Milestone with the A300  type  already exists at this Purchase Agreement. Specify new type or leave blank'));
    	Test.stopTest();
    }
    
     @isTest
    static void testDeliveryUpdateValidation(){
        List<Order__c> ord = [select Id , Name from Order__c];
       
        List<batch__c> batch = [select Id,Name from Batch__c ];
        List<Pre_Delivery_Payment__c> deliveryList = [select Id,Name,Batch__c from Pre_Delivery_Payment__c where batch__c=:batch[0].id];
        system.debug('Delivery List testCreationOfDeliveryMilestoneForACTypeSet----------'+ deliveryList.size());
        
        for(Pre_Delivery_Payment__c delivery: deliveryList){
            system.debug('delivery----'+ delivery.Id);
        }
         
        Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
         milestone.PA_Milestone_Type__c = 'Engine Substitution';
         milestone.Applicable_AC_Type__c= 'A300';
         milestone.Purchase_Agreement__c =ord[0].id;
         milestone.Description__c ='Test Milestone';
         milestone.Months_to_Delivery__c=10;
         insert milestone;
      	 List<Delivery_Milestone__c> deliveryMilestone = [select Id,Name from Delivery_Milestone__c  where delivery__c=:deliveryList[0].id];
         system.debug('Milestone List--------'+ deliveryMilestone.size());
         String unexpectedEx='';
         try{
         Pre_Delivery_Payment__c delivery = [select Id,Aircraft_Model__c from Pre_Delivery_Payment__c limit 1];
         delivery.Aircraft_Model__c= 'A320neo';
         update delivery ;
         }catch(Exception e){
            system.debug('Exception Message --------'+e.getMessage());
            unexpectedEx = e.getMessage();
        }
         System.assert(unexpectedEx.contains('Cannot change Aircraft Type as milestones exist. Please remove the milestones before changing Aircraft Type'));
    }
    
     @isTest
    static void testCreationOfDeliveryMilestoneForACTypeSet(){
        
		
		List<Order__c> ord = [select Id , Name from Order__c];
       
        List<batch__c> batch = [select Id,Name from Batch__c ];
        List<Pre_Delivery_Payment__c> deliveryList = [select Id,Name,Batch__c from Pre_Delivery_Payment__c where batch__c=:batch[0].id];
        system.debug('Delivery List testCreationOfDeliveryMilestoneForACTypeSet----------'+ deliveryList.size());
        
        for(Pre_Delivery_Payment__c delivery: deliveryList){
            system.debug('delivery----'+ delivery.Id);
        }
         
        Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
         milestone.PA_Milestone_Type__c = 'Engine Substitution';
         milestone.Applicable_AC_Type__c= 'A300';
         milestone.Purchase_Agreement__c =ord[0].id;
         milestone.Description__c ='Test Milestone';
         milestone.Months_to_Delivery__c=10;
         insert milestone;
      	 List<Delivery_Milestone__c> deliveryMilestone = [select Id,Name from Delivery_Milestone__c  where delivery__c=:deliveryList[0].id];
         system.debug('Milestone List--------'+ deliveryMilestone.size());
         System.assertEquals(1, deliveryMilestone.size());
       
 	 }
    
@isTest
    static void testCreationOfDeliveryMilestoneForACTypeMismatch(){
        
		
		List<Order__c> ord = [select Id , Name from Order__c];
        List<batch__c> batch = [select Id,Name from Batch__c ];
        List<Pre_Delivery_Payment__c> deliveryList = [select Id,Name from Pre_Delivery_Payment__c where Batch__c=:batch[0].id];
         
        Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
         milestone.PA_Milestone_Type__c = 'Engine Substitution';
         milestone.Applicable_AC_Type__c= 'A320neo';
         milestone.Purchase_Agreement__c = ord[0].Id;
         milestone.Description__c ='Test Milestone';
         milestone.Months_to_Delivery__c=10;
         insert milestone;
      	 List<Delivery_Milestone__c> deliveryMilestone = [select Id,Name from Delivery_Milestone__c ];
         system.debug('Milestone List--------'+ deliveryMilestone.size());
         System.assertEquals(0, deliveryMilestone.size());
       
 	 }
    
    @isTest
    static void  testDeleteOfPAMilestones(){
        List<Order__c> ord = [select Id , Name from Order__c];
        Order__c order = ord[0];
        List<batch__c> batch = [select Id,Name from Batch__c ];
        List<Pre_Delivery_Payment__c> deliveryList = [select Id,Name from Pre_Delivery_Payment__c where Id=:batch[0].id];
         
        Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
         milestone.PA_Milestone_Type__c = null;
         milestone.Applicable_AC_Type__c= 'A300';
         milestone.Purchase_Agreement__c = order.Id;
         milestone.Description__c ='Test Milestone 1';
         milestone.Months_to_Delivery__c=10;
         insert milestone;
        
         Purchase_Agreement_Milestone__c milestone1 = new Purchase_Agreement_Milestone__c();
         milestone1.PA_Milestone_Type__c = null;
         milestone1.Applicable_AC_Type__c= 'A310';
         milestone1.Purchase_Agreement__c = order.Id;
         milestone1.Description__c ='Test Milestone 2';
         milestone1.Months_to_Delivery__c=10;
         insert milestone1;
        
        
        List<Purchase_Agreement_Milestone__c> pamilestoneList =  [select Id,Name from Purchase_Agreement_Milestone__c ];
        system.debug('pamilestoneList Size----' +pamilestoneList.size());
        
        List<Delivery_Milestone__c> deliveryMilestoneList = [select Id,Name from Delivery_Milestone__c ];
        system.debug('Delivery Milestone Size------------'+ deliveryMilestoneList.size());
        deliveryMilestoneList[0].Months_to_Delivery__c = 16;
    	update  deliveryMilestoneList[0];
        delete pamilestoneList;
        List<Delivery_Milestone__c> deliveryMilestoneUpdatedList = [select Id,Name from Delivery_Milestone__c ];
        System.assertEquals(1, deliveryMilestoneUpdatedList.size());
    }
    
    @isTest
    static void testCopyOfMilestonesFromOtherPA(){
        List<Order__c> ord = [select Id , Name from Order__c];

        List<batch__c> batch = [select Id,Name from Batch__c ];
        List<Pre_Delivery_Payment__c> deliveryList = [select Id,Name from Pre_Delivery_Payment__c where Id=:batch[0].id];
         
        Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
         milestone.PA_Milestone_Type__c = 'Engine Substitution';
         milestone.Applicable_AC_Type__c= 'A300';
         milestone.Purchase_Agreement__c = ord[0].Id;
         milestone.Description__c ='Test Milestone';
         milestone.Months_to_Delivery__c=10;
         insert milestone;
        
        Order__c ord2 = ord[1];
        ord2.Import_Milestones_From_Purchase_Agrement__c = ord[0].id ;
       	update ord2;
        List<Purchase_Agreement_Milestone__c> milestonesList = [select Id,Name from Purchase_Agreement_Milestone__c where 
                                                                Purchase_Agreement__c =:ord2.id];
         System.assertEquals(1, milestonesList.size());
    }
    
    @isTest
    static void testDeletionOfMilestonesFromPreviousPA(){
        List<Order__c> ord = [select Id , Name from Order__c];

        List<batch__c> batch = [select Id,Name from Batch__c ];
        List<Pre_Delivery_Payment__c> deliveryList = [select Id,Name from Pre_Delivery_Payment__c where Id=:batch[0].id];
         
        Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
         milestone.PA_Milestone_Type__c = 'Engine Substitution';
         milestone.Applicable_AC_Type__c= 'A300';
         milestone.Purchase_Agreement__c = ord[0].Id;
         milestone.Description__c ='Test Milestone';
         milestone.Months_to_Delivery__c=10;
         insert milestone;
        
        Order__c ord2 = ord[1];
        ord2.Import_Milestones_From_Purchase_Agrement__c = ord[0].id ;
       	update ord2;
        List<Purchase_Agreement_Milestone__c> milestonesList = [select Id,Name from Purchase_Agreement_Milestone__c where 
                                                                Purchase_Agreement__c =:ord2.id];
        System.assertEquals(1, milestonesList.size());
        ord2.Import_Milestones_From_Purchase_Agrement__c = null;
        update ord2;
        List<Purchase_Agreement_Milestone__c> milestonesList2 = [select Id,Name from Purchase_Agreement_Milestone__c where 
                                                                Purchase_Agreement__c =:ord2.id];
       	System.assertEquals(0, milestonesList2.size());
        
    }
    
    @isTest
    static void testPAMilestonesChangePA(){
        List<Order__c> ord = [select Id , Name from Order__c];

        List<batch__c> batch = [select Id,Name from Batch__c ];
        List<Pre_Delivery_Payment__c> deliveryList = [select Id,Name from Pre_Delivery_Payment__c where Id=:batch[0].id];
         
        Purchase_Agreement_Milestone__c milestone = new Purchase_Agreement_Milestone__c();
        milestone.PA_Milestone_Type__c = 'Engine Substitution';
        milestone.Applicable_AC_Type__c= 'A300';
        milestone.Purchase_Agreement__c = ord[0].Id;
        milestone.Description__c ='Test Milestone';
        milestone.Months_to_Delivery__c=10;
        insert milestone;
     	
        Purchase_Agreement_Milestone__c milestone1 = new Purchase_Agreement_Milestone__c();
        milestone1.PA_Milestone_Type__c = 'Model Substitution';
        milestone1.Applicable_AC_Type__c= null;
        milestone1.Purchase_Agreement__c = ord[2].Id;
        milestone1.Description__c ='Test Milestone 2';
        milestone1.Months_to_Delivery__c=15;
        insert milestone1;
        
        Order__c ord2 = ord[1];
        ord2.Import_Milestones_From_Purchase_Agrement__c = ord[0].Id;
        update ord2;
        
    
        ord2.Import_Milestones_From_Purchase_Agrement__c = ord[2].Id;
        update ord2;
        
         List<Purchase_Agreement_Milestone__c> milestonesList2 = [select Id,Name from Purchase_Agreement_Milestone__c where 
                                                                Purchase_Agreement__c =:ord2.id];
        System.assertEquals(1, milestonesList2.size());
        
    }
    
}