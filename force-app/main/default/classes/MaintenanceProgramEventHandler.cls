public class MaintenanceProgramEventHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
    private static Set<String> standardCA_RecTypeDevloperNames = new Set<String>{'Airframe','APU','Engine','Landing_Gear','Propeller'};       
		
    // Constructor
    private final string triggerBefore = 'MaintenanceProgramEventHandlerBefore';
    private final string triggerAfter = 'MaintenanceProgramEventHandlerAfter';
    public MaintenanceProgramEventHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
     public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('MaintenanceProgramEventHandler.beforeInsert(+)');
		defaulting();
        SOQLMapList();
        Maintenance_Program_Event__c[] newMPEList = (list<Maintenance_Program_Event__c>)(trigger.new) ;
        Set<Id> setMPIds = new Set<Id>();
        
       	for(Maintenance_Program_Event__c newmpe :newMPEList){
        checkDuplicateValuesForEventCreation(newmpe);
        setMPIds.add(newmpe.Maintenance_Program__c);
        }
        validateMappingRecords(setMPIds,newMPEList);
        checkEngTempLPPReplcmnt(newMPEList);
        system.debug('MaintenanceProgramEventHandler.beforeInsert(+)');     
    }
     
   public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('MaintenanceProgramEventHandler.beforeUpdate(+)');
        Set<Id> setMPIds = new Set<Id>();
		defaulting();
        SOQLMapList();
        Maintenance_Program_Event__c[] newMPEList = (list<Maintenance_Program_Event__c>)(trigger.new) ;
       	for(Maintenance_Program_Event__c newmpe :newMPEList){
           	Maintenance_Program_Event__c oldmpe = (Maintenance_Program_Event__c)Trigger.oldMap.get(newmpe.Id);
            if(newmpe.Assembly__c != oldmpe.Assembly__c || newmpe.Event_Type_Global__c!= oldmpe.Event_Type_Global__c){
                checkDuplicateValuesForEventCreation(newmpe);
                setMPIds.add(newmpe.Maintenance_Program__c);
            }
        
        }
        validateMappingRecords(setMPIds,newMPEList);
        checkEngTempLPPReplcmnt(newMPEList);
        system.debug('MaintenanceProgramEventHandler.beforeUpdate(-)');
    }
     
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        system.debug('MaintenanceProgramEventHandler.beforeDelete(+)');
        
        
        system.debug('MaintenanceProgramEventHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('MaintenanceProgramEventHandler.afterInsert(+)');

		//LeaseWareUtils.CRUDEventInfo(trigger.newMap.keySet(), null, 0);
		List<Maintenance_Program_Event__c> mpeList = (list<Maintenance_Program_Event__c>)(trigger.new) ; 
        Set<Id> setMPIds = new Set<Id>();
        for(Maintenance_Program_Event__c mpe: mpeList ){
            setMPIds.add(mpe.Maintenance_Program__c);
      
       	}
		LeaseWareUtils.projectedEventOperations(setMPIds,null);
        CreateMaintenanceEventTask(mpeList);
        UpdateHistEvent();
        system.debug('MaintenanceProgramEventHandler.afterInsert(-)');      
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('MaintenanceProgramEventHandler.afterUpdate(+)');
		Maintenance_Program_Event__c[] newMPEList = (list<Maintenance_Program_Event__c>)(trigger.new) ;
        Maintenance_Program_Event__c[] oldMPEList = (list<Maintenance_Program_Event__c>)(trigger.old) ;
        Set<Id> setMPEIdsToBeUpdated = new Set<Id>();
		//LeaseWareUtils.CRUDEventInfo(trigger.newMap.keySet(), null, 1);
		//LeaseWareUtils.updateProjectedEvent(null, trigger.newMap.keySet(),'UPDATE');
		 SOQLMapList();
        for(Maintenance_Program_Event__c newmpe :newMPEList){
             Boolean updateProjEvent = FALSE;
             Maintenance_Program_Event__c oldmpe = (Maintenance_Program_Event__c)Trigger.oldMap.get(newmpe.Id);
            if(oldmpe.Assembly__c !=newmpe.Assembly__c || oldmpe.Event_Type_Global__c != newmpe.Event_Type_Global__c){
				// checkFor Associated Projected Events for the mpe
				system.debug('mapMPE----------'+ mapMPEPE);
                if(mapMPEPE.get(newmpe.id) == 0){
                  	system.debug('Projected Event does not exist for the mpe Id so can update the mpe assembly and event type');
                }else{
                    system.debug(' Projected Events Exists .....');
                    newmpe.addError('Cannot change the Event type and assembly type as Projected Events exists for the Maintenance Program Event');
                }
			    
            }
          	if(oldmpe.Never_Exceed_Period_Months__c != newmpe.Never_Exceed_Period_Months__c || 
                oldmpe.Never_Exceed_Period_Hours__c != newmpe.Never_Exceed_Period_Hours__c ||
                oldmpe.Never_Exceed_Period_Cycles__c != newmpe.Never_Exceed_Period_Cycles__c ||
               	oldmpe.Interval_2_Months__c != newmpe.Interval_2_Months__c ||
                oldmpe.Interval_2_Hours__c != newmpe.Interval_2_Hours__c ||
                oldmpe.Interval_2_Cycles__c != newmpe.Interval_2_Cycles__c||
				oldmpe.Event_Cost__c != newmpe.Event_Cost__c ){
                    
                      updateProjEvent = TRUE;
                   	  system.debug('Update Proj Event Flag----' + updateProjEvent + 'for MPE--'+ newmpe.id);
                	  if (updateProjEvent) {
                    		setMPEIdsToBeUpdated.add(newmpe.id);
                      }
                   
                }
        }
        if(!setMPEIdsToBeUpdated.isEmpty()){
        	Database.executeBatch(new BatchUpdateOfProjectedEvent(null,setMPEIdsToBeUpdated));
        }
        CreateMaintenanceEventTask(newMPEList);
        UpdateHistEvent();
        system.debug('MaintenanceProgramEventHandler.afterUpdate(-)');
    }
     
    public void afterDelete()
    {

        system.debug('MaintenanceProgramEventHandler.afterDelete(+)');
        
        system.debug('MaintenanceProgramEventHandler.afterDelete(-)');      
    }

    public void afterUnDelete()
    {
        system.debug('MaintenanceProgramEventHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('MaintenanceProgramEventHandler.afterUnDelete(-)');        
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	private void defaulting(){
		
        Maintenance_Program_Event__c[] listNew = (list<Maintenance_Program_Event__c>)trigger.new ;
        for(Maintenance_Program_Event__c curMP:listNew){
            if(curMP.Interval_2_Cycles__c==null ) curMP.Interval_2_Cycles__c = curMP.Never_Exceed_Period_Cycles__c;
            if(curMP.Interval_2_Hours__c==null ) curMP.Interval_2_Hours__c = curMP.Never_Exceed_Period_Hours__c;
            if(curMP.Interval_2_Months__c==null ) curMP.Interval_2_Months__c = curMP.Never_Exceed_Period_Months__c;
        }		
    }

    map<String,list<Maintenance_Program_Event__c>> mapMP = new 	map<String,list<Maintenance_Program_Event__c>>();
    map<Id,Integer> mapMPEPE = new 	map<Id,Integer>();
    private void SOQLMapList(){
        list<Maintenance_Program_Event__c> mpeList = (list<Maintenance_Program_Event__c>)trigger.new ;
        Set<Id> mpIds = new Set<Id>();
        Set<Id> mpeIds = new Set<Id>();
      	for(Maintenance_Program_Event__c mpe :mpeList ){
            mpIds.add(mpe.Maintenance_Program__c);
            mpeIds.add(mpe.id);
       	}
        List<Maintenance_Program_Event__c> listOfMPE;
        List<Maintenance_Program_Event__c> oldMPEList = [select Id,Name,Assembly__c,Event_Type_Global__c,Maintenance_Program__c from Maintenance_Program_Event__c 
                                                         where Maintenance_Program__C IN : mpIds];
        for(Maintenance_Program_Event__c mpEvent : [select Id,(select Id from Assembly_Event_Info__r) from Maintenance_Program_Event__c where Id IN:mpeIds]){
            list<Assembly_Event_Info__c> listProjectedEvents;
            try{
                listProjectedEvents = mpEvent.Assembly_Event_Info__r;
            }catch(QueryException ex){
                listProjectedEvents = new list<Assembly_Event_Info__c>();
                for(Assembly_Event_Info__c pe:mpEvent.Assembly_Event_Info__r){
                    listProjectedEvents.add(pe);
                }
            }
            mapMPEPE.put(mpEvent.id, listProjectedEvents.size());
        }
        for(Maintenance_Program_Event__c mpe: oldMPEList){
            String keyString = mpe.Maintenance_Program__c + mpe.Assembly__c ;
            if(mapMP.get(keyString) == null){
                listOfMPE   = new List<Maintenance_Program_Event__c>();
                listOFMPE.add(mpe);
                
            }else{
                listOFMPE = mapMP.get(keyString);
                listOFMPE.add(mpe);
                
            }
            mapMP.put(keyString,listOFMPE);
        }
        
        system.debug('mapMP--------'+ mapMP);
     }
    
    
    private void checkDuplicateValuesForEventCreation(Maintenance_Program_Event__c curMPE){
        
        system.debug('checkDuplicateValuesForEventCreation------');
      	
            list<Maintenance_Program_Event__c> alreadyExistingMPEList = mapMP.get(curMPE.Maintenance_Program__c+curMPE.Assembly__c);
            if(alreadyExistingMPEList!=null && alreadyExistingMPEList.size()>0){
                system.debug(' alreadyExistingMPEList size---'+ alreadyExistingMPEList.size());
                for(Maintenance_Program_Event__c oldMPE : alreadyExistingMPEList){
                    if(oldMPE.Event_Type_Global__c == curMPE.Event_Type_Global__c){
                        system.debug('Event Exists');
                        curMPE.addError('Cannot save Maintenance Program Event as duplicate of the same Assembly Type and Event Type combination already exists');
                    }
                }
            }
    }

    //Before Insert, Before Update
    //Hint: Engine Template can be entered only for LLP Replacement event type
    private void checkEngTempLPPReplcmnt(list<Maintenance_Program_Event__c> listMPE){
        for(Maintenance_Program_Event__c curMPE :listMPE ){
            if(!'LLP Replacement'.equals(curMPE.Event_Type_Global__c) && curMPE.Engine_Template__c!=Null){
                curMPE.Engine_Template__c.addError('Engine Template can be entered only for LLP Replacement event type');
            }
        }
    }

    //afterInsert, afterUpdate
    //Purpose: Create Maintenance Event Task based on Engine Template and it's LLP Template Records
    
    private void CreateMaintenanceEventTask(list<Maintenance_Program_Event__c> listMPE){
        system.debug('MaintenanceProgramEventHandler.CreateMaintenanceEventTask(+)');
       
        Set<Id> SetNewEngTempIDs = new Set<Id>();
        Set<Id> SetDelEngTempIDs = new Set<Id>();
        set<ID> SetMPEIDs= new Set<Id>();
        Maintenance_Program_Event__c oldMPE;
        for(Maintenance_Program_Event__c mpe :listMPE ){
            //case 1- Insert- curEngTempId has value OR Update- PrevEngTempID=Null and curEngTempId has value
            if(Trigger.IsUpdate) oldMPE = (Maintenance_Program_Event__c)Trigger.oldMap.get(mpe.Id);
            
            If( (Trigger.IsInsert || (Trigger.IsUpdate && oldMPE.Engine_Template__c==Null)) && mpe.Engine_Template__c!=Null){
				SetNewEngTempIDs.add(mpe.Engine_Template__c);
			}
			//case 2- Update case- PrevEngTempID!=Null and curEngTempId=Null
			If(Trigger.IsUpdate && oldMPE.Engine_Template__c!=Null  && mpe.Engine_Template__c==Null ) {
				setDelEngTempIDs.add(oldMPE.Engine_Template__c);
                SetMPEIDs.add(oldMPE.id);
			}
			//case 3- Update case- PrevEngTempID!=Null and curEngTempId=!Null and PrevEngTempID !=curEngTempId
			If(Trigger.IsUpdate && oldMPE.Engine_Template__c!=Null  && mpe.Engine_Template__c!=Null && oldMPE.Engine_Template__c!= mpe.Engine_Template__c) {
				setDelEngTempIDs.add(oldMPE.Engine_Template__c);
                SetMPEIDs.add(oldMPE.id);
				SetNewEngTempIDs.add(mpe.Engine_Template__c);
            }
            //Case 4- No Action,when mpe.Engine_Template__c has value not getting update (or has value as Null and no change in the value).
        }
        list<Maintenance_Event_Task__c> listMET = new list<Maintenance_Event_Task__c>();
        
        listMET=[select id, Maintenance_Program_Event__c,
                    (Select id from Supplemental_Rent_Event_Requirements__r)
                 from Maintenance_Event_Task__c
                 where Maintenance_Program_Event__c in : SetMPEIDs and Y_Hidden_EngineTemplate__c in : setDelEngTempIDs];
        system.debug('size of listMET='+listMET.size());
        
        boolean errorFlag=false; 
        if(listMET.size()>0){
            for(Maintenance_Event_Task__c curMET: listMET){
                list<Supp_Rent_Event_Reqmnt__c> listEventReqmnt;
                try{
                    listEventReqmnt = curMET.Supplemental_Rent_Event_Requirements__r;
                }catch(QueryException ex){
                    listEventReqmnt = new list<Supp_Rent_Event_Reqmnt__c>();
                    for(Supp_Rent_Event_Reqmnt__c r:curMET.Supplemental_Rent_Event_Requirements__r){
                        listEventReqmnt.add(r);
                    }
                }
                system.debug('size of SRER='+ listEventReqmnt.size());
                if(listEventReqmnt.size()>0 && trigger.newmap.containsKey(curMET.Maintenance_Program_Event__c)){
                    trigger.newmap.get(curMET.Maintenance_Program_Event__c).addError('Update of Engine Template will cause Maintenance Event Tasks to be deleted/recreated accordingly. Remove Supplemental Rent Event Requirement association first to complete this change');
                    errorFlag=true;
                    break;
                }
            }
        }
        
        if(errorFlag) return;

        system.debug('size of setDelEngTempIDs='+setDelEngTempIDs.size());
        system.debug('size of SetNewEngTempIDs='+SetNewEngTempIDs.size());

        //Delete Maintenance Event Tasks for case 2 and case 3
        if(setDelEngTempIDs.size()>0 && SetMPEIDs.size()>0) {
            Delete [select id,name from Maintenance_Event_Task__c where Y_Hidden_EngineTemplate__c in : setDelEngTempIDs and Maintenance_Program_Event__c in : SetMPEIDs];
        }     

        //For case 1 (Insert) and case 3(Update)
        list<LLP_Template__c> listLLTemplate= new list<LLP_Template__c>();
        listLLTemplate=[ select id,Module_N__c,Part_Name__c,Replacement_Cost_Current_Catalog_Year__c, Engine_Template__c,
                            Engine_Template__r.Name
                        from LLP_Template__c 
                        where Engine_Template__c IN :SetNewEngTempIDs
                        ];
        system.debug('size of listLLTemplate='+listLLTemplate.size());
        map<id, List<LLP_Template__c>> mapEngTempLLTempIds = new map<id, List<LLP_Template__c>>();  // <EngTemplate ID,LLPTemplateRec>
        map<id, decimal> mapSumpRepCost = new map<id, decimal>(); // <EngTemp ID, sum of Replacement Cost of all LLP Templates for that EngTemplate>
        List<Maintenance_Event_Task__c> listMainEvntTask=new List<Maintenance_Event_Task__c>();
       
       //Two things are doing here :
       // 1. Creating map<EngTemplate ID,LLPTemplateRec> 
       // 2. sum of all Replacement_Cost for that Engine Temp id map<EngTemp ID, sum of LLP  Replacement Cost>

        if(listLLTemplate.size()>0) {
            for(LLP_Template__c curLLPTemp :listLLTemplate){
                if( !mapEngTempLLTempIds.containsKey(curLLPTemp.Engine_Template__c)){
                    mapEngTempLLTempIds.put(curLLPTemp.Engine_Template__c,new List<LLP_Template__c>());
                }
                mapEngTempLLTempIds.get(curLLPTemp.Engine_Template__c).add(curLLPTemp);  
                
                if( !mapSumpRepCost.containsKey(curLLPTemp.Engine_Template__c)){
                    mapSumpRepCost.put(curLLPTemp.Engine_Template__c,0);
                }
                mapSumpRepCost.put(curLLPTemp.Engine_Template__c,mapSumpRepCost.get(curLLPTemp.Engine_Template__c)+curLLPTemp.Replacement_Cost_Current_Catalog_Year__c);
            }
        }  

        system.debug('size of mapEngTempLLTempIds='+mapEngTempLLTempIds.size());
        system.debug('size of mapSumpRepCost='+mapSumpRepCost.size());

        //Hint: Creating Maintenance_Event_Task based on LLPs task for that Engine Template ID
        for(Maintenance_Program_Event__c mpe :listMPE){
            if(Trigger.IsUpdate) oldMPE = (Maintenance_Program_Event__c)Trigger.oldMap.get(mpe.Id);

            //Case 4:When no chagne in Engine TemplateId in MPE OR In MPE-> set EngineTemplate as Null then Skipping to create MET Record
            if((Trigger.IsUpdate && oldMPE.Engine_Template__c==mpe.Engine_Template__c) || mpe.Engine_Template__c==Null) {
                continue;
            }else if(!mapEngTempLLTempIds.containskey(mpe.Engine_Template__c)){ 
                //size of LLPs records for that Engine is zero then throw validation message and break the loop
                mpe.Engine_Template__c.addError('Cannot link Engine Template without LLP Templates, create LLP Templates first');
                break;
            }
            else{ //for case 1 (Insert) and case 3(Update)
                for(LLP_Template__c curLLPTemp: mapEngTempLLTempIds.get(mpe.Engine_Template__c)){ //reterive all LLPs record for that Engine
                    listMainEvntTask.add(new Maintenance_Event_Task__c(
                        Remark__c=curLLPTemp.Engine_Template__r.Name,
                        Name=curLLPTemp.Module_N__c==Null? curLLPTemp.Part_Name__c : curLLPTemp.Module_N__c +'-'+curLLPTemp.Part_Name__c,
                        TaskCostMin__c=curLLPTemp.Replacement_Cost_Current_Catalog_Year__c,
                        Maintenance_Program_Event__c=mpe.id,
                        TaskCostEventPrcntg__c=mapSumpRepCost.get(curLLPTemp.Engine_Template__c)!=0?((curLLPTemp.Replacement_Cost_Current_Catalog_Year__c*100)/mapSumpRepCost.get(curLLPTemp.Engine_Template__c)).setscale(2):0, // case of Engine template                        
                        Y_Hidden_TaskCostEvent__c= curLLPTemp.Replacement_Cost_Current_Catalog_Year__c, // case of Engine template - Internal purpose which ever is max(Task CostEven% in currency format,TaskCostMin) 
                        Y_Hidden_EngineTemplate__c=curLLPTemp.Engine_Template__c,
                        Y_Hidden_LLPTemplate__c=curLLPTemp.id
                    ));
                }
            }
        }
        system.debug('size of listMainEvntTask='+listMainEvntTask.size());
        //system.debug('listMainEvntTask='+ JSON.serializePretty(listMainEvntTask));
        if (listMainEvntTask.size() > 0) Insert listMainEvntTask;
        system.debug('MaintenanceProgramEventHandler.CreateMaintenanceEventTask(-)');
    }
    
    private void UpdateHistEvent(){
        list<Maintenance_Program_Event__c> mpeList = (list<Maintenance_Program_Event__c>)trigger.new ;
        List<Assembly_Eligible_Event__c> updateHElist = new List<Assembly_Eligible_Event__c>();
        
        List<Assembly_Eligible_Event__c> HEList = [Select Id, Name, Minor_Event__c, Y_Hidden_Maintenance_Program_Event__c, Y_Hidden_Maintenance_Program_Event__r.Minor_Event__c 
                                                   from Assembly_Eligible_Event__c
                                                   where Y_Hidden_Maintenance_Program_Event__c in :mpeList ];
        system.debug('HEList '+HEList);
        Maintenance_Program_Event__c tempMpe;
        for(Assembly_Eligible_Event__c histEvent : HEList){
           tempMpe = (Maintenance_Program_Event__c)trigger.newMap.get(histEvent.Y_Hidden_Maintenance_Program_Event__c);
           histEvent.Minor_Event__c = tempMpe.Minor_Event__c;
            updateHElist.add(histEvent);
        }
        if (!updateHElist.isEmpty()){ 
            system.debug('updateHElist '+ updateHElist);
            LeasewareUtils.clearfromTrigger();
            update updateHElist;
        }
    }
    
     /* Added by PriyankaC as a part of Asana Task : https://app.asana.com/0/1129279595336839/1198745014945789
       Description : This method is used to validate MPE record before creation or updation for any new Assembly_Type (Eg:Thrust Reverser) 
                     except the standard Assembly_Types(Eg: Airframe, APU, Engine etc)
                     We have Mapping Object record  : Event Type => 'Assembly Type Event'
                                                      Key =>'TR - EN1 LH' ,     
                                                      Value =>'Thrust Reverser' ,
                                                      
                     We have Assembly Object record : Type =>'TR - EN1 LH' ,
                                                      RecordType =>'Thrust Reverser'
       When we create a MPE record for Thrust Reverser, we get all assemblies for Thrust Reverser and match the MP(MPE) == MP(Assembly)
       if the corresponding assembly has tyep == 'TR - EN1 LH' and if Mapping Object Record does not have the key 'TR - EN1 LH',
       we throw the error.
    */
     private static void validateMappingRecords(Set<Id> setMPIds,List<Maintenance_Program_Event__c> newMPEList){
        System.debug('validateMappingRecords : IN');
        map<id,Constituent_Assembly__c> mapAssembly = new map<Id,Constituent_Assembly__c>();
        map<id,List<Constituent_Assembly__c>> mapMP_ASM = new map<Id,List<Constituent_Assembly__c>>();
        
        mapAssembly = new map < id, Constituent_Assembly__c >([select id,Name,RecordType.DeveloperName,Type__c,Attached_Aircraft__c,Y_Hidden_Maintenance_Program__c,
                                                                Attached_Aircraft__r.Maintenance_Program__c
                                                                from Constituent_Assembly__c where Y_Hidden_Maintenance_Program__c 
                                                                IN:setMPIds AND RecordType.DeveloperName not IN:standardCA_RecTypeDevloperNames ]);
        
         Map<String,String> mapAssemblyEventType = LeaseWareUtils.getMappingData('Assembly Type Event'); //('TR - EN1 LH': Thrust Reverser)
         Map<Id,Schema.RecordTypeInfo> mapAssemblyRecType = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosById();
        
         for(Constituent_Assembly__c curAsmbly :mapAssembly.values()){
             if(mapMP_ASM.containsKey(curAsmbly.Y_Hidden_Maintenance_Program__c)){
                 mapMP_ASM.get(curAsmbly.Y_Hidden_Maintenance_Program__c).add(curAsmbly);
             } else {
                 mapMP_ASM.put(curAsmbly.Y_Hidden_Maintenance_Program__c, new List<Constituent_Assembly__c> { curAsmbly });
             }
         }
         
         for(Maintenance_Program_Event__c newMPE : newMPEList){
             List<Constituent_Assembly__c>  listassemblies;
                 
             if(mapMP_ASM.containsKey(newMPE.Maintenance_Program__c)){
                 listassemblies  = mapMP_ASM.get(newMPE.Maintenance_Program__c);
             }
             else{
                 listassemblies = new List<Constituent_Assembly__c>();
             }
                  
             for(Constituent_Assembly__c curAsmbly :listassemblies){ 
                     if(newMPE.Assembly__c == mapAssemblyRecType.get(curAsmbly.RecordTypeId).getName() && !mapAssemblyEventType.containsKey(curAsmbly.Type__c)){ 
                         newMPE.addError('Cannot match Assembly Type with Maintenance Program assembly type. Please create a link using a Mapping object');  
                     } 
                   }
            }
      }

}