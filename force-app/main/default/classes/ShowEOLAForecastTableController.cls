public class ShowEOLAForecastTableController {
    @AuraEnabled
    public static EOLAForecastWrapper loadInitData(Id recordId, String fxMonthlyOutputId){
    	Monthly_Scenario__c msRecord = [SELECT Id,Eola_Rate__c,RateBasis_Label__c,RateBasis_Value__c,Eola_Amount__c, Start_Date__c, End_Date__c, Fx_Component_Output__r.Rate_Basis__c, Fx_Component_Output__r.Type__c,Fx_Component_Output__r.Event_Type__c, Month_Since_Last_Event_Weightage__c FROM Monthly_Scenario__c WHERE Id =: fxMonthlyOutputId];
	    //System.assert(false,msRecord.Fx_Component_Output__r.Rate_Basis__c);
	    EOLAForecastWrapper llpfWrap = new EOLAForecastWrapper(msRecord);
	    if(msRecord != null){
            if(msRecord.Fx_Component_Output__r.Type__c.contains('Engine') && msRecord.Fx_Component_Output__r.Event_Type__c.contains('LLP'))
            	llpfWrap.msAssemblyType = 'LLP';
            
            for(Monthly_Scenario__c msLLP : [SELECT Id, Start_Date__c, End_Date__c, Fx_Component_Output__r.Type__c,LLP_Cost__c, Fx_Component_Output__r.Name, FC__c,Eola_Amount__c, CSO__c, Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c ,
                                             Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name, Event_Date__c,Event_Expense_Amount__c,Cycles_Remaining__c,Fx_Component_Output__r.Assembly__r.Name,Fx_Component_Output__r.Life_Limited_Part_LLP__r.Part_Number__c,
                                             Month_Since_Last_Event_Weightage__c FROM Monthly_Scenario__c 
                                             WHERE Fx_Component_Output__r.Fx_General_Input__c = : recordId
                                             AND Start_Date__c >= : msRecord.Start_Date__c
                                             AND End_Date__c <= : msRecord.End_Date__c
                                             AND Fx_Component_Output__r.Life_Limited_part_LLP__c != NULL
                                             AND Fx_Component_Output__r.Type__c =: msRecord.Fx_Component_Output__r.Type__c
                                             ORDER BY Start_Date__c,Fx_Component_Output__r.Type__c]){
                                               
                                                 LLPForecastWrapper llpWrapper = new  LLPForecastWrapper(msLLP); 
                                                 llpfWrap.msRecordList.add(llpWrapper);                 
                                             }
            
            llpfWrap.msMonth = fetchMonth(msRecord.Start_Date__c);
	    }
	    return llpfWrap;
    }

    public static String fetchMonth(Date startData){
    	Map <Integer, String> monthNames = new Map <Integer, String> {1=>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May', 6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec'};
    	return monthNames.get(startData.month())+' '+startData.year();
    }

    public class EOLAForecastWrapper {
        @AuraEnabled public String rateBasisLabel {get;set;}
        @AuraEnabled public String rateBasis {get;set;}
        @AuraEnabled public Decimal rateBasisValue {get;set;}
        @AuraEnabled public Decimal currentRate {get;set;}
        @AuraEnabled public Decimal amount {get;set;}
        @AuraEnabled public List<LLPForecastWrapper> msRecordList {get;set;}
        @AuraEnabled public String msAssemblyType{get;set;}
        @AuraEnabled public String msMonth {get;set;}
        
        public EOLAForecastWrapper(Monthly_Scenario__c ms){
            this.msRecordList = new List<LLPForecastWrapper>();
            if(ms.RateBasis_Label__c.contains('CSO'))
                this.rateBasisLabel = 'Cycles Used';
            else
                this.rateBasisLabel = ms.RateBasis_Label__c;
            this.rateBasis = ms.Fx_Component_Output__r.Rate_Basis__c;
            this.rateBasisValue = ms.RateBasis_Value__c;
            if(ms.Fx_Component_Output__r.Rate_Basis__c == 'Monthly'){
                this.rateBasisValue = ms.Month_Since_Last_Event_Weightage__c;
            }
            this.currentRate = ms.Eola_Rate__c;
            this.amount = ms.Eola_Amount__c;
            this.msAssemblyType = '';
            this.msMonth = '';
        }
    }
    
    public class LLPForecastWrapper {
        @AuraEnabled public String msLLPName {get;set;}
        @AuraEnabled public Decimal msLLPCost {get;set;}
        @AuraEnabled public Decimal msLifeLimit{get;set;}
        @AuraEnabled public Decimal msCostPerCycle {get;set;}
        @AuraEnabled public Decimal msCSO {get;set;}
        @AuraEnabled public Decimal msEOLA {get;set;}
        public LLPForecastWrapper(Monthly_Scenario__c ms){
            msLLPName = ms.Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name;
            msLLPCost = ms.LLP_Cost__c;
            msLifeLimit = ms.Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c;
            if(msLLPCost != null && (msLifeLimit != null && msLifeLimit != 0)){
                msCostPerCycle = msLLPCost/msLifeLimit;
            }
            msCSO = ms.CSO__c;
            msEOLA = ms.Eola_Amount__c;
        }
        
    }

}