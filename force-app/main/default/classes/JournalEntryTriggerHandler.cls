public class JournalEntryTriggerHandler implements ITrigger{

    private final string triggerBefore = 'JournalEntryTriggerHandlerBefore';
    private final string triggerAfter = 'JournalEntryTriggerHandlerAfter';
    public JournalEntryTriggerHandler(){
        system.debug('INSIDE JournalEntryTriggerHandler');
    }
    
     /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void  bulkBefore(){
    }
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void  bulkAfter(){
    }
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void  beforeInsert(){
        setSequenceNumbers();
    }
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void  beforeUpdate(){}
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void  beforeDelete(){
       
        system.debug('Dimension.beforeDelete(-)');
    }
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void  afterInsert(){
        
       createJournalDimension();
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        
    }
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void  afterDelete(){
         
    }
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void  afterUnDelete(){}   
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
   public void  andFinally(){}

    private void createJournalDimension(){
        system.debug('createJournalDimension(++)');
        list<Journal_Entry__c> newList =  (list<Journal_Entry__c>)trigger.New;
        Set<Id> leaseIds = new Set<Id>();
        for(Journal_Entry__c jEntry : newList){
            system.debug('Lease Id---'+ jEntry.Y_Hidden_Lease_Id__c);
            leaseIds.add(jEntry.Y_Hidden_Lease_Id__c);
        }
        List<Dimension_Application__c> dimAppList = [select Id,Name,Lease__c,Dimension_Value__r.Name from Dimension_Application__c where Lease__c IN : leaseIds];
        system.debug('dimAppList Size----'+ dimAppList.size());
        map<Id,List<Dimension_Application__c>> mapDimAppListForLease = new map<Id,List<Dimension_Application__c>>();
       
        for(Dimension_Application__c dimApp : dimAppList){
            if(mapDimAppListForLease.get(dimApp.Lease__c)==null){
                mapDimAppListForLease.put(dimApp.Lease__c,new List<Dimension_Application__c>());
            }
            mapDimAppListForLease.get(dimApp.Lease__c).add(dimApp);
        }
        system.debug('mapDimAppListForLease---'+mapDimAppListForLease);
        List<Journal_Dimension__c> journalDimensionListToAdd = new List<Journal_Dimension__c>();
        
        for(Journal_Entry__c jEntry : newList){
            system.debug('invoice_type__c:::'+jEntry.transaction_type__c);
           if(mapDimAppListForLease==null || mapDimAppListForLease.get(jEntry.Y_Hidden_Lease_Id__c)==null) return ;
  		   for(Dimension_Application__c dimApp : mapDimAppListForLease.get(jEntry.Y_Hidden_Lease_Id__c))
    	   {
               Journal_Dimension__c journalDimension = new Journal_Dimension__c();
                journalDimension.Name = dimApp.Name;
                journalDimension.Dimension_Value__c = dimApp.Dimension_Value__r.Name;  
                journalDimension.Journal_Entry__c = jEntry.id;
                journalDimensionListToAdd.add(journalDimension);
            		
       	   }
     	}
       system.debug('journalDimensionListToAdd----'+ journalDimensionListToAdd.size());
       if(journalDimensionListToAdd.size()>0)insert journalDimensionListToAdd;
       system.debug('createJournalDimension(---)');
       
}
private void SetSequenceNumbers() 
{
 
    system.debug('SetSequenceNumbers++');
    list<Journal_Entry__c> jelist = (list<Journal_Entry__c>)trigger.new;
    LeaseWare_SequenceId__c sequenceStartNumber = LeaseWare_SequenceId__c.getValues('Sequence_Number_Journal_Entry');
    Map<Id,list<Journal_Entry__c>> journalsMap = new Map<Id,list<Journal_Entry__c>>();
    if (sequenceStartNumber != NULL && sequenceStartNumber.Active__c){
        //Get Sequence Number
        try { 
            sequenceStartNumber = LeaseWareUtils.getSequenceNumber('Journal');
        }catch (QueryException ex){
            if(ex.getMessage().contains('UNABLE_TO_LOCK_ROW') )
            jelist[0].addError(' Sequence generation in currently in progress. Please click Save again.');
        }
        Integer sequenceNumber=-1;
        //If Recent Sequence number not there , take Initial value.
        sequenceNumber=sequenceStartNumber.Sequence_Number_New__c ==NULL ?sequenceStartNumber.Initial_Value__c.intValue():sequenceStartNumber.Sequence_Number_New__c.intValue();
        //Assign Sequence Numbers
        system.debug('sequenceNumber:::'+sequenceNumber);
        List<Journal_Entry__c> jes =  null;
        for(Journal_Entry__c je : jelist){
            if(je.invoice__c!=null){
                jes = journalsMap.get(je.invoice__c);
                if(jes ==null){
                    jes = new List<Journal_Entry__c>();
                }
                jes.add(je);
                journalsMap.put(je.invoice__c,jes);
            }
            else if(je.payment__c!=null){
                jes = journalsMap.get(je.payment__c);
                if(jes ==null){
                    jes = new List<Journal_Entry__c>();
                }
                jes.add(je);
                journalsMap.put(je.payment__c,jes);
            }
            else if(je.prepayment__c!=null){
                jes = journalsMap.get(je.prepayment__c);
                if(jes ==null){
                    jes = new List<Journal_Entry__c>();
                }
                jes.add(je);
                journalsMap.put(je.prepayment__c,jes);
            }
            else if(je.credit_memo__c!=null){
                jes = journalsMap.get(je.credit_memo__c);
                if(jes ==null){
                    jes = new List<Journal_Entry__c>();
                }
                jes.add(je);
                journalsMap.put(je.credit_memo__c,jes);
            }
        }
        system.debug('journalsMap::::'+journalsMap);
        List<Journal_Entry__c> journals  = new  List<Journal_Entry__c>();
        for(Id ids : journalsMap.keySet()){
            journals = journalsMap.get(ids);
            for(Journal_Entry__c je : journals){
                je.y_hidden_journal_number__c = sequenceNumber;
            }
            system.debug('sequenceNumber:::'+sequenceNumber);
            sequenceNumber++;
        }     
        try {
                    //Update Custom Setting with Latest sequence number. 
            LeaseWareUtils.updateSequenceNumber('Journal',sequenceNumber);
        }catch (DmlException ex) {
            //Both Query Exception and dmlException should be handled
            if(ex.getMessage().contains('The record you are attempting to edit, or one of its related records, is currently being modified by another user')) 
                jelist[0].addError('Sequence generation in currently in progress. Please click Save again.');
        }
    }
    system.debug('SetSequenceNumbers--');
    return;
} 
/*private void SetSequenceNumbers() 
{
 
    system.debug('SetSequenceNumbers++');
    list<Journal_Entry__c> jelist = (list<Journal_Entry__c>)trigger.new;
    list<Journal_Entry__c> listToUpdate = new list<Journal_Entry__c>();
    Map<Id,list<Journal_Entry__c>> journalsMap = new Map<Id,list<Journal_Entry__c>>();
    List<AggregateResult> result  = [select max(y_Hidden_journal_Number__c) total from Journal_Entry__c];
    
    Integer maxValue = 0;

    try{
        maxValue = Integer.ValueOf(result[0].get('total'));
       
    }
    catch(Exception e){
        System.debug('Error:::'+e.getMessage());
        maxValue = 0;
    }
    if(maxValue==null)maxValue = 0;
    Integer curValue = maxValue + 1;
    system.debug('MaxValue:::'+MaxValue+'CurValue:::'+curValue);
    for(Journal_Entry__c je : jelist){
        List<Journal_Entry__c> jes = null;
        if(je.invoice__c!=null){
            jes = journalsMap.get(je.invoice__c);
            if(jes ==null){
                jes = new List<Journal_Entry__c>();
            }
            jes.add(je);
            journalsMap.put(je.invoice__c,jes);
        }
        else if(je.payment__c!=null){
            jes = journalsMap.get(je.payment__c);
            if(jes ==null){
                jes = new List<Journal_Entry__c>();
            }
            jes.add(je);
            journalsMap.put(je.payment__c,jes);
        }
    }
    system.debug('journalsMap::::'+journalsMap);
    for(Id ids : journalsMap.keySet()){
        List<Journal_Entry__c> journals = journalsMap.get(ids);
        for(Journal_Entry__c je : journals){
            je.y_hidden_journal_number__c = curValue;
        }
        system.debug('CurValue:::'+curValue);
        curValue++;
    }
    system.debug('SetSequenceNumbers--');
} */  
    
}