public class AirlineRecommendation {
	
    public class Properties implements Comparable, AirlineRecommendationInterface{
        @AuraEnabled public String operatorName {get; set;}
        @AuraEnabled public Integer rank {get; set;}
        @AuraEnabled public String color {get; set;}
        @AuraEnabled public Integer size {get; set;}
        @AuraEnabled public String operatorId{get; set;}
        @AuraEnabled public Integer weight{get; set;}
        @AuraEnabled public Integer x_axis{get; set;} 
        @AuraEnabled public Integer y_axis{get; set;}
        @AuraEnabled public String ICAO {get; set;}
        @AuraEnabled public Decimal ICAOPrecentage {get; set;}
        
        @AuraEnabled public Integer aircraftTypeCount {get; set;}
        @AuraEnabled public Integer aircraftVariantCount {get; set;}
        @AuraEnabled public Integer aircraftEngineCount {get; set;}
        @AuraEnabled public Integer vintageMinRange {get; set;}
        @AuraEnabled public Integer vintageMaxRange {get; set;}
        
        @AuraEnabled public String aircraftType {get; set;}
        @AuraEnabled public String aircraftVariant {get; set;}
        @AuraEnabled public String aircraftEngine {get; set;}
        
        @AuraEnabled public boolean isExistingCustomer {get; set;}
        
        @AuraEnabled public Integer delivery {get; set;}
        @AuraEnabled public Map<String,String> leasesList {get; set;}
        
        @AuraEnabled public List<String> leasesVsOwned {get; set;}
        @AuraEnabled public Integer leasedCount {get; set;}
        @AuraEnabled public Integer totalLeasedVsOwned {get; set;}
        
        @AuraEnabled public String creditScore {get; set;}
        @AuraEnabled public Integer numberOfAircraft {get; set;}
        @AuraEnabled public Integer numberOfDeals {get; set;}
        @AuraEnabled public Integer NumberOfSimilarType {get; set;}
         
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(((Properties)objToCompare).weight - weight);
        }    
    }
    
    public class OperatorWrapper {
        @AuraEnabled public Integer weight {get; set;}
        @AuraEnabled public String operatorName {get; set;}
        @AuraEnabled public String operatorId{get; set;}
        @AuraEnabled public Integer aircraftTypeCount {get; set;}
        @AuraEnabled public Integer aircraftVariantCount {get; set;}
        @AuraEnabled public Integer aircraftEngineCount {get; set;}
        @AuraEnabled public Integer vintageMinRange {get; set;}
        @AuraEnabled public Integer vintageMaxRange {get; set;}
        @AuraEnabled public boolean isExistingCustomer {get; set;}
    }
}