public class AircraftWrapper {
    public class Properties implements Comparable, AircraftWrapperInterface{
        @AuraEnabled public String aircraft {get; set;}
        @AuraEnabled public Integer rank {get; set;}
        @AuraEnabled public String color {get; set;}
        @AuraEnabled public Integer size {get; set;}
        
        @AuraEnabled public String fleetId{get; set;}
        @AuraEnabled public String operatorId{get; set;}
        @AuraEnabled public String operatorName{get; set;}
        @AuraEnabled public Integer weight{get; set;}
        
        @AuraEnabled public Integer aircraftTypeCount {get; set;}
        @AuraEnabled public Integer aircraftVariantCount {get; set;}
        @AuraEnabled public Integer aircraftEngineCount {get; set;}
        @AuraEnabled public Integer vintageMinRange {get; set;}
        @AuraEnabled public Integer vintageMaxRange {get; set;}
        
        @AuraEnabled public String aircraftType {get; set;}
        @AuraEnabled public String aircraftVariant {get; set;}
        @AuraEnabled public String aircraftEngine {get; set;}
        @AuraEnabled public String aircraftVintage {get; set;}
        @AuraEnabled public Decimal aircraftMtow {get; set;}
        @AuraEnabled public Integer aircraftDom {get; set;}
        
        @AuraEnabled public boolean isExistingCustomer {get; set;}
        
        @AuraEnabled public Map<String,String> leasesList {get; set;}
        @AuraEnabled public List<String> leasesVsOwned {get; set;}
        @AuraEnabled public Integer leasedCount {get; set;}
        
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(((Properties)objToCompare).weight - weight);
        }    
    }
    
    public class AircraftWeightage {
        @AuraEnabled public Integer weight {get; set;}
        @AuraEnabled public Integer aircraftTypeCount {get; set;}
        @AuraEnabled public Integer aircraftVariantCount {get; set;}
        @AuraEnabled public Integer aircraftEngineCount {get; set;}
        @AuraEnabled public Integer vintageMinRange {get; set;}
        @AuraEnabled public Integer vintageMaxRange {get; set;}
        @AuraEnabled public boolean isExistingCustomer {get; set;}
    }
}