public with sharing class FleetAcquisitionAnalysisCalulcation {

	private final Fleet_Acquisition_Analysis__c thisPBL;
	public static final Integer numberOfFrequency = 12;
	public static Fleet_Acquisition_Analysis__c curPBL {get;set;}
	private static list<Decimal> NetcashflowforIRR = new list<Decimal>();

	private static ChartCommonStruct[] currentAnnualDepreciationSchedule = new list<ChartCommonStruct>();	
	private static ChartCommonStruct[] currentChart_Buy = new list<ChartCommonStruct>();	
	private static ChartCommonStruct[] currentAmortizationTable_Buy = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_OL = new list<ChartCommonStruct>();	
	private static ChartCommonStruct[] currentAmortizationTable_FL = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_FL = new list<ChartCommonStruct>();
	
	private static Decimal currentBuyNPV = 0;
	private static Decimal currentLeaseNPV = 0;
	private static Decimal currentFinLeaseNPV = 0;
	

	public static ChartCommonStruct[] getChartDepreciation(){
		return currentAnnualDepreciationSchedule;
	}
	public static Decimal getNPV_Buy(){
		return currentBuyNPV;
	}
	public static Decimal getNPV_OL(){
		return currentLeaseNPV;
	}
	public static Decimal getNPV_FL(){
		return currentFinLeaseNPV;
	}
	public static boolean getNPV_Buy_Rank(){
		return currentBuyNPV>currentLeaseNPV && currentBuyNPV>currentFinLeaseNPV;
	}
	public static boolean getNPV_OL_Rank(){
		return currentLeaseNPV>currentBuyNPV && currentLeaseNPV>currentFinLeaseNPV;
	}	

	public static boolean getNPV_FL_Rank(){
		return currentFinLeaseNPV>currentBuyNPV && currentFinLeaseNPV>currentLeaseNPV;
	}
	public static ChartCommonStruct[] getChart_Buy(){
		return currentChart_Buy;
	}
	public static ChartCommonStruct[] getChart_OL(){
		return currentChart_OL;
	}	
	public static ChartCommonStruct[] getChart_Fl(){
		return currentChart_FL;
	}	
	public static ChartCommonStruct[] getAmortizationTable_Buy(){
		return currentAmortizationTable_Buy;
	}
	public static ChartCommonStruct[] getAmortizationTable_FL(){
		return currentAmortizationTable_FL;
	}	


				
    public FleetAcquisitionAnalysisCalulcation(ApexPages.StandardController stdController) {
        this.thisPBL = (Fleet_Acquisition_Analysis__c)stdController.getRecord();
        Id PNLId = thisPBL.Id;
        try{
        	
        	curPBL = [select ID, Name, aircraft__c, aircraft__r.Name,aircraft__r.Depreciation_Schedule__c, Interest_Rate_F__c, Monthly_Lease_Payment__c,  
                    Residual_Value__c, Security_Deposit__c, Tax_rate__c, Loan_Principle__c,Term_Months__c 
                    ,Monthly_Debt_Repayment_Amount__c,Balloon_Payment__c,Purchase_Price__c,Debt_Percentage__c
                    ,Monthly_Repayment_Amount__c,After_Tax_Discount_Rate__c,After_Tax_Discount_Rate_Annual__c
                    ,Down_Payment_Percentage_F__c,Down_Payment_Amount__c,Remaining_Balance__c,Monthly_Lease_Payment_FL__c,Monthly_Lease_Payment_FL_F__c
                    ,Lease_Rate_Factor__c,Buy_Out_Price_At_Lease_Expiration__c,Swap_Rate__c,Annual_interest_rate__c ,WACC__c
                    from Fleet_Acquisition_Analysis__c 
                    where Id = :PNLId limit 1 ];  
             
            // term can not be null
            Integer nTerm = curPBL.Term_Months__c.intValue();
            Decimal tempPMT = curPBL.Monthly_Repayment_Amount__c;
			Decimal tempPMT_FL = LeaseWareUtils.zeroIfNull(curPBL.Monthly_Lease_Payment_FL__c);
			Decimal AfterTaxDiscountRate = (curPBL.Interest_Rate_F__c/12)*(1-LeaseWareUtils.zeroIfNull(curPBL.Tax_rate__c)/100);
			//cal for buy
        	calculateChartDepreciationForBuy(curPBL.Purchase_Price__c,curPBL.aircraft__r.Depreciation_Schedule__c); 
            calculateAmortizationTable_Buy(nTerm,tempPMT,curPBL.Loan_Principle__c,curPBL.Interest_Rate_F__c,curPBL.Balloon_Payment__c);
			calculateChartSequenceForBuy(nterm,tempPMT*-1,curPBL.Tax_rate__c,curPBL.Residual_Value__c,curPBL.Balloon_Payment__c,AfterTaxDiscountRate,curPBL.Purchase_Price__c);
			//cal for Operating lease
			calculateChartSequenceForOperLease(nterm,curPBL.Tax_rate__c,AfterTaxDiscountRate,curPBL.Monthly_Lease_Payment__c,curPBL.Security_Deposit__c);
			//cal for Finance lease
			calculateAmortizationTable_FL(nTerm,tempPMT_FL,curPBL.Remaining_Balance__c,curPBL.Interest_Rate_F__c,curPBL.Balloon_Payment__c,curPBL.Down_Payment_Amount__c);
			calculateChartSequenceForFinanceLease(nterm,curPBL.Tax_rate__c,curPBL.Interest_Rate_F__c/12,-1*tempPMT_FL,curPBL.Down_Payment_Amount__c,curPBL.Buy_Out_Price_At_Lease_Expiration__c,curPBL.Residual_Value__c);

			
        }catch(exception e){
            //throw new leaseware__LeaseWareException('Invalid Nal data. Please edit and save the Prospect Buy/Lease before opening the NAL Calculation. ' +e);
        }
    }

	// parameter Finance Lease: 
	// Period, depreciationPeriod , Frequency , PMT_val
	// Display : Month#,DebtRepayment,InterestTaxShield ,Depreciation Tax Shield,Residual Value,Ballon Payment,Net Cash Savings or (Outflow)
	
	private static void calculateChartSequenceForFinanceLease(Integer nterm,Decimal taxRate,Decimal monthlyIntRate,Decimal leasePayment,Decimal Deposit,Decimal balloonPMT,Decimal MRPEndOfLease){

		Decimal tempTaxRate = taxRate==null?0:taxRate/100;
		Decimal templeasePayment = leasePayment==null?0:leasePayment;
		Decimal leasePMTTaxSheild ,tempIntTaxSheild,tempUnrelGain,tempNetcashflow= 0,tempDepreciationTaxShield;
		Decimal tempballoonPMT = balloonPMT==null?0:balloonPMT;

		// for #0th period
		currentChart_FL.add(	new ChartCommonStruct(0  ,-1*LeaseWareUtils.zeroIfNull(Deposit) ,null
	    					,null,null,null,-1*LeaseWareUtils.zeroIfNull(Deposit),null,null,null) );
		NetcashflowforIRR.clear(); //reusing
		Boolean flagIRR =false;	   
		String HoverText4, HoverText5;							
		for(Integer tempPeriodSequence=1,j=0;tempPeriodSequence<=nterm ;tempPeriodSequence++ ){
			tempIntTaxSheild = currentAmortizationTable_FL[tempPeriodSequence].d3 * tempTaxRate;
			leasePMTTaxSheild = templeasePayment * tempTaxRate;
			
			j= Math.floor((tempPeriodSequence-1.0)/numberOfFrequency).intValue();
			if(j<currentAnnualDepreciationSchedule.size()){
				tempDepreciationTaxShield = currentAnnualDepreciationSchedule[j].d3 / numberOfFrequency * tempTaxRate ; // d3 point to depreciation amount per month
				HoverText5 ='Depreciation * Tax Rate =' + (currentAnnualDepreciationSchedule[j].d3 / numberOfFrequency).setScale(0, RoundingMode.HALF_UP) + '*' + tempTaxRate;
			}else{
				tempDepreciationTaxShield = 0;
				HoverText5 =' 0 ' ;
			}		
	
			if(tempPeriodSequence == nterm){
				tempUnrelGain = -tempballoonPMT+(LeaseWareUtils.zeroIfNull(MRPEndOfLease) - tempballoonPMT) ;
				HoverText4 = 'BuyOut ='+  (-tempballoonPMT) + ' ,' + 'Unrealized Gain = ' + ((LeaseWareUtils.zeroIfNull(MRPEndOfLease) - tempballoonPMT));
				
				system.debug('tempballoonPMT'+tempballoonPMT);
				system.debug('MRPEndOfLease'+MRPEndOfLease);
				system.debug('tempUnrelGain'+tempUnrelGain);
			}
			tempNetcashflow = templeasePayment + tempIntTaxSheild +  tempDepreciationTaxShield +LeaseWareUtils.zeroIfNull(tempUnrelGain) ;
			currentChart_FL.add(	new ChartCommonStruct(tempPeriodSequence  ,null , templeasePayment
	    					,tempIntTaxSheild ,tempUnrelGain,tempDepreciationTaxShield,tempNetcashflow,null,HoverText4,HoverText5) );	
		    	
	    	NetcashflowforIRR.add(tempNetcashflow);					
		}
		
 		currentFinLeaseNPV = (LeaseWareUtils.calculateNPV(monthlyIntRate,NetcashflowforIRR) + -1*LeaseWareUtils.zeroIfNull(Deposit)).setScale(0, RoundingMode.HALF_UP);
 		//for IRR calc : added Deposit as first
 			//NetcashflowforIRR.add(0,-1*LeaseWareUtils.zeroIfNull(Deposit));
			//currentFinLeaseIRR = calIRR( NetcashflowforIRR) ;
		
	}

	private static void calculateAmortizationTable_FL(Integer term,Decimal PMT,Decimal Principle,Decimal AnuualRate,Decimal BalloonPMT,Decimal deposit){
		
		Decimal tempOutstandingBalance = Principle,tempInterestExp,tempPrinciple;
		
		// for #0th period
		currentAmortizationTable_FL.add(	new ChartCommonStruct(0  ,null ,LeaseWareUtils.zeroIfNull(deposit)
    					,0,LeaseWareUtils.zeroIfNull(Principle),null) );	
    	Decimal temp_BalloonPMT = BalloonPMT==null?0:BalloonPMT ;					
		for(Integer tempPeriodSequence=1;tempPeriodSequence<= term; tempPeriodSequence++){
			tempInterestExp = (tempOutstandingBalance * AnuualRate/(numberOfFrequency *100)) ;
			if(tempPeriodSequence== term){
				
				tempPrinciple = PMT-tempInterestExp + temp_BalloonPMT;
				tempOutstandingBalance = tempOutstandingBalance - 	tempPrinciple	;		
				
				currentAmortizationTable_FL.add(	new ChartCommonStruct(tempPeriodSequence  ,PMT +temp_BalloonPMT  
		    					, tempPrinciple,tempInterestExp,tempOutstandingBalance,null) );	
		    	continue;							
			}
			tempPrinciple = PMT-tempInterestExp ;
			tempOutstandingBalance = tempOutstandingBalance - 	tempPrinciple	;	
			
			currentAmortizationTable_FL.add(	new ChartCommonStruct(tempPeriodSequence  ,PMT 
	    					,tempPrinciple,tempInterestExp,tempOutstandingBalance,null) );	
		}
	}


	// parameter 
	// Period, depreciationPeriod , Frequency , PMT_val
	// Display : Month#,DebtRepayment,InterestTaxShield ,Depreciation Tax Shield,Residual Value,Ballon Payment,Net Cash Savings or (Outflow)
	
	private static void calculateChartSequenceForOperLease(Integer nterm,Decimal taxRate,Decimal afterTaxDiscRate,Decimal leasePayment,Decimal SecurityDeposit){

		Decimal tempTaxRate = taxRate==null?0:taxRate/100;
		Decimal tempAfterTaxDiscRate = afterTaxDiscRate==null?0:afterTaxDiscRate ;
		Decimal tempSD = null,templeasePayment = leasePayment==null?0:leasePayment;
		Decimal leasePMTTaxSheild,tempNetcashflow= 0 ;

		// for #0th period
		currentChart_OL.add(	new ChartCommonStruct(0  ,-1*LeaseWareUtils.zeroIfNull(SecurityDeposit) ,null
	    					,null,-1*LeaseWareUtils.zeroIfNull(SecurityDeposit),null) );
		NetcashflowforIRR.clear(); //reusing
		Boolean flagIRR =false;	    							
		for(Integer tempPeriodSequence=1;tempPeriodSequence<=nterm ;tempPeriodSequence++ ){
			leasePMTTaxSheild = templeasePayment * tempTaxRate;
			if(tempPeriodSequence == nterm){
				tempSD = LeaseWareUtils.zeroIfNull(SecurityDeposit);
			}
			tempNetcashflow = LeaseWareUtils.zeroIfNull(tempSD) - templeasePayment +  leasePMTTaxSheild ;
			currentChart_OL.add(	new ChartCommonStruct(tempPeriodSequence  ,tempSD , -1*templeasePayment
	    					,leasePMTTaxSheild ,tempNetcashflow,null) );	
		    	
	    	NetcashflowforIRR.add(tempNetcashflow);					
		}
		
 		currentLeaseNPV = (LeaseWareUtils.calculateNPV(afterTaxDiscRate,NetcashflowforIRR) + -1*LeaseWareUtils.zeroIfNull(SecurityDeposit)).setScale(0, RoundingMode.HALF_UP);
 		//for IRR calc : added SD as first
 			//NetcashflowforIRR.add(0,-1*LeaseWareUtils.zeroIfNull(SecurityDeposit));
			//currentLeaseIRR = calIRR( NetcashflowforIRR) ;
	}


	// parameter 
	// Period, depreciationPeriod , Frequency , PMT_val
	// Display : Month#,DebtRepayment,InterestTaxShield ,Depreciation Tax Shield,Residual Value,Ballon Payment,Net Cash Savings or (Outflow)
	
	private static void calculateChartSequenceForBuy(Integer nterm,Decimal Debt_PMT_val,Decimal taxRate,Decimal residualVal,Decimal BalloonPMT,Decimal afterTaxDiscRate,Decimal totalPrinciple){

		Decimal tempTaxRate = taxRate==null?0:taxRate/100,tempBuyNPV =0;
		Decimal tempTaxRate1 = 1+ tempTaxRate;
		Decimal tempAfterTaxDiscRate = afterTaxDiscRate==null?0:afterTaxDiscRate ;
		Decimal tempInterestTaxShield,tempDepreciationTaxShield =0,tempResidualValue =null ,tempBallonPmt=null,tempNetcashflow=0,tempDepreciation=0;
		Decimal tempCarryingValue = totalPrinciple;
			currentChart_Buy.add(	new ChartCommonStruct(0  ,null ,null
	    					,null,null,null) );
	    					
		NetcashflowforIRR.clear(); //reusing	    
		String HoverText4_ResidualValue, HoverText5;		    							
		for(Integer tempPeriodSequence=1,j=0;tempPeriodSequence<=nterm ;tempPeriodSequence++ ){
			tempInterestTaxShield = currentAmortizationTable_Buy[tempPeriodSequence].d3 * tempTaxRate;
			j= Math.floor((tempPeriodSequence-1.0)/numberOfFrequency).intValue();
			if(j<currentAnnualDepreciationSchedule.size()){
				tempDepreciation = currentAnnualDepreciationSchedule[j].d3 / numberOfFrequency ; 
				tempDepreciationTaxShield = tempDepreciation * tempTaxRate ; // d3 point to depreciation amount per month
			}else{
				tempDepreciation =0;
				tempDepreciationTaxShield = 0;
			}

			if(tempCarryingValue>0) {
				tempCarryingValue = tempCarryingValue - tempDepreciation ;
				if(tempCarryingValue<0) tempCarryingValue =0;
			}

			if(tempPeriodSequence == nterm){
				tempResidualValue = residualVal==null?0:residualVal;

				HoverText4_ResidualValue = 
								'Residual Value (Sale Price) : ' + LeaseWareUtils.formatCurrency(tempResidualValue,0)
							+	'\n Carrying Value : '+ LeaseWareUtils.formatCurrency(totalPrinciple,0)
							+	'\n Accumulated Depreciation : '+ LeaseWareUtils.formatCurrency(totalPrinciple - tempCarryingValue,0)															
							+	'\n Net Book Value : '+ LeaseWareUtils.formatCurrency(tempCarryingValue,0)
							+	'\n Gain/Loss On Sale Of Aircraft : '+ LeaseWareUtils.formatCurrency(tempResidualValue-tempCarryingValue,0)
							;
				
				if(tempCarryingValue>tempResidualValue) {

					HoverText4_ResidualValue = HoverText4_ResidualValue 
									+	'\n Tax Payment/Shield : '+ LeaseWareUtils.formatCurrency( (tempCarryingValue - tempResidualValue) * tempTaxRate,0)
									+	'\n Net Proceeds From Sale Of Aircraft : '+ LeaseWareUtils.formatCurrency(tempResidualValue + (tempCarryingValue - tempResidualValue) * tempTaxRate,0) ;
					tempResidualValue = tempResidualValue + (tempCarryingValue - tempResidualValue) * tempTaxRate ;									
				}
				else if(tempCarryingValue<tempResidualValue){
					HoverText4_ResidualValue = HoverText4_ResidualValue 
					 				+	'\n Tax Payment/Shield : '+ LeaseWareUtils.formatCurrency( -1* (tempResidualValue - tempCarryingValue) * tempTaxRate ,0)
					 				+	'\n Net Proceeds From Sale Of Aircraft : '+ LeaseWareUtils.formatCurrency(tempResidualValue - (tempResidualValue - tempCarryingValue) * tempTaxRate,0) ;
					tempResidualValue = tempResidualValue - (tempResidualValue - tempCarryingValue) * tempTaxRate ;					 				
				}
				else {
					HoverText4_ResidualValue = HoverText4_ResidualValue 
									+	'\n Tax Payment/Shield : '+ LeaseWareUtils.formatCurrency( tempResidualValue * tempTaxRate ,0)
									+	'\n Net Proceeds From Sale Of Aircraft : '+ LeaseWareUtils.formatCurrency( tempResidualValue  * (1 - tempTaxRate ),0) ;
					tempResidualValue = tempResidualValue    - tempResidualValue * tempTaxRate  ;									
				}
				tempBallonPmt = BalloonPMT==null?0:BalloonPMT*-1;
			}
			tempNetcashflow = Debt_PMT_val + tempInterestTaxShield +  tempDepreciationTaxShield 
					+   LeaseWareUtils.zeroIfNull(tempResidualValue) + LeaseWareUtils.zeroIfNull(tempBallonPmt) ;
			currentChart_Buy.add(	new ChartCommonStruct(tempPeriodSequence  ,Debt_PMT_val , tempInterestTaxShield
	    					,tempDepreciationTaxShield ,tempResidualValue,tempBallonPmt,tempNetcashflow,null,HoverText4_ResidualValue,HoverText5) );	

	    	NetcashflowforIRR.add(tempNetcashflow);					
		}
		
		
 		currentBuyNPV = LeaseWareUtils.calculateNPV(afterTaxDiscRate,NetcashflowforIRR).setScale(0, RoundingMode.HALF_UP);
 			//currentBuyIRR = calIRR( NetcashflowforIRR) ;
	}

	//calculateAmortizationTable_Buy(term,curPBL.Monthly_Repayment_Amount__c,curPBL.Loan_Principle__c,curPBL.Interest_Rate_F__c); 
	private static void calculateAmortizationTable_Buy(Integer term,Decimal PMT,Decimal Principle,Decimal AnuualRate,Decimal BalloonPMT){
		
		Decimal tempOutstandingBalance = Principle,tempInterestExp,tempPrinciple;
		// Period #0
		currentAmortizationTable_Buy.add(	new ChartCommonStruct(0  ,null ,null
    					,null,null,tempOutstandingBalance) );	
    	Decimal temp_BalloonPMT = BalloonPMT==null?0:BalloonPMT ;					
		for(Integer tempPeriodSequence=1;tempPeriodSequence<= term; tempPeriodSequence++){
			tempInterestExp = (tempOutstandingBalance * AnuualRate/(numberOfFrequency *100)) ;
			if(tempPeriodSequence== term){
				
				tempPrinciple = PMT-tempInterestExp + temp_BalloonPMT;
				tempOutstandingBalance = tempOutstandingBalance - 	tempPrinciple	;		
				
				currentAmortizationTable_Buy.add(	new ChartCommonStruct(tempPeriodSequence  ,PMT +temp_BalloonPMT  ,null
		    					,tempInterestExp, tempPrinciple,tempOutstandingBalance) );	
		    	continue;							
			}
			tempPrinciple = PMT-tempInterestExp ;
			tempOutstandingBalance = tempOutstandingBalance - 	tempPrinciple	;	
			
			currentAmortizationTable_Buy.add(	new ChartCommonStruct(tempPeriodSequence  ,PMT ,null
	    					,tempInterestExp,tempPrinciple,tempOutstandingBalance) );	
		}
		
		
		
	}


    private static void calculateChartDepreciationForBuy(decimal principle,Id DSId){
    	
    	Decimal tempPrinciple = principle==null?0:principle;
    	if(DSId == null) return;
    	Depreciation_Rate__c[] DRList = [select Depreciation_Rate__c,Period_Sequence__c from Depreciation_Rate__c
    							where Depreciation_schedule__c = :DSId 
    							order by Period_Sequence__c];
    	
    	
    	Decimal tempDeprication,tempComulativeDeprication=0;
    	Integer tempPeriodSequence = 1;
    	for(Depreciation_Rate__c curDRRec:DRList){
    		
    		tempDeprication = (curDRRec.Depreciation_Rate__c * tempPrinciple) /100;
    		tempComulativeDeprication =tempComulativeDeprication +  tempDeprication;
    		currentAnnualDepreciationSchedule.add(	new ChartCommonStruct(tempPeriodSequence  ,tempPeriodSequence*numberOfFrequency ,curDRRec.Depreciation_Rate__c
    					,tempDeprication,tempPrinciple - tempComulativeDeprication,null));
    		
    		
    		tempPeriodSequence++;
    	}
    	
    	
    }


	// class structure
	public class ChartCommonStruct{
		public Integer I1{get;set;}
		public Decimal d1{get;set;}
		public Decimal d2{get;set;} // print decimal till 2 precision scale
		public Decimal d3 {get;set;}
		public Decimal d4 {get;set;}
		public Decimal d5 {get;set;}
		public Decimal d6 {get;set;}
		public Decimal d7 {get;set;}
		public string  HT5 {get;set;}
		public string  HT4 {get;set;}
		
        public ChartCommonStruct(Integer I1,Decimal D1,Decimal d2,Decimal d3,Decimal d4,Decimal d5){
			this.I1=I1;
			if(d1 != null) this.d1 = d1.setScale(0, RoundingMode.HALF_UP); else this.d1 = null;
			if(d2 != null) this.d2 = d2.setScale(2, RoundingMode.HALF_UP); else this.d2 = null;
			if(d3 != null) this.d3 = d3.setScale(0, RoundingMode.HALF_UP); else this.d3 = null;
			if(d4 != null) this.d4 = d4.setScale(0, RoundingMode.HALF_UP); else this.d4 = null;
			if(d5 != null) this.d5 = d5.setScale(0, RoundingMode.HALF_UP); else this.d5 = null;
					
        }

        public ChartCommonStruct(Integer I1,Decimal D1,Decimal d2,Decimal d3,Decimal d4,Decimal d5,Decimal d6,Decimal d7,string HText4,string HText5){
			this.I1=I1;
			if(d1 != null) this.d1 = d1.setScale(0, RoundingMode.HALF_UP); else this.d1 = null;
			if(d2 != null) this.d2 = d2.setScale(2, RoundingMode.HALF_UP); else this.d2 = null;
			if(d3 != null) this.d3 = d3.setScale(0, RoundingMode.HALF_UP); else this.d3 = null;
			if(d4 != null) this.d4 = d4.setScale(0, RoundingMode.HALF_UP); else this.d4 = null;
			if(d5 != null) this.d5 = d5.setScale(0, RoundingMode.HALF_UP); else this.d5 = null;
			if(d6 != null) this.d6 = d6.setScale(0, RoundingMode.HALF_UP); else this.d6 = null;
			if(d7 != null) this.d7 = d7.setScale(0, RoundingMode.HALF_UP); else this.d7 = null;	
			this.HT5 = HText5;		
			this.HT4 = HText4;
        }
        
	}

}