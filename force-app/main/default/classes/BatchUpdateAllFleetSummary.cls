public class BatchUpdateAllFleetSummary implements 
Database.Batchable<sObject>, Database.Stateful { 
    
    Integer numOfFleetSummaryUpdated;
    Integer numOfFleetSummaryDeleted;
    private static integer batchSize = 50;
    String  exceptionMessage='';
    string logMessageString='';
    
    public BatchUpdateAllFleetSummary(){
        numOfFleetSummaryUpdated =0;
        numOfFleetSummaryDeleted =0;
    }
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {   
        return Database.getQueryLocator(
            [select id from Operator__c order by name ]
        );
    }
    
    public void execute(Database.BatchableContext BC, List<Operator__c> listOfOperatorIds)
    {
        
       
            Savepoint spStart = Database.setSavepoint();
            try{
                updateFleetSummary(listOfOperatorIds);
            }catch(Exception ex){
                Database.rollback(spStart);
                exceptionMessage += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
                system.debug('Exception : ' + ex +  + '===' +exceptionMessage);
            }
        
   	}
    
    
    public void updateFleetSummary(List<Operator__c> listOfOperatorIds){
       
            system.debug('Update Fleet Summary.....' + listOfOperatorIds);
            map<string,Fleet_Summary__c> mapFS= new map<string,Fleet_Summary__c>();
            list<Fleet_Summary__c> listFSAll= [select id,Age__c,Aircraft_Type__c,Controlling_Airline__c,Count__c
                                               ,Lessor__c,Manager__c,Operator__c,Status__c,Value_m__c,Variant__c
                                               from Fleet_Summary__c where Operator__c IN : listOfOperatorIds];
            string strkey;
            for(Fleet_Summary__c currec : listFSAll){
                strkey= ''+ currec.Age__c+ currec.Aircraft_Type__c+ currec.Controlling_Airline__c+ currec.Count__c+ currec.Manager__c
                    + currec.Operator__c+ currec.Status__c+ currec.Value_m__c+ currec.Variant__c;
                mapFS.put(strkey,currec);
            }
            
            //2. insert records to fleet summary without controling airline    
            map < id, Operator__c > mapOp = new map < id, Operator__c > ([select id, name, Group_Parent__c, Group_Parent__r.name from Operator__c where id IN:listOfOperatorIds]);
            
            list < AggregateResult > listWF = [select Aircraft_Operator__c AircraftOperator, AircraftType__c AircraftType, AircraftVariant__c AircraftVariant, AircraftStatus__c AircraftStatus, Manager__c Manager, count(name) cnt, avg(AircraftAge__c) avgage, sum(CurrentMarketValue_m__c) avgMV
                                               from Global_Fleet__c
                                               where  Aircraft_Operator__c IN : listOfOperatorIds
                                               group by Aircraft_Operator__c, AircraftType__c, AircraftVariant__c, AircraftStatus__c, Manager__c
                                              ];
            system.debug('size=' + listWF.size());
            Operator__c tempOp;
            list < Fleet_Summary__c > summList = new list < Fleet_Summary__c > ();
            Id Cont_Oper_Id, OrgOpId;
            string namestr;
            for (AggregateResult curRec: listWF) {
                tempOp = mapOp.get((Id) curRec.get('AircraftOperator'));
                system.debug(curRec.get('AircraftOperator') + '=' + curRec.get('AircraftType') + '=' + curRec.get('AircraftVariant') + '=' + curRec.get('AircraftStatus') + '=' + curRec.get('cnt') + '===' + tempOp.Group_Parent__r.name);
                Cont_Oper_Id = tempOp.Group_Parent__c == null ? tempOp.Id : tempOp.Group_Parent__c;
                namestr = tempOp.name + ' ' + (string) curRec.get('AircraftType') + ' ' + (string) curRec.get('AircraftVariant') + ' ' + (string) curRec.get('AircraftStatus');
                if (namestr != null && namestr.length() > 79) namestr = namestr.left(77) + '...';
                
                strkey= ''+(Decimal) curRec.get('avgage')+ (string) curRec.get('AircraftType')+ Cont_Oper_Id+ (Integer) curRec.get('cnt')+ (string) curRec.get('Manager')
                    + (Id) curRec.get('AircraftOperator')+ (string) curRec.get('AircraftStatus')+ (Decimal) curRec.get('avgMV') + (string) curRec.get('AircraftVariant');
                if(mapFS.containsKey(strkey)){
                    summList.add(mapFS.get(strkey));
                    mapFS.remove(strkey);
                }else{
                    summList.add(new Fleet_Summary__c(
                        name = namestr, Aircraft_Type__c = (string) curRec.get('AircraftType'), Controlling_Airline__c = Cont_Oper_Id, Count__c = (Integer) curRec.get('cnt'), Operator__c = (Id) curRec.get('AircraftOperator'), Status__c = (string) curRec.get('AircraftStatus'), Variant__c = (string) curRec.get('AircraftVariant'), Manager__c = (string) curRec.get('Manager'), Age__c = (Decimal) curRec.get('avgage'), Value_m__c = (Decimal) curRec.get('avgMV')
                    ));          
                }
                
            }
            system.debug('summList size=' + summList.size());
            // Later we will insert in 1 shot insert summList;  
            Cont_Oper_Id = null;
            OrgOpId = null;
            //3. insert records to fleet summary with controling airline
            for (AggregateResult curRec: listWF) {
                tempOp = mapOp.get((Id) curRec.get('AircraftOperator'));
                system.debug(curRec.get('AircraftOperator') + '=' + curRec.get('AircraftType') + '=' + curRec.get('AircraftVariant') + '=' + curRec.get('AircraftStatus') + '=' + curRec.get('cnt') + '===' + tempOp.Group_Parent__r.name);
                Cont_Oper_Id = tempOp.Group_Parent__c == null ? tempOp.Id : tempOp.Group_Parent__c;
                if (tempOp.Group_Parent__c != null) {
                    namestr = tempOp.name + ' ' + (string) curRec.get('AircraftType') + ' ' + (string) curRec.get('AircraftVariant') + ' ' + (string) curRec.get('AircraftStatus');
                    if (namestr != null && namestr.length() > 79) namestr = namestr.left(77) + '...';
                    
                    strkey= ''+(Decimal) curRec.get('avgage')+ (string) curRec.get('AircraftType')+ Cont_Oper_Id+ (Integer) curRec.get('cnt')+ (string) curRec.get('Manager')
                        + (Id) curRec.get('AircraftOperator')+ (string) curRec.get('AircraftStatus')+ (Decimal) curRec.get('avgMV') + (string) curRec.get('AircraftVariant');
                    if(mapFS.containsKey(strkey)){
                        summList.add(mapFS.get(strkey));
                        mapFS.remove(strkey);
                    }else{
                        summList.add(new Fleet_Summary__c(
                            name = namestr, Aircraft_Type__c = (string) curRec.get('AircraftType'), Controlling_Airline__c = (Id) curRec.get('AircraftOperator'), Count__c = (Integer) curRec.get('cnt'), Operator__c = (Id) curRec.get('AircraftOperator'), Status__c = (string) curRec.get('AircraftStatus'), Variant__c = (string) curRec.get('AircraftVariant'), Manager__c = (string) curRec.get('Manager'), Age__c = (Decimal) curRec.get('avgage'), Value_m__c = (Decimal) curRec.get('avgMV')
                        ));
                    }
                }
                
            }
            system.debug('2summList size=' + summList.size());
            // Later we will insert in 1 shot insert summList;  
            
            map < string, Id > maplessorId = new map < string, Id > ();
            list < AggregateResult > listLessorWF = [select Related_Owner__c R_OW, Manager__c MAN from Global_Fleet__c
                                                     where Related_Owner__c != null and Aircraft_Operator__c IN:listOfOperatorIds
                                                     group by Related_Owner__c, Manager__c
                                                    ];
            system.debug('listLessorWF=' + listLessorWF.size());
            for (AggregateResult curRec: listLessorWF) {
                maplessorId.put((string) curRec.get('MAN'), (ID) curRec.get('R_OW'));
            }
            system.debug('maplessorId=' + maplessorId.size());
            for (Fleet_Summary__c curRec: summList) {
                curRec.Lessor__c = maplessorId.get(curRec.Manager__c);
            }

            numOfFleetSummaryUpdated+=summList.size();
            upsert summList;
            numOfFleetSummaryDeleted +=mapFS.size();
            delete mapFS.values();
      
        
 }
    
    public void finish(Database.BatchableContext BC) {
        
        if(String.isEmpty(exceptionMessage)){
       
        logMessageString += '<BR>' +'\n No of fleet summary updated = '+ numOfFleetSummaryUpdated +'\n';
       
        LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
        LeaseWareStreamUtils.emailWithAttachedLog((new String[] { UserInfo.getUserEmail() }) ,' Update Fleet Summary All ' ,logMessageString,null);
        LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
        }else{
            LeaseWareUtils.sendEmail('Update Fleet Summary All', 'There was an unexpected error updating all Fleet Summary. Please contact your LeaseWorks System Administrator. \n \n \n  <br/><br/>Reason :'+ exceptionMessage, UserInfo.getUserEmail());
                
        }
        
      
        
    }

    @AuraEnabled
	public static List<ListView> getListViews() {
    system.debug('getListViews----');
    String sObjectname  = getNamespacePrefix()+ 'Global_Fleet__c';
    List<ListView> listviews =
        [SELECT Id, Name FROM ListView WHERE SobjectType = :sObjectname];
      return listViews;
    }
    
    @AuraEnabled
    public static string getNamespacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled
    public static void updateFleetSummaryAll(){
       Database.executeBatch(new BatchUpdateAllFleetSummary(),getBatchSize());
    }
    public static  Integer getBatchSize(){
        ServiceSetting__c lwServiceSettings = ServiceSetting__c.getInstance('UpdateAllFleetSummaryBatch');
        if(lwServiceSettings != null && lwServiceSettings.Batch_Size__c != null)batchSize = integer.valueOf(lwServiceSettings.Batch_Size__c);
        return batchSize;
    }

}