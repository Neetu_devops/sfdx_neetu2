public with sharing class ReturnConditionForTSOController {
    
    @auraEnabled
    public static List<WrapperClass> fetchReturnCondition (Id ScenarioId){
        try{
            List<WrapperClass> lstWrapperClass = new List<WrapperClass>();
            Scenario_Input__c scenarioInp = ForecastSOQLServices.getScenarioInput(ScenarioId);
            List<Scenario_Component__c> lstSecnarioCmp =  ForecastSOQLServices.getScenarioOutput(scenarioInp.Id);
            
            //Second Header Set
            Set<String> setSubHeader = new Set<String>{'Month','FC','FH'};
                
                Map<String,Monthly_Scenario__c> mapMonthlyScenarioWithType = new Map<String,Monthly_Scenario__c>();
            Map<String,List<WrapperClass>> mapAssemblyTypeWithWrapperClassList = new Map<String,List<WrapperClass>>();
            //Map monthlyScenarioAsPerType
            //map monthly scenario as per type with return month true
            for(Monthly_Scenario__c ms: [SELECT Name,Hours_Remaining__c,Cycles_Remaining__c,Fx_Component_Output__r.Assembly_Display_Name__c,
                                         Months_Remaining__c,RateBasis_Value__c,Shortfall_Month__c,
                                         Return_Month__c, Fx_Component_Output__r.Rate_Basis__c,Fx_Component_Output__r.FH__c, 
                                         Month_Since_Overhaul__c,Fx_Component_Output__r.Name,FH__c,TSN__c,Shortfall_FC__c,
                                         Shortfall_FH__c, FC__c, Fx_Component_Output__r.Type__c,TSO__c,CSO__c, Fx_Component_Output__r.Event_Type__c
                                         FROM Monthly_Scenario__c
                                         WHERE Fx_Component_Output__r.Fx_General_Input__c =: ScenarioId 
                                         AND Return_Month__c = true
                                         AND Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL 
                                         ORDER BY Start_Date__c,Fx_Component_Output__r.Type__c  ]){
                                             if(!ms.Fx_Component_Output__r.Type__c.contains('General'))
                                                 mapMonthlyScenarioWithType.put(ms.Fx_Component_Output__r.Assembly_Display_Name__c,ms);
                                             
                                         }
            
            if(lstSecnarioCmp.size()>0){
                for(Scenario_Component__c scenarioComponent : lstSecnarioCmp){
                    if(scenarioComponent.Type__c != 'General'){
                        WrapperClass wrapper = new WrapperClass();
                        
                        wrapper.assemblyDisplayName = scenarioComponent.Assembly_Display_Name__c;
                        wrapper.assemblyType = scenarioComponent.Type__c;
                        wrapper.eventType = scenarioComponent.Event_Type__c;
                        
                        //modified april 10, 2019
                        //wrapper.returnConditionApplicability = scenarioInp.Return_Condition_Applicability__c;
                        //wrapper.returnConditionApplicability = scenarioComponent.Fx_General_Input__r.Asset__r.Lease__r.Minimum_Return_Conditions__r.RC_Applicability__c;
                        
                        
                        //modified december 26, 2019
                        wrapper.returnConditionApplicability = scenarioComponent.RC_Applicability__c;
                        
                        wrapper.isLLP  = scenarioComponent.Event_Type__c.contains('LLP') ? true : false;
                        Monthly_Scenario__c ms = new Monthly_Scenario__c();
                        if(mapMonthlyScenarioWithType.containsKey(scenarioComponent.Assembly_Display_Name__c))
                            ms = mapMonthlyScenarioWithType.get(scenarioComponent.Assembly_Display_Name__c);
                        
                        if(setSubHeader.size()>0){
                            for(String subHeaderVal : setSubHeader){
                                InnerWrapperClass innerWrapper = new InnerWrapperClass();
                                if(subHeaderVal == 'Month'){
                                    innerWrapper.name = subHeaderVal;
                                    innerWrapper.startOfLease = scenarioComponent.MSO_Start_of_Lease__c;
                                    innerWrapper.remainingLifePerContract = scenarioComponent.RC_Months__c;
                                    if(scenarioComponent.First_Event__c){
                                        innerWrapper.performanceLimit = scenarioComponent.Life_Limit_Interval_1_Months__c;
                                    }else{
                                        innerWrapper.performanceLimit = scenarioComponent.Life_Limit_Interval_2_Months__c;
                                    }
                                    //if(ms.Return_Month__c == True){
                                    innerWrapper.usageSincePR = ms.Month_Since_Overhaul__c;
                                    //Decimal calcShortfall = innerWrapper.remainingLifePerContract - innerWrapper.projectedReturnCondition ;
                                    if(wrapper.returnConditionApplicability == 'Remaining To Event' )
                                        innerWrapper.projectedReturnCondition = ms.Months_Remaining__c;
                                    else
                                        innerWrapper.projectedReturnCondition = ms.Month_Since_Overhaul__c;
                                    
                                    innerWrapper.shortfall = ms.Shortfall_Month__c;
                                    //innerWrapper.returnInBetterCondition = ms.Return_Better_Condition_Month__c ;
                                    //}
                                    
                                }else if(subHeaderVal == 'FC'){
                                    innerWrapper.name = subHeaderVal;
                                    innerWrapper.startOfLease = scenarioComponent.CSO_Start_of_Lease__c;
                                    innerWrapper.remainingLifePerContract = scenarioComponent.RC_FC__c == null ? 0.0 : scenarioComponent.RC_FC__c;
                                    if(scenarioComponent.First_Event__c){
                                        innerWrapper.performanceLimit = scenarioComponent.Life_Limit_Interval_1_Cycles__c == null ? 0.0 : scenarioComponent.Life_Limit_Interval_1_Cycles__c;
                                    }else{
                                        innerWrapper.performanceLimit = scenarioComponent.Life_Limit_Interval_2_Cycles__c == null ? 0.0 : scenarioComponent.Life_Limit_Interval_2_Cycles__c;
                                    }
                                    
                                    //if(ms.Return_Month__c == True){
                                    if(wrapper.returnConditionApplicability == 'Remaining To Event' )
                                        innerWrapper.projectedReturnCondition = ms.Cycles_Remaining__c;
                                    else
                                        innerWrapper.projectedReturnCondition = ms.CSO__c;
                                    
                                    innerWrapper.usageSincePR = ms.CSO__c;
                                    //Decimal calcShortfall = innerWrapper.remainingLifePerContract - innerWrapper.projectedReturnCondition ;
                                    innerWrapper.shortfall = ms.Shortfall_FC__c;
                                    //innerWrapper.returnInBetterCondition = ms.Return_Better_Condition_FC__c ;
                                    // }
                                    
                                }else if(subHeaderVal == 'FH'){
                                    innerWrapper.name = subHeaderVal;
                                    if(scenarioComponent.Event_Type__c.contains('LLP')){
                                        innerWrapper.startOfLease = null;
                                        innerWrapper.remainingLifePerContract = null;
                                        innerWrapper.performanceLimit = null;
                                        innerWrapper.projectedReturnCondition = null;
                                        innerWrapper.usageSincePR =null;
                                        innerWrapper.shortfall = null; 
                                    }else{
                                        innerWrapper.startOfLease = scenarioComponent.TSO_Start_of_Lease__c;
                                        innerWrapper.remainingLifePerContract = scenarioComponent.RC_FH__c == null ? 0.0 : scenarioComponent.RC_FH__c;
                                        // innerWrapper.tso = ms.TSO__c == null ? 0.0 : ms.TSO__c ;
                                        if(scenarioComponent.First_Event__c){
                                            innerWrapper.performanceLimit = scenarioComponent.Life_Limit_Interval_1_Hours__c == null ? 0.0 : scenarioComponent.Life_Limit_Interval_1_Hours__c;
                                        }else{
                                            innerWrapper.performanceLimit = scenarioComponent.Life_Limit_Interval_2_Hours__c == null ? 0.0 : scenarioComponent.Life_Limit_Interval_2_Hours__c;
                                        }
                                        
                                        
                                        //if(ms.Return_Month__c == True){
                                        if(wrapper.returnConditionApplicability == 'Remaining To Event' )
                                            innerWrapper.projectedReturnCondition = ms.Hours_Remaining__c;
                                        else
                                            innerWrapper.projectedReturnCondition = ms.TSO__c;
                                        innerWrapper.usageSincePR = ms.TSO__c;
                                        
                                        //Decimal calcShortfall = innerWrapper.remainingLifePerContract - innerWrapper.projectedReturnCondition ;
                                        innerWrapper.shortfall = ms.Shortfall_FH__c;
                                        //innerWrapper.returnInBetterCondition = ms.Return_Better_Condition_FH__c ;
                                        //}
                                    }
                                    
                                }
                                wrapper.listInnerWrapperClass.add(innerWrapper);
                            }
                        }
                        lstWrapperClass.add(wrapper);
                    } 
                } 
            }
            
            for(WrapperClass wc1 : lstWrapperClass){
                for(WrapperClass wc2 : lstWrapperClass){
                    if(wc1.assemblyType == wc2.assemblyType && wc2.eventType == 'LLP Replacement' && wc1.eventType != 'LLP Replacement'){
                        wc1.LLP = wc2;
                    }
                }    
            }   
            System.debug('lstWrapperClass'+lstWrapperClass);            
            return lstWrapperClass;
        }catch(Exception e){
            throw new AuraHandledException('An internal Server Error has Occured: '+e.getMessage()+' Line Number: '+e.getLineNumber());
        }
        
    }
    
    public class WrapperClass{
        @AuraEnabled public String assemblyDisplayName{get; set;}
        @AuraEnabled public String assemblyType{get; set;}
        @AuraEnabled public String eventType{get; set;}
        @AuraEnabled public Boolean isLLP{get; set;}
        @AuraEnabled public String returnConditionApplicability{get; set;}
        @AuraEnabled public WrapperClass LLP{get;set;}
        @AuraEnabled public List<InnerWrapperClass> listInnerWrapperClass{get; set;}
        public WrapperClass(){
            this.listInnerWrapperClass = new List<InnerWrapperClass>();
        }
    }
    
    public class InnerWrapperClass{
        @AuraEnabled public String name{get; set;}
        @AuraEnabled public Decimal startOfLease{get; set;}
        @AuraEnabled public Decimal remainingLifePerContract{get; set;}
        //@AuraEnabled public Decimal tso{get; set;}
        @AuraEnabled public Decimal usageSincePR{get; set;}
        //@AuraEnabled public Decimal returnInBetterCondition{get; set;}
        @AuraEnabled public Decimal performanceLimit{get; set;}
        @AuraEnabled public Decimal projectedReturnCondition{get; set;}
        @AuraEnabled public Decimal shortfall{get; set;}
    }
}