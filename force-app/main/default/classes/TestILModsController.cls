@isTest
public class TestILModsController {
	
    
    @isTest
    static void loadTestCases(){
        
        Country__c country =new Country__c(Name='United States');
        insert country;

        Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
     // opr.Country__c = 'United States';
        opr.Country_Lookup__c = country.id;
        opr.Region__c = 'Asia';
        insert opr;
    
        Customer_Interaction_Log__c ilog = new Customer_Interaction_Log__c(
             Name = 'Test',
             Related_To_Operator__c = opr.Id);
		insert ilog;	
        
        String namespace = ILModsController.getNamespacePrefix();
        String objName = namespace + 'Customer_Interaction_Log__c'; 
        ILModsController.getColumns( objName, null);
        ILModsController.getColumns( objName, 'leaseworks__Quick');
        ILModsController.getNamespacePrefix();
        
    }
    
    
    
    
}