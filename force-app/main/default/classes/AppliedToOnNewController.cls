public with sharing class AppliedToOnNewController extends PaymentNewControllerBase {
    public AppliedToOnNewController(ApexPages.StandardController ingored) {
        super(Invoice__c.SObjectType, Applied_To__c.SObjectType);
    }
    
}