public class BatchCreateSnapshot implements Database.Batchable<sObject>, Database.Stateful{
    
    public List<ParentChild> mParentChildList; 
    public Map<String, List<Id>> mParentChildIdMap;
    public List<SnapshotObjectField> childFieldSnopObj;
    public List<RecordDetailJson> recordDetailList;
    public List<ContentVersion> contentVersionList;
    public String rel_obj_api;
    public String childApi;
    public String parent;
    public Snapshot__c mSnapshot;
    public Set<String> IdCheckSet;
    
    Map<Id, List<RecordDetailChildJson>> parentChildMap = new Map<Id, List<RecordDetailChildJson>>();
    List<Id> childIdList = new List<Id>();
    
    public BatchCreateSnapshot(Snapshot__c snapshot, List<ParentChild> parentChildList, Map<String, List<Id>> parentChildIds, List<RecordDetailJson> recordDetail, Set<String> mIdCheckSet) { 
        System.debug('BatchCreateSnapshot constructor parentChildList '+parentChildList );
        System.debug('BatchCreateSnapshot constructor parentChildIds '+parentChildIds );
        mParentChildList = parentChildList;
        mParentChildIdMap = parentChildIds;
        recordDetailList = recordDetail;
        contentVersionList = new List<ContentVersion>();
        mSnapshot = snapshot;
        IdCheckSet = mIdCheckSet;
        if(IdCheckSet == null)
        	IdCheckSet = new Set<String>();
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('BatchCreateSnapshot mParentChildList '+mParentChildList ); 
        if(mParentChildList != null && mParentChildList.size() > 0) {
            ParentChild parentChild = mParentChildList.get(0);
            System.debug('BatchCreateSnapshot parentChild '+parentChild ); 
            System.debug('BatchCreateSnapshot mParentChildIdMap '+mParentChildIdMap ); 
            if(parentChild != null && parentChild.child != null) {
                String obj = parentChild.child;
                rel_obj_api = parentChild.related_obj_api;
                parent = parentChild.parent;
                childApi = obj;
                
                childFieldSnopObj = getFieldsForObject(obj);
                if(childFieldSnopObj == null)
                    return null;
                
                Set<String> fieldList = new Set<String>();
                for(SnapshotObjectField field : childFieldSnopObj) {
                    fieldList.add(field.ApiName);
                }
                String fields = String.join(new List<String>(fieldList),',');
                
                String query = 'select '+fields+' from '+obj+ ' where '; 
                List<Id> parentIdList = mParentChildIdMap.get(parent);
                System.debug('BatchCreateSnapshot parentIdList '+parentIdList);
                if(parentIdList == null)
                    parentIdList = new List<Id>();
                if(!String.isBlank(parentChild.related_obj_api))
                    query = query + parentChild.related_obj_api ; 
                else 
                    query = query + ' Id ';
                query = query + ' in :parentIdList' ;
                
                System.debug('BatchCreateSnapshot obj '+obj +' , rel_obj_api '+rel_obj_api +' , parentIdList: '+parentIdList); 
                return Database.getQueryLocator(query);
            }
        }
        return null;
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> objDataList) {
        if(objDataList == null || objDataList.size() <= 0) return;
        for(SObject objData: objDataList) {
            childIdList.add(objData.Id);
            String name = null;
            if(objData.get('Name') != null) 
                name = String.valueOf(objData.get('Name'));
            
            RecordDetailChildJson itemJson = new RecordDetailChildJson();
            itemJson.childId = objData.Id;
            itemJson.childName = name;
            for(SnapshotObjectField snap: childFieldSnopObj) {
                if(objData.get(snap.ApiName) != null) {
                    try {   
                        snap.Value = String.valueOf(objData.get(snap.ApiName));
                        
                        //Is to store parent child ids for indexing
                        if(rel_obj_api.equalsIgnoreCase(snap.ApiName) && snap.Value != null) {
                            RecordDetailChildJson childJson = new RecordDetailChildJson();
                            childJson.childId = objData.Id;
                            childJson.childName = name;
                            if(parentChildMap.get(snap.Value) == null) {
                                List<RecordDetailChildJson> childIds = new List<RecordDetailChildJson>();
                                childIds.add(childJson);
                                parentChildMap.put(snap.Value, childIds);
                            } else {
                                List<RecordDetailChildJson> childIds = parentChildMap.get(snap.Value);
                                childIds.add(childJson);
                                parentChildMap.put(snap.Value, childIds);
                            }
                        } 
                    }
                    catch(Exception e) {
                        snap.Value = null;
                    }
                }
                else 
                    snap.Value = null;
            }
            String title = childApi + '-' + objData.Id;
            createJson(mSnapshot.Id, title, childFieldSnopObj, objData.Id); 
            
            if(parent.equals(childApi)) {
                List<RecordDetailChildJson> itemList = new List<RecordDetailChildJson>();
                itemList.add(itemJson);
                RecordDetailJson recordDetailJson = new RecordDetailJson();
                recordDetailJson.child_api = childApi; 
                recordDetailJson.related_obj_api = '';
                recordDetailJson.parentId = null;
                recordDetailJson.itemIdList = itemList;
                if(recordDetailList == null)
                    recordDetailList = new List<RecordDetailJson>();
                recordDetailList.add(recordDetailJson);
            }
            
            
        }
        System.debug('BatchCreateSnapshot execute childIdList: '+childIdList + ' parent '+parent + ' recordDetailList: ' +recordDetailList);       
    }
    
    public void finish(Database.BatchableContext BC) {
        childFieldSnopObj = null;
        if(childIdList.size() > 0) {
            String sObjName = childIdList[0].getSObjectType().getDescribe().getName();
            List<Id> parentIdList = mParentChildIdMap.get(sObjName);
            if(mParentChildIdMap.get(sObjName) == null) {
                mParentChildIdMap.put(sObjName, childIdList);
            } else {
                List<Id> ids = mParentChildIdMap.get(sObjName);
                ids.addAll(childIdList);
                mParentChildIdMap.put(sObjName, childIdList);
            }
            if(parentChildMap.size() > 0) {
                for (Id key : parentChildMap.keySet()) {
                    List<RecordDetailChildJson> localChildList = parentChildMap.get(key);
                    RecordDetailJson recordDetailJson = new RecordDetailJson();
                    recordDetailJson.child_api = childApi; 
                    recordDetailJson.related_obj_api = rel_obj_api;
                    recordDetailJson.parentId = key;
                    recordDetailJson.itemIdList = localChildList;
                    if(recordDetailList == null)
                        recordDetailList = new List<RecordDetailJson>();
                    recordDetailList.add(recordDetailJson);
                }
            }
            parentChildMap = null;
        }
        
        system.debug('BatchCreateSnapshot finish '+childApi);
    	if(contentVersionList.size() > 0 ) 
            insert contentVersionList;
        
        List<ParentChild> parentChildList = new  List<ParentChild>();
        if(mParentChildList != null && mParentChildList.size() > 0) {
            mParentChildList.remove(0);
            parentChildList = mParentChildList;
        }
        if(parentChildList != null && parentChildList.size() > 0) {
            //execute batch again 
            Database.executeBatch(new BatchCreateSnapshot(mSnapshot, parentChildList, mParentChildIdMap, recordDetailList, IdCheckSet), 30);        
        }
        else{
            //Create Indexing file
            if(recordDetailList != null && recordDetailList.size() > 0) {
                ContentVersion contentversionVarient = new ContentVersion();
                contentversionVarient.Title = SnapshotController.INDEX_FILE_NAME;
                contentversionVarient.contentLocation='S';
                contentversionVarient.PathOnClient = SnapshotController.INDEX_FILE_NAME + '.json';
                contentversionVarient.FirstPublishLocationId = mSnapshot.Id;
                contentversionVarient.VersionData = Blob.valueOf(JSON.serialize(recordDetailList));
                insert contentversionVarient;
            }
        }
        
    }
    
    
    /***
     * Get all the fields for a specified object name and store it in SnapshotObjectField class.
     */ 
    public List<SnapshotObjectField> getFieldsForObject(String objName){
        //System.debug('getFieldsForObject objName '+objName);       
        if(objName == null)
            return null;
        List<SnapshotObjectField> objFieldList = new List<SnapshotObjectField>();
        //if exception its a wrong object
        Map<string,SObjectField> fieldMap;
        try {
            fieldMap = schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        }
        catch(Exception e1){
            System.debug('getFieldsForObject Exception '+e1);
            String m = 'Invalid Object name '+objName +'. Please check Snapshot Configuration settings.';
            AuraHandledException e = new AuraHandledException(m);
            e.setMessage(m);
            throw e;
        }
        /* 
         * To avoid duplicate field error if someone create a local field with same name, 
         * created a fieldCheckSet to add unique field api name
		*/
        Set<String> fieldCheckSet = new Set<String>();
        for(string fieldName: fieldMap.keySet()) {
            string strFieldLocalName = fieldMap.get(fieldName).getDescribe().getLocalName();
            if(!fieldCheckSet.contains(strFieldLocalName)) {
                SnapshotObjectField objField = new SnapshotObjectField();
                objField.Label = fieldMap.get(fieldName).getDescribe().getLabel();
                objField.ApiName = fieldName;
                objField.FieldType = fieldMap.get(fieldName).getDescribe().getType();
                if(objField.FieldType == Schema.DisplayType.REFERENCE ){
                    Schema.DescribeFieldResult dfr1 = fieldMap.get(fieldName).getDescribe();  
                    String RelatedObjNameChild = String.valueOf(dfr1.getReferenceTo()).substringBetween('(',')');
                    objField.RelObjectName = RelatedObjNameChild;
                }
                objFieldList.add(objField);
                fieldCheckSet.add(strFieldLocalName);
            }
        }
        fieldCheckSet = null;
        //System.debug('getFieldsForObject objFieldList : '+ JSON.serializePretty(objFieldList));
        //System.debug('getFieldsForObject objFieldList : '+ objFieldList);
        return objFieldList;
    }
    
    /***
     * Creates a json file for the record.
     */ 
    public void createJson(Id recordId, String title, List<SnapshotObjectField> objData, Id objectId){
        //System.debug('createJson objFieldList: '+ objData);
        ContentVersion contentversionVarient = new ContentVersion();
        contentversionVarient.Title = title;
        contentversionVarient.contentLocation='S';
        contentversionVarient.PathOnClient = title +'.json';
        contentversionVarient.FirstPublishLocationId = recordId;
        contentversionVarient.VersionData = Blob.valueOf(JSON.serialize(objData));
        
        //To avoid adding duplicate record to snapshot objects
        if(objectId != null && !IdCheckSet.contains(objectId)) {
        	contentVersionList.add(contentversionVarient);
            IdCheckSet.add(objectId);
        }
    }
    
    
    public class SnapshotObjectField implements Comparable{
        
        @AuraEnabled public String Label  {get; set;}
        @AuraEnabled public String ApiName  {get; set;}
        @AuraEnabled public String RelObjectName  {get; set;}
        @AuraEnabled public String RelObjectFieldValue  {get; set;}
        @AuraEnabled public Schema.DisplayType FieldType  {get; set;}
        @AuraEnabled public String Value  {get; set;}
        @AuraEnabled public String FieldCurrentStatus {get; set;} //To indicate z_unused, y_hidden
        
        public Integer compareTo(Object objToCompare) {
            SnapshotObjectField snap = (SnapshotObjectField)(objToCompare);
            if(snap == null)
                return 1;
            if (this.Label > snap.Label) 
                return 1;
            if (this.Label == snap.Label) 
                return 0;
            
            return -1;
        }    
    }
    
    public class RelatedObjectJson {
        @AuraEnabled public String obj_api  {get; set;}
        @AuraEnabled public String related_obj_api  {get; set;}
        @AuraEnabled public List<RelatedObjectJson> child  {get; set;}
    }
    
    public class RecordDetailJson {
        @AuraEnabled public String child_api  {get; set;}
        @AuraEnabled public String related_obj_api  {get; set;}
        @AuraEnabled public Id parentId {get; set;}
        @AuraEnabled public List<RecordDetailChildJson> itemIdList  {get; set;}
    }
    
    public class RecordDetailChildJson {
        @AuraEnabled public Id childId {get; set;}
        @AuraEnabled public String childName {get; set;}
    }
    
    Public Class ParentChild {
        @AuraEnabled public String parent {get; set;}
        @AuraEnabled public String child {get; set;}
        @AuraEnabled public String related_obj_api {get; set;}
    }
    
}