public class DelinquencyScheduler implements Database.Batchable<sObject> ,Schedulable,Database.Stateful
{
    
    private Process_Request_Log__c ReqLog;
    integer iCount = 0;
    private static integer batchSize = 50;
    private static string createDelinquency = LeaseWareUtils.getLWSetup_CS('CREATE_DELINQUENCY');
    List<String> ListOfErrorMessages;
    
    public DelinquencyScheduler() {
    }
    
    //START METHOD 
    //Query for all invoices whihc is not paid
    public Database.QueryLocator start(Database.BatchableContext BC)           
    {          	
        boolean errorFlg=false;
        string strSessionId = string.valueOf(datetime.now());
        ReqLog = new Process_Request_Log__c(Status__c ='Success',Type__c = 'Delinquency Scheduler', 
                                            Comments__c=strSessionId, Exception_Message__c='');
        
        insert ReqLog; // insert record to get Id
        
        string pkgPrefix = LeaseWareUtils.getNamespacePrefix();
        system.debug('pkgPrefix = '+pkgPrefix);
        String strQuery = 'SELECT Id,Name, RecordType.DeveloperName, '+pkgPrefix+'Status__c,'+pkgPrefix+'Lease__r.'+pkgPrefix+'Operator__r.Name, '
            +pkgPrefix+'Lease__r.'+pkgPrefix+'Operator__r.'+pkgPrefix+'Marketing_Rep__c,'+pkgPrefix+'Lease__r.'+
            +pkgPrefix+'Aircraft__r.'+pkgPrefix+'MSN_Number__c,'+pkgPrefix+'Ageing__c,'+pkgPrefix+'Total_Amount_Due__c,'+pkgPrefix+'Date_of_MR_Payment_Due__c,' +
            +pkgPrefix+'Amount_Paid__c,'+pkgPrefix+'Accounting_Description_F__c,'+pkgPrefix+'Invoice_Date__c,'+pkgPrefix+'Invoice_Number_F__c,' +
            +pkgPrefix+'Invoice_Type__c,'+pkgPrefix+'Balance_Due__c,'+pkgPrefix+'Date_Invoice_Sent__c,'+pkgPrefix+'Amount__c,(Select id,Name,'+pkgPrefix+'Amount__c From '
            +pkgPrefix+'Payments__r where '+pkgPrefix+'Payment_Date__c =LAST_N_DAYS:30 AND '+
            +pkgPrefix+ 'Payment_status__c NOT IN(\'Cancelled\',\'Pending\')) FROM '
            +pkgPrefix+'Invoice__c where '+pkgPrefix+'status__c IN( \'Approved\',\'Partially Paid\' ) AND  RecordType.DeveloperName != \'Credit_Note\' AND RecordType.DeveloperName != \'Interest\''+ (Test.isRunningTest()?' LIMIT 5':'');              
        
        System.Debug('StrQuery=='+StrQuery);
        return Database.getQueryLocator(StrQuery);
    }   
    
    //EXECUTE METHOD 
    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Delinquency_Staging__c> listDelinStagingToInsert = new List<Delinquency_Staging__c> ();
        database.SaveResult[] res = new List<database.SaveResult>(); integer i=0;
        string errormsg;
        iCount+=scope.size();
        
        if(!String.isBlank(createDelinquency) && !createDelinquency.equals('') && createDelinquency.equals('YES')){
            if(scope.size()>0){
                listDelinStagingToInsert = mapInvoiceToStaging((list<Invoice__c>)scope);
            }       
            for(Delinquency_Staging__c curDelStaging:listDelinStagingToInsert){
                res.add(database.insert(curDelStaging, true));
            }  
        }
        
        for(database.SaveResult sr : res){
            if (!sr.isSuccess()){
                errormsg = 'ERROR: Delinquency Staging insert failed for ' + sr.getId() +' .\n';
                for (Database.Error err:sr.getErrors()) {
                    errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                    errormsg += ('Fields that affected this error: ' + err.getFields() + '\n');
                }
                ReqLog.Exception_Message__c += ('\n' +errormsg);
            }
        }
    }   
    
    // Logic to be Executed at finish
    public void finish(Database.BatchableContext BC)
    {   
        LeaseWareUtils.insertExceptionLogs();
        ReqLog.Exception_Message__c = ReqLog.Exception_Message__c.left(2000);
        System.debug('ReqLog:' + ReqLog);
        update ReqLog;
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        
        string strMsg = 'DelinquencyScheduler Job with ID ' + BC.getJobId() + ' completed with status ' + a.Status 
            + '.\n' + a.TotalJobItems + ' batches with '+ a.JobItemsProcessed + ' processed and ' + a.NumberOfErrors + ' failures.';
        strMsg+=('\nTotal Number of records processed : ' + iCount);

        if(!String.isBlank(createDelinquency) && !createDelinquency.equals('') && createDelinquency.equals('YES')){ 
            LeaseWareUtils.sendEmailToLWAdmins( 'DelinquencyScheduler Batch finished ('+ Date.today().format() +')', strMsg);
        }
    }
    
    public void execute(SchedulableContext sc)
    {
        executeDelinquencySchedulerBatch();
    }
    
    public static void executeDelinquencySchedulerBatch()    
    {
        system.debug('executeDelinquencySchedulerBatch ++');
        ID batchprocessid = Database.executeBatch( new DelinquencyScheduler(), getBatchSize());
        system.debug('batchprocessid = ' + batchprocessid);
        system.debug('executeDelinquencySchedulerBatch --');      
    } 
    
    public static Integer getBatchSize(){
        ServiceSetting__c lwServiceSettings = ServiceSetting__c.getInstance('InvoiceDelinquencyBatch');
        if(lwServiceSettings != null && lwServiceSettings.Batch_Size__c != null)batchSize = integer.valueOf(lwServiceSettings.Batch_Size__c);
        return batchSize;
    }
    
    private List<Delinquency_Staging__c> mapInvoiceToStaging(List<Invoice__c> listOfInvoice){
        
        List<Delinquency_Staging__c> listDelinStagingToInsert  = new List<Delinquency_Staging__c>();
        Decimal sumOfCashReceivedInLast30Days;
        
        System.debug('Size of invoice list:' + listOfInvoice.size());
        for(Invoice__c curInvoice : listOfInvoice){
            System.debug('Selected Invoice :: '+curInvoice.id + ':: Operator + MSN + Type ::' + curInvoice.Lease__r.Operator__r.Name +'--'+ curInvoice.Lease__r.Aircraft__r.MSN_Number__c +'--'+ curInvoice.Invoice_Type__c);
            Id recordTypeId = curInvoice.RecordTypeId;
            sumOfCashReceivedInLast30Days = 0;
            System.debug(curInvoice.Payments__r.size());
            if(curInvoice.Payments__r.size()>0){
                for(Payment__c curPayment: curInvoice.Payments__r){
                    sumOfCashReceivedInLast30Days += curPayment.Amount__c;
                }              
            }
            
            String InvoiceType = curInvoice.Invoice_Type__c.equals('Aircraft MR') ? 'MaintenenceReserveFunds':curInvoice.Invoice_Type__c.equals('Rent')?'Rental':'Others';        
            if(curInvoice.Lease__r.Operator__r.Name != null && curInvoice.Lease__r.Aircraft__r.MSN_Number__c != null)
            {        
                Delinquency_Staging__c curStaging = new Delinquency_Staging__c( Name = curInvoice.Name,
                                                                               CompanyName__c = curInvoice.Lease__r.Operator__r.Name,
                                                                               MSN__c = curInvoice.Lease__r.Aircraft__r.MSN_Number__c,
                                                                               Cash_Received_in_Last_30_Days__c = LeasewareUtils.zeroIfNull(sumOfCashReceivedInLast30Days), 
                                                                               Days_Overdue__c =LeasewareUtils.zeroIfNull(curInvoice.Ageing__c),
                                                                               Due_Date__c = curInvoice.Date_of_MR_Payment_Due__c,
                                                                               Invoice_Amount__c = curInvoice.Total_Amount_Due__c,
                                                                               Invoice_Currency__c = 'USD',
                                                                               Invoice_Date__c = curInvoice.Invoice_Date__c,
                                                                               Invoice_Description__c = curInvoice.Accounting_Description_F__c, 
                                                                               Invoice_Number__c = curInvoice.Invoice_Number_F__c,
                                                                               Invoice_Type__c = InvoiceType,
                                                                               MaintBal1__c = 0.0,
                                                                               MaintBal2__c = 0.0,
                                                                               MaintRatio1__c = 0.0,
                                                                               MaintRatio2__c = 0.0,
                                                                               Lookup_MarketingRep__c = curInvoice.Lease__r.Operator__r.Marketing_Rep__c,
                                                                               Outstanding_Amount__c = curInvoice.Balance_Due__c,
                                                                               RentBal1__c = 0.0,
                                                                               RentBal2__c = 0.0,
                                                                               RentRatio1__c = 0.0,
                                                                               RentRatio2__c = 0.0,
                                                                               RunDate1__c = Date.today(),
                                                                               RunDate2__c = Date.today(),
                                                                               Total_Outstanding__c = curInvoice.Balance_Due__c
                                                                               
                                                                              );
                
                System.debug('Current Del Staging: ' + curStaging);
                listDelinStagingToInsert.add(curStaging);
            } 
        }
        System.debug('Size of Del Staging to insert : ' + listDelinStagingToInsert.size());
        return listDelinStagingToInsert;
    }
}