/**
***********************************************************************************************************************
* Class: Notify_NotificationSetupController
* @author Created by Aman, Lease-Works, 
* @date 28/08/2020
* @version 1.0
* ---------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to configure the Notification.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*	1.0       A.K.G                 17.08.2020      [Initial Code]
***********************************************************************************************************************
*/
public class Notify_NotificationSetupController {

    /**
    * @description get the field value to display the content on the UI.
    *
    * @param recordId - String, contains record Id of Notification Subscription record.
    *
    * @return Notify_Utility.NotificationSetupWrapper - wrapper containg all the necessary information to display the content on UI.
    */
    @AuraEnabled
    public static String getLoadData(String recordId) {
        try {
            System.debug('recordId: '+recordId);
            if (recordId != null && recordId != '')  {
                Notifications_Subscription__c[] notificationSubList = [SELECT Id, Name,Field_API_Name__c, Field_Name__c, Record_Filter_Criteria__c, Message__c,
                                                                        Notification_Type__c, Notify_Days__c, Object_API_Name__c, Object_Name__c, Title__c, UserIds__c, Users__c, Notify_Day_Type__c, Active__c, Async_JobId__c,
                                                                        Status__c, Record_Filter_Criteria_Text__c, OwnerId,Owner.Name
                                                                        FROM Notifications_Subscription__c WHERE Id =: recordId];
                if (!notificationSubList.isEmpty()) {
                    return JSON.serialize(new Notify_Utility.NotificationSetupWrapper(notificationSubList[0]));
                }
            }
            return JSON.serialize(new Notify_Utility.NotificationSetupWrapper());
        }
        catch (Exception ex) { 
            System.debug('ERROR: ' + ex.getMessage() + ' at Line Number: ' + ex.getLineNumber());
            throw new AuraHandledException(ex.getMessage() + ' : Line Number: '+ex.getLineNumber()); }
    }

    @AuraEnabled
    public static boolean checkIsSysAdmin(Id userId){
        {
            boolean isAdmin;
             try {
                User thisUser = [Select id From User u Where id=:userId AND IsActive = true AND u.Profile.Name = 'System Administrator' 
                         And u.Profile.PermissionsModifyAllData=true limit 1];
                 isAdmin=true;
             } catch(Exception exc) { isAdmin = false; }
             return isAdmin;
         }
    }
    @AuraEnabled
    public static ApexTrigger[] existingTriggerCheck(String triggerName) {
        return  Notify_Utility.existingTriggerCheck(triggerName);
    }
    @AuraEnabled
    public static Boolean checkNotificationSetting() {
        List<Notification_Setting__c> nsList = [SELECT Id, OwnerId, IsDeleted, Name, Disable_all_emails__c, Default_CC_Address__c, Reply_To_Address__c, Keep_Log_records_Days__c, From_Adress_Override__c, Email_Footer__c FROM Notification_Setting__c];
        if(nsList.size() > 0) return true;
        else return false;
    }

    @AuraEnabled
    public static String deployMetadata(String data,String testClassName) {
        System.debug('data: ' + data);
        system.debug('testClassName----'+ testClassName);
        return Notify_Utility.deployMetadata(data,testClassName);
    }

    @AuraEnabled
    public static String checkAsyncRequest(String asyncId) {
        System.debug('checkAsyncRequest: asyncId: '+asyncId);
        return Notify_Utility.checkAsyncRequest(asyncId);
    }

    @AuraEnabled
    public static String testNotificationData(String loadData) {
        System.debug('testNotificationData: loadData: '+loadData);
        return Notify_RecordAccessUtility.testNotificationConfigurations(loadData);
    }
    @AuraEnabled
    public static Boolean validateQuery(String objName, String filter){
        String query = 'Select Id from '+objName+ ' Where '+filter;
        Boolean queryValidated = false;
        try{
            List<sObject> sObjList = Database.query(query);
            queryValidated = true;
        }
        catch(Exception e){  queryValidated = false; }
           
       
        return queryValidated;
    }

    @AuraEnabled
    public static Notifications_Subscription__c saveNotificationData(String recordId, String loadData, Boolean isClone) {
        try {
            
            System.debug('loadData: ' + loadData);
            if (loadData != null && loadData != ''){
                Notify_Utility.NotificationSetupWrapper nsWrap = (Notify_Utility.NotificationSetupWrapper) JSON.deserialize(loadData, Notify_Utility.NotificationSetupWrapper.class);
                system.debug('nswrap----'+nswrap);
                if (nsWrap != null) {
                    Notifications_Subscription__c notify = new Notifications_Subscription__c();
                    if (recordId != null && recordId != '' && !isClone) { notify.Id = recordId; }
                    notify.Object_Name__c = nsWrap.selectedObjectLabel;
                    notify.Object_API_Name__c = nsWrap.selectedObjectName;
                    
                    notify.Users__c = nsWrap.selectedUserGroup;
                    notify.UserIds__c = nsWrap.selectedUserGroupIds;
                    
                    notify.Notification_Type__c = nsWrap.notificationType;
                    notify.Field_Name__c = nsWrap.fieldName;
                    notify.Field_API_Name__c = nsWrap.fieldAPIName;
                    notify.Notify_Day_Type__c = nsWrap.notifyDayType;
                    notify.Notify_Days__c = nsWrap.notifyDays;
                    
                    notify.Record_Filter_Criteria__c = nsWrap.recordFilterCriteria == null ? '' : nsWrap.recordFilterCriteria;
                    notify.Record_Filter_Criteria_Text__c = (notify.Record_Filter_Criteria__c.length() > 255 ? notify.Record_Filter_Criteria__c.substring(0, 252) + '...' : notify.Record_Filter_Criteria__c);
                    notify.Title__c = nsWrap.title;
                    notify.Message__c = nsWrap.messageBody == null ? '' :nsWrap.messageBody;
                    notify.Async_JobId__c = nsWrap.asyncJobId;
                    notify.Active__c = nsWrap.isActive;
                    system.debug('nsWrap.isActive---'+nsWrap.isActive);
                    //1. If the  Active is true, 
                    //a. Async Job Id is blank which means trigger already present, hence Running
                    //b. Async Job Id is not blank---Check if it is deployed or not based on that set the status.
                    if(nsWrap.isActive) {
                        system.debug('nsWrap.asyncJobId--'+nsWrap.asyncJobId);
                        if(nsWrap.notificationType == 'On Date'){
                            notify.Status__c = 'Running';
                        }else{
                        if(String.isEmpty(nsWrap.asyncJobId)){
                            String triggerName = Notify_Utility.generateTriggerName(nsWrap.selectedObjectName);
                            List<ApexTrigger> trig = Notify_Utility.existingTriggerCheck(triggerName);
                            system.debug('trig---'+trig);
                            if(trig==null || trig.size()==0){ notify.Status__c = 'Pending';}
                            else{  notify.Status__c = 'Running';}
                        }else{
                            String deploystatus = checkAsyncRequest(nsWrap.asyncJobId);
                            Notify_Utility.Notify_AsyncWrapper aWrap = (Notify_Utility.Notify_AsyncWrapper) JSON.deserialize(deploystatus, Notify_Utility.Notify_AsyncWrapper.class);
                            if(aWrap.isDeployed && String.isEmpty(aWrap.errorMessage)){ notify.Status__c = 'Running';}
                            else{notify.Status__c = 'Pending';}
                             
                        }
                    }
                    }else{ notify.Status__c = ' Not Running';  }// 
                        //If Inactive set as Not Running
                        if(notify.Status__c == 'Pending'){
                             //Check if the Scheduler is already scheduled if not then call the scheduler
                          
                             //boolean isSchedulerRunning = system.isScheduled();
                             boolean isSchedulerRunning = checkSchedulerIsScheduled();
                             system.debug('isSchedulerRunning----'+isSchedulerRunning);
                             if(!isSchedulerRunning){
                                 system.debug('Call the Scheduler......');
                                 try{
                                     //Call the Scheduler After - mins from Here
                                    String nextFireTime = system.now().addMinutes(Notify_Utility.NOTIFICATION_STATUS_CHECK_SCH_TIME).format('ss mm HH dd MM ? yyyy');
                                    System.schedule('Update Subscription Status', nextFireTime, new Notify_SubscriptionStatusScheduler());
                                 }catch(Exception e){
                                     system.debug('Some Exception occured during Scheduling of the Job-----'+ e.getMessage());
                                 }
                            }
                             
							  
                        }
                        //Shweta Commented 
                       // else if (nsWrap.status == 'Running') { notify.Active__c = true; }
                    
                    
                    //if (notify.Status__c == 'Running') { notify.Async_JobId__c = null; }
                    Notify_Utility.setSourceOfCall('INTERNAL_API');
                    upsert notify;

                    return notify;
                }
            }
            return null;
        }
        catch (Exception ex) { 
            system.debug('Exception e---'+ ex.getMessage());
            if(ex.getMessage().contains('DUPLICATES_DETECTED')){
                throw new AuraHandledException(' Error trying to save a Notification with the same criteria Type + Object + Field + Title. Duplicates are not allowed. Please change at least one notification criteria to save it'); 
            }else{
                throw new AuraHandledException(ex.getMessage()); }
            }
           
    }
     
    public static boolean checkSchedulerIsScheduled(){
        List<ApexClass> apList = [Select Id from ApexClass where Name = 'Notify_SubscriptionStatusScheduler'];
        
        Integer enqueuedJobs = [SELECT COUNT() FROM AsyncApexJob 
                                WHERE JobType='BatchApex' 
                                AND Status IN ('Processing','Preparing','Queued') 
                                AND ApexClassID =: apList[0].Id] ;
        
        if(enqueuedJobs <= 0) { return false; } //EXECUTE YOUR BATCH HERE
        else{ return true; }//BATCH IS ALREADY SCHEDULED - SHOW NOTIFICATION
    }

  
   

  @AuraEnabled
  public static boolean checkNameUpdateable(String objectAPIName){
     system.debug('objectAPIName----'+ objectAPIName);
     Schema.SObjectType soType = Schema.getGlobalDescribe().get(objectAPIName);
     DescribeFieldResult nameDescribe = soType.getDescribe().fields.getMap().get('Name').getDescribe();
     if(nameDescribe.isAutoNumber() || !nameDescribe.isUpdateable()) return false;
     else return true;
 
  }

  @AuraEnabled
  
  public static boolean validateMergeFieldValues(String messageBody,String title,String objName){
    system.debug('validateMergeFieldValues----' );
    boolean isValid = true;
    system.debug('title-----'+title);
    String str = messageBody + title;
    system.debug('str-----'+str);
    Matcher imgMatcher = Notify_Utility.FIELD_TAG_PATTERN.matcher( str );
    
    system.debug('imgMatcher---'+imgMatcher);
    String fieldAPIName;
   
    Set<String> fieldNamesAPISet = new Set<String>();
    while (imgMatcher.find()) {
        System.debug(imgMatcher.group().stripHtmlTags());
        fieldAPIName = imgMatcher.group()?.substringBetween('{{', '}}');
        fieldNamesAPISet.add(fieldAPIName.stripHtmlTags().toLowerCase());
    }
    Set<String> fieldListForObject =new Set<String>();

    SObjectType sObjType = ((SObject) Type.forName(objName).newInstance()).getSObjectType();
    Map<String, Schema.SObjectField> Objfields = sobjType.getDescribe().fields.getMap();
    for(Schema.sObjectField field : Objfields.values()) {
        Schema.DescribeFieldResult dfield = field.getDescribe();
        String fieldName = dfield.getName().toLowerCase();
        fieldListForObject.add(fieldName);
       
        if(String.ValueOf(dfield.getType()) == 'Reference'){
            system.debug(fieldName.endswith('byid'));
            if(fieldName.endswith('byid')){
                fieldListForObject.add(fieldName.replace('byid','by.name'));
            }else{
                fieldListForObject.add(Utility.replaceRefField(fieldName, 'c', 'r.name'));
            }
            
        }
    }
    system.debug('fieldListForObject----' + fieldListForObject);

    for(String s : fieldNamesAPISet){
        system.debug('s---'+ s + '---'+fieldListForObject.contains(s));
        if(!fieldListForObject.contains(s)){
            isValid = false;
            break;
        }
    }
    system.debug('isValid----'+ isValid);
    return isValid;
}
   
}