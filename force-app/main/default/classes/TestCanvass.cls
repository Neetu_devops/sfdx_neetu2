/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class TestCanvass {
    
    @isTest(SeeAllData = true)
    static void testCanvasController1() {
        
        string ACName = '%TestSeededMSN1%';
        List<Lease__c> AC1 = [select id, Aircraft__c, Operator__c from Lease__c where Name like :ACName limit 1];
        if(AC1.size() > 0){
            list < string > customerArray_p = new list < string > ();
            customerArray_p.add(AC1[0].Operator__c);
            
            list < string > lessorArray_p = new list < string > ();
            list < string > counterpartyArray_p = new list < string > ();
            list < string > leaseArray_p = new list < string > ();
            
            list < string > msnArray_p = new list < string > ();
            msnArray_p.add(AC1[0].Aircraft__c);
            
            list < string > WFArray_p = new list < string > ();
            list < string > esnArray_p = new list < string > ();
            //list < string > atArray_p = new list < string > ();
            list < string > EmptyArray_p = new list < string > ();
            String leaseRentDef ='';
           
            
            // Code Added on 01-04-2019
            // Code for changes regarding Asset Quantity and Type - START.
            list<CanvasApexController.AssetQuantityTypeWrapper> atArray_pList = new List<CanvasApexController.AssetQuantityTypeWrapper>();
            CanvasApexController.AssetQuantityTypeWrapper assetQuantityType = new CanvasApexController.AssetQuantityTypeWrapper();
            atArray_pList.add(assetQuantityType);
            String atArray_p = JSON.serialize(atArray_pList);
            String engineArray_p = JSON.serialize(atArray_p);
            // Code for changes regarding Asset Quantity and Type - END.
           
	
           list<CanvasApexController.CanvasWrapper> leaseRentDefList = new List<CanvasApexController.CanvasWrapper>();
            CanvasApexController.CanvasWrapper leaseRent = new CanvasApexController.CanvasWrapper();
            leaseRent.leaseArray_p = leaseArray_p;
            leaseRent.rentDeferralId=null;
            leaseRentDefList.add(leaseRent);
            String testing = JSON.serialize(leaseRentDefList);

            
           // String leaseRentDefIdsVar ='[{"leaseArray_p":'+test+',"rentDeferralId":null}]';

            CanvasApexController.callSaveApex('2019-01-01', 'N/A', 'Lease', null,null,'Pipeline', 'New', 
                                              'Aircraft', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p,
                                              counterpartyArray_p, EmptyArray_p, 'From My Fleet', msnArray_p, EmptyArray_p,
                                              esnArray_p, EmptyArray_p,
                                              WFArray_p, EmptyArray_p, atArray_p, engineArray_p, null, null, 'Test',
                                              'Weekly Summary Notes',
                                              'weeklySummary_p','','','',testing);
            
            // Code for changes regarding Asset Quantity and Type - START.
            atArray_pList = new List<CanvasApexController.AssetQuantityTypeWrapper>();
            assetQuantityType = new CanvasApexController.AssetQuantityTypeWrapper();
            
            assetQuantityType.assetType = '737-200';
            atArray_pList.add(assetQuantityType);
            atArray_p = JSON.serialize(atArray_pList);
            // Code for changes regarding Asset Quantity and Type - END.
            
            //atArray_p.add('737-200');
            CanvasApexController.callSaveApex('2019-01-01', 'N/A', 'Lease',  null,null,'Pipeline', 'New', 'Aircraft', 
                                              customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p,counterpartyArray_p, 
                                              EmptyArray_p, 'By Quantity and Type', msnArray_p, EmptyArray_p, esnArray_p, 
                                              EmptyArray_p, WFArray_p, EmptyArray_p, atArray_p, engineArray_p, null, null, 
                                              'Test','Weekly Summary Notes', 'weeklySummary_p','','','',testing);
            
            //Added on 15/03/2019 for dynamic namespace prifix.
            CanvasApexController.getNameSpacePrifix();
            CanvasApexController.getShowListVisibility();
            CanvasApexController.getDealGroupVisibility();
            CanvasApexController.getMarketingRepType();
            CanvasApexController.getCurrentUser();
        }
    }
    
    @isTest(SeeAllData = true)
    static void testCanvasController2() {
        
        string ACName = '%TestSeededMSN1%';
        List<Lease__c> AC1 = [select id, Aircraft__c, Operator__c from Lease__c where name like : ACName limit 1];
        if(AC1.size() > 0){
            list < string > customerArray_p = new list < string > ();
            //customerArray_p.add(AC1.Operator__c);
            
             Country__c country;
			try{
				country = [select Id,Name from Country__c limit 1];
			}catch(QueryException e){
				 country =new Country__c(Name='Test Country');
				insert country;
			}
            Counterparty__c lessor1 = new Counterparty__c(Address_Street__c = '1240/29', City__c = 'Faridabad', Zip_Code__c = '121008', Country_Lookup__c = country.id,
                                                          Fleet_Aircraft_Of_Interest__c = 'A320', Status__c = 'Approved', Location__Latitude__s = 23.45555555,
                                                          Location__Longitude__s = 73.23233232,
                                                          Main_Email_Address__c = 'test1@lws.com',
                                                          Subsidiary_Entity__c = true,
                                                          Billing_Email_Address__c = 'test2@lws.com');
            insert lessor1;
            
            Bank__c cp1 = new Bank__c(Name='CP1', Country_Lookup__c=country.Id);  //Counterparty 
            insert cp1;
            
            list < string > lessorArray_p = new list < string > ();
            lessorArray_p.add(lessor1.Id);
            
            list < string > counterpartyArray_p = new list < string > ();
            counterpartyArray_p.add(cp1.Id);
            
            
            list < string > WFArray_p = new list < string > ();
            list < string > msnArray_p = new list < string > ();
            msnArray_p.add(AC1[0].Aircraft__c);
            
            list < string > esnArray_p = new list < string > ();
            list < string > leaseArray_p = new list < string > ();
         	list < string > EmptyArray_p = new list < string > ();
            
            // Code Added on 01-04-2019
            //list < string > engineArray_p = new list < string > ();
            //engineArray_p.add('CFM56');
            
            // Code for changes regarding Asset Quantity and Type - START.
            list<CanvasApexController.AssetQuantityTypeWrapper> engineArray_pList = new List<CanvasApexController.AssetQuantityTypeWrapper>();
            CanvasApexController.AssetQuantityTypeWrapper assetQuantityType = new CanvasApexController.AssetQuantityTypeWrapper();
            engineArray_pList.add(assetQuantityType);
            String atArray_p = JSON.serialize(engineArray_pList);
            
            engineArray_pList = new List<CanvasApexController.AssetQuantityTypeWrapper>();
            assetQuantityType = new CanvasApexController.AssetQuantityTypeWrapper();
            assetQuantityType.assetType = 'CFM56';
            engineArray_pList.add(assetQuantityType);
            String engineArray_p = JSON.serialize(engineArray_pList);
            // Code for changes regarding Asset Quantity and Type - END.
            
            list<CanvasApexController.CanvasWrapper> leaseRentDefList = new List<CanvasApexController.CanvasWrapper>();
            CanvasApexController.CanvasWrapper leaseRent = new CanvasApexController.CanvasWrapper();
            leaseRent.leaseArray_p = leaseArray_p;
            leaseRent.rentDeferralId=null;
            leaseRentDefList.add(leaseRent);
            String testing = JSON.serialize(leaseRentDefList);
            
            CanvasApexController.callSaveApex('2019-01-01', 'N/A', 'Lease',  null,null,'Pipeline', 'New', 'Engine', 
                                              customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p,counterpartyArray_p,
                                              EmptyArray_p, 'By Quantity and Type', msnArray_p, EmptyArray_p, esnArray_p, 
                                              EmptyArray_p, WFArray_p, EmptyArray_p, atArray_p, engineArray_p, null, null,
                                              'Test','Weekly Summary Notes', 'weeklySummary_p','','','',testing);
            
            Constituent_Assembly__c CA1 = [select id from Constituent_Assembly__c limit 1];
            esnArray_p.add(CA1.Id);
            LeaseWareUtils.clearFromTrigger();
            CanvasApexController.callSaveApex('2019-01-01', 'N/A', 'Lease', null,null,'Pipeline', 'New', 'Engine',
                                              customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p, counterpartyArray_p, 
                                              EmptyArray_p, 'From My Fleet', msnArray_p, EmptyArray_p, esnArray_p, EmptyArray_p,
                                              WFArray_p, EmptyArray_p, atArray_p, engineArray_p, null, null, 'Test',
                                              'Weekly Summary Notes', 'weeklySummary_p','','','',testing);
            
            //Expecting array 
            LeaseWareUtils.clearFromTrigger();
            WFArray_p.add(CA1.Id);
            CanvasApexController.callSaveApex('2019-01-01', 'N/A', 'Lease', null,null,'Pipeline', 'New', 'Aircraft', 
                                              customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p, counterpartyArray_p, 
                                              EmptyArray_p, 'From World Fleet', msnArray_p, EmptyArray_p, esnArray_p, 
                                              EmptyArray_p, WFArray_p, EmptyArray_p, atArray_p, engineArray_p, null, null,
                                              'Test','Weekly Summary Notes', 'weeklySummary_p','','','',testing);
        }
    }
    
    
 
    
  
    
    @isTest(SeeAllData = true)
    static void testDynamicPicklistApexController1() {
        DynamicPicklistApexController.getDynPickList('Aircraft__c', 'Aircraft_Type__c', null, null);
    }
    
      
    //Test for Transaction
    
    @isTest(SeeAllData = true)
    static void testCanvasTransController1() {
        
        string ACName = '%TestSeededMSN1%';
        List<Lease__c> AC1 = [select id, Aircraft__c, Operator__c from Lease__c where name like : ACName limit 1];
        if(AC1.size() > 0){
            list < string > customerArray_p = new list < string > ();
            customerArray_p.add(AC1[0].Operator__c);
            
            list < string > lessorArray_p = new list < string > ();
            
            list < string > msnArray_p = new list < string > ();
            msnArray_p.add(AC1[0].Aircraft__c);
            
            list < string > WFArray_p = new list < string > ();
            list < string > esnArray_p = new list < string > ();
            list < string > atArray_p = new list < string > ();
            list < string > EmptyArray_p = new list < string > ();
            
            //
            list < string > OtherSellerType_p = new list < string > ();
            list < string > MSNNameText_p = new list < string > ();
            string AircraftType_p = '';
            string Varient_p = '';
            string Engine_p = '';
            string EngineVariant_p = '';
            
            CanvasApexController.getDealType_Apex();
            CanvasApexController.callSaveApexTrans('TransName1', 'Acquisition', 'ST', 'SLB', 'Airline', OtherSellerType_p, 'MSN', MSNNameText_p, null, null, AircraftType_p,
                                                   Varient_p, Engine_p, EngineVariant_p,
                                                   msnArray_p, EmptyArray_p, customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
            
            CanvasApexController.callSaveApexTrans('TransName1', 'Disposal', 'ST', 'SLB', 'Airline', OtherSellerType_p, 'MSN', MSNNameText_p, null, null, AircraftType_p,
                                                   Varient_p, Engine_p, EngineVariant_p,
                                                   msnArray_p, EmptyArray_p, customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
        }
    }
    
    //Test for Transaction (ok)
    
    @isTest(SeeAllData = true)
    static void testCanvasTransController2() {
        
        string ACName = '%TestSeededMSN1%';
        List<Lease__c> AC1 = [select id, Aircraft__c, Operator__c from Lease__c where name like : ACName limit 1];
        if(AC1.size() > 0){
            
			 Country__c country;
				try{
					country = [select Id,Name from Country__c limit 1];
				}catch(QueryException e){
					 country =new Country__c(Name='Test Country');
					insert country;
				}
            
            Counterparty__c lessorRec = new Counterparty__c(Address_Street__c = '1240/29', City__c = 'Faridabad', 
                                                            Zip_Code__c = '121008', Country_Lookup__c = country.id,
                                                            Fleet_Aircraft_Of_Interest__c = 'A320', Status__c = 'Approved', Location__Latitude__s = 23.45555555,
                                                            Location__Longitude__s = 73.23233232);
            LeaseWareUtils.clearFromTrigger();
            insert lessorRec;
            
            list < string > customerArray_p = new list < string > ();
            customerArray_p.add(AC1[0].Operator__c);
            
            list < string > lessoridArray_p = new list < string > ();
            lessoridArray_p.add(lessorRec.Id); //Lessor
            
            list < string > sellerOther_p = new list < string > ();
            sellerOther_p.add('12345p'); //Other
            
            list < string > msnText_p = new list < string > ();
            msnText_p.add('12345MSN'); //MSN Text
            
            list < string > lessorArray_p = new list < string > ();
            
            list < string > msnArray_p = new list < string > ();
            msnArray_p.add(AC1[0].Aircraft__c);
            
            list < string > WFArray_p = new list < string > ();
            list < string > esnArray_p = new list < string > ();
            list < string > atArray_p = new list < string > ();
            list < string > EmptyArray_p = new list < string > ();
            
            //
            list < string > OtherSellerType_p = new list < string > ();
            list < string > MSNNameText_p = new list < string > ();
            string AircraftType_p = '727';
            string Varient_p = '';
            string Engine_p = '';
            string EngineVariant_p = '';
            
            //Acquisition Airline
            CanvasApexController.callSaveApexTrans('TransName1', 'Acquisition', 'ST', 'SLB', 'Airline', OtherSellerType_p, 'MSN', msnText_p, null, null, AircraftType_p,
                                                   Varient_p, Engine_p, EngineVariant_p,
                                                   msnArray_p, EmptyArray_p, customerArray_p, EmptyArray_p, lessoridArray_p, EmptyArray_p);
            
            //Acquisition Lessor
            CanvasApexController.callSaveApexTrans('TransName1', 'Acquisition', 'ST', 'SLB', 'Airline', OtherSellerType_p, 'MSN', msnText_p, null, null, AircraftType_p,
                                                   Varient_p, Engine_p, EngineVariant_p,
                                                   msnArray_p, EmptyArray_p, EmptyArray_p, EmptyArray_p, lessoridArray_p, EmptyArray_p);
            
            //Acquisition Other
            CanvasApexController.callSaveApexTrans('TransName1', 'Acquisition', 'ST', 'SLB', 'Airline', OtherSellerType_p, 'MSN', msnText_p, null, null, AircraftType_p,
                                                   Varient_p, Engine_p, EngineVariant_p,
                                                   EmptyArray_p, EmptyArray_p, EmptyArray_p, EmptyArray_p, EmptyArray_p, EmptyArray_p);
            
            //Disposal Airline
            CanvasApexController.callSaveApexTrans('TransName1', 'Disposal', 'LOI', 'SLB', 'Airline', OtherSellerType_p, 'MSN', msnText_p, null, null, AircraftType_p,
                                                   Varient_p, Engine_p, EngineVariant_p,
                                                   msnArray_p, EmptyArray_p, customerArray_p, EmptyArray_p, lessoridArray_p, EmptyArray_p);
            
            customerArray_p.add('ae763816721v');
            //Exception
            CanvasApexController.callSaveApexTrans('TransName1', 'Disposal', 'LOI', 'SLB', 'Airline', OtherSellerType_p, 'MSN', msnText_p, null, null, AircraftType_p,
                                                   Varient_p, Engine_p, EngineVariant_p,
                                                   msnArray_p, EmptyArray_p, customerArray_p, EmptyArray_p, lessoridArray_p, EmptyArray_p);
        }
    }
    
    //Test for Marketing Campaign
    
    @isTest(SeeAllData = true)
    static void testCanvasCampaignController1() {
        
        string ACName = '%TestSeededMSN1%';
        List<Lease__c> AC1 = [select id, Aircraft__c, Operator__c from Lease__c where name like : ACName limit 1];
        if(AC1.size() > 0){
            list < string > customerArray_p = new list < string > ();
            customerArray_p.add(AC1[0].Operator__c); //Airlne
            
            list < string > lessorArray_p = new list < string > ();
            
            list < string > msnArray_p = new list < string > ();
            msnArray_p.add(AC1[0].Aircraft__c);
            
            list < string > WFArray_p = new list < string > ();
            list < string > esnArray_p = new list < string > ();
            list < string > atArray_p = new list < string > ();
            list < string > EmptyArray_p = new list < string > ();
            list < string > AircraftITArray_p = new list < string > ();
            //
            list < string > OtherSellerType_p = new list < string > ();
            list < string > MSNNameText_p = new list < string > ();
            string AircraftType_p = '';
            string Varient_p = '';
            string Engine_p = '';
            string EngineVariant_p = '';
            
            //Acquisiton Airline
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Aircraft New Order', 'CatrgotyText', 'Marketing', customerArray_p, EmptyArray_p, AircraftITArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
            
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Transaction', 'CatrgotyText', 'Marketing', customerArray_p, EmptyArray_p, AircraftITArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
            
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Current Fleet', 'CatrgotyText', 'Marketing', customerArray_p, EmptyArray_p, AircraftITArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
        }
    }
    
    //Test for Marketing Campaign (ok)
    
    @isTest(SeeAllData = true)
    static void testCanvasCampaignController2() {
        
        string ACName = '%TestSeededMSN1%';
        List<Lease__c> AC1 = [select id, Aircraft__c, Operator__c from Lease__c where name like : ACName limit 1];
        if(AC1.size() > 0){
            Operator__c ProsCustomer = [select id from Operator__c limit 1];
            //Aircraft__c Aircraft=[select id from Aircraft__c limit 1];
            
            Transaction__c myTxn = new Transaction__c(Name = 'My Txn', Ascend_Value__c = 3000000, Seller_Type__c = 'Airline',
                                                      Transaction_Type__c = 'Acquisition',
                                                      Estimated_Closing__c = system.today().addYears(1),
                                                      Seller_Airline__c = ProsCustomer.Id);
            
            LeaseWareUtils.clearFromTrigger();
            insert myTxn;
            Aircraft_In_Transaction__c AircraftInTrans = new Aircraft_In_Transaction__c(Transaction__c = myTxn.id, Name = '123');
            LeaseWareUtils.clearFromTrigger();
            insert AircraftInTrans;
            
            list < string > customerArray_p = new list < string > ();
            customerArray_p.add(AC1[0].Operator__c);
            list < string > lessorArray_p = new list < string > ();
            
            list < string > msnArray_p = new list < string > ();
            msnArray_p.add(AC1[0].Aircraft__c);
            
            list < string > WFArray_p = new list < string > ();
            list < string > esnArray_p = new list < string > ();
            list < string > atArray_p = new list < string > ();
            list < string > EmptyArray_p = new list < string > ();
            list < string > AircraftITArray_p = new list < string > ();
            AircraftITArray_p.add(AircraftInTrans.Id);
            //
            list < string > OtherSellerType_p = new list < string > ();
            list < string > MSNNameText_p = new list < string > ();
            string AircraftType_p = '';
            string Varient_p = '';
            string Engine_p = '';
            string EngineVariant_p = '';
            
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Aircraft New Order', 'CatrgotyText', 'Marketing', customerArray_p, EmptyArray_p, AircraftITArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
            
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Current Fleet', 'CatrgotyText', 'Marketing', customerArray_p, EmptyArray_p, AircraftITArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
            
            //passing aircrafft (not AIT)
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Transaction', 'CatrgotyText', 'Marketing', msnArray_p, EmptyArray_p, EmptyArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
            
            //Exception handling1
            msnArray_p.add('aefg656278');
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Transaction', 'CatrgotyText', 'Marketing', msnArray_p, EmptyArray_p, EmptyArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
            
            //exception handling
            lessorArray_p.add('a4321hff654fhfg65');
            CanvasApexController.callSaveApexCampaign('CampaignName', 'Current Fleet', 'CatrgotyText', 'Marketing', customerArray_p, EmptyArray_p, AircraftITArray_p, EmptyArray_p, 'PlacementPriorityText', customerArray_p, EmptyArray_p, lessorArray_p, EmptyArray_p);
        }
    }
    
}