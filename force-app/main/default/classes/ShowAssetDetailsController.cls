public with sharing class ShowAssetDetailsController {
    @AuraEnabled
    public static AircraftProposalWrapper getAssetTermForMarketingActivity(Id marketingActivity){

        List<Aircraft_Proposal__c> lstAssetTermNotFinal = [SELECT Id, Name, Deal_Analysis__c
                                                           FROM Aircraft_Proposal__c 
                                                           WHERE Marketing_Activity__c =: MarketingActivity];
                                                           
        Deal_Term_Configuration__mdt[] dealTermConfiguration = [SELECT Deal_Term_Header__c 
                                                                FROM Deal_Term_Configuration__mdt];
                                                                
        AircraftProposalWrapper aircraftProposalWrapperObj = new AircraftProposalWrapper(lstAssetTermNotFinal);
        
        aircraftProposalWrapperObj.dealTermHeader = dealTermConfiguration.size() > 0 && String.isNotBlank(dealTermConfiguration[0].Deal_Term_Header__c) ?
                                                    dealTermConfiguration[0].Deal_Term_Header__c : 'Assets not highlighted (green background) are missing Commercial Terms';

        
        return aircraftProposalWrapperObj; 
    }


    public class  AircraftProposalWrapper{
        @AuraEnabled
        public List<Aircraft_Proposal__c> lstAssetTermNotFinal;
        @AuraEnabled
        public String dealTermHeader;
        
        public AircraftProposalWrapper(List<Aircraft_Proposal__c> lstAssetTermNotFinal){
            this.lstAssetTermNotFinal = lstAssetTermNotFinal;
        }

    }
}