/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestXMLFinScheduler {

	@isTest(seeAllData=true)
    static  void myUnitTest() {
    	// for code coverage run against the existing data
	      /*  Test.startTest();
	        try{
		        
	        	XMLFinScheduler x= new XMLFinScheduler();
	        }catch(System.DmlException e){
	        	system.debug('Expected - Ignore');
	        }
	        
	        Test.stopTest(); */     	
    }

    /*
	@isTest(seeAllData=true)
    static void CRUDTest() {
        // TO DO: implement unit test
        System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
    	map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);

        System.debug('4.Number of Queries used in this apex code so far: ' + Limits.getQueries());

        mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');		
		TestLeaseworkUtil.createUR(mapInOut);
		System.debug('5.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        Id leaseId = (Id)mapInOut.get('Aircraft__c.lease__c');
        // Insert Invoice 
		TestLeaseworkUtil.setRecordTypeOfInvoice(mapInOut);
		TestLeaseworkUtil.setRecordTypeOfPayment(mapInOut);
		
		System.debug('6.Number of Queries used in this apex code so far: ' + Limits.getQueries());
		Test.startTest();
        Invoice__c newUrInv=new Invoice__c(Lease__c=leaseId, Name='Test UR Inv', RecordTypeId=(Id)mapInOut.get('Invoice__c.Aircraft MR'), Invoice_date__c=Date.today(),
                Invoice_Type__c='Aircraft MR', Refresh_Values_From_UR__c=true, 
                Utilization_Report__c=[select id from Utilization_Report__c where aircraft__r.lease__c=:leaseId  
                                       and status__c = 'Approved By Lessor' and MR_Assembly__c >0 limit 1].id );
        LeaseWareUtils.clearFromTrigger();insert newURInv;

		System.debug('7.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        newURInv.Status__c='Approved';
        LeaseWareUtils.clearFromTrigger();update newURInv;

		System.debug('8.Number of Queries used in this apex code so far: ' + Limits.getQueries());  //62

        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=newURInv.id, payment_date__c=date.today(), RecordTypeId=(Id)mapInOut.get('Payment__c.Aircraft MR'), Invoice_Type__c='Aircraft MR', Amount__c=1600);
        LeaseWareUtils.clearFromTrigger();insert newPayment;        
        
        System.debug('9.Number of Queries used in this apex code so far: ' + Limits.getQueries());  
        
	        
	        try{
		        
	        	ID batchprocessid = Database.executeBatch(new  XMLFinScheduler());
	        }catch(System.DmlException e){
	        	system.debug('Expected - Ignore');
	        }
	        
	        Test.stopTest();          
        
    }
*/
}