public with sharing class LegalEntityController {
    
    @AuraEnabled
    public static String getNamespacePrefix(){
         return LeaseWareUtils.getNamespacePrefix();
    }
    @AuraEnabled
    public static List<LegalEntityData> getLegalEntityData(String leaseId) {
        list<lease_hierarchy__c> leaseHrchyList = [select name,Legal_Entity__r.id,Legal_Entity__r.name,lease__c,RentPayable__c,RentPayableIn__c
							                        FROM lease_hierarchy__c WHERE lease__r.id = :leaseId and name !='Lessor' order by name asc];
        list<LegalEntityData> legalEntityList = new list<LegalEntityData>();
        for(lease_hierarchy__c hrcy:leaseHrchyList){
            legalEntityList.add(new LegalEntityData(hrcy.Legal_Entity__r.id,hrcy.name,hrcy.Legal_Entity__r.name,hrcy.RentPayableIn__c,hrcy.RentPayable__c));
       }
        return legalEntityList;
    }

    @AuraEnabled
    public static String saveLeaseHierarchy(String legalEntityString,String leaseId){
        System.debug('saveLeaseHierarchy(++)');
        list<LegalEntityData> legalEntityList = new list<LegalEntityData>();
        object[] values = (object[])System.JSON.deserializeUntyped(legalEntityString);
        String eachEntity ='';
        for(Object obj :values){
            eachEntity = JSON.serialize(obj);
            LegalEntityData data = (LegalEntityData)JSON.deserialize(eachEntity, LegalEntityData.class);
            legalEntityList.add(data);
        }
        Map<String,LegalEntityData> lessorMap = new Map<String,LegalEntityData>();
        System.debug('LegalEntityList:'+legalEntityList.size());
        System.debug('LegalEntityList:'+legalEntityList);
        for(LegalEntityData hrcy : legalEntityList){
            if(hrcy.Name =='Head Lessor'){
                lessorMap.put('HeadLessor',hrcy);
             }
            if(hrcy.Name =='Head Lessor II'){
                lessorMap.put('HeadLessorII',hrcy);
               } 
        }
        convertAndUpdateList(legalEntityList,leaseId,lessorMap);
        return null;
    }
    
    private static void convertAndUpdateList(list<LegalEntityData> legalEntityList,String leaseId, Map<String,LegalEntityData> lessorMap){
        list<lease_hierarchy__c> leaseHrchyList = [select Legal_Entity__r.id,name,Legal_Entity__r.name,lease__c,RentPayable__c,RentPayableIn__c
							                      FROM lease_hierarchy__c WHERE lease__r.id = :leaseId and name !='Lessor'];
       
            for(lease_hierarchy__c hrcy : leaseHrchyList){
                LegalEntityData data = lessorMap.get(hrcy.Name);
                     hrcy.RentPayableIn__c = data.PickAmountPercent;
                     hrcy.RentPayable__c =data.AmountorPercent; 
        }
               
             update leaseHrchyList;        
    }
    
    public class LegalEntityData{
        @AuraEnabled public string id{get;set;}
        @AuraEnabled public string Name{get;set;}
        @AuraEnabled public string rentPayableTo{get;set;}
        @AuraEnabled public string PickAmountPercent{get;set;}
        @AuraEnabled public Decimal AmountorPercent{get;set;}
        public LegalEntityData(Id id,String category,String entityName,String pickValue,Decimal amtorPercent){
            this.id = id;
            if(category.equals('HeadLessor'))
            this.Name = 'Head Lessor';
            else if (category.equals('HeadLessorII'))
            this.Name = 'Head Lessor II';
            this.rentPayableTo = entityName;
            this.PickAmountPercent = pickValue;
            this.AmountorPercent = amtorPercent;
        }
    }
}