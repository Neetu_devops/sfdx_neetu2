@isTest
public class TestDelinquency {
    @isTest(seeAllData=true)
    public static void testInvoiceDelinInsert(){
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN2';
        Aircraft__c AC1 = [select id,Lease__c,Assigned_To__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        
        Delinquency_Staging__c ds = new Delinquency_Staging__c(
            name = 'Test',
            MSN__c = 'TestSeededMSN2',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 19)
            ,MaintBal2__c  = 0.0
            ,RentBal2__c  = 0.0
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4637'
            ,Invoice_Type__c   = 'Rental'
            ,Invoice_Date__c = date.newInstance(2019, 05, 28)
            ,Due_Date__c = Date.newInstance(2019, 07, 15)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 228937.50
            ,Outstanding_Amount__c = 228937.5
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 0
            ,Total_Outstanding__c = 0.00
            ,Cash_Received_in_Last_30_Days__c = 100.00            
        );
        test.startTest();
        try{   
            LeaseWareUtils.clearFromTrigger();
            System.debug('1.Number of Queries : ' + Limits.getQueries());    
            insert ds;
            System.debug('2.Number of Queries : ' + Limits.getQueries());
            List<Aircraft_Delinquency__c> listAcDel = [select id,Lease__c,Amount_Outstanding_1_30_Days__c from Aircraft_Delinquency__c where Aircraft__c =: AC1.id];
            List<Invoice_Delinquency__c> listInvDel = [select id,Amount_Nt_Due__c,Amount_Outstnding_1_30_Days__c,Invoice_Amount__c from Invoice_Delinquency__c where Aircraft_Delinquency__c =: listAcDel[0].id];
            System.assert(listInvDel.size()>0,true);
            System.assertEquals(listAcDel[0].Lease__c,AC1.Lease__c);
            System.assertEquals(listInvDel[0].Amount_Nt_Due__c,listInvDel[0].Invoice_Amount__c);
        }catch (System.DmlException e) {
            System.debug(e);
        }  
        test.stopTest();
    }
    
    @isTest(seeAllData=true)
    public static void testAircraftConsolidatedFieldsForSameAsset(){
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN2';
        Aircraft__c AC1 = [select id,Lease__c,Assigned_To__c,lease__r.Operator__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        
        Test.startTest();
        dataSetup();
        
        List<Delinquency_Data__c> listDD = [select id
                                            ,Operator__r.Name
                                            ,Last_Delinquency_History__c
                                            ,Y_Hidden_Total_MR_Invoice_Amount__c
                                            ,Y_Hidden_Total_Rental_Invoice_Amount__c
                                            ,Y_Hidden_Total_Other_Invoice_Amount__c
                                            ,( select id,Week_Ending__c
                                              ,Total_Current_Period__c
                                              ,Total_MR_Invoice_Amount__c
                                              ,Total_Rental_Invoice_Amount__c
                                              ,Y_Hidden_Total_Other_Invoice_Amount__c
                                              from Delinquency_History__r
                                              order by Week_Ending__c desc
                                             )
                                            from Delinquency_Data__c where Operator__c =: AC1.lease__r.Operator__c];
        
        list<Delinquency_History__c>   listDH = [select id,Delinquency__c
                                                 , Delinquency__r.Operator__r.name
                                                 ,Current_MR_Balance__c
                                                 ,Current_Rent_Balance__c
                                                 ,Prev_MR_Balance__c,Prev_Rent_Balance__c
                                                 ,Week_Ending__c
                                                 ,Total_MR_Invoice_Amount__c
                                                 ,Total_Rental_Invoice_Amount__c
                                                 ,Y_Hidden_Total_Other_Invoice_Amount__c
                                                 ,( select id,name,MSN__c,Aircraft__c
                                                   ,Current_MR_Balance__c
                                                   ,MR_Ratio_Current_Period__c
                                                   ,Current_Rent_Balance__c
                                                   ,Rent_Ratio_Current_Period__c
                                                   ,Prev_MR_Balance__c,MR_Ratio_Previous_Period__c
                                                   ,Prev_Rent_Balance__c,Rent_Ratio_Previous_Period__c
                                                   ,Aircraft__r.Type_Variant__c
                                                   ,Aircraft__r.Engine_Type__c
                                                   ,Aircraft__r.Date_of_Manufacture__c
                                                   ,Aircraft__r.Availability_Date__c                                                                      
                                                   ,Amount_Not_Due__c
                                                   ,Amount_Outstanding_1_30_Days__c
                                                   ,Amount_Outstanding_31_60_Days__c
                                                   ,Amount_Outstanding_61_90_Days__c
                                                   ,Amount_Outstanding_Greater_Than_90_Days__c
                                                   ,Y_Hidden_Total_MR_Invoice_Amount__c
                                                   ,Y_Hidden_Total_Rental_Invoice_Amount__c
                                                   ,Y_Hidden_Total_Other_Invoice_Amount__c 
                                                   from Aircraft_Delinquency__r
                                                  )
                                                 from Delinquency_History__c 
                                                 where Delinquency__r.Operator__c =: AC1.lease__r.Operator__c
                                                 order by Week_Ending__c desc];
        
        List<Aircraft_Delinquency__c> listAD = [select id
                                             ,name 
                                             ,Delinquency_History__c
                                             ,Delinquency_History__r.Delinquency__c
                                             ,Delinquency_History__r.Delinquency__r.Operator__c
                                             ,Y_Hidden_Total_Rental_Invoice_Amount__c
                                             ,Y_Hidden_Total_MR_Invoice_Amount__c
                                             ,Y_Hidden_Total_Other_Invoice_Amount__c
                                             ,Aircraft__c
                                             ,Amount_Not_Due__c
                                             ,Amount_Outstanding_1_30_Days__c
                                             ,Amount_Outstanding_31_60_Days__c
                                             ,Amount_Outstanding_61_90_Days__c
                                             ,Amount_Outstanding_Greater_Than_90_Days__c
                                             from Aircraft_Delinquency__c 
                                             where Delinquency_History__r.Delinquency__r.Operator__c =: AC1.lease__r.Operator__c];

        System.assertEquals(2,listAD.size());

      //  System.assertEquals(20000,listAD[0].Y_Hidden_Total_Rental_Invoice_Amount__c);

        for(Delinquency_Data__c curDD: listDD){
            System.debug('Test : DD' + curDD);
              System.assertEquals(30000,curDD.Y_Hidden_Total_Rental_Invoice_Amount__c);
            for(Delinquency_History__c curHist: curDD.Delinquency_History__r){
                System.assertEquals(30000,curHist.Total_Rental_Invoice_Amount__c);
            }            
        }
        test.stopTest();
    }
    
    @isTest(seeAllData=true)
    public static void testOperatorConsolidatedFieldsForSameOperator(){
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN2';
        Aircraft__c AC1 = [select id,Lease__c,Assigned_To__c,lease__r.Operator__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        
        string ACName2 = 'TestSeededMSN3';
        Aircraft__c AC2 = [select id,Lease__c,Assigned_To__c,lease__r.Operator__c from Aircraft__c where MSN_Number__c = :ACName2 limit 1];
        mapInOut.put('Aircraft__c.Id',AC2.Id);
        mapInOut.put('Aircraft__c.lease__c',AC2.lease__c);
               
        Test.startTest();
        dataSetup();
        List<Operator__c> listOperator = [select id, Y_Hidden_Total_Amount_Not_Due__c,Y_Hidden_Total_Amount_Outstanding_1_30__c,Y_Hidden_Total_Amount_Outstanding_90__c,Y_Hidden_Total_Rental_Invoice_Amount__c,
        Y_Hidden_Average_Days_Overdue__c, Y_Hidden_Total_Cash_Rec_in_Last_30_Days__c from Operator__c where id =: AC1.lease__r.Operator__c];
        
        if(!listOperator.isEmpty() || listOperator != null){
            for(Operator__c curOp: listOperator){
                System.assertEquals(10000,listOperator[0].Y_Hidden_Total_Amount_Not_Due__c);
                System.assertEquals(20000,listOperator[0].Y_Hidden_Total_Amount_Outstanding_1_30__c);
                System.assertEquals(30000,listOperator[0].Y_Hidden_Total_Rental_Invoice_Amount__c);
                System.assertEquals(10000,listOperator[0].Y_Hidden_Total_Amount_Outstanding_90__c);
                System.assertEquals(39,listOperator[0].Y_Hidden_Average_Days_Overdue__c);
                System.assertEquals(500,listOperator[0].Y_Hidden_Total_Cash_Rec_in_Last_30_Days__c); 
            }
        }
        
        test.stopTest();
    }
    
    @isTest(seeAllData = true)
    public static void updateInvoiceDelinInsert(){
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN2';
        Aircraft__c AC1 = [select id,Lease__c,Assigned_To__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        
        Delinquency_Staging__c ds = new Delinquency_Staging__c(
            name = 'Test',
            MSN__c = 'TestSeededMSN2',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 15)
            ,MaintBal2__c  = 0.0
            ,RentBal2__c  = 0.0
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4637'
            ,Invoice_Type__c   = 'Rental'
            ,Invoice_Date__c = date.newInstance(2019, 05, 28)
            ,Due_Date__c = Date.newInstance(2019, 07, 15)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 10000
            ,Outstanding_Amount__c = 10000
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 0
            ,Total_Outstanding__c = 10000
            ,Cash_Received_in_Last_30_Days__c = 0.00            
        );
        insert ds;
        test.startTest();
        try{   
            Delinquency_Staging__c ds1 = new Delinquency_Staging__c(
            name = 'Test',
            MSN__c = 'TestSeededMSN2',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 19)
            ,MaintBal2__c  = 0.0
            ,RentBal2__c  = 0.0
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4637'
            ,Invoice_Type__c   = 'Rental'
            ,Invoice_Date__c = date.newInstance(2019, 05, 28)
            ,Due_Date__c = Date.newInstance(2019, 07, 15)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 10000
            ,Outstanding_Amount__c = 10000
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 3
            ,Total_Outstanding__c = 10000
            ,Cash_Received_in_Last_30_Days__c = 0.00            
        );

            insert ds1;
            
            List<Aircraft_Delinquency__c> listAcDel = [select id,Lease__c,Amount_Outstanding_1_30_Days__c,
                                                    (Select id,name
                                                    ,Invoice_Type__c
                                                    ,Invoice_Amount__c
                                                    ,Amount_Nt_Due__c
                                                    ,Amount_Outstnding_1_30_Days__c
                                                     from Invoice_Delinquency__r) from Aircraft_Delinquency__c where Aircraft__c =: AC1.id];


            Set<id> setADId = new Set<id>();
            for (Aircraft_Delinquency__c variable : listAcDel) {
                setADId.add(variable.id);
            }

            List<Invoice_Delinquency__c> listInvDel = [select id,name,Amount_Nt_Due__c,Amount_Outstnding_1_30_Days__c,Invoice_Amount__c from Invoice_Delinquency__c where Aircraft_Delinquency__c in: setADId];


            System.assertEquals(2,listInvDel.size());
           // System.assertEquals(10000,listInvDel[0].Amount_Nt_Due__c);
           /// System.assertEquals(0,listInvDel[1].Amount_Nt_Due__c);
            //System.assertEquals(10000,listInvDel[1].Amount_Outstnding_1_30_Days__c);

        }catch (System.DmlException e) {
            System.debug(e);
        }  
        test.stopTest();
    }

     @isTest(seeAllData = true)
    public static void updateInvoiceInsertNegative(){
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN2';
        Aircraft__c AC1 = [select id,Lease__c,Assigned_To__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        
        Delinquency_Staging__c ds = new Delinquency_Staging__c(
            name = 'Test',
            MSN__c = 'TestSeededMSN2',
            CompanyName__c = null,
            RunDate2__c = null
            ,MaintBal2__c  = 0.0
            ,RentBal2__c  = 0.0
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4637'
            ,Invoice_Type__c   = 'Rental'
            ,Invoice_Date__c = date.newInstance(2019, 05, 28)
            ,Due_Date__c = Date.newInstance(2019, 07, 15)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 10000
            ,Outstanding_Amount__c = 10000
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 0
            ,Total_Outstanding__c = 10000
            ,Cash_Received_in_Last_30_Days__c = 0.00            
        );
       
        test.startTest();
        try{   
            insert ds;
            List<Aircraft_Delinquency__c> listAcDel = [select id,Lease__c,Amount_Outstanding_1_30_Days__c,
                                                    (Select id,name
                                                    ,Invoice_Type__c
                                                    ,Invoice_Amount__c
                                                    ,Amount_Nt_Due__c
                                                    ,Amount_Outstnding_1_30_Days__c
                                                     from Invoice_Delinquency__r) from Aircraft_Delinquency__c where Aircraft__c =: AC1.id];

            System.assertEquals(0,listAcDel.size());

        }catch (DmlException e) {
            System.debug(e);
        }catch (Exception e) {
            System.debug(e);
        }  
        test.stopTest();
    }

    private static void dataSetup(){
        
        Delinquency_Staging__c ds1 = new Delinquency_Staging__c(
            name = 'Test1',
            MSN__c = 'TestSeededMSN2',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 15)
            ,MaintBal2__c  = 0.0
            ,RentBal2__c  = 0.0
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4637'
            ,Invoice_Type__c   = 'Rental'
            ,Invoice_Date__c = date.newInstance(2019, 03, 04)
            ,Due_Date__c = Date.newInstance(2019, 04, 10)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 10000
            ,Outstanding_Amount__c = 10000
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 0
            ,Total_Outstanding__c = 10000
            ,Cash_Received_in_Last_30_Days__c = 100.00            
        );
        insert ds1;
        
        Delinquency_Staging__c ds2 = new Delinquency_Staging__c(
            name = 'Test2',
            MSN__c = 'TestSeededMSN2',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 15)
            ,MaintBal2__c  = 0.0
            ,RentBal2__c  = 0.0
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4638'
            ,Invoice_Type__c   = 'Rental'
            ,Invoice_Date__c = date.newInstance(2019, 07, 03)
            ,Due_Date__c = Date.newInstance(2019, 07, 10)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 20000
            ,Outstanding_Amount__c = 20000
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 5
            ,Total_Outstanding__c = 20000
            ,Cash_Received_in_Last_30_Days__c = 200.00              
        );       
        insert ds2;
        
        Delinquency_Staging__c ds3 = new Delinquency_Staging__c(
            name = 'Test3',
            MSN__c = 'TestSeededMSN3',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 15)
            ,MaintBal2__c  = 0.0
            ,RentBal2__c  = 0.0
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4639'
            ,Invoice_Type__c   = 'Rental'
            ,Invoice_Date__c = date.newInstance(2019, 03, 27)
            ,Due_Date__c = Date.newInstance(2019, 03, 27)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 10000
            ,Outstanding_Amount__c = 10000
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 110
            ,Total_Outstanding__c = 5000
            ,Cash_Received_in_Last_30_Days__c = 200.00              
        );       
        insert ds3;
    }

     @isTest(seeAllData = true)
    public static void testAegingBatch(){
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__r.Operator__r.Name, MSN_Number__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        
        Lease__c lease = [select id,Name,Lease_Start_Date_New__c,Lease_End_Date_New__c,Aircraft__c from lease__c where
        Aircraft__c =  :AC1.Id limit 1];

        lease.Lease_Start_Date_New__c=date.valueOf('2018-01-01');
        lease.Lease_End_Date_New__c=date.valueOf('2022-12-31');
        update lease;
        //Utilization_Report__c util = [Select id, Name from Utilization_Report__c where Aircraft__c =:AC1.Id];

        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='50',
            Airframe_Cycles_Landing_During_Month__c= 50,
            Aircraft__c = AC1.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'Actual',
            Month_Ending__c = Date.newInstance(2018,02,28)           
        );

        insert utilizationRecord;
        Utilization_Report__c utilzation1 = [select Id ,Status__c from Utilization_Report__c where Id=:utilizationRecord.id ];
        utilzation1.Status__c='Approved By Lessor';
        update utilzation1;
  	
        List<Invoice__c> listIncv = [Select id from Invoice__c where Utilization_Report__c =:utilizationRecord.Id];

        if(listIncv.size()>0){
            Invoice__c inv = listIncv[0];
            inv.Status__c = 'Approved';
            update inv;
        }
        test.startTest();
        LeaseWareUtils.clearFromTrigger();
       
        DelinquencyScheduler ds = new DelinquencyScheduler();
        try{
            DelinquencyScheduler.executeDelinquencySchedulerBatch() ;
        }catch(DmlException e){
            system.debug(e);
            LeaseWareUtils.createExceptionLog(e, 'Insert of delinquency staging failed from scheduler', null, null,'Delinquency', true);
        }
        List<Delinquency_Staging__c> delStaging = [Select id from Delinquency_Staging__c where MSN__c =:AC1.MSN_Number__c];
        test.stopTest();
    } 

        private static void dataSetupForACG(){
        
        Delinquency_Staging__c ds1 = new Delinquency_Staging__c(
            name = 'Test1',
            MSN__c = 'TestSeededMSN2',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 15)
            ,MaintBal2__c  = 5000
            ,RentBal2__c  = 1000
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4637'
            ,Invoice_Type__c   = ' '
            ,Invoice_Date__c = date.newInstance(2019, 03, 04)
            ,Due_Date__c = Date.newInstance(2019, 04, 10)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 6000
            ,Outstanding_Amount__c = 0.0
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 0
            ,Total_Outstanding__c = 0.0
            ,Cash_Received_in_Last_30_Days__c = 0.0            
        );
        insert ds1;
      
        Delinquency_Staging__c ds3 = new Delinquency_Staging__c(
            name = 'Test3',
            MSN__c = 'TestSeededMSN3',
            CompanyName__c = 'TestSeededOPER2',
            RunDate2__c = Date.newInstance(2019, 07, 15)
            ,MaintBal2__c  = 1000
            ,RentBal2__c  = 1000
            ,MaintBal1__c = 0.0
            ,RentBal1__c  = 0.0
            ,MaintRatio2__c  = 0.0 
            ,RentRatio2__c   = 0.0
            ,MaintRatio1__c   = 0.0
            ,RentRatio1__c  = 0.0
            ,Invoice_Number__c = '4639'
            ,Invoice_Type__c   = ' '
            ,Invoice_Date__c = date.newInstance(2019, 03, 27)
            ,Due_Date__c = Date.newInstance(2019, 03, 27)
            ,Invoice_Description__c = 'Rental covering period 15/Jul/2019 to 14/Aug/2019'
            ,Invoice_Amount__c = 2000
            ,Outstanding_Amount__c = 0.0
            ,Invoice_Currency__c = 'USD'
            ,Days_Overdue__c = 110
            ,Total_Outstanding__c = 0.0
            ,Cash_Received_in_Last_30_Days__c = 0.0           
        );       
        insert ds3;
    }
      @isTest(seeAllData = true)
    public static void testACGSpecificDelinq(){
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN2';
        Aircraft__c AC1 = [select id,Lease__c,Assigned_To__c,lease__r.Operator__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        
        Test.startTest();
        dataSetupForACG();
        
        List<Delinquency_Data__c> listDD = [select id
                                            ,Operator__r.Name
                                            ,Last_Delinquency_History__c
                                            ,Total_Outstnding__c
                                            ,Total_Outstanding_Rental_Invoice_Amount__c
                                            ,Total_Outstanding_MR_Invoice_Amoun__c
                                            ,Y_Hidden_Total_MR_Invoice_Amount__c
                                            ,Y_Hidden_Total_Rental_Invoice_Amount__c
                                            ,Y_Hidden_Total_Other_Invoice_Amount__c
                                            ,( select id,Week_Ending__c
                                              ,Total_Current_Period__c
                                              ,Total_Outstnding__c
                                              ,Total_MR_Invoice_Amount__c
                                              ,Total_Rental_Invoice_Amount__c
                                              ,Y_Hidden_Total_Other_Invoice_Amount__c
                                              from Delinquency_History__r
                                              order by Week_Ending__c desc
                                             )
                                            from Delinquency_Data__c where Operator__c =: AC1.lease__r.Operator__c];
        
        list<Delinquency_History__c>   listDH = [select id,Delinquency__c
                                                 , Delinquency__r.Operator__r.name
                                                 ,Current_MR_Balance__c
                                                 ,Current_Rent_Balance__c
                                                 ,Prev_MR_Balance__c,Prev_Rent_Balance__c
                                                 ,Week_Ending__c
                                                 ,Total_MR_Invoice_Amount__c
                                                 ,Total_Rental_Invoice_Amount__c
                                                 ,Total_Outstnding__c
                                                 ,Y_Hidden_Total_Other_Invoice_Amount__c
                                                 ,( select id,name,MSN__c,Aircraft__c
                                                   ,Current_MR_Balance__c
                                                   ,MR_Ratio_Current_Period__c
                                                   ,Current_Rent_Balance__c
                                                   ,Rent_Ratio_Current_Period__c
                                                   ,Prev_MR_Balance__c,MR_Ratio_Previous_Period__c
                                                   ,Prev_Rent_Balance__c,Rent_Ratio_Previous_Period__c
                                                   ,Aircraft__r.Type_Variant__c
                                                   ,Aircraft__r.Engine_Type__c
                                                   ,Aircraft__r.Date_of_Manufacture__c
                                                   ,Aircraft__r.Availability_Date__c                                                                      
                                                   ,Amount_Not_Due__c
                                                   ,Amount_Outstanding_1_30_Days__c
                                                   ,Amount_Outstanding_31_60_Days__c
                                                   ,Amount_Outstanding_61_90_Days__c
                                                   ,Amount_Outstanding_Greater_Than_90_Days__c
                                                   ,Y_Hidden_Total_MR_Invoice_Amount__c
                                                   ,Y_Hidden_Total_Rental_Invoice_Amount__c
                                                   ,Y_Hidden_Total_Other_Invoice_Amount__c 
                                                   from Aircraft_Delinquency__r
                                                  )
                                                 from Delinquency_History__c 
                                                 where Delinquency__r.Operator__c =: AC1.lease__r.Operator__c
                                                 order by Week_Ending__c desc];
        
        List<Aircraft_Delinquency__c> listAD = [select id
                                             ,name 
                                             ,Delinquency_History__c
                                             ,Delinquency_History__r.Delinquency__c
                                             ,Delinquency_History__r.Delinquency__r.Operator__c
                                             ,Y_Hidden_Total_Rental_Invoice_Amount__c
                                             ,Y_Hidden_Total_MR_Invoice_Amount__c
                                             ,Y_Hidden_Total_Other_Invoice_Amount__c
                                             ,Aircraft__c
                                             ,Amount_Not_Due__c
                                             ,Amount_Outstanding_1_30_Days__c
                                             ,Amount_Outstanding_31_60_Days__c
                                             ,Amount_Outstanding_61_90_Days__c
                                             ,Amount_Outstanding_Greater_Than_90_Days__c
                                             from Aircraft_Delinquency__c 
                                             where Delinquency_History__r.Delinquency__r.Operator__c =: AC1.lease__r.Operator__c];

        System.assertEquals(2,listAD.size());

        //System.assertEquals(20000,listAD[0].Y_Hidden_Total_Rental_Invoice_Amount__c);

        for(Delinquency_Data__c curDD: listDD){
                System.debug('DD from TEST : Total_Outstanding__c :  ' + curDD.Total_Outstnding__c);
                System.debug('DD from TEST : Total_Outstanding_Rental_Invoice_Amount__c :  '+ curDD.Total_Outstanding_Rental_Invoice_Amount__c);
                 System.debug('DD from TEST : Total_Outstanding_MR_Invoice_Amoun__c : ' + curDD.Total_Outstanding_MR_Invoice_Amoun__c);
               System.assertEquals(2000,curDD.Total_Outstanding_Rental_Invoice_Amount__c);
            for(Delinquency_History__c curHist: curDD.Delinquency_History__r){
                System.debug(curHist);
                 System.debug('DH from TEST : Total_Outstanding__c :  ' + curHist.Total_Outstnding__c);
                 System.debug('DH from TEST : Total_MR_Invoice_Amount__c :  ' + curHist.Total_MR_Invoice_Amount__c);
                 System.debug('DH from TEST : Total_Rental_Invoice_Amount__c :  ' + curHist.Total_Rental_Invoice_Amount__c);
                System.assertEquals(6000,curHist.Total_MR_Invoice_Amount__c);
            }            
        }
        test.stopTest();
    }
}