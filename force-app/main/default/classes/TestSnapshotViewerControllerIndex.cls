@isTest
public class TestSnapshotViewerControllerIndex {
	
    @isTest
    static void loadTestCases() {
        Date myDate1 = Date.newInstance(2010, 2, 17);
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'V2524',
            MSN_Number__c = '1230',
            MTOW_Leased__c = 134455,
            Aircraft_Type__c = 'A320',
            Aircraft_Variant__c = '400',
            Date_of_Manufacture__c = myDate1 );
        LeaseWareUtils.clearFromTrigger();
        insert aircraft;
        SnapshotCreatorControllerIndex.createSnapshots(aircraft.Id, 'Test class');
    	SnapshotViewerControllerIndex.getAllSnapshots(aircraft.Id, 1, 10);
        List<Snapshot__c>  snapList = [select Id from Snapshot__c limit 1];
        if(snapList != null && snapList.size() > 0 ) {
        	SnapshotViewerControllerIndex.getSingleSnapshotData(snapList[0].Id, aircraft.Id);
            SnapshotViewerControllerIndex.getSnapshotRecDetail(snapList[0].Id, aircraft.Id);
        }
        
        
    }
    
}