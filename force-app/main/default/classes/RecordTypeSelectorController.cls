/**
*******************************************************************************
* Class: RecordTypeSelectorController
* @author Created by Bhavna, Lease-Works, 
* @date 10/06/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to find Record Types of a given Object
*
* History:
* - VERSION   DEVELOPER NAME    	DATE         	DETAIL FEATURES
*
********************************************************************************
*/
public class RecordTypeSelectorController {
    /**
    * @description Retreives RecordTypes for the given Object Name
    *
    * @param objectName - String, Object name for which record types need to be find
    *
    * @return rtWrapper - RecordTypeWrapper, RecordTypeWrapper record
    */
	@AuraEnabled
    public static RecordTypeWrapper getRecordTypes(String objectName) {
        List<RecordTypes> recordTypesList = new List<RecordTypes>();
        String defaultRecordType;
        
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + '' + objectName);
             
        for(Schema.RecordTypeInfo rti : objectDescribe.getRecordTypeInfos()) {
            if(rti.isDefaultRecordTypeMapping()) {
                defaultRecordType = rti.getRecordTypeId();
            }
            
            if(rti.isActive()) {
                if(rti.getName() != 'Master') {
                    RecordTypes rtRecord = new RecordTypes(rti.getRecordTypeId(), rti.getName());
                    recordTypesList.add(rtRecord);
                }
            }            
        }
        
        RecordTypeWrapper rtWrapper = new RecordTypeWrapper(defaultRecordType, recordTypesList);
        
        return rtWrapper;
    }
    
    
    /**
    * @description RecordTypeWrapper to hold details related to RecordTypes of the Object
    *
    */
    public class RecordTypeWrapper {
        @AuraEnabled public String defaultRecordType {get; set;}
        @AuraEnabled public List<RecordTypes> recordTypeList {get; set;}
        
        public RecordTypeWrapper(String defaultRecordType, List<RecordTypes> recordTypeList) {
            this.defaultRecordType = defaultRecordType;
            this.recordTypeList = recordTypeList;
        }
    }
    
    
    /**
    * @description RecordTypes to hold details related to each RecordType of the Object
    *
    */
    public class RecordTypes {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String name {get; set;}
        
        public RecordTypes(String Id, String name) {
            this.Id = Id;
            this.name = name;
        }
    }
}