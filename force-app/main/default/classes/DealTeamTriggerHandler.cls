public class DealTeamTriggerHandler implements ITrigger{

    
    // Constructor
    private final string triggerBefore = 'DealTeamTriggerHandlerBefore';
    private final string triggerAfter = 'DealTeamTriggerHandlerAfter';
    public DealTeamTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        for(Deal_Team__c curRec : (List<Deal_Team__c>)trigger.new){
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Complete__c))curRec.Recipients_on_Complete__c.addError('Invalid email address or comma separated list of emails.');
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Incomplete__c))curRec.Recipients_on_Incomplete__c.addError('Invalid email address or comma separated list of emails.');
            
            if(curRec.Complete__c) curRec.Completed_On__c = system.now();
            else curRec.Completed_On__c = null;
        }
            
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('PricingAnalysisTriggerHandler.beforeUpdate(+)');
        
        for(Deal_Team__c curRec : (List<Deal_Team__c>)trigger.new){
            Deal_Team__c oldRec =(Deal_Team__c) Trigger.oldMap.get(curRec.Id); 
            if(curRec.Complete__c != oldRec.Complete__c) {
                if(curRec.Complete__c) curRec.Completed_On__c = system.now();
                else curRec.Completed_On__c = null;
            }

            system.debug('curRec.Completed_On__c-----'+ curRec.Completed_On__c);
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Complete__c))curRec.Recipients_on_Complete__c.addError('Invalid email address or comma separated list of emails.');
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Incomplete__c))curRec.Recipients_on_Incomplete__c.addError('Invalid email address or comma separated list of emails.');
        }
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

          

    }
     
    public void afterInsert()
    {
        updateDealTeamsOnDeal(trigger.new);         
    }
     
    public void afterUpdate()
    {
       
        List<Deal_Team__c> dealTeamListToUpdate = new List<Deal_Team__c>();
        for(Deal_Team__c curRec : (List<Deal_Team__c>)trigger.new){
            Deal_Team__c oldRec =(Deal_Team__c) Trigger.oldMap.get(curRec.Id); 
            if(curRec.Complete__c != oldRec.Complete__c) {
                dealTeamListToUpdate.add(curRec);
            }
        }
        if(dealTeamListToUpdate.size()>0){
            updateDealTeamsOnDeal(dealTeamListToUpdate); 
        }
    }
     
    public void afterDelete()
    {
        updateDealTeamsOnDeal(trigger.old);  
    }

    public void afterUnDelete()
    {
             
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


    private void updateDealTeamsOnDeal(list<Deal_Team__c> newList){
        
        set<id> setNoSelectOnDeletion = new set<id>();
        set<id> maActivityIdSet = new set<id>();
        for(Deal_Team__c curRec:newList){
            maActivityIdSet.add(curRec.Marketing_Activity__c);
            if(trigger.isDelete) setNoSelectOnDeletion.add(curRec.id);
        }
        if(!maActivityIdSet.isEmpty()){
            List<Marketing_Activity__c> dealList = [select Id,Revision_Start_Date__c,Teams_Summary_Format__c,Completed_teams__c,Not_Completed_Teams__c, (select Id,Name ,Complete__c,Completed_On__c,Delay__c from Deal_Teams__r where id not in :setNoSelectOnDeletion order by Sort_Order__c asc) from Marketing_Activity__c where Id in :MaActivityIdSet ];
            String substituteStr;
            List<Deal_Team__c> dealTeamListToUpdate = new List<Deal_Team__c>();
            for(Marketing_Activity__c dealRec : dealList){
                substituteStr='';
                dealRec.Not_Completed_Teams__c='';
                dealRec.Completed_Teams__c='';
                //Default being Newline & Delay
                if(String.isEmpty(dealRec.Teams_Summary_Format__c))dealRec.Teams_Summary_Format__c = 'Newline & Delay';
                if(dealRec.Teams_Summary_Format__c.equals('Comma-separated')){
                    substituteStr=', ';
                }else {
                    substituteStr ='\n';
                }
               
                system.debug('dealRec.Teams_Summary_Format__c----'+dealRec.Teams_Summary_Format__c);
                for(Deal_Team__c dealTeamRec : dealRec.Deal_Teams__r){
                   
                    if(dealTeamRec.Complete__c ){
                        String delay='';
                         //If the Completed Date is not null and Revision startdate is not null and Completed Date > Revision Start Date calculate delay else append the stored delay
                        if(dealTeamRec.Completed_On__c !=null && dealRec.Revision_Start_Date__c!=null && dealTeamRec.Completed_On__c>dealRec.Revision_Start_Date__c){
                            system.debug('Calculate Delay and append.....');
                           
                            //Calculate the Delay and append to the Completed Teams
                            delay = getDelay(dealTeamRec.Completed_On__c, dealRec.Revision_Start_Date__c);
                            system.debug('delay----'+delay);
                            dealTeamRec.Delay__c = delay;
                            dealTeamListToUpdate.add(dealTeamRec);
                        }else{
                            //Append stored delay to the Completed Teams
                            delay = LeasewareUtils.blankIfNull(dealTeamRec.Delay__c);
                        }

                        if(dealrec.Teams_Summary_Format__c.equals('Newline & Delay')){
                            dealRec.Completed_Teams__c = String.isBlank(dealRec.Completed_Teams__c)?dealTeamRec.name +'  ('+delay +') ' : dealRec.Completed_Teams__c+'\n'+dealTeamRec.name+' ('+ delay +')';
                        }else{
                            dealRec.Completed_Teams__c = String.isBlank(dealRec.Completed_Teams__c)?dealTeamRec.name : dealRec.Completed_Teams__c+substituteStr+dealTeamRec.name;
                        }
                    

                    }else{
                        
                          dealRec.Not_Completed_Teams__c = String.isBlank(dealRec.Not_Completed_Teams__c)?dealTeamRec.name : dealRec.Not_Completed_Teams__c+substituteStr+dealTeamRec.name;
                          dealTeamRec.Delay__c = '';
                          dealTeamListToUpdate.add(dealTeamRec);
                    }
                    
                    

            }

            }

            LeaseWareUtils.TriggerDisabledFlag = true;
            if(dealTeamListToUpdate.size()>0){
                update dealTeamListToUpdate;
            }
            update dealList;
            LeaseWareUtils.TriggerDisabledFlag = false;
        }
    }



    private String getDelay(DateTime endTime, DateTime startTime){
       
        Decimal delay=0;
        if(endTime != null && startTime != null){
            system.debug('calculate Delay---' +endTime + '-'+ startTime);
            decimal timeInMillisec = decimal.valueOf((endTime.getTime() - startTime.getTime()));
            system.debug('timeInMillisec---' +timeInMillisec);
            decimal hoursnew = timeInMillisec/(1000*60*60);
            system.debug('hoursnew----'+hoursnew);
            if(hoursnew >=24 ){
                //If delay is greater than 24 show in days
                system.debug('Show in Days');
                delay = Decimal.valueOf( ((hoursnew /24).setScale(2, RoundingMode.HALF_DOWN)).format());
                return delay==1?delay+' day' :delay+' days';
            }else{
                //If delay is less than 24 show in hrs
                //If delay is 0, then show in mins
                if(hoursnew<1) {
                    system.debug('Show in minutes');
                    decimal minutesnew = (timeInMillisec/(1000*60)).setScale(0);
                    system.debug('minutesnew----'+ minutesnew);
                    return minutesnew<=1?minutesnew+' min' :minutesnew+' mins';
                }
                else{
                    system.debug('Show in Hours');
                    delay = hoursNew.setScale(0, RoundingMode.DOWN);
                    return delay==1?delay+' hour' :delay+' hours';
                }
            
            }
        }
        return '';
    }
}