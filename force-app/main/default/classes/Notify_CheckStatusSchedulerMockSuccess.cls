@isTest
public class Notify_CheckStatusSchedulerMockSuccess implements WebServiceMock {
    public void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            Notify_MetadataService.DeployResult deployRes = new Notify_MetadataService.DeployResult();
            deployRes.done= false;
            deployRes.id='0Af6w00000676XJCAY';
            deployRes.success=true;
            deployRes.status='Success';
            Notify_MetadataService.checkDeployStatusResponse_element respElement = new Notify_MetadataService.checkDeployStatusResponse_element();
            respElement.result = deployRes;
            response.put('response_x', respElement); 
        }
    
    
    
}