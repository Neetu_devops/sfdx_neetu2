public class InvoiceLineItemHandler implements ITrigger{
    
    private final string triggerBefore = 'InvoiceLineItemHandlerBefore';
    private final string triggerAfter = 'InvoiceLineItemHandlerAfter';
    
    
    
    public InvoiceLineItemHandler()
    {
    }
    
    public void bulkBefore()
    {
    }
    
    public void bulkAfter()
    {
    }
    
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('InvoiceLineItemHandler.beforeInsert(+)');
        insUpdInvoiceLineItem();
        system.debug('InvoiceLineItemHandler.beforeInsert(-)');     
    }
    
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InvoiceLineItemHandler.beforeUpdate(+)');
        if(!LeaseWareUtils.isFromTrigger('AllowDMLOnUR')){//validation should not be called if it is from reconciliation.
            validateLIUpdate();
        }
        insUpdInvoiceLineItem();
        system.debug('InvoiceLineItemHandler.beforeUpdate(-)');
    }
    
    public void beforeDelete()
    {  
        
        system.debug('InvoiceLineItemHandler.beforeDelete(+)');
        
        UpdateTotalOnInvoice();
        
        system.debug('InvoiceLineItemHandler.beforeDelete(-)');         
        
    }
    
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InvoiceLineItemHandler.afterInsert(+)');
        
        UpdateTotalOnInvoice();
        
        system.debug('InvoiceLineItemHandler.afterInsert(-)');      
    }
    
    public void afterUpdate()
    {
        
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InvoiceLineItemHandler.afterUpdate(+)');
        
        UpdateTotalOnInvoice();
        
        system.debug('InvoiceLineItemHandler.afterUpdate(-)');      
    }
    
    public void afterDelete()
    {
        system.debug('InvoiceLineItemHandler.afterDelete(+)');
        
        
        system.debug('InvoiceLineItemHandler.afterDelete(-)');      
    }
    
    public void afterUnDelete()
    {
        system.debug('InvoiceLineItemHandler.afterUnDelete(+)');
        
        system.debug('InvoiceLineItemHandler.afterUnDelete(-)');        
    }
    
    public void andFinally()
    {
    }
    
    //before insert, before update
    private void insUpdInvoiceLineItem(){
        
        list<Invoice_Line_Item__c> listNew = (list<Invoice_Line_Item__c>)trigger.New;
        list<Id> listSRUIds = new list<Id>();
        
        String NonMRRecTypeID = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId(); //API name is 'Rent' but record type name is Non-MR
        for(Invoice_Line_Item__c curInvLI: listNew){
            
            if(curInvLI.RecordTypeId != NonMRRecTypeID)
            {
                if(trigger.isInsert && 'Provision'.equals(curInvLI.Y_Hidden_UR_Type__c)){
                    curInvLI.Y_Hidden_Flight_Hours_FH__c = curInvLI.Flight_Hours_FH__c;
                    curInvLI.Y_Hidden_Flight_Cycles_FC__c = curInvLI.Flight_Cycles_FC__c;
                }                
                listSRUIds.add(curInvLI.Assembly_Utilization__c);
            }            
        }
        //Added by PriyankaC : used to update the rich text field : Line Item Details
        
        Map<Id,Utilization_Report_List_Item__c> mapSuppRentUtil;
        
        if(listSRUIds.size() > 0)
        {
            mapSuppRentUtil = new Map<Id,Utilization_Report_List_Item__c>([Select Id,Name,Assembly_MR_Info__r.Rounding_Decimals_Interpolated_MR_Rate__c,Assembly_MR_Info__r.Rounding_Method_on_Interpolated_MR_Rate__c,
                                                                  Assembly_MR_Info__r.Monthly_Threshold_Amount__c,Assembly_MR_Info__r.Flat_Rate_Upto_Threshold__c,Assembly_MR_Info__r.Monthly_Threshold_Guaranteed__c,
                                                                  Assembly_MR_Info__r.Derate_Interpolation_Choice__c,Assembly_MR_Info__r.Larger_Of_FH_FC__c,
                                                                  Type_F__c,Assembly_MR_Info__r.Ratio_Table__r.Name,Effective_Unit__c,Interpolation_Rate_Choice__c,Y_Hidden_Adjustment_Amount_Lower__c,Y_Hidden_Adjustment_Amount_Upper__c,
                                                                  Effective_FH_FC_Ratio__c,Effective_Derate__c,Y_Hidden_Effective_Rate_Calculated__c,Net_Units_F__c,
                                                                  Effective_Rate_Final__c,Effective_Escalation_Factor__c,Rate_Beyond_Threshold_F__c
                                                                  From Utilization_Report_List_Item__c 
                                                                  Where Id in :listSRUIds]);
        
        }
                
        if(mapSuppRentUtil != null && mapSuppRentUtil.size() > 0){
         System.debug('insUpdInvoiceLineItem SRU '+mapSuppRentUtil.size());
            for(Invoice_Line_Item__c curInvLI: listNew){
                  Utilization_Report_List_Item__c sru = mapSuppRentUtil.get(curInvLI.Assembly_Utilization__c);        
                
                if(sru == null) {continue;}
                	System.debug(' if 1'+ sru.Id +' --'+curInvLI.Assembly_Utilization__c);
                    String Rate, Ratio,Derate,DerateChoice,EscalatedRate,EscalationFactor,Rounding;
                           String InterpolationChoice,NetUnits,EffectiveUnits,RateRangeL,RateRangeU,MonthlyThresholdAmount,RateUptoThreshold;
                           String LargerOfFH_FC,MonthlyThresholdGuaranteed;
                           
                           //for Rate
                           if(sru.Rate_Beyond_Threshold_F__c != null){
                               Decimal Rateformat = sru.Rate_Beyond_Threshold_F__c; // this need to be changed as we are no longer populating the field (MR Rate Escalated then Interpolated) :As per @anjani, this is on hold so not making the change now 
                               Rate = (Rateformat.format().contains('.')?Rateformat.format():(Rateformat.format()+'.00'));
                           }
                           else{
                               Rate = '0 (not set)';
                           }
                           
                           if((sru.Assembly_MR_Info__r.Ratio_Table__c != null || sru.Assembly_MR_Info__r.Ratio_Table__c != '') && sru.Effective_Unit__c != 'Monthly'){
                           //for RatesRange Lower
                           if(sru.Y_Hidden_Adjustment_Amount_Lower__c != null){
                               RateRangeL = String.valueOf(sru.Y_Hidden_Adjustment_Amount_Lower__c.setscale(2));
                           }
                           else{
                               RateRangeL = '0 (not set)';
                           }
                           //for RatesRange Upper
                           if(sru.Y_Hidden_Adjustment_Amount_Upper__c != null){
                               RateRangeU = String.valueOf(sru.Y_Hidden_Adjustment_Amount_Upper__c.setscale(2));
                           }
                           else{
                               RateRangeU = '0 (not set)';
                           }
                           //for Ratio
                           if(sru.Effective_FH_FC_Ratio__c != null){
                               Ratio = String.valueOf(sru.Effective_FH_FC_Ratio__c.setscale(2));
                           }
                           else{
                               Ratio = '0 (not set)';
                           }
                           //for Derate
                           if(sru.Effective_Derate__c != null){
                               Derate = String.valueOf(sru.Effective_Derate__c.setscale(2));
                           }
                           else{
                               Derate = '0 (not set)';
                           }
                           //for Interpolation
                           if(sru.Interpolation_Rate_Choice__c != null){
                               InterpolationChoice = String.valueOf(sru.Interpolation_Rate_Choice__c);
                           }
                           else{
                               InterpolationChoice = '0 (not set)';
                           }
                           //for Derate Choice
                           if(sru.Assembly_MR_Info__r.Derate_Interpolation_Choice__c != null){
                               DerateChoice = String.valueOf(sru.Assembly_MR_Info__r.Derate_Interpolation_Choice__c);
                           }
                           else{
                               DerateChoice = '0 (not set)';
                           }
                           }
                           
                           //for Escalated Rate
                           if(sru.Effective_Rate_Final__c != null){
                               Decimal EscalatedRateFormat = sru.Effective_Rate_Final__c;
                               EscalatedRate = (EscalatedRateFormat.format().contains('.')?EscalatedRateFormat.format():(EscalatedRateFormat.format()+'.00'));
                           }
                           else{
                               EscalatedRate = '0 (not set)';
                           }
                           //for Escalation Factor
                           if(sru.Effective_Escalation_Factor__c != null){
                               EscalationFactor = String.valueOf(sru.Effective_Escalation_Factor__c.setscale(2));
                           }
                           else{
                               EscalationFactor = '0 (not set)';
                           }
                           
                           //for Rounding Decimals,Method
                           if(sru.Assembly_MR_Info__r.Rounding_Method_on_Interpolated_MR_Rate__c != null && sru.Assembly_MR_Info__r.Rounding_Decimals_Interpolated_MR_Rate__c != null){
                               Rounding = String.valueOf(sru.Assembly_MR_Info__r.Rounding_Method_on_Interpolated_MR_Rate__c)+','+String.valueOf(sru.Assembly_MR_Info__r.Rounding_Decimals_Interpolated_MR_Rate__c);
                           }
                           else{
                               Rounding = '(not set)';
                           }
                           //for Units =>NetUnit EffectiveUnit
                           if(sru.Net_Units_F__c != null || sru.Effective_Unit__c != null){
                               NetUnits = String.valueOf(sru.Net_Units_F__c)+' '+String.valueOf(sru.Effective_Unit__c);
                           }
                           else{
                               NetUnits = '0 (not set)';
                           }
                           //for Monthly Threshold Amount
                           if(sru.Assembly_MR_Info__r.Monthly_Threshold_Amount__c != null){
                               Decimal MTAFormat = sru.Assembly_MR_Info__r.Monthly_Threshold_Amount__c;
                               MonthlyThresholdAmount = (MTAFormat.format().contains('.')?MTAFormat.format():(MTAFormat.format()+'.00'));
                           
                           }
                           else{
                               MonthlyThresholdAmount = '0 (not set)';
                           }
                           //for Rate upto Threshold
                           if(sru.Assembly_MR_Info__r.Flat_Rate_Upto_Threshold__c != null) {
                               Decimal RUTFormat = sru.Assembly_MR_Info__r.Flat_Rate_Upto_Threshold__c;
                               RateUptoThreshold = (RUTFormat.format().contains('.')?RUTFormat.format():(RUTFormat.format()+'.00'));
                           
                           }
                           else{
                               RateUptoThreshold = '0 (not set)';
                           }
                           //for Monthly Threshold Guaranteed
                           if(String.valueOf(sru.Assembly_MR_Info__r.Monthly_Threshold_Guaranteed__c) == 'True' ){
                               MonthlyThresholdGuaranteed = 'True';
                           }else{
                               MonthlyThresholdGuaranteed = 'False';
                           }
                           
                           //for Consider Larger Of FH & FC
							if(String.valueOf(sru.Assembly_MR_Info__r.Larger_Of_FH_FC__c) == 'True' ){
                               LargerOfFH_FC = 'True';
                           }
                           else{
                               LargerOfFH_FC = 'False';
                           }
                           
                           
                           //check for Supplemental rent type : flat or ratio
                           if((sru.Assembly_MR_Info__C != null && sru.Assembly_MR_Info__r.Ratio_Table__c != null) && sru.Effective_Unit__c != 'Monthly'){
                               System.debug('insUpdInvoiceLineItem : ABCD RatioTable '+sru.Assembly_MR_Info__r.Ratio_Table__c);
                               
                               curInvLI.Line_Item_Details__c = '<html><body>'+
                                   +' Rate: $'+Rate+'&nbsp;<br></br>'+
                                   +' Ratio Table: <a href="/" target="_blank">'+sru.Assembly_MR_Info__r.Ratio_Table__r.Name+'</a>&nbsp;<br></br>'+
                                   +' Rates Range: '+RateRangeL+'-'+RateRangeU+' &nbsp;<br></br>'+
                                   +' Ratio: '+Ratio+'&nbsp;<br></br>'+
                                   +' Derate: '+Derate+'&nbsp;<br></br>'+
                                   +' Interpolation: '+InterpolationChoice+'&nbsp;<br></br>'+
                                   +' Escalated Rate: $'+EscalatedRate+'&nbsp;<br></br>'+
                                   +' Escalation factor: '+EscalationFactor+'&nbsp;<br></br>'+
                                   +' Rounding: '+Rounding+'&nbsp;<br></br><br></br>'+
                                   +' Monthly Threshold Amount: $'+MonthlyThresholdAmount+'&nbsp;<br></br>'+
                                   +' Rate Upto Threshold: $'+RateUptoThreshold+'&nbsp;<br></br>'+
                                   +' Monthly Threshold Guaranteed: '+MonthlyThresholdGuaranteed+'&nbsp;<br></br>'+
                                   +' Consider larger of FH & FC: '+LargerOfFH_FC+'&nbsp;<br></br><br></br>'+
                                   +' Units: '+NetUnits+'&nbsp;<br></br>'+
                                   +'</body></html>'; 
                           }
                           else {
                               System.debug('insUpdInvoiceLineItem : else3 RatioTable'+sru.Assembly_MR_Info__r.Ratio_Table__c);
                               
                               curInvLI.Line_Item_Details__c = '<html><body>'+
                                   +' Rate: $'+Rate+'&nbsp;<br></br><br></br>'+
                                   +' Escalated Rate: $'+EscalatedRate+'&nbsp;<br></br>'+
                                   +' Escalation factor: '+EscalationFactor+'&nbsp;<br></br>'+
                                   +' Rounding: '+Rounding+'&nbsp;<br></br><br></br>'+
                                   +' Monthly Threshold Amount: $'+MonthlyThresholdAmount+'&nbsp;<br></br>'+
                                   +' Rate Upto Threshold: $'+RateUptoThreshold+'&nbsp;<br></br>'+
                                   +' Monthly Threshold Guaranteed: '+MonthlyThresholdGuaranteed+'&nbsp;<br></br>'+
                                   +' Consider larger of FH & FC: '+LargerOfFH_FC+'&nbsp;<br></br><br></br>'+
                                   +' Units: '+NetUnits+'&nbsp;<br></br>'+
                                   +' </body></html>';
                           }
                           
                       } 
        }
    }
    
    // after in,after upd,before del
    private void UpdateTotalOnInvoice()
    {
        list<Invoice_Line_Item__c> listNew = (list<Invoice_Line_Item__c>)(trigger.isDelete?trigger.old:trigger.New);
        set<id> setInvIds = new set<Id>();
        map<id, Invoice__c> mapInvs;
        list<Invoice__c> listInvsToUpd = new list<Invoice__c>();
        
        set<id> setLeaseIds = new set<id>();
        //		set<id> setOrigInvIds = new set<id>();
        set<string> setAsmblyMRInfosToUpd = new set<string>();		
        list<Assembly_MR_Rate__c> listAsmblyMRInfosToUpd = new list<Assembly_MR_Rate__c>();
        map<string, Assembly_MR_Rate__c> mapAssemblyMRInfo = new map<string, Assembly_MR_Rate__c>();
        //		map<string, decimal> mapOriginalInvLIs = new map<string, decimal>();
        
        for(Invoice_Line_Item__c curInvLI: listNew){
            setInvIds.add(curInvLI.Invoice__c);
        }
        mapInvs = new map<id, Invoice__c>([select id, Amount__c, recordType.developerName, Lease__c,status__c,
                                           Assumed_Estimated_Invoice__c, Rent__r.Rent_Type__c 
                                           from Invoice__c where id in :setInvIds]);
        
        for(Invoice__c curInv:mapInvs.values()){
            setLeaseIds.add(curInv.Lease__c);
            //          if(curInv.Assumed_Estimated_Invoice__c!=null)setOrigInvIds.add(curInv.Assumed_Estimated_Invoice__c);
        }
        list<Assembly_MR_Rate__c> listAsmblyMRInfo = [select id, Lease__c, Assembly_Lkp__c, Assembly_Event_Info__c
                                                      from Assembly_MR_Rate__c where Lease__c in :setLeaseIds];
        for(Assembly_MR_Rate__c curAsMRInf : listAsmblyMRInfo){
            //string strKey = curAsMRInf.Lease__c + '-' + curAsMRInf.Assembly_Lkp__c + '-' + curAsMRInf.Assembly_Event_Info__c;
            //system.debug('Putting Key ' + strKey);
            //mapAssemblyMRInfo.put(strKey, curAsMRInf);
            
            //June 13, 2019 (Invoice Module)
            mapAssemblyMRInfo.put(curAsMRInf.Id, curAsMRInf); 
        }
        
        
        Invoice__c curInv;
        setInvIds.clear();
        for(Invoice_Line_Item__c curInvLI: listNew){
            
            curInv = mapInvs.get(curInvLI.Invoice__c);
            
            Integer sign = 1;
            
            //check whether it is credit note or not.
            if(curInv.recordType.developerName == 'Manual_MR_Credit_Note'){
                sign = -1;
            }
            
            
            decimal dNewInvLIAmt = sign * leaseWareUtils.zeroIfNull(curInvLI.Total_Line_Amount__c);
            decimal dNewInvLIAmtLLP = sign * leaseWareUtils.zeroIfNull(curInvLI.Amount_Due_LLP__c);
            decimal dNetChange = 0, dNetChangeLLP = 0;
            decimal mrNetChanges = 0;
            
            if(trigger.isInsert){

                //Below code is not applicable for 'Non MR Invoices' since invoice header already has the amount set
                //Applicable only for MR invoice line items and PBH Rent
                String NonMRRecTypeID = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId(); //Non-MR
                if(curInvLI.RecordTypeId != NonMRRecTypeID || curInv.Rent__r.Rent_Type__c == 'Power By The Hour')
                {
                    dNetChange = dNewInvLIAmt;
                    // dNetChangeLLP = dNewInvLIAmtLLP;
                    curInv.Amount__c = (curInv.Amount__c==null?0:curInv.Amount__c) + dNetChange;
                    setInvIds.add(curInv.Id);
                    mapInvs.put(curInv.Id, curInv);
                }                
            }else if(trigger.isUpdate){
               
                mrNetChanges = dNewInvLIAmt;
            }else{//Delete
                dNetChange = -dNewInvLIAmt;
                // dNetChangeLLP = -dNewInvLIAmtLLP;
                curInv.Amount__c = (curInv.Amount__c==null?0:curInv.Amount__c) + dNetChange;
                setInvIds.add(curInv.Id);
                mapInvs.put(curInv.Id, curInv);
            }
            
            //anjani : If the Invoice is Provision Type, then do not update to MR Rate Info page. Provision is just for Information.
            if('Provision'.equals(curInvLI.Y_Hidden_UR_Type__c)) continue;
            
            string strKey;// = curInv.Lease__c + '-' + curInvLI.Assembly__c + '-' + curInvLI.Projected_Event__c;
            
            //June 13, 2019 (Invoice Module)
            if(curInvLI.Assembly_MR_Info__c != null){
                strKey = curInvLI.Assembly_MR_Info__c;
            }
            
            system.debug('Getting Key ' + strKey);
            
            Assembly_MR_Rate__c thisAsmblyMRInf = mapAssemblyMRInfo.get(strKey);
            
            decimal TotAdjAmt=0;
            
            /*
try{
strKey = curInv.Assumed_Estimated_Invoice__c + '-' + curInvLI.Assembly__c + '-' + curInvLI.Projected_Event__c;
TotAdjAmt = LeaseWareUtils.ZeroIfNull(mapOriginalInvLIs.get(strKey));
}catch(exception e){
TotAdjAmt = 0;
}
*/
            
            //added on Aug 21, 2019
            if(thisAsmblyMRInf != null){               
                System.debug('LeaseWareUtils.isFromTrigger(MR_INVOICE_UPDATE)'+LeaseWareUtils.isFromTrigger('MR_INVOICE_UPDATE'));
                System.debug('-Final-'+dNetChange+' - '+TotAdjAmt+' - '+mrNetChanges);
                thisAsmblyMRInf.Snapshot_Event_Record_ID__c = 'I' + curInvLI.Id;
                thisAsmblyMRInf.Date_Of_Snapshot_Event__c = curInvLI.Invoice_Date__c;
                mapAssemblyMRInfo.put(strKey, thisAsmblyMRInf);
                setAsmblyMRInfosToUpd.add(strKey);
            }
            
            //June 13, 2019 (Invoice Module)
            if(curInvLI.Assembly_MR_Info__c != null){
                continue;
            }
            
        }
        
        System.debug('Number of invoices to update=' + setInvIds.size());
        if(setInvIds.size()>0){
            for(Id curInvId : setInvIds){
                listInvsToUpd.add(mapInvs.get(curInvId));
            }
            try{
                update listInvsToUpd;
            }
            catch(DmlException ex){
                System.debug(ex);
                string errorMessage='';
                for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                    // Process exception here
                    errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                }
                system.debug('errorMessage=='+errorMessage);
                for(invoice__c inv : listInvsToUpd){
                    inv.addError(errorMessage);
                }
            }
        }
        if(setAsmblyMRInfosToUpd.size()>0){
            for(string curId : setAsmblyMRInfosToUpd){
                system.debug('Record to Upd ' + curId + ', ' + mapAssemblyMRInfo.get(curId));
                listAsmblyMRInfosToUpd.add(mapAssemblyMRInfo.get(curId));
            }
            
            // anjani : disable trigger before update, so that we wont get any validatior error after lease approved.
            //          LeaseWareUtils.TriggerDisabledFlag = true; Can't switch off. Snapshot creation depends on MR Assembly Trigger.
            LeaseWareUtils.setFromTrigger('SKIP_LEASE_LOCK_VALIDATION');// this will skip checking Lease approval check
            try{
                update listAsmblyMRInfosToUpd;
            }catch(DMLException e){
                String exMessage = e.getMessage();
                String customValidationString = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
                if(exMessage.contains(customValidationString)){
                    Integer index1 = exMessage.lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                    Integer index2 = exMessage.indexOf(': [');
                    exMessage= exMessage.substring(index1+customValidationString.length()+1,index2);
                    system.debug('exMessage---'+exMessage);
                    
                    system.debug('Filtered Error Message---'+ exMessage);
                }
                listNew[0].addError(exMessage);
                return;
            }
            
            LeaseWareUtils.unsetTriggers('SKIP_LEASE_LOCK_VALIDATION');
            //          LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger enabled for all users at apex level
        }
    }

    /**************************************************************************
     * This method validates the update made on the line item. Considerations:
     * 1.Line Comments is the only field that can always be edited irrespective of the invoice status
     * 2.Invoiced line amount cannot be edited manually
     **************************************************************************/
    private void validateLIUpdate()
    {
        Map<String, Schema.SObjectField> mapInvLIField = Schema.SObjectType.Invoice_Line_Item__c.fields.getMap();
        list<String> listFieldsToCheck = new list<String>();
        set<string> setSkipFields = RLUtility.getAllFieldsFromFieldset('Invoice_Line_Item__c','SkipFieldsFromValidations');

        list<Invoice_Line_Item__c> newLstInvLI =  (list<Invoice_Line_Item__c>)trigger.New ; 

        set<Id> setInvIds = new set<Id>();
        for(Invoice_Line_Item__c curInvLI: newLstInvLI){
            setInvIds.add(curInvLI.Invoice__c);
        }
        map<id, Invoice__c> mapInvs = new map<id, Invoice__c>([select id, Rent__r.Rent_Type__c 
                                           from Invoice__c where id in :setInvIds]);

        for(String Field:mapInvLIField.keyset())
        {
            Schema.DescribeFieldResult thisFieldDesc = mapInvLIField.get(Field).getDescribe();
            System.debug('Field Name is ' + thisFieldDesc.getName() + '. Local ' + thisFieldDesc.getLocalName());

            if(!thisFieldDesc.isUpdateable() || setSkipFields.contains(leasewareutils.getNamespacePrefix() + thisFieldDesc.getLocalName())) continue; //Add the fields that are always updatable to the list.
                listFieldsToCheck.add(thisFieldDesc.getLocalName());
        }

        Invoice_Line_Item__c oldInvLI ; 
        Invoice__c invoice;
        for(Invoice_Line_Item__c curInvLI:newLstInvLI)
        {
            oldInvLI = (Invoice_Line_Item__c)trigger.oldMap.get(curInvLI.id);
            invoice = mapInvs.get(oldInvLI.Invoice__c);
            if(oldInvLI.Invoice_Status__c==null || ''.equals(oldInvLI.Invoice_Status__c)) continue;            
        
            //Pending - can change only MR line amount, comments
            for(String curField:listFieldsToCheck)
            {
                if(curInvLI.get(curField) !=  oldInvLI.get(curField))
                {   
                    Schema.DescribeFieldResult thisFieldDesc = mapInvLIField.get(leasewareutils.getNamespacePrefix().toLowerCase() + curField).getDescribe();
                    System.debug(thisFieldDesc.getLabel() + ' has changed from' + oldInvLI.get(curField) + ' to ' + curInvLI.get(curField));

                    if('Pending'.equals(curInvLI.Invoice_Status__c))
                    {   
                        if(curField != 'Amount_Due__c')
                        {
                            // Value has changed. Only allowed change is change in amount.
                            curInvLI.addError(thisFieldDesc.getLabel() + ' cannot be edited.');                            
                        }
                        else                        
                        {
                            curInvLI.addError('Invoice Line Items cannot be manually modified. Please utilize ‘Invoice Adjustments’ functionality if you wish to update the line amount.');                            
                        }                        
                    }
                    else
                    {
                        if(curField == 'Amount_Due__c' && 'Approved'.equals(curInvLI.Invoice_Status__c)){
                            curInvLI.addError('Invoice Line Items cannot be manually modified. Please utilize ‘Invoice Adjustments’ functionality if you wish to update the line amount.');                            
                            break;
                        }
                        curInvLI.addError(thisFieldDesc.getLabel() + ' cannot be edited since the invoice is ' + curInvLI.Invoice_Status__c);                        
                    }
                    break;
                }
            }           

        }//End For - InvLI            
    }
    
    
}