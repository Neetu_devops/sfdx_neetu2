public class OperationalTransactionRegisterHandler implements Database.Batchable<sObject>,Database.Stateful {
    
    private Process_Request_Log__c reqLog;
    integer iteration2 = 1; // using this to restrict the iteration for insert
    private transient static integer batchSize ; // add custom setting , two separate
    integer totalRecords = 0;
    integer failedRecordCount = 0;
    Map<Id,FailedRecord> mapFailedRecords = new Map<Id,FailedRecord>();
    Boolean isDelete  = false;
    string exceptionmsg = '' ;
    
    public OperationalTransactionRegisterHandler(Boolean isDelete,integer iteration,Process_Request_Log__c reqLogNew) {
        this.isDelete = isDelete;
        this.iteration2 = iteration;
        this.reqLog = reqLogNew;
    }
    
    /* This method is being used from Lightning component UpdateTransactionRegister to initiate the batch process*/
    @AuraEnabled
    public static void initBatch(){     
        try {
            String prefix = LeaseWareUtils.getNamespacePrefix();
            prefix = prefix.removeEnd('__');
            System.debug('prefix:' + prefix);
            ApexClass aC = [SELECT Id,Name FROM ApexClass WHERE Name = 'OperationalTransactionRegisterHandler' AND NamespacePrefix =:prefix];
            System.debug('ApexClass --' + aC);
            // check if there isn't a batch job in status Processing, Queued, Holding or Preparing for this Apex class,only then initiate this batch
            // This is done to avoid concurrent batch runs on refresh of page. 

            if([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND  ApexClassID =:aC.Id AND (Status = 'Processing' OR Status = 'Preparing' OR  Status = 'Queued' OR  Status = 'Holding')] < 1){
                Process_Request_Log__c reqLogNew = new Process_Request_Log__c(Status__c ='PROCESSING', Type__c = 'Operational Transaction Register', Comments__c = '', Exception_Message__c = ''); 
                insert reqLogNew;         
                System.debug('starting batch: -----');
                Database.executeBatch(new OperationalTransactionRegisterHandler(true,1,reqLogNew),getDeleteBatchSize()); // runs with batch size 1 , and first iteration for delete
            }
           } catch (Exception e) {
            System.debug('ERROR: ' + e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }


    public Database.QueryLocator start(Database.BatchableContext BC)                // Start Method
    {
        System.debug('OperationalTransactionRegisterHandler.Batch.Start(+)');
        System.debug('Start ---' + isDelete +'---iteration2 ---'+ this.iteration2);
        String strQuery = getQuery(isDelete);
        
        System.Debug('StrQuery=='+strQuery);
        Database.QueryLocator queryLocator ;
        try{
            queryLocator = Database.getQueryLocator(strQuery);
        }catch(Exception e){
            exceptionmsg += e.getMessage();
            System.debug('Exception occured while getting the Operational Transaction Register records  = '+  e.getMessage());
            LeaseWareUtils.createExceptionLog(e, 'Exception while getting the Operational Transaction Register records', null, null,'Operational Transaction Register',true);  
            updateRequestLog(reqLog,'FAIL', LeaseWareUtils.cutString( e.getMessage() , 0, 254),
            'Failed to create records :' + e.getMessage() + e.getStackTraceString());
        }  
        System.debug('OperationalTransactionRegisterHandler.Batch.Start(-)');
        return queryLocator;
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope)          // Execute Logic
    {
        System.debug('OperationalTransactionRegisterHandler.batch.Execute(+)');
        System.debug('Execute ---' + isDelete);
        System.debug('reqLog---' + reqLog);
        if(isDelete && scope.size()>0){  // delete all old records
            database.DeleteResult[] res = new List<database.DeleteResult>();
            System.debug('Execute scope size in delete:' + scope.size());
            res = database.delete((list<Operational_Transaction_Register__c>) scope,false);
            for(Integer i=0; i < res.size(); i++) {
                if(res.get(i).isSuccess()) {
                    System.debug(' record deleted... : ' + scope[i] );
                }else{
                    Database.Error errors =  res.get(i).getErrors().get(0);     
                    System.debug(' record delete failed ... : ' + scope[i]);
                    string errormsg = 'ERROR: Operational Transaction Register Update failed for ' + res.get(i) +' .\n';
                    System.debug(errormsg);
                    for (Database.Error err:res.get(i).getErrors()) {
                        errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                        errormsg += ('Fields that affected this error: ' + err.getFields() + '\n');
                    }
                    exceptionmsg += errormsg;
                    if(reqLog !=null) {updateRequestLog(reqLog,'FAIL', LeaseWareUtils.cutString( exceptionmsg , 0, 254),
                        'Failed to create records :' + exceptionmsg);}
                    LeasewareUtils.createExceptionLog(null, errormsg,'Operational_TransactionRregister__c', null, 'parallel reporting deletion', false);
                }
            }
        }else{ // Insert new records
            System.debug('Execute scopr size in insert:' + scope.size());
            mapFailedRecords = createParallelReportingRecords((List<Lease__c>) scope);
            System.debug('mapFailedRecods --' + mapFailedRecords);
        }
        LeaseWareUtils.insertExceptionLogs();
        
        System.debug('OperationalTransactionRegisterHandler.batch.Execute(-)');
    }
    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('OperationalTransactionRegisterHandler.batch.Finish(+)');
        
        if(iteration2 == 1 && !Test.isRunningTest()){
                System.debug('calling next batch ---' + reqLog.status__c);
                OperationalTransactionRegisterHandler otrBatch = new OperationalTransactionRegisterHandler(false,(iteration2+1),reqLog);
                ID batchprocessid = Database.executeBatch(otrBatch,getInsertBatchSize()); // this will call the insert so keeping batch size as 1 , for each lease  
          }
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id =:BC.getJobId()];  
        
        System.debug('Finish ---Exceptionmsg ---' + exceptionmsg);

        if(!isDelete || String.isNotBlank(exceptionmsg)){
            String emailSubject = '[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] -';
            string strMsg = 'Parallel Reporting Job with ID ' + BC.getJobId() + ' completed with status ' + a.Status
                + '.\n' + a.TotalJobItems + ' batches with '+ a.JobItemsProcessed + ' processed and ' + a.NumberOfErrors + ' failures. <br/>';
            System.debug('From finish :' + mapFailedRecords.size());
            if(mapFailedRecords.values().size()>0){
                emailSubject += 'Operational Transaction Register creation failed \n';
                strMsg += createEmailBody( mapFailedRecords.values(),strMsg);
            }else if(String.isNotBlank(exceptionmsg)){
                emailSubject += 'Operational Transaction Register creation failed for some records with error : \n';
                strMsg += exceptionmsg;
            }
            else{
                emailSubject += ' Operational Transaction Register records created successfully \n';
                strMsg +=   '<b>Total records processed : </b>'+ totalRecords + '<br/>'+
                    '<b>Total records suceeded : </b>'+ (totalRecords - failedRecordCount) ;
            }
            try{
                LeaseWareUtils.sendEmail(emailSubject , strMsg, UserInfo.getUserEmail());
            }catch(EmailException e){
                LeaseWareUtils.sendEmail('Error sending email for Operational Transaction Register Records' ,'Unexpected Exception', UserInfo.getUserEmail());
            }
        }
        if(mapFailedRecords.isEmpty()){
            updateRequestLog(reqLog,'SUCCESS','','');
        }else{
            updateRequestLog(reqLog,'FAIL','Exception sent in email','');
        }
        System.debug('OperationalTransactionRegisterHandler.batch.Finish(-)');
    }
    
    /*Method to create Operational_Transaction_Record__c for all existing documents.
This method is used when batch is initiated from Lightning component button */
    public Map<Id,FailedRecord> createParallelReportingRecords(List<Lease__c> listLease){
        System.debug('OperationalTransactionRegisterHandler.createParallelReportingRecords(+)');
        Map<Id,Operational_Transaction_Register__c> mapToInsert = new Map<Id,Operational_Transaction_Register__c>();
        List<Operational_Transaction_Register__c> listWithErrors = new List<Operational_Transaction_Register__c>();
        Map<Id,Invoice__c> mapInvoices = new  Map<Id,Invoice__c>();
        Map<Id,Payment__c> mapPayments = new  Map<Id,Payment__c>();
        Map<Id,Payment_Receipt__c> mapPrePayments = new  Map<Id,Payment_Receipt__c>();
        Map<Id,Payable__c> mapPayable = new  Map<Id,Payable__c>();
        Map<Id,Payout__c> mapPayout = new  Map<Id,Payout__c>();
        Map<Id,Credit_Memo__c> mapCreditMemos = new  Map<Id,Credit_Memo__c>();
        Map<String,String> mapFailedReason = new  Map<String,String>();
        System.debug('Checking for lease :' + listLease);
        // create the map for each document, in this method querying for each child document of lease.  
        createChildRecordsMaps(listLease,mapInvoices,mapPayments, mapPrePayments,mapPayable,mapPayout,mapCreditMemos);
        
        // create the operational transaction records for each of these documents and get the map to insert
        mapToInsert = manageOperationTransactionRegisterRecord(mapInvoices,mapPayments, mapPrePayments,mapPayable,mapPayout,mapCreditMemos); // method to create the records for each documents
           
        //-----------------------------------------INSERT DATA-----------------------------------------
            database.SaveResult[] res = new List<database.SaveResult>();
            if(mapToInsert.values().size()>0){
                System.debug('mapToInsert ::' + mapToInsert.size());
                System.debug('mapToInsert ::' + mapToInsert);
                System.debug('Limits : DML ' + Limits.getDmlStatements());
                System.debug('Limits : Heap ' + Limits.getHeapSize());
                System.debug('Limits : Queries ' + Limits.getQueries());
                totalRecords += mapToInsert.values().size();
                res = database.insert(mapToInsert.values(),false);
                System.debug('Limits After : DML ' + Limits.getDmlStatements());
                System.debug('Limits After : Heap ' + Limits.getHeapSize());
                System.debug('Limits After : Queries ' + Limits.getQueries());
            }
            for(Integer i=0; i < res.size(); i++) {
                if(res.get(i).isSuccess()) {
                    System.debug(' record updated... : ' + mapToInsert.values()[i] );
                }else{
                    Database.Error errors =  res.get(i).getErrors().get(0);
                    listWithErrors.add(mapToInsert.values()[i]);
                    failedRecordCount =  failedRecordCount + listWithErrors.size();
                    mapFailedReason.put(mapToInsert.values()[i].Document_Type__c,errors.getMessage());
                    System.debug(' record insert failed ... : ' + mapToInsert.values()[i]);
                    string errormsg = 'ERROR: Parallel Reporting records creation failed for ' + res.get(i) +' .\n';
                    System.debug(errormsg);
                    for (Database.Error err:res.get(i).getErrors()) {
                        errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                        errormsg += ('Fields that affected this error: ' + err.getFields() + '\n');
                    }
                    if(ReqLog !=null) {ReqLog.Exception_Message__c += errormsg;}
                }
            }
           
            if(ReqLog !=null && ReqLog.Exception_Message__c!=null) {
                ReqLog.Exception_Message__c = ReqLog.Exception_Message__c.left(2000);
                update ReqLog;
            }
            // add the failed records to mapFailedRecods to get the emailbody
            for(Operational_Transaction_Register__c curRec:listWithErrors) {
                System.debug('FAILED:' + curRec.Document_Type__c);
                System.debug('mapFailedReason:' + mapFailedReason);
                Date recordDate;
                String recordStatus = '';
                String failedReason = '';
                String name = '';
                String srName = '';
                FailedRecord failedRec =  new FailedRecord();
                
                if(curRec.Document_Type__c.startsWith('I')){
                    Invoice__c record = mapInvoices.get(curRec.Invoice__c);
                    failedRec.DocumentName = record.Id;
                    failedRec.leaseId = record.lease__c;
                    failedRec.leaseName =  record.lease__r.Name;
                    name = record.Name;
                    recordDate = record.Invoice_Date__c;
                    failedReason = mapFailedReason.get('I'+'-'+String.valueOf(record.Id));
                }
                if(curRec.Document_Type__c.startsWith('P')){
                    Payment__c record = mapPayments.get(curRec.Payments_Credits__c);
                    failedRec.DocumentName = record.Id;
                    failedRec.leaseId = record.Invoice__r.Lease__c;
                    failedRec.leaseName =  record.Invoice__r.Lease__r.Name;
                    name = record.Name;
                    recordDate = record.Payment_Date__c;
                    failedReason = mapFailedReason.get('P'+'-'+String.valueOf(record.Id));
                }
                if(curRec.Document_Type__c.startsWith('PP')){
                    Payment_Receipt__c record = mapPrePayments.get(curRec.Prepayment__c);
                    failedRec.DocumentName = record.Id;
                    failedRec.leaseId = record.Lease__c;
                    failedRec.leaseName = record.Lease__r.Name;
                    name = record.Name;
                    recordDate = record.Payment_Date__c;
                    failedReason = mapFailedReason.get('PP'+'-'+String.valueOf(record.Id));
                }
                if(curRec.Document_Type__c.startsWith('PY')){
                    Payable__c record = mapPayable.get(curRec.Payables__c);
                    failedRec.DocumentName = record.Id;
                    failedRec.leaseId = record.Lease__c;
                    failedRec.leaseName = record.Lease__r.Name;
                    name = record.Name;
                    recordDate = record.Payable_Date__c;
                    failedReason = mapFailedReason.get('PY'+'-'+String.valueOf(record.Id));
                }
                if(curRec.Document_Type__c.startsWith('PO')){
                    Payout__c record = mapPayout.get(curRec.Payouts__c);
                    failedRec.DocumentName = record.Id;
                    failedRec.leaseId = record.Lease__c;
                    failedRec.leaseName = record.Lease__r.Name;
                    name = record.Name;
                    recordDate = record.Payout_Date__c;
                    failedReason = mapFailedReason.get('PO'+'-'+String.valueOf(record.Id));
                }
                if(curRec.Document_Type__c.startsWith('CM')){
                    credit_memo__c record = mapCreditMemos.get(curRec.Credit_Memo__c);
                    failedRec.DocumentName = record.Id;
                    failedRec.leaseId = record.Lease__c;
                    failedRec.leaseName = record.Lease__r.Name;
                    name = record.Name;
                    recordDate = record.Credit_Memo_Date__c;
                    failedReason = mapFailedReason.get('CM'+'-'+String.valueOf(record.Id));
                }
                failedRec.DocumentDate = String.valueOf(recordDate);
                failedRec.Name = name;
                failedRec.FailedReason = failedReason;
                mapFailedRecords.put(failedRec.DocumentName,failedRec);
            }           		
        System.debug('OperationalTransactionRegisterHandler.createParallelReportingRecords(-)');
        return mapFailedRecords;
    }
    
    private String getQuery(Boolean isDelete){
        String query = '';
        String prefix = LeaseWareUtils.getNamespacePrefix();
        if(isDelete){
             query += 'select id , name from '+ prefix +'Operational_transaction_register__c';
        }else{
            // cant use subqueries in start with Database.Querylocator: if size of child records is more than 200 it gives a Invalid Query Locator error
            query += 'select id,name from '+ prefix +'Lease__c';           
        }	
        return query;
    }
    
    /*************************************************************************************************************************
// Utility Method for creating a Operational_Transaction_Register__c record 
// This is created on insert of Invoice, Payment, Payables,Payout, Credit Memo , Prepayment(Paymnet_Studio__c)
// Used in Async util to create records from in Invoice Handler, PaymentTriggerHandler, PayableTriggerHandler,PayoutTriggerHandler
//PaymentStudioTRiggerHandler, CreditMemoTriggerHandler
****************************************************************************************************************************/
    public static void manageOperationTransactionRegisterRecord(Set<Id> setObjIds){
        System.debug('manageOperationTransactionRegisterRecord(+)');
        List<Operational_Transaction_Register__c> mapToInsert = new List<Operational_Transaction_Register__c>();
        
        List<Id> listIds = new List<Id>(setObjIds);     
        // Adding record if type is Invoice
        if(listIds[0].getSObjectType().getDescribe().getName().toLowercase().equals(String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'Invoice__c').toLowercase())){
            for(Invoice__c inv: [Select id, Name, Lease__c, Lease__r.Lessee__c ,Lease__r.Operator__c, Lease__r.Lessor__c from Invoice__c where id in:setObjIds]){   
                Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
                oprtnltrnsc.name = inv.Name;
                oprtnltrnsc.Lease__c = inv.Lease__c;
                oprtnltrnsc.Invoice__c = inv.Id;
                oprtnltrnsc.Lessee__c = ((inv.Lease__r.Lessee__c != null) ? inv.Lease__r.Lessee__c : inv.Lease__r.Operator__c);
                oprtnltrnsc.Lessor__c = inv.Lease__r.Lessor__c;
                oprtnltrnsc.Document_Type__c = 'I'+'-'+String.valueOf(inv.id); // need this just as internal marker of the OTR record.For creating the error logs in email
                mapToInsert.add(oprtnltrnsc);
            }
            // Adding record if type is Payment & Credit 
        }else if(listIds[0].getSObjectType().getDescribe().getName().toLowercase().equals(String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'Payment__c').toLowercase())){
            for(Payment__c paymnt: [Select id, Name, Lease__c, Invoice__c,Invoice__r.Lease__r.Lessee__c , Invoice__r.Lease__r.Lessor__c,Invoice__r.Lease__r.Operator__c from Payment__c where id in:setObjIds]){   
                Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
                oprtnltrnsc.name = paymnt.Name;
                oprtnltrnsc.Lease__c = paymnt.Lease__c;
                oprtnltrnsc.Payments_Credits__c = paymnt.Id;
                oprtnltrnsc.Lessee__c = ((paymnt.Invoice__r.Lease__r.Lessee__c != null) ? paymnt.Invoice__r.Lease__r.Lessee__c : paymnt.Invoice__r.Lease__r.Operator__c);
                oprtnltrnsc.Lessor__c = paymnt.Invoice__r.Lease__r.Lessor__c;
                oprtnltrnsc.Document_Type__c = 'P'+'-'+String.valueOf(paymnt.id);
                mapToInsert.add(oprtnltrnsc);
            }
            // Adding record if type is prepayment
        }else if(listIds[0].getSObjectType().getDescribe().getName().toLowercase().equals(String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'Payment_Receipt__c').toLowercase())){
            for(Payment_Receipt__c prepaymnt: [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Lessor__c,Lease__r.Operator__c from Payment_Receipt__c where id in:setObjIds]){   
                Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
                oprtnltrnsc.name = prepaymnt.Name;
                oprtnltrnsc.Lease__c = prepaymnt.Lease__c;
                oprtnltrnsc.Prepayment__c = prepaymnt.Id;
                oprtnltrnsc.Lessee__c = ((prepaymnt.Lease__r.Lessee__c != null) ? prepaymnt.Lease__r.Lessee__c : prepaymnt.Lease__r.Operator__c);
                oprtnltrnsc.Lessor__c = prepaymnt.Lease__r.Lessor__c;
                oprtnltrnsc.Document_Type__c = 'PP'+'-'+String.valueOf(prepaymnt.id);
                mapToInsert.add(oprtnltrnsc);
            }
            // Adding record if type is payables
        }else if(listIds[0].getSObjectType().getDescribe().getName().toLowercase().equals(String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'Payable__c').toLowercase())){
            for(Payable__c payable: [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Lessor__c,Lease__r.Operator__c from Payable__c where id in:setObjIds]){   
                Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
                oprtnltrnsc.name = payable.Name;
                oprtnltrnsc.Lease__c = payable.Lease__c;
                oprtnltrnsc.Payables__c = payable.Id;
                oprtnltrnsc.Lessee__c = ((payable.Lease__r.Lessee__c != null) ?payable.Lease__r.Lessee__c : payable.Lease__r.Operator__c);
                oprtnltrnsc.Lessor__c = payable.Lease__r.Lessor__c;
                oprtnltrnsc.Document_Type__c = 'PY'+'-'+String.valueOf(payable.id);
                mapToInsert.add(oprtnltrnsc);
            }
            // Adding record if type is payout
        }else if(listIds[0].getSObjectType().getDescribe().getName().toLowercase().equals(String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'Payout__c').toLowercase())){
            for(Payout__c payout: [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Lessor__c, Lease__r.Operator__c from Payout__c where id in:setObjIds]){   
                Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
                oprtnltrnsc.name = payout.Name;
                oprtnltrnsc.Lease__c = payout.Lease__c;
                oprtnltrnsc.Payouts__c = payout.Id;
                oprtnltrnsc.Lessee__c = ((payout.Lease__r.Lessee__c != null) ?payout.Lease__r.Lessee__c : payout.Lease__r.Operator__c);
                oprtnltrnsc.Lessor__c = payout.Lease__r.Lessor__c;
                oprtnltrnsc.Document_Type__c = 'PO'+'-'+String.valueOf(payout.id);
                mapToInsert.add(oprtnltrnsc);
            }
            // Adding record if type is Credit memo
        }else if(listIds[0].getSObjectType().getDescribe().getName().toLowercase().equals(String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'Credit_Memo__c').toLowercase())){
            for(Credit_Memo__c creditMemo: [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Lessor__c,Lease__r.Operator__c from Credit_Memo__c where id in:setObjIds]){   
                Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
                oprtnltrnsc.name = creditMemo.Name;
                oprtnltrnsc.Lease__c = creditMemo.Lease__c;
                oprtnltrnsc.Credit_Memo__c = creditMemo.Id;
                oprtnltrnsc.Lessee__c = ((creditMemo.Lease__r.Lessee__c != null) ?creditMemo.Lease__r.Lessee__c : creditMemo.Lease__r.Operator__c);
                oprtnltrnsc.Lessor__c = creditMemo.Lease__r.Lessor__c;
                oprtnltrnsc.Document_Type__c = 'CM'+'-'+String.valueOf(creditMemo.id);
                mapToInsert.add(oprtnltrnsc);
            }
        }
        if(mapToInsert.size()>0){
            try{
                System.debug('mapToInsert:' + mapToInsert); 
                insert mapToInsert;
            }    
            catch(Exception e){
                System.debug('Exception e: ' + e.getMessage());
                LeasewareUtils.createExceptionLog(e,e.getMessage(),'Operational_Transaction_Register__c','', 'Insert Operational_Transaction_Register__c',true);
            }  
        }
        System.debug('manageOperationTransactionRegisterRecord(-)');
    }
    // 	Method to create records from Lighning component button
    public map<Id,Operational_transaction_register__c> manageOperationTransactionRegisterRecord(Map<Id,Invoice__c> mapInvoices,
        Map<Id,Payment__c> mapPayments, Map<Id,Payment_Receipt__c> mapPrePayments, Map<Id,Payable__c> mapPayable,
        Map<Id,Payout__c> mapPayout ,Map<Id,Credit_memo__c> mapCreditMemos){

        System.debug('OperationalTransactionRegisterHandler.manageOperationTransactionRegisterRecord(+)');
        Map<Id,Operational_Transaction_Register__c> mapToInsert = new map<Id,Operational_Transaction_Register__c>();
        
        System.debug('----creating record for Invoices-----------');
        for(Invoice__c inv: mapInvoices.values()){  
            Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
            oprtnltrnsc.name = inv.Name;
            oprtnltrnsc.Lease__c = inv.Lease__c;
            oprtnltrnsc.Invoice__c = inv.Id;
            oprtnltrnsc.Lessee__c = ((inv.Lease__r.Lessee__c != null) ? inv.Lease__r.Lessee__c : inv.Lease__r.Operator__c);
            oprtnltrnsc.Lessor__c = inv.Lease__r.Lessor__c;
            oprtnltrnsc.Document_Type__c = 'I'+'-'+String.valueOf(inv.id);
            mapToInsert.put(oprtnltrnsc.Invoice__c,oprtnltrnsc);
        }
        System.debug('mapToInsert after Invoices::' + mapToInsert.size());
        
        System.debug('----creating record for Payments-----------');
        for(Payment__c paymnt: mapPayments.values()){   
            Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
            oprtnltrnsc.name = paymnt.Name;
            oprtnltrnsc.Lease__c = paymnt.Lease__c;
            oprtnltrnsc.Payments_Credits__c = paymnt.Id;
            oprtnltrnsc.Lessee__c = ((paymnt.Invoice__r.Lease__r.Lessee__c != null) ? paymnt.Invoice__r.Lease__r.Lessee__c : paymnt.Invoice__r.Lease__r.Operator__c);
            oprtnltrnsc.Lessor__c = paymnt.Invoice__r.Lease__r.Lessor__c;
            oprtnltrnsc.Document_Type__c = 'P'+'-'+String.valueOf(paymnt.id);
            mapToInsert.put(oprtnltrnsc.Payments_Credits__c,oprtnltrnsc);
                                }
        System.debug('mapToInsert after Payments::' + mapToInsert.size());
        System.debug('----creating record for Prepayments-----------');
        for(Payment_Receipt__c prepaymnt: mapPrePayments.values()){  
            Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
            oprtnltrnsc.name = prepaymnt.Name;
            oprtnltrnsc.Lease__c = prepaymnt.Lease__c;
            oprtnltrnsc.Prepayment__c = prepaymnt.Id;
            oprtnltrnsc.Lessee__c = ((prepaymnt.Lease__r.Lessee__c != null) ? prepaymnt.Lease__r.Lessee__c : prepaymnt.Lease__r.Operator__c);
            oprtnltrnsc.Lessor__c = prepaymnt.Lease__r.Lessor__c;
            oprtnltrnsc.Document_Type__c = 'PP'+'-'+String.valueOf(prepaymnt.id);
            mapToInsert.put(oprtnltrnsc.Prepayment__c,oprtnltrnsc);
        } 
        System.debug('mapToInsert after Prepayment::' + mapToInsert.size());
        System.debug('----creating record for Payables-----------');
        for(Payable__c payable: mapPayable.values()){   
            Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
            oprtnltrnsc.name = payable.Name;
            oprtnltrnsc.Lease__c = payable.Lease__c;
            oprtnltrnsc.Payables__c = payable.Id;
            oprtnltrnsc.Lessee__c = ((payable.Lease__r.Lessee__c != null) ?payable.Lease__r.Lessee__c : payable.Lease__r.Operator__c);
            oprtnltrnsc.Lessor__c = payable.Lease__r.Lessor__c;
            oprtnltrnsc.Document_Type__c = 'PY'+'-'+String.valueOf(payable.id);
            mapToInsert.put(oprtnltrnsc.Payables__c ,oprtnltrnsc);
        }
        System.debug('mapToInsert after Payables::' + mapToInsert.size());
        System.debug('----creating record for Payouts-----------');
        for(Payout__c payout: mapPayout.values()){   
            Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
            oprtnltrnsc.name = payout.Name;
            oprtnltrnsc.Lease__c = payout.Lease__c;
            oprtnltrnsc.Payouts__c = payout.Id;
            oprtnltrnsc.Lessee__c = ((payout.Lease__r.Lessee__c != null) ?payout.Lease__r.Lessee__c : payout.Lease__r.Operator__c);
            oprtnltrnsc.Lessor__c = payout.Lease__r.Lessor__c;
            oprtnltrnsc.Document_Type__c = 'PO'+'-'+String.valueOf(payout.id);
            mapToInsert.put( oprtnltrnsc.Payouts__c,oprtnltrnsc);
        }
        System.debug('mapToInsert after Payouts::' + mapToInsert.size());
        System.debug('----creating record for Payouts-----------');
        for(Credit_Memo__c creditMemo: mapCreditMemos.values()){   
            Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
            oprtnltrnsc.name = creditMemo.Name;
            oprtnltrnsc.Lease__c = creditMemo.Lease__c;
            oprtnltrnsc.Credit_Memo__c = creditMemo.Id;
            oprtnltrnsc.Lessee__c = ((creditMemo.Lease__r.Lessee__c != null) ?creditMemo.Lease__r.Lessee__c : creditMemo.Lease__r.Operator__c);
            oprtnltrnsc.Lessor__c = creditMemo.Lease__r.Lessor__c;
            oprtnltrnsc.Document_Type__c = 'CM'+'-'+String.valueOf(creditMemo.id);
            mapToInsert.put(oprtnltrnsc.Credit_Memo__c,oprtnltrnsc);
        }
        
        System.debug('mapToInsert after Event Invoice ::' + mapToInsert.size());
        System.debug('OperationalTransactionRegisterHandler.manageOperationTransactionRegisterRecord(-)');
        return mapToInsert; 
    }
    
    @TestVisible
    private string createEmailBody(List<FailedRecord> listFailedRecords,String strMsg ){
        String emailBody = strMsg;
        String url = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('createEmailBody : totalRecords:' + totalRecords);
        System.debug('createEmailBody : listFailedRecords:' + listFailedRecords);
        emailBody+=  '<b>Total records processed : </b>'+ totalRecords + '<br/>'+
            '<b>Total records suceeded : </b>'+ Math.abs((totalRecords - listFailedRecords.size())) + '<br/>';
        if(listFailedRecords.size()>0){
            emailBody+= '<br/> <table border="1" style="border-collapse: collapse"><caption><b>Operational Transaction record creation failed for records with following data :</b> </caption>'+
                '<tr><th><b>Date</b></th><th><b>Lease</b></th><th><b>Document Name</b></th>'+
                '<th><b>Failed Reason</b></th></tr>';
            for (FailedRecord str: listFailedRecords) {
                //1
                emailBody += '<tr><td>' + str.DocumentDate + '</td>';
                //2
                if(str.leaseId == null) {
                    emailBody += ('<td><div style="background-color:yellow;text-align:centre;">'+str.leaseName + '<a><u></u></a></div></td>');
                }else{
                    emailBody += ('<td><a href="'  + url + '/' + str.leaseId + '">' +str.leaseName + '</a></td>');
                }	
                //3
                if(str.DocumentName == null) {
                    emailBody += ('<td>'+''+'</td>');
                }else {
                    emailBody += ('<td><a href="'  + url + '/' + str.DocumentName + '">' +str.Name + '</a></td>');
                }
                //6
                if(String.isNotBlank(str.FailedReason)) {
                    emailBody += ('<td>'+str.FailedReason + '</td>');
                }
            }
            emailBody += '</table>';
        }else{
            emailBody += '<b>Exception Occured: </b>'+ strMsg + '<br/>';
        }
        return emailBody;
    }
    
    private void createChildRecordsMaps(List<Lease__c> listLease,Map<Id,Invoice__c> mapInvoices,
        Map<Id,Payment__c> mapPayments, Map<Id,Payment_Receipt__c> mapPrePayments, Map<Id,Payable__c> mapPayable,
        Map<Id,Payout__c> mapPayout ,Map<Id,Credit_memo__c> mapCreditMemos){
            
            List<Invoice__c> listInvoices = [Select id, Name, Lease__c,Lease__r.Name,Invoice_Date__c , Lease__r.Lessee__c ,Lease__r.Operator__c, Lease__r.Lessor__c  from Invoice__c where lease__c in :listLease];
            for(Invoice__c curInv :listInvoices) {
                mapInvoices.put(curInv.Id,curInv);
            }
            for(Payment__c curPaymnt : [Select id, Name, Lease__c, Invoice__c,Invoice__r.Lease__r.Lessee__c ,Invoice__r.Lease__r.Name,
                                        Invoice__r.Lease__r.Lessor__c,Invoice__r.Lease__r.Operator__c from Payment__c
                                        where Invoice__c in:listInvoices]) {
            mapPayments.put(curPaymnt.Id,curPaymnt);
            }
            
            for(Payment_Receipt__c prePaymnt : [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Name,Lease__r.Lessor__c,Lease__r.Operator__c,Prepayment__c from Payment_Receipt__c where Prepayment__c = true AND lease__c in :listLease ]) {
                mapPrePayments.put(prePaymnt.Id,prePaymnt); 
            }
            
            for(Payable__c payable : [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Lessor__c,Lease__r.Name,Lease__r.Operator__c  from Payable__c where lease__c in :listLease]) {
                mapPayable.put(payable.id,payable);
            }
            
            for(Payout__c payout : [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Lessor__c, Lease__r.Name,Lease__r.Operator__c  from Payout__c where lease__c in :listLease]) {
                mapPayout.put(payout.Id,payout);
            }
            
            for(credit_memo__c creditmemo : [Select id, Name, Lease__c,Lease__r.Lessee__c , Lease__r.Lessor__c,Lease__r.Name,Lease__r.Operator__c from Credit_Memo__c where lease__c in :listLease]) {
                mapCreditMemos.put(creditmemo.Id,creditmemo);
            }

        }
    
    /* Future method exposed, to be called from any other trigger or utility class as required*/   
    @future     
    public static void futureManageOperationTransactionRegisterRecord(Set<Id> setObjIds){
        manageOperationTransactionRegisterRecord(setObjIds);
    }
    
    // Batch size for deletion batch
    public static Integer getDeleteBatchSize(){
        ServiceSetting__c lwServiceSettings = ServiceSetting__c.getInstance('OperationalTnsctionRegisterDeleteBatch');
        if(lwServiceSettings != null && lwServiceSettings.Batch_Size__c != null) {
            batchSize = integer.valueOf(lwServiceSettings.Batch_Size__c);
        }
        if(batchSize == null){batchSize = 2000;}  
        System.debug('batchSize =' + batchSize) ;
        return batchSize;
    }
    
    // Batch size for insertion batch
    public static Integer getInsertBatchSize(){
        ServiceSetting__c lwServiceSettings = ServiceSetting__c.getInstance('OperationalTnsctionRegisterInsertBatch');
        if(lwServiceSettings != null && lwServiceSettings.Batch_Size__c != null) {
            batchSize = integer.valueOf(lwServiceSettings.Batch_Size__c);
        }
        if(batchSize == null){batchSize = 1;}  
        System.debug('batchSize =' + batchSize) ;
        return batchSize;
    }

    public static void updateRequestLog(Process_Request_Log__c ReqLog,String status,String comments,String excepStr)
    {   
        System.debug('ReqLog--' + ReqLog);
        ReqLog.status__c = status;
        ReqLog.Comments__c = comments;
        ReqLog.Exception_Message__c = excepStr.left(2000);      
        update ReqLog;
    }
    
    @TestVisible
    public class FailedRecord {
        @TestVisible public FailedRecord(){
        }
        @TestVisible public Id DocumentName  {get; set;}
        @TestVisible public String Name {get; set;}
        @TestVisible public String LeaseId {get; set;}
        @TestVisible public String LeaseName {get; set;}
        @TestVisible public String DocumentDate {get; set;}
        @TestVisible public String FailedReason {get; set;}
    }
    
}