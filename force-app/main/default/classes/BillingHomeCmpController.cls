public with sharing class BillingHomeCmpController {
    @AuraEnabled
    public static BillingWrapper getCommunityPrefix(){
        return new BillingWrapper(UtilityWithoutSharingController.getCommunityPrefix(), LesseeHomeController.getData());
    }
    public class BillingWrapper{
        @AuraEnabled public String communityPrefix;
        @AuraEnabled public LesseeHomeController.DataWrapper data;
        public BillingWrapper(String communityPrefix, LesseeHomeController.DataWrapper data){
            this.communityPrefix = communityPrefix;
            this.data = data;
        }
    }
}
