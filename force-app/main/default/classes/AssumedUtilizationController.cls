public class AssumedUtilizationController {
    
    @auraEnabled
    public static AssumedUtilizationDataWrapper getData(String recordId){
        
        List<Reconciliation_Schedule__c> rSRecordList = [Select Id, To_Date__c, From_Date__c, Lease__c, Type__c, Ratio_Variance__c,
                                                         Lease__r.Name, Lease__r.Aircraft__c, Lease__r.Aircraft__r.Name, Months__c,
                                                         Lease__r.Lease_Start_Date_New__c, Lease__r.Lease_End_Date_New__c,
                                                         Lease__r.Pro_Rata_NO_Of_Days_For_UR__c, Lease__r.aircraft__r.CSN__c,
                                                         Lease__r.aircraft__r.TSN__c
                                                         from Reconciliation_Schedule__c Where Id =: recordId];
        
        
        AssumedUtilizationDataWrapper reconciliationSchedule = new AssumedUtilizationDataWrapper();
        reconciliationSchedule.assumptionStartDate = rSRecordList[0].From_Date__c;
        reconciliationSchedule.assumptionEndDate = rSRecordList[0].To_Date__c;        
        reconciliationSchedule.leaseStartDate = rSRecordList[0].Lease__r.Lease_Start_Date_New__c;
        reconciliationSchedule.leaseEndDate = rSRecordList[0].Lease__r.Lease_End_Date_New__c;
        reconciliationSchedule.assetId = rSRecordList[0].Lease__r.Aircraft__c;
        reconciliationSchedule.asset = rSRecordList[0].Lease__r.Aircraft__r.Name;
        reconciliationSchedule.reconciliationStartDate = rSRecordList[0].From_Date__c;
        reconciliationSchedule.reconciliationEndDate = rSRecordList[0].To_Date__c;
        reconciliationSchedule.leaseId = rSRecordList[0].Lease__c;
        reconciliationSchedule.lease = rSRecordList[0].Lease__r.Name;
        reconciliationSchedule.leaseProRata = rSRecordList[0].Lease__r.Pro_Rata_NO_Of_Days_For_UR__c;
        reconciliationSchedule.aircraftCSN = rSRecordList[0].Lease__r.aircraft__r.CSN__c;
        reconciliationSchedule.aircraftTSN = rSRecordList[0].Lease__r.aircraft__r.TSN__c;
        reconciliationSchedule.invoiceDay = 1;
        reconciliationSchedule.assumptionFrequency = rSRecordList[0].From_Date__c.monthsBetween(rSRecordList[0].To_Date__c) + 1;
        
        // Get Just Previous Reconciliation Schedule
        List<Reconciliation_Schedule__c> previousReconciliationList = [Select Id, To_Date__c, From_Date__c
                                                                       from Reconciliation_Schedule__c Where
                                                                       To_Date__c <: rSRecordList[0].From_Date__c 
                                                                       Order By To_Date__c Desc Limit 1];
        
        if(previousReconciliationList.Size() > 0){
            
            Set<Date> monthEndingSet = new Set<Date>();
            
        Map<Id, FHFCWrapper> assemblyUtilizationFHFCMap = new Map<Id, FHFCWrapper>();
        
        FHFCWrapper uRFHFCCalculation = new FHFCWrapper();
        
        for(Utilization_Report__c urObj : [select Id, Name, Month_Ending__c, Type__c, Start_Date_F__c, End_Date_F__c,
                                           Airframe_Flight_Hours_Month__c, Airframe_Cycles_Landing_During_Month__c,
                                           (Select Id, Running_Hours_During_Month__c, Cycles_During_Month__c,
                                            Constituent_Assembly__c, Start_Date_F__c, End_Date_F__c
                                            from UR_List_Items__r)
                                           from Utilization_Report__c 
                                           Where Y_hidden_Lease__c =: rSRecordList[0].Lease__c 
                                           AND (Type__c = 'Actual' OR Type__c = 'True Up Gross' OR Type__c = 'Assumed/Estimated') 
                                           AND Status__c = 'Approved By Lessor'
                                           AND Month_Ending__c >=: previousReconciliationList[0].From_Date__c.toStartOfMonth()
                                           AND Month_Ending__c <=: previousReconciliationList[0].To_Date__c.toStartOfMonth().addMonths(1).addDays(-1)
                                           Order By CreatedDate Desc]){
                
              if((urObj.Type__c == 'Actual' || urObj.Type__c == 'True Up Gross') && !monthEndingSet.contains(urObj.Month_Ending__c)){
                 
                  // Calculate total actual FH and FC if UR Type is Actual or True Up Gross
                  uRFHFCCalculation.totalActualFH +=  leasewareutils.zeroifnull(urObj.Airframe_Flight_Hours_Month__c);
                  uRFHFCCalculation.totalActualFC +=  leasewareutils.zeroifnull(urObj.Airframe_Cycles_Landing_During_Month__c);
                  uRFHFCCalculation.totalActualMonths += CalculateMonths(urObj.Start_Date_F__c ,urObj.End_Date_F__c);
                  
                  for(Assembly_Utilization__c aUObj : urObj.UR_List_Items__r){
                      if(!assemblyUtilizationFHFCMap.containsKey(aUObj.Constituent_Assembly__c)){
                          assemblyUtilizationFHFCMap.put(aUObj.Constituent_Assembly__c,new FHFCWrapper());
                      }
                      assemblyUtilizationFHFCMap.get(aUObj.Constituent_Assembly__c).totalActualFH += leasewareutils.zeroifnull(aUObj.Running_Hours_During_Month__c);
                      assemblyUtilizationFHFCMap.get(aUObj.Constituent_Assembly__c).totalActualFC += leasewareutils.zeroifnull(aUObj.Cycles_During_Month__c);
                      assemblyUtilizationFHFCMap.get(aUObj.Constituent_Assembly__c).totalActualMonths += CalculateMonths(aUObj.Start_Date_F__c ,aUObj.End_Date_F__c);
                  }
                  
                  monthEndingSet.add(urObj.Month_Ending__c);
                  
              }else if(urObj.Type__c == 'Assumed/Estimated'){
                  
                  uRFHFCCalculation.totalAssumedFH +=  leasewareutils.zeroifnull(urObj.Airframe_Flight_Hours_Month__c);
                  uRFHFCCalculation.totalAssumedFC +=  leasewareutils.zeroifnull(urObj.Airframe_Cycles_Landing_During_Month__c);
                  uRFHFCCalculation.totalAssumedMonths += CalculateMonths(urObj.Start_Date_F__c ,urObj.End_Date_F__c);
                  
                  for(Assembly_Utilization__c aUObj : urObj.UR_List_Items__r){
                      if(!assemblyUtilizationFHFCMap.containsKey(aUObj.Constituent_Assembly__c)){
                          assemblyUtilizationFHFCMap.put(aUObj.Constituent_Assembly__c,new FHFCWrapper());
                      }
                      assemblyUtilizationFHFCMap.get(aUObj.Constituent_Assembly__c).totalAssumedFH += leasewareutils.zeroifnull(aUObj.Running_Hours_During_Month__c);
                      assemblyUtilizationFHFCMap.get(aUObj.Constituent_Assembly__c).totalAssumedFC += leasewareutils.zeroifnull(aUObj.Cycles_During_Month__c);
                      assemblyUtilizationFHFCMap.get(aUObj.Constituent_Assembly__c).totalAssumedMonths += CalculateMonths(aUObj.Start_Date_F__c ,aUObj.End_Date_F__c);
                  }
             }
       } 
 
            Decimal actualFHFCRatio;
            Decimal assumedFHFCRatio;
            Decimal ratioDifference;
            
            if(uRFHFCCalculation.totalActualFH != null &&
                uRFHFCCalculation.totalActualFC != null && uRFHFCCalculation.totalActualFC > 0){
            
                actualFHFCRatio = uRFHFCCalculation.totalActualFH / (uRFHFCCalculation.totalActualFC);
            }
              
            if(uRFHFCCalculation.totalAssumedFH != null && uRFHFCCalculation.totalAssumedFC != null && 
                uRFHFCCalculation.totalAssumedFC > 0){ 
        
                assumedFHFCRatio = uRFHFCCalculation.totalAssumedFH / (uRFHFCCalculation.totalAssumedFC);
            }    
        
            if(actualFHFCRatio != null && assumedFHFCRatio != null && assumedFHFCRatio > 0){
        
                ratioDifference = (1 - (actualFHFCRatio / assumedFHFCRatio)) * 100;
            }
        
            
            if(rSRecordList[0].Ratio_Variance__c == null || ratioDifference == null || ratioDifference > rSRecordList[0].Ratio_Variance__c){
            reconciliationSchedule.assumedMonthlyFH = (uRFHFCCalculation.totalActualFH /  (uRFHFCCalculation.totalActualMonths == 0 ? 1 : uRFHFCCalculation.totalActualMonths)).setscale(2,RoundingMode.HALF_UP);
            reconciliationSchedule.assumedMonthlyFC = (uRFHFCCalculation.totalActualFC /  (uRFHFCCalculation.totalActualMonths == 0 ? 1 : uRFHFCCalculation.totalActualMonths)).setscale(0,RoundingMode.HALF_UP);
        }else{
            reconciliationSchedule.assumedMonthlyFH = (uRFHFCCalculation.totalAssumedFH / ( uRFHFCCalculation.totalAssumedMonths == 0 ? 1 : uRFHFCCalculation.totalAssumedMonths)).setscale(2,RoundingMode.HALF_UP);
            reconciliationSchedule.assumedMonthlyFC = (uRFHFCCalculation.totalAssumedFC / ( uRFHFCCalculation.totalAssumedMonths == 0 ? 1 : uRFHFCCalculation.totalAssumedMonths)).setscale(0,RoundingMode.HALF_UP);
        }

        for(Constituent_Assembly__c constAssemblyObj : [Select Id, Name, Serial_Number__c, Type__c
                                                        From Constituent_Assembly__c 
                                                        where Asset__c =: rSRecordList[0].Lease__r.Aircraft__c
                                                            Order By Type__c]){
                                                            
                if(assemblyUtilizationFHFCMap.containsKey(constAssemblyObj.Id)){
                    
                    
             //Calculation of FH and FC on child level.
             actualFHFCRatio = assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualFH / (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualFC == null || assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualFC == 0 ? 1 : assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualFC);
             assumedFHFCRatio = assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedFH / (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedFC == null || assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedFC == 0 ? 1 : assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedFC);
                                                            
                    if(assumedFHFCRatio > 0){
                        ratioDifference = (1 - (actualFHFCRatio / assumedFHFCRatio)) * 100;
                    }
                                                            
             decimal avgFH;
             decimal avgFC;
             Boolean isTypeEngine = constAssemblyObj.Type__c.contains('Engine');
                                                            
             if(rSRecordList[0].Ratio_Variance__c == null || ratioDifference > rSRecordList[0].Ratio_Variance__c){
                 
                 avgFH = (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualFH /  (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualMonths == 0 ? 1 : assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualMonths)).setscale(2,RoundingMode.HALF_UP);
                 avgFC = (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualFC /  (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualMonths == 0 ? 1 : assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalActualMonths)).setscale(0,RoundingMode.HALF_UP);
           
             }else{
                 avgFH = (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedFH / ( assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedMonths == 0 ? 1 : assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedMonths)).setscale(2,RoundingMode.HALF_UP);
                 avgFC = (assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedFC / ( assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedMonths == 0 ? 1 : assemblyUtilizationFHFCMap.get(constAssemblyObj.Id).totalAssumedMonths)).setscale(0,RoundingMode.HALF_UP);
          	 }
             
             Decimal fhFC = avgFH/(avgFC == 0 ? 1 : avgFC);
             reconciliationSchedule.assumedUtilizationItemList.add(new AssumedUtilizationItemWrapper(constAssemblyObj, (avgFH == 0 ? null : avgFH), (avgFC == 0 ? null : avgFC), (fhFC == 0 ? null : fhFC), isTypeEngine));
                }else{
					Boolean isTypeEngine = constAssemblyObj.Type__c.contains('Engine');
                    reconciliationSchedule.assumedUtilizationItemList.add(new AssumedUtilizationItemWrapper(constAssemblyObj, null, null, null, isTypeEngine));
                }
            }
        }else{
            for(Constituent_Assembly__c constAssemblyObj : [Select Id, Name, Serial_Number__c, Type__c
                                                            From Constituent_Assembly__c 
                                                            where Asset__c =: rSRecordList[0].Lease__r.Aircraft__c
                                                            Order By Type__c]){
                                                                
                Boolean isTypeEngine = constAssemblyObj.Type__c.contains('Engine');
                reconciliationSchedule.assumedUtilizationItemList.add(new AssumedUtilizationItemWrapper(constAssemblyObj,null, null, null, isTypeEngine));
            }
       }
        return reconciliationSchedule;
    }
    
    @auraEnabled
    public static void createUtilizations(String data){
        // Create Utilization
    	Decimal FH_Tot=0,FC_Tot=0,FH_Input=0,FC_Input=0;
        
        AssumedUtilizationDataWrapper wrapperData = (AssumedUtilizationDataWrapper)System.JSON.deserialize(data, AssumedUtilizationDataWrapper.class);
        
        list<Utilization_Report__c> listInsertAssumedUR = new list<Utilization_Report__c>();        
      
        map<string,Constituent_Assembly__c> mapRelatedCAs = new map<string,Constituent_Assembly__c>();
        
        for(Constituent_Assembly__c relCA: [Select Id, Name, Serial_Number__c, Type__c,
                                            APU_TSN__c, APU_CSN__c, Derate__c, TSN__c, CSN__c, Current_TS__c,
                                            (Select Id, recordtype.DeveloperName, Inactive__c,
                                             Assembly_Event_Info__c, Lease__c
                                             From Assembly_MR_Info__r)
                                            From Constituent_Assembly__c 
                                            where Asset__c =: wrapperData.assetId]){
            mapRelatedCAs.put(relCA.type__c,relCA);
        }
        
        FH_Input = wrapperData.assumedMonthlyFH.setscale(2,RoundingMode.HALF_UP);
        FC_Input = wrapperData.assumedMonthlyFC.setscale(0,RoundingMode.HALF_UP);
        
        Date monthEndingDate =wrapperData.assumptionStartDate.toStartOfMonth().addMonths(1).addDays(-1);
        
        Decimal FH_Cal=FH_Input,FC_Cal=FC_Input;
        map<Date,Decimal> mapWigtageFactor = new map<Date,Decimal>();
        for(Integer counter=1; counter<=wrapperData.assumptionFrequency; counter++){
           
            FH_Cal=FH_Input;FC_Cal=FC_Input;
       
            /*
                Pro-rata can happen only in 2 situation. For the partial month of Lease Start or Lease end date.
                
            */
            // case 1 : Lease Start Date
            if(wrapperData.leaseStartDate.day()!=1 && wrapperData.leaseStartDate.toStartOfMonth().addMonths(1).addDays(-1)== monthEndingDate){
                if('Actual number of days in month/year'.equals(wrapperData.leaseProRata)){
                    mapWigtageFactor.put(monthEndingDate,Decimal.valueOf(wrapperData.leaseStartDate.daysBetween(monthEndingDate) + 1)/(Date.daysInMonth(wrapperData.leaseStartDate.Year(), wrapperData.leaseStartDate.month())));
                }else if('Fixed - 30/360'.equals(wrapperData.leaseProRata)){
                    mapWigtageFactor.put(monthEndingDate, Decimal.valueOf(Date.daysInMonth(wrapperData.leaseStartDate.Year(), wrapperData.leaseStartDate.month()) - wrapperData.leaseStartDate.day() + 1)/(30.0));
                }
                system.debug('1 mapWigtageFactor='+mapWigtageFactor);
                // else No Pro-rata
            }// case 2 : Lease End Date
            else if(wrapperData.leaseEndDate.addDays(1).day()!=1 && wrapperData.leaseEndDate.toStartOfMonth().addMonths(1).addDays(-1)== monthEndingDate){
                if('Actual number of days in month/year'.equals(wrapperData.leaseProRata)){
                    mapWigtageFactor.put(monthEndingDate,Decimal.valueOf(wrapperData.leaseEndDate.day())/(Date.daysInMonth(wrapperData.leaseEndDate.Year(), wrapperData.leaseEndDate.month())));
                }else if('Fixed - 30/360'.equals(wrapperData.leaseProRata)){
                    mapWigtageFactor.put(monthEndingDate, Decimal.valueOf(wrapperData.leaseEndDate.day() )/(30.0));
                }
                system.debug('2 mapWigtageFactor='+mapWigtageFactor);
                // else No Pro-rata
            }	
            
            if(mapWigtageFactor.containsKey(monthEndingDate)){
                FH_Cal = (FH_Cal * mapWigtageFactor.get(monthEndingDate)).setscale(2,RoundingMode.HALF_UP);
                FC_Cal = (FC_Cal * mapWigtageFactor.get(monthEndingDate)).setscale(0,RoundingMode.HALF_UP);						
            }
         
            Utilization_Report__c AssumedUR= new Utilization_Report__c(
		                                Name= 'AssumedUR'
                						,Status__c = 'Approved By Lessor'
		                                , Aircraft__c= wrapperData.assetId 
		                                , Airframe_Cycles_Landing_During_Month__c = FC_Cal
		                                ,Airframe_Flight_Hours_Month__c = FH_Cal
		                                , Month_Ending__c=monthEndingDate
										,Type__c= 'Assumed/Estimated'
										, True_Up_Gross__c = false
										,Y_Hidden_Bulk_Entry__c = true 
                                        ,Current_CSN__c = wrapperData.aircraftCSN + FC_Tot
                                        ,Current_TSN__c = wrapperData.aircraftTSN + FH_Tot
                                        ,Y_hidden_Lease__c = wrapperData.leaseId 
                						,Invoice_Day__c = wrapperData.invoiceDay
                						,Payment_Due_Day__c = wrapperData.paymentDue
		                                );

            FH_Tot +=FH_Cal;
            FC_Tot +=FC_Cal;  
            listInsertAssumedUR.add(AssumedUR); 
            monthEndingDate = monthEndingDate.toStartOfMonth().addMonths(2).addDays(-1);
            
        }
        
        if(listInsertAssumedUR.size() > 0){
            
            LeaseWareUtils.clearFromTrigger(); 	        		
            insert listInsertAssumedUR;
       
            map<id, List<Assembly_Utilization__c>> mapAssyUtilURLIs = new map<id, List<Assembly_Utilization__c>>();
            map<id, List<Utilization_Report_List_Item__c>> mapSupplRentUtilURLIs = new map<id, List<Utilization_Report_List_Item__c>>();	
            
            List <Assembly_Utilization__c>AllAssyUtilList=new List<Assembly_Utilization__c>();
            List<Utilization_Report_List_Item__c> insertURLIs = new List<Utilization_Report_List_Item__c>();
            
            Map <String,Decimal> assemblyURLIAndTSN = new Map<String,Decimal>();
            Map <String,Decimal> assemblyURLIAndCSN = new Map<String,Decimal>();
            
            Decimal FH_URLI,FC_URLI,Derate_URLI, Derate_URLI_Base;
            
            for(Utilization_Report__c curUR :listInsertAssumedUR){
                
           		map<id,LeaseWareUtils.AssemblyUtilizationWrapper> mapWrapper = new map<id,LeaseWareUtils.AssemblyUtilizationWrapper>();  
                Id tsid;
                for(AssumedUtilizationItemWrapper wrapperItemsList : wrapperData.assumedUtilizationItemList){
                    
                    //defaulting 
                    if(wrapperItemsList.fH!=null ) FH_URLI = wrapperItemsList.fH.setscale(2,RoundingMode.HALF_UP);
                    if(wrapperItemsList.fC!=null) FC_URLI = wrapperItemsList.fC.setscale(0,RoundingMode.HALF_UP);   
                    if(wrapperItemsList.derate!=null) Derate_URLI = wrapperItemsList.derate; 		
                    
                    if(mapWigtageFactor.containsKey(curUR.Month_Ending__c)){
                        FH_URLI = (FH_URLI * mapWigtageFactor.get(curUR.Month_Ending__c)).setscale(2,RoundingMode.HALF_UP);
                        FC_URLI = (FC_URLI * mapWigtageFactor.get(curUR.Month_Ending__c)).setscale(0,RoundingMode.HALF_UP);						
                    }
                    
                    Decimal assemblyURLITSN, assemblyURLICSN;
                    if(assemblyURLIAndTSN != null && assemblyURLIAndTSN.size()>0 
                       && assemblyURLIAndCSN != null && assemblyURLIAndCSN.size()>0
                       && assemblyURLIAndTSN.containsKey(wrapperItemsList.assetOrAssemblyId) 
                       && assemblyURLIAndCSN.containsKey(wrapperItemsList.assetOrAssemblyId)){
                           assemblyURLITSN = assemblyURLIAndTSN.get(wrapperItemsList.assetOrAssemblyId);
                           assemblyURLICSN = assemblyURLIAndCSN.get(wrapperItemsList.assetOrAssemblyId);
                       }else {
                           assemblyURLITSN = mapRelatedCAs.get(wrapperItemsList.assemblyType).TSN__c;
                           assemblyURLICSN = mapRelatedCAs.get(wrapperItemsList.assemblyType).CSN__c;
                       }
                    
                    tsid= null;
                    if(wrapperItemsList.assemblyType.Contains('Engine')) tsid = mapRelatedCAs.get(wrapperItemsList.assemblyType).Current_TS__c;
                    
                    LeaseWareUtils.AssemblyUtilizationWrapper assyWrapper = new LeaseWareUtils.AssemblyUtilizationWrapper
                        (wrapperItemsList.assetOrAssemblyId,
                         assemblyURLITSN,
                         assemblyURLICSN,
                         leasewareUtils.zeroIfNull(FC_URLI),
                         leasewareUtils.zeroIfNull(FH_URLI),
                         null,//APU_Hours
                         null, //APU Cycles            
                         mapRelatedCAs.get(wrapperItemsList.assemblyType).APU_TSN__c,    //APU_Hours_At_Month_Start,
                         mapRelatedCAs.get(wrapperItemsList.assemblyType).APU_CSN__c,     //APU_Cycles_At_Month_Start,
                         Derate_URLI,    //Derate
                         mapRelatedCAs.get(wrapperItemsList.assemblyType).Derate__c,//Prev Derate
                         tsid);
                    
                    mapWrapper.put(wrapperItemsList.assetOrAssemblyId, assyWrapper);                            
                    
                    assemblyURLIAndTSN.put(wrapperItemsList.assetOrAssemblyId,assemblyURLITSN + leasewareUtils.zeroIfNull(FH_URLI));
                    assemblyURLIAndCSN.put(wrapperItemsList.assetOrAssemblyId,assemblyURLICSN + leasewareUtils.zeroIfNull(FC_URLI));
                }// End - CA
                
                //Call util method and add below
                List<Assembly_Utilization__c> returnlstAssyUtil = new List<Assembly_Utilization__c>();
                List<Utilization_Report_List_Item__c> returnlstSupplUtil = new List<Utilization_Report_List_Item__c>();                
                LeaseWareUtils.CreateAssemblyUtilizationsForUR(curUR,mapWrapper,null,mapRelatedCAs,returnlstAssyUtil,returnlstSupplUtil);
                
                mapAssyUtilURLIs.put(curUR.id, returnlstAssyUtil);
                mapSupplRentUtilURLIs.put(curUR.id,returnlstSupplUtil);
                
                AllAssyUtilList.addall(returnlstAssyUtil);
                insertURLIs.addall(returnlstSupplUtil);						
                
            }
            
            //Insert the Assembly utilization records 
            
            LeaseWareUtils.clearFromTrigger();	
            insert AllAssyUtilList;
            
            //Create a relation between Assembly Utilization and Supplemental Rent Utilization records
            LeaseWareUtils.CreateUtilizationRelation(mapAssyUtilURLIs, mapSupplRentUtilURLIs);
            
            
            LeaseWareUtils.clearFromTrigger();
            Insert insertURLIs;					
         
        }
    }
    
    private static Decimal CalculateMonths(Date startDate, Date endDate)
    {
        if(startDate == null || endDate == null){
            return 1;
        }
        
        Decimal numOfDays = startDate.daysbetween(endDate) + 1;
        Decimal daysInMonth = date.daysInMonth(startDate.Year(), startDate.month());

        return numOfDays/daysInMonth;
    }
    
    public class FHFCWrapper{
        @AuraEnabled public Decimal totalActualFH{get; set;}
        @AuraEnabled public Decimal totalActualFC{get; set;}
        @AuraEnabled public Decimal totalAssumedFH{get; set;}
        @AuraEnabled public Decimal totalAssumedFC{get; set;}
        @AuraEnabled public Decimal totalActualMonths{get; set;}
        @AuraEnabled public Decimal totalAssumedMonths{get; set;}
        
        Public FHFCWrapper(){
            totalActualFH = 0;
            totalActualFC = 0;
            totalAssumedFH = 0;
            totalAssumedFC = 0;
            totalActualMonths = 0;
            totalAssumedMonths = 0;
        }
    }
    
    public class AssumedUtilizationDataWrapper{
        @AuraEnabled public Id leaseId{get;set;}
        @AuraEnabled public String lease{get; set;}
        @AuraEnabled public Date leaseStartDate{get; set;}
        @AuraEnabled public Date leaseEndDate{get; set;}
        @auraEnabled public String leaseProRata {get;set;}
        @AuraEnabled public Id assetId{get;set;}
        @auraEnabled public String asset {get;set;}
        @AuraEnabled public Decimal aircraftCSN{get;set;}
        @AuraEnabled public Decimal aircraftTSN{get;set;}
        @AuraEnabled public Date reconciliationStartDate{get;set;}
        @AuraEnabled public Date reconciliationEndDate{get;set;}
        @AuraEnabled public Date assumptionStartDate{get;set;}
        @AuraEnabled public Date assumptionEndDate{get;set;}
        @AuraEnabled public Integer assumptionFrequency{get;set;}
        @AuraEnabled public Decimal assumedMonthlyFH{get;set;}
        @AuraEnabled public Decimal assumedMonthlyFC{get;set;}
        @AuraEnabled public Integer invoiceDay{get;set;}
        @AuraEnabled public Integer paymentDue{get;set;}
        @auraEnabled public List<AssumedUtilizationItemWrapper> assumedUtilizationItemList {get;set;}
        
        
        Public AssumedUtilizationDataWrapper(){
            assumedUtilizationItemList = new List<AssumedUtilizationItemWrapper>();
        }
    }
    
    public class AssumedUtilizationItemWrapper{
        @AuraEnabled public String assetOrAssemblyName{get;set;}
        @AuraEnabled public Id assetOrAssemblyId{get; set;}
        @AuraEnabled public String serialNumber{get; set;}
        @AuraEnabled public String assemblyType{get; set;}
        @AuraEnabled public Decimal fH{get; set;}
        @AuraEnabled public Decimal fC{get; set;}
        @AuraEnabled public Decimal fhFC{get;set;}
        @AuraEnabled public Decimal derate{get;set;}
        @AuraEnabled public Boolean isTypeEngine{get;set;}
        
         Public AssumedUtilizationItemWrapper(Constituent_Assembly__c constAssemblyObj, Decimal fH, Decimal fC, Decimal fhFC, Boolean isTypeEngine){
             this.assetOrAssemblyId = constAssemblyObj.Id;
             this.assetOrAssemblyName = constAssemblyObj.Name;
             this.serialNumber = constAssemblyObj.Serial_Number__c;
             this.fH = fH;
             this.fC = fC;
             this.fhFC = fhFC;
             this.assemblyType= constAssemblyObj.Type__c;
             this.isTypeEngine = isTypeEngine;
         }
    }
}