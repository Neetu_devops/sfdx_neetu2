public class DimensionApplicationTriggerHandler implements ITrigger{

    private final string triggerBefore = 'DimensionApplicationTriggerHandlerBefore';
    private final string triggerAfter = 'DimensionApplicationTriggerHandlerAfter';
    public DimensionApplicationTriggerHandler(){
        system.debug('INSIDE DimensionApplicationTriggerHandler');
    }
    
     /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void  bulkBefore(){
    }
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void  bulkAfter(){
    }
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void  beforeInsert(){
        
        //validate
        list<Dimension_Application__c> newList =  (list<Dimension_Application__c>)trigger.New;
        Set<Id> leaseIds = new Set<Id>();
        for(Dimension_Application__c dimApp : newList){
            leaseIds.add(dimApp.Lease__c);
        }
        //Get the existing list
        map<Id,Lease__c> mapLease= new map<Id,Lease__c>([select Id,Name,(select Id,Name,Dimension_Name__c from Dimension_Applications__r)from Lease__c where ID IN :leaseIds]);
        system.debug('mapLease---'+ mapLease);
        for(Dimension_Application__c dimApp : newList){
            if(mapLease.get(dimApp.Lease__c)==null || mapLease.get(dimApp.Lease__c).Dimension_Applications__r==null)return ;
           
              for(Dimension_Application__c storedDimApp :mapLease.get(dimApp.Lease__c).Dimension_Applications__r){
                if(storedDimApp.Dimension_Name__c.equals(dimApp.Dimension_Name__c)){
                    dimApp.addError('Dimension Application with ' +dimApp.Dimension_Name__c+'  already exists');
                }
              }
            
        }
    }
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void  beforeUpdate(){}
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void  beforeDelete(){
       
        system.debug('Dimension.beforeDelete(-)');
    }
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void  afterInsert(){
       
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        
    }
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void  afterDelete(){
         
    }
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void  afterUnDelete(){}   
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
   public void  andFinally(){}

}