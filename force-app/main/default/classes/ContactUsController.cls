public without sharing class ContactUsController {
    
    @AuraEnabled
	public static List<ContactWrapper> getContacts(){
        
        List<ContactWrapper> contactList = new List<ContactWrapper>();
        String lessorName;
        for(Lessor__c setup : [select Name from Lessor__c limit 1]){
            lessorName =  setup.Name;
        }
        
        if(String.isNotBlank(lessorName)){
            List<Counterparty__c> lessorList = [select id from Counterparty__c where Name = :lessorName limit 1];
            if(lessorList.size()>0){
                system.debug('lessorList'+lessorList);
                for(Contact contact : [select id, Email, Description, Department from Contact 
                                       where Lessor__c in : lessorList and Show_in_Contact_Us__c = true order by Department]){
                    contactList.add(new ContactWrapper(contact.Department,contact.Description,contact.Email));
                }
            }
        }
        
        return contactList;
        
    }
    
    public class ContactWrapper{
        
        @AuraEnabled public String category {get;set;}
        @AuraEnabled public String description{get;set;}
        @AuraEnabled public String email {get;set;}
        
        public ContactWrapper(String category, String description, String email){
            this.category = category;
            this.description = description;
            this.email = email;            
        }
    }
}