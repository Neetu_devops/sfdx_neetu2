/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestFleetAcquisitionAnalysis {

    static testMethod void CRUD_FleetAcquisitionAnalysisTest() {
        // TO DO: implement unit test
        Fleet_Acquisition_Analysis__c c1 = new Fleet_Acquisition_Analysis__c(name='Dummy',Tax_rate__c=40,Term_Months__c =40
        			,Security_Deposit__c = 7000000,Residual_Value__c=5000000,Purchase_Price__c=26000000
        			,Monthly_Lease_Payment__c = 5000000,Debt_Percentage__c=80,Annual_interest_rate__c=4);
        			
        insert c1;
		
	    LeaseWareUtils.clearFromTrigger();	
	    
	    c1.Balloon_Payment__c = 7000000;
	    c1.Term_Months__c = 50;
	    update c1;

		
	    LeaseWareUtils.clearFromTrigger();	
	    
	    delete c1;
	    
		
	    LeaseWareUtils.clearFromTrigger();	
	    
	    undelete c1;	    	    
        
    }
    
    static testMethod void FleetAcquisitionCalculationTest() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createDepreciationSchedule(mapInOut);
    	Id DSID1 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;
		system.debug('DSID1'+DSID1);
    	//Lease__c lease1 = (Lease__c)mapInOut.get('Lease__c');
    	
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Depreciation_schedule__c = DSID1;
        //Aircraft1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
	    update     Aircraft1;  	         
		
	    LeaseWareUtils.clearFromTrigger();        
        
        Fleet_Acquisition_Analysis__c c1 = new Fleet_Acquisition_Analysis__c(name='Dummy',Tax_rate__c=40,Term_Months__c =40
        			,Security_Deposit__c = 7000000,Residual_Value__c=5000000,Purchase_Price__c=26000000
        			,Monthly_Lease_Payment__c = 5000000,Debt_Percentage__c=80,Annual_interest_rate__c=4
        			,Aircraft__c=Aircraft1.Id);
        			
        insert c1;
		
	    LeaseWareUtils.clearFromTrigger();	
	    
		
		
		    
		PageReference pageRefMap                 = Page.FleetAcquisitionAnalysisChart;
        Test.setCurrentPage(pageRefMap);  
        
        ApexPages.currentPage().getParameters().put('Id',c1.id);  
        ApexPages.StandardController scontroller = new ApexPages.standardController( c1 ); 
        Test.startTest();
            FleetAcquisitionAnalysisCalulcation cont = new FleetAcquisitionAnalysisCalulcation(scontroller); 
        Test.stopTest();

		FleetAcquisitionAnalysisCalulcation.getChartDepreciation();
		FleetAcquisitionAnalysisCalulcation.getNPV_Buy();
		FleetAcquisitionAnalysisCalulcation.getNPV_OL();
		FleetAcquisitionAnalysisCalulcation.getNPV_FL();
		FleetAcquisitionAnalysisCalulcation.getNPV_Buy_Rank();
		FleetAcquisitionAnalysisCalulcation.getNPV_OL_Rank();
		FleetAcquisitionAnalysisCalulcation.getNPV_FL_Rank();	
		FleetAcquisitionAnalysisCalulcation.getChart_Buy();
		FleetAcquisitionAnalysisCalulcation.getChart_OL();
		FleetAcquisitionAnalysisCalulcation.getChart_Fl();
		FleetAcquisitionAnalysisCalulcation.getAmortizationTable_Buy();	
		FleetAcquisitionAnalysisCalulcation.getAmortizationTable_FL();	

        
    }    
    
}