public with sharing class UtilityWithSharingController {

    public static List<Operator__c> getOperators(){
    	return [select id from Operator__c];
    }

}