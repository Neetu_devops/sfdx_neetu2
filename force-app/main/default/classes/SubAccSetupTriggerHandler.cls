public class SubAccSetupTriggerHandler implements ITrigger{
 	
    private final string triggerBefore = 'SubAccSetupTriggerHandlerBefore';
    private final string triggerAfter = 'SubAccSetupTriggerHandlerAfter';
    public SubAccSetupTriggerHandler(){}
     public void bulkBefore(){}
     
   
    public void bulkAfter(){}
     
    public void beforeInsert(){}
    public void beforeUpdate(){ }
    public void beforeDelete(){}
    
    private gl_account_code__c createGLCode(Accounting_setup__c acc,String name,String accCls,String sign,
                                                 String transSubType,boolean intr,String jedesc,String level,String transType,boolean vat){
        
        GL_account_code__c gl = new GL_Account_code__c (name = name,Account_Classification__c=accCls,
                                                           sign__c=sign,Transaction_Subtype__c= transSubType,Intercompany__c=intr,
                                                           JE_Description__c=jedesc,Level__c=level,Transaction_Type__c=transType,
                                                           VAT_GST_Entry__c=vat,Accounting_Setup__c=acc.id);
        return gl;
    }
    public void afterInsert(){
        system.debug('SubAccSetupTriggerHandlerBefore.afterInsert(++++)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        list<gl_account_code__c> glList =  new list<gl_account_code__c>();
        
        list<Accounting_setup__c> subaccList = ( list<Accounting_setup__c>)trigger.new;
        for(Accounting_setup__c acc :subaccList){
            if(acc.name == 'Base Rent/Deferred Revenue'){  
                GL_account_code__c gRent = createGlCode(acc,'AR - Rent','Accounts Receivable','D','Sales Invoice',false,
                                                        'AR - Rent','Lessor','Rent',false);
                glList.add(grent);
                GL_account_code__c grev = createGlCode(acc,'Base Rent Income','Revenue','C','Sales Invoice',false,
                                                       'Base Rent Income','Lessor','Rent',false);
                glList.add(grev);
                GL_account_code__c gderRevC = createGlCode (acc,'Unearned Revenue - Lease Income','Deferred Revenue Liability','C','Sales Invoice',false,
                                                            'Unearned Revenue - Lease Income','Lessor','Rent',false);
                glList.add(gderRevC);
                GL_account_code__c gderRevD = createGlCode (acc,'Unearned Revenue - Lease Income','Deferred Revenue Liability','D','Sales Invoice',false,
                                                            'Unearned Revenue - Lease Income','Lessor','Rent',false);
                glList.add(gderRevD);
             }
            else if(acc.name == 'Straight Line'){  
                GL_account_code__c gSlrC = createGlCode(acc,'Straight Line Expense','Straight Line Adjustment','D','Sales Invoice',false,
                                                        'Straight Line Expense','Lessor','Rent',false);
                glList.add(gSlrC);
                GL_account_code__c gSlrD = createGlCode(acc,'Straight Line Revenue','Straight Line Adjustment','C','Sales Invoice',false,
                                                        'Straight Line Revenue','Lessor','Rent',false);
                glList.add(gSlrD);
                GL_account_code__c gRevSlr = createGlCode (acc,'Prepaid Rent-Straight Line','Straight Line Adjustment','Both','Sales Invoice',false,
                                                            'Prepaid Rent-Straight Line','Lessor','Rent',false);
                glList.add(gRevSlr);
             }else if(acc.name == 'MX Invoices'){  
                GL_account_code__c gMx = createGlCode(acc,'A/R - MX','Accounts Receivable','D','Sales Invoice',false,
                                                        'A/R - MX','Lessor','Aircraft MR',false);
                glList.add(gMx);
                GL_account_code__c gLbtly = createGlCode(acc,'MX - Liability','Liability','C','Sales Invoice',false,
                                                       'MX - Liability','Lessor','Aircraft MR',false);
                glList.add(gLbtly);
             }else if(acc.name == 'Customer Payments'){  
                
               	 GL_account_code__c gCashR = createGlCode(acc,'Cash - Rent','Cash','D','Payment',false,
                                                          'Cash - Rent','Lessor','Rent',false);
                 glList.add(gCashR);
                 GL_account_code__c gCashMx = createGlCode(acc,'Cash - Mx','Cash','D','Payment',false,
                                                           'Cash - Mx','Lessor','Aircraft MR',false);
                 glList.add(gCashMx);
                 GL_account_code__c gCashInv = createGlCode(acc,'Cash - Interest','Cash','D','Payment',false,
                                                           'Cash - Interest','Lessor','Interest',false);
                 glList.add(gCashInv);
                 GL_account_code__c gBnkChrg = createGlCode(acc,'Bank - Fees','Bank Fees','D','Payment',false,
                                                           'Bank - Fees','Lessor','All',false);
                glList.add(gBnkChrg);
                
                GL_account_code__c gprePay = createGlCode(acc,'Customer Prepayments','Pre Payments','D','Payment',false,
                                                           'Customer Prepayments','Lessor','All',false);
                glList.add(gprePay);
                

                 GL_account_code__c gCashSD = createGlCode(acc,'Cash - SD','Cash','D','Payment',false,
                                             'Cash - SD','Lessor','Security Deposit',false);
                glList.add(gCashSD);
             
            }else if(acc.name == 'Late Interest'){  
               
               	 GL_account_code__c gInvAR = createGlCode(acc,'Interest Receivable','Accounts Receivable','D','Sales Invoice',false,
                                                          'Interest Receivable','Lessor','Interest',false);
                 glList.add(gInvAR);
                 GL_account_code__c gInvRev = createGlCode(acc,'Interest Revenue','Revenue','C','Sales Invoice',false,
                                                           'Interest Revenue','Lessor','Interest',false);
                 glList.add(gInvRev);
             }else if(acc.name == 'Head Lease Sub Lease Rent'){  
                
                
                GL_account_code__c grevP = createGlCode(acc,'Sub Lease Revenue','Revenue','C','Sales Invoice',True,
                                                        'Sub Lease Revenue','Lessor','Rent',false);
                glList.add(grevP);
                GL_account_code__c gderRevCP = createGlCode (acc,'Sub Lease Expense','Expense','D','Sales Invoice',True,
                                                        'Sub Lease Expense','Lessor','Rent',false);
                glList.add(gderRevCP);
                GL_account_code__c gderRevC = createGlCode (acc,'Deferred Revenue','Deferred Revenue Liability','C','Sales Invoice',true,
                                                        'Deferred Revenue','Lessor','Rent',false);
                glList.add(gderRevC);
                GL_account_code__c gderRevD = createGlCode (acc,'Deferred Expense','Deferred Revenue Liability','D','Sales Invoice',true,
                                             'Deferred Expense','Lessor','Rent',false);
                glList.add(gderRevD);

                GL_account_code__c gderRevDP = createGlCode (acc,'Intercompany A/P','Accounts Payable','C','Sales Invoice',True,
                                                        'Intercompany A/P','Lessor','Rent',false);
                glList.add(gderRevDP);

                 
                GL_account_code__c gLbtlyP = createGlCode(acc,'Intercompany A/R 1','Accounts Receivable','D','Sales Invoice',true,
                                                       'Intercompany A/R 1','HeadLessor','Rent',false);
                glList.add(gLbtlyP);
                 
                GL_account_code__c grevS = createGlCode(acc,'Head Lease Revenue 1','Revenue','C','Sales Invoice',True,
                                                        'Head Lease Revenue 1','HeadLessor','Rent',false);
                glList.add(grevS);
                GL_account_code__c gderRevCS = createGlCode (acc,'Head Lease Expense 1','Expense','D','Sales Invoice',True,
                                                        'Head Lease Expense 1','HeadLessor','Rent',false);
                glList.add(gderRevCS);
                GL_account_code__c gderRevC1 = createGlCode (acc,'Deferred Revenue 1','Deferred Revenue Liability','C','Sales Invoice',true,
                                                        'Deferred Revenue 1','HeadLessor','Rent',false);
                glList.add(gderRevC1);
                GL_account_code__c gderRevD1 = createGlCode (acc,'Deferred Expense 1','Deferred Revenue Liability','D','Sales Invoice',true,
                                            'Deferred Expense 1','HeadLessor','Rent',false);
                glList.add(gderRevD1);
                GL_account_code__c gderRevDS = createGlCode (acc,'Intercompany A/P 1','Accounts Payable','C','Sales Invoice',True,
                                                        'Intercompany A/P 1','HeadLessor','Rent',false);
                glList.add(gderRevDS);

                GL_account_code__c gLbtlyS = createGlCode(acc,'Intercompany A/R 2','Accounts Receivable','D','Sales Invoice',true,
                                                       'Intercompany A/R 2','HeadLessorII','Rent',false);
                glList.add(gLbtlyS);

                GL_account_code__c ghl2Rev = createGlCode(acc,'Head Lease Revenue 2','Revenue','C','Sales Invoice',true,
                                                       'Head Lease Revenue 2','HeadLessorII','Rent',false);
                glList.add(ghl2Rev);
                GL_account_code__c gderRevC2 = createGlCode (acc,'Deferred Revenue 2','Deferred Revenue Liability','C','Sales Invoice',true,
                                                        'Deferred Revenue 2','HeadLessorII','Rent',false);
                glList.add(gderRevC2);

              
                 
             }else if(acc.name == 'Head Lease Sub Lease Mx'){  
                
                
                GL_account_code__c gderRevDP = createGlCode (acc,'Intercompany A/P','Accounts Payable','C','Sales Invoice',True,
                                                        'Intercompany A/P','Lessor','Aircraft MR',false);
                glList.add(gderRevDP);

                 
                GL_account_code__c gLbtlyP = createGlCode(acc,'Intercompany A/R 1','Accounts Receivable','D','Sales Invoice',true,
                                                       'Intercompany A/R 1','HeadLessor','Aircraft MR',false);
                glList.add(gLbtlyP);
                
                GL_account_code__c gderRevDS = createGlCode (acc,'Intercompany A/P 1','Accounts Payable','C','Sales Invoice',True,
                                                        'Intercompany A/P 1','HeadLessor','Aircraft MR',false);
                glList.add(gderRevDS);

                GL_account_code__c gLbtlyS = createGlCode(acc,'Intercompany A/R 2','Accounts Receivable','D','Sales Invoice',true,
                                                       'Intercompany A/R 2','HeadLessorII','Aircraft MR',false);
                glList.add(gLbtlyS);

                 
             }else if(acc.name == 'Security Deposit'){  
               
                GL_account_code__c gInvAR = createGlCode(acc,'Security Deposit Receivable','Accounts Receivable','D','Sales Invoice',false,
                                                      'Security Deposit Receivable','Lessor','Security Deposit',false);
             glList.add(gInvAR);
             GL_account_code__c gInvRev = createGlCode(acc,'Security Deposit - Liability','Liability','C','Sales Invoice',false,
                                                       'Security Deposit - Liability','Lessor','Security Deposit',false);
             glList.add(gInvRev);
            }
            else if(acc.name == 'Credit Memo'){  
                GL_account_code__c gRevDec = createGlCode(acc,'Credit Memo Revenue Reduction','Contra-Revenue','D','Credit Memo',false,
                                                        'Credit Memo Revenue Reduction','Lessor','Credit Memo',false);
                glList.add(gRevDec);
                GL_account_code__c gUnAppCm = createGlCode(acc,'Unapplied Credit Memo','Contra-Asset','C','Credit Memo',false,
                                                    'Unapplied Credit Memo','Lessor','Credit Memo',false);
                glList.add(gUnAppCm);
            }
			
            
            
        }
        if(glList.size()>0) {
            try{
                insert glList;
            }
            catch(DmlException ex){
                System.debug(ex);
                string errorMessage='';
                for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                    // Process exception here
                    errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                }
                system.debug('errorMessage=='+errorMessage);
                for(Accounting_setup__c acc :subaccList){
                    acc.addError(errorMessage);
                }
                
            }
        }
        system.debug('SubAccSetupTriggerHandlerBefore.afterInsert(++++)');
        
    }
    
    public void afterUpdate(){}
 
    
    public void afterDelete(){}
    
    
    public void afterUnDelete(){}    
 
    public void andFinally(){}
}