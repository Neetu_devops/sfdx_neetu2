public without sharing class RefreshSummariesController {
	
    @AuraEnabled
    public static void refreshSummaries(Id campaignId){
         Utility.calculateBidSummary(new Set<Id>{campaignId});
    }
}