public without sharing class AuditSchedulerController {
    
    @auraEnabled
    public static void InsertAuditRecs(Date auditStartValue, Integer auditPeriodicity, Boolean isdelete, Date contractDate, Id recordId)
    {
        try{

            List<Lease__c> thisLease = [SELECT Id, Aircraft__c, Aircraft__r.Name, Lease_Start_Date_New__c, Lease_End_Date_New__c from Lease__c where Id=:recordId];
            
            set<date> setExistingAudits = new set<date>();
            list<Technical_Audit__c> listAuditsToAdd = new list<Technical_Audit__c>();
            list<Technical_Audit__c> listAuditsToDelete = new list<Technical_Audit__c>();
            
            Technical_Audit__c[] existingAudits = [select Id, Status__c, Audit_Date__c from Technical_Audit__c where Lease__c = :recordId];
            System.debug('existingAudits : '+existingAudits); 
            
            //delete existing Audit records if flag isdelete is true
            if(existingAudits.size()>0){
            for(Technical_Audit__c curAudit: existingAudits){
                setExistingAudits.add(curAudit.Audit_Date__c);
                if(curAudit.Status__c == 'Open' && isdelete){
                    listAuditsToDelete.add(curAudit);
                }
                system.debug('Existing Audits to Delete ' + listAuditsToDelete);
            }
                if(listAuditsToDelete.size()>0 ){
                delete listAuditsToDelete;
                }
            }
            
            
            if(thisLease.size() >0){
            
            system.debug('First Audit Date ' + auditStartValue);    
            Date curAuditDate = Date.newInstance(auditStartValue.year(), auditStartValue.month(), auditStartValue.day());
            system.debug('First Audit Date ' + curAuditDate);
            
            integer nNoOfSlips = curAuditDate.monthsBetween(thisLease[0].Lease_End_Date_New__c);
            integer nMonths= auditPeriodicity;
                decimal lastLoopVal;
                if(nMonths > 0){
             lastLoopVal =   (nNoOfSlips / nMonths) *auditPeriodicity;
                }    
           
            for(integer i=0; i<=nNoOfSlips; i+=nMonths){
                if(i != lastLoopVal){
                    listAuditsToAdd.add(new Technical_Audit__c(lease__c = thisLease[0].Id, Audit_Date__c=curAuditDate,Type__c='Technical Audit',
                                                               Asset__c = thisLease[0].Aircraft__c, Name= 'AU-'+thisLease[0].Aircraft__r.Name+'-'+curAuditDate.year()+'-'+curAuditDate.month()));
                    system.debug('Adding New Audit nNoOfSlips: ' + nNoOfSlips + ' nMonths: ' + nMonths + ' i: '+i+ ' curAuditDate: '+ curAuditDate);
                    }
                    else if(i== lastLoopVal){
                    System.debug('inside if');
                    listAuditsToAdd.add(new Technical_Audit__c(lease__c = thisLease[0].Id, Audit_Date__c=curAuditDate,Type__c='Re-Delivery',
                                                               Asset__c = thisLease[0].Aircraft__c, Name= 'AU-'+thisLease[0].Aircraft__r.Name+'-'+curAuditDate.year()+'-'+curAuditDate.month()));
                }
                System.debug('nMonths :'+nMonths);
                //for increment of the date in for loop
                curAuditDate=curAuditDate.addMonths(nMonths);  
                
            }
            System.debug('listAuditsToAdd :'+listAuditsToAdd);
            
            insert listAuditsToAdd;
            }
        }catch(exception e){
            system.debug('Exception ' + e.getMessage());
        }
    }

    @AuraEnabled
    Public static LeaseInfo getLeaseData(String recordId){
        List<Lease__c> lease = [Select Id, Name, Aircraft__c, Contractual_Delivery_Date__c,Contractual_Redelivery_Date__c,
                          Lease_Start_Date_New__c, Lease_End_Date_New__c 
                          from Lease__c
                          where Id =: recordId 
                         ];
        System.debug('lease :'+lease);
        if(lease.size()>0){
        LeaseInfo info = new LeaseInfo();
        
        info.ContractDeliverDate  = lease[0].Contractual_Delivery_Date__c;
        info.AgreementDate        = lease[0].Contractual_Redelivery_Date__c;
        info.LeaseStartDate       = lease[0].Lease_Start_Date_New__c;
        info.LeaseEndDate         = lease[0].Lease_End_Date_New__c;
        
        System.debug('info :'+info);
        return info;
        }
        return null;
    }
    
    public class LeaseInfo {
        @AuraEnabled public String LeaseId  {get; set;}
        @AuraEnabled public Date LeaseStartDate  {get; set;}
        @AuraEnabled public Date LeaseEndDate  {get; set;}
        @AuraEnabled public Date ContractDeliverDate  {get; set;}
        @AuraEnabled public Date AgreementDate  {get; set;}
        
    }
}