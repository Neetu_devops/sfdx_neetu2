/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestUndeleteTriggers {

    static testMethod void myUnitTest() {
        
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        insert operator;
        // Set up the Lease record
        //Date dateToday = Date.parse('01/30/2009');
        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');    
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10', Lease_Start_Date_New__c=Date.parse('01/01/2010'), Lease_End_Date_New__c=Date.today().addYears(1));
        insert lease;
        // Set up the Aircraft record.
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN,Date_of_Manufacture__c = System.today(), Aircraft_Type__c='737', Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available');
        insert a;
		

        lease.aircraft__c=a.id;
        update lease;
		system.debug('Updated Lease');
        Rent__c rent = new Rent__c();
        rent.Rent_Due_Date__c=Date.parse('03/27/2009');
        rent.Rent_Due_Amount__c=12345;
        rent.Name='Dummy'; //Don't worry, will get auto set.
        rent.RentPayments__c=lease.Id;
        rent.For_Month_Ending__c=Date.today().toStartOfMonth().addDays(-1);
        try {
            insert rent;    
        } catch (system.Dmlexception e) {
            system.debug (e);
        }

        try {
            rent.Is_Recalculate_Interest__c=true;
            update rent;
        } catch (system.Dmlexception e) {
            system.debug (e);
        }
        system.debug('Updated rent');

        try{
            delete rent;
            undelete rent;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }

        lease.aircraft__c=null;
        update lease;
            
        try{
            delete a;
            undelete a;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }

        try{
            delete lease;
            undelete lease;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }
            
        try{
            delete operator;
            undelete operator;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }

    }

    static testMethod void myUnitTest2() {

        // Set up the Aircraft record.
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN,Date_of_Manufacture__c = System.today(), Aircraft_Type__c='737', Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available');
        insert a;

        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');    
        
        Aircraft_Eligible_Event__c assEE= new Aircraft_Eligible_Event__c(Name= 'ca test', 
        Aircraft__c=a.Id, Start_Date__c=dateStart, End_Date__c=dateEnd, InitClaimAmt_MR_Engine_1__c=1000000,TSN_N__c = 100, CSN__c = 100,Event_Type__c='Heavy Maintenance 1');
        insert assEE;
        
	    LeaseWareUtils.clearFromTrigger();
	            
        Aircraft_Eligible_Event_Invoice__c invoice = new Aircraft_Eligible_Event_Invoice__c(
        Invoice_Date__c=Date.parse('02/12/2009'), 
        Aircraft_Eligible_Event__c = assEE.Id);
        insert invoice;
        system.debug('Inserted Invoice');

        LeaseWareUtils.clearFromTrigger();    
        try{
            delete invoice;
            undelete invoice;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }

        try{
            delete assEE;
            undelete assEE;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }

    }
    static testMethod void myUnitTest3() {

        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN,Date_of_Manufacture__c = System.today(), Aircraft_Type__c='737', Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available');
        insert a;
        
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='test', Aircraft_Type__c='737-300',
         Aircraft_Engine__c='737-300 @ CFM56', Engine_Model__c ='CFM56', Attached_Aircraft__c= a.Id,
         Description__c ='testtt', TSN__c=0, CSN__c=0,  Type__c ='Engine 1', Serial_Number__c='123',
         Engine_Thrust__c='-3B1');
        insert consttAssembly;
        
	    LeaseWareUtils.clearFromTrigger();
	    system.debug('Inserted CA'); 
        List<Maintenance_Program_Event__c> listMPE =  [select id from Maintenance_Program_Event__c where Maintenance_Program__r.Name like 'TestSeededMPN%'];
        
        List<Assembly_Event_Info__c> listAssemblyVar= [Select Assembly_Type__c, Event_Type__c from Assembly_Event_Info__c where Maintenance_Program_Event__c in :listMPE];

        map<string,string> mapprojevnt = new map<string,string> ();
        for(Assembly_Event_Info__c curProjEvent: listAssemblyVar) {
            mapprojevnt.put(curProjEvent.Assembly_Type__c, curProjEvent.Event_Type__c);
        }

        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');  
        Assembly_Eligible_Event__c aEE= new Assembly_Eligible_Event__c( Event_Type__c =mapprojevnt.get(consttAssembly.Type__c),Constituent_Assembly__c=consttAssembly.Id, Start_Date__c=dateStart, End_Date__c=dateEnd, TSN__c = 100, CSN__c = 100);
        insert aEE;

        LeaseWareUtils.clearFromTrigger();
	    
        Test.startTest();
        
	    LeaseWareUtils.clearFromTrigger();
        //Insert, update and delete URs to cover CMR updates
        Utilization_Report__c testURForCMR = new Utilization_Report__c(Name= 'thisURForCMR', Type__c = 'Actual',Aircraft__c=a.Id, FH_String__c='0', Airframe_Cycles_Landing_During_Month__c=0, Month_Ending__c=Date.parse('05/31/2008'), MR_20_Month_C_Check__c=1, Maintenance_Reserve_Engine_1__c=1, Maintenance_Reserve_Engine_2__c=1, Maintenance_Reserve_Engine_3__c=1, Maintenance_Reserve_Engine_4__c=1, Maintenance_Reserve_Engine_1_LLP__c=1, Maintenance_Reserve_Engine_2_LLP__c=2, Maintenance_Reserve_Engine_3_LLP__c=3, Maintenance_Reserve_Engine_4_LLP__c=4, Maintenance_Reserve_APU__c=1, Status__c='Open');
        try{
        insert testURForCMR;
        }catch(exception e){
            System.Debug('Expected exception');
        }
		system.debug('Inserted UR');  

//      Utilization_Report_List_Item__c AsmblyUR = [select id from Utilization_Report_List_Item__c limit 1];
//      try{
//          delete AsmblyUR;
//          undelete AsmblyUR;
//      }catch(Exception e){
            //system.debug('Exception '+ e);
//      }   
		        
	    LeaseWareUtils.clearFromTrigger();
        try{
            delete testURForCMR;
            undelete testURForCMR;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }
        
        LeaseWareUtils.clearFromTrigger();
        Scenario__c scen = new Scenario__c(Aircraft__c=a.id, C_Check_Month_Intervals__c='6', Custom_Months__c=6, Heavy_1_Maintenance_Interval_Months__c='6', 
                                Heavy_2_Maintenance_Interval_Months__c='6', Annual_Discount_Rate__c=3.5, Perpetuity_Growth_Rate__c=3.0,
                                Average_C_Check_Cost_Calculation__c='User Defined (Below)', User_Defined_C_Check_Cost__c=100); 
        try{
            insert scen;
            delete scen;
            undelete scen;
        }catch(Exception  e){        
            system.debug('Scenario - Exception '+e);
        }
                   
        try{
            delete aEE;
            undelete aEE;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }
            
        try{
            delete consttAssembly;
            undelete consttAssembly;
        }catch(Exception  e){        
            system.debug('Exception '+e);
        }
        
        Test.stopTest();

    }
}