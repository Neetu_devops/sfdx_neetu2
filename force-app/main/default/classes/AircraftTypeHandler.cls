public class AircraftTypeHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'AircraftTypeHandlerBefore';
    private final string triggerAfter = 'AircraftTypeHandlerAfter';
    public AircraftTypeHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('AircraftTypeHandler.beforeInsert(+)');

        system.debug('AircraftTypeHandler.beforeInsert(-)');    
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AircraftTypeHandler.beforeUpdate(+)');
        
        
        system.debug('AircraftTypeHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AircraftTypeHandler.beforeDelete(+)');
        
        
        system.debug('AircraftTypeHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {

        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftTypeHandler.afterInsert(+)');

        system.debug('AircraftTypeHandler.afterInsert(-)');         
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftTypeHandler.afterUpdate(+)');
        
        Aircraft_Type__c[] listNew = (list<Aircraft_Type__c>)trigger.new ;
        map<id, Aircraft_Type__c> mapATs = new map<id, Aircraft_Type__c>();
        
        for(Aircraft_Type__c curAT: listNew){
            mapATs.put(curAT.id, curAT);
        }
        
        list<Aircraft_Type_Operator__c>  listTypeOps = [select id, No_Of_Aircraft_In_Service__c, Aircraft_Type__c   
            from Aircraft_Type_Operator__c where Aircraft_Type__c in :listNew];
        for(Aircraft_Type_Operator__c curTypeOp: listTypeOps){
            curTypeOp.Consider_For_Campaigns__c=false;
            if(curTypeOp.No_Of_Aircraft_In_Service__c >= mapATs.get(curTypeOp.Aircraft_Type__c).Fleet_Size_To_Consider__c){
                curTypeOp.Consider_For_Campaigns__c=true;
            }

        }
        if(listTypeOps.size()>0)update listTypeOps;

        
        
        system.debug('AircraftTypeHandler.afterUpdate(-)');         
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftTypeHandler.afterDelete(+)');
        
        
        system.debug('AircraftTypeHandler.afterDelete(-)');         
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftTypeHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('AircraftTypeHandler.afterUnDelete(-)');       
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

  
}