public class ShortVersionFH_FC_Controller {
	@AuraEnabled
    public static UtilizationWrapper getUtilizationData(String recordId){
        
        String nameSpacePrefix = LeaseWareUtils.getNameSpacePrefix();
        if(ID.valueOf(recordId).getSobjectType().getDescribe().getName() == nameSpacePrefix+'Utilization_Report__c'){
            recordId = recordId;
        }
        else{
            if(ID.valueOf(recordId).getSobjectType().getDescribe().getName() == 'ProcessInstanceWorkitem'){
               recordId = [SELECT Id, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem where Id=:recordId][0].ProcessInstance.TargetObjectId;
            }
            else{
                recordId = [SELECT Id, ProcessInstance.TargetObjectId FROM ProcessInstanceStep where Id =: recordId][0].ProcessInstance.TargetObjectId;
            }
        }
        UtilizationWrapper uw = new UtilizationWrapper();
        
        if(ID.valueOf(recordId).getSobjectType().getDescribe().getName() == nameSpacePrefix+'Utilization_Report__c'){
            Utilization_Report__c ur = [Select Id, Name, Aircraft__c, Aircraft__r.MSN_Number__c,Aircraft__r.Name from Utilization_Report__c where Id=:recordId];
            
            List<Assembly_Utilization__c> assemblyUtilizationList = [SELECT Id, Name, Utilization_Report__c, For_The_Month_Ending__c, Constituent_Assembly__c,
                                                                     Constituent_Assembly__r.Type__c,Constituent_Assembly__r.Serial_Number__c,
                                                                     Report_Type_F__c, Type_F__c, Status_F__c, Comment__c, Running_Hours_During_Month__c, 
                                                                     Cycles_During_Month__c, TSN_At_Month_End__c, CSN_At_Month_End__c, FH_FC_Ratio__c, 
                                                                     APU_Hours__c, APU_Hours_At_Month_Start__c, APU_Hours_At_Month_End_F__c, APU_Cycles__c, 
                                                                     APU_Cycles_At_Month_Start__c, APU_Cycles_At_Month_End_F__c, Derate__c, APU_Cycles_At_Month_Start_F__c,
                                                                     APU_Hours_At_Month_Start_F__c, Y_Hidden_APU_FH_String__c, Y_Hidden_Running_Hours__c, 
                                                                     Y_Hidden_Running_Cycles__c, Unknown_FH_FC__c FROM Assembly_Utilization__c
                                                                     where Utilization_Report__c =: ur.Id]; 
            
            
            uw.Utilization = ur;
            if(assemblyUtilizationList.size()>0){
                uw.assemblyUtilizationList = assemblyUtilizationList;
                for(Assembly_Utilization__c assemblyUtilization : assemblyUtilizationList){
                    Decimal flightH = assemblyUtilization.Running_Hours_During_Month__c;
                    Decimal flightC = assemblyUtilization.Cycles_During_Month__c;
                    Decimal ratio;
                    if(flightH != null && (flightC !=null && flightC != 0)) {
                        ratio = flightH/flightC; 
                    }
                    if(assemblyUtilization.Constituent_Assembly__r.Type__c != null){
                        if(assemblyUtilization.Constituent_Assembly__r.Type__c.contains('Airframe')){
                            uw.totalFH = flightH == null ? 0 : flightH ;
                            
                            if(flightH != null) {
                                Integer min = (Integer)((flightH - Math.floor(flightH)) * 60);
                                uw.totalFHM = Math.floor(flightH) + ':' + (min > 10 ? min : 0) + String.valueOf(min);
                            }
                            else {
                                uw.totalFHM = '0:00';
                            }
                            uw.totalFC = flightC == null ? 0 : flightC ;
                            uw.totalRatio = ratio == null ? 0 : ratio ;
                            uw.totalTSN = assemblyUtilization.TSN_At_Month_End__c;
                            uw.totalCSN = assemblyUtilization.CSN_At_Month_End__c;
                        }
                    }
                }
            }
        }
        else{
            uw.errorMsg = 'Please put the component on Approval history record with Utilization as Target Object';
        }
        return uw;
    }
    public class UtilizationWrapper{
        @AuraEnabled public String errorMsg{get;set;}
        @AuraEnabled public String totalFHM{get;set;}
        @AuraEnabled public Decimal totalFC{get;set;}
        @AuraEnabled public Decimal totalFH{get;set;}
        @AuraEnabled public Decimal totalRatio{get;set;}
        @AuraEnabled public Decimal totalTSN{get;set;}
        @AuraEnabled public Decimal totalCSN{get;set;}
        @AuraEnabled public List<Assembly_Utilization__c> assemblyUtilizationList{get;set;}
        @AuraEnabled public Utilization_Report__c Utilization{get;set;}
        
        public UtilizationWrapper(){
            this.errorMsg ='';
            this.assemblyUtilizationList = new List<Assembly_Utilization__c>();
        }
    }
}