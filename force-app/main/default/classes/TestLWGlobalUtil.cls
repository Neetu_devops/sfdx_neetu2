/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *     
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
private class TestLWGlobalUtil {

    @isTest(seeAllData=true)
    static void myUnitTest() {

        LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
        if(lwSettings==null)lwSettings = new LeaseWorksSettings__c(SetupOwnerId=System.UserInfo.getUserId());
        lwSettings.Disable_All_Triggers__c=true;
        upsert lwSettings;


        LWGlobalUtils.setTriggersOff();
        LWGlobalUtils.setTriggersOn();
        LWGlobalUtils.setWFsOff();
        LWGlobalUtils.setWFsOn();

        LWGlobalUtils.getZ_FieldsWithReadPerm();
        LWGlobalUtils.copyLeaseStartEndDates();
        LWGlobalUtils.setRatioInfo('a0kG0000005tB2g');// dummy call
        
        LWGlobalUtils.emailZ_FieldsWithReadPerm('bobmatlw@gmail.com');
        
        LWGlobalUtils.CopyLLPDescToName();
        Assembly_MR_Rate__c[] listAMR = [select id from Assembly_MR_Rate__c where Assembly_Event_Info__r.Event_Type__c like '%LLP%'];
        LWGlobalUtils.UpdateStackCostPerCycle(listAMR[0].Id);
        LWGlobalUtils.setTriggersflagOn();
        LWGlobalUtils.setTriggersflagOff();
        LWGlobalUtils.isTriggerDisabled();
        LWGlobalUtils.createLWAdminPublicGroup();
        
    }

    @isTest(seeAllData=true)
    static void myUnitTest2() {
            
        Aircraft__c newAC = new Aircraft__c(Name='Dummy', Aircraft_Type__c='B737', MSN_Number__c='1111', Date_of_Manufacture__c=Date.today().addYears(-1), TSN__c=0, CSN__c=0);
        insert newAC;
        newAC = new Aircraft__c(Name='Dummy', Aircraft_Type__c='B747', MSN_Number__c='1112', Date_of_Manufacture__c=Date.today().addYears(-1), TSN__c=0, CSN__c=0);
        insert newAC;
        
        LWGlobalUtils.UpdateAllB7xTo7x();
        LWGlobalUtils.setBodyTypeAll();
        LWGlobalUtils.setAircraftID();
        
        LWGlobalUtils.setPlacementByDateOnLeases();
        LWGlobalUtils.setExternalIdOnWF();
        LWGlobalUtils.setMatchingOpsOnWF([select id from operator__c limit 1].id);
        LWGlobalUtils.createAircraftDS(newAC.Id);
        
        Constituent_Assembly__c[] caList =[select id,Attached_Aircraft__r.lease__c from Constituent_Assembly__c where name like 'TestSeededMSN1-E'];
        list<id> listCAIds = new list<id>();
        for(Constituent_Assembly__c curRec :caList){
        	listCAIds.add(curRec.Id);
        }
        LWGlobalUtils.setLowestLimiterOnCA(listCAIds);
	        Test.startTest();
	        try{
		        
	        	LWGlobalUtils.UpdateWorldFleetReferences();
	        }catch(System.DmlException e){
	        	system.debug('Expected - Ignore');
	        }
	        
	        Test.stopTest();        

        LWGlobalUtils.PopulateTopup(caList[0].Attached_Aircraft__r.lease__c);

    }
    
    

    
    @isTest(seeAllData=true)
    static void testLWUtils() {
        //leasewareUtils.getEffAdjFactor([select id from ratio_table__c limit 1][0].id, 2.5, 0, '',null,null);
        //leasewareUtils.getEffAdjFactor([select id from ratio_table__c limit 1][0].id, 1, 0, 'Lower Limit',null,null);
        //leasewareUtils.getEffAdjFactor([select id from ratio_table__c limit 1][0].id, 1.5, 0, 'Upper Limit',null,null);
        
        id leaseId = [select id from lease__c limit 1].id;
        set<string> setIds = new set<string>{leaseid};
        //leasewareUtils.followRecord(setIds);
        leasewareUtils.updateLatestPaymentOnLeaseFuture(setIds);
        //leasewareUtils.InsertAuditRecs(leaseId);
        //leasewareUtils.InsertRentRecs(leaseId);



        Id acID =[select id from aircraft__c limit 1].id;


        LeaseWareUtils.updateWFMatchingOps((new map<id, global_fleet__c>([select id from global_fleet__c limit 5]).keySet()), [select id from operator__c limit 1].id);
        
        
        string strMsg='';
            LWGlobalUtils.SeedAircraftMaintenanceEventLookup();
            LWGlobalUtils.SeedThrustSettingLookup();
        try{
            LWGlobalUtils.TSUptakeScript();

        }catch(exception e){
            strMsg+=('<BR>*** Exception ' +e.getMessage()+ ' from ' + e.getStackTraceString());
        }
        system.debug(strMsg);
        strMsg='';
        try{

            LWGlobalUtils.MPEAssemblyScript();

        }catch(exception e){
            strMsg+=('<BR>*** Exception ' +e.getMessage()+ ' from ' + e.getStackTraceString());
        }
        system.debug(strMsg);
        strMsg='';
        try{

            LWGlobalUtils.UpdateNextEventAC();

        }catch(exception e){
            strMsg+=('<BR>*** Exception ' +e.getMessage()+ ' from ' + e.getStackTraceString());
        }
        system.debug(strMsg);       
        strMsg='';
        try{

            LWGlobalUtils.UpdateMPEValueOnAC();

        }catch(exception e){
            strMsg+=('<BR>*** Exception ' +e.getMessage()+ ' from ' + e.getStackTraceString());
        }
        system.debug(strMsg);
        strMsg='';
        try{

            LWGlobalUtils.UpdateNextEventCA();

        }catch(exception e){
            strMsg+=('<BR>*** Exception ' +e.getMessage()+ ' from ' + e.getStackTraceString());
        }
        system.debug(strMsg);
        strMsg='';
        try{

            LWGlobalUtils.MPELGDataScript();
        }catch(exception e){
            strMsg+=('<BR>*** Exception ' +e.getMessage()+ ' from ' + e.getStackTraceString());
        }
        system.debug(strMsg);
                                        

    }
    

    //created this testcase to cross 75% coverage : anjani 
    static testMethod void TestcreateExceptionLog( ) 
    {
        try{
            Integer int1 = 25/0;
        }catch(Exception e){
            LeasewareUtils.createExceptionLog(e,'TestMsg','TestObject','TestRecIds',null,true);
        }
                
    }  
    

    
    @isTest(seeAllData=true)
    static void TestGenerateSnapshots( ) 
    {
        Lease__c[] l1 = [select id from Lease__c];
        
        try{
            if(l1.size()<4) LWGlobalUtils.GenerateSnapshots(l1[0].Id);
            else LWGlobalUtils.GenerateSnapshots('a0SG000000M71Eb');
        }catch(Exception e){
            
        }
                
    }        

    @isTest(seeAllData=true)
    static void TestMiscMethods() 
    {
        try{
            LWGlobalUtils.getNearestWorkDay(Date.Parse('1/3/2016'), [select id from operator__c 
                where Rent_Payments_Before_Holidays__c=true AND Country_Lookup__r.Name='India'
                limit 1].id);
        }catch(Exception e){
            
        }
        try{
            LWGlobalUtils.getNearestWorkDay(Date.Parse('1/3/2016'), [select id from operator__c 
                where Rent_Payments_Before_Holidays__c=false AND Country_Lookup__r.Name='India'
                limit 1].id);
        }catch(Exception e){
            
        }

	    Lease__c lease1 = [select id,name from Lease__c where name like '%TestSeededMSN1%' limit 1];    
	    lease1.Anticipated_Remarketing_Status__c = 'Placement';
	    update lease1;

        Deal__c myDeal = new Deal__c(Name = 'Test Deal');
        insert myDeal;	

		ID LeaseID = lease1.id;
        LWGlobalUtils.CreateCampaigns(LeaseID);
        LWGlobalUtils.DeleteProspects(myDeal.id);
        LWGlobalUtils.setMatchingOpsOnWFAll();
        LWGlobalUtils.CreateAuditSchedule(LeaseID);
        LWGlobalUtils.CreateInterestSchedule(LeaseID);

    }
    
    static testMethod void TestggetIRR() {
        Integer CFPeriod = 96;
        double[] CashFlow = new double[CFPeriod+1];
        Double initAmt= 29240000.00;
        for(Integer i=0;i<=CFPeriod;i++){
            
            cashFlow[i]= 424697;
            if(i==0)cashFlow[i]= -1*initAmt;
            if(i==CFPeriod) cashFlow[i]= 14904697;
        }

        Double irrr1= leasewareUtils.ggetIRR( cashFlow,0)*10000 ;
        system.debug('IRR='+ irrr1.round()); 
        System.assertEquals(irrr1.round(),116);         
    }
    
    static testMethod void TestggetPMT1() {
        Decimal PMT= leasewareUtils.calculatePMT1(0.003833333,36,-18.86,12.33).setScale(2);
        system.debug('PMT1='+PMT);     
        System.assertEquals(PMT,0.24);          
    }    
 
    @isTest(seeAllData=true)
    static void getAverageMonthlyTest() {
        system.debug('getAverageMonthlyAssemblyUtilization');
        Id ACID = 'a04G000000FZlFx';
        Date laastURDate = Date.parse('01/01/2015');
        map<id,map<String,object>> returnMap = new map<id,map<String,object>>();
        map<String,object> tempmap;
        leasewareUtils.getAverageMonthlyAssemblyUtilization(ACID,null,laastURDate,returnMap,null,null,true);
        for(Id CAID:returnMap.keySet()){
            system.debug('-----CAID='+CAID);
            tempmap = returnMap.get(CAID);
            system.debug('---------AvgCSN='+tempmap.get('AvgCSN'));  
            system.debug('---------AvgTSN='+tempmap.get('AvgTSN'));
        }       
        
        //Manasa: Commenting since it is not in use.
        // system.debug('getAverageMonthlyUtilization');
        // map<String,object> returnMapAC = new map<String,object>();
        // leasewareUtils.getAverageMonthlyUtilization(ACID,laastURDate,returnMapAC,null,null,true);
        // system.debug('---------AvgCSN='+returnMapAC.get('AvgCSN'));  
        // system.debug('---------AvgTSN='+returnMapAC.get('AvgTSN'));
    
        
    }      
    
    static testMethod void gettestCoverageATMO(){
        Aircraft_Type__c AT1= new Aircraft_Type__c(Name='Dummy',Aircraft_Type__c = 'A320');
        insert AT1;
        Aircraft_Type_Map__c ATM1 = new Aircraft_Type_Map__c(Name='Dummy',Aircraft_Type__c = AT1.Id,Type__c = 'A320');
        insert ATM1;
        
     Operator__c op1 = new Operator__c( Name = 'Oper' , Status__c = 'Approved' , 
                                                //Region__c = region ,
                                                Revenue__c = 100.00 ,
                                                Employees__c = 100 );    
                                                    
        insert op1;
        Aircraft_Type_Map_Operator__c ATMO1 = new Aircraft_Type_Map_Operator__c(Name='Dummy',Aircraft_Type_Map__c = ATM1.Id,Operator__c =op1.Id );
        insert ATMO1;
        try{
            ATMO1.No_Of_Aircraft_In_LOI__c= 1;
            update ATMO1;
        }catch(Exception ex){}
        
        Date startDate = Date.Parse('1/3/2016');
                Deal__c deal1 = new Deal__c( Name = 'Dummy',  
                                    Campaign_Active__c = true,
                                    Campaign_Start_Date__c = startDate ,
                                    LOI_Due_By__c = startDate.addDays(10) ,                                 
                                    Placement_By__c = startDate.addDays(12) ,
                                    All_Proposals_Due_By_Date__c = startDate.addDays(9));
                insert deal1 ;
                delete deal1 ;
    }    

    // UR :  Actual , True Up gross ,Manual Credit,
    @isTest(seeAllData=true)
    static void TestUR_UpdateRunningHoursCycles1() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
                

        Date ForMonthEnding = Date.parse('09/30/2013'); // Lease start Date date.newinstance(2013, 8, 17)
        // Actual UR
        Utilization_Report__c URActual = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URActual;
        Utilization_Report__c[] URListActual = [select id,status__c from Utilization_Report__c where Type__c = 'Actual' and aircraft__c = :AC1.Id];
            URListActual[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListActual[0];   
        
         
             
        
        // True Up Gross UR
        Utilization_Report__c URTUG = new Utilization_Report__c(
            Name= 'True Up Gross', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='130',Type__c = 'True Up Gross', 
             Airframe_Cycles_Landing_During_Month__c=140, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URTUG;
        Test.startTest();
        Utilization_Report__c[] URListURTUG = [select id,status__c from Utilization_Report__c where Type__c = 'True Up Gross' and aircraft__c = :AC1.Id];
            URListURTUG[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListURTUG[0];    

        //Manual Credit Adj
        Utilization_Report__c URMCA = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='10',Type__c = 'Manual Credits/Adjustments', 
             Airframe_Cycles_Landing_During_Month__c=20, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URMCA;
        Utilization_Report__c[] URListMCA = [select id,status__c from Utilization_Report__c where Type__c = 'Manual Credits/Adjustments' and aircraft__c = :AC1.Id];
            URListMCA[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListMCA[0]; 
            
        Test.stopTest();

        
        LWGlobalUtils.UR_UpdateRunningHoursCycles();
        LWGlobalUtils.UR_UpdateTSN_CSN_MonthStart();
        LWGlobalUtils.UpdateURTsnCsnMonthStart((String)mapInOut.get('Aircraft__c.Id'));    
        LWGlobalUtils.UpdateAssemblyURTsnCsnMonthStart((String)mapInOut.get('Constituent_Assembly__c.Id'));    
    }   


    // UR :  Actual ,Manual Credit, True Up gross
    @isTest(seeAllData=true)
    static void TestUR_UpdateRunningHoursCycles2() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        


        Date ForMonthEnding = Date.parse('09/30/2013'); // Lease start Date date.newinstance(2013, 8, 17)
        // Actual UR
        Utilization_Report__c URActual = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
                     Status__c='Approved By Lessor'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        System.debug('1.Number of Queries used : ' + Limits.getQueries());
        LeaseWareUtils.clearFromTrigger();insert URActual;
         System.debug('2.Number of Queries used : ' + Limits.getQueries());
        
        //Manual Credit Adj
        Utilization_Report__c URMCA = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='10',Type__c = 'Manual Credits/Adjustments', 
             Airframe_Cycles_Landing_During_Month__c=20, Month_Ending__c= ForMonthEnding,  
                     Status__c='Approved By Lessor'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URMCA;
         System.debug('4.Number of Queries used : ' + Limits.getQueries());
        Test.startTest();     
        
        // True Up Gross UR
        Utilization_Report__c URTUG = new Utilization_Report__c(
            Name= 'True Up Gross', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='130',Type__c = 'True Up Gross', 
             Airframe_Cycles_Landing_During_Month__c=140, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
         System.debug('6.Number of Queries used : ' + Limits.getQueries());
        LeaseWareUtils.clearFromTrigger();insert URTUG;
        
        Utilization_Report__c[] URListURTUG = [select id,status__c from Utilization_Report__c where Type__c = 'True Up Gross' and aircraft__c = :AC1.Id];
            URListURTUG[0].status__c = 'Approved By Lessor';
         System.debug('7.Number of Queries used : ' + Limits.getQueries());    
        LeaseWareUtils.clearFromTrigger();update URListURTUG[0];   
             
        Test.stopTest();
         System.debug('8.Number of Queries used : ' + Limits.getQueries());
        LWGlobalUtils.UR_UpdateRunningHoursCycles();
        LWGlobalUtils.UR_UpdateTSN_CSN_MonthStart();
        LWGlobalUtils.UpdateURTsnCsnMonthStart((String)mapInOut.get('Aircraft__c.Id'));    
        LWGlobalUtils.UpdateAssemblyURTsnCsnMonthStart((String)mapInOut.get('Constituent_Assembly__c.Id'));
         System.debug('9.Number of Queries used : ' + Limits.getQueries());     
    }   

    
    // UR : Assumed, Actual , True Up gross
    @isTest(seeAllData=true)
    static void TestUR_UpdateRunningHoursCycles3() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);


        // Assumed UR
        Date ForMonthEnding = Date.parse('09/30/2013'); // Lease start Date date.newinstance(2013, 8, 17)
        Utilization_Report__c URAssumed = new Utilization_Report__c(
            Name= 'Assumed', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='110',Type__c = 'Assumed/Estimated', 
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URAssumed;
        Utilization_Report__c[] URList = [select id,status__c from Utilization_Report__c where id = :URAssumed.Id];
            URList[0].status__c = 'Approved By Lessor';
            
        System.debug('---1.Number of Queries : ' + Limits.getQueries());       
        Test.startTest();            
        LeaseWareUtils.clearFromTrigger();update URList[0];        
        
        // Actual UR
        Utilization_Report__c URActual = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );

        LeaseWareUtils.clearFromTrigger();insert URActual;
        Utilization_Report__c[] URListActual = [select id,status__c from Utilization_Report__c where Type__c = 'Actual' and aircraft__c = :AC1.Id];
            URListActual[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListActual[0];        
        
        // True Up Gross UR
        Utilization_Report__c URTUG = new Utilization_Report__c(
            Name= 'True Up Gross', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='130',Type__c = 'True Up Gross', 
             Airframe_Cycles_Landing_During_Month__c=140, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URTUG;
        Test.stopTest();
        Utilization_Report__c[] URListURTUG = [select id,status__c from Utilization_Report__c where Type__c = 'True Up Gross' and aircraft__c = :AC1.Id];
            URListURTUG[0].status__c = 'Approved By Lessor';
            
        LeaseWareUtils.clearFromTrigger();update URListURTUG[0];        
        
        
        LWGlobalUtils.UR_UpdateRunningHoursCycles();
        LWGlobalUtils.UR_UpdateTSN_CSN_MonthStart();
        LWGlobalUtils.UpdateURTsnCsnMonthStart((String)mapInOut.get('Aircraft__c.Id'));     
        LWGlobalUtils.UpdateAssemblyURTsnCsnMonthStart((String)mapInOut.get('Constituent_Assembly__c.Id'));    
    }   

    
    // UR :  Actual , True Up gross ,Manual Credit,
    @isTest(seeAllData=true)
    static void TestUR_UpdateRunningHoursCycles4() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        

        Date ForMonthEnding = Date.parse('09/30/2013'); // Lease start Date date.newinstance(2013, 8, 17)
        // Actual UR
        Utilization_Report__c URActual = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URActual;
        Utilization_Report__c[] URListActual = [select id,status__c from Utilization_Report__c where Type__c = 'Actual' and aircraft__c = :AC1.Id];
            URListActual[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListActual[0];   
        
        // Actual UR2
        ForMonthEnding = Date.parse('10/31/2013');
        Utilization_Report__c URActual2 = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='200',Type__c = 'Actual', 
             Airframe_Cycles_Landing_During_Month__c=300, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        Test.startTest();
        LeaseWareUtils.clearFromTrigger();insert URActual2;
        ForMonthEnding = Date.parse('09/30/2013');
        Utilization_Report__c[] URListActual2 = [select id,status__c from Utilization_Report__c where Type__c = 'Actual' and Month_Ending__c > :ForMonthEnding  and aircraft__c = :AC1.Id];
            URListActual2[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListActual2[0];           
             
               
        Test.stopTest();
        
        LWGlobalUtils.UR_UpdateRunningHoursCycles();
        LWGlobalUtils.UR_UpdateTSN_CSN_MonthStart();
        LWGlobalUtils.UpdateURTsnCsnMonthStart((String)mapInOut.get('Aircraft__c.Id'));     
        LWGlobalUtils.UpdateAssemblyURTsnCsnMonthStart((String)mapInOut.get('Constituent_Assembly__c.Id'));    
    }   

    // UR :  Actual ,Manual Credit
    @isTest(seeAllData=true)
    static void TestUR_UpdateRunningHoursCycles5() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        

        Date ForMonthEnding = Date.parse('09/30/2013'); // Lease start Date date.newinstance(2013, 8, 17)
        // Actual UR
        Utilization_Report__c URActual = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URActual;
        Utilization_Report__c[] URListActual = [select id,status__c from Utilization_Report__c where Type__c = 'Actual' and aircraft__c = :AC1.Id];
            URListActual[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListActual[0];   
        
        //Manual Credit Adj
        Utilization_Report__c URMCA = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='10',Type__c = 'Manual Credits/Adjustments', 
             Airframe_Cycles_Landing_During_Month__c=20, Month_Ending__c= ForMonthEnding,  
                     Status__c='Open'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        Test.startTest();
        LeaseWareUtils.clearFromTrigger();insert URMCA;
        Utilization_Report__c[] URListMCA = [select id,status__c from Utilization_Report__c where Type__c = 'Manual Credits/Adjustments' and aircraft__c = :AC1.Id];
            URListMCA[0].status__c = 'Approved By Lessor';
        LeaseWareUtils.clearFromTrigger();update URListMCA[0];          
             
        
     
        Test.stopTest();
        
        LWGlobalUtils.UR_UpdateRunningHoursCycles();
        LWGlobalUtils.UR_UpdateTSN_CSN_MonthStart();
        LWGlobalUtils.UpdateURTsnCsnMonthStart((String)mapInOut.get('Aircraft__c.Id'));     
        LWGlobalUtils.UpdateAssemblyURTsnCsnMonthStart((String)mapInOut.get('Constituent_Assembly__c.Id'));    
    }   
    
    @isTest(seeAllData=true)
    static void Test_callGlobalMethod() {

        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];   

    	map<String,Object> mapInOut = new map<String,Object>();
    	
    	mapInOut.put('Maintenance_Program__c.Historical_Utilization_Months__c',10) ;
    	TestLeaseworkUtil.createMP(mapInOut); 
		Id MPId = (Id)mapInOut.get('Maintenance_Program__c.Id');
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Airframe') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Event_Type__c','Heavy 1') ;
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
    	TestLeaseworkUtil.createMPDetail(mapInOut);     	
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Airframe') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Event_Type__c','Heavy 2') ;
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',40);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
    	TestLeaseworkUtil.createMPDetail(mapInOut); 
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Airframe') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Event_Type__c','8C/12Y') ;
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',40);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
    	TestLeaseworkUtil.createMPDetail(mapInOut);
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Airframe') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Event_Type__c','4C/6Y') ;
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',40);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
    	TestLeaseworkUtil.createMPDetail(mapInOut);    	    	 
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Airframe') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Event_Type__c','C Check') ;
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',30);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
    	TestLeaseworkUtil.createMPDetail(mapInOut);  
		AC1.Maintenance_Program__c =  MPId;
        LeaseWareUtils.clearFromTrigger();update AC1; 	
        
        Component_Return_Condition__c RC1 = new Component_Return_Condition__c(Name='RC');
        LeaseWareUtils.clearFromTrigger();insert RC1;
        Lease__c[] leaserec = [select id from Lease__c where aircraft__c =:AC1.Id];
        leaserec[0].Return_Condition__c = RC1.Id;
        LeaseWareUtils.clearFromTrigger();update leaserec[0];


		list<Operator__c> op1 = [select id from Operator__c where name='China Southern Airlines'];
		if(op1.size()<1) op1 = [select id from Operator__c ];
		system.debug('------op1=' + op1[0].id);
		LWGlobalUtils.callGlobalMethod('updateFleetSummary',op1[0].Id);
        
    }

	// for code coverage Payment_Line_Item__c
    @isTest(seeAllData=true)
    static void Test_PaymentLineItemTrigger() {
    	Payment_Line_Item__c[] PIL = [select id from Payment_Line_Item__c];
    	try{
    		LeaseWareUtils.clearFromTrigger();update PIL[0];
    		LeaseWareUtils.clearFromTrigger();delete PIL[0];
    	}catch(Exception ex){
    		
    	}
    }	

	// for code coverage CountePartyNewTrigger, IssuanceTrigger, IssuanceWithInvestorTrigger
    @isTest(seeAllData=true)
    static void Test_CounterPartiesTrigger() {
    	Bank__c b1 = new Bank__c(name='Dummy');
    	try{
    		LeaseWareUtils.clearFromTrigger();insert b1;
    		LeaseWareUtils.clearFromTrigger();update b1;
    		LeaseWareUtils.clearFromTrigger();delete b1;
    	}catch(Exception ex){
    		
    	}
        
    	Issuance__c Issuance1 = new Issuance__c(name='Dummy');
    	try{
    		LeaseWareUtils.clearFromTrigger();insert Issuance1;
    		LeaseWareUtils.clearFromTrigger();update Issuance1;
    		LeaseWareUtils.clearFromTrigger();delete Issuance1;
    	}catch(Exception ex){
    		
    	}
        
    	List_of_Investors__c List_of_Investors1 = new List_of_Investors__c(name='Dummy');
    	try{
    		LeaseWareUtils.clearFromTrigger();insert List_of_Investors1;
    		LeaseWareUtils.clearFromTrigger();update List_of_Investors1;
    		LeaseWareUtils.clearFromTrigger();delete List_of_Investors1;
    	}catch(Exception ex){
    		
    	}        
    }    
    
    
    //Test Method for cutString and setProcessRequestLog Methods.
    @isTest(seeAllData=true)
    static void Test_UtilsMethod() {
    	
    	string res= LeaseWareUtils.cutString('Some Text String ', 0,5);
    	system.debug('res = '+res);
    	
    	
    	Process_Request_Log__c ReqLog =  new Process_Request_Log__c(Status__c ='Success',Type__c = 'Aerdata Service');
    	insert ReqLog;
    	AerDataServiceBatch.setProcessRequestLog('resBody', ReqLog.id, 'File.json', 'File.json');
    	
            
    }  
    
    @isTest(seeAllData=true)
    static void Test_TriggerMethod() {
        
        
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved');
        insert operator;
        
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10',Lease_Start_Date_New__c=Date.parse('01/01/2010'), Lease_End_Date_New__c=Date.parse('12/31/2012'), Rent_Date__c=Date.parse('01/01/2010'), Rent_End_Date__c=Date.parse('12/31/2012'));
        insert lease;
        
       //Floating_Rent_Inputs__c  fri = new Floating_Rent_Inputs__c(Lease__c=lease.Id, Name='FRI');
        //insert fri;
        
    }

    // DepartmentDefaultTrigger
    @isTest(seeAllData=true)
    static void testDepartment_Default(){
        Lessor__c setup = [select id from Lessor__c];
        Department_Default__c dd = new Department_Default__c(name = 'Dummy', Setup__c = setup.Id);
        LeaseWareUtils.clearFromTrigger();insert dd;
        
        LeaseWareUtils.clearFromTrigger();update dd;
        
        LeaseWareUtils.clearFromTrigger();delete dd;
        
    }
    
    // code coverage for InvoiceLineItemTrigger
    @isTest(seeAllData=true)
    static void testInvoiceLineItemTrigger(){
        Invoice_Line_Item__c[] ILList = [select id from Invoice_Line_Item__c];
        try{
            LeaseWareUtils.clearFromTrigger();update ILList[0] ;
        }catch(Exception ex){}
        try{
            Invoice_Line_Item__c ILrec = ILList[0].clone();
            LeaseWareUtils.clearFromTrigger();insert ILrec ;
        }catch(Exception ex){}

        try{
            LeaseWareUtils.clearFromTrigger();delete ILList[0] ;
        }catch(Exception ex){}        
    }    
    
}