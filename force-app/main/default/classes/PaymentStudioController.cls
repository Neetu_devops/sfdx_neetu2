/**
****************************************************************************************************************************************************
* Class: PaymentStudioController
* @author Created by Aman, Lease-Works, 
* @date 11/06/2019
* @version 1.0
* --------------------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to create Paymnet Studio and Payment studio line Items based on the Allocation done 
*	 by the Users.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*   1.0       A.K.G                 11.06.2019      Inital Code for working of Payment Studio
*   1.1       A.K.G                 07.06.2020      Added code for task [https://app.asana.com/0/1117824501069632/1178612462820417/f]
****************************************************************************************************************************************************
*/
public with sharing class PaymentStudioController {
    
    /**
    * @description Load Initail Data to load the Payment Studio Page with default mappings
    *
    * @return InitailLoadPaymentWrapper - Initial Data.
    */
    @AuraEnabled
    public static InitailLoadPaymentWrapper loadInitialLoadData(String recordId) {
        InitailLoadPaymentWrapper ipw;
        if (recordId != null && recordId != '') {
            Payment_Receipt__c[] pr = [SELECT Id, Name, Payment_Amount_Cur__c, Payment_Date__c, Total_Bank_Charges__c, Bank_Account_Lkp__c, Bank_Account_Lkp__r.Name, Approval_Status__c, Description__c, Operator__c, Operator__r.Name, Prepayment__c, Accounting_Period__c, Accounting_Period__r.Name, Lease__c, Lease__r.Name, Source_of_Funds__c FROM Payment_Receipt__c WHERE Id =: recordId];
            
            Payment_Receipt_Line_Item__c[] lineItem = [SELECT Id, Amount_Paid__c, Bank_Charges__c, Invoice__c, Invoice__r.Type__c, Invoice__r.Date_of_MR_Payment_Due__c, Invoice__r.Status__c, Invoice__r.Balance_Due__c, Invoice__r.Lease__r.Legal_Entity__c, Name, Payment_Receipt__c, Bank_Account_Lkp__c, Bank_Account_Lkp__r.Name, Bank_Account_Lkp__r.BA_Legal_Entity__c FROM Payment_Receipt_Line_Item__c WHERE Payment_Receipt__c =: recordId];
            
            if (!pr.isEmpty()) {
                ipw = new InitailLoadPaymentWrapper(pr[0], lineItem, recordId);
            }
        }
        else {
            ipw =  new InitailLoadPaymentWrapper();
        }
        return ipw;
    }
    
    @AuraEnabled
    public static List<LOC_Drawn_Down__c> loadPaymentAmount(String recordId) {
        return [SELECT Id, Amount__c FROM LOC_Drawn_Down__c WHERE (Letter_Of_Credit__c =: recordId AND Locked__c = false) Limit 1];
    }
    
    /**
    * @description Load Initail Data to load the Payment Studio Page with default mappings
    *
    * @param searchInvoiceName - Name of Invoice to filter out matching invoices.
    * @param selectedFilter - Id of Lease or Operator to filter the Invoice records.
    * @param selectedInvoiceType - Type filter for the Invoice.
    * @param allocatedInvoiceIds - list of id's allready allocated to be excluded from search result.
    * @param paymentSource - Source of payment selected by the user.
    *
    * @return List<InvoicePaymentWrapper> - A list of Approved Invoices based on search Filters.
    */
    @AuraEnabled
    public static List<InvoicePaymentWrapper> fetchInvoiceData(String searchInvoiceName, String selectedFilter, String selectedInvoiceType, String allocatedInvoiceIds, String paymentSource) {
        try {
            System.debug('searchInvoiceName: '+searchInvoiceName);
            System.debug('selectedFilter: '+selectedFilter);
            System.debug('selectedInvoiceType: '+selectedInvoiceType);
            System.debug('allocatedInvoiceIds: '+allocatedInvoiceIds);
            System.debug('paymentSource: '+paymentSource);
            
            String nameSpacePrefix = LeaseWareUtils.getNamespacePrefix();
            
            String query = 'SELECT Id, Name, ' + nameSpacePrefix + 'Invoiced_Amount_Formula__c, ' + nameSpacePrefix + 'Lease__c, ' + nameSpacePrefix + 'Lease__r.Name, ' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Operator__c, ' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Operator__r.Name, ' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Legal_Entity__c, ' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Lessor_Bank_For_MR__c, ' + nameSpacePrefix + 'Lease__r.' +
                nameSpacePrefix + 'Lessor_Bank_For_MR__r.Name, ' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Lessor_Bnk_for_Rent__c, ' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Lessor_Bnk_for_Rent__r.Name, ' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Lessor_Bank_For_SD__c, '+ + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Lessor_Bank_For_SD__r.Name, '+ + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Accounting_Setup_P__c, ' 
                + nameSpacePrefix + 'Days_On_Lease__c, ' + nameSpacePrefix + 'Invoice_Date__c, ' + nameSpacePrefix + 'Invoice_Sent__c, ' + nameSpacePrefix + 'Invoice_Type__c, ' + nameSpacePrefix + 'Month_Ending__c, ' + nameSpacePrefix + 'Maintenance_Reserve__c, ' + nameSpacePrefix + 'Prorate__c, ' + nameSpacePrefix + 'Rent_Amount__c, ' +
                nameSpacePrefix + 'Rent__c, ' + nameSpacePrefix + 'Status__c, ' + nameSpacePrefix + 'Type__c, ' + nameSpacePrefix + 'Invoice_Number__c, ' + nameSpacePrefix + 'Date_of_MR_Payment_Due__c, ' + nameSpacePrefix + 'Balance_Due__c, RecordTypeId, RecordType.Name, (SELECT Id, Name, ' + nameSpacePrefix + 'Total_Line_Amount__c, ' + nameSpacePrefix + 'Assembly__c, ' + nameSpacePrefix + 'Assembly__r.Name, ' + nameSpacePrefix + 'Assembly__r.Serial_Number__c, ' + nameSpacePrefix + 'Assembly_MR_Info__r.Assembly_Event_Info__r.Event_Type__c FROM ' + nameSpacePrefix + 'Invoice_Line_Items__r) FROM ' + nameSpacePrefix + 'Invoice__c WHERE ( ' + nameSpacePrefix + 'Status__c = \'Approved\')  And ' + nameSpacePrefix + 'payment_status__c IN (\'Open\',\'Partially Paid\') And '+ nameSpacePrefix + 'Balance_Due__c > 0 ';            
            
            if (selectedFilter != null && selectedFilter != '') {
                String objectName = ID.valueOf(selectedFilter).getSobjectType().getDescribe().getName();
                
                if (objectName == nameSpacePrefix+'Lease__c') {
                    query += ' AND (' + nameSpacePrefix + 'Lease__c =: selectedFilter)';
                }
                else if (objectName == nameSpacePrefix + 'Operator__c') {
                    query += ' AND (' + nameSpacePrefix + 'Lease__r.' + nameSpacePrefix + 'Operator__c =: selectedFilter)';
                }
            }
            if (searchInvoiceName != null && searchInvoiceName != '') {
                query += ' AND (Name like \'%' + searchInvoiceName + '%\')';
            }
            if (selectedInvoiceType != 'All' && selectedInvoiceType != null && selectedInvoiceType != '') {
                query += ' AND (' + nameSpacePrefix + 'Invoice_Type__c =: selectedInvoiceType)';
            }
            if (paymentSource == 'Security Deposit') {
                query += ' AND (' + nameSpacePrefix + 'Invoice_Type__c != \'Security Deposit\')';
            }
            if (allocatedInvoiceIds != null && allocatedInvoiceIds != ''){
                 query += ' AND (NOT Id IN ('+allocatedInvoiceIds+'))';
            }
            query += ' ORDER By ' + nameSpacePrefix + 'Date_of_MR_Payment_Due__c';
            System.debug('query: ' + query);
            
            List<InvoicePaymentWrapper> ipWrapList = new List<InvoicePaymentWrapper>();
            for (Invoice__c inv : Database.Query(query)) {
                ipWrapList.add(new InvoicePaymentWrapper(inv));
            }
            return ipWrapList;
        }
        catch (Exception ex) {
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    /**
    * @description Load Initail Data to load the Payment Studio Page with default mappings
    *
    * @param paymentReceipt_s - Data of Payment studio record to be inserted in the System.
    * @param allocatedInvoice_s - List of Allocated Invoice for Payment Studio Line Items.
    * @param paymentSource - Source of payment selected by the user.
    * @param selectedSourceId - Record Id selected in case Payment Source is not Cash.
    *
    * @return InitailLoadPaymentWrapper - Instance of Wrapper after saving of Payment studio Record.
    */
    @AuraEnabled
    public static InitailLoadPaymentWrapper savePaymentReceipt(String paymentReceipt_s, String allocatedInvoice_s, String paymentSource, String selectedSourceId) {
        try{
            string prefix = leasewareutils.getNamespacePrefix();
            System.debug('paymentReceipt: '+ paymentReceipt_s);
            System.debug('allocatedInvoice: '+ allocatedInvoice_s);
            system.debug('selectedSourceId::'+selectedSourceId);
            InitailLoadPaymentWrapper paymentReceipt = (InitailLoadPaymentWrapper) JSON.deserialize(paymentReceipt_s, InitailLoadPaymentWrapper.class);
            System.debug('paymentReceipt: '+ paymentReceipt);
            List<InvoicePaymentWrapper> allocatedInvoice = (List<InvoicePaymentWrapper>) JSON.deserialize(allocatedInvoice_s, List<InvoicePaymentWrapper>.class);
            
            Payment_Receipt__c pr = new Payment_Receipt__c();
            if (paymentReceipt != null) {
                Integer count = [SELECT count() FROM Payment_Receipt__c WHERE CreatedDate = THIS_YEAR];
                pr.Name = 'Payment '+System.Today().year()+' - '+(count+1);
                
                if (!paymentReceipt.paymentPrePaymentCKB) {
                    if (paymentSource == 'Letter of Credit') { pr.LOC_Drawn_Down__c = selectedSourceId; }
                    else if (paymentSource == 'MR Claims Payable Invoice') { pr.MR_Claim_Event_Payable__c = selectedSourceId; }
                    else if (paymentSource == 'Security Deposit') { pr.Security_Deposit__c = selectedSourceId; }
                }
                else {
                    pr.Name = 'Prepayment '+System.Today().year()+' - '+(count+1);
                    pr.Prepayment__c = paymentReceipt.paymentPrePaymentCKB;
                    if (paymentReceipt.paymentLessee != null && paymentReceipt.paymentLessee.Id != null) {
                        pr.Operator__c = paymentReceipt.paymentLessee.Id;
                        pr.Operator__r = paymentReceipt.paymentLessee;
                    }
                    if (paymentReceipt.paymentBankAccount != null && paymentReceipt.paymentBankAccount.Id != null) {
                        pr.Bank_Account_Lkp__c = paymentReceipt.paymentBankAccount.Id;
                        pr.Bank_Account_Lkp__r = paymentReceipt.paymentBankAccount;
                    }
                    if (paymentReceipt.paymentAccountingPeriod != null && paymentReceipt.paymentAccountingPeriod.Id != null) {
                        pr.Accounting_Period__c = paymentReceipt.paymentAccountingPeriod.Id;
                        pr.Accounting_Period__r = paymentReceipt.paymentAccountingPeriod;
                    }
                }
                if (paymentReceipt.paymentLease != null && paymentReceipt.paymentLease.Id != null) {
                    pr.Lease__c = paymentReceipt.paymentLease.Id;
                    pr.Lease__r = paymentReceipt.paymentLease;
                }
                pr.Source_of_Funds__c = paymentSource;
                pr.Description__c = paymentReceipt.paymentDesc;
                pr.Payment_Amount_Cur__c = paymentReceipt.paymentAmountReceived;
                pr.Payment_Date__c = paymentReceipt.paymentReceivedDate;
                pr.Approval_Status__c = 'Pending';
                
                insert pr;
            }
            
            list<Payment_Receipt_Line_Item__c> paymentLineItemToInsert = new List<Payment_Receipt_Line_Item__c>();
            if (allocatedInvoice != null && !allocatedInvoice.isEmpty()) {
                for (InvoicePaymentWrapper ipWrap : allocatedInvoice) {
                    
                    Payment_Receipt_Line_Item__c prli = new Payment_Receipt_Line_Item__c();
                    prli.Invoice__c = ipWrap.invoiceRecord.Id;
                    prli.Amount_Paid__c = ipWrap.amountPaid;
                    prli.Bank_Charges__c = ipWrap.bankCharges;
                    prli.Payment_Receipt__c = pr.Id;
                    
                    if (ipWrap.isLeaseBankAC && ipWrap.defaultLeaseBankAC != null && ipWrap.defaultLeaseBankAC.Id != null) {
                        prli.Bank_Account_Lkp__c = ipWrap.defaultLeaseBankAC.Id;
                        prli.Bank_Account_Lkp__r = ipWrap.defaultLeaseBankAC;
                    }
                    else if (!ipWrap.isLeaseBankAC && ipWrap.updatedLeaseBankAC != null && ipWrap.updatedLeaseBankAC.Id != null) {
                        prli.Bank_Account_Lkp__c = ipWrap.updatedLeaseBankAC.Id;
                        prli.Bank_Account_Lkp__r = ipWrap.updatedLeaseBankAC;
                    }
                    
                    paymentLineItemToInsert.add(prli);
                }
            }
            if (!paymentLineItemToInsert.isEmpty()) {
                insert paymentLineItemToInsert;
            }
            return new InitailLoadPaymentWrapper(pr,paymentLineItemToInsert,'');
        }
        catch(Exception ex){
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    /**
    * @description Load Initail Data to load the Payment Studio Page with default mappings
    *
    * @param paymentReceipt_s - Data of Payment studio record to be inserted in the System.
    *
    * @return InitailLoadPaymentWrapper - Instance of Wrapper after updating Payment Studio Record.
    */
    @AuraEnabled
    public static InitailLoadPaymentWrapper updateAccountingPeriodData(String paymentReceipt_s) {
        try {
            InitailLoadPaymentWrapper paymentReceipt = (InitailLoadPaymentWrapper) JSON.deserialize(paymentReceipt_s, InitailLoadPaymentWrapper.class);
            if (paymentReceipt != null && paymentReceipt.paymentReceivedDate != null) {
                List<Calendar_Period__c> accounitngPeriodList = [SELECT Id, Name FROM Calendar_Period__c WHERE Start_Date__c <=: paymentReceipt.paymentReceivedDate AND End_Date__c >=: paymentReceipt.paymentReceivedDate];
                if (!accounitngPeriodList.isEmpty()){
                    paymentReceipt.paymentAccountingPeriod = accounitngPeriodList[0];
                }
                else {
                    paymentReceipt.paymentAccountingPeriod = new Calendar_Period__c();
                }
            }
            return paymentReceipt;
        }
        catch (Exception ex) {
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    /**
    * @description Load Initail Data to load the Payment Studio Page with default mappings
    *
    * @param lesseeId - Id of Lessee to fetch all the related Legal Entity Id's
    *
    * @return Set<String> - A list of unique Legal Entity Id's.
    */
    @AuraEnabled
    public static String fetchRelatedLegalEntities(String lesseeId) {
        try {
            Set<String> response = new Set<String>();
            if (lesseeId != null && lesseeId != '') {
                for (Lease__c lease : [SELECT Id, Name, Legal_Entity__c FROM Lease__c WHERE Legal_Entity__c != null AND (Lessee__c =: lesseeId OR Operator__c =: lesseeId)]) {
                    response.add(lease.Legal_Entity__c);
                }
            }
            return JSON.serialize(response);
        }
        catch (Exception ex) {
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    /**
    * @description InitailLoadPaymentWrapper to hold details related to Payment record and respective invoice records
    *
    */
    public class InitailLoadPaymentWrapper{
        @AuraEnabled public Payment_Receipt__c paymentReceiptRecord {get;set;}
        @AuraEnabled public String paymentName {get;set;}
        @AuraEnabled public Decimal totalBankCharges {get;set;}
        @AuraEnabled public String paymentDesc {get;set;}
        @AuraEnabled public Date paymentReceivedDate {get;set;}
        @AuraEnabled public Account paymentBankAccount {get;set;}
        @AuraEnabled public Decimal paymentAmountReceived {get;set;}
        @AuraEnabled public Decimal paymentAmountToAllocate {get;set;}
        @AuraEnabled public List<Payment_Receipt_Line_Item__c> paymentReceiptLineItems {get;set;}
        @AuraEnabled public String paymentStatus {get;set;}
        @AuraEnabled public List<InvoicePaymentWrapper> paymentReceiptInvoiceList {get;set;}
        @AuraEnabled public Boolean paymentPrePaymentCKB {get;set;}
        @AuraEnabled public Lease__c paymentLease {get;set;}
        @AuraEnabled public Operator__c paymentLessee {get;set;}
        @AuraEnabled public Calendar_Period__c paymentAccountingPeriod {get;set;}
        @AuraEnabled public String paymentSource {get;set;}
        
        public InitailLoadPaymentWrapper(){
            this.paymentReceiptRecord = new Payment_Receipt__c();
            this.paymentName = '';
            this.paymentDesc = '';
            this.paymentReceivedDate = System.today();
            this.paymentBankAccount = new Account();
            this.paymentAmountReceived = 0.0;
            this.paymentAmountToAllocate = 0.0;
            this.paymentStatus = 'Pending';
            this.paymentReceiptLineItems = new list<Payment_Receipt_Line_Item__c>();
            this.paymentReceiptInvoiceList = new List<InvoicePaymentWrapper>();
            this.paymentPrePaymentCKB = false;
            this.paymentLease = new Lease__c();
            this.paymentLessee = new Operator__c();
            this.paymentSource = 'Cash';
            this.paymentAccountingPeriod = new Calendar_Period__c();
            List<Calendar_Period__c> calenderPeriodList = [SELECT Id, Name FROM Calendar_Period__c WHERE Start_Date__c <=: System.today() AND End_Date__c >=: System.today()];
            if(!calenderPeriodList.isEmpty()){
             	this.paymentAccountingPeriod = calenderPeriodList[0];
            }
        }
        
        public InitailLoadPaymentWrapper(Payment_Receipt__c pr, list<Payment_Receipt_Line_Item__c> paymentLineItemList, String recordId){
            if (pr != null) {
                this.paymentReceiptRecord = pr;
                this.totalBankCharges = pr.Total_Bank_Charges__c;
                this.paymentName = pr.Name;
                this.paymentDesc = pr.Description__c;
                this.paymentReceivedDate = pr.Payment_Date__c;
                this.paymentAmountReceived = pr.Payment_Amount_Cur__c;
                this.paymentAmountToAllocate = 0.0;
                this.paymentStatus = pr.Approval_Status__c;
                this.paymentSource = pr.Source_of_Funds__c;
                if (!pr.Prepayment__c) {
                    if (!paymentLineItemList.isEmpty()) {
                        this.paymentReceiptLineItems = paymentLineItemList;
                        this.paymentReceiptInvoiceList = new List<InvoicePaymentWrapper>();
                        Set<Id> invoiceIds = new Set<Id>();
                        
                        for (Payment_Receipt_Line_Item__c prli : paymentLineItemList) {
                            invoiceIds.add(prli.Invoice__c);
                        }
                        
                        for (Invoice__c inv : [SELECT Id, Name, Lease__c, Lease__r.Name, Lease__r.Legal_Entity__c, Lease__r.Lessor_Bank_For_MR__c, Lease__r.Lessor_Bank_For_MR__r.Name, Lease__r.Lessor_Bnk_for_Rent__c, Lease__r.Lessor_Bnk_for_Rent__r.Name, Lease__r.Lessor_Bank_For_SD__c, Lease__r.Lessor_Bank_For_SD__r.Name, Lease__r.Accounting_Setup_P__c, Days_On_Lease__c, Invoice_Date__c, Invoice_Sent__c, Invoice_Type__c, Month_Ending__c, Maintenance_Reserve__c, Prorate__c, Rent_Amount__c, Rent__c, Status__c, Type__c, Invoice_Number__c, Invoiced_Amount_Formula__c, Date_of_MR_Payment_Due__c, Balance_Due__c, RecordTypeId, RecordType.Name FROM Invoice__c WHERE ID IN: invoiceIds]) {
                            for (Payment_Receipt_Line_Item__c prli : paymentLineItemList) {
                                InvoicePaymentWrapper ipWrap = new InvoicePaymentWrapper(inv);
                                if (inv.Id == prli.Invoice__c) {
                                    ipWrap.amountPaid = prli.Amount_Paid__c;
                                    ipWrap.bankCharges = prli.Bank_Charges__c;
                                    //ipWrap.amountRemaining = inv.Balance_Due__c - (ipWrap.amountPaid + ipWrap.bankCharges);
                                    ipWrap.amountRemaining = inv.Balance_Due__c - ipWrap.amountPaid;
                                    
                                    if (prli.Bank_Account_Lkp__c != null && ipWrap.defaultLeaseBankAC.Id != prli.Bank_Account_Lkp__c) {
                                        ipWrap.isLeaseBankAC = false;
                                        ipWrap.updatedLeaseBankAC = prli.Bank_Account_Lkp__r;
                                    }
                                    this.paymentReceiptInvoiceList.add(ipWrap);
                                }
                            }
                        }
                    }
                }
                else{
                    this.paymentPrePaymentCKB = pr.Prepayment__c;
                    
                    this.paymentLessee = new Operator__c(); //LW-AKG 07-07-2020
                    if (pr.Operator__c != null) {
                        this.paymentLessee = pr.Operator__r;
                    }
                    
                    this.paymentLease = new Lease__c();
                    if (pr.Lease__c != null) {
                        this.paymentLease = pr.Lease__r;
                    }
                    
                    this.paymentBankAccount = new Account();
                    if (pr.Bank_Account_Lkp__c != null) {
                        this.paymentBankAccount = pr.Bank_Account_Lkp__r;
                    }
                    
                    this.paymentAccountingPeriod = new Calendar_Period__c();
                    if (pr.Accounting_Period__c != null) {
                        this.paymentAccountingPeriod = pr.Accounting_Period__r;
                    }
                }
            }
        }
    }
    
    /**
    * @description InvoicePaymentWrapper to hold details related to Invoice record and respective invoice records
    *
    */
    public class InvoicePaymentWrapper{
        @AuraEnabled public Boolean isAllocated {get;set;}
        @AuraEnabled public Invoice__c invoiceRecord {get;set;}
        @AuraEnabled public Decimal amountPaid {get;set;}
        @AuraEnabled public Decimal amountRemaining {get;set;}
        @AuraEnabled public Decimal bankCharges {get;set;}
        @AuraEnabled public boolean isOverPayed {get;set;}
        @AuraEnabled public String overDueStatus {get;set;}
        @AuraEnabled public Boolean isLeaseBankAC {get;set;} //LW-AKG 07-07-2020
        @AuraEnabled public Account defaultLeaseBankAC {get;set;} //LW-AKG 07-07-2020
        @AuraEnabled public Account updatedLeaseBankAC {get;set;} //LW-AKG 07-07-2020
        @AuraEnabled public Boolean isLeaseBankACRequired {get;set;} //LW-AKG 07-07-2020
        @AuraEnabled public String invoiceType {get;set;} //LW-AKG 12-10-2020 for Automation InvoiceType
        
        public InvoicePaymentWrapper(){
            this.isAllocated = false;
            this.invoiceRecord = new Invoice__c();
            this.amountPaid = 0.0;
            this.amountRemaining = 0.0;
            this.bankCharges = 0.0;
            this.overDueStatus = '';
            this.isOverPayed = false;
            this.isLeaseBankAC = false;
            this.defaultLeaseBankAC = new Account();
            this.updatedLeaseBankAC = new Account();
            this.isLeaseBankACRequired = false;
            this.invoiceType = '';
        }
        
        public InvoicePaymentWrapper(Invoice__c invoiceRecord){
            this.isAllocated = false;
            this.invoiceRecord = invoiceRecord;
            this.amountPaid = 0.0;
            this.bankCharges = 0.0;
            this.isOverPayed = false;
            this.isLeaseBankAC = false;
            this.defaultLeaseBankAC = new Account();
            this.updatedLeaseBankAC = new Account();
            this.isLeaseBankACRequired = false;
            
            if (invoiceRecord.Balance_Due__c != null && invoiceRecord.Balance_Due__c < 0) {
                this.isOverPayed = true;
            }
            
            if (this.amountPaid == 0) {
                this.amountRemaining = invoiceRecord.Balance_Due__c;
            }
            else {
                this.amountRemaining = invoiceRecord.Balance_Due__c - this.amountPaid;
            }
            
            this.overDueStatus = '';
            Date startDate = system.today().addDays(-3);
            Date endDate = system.today().addDays(3);
            
            if (invoiceRecord.Date_of_MR_Payment_Due__c != null) {
                if (invoiceRecord.Date_of_MR_Payment_Due__c < startDate) {
                    this.overDueStatus = 'RedDueDate_Style';
                }
                else if (invoiceRecord.Date_of_MR_Payment_Due__c >= startDate && invoiceRecord.Date_of_MR_Payment_Due__c <= endDate) {
                    this.overDueStatus = 'OrangeDueDate_Style';
                }
            }
            //LW-AKG 07-07-2020
            if (invoiceRecord.Invoice_Type__c == 'Aircraft MR' && invoiceRecord.Lease__r.Lessor_Bank_For_MR__c != null) {
                this.isLeaseBankAC = true;
                this.defaultLeaseBankAC = invoiceRecord.Lease__r.Lessor_Bank_For_MR__r;
            }
            else if (invoiceRecord.Invoice_Type__c == 'Rent' && invoiceRecord.Lease__r.Lessor_Bnk_for_Rent__c != null) {
                this.isLeaseBankAC = true;
                this.defaultLeaseBankAC = invoiceRecord.Lease__r.Lessor_Bnk_for_Rent__r;
            }
            else if (invoiceRecord.Invoice_Type__c == 'Security Deposit' && invoiceRecord.Lease__r.Lessor_Bank_For_SD__c != null) {
                this.isLeaseBankAC = true;
                this.defaultLeaseBankAC = invoiceRecord.Lease__r.Lessor_Bank_For_SD__r;
            }
            
            if (invoiceRecord.Lease__c != null && invoiceRecord.Lease__r.Accounting_Setup_P__c != null) {
                this.isLeaseBankACRequired = true;
            }
            //LW-AKG 07-07-2020

            //LW-AKG 12-10-2020
            if (invoiceRecord.Invoice_Type__c != null && invoiceRecord.Invoice_Type__c != '') {
                this.invoiceType = invoiceRecord.Invoice_Type__c.trim().deleteWhitespace();
            }
        }
    }
}