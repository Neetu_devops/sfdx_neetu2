public without sharing class EngineTemplateManagerController {
    
    @AuraEnabled
    public static List<ThrustWrapper> getThrustPicklist(String engineTemplateId) {
        system.debug('getThrustPicklist engineTemplateId: '+engineTemplateId);
        Engine_Template__c[] engineTemplate = [select Model__c from Engine_Template__c where Id =: engineTemplateId];
        String model = '';
        if(engineTemplate != null && engineTemplate.size() > 0 )
            model = engineTemplate[0].Model__c;
        
        Custom_Lookup__c[] thrustList = [select Id, Name from Custom_Lookup__c 
                                         where Lookup_Type__c = 'Engine' and 
                                         (Sub_LookupType__c = :model or Sub_LookupType__c = '') ];
        list<ThrustWrapper> wrapperList = new List<ThrustWrapper>();
        for(Custom_Lookup__c lookup: thrustList) {
            ThrustWrapper wrapper = new ThrustWrapper();
            wrapper.Id = lookup.Id;
            wrapper.Name = lookup.Name;
            wrapperList.add(wrapper);
        }
        return wrapperList;
    }
    
    
    public static List<String> getCatalogPriceForLimit(List<String> limitPNs) {
        List<LLP_Catalog__c> LLPCList = [
            select Part_Number_New__c
            from LLP_Catalog__c 
            where Part_Number_New__c in :limitPNs
        ];
        List<String> catalogPnList = new List<String>();
        if(LLPCList != null && LLPCList.size() > 0) {
            for(LLP_Catalog__c LLPC : LLPCList)
                catalogPnList.add(LLPC.Part_Number_New__c);
        }
        return catalogPnList;
    }
    
    @AuraEnabled
    public static List<LLPLimitWrapper> getLLPLimitData(String llpTemplateId, String catalogYear){
        List<LLP_Limit_Catalog__c> LLPCList = [
            select Id , Name , Part_Number__c, Thrust__c, Thrust__r.Name, Life_Limit_Cycles__c, Source__c 
            from LLP_Limit_Catalog__c 
            where LLP_Template__c =: llpTemplateId 
            and Name =: catalogYear 
            order by Part_Number__c 
        ];
        
        if(LLPCList != null && LLPCList.size() > 0) {
            List<String> partNumberList = new List<string>();
            for(LLP_Limit_Catalog__c LLPC: LLPCList)
                partNumberList.add(LLPC.Part_Number__c);
            List<String> catalogList = getCatalogPriceForLimit(partNumberList);
            
            List<LLPLimitWrapper> wrapperList = new List<LLPLimitWrapper>();
            String partNumber = '';
            for(LLP_Limit_Catalog__c LLPC: LLPCList){
                LLPLimitWrapper wrapper = new LLPLimitWrapper();
                wrapper.Id = LLPC.Id;
                wrapper.Name = LLPC.Name;
                System.debug('getLLPLimitData partNumber '+partNumber + ', wrapper.PartNumber:'+wrapper.PartNumber);
                if(partNumber.equalsIgnoreCase(LLPC.Part_Number__c))
                    wrapper.ShowPartNumber = false;
                else {
                    wrapper.ShowPartNumber = True;
                    partNumber = LLPC.Part_Number__c;
                    if(catalogList != null && catalogList.contains(LLPC.Part_Number__c))
                        wrapper.hasCatalogPrice = true;
                    else
                        wrapper.hasCatalogPrice = false;
                }
                wrapper.PartNumber = LLPC.Part_Number__c;
                System.debug('getLLPLimitData partNumber '+partNumber + ', wrapper.Name:'+wrapper.Name);
                wrapper.ThrustId = LLPC.Thrust__c;
                wrapper.ThrustName = LLPC.Thrust__r.Name;
                wrapper.LifeLimitCycles = LLPC.Life_Limit_Cycles__c;
                wrapper.Source = LLPC.Source__c;
                
                wrapperList.add(wrapper);
            }
            return wrapperList;
        }
        return null;
    }
    
    @AuraEnabled    
    public static void updateLLDLimitData(String dataStr, String newDataStr, String LLPTemplateId, String catalogYear) {
        System.debug('updateLLDLimitData dataStr: '+dataStr + ' newDataStr: ' +newDataStr );
        List<LLPLimitWrapper> wrapperList = new List<LLPLimitWrapper>();
        wrapperList = (List<LLPLimitWrapper>)JSON.deserialize(dataStr, List<LLPLimitWrapper>.class);                                                     
        System.debug('updateLLDLimitData wrapperList: '+wrapperList);

        List<LLP_Limit_Catalog__c> LLPCList = null;
        if(wrapperList != null) {        
            List<String> idList = new List<String>();
            for(LLPLimitWrapper wrapper : wrapperList){
                idList.add(wrapper.Id);
            }
            LLPCList = [
                select Id , Name , Part_Number__c, Thrust__c, Thrust__r.Name, Life_Limit_Cycles__c, Source__c 
                from LLP_Limit_Catalog__c 
                where Id in :idList
            ];
            
            for(LLPLimitWrapper wrapper : wrapperList){
                for(LLP_Limit_Catalog__c LLPC : LLPCList) {
                    if(LLPC.Id == wrapper.Id) {
                        LLPC.Part_Number__c = wrapper.PartNumber;
                        LLPC.Thrust__c = wrapper.ThrustId;
                        LLPC.Life_Limit_Cycles__c = wrapper.LifeLimitCycles;
                        LLPC.Source__c = wrapper.Source;
                    }
                }
            }
        }
        Savepoint sp = Database.setSavepoint();
        try{
            checkLLPLimitAlreadyExists(dataStr, newDataStr);
            if(LLPCList != null)
            	update LLPCList;
            if(newDataStr != null) {
                addNewLLPLimit(newDataStr, LLPTemplateId, catalogYear);      
            }  
        } catch(Exception e){
            Database.rollback(sp);
            System.debug('updateLLDLimitData Exception '+e);
            throw new AuraHandledException(Utility.getErrorMessage(e));
        }
    }
    
    public static void addNewLLPLimit(String newDataStr, String LLPTemplateId, String catalogYear){
        List<LLPLimitWrapper> newWrapperList;
        if(newDataStr != null) {
            newWrapperList = new List<LLPLimitWrapper>();
            newWrapperList = (List<LLPLimitWrapper>)JSON.deserialize(newDataStr, List<LLPLimitWrapper>.class); 
            System.debug('addNewLLPLimit wrapperList: '+newWrapperList);    
            if(newWrapperList != null) {
                List<LLP_Limit_Catalog__c> LLPCList = new List<LLP_Limit_Catalog__c>();
                for(LLPLimitWrapper wrapper : newWrapperList){
                    LLP_Limit_Catalog__c LLPC = new LLP_Limit_Catalog__c();
                    LLPC.Name = catalogYear;
                    LLPC.Part_Number__c = wrapper.PartNumber;
                    LLPC.Thrust__c = wrapper.ThrustId;
                    LLPC.Life_Limit_Cycles__c = wrapper.LifeLimitCycles;
                    LLPC.Source__c = wrapper.Source;
                    LLPC.LLP_Template__c = LLPTemplateId;
                    LLPCList.add(LLPC);
                }
                insert LLPCList;
            }
        }
    }
    
    public static void checkLLPLimitAlreadyExists(String dataStr, String newDataStr){
        List<LLPLimitWrapper> wrapperList = new List<LLPLimitWrapper>();
        if(dataStr != null)
            wrapperList = (List<LLPLimitWrapper>)JSON.deserialize(dataStr, List<LLPLimitWrapper>.class);
        List<LLPLimitWrapper> newWrapperList = new List<LLPLimitWrapper>();
        if(newDataStr != null)
        	newWrapperList = (List<LLPLimitWrapper>)JSON.deserialize(newDataStr, List<LLPLimitWrapper>.class); 
        List<LLPLimitWrapper> totalList = new List<LLPLimitWrapper>();
        if(wrapperList != null && wrapperList.size() > 0)
            totalList.addAll(wrapperList);
        if(newWrapperList != null && newWrapperList.size() > 0)
            totalList.addAll(newWrapperList);
        if(totalList != null && totalList.size() > 0) {
            for(integer i = 0; i < totalList.size(); i++){
                LLPLimitWrapper outerWrapper = totalList[i];
                for(integer j = i + 1; j < totalList.size(); j++){
					LLPLimitWrapper wrapper = totalList[j]; 
                    if(outerWrapper.PartNumber.equalsIgnoreCase(wrapper.PartNumber) && 
                       outerWrapper.ThrustId.equalsIgnoreCase(wrapper.ThrustId)) {
                        //throw exception
                           String m = 'Duplicate part number and Thrust found';
                           AuraHandledException e = new AuraHandledException(m);
                           e.setMessage(m);
                           throw e;
                    }
                }
            }
        }
        
    }
    
    @AuraEnabled
    public static void deleteLLPLimit(String llpcId){
        LLP_Limit_Catalog__c LLPTemp = [
            select Id, Name 
            from LLP_Limit_Catalog__c
            where Id =: llpcId
        ];
        delete LLPTemp;
    }
    
    @AuraEnabled
    public static List<String> getLLPCatalogYearTemplate(String llpTemplateId){
        String currentYear = String.valueOf(System.Today().year());
        List<AggregateResult> LLPCList = [
            select Name 
            from LLP_Catalog__c 
            where LLP_Template__c =: llpTemplateId 
            Group by Name
        ];
        System.debug('getLLPCatalogYearTemplate LLPCList: '+LLPCList);
        List<AggregateResult> LLPLCList = [
            select Name 
            from LLP_Limit_Catalog__c 
            where LLP_Template__c =: llpTemplateId 
            Group by Name
        ];
        System.debug('getLLPCatalogYearTemplate LLPLCList: '+LLPLCList);
        List<String> yearList = new List<String>();
        if(LLPCList != null && LLPCList.size() > 0) {
            for(AggregateResult LLPC: LLPCList) {
                yearList.add((String)LLPC.get('Name'));
            }
        }
        if(LLPLCList != null && LLPLCList.size() > 0) {
            for(AggregateResult LLPLC: LLPLCList) {
                String year = (String)LLPLC.get('Name');
                if(!yearList.contains(year))
                	yearList.add((String)LLPLC.get('Name'));
            }
        }
        
        if(!yearList.contains(currentYear))
            yearList.add(currentYear);
        yearList.sort();
        System.debug('getLLPCatalogYearTemplate yearList: '+yearList);
            
        return yearList;
    }
    
    @AuraEnabled
    public static List<LLPCatalogWrapper> getLLPCatalogData(String llpTemplateId, String currentYear){
        //Integer currentYear = System.Today().year();
        List<LLP_Catalog__c> LLPCList = [
            select Id , Name , Part_Number_New__c, Catalog_Price__c, Is_Default_Partnumber__c, Lead_Time_Days__c 
            from LLP_Catalog__c 
            where LLP_Template__c =: llpTemplateId 
            and Name =: currentYear
        ];
        
        if(LLPCList != null && LLPCList.size() > 0) {
            List<LLPCatalogWrapper> wrapperList = new List<LLPCatalogWrapper>();
            for(LLP_Catalog__c LLPC: LLPCList){
                LLPCatalogWrapper wrapper = new LLPCatalogWrapper();
                wrapper.Id = LLPC.Id;
                wrapper.Name = LLPC.Name;
                wrapper.PartNumber = LLPC.Part_Number_New__c;
                wrapper.CatalogPrice = LLPC.Catalog_Price__c;
                wrapper.IsDefaultPrice = LLPC.Is_Default_Partnumber__c;
                wrapper.LeadTimeDays = LLPC.Lead_Time_Days__c;
                wrapperList.add(wrapper);
            }
            return wrapperList;
        }
        return null;
    }
    
    @AuraEnabled    
    public static void updateLLDCatalogData(String dataStr, String newDataStr, String LLPTemplateId, String ctYear) {
        System.debug('updateLLDCatalogData dataStr: '+dataStr + ' newDataStr: ' +newDataStr );
        System.debug('updateLLDCatalogData ctYear: '+ctYear);
        List<LLPCatalogWrapper> wrapperList = new List<LLPCatalogWrapper>();
        wrapperList = (List<LLPCatalogWrapper>)JSON.deserialize(dataStr, List<LLPCatalogWrapper>.class);                                                     
        System.debug('updateLLDCatalogData wrapperList: '+wrapperList);
        List<LLP_Catalog__c> LLPCList = null;
        String partNumber = null;
        if(wrapperList != null) {
            List<String> idList = new List<String>();
            for(LLPCatalogWrapper wrapper : wrapperList){
                idList.add(wrapper.Id);
            }
            LLPCList = [
                select Id , Name , Part_Number_New__c, Catalog_Price__c, Is_Default_Partnumber__c, Lead_Time_Days__c 
                from LLP_Catalog__c
                where Id in :idList
            ];
            
            for(LLPCatalogWrapper wrapper : wrapperList){
                for(LLP_Catalog__c LLPC : LLPCList) {
                    if(LLPC.Id == wrapper.Id) {
                        LLPC.Part_Number_New__c = wrapper.PartNumber;
                        LLPC.Catalog_Price__c = wrapper.CatalogPrice;
                        if(wrapper.IsDefaultPrice == true) {
                            if(partNumber != null) {
                                String m = 'Multiple Default PartNumber selected';
                                AuraHandledException e = new AuraHandledException(m);
                                e.setMessage(m);
                                throw e;
                            }
                            else
                                partNumber = wrapper.PartNumber;
                        }
                        //LLPC.Is_Default_Partnumber__c = wrapper.IsDefaultPrice;
                        LLPC.Lead_Time_Days__c = wrapper.LeadTimeDays;
                    }
                }
            }
        }
        Savepoint sp = Database.setSavepoint();
        try{
            checkLLPCatalogAlreadyExists(dataStr,newDataStr);
            if(LLPCList != null)
                update LLPCList;
            
            if(newDataStr != null) {
                addNewLLPCatalog(newDataStr, LLPTemplateId, ctYear, partNumber);      
            }  
            else{
                if(partNumber != null)
                    updateTemplateDefaultPN(LLPTemplateId, partNumber);
            }
        } catch(Exception e){
            Database.rollback(sp);
            System.debug('updateLLDCatalogData Exception '+e);
            throw new AuraHandledException(Utility.getErrorMessage(e));
        }
    }
    
    public static void updateTemplateDefaultPN(String LLPTemplateId, String partNumber) {
        try{
            LLP_Template__c LLPT = [
                select Part_Number__c 
                from LLP_Template__c
                where Id = :LLPTemplateId ];
            LLPT.Part_Number__c = partNumber;
            update LLPT;
        }
        catch(Exception e){
            system.debug('updateTemplateDefaultPN Exception '+e);
        }
    }
    
    public static void addNewLLPCatalog(String newDataStr, String LLPTemplateId, String ctYear, String partNumber){
        List<LLPCatalogWrapper> newWrapperList;
        if(newDataStr != null) {
            newWrapperList = new List<LLPCatalogWrapper>();
            newWrapperList = (List<LLPCatalogWrapper>)JSON.deserialize(newDataStr, List<LLPCatalogWrapper>.class); 
            System.debug('addNewLLP wrapperList: '+newWrapperList );
            System.debug('addNewLLP ctYear: '+ctYear);
            
            if(newWrapperList != null) {
                List<LLP_Catalog__c> LLPCList = new List<LLP_Catalog__c>();
                for(LLPCatalogWrapper wrapper : newWrapperList){
                    LLP_Catalog__c LLPC = new LLP_Catalog__c();
                    LLPC.Name = ctYear;
                    LLPC.Part_Number_New__c = wrapper.PartNumber;
                    LLPC.Catalog_Price__c = wrapper.CatalogPrice;
                    if(wrapper.IsDefaultPrice == true) {
                        if(partNumber != null) {
                            String m = 'Multiple Default PartNumber selected';
                            AuraHandledException e = new AuraHandledException(m);
                            e.setMessage(m);
                            throw e;
                        }
                        else
                            partNumber = wrapper.PartNumber;
                    }
                    //  LLPC.Is_Default_Partnumber__c = wrapper.IsDefaultPrice;
                    LLPC.Lead_Time_Days__c = wrapper.LeadTimeDays;
                    LLPC.LLP_Template__c = LLPTemplateId;
                    LLPCList.add(LLPC);
                }
                insert LLPCList;
                updateTemplateDefaultPN(LLPTemplateId,partNumber);
            }
        }
    }
    
    public static void checkLLPCatalogAlreadyExists(String dataStr, String newDataStr){
        List<LLPCatalogWrapper> wrapperList = new List<LLPCatalogWrapper>();
        if(dataStr != null)
            wrapperList = (List<LLPCatalogWrapper>)JSON.deserialize(dataStr, List<LLPCatalogWrapper>.class);
        List<LLPCatalogWrapper> newWrapperList = new List<LLPCatalogWrapper>();
        if(newDataStr != null)
        	newWrapperList = (List<LLPCatalogWrapper>)JSON.deserialize(newDataStr, List<LLPCatalogWrapper>.class); 
        List<LLPCatalogWrapper> totalList = new List<LLPCatalogWrapper>();
        if(wrapperList != null && wrapperList.size() > 0)
            totalList.addAll(wrapperList);
        if(newWrapperList != null && newWrapperList.size() > 0)
            totalList.addAll(newWrapperList);
        if(totalList != null && totalList.size() > 0) {
            for(integer i = 0; i < totalList.size(); i++){
                LLPCatalogWrapper outerWrapper = totalList[i];
                for(integer j = i + 1; j < totalList.size(); j++){
					LLPCatalogWrapper wrapper = totalList[j]; 
                    if(outerWrapper.PartNumber.equalsIgnoreCase(wrapper.PartNumber)) {
                        //throw exception
                           String m = 'Duplicate part number';
                           AuraHandledException e = new AuraHandledException(m);
                           e.setMessage(m);
                           throw e;
                    }
                }
            }
        }
    }
    
    @AuraEnabled
    public static void deleteLLPCatalog(String llpcId){
        LLP_Catalog__c LLPTemp = [
            select Id, Name 
            from LLP_Catalog__c
            where Id =: llpcId
        ];
        delete LLPTemp;
    }
    
    @AuraEnabled
    public static EngineTemplateWrapper getTemplateBasicDetails(String recordId){
        List<Engine_Template__c> ET = [select Id, Name, Current_Catalog_Year__c from Engine_Template__c where Id =: recordId];
        if(ET != null && ET.size() > 0) {
            EngineTemplateWrapper wrapper = new EngineTemplateWrapper();
            wrapper.Id = ET[0].Id;
            wrapper.Name = ET[0].Name;
            wrapper.CurrentCatalogYear = ET[0].Current_Catalog_Year__c;
            return wrapper;
        }
        return null;
    }
    
    @AuraEnabled
    public static List<LLPTemplateWrapper> getTempData(String recordId){
        List<LLPTemplateWrapper> wrapperList = new List<LLPTemplateWrapper>();
        List<LLP_Template__c> LLPTempList = [
            select Id, Name , IIN__c, Part_Name__c, Module_N__c, 
            Part_Number__c, Life_Limit_Cycles__c , Sorting__c , Replacement_Cost_Current_Catalog_Year__c
            from LLP_Template__c
            where Engine_Template__c =: recordId
            order by Sorting__c
        ];
        for(LLP_Template__c LLPTemp : LLPTempList){
            LLPTemplateWrapper wrapper = new LLPTemplateWrapper();
            wrapper.Id = LLPTemp.Id;
            wrapper.Name = LLPTemp.Name;
            wrapper.IIN = LLPTemp.IIN__c;
            wrapper.LLPDesc = LLPTemp.Part_Name__c;
            wrapper.Module = LLPTemp.Module_N__c;
            wrapper.DefaultPn = LLPTemp.Part_Number__c;
            wrapper.LifeLimit = LLPTemp.Life_Limit_Cycles__c;
            wrapper.Sorting = LLPTemp.Sorting__c;
            try{
                wrapper.CostPerCycle = LLPTemp.Replacement_Cost_Current_Catalog_Year__c / LLPTemp.Life_Limit_Cycles__c;
            }
            catch(Exception e){
                wrapper.CostPerCycle =0;
                System.debug('getTempData Exception '+e);
            } 
            wrapperList.add(wrapper);
        }
        return wrapperList;
    }
    
    @AuraEnabled    
    public static void updateLLDData(String dataStr, String newDataStr, String recordId) {
        System.debug('generateFixedRentSchedule dataStr: '+dataStr + ' newDataStr: ' +newDataStr );
        List<LLPTemplateWrapper> wrapperList = new List<LLPTemplateWrapper>();
        wrapperList = (List<LLPTemplateWrapper>)JSON.deserialize(dataStr, List<LLPTemplateWrapper>.class);                                                     
        System.debug('generateFixedRentSchedule wrapperList: '+wrapperList);
        
        List<LLP_Template__c> LLPTempDbList  = null;
        if(wrapperList != null) {
            List<String> idList = new List<String>();
            for(LLPTemplateWrapper wrapper : wrapperList){
                idList.add(wrapper.Id);
            }
            LLPTempDbList = [
                select Id, Name , IIN__c, Part_Name__c, Module_N__c, 
                Part_Number__c, Life_Limit_Cycles__c , Sorting__c  
                from LLP_Template__c
                where Id in :idList
            ];
            for(LLPTemplateWrapper wrapper : wrapperList){
                for(LLP_Template__c LLPTemp : LLPTempDbList) {
                    if(LLPTemp.Id == wrapper.Id) {
                        LLPTemp.Name = wrapper.Name;
                        LLPTemp.IIN__c = wrapper.IIN;
                        LLPTemp.Part_Name__c = wrapper.LLPDesc;
                        LLPTemp.Module_N__c = wrapper.Module;
                        LLPTemp.Part_Number__c = wrapper.DefaultPn;
                        LLPTemp.Life_Limit_Cycles__c = wrapper.LifeLimit;
                        LLPTemp.Sorting__c = wrapper.Sorting;
                    }
                }
            }
        }
        Savepoint sp = Database.setSavepoint();
        try{
            if(LLPTempDbList != null)
                update LLPTempDbList;
            if(newDataStr != null) {
                addNewLLP(newDataStr, recordId);      
            }  
        } catch(Exception e){
            Database.rollback(sp);
            throw new AuraHandledException(Utility.getErrorMessage(e));
        }
    }
    
    public static void addNewLLP(String newDataStr, String recordId){
        List<LLPTemplateWrapper> newWrapperList;
        if(newDataStr != null) {
            newWrapperList = new List<LLPTemplateWrapper>();
            newWrapperList = (List<LLPTemplateWrapper>)JSON.deserialize(newDataStr, List<LLPTemplateWrapper>.class); 
            System.debug('addNewLLP wrapperList: '+newWrapperList);    
            
            if(newWrapperList != null) {
                List<LLP_Template__c> LLPTempList = new List<LLP_Template__c>();
                for(LLPTemplateWrapper wrapper : newWrapperList){
                    LLP_Template__c LLPTemp = new LLP_Template__c();
                    LLPTemp.Name = wrapper.LLPDesc;
                    LLPTemp.IIN__c = wrapper.IIN;
                    LLPTemp.Part_Name__c = wrapper.LLPDesc;
                    LLPTemp.Module_N__c = wrapper.Module;
                    LLPTemp.Part_Number__c = wrapper.DefaultPn;
                    LLPTemp.Life_Limit_Cycles__c = wrapper.LifeLimit;
                    LLPTemp.Engine_Template__c = recordId;
                    LLPTemp.Sorting__c = wrapper.Sorting;
                    LLPTempList.add(LLPTemp);
                }
                insert LLPTempList;
            }
        }
    }
    
    @AuraEnabled
    public static void deleteLLP(String llpId){
        LLP_Template__c LLPTemp = [
            select Id, Name 
            from LLP_Template__c
            where Id =: llpId
        ];
        delete LLPTemp;
    }
    
    @AuraEnabled
    public static string getNamespacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled
    Public static List<String> getPickListValues(String objectName, String fld) {
        String namespace = getNamespacePrefix();
        objectName = namespace + objectName;
        objectName = objectName.replace(namespace + namespace , namespace);
        
        if(!fld.equals('Name')) {
            fld = namespace + fld;
            fld = fld.replace(namespace + namespace , namespace);
        }
        
        system.debug('objObject --->' + objectName);
        system.debug('fld --->' + fld);
        List <String> allOpts = new list <String>();
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        return allOpts;
    }
    
    public class LLPLimitWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public String PartNumber {get; set;}
        @AuraEnabled public String ThrustId {get; set;}
        @AuraEnabled public String ThrustName {get; set;}
        @AuraEnabled public Decimal LifeLimitCycles {get; set;}
        @AuraEnabled public String Source {get; set;}
        @AuraEnabled public Boolean ShowPartNumber {get; set;}
        @AuraEnabled public Boolean hasCatalogPrice {get; set;}
    }
    
    public class LLPCatalogWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public String PartNumber {get; set;}
        @AuraEnabled public Decimal CatalogPrice {get; set;}
        @AuraEnabled public Boolean IsDefaultPrice {get; set;}
        @AuraEnabled public Decimal LeadTimeDays {get; set;}
        @AuraEnabled public Boolean ShowPartNumber {get; set;}
    }
    
    
    public class LLPTemplateWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public String IIN {get; set;}
        @AuraEnabled public String LLPDesc {get; set;}
        @AuraEnabled public String Module {get; set;}
        @AuraEnabled public String DefaultPn {get; set;}
        @AuraEnabled public Decimal LifeLimit {get; set;}
        @AuraEnabled public Decimal Sorting {get; set;}
        @AuraEnabled public Decimal CostPerCycle {get; set;}
    }
    
    public class EngineTemplateWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public String CurrentCatalogYear {get; set;}
    }
    
    public class ThrustWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
    }
}