/**
*******************************************************************************
* Class: RLUtility
* @author Created by Bhavna, Lease-Works,
* @date 30/05/2019
* @description This class has helper methods.
*
* History:
* - VERSION   DEVELOPER NAME        DATE             DETAIL FEATURES
*
*******************************************************************************
*/
public class RLUtility {
    /**
    * @description provides description of the given object
    *
    * @param objectName - String, object name to be described
    *
    * @return Schema.DescribeSObjectResult, description of the given object
    */
	public static Schema.DescribeSObjectResult describeObject(String objectName) {        
        Schema.DescribeSObjectResult objectDescribe = Schema.getGlobalDescribe().get(objectName).getDescribe();
        
        return objectDescribe;
    }
    
    
	/**
    * @description provides description of the given object
    *
    * @param objectDescribe - Schema.DescribeSObjectResult, description of the object
    * @param fieldSetName - String, FieldSet Name to be described
    *
    * @return Schema.FieldSet, description of the given FieldSet
    */   
    public static Schema.FieldSet describeFieldSet(Schema.DescribeSObjectResult objectDescribe, String fieldSetName) {        
        Map<String, Schema.FieldSet> fieldSetsByName = objectDescribe.FieldSets.getMap();
        Schema.FieldSet fieldSet = fieldSetsByName.get(leasewareutils.getNamespacePrefix() + fieldSetName);
        
        return fieldSet;
    }
    /**
    * @description provides all fields names of the given object's fieldset
    *
    * @param objectName   - String, api name of the object
    * @param fieldSetName - String, FieldSet Name to be described
    *
    * @return Set<String>, set of fields of the given FieldSet
    */ 
    public static Set<String> getAllFieldsFromFieldset(String objectName,String fieldSetName ){
        Schema.DescribeSObjectResult objectDescribe = describeObject(leasewareutils.getNamespacePrefix() + objectName);
        Map<String, Schema.FieldSet> fieldSetsByName = objectDescribe.FieldSets.getMap();
        Schema.FieldSet fieldSet = fieldSetsByName.get(leasewareutils.getNamespacePrefix() + fieldSetName);
        
        set<string> setSkipFields = new set<String>();
        if(fieldSet != null){
        for ( Schema.FieldSetMember field : fieldSet.getFields()   ) {  
            setSkipFields.add(field.getFieldPath());
        }
        }
        System.debug('setSkipFields :'+setSkipFields);
    
        return setSkipFields;
    }
    
    /**
    * @description provides icon name for of the given object
    *
    * @param objectName - String, object name whose Icon need to retrieved
    *
    * @return iconName - String, iconName for the given object
    */
    public static String findIconName(String objectName) {
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();
        
        String iconName;
        
        for(Schema.DescribeTabSetResult tsr : tabSetDesc){
            tabDesc.addAll(tsr.getTabs()); 
        }
        
        for(Schema.DescribeTabResult tr : tabDesc) {
            if(objectName == tr.getSobjectName()){
                if(tr.isCustom() == true) {
                    if(!tr.getIcons().isEmpty()) {
                        iconDesc.addAll(tr.getIcons());
                    }
                    else {
                        iconName = '';
                    }
                }
                else{
                    iconName = 'standard:' + objectName.toLowerCase();
                }
            }
            
            if(iconDesc.size() > 0) {
                for(Schema.DescribeIconResult icon : iconDesc) {
                    if(icon.getContentType() == 'image/svg+xml'){
                        iconName = 'custom:' + icon.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                        break;
                    }
                } 
            }                                 
        }
        return iconName;
    }

    /**
    * @description provides List of fields with wrapper from the fieldset
    *
    * @param objectName - String, object name 
    * @param fieldSetName - String, FieldSet Name to be described
    *
    * @return Wrapper, description of the given Fields with basic details
    */   
    public static List<FieldStructure> getFieldSets(String fieldSetName, String objectName) {
        String namespace = LeaseWareUtils.getNamespacePrefix();
        
        if(objectName.indexof(namespace) == -1) {
            objectName = namespace + objectName;
        }
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);
        
        System.debug('getFieldSets '+targetType + ' fieldSetName: '+fieldSetName);
        
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Schema.FieldSet fieldSetObj = describe.fieldSets.getMap().get(fieldSetName);
        
        System.debug('getFieldSets describe:'+describe +' fieldSetObj:'+fieldSetObj);
        
        if(fieldSetObj == null) {
            return null;
        }
        List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields();
        List<FieldStructure> fieldList = new List<FieldStructure>();
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
        {
            FieldStructure field = new FieldStructure();
            field.fieldName = fieldSetMemberObj.getFieldPath();
            field.label = fieldSetMemberObj.getLabel();
            field.type = String.valueOf(fieldSetMemberObj.getType()).toLowerCase();
            fieldList.add(field);
            //system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
        }
        system.debug('getFieldSetNames fieldList' + fieldList); //api name
        return fieldList;
    }

    public class FieldStructure {
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public Object value {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public String referenceId {get; set;}
        
        public FieldStructure() {
            value = null;
        }
    }
    
}