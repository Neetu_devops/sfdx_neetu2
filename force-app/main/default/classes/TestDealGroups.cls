@isTest
private class TestDealGroups {
@isTest
        static void TestfetchDealgroups() {
        Date d1 = Date.parse('02/22/2009'); 
        Date d2 = Date.parse('02/20/2009'); 
        Date d3 = Date.parse('03/15/2009');

        Deal_Group__c TestDealGroup1 = new Deal_Group__c(
        Name = 'Deal Group Asia ',                   
        Target_Completion_Date__c = d1,                  
        Comment__c = 'All deals in Asia'
                                       
        );
        insert TestDealGroup1;
        Deal_Group__c TestDealGroup2 = new Deal_Group__c(
        Name = 'Deal Group EU ',                   
        Target_Completion_Date__c = d2,                     
        Comment__c = 'All deals in EU'                                 
        );
        insert TestDealGroup2;
        Deal_Group__c TestDealGroup3 = new Deal_Group__c(
        Name = 'Deal Group Test ',                   
        Target_Completion_Date__c = d3,                          
        Comment__c = 'All deals'                                 
        );
        insert TestDealGroup3;
            test.startTest();
            Deal_Group__c TestDealGroup=  DealGroups.fetchDealGroupsDetails(TestDealGroup1.Id);
            
           List<Deal_And_Group_Assignment__c> dealgroupassignments= DealGroups.getDealGroups(TestDealGroup1.Id);
           system.assertEquals(0,dealgroupassignments.size()); //since there is no assignmnet on this deal group 
            DealGroups.updateComment(TestDealGroup1);
            test.stopTest();
        }
    
}