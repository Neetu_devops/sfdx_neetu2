/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAircraftNeed {

/* anjani : deprecating AircraftNeed -- revert*/

   
    static testMethod void insertNotUsingEngManufacturer() {
    	//map<String,String> mapInOut = new map<String,String>();
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
		
		TestLeaseworkUtil.createTSLookup(mapInOut);
		Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(
			Name='12345', Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
			, Engine_Model__c ='CFM56', Attached_Aircraft__c= (String)mapInOut.get('Aircraft__c.Id'), Description__c ='Test', 
				TSN__c=0, CSN__c=0,// TSLV__c=0, CSLV__c= 0,
			 Type__c = 'Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1',
			Manufacturer_List__c = null);
		LeaseWareUtils.clearFromTrigger();insert 	consttAssembly;		

		mapInOut.put('Transaction__c',null);
    	TestLeaseworkUtil.insertTransaction(mapInOut); 
    	Transaction__c trx1 = (Transaction__c)mapInOut.get('Transaction__c');
    	trx1.Transaction_Type__c = 'Acquisition';
    	LeaseWareUtils.clearFromTrigger();update trx1;
    	mapInOut.put('Aircraft_In_Transaction__c',null);
    	TestLeaseworkUtil.insertAircraft_In_Transaction(mapInOut);  
    	Aircraft_In_Transaction__c AIT1= (Aircraft_In_Transaction__c)mapInOut.get('Aircraft_In_Transaction__c');
		AIT1.Aircraft_Type_New__c = 'A320';
		AIT1.Status__c='Active' ;
		LeaseWareUtils.clearFromTrigger();update AIT1;
		
	    LeaseWareUtils.clearFromTrigger();	
	    		
		Aircraft_Need__c myACNeed = new Aircraft_Need__c(Operator__c=([select Id from operator__c limit 1]).Id, Aircraft_Type__c='A320'
				);
		LeaseWareUtils.clearFromTrigger();insert myACNeed;
		Matching_Aircraft__c[] fetchRec = [select Id from Matching_Aircraft__c];
		system.assertEquals(fetchRec.size()>0,true);
		
	    LeaseWareUtils.clearFromTrigger();	
	    myACNeed.Date_Needed__c = 	system.today().addMonths(48);
	    myACNeed.Lease_Start_Date__c  = 	system.today().addMonths(-48);

		LeaseWareUtils.clearFromTrigger();update myACNeed;	
				
		
	    LeaseWareUtils.clearFromTrigger();	
	    myACNeed.Search_Lease_Expirations__c	= false;
	    myACNeed.Search_Transactions__c	= false;
		LeaseWareUtils.clearFromTrigger();update myACNeed;
		
    }  

    static testMethod void insertTestCodeCoverage() {
    	//map<String,String> mapInOut = new map<String,String>();
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
		
		TestLeaseworkUtil.createTSLookup(mapInOut);
		Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(
			Name='12345', Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
			, Engine_Model__c ='CFM56', Attached_Aircraft__c= (String)mapInOut.get('Aircraft__c.Id'), Description__c ='Test', 
				TSN__c=0, CSN__c=0, //TSLV__c=0 , CSLV__c= 0,
			 Type__c = 'Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1',
			Manufacturer_List__c = null);
		LeaseWareUtils.clearFromTrigger();insert 	consttAssembly;		

		mapInOut.put('Transaction__c',null);
    	TestLeaseworkUtil.insertTransaction(mapInOut); 
    	Transaction__c trx1 = (Transaction__c)mapInOut.get('Transaction__c');
    	trx1.Transaction_Type__c = 'Acquisition';
    	LeaseWareUtils.clearFromTrigger();update trx1;
    	mapInOut.put('Aircraft_In_Transaction__c',null);
    	TestLeaseworkUtil.insertAircraft_In_Transaction(mapInOut);  
    	Aircraft_In_Transaction__c AIT1= (Aircraft_In_Transaction__c)mapInOut.get('Aircraft_In_Transaction__c');
		AIT1.Aircraft_Type_New__c = 'A320';
		AIT1.Status__c='Active' ;
		LeaseWareUtils.clearFromTrigger();update AIT1;
		
	    LeaseWareUtils.clearFromTrigger();	
	    		
		Aircraft_Need__c myACNeed = new Aircraft_Need__c(Operator__c=([select Id from operator__c limit 1]).Id, Aircraft_Type__c='A320'
				);
		LeaseWareUtils.clearFromTrigger();insert myACNeed;
		Matching_Aircraft__c[] fetchRec = [select Id from Matching_Aircraft__c];
		system.assertEquals(fetchRec.size()>0,true);
		
	    LeaseWareUtils.clearFromTrigger();	
	    myACNeed.Date_Needed__c = 	system.today().addMonths(48);
	    myACNeed.Lease_Start_Date__c = 	system.today().addMonths(-48);
			
	    myACNeed.Engine_Variant__c = '3B1';
	    myACNeed.Vintage_From__c = '2010';
	    myACNeed.Vintage_Range__c = null;
	    LeaseWareUtils.clearFromTrigger();update myACNeed;					

		
    }  

    static testMethod void insertUsingEngManufacturer() {
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	mapInOut.put('Aircraft__c.Engine__c','CFM56');
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	

		TestLeaseworkUtil.createTSLookup(mapInOut);
		Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(
			Name='12345', Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
			, Engine_Model__c ='CFM56', Attached_Aircraft__c= (String)mapInOut.get('Aircraft__c.Id'), Description__c ='Test', 
				TSN__c=0, CSN__c=0, //TSLV__c=0, CSLV__c= 0,
			 Type__c = 'Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1',
			Manufacturer_List__c = 'CFMI');
		insert 	consttAssembly;		


		Aircraft_Need__c myACNeed = new Aircraft_Need__c(Operator__c=([select Id from operator__c limit 1]).Id, Aircraft_Type__c='A320'
			,Engine_Type_List__c = 'CFM56');
		insert myACNeed;
		Matching_Aircraft__c[] fetchRec = [select Id from Matching_Aircraft__c];
		system.assertEquals(fetchRec.size()>0,true);
		myACNeed.Vintage_From__c =  '2000';
		myACNeed.Vintage_Range__c = '10';
		
	    LeaseWareUtils.clearFromTrigger();				
		update myACNeed;
		
	    LeaseWareUtils.clearFromTrigger();		
		delete myACNeed;
		
	    LeaseWareUtils.clearFromTrigger();		
		undelete myACNeed;
    }  
   
}