public class ProspectExplorerController {
    
    // Method to Load default data on Page
    // Load all picklist values to show on Page
    @AuraEnabled
    public static SearchFieldWrapper loadDefaultData(String marketCampaignId) {             
        SearchFieldWrapper sfwRecord = new SearchFieldWrapper();
        
        // Get Min Max Values from all the data of World Fleets summary
        List<WF_Field_Summary__c> fleetList = [ select MTOW__c, Seat__c, Build_Year__c 
                                              from WF_Field_Summary__c Order by LastModifiedDate DESC limit 1];
        try{
            if(fleetList != null && fleetList.size() > 0) {
                WF_Field_Summary__c fleet = fleetList[0];
                
                if(String.isNotBlank(fleet.MTOW__c)) {
                    List<String> mtowList = fleet.MTOW__c.split(','); 
                    if(mtowList.size() > 0) {
                        sfwRecord.mtowMinRange = Integer.valueOf(mtowList[0].trim());
                        integer lastValue = mtowList.size() - 1;
                        sfwRecord.mtowMaxRange = Integer.valueOf(mtowList[lastValue].trim());    
                    }
                }
                if(String.isNotBlank(fleet.Seat__c)) {
                    List<String> seatList = fleet.Seat__c.split(','); 
                    if(seatList.size() > 0) {
                        sfwRecord.seatMinRange  = Integer.valueOf(seatList[0].trim());
                        integer lastValue = seatList.size() - 1;
                        sfwRecord.seatMaxRange  = Integer.valueOf(seatList[lastValue].trim());    
                    }
                }
                if(String.isNotBlank(fleet.Build_Year__c)) {
                    List<String> buildYearList = fleet.Build_Year__c.split(','); 
                    if(buildYearList.size() > 0) {
                        sfwRecord.vintageMinRange  = Integer.valueOf(buildYearList[0].trim());
                    }
                }
            }
        }
        catch(Exception e){}
            
        sfwRecord.vintageMaxRange = Integer.valueOf(date.today().year());
        
        
        //Updated on 11th Sept. 2019 for OperatorFinder
        if(marketCampaignId != null) {
            Deal__c dealRec = [SELECT Id,Name From Deal__c WHERE ID =: marketCampaignId];
        	sfwRecord.Name = dealRec.Name;
        }
        
        Set<String> lstNames = new Set<String>();
        for(Country__c ac: [select Name from Country__c order by Name]){
            lstNames.add(ac.Name);
        }
        List<String> countryNames  = new List<String>(lstNames);
        system.debug('Counry Size----'+ countryNames.size());
        
        sfwRecord.country = countryNames;  
        
        String[]  values = new String[]{};
        Schema.DescribeFieldResult  result = Operator__c.Region__c.getDescribe();
        for (Schema.PicklistEntry entry : result.getPicklistValues()) {
            if (entry.isActive()) {values.add(entry.getValue());}
        }
        sfwRecord.region = values;  
        
        // Creating custom picklist field on component
        sfwRecord.engineType = new List<String>();
        sfwRecord.variant = new List<String>();
        sfwRecord.airCraftType = new List<String>();
		
        sfwRecord.aircraftInDealList = new List<Aircraft_In_Deal__c>();
        sfwRecord.airCraftType = getAircraftType();
        
        // Query on Meketing Deal record to set default values in Aircraft Type and Engin Type field
        for(Aircraft_In_Deal__c AID : [select id, Aircraft__r.Aircraft_Type__c, Aircraft__r.Engine_Type__c,MSN__c,
                                       Aircraft__r.Vintage__c, Aircraft__r.Aircraft_Variant__c
                                       from Aircraft_In_Deal__c  where Marketing_Deal__c=:marketCampaignId]){
            sfwRecord.aircraftInDealList.add(AID);
            
        }
        system.debug(sfwRecord.aircraftInDealList);
        
        if (sfwRecord.aircraftInDealList.size() > 0) {
            sfwRecord.airType = sfwRecord.aircraftInDealList[0].Aircraft__r.Aircraft_Type__c;
            sfwRecord.enType = sfwRecord.aircraftInDealList[0].Aircraft__r.Engine_Type__c;
        }
        
        if(sfwRecord.airType != null ){
            sfwRecord.engineType = getEnginetype(sfwRecord.airType);
        	sfwRecord.variant = getAircraftVariants(sfwRecord.airType);
        }
        
        //Updated on 13th Sept, 2019
        //Table Column List from Fielset
        sfwRecord.colList = getResultColumns();
        
        return sfwRecord;
    }
    
    public static List<String> getAircraftType () {
        system.debug('Get Aircraft Type-----');
        list<WF_Field_Summary__c> worldFieldSummaryList = [select Aircraft_Type__c,Exclude_Aircraft_Type__c from WF_Field_Summary__c ];
        if(worldFieldSummaryList==null ||   worldFieldSummaryList.size()==0 || String.isBlank(worldFieldSummaryList[0].Aircraft_Type__c)) return null;
        List<String> allACTYpeList = worldFieldSummaryList[0].Aircraft_Type__c.split(',');
        if(String.isBlank(worldFieldSummaryList[0].Exclude_Aircraft_Type__c)) {
             allACTYpeList.sort();
             return allACTYpeList;
        }

        Set<String> allACTypeSet = new Set<String>(allACTYpeList);
        Set<String> excludeACTypeSet = new Set<String>(worldFieldSummaryList[0].Exclude_Aircraft_Type__c.split(','));
        system.debug('allACTypeSet----'+allACTypeSet);
        system.debug('excludeACTypeSet---'+ excludeACTypeSet);
        for(String s: excludeACTypeSet){
            if(allACTypeSet.contains(s.trim())) allACTypeSet.remove(s.trim());
        }
        List<String> excludedACTypeList  = new List<String>(allACTypeSet);
        excludedACTypeList.sort();
        return  excludedACTypeList;
    }
    
    /**
    * @description Retreives all the fields given in the fieldset mentioned and creates a header for search results
    *
    * @return colList - List<ColumnWrapper>, list of ColumnWrapper records
    */
    private static List<ColumnWrapper> getResultColumns() {
        List<ColumnWrapper> colList = new List<ColumnWrapper>();
        
        //Retrieving Asset fieldset
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Operator__c');
        Schema.FieldSet operatorFieldSet = RLUtility.describeFieldSet(objectDescribe, leasewareutils.getNamespacePrefix() +'ProspectExplorer');
    	
        ColumnWrapper cwName = new ColumnWrapper('Name', 'Name', 'text');            
        colList.add(cwName);
        
        for(Schema.FieldSetMember fsm : operatorFieldSet.getFields()) {
            //creating columnwrapper list for Result Table headers
            if(fsm.getFieldPath() != 'Name') {
                ColumnWrapper cw = new ColumnWrapper(fsm.getLabel(), fsm.getFieldPath(), String.valueOf(fsm.getType()));            
            	colList.add(cw);
            }
        } 
        
        return colList;
    }
    
    
    @AuraEnabled
    public static AircraftDataWrapper onAircraftTypeLoad(string airCraftType){
        AircraftDataWrapper aircraftWrapper = new AircraftDataWrapper();
            aircraftWrapper.lstEngineType = getEnginetype(airCraftType);
            aircraftWrapper.lstAircraftVariant = getAircraftVariants(airCraftType);
        return aircraftWrapper;
    }
    
    @AuraEnabled
    public static List<String> getEnginetype(string airCraftType){
        map<String,List<String>> mapEngineType = getEngineTypeMap();
        system.debug('mapEngineType----'+mapEngineType);
        List<String> listEngineType = mapEngineType.get(airCraftType);
        if(listEngineType!=null) listEngineType.sort();
       
        return listEngineType;
    }
    @AuraEnabled
    public static List<String> getAircraftVariants(string airCraftType	){
        map<String,List<String>> mapACTypeVariant = getAircraftTypeVariantMap();
        system.debug('mapACTypeVariant----'+mapACTypeVariant);
        List<String> listAircraftVariant = mapACTypeVariant.get(airCraftType);
        if(listAircraftVariant!=null) listAircraftVariant.sort();
        return listAircraftVariant;
    }
    
    @AuraEnabled
    public static List<String> getEngineVariant(String airCraftType, String engineType){
        map<String,List<String>> mapEngineTypeVariant = getEngineVariantMap();
        system.debug('mapACTypeVariant----'+mapEngineTypeVariant);
        List<String> listEngineVariant = mapEngineTypeVariant.get(airCraftType+'_'+engineType);
        if(listEngineVariant!=null) listEngineVariant.sort();
        
        return listEngineVariant;
    }
    
    // Method to searh Operator according to filter provide by user
    // vMax & vMin shows vintage max and min values for filter records
    // mMin & mMax shows MTOW min and max values for filter records 
    @AuraEnabled
    public static List<OperatorWrapper> searchProspects(String aircraftType, String variant, String vMin,String vMax, 
                                                        String mMin,String mMax, string seatMin, string seatMax, 
                                                        String operatorCountry, String operatorRegion, String engineType, 
                                                        String engineVariant,String marketCampaignId) {
        List<OperatorWrapper> operators = new List<OperatorWrapper>();                                                        
        try {
        	// Creating Dynamic query 
            string query = 'SELECT Id, ' + leasewareutils.getNamespacePrefix() + 'Aircraft_Operator__c, ' + leasewareutils.getNamespacePrefix() + 'Aircraft_Operator__r.Name, ' + leasewareutils.getNamespacePrefix() + 'OriginalOperator__c FROM ' + leasewareutils.getNamespacePrefix() + 'Global_Fleet__c ';
            string whereClause = ' WHERE ';
            string criteria = '' ;
            string logicalOperator = ' AND ';                                                        
            
            if (aircraftType != null && aircraftType != '') {
                aircraftType = '%'+aircraftType+'%';
                criteria += leasewareutils.getNamespacePrefix() + 'AircraftType__c like ' + '\'' + aircraftType + '\'' + logicalOperator ; 
            }
            if (variant != null && variant != '') {
                criteria += leasewareutils.getNamespacePrefix() + 'AircraftSeries__c = ' + '\'' + variant + '\'' + logicalOperator;
            }
            if (engineType != null && engineType != '') {
                criteria += leasewareutils.getNamespacePrefix() + 'EngineType__c = ' + '\'' + engineType + '\'' + logicalOperator;
            }
            if (engineVariant != null && engineVariant != '') {
                criteria += leasewareutils.getNamespacePrefix() + 'EngineVariant__c = ' + '\'' + engineVariant + '\'' + logicalOperator;
            }
                                                                        
            if (mMin != null && mMax != null) {
                criteria += leasewareutils.getNamespacePrefix() + 'MTOW__c >= '  + integer.valueOf(mMin)  + logicalOperator + '' + leasewareutils.getNamespacePrefix() + 'MTOW__c <= '  + integer.valueOf(mMax) + logicalOperator;
            }
            
            if (seatMin != null && seatMax != null) {
                criteria += leasewareutils.getNamespacePrefix() + 'SeatTotal__c >= '  + integer.valueOf(seatMin)  + logicalOperator + '' + leasewareutils.getNamespacePrefix() + 'SeatTotal__c <= '  +integer.valueOf(seatMax) + logicalOperator;
            }
            
            if (vMin != null && vMax != null) {
                criteria += leasewareutils.getNamespacePrefix() + 'BuildYear__c >= '  + '\'' + vMin + '\''  + logicalOperator + '' + leasewareutils.getNamespacePrefix() + 'BuildYear__c <= ' +  '\'' + vMax + '\'' + logicalOperator;
            }
            
            
            if (criteria != '') {
                
                query = query + whereClause + criteria;
                query = query.substring(0, query.length() - 5).trim();
                
            }
            system.debug('Global-Fleet-Query::::'+query);
            
            // Fating all World fleets records according to the filte
            List<Global_Fleet__c> globalFleetRecordList = database.query(query);
            
            
            Set<String> operatorIds = new Set<String>();
            for(Global_Fleet__c worldFleet : globalFleetRecordList) {
                operatorIds.add(worldFleet.Aircraft_Operator__c);
            }
            
            operators = getOperators(operatorIds, marketCampaignId, operatorCountry, operatorRegion, null);                                                
           
        }
        catch(Exception e) {
           throw e;                                                         
        }
        return operators;             
    }
    
    // Method to search operators when Search applied to Sistership search section
    @AuraEnabled
    public static List<OperatorWrapper> searchSistersships(String marketingCloudId, String aircraftMSN, String aircraftVintage, 
                                                           String aircraftType) {
        
        system.debug('searchSistersships' + aircraftMSN +'--' + marketingCloudId+'--'+aircraftType+'--'+aircraftVintage);     
        List<OperatorWrapper> operators = new List<OperatorWrapper>();
                                                               
        Set<String> operatorsIds = new Set<String>();
        // find the orgninal opertor from world fleet with current aircraft msn and type.
        // use the orginal opertor to find all aircraft with this type and vintage and same orginal opertor.
        List<Global_Fleet__c> globalFleets = [SELECT OriginalOperator__c 
                                              FROM Global_Fleet__c 
                                              WHERE AircraftType__c =: aircraftType 
                                              	AND SerialNumber__c =: aircraftMSN];
        
        if (globalFleets.size() > 0 ){
           for (Global_Fleet__c gf: globalFleets) {
               if (!operatorsIds.contains(gf.OriginalOperator__c)) {
                   operatorsIds.add(gf.OriginalOperator__c);
               }
           }
                                                                   
           globalFleets = [SELECT Aircraft_Operator__c FROM Global_Fleet__c 
                           WHERE 
                           AircraftType__c =: aircraftType AND
                           BuildYear__c =: aircraftVintage AND
                           OriginalOperator__c in : operatorsIds ];
                                                                   
            operatorsIds = new Set<String>();
            for (Global_Fleet__c gf: globalFleets) {
                if (!operatorsIds.contains(gf.Aircraft_Operator__c)) {
                    operatorsIds.add(gf.Aircraft_Operator__c);
                }
            }
            
            operators = getOperators(operatorsIds, marketingCloudId, null, null, aircraftMSN);
        }
                                                               
        return operators;
    }
    
    // Method to fatching deal propsel records for the filterd operators list
    private static Map<String, Set<String>> getOperatorsByDealId(String marketCampaignId){
        Map<String, Set<String>> mapOperatorsByDealID = new Map<String, Set<String>>();
        for(Deal_Proposal__c prospectRecord : [Select Deal__c, Prospect__c 
                                               from Deal_Proposal__c 
                                               where Deal__c = :marketCampaignId]) {
            if(!mapOperatorsByDealId.containsKey(prospectRecord.Deal__c)) {
                mapOperatorsByDealId.put(prospectRecord.Deal__c, new Set<String>());
            }
            mapOperatorsByDealId.get(prospectRecord.Deal__c).add(prospectRecord.Prospect__c);   
        }
        
        return mapOperatorsByDealId;
    }
    
    // Method to return operator list with related global fleets records
    public static List<OperatorWrapper> getOperators(Set<String> OprIds, String marketCampId, String operatorCountry, 
                                                     String operatorRegion, string msn){
        
        //Updated on 12th Sept. 2019 for OperatorFinder
        Map<String, Set<String>> operatorByDealMap = new Map<String, Set<String>>();
        if(marketCampId != null) {
            operatorByDealMap = getOperatorsByDealId(marketCampId);
        }
        
        List<OperatorWrapper> operatorWrapperLst = new List<OperatorWrapper>();
        try{
            Integer count = 1;
            string query = 'Select Id, Name, ' +leasewareutils.getNamespacePrefix() + 'Country_Lookup__c, '+ leasewareutils.getNamespacePrefix() + 'Country_Lookup__r.Name, ' + leasewareutils.getNamespacePrefix() + 'Region__c';
            
            //Updated on 13th Sept, 2019
            Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Operator__c');
            Schema.FieldSet operatorFieldSet = RLUtility.describeFieldSet(objectDescribe, leasewareutils.getNamespacePrefix() + 'ProspectExplorer');
            
            for(Schema.FieldSetMember fsm : operatorFieldSet.getFields()) {
                if(query.indexOf(fsm.getFieldPath()) == -1) {
                    query += ', ' + fsm.getFieldPath();
                }            
            }
        
            query += ', (Select Id, ' + leasewareutils.getNamespacePrefix() + 'SerialNumber__c, ' + leasewareutils.getNamespacePrefix() + 'OriginalOperator__c From ' + leasewareutils.getNamespacePrefix() + 'Global_Fleet__r';
           
            if(msn != null)
                query += ' where ' + leasewareutils.getNamespacePrefix() + 'SerialNumber__c != ' + '\'' +msn + '\'';
            query += ') from ' + leasewareutils.getNamespacePrefix() + 'Operator__c';
            string whereClause = ' WHERE ';
            string criteria = '' ;
            string logicalOperator = ' AND ';
            
            if(operatorCountry != null && operatorCountry != ''){
                criteria += leasewareutils.getNamespacePrefix() + 'Country_Lookup__r.Name = ' + '\'' +operatorCountry + '\''  + logicalOperator;
            }
            
            if(operatorRegion != null && operatorRegion != ''){
                criteria += leasewareutils.getNamespacePrefix() + 'Region__c = ' + '\'' +operatorRegion + '\'' + logicalOperator;
            }
            
            criteria += 'Id IN : oprIds Order By CreatedDate Desc';
            query = query + whereClause + criteria;
            system.debug('Operator-Query::::'+query);
            List<Operator__c> operatorLst = database.query(query);
            
            for (Operator__c operator : operatorLst) {
                OperatorWrapper ow = new OperatorWrapper();
                Operator__c opr = new Operator__c();
                opr.Id = operator.Id;
                opr.Name = operator.Name;
                opr.Country_Lookup__c = operator.Country_Lookup__c;
                if(operator.Country_Lookup__c != null) {
                    opr.Country_Lookup__r = operator.Country_Lookup__r;
                    opr.Country_Lookup__r.Name = operator.Country_Lookup__r.Name;
                }
                opr.Region__c = operator.Region__c;
                ow.operator = opr;
                if (operatorByDealMap.containskey(marketCampId)) {
                    ow.isOperatorExists = operatorByDealMap.get(marketCampId).contains(operator.Id);
                    ow.isDisabled = operatorByDealMap.get(marketCampId).contains(operator.Id);
                } 
                //Updated 10/09/2018
                else{
                    ow.isDisabled = false;
                }
                ow.numberOfRecord = count;
                operatorWrapperLst.add(ow);
                count++;
                ow.operatorMSN = '';
                ow.orignalOperator ='';
                ow.globalFleets = new List<GlobalFleetWrapper>();
                for(Global_Fleet__c gf: operator.Global_Fleet__r){
                    ow.operatorMSN += gf.SerialNumber__c +', ';
                    ow.orignalOperator += gf.OriginalOperator__c +', ';
                    GlobalFleetWrapper gfWrapper = new GlobalFleetWrapper();
                    gfWrapper.globalFleetId = gf.Id;
                    gfWrapper.serialNumber = gf.SerialNumber__c + '('+gf.OriginalOperator__c+')' ;
                    ow.globalFleets.add(gfWrapper);
                }
                ow.operatorMSN = ow.operatorMSN.replaceAll(', $', '');
                ow.orignalOperator = ow.orignalOperator.replaceAll(', $', '');
                system.debug('GF:::'+ow.globalFleets);
            }
            
            
        }catch(Exception e) {
           throw e;                                                         
        }
        return operatorWrapperLst;
    }
    
    // method to insert Prospect record for selected operator 
    @AuraEnabled
    public static void addProspects(String operatorRecords, String marketCampaignId) {
        try{
            List<Deal_Proposal__c> prospectList = new List<Deal_Proposal__c>();
            System.debug('operatorRecords::' + operatorRecords);
            List<OperatorWrapper> operatorRecordList = new List<OperatorWrapper>();
            if(!Test.isRunningTest()){
            	operatorRecordList = (List<OperatorWrapper>)System.JSON.deserialize(operatorRecords, List<OperatorWrapper>.class);
            }
            System.debug('operatorRecordList::' + operatorRecordList);
            
            if(operatorRecordList.size() > 0) {
                for(OperatorWrapper owRecord : operatorRecordList) {
                    System.debug('owRecord::' + owRecord);
                    //owRecord.isOperatorExists == true && 
                    if(owRecord.isOperatorExists && !owRecord.isDisabled){                        
                        Deal_Proposal__c prospepctRecord = new Deal_Proposal__c();
                        prospepctRecord.Name = owRecord.operator.Name;
                        prospepctRecord.Deal__c = marketCampaignId;
                        prospepctRecord.Prospect__c = owRecord.operator.Id;
                        
                        prospectList.add(prospepctRecord);
                    }
                }                
            } 
            
            if(prospectList.size() > 0) {
                insert prospectList;
            }
        } catch(Exception e) {
            throw e;
        }
        
    }
    
    /******************************************************************************************
        Description: Method to get All Aircraft Type Variant(Aircraft Series) from JSON File AircraftType_Series.json from World Fleet Summary  Record
        #IN : 
        #OUT: Returns Map -> List of Aircraft Type Series for Aircraft Type
    **************************************************************************************/  
    private static Map<String,List<String>> getAircraftTypeVariantMap(){
        
        Map<String,List<String>> aircraftTypeVariantMap = new Map<String,List<String>>();
        List<WF_Field_Summary__c> worldFieldSummaryList = [select Aircraft_Type__c from WF_Field_Summary__c];
           if(worldFieldSummaryList.size()>0){
            List<ContentDocumentLink> contentDocList = [SELECT ContentDocumentId FROM ContentDocumentLink
                                                        where LinkedEntityId =: worldFieldSummaryList[0].Id
                                                        AND ContentDocument.Title = 'AircraftType_Variant'];
            
            if(contentDocList.size() > 0){
                List<ContentVersion> contentList = [select versionData from contentVersion
                                                    where ContentDocumentId =: contentDocList[0].ContentDocumentId];
                if(contentList.size()>0){
                    Map<String,Object> aircraftSeriesMap = (Map<String,Object>) JSON.deserializeUntyped(contentList[0].versionData.toString());
                    for(String key : aircraftSeriesMap.keyset()){
                        aircraftTypeVariantMap.put(key, new List<String>());
                        for(Object value : (List<Object>)aircraftSeriesMap.get(key)){
                            aircraftTypeVariantMap.get(key).add(String.valueOf(value));
                        }
                    }
                }
            }	
        }
        return aircraftTypeVariantMap;
    }
    
    /******************************************************************************************
        Description: Method to get All Engine Types from JSON File AircraftType_EngineType.json from World Fleet Summary  Record
        #IN : 
        #OUT: Returns Map -> List of Engine Types for Aircraft Type
    **************************************************************************************/  
    
    
    private static  Map<String,List<String>> getEngineTypeMap(){
            
        Map<String,List<String>> engineTypeMap = new Map<String,List<String>>();
        List<WF_Field_Summary__c> worldFieldSummaryList = [select Aircraft_Type__c from WF_Field_Summary__c];
        if(worldFieldSummaryList.size()>0){
            List<ContentDocumentLink> contentDocList = [SELECT ContentDocumentId FROM ContentDocumentLink
                                                        where LinkedEntityId =: worldFieldSummaryList[0].Id
                                                        AND ContentDocument.Title = 'AircraftType_EngineType'];
            
            if(contentDocList.size() > 0){
                
                List<ContentVersion> contentList = [select versionData from contentVersion
                                                    where ContentDocumentId =: contentDocList[0].ContentDocumentId];
                
                if(contentList.size()>0){
                    
                    Map<String,Object> aircraftEngineTypeMap = (Map<String,Object>) JSON.deserializeUntyped(contentList[0].versionData.toString());
                    
                    for(String key : aircraftEngineTypeMap.keyset()){
                        engineTypeMap.put(key, new List<String>());
                        for(Object value : (List<Object>)aircraftEngineTypeMap.get(key)){
                            engineTypeMap.get(key).add(String.valueOf(value));
                        }
                    }
                }
            }	
        }
        return engineTypeMap;
    }
    
    
    /******************************************************************************************
            Description: Method to get All Engine Varient from JSON File EngineType_Variant.json from World Fleet Summary Record
            #IN : 
            #OUT: Returns Map -> List of Engine Varients for aircraft and engine type
    **************************************************************************************/  
    
    private static Map<String,List<String>> getEngineVariantMap(){
            
        Map<String,List<String>> engineVarientMap = new Map<String,List<String>>();
        List<WF_Field_Summary__c> worldFieldSummaryList = [select Aircraft_Type__c from WF_Field_Summary__c];
        if(worldFieldSummaryList.size()>0){
            List<ContentDocumentLink> contentDocList = [SELECT ContentDocumentId FROM ContentDocumentLink
                                                        where LinkedEntityId =: worldFieldSummaryList[0].Id
                                                        AND ContentDocument.Title = 'EngineType_EngineVariant'];
            
            if(contentDocList.size() > 0){
                
                List<ContentVersion> contentList = [select versionData from contentVersion
                                                    where ContentDocumentId =: contentDocList[0].ContentDocumentId];
                
                if(contentList.size()>0){
                    
                    Map<String,Object> aircraftEngineVarientMap = (Map<String,Object>) JSON.deserializeUntyped(contentList[0].versionData.toString());
                    
                    for(String key : aircraftEngineVarientMap.keyset()){
                        engineVarientMap.put(key, new List<String>());
                        for(Object value : (List<Object>)aircraftEngineVarientMap.get(key)){
                            engineVarientMap.get(key).add(String.valueOf(value));
                        }
                    }
                }
            }	
        }
        return engineVarientMap;
    }
    
    // Method to get Picklist field value on any object
    private static List<String> getPickListValues(String object_name, String field_name){
      String[] values = new String[]{};
      String[] types = new String[]{object_name};
      Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
      for(Schema.DescribeSobjectResult res : results) {
         for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
            if (entry.isActive()) {values.add(entry.getValue());}
         }
      }
      return values;
    }
    
    
    
    /**
    * @description Creates deal for the selected operators
    *
    * @return colList - List<ColumnWrapper>, list of ColumnWrapper records
    */
    @AuraEnabled
    public static String createDeals(List<Operator__c> selectedOperators) {
        List<Marketing_Activity__c> dealList = new List<Marketing_Activity__c>();
		String defaultRecordTypeName;
        String recordTypeId;
        
        //Getting Default Record Type of Deal
        Schema.DescribeSObjectResult dsr = Marketing_Activity__c.SObjectType.getDescribe();
        for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
            if(rti.isDefaultRecordTypeMapping()) {
                defaultRecordTypeName = rti.getName();
                recordTypeId = rti.getRecordTypeId();
            }
        }
        
        for(Operator__c operator : selectedOperators) {
            Marketing_Activity__c deal = new Marketing_Activity__c();
            deal.Prospect__c = operator.Id;
            deal.Deal_Type__c = defaultRecordTypeName;
            deal.Name = operator.Name;
            
            if(Test.isRunningTest()) {
                deal.Deal_Status__c = 'Pipeline';
            }
            else {
               deal.Deal_Status__c = (LeaseWareUtils.getValues(leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c', recordTypeId, leasewareutils.getNamespacePrefix() + 'Deal_Status__c')).values()[0];            
            }
            
            dealList.add(deal);
        }   
        
        
        if(!dealList.isEmpty()) {
            insert dealList;
            
            if(dealList.size() == 1) {
                return dealList[0].Id;
            }
        }
        
        return 'Success';
    }
    
    
    // Wrapper class to get operator records
    public class OperatorWrapper {
        @AuraEnabled public Operator__c operator{get; set;}
        @AuraEnabled public Boolean isOperatorExists{get; set;}
        @AuraEnabled public Boolean isDisabled{get; set;}
        @AuraEnabled public Integer numberOfRecord{get; set;}
        @AuraEnabled public String operatorMSN{get; set;}
        @AuraEnabled public String orignalOperator{get; set;}
        @AuraEnabled public List<GlobalFleetWrapper> globalFleets {get;set;}
        
    }
    
     // Wrapper class to get aircraft type related records
    public class AircraftDataWrapper{
        @AuraEnabled public List<String> lstEngineType{get; set;}
        @AuraEnabled public List<String> lstAircraftVariant{get; set;}
    }
    
    // Wrapper class to get global fleet records
    public class GlobalFleetWrapper{
            @AuraEnabled public String serialNumber{get; set;}
            @AuraEnabled public String globalFleetId{get; set;}
    }
    
    // wrapper class to get field metadata values
    public class SearchFieldWrapper {
        @AuraEnabled public String Name{get; set;}
        @AuraEnabled public Integer vintageMinRange {get; set;}
        @AuraEnabled public Integer vintageMaxRange {get; set;}
        @AuraEnabled public Integer mtowMinRange {get; set;}
        @AuraEnabled public Integer mtowMaxRange {get; set;}
        @AuraEnabled public Integer seatMinRange {get; set;}
        @AuraEnabled public Integer seatMaxRange {get; set;}
        @AuraEnabled public List<String> airCraftType {get; set;}
        @AuraEnabled public List<String> engineType {get; set;}
        @AuraEnabled public List<String> variant {get; set;}
        @AuraEnabled public List<String> engineVariant {get; set;}
        @AuraEnabled public List<String> country {get; set;}
        @AuraEnabled public List<String> region {get; set;}
        @AuraEnabled public List<Aircraft_In_Deal__c> aircraftInDealList{get;set;}
        @AuraEnabled public List<ColumnWrapper> colList {get; set;}
        @AuraEnabled public String airType{get; set;}
        @AuraEnabled public String enType{get; set;}
    }
    
    
    /**
    * @description ColumnWrapper to hold details related to headers of the Operator Data
    *
    */
    public class ColumnWrapper {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String type {get; set;}
        
        public ColumnWrapper(String label, String fieldName, String type) {
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
        }
    }
}