public class FundCodeTriggerHandler implements ITrigger{
    
    private final string triggerBefore = 'FundCodeTriggerHandlerBefore';
    private final string triggerAfter = 'FundCodeTriggerHandlerAfter';

    public FundCodeTriggerHandler()
    {
    }
    
    public void bulkBefore()
    {
    }
    
    public void bulkAfter()
    {
    }
    
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('FundCodeTriggerHandler.beforeInsert(+)'); 
        Fund_Code__c[] newFundCodes = (list<Fund_Code__c>)trigger.new;

        LeaseWare_SequenceId__c fundCodeStartNumber;

        Set<Id> assemblyIds = new Set<Id>();
        Map<Id, Constituent_Assembly__c> mapAssemblies = new   Map<Id, Constituent_Assembly__c> ();
        for (Fund_Code__c curFundCode :newFundCodes ) {
            assemblyIds.add(curFundCode.Assembly_Lkp__c);
        }

        for(Constituent_Assembly__c curAssembly : [Select id, name ,Type__c,Attached_Aircraft__r.MSN_Number__c from 
                                                    Constituent_Assembly__c where id in: assemblyIds]){
            mapAssemblies.put(curAssembly.Id,curAssembly);
        }

        fundCodeStartNumber = LeaseWare_SequenceId__c.getValues('Sequence_Number_FundCode');
        if (fundCodeStartNumber != null && fundCodeStartNumber.Active__c){
			//Get Sequence Number
        	try { 
                //Row is Locked for UPDATE in getSequnceNumber through out current transaction.
                //Other users trying to do SELECT UPDATE will receive error.
                fundCodeStartNumber = LeaseWareUtils.getSequenceNumber('FundCode');
        	}catch (QueryException ex){
        		if(ex.getMessage().contains('UNABLE_TO_LOCK_ROW') )
                newFundCodes[0].addError('FundCode Id generation in currently in progress. Please click Save again.');
        	}
        	Integer sequenceNumber=-1;
        	
        	//If Recent Sequence number not there , take Initial value.
        	sequenceNumber=fundCodeStartNumber.Sequence_Number_New__c == null ?fundCodeStartNumber.Initial_Value__c.intValue():fundCodeStartNumber.Sequence_Number_New__c.intValue();
        	//Assign Sequence Numbers
        	for (Fund_Code__c curFundCode :newFundCodes ) {
                curFundCode.Sequence_Number__c = sequenceNumber++;
                Constituent_Assembly__c assembly = mapAssemblies.get(curFundCode.Assembly_Lkp__c);
                curFundCode.Name = assembly.Type__c +'-'+ assembly.Attached_Aircraft__r.MSN_Number__c +'-'+curFundCode.Sequence_Number__c ;   
            }			
        	try {
            	//Update Custom Setting with Latest sequence number. 
        		LeaseWareUtils.updateSequenceNumber('FundCode',sequenceNumber);
        	}catch (DmlException ex) {
           		//Both Query Exception and dmlException should be handled
        		if(ex.getMessage().contains('The record you are attempting to edit, or one of its related records, is currently being modified by another user')) 
                newFundCodes[0].addError('Lease Id generation in currently in progress. Please click Save again.');
        	}       
        }
        system.debug('FundCodeTriggerHandler.beforeInsert(-)'); 
    }

    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('FundCodeTriggerHandler.beforeUpdate(+)'); 
        Fund_Code__c[] newFundCodes = (list<Fund_Code__c>)trigger.new;
        for (Fund_Code__c curFundCode :newFundCodes ) {
            Fund_Code__c oldCode = (Fund_Code__c)Trigger.oldMap.get(curFundCode.Id);
            if(curFundCode.Assembly_Lkp__c != oldCode.Assembly_Lkp__c){
                curFundCode.addError('This Fund Code is associated to the selected Assembly and cannot be changed');
            }        
        }
        system.debug('FundCodeTriggerHandler.beforeUpdate(-)'); 
    }
    
    public void beforeDelete()
    {      
        
    }
    
    public void afterInsert()
    {  
    }
    
    public void afterUpdate()
    {    
    }
    
    public void afterDelete()
    {
    }
    
    public void afterUnDelete()
    {    
    }
    
    public void andFinally()
    {
    }
}
