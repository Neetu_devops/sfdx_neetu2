public  with sharing class ShowLLPSummaryTableController {
 
    @AuraEnabled
    public static List<LLPSummaryWrapper> loadInitData(Id recordId, String filtertype){
        try{
        //Monthly_Scenario__c msRecord = [SELECT Id, Start_Date__c, End_Date__c, Fx_Component_Output__r.Type__c FROM Monthly_Scenario__c WHERE Id =: fxMonthlyOutputId];
        
        List<LLPSummaryWrapper> llpfWrap = new List<LLPSummaryWrapper>();
        List<Monthly_Scenario__c> msRecordList = [SELECT Id,Name, Start_Date__c, End_Date__c, Fx_Component_Output__r.Type__c,
                                                  Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c,
                                                  Fx_Component_Output__r.Name, Fx_Component_Output__r.RC_FC__c, TSO__c,
                                                  CSO__c, Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c ,Fx_Component_Output__r.Life_Limited_Part_LLP__r.id,
                                                  Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name, Cycles_Remaining__c,
                                                  Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c,Fx_Component_Output__r.RC_FH__c,
                                                  Fx_Component_Output__r.Assembly__r.Name,Shortfall_FC__c,
                                                  Fx_Component_Output__r.Life_Limited_Part_LLP__r.Part_Number__c,Hours_Remaining__c,
                                                  Months_Remaining__c,Month_Since_Overhaul__c,Shortfall_Month__c,Shortfall_FH__c,
                                                  Fx_Component_Output__r.RC_Months__c,Fx_Component_Output__r.Life_Limit_Interval_2_Months__c,Fx_Component_Output__r.RC_Applicability__c FROM Monthly_Scenario__c 
                                                  WHERE Fx_Component_Output__r.Fx_General_Input__c = : recordId
                                                  AND Fx_Component_Output__r.Life_Limited_part_LLP__c != NULL
                                                  AND Fx_Component_Output__r.Event_Type__c =: filtertype.split('#')[0]
                                                  AND Fx_Component_Output__r.Type__c =: filtertype.split('#')[1]
                                                  AND Return_Month__c = True
                                                  ORDER BY Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name,Start_Date__c,Fx_Component_Output__r.Type__c];
        
        
            if(!msRecordList.isEmpty()){
                for(Monthly_Scenario__c ms : msRecordList){
                    LLPSummaryWrapper llpWrapper = new LLPSummaryWrapper();
                    llpWrapper.recordId = ms.Fx_Component_Output__r.Life_Limited_Part_LLP__r.id;
                    llpWrapper.Name = ms.Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name;
                    
                    //Month
                    llpWrapper.msConditionPerContract.month = ms.Fx_Component_Output__r.RC_Months__c;
                    llpWrapper.msUsageSincePR.month = ms.Month_Since_Overhaul__c;
                    if(ms.Fx_Component_Output__r.RC_Applicability__c == 'Remaining To Event')
                        llpWrapper.msProjected.month = ms.Months_Remaining__c;
                    else
                        llpWrapper.msProjected.month =  ms.Month_Since_Overhaul__c;
                   
                    llpWrapper.msPerformanceLimit.month = ms.Fx_Component_Output__r.Life_Limit_Interval_2_Months__c;
                    llpWrapper.msShortfall.month = ms.Shortfall_Month__c;
                    
                    //Cycle
                    llpWrapper.msConditionPerContract.cycle = ms.Fx_Component_Output__r.RC_FC__c;
                    llpWrapper.msUsageSincePR.cycle = ms.CSO__c;
                    if(ms.Fx_Component_Output__r.RC_Applicability__c == 'Remaining To Event')
                        llpWrapper.msProjected.cycle = ms.Cycles_Remaining__c;
                    else
                        llpWrapper.msProjected.cycle =  ms.CSO__c;
                    llpWrapper.msPerformanceLimit.cycle =  ms.Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c;
                    llpWrapper.msShortfall.cycle = ms.Shortfall_FC__c;
                    
                    //Hour
                    llpWrapper.msConditionPerContract.hour = ms.Fx_Component_Output__r.RC_FH__c;
                    llpWrapper.msUsageSincePR.hour = ms.TSO__c;
                        if(ms.Fx_Component_Output__r.RC_Applicability__c == 'Remaining To Event')
                        llpWrapper.msProjected.hour = ms.Hours_Remaining__c;
                    else
                        llpWrapper.msProjected.hour =  ms.TSO__c;
                    llpWrapper.msPerformanceLimit.hour = ms.Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c;
                    llpWrapper.msShortfall.hour = ms.Shortfall_FH__c;
                    
                    llpfWrap.add(llpWrapper);
                }
            }
        
        return llpfWrap;
        }catch(Exception e){
            system.debug('Unexpected error='+e);
            throw new AuraHandledException('An internal Server Error has Occured: '+e.getMessage()+' Line Number: '+e.getLineNumber());
        }
    }

    public class LLPSummaryWrapper {
        @AuraEnabled public String recordId {get;set;}
        @AuraEnabled public String Name {get;set;}
    	@AuraEnabled public MonthlyScenarioWrapper msConditionPerContract {get;set;}
        @AuraEnabled public MonthlyScenarioWrapper msPerformanceLimit {get;set;}
        @AuraEnabled public MonthlyScenarioWrapper msUsageSincePR {get;set;}
        @AuraEnabled public MonthlyScenarioWrapper msProjected {get;set;}
        @AuraEnabled public MonthlyScenarioWrapper msShortfall {get;set;}

    	public LLPSummaryWrapper(){
    		this.msConditionPerContract = new MonthlyScenarioWrapper();
            this.msPerformanceLimit = new MonthlyScenarioWrapper();
            this.msUsageSincePR = new MonthlyScenarioWrapper();
            this.msProjected = new MonthlyScenarioWrapper();
            this.msShortfall = new MonthlyScenarioWrapper();
    	}
    }
    public class MonthlyScenarioWrapper{
        @AuraEnabled public Decimal month {get;set;}
        @AuraEnabled public Decimal cycle{get; set;}
        @AuraEnabled public Decimal hour{get; set;}
        
    }
}