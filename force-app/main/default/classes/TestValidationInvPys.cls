@isTest
public class TestValidationInvPys {
    
    @testSetup
    static void setup(){
        
	    LeaseWareUtils.clearFromTrigger(); 
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
        insert aircraft;
        
         Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;
       
        
	    LeaseWareUtils.clearFromTrigger(); 
        Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,Lease_End_Date_New__c=Date.today().addYears(10),Lease_Start_Date_New__c=Date.today());
        insert lease;
   		
        
        
	    LeaseWareUtils.clearFromTrigger(); 
        Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                                            Name='Rent Schedule Fixed',
                                            Lease__c=lease.Id,
                                            Rent_Type__c='Fixed Rent',
                                            Rent_Period__c='Monthly',
                                            Base_Rent__c=200000.00,
    										Start_Date__c=Date.today() , // YYYY-MM-DD
                                           	Rent_End_Date__c=Date.today().addYears(10) ,  // YYYY-MM-DD
                                            Invoice_Generation_Day__c='15',
                                            Rent_Period_End_Day__c='14',
    										Rent_Due_Type__c='Fixed',
                                            Rent_Due_Day__c=15,
    										Escalation__c=3,
    										Escalation_Month__c='January',
    										Pro_rata_Number_Of_Days__c='Actual number of days in month/year');
	    insert MultiRentSch;
        
        
	    LeaseWareUtils.clearFromTrigger(); 
        rent__c rent2 = new rent__c(RentPayments__c= lease.id,Name='New Rent 1',For_Month_Ending__c=system.today().addMonths(1).toStartOfMonth().addDays(-1),start_date__c=system.today().toStartOfMonth(),Stepped_Rent__c =MultiRentSch.id);
        insert rent2;
        
        Invoice__c invoice2 = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rent2.id,amount__c = rent2.Rent_Due_Amount__c,status__c ='Pending',
                                            Date_of_MR_Payment_Due__c = Date.valueOf(datetime.newInstance(system.today().year(),system.today().month(), 17)),
                                           invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
        
	    LeaseWareUtils.clearFromTrigger(); 
        insert invoice2;
    }
    
    @isTest
    static void testManualUpdate(){
        Test.startTest();
         
        list<Invoice__c> invoices = [Select Id,Status__c From invoice__c];
        System.assertEquals(1, invoices.size());
        invoices.get(0).payment_status__c ='Paid';
        Boolean expectedExceptionThrown = false;
        
        
	    LeaseWareUtils.clearFromTrigger();
        try{
            update invoices;
        }
        catch(DMLException e){
            system.debug('e.getMessage()::::'+e.getMessage());
            expectedExceptionThrown =  e.getMessage().contains('Please create an approved payment relating to this invoice in order to update the Payment Status.') ? true : false ;
        }      
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
        }
        System.AssertEquals(expectedExceptionThrown, true); 
        expectedExceptionThrown = false;

        LeaseWareUtils.clearFromTrigger();
        invoices.get(0).payment_status__c ='Credited';
        try{
            update invoices;
        }
        catch(DMLException e){
            system.debug('e.getMessage()::::'+e.getMessage());
            expectedExceptionThrown =  e.getMessage().contains('Please create an approved payment relating to this invoice in order to update the Payment Status.') ? true : false ;
        }  
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
        }
        System.AssertEquals(expectedExceptionThrown, true); 
        Test.stopTest();
        
    }
    
    @isTest
    static void testPaymentCreation(){
        Test.startTest();
        Boolean expectedExceptionThrown = false;
        list<Invoice__c> invoices = [Select Id,Status__c From invoice__c];
        System.assertEquals(1, invoices.size());
       	
        
        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoices.get(0).id, payment_date__c=date.today(), 
                                                //RecordTypeId= Schema.SObjectType.payment__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId(),
                                                Amount__c=1600,payment_status__c='Pending');
        
        
	    LeaseWareUtils.clearFromTrigger();
        try{
            insert newPayment;
        }
        catch(DMLException e){
            system.debug('e.getMessage()::::'+e.getMessage());
            expectedExceptionThrown =  e.getMessage().contains('Payments can only be created on approved unpaid invoices.') ? true : false ;
        }  
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
        }     
        System.AssertEquals(expectedExceptionThrown, true);  
        expectedExceptionThrown = false;
        invoices.get(0).status__c ='Declined';
        try{
            insert newPayment;
        }
        catch(DMLException e){
            system.debug('e.getMessage()::::'+e.getMessage());
            expectedExceptionThrown =  e.getMessage().contains('Payments can only be created on approved unpaid invoices.') ? true : false ;
           
        }  
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
        }
        System.AssertEquals(expectedExceptionThrown, true); 
        Test.stopTest();
        
    }
     @isTest
    static void testAmountPaid(){
        Test.startTest();
         
        list<Invoice__c> invoices = [Select Id,Status__c,amount__c From invoice__c];
        System.assertEquals(1, invoices.size());
        system.debug('Invoice Amount:::'+invoices.get(0).amount__c);
        invoices.get(0).status__c ='Approved';
        Boolean expectedExceptionThrown =false;
        
        
	    LeaseWareUtils.clearFromTrigger();
        update invoices;
		Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoices.get(0).id, payment_date__c=date.today(), 
                                                //RecordTypeId= Schema.SObjectType.payment__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId(),
                                                Amount__c=200001,payment_status__c='Pending');
        
        
	    LeaseWareUtils.clearFromTrigger();
        try{
            insert newPayment;
        }
        catch(DMLException e){
            system.debug('e.getMessage()::::'+e.getMessage());
            expectedExceptionThrown =  e.getMessage().contains('The amount paid/credited should be less than or equal to the total invoice amount') ? true : false ;
           
        }  
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
        } 
        System.AssertEquals(expectedExceptionThrown, true); 
        expectedExceptionThrown =false;
        newPayment.amount__c = 100000;
        insert newPayment;
        LeaseWareUtils.clearFromTrigger();
        Payment__c newPayment1 = new Payment__c(Name='Test', Invoice__c=invoices.get(0).id, payment_date__c=date.today(),                                                 
                                                Amount__c=200001,payment_status__c='Approved');
         try{
            insert newPayment1;
        }
        catch(DMLException e){
            system.debug('e.getMessage()::::'+e.getMessage());
            expectedExceptionThrown =  e.getMessage().contains('The amount paid/credited should be less than or equal to the total invoice amount') ? true : false ;
            
        } 
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
        } 
        System.AssertEquals(expectedExceptionThrown, true);  
        
        LeaseWareUtils.clearFromTrigger();
        newPayment = new Payment__c(Name='Test', Invoice__c=invoices.get(0).id, payment_date__c=date.today(), 
                                                //RecordTypeId= Schema.SObjectType.payment__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId(),
                                                Amount__c=200000,payment_status__c='Approved');
        
        insert newPayment;
        Invoice__c inv = [Select Id,Status__c,payment_status__c,amount__c From invoice__c limit 1];
        System.assertEquals('Approved', inv.status__c);
        System.assertEquals('Paid', inv.payment_status__c);
        Test.stopTest();
    }
    
    @isTest
    static void testPartialStatus(){
        Test.startTest();
         
        Invoice__c invoices = [Select Id,Status__c From invoice__c limit 1];
        Boolean expectedExceptionThrown = false;
       	
        invoices.status__c = 'Approved';
        LeaseWareUtils.clearFromTrigger();
        update invoices;
        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoices.id, payment_date__c=date.today(), 
                                                //RecordTypeId= Schema.SObjectType.payment__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId(),
                                                Amount__c=1600,payment_status__c='Pending');
        
        
	    LeaseWareUtils.clearFromTrigger();
        insert newPayment;
        invoices = [Select Id,Status__c,payment_status__c From invoice__c where id =:invoices.id];
        system.assertEquals('Approved', invoices.status__c);
        system.assertEquals('Open', invoices.payment_status__c);

        LeaseWareUtils.clearFromTrigger();
        delete newPayment;

        invoices = [Select Id,Status__c From invoice__c where id =:invoices.id];
        system.assertEquals('Approved', invoices.status__c);

        LeaseWareUtils.clearFromTrigger();
        Payment__c newPayment1 = new Payment__c(Name='Test', Invoice__c=invoices.id, payment_date__c=date.today(), 
                                                //RecordTypeId= Schema.SObjectType.payment__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId(),
                                                Amount__c=1600,payment_status__c='Approved');
        insert newPayment1;
        LeaseWareUtils.clearFromTrigger();
        invoices = [Select Id,Status__c,payment_status__c From invoice__c where id =:invoices.id];
        system.assertEquals('Partially Paid', invoices.payment_status__c);
        system.assertEquals('Approved', invoices.status__c);
        LeaseWareUtils.clearFromTrigger();
        try{
           delete newPayment1;
        }
        catch(DMLException e){
            system.debug('e.getMessage()::::'+e.getMessage());
            expectedExceptionThrown =  e.getMessage().contains('Only Pending/Cancelled Payments & Credits can be deleted') ? true : false ;
            
        } 
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
        }  
        System.AssertEquals(expectedExceptionThrown, true);                                           
        Test.stopTest();


    }
    @isTest
    static void testRentInvoiceCategory(){
        Test.startTest();
        Invoice__c invoice = [Select Id,invoice_type__c,invoice_category__c From invoice__c limit 1];
        Boolean expectedExceptionThrown = false;
        system.assertEquals(invoice.invoice_type__c, invoice.invoice_category__c);
        system.assertEquals('Rent',  invoice.invoice_category__c);
        
        LeaseWareUtils.clearFromTrigger(); 
        invoice.invoice_category__c = 'Other';
        try{
            update invoice;
         }
         catch(DMLException e){
             system.debug('e.getMessage()::::'+e.getMessage());
             expectedExceptionThrown =  e.getMessage().contains('Invoice Category can only be modified on \'Manual (Other)\' invoices. For all other types of invoices, this will be automatically set by the system.') ? true : false ;    
         } 
        System.AssertEquals(expectedExceptionThrown, true);
        test.stopTest();
    }
    
    @isTest
    static void testOtherInvoiceCategory(){
        Test.startTest();
        lease__c Lease = [select id from lease__c limit 1];
        Invoice__c invoice = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,status__c ='Pending',
                                            Date_of_MR_Payment_Due__c = Date.valueOf(datetime.newInstance(system.today().year(),system.today().month(), 17)),
                                            invoice_type__c = 'Other',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Other').getRecordTypeId());
        
	    LeaseWareUtils.clearFromTrigger(); 
        insert invoice;

        invoice = [Select Id,invoice_type__c,invoice_category__c from invoice__c where id =:invoice.id];
        Boolean expectedExceptionThrown = false;
        system.assertEquals(invoice.invoice_type__c, invoice.invoice_category__c);
        system.assertEquals('Other',  invoice.invoice_category__c);
        
        invoice.invoice_category__c = 'JCT Rental';
        LeaseWareUtils.clearFromTrigger(); 
        update invoice;
        system.assertEquals(invoice.invoice_type__c,'Other');
        system.assertEquals('JCT Rental',  invoice.invoice_category__c);
        test.stopTest();
    }
    
    
}