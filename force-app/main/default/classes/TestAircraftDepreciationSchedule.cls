/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAircraftDepreciationSchedule {

    static testMethod void CRUDTest() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createDepreciationSchedule(mapInOut);
    	Id DSID1 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;
    	TestLeaseworkUtil.createDepreciationSchedule3Years(mapInOut);
    	Id DSID2 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;    	
    	
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c.Insert',null);
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    Aircraft1.Depreciation_schedule__c = DSID1;
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Statutory_Depreciation_Schedule__c = DSID2;
	    Aircraft1.Tax_Depreciation_Schedule__c = DSID1;
	    
	    Insert     Aircraft1;  	         

		
	    LeaseWareUtils.clearFromTrigger();        

		AircraftTriggerHandler.createAircraftDS(Aircraft1.Id);
		
		
	    LeaseWareUtils.clearFromTrigger();         
        
        Aircraft_Depreciation_Schedule__c[] ASList = [select Id from Aircraft_Depreciation_Schedule__c where Aircraft__c = :Aircraft1.Id];
        system.assertEquals(ASList.size(),37);
        
        ASList[0].Depreciation__c = 2222;
        try{
        	Update ASList[0];
        	system.assertEquals(1,2);
        }catch(Exception ex){
        	system.debug('ex'+ex);
        }
        
		
	    LeaseWareUtils.clearFromTrigger();
	            
        try{
        	delete ASList[1];
        	system.assertEquals(1,2);
        }catch(Exception ex){
        	system.debug('ex'+ex);
        }
     
        
    }

    static testMethod void TaxValueDSTest() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createDepreciationSchedule(mapInOut);
    	Id DSID1 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;
    	TestLeaseworkUtil.createDepreciationSchedule3Years(mapInOut);
    	Id DSID2 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;    	
    	
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c.Insert',null);
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    Aircraft1.Depreciation_schedule__c = DSID1;
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Statutory_Depreciation_Schedule__c = DSID1;
	    Aircraft1.Tax_Depreciation_Schedule__c = DSID2;
	    Insert     Aircraft1;  	         

		
	    LeaseWareUtils.clearFromTrigger();        

		
	    LeaseWareUtils.clearFromTrigger();        
        
        AircraftTriggerHandler.createAircraftDS(Aircraft1.Id);
        
        Aircraft_Depreciation_Schedule__c[] ASList = [select Id from Aircraft_Depreciation_Schedule__c where Aircraft__c = :Aircraft1.Id];
        system.assertEquals(ASList.size(),37);
    }
    
    static testMethod void UsingAircraftLeaseTest() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createDepreciationSchedule(mapInOut);
    	Id DSID1 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;
		system.debug('DSID1'+DSID1);
    	//Lease__c lease1 = (Lease__c)mapInOut.get('Lease__c');
    	
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Depreciation_schedule__c = DSID1;
        //Aircraft1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
	    update     Aircraft1;  	         

        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id');
        
		mapInOut.put('Lease__c.Aircraft__c',Aircraft1.Id);
		mapInOut.put('Lease__c.Operator__c',OperatorId1);
		mapInOut.put('Lease__c.Name','Lease1');	 
		//mapInOut.put('Lease__c',null);		
		TestLeaseworkUtil.createLease(mapInOut);
	    
		
	    LeaseWareUtils.clearFromTrigger();		
	    
		Aircraft__c ac1 = [select Id,Date_Acquired__c from Aircraft__c where Id=:Aircraft1.Id];
		
	    LeaseWareUtils.clearFromTrigger();	
	    system.debug('start DS generation');    
    
	    ac1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    update ac1;
		
	    LeaseWareUtils.clearFromTrigger();		    
		String msg=AircraftTriggerHandler.createAircraftDS(ac1.Id);		
		system.debug('status msg'+msg);
		
	    LeaseWareUtils.clearFromTrigger();        
        
        Aircraft_Depreciation_Schedule__c[] ASList = [select Id from Aircraft_Depreciation_Schedule__c where Aircraft__c = :ac1.Id];
        system.debug('ASList.size()'+ASList.size());
        system.assertEquals(ASList.size(),25);
        

        
    }  
    
    static testMethod void UsingLeaseTest() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createDepreciationSchedule(mapInOut);
    	Id DSID1 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;

    	//Lease__c lease1 = (Lease__c)mapInOut.get('Lease__c');
    	
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Depreciation_schedule__c = DSID1;
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    update     Aircraft1; 
	    
		
	    LeaseWareUtils.clearFromTrigger();	    
	    //Aircraft__c acc1 = [select Id from Aircraft__c where Id=:Aircraft1.Id];
        //acc1.Y_Hidden_Generate_Depreciation_Schedule__c=true;	
        //update     acc1; 
		AircraftTriggerHandler.createAircraftDS(Aircraft1.Id);		            
		
	    LeaseWareUtils.clearFromTrigger();	     	         

        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id');
        
		mapInOut.put('Lease__c.Aircraft__c',Aircraft1.Id);
		mapInOut.put('Lease__c.Operator__c',OperatorId1);
		mapInOut.put('Lease__c.Name','Lease1');	 
		//mapInOut.put('Lease__c',null);		
		TestLeaseworkUtil.createLease(mapInOut);

		
	    LeaseWareUtils.clearFromTrigger();        
        
		Lease__c ac1 = [select Id from Lease__c ];
	    ac1.Lease_End_Date_New__c = system.today().addYears(4);
		update ac1;        

		
	    LeaseWareUtils.clearFromTrigger();
	            
        Lease_Depreciation_Schedule__c[] ASList = [select Id from Lease_Depreciation_Schedule__c ];
        system.assertEquals(ASList.size(),25);
        
        ASList[0].Depreciation__c = 2222;
        try{
        	Update ASList[0];
        	system.assertEquals(1,2);
        }catch(Exception ex){
        	system.debug('ex'+ex);
        }
        
		
	    LeaseWareUtils.clearFromTrigger();
	            
        try{
        	delete ASList[1];
        	system.assertEquals(1,2);
        }catch(Exception ex){
        	system.debug('ex'+ex);
        }
        
    }  


    static testMethod void CapitalizedCostTest() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createDepreciationSchedule(mapInOut);
    	Id DSID1 = (Id)mapInOut.get('Depreciation_schedule__c.Id')   ;

    	
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c.Insert',null);
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    Aircraft1.Depreciation_schedule__c = DSID1;
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Statutory_Depreciation_Schedule__c = DSID1;
	    Aircraft1.Tax_Depreciation_Schedule__c = DSID1;
	    Insert     Aircraft1;  	         

		
	    LeaseWareUtils.clearFromTrigger();        
        
		// create equip
		Equipment__c E1 = new Equipment__c(Name='Dummy'
					,Aircraft__c = Aircraft1.Id
					,Part_Number__c = '1111'
					,Serial_Number__c = '2222'); 
		insert 	E1;
		
	    LeaseWareUtils.clearFromTrigger(); 
	    // Capitalized costs
	    Capitalized_Cost__c CC1 = new Capitalized_Cost__c(Name='Dummy'
	    			,Aircraft__c = Aircraft1.Id
	    			,Equipment__c = E1.Id
	    			,Start_Date__c = system.today()
	    			,Book_Value__c =12000
	    			,Depreciation_Schedule__c =DSID1
	    			,Statutory_Value__c =12000
	    			,Statutory_Depreciation_Schedule__c = DSID1
	    			,Tax_Value__c = 12000
	    			,Tax_Depreciation_Schedule__c =DSID1 );
	    				       
		insert 	 CC1 ;    				       
		
	    LeaseWareUtils.clearFromTrigger(); 

        AircraftTriggerHandler.createAircraftDS(Aircraft1.Id);		
        Aircraft_Depreciation_Schedule__c[] ASList = [select Id from Aircraft_Depreciation_Schedule__c where Aircraft__c = :Aircraft1.Id];
        system.assertEquals(ASList.size(),25);
        
        
        // Capitalised Cost some CRUD Operation
		
	    LeaseWareUtils.clearFromTrigger();        
        CC1.Tax_Value__c =24000;
        CC1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
        update CC1;
		
	    LeaseWareUtils.clearFromTrigger();        
        delete CC1;
    }

    static testMethod void CapitalizedCost_DepMethodTest_Part1() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	Depreciation_schedule__c DS_SL= new Depreciation_schedule__c(name = 'SL', Depreciation_Method__c ='Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_SL;
        DS_SL.Status__c ='Active';
        update DS_SL;
   	
   		Id  DSID1 = DS_SL.Id;
    	
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c.Insert',null);
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    Aircraft1.Depreciation_schedule__c = DSID1;
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Statutory_Depreciation_Schedule__c = DSID1;
	    Aircraft1.Tax_Depreciation_Schedule__c = DSID1;
	    Insert     Aircraft1;  	         

		
	    LeaseWareUtils.clearFromTrigger();        
        
		// create equip
		Equipment__c E1 = new Equipment__c(Name='Dummy'
					,Aircraft__c = Aircraft1.Id
					,Part_Number__c = '1111'
					,Serial_Number__c = '2222'); 
		insert 	E1;
		
	    LeaseWareUtils.clearFromTrigger(); 
	    // Capitalized costs
	    Capitalized_Cost__c CC1 = new Capitalized_Cost__c(Name='Dummy'
	    			,Aircraft__c = Aircraft1.Id
	    			,Equipment__c = E1.Id
	    			,Start_Date__c = system.today()
	    			,Book_Value__c =12000
	    			,Depreciation_Schedule__c =DSID1
	    			,Statutory_Value__c =12000
	    			,Statutory_Depreciation_Schedule__c = DSID1
	    			,Tax_Value__c = 12000
	    			,Tax_Depreciation_Schedule__c =DSID1 );
	    				       
		insert 	 CC1 ;    				       
		
	    LeaseWareUtils.clearFromTrigger(); 

        // Capitalised Cost some CRUD Operation
		
	    LeaseWareUtils.clearFromTrigger();        
        CC1.Tax_Value__c =24000;
        CC1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
        update CC1;
        
        
    	Depreciation_schedule__c DS_DDB= new Depreciation_schedule__c(name = 'Double Declining Balance', Depreciation_Method__c ='Double Declining Balance'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_DDB;
        DS_DDB.Status__c ='Active';
        update DS_DDB;
   	
   		DSID1 = DS_DDB.Id;        

        // Capitalised Cost some CRUD Operation
		
	    LeaseWareUtils.clearFromTrigger();        
        CC1.Depreciation_Schedule__c =DSID1;
        CC1.Statutory_Depreciation_Schedule__c =DSID1;
        CC1.Tax_Depreciation_Schedule__c =DSID1;
        CC1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
        update CC1;

    	DS_DDB= new Depreciation_schedule__c(name = '150% Declining Balance', Depreciation_Method__c ='150% Declining Balance'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_DDB;
        DS_DDB.Status__c ='Active';
        update DS_DDB;
   	
   		DSID1 = DS_DDB.Id;        

        // Capitalised Cost some CRUD Operation
		
	    LeaseWareUtils.clearFromTrigger();        
        CC1.Depreciation_Schedule__c =DSID1;
        CC1.Statutory_Depreciation_Schedule__c =DSID1;
        CC1.Tax_Depreciation_Schedule__c =DSID1;
        CC1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
        update CC1;
        
    	DS_DDB= new Depreciation_schedule__c(name = 'Hybrid - DDB And Straight Line', Depreciation_Method__c ='Hybrid - DDB And Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_DDB;
        DS_DDB.Status__c ='Active';
        update DS_DDB;
   	
   		DSID1 = DS_DDB.Id;        

        // Capitalised Cost some CRUD Operation
		
	    LeaseWareUtils.clearFromTrigger();        
        CC1.Depreciation_Schedule__c =DSID1;
        CC1.Statutory_Depreciation_Schedule__c =DSID1;
        CC1.Tax_Depreciation_Schedule__c =DSID1;
        CC1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
        update CC1;        

    	DS_DDB= new Depreciation_schedule__c(name = 'Hybrid - 150% DB And Straight Line', Depreciation_Method__c ='Hybrid - 150% DB And Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_DDB;
        DS_DDB.Status__c ='Active';
        update DS_DDB;
   	
   		DSID1 = DS_DDB.Id;        

        // Capitalised Cost some CRUD Operation
		
	    LeaseWareUtils.clearFromTrigger();        
        CC1.Depreciation_Schedule__c =DSID1;
        CC1.Statutory_Depreciation_Schedule__c =DSID1;
        CC1.Tax_Depreciation_Schedule__c =DSID1;
        CC1.Y_Hidden_Generate_Depreciation_Schedule__c=true;
        update CC1;        

    }

    static testMethod void DeprMethodTest_Part1() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	Depreciation_schedule__c DS_SL= new Depreciation_schedule__c(name = 'SL', Depreciation_Method__c ='Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_SL;
        DS_SL.Status__c ='Active';
        update DS_SL;
        
    	Depreciation_schedule__c DS_DDB= new Depreciation_schedule__c(name = 'Double Declining Balance', Depreciation_Method__c ='Double Declining Balance'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_DDB;
        DS_DDB.Status__c ='Active';
        update DS_DDB;

    	Depreciation_schedule__c DS_150DB= new Depreciation_schedule__c(name = '150% Declining Balance', Depreciation_Method__c ='150% Declining Balance'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_150DB;
        DS_150DB.Status__c ='Active';
        update DS_150DB;
                
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c.Insert',null);
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Depreciation_Schedule__c = DS_SL.Id;
	    Aircraft1.Statutory_Depreciation_Schedule__c = DS_DDB.Id;
	    Aircraft1.Tax_Depreciation_Schedule__c = DS_150DB.Id;
	    
	    Insert     Aircraft1;  	         

		
	    LeaseWareUtils.clearFromTrigger();        

		AircraftTriggerHandler.createAircraftDS(Aircraft1.Id);
		
		
	    LeaseWareUtils.clearFromTrigger();         
        
        Aircraft_Depreciation_Schedule__c[] ASList = [select Id from Aircraft_Depreciation_Schedule__c where Aircraft__c = :Aircraft1.Id];
        system.assertEquals(ASList.size(),37);
    }
 
 	//Hybrid - 150% DB And Straight Line,Hybrid - DDB And Straight Line
    static testMethod void DeprMethodTest_Part2() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	Depreciation_schedule__c DS_SL= new Depreciation_schedule__c(name = 'SL', Depreciation_Method__c ='Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_SL;
        DS_SL.Status__c ='Active';
        update DS_SL;
        
    	Depreciation_schedule__c DS_DDB= new Depreciation_schedule__c(name = 'Hybrid - DDB And Straight Line', Depreciation_Method__c ='Hybrid - DDB And Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_DDB;
        DS_DDB.Status__c ='Active';
        update DS_DDB;

    	Depreciation_schedule__c DS_150DB= new Depreciation_schedule__c(name = 'Hybrid - 150% DB And Straight Line', Depreciation_Method__c ='Hybrid - 150% DB And Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_150DB;
        DS_150DB.Status__c ='Active';
        update DS_150DB;
                
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c.Insert',null);
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Depreciation_Schedule__c = DS_SL.Id;
	    Aircraft1.Statutory_Depreciation_Schedule__c = DS_DDB.Id;
	    Aircraft1.Tax_Depreciation_Schedule__c = DS_150DB.Id;
	    
	    Insert     Aircraft1;  	         

		
	    LeaseWareUtils.clearFromTrigger();        

		AircraftTriggerHandler.createAircraftDS(Aircraft1.Id);
		
		
	    LeaseWareUtils.clearFromTrigger();         
        
        Aircraft_Depreciation_Schedule__c[] ASList = [select Id from Aircraft_Depreciation_Schedule__c where Aircraft__c = :Aircraft1.Id];
        system.assertEquals(ASList.size(),37);
    } 
     
    static testMethod void updateADSBasedOnImpairmentFu_Test() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	Depreciation_schedule__c DS_SL= new Depreciation_schedule__c(name = 'SL', Depreciation_Method__c ='Straight Line'
    		,Number_of_Period_Years__c = 3
            ,Status__c = 'New');
        insert DS_SL;
        DS_SL.Status__c ='Active';
        update DS_SL;
        

                
    	mapInOut.put('Aircraft__c.msn','MSN1');
    	mapInOut.put('Aircraft__c.Insert',null);
    	mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        Aircraft__c Aircraft1 = (Aircraft__c) mapInOut.get('Aircraft__c');
        System.debug('Aircraft1' + Aircraft1);
		
	    LeaseWareUtils.clearFromTrigger();
	    Aircraft1.Date_Acquired__c = system.today().toStartOfMonth().addDays(5);
	    Aircraft1.Equipment_Cost__c = 10000;
	    Aircraft1.Depreciation_Schedule__c = DS_SL.Id;
	    Aircraft1.Statutory_Depreciation_Schedule__c = DS_SL.Id;
	    Aircraft1.Tax_Depreciation_Schedule__c = DS_SL.Id;
	    
	    Insert     Aircraft1;  	         

		
	    LeaseWareUtils.clearFromTrigger();        

		AircraftTriggerHandler.createAircraftDS(Aircraft1.Id);
		
		
	    LeaseWareUtils.clearFromTrigger();         
        LeaseWareUtils.setFromTrigger('InsertedFromAircraftTriggerHandler');
        Aircraft_Depreciation_Schedule__c[] ASList = [select Id from Aircraft_Depreciation_Schedule__c where Aircraft__c = :Aircraft1.Id order by Period_Sequence__c];
        ASList[0].Impairment_Value__c =10000;
        update ASList[0];
        system.assertEquals(ASList.size(),37); 
		
	    LeaseWareUtils.clearFromTrigger();            	
    	AircraftDepreciationScheduleHandler.updateADSBasedOnImpairmentFuture(Aircraft1.Id,1,null) ;
    } 
     
}