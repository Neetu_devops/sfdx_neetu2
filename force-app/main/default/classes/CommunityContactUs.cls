public with sharing class CommunityContactUs {
    
    // method to get current user information  
    @AuraEnabled
    public static String getContactUsInfo(String department, String mailSubject) {
        System.debug('getContactUsInfo department:'+department+ ' ,mailSubject:' +mailSubject);
        List<ContactUsController.ContactWrapper> contactList = ContactUsController.getContacts();
        String email = null;
        if(contactList != null) {
            for(ContactUsController.ContactWrapper contactWrapper: contactList) {
                if(department != null && department.equalsIgnoreCase(contactWrapper.category)) {
                    email = contactWrapper.email;
                }
            }
        }    
        // query current user information  
        List<User> oUser = [select id, Name, CompanyName, FirstName, LastName 
                    FROM User Where id =: userInfo.getUserId()];
        String userInfo = '';
        if(oUser.size() > 0 ) {
            User ur = oUser[0];
            if(ur.FirstName != null) {
                userInfo = ur.FirstName + ' '+ ur.LastName;
            }
            else { userInfo = ur.LastName; }
            if(ur.CompanyName != null) {
                userInfo = userInfo + ', '+ur.CompanyName;
            }
        }
        String emailContent = '?subject='+(String.isNotEmpty(mailSubject) ? mailSubject+' - ': '')+userInfo;
        if(email != null)  {
            emailContent = email + emailContent;
        }

        return emailContent;
    }
}
