public class NewsComponentCntrl
{
    @TestVisible
    private Id recordId {get;set;}
   @TestVisible
   private list<NewsArticleWrapper> articles               {get;set;}
   @TestVisible
   private string defaultMsg                           {get;set;}
   @TestVisible
   private string aviatortoken                         {get;set;}
   @TestVisible
   private string requestAviatorUrl                    {get;set;}
   @TestVisible
   private boolean showMore                            {get;set;}
   @TestVisible
   private string aircraftTypes                        {get;set;}
   @TestVisible
   private string capaURL							   {get;set;}
   @TestVisible
   private string capaToken							   {get;set;}
    private static string operatorNameFilter = LeaseWareUtils.getLWSetup_CS('OPERATOR_NAME_FILTER') ;
    private List<String>  operatorNames = new List<String>();
    List<String> listNewsSource = new List<String>();
    boolean isBoth = false;
    private static boolean opName = false;
    private final string  CAPANews = 'CAPA-News';
    private final string  CAPAReports = 'CAPA-Reports';
    private final string  Name = 'World Fleet Name/Name';
    Integer missingArticles = 0;
    public NewsComponentCntrl()
    {
        showMore = false;
        articles     = new list<NewsArticleWrapper>();
        //defaultMsg = NewsFeedServices.NONEWSFOUNDMSG;       
        aviatorToken = NewsFeedServices.requestAviatorApiToken();
        requestAviatorUrl = NewsFeedServices.requestAviatorUrl();
        capaToken = CAPANewsFeedService.requestCAPAApiToken();
        capaURL = CAPANewsFeedService.requestCAPAEndpointUrl();      
    }
    
    public String getNewFeeds(String newsSource,Id recordId,Integer pageNumber, Integer pageSize,String searchKeywrd, 
                                String startDt,String endDt,String fieldName,String regionValue, String categoriesValue)
    {  
        system.debug('newsSource=='+newsSource);
        system.debug('recordId=='+recordId);
        listNewsSource.add(newsSource);

        if(Name.equals(fieldName)) {opName = true;}

        // If recordId is null call home feed service
        if(recordId == null){
            articles = getNewsFeedForHome(newsSource,recordId,pageNumber,pageSize,searchKeywrd, startDt,endDt,fieldName,regionValue,categoriesValue);
        }else{
            articles = getNewsFeedForOperator(newsSource,recordId,pageNumber,pageSize,searchKeywrd, startDt,endDt,fieldName,regionValue,categoriesValue);
        }
        Integer articlesReceived = articles.size();
        List<NewsWrapper> articleList = new List<NewsWrapper>();
			for(NewsArticleWrapper rec:articles ) {
				articleList.add(new NewsWrapper(rec));
			}
			articleList.sort();
			articles.clear();
			for(NewsWrapper curRec: articleList) {
				articles.add(curRec.curNews);
            }
            if(articles.size()>0){
                articles[0].articlesNotReturned = missingArticles;
            }
        return Json.serialize(articles);
    } 

    // Filter articles for operator and alias name.  
    // IN : list of articles from service 
    // OUT : returning selected articles to display
    private List<NewsArticleWrapper> filterArticles(list<NewsArticleWrapper> articles,String newsSource){

        Set<NewsArticleWrapper> setOfNews = new Set<NewsArticleWrapper>();
        Set<NewsArticleWrapper> finalSetOfNews = new Set<NewsArticleWrapper>();
        List<String> listTagName = new List<String>();
        System.debug('operatorNames:' + operatorNames);
        String operatorName = '',operatorLesse = '',operatorWF = '';
        if(operatorNames.size()>0) {operatorName = operatorNames[0];}
        if(operatorNames.size()>1) {operatorLesse = operatorNames[1];}
        if(operatorNames.size()>2) {operatorWF = operatorNames[2];}

        for(NewsArticleWrapper data : articles){
            boolean isOPpresent = false ,isAliaspresent = false,isOLpresent = false ,isOWFpresent = false; 
            if((newsSource.equalsIgnoreCase(CAPANews) ||  newsSource.equalsIgnoreCase(CAPAReports)) && String.isNotBlank(data.tag)){
               listTagName = data.tag.split(',');
            }
            if(data.article != null){
                if(String.isNotBlank(operatorWF)){
                    System.debug('Searching for WF name in Cntrl=' + operatorWF);
                    isOWFpresent =  Pattern.compile('(?i)\\b'+operatorWF.replace('(', 'x').replace(')', 'x')+'\\b').matcher(data.article.replace('(', 'x').replace(')', 'x')).find() ;
                    if(!isOWFpresent){
                        for(String str : listTagName){
                            System.debug('Searching for TAG in WF name =' + str);
                            isOWFpresent =  Pattern.compile('(?i)\\b'+operatorWF.replace('(', 'x').replace(')', 'x')+'\\b').matcher(str.replace('(', 'x').replace(')', 'x')).find() ;
                            if(isOWFpresent){break;}
                        }
                    }
                }
                if(String.isNotBlank(operatorName) && ((opName && String.isBlank(operatorWF)) || !opName)){ // looking for operator name match
                    System.debug('Searching for operator name in Cntrl=' + operatorName);
                    isOPpresent =  Pattern.compile('(?i)\\b'+operatorName.replace('(', 'x').replace(')', 'x')+'\\b').matcher(data.article.replace('(', 'x').replace(')', 'x')).find() ;
                    if(!isOPpresent){
                        for(String str : listTagName){
                            isOPpresent =  Pattern.compile('(?i)\\b'+operatorName.replace('(', 'x').replace(')', 'x')+'\\b').matcher(str.replace('(', 'x').replace(')', 'x')).find() ;
                            if(isOPpresent){break;}
                        }
                    }
                }
                if(String.isNotBlank(operatorLesse)  && !opName){ // looking for operator lessee name match
                    isOLpresent =  Pattern.compile('(?i)\\b'+operatorLesse.replace('(', 'x').replace(')', 'x')+'\\b').matcher(data.article.replace('(', 'x').replace(')', 'x')).find() ;
                    if(!isOLpresent){
                        for(String str : listTagName){
                           // System.debug('Searching for TAG name =' + str); // looking for operator WF name match
                            isOLpresent =  Pattern.compile('(?i)\\b'+operatorLesse.replace('(', 'x').replace(')', 'x')+'\\b').matcher(str.replace('(', 'x').replace(')', 'x')).find() ;
                            if(isOLpresent){break;}
                        }
                    }
                }
                if(isOPpresent || isOLpresent || isOWFpresent ) {    
                    setOfNews.add(data);  
                }    
            }
        }
        System.debug('setOfNews:' + setOfNews.size());
        Set<NewsArticleWrapper> setOfNews2 = new Set<NewsArticleWrapper>();
        Set<NewsArticleWrapper> setOfNews3 = new Set<NewsArticleWrapper>();
        for(NewsArticleWrapper data : articles){
            if(!setOfNews.contains(data)) {setOfNews2.add(data);}
        }

        finalSetOfNews.addAll(setOfNews);
        setOfNews.clear();
        if(!opName){
            for(Integer i = 3 ; i< operatorNames.size() ; i++ ){
                System.debug('Alias name:' + operatorNames[i]);
                
                if(operatorNames[i] == null) {continue ;}
             
                System.debug('setOfNews2:' + setOfNews2.size());
                for(NewsArticleWrapper data : setOfNews2){
                    if((newsSource.equalsIgnoreCase(CAPANews) ||  newsSource.equalsIgnoreCase(CAPAReports)) && String.isNotBlank(data.tag)){
                        listTagName = data.tag.split(',');
                     }
                     if(data.article != null){
                         if(String.isNotBlank(operatorNames[i])){
                           System.debug('operator alias name:' + operatorNames[i]); 
                             if(Pattern.compile('(?i)\\b'+ operatorNames[i].replace('(', 'x').replace(')', 'x')+'\\b').matcher(data.article.replace('(', 'x').replace(')', 'x')).find()) {
                                 setOfNews.add(data);
                             }else{
                                for(String str : listTagName){
                                    System.debug('Searching for TAG in alias name =' + str);
                                    if(Pattern.compile('(?i)\\b'+operatorNames[i].replace('(', 'x').replace(')', 'x')+'\\b').matcher(str.replace('(', 'x').replace(')', 'x')).find()){
                                       // System.debug('Addding from Alias --' + data.articleId +'---'+ data.title.substring(0,40));
                                        setOfNews.add(data);
                                        break;
                                    }
                                }
                             }
                         }
                     }
                 } 
                 if (setOfNews.size() == 0 ) {continue ;}
                 for(NewsArticleWrapper data : setOfNews2){
                     if(!setOfNews.contains(data)) {setOfNews3.add(data);}
                 }
                 finalSetOfNews.addAll(setOfNews);
                 setOfNews.clear();
                 setOfNews2.clear();
                 setOfNews2 = new Set<NewsArticleWrapper>(setOfNews3);
                 System.debug('setOfNews2:' + setOfNews2.size());
             }        
         }
         System.debug('finalSetOfNews:' + finalSetOfNews.size());
         List<NewsArticleWrapper> listOfNews = new List<NewsArticleWrapper>(finalSetOfNews);
         System.debug('listOfNews:' + listOfNews.size()); 
         return listOfNews;
        }
  
    // this method calls service from operator page
    private List<NewsArticleWrapper> getNewsFeedForOperator(String newsSource,Id recordId,Integer pageNumber, Integer pageSize,String searchKeywrd,
                                                            String startDt, String endDt,String fieldName,String regionValue, String categoriesValue){                       
        if(newsSource.equalsIgnoreCase(CAPANews) ||  newsSource.equalsIgnoreCase(CAPAReports)){
            System.debug('Calling news Feed service method From CAPANewsFeedService ---' + listNewsSource);
            CAPANewsFeedService capaService  = new CAPANewsFeedService(); 
            String response = capaService.getNewsAndReportsFeed( recordId , capaURL , capaToken , newsSource,pageNumber,pageSize,fieldName,startDt,endDt); 
            if(String.isNotBlank(response)){
                articles = (List<NewsArticleWrapper>) System.Json.deserialize(response, List<NewsArticleWrapper>.class);
            }else{
                throw new AuraHandledException ('Please check Design Parameters/Token/EndpointUrl, for any missing values. ');
            }  
            operatorNames = capaService.getOperatorName(); 
        } else if( newsSource.equalsIgnoreCase('Aviator')) {
            System.debug('Region value in Controller :' + regionValue);
            NewsFeedServices newsService  = new NewsFeedServices();
            String response = newsService.getNewsArticles( recordId , aircraftTypes ,requestAviatorUrl, aviatorToken, pageNumber, pageSize,
                                                                    searchKeywrd,startDt,endDt,fieldName,regionValue, categoriesValue);      
            if(String.isNotBlank(response)){                                               
                articles = (List<NewsArticleWrapper>) System.Json.deserialize(response, List<NewsArticleWrapper>.class); 
            }else{
                throw new AuraHandledException ('Please check Design Parameters/Token/EndpointUrl, for any missing values. ');
            }  
            operatorNames = newsService.getOperatorName(); 
        }
        List<NewsArticleWrapper> formattedArticles = new List<NewsArticleWrapper>();
        
        for(NewsArticleWrapper data : articles){
            if(data != null && data.article != null){
                data.article = data.article.stripHtmlTags();  
                formattedArticles.add(data); 
            }       
        }
        if (String.isNotBlank(operatorNameFilter) && operatorNameFilter.equalsIgnoreCase('ON')){
            System.debug('articles in : '+formattedArticles.size());
            if(newsSource.equalsIgnoreCase(CAPANews)){
                formattedArticles = filterArticles(formattedArticles,newsSource);
            }
            System.debug('articles out : '+formattedArticles.size());
        }
        if(articles.size()>0 && formattedArticles.size()>0){
            missingArticles = (articles.size() - formattedArticles.size());
        }
        return formattedArticles;
    }

    // calles service from Home and App page
    private List<NewsArticleWrapper> getNewsFeedForHome(String newsSource,Id recordId,Integer pageNumber, Integer pageSize, String searchKeywrd,
                                                        String startDt, String endDt,String fieldName,String regionValue,String categoriesValue ){     
        String response = '';
        if(newsSource.equalsIgnoreCase(CAPANews) ||  newsSource.equalsIgnoreCase(CAPAReports)){
            System.debug('Calling news Feed service method From CAPANewsFeedService For Home');
            CAPANewsFeedService capaService  = new CAPANewsFeedService();
            response = capaService.getNewsAndReportsFeed( recordId , capaURL , capaToken ,newsSource,pageNumber,pageSize,fieldName,startDt,endDt);     
        } else if(newsSource.equalsIgnoreCase('Aviator')) {
            System.debug('Calling news Feed service method From AviatorFeedService For Home');
            NewsFeedServices newsService = new NewsFeedServices();
            response = newsService.getNewsArticles( recordId , aircraftTypes , requestAviatorUrl,aviatorToken 
                                                            ,pageNumber,pageSize,searchKeywrd,startDt,endDt,fieldName,regionValue,categoriesValue); 
        }  
        if(String.isNotBlank(response)){ 
            articles = (List<NewsArticleWrapper>) System.Json.deserialize(response, List<NewsArticleWrapper>.class);
        }else{
            throw new AuraHandledException ('Please check Design Parameters/Token/EndpointUrl, for any missing values.');
        }                 

        List<NewsArticleWrapper> formattedArticles = new List<NewsArticleWrapper>();
        for(NewsArticleWrapper data : articles){
            if(data != null && data.article != null ){
                data.article = data.article.stripHtmlTags(); 
                formattedArticles.add(data);  
            }                
        }
        if(articles.size()>0 && formattedArticles.size()>0){
            System.debug('came here home: ' + (articles.size() - formattedArticles.size()));
            missingArticles = (articles.size() - formattedArticles.size());
        }
        	return formattedArticles;
    }

    private class NewsWrapper implements Comparable {

		public NewsArticleWrapper curNews;
		public NewsWrapper (NewsArticleWrapper curNews){
			this.curNews = curNews;
		}
		public Integer compareTo(Object compareTo){
			NewsWrapper compareToNews = (NewsWrapper)compareTo;
			if(curNews.articleDate > compareToNews.curNews.articleDate) return -1;
			if(curNews.articleDate < compareToNews.curNews.articleDate) return 1;
			return 0;
		}
    }
    
    @testVisible
    public class NewsArticleWrapper {
        String name {get;set;}
        Id operator {get;set;}
        Id lessor {get;set;}
        Id setup {get;set;}
        @testVisible
        Date articleDate {get;set;}
        String article {get;set;}
        String tag {get;set;}
        String link {get;set;}
        String title {get;set;}
        String articleId {get;set;}
        string newsSource {get;set;}
        string fullArticle {get;set;}
        @testVisible
        boolean isLastPage {get;set;}
        String type {get; set;}
        Id newsId {get; set;}
        integer totalEntries {get;set;}
        integer page {get;set;}
        integer totalPages {get;set;}
        @testVisible
        integer articlesNotReturned {get;set;}
    }

}