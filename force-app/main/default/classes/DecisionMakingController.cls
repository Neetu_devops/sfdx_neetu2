public with sharing class DecisionMakingController {
    @AuraEnabled
    public static DecisionMakingWrapper getDecisionSummary(Id scenarioInp){
        try{
            //System.debug('scenarioInp: '+scenarioInp);
            Scenario_Input__c scenarioInpRec = [SELECT Id,Asset__c, Name, Lease__c, Lease__r.Name, Asset__r.Lease__r.Lease_End_Date_New__c, Number_Of_Months_For_Projection__c,
                                                Asset__r.Aircraft_Type__c, Asset__r.MSN_Number__c, Asset__r.Registration_Number__c,
                                                Asset__r.Lease__c, Asset__r.Lease__r.Name, NPV__c, IRR__c, Cost_per_FH_Maintenance__c,
                                                Cost_per_FH_Rent__c, Cost_Per_FH_Total__c, Net_EOLA_Payment__c, Maintence_Reserves_Balance__c,
                                                Re_Delivery_Condition_Status__c, Discount_Rate__c, Extension_Assumption__c, Base_Rent__c,
                                                Ext_Base_Rent__c, Investment_Required_Purchase_Price__c, Estimated_Residual_Value__c,Total_Restoration_Cost__c,
                                                Avg_CY_between_SV_Engine_1__c,Avg_CY_between_SV_Engine_2__c
                                                FROM Scenario_Input__c WHERE ID =: scenarioInp];
            
            DecisionMakingWrapper dmw = new DecisionMakingWrapper();
            if(scenarioInpRec.Asset__r.Lease__r.Lease_End_Date_New__c != null){
                Integer leaseDate= scenarioInpRec.Number_Of_Months_For_Projection__c  == null ? 0 : Integer.valueOf(scenarioInpRec.Number_Of_Months_For_Projection__c);
                dmw.leaseEndDate = scenarioInpRec.Asset__r.Lease__r.Lease_End_Date_New__c.addMonths(leaseDate).format();
            }
            else {
                dmw.leaseEndDate = '';
            }
            dmw.aircraftType = scenarioInpRec.Asset__r.Aircraft_Type__c;
            dmw.aircraftId = scenarioInpRec.Asset__c;
            dmw.serialNo = scenarioInpRec.Asset__r.MSN_Number__c; 
            dmw.registrationNo = scenarioInpRec.Asset__r.Registration_Number__c;
            
            dmw.leaseId = scenarioInpRec.Lease__c;
            dmw.leaseName = scenarioInpRec.Lease__r.Name;
            dmw.npv = scenarioInpRec.NPV__c;
            dmw.irr = scenarioInpRec.IRR__c;
            
            dmw.costPerFH = scenarioInpRec.Cost_per_FH_Maintenance__c;
            dmw.isLessor = Utility.checkAirline();
            dmw.costPerFHForRent = scenarioInpRec.Cost_per_FH_Rent__c;
            dmw.costPerFHForMain = scenarioInpRec.Cost_Per_FH_Total__c;
            
            dmw.netEOLAPayment = scenarioInpRec.Net_EOLA_Payment__c;
            dmw.maintenceReservesBalance = scenarioInpRec.Maintence_Reserves_Balance__c;
            dmw.avgCYEngine1 = scenarioInpRec.Avg_CY_between_SV_Engine_1__c;
            dmw.avgCYEngine2 = scenarioInpRec.Avg_CY_between_SV_Engine_2__c;
            
            dmw.status = scenarioInpRec.Re_Delivery_Condition_Status__c;
            dmw.discountRate = scenarioInpRec.Discount_Rate__c;
            dmw.timeSpan = scenarioInpRec.Number_Of_Months_For_Projection__c;
            dmw.isExtension = scenarioInpRec.Extension_Assumption__c;
            
            dmw.baseRent = scenarioInpRec.Base_Rent__c;
            dmw.proposedRent = scenarioInpRec.Ext_Base_Rent__c;
            dmw.investment = scenarioInpRec.Investment_Required_Purchase_Price__c;
            dmw.residualValue = scenarioInpRec.Estimated_Residual_Value__c;
            dmw.totalRestorationCost = scenarioInpRec.Total_Restoration_Cost__c;
            return dmw;
        }catch(Exception ex){
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    public class DecisionMakingWrapper{
        @auraEnabled public String leaseId{get; set;}
        @auraEnabled public String leaseName{get; set;}
        @auraEnabled public Decimal npv{get; set;}
        @auraEnabled public Decimal irr{get; set;}
        @auraEnabled public Decimal costPerFH{get; set;}
        @auraEnabled public Decimal costPerFHForRent{get; set;}
        @auraEnabled public Decimal costPerFHForMain{get; set;}
        @auraEnabled public Decimal netEOLAPayment{get; set;}
        @auraEnabled public Decimal maintenceReservesBalance{get; set;}
        @auraEnabled public Decimal avgCYEngine1{get; set;}
        @auraEnabled public Decimal avgCYEngine2{get; set;}
        @auraEnabled public String  status{get; set;}
        @auraEnabled public Decimal discountRate{get; set;}
        @auraEnabled public Decimal investment{get; set;}
        @auraEnabled public Decimal timeSpan{get; set;}
        @auraEnabled public Double residualValue{get; set;}
        @auraEnabled public String isLessor{get; set;}
        @auraEnabled public Boolean isExtension{get; set;}
        @auraEnabled public Decimal baseRent{get; set;}
        @auraEnabled public Decimal proposedRent{get; set;}
        @auraEnabled public Id aircraftId {get;set;}
        @auraEnabled public Decimal totalRestorationCost {get;set;}
        
        @auraEnabled public String leaseEndDate{get; set;}
        @auraEnabled public String aircraftType{get; set;}
        @auraEnabled public String serialNo{get; set;}
        @auraEnabled public String registrationNo{get; set;}
    }
}