public with sharing class UpdateSnapshotComments {
    
     @AuraEnabled
    public static String updateSnapshotCommentFunc(Id recordId, String comments) {
        system.debug('Comments------------' +comments + ', RecordId' +recordId);
        try{
        projected_event_snapshot__c snapshotObj = [select id,Snapshot_Comment__c from projected_event_snapshot__c where id=:recordId];
        snapshotObj.Snapshot_Comment__c = comments;
        upsert snapshotObj;
        return 'Snapshot Comment updated';
        }catch(Exception e){
            return e.getMessage();
        }
        

}
    
}