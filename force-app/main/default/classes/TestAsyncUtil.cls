@isTest
private class TestAsyncUtil {

	@isTest(seeAllData=false)
    static  void AsyncCallPE_Test() {
  
        test.startTest();
        list<string> listIds = new list<string>{'someid1','someid2'};
        AsyncCallPE__e  asyncEvent = new AsyncCallPE__e (
            Method_To_Call__c='CallABCMethod', 
            Impacted_Ids__c= JSON.serialize(listIds)
        );
        // Call method to publish events
        Database.SaveResult sr = EventBus.publish(asyncEvent);
        // Inspect publishing result 
        if (sr.isSuccess()) {
            System.debug('Successfully published event.');
        } else {
            for(Database.Error err : sr.getErrors()) {
                System.debug('Error returned: ' +
                            err.getStatusCode() +
                            ' - ' +
                            err.getMessage());
            }
        }   
        
        test.stopTest();

    }


}