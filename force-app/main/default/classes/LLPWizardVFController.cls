public class LLPWizardVFController {
    private Id parentId;
    private string RecordTypeName;
    public Constituent_Assembly__c CARec{get;set;}
    public list<LLPRecWrapper> LLPRecWrapperList {get;set;}
    private map<string,LLPRecWrapper> mapLLPRecWrapperList;
    private map<string,LLP_Template__c> mapLLPTemplateByPartNumber;
    private map<string,Sub_Components_LLPs__c> mapLLPSByPartName;
    
    public LLPWizardVFController(ApexPages.StandardController controller) {
      system.debug('LLPWizardVFController1+');
      
      system.debug('LLPWizardVFController1-');             
    }       
    public LLPWizardVFController(ApexPages.StandardSetController controller){   
        
        system.debug('LLPWizardVFController+');

        /* Below line will return selected URs, but i need parent id i.e. Acircraft id
            controller.getRecord()='+controller.getRecord()
        */
        parentId = Apexpages.currentPage().getParameters().get('id');// this will return Assembly Id
        system.debug('parentId='+parentId);

        mapLLPRecWrapperList = new map<string,LLPRecWrapper>();
        mapLLPTemplateByPartNumber = new map<string,LLP_Template__c>();
        initialize();
        LLPRecWrapperList = mapLLPRecWrapperList.values();
        system.debug('LLPWizardVFController-');                 
    }       

    private void initialize(){

        CARec = [select id,Name,Type__c,Engine_Template_CA__c,recordType.Name,recordType.DeveloperName 
                                         ,(select id,Name,Part_Number__c from Sub_Components_LLPs__r  )
                                         
                                         from Constituent_Assembly__c where id =:parentId];
        RecordTypeName = CARec.recordType.DeveloperName ;
        map<string,Integer> mapLLPTemplateCount = new map<string,Integer>();
        
        mapLLPSByPartName = new map<string,Sub_Components_LLPs__c>();
        
        for(Sub_Components_LLPs__c curLLP:CARec.Sub_Components_LLPs__r){
            mapLLPSByPartName.put(curLLP.Name,curLLP);
        }  
        system.debug('mapLLPSByPartName.keySet='+mapLLPSByPartName.keySet());
        LLP_Template__c[] listLLPs = [ select id,name,Disk_Stage__c,Module_N__c,Lead_Time__c,Life_Limit_Cycles__c
                                        ,Part_Number__c,Replacement_Cost__c,Replacement_Cost_Reset_Date__c,Date_Of_Load__c
                                      from LLP_Template__c 
                                      where Engine_Template__c =:CARec.Engine_Template_CA__c
                                      and name in :mapLLPSByPartName.keySet()
                                      order by name,CreatedDate desc];
        system.debug('listLLPs size='+listLLPs.size());                              
        // insert into mapLLPRecWrapperList
        LLPRecWrapper tempLLPRec;
        Integer counter=1;
        for(LLP_Template__c curLLPTempRec:listLLPs){
            if(mapLLPTemplateCount.containsKey(curLLPTempRec.Name)) mapLLPTemplateCount.put(curLLPTempRec.Name,mapLLPTemplateCount.get(curLLPTempRec.Name)+1);
            else mapLLPTemplateCount.put(curLLPTempRec.Name,1);
        }
        for(LLP_Template__c curLLPTempRec:listLLPs){
            if(mapLLPTemplateCount.get(curLLPTempRec.Name)<=1) continue;
            mapLLPTemplateByPartNumber.put(curLLPTempRec.Name+'-'+curLLPTempRec.Part_Number__c,curLLPTempRec);
            //if(mapLLPSByPartName.containsKey(curLLPTempRec.Name)) continue;
            if(mapLLPRecWrapperList.containsKey(curLLPTempRec.Name)){
                tempLLPRec=mapLLPRecWrapperList.get(curLLPTempRec.Name);
            }else{
                tempLLPRec=new LLPRecWrapper();
                tempLLPRec.SerialNumber = counter++;
                tempLLPRec.Part_Name = curLLPTempRec.Name;
                tempLLPRec.selectedPartNumber = mapLLPSByPartName.get(curLLPTempRec.Name).Part_Number__c;//curLLPTempRec.Part_Number__c;
                tempLLPRec.selectedLLPTemplate= curLLPTempRec;
            }
            
            tempLLPRec.partNumberOptions.add(new SelectOption( curLLPTempRec.Part_Number__c,curLLPTempRec.Part_Number__c ));
            mapLLPRecWrapperList.put(curLLPTempRec.Name,tempLLPRec );
        }
    }
    public void changeSelectedTemplate(){
        for(LLPRecWrapper currec: LLPRecWrapperList){
            currec.selectedLLPTemplate =  mapLLPTemplateByPartNumber.get(curRec.Part_Name+'-'+curRec.selectedPartNumber);
        }
    }
    public PageReference saveLLP(){
        boolean errorFlag=false;
        Savepoint sp = Database.setSavepoint();
        system.debug('saving...');
        list<Sub_Components_LLPs__c> listUpdLLPs = new list<Sub_Components_LLPs__c>();
        LLP_Template__c curLLPTmpl;
        //Revert back to getRecordTypeInfosByName since API Name is different on LLP
        Map<String,Schema.RecordTypeInfo> LLP_MapByName = Schema.SObjectType.Sub_Components_LLPs__c.getRecordTypeInfosByName();
        Id recordTypeId_LLP   =  LLP_MapByName.get(RecordTypeName).getRecordTypeId();
        try{
            Sub_Components_LLPs__c updLLP;
            for(LLPRecWrapper curRec  :LLPRecWrapperList){
                curLLPTmpl = currec.selectedLLPTemplate;//mapLLPTemplateByPartNumber.get(curRec.Part_Name+'-'+curRec.selectedPartNumber);
                system.debug('rec...' + curRec.SerialNumber + curRec.Part_Name + '=='+curRec.selectedPartNumber );
                updLLP = mapLLPSByPartName.get(curRec.Part_Name);
                /*Sub_Components_LLPs__c newLLP = new Sub_Components_LLPs__c(Constituent_Assembly__c=parentId, Name=curLLPTmpl.Name, 
                        RecordTypeId=recordTypeId_LLP, 
                        Disk_Stage__c=curLLPTmpl.Disk_Stage__c, Lead_Time__c=curLLPTmpl.Lead_Time__c, Life_Limit_Cycles__c=curLLPTmpl.Life_Limit_Cycles__c,
                        Part_Number__c=curLLPTmpl.Part_Number__c, Serial_Number__c='00000', CSN__c=0, Replacement_Cost__c=curLLPTmpl.Replacement_Cost__c,
                        Replacement_Cost_Reset_Date__c=curLLPTmpl.Replacement_Cost_Reset_Date__c);
                 */
                updLLP.Disk_Stage__c =curLLPTmpl.Disk_Stage__c;
                updLLP.Module_N__c =curLLPTmpl.Module_N__c;
                updLLP.Lead_Time__c =curLLPTmpl.Lead_Time__c;
                updLLP.Override_Life_Limit_Cycle__c =curLLPTmpl.Life_Limit_Cycles__c;
                updLLP.Part_Number__c =curLLPTmpl.Part_Number__c;
                updLLP.LLP_DB_LLP__c  =curLLPTmpl.Id;
                updLLP.Replacement_Cost__c =curLLPTmpl.Replacement_Cost__c;
                updLLP.Replacement_Cost_Reset_Date__c =curLLPTmpl.Replacement_Cost_Reset_Date__c;
                  
                listUpdLLPs.add(updLLP);
                
            }
            system.debug('listUpdLLPs'+listUpdLLPs.size());
            if(listUpdLLPs.size()>0)update listUpdLLPs;
        }catch(exception e){
            system.debug('Exception '+ e.getMessage());
            errorFlag=false;
        } 
        
        if(!errorFlag){
            PageReference pr = new PageReference('/' + parentId);
            pr.setRedirect(true);
            return pr; 
        } else{
            Database.rollback(sp);
            errorFlag=false;
        }   
        
        return null;
        
    }

    public PageReference cancelLLP(){
        
            PageReference pr = new PageReference('/' + parentId);
            pr.setRedirect(true);
            return pr; 
    }
    
    // LLP Rec structure    
    public class LLPRecWrapper{
        public Integer SerialNumber{get;set;}
        public Id LLPId{get;set;}
        public string Part_Name{get;set;}
        public string selectedPartNumber{get;set;}
        public List<SelectOption> partNumberOptions{get;set;}
        public LLP_Template__c selectedLLPTemplate{get;set;}
        public LLPRecWrapper(){
            partNumberOptions = new List<SelectOption>();
        
        }
    }    
    
}