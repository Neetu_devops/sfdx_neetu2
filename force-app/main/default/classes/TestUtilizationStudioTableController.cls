/**
*******************************************************************************
* Class: TestUtilizationStudioTableController
* @author Created by Bhavna, Lease-Works, 
* @date  6/14/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to find Record Types of a given Object
*
* History:
* - VERSION   DEVELOPER NAME    	DATE         	DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestUtilizationStudioTableController {
    
    /**
* @description Created Test Data to test all the Methods of UtilizationStudioTableController.
*  	 
* @return null
*/
    @testSetup 
    static void setup(){
        
        Date dateField = System.today(); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
            
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  
        
                
        List<Constituent_Assembly__c> constituentList = new List<Constituent_Assembly__c>();
        for(Integer i=0;i<3;i++){
            Constituent_Assembly__c consituentRecord = new Constituent_Assembly__c();
            consituentRecord.Serial_Number__c = '2';
            //consituentRecord.CSLV__c = 2+i;
            consituentRecord.CSN__c = 3;
            //consituentRecord.TSLV__c = 4+i;
            consituentRecord.TSN__c = 5;
            consituentRecord.Last_CSN_Utilization__c = 3;
            consituentRecord.Last_TSN_Utilization__c = 5;
            consituentRecord.Attached_Aircraft__c = testAircraft.Id;
            consituentRecord.Name ='Testing';
            if (i == 0){
                consituentRecord.Type__c ='Airframe'; 
            }
            else if (i == 1){
                consituentRecord.Type__c ='Engine 1'; 
            }
            else{
                consituentRecord.Type__c ='Landing Gear - Right Main'; 
            }
            
            constituentList.add(consituentRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert constituentList;
        LWGlobalUtils.setTriggersflagOff();
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Month_Ending__c = lastDayOfMonth
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        utilizationRecord.Status__c = 'Approved By Lessor';
        update utilizationRecord;
        
        List<Utilization_Report_List_Item__c> utilizationReportList = new List<Utilization_Report_List_Item__c>();
        for(Integer i=0;i<3;i++){
            Utilization_Report_List_Item__c utilizationReportRecord = new Utilization_Report_List_Item__c();
            utilizationReportRecord.Comments__c = 'Testing'+i;
            utilizationReportRecord.Utilization_Report__c = utilizationRecord.Id;
            utilizationReportRecord.Cycles_During_Month__c = 2;
            utilizationReportRecord.Running_Hours_During_Month__c = 3;
            if(i == 0){
                utilizationReportRecord.Constituent_Assembly__c = constituentList[0].Id;
                utilizationReportRecord.Report_Item__c ='Airframe';
            }
            else if(i == 1){
                utilizationReportRecord.Constituent_Assembly__c = constituentList[1].Id;
                utilizationReportRecord.Report_Item__c ='Engine';
            }
            else{
                utilizationReportRecord.Constituent_Assembly__c = constituentList[2].Id;
                utilizationReportRecord.Report_Item__c ='Landing Gear - Right Main';
            }
            utilizationReportRecord.Effective_Unit__c = 'Monthly';
            utilizationReportRecord.Threshold_Units__c = 2;
            utilizationReportRecord.Rate_Up_To_Threshold__c = 3;
            utilizationReportRecord.Threshold_Amount__c = 4 ;
            utilizationReportRecord.Monthly_Threshold_Guaranteed__c = true;
            utilizationReportRecord.Report_Item_Subtype__c = '23';
            utilizationReportRecord.Net_Units__c = 3;
            utilizationReportRecord.Rate_Beyond_Threshold__c = 6;
            utilizationReportRecord.TSN_At_Month_Start__c = 4;
            utilizationReportRecord.CSN_At_Month_Start__c = 6;
            utilizationReportRecord.Y_Hidden_MR__c = 9;
            utilizationReportList.add(utilizationReportRecord);
        }
        LWGlobalUtils.setTriggersflagOn();  
        insert utilizationReportList;
        LWGlobalUtils.setTriggersflagOff();
        
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Amount__c = 100,
            Status__c = 'Approved'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Payment__c paymentRecord = new Payment__c(
            Name = 'Payment Testing',
            Invoice__c = invoiceRecord.Id,
            Amount__c = invoiceRecord.Amount__c
        );
        insert paymentRecord;
        
        List<Payment_Line_Item__c> paymentLineList = new List<Payment_Line_Item__c>();
        for(Integer i=0;i<3;i++){
            Payment_Line_Item__c paymentLineRecord = new Payment_Line_Item__c();
            if(i == 0){
                paymentLineRecord.Name = 'Airframe-23';
                paymentLineRecord.Assembly_Utilization__c = utilizationReportList[0].Id;
            }
            else if(i == 1){
                paymentLineRecord.Name = 'Engine-23';
                paymentLineRecord.Assembly_Utilization__c = utilizationReportList[1].Id;
            }
            else{
                paymentLineRecord.Name = 'Landing Gear - Right Main-23';
                paymentLineRecord.Assembly_Utilization__c = utilizationReportList[2].Id;
            }
            paymentLineRecord.Amount_Paid__c = 9;
            paymentLineRecord.Payment__c = paymentRecord.Id;
            
            paymentLineList.add(paymentLineRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert paymentLineList;
        LWGlobalUtils.setTriggersflagOff();
        
        
        List<Invoice_Line_Item__c> invoiceList = new List<Invoice_Line_Item__c>();
        for(Integer i=0;i<3;i++){
            Invoice_Line_Item__c invoiceLineItemRecord = new Invoice_Line_Item__c();
            invoiceLineItemRecord.Name='Testing'+i;
            if(i == 0){
                invoiceLineItemRecord.Assembly_Utilization__c = utilizationReportList[0].Id;  
            }
            else if(i == 1){
                invoiceLineItemRecord.Assembly_Utilization__c = utilizationReportList[1].Id;  
            }
            else{
                invoiceLineItemRecord.Assembly_Utilization__c = utilizationReportList[2].Id;  
            }
            
            invoiceLineItemRecord.Invoice__c = invoiceRecord.Id;
            invoiceLineItemRecord.Amount_Due__c = 20;
            invoiceList.add(invoiceLineItemRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceList;
        LWGlobalUtils.setTriggersflagOff();
        
        
        List<Utilization_Report_LLP__c> utilizationReportLLPList = new List<Utilization_Report_LLP__c>();
        for(Integer i=0;i<3;i++){
            Utilization_Report_LLP__c utilizationReportLLPRecord = new Utilization_Report_LLP__c();
            utilizationReportLLPRecord.Y_Hidden_MR__c = 2;
            if(i == 0){
                utilizationReportLLPRecord.Utilization_Report_List_Item__c = utilizationReportList[0].Id;  
            }
            else if(i == 1){
                utilizationReportLLPRecord.Utilization_Report_List_Item__c = utilizationReportList[1].Id;  
            }
            else{
                utilizationReportLLPRecord.Utilization_Report_List_Item__c = utilizationReportList[2].Id;  
            }
            utilizationReportLLPRecord.Cycles_During_Month__c =  3;
            utilizationReportLLPList.add(utilizationReportLLPRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationReportLLPList;
        LWGlobalUtils.setTriggersflagOff();
        
    }
    
    
    /**
* @description This Methods gives the code coverage of  getDealCommercialTerms,setAssetInDeal,createAssetInDeal
*  	getPageLayoutFields,getRecordIdPrefix,getDealAnalysisRecTypeId method of UtilizationStudioTableController
*
* @return null
*/
    @isTest static void testUtilizationStudioTable(){
        Aircraft__c aircraftList = [select id,Name from Aircraft__c];
        system.debug('----------aircraftList'+aircraftList);
        Utilization_Report__c utilizationlist = [select id,Status__c from Utilization_Report__c];
        system.debug('----------utilizationlist'+utilizationlist);
        
        
        UtilizationStudioTableController.fetchUtilizationData(aircraftList.Id,utilizationlist.Id);
        UtilizationStudioTableController.fetchUtilizationData(aircraftList.Id,NULL);
        LWGlobalUtils.setTriggersflagOn(); 
        utilizationlist.Status__c = 'Rejected By Lessor';
        update utilizationlist;
        UtilizationStudioTableController.fetchUtilizationData(aircraftList.Id,utilizationlist.Id);
        LWGlobalUtils.setTriggersflagOff();
        
    }
    
    @isTest static void testfetchUtilizationOnSubmittedStatus(){
        Aircraft__c aircraftList = [select id,Name from Aircraft__c];
        system.debug('----------aircraftList'+aircraftList);
        Utilization_Report__c utilizationlist = [select id,Status__c from Utilization_Report__c];
        system.debug('----------utilizationlist'+utilizationlist);
        LWGlobalUtils.setTriggersflagOn(); 
        utilizationlist.Status__c = 'Submitted To Lessor';
        update utilizationlist;
        UtilizationStudioTableController.fetchUtilizationData(aircraftList.Id,utilizationlist.Id);
        LWGlobalUtils.setTriggersflagOff();
        
    }   
    
    @isTest static void testfetchUtilizationOnNOStatus(){
        Aircraft__c aircraftList = [select id,Name from Aircraft__c];
        system.debug('----------aircraftList'+aircraftList);
        Utilization_Report__c utilizationlist = [select id,Status__c from Utilization_Report__c];
        system.debug('----------utilizationlist'+utilizationlist);
        LWGlobalUtils.setTriggersflagOn(); 
        utilizationlist.Status__c = 'Open';
        update utilizationlist;
        UtilizationStudioTableController.fetchUtilizationData(aircraftList.Id,utilizationlist.Id);
        LWGlobalUtils.setTriggersflagOff();
        
    }   
    
    
    @isTest static void testsaveUtilization() {
        Lease__c[] leaselist = [select id,Name from Lease__c];
        Operator__c[] operatorlist = [select id,Name from Operator__c];
        Invoice__c[] invoicelist = [select id,Name from Invoice__c];
        Utilization_Report__c utilizationlist = [select id,Name,Status__c,Type__c,Month_Ending__c from Utilization_Report__c];
        Constituent_Assembly__c[] constituentlist = [select id,Serial_Number__c,CSN__c,Type__c,Assembly_Display_Name__c, 
                                                     Attached_Aircraft__c,TSN__c from Constituent_Assembly__c];
        Utilization_Report_List_Item__c[] utilizationReportlist = [select id,Comments__c,Constituent_Assembly__r.Assembly_Display_Name__c 
                                                                   from Utilization_Report_List_Item__c];
        Invoice_Line_Item__c[] invoiveLineItemlist = [select id,Name,Assembly_Utilization__c,Invoice__c,Amount_Due__c
                                                      from Invoice_Line_Item__c];
        Aircraft__c aircraftList = [select id,Name from Aircraft__c];
        List<Decimal> fhDataList = new List<Decimal>();
        Date dateField = System.today(); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        
        List<UtilizationStudioTableController.ComponentWrapper > ComponentWrapperList = new List<UtilizationStudioTableController.ComponentWrapper>();
        List<UtilizationStudioTableController.UtilizationWrapper > utilizationWrapperList = new List<UtilizationStudioTableController.UtilizationWrapper>();
        UtilizationStudioTableController.UtilizationWrapper utilization = new UtilizationStudioTableController.UtilizationWrapper();
        utilization.leaseId = leaselist[0].Id;
        utilization.errorMsg = 'Error';
        utilization.nameSpacePrefix = '';
        LWGlobalUtils.setTriggersflagOn(); 
        utilizationlist.Status__c = 'Rejected By Lessor';
        update utilizationlist;
        utilization.utilizationRec = utilizationlist;
        LWGlobalUtils.setTriggersflagOff();
        utilization.isUtilizationCreated = 'utilizationNotCreated';
        utilization.totalFH = 0;
        utilization.totalFHM = '0:00';
        utilization.totalFC = 0;
        utilization.totalBilled = 0;
        utilization.totalBalance =0;
        utilization.totalPaid = 0;
        utilization.totalRatio = 0;
        utilization.totalTSN = 0;
        utilization.totalCSN = 0;
        utilization.totalCummulativeMR = 0;
        utilization.utilizationName = utilizationlist.Name;
        utilization.leaseName = leaselist[0].Name;
        utilization.type = 'Airframe';
        utilization.status = 'Approved';
        utilization.pItem = '';
        utilization.utilizationId = utilizationlist.Id;
        utilization.aircraft = aircraftList.Id;
        utilization.lesseeName = operatorlist[0].Name;
        utilization.periodEnding = lastDayOfMonth;
        utilization.recieved = system.today();
        utilization.proRate = 1;
        utilization.showSubmitBtn = 'hidebtn';
        utilization.updateBtn = '';
        utilization.isAssemblyUtilizationCreated = true;
        utilization.invoiceName = invoicelist[0].Name;
        utilization.invoiceId = invoicelist[0].Id;
        utilization.isInvoiceCreated ='InvoiceNotCreated';
        utilization.showInputField = '';
        utilization.hideOuputField = '';
        utilization.ccEmailAddress= UserInfo.getUserEmail();
        utilization.lstComponentWrapper= ComponentWrapperList;
        utilizationWrapperList.add(utilization);
        
        UtilizationStudioTableController.ComponentWrapper Component = new UtilizationStudioTableController.ComponentWrapper();
        Component.showHelpText = 'Testing';
        Component.staticResourceVal = 'Engine';
        Component.isEOLA = true;
        Component.showSameData =true;
        Component.hideInputForSameType = 'hideInputForSameType';
        Component.assembly =constituentlist[0].Assembly_Display_Name__c;
        Component.flightH = 8;
        Component.flightHM = '';
        Component.flightC = 2;
        Component.ratio = 0.5;
        Component.serialNumber = '1';
        Component.assemblyId =constituentlist[0].Id;
        Component.tsn = 0;
        Component.csn = 0;
        Component.assemblyUtilizationRecord =utilizationReportlist[0];
        Component.mrEvent = '';
        Component.billed = 0;
        Component.units = 0;
        Component.rate = 0;
        Component.cummulativeMR = 0;
        Component.comment='';
        Component.invoiceRec = invoiveLineItemlist[0] ;
        Component.paid =1;
        Component.balance= 2;
        Component.days= 3;
        Component.revenueType='';
        Component.glAccount='';
        Component.fhData=fhDataList;
        ComponentWrapperList.add(Component);
        
        
        LWGlobalUtils.setTriggersflagOn();
        String utilization_s = JSON.serialize(utilization);
        UtilizationStudioTableController.saveUtilization(utilization);
        LWGlobalUtils.setTriggersflagOff();
        
        
    }
    @isTest static void testsubmitUtilizationApproval(){
        Utilization_Report_List_Item__c[] utilizationReportlist = [select id,Comments__c,Constituent_Assembly__r.Assembly_Display_Name__c 
                                                                   from Utilization_Report_List_Item__c];
        Utilization_Report__c utilizationlist = [select id,Status__c from Utilization_Report__c];
        
        UtilizationStudioTableController.getListViews();
        UtilizationStudioTableController.saveCommentOnListItems(utilizationReportlist[0]);
        UtilizationStudioTableController.submitUtilizationApprovalRequest(utilizationlist.Id);
        
    }
    
}