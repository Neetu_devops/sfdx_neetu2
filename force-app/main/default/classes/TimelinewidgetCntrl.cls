/* 
** @lastupdated : 2016.08.12 : 2:30 PM
** TimeLine page is used to display all the aircraft & its assemblies maintenance events that supposed to be happen in future.   
*/
public class TimelinewidgetCntrl
{       
    private string aircraftId                                               {get;set;}
    public list<Timeline> timelines                                         {get;set;}  
    private list<TimelinewidgetService.TimelineWrapper> TimelineWrapperlist {get;set;}  
    public map<string,string> legendlabels                                  {get;set;}
    public Aircraft__c aircraft                                             {get;set;}
    public TimelinewidgetCntrl(ApexPages.StandardController controller) 
    {
        timelines           = new list<Timeline>();
        TimelineWrapperlist = new list<TimelinewidgetService.TimelineWrapper>();
        legendlabels        = new map<string,string>();
        aircraftId          = ApexPages.currentPage().getParameters().get('id');
        Map<String,Custom_Lookup__c> lookUpMapping  = new Map<String,Custom_Lookup__c>(); 
        
        if( aircraftId!=null )     
        {
            //
            aircraft = [ SELECT Name,Date_of_Manufacture__c,Next_Heavy_1_Date_F__c,Next_Heavy_2_Date_F__c,Aircraft_Type__c,
                                Next_Heavy_3_Date_RU__c,Next_Heavy_4_Date_RU__c,Escalated_Heavy1_Cost__c,Escalated_Heavy2_Cost__c,
                                Escalated_Heavy3_Cost__c,Escalated_Heavy4_Cost__c,Last_UR_Approved_For_The_Month_Of__c
                                ,Maintenance_Program__c,Heavy_1_Interval_Months__c,Heavy_2_Interval_Months__c,Heavy_3_Interval_Months__c,Heavy_4_Interval_Months__c
                                ,lease__c,Utilization_Current_As_Of__c,
                                Last_Heavy_1_Date_F__c,Last_Heavy_2_Date_F__c, Last_Heavy_3_Date_F__c,Last_Heavy_4_Date_F__c
                         FROM Aircraft__c 
                             WHERE Id=:aircraftId ]; 
			map<string,Maintenance_Program_Event__c> mapMPE = new map<string,Maintenance_Program_Event__c>();
			Maintenance_Program_Event__c[] listMPE = [select Maintenance_Program__c,Id,Event_Type__c
    					,Never_Exceed_Period_Cycles__c,Never_Exceed_Period_Hours__c,Never_Exceed_Period_Months__c
    					,Interval_2_Cycles__c,Interval_2_Hours__c,Interval_2_Months__c
    					,Assembly__c,Event_Cost__c
    			 	from Maintenance_Program_Event__c 
    			 	where Maintenance_Program__c = :aircraft.Maintenance_Program__c
    			 	and Assembly__c !=null
    			 	order by Event_Type__c];

				//1. insert the missing record if any.
				for(Maintenance_Program_Event__c curMPE: listMPE){
					if('Airframe'.equals(curMPE.Assembly__c)) mapMPE.put(curMPE.Event_Type__c,curMPE);
					else mapMPE.put(curMPE.Assembly__c,curMPE);
				}

			Date MaxEndDate;
			
			map<String,Assembly_MR_Rate__c> mapCAMRRates= new map<String,Assembly_MR_Rate__c>();
			if(aircraft.lease__c!=null){
				Lease__c leaseRec = [	select id
												,Lease_End_Date_New__c
												,Lease_Start_Date_New__c
												,MR_Escalation_LLP__c,MR_Escalation_Month__c,MR_Escalation__c
												,Heavy_Maintenance_Rate_Basis__c,Heavy_Maintenance_2_Rate_Basis__c,Heavy_Maintenance_3_Rate_Basis__c,Heavy_Maintenance_4_Rate_Basis__c,C_Check_Rate_Basis__c,Other_Rate_Basis__c
												
												,LLP_Replacement_Cost_Escalation__c,Replacement_Cost_Escalation_Month__c
												,(select id 
													,Assembly_Type__c,Base_MRR__c,Current_MRR__c,Flat_Rate_Upto_Threshold__c,Monthly_Threshold__c,Monthly_Threshold_Guaranteed__c,Rate_Basis__c
												from Assembly_MR_Rates__r where Assembly_Type__c!=null)
												
											from lease__c
											where Aircraft__c = :aircraftId limit 1
										];
				MaxEndDate = leaseRec.Lease_End_Date_New__c;
				for(Assembly_MR_Rate__c CurRec:leaseRec.Assembly_MR_Rates__r){
						mapCAMRRates.put(CurRec.Assembly_Type__c,CurRec);
				}
			}
			map<id,map<String,object>> returnMapCA = new map<id,map<String,object>>();
			system.debug('Lease End Date ='+MaxEndDate);
			system.debug('system now ='+system.now());
            leasewareUtils.getAverageMonthlyAssemblyUtilization(aircraftId,null,aircraft.Last_UR_Approved_For_The_Month_Of__c,returnMapCA,null,null,true);



            
			lookUpMapping = TimelinewidgetService.getlookUps( aircraft.Aircraft_Type__c );
			
            list<Constituent_Assembly__c> assemblies = new list<Constituent_Assembly__c> ();
            assemblies = [ SELECT Id,Latest_Utilization_Date__c,Type__c,Restoration_Overhaul_Cost__c,Y_Hidden_Est_Next_Shop_Visit_RU1__c,Last_Event_Date_F__c,
                            Serial_Number__c,Date_of_Manufacture__c,Life_Limit__c,Life_Limit_Hours__c,Average_Utilization__c,Average_Monthly_Utilization_Cycles__c,Life_Limit_Calendar_Months__c,
                            Performance_Limit_2_Calendar_Months__c,Estimated_Next_Shop_Visit_F__c,Attached_Aircraft__r.Last_UR_Approved_For_The_Month_Of__c
                           FROM Constituent_Assembly__c 
                           WHERE Attached_Aircraft__c=:aircraftId  ];
            
            list<Aircraft_Eligible_Event__c> forcastedEvents = new list<Aircraft_Eligible_Event__c>();
            
            boolean eventInCQ = false;
            
            Date NextEventDate;
            
            Date LastEventDate;
            Date ApprovedLastUtilizationDate;
            decimal MonthsToNextEventDate;
            string AverageUtilization;
            string Interval;
            string rateBasis='Hours';
            Decimal nEngineAvgHrs,nLLPAvgCycles;
            string MPEType;
            string H_Interval; 
            Date DateofManuf;
            Date LastUtilizatonDate;
            
 			
 			
 			system.debug('LastUtilizatonDate ='+LastUtilizatonDate);
 			if(MaxEndDate==null) MaxEndDate= system.today().addYears(10);//defaulting in case of null
 			
 			
            for( Constituent_Assembly__c assembly : assemblies )
            {
            	ApprovedLastUtilizationDate =assembly.Attached_Aircraft__r.Last_UR_Approved_For_The_Month_Of__c;
            	
            	LastUtilizatonDate=assembly.Latest_Utilization_Date__c;
            	if(assembly.Date_of_Manufacture__c!=null)
            		DateofManuf = assembly.Date_of_Manufacture__c;
            	else
            		DateofManuf=null;
            		
            		
	            if(assembly.Type__c.startswith('Landing')) {
	            	if(assembly.Average_Monthly_Utilization_Cycles__c==null) AverageUtilization='NA';
	            	else AverageUtilization= assembly.Average_Monthly_Utilization_Cycles__c + ' Cycles';
	            	
	            	//Interval 
	            	//if(assembly.Life_Limit__c==null) Interval='NA';
	            	//else Interval= (Integer)assembly.Life_Limit__c + ' Cycles';
	            	
	            	//New Interval for Landing Case
	            	if(assembly.Life_Limit_Calendar_Months__c!=null) {
	            		Interval= (Integer)assembly.Life_Limit_Calendar_Months__c + ' Months';
	            	}
	          		else{
	          			if(assembly.Life_Limit__c==null) Interval='NA';
	          			else Interval= assembly.Life_Limit__c + ' Cycles';
	          		}
          		
          		
	            }
				else {
					if(assembly.Average_Utilization__c ==null) AverageUtilization='NA';
					else AverageUtilization= assembly.Average_Utilization__c + ' Hours'; 
					
					//Interval 
	            	if(assembly.Life_Limit_Hours__c==null) Interval='NA';
	            	else Interval= (Integer)assembly.Life_Limit_Hours__c + ' Hours'; 
				}
             	LastEventDate = assembly.Last_Event_Date_F__c;
             	
             	if(LastEventDate==null)LastEventDate = assembly.Date_of_Manufacture__c;
            	system.debug('Airframe LastEventDate====='+LastEventDate);
            	NextEventDate = assembly.Estimated_Next_Shop_Visit_F__c;
            	if(NextEventDate==null){
            		if(assembly.Type__c.startsWith('Eng'))MPEType ='Engine';
            		else if(assembly.Type__c.contains('Main'))MPEType ='Landing Gear - Main';
            		else if(assembly.Type__c.contains('Wing'))MPEType ='Landing Gear - Wing';
            		else if(assembly.Type__c.contains('Nose'))MPEType ='Landing Gear - Nose';
            		else if(assembly.Type__c.startsWith	('APU'))MPEType ='APU';
            		else if(assembly.Type__c.startsWith('Propeller'))MPEType ='Propeller';
            		
            		if(assembly.Type__c.startsWith('Lan'))rateBasis='Cycles';
            		if(mapCAMRRates.containsKey(assembly.Type__c) && mapCAMRRates.get(assembly.Type__c).Rate_Basis__c!=null)rateBasis = mapCAMRRates.get(assembly.Type__c).Rate_Basis__c;
            		try{
                		nEngineAvgHrs = LeaseWareUtils.zeroIfNull((Decimal)(returnMapCA.get(assembly.id).get('AvgTSN')));
                		nLLPAvgCycles =  LeaseWareUtils.zeroIfNull((Decimal)(returnMapCA.get(assembly.id).get('AvgCSN')));
            		}catch(Exception ex){
            			nEngineAvgHrs =0;
            			nLLPAvgCycles =0;
            			if(assembly.Average_Utilization__c!=null)nEngineAvgHrs = assembly.Average_Utilization__c;
            			if(assembly.Average_Monthly_Utilization_Cycles__c!=null)nLLPAvgCycles = assembly.Average_Monthly_Utilization_Cycles__c;
            			
            			system.debug('Error ---'+ex);
            		}            		
            		system.debug('pre NextEventDate ---'+NextEventDate);
            		NextEventDate=TimelinewidgetService.getNextEventDate(rateBasis,assembly.Date_of_Manufacture__c,nEngineAvgHrs,nLLPAvgCycles,assembly.Life_Limit__c,assembly.Life_Limit_Hours__c,assembly.Life_Limit_Calendar_Months__c,mapMPE.get(MPEType));
            	}
            	system.debug('CA='+assembly.Type__c+'=NextEventDate='+NextEventDate);
            	
            	
	             //New Code
	            if(LastUtilizatonDate==null)LastUtilizatonDate=LastEventDate;
	            system.debug('LastUtilizatonDate original = '+LastUtilizatonDate);
	            if(NextEventDate!=null)MonthsToNextEventDate =  LastUtilizatonDate.monthsBetween(NextEventDate);	

	            system.debug('MonthsToNextEventDate ='+MonthsToNextEventDate);
	            system.debug('NextEventDate ='+NextEventDate);
	            system.debug('LastUtilizatonDate ='+LastUtilizatonDate);
            	system.debug('LastEventDate ='+LastEventDate);
           	
            	
                if( NextEventDate!=null && NextEventDate >= system.now()  && NextEventDate <= MaxEndDate.addYears(20)) 
                {
                    Timeline tml  = TimelinewidgetService.createTimeline(ApprovedLastUtilizationDate,DateofManuf,Interval,AverageUtilization, MonthsToNextEventDate,LastEventDate, NextEventDate , 
                                      integer.valueof(assembly.Restoration_Overhaul_Cost__c) , 
                                      assembly.Type__c+'-'+assembly.Serial_Number__c );
                    TimelineWrapperlist.add( new TimelinewidgetService.TimelineWrapper( tml ) );
                }
            }           
			
            map<String,object> returnMapAC = new map<String,object>();

            leasewareUtils.getAverageMonthlyUtilization(aircraftId,aircraft.Last_UR_Approved_For_The_Month_Of__c,returnMapAC,null,null,true);

            try{
                	nEngineAvgHrs = LeaseWareUtils.zeroIfNull((Decimal)(returnMapAC.get('AvgTSN')));
                	nLLPAvgCycles =  LeaseWareUtils.zeroIfNull((Decimal)(returnMapAC.get('AvgCSN')));  
			}catch(Exception ex){
            			nEngineAvgHrs =0;
            			nLLPAvgCycles =0;
            }                	                 
            // Heavy1
            NextEventDate =aircraft.Next_Heavy_1_Date_F__c;
            	if(NextEventDate==null){
           			MPEType = 'Heavy 1';
           			rateBasis='Months';
            		if(mapCAMRRates.containsKey(MPEType) && mapCAMRRates.get(MPEType).Rate_Basis__c!=null)rateBasis = mapCAMRRates.get(MPEType).Rate_Basis__c;
            		NextEventDate=TimelinewidgetService.getNextEventDate(rateBasis,aircraft.Date_of_Manufacture__c,nEngineAvgHrs,nLLPAvgCycles,null,null,aircraft.Heavy_1_Interval_Months__c,mapMPE.get(MPEType));
            	}   
            system.debug('Heavy 1=NextEventDate='+NextEventDate);	         
            if( NextEventDate!=null && NextEventDate >= system.now() && NextEventDate <= MaxEndDate.addYears(20))
            {
            	if(aircraft.Heavy_1_Interval_Months__c==null) H_Interval='NA';
            	else H_Interval = aircraft.Heavy_1_Interval_Months__c + ' Months';
            	
                Timeline tml  = TimelinewidgetService.createTimeline(aircraft.Last_UR_Approved_For_The_Month_Of__c, aircraft.Date_of_Manufacture__c,H_Interval ,AverageUtilization, MonthsToNextEventDate,aircraft.Last_Heavy_1_Date_F__c==null?aircraft.Date_of_Manufacture__c:aircraft.Last_Heavy_1_Date_F__c, NextEventDate , 
                                    integer.valueof(aircraft.Escalated_Heavy1_Cost__c) , 'Heavy1' );
                TimelineWrapperlist.add( new TimelinewidgetService.TimelineWrapper( tml ) );
            }    
            // Heavy2
            NextEventDate =aircraft.Next_Heavy_2_Date_F__c;
            	if(NextEventDate==null){
           			MPEType = 'Heavy 2';
           			rateBasis='Months';
            		if(mapCAMRRates.containsKey(MPEType) && mapCAMRRates.get(MPEType).Rate_Basis__c!=null)rateBasis = mapCAMRRates.get(MPEType).Rate_Basis__c;
            		NextEventDate=TimelinewidgetService.getNextEventDate(rateBasis,aircraft.Date_of_Manufacture__c,nEngineAvgHrs,nLLPAvgCycles,null,null,aircraft.Heavy_2_Interval_Months__c,mapMPE.get(MPEType));
            	}   
            system.debug('Heavy 2=NextEventDate='+NextEventDate);	                       
            if( NextEventDate!=null && NextEventDate >= system.now() && NextEventDate <= MaxEndDate.addYears(20))
            {
            	if(aircraft.Heavy_2_Interval_Months__c==null) H_Interval='NA';
            	else H_Interval = aircraft.Heavy_2_Interval_Months__c + ' Months';
            	
                Timeline tml  = TimelinewidgetService.createTimeline(aircraft.Last_UR_Approved_For_The_Month_Of__c, aircraft.Date_of_Manufacture__c ,H_Interval,AverageUtilization, MonthsToNextEventDate,aircraft.Last_Heavy_2_Date_F__c==null?aircraft.Date_of_Manufacture__c:aircraft.Last_Heavy_2_Date_F__c, NextEventDate , 
                                    integer.valueof(aircraft.Escalated_Heavy2_Cost__c) , 'Heavy2' );
                TimelineWrapperlist.add( new TimelinewidgetService.TimelineWrapper( tml ) );
            }
            // Heavy3
            NextEventDate =aircraft.Next_Heavy_3_Date_RU__c;
            	if(NextEventDate==null){
           			MPEType = 'Heavy 3';
           			rateBasis='Months';
            		if(mapCAMRRates.containsKey(MPEType) && mapCAMRRates.get(MPEType).Rate_Basis__c!=null)rateBasis = mapCAMRRates.get(MPEType).Rate_Basis__c;
            		NextEventDate=TimelinewidgetService.getNextEventDate(rateBasis,aircraft.Date_of_Manufacture__c,nEngineAvgHrs,nLLPAvgCycles,null,null,aircraft.Heavy_3_Interval_Months__c,mapMPE.get(MPEType));
            	}   
            system.debug('Heavy 3=NextEventDate='+NextEventDate);            
            if( NextEventDate!=null && NextEventDate >= system.now() && NextEventDate <= MaxEndDate.addYears(20))
            {
            	if(aircraft.Heavy_3_Interval_Months__c==null) H_Interval='NA';
            	else H_Interval = aircraft.Heavy_3_Interval_Months__c + ' Months';
            	
                Timeline tml  = TimelinewidgetService.createTimeline(aircraft.Last_UR_Approved_For_The_Month_Of__c, aircraft.Date_of_Manufacture__c ,H_Interval,AverageUtilization, MonthsToNextEventDate,aircraft.Last_Heavy_3_Date_F__c==null?aircraft.Date_of_Manufacture__c:aircraft.Last_Heavy_3_Date_F__c, NextEventDate , 
                                    integer.valueof(aircraft.Escalated_Heavy3_Cost__c) , 'Heavy3' );
                TimelineWrapperlist.add( new TimelinewidgetService.TimelineWrapper( tml ) );
            } 
            // Heavy4
            NextEventDate =aircraft.Next_Heavy_4_Date_RU__c;
            	if(NextEventDate==null){
           			MPEType = 'Heavy 4';
           			rateBasis='Months';
            		if(mapCAMRRates.containsKey(MPEType) && mapCAMRRates.get(MPEType).Rate_Basis__c!=null)rateBasis = mapCAMRRates.get(MPEType).Rate_Basis__c;
            		NextEventDate=TimelinewidgetService.getNextEventDate(rateBasis,aircraft.Date_of_Manufacture__c,nEngineAvgHrs,nLLPAvgCycles,null,null,aircraft.Heavy_4_Interval_Months__c,mapMPE.get(MPEType));
            	}   
            system.debug('Heavy 4=NextEventDate='+NextEventDate);              
            if( NextEventDate!=null && NextEventDate >= system.now() && NextEventDate <= MaxEndDate.addYears(20) )
            {
            	if(aircraft.Heavy_4_Interval_Months__c==null) H_Interval='NA';
            	else H_Interval = aircraft.Heavy_4_Interval_Months__c + ' Months';
                Timeline tml  = TimelinewidgetService.createTimeline(aircraft.Last_UR_Approved_For_The_Month_Of__c, aircraft.Date_of_Manufacture__c ,H_Interval ,AverageUtilization, MonthsToNextEventDate,aircraft.Last_Heavy_4_Date_F__c==null?aircraft.Date_of_Manufacture__c:aircraft.Last_Heavy_4_Date_F__c, NextEventDate , 
                                    integer.valueof(aircraft.Escalated_Heavy4_Cost__c) , 'Heavy4' );
                TimelineWrapperlist.add( new TimelinewidgetService.TimelineWrapper( tml ) );
            }     
            
            for( TimelinewidgetService.TimelineWrapper tmlWrap : TimelineWrapperlist )
            {
                if( TimelinewidgetService.isCurrentQuarter( tmlWrap.timeline.forcastedEventDateTo ) )
                    eventInCQ = true;
            }
            
            TimelineWrapperlist.sort();
            
            system.debug( 'TimelineWrapperlist=='+eventInCQ+'  '+TimelineWrapperlist.size()+'  '+TimelineWrapperlist );
            if( !eventInCQ )
                timelines.add(new Timeline()); // adding todays block
            else
            {
                Timeline tmlPreviousQtr = TimelinewidgetService.createTimeline(aircraft.Last_UR_Approved_For_The_Month_Of__c, aircraft.Date_of_Manufacture__c ,Interval,AverageUtilization, MonthsToNextEventDate,LastEventDate, system.today().addDays(-90) ,0, '' );
                timelines.add(tmlPreviousQtr);          
            }
            
            for( TimelinewidgetService.TimelineWrapper timeWrapper : TimelineWrapperlist )
            {  
                Timeline tml  =  timeWrapper.Timeline;	
				
				TimelinewidgetService.ChartEntity chartEnt = TimelinewidgetService.getChartEntity( tml.eventType );
                legendlabels.put( chartEnt.legendLabel,chartEnt.barColor );
								
				Custom_Lookup__c curLookup = lookUpMapping.get( tml.eventTypeLong );				
				if( curLookup!=null && curLookup.Short_Name__c!=null ) 	
				{
					tml.eventType 	   = curLookup.Short_Name__c;
					tml.eventTypeAlias = curLookup.Short_Name__c;				
				}	
				
                timelines.add(tml); 
            }  
            
            list<Timeline> timelines3 = new list<Timeline>();
            timelines3 = TimelinewidgetService.createTimelineWidget( timelines );
            
            timelines  = new list<Timeline>();
            
            timelines.addAll( timelines3 );  
      }
      
      system.debug('TimeLineWidget Debug Testing = '+timelines);
      system.debug('TimeLineWidget Debug Testing size = '+timelines.size());
    }
    
    

}