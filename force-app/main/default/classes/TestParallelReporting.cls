@isTest
public class TestParallelReporting {
    
    @TestSetup
    static void setData(){
		Date dateField = System.today();
		Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
		Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);

        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
		insert operator;

		Lease__c leaseRecord = new Lease__c(
			Name = 'Testing',
			Lease_Start_Date_New__c = Date.valueOf('2020-03-01'),
			Lease_End_Date_New__c = Date.valueOf('2021-11-12'),
			Threshold_Claim_Amount__c = 5000,
            Lessee__c=operator.Id
			);
		insert leaseRecord;

		// Set up the Aircraft record
		Aircraft__c asset = new Aircraft__c(Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available', Derate__c=20, MSN_Number__c = '28216');
		LeaseWareUtils.clearFromTrigger();
		insert asset;

		// update lease reocrd with aircraft
		leaseRecord.Aircraft__c=asset.id;
		LeaseWareUtils.clearFromTrigger();
		update leaseRecord;

        Cash_Security_Deposit__c cashSD = new Cash_Security_Deposit__c(name ='Test SD',lease__c = leaseRecord.id,Security_Deposit_Cash__c = 100000 );
        insert cashSD;

		Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
		insert legalEntity;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);

		List<Constituent_Assembly__c> assemblyList = new List<Constituent_Assembly__c>();
		assemblyList.add(new Constituent_Assembly__c(Name='Airframe', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
		                                             TSLV__c=0, CSLV__c= 0, Type__c ='Airframe', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId(),
		                                             Serial_Number__c='Airframe'));
		assemblyList.add(new Constituent_Assembly__c(Name='Engine 1', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
		                                             TSLV__c=0, CSLV__c= 0, Type__c ='Engine 1', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId(),
		                                             Serial_Number__c='Engine 1'));

		LeaseWareUtils.clearFromTrigger();
		insert assemblyList;

		// Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(name='Test',Current_Catalog_Year__c='2020');
		insert mp;

		List<Maintenance_Program_Event__c> mpeList = new List<Maintenance_Program_Event__c>();
		mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Airframe', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = '4C/6Y'));
		mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Engine', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = 'Performance Restoration'));
		insert mpeList;

		// Set up Project Event for Airframe
		List<Assembly_Event_Info__c> projectedEventList = new List<Assembly_Event_Info__c>();
		projectedEventList.add(new Assembly_Event_Info__c(Name = 'Airframe - 4C/6Y', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[0].Id, Maintenance_Program_Event__c = mpeList[0].Id));
		projectedEventList.add(new Assembly_Event_Info__c(Name = 'Engine 1 - PR', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[1].Id, Maintenance_Program_Event__c = mpeList[1].Id));
		insert projectedEventList;

		List<Assembly_MR_Rate__c> supplementalRentList = new List<Assembly_MR_Rate__c>();
		supplementalRentList.add(new Assembly_MR_Rate__c(Name='Airframe - 4C/6Y', Assembly_Lkp__c = assemblyList[0].id, Assembly_Event_Info__c = projectedEventList[0].id,
		                                                 Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Induction Date', Rate_Basis__c='Cycles',
		                                                 Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
		supplementalRentList.add(new Assembly_MR_Rate__c(Name='Engine 1 - PR', Assembly_Lkp__c = assemblyList[1].id, Assembly_Event_Info__c = projectedEventList[1].id,
		                                                 Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Removal Date', Rate_Basis__c='Cycles',
		                                                 Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
		 insert supplementalRentList;

         List<Assembly_Eligible_Event__c> historicalEventList = new List<Assembly_Eligible_Event__c>();
		historicalEventList.add(new Assembly_Eligible_Event__c(Name ='Airframe 4C/6Y', Constituent_Assembly__c = assemblyList[0].Id, Projected_Event__c = projectedEventList[0].id,
		                                                       Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
		                                                       Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = '4C/6Y'));
		historicalEventList.add(new Assembly_Eligible_Event__c(Name ='Engine 1 PR', Constituent_Assembly__c = assemblyList[1].Id, Projected_Event__c = projectedEventList[1].id,
		                                                       Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
		                                                       Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = 'Performance Restoration'));
		
        LWGlobalUtils.setTriggersflagOn();
		insert historicalEventList;
		LWGlobalUtils.setTriggersflagOff();

		List<Reserve_Snapshot__c> mrHistoryList = new List<Reserve_Snapshot__c>();
		mrHistoryList.add(new Reserve_Snapshot__c(Name = 'Airframe SB', Lease__c = leaseRecord.Id, Assembly__c = assemblyList[0].Id, Snapshot_Date__c = Date.valueOf('2019-06-30'), Event_Type__c = '4C/6Y',
		                                          Type__c = 'Starting Balance',  Assembly_MR_Info__c = supplementalRentList[0].Id));
		mrHistoryList.add(new Reserve_Snapshot__c(Name = 'Airframe MRP', Lease__c = leaseRecord.Id, Assembly__c = assemblyList[0].Id, Snapshot_Date__c = Date.valueOf('2019-07-06'), Event_Type__c = '4C/6Y',
		                                          Type__c = 'MR Payment', Assembly_MR_Info__c = supplementalRentList[0].Id));
		mrHistoryList.add(new Reserve_Snapshot__c(Name = 'Airframe AE', Lease__c = leaseRecord.Id, Assembly__c = assemblyList[0].Id, Snapshot_Date__c = Date.valueOf('2019-07-25'), Event_Type__c = '4C/6Y',
		                                          Type__c = 'Assembly Event', Assembly_MR_Info__c = supplementalRentList[0].Id));
		mrHistoryList.add(new Reserve_Snapshot__c(Name = 'Engine SB', Lease__c = leaseRecord.Id, Assembly__c = assemblyList[1].Id, Snapshot_Date__c = Date.valueOf('2019-06-30'), Event_Type__c = 'Performance Restoration',
		                                          Type__c = 'Starting Balance',  Assembly_MR_Info__c = supplementalRentList[1].Id));
		mrHistoryList.add(new Reserve_Snapshot__c(Name = 'Engine MRP', Lease__c = leaseRecord.Id, Assembly__c = assemblyList[1].Id, Snapshot_Date__c = Date.valueOf('2019-07-06'), Event_Type__c = 'Performance Restoration',
		                                          Type__c = 'MR Payment', Assembly_MR_Info__c = supplementalRentList[1].Id));
		mrHistoryList.add(new Reserve_Snapshot__c(Name = 'Engine AE', Lease__c = leaseRecord.Id, Assembly__c = assemblyList[1].Id, Snapshot_Date__c = Date.valueOf('2019-07-25'), Event_Type__c = 'Performance Restoration',
		                                          Type__c = 'Assembly Event', Assembly_MR_Info__c = supplementalRentList[1].Id));

		insert mrHistoryList;

		List<Claims__c> claimList = new List<Claims__c>();
		claimList.add(new Claims__c(Name ='Airframe 4C/6Y', Supplemental_Rent__c = supplementalRentList[0].Id, Approved_Claim_Amount__c = 1000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
		claimList.add(new Claims__c(Name ='Engine 1 PR', Supplemental_Rent__c = supplementalRentList[1].Id, Approved_Claim_Amount__c = 1000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
		insert claimList;

		Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                          Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                                          Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
        LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;

		Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing 1',
            FH_String__c ='61',
            Airframe_Cycles_Landing_During_Month__c= 23,
            Aircraft__c = asset.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
			Type__c = 'Actual',
			Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2020,03,31)          
        );
        
        insert utilizationRecord1;
	}

    @isTest(seeAllData=false)
    static void testBatch(){
                
        Lease__c lease = [Select id, Name from Lease__c];
        operator__c lessee = [select id from operator__c limit 1];
        
        Test.StartTest();
        cash_security_deposit__c sd = [select id,name, Deposit_Payments_Received__c,Deposit_Returned__c ,
                                       refund_committed__c,Security_Depo_Cash__c from cash_security_deposit__c where Lease__c=:lease.Id];
        
        sd.Deposit_Payments_Received__c = 100000;        
        update sd;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Payment__c; 
        Map<String,Schema.RecordTypeInfo> PaymentTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = PaymentTypeInfo.get('Payment').getRecordTypeId();
         
        Constituent_Assembly__c ConsttAssembly =[select id,name from Constituent_Assembly__c where Attached_Aircraft__r.Lease__c =: lease.id and 
                                                 Type__c='Airframe']; 
        Assembly_MR_Rate__c assemblyMRInfo1 = [select id,Name,Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c,Assembly_Lkp__r.Type__c
                                                ,Available_Reference_Date__c,Assembly_Event_Info__r.Event_Type__c
                                                from Assembly_MR_Rate__c where Lease__c =:lease.Id 
                                               AND RecordType.DeveloperName = 'MR' AND Assembly_Lkp__c =:ConsttAssembly.Id limit 1];
        Utilization_Report__c utilization = [Select id, name from Utilization_Report__c where Aircraft__r.Lease__c =:lease.Id];
        
        // create Invoice record
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilization.Id, 
            Lease__c = assemblyMRInfo1.Lease__c,
            Amount__c = 10000,
            status__c = 'Approved',
            Comments__c = 'Test Comment,Test Comment,,Test Comment,,Test Comment,'
        );
        insert invoiceRecord;
                 
        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoiceRecord.id, payment_date__c=date.today(), RecordTypeId=rtId, Amount__c=1600
                                              );
        insert newPayment;
        
         LeaseWareUtils.clearFromTrigger(); 
        credit_memo__c creditMemo = new credit_memo__c(amount__c = 200000.00,Lease__c = lease.id,
                                                       Credit_Memo_Date__c=system.today(),
                                                       lessee__c = lessee.id);
        
        insert creditMemo;
        
        payable__c py = new payable__c(payable_amount__c = 50000,status__c='Approved',Payable_Due_Date__c=system.today(),lease__c = lease.id,
                                       cash_security_deposit__c=sd.id);
        insert py;
        py.status__c = 'Approved';
        update py;            
        
        payout__c po = new payout__c(Lease__c = lease.id,Payout_Amount__c=150000,Payout_Type__c ='Cash',Status__c='Pending',Payout_Date__c=system.today());        
        insert po;
        
        payable_line_item__c pli = new payable_line_item__c(payable__c = py.id,payout__c=po.id);
        insert pli;
        Test.stopTest();
        Process_Request_Log__c reqLogNew = new Process_Request_Log__c(Status__c ='PROCESSING', Type__c = 'Operational Transaction Register', Comments__c = '', Exception_Message__c = ''); 
        insert reqLogNew;         

        Database.executeBatch(new OperationalTransactionRegisterHandler(false,1,reqLogNew),1);
    }
    @isTest
    static void testBatch2(){

        Lease__c lease = [Select id, Name from Lease__c];
        operator__c lessee = [select id from operator__c limit 1];
        
         Test.startTest();
        Constituent_Assembly__c ConsttAssembly =[select id,name from Constituent_Assembly__c where Attached_Aircraft__r.Lease__c =: lease.id and 
                                                 Type__c='Airframe']; 
        Assembly_MR_Rate__c assemblyMRInfo1 = [select id,Name,Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c,Assembly_Lkp__r.Type__c,Lease__r.lessee__c,Lease__r.operator__c,
                                               Available_Reference_Date__c,Assembly_Event_Info__r.Event_Type__c
                                                from Assembly_MR_Rate__c where Lease__c =:lease.Id 
                                               AND RecordType.DeveloperName = 'MR' AND Assembly_Lkp__c =:ConsttAssembly.Id limit 1];

        Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: ConsttAssembly.Id LIMIT 1];

        Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: assemblyMRInfo1.Id LIMIT 1];
        MRClaimController.getAssemblyMRInfo(claim.Id);
		MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
		MRClaimController.getAssemblyMRInfos(claim.Id, ''); 
		MRClaimController.findMRClaimAvailable(assemblyMRInfo1, claim.Id);

		claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,Supplemental_Rent__r.Lease__r.lessor__c,
		         Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c,Supplemental_Rent__r.Lease__r.lessee__c,Supplemental_Rent__r.Lease__r.operator__c,
		         Approved_Claim_Amount__c, Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c,Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c
		         FROM Claims__c Where Supplemental_Rent__c =: assemblyMRInfo1.Id LIMIT 1];
		LeasewareUtils.clearFromTrigger();
		MRClaimController.calculateTotalInvoiceAmount(claim.Id);
		MRClaimController.createEventInvoice(claim, assemblyMRInfo1, claim.Approved_Claim_Amount__c, '2019-08-08', 'Removal',2000.45, 0);

        payable__c py = [select payable_amount__c ,status__c,Payable_Due_Date__c,lease__c,lessee__c,claim__c,Payable_Type__c 
                           from payable__c limit 1];
    
     
        payout__c  payoutRec = new payout__c();
        payoutRec.Lease__c = py.Lease__c;
        payoutRec.Lessee__c= py.Lessee__c;
        payoutRec.Payout_Date__c = System.today();
        payoutRec.Payout_Type__c = 'Offset';
        payoutRec.Status__c = 'Approved';
        payoutRec.Payables_Type__c = py.Payable_Type__c;
        payoutRec.Payout_Amount__c = 150000;
        LeasewareUtils.clearFromTrigger();
        LWGlobalUtils.setTriggersflagOn(); 
        insert payoutRec;
        LWGlobalUtils.setTriggersflagOff();

        Test.stopTest();
        Process_Request_Log__c reqLogNew = new Process_Request_Log__c(Status__c ='PROCESSING', Type__c = 'Operational Transaction Register', Comments__c = '', Exception_Message__c = ''); 
        insert reqLogNew;         

        Database.executeBatch(new OperationalTransactionRegisterHandler(false,1,reqLogNew),1);
    }

    @isTest(seeAllData=false)
    static void testCreateInvoiceReporting(){
        Aircraft__c acRecord = [Select id, Name, Maintenance_Program__c from Aircraft__c ];
        Lease__c lease = [Select id, Name from Lease__c];
        
        Test.startTest();
        Constituent_Assembly__c ConsttAssembly =[select id,name from Constituent_Assembly__c where Attached_Aircraft__r.Lease__c =: lease.id and 
                                                 Type__c='Airframe'];  // Checking for APU
        Assembly_MR_Rate__c assemblyMRInfo1 = [select id,Name,Lease__c from Assembly_MR_Rate__c where Lease__c =:lease.Id 
                                               AND RecordType.DeveloperName = 'MR' AND Assembly_Lkp__c =:ConsttAssembly.Id limit 1];
        Utilization_Report__c utilization = [Select id, name from Utilization_Report__c where Aircraft__r.Lease__c =:lease.Id];
        
        // create Invoice record
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilization.Id,
            Lease__c = assemblyMRInfo1.Lease__c,
            Amount__c = 10000,
            status__c = 'Pending',
            Comments__c = 'Test Comment,Test Comment,,Test Comment,,Test Comment,'
        );
        insert invoiceRecord;
        Test.stopTest();
        
        Operational_Transaction_Register__c rec1 = [Select id, name ,Module__c,Transaction_Source__c, Lease__c ,Lessor__c, Lessee__c, Invoice__c from Operational_Transaction_Register__c];
        System.debug('Test:' + rec1); 
        System.assertEquals('Aircraft MR', rec1.Transaction_Source__c);
        System.assertEquals('Receivable', rec1.Module__c); 
    }
    
    @isTest(seeAllData=false)
    static void testCreatePaymentReporting(){        
        Lease__c lease = [Select id, Name from Lease__c];
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Payment__c; 
        Map<String,Schema.RecordTypeInfo> PaymentTypeInfo = cfrSchema.getRecordTypeInfosByName();
        System.debug('PaymentTypeInfo:' + PaymentTypeInfo);
        System.debug('PaymentTypeInfo:' + PaymentTypeInfo.get('Payment'));
        Id rtId = PaymentTypeInfo.get('Payment').getRecordTypeId();
        
        Test.startTest();
        Constituent_Assembly__c ConsttAssembly =[select id,name from Constituent_Assembly__c where Attached_Aircraft__r.Lease__c =: lease.id and 
                                                 Type__c='Airframe'];  // Checking for APU
        Assembly_MR_Rate__c assemblyMRInfo1 = [select id,Name,Lease__c from Assembly_MR_Rate__c where Lease__c =:lease.Id 
                                               AND RecordType.DeveloperName = 'MR' AND Assembly_Lkp__c =:ConsttAssembly.Id limit 1];
        Utilization_Report__c utilization = [Select id, name from Utilization_Report__c where Aircraft__r.Lease__c =:lease.Id];
        
        // create Invoice record
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilization.Id,
            Lease__c = assemblyMRInfo1.Lease__c,
            Amount__c = 10000,
            status__c = 'Approved'
        );
        insert invoiceRecord;
        
        LeaseWareUtils.clearFromTrigger(); 
        
        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoiceRecord.id, payment_date__c=date.today(), RecordTypeId=rtId, Amount__c=1600
                                              );
        insert newPayment;	

        Test.stopTest();
        
        List<Operational_Transaction_Register__c> listRec1 = [Select id, name ,Module__c,Transaction_Source__c, Lease__c ,Lessor__c, 
                                                              Lessee__c, Invoice__c from Operational_Transaction_Register__c where Payments_Credits__c =:newPayment.Id];
        System.debug('Test:' + listRec1.size()); 
        System.assertEquals('Aircraft MR', listRec1[0].Transaction_Source__c);
        System.assertEquals('Receivable', listRec1[0].Module__c); 
    }
    
    @isTest
    public static void testPrepymt(){
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Test.startTest();
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = system.today().addYears(2),
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;
        
        Payment_Receipt__c payRecord = new Payment_Receipt__c(
            Name='Testing',
            Approval_Status__c = 'Pending',
            Prepayment__c = true,
            Lease__c = leaseRecord.Id,
            Payment_Amount_Cur__c = 100000
        );
        insert payRecord; 

        test.stopTest();    
        List<Operational_Transaction_Register__c> listRec1= [Select id, name ,Module__c,Transaction_Type__c,Transaction_Source__c,
                                                  Lease__c ,Lessor__c, Lessee__c, Invoice__c from Operational_Transaction_Register__c where Prepayment__c =:payRecord.Id	];
        System.debug('Test:' + listRec1); 
        System.assertEquals('Payment', listRec1[0].Transaction_Source__c);
        System.assertEquals('Receivable', listRec1[0].Module__c); 
        System.assertEquals('Prepayment', listRec1[0].Transaction_Type__c);  
    }
    
    @isTest
    static void testCreditMemoReporting(){
        Test.startTest();
        Lease__c lease = [select id from lease__c limit 1];
        operator__c lessee = [select id from operator__c limit 1];
        credit_memo__c creditMemo = new credit_memo__c(amount__c = 200000.00,Lease__c = lease.id,
                                                       Credit_Memo_Date__c=system.today(),
                                                       lessee__c = lessee.id);
        
        insert creditMemo;
        
        test.stopTest();
        List<Operational_Transaction_Register__c> listRec1 = [Select id, name ,Module__c,Transaction_Type__c,
        Transaction_Source__c, Lease__c ,Lessor__c, Lessee__c, Invoice__c from Operational_Transaction_Register__c where Credit_Memo__c=:creditMemo.Id];
        System.debug('Test Credit Memo:' + listRec1); 
        System.assertEquals('Credit Memo', listRec1[0].Transaction_Source__c);
        System.assertEquals('Receivable', listRec1[0].Module__c); 
        System.assertEquals('Credit Memo', listRec1[0].Transaction_Type__c);
    }
    
    @istest
    static void testPaybles(){
            Test.startTest();
        lease__c lease = [select id,Security_Deposit_Ca__c,Total_Loc__c from lease__c limit 1];
        operator__c lessee = [select id from operator__c limit 1];
        
        LeaseWareUtils.clearFromTrigger();
        
        cash_security_deposit__c sd = [select id,name, Deposit_Payments_Received__c,Deposit_Returned__c ,
                                       refund_committed__c,Security_Depo_Cash__c from cash_security_deposit__c where Lease__c=:lease.Id];
        
        sd.Deposit_Payments_Received__c = 100000;
        LeaseWareUtils.clearFromTrigger();
        update sd;
        LeaseWareUtils.clearFromTrigger();
        Invoice__c invoice = new Invoice__c(Invoice_Date__c =system.today(),lease__c = lease.id,
                                            Cash_Security_Deposit__c =  sd.id,
                                            invoice_type__c = 'Security Deposit',
                                            recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Security_Deposit').getRecordTypeId(),
                                            status__c ='Approved');
        insert invoice;
        
        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoice.id, payment_date__c=date.today(),
                                               Amount__c=100000,payment_status__c='Pending');
        insert newPayment;
        
        LeaseWareUtils.clearFromTrigger();

        payment__c pmt = [select id,payment_status__c from payment__c];
        pmt.payment_status__c = 'Approved';
        update pmt;
        
        payable__c py = new payable__c(payable_amount__c = 50000,status__c='Approved',Payable_Due_Date__c=system.today(),lease__c = lease.id,
                                       cash_security_deposit__c=sd.id);
        insert py;
        
     
        test.stopTest();
        List<Operational_Transaction_Register__c> listRec1 =  [Select id, name ,Module__c,Transaction_Type__c,Transaction_Source__c, 
                                                               Lease__c ,Lessor__c, Lessee__c, Invoice__c
                                                               from Operational_Transaction_Register__c where payables__c =: py.Id];
        System.debug('Test Payable:' + listRec1); 
        System.assertEquals('Security Deposit Refund', listRec1[0].Transaction_Source__c);
        System.assertEquals('Payable', listRec1[0].Module__c); 
        System.assertEquals('Payable', listRec1[0].Transaction_Type__c); 
        
    }

    @istest
    static void testEventInvoice(){    
		Constituent_Assembly__c assembly = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Airframe' LIMIT 1];

		Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: assembly.Id LIMIT 1];

		Assembly_MR_Rate__c mrInfo = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c,Assembly_Lkp__r.Type__c
                                        ,Available_Reference_Date__c,Assembly_Event_Info__r.Event_Type__c
                                        
		                              FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: assembly.Id LIMIT 1];

		Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];

		Test.startTest();
		MRClaimController.getAssemblyMRInfo(claim.Id);
		MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
		MRClaimController.getAssemblyMRInfos(claim.Id, ''); 
		MRClaimController.findMRClaimAvailable(mrInfo, claim.Id);

		claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,Supplemental_Rent__r.Lease__r.Lessor__c,
		         Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c,Supplemental_Rent__r.Lease__r.lessee__c,Supplemental_Rent__r.Lease__r.operator__c
		         ,Approved_Claim_Amount__c, Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c,Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c
		         FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];
		LeasewareUtils.clearFromTrigger();
		MRClaimController.calculateTotalInvoiceAmount(claim.Id);
		MRClaimController.createEventInvoice(claim, mrInfo, claim.Approved_Claim_Amount__c, '2019-08-08', 'Removal', 2000.45, 0);

		Test.stopTest();
        
         List<Operational_Transaction_Register__c> listRec1= [Select id, name ,Module__c,
                                                              Transaction_Type__c,Transaction_Source__c, Lease__c ,Lessor__c,
                                                              Lessee__c, Invoice__c from Operational_Transaction_Register__c];
        System.debug('Test Event Invoice:' + listRec1);     
        System.debug('Test Event Invoice:' + listRec1.size());    
        System.assertEquals('MR Claim Event', listRec1[0].Transaction_Source__c, 'Incorrect transaction source' );
        System.assertEquals('Payable', listRec1[0].Transaction_Type__c,'Type has to be Payable') ;  
    }

    @istest
    static void testEventInvoicePaid(){    
        
		Constituent_Assembly__c assembly = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Airframe' LIMIT 1];

		Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: assembly.Id LIMIT 1];

		Assembly_MR_Rate__c mrInfo = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c,Assembly_Lkp__r.Type__c
                                        ,Available_Reference_Date__c,Assembly_Event_Info__r.Event_Type__c
                                        
		                              FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: assembly.Id LIMIT 1];

		Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];

		Test.startTest();
		MRClaimController.getAssemblyMRInfo(claim.Id);
		MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
		MRClaimController.getAssemblyMRInfos(claim.Id, ''); 
		MRClaimController.findMRClaimAvailable(mrInfo, claim.Id);

		claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,
                Supplemental_Rent__r.Lease__r.lessee__c,Supplemental_Rent__r.Lease__r.operator__c, Supplemental_Rent__r.Lease__r.Lessor__c
		         ,Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c,
		         Approved_Claim_Amount__c, Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c,Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c
		         FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];
		LeasewareUtils.clearFromTrigger();
		MRClaimController.calculateTotalInvoiceAmount(claim.Id);
		MRClaimController.createEventInvoice(claim, mrInfo, claim.Approved_Claim_Amount__c, '2019-08-08', 'Removal', 2000.45,0);

        payable__c py = [select payable_amount__c ,status__c,Payable_Due_Date__c,lease__c,lessee__c,claim__c,Payable_Type__c 
                        from payable__c limit 1];
     
     
        payout__c  payoutRec = new payout__c();
        payoutRec.Lease__c = py.Lease__c;
        payoutRec.Lessee__c= py.Lessee__c;
        payoutRec.Payout_Date__c = System.today();
        payoutRec.Payout_Type__c = 'Offset';
        payoutRec.Status__c = 'Approved';
        payoutRec.Payables_Type__c = py.Payable_Type__c;
        payoutRec.Payout_Amount__c = claim.Approved_Claim_Amount__c;
        LeasewareUtils.clearFromTrigger();
        insert payoutRec;
        Test.stopTest();

        List<Operational_Transaction_Register__c> listRec1= [Select id, name ,Module__c,
                                                              Transaction_Type__c,Transaction_Source__c, Lease__c ,Lessor__c,
                                                              Lessee__c, Invoice__c from Operational_Transaction_Register__c where payouts__c = :payoutRec.id];
        System.debug('Test Event Invoice:' + listRec1);     
        System.debug('Test Event Invoice:' + listRec1.size());    
        System.assertEquals('MR Claim Event', listRec1[0].Transaction_Source__c, 'The transaction source is incorrect' );
        System.assertEquals('Payout - Offset', listRec1[0].Transaction_Type__c,'Type has to be Payout if event invoice is paid') ;  
    }

    
    @istest
    static void testPayouts(){
        Test.startTest();
        
        cash_security_deposit__c sd = [select id,refund_committed__c,Security_Depo_Cash__c,Deposit_Payments_Received__c from cash_security_deposit__c limit 1];
        lease__c lease = [select id,Security_Deposit_Ca__c,Total_Loc__c from lease__c limit 1];
        
        Invoice__c invoice = new Invoice__c(Invoice_Date__c =system.today(),lease__c = lease.id,
                                            Cash_Security_Deposit__c =  sd.id,
                                            invoice_type__c = 'Security Deposit',
                                            recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Security_Deposit').getRecordTypeId(),
                                            status__c ='Approved');
        insert invoice;
        
        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoice.id, payment_date__c=date.today(),
                                               Amount__c=100000,payment_status__c='Pending');
        insert newPayment;
        
        LeaseWareUtils.clearFromTrigger();
        
        
        Invoice__c invoices = [Select Id,Status__c From invoice__c limit 1];
        sd.Deposit_Payments_Received__c = 100000;
        LeaseWareUtils.clearFromTrigger();
        update sd;
        payable__c py = new payable__c(payable_amount__c = 50000,status__c='Pending',Payable_Due_Date__c=system.today(),lease__c = lease.id,
                                       cash_security_deposit__c=sd.id);
        
        LeaseWareUtils.clearFromTrigger();
        insert py;
        payable__c py1 = new payable__c(payable_amount__c = 100000,status__c='Pending',Payable_Due_Date__c=system.today(),lease__c = lease.id,
                                        cash_security_deposit__c=sd.id);
        LeaseWareUtils.clearFromTrigger();
        insert py1;
        LeaseWareUtils.clearFromTrigger();
        py.status__c = 'Approved';
        update py;
        
        payout__c po = new payout__c(Lease__c = lease.id,Payout_Amount__c=150000,Payout_Type__c ='Cash',Status__c='Pending',Payout_Date__c=system.today());
        LeaseWareUtils.clearFromTrigger();
        insert po;
        
        payable_line_item__c pli = new payable_line_item__c(payable__c = py.id,payout__c=po.id);
        insert pli;

        test.stopTest();
        
         List<Operational_Transaction_Register__c> listRec1= [Select id, name ,Module__c,
                                                              Transaction_Type__c,Transaction_Source__c, Lease__c ,Lessor__c,
                                                              Lessee__c, Invoice__c from Operational_Transaction_Register__c where payouts__c =: po.Id];
        System.debug('Test Payout:' + listRec1); 
        System.assertEquals('Payable', listRec1[0].Module__c); 
        System.assertEquals('Payout - Cash', listRec1[0].Transaction_Type__c);         
    }

    @isTest(seeAllData=false)
    static void testCodeCoverage1(){
		 Test.startTest();
		Lease__c lease = [select id from Lease__c limit 1];
        Invoice__c invoice = new Invoice__c(Invoice_Date__c =system.today(),lease__c = lease.id,
                invoice_type__c = 'Other',
                recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Other').getRecordTypeId(),
                status__c ='Approved');
                insert invoice;
		

        OperationalTransactionRegisterHandler batch = new OperationalTransactionRegisterHandler(false,1,new Process_Request_Log__c());
		OperationalTransactionRegisterHandler.getInsertBatchSize();

        OperationalTransactionRegisterHandler.initBatch();

        Operational_Transaction_Register__c oprtnltrnsc = new Operational_Transaction_Register__c();
        oprtnltrnsc.name = invoice.Name;
        oprtnltrnsc.Lease__c = invoice.Lease__c;
        oprtnltrnsc.Invoice__c = invoice.Id;
        oprtnltrnsc.Lessee__c = ((invoice.Lease__r.Lessee__c != null) ?invoice.Lease__r.Lessee__c : invoice.Lease__r.Operator__c);
        oprtnltrnsc.Lessor__c = invoice.Lease__r.Lessor__c;
        oprtnltrnsc.Document_Type__c = String.valueOf(invoice.Id);
        
        insert oprtnltrnsc;
		
        OperationalTransactionRegisterHandler.FailedRecord failedRec = new OperationalTransactionRegisterHandler.FailedRecord();  

        failedRec.DocumentName = Invoice.Id ;
        failedRec.Name = invoice.Name ;
        failedRec.DocumentDate = String.valueOf(Date.today()) ;
        failedRec.FailedReason = 'Missing value' ;
        
        List<OperationalTransactionRegisterHandler.FailedRecord> listFailedRecords = new List<OperationalTransactionRegisterHandler.FailedRecord>();
        listFailedRecords.add(failedRec);
        
		String strMsg = '';

		batch.createEmailBody(listFailedRecords, strMsg);
		Test.stopTest();
    }

    @isTest(seeAllData=false)
    static void testCodeCoverage2(){
		 Test.startTest();
		Lease__c lease = [select id from Lease__c limit 1];
        Invoice__c invoice = new Invoice__c(Invoice_Date__c =system.today(),lease__c = lease.id,
                invoice_type__c = 'Other',
                recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Other').getRecordTypeId(),
                status__c ='Approved');
                insert invoice;
		
                OperationalTransactionRegisterHandler.futureManageOperationTransactionRegisterRecord(new Set<Id>{invoice.Id});
                OperationalTransactionRegisterHandler batch = new OperationalTransactionRegisterHandler(false,1,new Process_Request_Log__c());
                batch.createParallelReportingRecords(new List<Lease__c>{lease});

		Test.stopTest();
    }
    
}