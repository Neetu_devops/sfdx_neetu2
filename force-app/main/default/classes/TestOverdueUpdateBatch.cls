@isTest
public with sharing class TestOverdueUpdateBatch {
    @testsetup
    static void setupData(){
            Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
            insert aircraft;

            Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
            insert operator;

            Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,Lease_End_Date_New__c=Date.today().addYears(10),Lease_Start_Date_New__c=Date.today().toStartOfMonth());
            insert lease;
            
            Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                                                Name='Rent Schedule Fixed',
                                                Lease__c=lease.Id,
                                                Rent_Type__c='Fixed Rent',
                                                Rent_Period__c='Monthly',
                                                Base_Rent__c=200000.00,
                                                Start_Date__c=system.today().toStartOfMonth() , // YYYY-MM-DD
                                                Rent_End_Date__c=system.today().addYears(10) ,  // YYYY-MM-DD
                                                Invoice_Generation_Day__c='1',
                                                Rent_Period_End_Day__c='30',
                                                Rent_Due_Type__c='Fixed',
                                                Rent_Due_Day__c=15,
                                                Escalation__c=3,
                                                Escalation_Month__c='January',
                                                Pro_rata_Number_Of_Days__c='Actual number of days in month/year');
            insert MultiRentSch;
            
           
            rent__c rent1 = new rent__c(RentPayments__c= lease.id,Name='New Rent 1',For_Month_Ending__c=system.today().addMonths(1).toStartOfMonth().addDays(-1),start_date__c=system.today().toStartOfMonth(),Stepped_Rent__c =MultiRentSch.id);
            rent__c rent2 = new rent__c(RentPayments__c= lease.id,Name='New Rent 2',For_Month_Ending__c=system.today().addMonths(2).toStartOfMonth().addDays(-1),start_date__c=system.today().addMonths(1).toStartOfMonth(),Stepped_Rent__c =MultiRentSch.id);
            rent__c rent3 = new rent__c(RentPayments__c= lease.id,Name='New Rent 3',For_Month_Ending__c=system.today().addMonths(3).toStartOfMonth().addDays(-1),start_date__c=system.today().addMonths(2).toStartOfMonth(),Stepped_Rent__c =MultiRentSch.id);
            rent__c rent4 = new rent__c(RentPayments__c= lease.id,Name='New Rent 3',For_Month_Ending__c=system.today().addMonths(4).toStartOfMonth().addDays(-1),start_date__c=system.today().addMonths(3).toStartOfMonth(),Stepped_Rent__c =MultiRentSch.id);


            list<rent__c> rentList = new list<rent__c>{rent1,rent2,rent3,rent4};
            insert rentList;

            rentList = [select id,Rent_Due_Amount__c from rent__c order by start_date__c];
            Invoice__c invoice = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rentList[0].id,amount__c = rentList[0].Rent_Due_Amount__c,status__c ='Approved',
                                                Date_of_MR_Payment_Due__c = system.today().toStartOfMonth().addDays(-1),payment_status__c = 'Open',
                                                invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
            Invoice__c invoice2 = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rentList[1].id,amount__c = rentList[1].Rent_Due_Amount__c,status__c ='Approved',
                                               Date_of_MR_Payment_Due__c = system.today().toStartOfMonth().addDays(-1),payment_status__c = 'Partially Paid',
                                               invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
            Invoice__c invoice3 = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rentList[2].id,amount__c = rentList[2].Rent_Due_Amount__c,status__c ='Declined',
                                                Date_of_MR_Payment_Due__c = system.today().toStartOfMonth().addDays(-1),overdue__c = true,payment_status__c = 'Open',
                                                invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
            Invoice__c invoice4 = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rentList[3].id,amount__c = rentList[3].Rent_Due_Amount__c,status__c ='Approved',
                                                Date_of_MR_Payment_Due__c = system.today().addMonths(10).toStartOfMonth().addDays(-1),overdue__c = true,payment_status__c = 'Open',
                                                invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
            LeaseWareUtils.TriggerDisabledFlag=true; 
                insert new list<invoice__c>{invoice,invoice2,invoice3,invoice4};
            LeaseWareUtils.TriggerDisabledFlag=false;
    }
    @isTest
    static void testOverdueBatch(){
       
        
        Test.startTest();
        UpdateRentOnLeaseScheduler sch = new UpdateRentOnLeaseScheduler();
        sch.execute(null);
        Test.stopTest();

        list<invoice__c> invList = [select id,overdue__c,Date_of_MR_Payment_Due__c,rent__r.start_date__c,balance_due__c from invoice__c order by rent__r.start_date__c];
        for(invoice__c inv:invList){
            system.debug('INV ::'+inv.Date_of_MR_Payment_Due__c+':::'+inv.overdue__c+':::'+inv.rent__r.start_date__c+':::'+inv.balance_due__c );
        }
        system.assertEquals(4,invlist.size());
        system.assertEquals(true,invlist[0].overdue__c);
        system.assertEquals(true,invlist[1].overdue__c);
        system.assertEquals(true,invlist[2].overdue__c);
        system.assertEquals(false,invlist[3].overdue__c);
    }
}