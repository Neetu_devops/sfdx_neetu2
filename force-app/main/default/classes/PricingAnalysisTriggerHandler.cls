public class PricingAnalysisTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'PricingAnalysisTriggerHandlerBefore';
    private final string triggerAfter = 'PricingAnalysisTriggerHandlerAfter';
    public PricingAnalysisTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('PricingAnalysisTriggerHandler.beforeInsert(+)');
        UpdatePricingOutput();
        setRecordType();
        

        system.debug('PricingAnalysisTriggerHandler.beforeInsert(-)');       
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('PricingAnalysisTriggerHandler.beforeUpdate(+)');
        //PricingSummaryString();
        
        system.debug('PricingAnalysisTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('PricingAnalysisTriggerHandler.beforeDelete(+)');
        
        boolean errorflag=false;
        
    list<Pricing_Output_New__c > newDeletedList = (list<Pricing_Output_New__c >)trigger.old;
        for(Pricing_Output_New__c curRec:newDeletedList){
            //Start Nagaveni - Task - Validation for deleting the Economic Analysis records if it is in approval queue
      //Deletion of Economic Analysis Request should be allowed only when it has Status 'Open' and 'PCM Review Rejected'
            if (trigger.IsDelete){
                if (curRec.Y_Hidden_Status__c == 'Open' || curRec.Y_Hidden_Status__c == 'PCM Review Rejected' ){
                    system.debug('Deleting Economic Analysis From Economic Analysis Request When Status is Open or PCM Review Rejected ');
                } else {
                    system.debug('Nagaveni Error deleting Economic Analysis Record');
                    curRec.addError('Cannot Delete Economic Analysis When Status of Economic Analysis Request is in '+ curRec.Y_Hidden_Status__c + '');
                    errorflag=true;
                    
                }
            }
            //End - Task - Validation for deleting the Economic Analysis records if it is in approval queue  
        }
        
      
        
        if(!errorflag)updateATName();
        system.debug('PricingAnalysisTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('PricingAnalysisTriggerHandler.afterInsert(+)');
        
    updateATName();
        system.debug('PricingAnalysisTriggerHandler.afterInsert(-)');        
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('PricingAnalysisTriggerHandler.afterUpdate(+)');
        setOnlyOneFinalPricing();
    
        system.debug('PricingAnalysisTriggerHandler.afterUpdate(-)');        
    }
     
    public void afterDelete()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('PricingAnalysisTriggerHandler.afterDelete(+)');
        
        
        system.debug('PricingAnalysisTriggerHandler.afterDelete(-)');        
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('PricingAnalysisTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('PricingAnalysisTriggerHandler.afterUnDelete(-)');      
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


    //only before Ins
  private void UpdatePricingOutput(){
    
    list<Pricing_Output_New__c > newList = (list<Pricing_Output_New__c >)trigger.new;
    
    set<id> setRequestIds = new set<id>();
        set<id> setAssetDetailIds = new set<id>();
    for(Pricing_Output_New__c CurRec:newList){
      setRequestIds.add(CurRec.Pricing_Run__c);
    }
    
    map<id,Pricing_Run_New__c> mapRequestList = new map<id,Pricing_Run_New__c>([select id,Additional_Notes__c,Notes__c,
                        Final__c 
                        from Pricing_Run_New__c
                        where id in :setRequestIds]);
    
    Pricing_Run_New__c TempParentRec;
        map<id, id> mapDAsByAsset = new map<id, id>();
    for(Pricing_Output_New__c curRec:newList){  
      TempParentRec = mapRequestList.get(curRec.Pricing_Run__c);
      if(TempParentRec==null) continue;
      curRec.Additional_Notes__c = TempParentRec.Additional_Notes__c;
      curRec.Notes__c = TempParentRec.Notes__c;      
            if(TempParentRec.Final__c == true && CurRec.Asset_Term__c!=null)setAssetDetailIds.add(CurRec.Asset_Term__c);
            mapDAsByAsset.put(CurRec.Asset_Term__c, TempParentRec.Id);
    }

        list<Aircraft_Proposal__c> listAssetDetails = [SELECT ID, Deal_Analysis__c, Name
                                                       FROM Aircraft_Proposal__c
                                                       WHERE Id IN :setAssetDetailIds];
        for(Aircraft_Proposal__c curAssetDet : listAssetDetails){
            curAssetDet.Deal_Analysis__c = mapDAsByAsset.get(curAssetDet.id);
        }
    	LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled
    	update listAssetDetails;
    	LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger disabled        

  }

  // after insert, before delete
  private void updateATName(){

		list<Pricing_Output_New__c > newList = (list<Pricing_Output_New__c >)(trigger.IsDelete?trigger.old:trigger.new);
        set<id> setRequestIds  = new set<id>();
        set<id> setIfDeleted  = new set<id>();
        for(Pricing_Output_New__c curRec:newList){
          setRequestIds.add(curRec.Pricing_Run__c);
          if(trigger.IsDelete)setIfDeleted.add(curRec.Id);
        }
        
        list<Pricing_Run_New__c> listEconomicReqList = [select id ,Y_Hidden_Asset_Term_String__c,Status__c,Aircraft_Type__c,
                                                        (select id,Asset_Term__r.Name,Asset_Term__r.Asset_Type__c 
                                                         from Pricing_Output_New__r
                                                         where id not in :setIfDeleted
                                                        )
                                                    from Pricing_Run_New__c 
                                                    where id in :setRequestIds];
		string ATNameStr; 
		set<string> setAircraftType  = new set<string>();               
		for(Pricing_Run_New__c CurReq : listEconomicReqList){
			ATNameStr =null;
            setAircraftType.clear();
			for(Pricing_Output_New__c curOutput :CurReq.Pricing_Output_New__r ){
                if(ATNameStr==null) ATNameStr = curOutput.Asset_Term__r.Name;
				else ATNameStr += ', ' + curOutput.Asset_Term__r.Name;
				
				if(curOutput.Asset_Term__r.Asset_Type__c!=null && curOutput.Asset_Term__r.Asset_Type__c!='') setAircraftType.add(curOutput.Asset_Term__r.Asset_Type__c);
			}
			if(ATNameStr!=null && ATNameStr.length()>255) ATNameStr=ATNameStr.left(251) + '...';
			CurReq.Y_Hidden_Asset_Term_String__c = ATNameStr;
			CurReq.Aircraft_Type__c='';

			for(string curAircraftType:setAircraftType){
                if((CurReq.Aircraft_Type__c.length() + curAircraftType.length() ) <251 ){
					CurReq.Aircraft_Type__c+= curAircraftType  + '\n' ;
                }else{ 
                    CurReq.Aircraft_Type__c+= '...'; 
                    break;  
                }
			}
		}	
    
		LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled
		update listEconomicReqList;
		LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger disabled
    }
    //after update,after insert
    private void setOnlyOneFinalPricing(){
        system.debug('setOnlyOneFinalPricing+');
        list<Pricing_Output_New__c > newList = (list<Pricing_Output_New__c >)trigger.new;

        /* Mark other primary Timeline to false*/
        set<Id> setAssetTermId = new set<Id>();    
        for(Pricing_Output_New__c curRec:newList){
            if(trigger.IsInsert && curRec.Final_Pricing__c){
                if(setAssetTermId.contains(curRec.Asset_Term__c))curRec.Final_Pricing__c.addError('You can mark only one final pricing.');
                setAssetTermId.add(curRec.Asset_Term__c);              
            }
            else if(trigger.IsUpdate && curRec.Final_Pricing__c && !((Pricing_Output_New__c)trigger.oldMap.get(curRec.Id)).Final_Pricing__c){
                if(setAssetTermId.contains(curRec.Asset_Term__c))curRec.Final_Pricing__c.addError('You can mark only one final pricing.');
                setAssetTermId.add(curRec.Asset_Term__c);              
            }            
        }
        
        system.debug('setAssetTermId='+setAssetTermId.size());
        if(!setAssetTermId.isEmpty()){
            list<Pricing_Output_New__c> updatePricing= new list<Pricing_Output_New__c>();
            map<id,Aircraft_Proposal__c> mapATAll = new map<id,Aircraft_Proposal__c>([select id
                                            ,(select id,Final_Pricing__c
                                                from Pricing_Output_new__r where (id not in :newList) and Final_Pricing__c= true)
                                                 
                                        from  Aircraft_Proposal__c where Id in :setAssetTermId]);
            
            for(Aircraft_Proposal__c curAT:mapATAll.values()){
                
                for(Pricing_Output_New__c curPricing:curAT.Pricing_Output_new__r){
                    curPricing.Final_Pricing__c =false;
                    updatePricing.add(curPricing);
                }
                
            }
            system.debug('updatePricing='+updatePricing.size());
            if(!updatePricing.IsEmpty()){
                LWGlobalUtils.setTriggersflagOn(); //TriggerDisabledFlag=true;
                update updatePricing;
                LWGlobalUtils.setTriggersflagOff();   //TriggerDisabledFlag=false;
            }
            
            list<Aircraft_Proposal__c> updateAT = new list<Aircraft_Proposal__c>();
            Aircraft_Proposal__c tempAT;            
            for(Pricing_Output_New__c curPricing:newList){
                system.debug('curPricing.Final_Pricing__c='+curPricing.Final_Pricing__c + curPricing.id);
                if(curPricing.Final_Pricing__c){
                    tempAT = mapATAll.get(curPricing.Asset_Term__c);
                    tempAT.IRR__c = curPricing.IRR_with_MR_and_Security_Deposit__c;
                    tempAT.Annual_EBT__c = curPricing.Avg_Annual_EBT_with_MR_Sec_Deposit__c;
                    tempAT.Rent__c = curPricing.Total_Rent_inc_NVA__c;
                    tempAT.Base_Swap_Rate__c = curPricing.Base_Swap_Rate_F__c;
                    tempAT.Swap_Tenor__c = curPricing.Swap_Tenor__c;
                    tempAT.Est_LRF__c = curPricing.Lease_Rate_Factor__c;
                    tempAT.Est_All_In_Cost__c = curPricing.Estimated_All_In_Cost__c;
                    tempAT.Early_Exit_Term__c = curPricing.Early_Exit_Term_Yrs__c;
                    tempAT.Early_Exit_IRR__c = curPricing.Early_Exit_IRR__c;
                    
                    tempAT.Engine_Manufacturer__c = curPricing.Engine_Manufacturer__c;
                    tempAT.Engine_Type_new__c = curPricing.Engine_Type__c;
                    tempAT.Maintenance_Reserves__c = curPricing.Maintenance_Reserves__c;
                    tempAT.Type_of_MR_If_Cash__c = curPricing.Type_of_MR_If_Cash__c;
                    tempAT.MR_Base_Year__c = curPricing.MR_Base_Year__c;
                    tempAT.Airframe_6Y__c = curPricing.Airframe_6Y_8Y_9Y_per_Month__c;
                    tempAT.Airframe_8Y__c = curPricing.Airframe_12_Y_per_Month__c;
                    tempAT.Engine_1st_Run_per_FH__c = curPricing.Engine_1st_Run_per_FH__c;
                    tempAT.Engine_Subsequent_Run_per_FH__c = curPricing.Engine_Subsequent_Run_per_FH__c;
                    tempAT.Engine_LLP_per_Cycle__c = curPricing.Engine_LLP_per_Cycle__c;
                    tempAT.Landing_Gear_per_Month__c = curPricing.Landing_Gear_per_Month__c;
                    
                    tempAT.APU_per_Hour__c = curPricing.APU_per_Hour__c;
                    tempAT.Rate_Type__c = curPricing.Rate_Type__c;
                    tempAT.Rent_Base_Month__c = curPricing.Rent_Base_Month__c;
                    tempAT.Y_Hidden_Estimated_NBV_at_Delivery__c = curPricing.Estimated_All_In_Cost__c;
                    tempAT.Y_Hidden_Estimated_NBV_at_the_EOL__c = curPricing.NBV_at_Lease_Maturity__c;
                    tempAT.Deposit_Type__c = curPricing.Deposit_Type__c;
                    tempAT.Rent_Multiple__c = curPricing.Deposit_Rent_Multiple__c;
                    tempAT.Deposit__c = curPricing.Deposit__c;
                    
                    tempAT.MTOW_lbs__c = curPricing.MTOW_lbs__c;
                    tempAT.Est_Del_Lease_Rate__c = curPricing.Total_Escalated_Rent_Inc_NVA__c;
                    tempAT.Purchase_Price_Inc_NVA_If_Known__c = curPricing.Purchase_Price_Inc_NVA_If_Known__c;
                    tempAT.Price_As_of_Date__c = curPricing.Price_As_of_Date__c;
                    tempAT.Non_Value_Added_IFE__c = curPricing.Non_Value_Add__c;
                    tempAT.ROE_with_MR_and_Security_Deposit__c = curPricing.ROE_with_MR_and_Security_Deposit__c;
                    tempAT.Purchase_Price_Inc_NVA_If_Known__c = curPricing.Purchase_Price_Inc_NVA_If_Known__c; 
                    
                    
                                        
                    
                    
                if(curPricing.Lease_Term__c==null || curPricing.Lease_Term__c=='End of Lease (If Currently on Lease)') tempAT.Term_Mos__c = 0;
                else tempAT.Term_Mos__c = decimal.valueOf(curPricing.Lease_Term__c.split(' ',0)[0]) *12 ;                    

                    updateAT.add(tempAT);                    
                }            
            }
            if(!updateAT.IsEmpty()){
                LWGlobalUtils.setTriggersflagOn(); //TriggerDisabledFlag=true;
                update updateAT;
                LWGlobalUtils.setTriggersflagOff();   //TriggerDisabledFlag=false;
            }            
        }
        system.debug('setOnlyOneFinalPricing-');
        
    }  
	
	// before insert set same record type as parent record type name.
    private void setRecordType() {
    
        List<Pricing_Output_New__c > newList = (List<Pricing_Output_New__c >)Trigger.new;
        
        Set<Id> setCommercialTermId = new Set<Id>();
        
        for(Pricing_Output_New__c CurRec:newList) {
            setCommercialTermId.add(CurRec.Pricing_Run__c);
        }
        
        if(setCommercialTermId.size() > 0) {
        
            Map<Id, Pricing_Run_New__c> mapCommercialTerm = new Map<Id, Pricing_Run_New__c>([SELECT Id, RecordType.Name
                                                                                             FROM Pricing_Run_New__c
                                                                                             WHERE Id IN :setCommercialTermId]);
            
            Map<String, Schema.RecordTypeInfo> mapRecordTypeInfoSchema = Schema.SObjectType.Pricing_Output_New__c.getRecordTypeInfosByName();
            
            for(Pricing_Output_New__c curRec : newList) { 
                String recordTypeName = mapCommercialTerm.get(curRec.Pricing_Run__c).RecordType.Name;
                if(mapRecordTypeInfoSchema.containsKey(recordTypeName)) {
                    curRec.RecordTypeId = mapRecordTypeInfoSchema.get(recordTypeName).getRecordTypeId();
                }
            }
        }
        
    }


}