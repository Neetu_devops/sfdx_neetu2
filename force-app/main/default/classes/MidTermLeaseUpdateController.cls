public class MidTermLeaseUpdateController {
    
    @AuraEnabled
    public static string getNamespacePrefix(){
        System.debug('LeaseWareUtils.getNamespacePrefix :'+LeaseWareUtils.getNamespacePrefix());
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled
    Public static Lease__c getLeaseInfo(String recordId){
        List<Lease__c> leaseList = [SELECT Id, Name, Aircraft__c, Aircraft__r.MSN_Number__c, Lessor__c, 
                          Lessor__r.Name, Lessee__c, Lessee__r.name,Lease_Type_Legal__c,
                          Lease_Start_Date_New__c, Lease_End_Date_New__c 
                          FROM Lease__c
                          WHERE Id =: recordId 
                         ];
        if(leaseList.size() > 0){
        return leaseList[0];
        }
        return null;
    } 
    
    @AuraEnabled
    Public static Date getExtensionStartDate(Date endDate){
        System.debug('endDate :'+endDate);
        Date newDate = endDate.addDays(1);
        System.debug('newDate '+newDate);
        
        return newDate;
    }
    
    @AuraEnabled
    Public static Lease_Extension_History__c createLeaseExtension(String recordId, String etype, Date dateOfUpdate,Date exStartDAte, Date exEndDate, Integer term,
                                                                  Boolean isUpdateRent, String comments)
    {
        System.debug('createLeaseExtension : recordId :'+recordId+' eType: '+eType+' dateOfUpdate :'+dateOfUpdate+' exStartDAte :'+exStartDAte+' exEndDate: '+exEndDate+' comments :'+comments+'isUpdateRent :'+isUpdateRent+' term: '+term);
        Id leRecordTypeId;
        if (Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Lease_Extension') != null) {    
            leRecordTypeId = Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Lease_Extension').getRecordTypeId();
            
        }
        System.debug('leRecordTypeId :'+leRecordTypeId);
        Lease__c  lease = [SELECT Id,Name,Previous_Lease_End_Date__c,Lease_End_Date_New__c,Previous_Lease_Term__c,Extension_Period_Start_date__c,Extension_Period_End_date__c,Lease_Term__c
                           FROM Lease__c 
                           WHERE Id =:recordId];
        
        
        String extensionDate = exEndDate.format();
        System.debug('extensionDate :'+extensionDate);
        Lease_Extension_History__c leaseExt = new Lease_Extension_History__c();
        leaseExt.Name = lease.Name+' End Date changed to '+extensionDate;
        leaseExt.Type__c = eType;
        leaseExt.Date_Updated__c = dateOfUpdate;
        leaseExt.Extension_Period_Start_date__c = exStartDAte;
        leaseExt.Extension_Period_End_date__c = exEndDate;                                                            
        leaseExt.Lease__c = recordId;
        leaseExt.RecordTypeId = leRecordTypeId;
        leaseExt.Comments__c  =  comments;
        leaseExt.Term_of_Extension__c = term;       
        leaseExt.Previous_Lease_End_Date__c = lease.Lease_End_Date_New__c;
        if(lease != null){
        lease.Previous_Lease_End_Date__c = lease.Lease_End_Date_New__c;
        lease.Previous_Lease_Term__c =  lease.Lease_Term__c;
        lease.Extension_Period_Start_date__c = exStartDAte;
        lease.Extension_Period_End_date__c = exEndDate;
        lease.Lease_End_Date_New__c = exEndDate; 
        }
        
        List<Stepped_Rent__c> rentSchedule = new List<Stepped_Rent__c>();
        if(isUpdateRent != null && isUpdateRent==true){
            System.debug(' isUpdateRent : '+isUpdateRent);
             rentSchedule = [Select Id,Name,IsNewSchedule__c,IsUpdatedSchedule__c,LastModifiedDate ,Lease__c from Stepped_Rent__c
                                                  where Lease__c =: recordId ORDER BY  LastModifiedDate  Desc];
            System.debug(' rentSchedule : '+rentSchedule.size());
            if(rentSchedule.size() > 0){
                System.debug('if New :'+rentSchedule[0].IsNewSchedule__c);
                System.debug('if Updated :'+rentSchedule[0].IsUpdatedSchedule__c);
                if(rentSchedule[0].IsNewSchedule__c){
                    System.debug('if New :'+rentSchedule[0].IsNewSchedule__c);
                        leaseExt.New_Rent_Schedule__c = rentSchedule[0].Id;
                    System.debug('if New : '+leaseExt.New_Rent_Schedule__c);
                }
                else if(rentSchedule[0].IsUpdatedSchedule__c) {
                    System.debug('if Updated :'+rentSchedule[0].IsUpdatedSchedule__c);
                    leaseExt.Updated_Rent_Schedule__c = rentSchedule[0].Id;
                    System.debug('if Updated : '+leaseExt.Updated_Rent_Schedule__c);
                }
             }
             //in order to avoid recursive behaviour of MultiRentHandler trigger
                LeaseWareUtils.TriggerDisabledFlag=true;
                rentSchedule[0].IsNewSchedule__c = false;
                rentSchedule[0].IsUpdatedSchedule__c = false;
                update rentSchedule[0];
                System.debug('Trigger Disabled and uPDATED :'+rentSchedule[0].IsNewSchedule__c+ ' : '+rentSchedule[0].IsUpdatedSchedule__c);
                LeaseWareUtils.TriggerDisabledFlag=false;
        }
        
        LW_Setup__c lwSetup = LW_Setup__c.getInstance('IsLeaseExtensionAllowedOnLease');
        if(lwSetup == null || lwSetup.Value__c != 'ON') {
        insert leaseExt;
        System.debug('leaseExt :'+leaseExt);
            update lease;
        }
        return leaseExt;
    }
    
    @AuraEnabled
    Public static Lease_Extension_History__c createLeaseAmendment(String recordId, String aType, Date dateOfUpdate,
                                                                  String aName, Date aDate, String comments){
                                                                      
                                                                      System.debug('createLeaseAmendment : recordId :'+recordId+' aType: '+aType+' dateOfUpdate :'+dateOfUpdate+' aName :'+aName+' aDate: '+aDate+' comments :'+comments);
                                                                      Id amdRecordTypeId;
                                                                      if (Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Lease_Amendment') != null) {    
                                                                          amdRecordTypeId = Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Lease_Amendment').getRecordTypeId();
                                                                          
                                                                      }
                                                                      System.debug('amdRecordTypeId :'+amdRecordTypeId);
                                                                      
                                                                      Lease_Extension_History__c amend = new Lease_Extension_History__c();
                                                                      amend.Name = aName;
                                                                      amend.Type__c = aType;
                                                                      amend.Date_of_Amendment__c = aDate;
                                                                      amend.Date_Updated__c = dateOfUpdate;
                                                                      amend.Lease__c = recordId;
                                                                      amend.RecordTypeId = amdRecordTypeId;
                                                                      amend.Comments__c  =  comments;
                                                                      
                                                                      insert amend;
                                                                      System.debug('amend :'+amend);
                                                                      return amend;
                                                                  }
    @AuraEnabled
    Public static Lease_Extension_History__c createSideLetter(String recordId, String sType, Date dateOfUpdate,
                                                              String sName, Date sDate, String comments){
                                                                  
                                                                  System.debug('createSideLetter : recordId :'+recordId+' sType: '+sType+' dateOfUpdate :'+dateOfUpdate+' sName :'+sName+' sDate: '+sDate+' comments :'+comments);
                                                                  Id slRecordTypeId;
                                                                  if (Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Side_Letter') != null) {    
                                                                      slRecordTypeId = Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Side_Letter').getRecordTypeId();
                                                                      
                                                                  }
                                                                  System.debug('slRecordTypeId :'+slRecordTypeId);
                                                                  
                                                                  Lease_Extension_History__c sideLettr = new Lease_Extension_History__c();
                                                                  sideLettr.Name = sName;
                                                                  sideLettr.Type__c = sType;
                                                                  sideLettr.Effective_date_of_side_letter__c = sDate;
                                                                  sideLettr.Date_Updated__c = dateOfUpdate;
                                                                  sideLettr.Lease__c = recordId;
                                                                  sideLettr.RecordTypeId = slRecordTypeId;
                                                                  sideLettr.Comments__c  =  comments;
                                                                  
                                                                  insert sideLettr;
                                                                  System.debug('sideLettr :'+sideLettr);
                                                                  return sideLettr;
                                                                  
                                                              }                                                                
    @AuraEnabled
    Public static Lease_Extension_History__c createNovation(String recordId,String nType,Date dateOfUpdate,String nName,
                                                            Date nDate,String comments,String lessor,String managingCompany,String guarantor,
                                                            String financir,String securty,String lessorMR,String lessorRent)
    {
        
        System.debug('createNovation : recordId :'+recordId+' nType: '+nType+' dateOfUpdate :'+dateOfUpdate+' nName :'+nName+' nDate: '+nDate+' comments :'+comments);
        System.debug('createNovation : lessor :'+lessor+' guarantor: '+guarantor+'managingCompany :'+managingCompany+ ' financir :'+financir+' securty :'+securty+' lessorMR: '+lessorMR+' lessorRent :'+lessorRent);
         
        Lease__c lease = [SELECT Id,Lessor__c,Guarantor__c,Financier__c,Managing_Company__c,Security_Trustee__c,
                          Lessor_Bank_For_MR__c,Lessor_Bnk_for_Rent__c
                          FROM Lease__c 
                          WHERE Id =:recordId];
        System.debug('Guarantor1 : '+lease.Guarantor__c);
        
        
        
        Id novRecordTypeId;
        if (Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Novation') != null) {    
            novRecordTypeId = Schema.SObjectType.Lease_Extension_History__c.getRecordTypeInfosByDeveloperName().get('Novation').getRecordTypeId();
            
        }
        System.debug('novRecordTypeId :'+novRecordTypeId);
        
        Lease_Extension_History__c nov = new Lease_Extension_History__c();
        nov.Name = nName;
        nov.Type__c = nType;
        nov.Effective_date_of_Novation__c = nDate;
        nov.Date_Updated__c = dateOfUpdate;
        nov.Lease__c = recordId;
        nov.RecordTypeId = novRecordTypeId;
        nov.Comments__c  =  comments;
        nov.Previous_Lessor__c = lease.Lessor__c;
        System.debug('Guarantor3 : '+lease.Guarantor__c);
        nov.Previous_Guarantor__c = lease.Guarantor__c;
        System.debug('Guarantor4 : '+lease.Guarantor__c);
        nov.Previous_Financier__c = lease.Financier__c;
        nov.Previous_Managing_Company__c = lease.Managing_Company__c;
        nov.Previous_Security_Trustee__c = lease.Security_Trustee__c;
        nov.Previous_Lessor_Bank_for_MR__c = lease.Lessor_Bank_For_MR__c;
        nov.Previous_Lessor_Bank_for_Rent__c = lease.Lessor_Bnk_for_Rent__c;
        nov.New_Lessor__c = lessor;
        nov.New_Managing_Company__c = managingCompany;                                 
        nov.New_Guarantor__c = guarantor;
        nov.New_Financier__c = financir;
        nov.New_Security_Trustee__c = securty;
        nov.New_Lessor_Bank_for_MR__c = lessorMR;
        nov.New_Lessor_Bank_for_Rent__c = lessorRent; 
        
        if(lessor != null){
           lease.Lessor__c = lessor; 
        }
        if(guarantor != null){
            lease.Guarantor__c = guarantor;
        }
       System.debug('Guarantor2 : '+lease.Guarantor__c);
        if(managingCompany != null){
            lease.Managing_Company__c = managingCompany;
        }
        if(financir != null){
            lease.Financier__c = financir;
        }
        if(securty != null){
            lease.Security_Trustee__c = securty;
        }
        if(lessorMR != null){
            lease.Lessor_Bank_For_MR__c = lessorMR;
        }
        if(lessorRent != null){
            lease.Lessor_Bnk_for_Rent__c = lessorRent;
        }
        lease.Lease_Novation_Date__c = nDate;
        
        insert nov;
        update lease;
        System.debug('nov :'+nov);
        return nov;                                                           
        
    }
    
    @AuraEnabled
    public static LookUpWrapper fetchLookupData(String lookupId, String flag){
        System.debug('getLookupData recordId :'+lookupId+' flag :'+flag);
        if(flag == 'Lessor'){
            List<Counterparty__c> lookupRecord = [Select Id,Name from Counterparty__c where Id =:lookupId];
            System.debug('getLookupData lessor lookupRecord :'+lookupRecord);
            if(lookupRecord.size() > 0){
            LookUpWrapper rec = new LookUpWrapper();
            rec.LId = lookupRecord[0].Id;
            rec.LName = lookupRecord[0].Name;
            
            System.debug('getLookupData rec :'+rec);
            return rec;
            }else{
                return null;
            }
        }
        else if(flag == 'Account'){
            List<Account>  lookupRecord = [Select Id, Name from Account where Id =:lookupId]; 
            System.debug('getLookupData account lookupRecord :'+lookupRecord);
            if(lookupRecord.size() > 0){
            LookUpWrapper rec = new LookUpWrapper();
            rec.LId = lookupRecord[0].Id;
            rec.LName = lookupRecord[0].Name;
            
            System.debug('getLookupData rec :'+rec);
            return rec;
        }
            else{
                return null;
            }
        }
        else if(flag == 'Bank'){
            
            List<Bank__c>  lookupRecord = [Select Id, Name from Bank__c where Id =:lookupId]; 
            System.debug('getLookupData bank lookupRecord :'+lookupRecord);
            if(lookupRecord.size()>0){
            LookUpWrapper rec = new LookUpWrapper();
            rec.LId = lookupRecord[0].Id;
            rec.LName = lookupRecord[0].Name;
            
            System.debug('getLookupData rec :'+rec);
            return rec;
        }
            else{
                return null;
            }
        
    }
        return null;
    }
    
    @AuraEnabled
    Public static List<String> getPickListValues(String objectName, String fld) {
        String namespace = getNamespacePrefix();
        objectName = namespace + objectName;
        objectName = objectName.replace(namespace + namespace , namespace);
        
        if(!fld.equals('Name')) {
            fld = namespace + fld;
            fld = fld.replace(namespace + namespace , namespace);
        }
        
        system.debug('objObject --->' + objectName);
        system.debug('fld --->' + fld);
        List <String> allOpts = new list <String>();
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        return allOpts;
    }
    
    @AuraEnabled
    public static Id saveFile(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method,
        //  which is save the check data and return the attachemnt Id after insert,
        //  next time (in else) we are call the appentTOFile() method
        //   for update the attachment with remains chunks
           
        System.debug('saveFile: parentId :'+parentId+'fileName :'+fileName+'base64Data :'+base64Data+'contentType :'+contentType+'fileId :'+fileId);
        if (fileId == '') {
            System.debug('saveFile: if');
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            System.debug('saveFile: else');
            appendToFile(fileId, base64Data);
        }
        
        System.debug('saveFile: id  :'+Id.valueOf(fileId));
        return Id.valueOf(fileId);
    }
    
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
       System.debug('saveTheFile : parentId :'+parentId+'fileName :'+fileName+'base64Data :'+base64Data+'contentType :'+contentType);
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = fileName; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = fileName; // Display name of the files
        conVer.VersionData = EncodingUtil.base64Decode(base64Data); // converting your binary string to Blog
        System.debug('conVer : '+conVer);
        insert conVer;    //Insert ContentVersion
        
        
        // First get the Content Document Id from ContentVersion Object
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        //create ContentDocumentLink  record 
        ContentDocumentLink conDocLink = New ContentDocumentLink();
        conDocLink.LinkedEntityId = parentId; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
        conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
        conDocLink.shareType = 'V';
        System.debug('conDocLink : '+conDocLink);
        insert conDocLink;
        
        
        System.debug('saveTheFile oAttachment.Id; '+conDocLink.Id);
        return conDocLink.Id;
    }
    
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');        
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id = : fileId
        ];
        
        String existingBody = EncodingUtil.base64Encode(a.Body);        
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);        
        update a;
    }
    
    public class LeaseUpdateHistoryData{
        @AuraEnabled public String RentId  {get; set;}
        @AuraEnabled public String RentName  {get; set;}
        @AuraEnabled public Boolean RentComment  {get; set;}
        @AuraEnabled public Date LeaseExtStartDate  {get; set;}
        @AuraEnabled public Date LeaseExtEndDate  {get; set;}
        @AuraEnabled public Boolean UpdateRent {get; set;} 
        @AuraEnabled public String LEType  {get; set;}
        @AuraEnabled public String  AmendmentName {get; set;}
        @AuraEnabled public String AmmendComments  {get; set;}
        @AuraEnabled public Date AmendmentDate  {get; set;}
        
    }
    
    public class LookUpWrapper{
        @AuraEnabled public String LId  {get; set;}
        @AuraEnabled public String LName  {get; set;}
    }
}