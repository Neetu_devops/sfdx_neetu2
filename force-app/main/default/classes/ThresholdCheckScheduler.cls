global with sharing class ThresholdCheckScheduler implements Schedulable, Database.Batchable<SObject>{
	

	public static String strDueLeases='';
	static String[] toAddresses = new String[]{};
	
    global void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new ThresholdCheckScheduler());
    }

	global Database.QueryLocator start(Database.BatchableContext BC){
		String strQuery='Select Id, Name, Aircraft__c, Operator__c from Lease__c where Aircraft__c != null and Lease_Start_Date__c <= TODAY and Lease_End_Date__c >= TODAY';
		System.debug(strQuery);
		return Database.getQueryLocator(strQuery); 
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope){ 
		//anjani deprecated
		//String lifelimitLLPs=LeaseWareUtils.findThresholdLimits((Lease__c[])scope);
		//if(lifelimitLLPs!='')LeaseWareUtils.sendEmail('Life Limit Email', lifelimitLLPs, 'LW_CA_LLP_ThresholdGroup', 'Regular');
	
	}
	
	global void finish(Database.BatchableContext BC){
		
		
/*
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		LeaseWareUtils.getEmailsFromGroup('LW_RentSchedulerGroup', toAddresses);
		mail.setToAddresses(toAddresses); 
		mail.setSubject('Rent Overdue check over for '+ Date.today());
		mail.setPlainTextBody('Rent Overdue Check performed for the day.. (TODO) Just a debug mail, will be removed later.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
*/		
	}

}