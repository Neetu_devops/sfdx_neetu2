@isTest
public class TestAssetReviewItemController {

    @isTest
    static void loadTestCases(){
        Aircraft__c aircraft = new Aircraft__c(
            Name = 'TestAircraft',
            MSN_Number__c = '9898',
            TSN__c = 100,
            CSN__c = 100);
        insert aircraft;
        
        Asset_Review__c assetReview = new Asset_Review__c(
        	Name = 'Test asset review',
            Review_Type__c = 'Annual',
            Review_Status__c = 'OPEN',
            Asset__c = aircraft.Id
        );
        insert assetReview;
        
		String RecordTypeName = 'Physical Item';
        Map < String, Schema.RecordTypeInfo > Trans_MapByName = Schema.SObjectType.Asset_Review_Items__c.getRecordTypeInfosByName();
        Id recordTypeId_lease = Trans_MapByName.get(RecordTypeName).getRecordTypeId();
        Asset_Review_Items__c reviewRecordItem = new Asset_Review_Items__c(
            RecordTypeId = recordTypeId_lease ,
        	Item_Description__c = 'Test description',
            Review_Item_Status__c ='COMPLETED', 
            Item_Zone__c = 'EXT - EMPENNAGE',
            Asset_Review__c = assetReview.Id
        );
        insert reviewRecordItem;
        
        Asset_Review_Findings__c findings = new Asset_Review_Findings__c(
        	Finding_Description__c = 'test findings desc',
            Review_Item__c = reviewRecordItem.Id,
            Finding_Status__c = 'OPEN'
        );
        insert findings;

        AssetReviewItemController.getDefaultFieldSet();
		String namespaceStr = AssetReviewItemController.getNamespacePrefix();
		//String fieldSetName = namespaceStr + 'AssetReview';
        String fieldSetName = 'AssetReview';
        AssetReviewItemController.getSections(RecordTypeName, assetReview.Id );
    	AssetReviewItemController.getColumns('Asset_Review_Items__c', fieldSetName);
        AssetReviewItemController.getColumns('Asset_Review_Items__c', null);
        
        AssetReviewItemController.getRows('Asset_Review_Items__c', 'Asset_Review__c', assetReview.Id, fieldSetName, 
                                            RecordTypeName, 'Item_Zone__c', 'EXT - EMPENNAGE');
    
        AssetReviewItemController.getRowsBasedOnStatus('Asset_Review_Findings__c', 'Review_Item__c', 
                                                          reviewRecordItem.Id, fieldSetName, 'OPEN', 'Finding_Status__c');
        //fieldSetName = namespaceStr + 'AssetReviewDetailFeildset';
        fieldSetName = 'AssetReviewDetailFeildset';
        AssetReviewItemController.getRecordDetails('Asset_Review_Items__c', reviewRecordItem.Id, fieldSetName);
    
    }
    
    
    
}