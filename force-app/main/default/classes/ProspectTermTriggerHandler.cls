public class ProspectTermTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'ProspectTermTriggerHandlerBefore';
    private final string triggerAfter = 'ProspectTermTriggerHandlerAfter';
    public ProspectTermTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('ProspectTermTriggerHandler.beforeInsert(+)');
		MSNOfInterestUpdateIRR();
		//getCMLRFromWF();
		Defaulting();
	    system.debug('ProspectTermTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
		
    	system.debug('ProspectTermTriggerHandler.beforeUpdate(+)');
   		MSNOfInterestUpdateIRR();
		//getCMLRFromWF();
		Defaulting();
    	system.debug('ProspectTermTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('ProspectTermTriggerHandler.beforeDelete(+)');
    	
  		MSN_s_Of_Interest__c[] newProspects = (list<MSN_s_Of_Interest__c>)trigger.old;
  		for(MSN_s_Of_Interest__c curRec  :newProspects){
  			system.debug('curRec.Campaign_Active__c='+curRec.Campaign_Active__c);
  			if(!curRec.Campaign_Active__c){
  				curRec.addError('You cannot delete proposed terms from an inactive marketing campaign.');
  			}
  		}   
    	
    	system.debug('ProspectTermTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('ProspectTermTriggerHandler.afterInsert(+)');
    	MSNofInterestNoDupsAndOnlyValidAC();
    	
    	system.debug('ProspectTermTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('ProspectTermTriggerHandler.afterUpdate(+)');
    	
    	MSNofInterestNoDupsAndOnlyValidAC();
		changeAITStatus();
    	system.debug('ProspectTermTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('ProspectTermTriggerHandler.afterDelete(+)');
    	
    	MSNofInterestNoDupsAndOnlyValidAC();
    	system.debug('ProspectTermTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('ProspectTermTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('ProspectTermTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	//before insert, before update
	private void MSNOfInterestUpdateIRR(){
		
		MSN_s_Of_Interest__c[] newProspects = (list<MSN_s_Of_Interest__c>)trigger.new;

		set<id> setDPIds = new set<id>();
		for(MSN_s_Of_Interest__c curMSNofInt: newProspects){
			setDPIds.add(curMSNofInt.Deal_Proposal__c);
		}
		
		map<id, Deal_Proposal__c> mapDPs= new map<id, Deal_Proposal__c>([select id, Rent__c, Term_Mos__c, Copy_Rent_Term_To_Proposed_Terms__c
					 from Deal_Proposal__c where id in :setDPIds]);
	
		for(MSN_s_Of_Interest__c curMSNofInt: newProspects){
			
			if(trigger.isInsert){
				if(curMSNofInt.Deal_Proposal__c!=null){
					Deal_Proposal__c curParent = mapDPs.get(curMSNofInt.Deal_Proposal__c);
					if(curParent.Copy_Rent_Term_To_Proposed_Terms__c){
						if(curMSNofInt.Rent_000__c==null)curMSNofInt.Rent_000__c=curParent.Rent__c;
						if(curMSNofInt.Term_Mos__c==null)curMSNofInt.Term_Mos__c=curParent.Term_Mos__c;
					}
				}
				
			}
			if(curMSNofInt.Rent_000__c==null || curMSNofInt.Rent_000__c <=0 
				|| curMSNofInt.Term_Mos__c==null || curMSNofInt.Term_Mos__c <= 0){
				// no calculation needed for IRR/NPV
				curMSNofInt.NPV_Upfront__c=null;
				curMSNofInt.NPV_Amortize__c=null;
				curMSNofInt.NPV_End_Of_Lease__c=null;
				curMSNofInt.IRR__c=null;
				curMSNofInt.IRR_Amortize__c=null;
				curMSNofInt.IRR_End_Of_Lease__c=null;
				continue;
				
			}
			// else ...
			try{
				double investment;
				double[] CFs = new Decimal[curMSNofInt.Term_Mos__c.intValue()];
				
				double rent=curMSNofInt.Rent_000__c==null?0:curMSNofInt.Rent_000__c.doubleValue();
				for(integer i=0;i<curMSNofInt.Term_Mos__c.intValue();i++){
					CFs[i]=rent;
				}
	
				curMSNofInt.NPV_Upfront__c=null;
				curMSNofInt.NPV_Amortize__c=null;
				curMSNofInt.NPV_End_Of_Lease__c=null;
				curMSNofInt.IRR__c=null;
				curMSNofInt.IRR_Amortize__c=null;
				curMSNofInt.IRR_End_Of_Lease__c=null;
		
				if('IRR'.equals(curMSNofInt.Evaluation_Approach__c)){
					if(curMSNofInt.Upfront__c){
						investment=curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue();
						Decimal annualIRR=100*LeaseWareUtils.CalculateIRR(investment, CFs, null, null, LeaseWareUtils.Periodicity.Monthly);
						curMSNofInt.IRR__c=annualIRR>999.99?999.99:annualIRR;
					}
	
					if(curMSNofInt.End_Of_Lease__c){
						investment=curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue();
						CFs[curMSNofInt.Term_Mos__c.intValue()-1]-=investment;
						investment=0;
						Decimal annualIRR=100*LeaseWareUtils.CalculateIRR(investment, CFs, null, null, LeaseWareUtils.Periodicity.Monthly);
						curMSNofInt.IRR_End_Of_Lease__c=annualIRR>999.99?999.99:annualIRR;
						CFs[curMSNofInt.Term_Mos__c.intValue()-1]+=(curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue()); //Revert the change for Amortize..
					}
	
					if(curMSNofInt.Amortize__c){
						investment=curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue();
						for(integer i=0;i<curMSNofInt.Term_Mos__c.intValue();i++){
							CFs[i]-=(investment/curMSNofInt.Term_Mos__c.intValue());
						}
						investment=0;
						Decimal annualIRR=100*LeaseWareUtils.CalculateIRR(investment, CFs, null, null, LeaseWareUtils.Periodicity.Monthly);
						curMSNofInt.IRR_Amortize__c=annualIRR>999.99?999.99:annualIRR;
					}
						
					
					curMSNofInt.NPV_Upfront__c=null;
					curMSNofInt.NPV_Amortize__c=null;
					curMSNofInt.NPV_End_Of_Lease__c=null;
		
				}else{ //NPV
					if(curMSNofInt.Upfront__c){
						investment=curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue();
						curMSNofInt.NPV_Upfront__c=LeaseWareUtils.CalculateNPV(investment, CFs, (double)(curMSNofInt.Discount_Rate__c==null?0:curMSNofInt.Discount_Rate__c/100), LeaseWareUtils.Periodicity.Monthly);
					}
					if(curMSNofInt.End_Of_Lease__c){
						investment=curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue();
						CFs[curMSNofInt.Term_Mos__c.intValue()-1]-=investment;
						investment=0;
						curMSNofInt.NPV_End_Of_Lease__c=LeaseWareUtils.CalculateNPV(investment, CFs, (double)(curMSNofInt.Discount_Rate__c==null?0:curMSNofInt.Discount_Rate__c/100), LeaseWareUtils.Periodicity.Monthly);
						CFs[curMSNofInt.Term_Mos__c.intValue()-1]+=(curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue()); //Revert the change for Amortize..
					}
					if(curMSNofInt.Amortize__c){
						investment=curMSNofInt.Investment_Required__c==null?0:curMSNofInt.Investment_Required__c.doubleValue();
						for(integer i=0;i<curMSNofInt.Term_Mos__c.intValue();i++){
							CFs[i]-=(investment/curMSNofInt.Term_Mos__c.intValue());
						}
						investment=0;
						curMSNofInt.NPV_Amortize__c=LeaseWareUtils.CalculateNPV(investment, CFs, (double)(curMSNofInt.Discount_Rate__c==null?0:curMSNofInt.Discount_Rate__c/100), LeaseWareUtils.Periodicity.Monthly);
					}
					curMSNofInt.IRR__c=null;
					curMSNofInt.IRR_Amortize__c=null;
					curMSNofInt.IRR_End_Of_Lease__c=null;
		
				}
			}catch(exception e){
				system.debug('Exception while calculating NPV/IRR' + e);
				curMSNofInt.NPV_Upfront__c=null;
				curMSNofInt.NPV_Amortize__c=null;
				curMSNofInt.NPV_End_Of_Lease__c=null;
				curMSNofInt.IRR__c=null;
				curMSNofInt.IRR_Amortize__c=null;
				curMSNofInt.IRR_End_Of_Lease__c=null;
			}
		}
		
	}
	
	//after insert, after update ,after delete
	private void MSNofInterestNoDupsAndOnlyValidAC(){

		if(LeaseWareUtils.IsFromTrigger('MSNofInterestNoDupsAndOnlyValidAC')) return;
		LeaseWareUtils.setFromTrigger('MSNofInterestNoDupsAndOnlyValidAC');
	
		//MSN_s_Of_Interest__c[] newProspects = (list<MSN_s_Of_Interest__c>)trigger.new;
	
	    set<ID> setDPIds = new set<ID>();
	    set<ID> setAcIds = new set<ID>();
	    set<ID> setDealIds = new set<ID>();
	
		set<ID> setDPIdsForMSN = new set<ID>();
	    map<String,Id> mapMSNofInt = new  map<String,Id>();
	
	    MSN_s_Of_Interest__c[] allRec ;
	    set<String> tempSet;
	    if(trigger.IsDelete) allRec = (list<MSN_s_Of_Interest__c>)trigger.old;
	    else allRec = (list<MSN_s_Of_Interest__c>)trigger.new ;
	    for(MSN_s_Of_Interest__c curMSNofInt: allRec){
	    	setDPIds.add(curMSNofInt.Deal_Proposal__c);
	    	setAcIds.add(curMSNofInt.Aircraft__c);
	    	setDealIds.add(curMSNofInt.Deal_ID__c);
	    	if(trigger.IsUpdate) {
	    		if(((MSN_s_Of_Interest__c)trigger.oldMap.get(curMSNofInt.Id)).Awarded__c != curMSNofInt.Awarded__c){
	    			mapMSNofInt.put(((string)(curMSNofInt.Deal_ID__c)).left(15) + '-' + ((string)(curMSNofInt.Aircraft__c)).left(15),curMSNofInt.Id);
					setDPIdsForMSN.add(curMSNofInt.Deal_Proposal__c);
	    		}
	    		if(((MSN_s_Of_Interest__c)trigger.oldMap.get(curMSNofInt.Id)).MSN_of_Interest__c != curMSNofInt.MSN_of_Interest__c){
	    			setDPIdsForMSN.add(curMSNofInt.Deal_Proposal__c);
	    		}
	    	}
	    	else{
	    		if(curMSNofInt.Awarded__c){
	    			mapMSNofInt.put(((string)(curMSNofInt.Deal_ID__c)).left(15) + '-' + ((string)(curMSNofInt.Aircraft__c)).left(15),curMSNofInt.Id);
	    		}
	    		setDPIdsForMSN.add(curMSNofInt.Deal_Proposal__c);
	    	}
	    }
		//update MSN in Deal_Proposal__c
		if(!setDPIdsForMSN.isEmpty()){
			string msnStr,awardedMSNStr;	
			Deal_Proposal__c[] listDealProp = [select Interested_MSN_List__c,MSN_s_Deal_Proposal__c,Id 
						,(select MSN_of_Interest__c,Id,Awarded__c from Deal_Proposal_Aircraft__r) from  Deal_Proposal__c where Id in :setDPIdsForMSN];
			for(Deal_Proposal__c curRec:listDealProp){
				msnStr = null;
				awardedMSNStr = null;
				curRec.MSN_s_Deal_Proposal__c = '';
				curRec.Interested_MSN_List__c = '';
				for(MSN_s_Of_Interest__c curMSN:curRec.Deal_Proposal_Aircraft__r){
					if(curMSN.Awarded__c){
						if(awardedMSNStr==null)  awardedMSNStr = curMSN.MSN_of_Interest__c;
						else awardedMSNStr += ', ' + curMSN.MSN_of_Interest__c;						
					}	
					if(msnStr==null)  msnStr = curMSN.MSN_of_Interest__c;
					else msnStr += ', ' + curMSN.MSN_of_Interest__c;					
					
				}
				if(msnStr!=null && msnStr.length()>254) msnStr = msnStr.left(251) + '...';
				curRec.Interested_MSN_List__c=msnStr;

				if(awardedMSNStr!=null && awardedMSNStr.length()>254) awardedMSNStr = awardedMSNStr.left(251) + '...';
				curRec.MSN_s_Deal_Proposal__c=awardedMSNStr;
												
			}
			
			LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled
			update listDealProp;
			LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger disabled
		}
	
	    Aircraft_In_Deal__c[] listACInD = [select Id, Marketing_Deal__c,Next_Operator__c from Aircraft_In_Deal__c where Marketing_Deal__c in :setDealIds  
	        and Id in :setAcIds];
	        
	    set<string> setAcInD = new set<string>();
	    String key;
	    list<Aircraft_In_Deal__c> listUpdateAircraftInDeal = new list<Aircraft_In_Deal__c>();
	
	    for(Aircraft_In_Deal__c curAiD: listACInD){
	    	key= ((string)(curAiD.Marketing_Deal__c)).left(15) + '-' + ((string)(curAiD.Id)).left(15);
	    	setAcInD.add(key);
	    	if(mapMSNofInt.containsKey(key)){
				if(trigger.IsDelete){
	    			curAiD.Next_Operator__c = null;
	    			listUpdateAircraftInDeal.add(curAiD);
				}
				else if(trigger.IsInsert){
						if(curAiD.Next_Operator__c == null){
							curAiD.Next_Operator__c = ((MSN_s_Of_Interest__c)trigger.newMap.get(mapMSNofInt.get(key))).Parent_Operator__c;
							listUpdateAircraftInDeal.add(curAiD);
						}
						else{
							((MSN_s_Of_Interest__c)trigger.newMap.get(mapMSNofInt.get(key))).Awarded__c.addError('An aircraft can be awarded to only one operator.');
						}								
					
				}else{
					// for update case
					if(((MSN_s_Of_Interest__c)trigger.newMap.get(mapMSNofInt.get(key))).Awarded__c){
						if(curAiD.Next_Operator__c == null){
							curAiD.Next_Operator__c = ((MSN_s_Of_Interest__c)trigger.newMap.get(mapMSNofInt.get(key))).Parent_Operator__c;
							listUpdateAircraftInDeal.add(curAiD);
						}
						else{
							((MSN_s_Of_Interest__c)trigger.newMap.get(mapMSNofInt.get(key))).Awarded__c.addError('An aircraft can be awarded to only one operator.');
						}						
					}
					else{
	    				curAiD.Next_Operator__c = null;
	    				listUpdateAircraftInDeal.add(curAiD);					
					}
				}
	    	}
	    } 
		
		// if this is a delete trigger , then return . rest of the code is for Insert and Update
		if(trigger.IsDelete) {
	//		if(!listUpdateAircraftInDeal.isEmpty()) update listUpdateAircraftInDeal; 
	
		    try{
				if(!listUpdateAircraftInDeal.isEmpty()) database.update(listUpdateAircraftInDeal, true);
		    }catch(exception e){
		        if(e.getMessage().contains('You cannot modify an aircraft in an inactive campaign.')){
		            string strMsg = 'This Aircraft cannot be modified as it is part of an inactive campaign.';
		            allRec[0].Aircraft__c.addError(strMsg, false);
		        }
		    }
			return;
		}
		
	    for(MSN_s_Of_Interest__c curMSNofInt: allRec){
	    	if(!setAcInD.contains(((string)(curMSNofInt.Deal_ID__c)).left(15) + '-' + ((string)(curMSNofInt.Aircraft__c)).left(15))){
	            curMSNofInt.addError('Selected aircraft is not part of this deal.');
	            return;
	        }
	    }
	
	    MSN_s_Of_Interest__c[] ExistingMSNs = [select Aircraft__c, Aircraft__r.Name, Deal_Proposal__c, Deal_Proposal__r.Name from MSN_s_Of_Interest__c where Deal_Proposal__c in :setDPIds];
	    
	    set<String> setMSNsOfInt = new set<String>(); 
	
	    for(MSN_s_Of_Interest__c curMSN:ExistingMSNs){
	        if(setMSNsOfInt.contains(curMSN.Deal_Proposal__c + '-' + curMSN.Aircraft__c )){
	            trigger.new[0].addError(curMSN.Aircraft__r.Name + ' is already added to this Marketing Prospect '+ curMSN.Deal_Proposal__r.Name +'.');
	            return;
	        }
	        system.debug('Adding '+ curMSN.Deal_Proposal__c + '-' + curMSN.Aircraft__c);
	        setMSNsOfInt.add(curMSN.Deal_Proposal__c + '-' + curMSN.Aircraft__c);
	    }
	    
	    //updating at last : if no error in any other validation
	//    if(!listUpdateAircraftInDeal.isEmpty()) update listUpdateAircraftInDeal;
	    
	    
	    try{
			if(!listUpdateAircraftInDeal.isEmpty()) database.update(listUpdateAircraftInDeal, true);
	    }catch(exception e){
	        if(e.getMessage().contains('You cannot modify an aircraft in an inactive campaign.')){
	            string strMsg = 'This Aircraft cannot be modified as it is part of an inactive campaign.';
	            allRec[0].Aircraft__c.addError(strMsg, false);
	        }
	    }
		
	}
	
    //before insert and before update
    //Initial Rent and Initial Term Defaulting
    	
	
	private void Defaulting(){
		system.debug('Defaulting values in Prospect Term page');
		list<MSN_s_Of_Interest__c> listProspectTerm=(list<MSN_s_Of_Interest__c>)trigger.new;
		system.debug('listProspectTerm ='+listProspectTerm);
		
		for(MSN_s_Of_Interest__c curRec: listProspectTerm){
				
				if(curRec.Rent_000__c==null){
					curRec.Rent_000__c=curRec.Initial_Rent__c;
				}
				
				if(curRec.Term_Mos__c==null){
					curRec.Term_Mos__c=curRec.Initial_Term__c;
				}
		}	
	}
	
	
/*	
	private void getCMLRFromWF()
	{
		
		MSN_s_Of_Interest__c[] newProspects = (list<MSN_s_Of_Interest__c>)trigger.new;

		set<string> setMSNs = new set<string>();
		for(MSN_s_Of_Interest__c curMSNofInt: newProspects)setMSNs.add(curMSNofInt.MSN_of_Interest__c);
		
		map<string, Global_Fleet__c> mapWordFleet = new map<string, Global_Fleet__c>();
		list<Global_Fleet__c> listWF = [select id, CurrentMarketLeaseRate_m__c, AircraftType__c, SerialNumber__c
			 from Global_Fleet__c where SerialNumber__c in :setMSNs];
		for(Global_Fleet__c curWF : listWF)mapWordFleet.put(curWF.AircraftType__c + '-' + curWF.SerialNumber__c, curWF);

		for(MSN_s_Of_Interest__c curMSNofInt: newProspects){
			if(trigger.isUpdate){
				curMSNofInt.Current_Market_Lease_Rate_M__c = null;
			}
			Global_Fleet__c WFMatch = mapWordFleet.get(curMSNofInt.Aircraft_Type__c + '-' + curMSNofInt.MSN_of_Interest__c);
			if(WFMatch != null)curMSNofInt.Current_Market_Lease_Rate_M__c = WFMatch.CurrentMarketLeaseRate_m__c;
		}		
	}	
	*/
	
	
	//after update
	//Code to change the value of Picklist (AIT Status) 
	//From 'Available' to 'Placed' under AIT Page.
	//if Transcation type is 'New Order' and Checkbox 'Awarded' is ticked in AIC (Proposed Term).
	
	public void changeAITStatus(){
		list<MSN_s_Of_Interest__c> listProposedTerm=(list<MSN_s_Of_Interest__c>)trigger.new;
		
		//set to hold the id of ait which need to be available
		set<id> setAITIdAvailable=new set<id>();
		//set to hold the id of ait which need to be placed
		set<id> setAITIdPlace=new set<id>();
		
		MSN_s_Of_Interest__c oldRec;
		for(MSN_s_Of_Interest__c curRec:listProposedTerm ){
			//for checking the old value with new value
			oldRec=(MSN_s_Of_Interest__c)trigger.oldmap.get(curRec.Id);
			if(curRec.Awarded__c!=oldRec.Awarded__c && curRec.Y_Hidden_AIT__c!=null && 'New Order (OEM)'.equals(curRec.Y_Hidden_Transaction_Type__c)){
				
				if(curRec.Awarded__c){
					setAITIdPlace.add(curRec.Y_Hidden_AIT__c);
				}else{
					setAITIdAvailable.add(curRec.Y_Hidden_AIT__c);
				}
				system.debug('added ait id in set');
			}
		}
		
		if(setAITIdPlace.size()>0 ||setAITIdAvailable.size()>0 ){
			list<Aircraft_In_Transaction__c> listAIT= [select id,name from Aircraft_In_Transaction__c 
													where id in:setAITIdPlace or id in:setAITIdAvailable ];
													
			for(Aircraft_In_Transaction__c curRec: listAIT){
				if(setAITIdPlace.contains(curRec.Id)){
					curRec.AIT_Status__c='Placed';
				}
				else if(setAITIdAvailable.contains(curRec.Id)){
					curRec.AIT_Status__c='Available';
				}
			
			}								
			
			LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled
			update listAIT;
			LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger enable
		}
	}	
	
	
}