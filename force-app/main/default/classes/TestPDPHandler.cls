@isTest
private class TestPDPHandler {
    
    @TestSetup static void setUp() {

        Test.startTest();
            Date dte = Date.newInstance(2018, 9, 21);
            Date oneMonthBefore = dte.addDays(-30);
            Date oneMonthLater = dte.addDays(30);
            List<Order__c> orders = TestLeaseworkUtil.createPurchaseAgreement(1, true, new Map<String, Object>{'Escalation__c' => 5, 'Order_Sign_Date__c' => dte });
            Order__c ord = orders[0];
            List<Structure__c> strucs = TestLeaseworkUtil.createStructures(1, ord.Id, true, new Map<String, Object>());
            Id structLineRTI = Schema.SObjectType.PDP_Structure_Line__c.getRecordTypeInfosByDeveloperName().get('Variable').getRecordTypeId();
            List<PDP_Structure_Line__c> line1 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 1, 
                'Payment__c' => 1, 'Month_Prior_Delivery__c' => 1});
            List<PDP_Structure_Line__c> line2 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 2, 
                'Payment__c' => 1, 'Month_Prior_Delivery__c' => 2});
            List<PDP_Structure_Line__c> line3 = TestLeaseworkUtil.createStructureLine(1,strucs[0].Id,true,new Map<String,Object>{'RecordTypeId' => structLineRTI,'PaymentNum__c'=> 3, 
                'Payment__c' => 1, 'Month_Prior_Delivery__c' => 3});

            List<Batch__c> batches = TestLeaseworkUtil.createBatch(1, strucs[0].Id, orders[0].Id, true, new Map<String, Object>());
            Id recordTypeId = Schema.SObjectType.Engine_Base_Price__c.getRecordTypeInfosByDeveloperName().get('CFM').getRecordTypeId();
            List<Engine_Base_Price__c> ebp = TestLeaseworkUtil.createEBP(1, ord.Id, true, new Map<String, Object>{'RecordTypeId' => recordTypeId, 'Aircraft_Type__c' => 'A300', 'Manufacturer__c' => 'CFM',
             'Engine_Type__c' => 'CFM56-5B4/P', 'Base_Reference_Net_Price__c' => 15, 'Price__c' => 10, 'Allowance__c' => 1});
            Id rti = Schema.SObjectType.Aircraft_Base_Price__c.getRecordTypeInfosByDeveloperName().get('Airbus').getRecordTypeId();
            List<Aircraft_Base_Price__c> abp = TestLeaseworkUtil.createABP(1, ord.Id, true, new Map<String, Object>{'RecordTypeId' => rti,'Aircraft_Type__c' => 'A300'});

            Assumed_Escalation__c ac = new Assumed_Escalation__c();
            ac.Name = 'Assumed Escalation';
            ac.Percentage__c = 5;
            insert ac;
            EngineManufacturersCap__c Emc = new EngineManufacturersCap__c();
            Emc.Name = 'CFM';
            Emc.AVAssumed__c = 100;
            Emc.Fleet__c = 'A319/A320';
            Emc.InitialCap__c = 3.50;
            Emc.ShareCap__c = 7.50;
            insert Emc;
            EscalationCS__c Ecs = new EscalationCS__c();
            Ecs.Name='Airbus';
            Ecs.Assumed_Escalation__c=4;
            Ecs.AV_Assumed__c=100;
            Ecs.Initial_CAP__c=2.85;
            Ecs.Share_CAP__c=100;
            insert Ecs;

            List<Cost_Revision__c> crf = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'Airbus',
                'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf;

            String itype = 'Airbus (Credit)';
            List<Cost_Revision__c> crf2 = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => itype,
                'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf2;

            List<Cost_Revision__c> crf3 = TestLeaseworkUtil.createCostRevision(1, true, new Map<String, Object>{'Base_Date__c' => dte, 'IndexType__c' => 'CFM',
                'Revision_Start_Date__c' => oneMonthBefore, 'Revision_End_Date__c' => oneMonthLater});
            update crf3;

        Test.stopTest();
        List<Pre_Delivery_Payment__c> pdps = TestLeaseworkUtil.createPDP(1, batches[0].Id, true, 
            new Map<String, Object>{'Aircraft_Base_Price__c' => abp[0].Id, 'Engine_Base_Price__c' => ebp[0].Id, 'Aircraft_Model__c' => 'A300',
            'Delivery_Date__c' => dte, 'Rank__c' => 'A', 'Engine_Manufacturer__c' => 'CFM', 'Engine_Type__c' => 'CFM56-5B4/P'});

    }

    @isTest static void setAirframeManufacturerTest() {

        PDPHandler handler = new PDPHandler();
        List<Pre_Delivery_Payment__c> pdps = [Select Aircraft_Model__c,Airframe_Manufacturer__c,Engine_Type__c,Engine_Base_Price__c,AircraftPriceIndex__c,
            Aircraft_Base_Price__c,Batch__c,Order_Sign_Date__c,Escalated_PPRF__c, Delivery_Date__c,Escalation_Engine__c, 
            AirframeCredit__c FROM Pre_Delivery_Payment__c];

        pdps[0].Aircraft_Model__c = 'A310';
        //update pdps; Commented by Neetu

        handler.loadMaps(pdps);
        handler.setAirframeManufacturer();

        String expectedvalue = 'Airbus';
        Pre_Delivery_Payment__c pdp = [ SELECT Id, Airframe_Manufacturer__c FROM Pre_Delivery_Payment__c LIMIT 1 ];
        System.assertEquals(pdp.Airframe_Manufacturer__c, expectedvalue);
    }

    @isTest static void calculateAirframeCapTest() {

        PDPHandler handler = new PDPHandler();
        EscalationCS__c escCS = EscalationCS__c.getValues('Airbus');
        escCs.Initial_CAP__c = 1;
        escCs.Share_CAP__c = 2;
        escCs.AV_Assumed__c = 6;
        update escCS;
        Assumed_Escalation__c asummedEsc = Assumed_Escalation__c.getValues('Assumed Escalation');
        asummedEsc.Percentage__c = 4;
        update asummedEsc;
        Decimal cap = handler.calculateAirframeCap(escCS,asummedEsc);
        Decimal expectedvalue = (escCs.Initial_CAP__c+(asummedEsc.Percentage__c-escCS.Share_CAP__c)/2)/100;
        System.assertEquals(cap,expectedvalue);
    }

    @isTest static void calculateAirframeCapTestEscalationGraterThanCS() {

        PDPHandler handler = new PDPHandler();
        EscalationCS__c escCS = EscalationCS__c.getValues('Airbus');
        escCs.Initial_CAP__c = 1;
        escCs.Share_CAP__c = 2;
        escCs.AV_Assumed__c = 3;
        update escCS;
        Assumed_Escalation__c asummedEsc = Assumed_Escalation__c.getValues('Assumed Escalation');
        asummedEsc.Percentage__c = 4;
        update asummedEsc;
        Decimal cap = handler.calculateAirframeCap(escCS,asummedEsc);
        Decimal expectedvalue =  (escCS.Initial_CAP__c + (escCS.AV_Assumed__c - escCS.Share_CAP__c) / 2 + (asummedEsc.Percentage__c-escCS.AV_Assumed__c))/100;
        System.assertEquals(cap,expectedvalue);
    }

    @isTest static void calculateAirframeCapTestCapGTEscalation() {

        PDPHandler handler = new PDPHandler();
        EscalationCS__c escCS = EscalationCS__c.getValues('Airbus');
        escCs.Initial_CAP__c = 1;
        escCs.Share_CAP__c = 6;
        update escCS;

        Assumed_Escalation__c asummedEsc = Assumed_Escalation__c.getValues('Assumed Escalation');
        asummedEsc.Percentage__c =4;
        update asummedEsc;

        Decimal cap = handler.calculateAirframeCap(escCS,asummedEsc);
        Decimal expectedvalue = escCs.Initial_CAP__c/100;
        System.assertEquals(cap,expectedvalue);
    }

    @isTest static void calculateEngineCapTestCapGTEscalation() {

        PDPHandler handler = new PDPHandler();
        EngineManufacturersCap__c  escCS = EngineManufacturersCap__c.getValues('CFM');
        escCs.InitialCap__c  = 1;
        escCs.ShareCap__c  = 2;
        escCs.AVAssumed__c = 3;
        update escCS;

        Assumed_Escalation__c asummedEsc = Assumed_Escalation__c.getValues('Assumed Escalation');
        asummedEsc.Percentage__c =4;
        update asummedEsc;
        Decimal cap = handler.calculateEngineCap(escCS,asummedEsc);
        Decimal expectedvalue = (escCS.InitialCap__c + (escCS.AVAssumed__c - escCS.ShareCap__c) / 2 + (asummedEsc.Percentage__c-escCS.AVAssumed__c))/100;
        System.assertEquals(cap,expectedvalue);
    }

    @isTest static void calculateEngineCapTestShareCapGTEscalation() {

        PDPHandler handler = new PDPHandler();
        EngineManufacturersCap__c  escCS = EngineManufacturersCap__c.getValues('CFM');
        escCs.InitialCap__c  = 1;
        escCs.ShareCap__c  = 6;
        escCs.AVAssumed__c = 3;
        update escCS;
        Assumed_Escalation__c asummedEsc = Assumed_Escalation__c.getValues('Assumed Escalation');
        asummedEsc.Percentage__c =4;
        update asummedEsc;
        Decimal cap = handler.calculateEngineCap(escCS,asummedEsc);
        Decimal expectedvalue = escCs.InitialCap__c/100;
        System.assertEquals(cap,expectedvalue);
    }

    @isTest static void calculateCapFactorTest() {
        
        PDPHandler handler = new PDPHandler();

        EscalationCS__c escCS = EscalationCS__c.getValues('Airbus');
        Assumed_Escalation__c asummedEsc = Assumed_Escalation__c.getValues('Assumed Escalation');
        

        Pre_Delivery_Payment__c pdp = [ SELECT Id, Delivery_Date__c, Order_Sign_Date__c FROM Pre_Delivery_Payment__c LIMIT 1 ];
        Decimal cap = handler.calculateAirframeCap(escCS, asummedEsc);
        Decimal capFactor = handler.calculateCapFactor(pdp, cap, pdp.Delivery_Date__c);
        System.assertEquals(capFactor, 1);
    }
    
    @isTest static void setAutomaticSchedulesTest() {

        Pre_Delivery_Payment__c pdp = [ SELECT Id, Delivery_Date__c, Order_Sign_Date__c, Engine_Type__c FROM Pre_Delivery_Payment__c LIMIT 1 ];
        List<Structure__c> strucs = [ SELECT Id FROM Structure__c ];
        pdp.Engine_Type__c = 'CFM56-5B3/3';
        Test.startTest();

            PDP_Payment_Schedule__c pdpSch = new PDP_Payment_Schedule__c();
            pdpSch.PDP__c = pdp.Id;
            pdpSch.Percentage_of_PDPRP__c = 10;
            pdpSch.Payment_Months_Before_Delivery__c = 2;
            pdpSch.Paid_Amount_M__c = 1;
            insert pdpSch;


            Id oldId = pdpSch.Id;
        Test.stopTest();

        update pdp;

        List<PDP_Payment_Schedule__c> pdpschds = [ SELECT Id FROM PDP_Payment_Schedule__c WHERE PDP__c =: pdp.Id ];
        System.assertEquals(pdpschds.size(), 4);
    }

    @isTest static void getScheduleLineNameTest() {

        Pre_Delivery_Payment__c pdp = [ SELECT Id, Delivery_Date__c FROM Pre_Delivery_Payment__c LIMIT 1 ];
        
        Id rec = Schema.SObjectType.PDP_Structure_Line__c.getRecordTypeInfosByDeveloperName().get('Fixed').getRecordTypeId();
        PDP_Structure_Line__c strctLine = new PDP_Structure_Line__c();
        strctLine.RecordTypeId = rec;
        strctLine.Month_Prior_Delivery__c = 1;
        strctLine.Fixed_Date__c = Date.newInstance(2018, 5, 1);

        String expectedvalue = 'May-18 Payment';
        PDPHandler handler = new PDPHandler();
        String lineName = handler.getScheduleLineName(strctLine, pdp);
        System.assertEquals(expectedvalue, lineName);
    }

    @isTest static void returnDateConvertedTest() {

        Pre_Delivery_Payment__c pdp = [ SELECT Id, Delivery_Date__c FROM Pre_Delivery_Payment__c LIMIT 1 ];
        
        PDP_Structure_Line__c strctLine = new PDP_Structure_Line__c();
        strctLine.Month_Prior_Delivery__c = 1;
        strctLine.Fixed_Date__c = Date.newInstance(2018, 5, 1);

        Date expectedvalue = Date.newInstance(2018, 8, 21);
        PDPHandler handler = new PDPHandler();
        Date actualResult = handler.returnDateConverted(strctLine, pdp);
        System.assertEquals(expectedvalue, actualResult);
    }
    
    @isTest static void PDPCodeCoverageTestcase() {
        try{
            //TestLeaseworkUtils.createBatch(1,ID structureId,ID PAId, Boolean doInsert,Map<String,Object> nameValueMap);
        }catch(Exception ex){
            
        }
        
    }
    @isTest 
    static void validateEngScenarioCase(){
      Date dte = Date.newInstance(2018, 9, 21);
      Date oneMonthBefore = dte.addDays(-30);
      Date oneMonthLater = dte.addDays(30);
      List<Order__c> orders = [select Id from Order__c];
      Order__c ord = orders[0];

      List<Batch__c> batches = [select Id from Batch__c where Purchase_Agreement__c =: ord.id];
      Id recordTypeIdPW = Schema.SObjectType.Engine_Base_Price__c.getRecordTypeInfosByDeveloperName().get('PW').getRecordTypeId();
      List<Engine_Base_Price__c> ebp2 = TestLeaseworkUtil.createEBP(1, ord.Id, true, new Map<String, Object>{'RecordTypeId' => recordTypeIdPW, 'Aircraft_Type__c' => 'A300', 'Manufacturer__c' => 'PW',
      'Engine_Type__c' => 'PW1127G', 'Base_Reference_Net_Price__c' => 150, 'Price__c' => 100, 'BP_F__c'=>10,'BRA_F__c'=>10,'Allowance__c' => 20});
      List<Engine_Base_Price__c> ebp = [select Id from Engine_Base_Price__c order by Manufacturer__c asc];
      Pre_Delivery_Payment__c fetchpdp= [select Id,Engine_Type_Eng_Sc__c,Engine_Mf_Eng_Sc__c from Pre_Delivery_Payment__c  ];
      fetchpdp.Engine_Mf_Eng_Sc__c='PW';
      fetchpdp.Engine_Type_Eng_Sc__c='PW1127G';
      update fetchpdp;
     
      List<Aircraft_Base_Price__c> abp  = [select Id from Aircraft_Base_Price__c];
      List<Pre_Delivery_Payment__c> pdps2 = TestLeaseworkUtil.createPDP(1, batches[0].Id, true, 
            new Map<String, Object>{'Aircraft_Base_Price__c' => abp[0].Id, 'Engine_Base_Price__c' => ebp[1].Id, 'Aircraft_Model__c' => 'A300',
            'Delivery_Date__c' => dte, 'Rank__c' => 'B', 'Engine_Manufacturer__c' => 'PW', 'Engine_Type__c' => 'PW1127G','Engine_Mf_Eng_Sc__c' => 'CFM','Engine_Type_Eng_Sc__c'=>'CFM56-5B4/P'});


       List<Pre_Delivery_Payment__c> pdpList = [SELECT Id, Delivery_Date__c,Engine_Net_Base_Price__c,Escalation_Engine__c,Total_BFE__c,Engine_Base_Price_Eng_Sc_DP__c,Engine_Escalation_Factor_DP__c,Discounted_Price_at_Delivery__c,Total_Financial_Price_At_Delivery__c, Order_Sign_Date__c, Engine_Type__c FROM Pre_Delivery_Payment__c  ];
       system.assertEquals(pdpList[0].Engine_Net_Base_Price__c, pdpList[1].Engine_Base_Price_Eng_Sc_DP__c);
       system.assertEquals(pdpList[1].Engine_Net_Base_Price__c, pdpList[0].Engine_Base_Price_Eng_Sc_DP__c);
    }

    @IsTest
    private static void validateEngScenarioNullCase(){
      Pre_Delivery_Payment__c fetchpdp= [select Id,Engine_Type_Eng_Sc__c,Engine_Mf_Eng_Sc__c from Pre_Delivery_Payment__c  ];
      fetchpdp.Engine_Mf_Eng_Sc__c=null;
      fetchpdp.Engine_Type_Eng_Sc__c=null;
      update fetchpdp;
      List<Pre_Delivery_Payment__c> pdpList = [SELECT Id, Delivery_Date__c,Discounted_Price_at_Delivery_Eng_Sc__c,Engine_Net_Base_Price__c,Escalation_Engine__c,Total_BFE__c,Engine_Base_Price_Eng_Sc_DP__c,Engine_Escalation_Factor_DP__c,Discounted_Price_at_Delivery__c,Total_Financial_Price_At_Delivery__c, Order_Sign_Date__c, Engine_Type__c FROM Pre_Delivery_Payment__c  ];
      system.assertEquals(pdpList[0].Engine_Base_Price_Eng_Sc_DP__c,null);
      system.assertEquals(pdpList[0].Discounted_Price_at_Delivery_Eng_Sc__c,null);
    }
     
    @IsTest
    private static void validateEngScACEngTypeNotExists(){
      Pre_Delivery_Payment__c fetchpdp= [select Id,Engine_Type_Eng_Sc__c,Engine_Mf_Eng_Sc__c from Pre_Delivery_Payment__c  ];
      fetchpdp.Engine_Mf_Eng_Sc__c='PW';
      fetchpdp.Engine_Type_Eng_Sc__c='PW1127G';
      update fetchpdp;
      List<Pre_Delivery_Payment__c> pdpList = [SELECT Id, Delivery_Date__c,Discounted_Price_at_Delivery_Eng_Sc__c,Engine_Net_Base_Price__c,Escalation_Engine__c,Total_BFE__c,Engine_Base_Price_Eng_Sc_DP__c,Engine_Escalation_Factor_DP__c,Discounted_Price_at_Delivery__c,Total_Financial_Price_At_Delivery__c, Order_Sign_Date__c, Engine_Type__c FROM Pre_Delivery_Payment__c  ];
      system.assertEquals(pdpList[0].Engine_Base_Price_Eng_Sc_DP__c,null);
      system.assertEquals(pdpList[0].Discounted_Price_at_Delivery_Eng_Sc__c,null);
    }
}