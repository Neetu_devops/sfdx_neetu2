@isTest
public class SelectAircraftcntrl_Test{
    @testSetup
    public static void testSetup(){
        Project__c project = new Project__c();
        project.Name = 'Test';
        project.Bid_field__c = 'Sale_Price__c';
        project.Economic_Closing_Date__c = System.today();
        project.Project_Deal_Type__c = 'Sale';
        insert project; 
        
        Aircraft__c aircraft = new Aircraft__c();
        aircraft.Name= '1123 Test';
        aircraft.MSN_Number__c = '1230';
        insert aircraft;
        
        Aircraft__c aircraft2 = new Aircraft__c();
        aircraft2.Name= '11232 Test';
        aircraft2.MSN_Number__c = '123012';
        insert aircraft2;
        
        Counterparty__c counterParty = new Counterparty__c();
        counterParty.Name = 'Leassor Test';
        insert counterParty;
        
        
        Marketing_Activity__c marketingActivity = new Marketing_Activity__c();
        marketingActivity.Name = 'Test MA';
        marketingActivity.Lessor__c = counterParty.Id;
        marketingActivity.Deal_Type__c = 'Sale';
        marketingActivity.Inactive_MA__c = False;
        marketingActivity.Deal_Status__c = 'LOI';
        marketingActivity.campaign__c = project.Id;
        insert marketingActivity;
        
        Aircraft_Proposal__c asset = new Aircraft_Proposal__c();
        asset.Marketing_Activity__c = marketingActivity.Id;
        asset.Aircraft__c = aircraft.Id;
        asset.Sale_Price__c = 500;
        insert asset;
        
        SelectAircraftcntrl.getData(project.Id);
        
        Operator__c operator = new Operator__c ();
        operator.Name = 'Airline Test';
        insert operator;
    }
   
    @isTest
    public static void selectAircraftcntrl_Test1(){
        Test.startTest();
        List<Aircraft__c> aircraftList = [Select Id From Aircraft__c];
        List<Project__c> project = [Select Id From Project__c];
        List<Counterparty__c> counterParty = [Select Id From Counterparty__c];
        List<Operator__c> operator = [Select Id From Operator__c];
        List<Marketing_Activity__c> marketingActivity = [Select Id From Marketing_Activity__c];
       
        SelectAircraftcntrl.saveProject(project[0].Id,new List<Id>{aircraftList[0].Id, aircraftList[1].Id}, new List<Id>{counterParty[0].Id, operator[0].Id});
        SelectAircraftcntrl.detachDeals(project[0].Id, new List<Id>{marketingActivity[0].Id}, 'Yes');
        Test.stopTest();
    }
        
    @isTest
    public static void selectAircraftcntrl_Test2(){
        Test.startTest();
        List<Project__c> project = [Select Id From Project__c];
       
        List<Marketing_Activity__c> deals = [select id from marketing_activity__c where campaign__c = :project[0].Id];
        SelectAircraftcntrl.detachDeals(project[0].Id, new List<Id>{deals[0].Id}, 'No');
        SelectAircraftcntrl.deleteProjectRecord(project[0].Id,'Yes');
        Test.stopTest();
    } 
	
	@isTest
    private static void RefreshCampaignSummaries_Test(){
        List<Project__c> project = [Select Id From Project__c];
        RefreshSummariesController.refreshSummaries(project[0].Id); 
    }

}