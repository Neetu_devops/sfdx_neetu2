/**
****************************************************************************************************************************************************************
* Class: ApplyCreditMemoController
* @author Created by Aman, Lease-Works, 
* @date 17/08/2020
* @version 1.0
* --------------------------------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic Apply a credit Memo to an Invoice both from Credit Memo and invoice Object record page.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*	1.0       A.K.G                 17.08.2020      Initial Data & Credit Memo Payment creation
****************************************************************************************************************************************************************
*/
public class ApplyCreditMemoController {
    public static String MR_RECORDTYPE_ID = Schema.SObjectType.Assembly_MR_Rate__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId();
    
    /**
* @description: Check the callng sObject via recordId and fetches all the required data to render on the UI.
*
* @param recordId - String, contains record Id of the Object loading the CMP.
*
* @return CreditMemoWrapper - Wrapper contianing required data for Invoice and Credit memo.
*/
    @AuraEnabled
    public static CreditMemoWrapper fetchInitData(String recordId) {
        try {
            CreditMemoWrapper cmWrap = new CreditMemoWrapper();
            if (recordId != null && recordId != '') {
                String objName = Id.valueOf(recordId).getSobjectType().getDescribe().getName();
                String nameSpacePrefix = LeaseWareUtils.getNamespacePrefix();
                
                if (objName == nameSpacePrefix+'Credit_Memo__c') {
                    Credit_Memo__c[] cmList = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c, Credit_Memo_Date__c, Credit_Memo_Reason__c, 
                                               Due_Date__c, Lease__c, Lessee__c, Memo__c, Status__c, Allocated_Amount__c, Y_Hidden_IsStandalone__c 
                                               FROM Credit_Memo__c 
                                               WHERE Id =: recordId];
                    
                    if (!cmList.isEmpty()) {
                        cmWrap.creditMemoRec = cmList[0];
                        if (cmList[0].Status__c != 'Approved') {
                            cmWrap.isNotValid = true;
                            return cmWrap;
                        }
                    }
                }
                else if (objName == nameSpacePrefix+'Invoice__c') {
                    cmWrap.loadedFromInvoice = true;
                    
                    Invoice__c[] invList = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.DeveloperName, 
                                            RecordTypeId, Invoice_Type__c, Accounting_Period__c, Accounting_Period__r.Name, Accounting_Period__r.Closed__c, 
                                            Y_hidden_AccEnabled__c, Status__c,payment_status__c,Rent__r.Rent_Type__c
                                            FROM Invoice__c 
                                            WHERE Id =: recordId];
                    
                    if (!invList.isEmpty()) {
                        cmWrap.invoiceRec = invList[0];
                        if (invList[0].Balance_Due__c <= 0 || !(invList[0].Status__c.equals('Approved') && (invList[0].payment_Status__c.equals('Partially Paid')||invList[0].payment_Status__c.equals('Open')))) {
                            cmWrap.isNotValid = true;
                            return cmWrap;
                        }
                        if (cmWrap.invoiceRec.RecordType.DeveloperName.contains('MR') || cmWrap.invoiceRec.Invoice_Type__c.contains('MR') || cmWrap.invoiceRec.Rent__r.Rent_Type__c == 'Power By The Hour')  {
                            cmWrap.isMRInvoice = true;
                        }
                        system.debug('cmWrap.isMRInvoice::'+cmWrap.isMRInvoice);
                        
                        cmWrap.mrBreakOut = new List<InvoiceBreakOutWrapper>();
                        for (Invoice_Line_Item__c item : [SELECT Id, Name, Assembly__c, Assembly__r.Name, Assembly_Utilization__c, Module__c, 
                                                          Assembly_MR_Info__c, total_line_amount__c, Balance_Due__c
                                                          FROM Invoice_Line_Item__c 
                                                          WHERE Invoice__c =: cmWrap.invoiceRec.Id]) {
                                                              cmWrap.mrBreakOut.add(new InvoiceBreakOutWrapper(item));
                                                          }
                        
                        
                        if (invList[0].Accounting_Period__c != null) {
                            cmWrap.accountingPeriodRec = invList[0].Accounting_Period__r;
                        }
                    }
                }
            }
            return cmWrap;
        }
        catch(Exception ex){
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    /**
* @description: fetch invoice lineitem record upon selection of Invoice when loaded from Credit Memo screen.
*
* @param creditMemoData - Wrapper, containing Invoice record Id to fetch the related Invoice Line Items.
*
* @return CreditMemoWrapper - Wrapper contianing required data for Invoice and Credit memo.
*/
    @AuraEnabled
    public static CreditMemoWrapper fetchInvoiceLineItemData(String creditMemoData) {
        try {
            CreditMemoWrapper cmWrap = (CreditMemoWrapper) JSON.deserialize(creditMemoData, CreditMemoWrapper.class);
            
            if (cmWrap.invoiceRec != null){
                cmWrap.isMRInvoice = false;
                if (cmWrap.invoiceRec.RecordType.DeveloperName.contains('MR') || cmWrap.invoiceRec.Invoice_Type__c.contains('MR') || cmWrap.invoiceRec.Rent__r.Rent_Type__c == 'Power By The Hour')  {
                    cmWrap.isMRInvoice = true;
                }
                system.debug('fetchInvoiceLineItemData - cmWrap.isMRInvoice::'+cmWrap.isMRInvoice);
                
                cmWrap.mrBreakOut = new List<InvoiceBreakOutWrapper>();
                
                for(Invoice_Line_Item__c item : [SELECT Id, Name, Assembly__c, Assembly__r.Name, Assembly_Utilization__c, Module__c, 
                                                 Assembly_MR_Info__c, total_line_amount__c, Balance_Due__c
                                                 FROM Invoice_Line_Item__c 
                                                 WHERE Invoice__c =: cmWrap.invoiceRec.Id]) {
                                                     cmWrap.mrBreakOut.add(new InvoiceBreakOutWrapper(item));
                                                 }
            }
            return cmWrap;
        }
        catch(Exception ex){
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    /**
* @description: Creates the credit and credit line items
*
* @param creditMemoData - String, Wrapper object containing all the information entered by the user.
*
* @return null
*/
    @AuraEnabled
    public static void applyCreditMemo(String creditMemoData){
        try {
            CreditMemoWrapper cmWrap = (CreditMemoWrapper) JSON.deserialize(creditMemoData, CreditMemoWrapper.class);
            System.debug('cmWrap: ' + cmWrap);
            
            if (cmWrap != null && cmWrap.creditMemoRec != null && cmWrap.invoiceRec != null) {
                system.debug('cmWrap.actualAllocatedAmount::'+cmWrap.actualAllocatedAmount);
                system.debug('cmWrap.amountToCredit::'+cmWrap.amountToCredit);
                //Decimal amount = (cmwrap.mrBreakOut != null && cmWrap.mrBreakOut.size() > 0) ? cmWrap.actualAllocatedAmount : cmWrap.amountToCredit;
                Decimal amount = leaseWareUtils.zeroIfNull(cmWrap.amountToCredit).setscale(2,System.RoundingMode.HALF_UP);
                system.debug('Amount::'+amount);
                Payment__c paymentRec = new Payment__c(Credit_Memo__c = cmWrap.creditMemoRec.id, Amount__c = amount, Y_hidden_Auto_Create_PLI__c = false, 
                                                       invoice__c = cmWrap.invoiceRec.id, payment_status__c = 'Approved', payment_type__c ='CN_Payment',
                                                       Credit_Memo_Reason__c = cmWrap.creditMemoRec.Credit_Memo_Reason__c);
                paymentRec.accounting_period__c = cmWrap.accountingPeriodRec==null?null: cmWrap.accountingPeriodRec.id;
                paymentRec.Payment_Date__c = cmWrap.DateOfCredit==null?System.today():cmWrap.DateOfCredit;
                try{
                    insert paymentRec;
                }
                catch(DmlException ex){
                    System.debug(ex);
                    LeaseWareUtils.createExceptionLog(ex,'Error inseting Credit'+paymentRec,null, null,'Credit Memo', false, '');
                    throw new AuraHandledException(ex.getMessage());
                }catch(Exception ex){
                    System.debug(ex);
                    LeaseWareUtils.createExceptionLog(ex,'Error inseting Credit'+paymentRec,null, null,'Credit Memo', false, '');
                    throw new AuraHandledException(ex.getMessage());
                }
                
                if (cmwrap.mrBreakOut != null && cmWrap.mrBreakOut.size() > 0) {
                    //inserting payment line items.
                    List<Payment_Line_Item__c> listPmtLIs = new List<Payment_Line_Item__c>();
                    String payLIMRRecTypeId = Schema.SObjectType.Payment_Line_Item__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId();
                    String payLINonMRRecTypeId = Schema.SObjectType.Payment_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Non_MR').getRecordTypeId();
                    boolean isOverride = false;
                    for (InvoiceBreakOutWrapper brw : cmWrap.mrBreakOut) {
                        Invoice_line_item__c curInvLI = brw.lineItemRec;
                        System.debug('Inv LI--' + curInvLI.id);
                        if (curInvLI != null) {
                            isOverride =  false;
                            if(cmWrap.isMRInvoice)
                            {
                                if(null == brw.amountAllocated||brw.amountAllocated == 0.0){
                                    isOverride = true;// only required for MR Invoice, all others will have single line items.
                                }
                                listPmtLIs.add(new Payment_Line_Item__c(Name = curInvLI.Name, Payment__c = paymentRec.Id, Module__c=curInvLI.Module__c, 
                                                                        Assembly_Utilization__c = curInvLI.Assembly_Utilization__c, Assembly__c = curInvLI.Assembly__c, 
                                                                        Amount_paid__c = brw.amountAllocated, Invoice_Line_Item__c = curInvLI.Id, Override__c = isOverride,
                                                                        Assembly_MR_Info__c = curInvLI.Assembly_MR_Info__c, RecordTypeId = payLIMRRecTypeId));
                            }
                            else 
                            {
                                
                                listPmtLIs.add(new Payment_Line_Item__c(Name = curInvLI.Name, Payment__c = paymentRec.Id, Module__c=curInvLI.Module__c,                                                                     
                                                                        Amount_paid__c = amount, Invoice_Line_Item__c = curInvLI.Id, 
                                                                        RecordTypeId = payLINonMRRecTypeId));
                            }
                        }
                    }
                    if (!listPmtLIs.isempty()) { 
                        try{
                            LeaseWareUtils.setFromTrigger('CREATE_PY_LINE_ITEM_CREDIT_MEMO');
                            insert listPmtLIs; 
                            LeaseWareUtils.unsetTriggers('CREATE_PY_LINE_ITEM_CREDIT_MEMO');
                        }
                        catch(DmlException ex){
                            System.debug(ex);
                            LeaseWareUtils.createExceptionLog(ex,'Error inserting credit line items'+listPmtLIs[0],null, null,'Credit Memo', false, '');
                            throw new AuraHandledException(ex.getMessage());
                        }catch(Exception ex){
                            System.debug(ex);
                            LeaseWareUtils.createExceptionLog(ex,'Error inserting credit line items'+listPmtLIs[0],null, null,'Credit Memo', false, '');
                            throw new AuraHandledException(ex.getMessage());
                        }
                    }
                }
                
                Credit_Memo__c creditMemo = [SELECT Id, Allocated_Amount__c FROM Credit_Memo__c WHERE Id =: cmWrap.creditMemoRec.Id];
                creditMemo.Allocated_Amount__c = (creditMemo.Allocated_Amount__c == null ? 0.0 : creditMemo.Allocated_Amount__c) + amount;
                //cmWrap.creditMemoRec.Allocated_Amount__c = cmWrap.creditMemoRec.Allocated_Amount__c+cmWrap.amountToCredit;
                try{
                    update creditMemo;
                }
                catch(DmlException ex){
                    System.debug(ex);
                    LeaseWareUtils.createExceptionLog(ex,'Error inserting credit line items',null, null,'Credit Memo', false, '');
                    throw new AuraHandledException(ex.getMessage());
                }catch(Exception ex){
                    System.debug(ex);
                    LeaseWareUtils.createExceptionLog(ex,'Error inserting credit line items',null, null,'Credit Memo', false, '');
                    throw new AuraHandledException(ex.getMessage());
                }
                
            }
        }catch(Exception ex){
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
    
    /**
* @description: CreditMemoWrapper to hold data related to Credit Memo and Invoice along with Credit Breakdown
*/
    public class CreditMemoWrapper{
        @AuraEnabled public Credit_Memo__c creditMemoRec { get; set; }
        @AuraEnabled public Invoice__c invoiceRec { get; set; }
        @AuraEnabled public Boolean loadedFromInvoice { get; set; }
        @AuraEnabled public Boolean isNotValid { get; set; }
        @AuraEnabled public Decimal amountToCredit { get; set; }
        @AuraEnabled public Decimal actualAllocatedAmount { get; set; }
        @AuraEnabled public List<InvoiceBreakOutWrapper> mrBreakOut { get; set; }
        @AuraEnabled public Date dateOfCredit { get; set; }
        @AuraEnabled public Calendar_Period__c accountingPeriodRec {get;set;}
        @AuraEnabled public Boolean isMRInvoice {get;set;}
        
        public CreditMemoWrapper(){
            this.creditMemoRec = new Credit_Memo__c();
            this.invoiceRec = new Invoice__c();
            this.loadedFromInvoice = false;
            this.isNotValid = false;
            this.amountToCredit = 0.0;
            this.actualAllocatedAmount = 0.0;
            this.mrBreakOut = new List<InvoiceBreakOutWrapper>();
            this.dateOfCredit = System.today();
            this.accountingPeriodRec = new Calendar_Period__c();
            this.isMRInvoice = false;
        }
    }
    
    /**
* @description: InvoiceBreakOutWrapper to hold data related to amount allocated to Invoice Line Items
*/
    public class InvoiceBreakOutWrapper{
        @AuraEnabled public Invoice_Line_Item__c lineItemRec { get; set; }
        @AuraEnabled public Decimal amountAllocated { get; set; }
        @AuraEnabled public Boolean overPayed { get; set; }
        
        public InvoiceBreakOutWrapper(){
            this.lineItemRec = new Invoice_Line_Item__c();
            this.amountAllocated = 0.0;
            this.overPayed = false;
        }
        
        public InvoiceBreakOutWrapper(Invoice_Line_Item__c item){
            this.lineItemRec = item;
            this.amountAllocated = 0.0;
            this.overPayed = false;
        }
    }
}