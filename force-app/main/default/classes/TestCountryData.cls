@isTest
public class TestCountryData {
    
	@testSetup static void TestSetupMethod() {
        List<Country__c> countryList = new List<Country__c>();
        Country__c country1 = new Country__c(name='India');
        Country__c country2 = new Country__c(name='United States'); 
        Country__c country3 = new Country__c(name='Germany'); 
          Country__c country4 = new Country__c(name='New Zealand'); 
        countryList.add(country1);
        countryList.add(country2);
        countryList.add(country3);
         countryList.add(country4);
        insert countryList;
        
            
    }
    
    
 @isTest
    private static void testSearchableFieldCountryOnOperator(){
        List<Country__c> countryList = [select Id,Name from Country__c];
        map<string,Id> mapCountry = new map<string,Id>();
        for(Country__c cou : countryList){
            mapCountry.put(cou.Name,cou.Id);
        }
        Operator__c opr = new Operator__c(Name ='Test Opr', Country_Lookup__c=mapCountry.get('India'),Billing_COuntry_Lookup__c=mapCountry.get('Germany'),
                                         Country_Of_Registration_Lookup__c=mapCountry.get('United States'));
        insert opr;
        Leasewareutils.clearFromTrigger();
        Operator__C opr1 = [select Id,Name,Country_Lookup__c,Y_Hidden_Country__c from Operator__c];
        system.assert(opr1.Y_Hidden_Country__c.contains('India,Germany,United States'),'Country Look up Field Name Set on Hidden Field');
        opr1.Country_Lookup__c = mapCountry.get('New Zealand');
        update opr1;
        Operator__C opr2 = [select Id,Name,Country_Lookup__c,Y_Hidden_Country__c from Operator__c];
        system.assert(opr2.Y_Hidden_Country__c.contains('New Zealand,Germany,United States'),'Country Look up Field Name Set on Hidden Field');
    }
    
    @isTest
    private static void testSearchableFieldCountryOnLessor(){
        List<Country__c> countryList = [select Id,Name from Country__c];
        map<string,Id> mapCountry = new map<string,Id>();
        for(Country__c cou : countryList){
            mapCountry.put(cou.Name,cou.Id);
        }
        Counterparty__c lessor = new Counterparty__c(Name ='Test Lessor', Country_Lookup__c=mapCountry.get('India'),
                                         Country_Of_Registration_Lookup__c=mapCountry.get('United States'));
        insert lessor;
        Leasewareutils.clearFromTrigger();
        Counterparty__c lessor1 = [select Id,Name,Country_Lookup__c,Y_Hidden_Country__c from Counterparty__c];
        system.assert(lessor1.Y_Hidden_Country__c.contains('India,United States'),'Country Look up Field Name Set on Hidden Field');
        lessor1.Country_Lookup__c = mapCountry.get('New Zealand');
        lessor1.Country_Of_Registration_Lookup__c = mapCountry.get('India');
        update lessor1;
        Counterparty__c lessor2 = [select Id,Name,Country_Lookup__c,Y_Hidden_Country__c from Counterparty__c];
        system.assert(lessor2.Y_Hidden_Country__c.contains('New Zealand,India'),'Country Look up Field Name Set on Hidden Field');
    }
    
    
     @isTest
    private static void testSearchableFieldCountryOnCounterparty(){
        List<Country__c> countryList = [select Id,Name from Country__c];
        map<string,Id> mapCountry = new map<string,Id>();
        for(Country__c cou : countryList){
            mapCountry.put(cou.Name,cou.Id);
        }
        Bank__c cp = new Bank__c(Name ='Test CP', Country_Lookup__c=mapCountry.get('India')
                                         );
        insert cp;
        Leasewareutils.clearFromTrigger();
        Bank__c cp1 = [select Id,Name,Country_Lookup__c,Y_Hidden_Country__c from Bank__c];
        system.assert(cp1.Y_Hidden_Country__c.contains('India'),'Country Look up Field Name Set on Hidden Field');
        cp1.Country_Lookup__c = mapCountry.get('New Zealand');
       
        update cp1;
        Bank__c cp2 = [select Id,Name,Country_Lookup__c,Y_Hidden_Country__c from Bank__c];
        system.assert(cp2.Y_Hidden_Country__c.contains('New Zealand'),'Country Look up Field Name Set on Hidden Field');
    }
}