@isTest
public class TestUtilityWithSharingController {
    @isTest
    public static void testGetOperators(){
        
        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        Test.startTest();
        UtilityWithSharingController.getOperators();
        Test.stopTest();
    }
}