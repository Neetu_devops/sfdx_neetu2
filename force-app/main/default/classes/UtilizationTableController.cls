public without sharing class UtilizationTableController {
	
    @AuraEnabled
    public static ContainerWrapper loadData(String assemblyId){
        
        ContainerWrapper container = new ContainerWrapper();
        System.debug('Assembly Id: '+assemblyId);
        Constituent_Assembly__c asm = [Select Id, Lease_Start_Date__c, Lease_End_Date__c from Constituent_Assembly__c where Id=:assemblyId];
        AggregateResult[] groupedResults = [select max(Period_End_Date__c) latestYear from Assembly_Utilization_External__c  
                                            where Assembly_Lkp__c =: assemblyId group by Assembly_Lkp__c];
        System.debug(groupedResults);
		
        if(groupedResults.size()>0){
        	container.latestYear = ((Date)groupedResults[0].get('latestYear')).year();    
        }
		
        groupedResults = [select min(Period_End_Date__c) leastYear from Assembly_Utilization_External__c  
                                            where Assembly_Lkp__c =: assemblyId group by Assembly_Lkp__c];
        
        if(groupedResults.size()>0){
        	container.leastYear = ((Date)groupedResults[0].get('leastYear')).year();    
        }
        
        if(asm.Lease_End_Date__c != null && asm.Lease_Start_Date__c != null){
            container.leastYear = asm.Lease_Start_Date__c.year();
            container.startDate = asm.Lease_Start_Date__c;
            container.endDate = asm.Lease_End_Date__c;
        }
        else{
            if(container.latestYear != null){
                container.startDate = Date.newInstance(container.latestYear, 1, 1);
                container.endDate = Date.newInstance(container.latestYear, 12 , 31);
            }
        }
        if(container.latestYear != null)
            container.utilizationList = getUtilizations(assemblyId, container.startDate, container.endDate,container.latestYear);
        return container;
    }
    
    @AuraEnabled
    public static List<UtilizationWrapper> getUtilizations(String assemblyId, Date startDate, Date endDate, Integer year){
        
        List<UtilizationWrapper> utilizationList = new List<UtilizationWrapper>();
        
        for(Assembly_Utilization_External__c assemblyUR : [select id, Period_End_Date__c, Hours__c, Cycles__c
                                                           from Assembly_Utilization_External__c
                                                          where Assembly_Lkp__c =: assemblyId
                                                          and Period_End_Date__c >=: startDate
                                                          and Period_End_Date__c <=: endDate
                                                          and CALENDAR_YEAR(Period_End_Date__c) =:year 
                                                          order by Period_End_Date__c desc]){
        
        	utilizationList.add(new UtilizationWrapper(assemblyUR.Period_End_Date__c, assemblyUR.Hours__c, assemblyUR.Cycles__c));                                                      
        }
        
        return utilizationList;
    }
    
    public class ContainerWrapper{
        @AuraEnabled public Integer latestYear {get;set;}
        @AuraEnabled public Integer leastYear {get;set;}
        @AuraEnabled public Date startDate {get;set;}
        @AuraEnabled public Date endDate {get;set;}
        @AuraEnabled public List<UtilizationWrapper> utilizationList {get;set;}
        
        public ContainerWrapper(){
			utilizationList = new List<UtilizationWrapper>();            
        }
    }
    
    public class UtilizationWrapper{
        @AuraEnabled public Date urDate {get;set;}
        @AuraEnabled public Decimal hours {get;set;}
		@AuraEnabled public Decimal cycles {get;set;}
	
        public UtilizationWrapper(Date urDate, Decimal hours, Decimal cycles){
            this.urDate = urDate;
            this.hours = hours;
            this.cycles = cycles;
        }        
    }

}