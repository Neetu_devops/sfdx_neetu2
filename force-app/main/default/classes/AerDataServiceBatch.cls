// @Lastmodified : 2016.12.08 
//This batch class used to schedule AerDataService
/*---------Developer Console Script----Run batch instantly---------
    ID batchprocessid = Database.executeBatch(new AerDataServiceBatch(),200);    // running a batch with batch size 1
    System.schedule('AerDataServiceBatchJob', '0 15 * * * ?', new AerDataServiceBatch());
----------------*/
global class AerDataServiceBatch implements Database.Batchable<sObject> , Database.AllowsCallouts, 
                                            Schedulable , Database.Stateful
{ 
	
	private Process_Request_Log__c ReqLog;
	integer iCount = 0;
    private static integer batchSize = 200;

    @testVisible List<String> ListOfErrorMessages;
   //Constructor
   global AerDataServiceBatch()
   { 
		this.ListOfErrorMessages  = new List<String>();
		ServiceSetting__c lwServiceSettings = ServiceSetting__c.getInstance('Aerdata');
		if(lwServiceSettings != null && lwServiceSettings.Batch_Size__c != null)batchSize = integer.valueOf(lwServiceSettings.Batch_Size__c);
   }
    
   //START METHOD    
   global Database.QueryLocator start(Database.BatchableContext BC)                 // Start Method
   {        
   	
		boolean errorFlg=false;
        string strSessionId = string.valueOf(datetime.now());
		ReqLog = new Process_Request_Log__c(Status__c ='Success',Type__c = 'Aerdata Service', 
                                            Comments__c=strSessionId, Exception_Message__c='');
       	        
        //Calling web-service and Inserting All IncomingData records
        string statusMessage = AerDataService.refreshData(ReqLog);
//        string statusMessage = AerDataService.refreshDataUsingAssetsContracts(ReqLog);
        System.Debug('message=='+statusMessage);        
        insert ReqLog; // insert record to get Id
       
        if(statusMessage!=null) {
        	integer Filecount=1;
        	for(string str:AerDataService.AllJSON){ 
        		setProcessRequestLog(str,ReqLog.Id,'AerData'+Filecount +'.json','AerData'+Filecount +'.json');
        		Filecount++;
        	}
        	
        }
        
        LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
        string pkgPrefix = '';
        if( lwSettings.Package_Prefix__c!=null )
            pkgPrefix = lwSettings.Package_Prefix__c;
        	system.debug('pkgPrefix = '+pkgPrefix);
        String strQuery = 'SELECT Id FROM '+pkgPrefix+'Incoming_Data__c where Session_Id__c = \'' + strSessionId + '\'';
                    
        System.Debug('StrQuery=='+StrQuery);        
        return Database.getQueryLocator(StrQuery);
   }   
   
   //EXECUTE METHOD 
   global void execute(Database.BatchableContext BC, List<sObject> scope)           // Execute Logic    
   {
       string errormsg;

       iCount+=scope.size();
       database.SaveResult[] res = database.update((list<Incoming_Data__c>)scope, false);
       for(database.SaveResult sr : res){
           if (!sr.isSuccess()){
               errormsg = 'ERROR: Incoming Data Update failed for ' + sr.getId() +' .\n';
               for (Database.Error err:sr.getErrors()) {
                   errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                   errormsg += ('Fields that affected this error: ' + err.getFields() + '\n');
               }
               ReqLog.Exception_Message__c += ('\n' +errormsg);
           }
       }
   }   
   
   // Logic to be Executed at finish
   global void finish(Database.BatchableContext BC)
   {   
      LeaseWareUtils.insertExceptionLogs();
      ReqLog.Exception_Message__c = ReqLog.Exception_Message__c.left(2000);
      update ReqLog;
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                         TotalJobItems, CreatedBy.Email
                         FROM AsyncApexJob WHERE Id =:BC.getJobId()];
       
       string strMsg = 'AerData Service Job with ID ' + BC.getJobId() + ' completed with status ' + a.Status 
           + '.\n' + a.TotalJobItems + ' batches with '+ a.JobItemsProcessed + ' processed and ' + a.NumberOfErrors + ' failures.';
       strMsg+=('\nTotal Number of records processed : ' + iCount);
      
      LeaseWareUtils.sendEmailToLWAdmins( 'AerDataService Batch finished ('+ Date.today().format() +')', strMsg);
   }
   
   global void execute(SchedulableContext sc)
   {
      executeAerDataBatch();
   }
   
    public static void executeAerDataBatch()    
    {
    	system.debug('executeAerDataBatch ++');
        ID batchprocessid = Database.executeBatch( new AerDataServiceBatch(), batchSize);
        system.debug('batchprocessid = ' + batchprocessid);
        system.debug('executeAerDataBatch --');       
    } 
    
    public static void setProcessRequestLog(string resBody,ID ReqLogId,String Title,String Filename){
        LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
	    ContentVersion conVer = new ContentVersion();
	    conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
	    conVer.PathOnClient = Filename; // The files name, extension is very important here which will help the file in preview.
	    conVer.Title = Title; // Display name of the files
	    //conver.FileType = 'PDF';
	    conVer.VersionData = blob.valueof(resBody); //blob.toPDF(resBody) ; Blob.valueof(resBody); // converting your binary string to Blog
		conVer.Description = 'Leaseworks Internal';
		
	    insert conVer;
	    
	    ContentVersion cover = [select ContentDocumentId from ContentVersion where id = :conVer.Id limit 1];
	    ContentDocumentLink cDe = new ContentDocumentLink();
	    cDe.ContentDocumentId = cover.ContentDocumentId;
	    cDe.LinkedEntityId = ReqLogId; // you can use objectId,GroupId etc
	    cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
	    cDe.Visibility = 'AllUsers';
	    insert cDe;
        LeaseWareUtils.TriggerDisabledFlag = false;
  }  
   
}