/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMRRateTriggerHandler {

	@isTest(seeAllData=true)
    static  void MRRateTriggerHandler_CRUD() {
        // TO DO: implement unit test
        string ACName = 'TestSeededMSN3';
        Aircraft__c AC1 = [select id,lease__c,Lease__r.Lease_start_Date_new__c,Lease__r.Lease_end_Date_new__c from Aircraft__c where MSN_Number__c = :ACName limit 1]; 
        
        Assembly_MR_Rate__c[] AssemblyMRRate = [select id,Name ,Rate_Basis__c,Engine_Template__c,Lease__c,Esc_Periodicity_Mos__c,First_Escalation_Date__c
                                                ,Ratio_Table__c,Rounding_Method_On_Esc_MR_Rate__c,Rounding_Method_on_Interpolated_MR_Rate__c,
                                                Rounding_Decimals_Escalated_MR_Rate__c, Rounding_Decimals_Interpolated_MR_Rate__c from Assembly_MR_Rate__c 
        	                                    where lease__c = :AC1.lease__c]  ;    
        AssemblyMRRate[0].MR_Rate_Start_Date__c = 	AC1.Lease__r.Lease_start_Date_new__c;
        AssemblyMRRate[0].MR_Rate_End_Date__c = 	AC1.Lease__r.Lease_ENd_Date_new__c;
        AssemblyMRRate[0].Base_MRR__c = 100;
        AssemblyMRRate[0].MR_Escalation_Month__c = 'March';
        AssemblyMRRate[0].Rate_Basis__c = 'Monthly';
        AssemblyMRRate[0].Annual_Escalation__c = 10;
        AssemblyMRRate[0].Escalation_Type__c ='Advanced';
        LeaseWareUtils.clearFromTrigger();update AssemblyMRRate[0];
          
        LeaseWareUtils.clearFromTrigger();        
        AssemblyMRRateTriggerHandler tr = new AssemblyMRRateTriggerHandler();

        LeaseWareUtils.clearFromTrigger(); 
        tr.Create_MRRateTable(AssemblyMRRate[0]);
        
        MR_Rate__c[] mrRateList = [select id from MR_Rate__c where Assembly_MR_Info__r.Lease__c =:AC1.lease__c]	;
        mrRateList[0].mr_rate__c = 230;
        LeaseWareUtils.clearFromTrigger(); update mrRateList[0];
        
        mrRateList[1].mr_rate__c = 235;
        LeaseWareUtils.clearFromTrigger(); update mrRateList[1];

        mrRateList[1].Esc_Per__c = 4;
        LeaseWareUtils.clearFromTrigger(); update mrRateList[1];
        
        LeaseWareUtils.clearFromTrigger(); delete mrRateList[0];
    }

    @isTest(seeAllData=true)
    static  void UpdEngTemplate() {
        // TO DO: implement unit test
        string ACName = 'TestSeededMSN3';
        Aircraft__c AC1 = [select id,lease__c,Lease__r.Lease_start_Date_new__c,Lease__r.Lease_end_Date_new__c from Aircraft__c where MSN_Number__c = :ACName limit 1]; 
        //Engine_Template__c[] EngTemList =  [select id from Engine_Template__c ];
        Custom_Lookup__c[] listThrust = [select id from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true and name ='Default'];
        Engine_Template__c et1 = new Engine_Template__c(name='CFM56',Manufacturer__c ='CFMI'
                            ,Model__c='CFM56',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2015');
        
        LeaseWareUtils.clearFromTrigger();insert et1;

        Assembly_MR_Rate__c[] AssemblyMRRate = [select id,Rate_Basis__c,Lease__c from Assembly_MR_Rate__c 
        	where lease__c = :AC1.lease__c]  ;    
        AssemblyMRRate[0].MR_Rate_Start_Date__c = 	AC1.Lease__r.Lease_start_Date_new__c;
        AssemblyMRRate[0].MR_Rate_End_Date__c = 	AC1.Lease__r.Lease_ENd_Date_new__c;
        AssemblyMRRate[0].Base_MRR__c = 100;
        AssemblyMRRate[0].MR_Escalation_Month__c = 'March';
        AssemblyMRRate[0].Rate_Basis__c = 'Monthly';
        AssemblyMRRate[0].Annual_Escalation__c = 10;
        AssemblyMRRate[0].Escalation_Type__c ='LLP Catalog';
        AssemblyMRRate[0].Engine_Template__c=et1.id;
        LeaseWareUtils.clearFromTrigger();update AssemblyMRRate[0];
     
    }
}