@isTest
public class TestLLPCatalogTriggerHandler {
	
    Static String deletedYear = '2018';
    
    @testSetup
    static void testData(){
        
		Custom_Lookup__c custom1 = new Custom_Lookup__c();
        custom1.Name='Default';
        custom1.Lookup_Type__c = 'Engine';
        custom1.Active__c = true;
        insert custom1;
        
        Engine_Template__c engineTemplate = new Engine_Template__c();
        engineTemplate.Name = 'TestEngineTemplate';
        engineTemplate.Model__c='PW150A';
        engineTemplate.Thrust_LP__c = custom1.Id;
        engineTemplate.Current_Catalog_Year__c='2018';
        insert engineTemplate;
        
        LLP_Template__c LLPTemplate = new LLP_Template__c();
        LLPTemplate.Engine_Template__c = engineTemplate.Id;
        LLPTemplate.Name='TestLLPTemplate';
        LLPTemplate.Part_Name__c='testpartname';
        LLPTemplate.Part_Number__c = '3344';
        LLPTemplate.Life_Limit_Cycles__c = 10;
        insert LLPTemplate;
        
        LLP_Catalog__C LLPCatalog = new LLP_Catalog__C();
        LLPCatalog.LLP_Template__c = LLPTemplate.Id;
        LLPCatalog.Name='2018';
        LLPCatalog.Catalog_Price__c=1000;
        LLPCatalog.Part_Number_New__c='123456';
        insert LLPCatalog;
        
        LLP_Catalog__C LLPCatalog1 = new LLP_Catalog__C();
        LLPCatalog1.LLP_Template__c = LLPTemplate.Id;
        LLPCatalog1.Name='2019';
        LLPCatalog1.Catalog_Price__c=10000;
        LLPCatalog1.Part_Number_New__c='123456';
        insert LLPCatalog1;
    }
    
    
    @isTest
    public static void testLLPCatalogPriceInsertionValidation() {
        try {
            LLP_Catalog__C LLPCatalog = new LLP_Catalog__C();
            LLP_Template__c LLPTemplate = [select id from LLP_Template__c];
            LLPCatalog.LLP_Template__c = LLPTemplate.Id;
            LLPCatalog.Name='2021';
            LLPCatalog.Catalog_Price__c=1000;
            LLPCatalog.Part_Number_New__c='123456';
            insert LLPCatalog;
        }
        catch(Exception e){
           System.debug('e.getMessage() ' + e.getMessage());
           String expectedError = 'Latest catalog year is 2019 and the oldest catalog year is 2018.Please add catalog record for 2020 or 2017.';
            if(e.getMessage().contains(expectedError)) {
             	system.assert(true);
            } else {
                system.assert(false);
            }
        }
    }
    
    @isTest
    public static void testLLPCatalogPriceUpdation() {
        try {
            LLP_Catalog__C LLPCatalog = [select id,Name,Catalog_Price__c,Part_Number_New__c 
                                         from LLP_Catalog__C where Name =: '2018'];
            LLPCatalog.Name = '2017';
            update LLPCatalog;
        }
        catch(Exception e){
           System.debug('e.getMessage() ' + e.getMessage());
           String expectedError = 'Cannot update catalog record to 2017.Catalog years are not in sequence.';
            if(e.getMessage().contains(expectedError)){
           		System.assert(true);
            }else {
                System.assert(false);
            }
        
        }
    }
    
    @isTest
    public static void testLLPCatalogPriceDeletionValidation() {
        try {
            
            LLP_Catalog__C LLPCatalog = [select id,Name,Catalog_Price__c,Part_Number_New__c 
                                         from LLP_Catalog__C where Name =: TestLLPCatalogTriggerHandler.deletedYear];
            delete LLPCatalog;
        }
        catch(Exception e){
           System.debug('e.getMessage() ' + e.getMessage());
           String expectedError = 'Latest catalog year is 2019.Cannot delete catalog record for 2018.';
           if(e.getMessage().contains(expectedError)){
           		system.assert(true);
           } else {
               system.assert(false);
           }
        }
    }
}