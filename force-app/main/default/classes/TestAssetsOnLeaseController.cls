@isTest
public class TestAssetsOnLeaseController {
    @isTest
    public static void testgetData(){
        Date lastDayOfMonth = System.today().addMonths(2).toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        Custom_Lookup__c lookup = new Custom_Lookup__c(Name = 'Default', Lookup_Type__c = 'Engine', Active__c = true);
        insert lookup;

        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        Lease__c leaseRecord=new Lease__c();
        leaseRecord.Security_Deposit__c=200;
        leaseRecord.Lessee__c=operatorRecord.Id;
        leaseRecord.Lease_Id_External__c='test local';
        leaseRecord.Lease_Start_Date_New__c=minStartDate;
        leaseRecord.Lease_End_Date_New__c=lastDayOfMonth;
        leaseRecord.Lease_Status_External__c = 'Active';
        insert leaseRecord;
        
        Id recordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        system.debug('recordTypeId'+recordTypeId);
        Aircraft__c aircraftRecord=new Aircraft__c();
        aircraftRecord.Name='test aircraft';
        aircraftRecord.MSN_Number__c='123';
        aircraftRecord.Date_of_Manufacture__c = minStartDate;
        aircraftRecord.Aircraft_Type__c='737';
        aircraftRecord.Aircraft_Variant__c='400';
        aircraftRecord.TSN__c=1.00;
        aircraftRecord.CSN__c=1;
        aircraftRecord.Status__c='Active';
        aircraftRecord.RecordTypeId=recordTypeId;
        insert aircraftRecord;
        aircraftRecord.Lease__c = leaseRecord.Id;
        update aircraftRecord;
        
        leaseRecord.Aircraft__c = aircraftRecord.Id; 
        update leaseRecord;
        
        Constituent_Assembly__c assembly=new Constituent_Assembly__c();
        assembly.Name='test assembly';
        assembly.TSO_External__c=2;
        assembly.CSN__c=4;
        assembly.TSN__c=5;
        assembly.CSO_External__c=3;
        assembly.Type__c='Engine 1';
        assembly.Attached_Aircraft__c=aircraftRecord.Id;
        assembly.Asset__c=aircraftRecord.Id;
        assembly.Serial_Number__c='1';
        insert assembly; 
        
        Test.startTest();
        AssetsOnLeaseController.getData(false);
        Test.stopTest();
    }
    @isTest
    public static void testgetDataForOtherRecordType(){
        Date lastDayOfMonth = System.today().addMonths(2).toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        Custom_Lookup__c lookup = new Custom_Lookup__c(Name = 'Default', Lookup_Type__c = 'Engine', Active__c = true);
        insert lookup;
        
        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        Lease__c leaseRecord=new Lease__c();
        leaseRecord.Security_Deposit__c=200;
        leaseRecord.Lessee__c=operatorRecord.Id;
        leaseRecord.Lease_Id_External__c='test local';
        leaseRecord.Lease_Start_Date_New__c=minStartDate;
        leaseRecord.Lease_End_Date_New__c=lastDayOfMonth;
        leaseRecord.Lease_Status_External__c = 'Active';
        insert leaseRecord;
        
        Id recordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();
        system.debug('recordTypeId'+recordTypeId);
        Aircraft__c aircraftRecord=new Aircraft__c();
        aircraftRecord.Name='test aircraft';
        aircraftRecord.MSN_Number__c='123';
        aircraftRecord.Date_of_Manufacture__c = minStartDate;
        aircraftRecord.Aircraft_Type__c='737';
        aircraftRecord.Aircraft_Variant__c='400';
        aircraftRecord.TSN__c=1.00;
        aircraftRecord.CSN__c=1;
        aircraftRecord.Status__c='Active';
        aircraftRecord.RecordTypeId=recordTypeId;
        insert aircraftRecord;
        aircraftRecord.Lease__c = leaseRecord.Id;
        update aircraftRecord;
        
        leaseRecord.Aircraft__c = aircraftRecord.Id; 
        update leaseRecord;
        
        Constituent_Assembly__c assembly=new Constituent_Assembly__c();
        assembly.Name='test assembly';
        assembly.TSO_External__c=2;
        assembly.CSN__c=4;
        assembly.TSN__c=5;
        assembly.CSO_External__c=3;
        assembly.Type__c='Engine 1';
        assembly.Attached_Aircraft__c=aircraftRecord.Id;
        assembly.Asset__c=aircraftRecord.Id;
        assembly.Serial_Number__c='1';
        insert assembly; 
        
        Test.startTest();
        AssetsOnLeaseController.getData(false);
        Test.stopTest();
    }
}