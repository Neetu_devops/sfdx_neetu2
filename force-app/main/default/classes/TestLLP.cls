/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestLLP {

	@isTest(seeAllData=true)
    static  void CRUD_LLPatSVTriggerHandler_Test() {
        // TO DO: implement unit test
	    Constituent_Assembly__c[] CAlist = [select id from Constituent_Assembly__c where name='TestSeededMSN1-E'
	    									and type__c like'Engine%'];
        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009'); 
        Test.startTest();    
        list<Assembly_Eligible_Event__c>   listCAEE = new list<Assembly_Eligible_Event__c>();
    	for(Constituent_Assembly__c curRec: CAlist){
        	Assembly_Eligible_Event__c aEE= new Assembly_Eligible_Event__c( Event_Type__c='Performance Restoration',
        		Constituent_Assembly__c=curRec.Id, Start_Date__c=dateStart, End_Date__c=dateEnd, Claim_Amount__c=1000000,TSN__c = 100, CSN__c = 100);
    		listCAEE.add( aEE);
    		
    	}
		LeaseWareUtils.clearFromTrigger(); insert listCAEE;

		LLP_SV__c[] LLPSVList = [select id from LLP_SV__c where Assembly_Maintenance_Event__c in :listCAEE];
		for(LLP_SV__c curRec :LLPSVList){
			curRec.tsn__c =111;
		}
		LeaseWareUtils.clearFromTrigger(); update LLPSVList;
        Test.stopTest(); 
    }
    
	@isTest(seeAllData=true)
    static  void CRUD_LLPBaseReadingCorrection_Test() {
        // TO DO: implement unit test
        
        
        
        
	    Constituent_Assembly__c[] CAlist = [select id from Constituent_Assembly__c where name='TestSeededMSN1-E'
	    									and type__c like'Engine%'];
        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009'); 
        Test.startTest();    
        list<Assembly_Eligible_Event__c>   listCAEE = new list<Assembly_Eligible_Event__c>();
    	for(Constituent_Assembly__c curRec: CAlist){
        	Assembly_Eligible_Event__c aEE= new Assembly_Eligible_Event__c( Event_Type__c='Performance Restoration',
        		Constituent_Assembly__c=curRec.Id, Start_Date__c=dateStart, End_Date__c=dateEnd, Claim_Amount__c=1000000,TSN__c = 100, CSN__c = 100);
    		listCAEE.add( aEE);
    		
    	}
		LeaseWareUtils.clearFromTrigger(); insert listCAEE;

		LLP_SV__c[] LLPSVList = [select id from LLP_SV__c where Assembly_Maintenance_Event__c in :listCAEE limit 1];
		for(LLP_SV__c curRec :LLPSVList){
			curRec.tsn__c =111;
			curRec.Status__c='Open';
		}
		LeaseWareUtils.clearFromTrigger(); update LLPSVList;
		LeaseWareUtils.clearFromTrigger();LwglobalUtils.UpdateLLPsFromAME(listCAEE[0].Id);
        Test.stopTest(); 
    }    
    
}