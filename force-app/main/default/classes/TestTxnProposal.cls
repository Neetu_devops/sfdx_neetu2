/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest 
private class TestTxnProposal {


    static testMethod void myUnitTest() {
    	map<String,Object> mapInOut = new map<String,Object>();
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId = (String)mapInOut.get('Operator__c.Id');    	
		
	    LeaseWareUtils.clearFromTrigger();
	        	
        Transaction__c myTxn = new Transaction__c(Name='My Txn', Ascend_Value__c=3000000, Seller_Type__c='Airline', 
        	Estimated_Closing__c= system.today().addYears(1) ,
        	Seller_Airline__c=OperatorId);
        
        insert myTxn;
        
        myTxn.Seller_Type__c='Other';
        myTxn.Seller__c='Other';
        update myTxn;
        
        Transaction_Proposal__c myTxnProp = new Transaction_Proposal__c(Transaction__c=myTxn.id, Name='Test Txn Prop', Bid__c=200000,Purchase_Price__c = 2500000
        											);
        
        insert myTxnProp;
        
        Aircraft_In_Transaction__c newAiT = new Aircraft_In_Transaction__c (Transaction__c=myTxn.id, Name='123');
        insert newAiT;

		try{
			insert newAiT;
		}catch(exception e){
			system.debug('Expected ' + e.getMessage());
		}
		
	    LeaseWareUtils.clearFromTrigger();
		Aircraft_In_Transaction__c newAiT2= newAiT.clone();
		
	    LeaseWareUtils.clearFromTrigger();
		newAiT.Name='234';        
        update newAiT;
		
	    LeaseWareUtils.clearFromTrigger();        
        insert newAiT2;
		
	    LeaseWareUtils.clearFromTrigger();        
        delete newAiT;
		
	    LeaseWareUtils.clearFromTrigger();        
        delete newAiT2;
        
    }
    
	static testMethod void AITCrudTest(){
		map<String,Object> mapInOut = new map<String,Object>();
		
		mapInOut.put('Transaction__c',null);
    	TestLeaseworkUtil.insertTransaction(mapInOut); 
    	Transaction__c trx1 = (Transaction__c)mapInOut.get('Transaction__c');
    	trx1.Transaction_Type__c = 'Acquisition';
    	update trx1;
     	
		
	    LeaseWareUtils.clearFromTrigger();
	    		
		Aircraft_In_Transaction__c newAiT = new Aircraft_In_Transaction__c(Name = '345',
			Transaction__c = trx1.id,
        	Lease_End_Date__c = Date.parse('10/31/2016'),
        	Data_as_of_Eng_1__c = Date.parse('02/27/2016'),
        	Engine1_TSN__c = 1000,
        	Engine_1_Lowest_Limiter_Cycles__c = 2000,
        	Engine_1_FH__c = 100,
         	Engine_1_FC__c = 100,

        	Engine_2_Lowest_Limiter_Cycles__c = 2000,         	
        	Engine_2_FH__c = 100,
         	Engine_2_FC__c = 100,         	
         	Life_Limit__c = 23000,
        	Life_Limit_On_Lowest_Limiter__c = 25000,
        	Life_Limit_On_Lowest_Limiter_Eng_2__c = 25000,
        	Rent__c = 275000,
        	Term_Mo__c = 25,
        	Equity__c = 250,
        	Price__c = 800);
		
		insert newAiT;
		
		
	    LeaseWareUtils.clearFromTrigger();
	    
	    newAiT.Life_Limit_On_Lowest_Limiter__c = 28000;
	    newAiT.Equity__c = null;
	    newAiT.Equity_Percentage__c= 25;
	    newAiT.Engine_1_FH__c = null;
	    newAiT.Engine_1_FC__c = 100;
	    newAiT.FH_FC_Engine_1__c = 25;
	    newAiT.Engine_2_FH__c = null;
	    newAiT.Engine_2_FC__c = 100;
	    newAiT.FH_FC_Engine_2__c = 25;	
	    newAiT.Engine_1_Lowest_Limiter_Cycles__c = null;
	    newAiT.Cycles_Remaining_Engine_1__c = 25;
		newAiT.Engine_2_Lowest_Limiter_Cycles__c =null;
		newAiT.Cycles_Remaining_Engine_2__c = 25;
		newAiT.Life_Limit_On_Lowest_Limiter_Eng_2__c = 100;   
	    update newAiT;

		
	    LeaseWareUtils.clearFromTrigger();
	    
	    delete newAiT;	    
		
	    LeaseWareUtils.clearFromTrigger();
	    
	    Undelete newAiT;		    
	}    
	
    @isTest(seeAllData=true)
    static void CRUD_TransactionTerm_Test() {

        Transaction__c myTxn = new Transaction__c(Name='My Txn', Ascend_Value__c=3000000,
            Seller_Type__c='Other', 
            Estimated_Closing__c= system.today().addYears(1) ,
            Seller__c='Other');
        LeaseWareUtils.clearFromTrigger();insert myTxn;
        
        Aircraft_In_Transaction__c newAiT = new Aircraft_In_Transaction__c (Transaction__c=myTxn.Id, Name='123');
        LeaseWareUtils.clearFromTrigger();insert newAiT;

        Transaction_Proposal__c myTxnProp = new Transaction_Proposal__c(Transaction__c=myTxn.Id, Name='Test Txn Prop', Bid__c=200000);
        LeaseWareUtils.clearFromTrigger();insert myTxnProp;

        Transaction_Terms__c  TT = new Transaction_Terms__c(name='ddd',Aircraft_In_Transaction__c =newAiT.Id ,Transaction_Prospect__c=myTxnProp.Id , Bid_M__c=10000);
        LeaseWareUtils.clearFromTrigger();insert TT;
        
        LeaseWareUtils.clearFromTrigger();update TT;
        
        LeaseWareUtils.clearFromTrigger();delete TT;
                    
    } 	
}