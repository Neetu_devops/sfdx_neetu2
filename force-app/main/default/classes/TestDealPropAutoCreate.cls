/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDealPropAutoCreate {
    //TestLeaseworkUtil globalTestUtil = new TestLeaseworkUtil();
    
    @isTest(seeAllData=true)
    static  void myUnitTest() {
        
        Deal__c newDeal = new Deal__c(Name='The Deal');
        insert newDeal;
        Deal_Proposal__c DP = new Deal_Proposal__c(Name='Deal Prop', Terms_Communicated__c=true, Deal__c=newDeal.Id, Prospect__c=[select Id from Operator__c limit 1].id);
        try{
            insert DP;
        }catch(exception e){
            system.debug('Excepted, can\'t create proposal without AC In Deal' + e);
        }

        Aircraft_In_Deal__c newACInDeal;
        try{
            newACInDeal = new Aircraft_In_Deal__c(Name='The AC in Deal', Marketing_Deal__c=newDeal.Id, Aircraft__c=[select Id from aircraft__c where id not in (select aircraft__c from Aircraft_In_Deal__c) limit 1].Id);
            insert newACInDeal;
            
            update newACInDeal;
        }catch(exception e){
            system.debug('Exception '+e.getMessage() );
        }
        
        try{
            insert DP;
        }catch(exception e){
            system.debug('Can\'t check Terms_Communicated__c while creating DP.' + e);
        }

        try{
            DP.Terms_Communicated__c=false;
            update DP;
        }catch(exception e){
            system.debug('Terms_Communicated__c can be checked while creating DP.' + e);
        }

        try{
            DP.Terms_Communicated__c=true;
            update DP;
        }catch(exception e){
            system.debug('Can\'t check Terms_Communicated__c without providing term parameters.' + e);
        }
        
        MSN_s_Of_Interest__c[] Terms = [select id, Term_Mos__c, Rent_000__c, Investment_Required_Initial_Estimate__c
                from MSN_s_Of_Interest__c where Deal_Proposal__c = :DP.id];
        for(MSN_s_Of_Interest__c curTerm: Terms){
            curTerm.Term_Mos__c = 10;
            curTerm.Rent_000__c = 1000;
            curTerm.Investment_Required_Initial_Estimate__c = 2000;
        }
        update Terms;
        update DP;
    
        try{    
            delete newACInDeal;
        }catch(exception e){
            system.debug('Can\'t delete AID if terms exist.' + e);
        }

        try{    
            newACInDeal = new Aircraft_In_Deal__c(Name='The AC in Deal', Marketing_Deal__c=newDeal.Id, Aircraft__c=[select Id from Aircraft__c where Aircraft_type__c='A300' limit 1].Id);
            insert newACInDeal;
            delete newACInDeal;
        }catch(exception e){
            system.debug('Exception  ' + e);
        }

        
    }
    // validate AircraftInDeal_InsUpdDel trigger : Insert mode : negative testcase
    @isTest(seeAllData=false)
    static  void triggerInsert1Test() {    
        Deal__c newDeal = new Deal__c(Name='The Deal');
        LeaseWareUtils.clearFromTrigger();insert newDeal;     
        Aircraft_In_Deal__c newACInDeal;
        try{
            newACInDeal = new Aircraft_In_Deal__c(Name='The AC in Deal');
            LeaseWareUtils.clearFromTrigger();insert newACInDeal;
            
            // Below code should not be reachable
            System.assertEquals(1,2);
        }catch (System.DmlException e) {
            System.debug('Expected failure : triggerInsertTest   ' + e);
            System.assertEquals('Please select either Aircraft or Aircraft In Transaction. One of the values is required.',e.getDmlMessage(0));
        
        } 
    }
    @isTest(seeAllData=false)
    static  void triggerMiscTest() {    
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);   

        mapInOut.put('Aircraft__c.variant1','A320');   
        mapInOut.put('Aircraft__c.variant1_add','-200'); 
        mapInOut.put('Aircraft__c.msn','54321'); 
        mapInOut.put('Aircraft__c.CurrentLesseeShortName','AI'); 
            
        TestLeaseworkUtil.insertAircraft(mapInOut);  
        
        Deal__c newDeal = new Deal__c(Name='The Deal');
        LeaseWareUtils.clearFromTrigger();insert newDeal;     
        Aircraft_In_Deal__c newACInDeal;
        system.debug('Aircraft__c.Id ' + (string)mapInOut.get('Aircraft__c.Id'));
        newACInDeal = new Aircraft_In_Deal__c(Name='The AC in Deal',Marketing_Deal__c=newDeal.Id, aircraft__c =(string) mapInOut.get('Aircraft__c.Id'));
        LeaseWareUtils.clearFromTrigger();insert newACInDeal;
        
        ApexPages.currentPage().getParameters().put('Id',newACInDeal.id);  
        ApexPages.StandardController scontroller = new ApexPages.standardController( newACInDeal );
		AiCSetRecTypeOnNewController A1 = new   AiCSetRecTypeOnNewController(scontroller);     
    }    
    // validate AircraftInDeal_InsUpdDel trigger : Insert Mode
    @isTest(seeAllData=false)
    static  void triggerInsert2Test() {  
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);   

        mapInOut.put('Aircraft__c.variant1','A320');   
        mapInOut.put('Aircraft__c.variant1_add','-200'); 
        mapInOut.put('Aircraft__c.msn','54321'); 
        mapInOut.put('Aircraft__c.CurrentLesseeShortName','AI'); 
            
        TestLeaseworkUtil.insertAircraft(mapInOut);  
        
        Deal__c newDeal = new Deal__c(Name='The Deal');
        LeaseWareUtils.clearFromTrigger();insert newDeal;     
        Aircraft_In_Deal__c newACInDeal;
        system.debug('Aircraft__c.Id ' + (string)mapInOut.get('Aircraft__c.Id'));
        newACInDeal = new Aircraft_In_Deal__c(Name='The AC in Deal',Marketing_Deal__c=newDeal.Id, aircraft__c =(string) mapInOut.get('Aircraft__c.Id'));
        LeaseWareUtils.clearFromTrigger();insert newACInDeal;

    }   
    // validate AircraftInDeal_InsUpdDel trigger : Update Mode :negative testcase
    @isTest(seeAllData=false)
    static  void triggerUpdate1Test() {  
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        
        TestLeaseworkUtil.insertAircraft(mapInOut);  
        
        Deal__c newDeal = new Deal__c(Name='The Deal');
        LeaseWareUtils.clearFromTrigger();insert newDeal;     
        Aircraft_In_Deal__c newACInDeal;
        try{
            system.debug('Aircraft__c.Id ' + (string)mapInOut.get('Aircraft__c.Id'));
            newACInDeal = new Aircraft_In_Deal__c(Name='The AC in Deal',Marketing_Deal__c=newDeal.Id, aircraft__c = (string)mapInOut.get('Aircraft__c.Id'));
            LeaseWareUtils.clearFromTrigger();insert newACInDeal;
            newACInDeal.aircraft__c = null;
            LeaseWareUtils.clearFromTrigger();update newACInDeal ;
            System.assertEquals(1,2);
        }catch (System.DmlException e) {
            System.debug('Expected failure : triggerInsertTest   ' + e);
            System.assertEquals('Please select either Aircraft or Aircraft In Transaction. One of the values is required.',e.getDmlMessage(0));
        
        } 

    }    
    @isTest(seeAllData=false) 
    static  void testDuplicateProspectCheck() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        // Add 3 aircraft
        mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft1 = (string) mapInOut.get('Aircraft__c.Id');
        
        mapInOut.put('Aircraft__c.msn','MSN2');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft2 = (string) mapInOut.get('Aircraft__c.Id');

        
        // Create Campaign        
		TestLeaseworkUtil.createDeal(mapInOut);
        Test.startTest();
        // Create Aircraft in Campaign
        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft1);
        mapInOut.put('Aircraft_In_Deal__c.Marketing_Deal__c',(String)mapInOut.get('Deal__c.Id'));
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        
        Id AICId1 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        

        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft2);
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        Id AICId2 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
       
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId = (String)mapInOut.get('Operator__c.Id');
        
        
        LeaseWareUtils.clearFromTrigger();        
          
        // Create Operator
        mapInOut.put('Operator__c.Name','Jet1');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId2 = (String)mapInOut.get('Operator__c.Id');        
        
        
        LeaseWareUtils.clearFromTrigger();        
          
        
        // Create First Prospect
        mapInOut.put('Deal_Proposal__c.Deal__c',(String)mapInOut.get('Deal__c.Id'));
        mapInOut.put('Deal_Proposal__c.Prospect__c',OperatorId);
		TestLeaseworkUtil.createDealProsposal(mapInOut); 
        
        
        LeaseWareUtils.clearFromTrigger();        
          
		
        // Create Second Prospect
        mapInOut.put('Deal_Proposal__c.Deal__c',(String)mapInOut.get('Deal__c.Id'));
        mapInOut.put('Deal_Proposal__c.Prospect__c',OperatorId2);
		TestLeaseworkUtil.createDealProsposal(mapInOut); 	
		Test.stopTest();
		Deal_Proposal__c DPRec = [select Id,Prospect__c from Deal_Proposal__c where Prospect__c = :OperatorId2 limit 1];
		
        
        LeaseWareUtils.clearFromTrigger();		
		//setting first operatot
		DPRec.Prospect__c = OperatorId;
		try{
			update DPRec;
			system.assert(false);// this should not get called
		}catch (System.DmlException e) {
            System.debug('Expected failure : testDuplicateProspectCheck Update   ' + e);
            System.assertEquals('The prospect Air India already exists under this campaign. You can add a prospect to a campaign only once.',e.getDmlMessage(0));
        
        } 
        
        LeaseWareUtils.clearFromTrigger();        
          
		
        // Create Third Prospect with same Operator Name
		try{
			TestLeaseworkUtil.createDealProsposal(mapInOut); 
			system.assert(false);// this should not get called
		}catch (System.DmlException e) {
            System.debug('Expected failure : testDuplicateProspectCheck Insert  ' + e);
            System.assertEquals('The prospect Jet1 already exists under this campaign. You can add a prospect to a campaign only once.',e.getDmlMessage(0));
        
        } 			
		
    }         
     
}