//Last modified : 2016.11.21
public class OperatorHandler implements ITrigger{
     
    // Constructor
    private final  string triggerBefore = 'OperatorHandlerBefore';
    private final  string triggerAfter = 'OperatorHandlerAfter';
    private  map<Id, String> mapCountry = new map<Id,String>();
    public OperatorHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        if(Trigger.isDelete)return;
            
                Set<Id> countryIds = new Set<Id>();
                for(Operator__c opr : (list<Operator__c>)trigger.new){
                    if(opr.Country_Lookup__c!=null){
                        countryIds.add(opr.Country_Lookup__c);
                    }
                    if(opr.Billing_Country_Lookup__c!=null){
                        countryIds.add(opr.Billing_Country_Lookup__c);
                    }
                    if(opr.Country_Of_Registration_Lookup__c!=null){
                        countryIds.add(opr.Country_Of_Registration_Lookup__c);
                    }
                    
                }
                system.debug('countryIds---'+countryIds);
                for(Country__c cou:  [select Id,Name from Country__c where ID IN: countryIds]){
                    mapCountry.put(cou.Id,cou.Name);
                }
            
         
       
        


    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {

        //LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('OperatorHandler.beforeInsert(+)');
        befInsUpd();
        EmailFieldValidations();
        updateSearchableFieldOfCountry(trigger.new);
        system.debug('OperatorHandler.beforeInsert(+)');    
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('OperatorHandler.beforeUpdate(+)');
        befInsUpd();
        EmailFieldValidations();
        List<Operator__c> oprListToUpdateTextCountry = new List<Operator__c>();
        Operator__c[] listNew = (list<Operator__c>)trigger.new ;
        Operator__c oldRec;
        for(Operator__c curRec : listNew){
            oldRec = (Operator__c)trigger.oldMap.get(curRec.id);
            if(curRec.Country_Lookup__c != oldRec.Country_Lookup__c || curRec.Billing_Country_Lookup__c != oldRec.Billing_Country_Lookup__c 
                || curRec.Country_Of_Registration_Lookup__c != oldRec.Country_Of_Registration_Lookup__c){
                    oprListToUpdateTextCountry.add(curRec);
                }
        }
        if(oprListToUpdateTextCountry.size()>0){
            updateSearchableFieldOfCountry(oprListToUpdateTextCountry);
        }
       
        
        system.debug('OperatorHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        system.debug('OperatorHandler.beforeDelete(+)');
        
        
        system.debug('OperatorHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('OperatorHandler.afterInsert(+)');
        createAccount();
        
        system.debug('OperatorHandler.afterInsert(-)');         
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('OperatorHandler.afterUpdate(+)');
        
        // calling quick action
        QuickActionCall();
        system.debug('OperatorHandler.afterUpdate(-)');         
    }
     
    public void afterDelete()
    {

        system.debug('OperatorHandler.afterDelete(+)');
        
        
        
        system.debug('OperatorHandler.afterDelete(-)');         
    }

    public void afterUnDelete()
    {
        system.debug('OperatorHandler.afterUnDelete(+)');
        
        system.assert(false); 
        
        system.debug('OperatorHandler.afterUnDelete(-)');       
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

    
    private void QuickActionCall(){

		list<Operator__c> listNew =  (list<Operator__c>)trigger.new;
		
		// 1. call Associate_World_Fleet 
		if(listNew[0].getQuickActionName() == Schema.Operator__c.QuickAction.Associate_World_Fleet){
			string returnMessage = LWGlobalUtils.setMatchingOpsOnWF(listNew[0].Id);
			
			if(returnMessage.contains('Could not update World Fleet.')){
				listNew[0].addError(returnMessage);
			}
		}
		
		//2. Associate World Fleet All
		if(listNew[0].getQuickActionName() == Schema.Operator__c.QuickAction.Associate_World_Fleet_All){
			//deprecated
		}
		
		
		//3. Update_Fleet_Summary
		if(listNew[0].getQuickActionName() == Schema.Operator__c.QuickAction.Update_Fleet_Summary){
			string returnMessage = LWGlobalUtils.callGlobalMethod('updateFleetSummary', listNew[0].Id);
			
			if(returnMessage.contains('Unexpected error')){
				listNew[0].addError(returnMessage);
			}
		}
		
		//4. Update_Fleet_Summary_All
	/*	if(listNew[0].getQuickActionName() == Schema.Operator__c.QuickAction.Update_Fleet_Summary_All){
			string returnMessage = LWGlobalUtils.callGlobalMethod('updateFleetSummaryAll', listNew[0].Id);
			//LeasewareUtils.updateFleetSummaryAll()
			if(returnMessage.contains('Unexpected error :')){
				listNew[0].addError(returnMessage);
			}
		}*/
		
		
		
    }
    
    
    
    //before insert, before update
    private void befInsUpd(){
    	list<Operator__c> listNew =  (list<Operator__c>)trigger.new;
    	Operator__c Oldrec;
    	set<string> setAaircraftTypeInterested = new set<string>();
    	list<string> strATGP;
    	for(Operator__c opNewRec: listNew ){
    		setAaircraftTypeInterested.clear();
    		if(trigger.IsInsert && opNewRec.Aircraft_Type_Of_Interest_gp__c!=null){
    			opNewRec.Not_Contacted_For_AC_Types_gp__c = opNewRec.Aircraft_Type_Of_Interest_gp__c;
    		}else if(trigger.isupdate){
    			Oldrec = (Operator__c)trigger.oldmap.get(opNewRec.Id);
    			if(Oldrec.Aircraft_Type_Of_Interest_gp__c != opNewRec.Aircraft_Type_Of_Interest_gp__c ||  Oldrec.Contacted_For_AC_Types_gp__c != opNewRec.Contacted_For_AC_Types_gp__c){
    				opNewRec.Not_Contacted_For_AC_Types_gp__c =null;
    				if(opNewRec.Aircraft_Type_Of_Interest_gp__c!=null){
    					strATGP = opNewRec.Aircraft_Type_Of_Interest_gp__c.split(';');
    					setAaircraftTypeInterested.addAll(new Set<string>(strATGP));
    					if(opNewRec.Contacted_For_AC_Types_gp__c!=null){
    						strATGP = opNewRec.Contacted_For_AC_Types_gp__c.split(';');
    						setAaircraftTypeInterested.removeAll(new Set<string>(strATGP));
    					}
   						opNewRec.Not_Contacted_For_AC_Types_gp__c = LeasewareUtils.convertSetStrToMultiPick(setAaircraftTypeInterested);
    				}
    			}//if any change
            }//update case
            
           
    	}//for loop
    	
    	
	}
	
    //before insert, before update
	private void EmailFieldValidations(){
	
		list<Operator__c> listNew =  (list<Operator__c>)trigger.new;
		list<string> listEmailIdsforEmail1;
		Pattern emailPattern = Pattern.compile('^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,})$');
		
		for(Operator__c theOperator: listNew ){
			if(theOperator.Main_Email_Address_New__c==null && theOperator.Billing_Email_Address_New__c==null) continue;
		    
		    if(theOperator.Main_Email_Address_New__c!=null){
		    	listEmailIdsforEmail1 = theOperator.Main_Email_Address_New__c.split(',', 0);
			    for(string strEmail : listEmailIdsforEmail1){
			        Matcher emailMatcher = emailPattern.matcher(strEmail.trim());
			        if(!emailMatcher.matches()){
			            theOperator.Main_Email_Address_New__c.addError('Invalid email address or comma separated list of emails.');
			            break;
			        }
				}		    	
		    }
		    
		    if(theOperator.Billing_Email_Address_New__c!=null){
		    	listEmailIdsforEmail1 = theOperator.Billing_Email_Address_New__c.split(',', 0);
	 			for(string strEmail : listEmailIdsforEmail1){
			        Matcher emailMatcher = emailPattern.matcher(strEmail.trim());
			        if(!emailMatcher.matches()){
			            theOperator.Billing_Email_Address_New__c.addError('Invalid email address or comma separated list of emails.');
			            break;
			        }
				}
		    }
		
		
		}
		
		
	} 
	
	private void createAccount(){
        if( 'ON'.equals(LeaseWareUtils.getLWSetup_CS('Auto_Creation_of_Accounts'))){
            list<Operator__c> listNew =  (list<Operator__c>)trigger.new;
            List<String> oprNameList = new List<String>();
            for(Operator__c opr: listNew){
                oprNameList.add(opr.Name);
       	    }
            LeasewareUtils.createAccount(oprNameList, 'Operator');
        }
    }

   private void updateSearchableFieldOfCountry(List<Operator__c> oprListToUpdate){
        
        String countryName,billingCountryName,countryOfRegName,textFieldCountry;
        for(Operator__c opr : oprListToUpdate){
            countryName = opr.Country_Lookup__c==null?'':mapCountry.containsKey(opr.Country_Lookup__c)?mapCountry.get(opr.Country_Lookup__c):'';
            billingCountryName= opr.Billing_Country_Lookup__c==null?'':mapCountry.containsKey(opr.Billing_Country_Lookup__c)?mapCountry.get(opr.Billing_Country_Lookup__c):'';
            countryOfRegName = opr.Country_Of_Registration_Lookup__c==null?'':mapCountry.containsKey(opr.Country_Of_Registration_Lookup__c)?mapCountry.get(opr.Country_Of_Registration_Lookup__c):'';
            textFieldCountry = countryName +','+billingCountryName +','+ countryOfRegName ;
            if(textFieldCountry!=null && textFieldCountry.length()>255) opr.Y_Hidden_Country__c = textFieldCountry.left(251) + '...'; 
            else   opr.Y_Hidden_Country__c = textFieldCountry ;

            system.debug('Hidden Country---'+ opr.Y_Hidden_Country__c);
        }
       
        

    }
}