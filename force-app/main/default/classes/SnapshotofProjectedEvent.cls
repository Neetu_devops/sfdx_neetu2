public without sharing class SnapshotofProjectedEvent {

    @AuraEnabled
    public static String createSnapShot(Id recordId, String comments) {
        system.debug('Comments------------' +comments + ', RecordId' +recordId);
        
        try{
            
    
        Map < String, Schema.SObjectField > mapParentFields = Schema.SObjectType.Assembly_Event_Info__c.fields.getMap();
        Map < String, Schema.SObjectField > mapChildFields = Schema.SObjectType.Projected_Event_Snapshot__c.fields.getMap();

        String strQry = 'select ';
        Schema.DescribeFieldResult thisPEFieldDesc;

        for (String Field: mapParentFields.keyset()) {
            thisPEFieldDesc = mapParentFields.get(Field).getDescribe();
            strQry += thisPEFieldDesc.getLocalName() + ', ';
        }
        strQry = strQry.left(strQry.length() - 2);
        strQry += ' from ' + Schema.SObjectType.Assembly_Event_Info__c.getName();
        strQry += ' where id = \'' + recordId + '\'';
        system.debug('strQry====' + strQry);
        Assembly_Event_Info__c projectedEvent = (Assembly_Event_Info__c) database.query(strQry);   

        system.debug('projectedEvent====' + projectedEvent);

        // Insert record frpm Projected Event to Snapshot

        Projected_Event_Snapshot__c snapshotObj = new Projected_Event_Snapshot__c();
        Schema.DescribeFieldResult thisSnapshotFieldDesc;
        Id RcTypeId;

        // this below loop used to update name, RcTypeId, ID
        

            for (String snapshotField: mapChildFields.keyset()) {
            //  RcTypeId = Schema.SObjectType.Assembly_Event_Info__c.getRecordTypeInfosByName().get(projectedEvent.Name).getRecordTypeId();
                thisSnapshotFieldDesc = mapChildFields.get(snapshotField).getDescribe();
               if (thisSnapshotFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula, Lkp
               if (!thisSnapshotFieldDesc.isCustom() && !thisSnapshotFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
               if (thisSnapshotFieldDesc.getLocalName().equalsIgnoreCase('Projected_Event__c')) continue; 
               if (!strQry.contains(thisSnapshotFieldDesc.getLocalName())) continue; 
               system.debug('Snapshot field desc----------' + thisSnapshotFieldDesc.getLocalName());
               system.debug('Snapshot Field----' +snapshotField );
                system.debug('Get Value------' +projectedEvent.get(snapshotField));
               snapshotObj.put(thisSnapshotFieldDesc.getLocalName(), projectedEvent.get(snapshotField));
            }


           system.debug('Projected Event Id---'+ projectedEvent.Id);
            snapshotObj.Projected_Event__c = projectedEvent.Id;
           snapshotObj.Name = projectedEvent.Name + '-'+ system.now();
            
            //capturing global picklist and Formula field from PE to text field in PE revision object
            /***************Commenting this as dynamicaaly it is copying now, no need of manual copy**********/
     /*     snapshotObj.Assembly_Type__c = projectedEvent.Assembly_Type__c; 
            snapshotObj.Event_Due_Date_Calculated__c=projectedEvent.Event_Due_Date_Calculated__c;
            snapshotObj.Event_Due_CSN_Calculated__c=projectedEvent.Event_Due_CSN_Calculated__c;
            snapshotObj.Event_Due_Date__c=projectedEvent.Event_Due_Date__c;
            snapshotObj.Event_Due_Override__c=projectedEvent.Event_Due_Override__c;
            snapshotObj.Event_Due_TSN_Calculated__c=projectedEvent.Event_Due_TSN_Calculated__c;
            snapshotObj.Event_Override_Source__c=projectedEvent.Event_Override_Source__c;
            snapshotObj.Event_Type__c= projectedEvent.Event_Type__c;
            snapshotObj.Interval_2_Cycles__c = projectedEvent.Interval_2_Cycles__c;
            snapshotObj.Interval_1_Cycles__c = projectedEvent.Interval_1_Cycles__c;
            snapshotObj.Interval_2_Hours__c = projectedEvent.Interval_2_Hours__c;
            snapshotObj.Interval_1_Hours__c = projectedEvent.Interval_1_Hours__c;
            snapshotObj.Interval_2_Months__c = projectedEvent.Interval_2_Months__c;
            snapshotObj.Interval_1_Months__c = projectedEvent.Interval_1_Months__c;
  			snapshotObj.Since_Last_Cycles__c = projectedEvent.Since_Last_Cycles__c;
            snapshotObj.Since_Last_Hours__c = projectedEvent.Since_Last_Hours__c;
            snapshotObj.Since_Last_Months__c = projectedEvent.Since_Last_Months__c;
            snapshotObj.Status__c = projectedEvent.Status__c;
            snapshotObj.To_Go_Cycles__c = projectedEvent.To_Go_Cycles__c;
            snapshotObj.To_Go_Hours__c=projectedEvent.To_Go_Hours__c;
            snapshotObj.To_Go_Months__c= projectedEvent.To_Go_Months__c;*/
            
            //update Comments
            //
            snapshotObj.Snapshot_Comment__c = comments;
            
                
            
            insert snapshotObj;
            
            return 'SUCCESS';
       }
        catch(Exception e){
            system.debug(
                e.getStackTraceString());
            system.debug('Exception e--------'+ e.getMessage() + e.getLineNumber());
            return e.getMessage();
        }
        
    }
}