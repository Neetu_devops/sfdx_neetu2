public with sharing class Notify_LookupUtilityController {
    @AuraEnabled (Cacheable = true)
    public static List<LookupSearchResult> search(String objectName, String strQueryField, String filterFieldAPIName, String searchTerm, List<String> selectedIds, String iconName, String whereClause, String recordCount) {
        try {
            String strQuery;
            List<Id> selectedIdSet = new List<Id>();
            for (Id objId : (List<Id>) selectedIds) {
                if (String.valueOf(objId.getsobjecttype()) == objectName) {
                    selectedIdSet.add(objId);
                }
            }
            if (searchTerm == '') {
                strQuery = 'SELECT Id, ' + String.escapeSingleQuotes(strQueryField) 
                + ' FROM ' 
                + String.escapeSingleQuotes(objectName) 
                + (selectedIdSet.size() > 0 ? ' WHERE Id NOT IN: selectedIdSet' + (whereClause != '' ?  + ' AND ' + whereClause : '') : (whereClause != '' ? ' WHERE ' + whereClause : ''))
                + ' LIMIT ' + Integer.valueOf(recordCount);
            } else {
                strQuery = 'SELECT Id, ' + String.escapeSingleQuotes(strQueryField) 
                + ' FROM ' 
                + String.escapeSingleQuotes(objectName) 
                + ' WHERE ' + filterFieldAPIName + '  LIKE \'' + searchTerm + '%\'' + (selectedIdSet.size() > 0 ? ' AND Id NOT IN: selectedIdSet' : '' )  + (whereClause != '' ? ' AND ' + whereClause : '')
                + ' LIMIT ' + Integer.valueOf(recordCount);
            }
            System.debug('strQuery:' + strQuery);
            List<SObject> lstResult = database.query(strQuery);

            // Prepare results
            List<LookupSearchResult> results = new List<LookupSearchResult>();
            for (SObject sobj : lstResult) {
                results.add(new LookupSearchResult(sobj.Id, objectName, iconName, String.valueOf(sobj.get(filterFieldAPIName))));
            }
            results.sort();
            return results;
        }
        catch (Exception ex) {
            System.debug('ERROR: ' + ex.getStackTraceString() + ': Line: ' + ex.getLineNumber() + ': MSG : ' + ex.getMessage());
            return null;
        }
    }
    
    public class LookupSearchResult implements Comparable {
        @AuraEnabled public Id id { get; set; }
        @AuraEnabled public String icon { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String sObjectType { get; set; }
        
        public LookupSearchResult(Id id, String sObjectType, String icon, String name) {
            this.id = id;
            this.sObjectType = sObjectType;
            this.icon = icon;
            this.name = name;
        }
        
        public Integer compareTo(Object compareTo) {
            LookupSearchResult lsr = (LookupSearchResult) compareTo;
            if (this.name == null) { return (lsr.name == null) ? 0 : 1; }
            if (lsr.name == null) { return -1; }
            return this.name.compareTo(lsr.name);
        }
    }
}