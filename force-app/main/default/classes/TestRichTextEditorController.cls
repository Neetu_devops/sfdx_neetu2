/**
*******************************************************************************
* Class: TestRichTextEditorController
* @author Created by Aman, Lease-Works, 
* @date 17/08/2020
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
* Test all the Methods of RichTextEditorController.
*
* History:
* VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestRichTextEditorController {
    @isTest static void checkGetSet() {
        String nameSpacePrefix = leasewareutils.getNamespacePrefix();
        Test.startTest();
        Aircraft__c asset = new Aircraft__c(MSN_Number__c = '12345', Date_of_Manufacture__c = System.today(), TSN__c = 0, CSN__c = 0,
                                            Threshold_for_C_Check__c = 1000, Status__c = 'Available', Derate__c = 20, Comments_Technical__c = 'ABC');
        LeaseWareUtils.clearFromTrigger();
        insert asset;
        
        RichTextEditorController.getFieldData('', nameSpacePrefix+'Comments_Technical__c');
        RichTextEditorController.getFieldData(asset.Id, nameSpacePrefix+'Comments_Technical__c');
        RichTextEditorController.saveFieldData(asset.Id, nameSpacePrefix+'Comments_Technical__c', 'XYZ', '');
        
        Set<String> filesToDelete = new Set<String>();
        ContentVersion cVersion = new ContentVersion(Title = 'Penguins', PathOnClient = 'Penguins.jpg', 
                                                    VersionData = Blob.valueOf('Test Content'), IsMajorVersion = true );
        insert cVersion;
        filesToDelete.add(cVersion.Id);
        RichTextEditorController.saveFieldData(asset.Id, nameSpacePrefix+'Comments_Technical__c', 'XYZ', JSON.serialize(filesToDelete));

        ContentVersion cVersion2 = new ContentVersion(Title = 'Penguins', PathOnClient = 'Penguins.jpg', 
                                                    VersionData = Blob.valueOf('Test Content'), IsMajorVersion = true );
        insert cVersion2;
        filesToDelete.add(cVersion2.Id);
        RichTextEditorController.deleteFiles(JSON.serialize(filesToDelete));
        Test.stopTest();
    }
}