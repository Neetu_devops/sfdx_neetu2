//@updated 2016.12.02 : Arjun : Prospect News Feed page controller
public class ProspectNewsArticlesCont 
{ 
    public string aircraftType {get;set;}
    public ProspectNewsArticlesCont (ApexPages.StandardController stdController) 
    {
        Set<string> aircraftTypes = new Set<string>();
        
        Deal_Proposal__c   prospect =  [ SELECT Id,Prospect__c FROM Deal_Proposal__c 
                      WHERE Id=:ApexPages.currentPage().getParameters().get('id') ];    
        
            //Getting AircraftType from First Proposed Term 
            list<MSN_s_Of_Interest__c> prosposedTerms = new list<MSN_s_Of_Interest__c>
                                                        ([ SELECT Id,Aircraft_Type__c 
                                                          FROM MSN_s_Of_Interest__c 
                                                          WHERE Deal_Proposal__c=:prospect.Id ]);
                                                          
            for( MSN_s_Of_Interest__c prosposedTerm: prosposedTerms )
                aircraftTypes.add( prosposedTerm.Aircraft_Type__c ); 
            
            list<string> aircraftTypesList = new list<string>();
            aircraftTypesList.addAll( aircraftTypes );
            
            aircraftType = String.join( aircraftTypesList, ' ,' );  
            
            system.debug('aircraftType=='+aircraftType);
    }
}