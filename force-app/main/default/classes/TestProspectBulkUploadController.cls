@isTest
public class TestProspectBulkUploadController {
    @isTest
    static void testMethod1() {
        operator__c testAirlines = new operator__c(Name='Test Airlines');
        insert testAirlines;
        
        counterParty__c testlessors = new counterParty__c(Name='Test lessors');
        insert testlessors;
        
        bank__c testCounterParties = new bank__c(Name='Test CounterParty');
        insert testCounterParties;
        
        Aircraft__c testAircraft = new Aircraft__c(MSN_Number__c='12345',Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0,
                                        Threshold_for_C_Check__c=1000, Status__c='Available', Derate__c=20);
        insert testAircraft;
        
        ProspectBulkUploadController.ProspectDataWrapper wrapperObj = ProspectBulkUploadController.getProspectData(true, true, true);
        
        String airline = '[{"isChecked":true,"recordId":"'+testAirlines.Id+'","recordName":"Air Travels"}]';
       string lessor = '[{"isChecked":true,"recordId":"'+testlessors.Id+'","recordName":"ACG"}]';
      String counterParty = '[{"isChecked":true,"recordId":"'+testCounterParties.Id+'","recordName":"Bank 1"}]';
      String aircraft = '[{"Id":"'+testAircraft.Id+'","Name":"6653 (A321)"}]';
        
        LW_Setup__c setting = new LW_Setup__c();
        setting.Name = 'Client Controller Name';
        setting.Value__c = 'DataSourceController';
        insert setting;
        try{
            ProspectBulkUploadController.CheckTradingProfileofProspect(airline, lessor, counterParty, aircraft);
        }catch(Exception e){
            System.debug('An exception occurred: ' + e.getMessage());
        }
        delete setting;
        try{
            ProspectBulkUploadController.CheckTradingProfileofProspect(airline, lessor, counterParty, aircraft);
        }catch(Exception e){
            System.debug('An exception occurred: ' + e.getMessage());
        }
    }
}