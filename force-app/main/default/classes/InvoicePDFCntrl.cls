//LastModified : 2016.09.01 9PM
public class InvoicePDFCntrl 
{
	//Commenting out code to be used in future and wouldn't affect code coverage.
    /*public list<Invoice__c> invoices  {get;set;}
    public string lesseeName      {get;set;}
    public string lessorName      {get;set;}    
    public string setupLessorLogo {get;set;}
    public string todaysDate      {get;set;}
    public InvoicePDFCntrl()
    {
        lesseeName  =  lessorName = '';
        invoices   = new list<Invoice__c>();
        list<string> incScheduleIds = new list<string>();
        string incScheduleIdParam  = ApexPages.currentPage().getParameters().get('Ids');
        todaysDate = system.today().format();   
        if( incScheduleIdParam!=null )     
        {                   
            incScheduleIds = incScheduleIdParam.split(',');     
        }           
        
        system.debug('incScheduleIds==='+incScheduleIds); 
        
        invoices =  [ SELECT Id,Name,Rent__c,Invoiced_Amount_Formula__c,Lease__r.Lessee__c,Lease__r.Operator__c,
                       Lease__r.Lessee__r.Name,Lease__r.Operator__r.Name,Lease__r.Lessee__r.Main_Email_Address__c,
                       Lease__r.Operator__r.Main_Email_Address__c,Date_of_MR_Payment_Due__c,Invoice_Date__c,
                       Lease__r.Lessee__r.Address_Street__c,Lease__r.Lessee__r.City__c,Lease__r.Lessee__r.Zip_Code__c,
                       Lease__r.Lessee__r.Country__c,Lease__r.Operator__r.Address_Street__c,Lease__r.Operator__r.City__c,
                       Lease__r.Operator__r.Zip_Code__c,Lease__r.Operator__r.Country__c,Lease__r.Aircraft__r.MSN_Number__c,
                       Month_Ending__c,Lease__r.Aircraft__r.Aircraft_Type__c,Lease__r.lessor_Bank_Name__c,
                       Lease__r.Lessor_Bank_Routing_Number_For_Rent__c,
                       Lease__r.Lessor_Bank_Swift_Code__c,Lease__r.Lessor_Bank_Account_Number__c,Lease__r.Lessor__r.Name,
                       Lease__r.Registration_Number__c,Lease__r.Lessor_Account_Name__c,Lease__r.Lessor_Act_Name__c,
                       Lease__r.Lessor__r.Address_Street__c,Lease__r.Lessor__r.City__c,Lease__r.Lessor__r.Zip_Code__c,
                       Lease__r.Lessor__r.Country__c,Adjustment_1__c,Adjustment_2__c,Adjustment_3__c,Reason_Code_1__c,
                       Reason_Code_2__c,Reason_Code_3__c    
                      FROM Invoice__c
                      WHERE Id in: incScheduleIds
                      ORDER BY Lease__r.Aircraft__r.MSN_Number__c ];        
                
        if( !invoices.isEmpty() )
        {   
           if( invoices[0].Lease__r.Lessee__c!=null )
              lesseeName = invoices[0].Lease__r.Lessee__r.Name;           
           else if( invoices[0].Lease__r.Operator__c!=null )
              lesseeName = invoices[0].Lease__r.Operator__r.Name;
        }
        
        Lessor__c setupLessor = [ SELECT Lease_Logo__c,Name,Address_Street__c,City__c,Country__c,Zip_Code__c 
                                  FROM Lessor__c Limit 1 ];
        if( setupLessor!=null ) 
        {
            setupLessorLogo = setupLessor.Lease_Logo__c;
            lessorName      = setupLessor.Name;
        }   
        system.debug('invoices==  '+invoices.size());
    }*/
}