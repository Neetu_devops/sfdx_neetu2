@isTest

public with sharing class TestPDPUtil {

	public static List<Order__c> createPurchaseAgreement(Integer numToInsert, Boolean doInsert,Map<String,Object> nameValueMap) {
		List<Order__c> listToInsert = new List<Order__c>();
		for(Integer i = 0; i< numToInsert; i++ ) {
			Order__c ord = new Order__c(); 
			ord.Name = 'Test PA '+i;
			listToInsert.add(ord); 
		}
		return createRecords(listToInsert,doInsert,nameValueMap);
	}

	public static List<Batch__c> createBatch(Integer numToInsert,ID structureId,ID PAId, Boolean doInsert,Map<String,Object> nameValueMap) {
		List<Batch__c> listToInsert = new List<Batch__c>();
		for(Integer i = 0; i< numToInsert; i++ ) {
			Batch__c bch = new Batch__c(); 
			bch.Name = 'Test Batch '+ i;
			bch.PDP_Structure__c = structureId;
			bch.Purchase_Agreement__c = PAid;
			listToInsert.add(bch); 
		}
		return createRecords(listToInsert,doInsert,nameValueMap);
	}

	public static List<Structure__c> createStructures(Integer numToInsert, Id PAId,Boolean doInsert,Map<String,Object> nameValueMap) {
		List<Structure__c> listToInsert = new List<Structure__c>();
		for(Integer i = 0; i< numToInsert; i++ ) {
			Structure__c strc = new Structure__c(); 
			strc.Name = 'Test Structure '+ i; 
			strc.Purchase_Agreement__c = PAId;
			listToInsert.add(strc);
		}
		return createRecords(listToInsert,doInsert,nameValueMap);
	}
	
	public static List<Pre_Delivery_Payment__c> createPDP(Integer numToInsert,Id batchId, Boolean doInsert,Map<String,Object> nameValueMap) {
		List<Pre_Delivery_Payment__c> listToInsert = new List<Pre_Delivery_Payment__c>();
		for(Integer i = 0; i< numToInsert; i++ ) {
			Pre_Delivery_Payment__c pdp = new Pre_Delivery_Payment__c(); 
			pdp.Name = 'Test PDP '+ i; 
			pdp.Batch__c = batchId;
			listToInsert.add(pdp);
		}
		return createRecords(listToInsert,doInsert,nameValueMap);
	}

	public static List<Engine_Base_Price__c> createEBP(Integer numToInsert,Id PAId, Boolean doInsert,Map<String,Object> nameValueMap) {
		List<Engine_Base_Price__c> listToInsert = new List<Engine_Base_Price__c>();
		for(Integer i = 0; i< numToInsert; i++ ) {
			Engine_Base_Price__c ebp = new Engine_Base_Price__c(); 
			ebp.Name = 'Test EBP '+ i; 
			ebp.Order__c = PAId;
			listToInsert.add(ebp);
		}
		return createRecords(listToInsert,doInsert,nameValueMap);
	}

	public static List<Aircraft_Base_Price__c> createABP(Integer numToInsert,Id PAId, Boolean doInsert,Map<String,Object> nameValueMap) {
		List<Aircraft_Base_Price__c> listToInsert = new List<Aircraft_Base_Price__c>();
		for( Integer i = 0; i< numToInsert; i++ ) {
			Aircraft_Base_Price__c abp = new Aircraft_Base_Price__c(); 
			abp.Name = 'Test ABP '+ i; 
			abp.Order__c = PAId;
			listToInsert.add(abp);
		}
		return createRecords(listToInsert,doInsert,nameValueMap);
	}
	
	public static List<SObject> createStructureLine(Integer numToInsert,Id structId, Boolean doInsert,Map<String,Object> nameValueMap) {
		List<PDP_Structure_Line__c> listToInsert = new List<PDP_Structure_Line__c>();
		for(Integer i = 0; i< numToInsert; i++ ) {
			PDP_Structure_Line__c line = new PDP_Structure_Line__c(); 
			line.Name = 'Test strucLine '+ i;
			line.Payment__c=4;
			line.Fixed_Date__c=Date.today();
			line.PDP_Structure__c = structId;
			listToInsert.add(line);
		}
		return createRecords(listToInsert,doInsert,nameValueMap);
	}

	public static List<SObject> createCostRevision(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap) {
		List<Cost_Revision__c> listToInsert = new List<Cost_Revision__c>();
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Cost_Revision__c cr = new Cost_Revision__c();
			cr.Name = 'Test ' + i;
			listToInsert.add(cr);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}

    public static List<SObject> createPdpPaymentSchedule(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap) {
		List<PDP_Payment_Schedule__c> listToInsert = new List<PDP_Payment_Schedule__c>();
		for ( Integer i = 0; i < numToInsert; i++ ) {
            List<Pre_Delivery_Payment__c> testDelivery = [SELECT Name FROM Pre_Delivery_Payment__c ];
            PDP_Payment_Schedule__c testPaySch = new PDP_Payment_Schedule__c();
            testPaySch.Name = 'NameNewTest'+i;                
            testPaySch.Paid_Amount_M__c= 11650960;
            testPaySch.Total_Payment_Due_mm__c =11650960;
			testPaySch.Percentage_of_PDPRP__c = 123.12;
            testPaySch.PDP__c = testDelivery[i].Id;
			listToInsert.add(testPaySch);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}

	public static List<SObject> createPdpInterest(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap) {
		List<PDP_Interest__c> listToInsert = new List<PDP_Interest__c>();
		List<PDP_Payment_Schedule__c> listPdpPay = [SELECT Id, Name, Paid_Amount_M__c, Total_Payment_Due_mm__c, Percentage_of_PDPRP__c FROM PDP_Payment_Schedule__c ];
		List<Pre_Delivery_Payment__c> testDelivery = [SELECT Id, Name FROM Pre_Delivery_Payment__c ];
		for ( Integer i = 0; i < numToInsert; i++ ) {
			PDP_Interest__c testPdpInt = new PDP_Interest__c();
			testPdpInt.PDP_Schedule__c = listPdpPay[i].Id;
			testPdpInt.Delivery__c = testDelivery[i].Id;
			testPdpInt.Final_Paymant_Date__c   = Date.valueOf(date.newInstance(2018, 12, 12));
			testPdpInt.Initial_Payment_Date__c = Date.valueOf(date.newInstance(2017, 12, 12));
			
			listToInsert.add(testPdpInt);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// start @g.chamorro
	// - Operator__c
	public static List<SObject> createOperator(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap) {
		
		List<Operator__c> listToInsert = new List<Operator__c>();
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Operator__c obj = new Operator__c();
			obj.Name = 'test'+i;
			// obj.Status__c='Approved';
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	//- Aircraft__c
	public static List<SObject> createAircraft(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap) {
		List<Aircraft__c> listToInsert = new List<Aircraft__c>();	
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Aircraft__c obj = new Aircraft__c();
			obj.Name = 'test'+i;
			// obj.MSN_Number__c='12345';
			// obj.Aircraft_Type__c='737';
			// obj.Date_of_Manufacture__c = System.today();
			// obj.Aircraft_Variant__c='300';
			// obj.TSN__c=0;
			// obj.CSN__c=0;
			// obj.Threshold_for_C_Check__c=1000;
			// obj.Status__c='Available';
			// obj.Model_Code__c='A320';
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// - Lease__c
	public static List<SObject> createLease(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap, Id operatorId, Id aircraftId) {
		List<Lease__c> listToInsert = new List<Lease__c>();	
		
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Lease__c obj = new Lease__c();

			obj.Name='testLease';
			obj.Operator__c=operatorId;
			obj.aircraft__c = aircraftId;
			obj.Lease_Start_Date_New__c=Date.parse('01/01/2010');//req
			obj.Lease_End_Date_New__c=Date.parse('12/31/2012');//req

			// obj.UR_Due_Day__c='10';
			// obj.First_Escalation_Date__c = Date.parse('12/31/2012'); 
			// obj.Lease_Start_Date_New__c=Date.parse('01/01/2010');
			// obj.Lease_End_Date_New__c=Date.parse('12/31/2012');
			// obj.Rent_Date__c=Date.parse('01/01/2010');
			// obj.Rent_End_Date__c=Date.parse('12/31/2012'); 
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// - Assembly_MR_Rate__c
	public static List<SObject> createAssembly_MR_Rate(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap, Id leaseId) {
		List<Assembly_MR_Rate__c> listToInsert = new List<Assembly_MR_Rate__c>();	
		
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Assembly_MR_Rate__c obj = new Assembly_MR_Rate__c();
			obj.Name = 'Airframe - 4C/6Y';
			obj.Lease__c = leaseId;
			// obj.Assembly_Type__c='Airframe';
			obj.Base_MRR__c=32000;
			obj.Rate_Basis__c='Hours';
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// - Custom_Lookup__c
	public static List<SObject> createCustom_Lookup(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap) {
		List<Custom_Lookup__c> listToInsert = new List<Custom_Lookup__c>();	
		
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Custom_Lookup__c obj = new Custom_Lookup__c();
			obj.Name='Default';
			obj.Lookup_Type__c='Engine';
			// obj.active__c=true;
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// - Constituent_Assembly__c
	public static List<SObject> createConstituent_Assembly(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap, Id Attached_Aircraft_Id) {
		List<Constituent_Assembly__c> listToInsert = new List<Constituent_Assembly__c>();	
		
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Constituent_Assembly__c obj = new Constituent_Assembly__c();
			obj.Name='consituentAssetName';
			obj.Attached_Aircraft__c= Attached_Aircraft_Id;
			// obj.Aircraft_Engine__c='737-300 @ CFM56';
			// obj.Engine_Model__c ='CFM56';
			// obj.Description__c ='Desc';
			// obj.Type__c ='Engine 1';
			// obj.Engine_Thrust__c='-3B1';
			obj.TSN__c=0;//req
			obj.CSN__c=0;//req
			obj.Serial_Number__c='123';//req
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// - Assembly_Event_Info__c
	public static List<SObject> createAssembly_Event_Info(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap, Id consttAssembly_Id) {
		List<Assembly_Event_Info__c> listToInsert = new List<Assembly_Event_Info__c>();	
		
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Assembly_Event_Info__c obj = new Assembly_Event_Info__c();
			obj.Name='11224';
			obj.Constituent_Assembly__c =consttAssembly_Id;
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// - Lessor__c
	public static List<SObject> createLessor(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap) {
		List<Lessor__c> listToInsert = new List<Lessor__c>();	
		
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Lessor__c obj = new Lessor__c();
			obj.Name='New Lessor';
			// obj.Lease_Logo__c='http://abcd.lease-works.com/a.gif';
			// obj.Phone_Number__c='2342';
			// obj.Email_Address__c='a@lease.com';
			// obj.Auto_Create_Campaigns__c=true;
			// obj.Number_Of_Months_For_Narrow_Body__c=24;
			// obj.Number_Of_Months_For_Wide_Body__c=24;
			// obj.Narrowbody_Checked_Until__c=date.today();
			// obj.Widebody_Checked_Until__c=date.today();
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// - Utilization_Report__c
	public static List<SObject> createUtilization_Report(Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap, Id Aircraft_Id, Id lease_Id) {
		List<Utilization_Report__c> listToInsert = new List<Utilization_Report__c>();	
		Date initialDate = Date.parse('01/31/2010');
		for ( Integer i = 0; i < numToInsert; i++ ) {
			Utilization_Report__c obj = new Utilization_Report__c();
			obj.Name = 'Utilization Name' + i ;
			obj.Aircraft__c = Aircraft_Id;
			obj.Y_hidden_Lease__c = lease_Id;
			if( i == 0 ) {
				obj.Month_Ending__c = initialDate;
			}
			else {
				obj.Month_Ending__c = initialDate.addMonths(i);
			}
	
			// obj.Airframe_Flight_Hours_Month__c = 12;
			// obj.FH_String__c = '120';
			// obj.Airframe_Cycles_Landing_During_Month__c= 80;			
			
			listToInsert.add(obj);
		}
		return createRecords(listToInsert, doInsert, nameValueMap);
	}
	// stop @g.chamorro


	public static List<SObject> createRecords( List<SObject> records, Boolean doInsert, Map<String, Object> attributes ) {
        Integer i = 0;
        if( attributes != null ){
            for ( Integer j =0; j < records.size(); j ++ ) {
                // get the first SObject getting created
                Sobject record = records[j];
                for (String key : attributes.keySet()) {
                    // get the first value in attributes
                    Object value = attributes.get(key);
                    // is the instance a list of objects?
                    if (value instanceof List<Object>) {
                        object obj =  ((List<Object>) value).get(i);
                        // get the first item in the list and is if it is an SObject
                        // set foriegn keys in the objects being created.
                        if( obj instanceof Sobject ){
                            Id sObjectId = ((SObject) obj).Id;
                            record.put( key,sObjectId );
                        }
                        // It is a field and value to be added to the sobject. This will create a unique value
                        // for each item in the list of objects.  The inner list of objects is incremented on the
                        // outer loop so each record being created will have unique values.
                        else {
                            record.put(key,obj);
                        }
                    } else {
                    // set the fields values on the sobject
                        record.put(key, value);
                    }
                }
                i++;
            }
        }
        if (doInsert) {
            insert records;
        }
        return records;
    }
}