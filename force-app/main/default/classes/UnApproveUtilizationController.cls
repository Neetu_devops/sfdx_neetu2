public without sharing class UnApproveUtilizationController {
    
    @AuraEnabled
    public static String validateFunc(Id utilizationId){
        system.debug('validateFunc------'+ utilizationId);
        String message;
        if(!LeaseWareUtils.isAllowedToDeleteUnApproveUtil()) return 'You do not have a sufficient privileges to perform this operation';
        List<Utilization_Report__c>  listUtilizationForAsset =getLatestUtilizationForAsset(utilizationId);
         if(listUtilizationForAsset.size()>0){
            system.debug('listUtilizationForAsset---'+listUtilizationForAsset.size());
            Utilization_Report__c utilReport = listUtilizationForAsset.get(0);
            system.debug('utilReport----'+utilReport.Id + utilReport.Type__c);
            system.debug('utilizationId----'+utilizationId);
            if( utilReport.Id != utilizationId) return 'Only Latest Utilizations can be made unapproved';
            if(utilReport.Status__c !='Approved By Lessor')return 'Only approved utilizations can be made unapproved';
         }
        return 'UnApprove';
    }
    
    @AuraEnabled
    public static List<Utilization_Report__c> getLatestUtilizationForAsset(Id utilizationId){
        //Get the aircraft id in order to check if it is the latest  utilization 
        Utilization_Report__c utilzation = [select Id,Type__c,Month_Ending__c,Aircraft__c from Utilization_Report__c where Id=:utilizationId];
        
        AggregateResult maxMonth = [select max(Month_Ending__c) LatestMonth from  Utilization_Report__c 
                                    where aircraft__c = :utilzation.Aircraft__c and Type__c = 'Actual'
                                   ];
        List<Utilization_Report__c> latestUtilizationList =([select Id,Type__c,Status__c,Month_Ending__c,    
                                                             (select id, Constituent_Assembly__c, Constituent_Assembly__r.Type__c
                                                              from UR_List_Items__r) //Assembly Utilization
                                                             from Utilization_Report__c 
                                                             where Month_Ending__c =: ((Date)maxMonth.get('LatestMonth'))
                                                             and aircraft__c = :utilzation.Aircraft__c
                                                             and Type__c != 'Assumed/Estimated'
                                                             order by End_Date_F__c desc, CreatedDate desc]);
        
        return latestUtilizationList;
    }
    @AuraEnabled
    public static  String unapproveAction(Id utilizationId){
        String message;
       
        map<String,Decimal> mapAssemblyFHFC = new map<String,Decimal>();
        map<String,Decimal> mapAssemblyAPUFHFCs = new map<String,Decimal>();
        Map<Id, Decimal> AssemblyFHs = new Map<Id, Decimal>();
        Map<Id, Decimal> AssemblyFCs = new Map<Id, Decimal>();
        Map<Id, Decimal> AssemblyAPUFH = new Map<Id, Decimal>();
        Map<Id, Decimal> AssemblyAPUFC = new Map<Id, Decimal>();
        map<string,list<Utilization_Report__c>> mapURBasedonAC = new map<string,list<Utilization_Report__c>>();
        Map<string, string> mapAssyMissingFH = new Map<string, string>();//Key: Assy, value: Period ending string
        Map<string, string> mapAssyMissingFC = new Map<string, string>();//Key: Assy, value: Period ending string
        Map<string, string> mapRemoveAssyFromMissingFHFC = new Map<string, string>();//Key: Assy, value: Period ending string
        map<string,Decimal> mapLocalVariable = new map<string,Decimal>();
        Map<String,Utilization_Snapshot__c> mapUtilSnapshotByExactDate = new Map<String,Utilization_Snapshot__c>();
        Map<Id,Utilization_Snapshot__c> mapSnapshot = new Map<Id,Utilization_Snapshot__c>();
        Map<Id, Decimal> AssemblyDerate = new Map<Id, Decimal>();
        List<string> lstMissingAssetFH = new list<String>();
        List<string> lstMissingAssetFC = new list<String>();
        //The Utilization details of which we are unapproving
        Utilization_Report__c utilization = ([select Id ,Y_hidden_lease__c,Type__c, Aircraft__c,Y_Hidden_Running_Hours__c,Y_Hidden_Running_Cycles__c,Airframe_Flight_Hours_Month__c,Airframe_Cycles_Landing_During_Month__c
                                              ,Current_TSN__c,Current_CSN__c,Last_Actual_Monthly_Utilization__c,Unknown_FH_FC__c 
                                              ,(select id, Constituent_Assembly__c, Y_Hidden_Prev_Derate__c,Constituent_Assembly__r.Type__c, APU_Hours_At_Month_Start__c, APU_Cycles_At_Month_Start__c,
                                                Cycles_During_Month__c, Running_Hours_During_Month__c,Type_F__c,
                                                Y_Hidden_Running_Cycles__c,Y_Hidden_Running_Hours__c,                                                      
                                                TSN_At_Month_Start__c, CSN_At_Month_Start__c,
                                                APU_Hours__c,APU_Cycles__c,
                                                For_The_Month_Ending__c,Unknown_FH_FC__c
                                                from UR_List_Items__r) //Assembly Utilization
                                              ,Month_Ending__c,End_Date_F__c    
                                              from Utilization_Report__c 
                                              where Id =:utilizationId]);								  
		
		//Ref: Reconciliation Studio.
		for(Reconciliation_Schedule__c reconSchedule : [select id, Lease__c, From_date__c, To_date__c
                                                             from Reconciliation_Schedule__c
                                                             where lease__c =: utilization.Y_hidden_lease__c
															and (status__c = 'Complete' Or status__c = 'Reconciled')]){
                                                           
		   Date startDate = reconSchedule.From_date__c.toStartOfMonth();
		   Date endDate = reconSchedule.To_date__c.addMonths(1).toStartOfMonth().addDays(-1);
		   
		   if(startDate <= utilization.Month_Ending__c
			  && endDate >= utilization.Month_Ending__c){
				  String errorMgs = 'You cannot update this utilization as it is part of the ';
				  errorMgs += 'reconciliation done for the period ' + reconSchedule.From_date__c.month() + '/' + reconSchedule.From_date__c.year();
				  errorMgs += ' - ' + reconSchedule.To_date__c.month() + '/' + reconSchedule.To_date__c.year();
																				   
			   throw new AuraHandledException(errorMgs);
		   }
		   
	   }
											  
        //just confirm just this Utilization needs to be added or all the Ur for that asset
        list<Utilization_Report__c> tempURList;
         List<Utilization_Report__c> utilizationList =([select Id ,End_Date_F__c,Last_Actual_Monthly_Utilization__c,
                                Month_Ending__c    
                                from Utilization_Report__c  
                                where aircraft__c = :utilization.Aircraft__c 
                                And (Type__c = 'Actual' OR Type__c = 'True Up Gross')
                                And Status__c = 'Approved By Lessor'
                                and Id !=: utilizationId
                                order by Month_Ending__c desc, End_Date_F__c desc,CreatedDate desc
        ]);

        //Adding the UR to the Map which has to be sent to the existing function
        if(mapURBasedonAC.containsKey(utilization.aircraft__c))tempURList = mapURBasedonAC.get(utilization.aircraft__c);
        else tempURList = new list<Utilization_Report__c>();
        tempURList.add(utilization);
        mapURBasedonAC.put(utilization.aircraft__c,tempURList);
        
        mapLocalVariable.put(utilization.aircraft__c + 'FH',utilization.Airframe_Flight_Hours_Month__c);
        mapLocalVariable.put(utilization.aircraft__c + 'FC',utilization.Airframe_Cycles_Landing_During_Month__c);
        
        if(utilization.Unknown_FH_FC__c)
        {
            if(utilization.Airframe_Flight_Hours_Month__c == null) lstMissingAssetFH.add(utilization.Aircraft__c);
            if(utilization.Airframe_Cycles_Landing_During_Month__c == null) lstMissingAssetFC.add(utilization.Aircraft__c);
        }

        //  if(utilization.Last_Actual_Monthly_Utilization__c  == null){ 
        List<Assembly_Utilization__c> asmblyUtilizationList = utilization.UR_List_Items__r;
        for(Assembly_Utilization__c asmblyUtil: asmblyUtilizationList){
            AssemblyDerate.put(asmblyUtil.Constituent_Assembly__c,asmblyUtil.Y_Hidden_Prev_Derate__c);// adding prev derate value
            
            mapAssemblyFHFC.put(asmblyUtil.Constituent_Assembly__c+'FH', asmblyUtil.Running_Hours_During_Month__c );
            mapAssemblyFHFC.put(asmblyUtil.Constituent_Assembly__c+'FC',asmblyUtil.Cycles_During_Month__c);
            AssemblyFHs.put(asmblyUtil.Constituent_Assembly__c, asmblyUtil.Running_Hours_During_Month__c==null?0:-asmblyUtil.Running_Hours_During_Month__c);
            AssemblyFCs.put(asmblyUtil.Constituent_Assembly__c, asmblyUtil.Cycles_During_Month__c==null?0:-asmblyUtil.Cycles_During_Month__c);
            if(asmblyUtil.Constituent_Assembly__r.Type__c.contains('APU')){     
                decimal dAPUFH = asmblyUtil.APU_Hours__c == null? 0: -asmblyUtil.APU_Hours__c;
                decimal dAPUFC = asmblyUtil.APU_Cycles__c == null? 0: -asmblyUtil.APU_Cycles__c;
                AssemblyAPUFH.put(asmblyUtil.Constituent_Assembly__c, dAPUFH);
                AssemblyAPUFC.put(asmblyUtil.Constituent_Assembly__c, dAPUFC);
            }
            //Remove the period from assembly utilization status string
            //Check if the assembly has Unknown FH/FC
           //Remove the period from assembly utilization status string
           String strPeriod = LeaseWareUtils.getDatetoString(asmblyUtil.For_The_Month_Ending__c,'YYYY-MON-DD');
           mapRemoveAssyFromMissingFHFC.put(asmblyUtil.Constituent_Assembly__c, strPeriod);         
        }
        Date lastUREndDate ;
        if(utilization.Last_Actual_Monthly_Utilization__c  != null){
            Utilization_Report__c lastUR  = [select Id ,Y_Hidden_Running_Hours__c,Y_Hidden_Running_Cycles__c,Airframe_Flight_Hours_Month__c,Airframe_Cycles_Landing_During_Month__c
                                             ,Current_TSN__c,Current_CSN__c,Last_Actual_Monthly_Utilization__c,Unknown_FH_FC__c
                                             ,(select id, Constituent_Assembly__c, Constituent_Assembly__r.Type__c, APU_Hours_At_Month_Start__c, APU_Cycles_At_Month_Start__c,
                                               Cycles_During_Month__c, Running_Hours_During_Month__c,Type_F__c,
                                               Y_Hidden_Running_Cycles__c,Y_Hidden_Running_Hours__c,                                                      
                                               TSN_At_Month_Start__c, CSN_At_Month_Start__c,
                                               APU_Hours__c,APU_Cycles__c,
                                               For_The_Month_Ending__c,Unknown_FH_FC__c
                                               from UR_List_Items__r) //Assembly Utilization
                                             ,Month_Ending__c,End_Date_F__c    
                                             from Utilization_Report__c where id=:utilization.Last_Actual_Monthly_Utilization__c ];
            List<Assembly_Utilization__c> lastURAsmblyUtilizationList = lastUR.UR_List_Items__r;
            System.debug('lastUREndDate1--' + lastUR.End_Date_F__c);
            lastUREndDate = lastUR.End_Date_F__c;
            for(Assembly_Utilization__c lastURAsmblyUtil: lastURAsmblyUtilizationList){

                mapAssemblyFHFC.put(lastURAsmblyUtil.Constituent_Assembly__c+'LASTFH', lastURAsmblyUtil.Running_Hours_During_Month__c );
                mapAssemblyFHFC.put(lastURAsmblyUtil.Constituent_Assembly__c+'LASTFC',lastURAsmblyUtil.Cycles_During_Month__c);
               
                system.debug('AssemblyFHs.get(lastURAsmblyUtil.Constituent_Assembly__c)----'+AssemblyFHs.get(lastURAsmblyUtil.Constituent_Assembly__c));
                system.debug('AssemblyFCs.get(lastURAsmblyUtil.Constituent_Assembly__c)----'+AssemblyFCs.get(lastURAsmblyUtil.Constituent_Assembly__c));
               
                system.debug('lastURAsmblyUtil.Running_Hours_During_Month__c---'+lastURAsmblyUtil.Running_Hours_During_Month__c);
                system.debug('lastURAsmblyUtil.Cycles_During_Month__c---'+lastURAsmblyUtil.Cycles_During_Month__c);
                AssemblyFHs.put(lastURAsmblyUtil.Constituent_Assembly__c, AssemblyFHs.get(lastURAsmblyUtil.Constituent_Assembly__c)+Leasewareutils.ZeroIfNull(lastURAsmblyUtil.Running_Hours_During_Month__c));
                AssemblyFCs.put(lastURAsmblyUtil.Constituent_Assembly__c, AssemblyFCs.get(lastURAsmblyUtil.Constituent_Assembly__c)+Leasewareutils.ZeroIfNull(lastURAsmblyUtil.Cycles_During_Month__c));
                
                if(lastURAsmblyUtil.Constituent_Assembly__r.Type__c.contains('APU')){     
                    decimal dAPUFHtemp = AssemblyAPUFH.get(lastURAsmblyUtil.Constituent_Assembly__c) + Leasewareutils.ZeroIfNull(lastURAsmblyUtil.APU_Hours__c);
                    decimal dAPUFCtemp = AssemblyAPUFC.get(lastURAsmblyUtil.Constituent_Assembly__c) + Leasewareutils.ZeroIfNull(lastURAsmblyUtil.APU_Cycles__c);
                    AssemblyAPUFH.put(lastURAsmblyUtil.Constituent_Assembly__c,dAPUFHtemp );
                    AssemblyAPUFC.put(lastURAsmblyUtil.Constituent_Assembly__c, dAPUFCtemp);
                    mapAssemblyAPUFHFCs.put(lastURAsmblyUtil.Constituent_Assembly__c+'LASTFH',lastURAsmblyUtil.APU_Hours__c);
                    mapAssemblyAPUFHFCs.put(lastURAsmblyUtil.Constituent_Assembly__c+'LASTFC',lastURAsmblyUtil.APU_Cycles__c);
                }     
            }
        }
        
        String invoiceUpdateStatus =  URTriggerHandler.delinkInvoiceFromUtilization(null,utilization);
        if(invoiceUpdateStatus !='SUCCESS') return invoiceUpdateStatus;
        //Calling the existing method to update the chnages
        if(utilizationList.size()>0){  // if end date is not present in case of true up , take the last utilization end date. 
            lastUREndDate =(lastUREndDate == null) ? utilizationList[0].End_Date_F__c : lastUREndDate; 
        }
        System.debug('lastUREndDate1--' + lastUREndDate);
        fetchUtilizationSnapshot2(new Set<Id>{utilization.Aircraft__c}, mapUtilSnapshotByExactDate, mapSnapshot);
        URTriggerHandler.updateAssetRelatedRecord(AssemblyFHs,AssemblyFCs,mapLocalVariable,mapRemoveAssyFromMissingFHFC,AssemblyAPUFH,AssemblyAPUFC
                                                  ,mapAssemblyFHFC,mapAssemblyAPUFHFCs,mapAssyMissingFH
                                                  ,mapAssyMissingFC
                                                  ,AssemblyDerate,mapURBasedonAC, utilization.Month_Ending__c,lstMissingAssetFH,lstMissingAssetFC,lastUREndDate,null,
                                                  mapUtilSnapshotByExactDate, mapSnapshot,'UnapproveUR');
        
        LeaseWareUtils.TriggerDisabledFlag=true; 
        //Making the Status to Open once all the changes are reverted
        utilization.Status__c ='Open';
        utilization.Invoice__c = null; //removing the link to invoice look up
        update utilization;
        LeaseWareUtils.TriggerDisabledFlag=false;
        
        
        //Ref : Reconciliation Studio
        // To update Latest approved Actual on Reconciliation Studio.
        if(utilization.Type__c == 'Actual'){
        
            Map<Id, Set<Date>> leaseWithMonthEndingMap = new Map<Id, Set<Date>> ();
            leaseWithMonthEndingMap.put(utilization.y_hidden_lease__c, new Set<Date>{utilization.Month_Ending__c});
            URTriggerHandler.updateLastActualUROnReconSchedule(leaseWithMonthEndingMap);
        }
        
        message='SUCCESS';
        return message;
    }
    
    // have to add this static method to call from static context 
private static void fetchUtilizationSnapshot2(Set<id> assetIds ,map<String,Utilization_Snapshot__c> mapUtilSnapshotByExactDate, map<Id,Utilization_Snapshot__c> mapSnapshot){
    system.debug('fetchUtilizationSnapshot2 for asset ----'+ assetIds);
    List<Utilization_Snapshot__c> utilSnapshotList = [select Id,Name, Asset__c,Snapshot_Date__c,Snapshot_Status_F__c,Utilization__c,Utilization_true_up__c,TSN_as_of_Date__c,CSN_as_of_Date__c,
                                                      (select Id,Name,Snapshot_Date__c,Snapshot_Status__c,Utilization__C,Utilization_true_up__c,
                                                      Assembly_Lkp__c,Assembly_Type__c,TSN_as_of_Date__c ,CSN_as_of_Date__c 
                                                      from Assembly_Utilization_Snapshots_New__r)
                                                      from Utilization_Snapshot__c  where Asset__c IN : assetIds];
   System.debug('utilSnapshotList----' + utilSnapshotList.size() +'----'+ utilSnapshotList);
    if(utilSnapshotList!=null && utilSnapshotList.size()>0){
        for(Utilization_Snapshot__c snapshot: utilSnapshotList){
            mapSnapshot.put(snapshot.Id,snapshot);         
            String keyString2= snapshot.Asset__c +'_'+snapshot.Snapshot_Date__c;
            System.debug('keyString2 = ' + keyString2);           
            mapUtilSnapshotByExactDate.put(keyString2,snapshot);
            System.debug('mapUtilSnapshotByExactDate-----' + mapUtilSnapshotByExactDate.size() +'----'+ mapUtilSnapshotByExactDate);
        }
    }
}


}