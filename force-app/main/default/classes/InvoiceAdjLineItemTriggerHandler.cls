public class InvoiceAdjLineItemTriggerHandler implements ITrigger{
    
    private final string triggerBefore = 'InvoiceAdjLineItemTriggerHandlerBefore';
    private final string triggerAfter = 'InvoiceAdjLineItemTriggerHandlerAfter';
    private static map<id, Invoice_Adjustment__c> mapInvAdj_BefIns_BefUpd;
    
    
    public InvoiceAdjLineItemTriggerHandler()
    {
    }
    
    public void bulkBefore()
    {
        system.debug('InvoiceAdjLineItemTriggerHandler.bulkBefore(+)');
        if(trigger.isInsert || trigger.isUpdate)
        {
            set<Id> setInvAdj = new set<Id>();
            for(Invoice_Adjustment_Line_Item__c newInvAdjLI : (List<Invoice_Adjustment_Line_Item__c>)Trigger.new)
            {
                setInvAdj.add(newInvAdjLI.Invoice_Adjustment__c);
            }

            mapInvAdj_BefIns_BefUpd = new map<id, Invoice_Adjustment__c>
                                                ([select id, Name, Approval_Status__c from Invoice_Adjustment__c where id in :setInvAdj]);
        }
        system.debug('InvoiceAdjLineItemTriggerHandler.bulkBefore(-)');
    }		
	
    public void bulkAfter()
    {
    }
    
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('InvoiceAdjLineItemTriggerHandler.beforeInsert(+)');

        setNameForAdjLI();
        setApprovalStatus();
        
        system.debug('InvoiceAdjLineItemTriggerHandler.beforeInsert(-)');     
    }
    
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InvoiceAdjLineItemTriggerHandler.beforeUpdate(+)');
        
        validateRecordChanges();        
		setApprovalStatus();
        
        system.debug('InvoiceAdjLineItemTriggerHandler.beforeUpdate(-)');
    }
    
    public void beforeDelete()
    {          
        system.debug('InvoiceAdjLineItemTriggerHandler.beforeDelete(+)');

        //This would be executed only when user tries to manually delete the adjustment line item
        //On deletion of adjustment, cascade delete of line items happens and adj line item trigger would not be invoked
        for(Invoice_Adjustment_Line_Item__c tempAdjLI: (List<Invoice_Adjustment_Line_Item__c>)trigger.old){
            tempAdjLI.addError('Invoice Adjustment Line Items cannot be manually modified or deleted');
        }
        
        system.debug('InvoiceAdjLineItemTriggerHandler.beforeDelete(-)');         
        
    }
    
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InvoiceAdjLineItemTriggerHandler.afterInsert(+)');
        
        updateTransactions();
        
        system.debug('InvoiceAdjLineItemTriggerHandler.afterInsert(-)');      
    }
    
    public void afterUpdate()
    {
        
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InvoiceAdjLineItemTriggerHandler.afterUpdate(+)');
        
        updateTransactions();
        
        system.debug('InvoiceAdjLineItemTriggerHandler.afterUpdate(-)');      
    }
    
    public void afterDelete()
    {
        system.debug('InvoiceAdjLineItemTriggerHandler.afterDelete(+)');
        
        system.debug('InvoiceAdjLineItemTriggerHandler.afterDelete(-)');      
    }
    
    public void afterUnDelete()
    {
        system.debug('InvoiceAdjLineItemTriggerHandler.afterUnDelete(+)');
        
        system.debug('InvoiceAdjLineItemTriggerHandler.afterUnDelete(-)');        
    }
    
    public void andFinally()
    {
    }

    //This method sets the name for the Adjustment Line Item in the format ADJ 1 + (INVOICE LINEITEM NAME)
    //before insert
    private void setNameForAdjLI()
    {   
        System.debug('InvoiceAdjLineItemTriggerHandler::setNameForAdjLI(+)');

        set<Id> setInvLI = new set<Id>();
        for(Invoice_Adjustment_Line_Item__c newInvAdjLI : (List<Invoice_Adjustment_Line_Item__c>)Trigger.new)
        {
            setInvLI.add(newInvAdjLI.Linked_Invoice_Line_Item__c);
        }

        map<id, Invoice_line_item__c> mapInvLI = new map<id, Invoice_line_item__c>([select id,Name from Invoice_Line_Item__c where id in :setInvLI]);

        for(Invoice_Adjustment_Line_Item__c newInvAdjLI : (List<Invoice_Adjustment_Line_Item__c>)Trigger.new){
            String[] AdjName = mapInvAdj_BefIns_BefUpd.get(newInvAdjLI.Invoice_Adjustment__c).Name.split(' ');
            String adjLIName ='';
            if(AdjName != null && AdjName.size() > 1){
                adjLIName = AdjName[0] + ' ' + Integer.ValueOf(AdjName[1]) + ' ' + mapInvLI.get(newInvAdjLI.Linked_Invoice_Line_Item__c).Name;
            }
            else{
                adjLIName = 'ADJ' + ' ' + mapInvLI.get(newInvAdjLI.Linked_Invoice_Line_Item__c).Name;
            }
            
            if(adjLIName.length() > 79){
                newInvAdjLI.Name = adjLIName.Left(76) + '...';
            }
            else{
                newInvAdjLI.Name = adjLIName;
            }

            System.debug('Setting name as ' + newInvAdjLI.Name);
        }

        System.debug('InvoiceAdjLineItemTriggerHandler::setNameForAdjLI(-)');
    }
    
    //This method would set the approval status of the parent adjustment to the adjustment line item    
    //NOTE: This cannot be a formula field but has to be done in the trigger to be able to use it
    //in the filter criteria for roll-up summary fields
    //before insert, before update
    private void setApprovalStatus()
    {   
        System.debug('InvoiceAdjLineItemTriggerHandler::setApprovalStatus(+)');
        
        for(Invoice_Adjustment_Line_Item__c newAdjLI : (List<Invoice_Adjustment_Line_Item__c>)Trigger.new){
            newAdjLI.Invoice_Adjustment_Status__c = mapInvAdj_BefIns_BefUpd.get(newAdjLI.Invoice_Adjustment__c).Approval_Status__c;
            System.debug('Setting status for' + newAdjLI.Name + ' to ' + newAdjLI.Invoice_Adjustment_Status__c);
        } 
        
        System.debug('InvoiceAdjLineItemTriggerHandler::setApprovalStatus(-)');
    } 

    //This method restricts modifications on approved/declined adjustments except 'Comments'
    //before update
    public void validateRecordChanges()
    {
        System.debug('InvoiceAdjLineItemTriggerHandler::validateRecordChanges(+)');

        Map<String, Schema.SObjectField> mapInvAdjLIField = Schema.SObjectType.Invoice_Adjustment_Line_Item__c.fields.getMap();
        list<String> listFieldsToCheck = new list<String>();
        set<string> setSkipFields = RLUtility.getAllFieldsFromFieldset('Invoice_Adjustment_Line_Item__c','SkipFieldsFromValidations');


        for(String Field:mapInvAdjLIField.keyset())
        {
            Schema.DescribeFieldResult thisFieldDesc = mapInvAdjLIField.get(Field).getDescribe();
            System.debug('Field Name is ' + thisFieldDesc.getName() + '. Local ' + thisFieldDesc.getLocalName());

            if(!thisFieldDesc.isUpdateable() || setSkipFields.contains(leasewareutils.getNamespacePrefix() + thisFieldDesc.getLocalName())) continue; //Add the fields that are always updatable to the list.
                listFieldsToCheck.add(thisFieldDesc.getLocalName());
        }

        for(Invoice_Adjustment_Line_Item__c newInvAdjLI : (List<Invoice_Adjustment_Line_Item__c>)Trigger.new)
        {  
           String status = mapInvAdj_BefIns_BefUpd.get(newInvAdjLI.Invoice_Adjustment__c).Approval_Status__c;

           //Check if any field is changed on 'Approved'/'Declined' adjustments
           Invoice_Adjustment_line_Item__c oldInvAdjLI = (Invoice_Adjustment_line_Item__c)Trigger.oldMap.get(newInvAdjLI.id);
           for(String curField:listFieldsToCheck){
                if(newInvAdjLI.get(curField) !=  oldInvAdjLI.get(curField)){   
                    Schema.DescribeFieldResult thisFieldDesc = mapInvAdjLIField.get(leasewareutils.getNamespacePrefix().toLowerCase() + curField).getDescribe();
                    System.debug(thisFieldDesc.getLabel() + ' has changed from' + oldInvAdjLI.get(curField) + ' to ' + newInvAdjLI.get(curField));                    
                    
                    //Adjustment line amount can be modified only from the 'Invoice Adjustments' UI
                    if(curField == 'Line_Adjustment_Amount__c' && !LeaseWareUtils.isFromTrigger('INVOICE_ADJ_LI_UPDATE'))   {
                        newInvAdjLI.addError('Adjustment line amount cannot be manually modified. Please utilize �Invoice Adjustments� functionality if you wish to update the amount.');                        
                        continue;
                    } 

                    if(!'Pending'.equals(newInvAdjLI.Invoice_Adjustment_Status__c) && ('Approved'.equals(status) || 'Declined'.equals(status))){
                        newInvAdjLI.addError('Approved/Declined Invoice Adjustment records cannot be modified');                        
                        continue;
                    } 
                }
            }//end for fields check   
        }

        System.debug('InvoiceAdjLineItemTriggerHandler::validateRecordChanges(-)');
    }

    // This will  create , update or delete the transaction records for the invoice adjustment line items
    //after insert, after update
    private void updateTransactions()
    {
        System.debug('InvoiceAdjLineItemTriggerHandler::updateTransactions(+)');

        set<id> setInvLI = new set<Id>();
        Set<Id> setLeaseId = new Set<Id>();
        boolean isStatusApproved = false; 
        for(Invoice_Adjustment_Line_Item__c newInvAdjLI :  (List<Invoice_Adjustment_Line_Item__c>)Trigger.new){
            setInvLI.add(newInvAdjLI.Linked_Invoice_Line_Item__c);
            if('Approved'.equals(newInvAdjLI.Invoice_Adjustment_Status__c)){
                isStatusApproved = true;
            }
        }

        for(Invoice_Line_Item__c tempInvLI:[select id, Assembly_MR_Info__c,Invoice__r.Lease__c from Invoice_Line_Item__c where id in :setInvLI]){
            setLeaseId.add(tempInvLI.Invoice__r.Lease__c); 
        }
        
        if(LeaseWareUtils.isFromTrigger('MR_INVOICE_ADJ_UPDATE') || LeaseWareUtils.isFromTrigger('INVOICE_ADJ_LI_UPDATE')) {
              /*  If status of Invoice Adjustment is Pending or Declined, we dont update anything on SR from here
               so we need to call the transaction batch explicitly to create / update transactions 
               If status of Invoice Adjustment is Approved , there is a Roll up summary field on Invoice Line Item, which gets updated 
               in method InvoiceLineItemHandler.UpdateTotalOnInvoice() and it calls update on AssemblyMRRateTriggerHandler , which calls the transaction batch 
               so we stop the transaction batch call from here.
           */ 

           if(!System.isFuture() && !System.isBatch() && setLeaseId.size()>0 && !isStatusApproved){
                // This is to prevent Transaction batch getting called again from SR trigger in status Pending / Declined
                LeaseWareUtils.setFromTrigger('MR_SR_TRANSACTION');  
                System.debug('Creating transactions from InvLIneAdjst trigger :');
                Database.executeBatch(new SupplementalRentTransactionsBatch(setLeaseId,false),SupplementalRentTransactionsBatch.getBatchSize());            
            }
            }     
        LeaseWareUtils.unsetTriggers('MR_SR_TRANSACTION');
        System.debug('InvoiceAdjLineItemTriggerHandler::updateTransactions(-)');
    }  
}