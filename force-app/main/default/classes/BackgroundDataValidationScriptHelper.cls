public class BackgroundDataValidationScriptHelper {

   
    //Methods to check if there is any record type mismatch on the invoices
    public list<String> invoiceRecordTypeMismatchStart(map<string,string> mapParam){ 
        
        String qString = 'Select id, name, recordType.DeveloperName, invoice_type__c '
                +' from Invoice__c ';
        return new list<String>{qString};  
    }
    public string invoiceRecordTypeMismatchExecute(list<Invoice__c> listInvoice, map<string,string> mapParam){

        list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
        string parentId = mapParam.get('PARENT_ID');
        string processReqId = mapParam.get('PROCESS_REQ_ID'); 
                     

        string retString = 'Checking invoice record type ---- ';  

        map<string, set<string>> mapRecTypeToInvType  = new map<string, set<string>>
        {
            'Security_Deposit' => new set<string>{'Security Deposit'},
            'Rent' => new set<string>{'Rent'},
            'Other' => new set<string>{'Other','Manual'},
            'Monthly_Utilization' => new set<string>{'Aircraft MR'},
            'Manual_MR_Invoice' => new set<string>{'Aircraft MR'},
            'MR_Reconciliation' => new set<string>{'Aircraft MR','Non MR'},
            'Credit_Note_Rent' => new set<string>{'Rent'}, // old records
            'Credit_Note_MR' => new set<string>{'Aircraft MR'}, // old records
            'Interest' => new set<string>{'Interest'}           
        };

        for(Invoice__c inv : listInvoice){
            boolean hasError = false;
            
            //Credit_Note,Manual_MR_Credit_Note are not in use since Sep'20 release. Hence ignoring those records.
            if(inv.RecordType.DeveloperName == 'Credit_Note' || inv.RecordType.DeveloperName == 'Manual_MR_Credit_Note'){
                continue;
            }
            
            if(String.IsBlank(inv.RecordType.DeveloperName)){
                hasError = true;
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name= 'Blank Record Type' + inv.Name,
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c=inv.Name,
                    Issue_Record_Id__c=inv.id,
                    Comment__c = 'Record Type is Blank'
                    ));
            }
            else if(!mapRecTypeToInvType.containsKey(inv.RecordType.DeveloperName)){
                hasError = true;
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name='MismatchRecorType-'+inv.Name,
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c=inv.Name,
                    Issue_Record_Id__c=inv.id,
                    Comment__c = 'Record Type Mismatch : RecordType Not found in documented list ' + inv.RecordType.DeveloperName
                    ));                
            }
            else if(!mapRecTypeToInvType.get(inv.RecordType.DeveloperName).contains(inv.Invoice_Type__c)){
                hasError = true;
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name='MismatchRecorType-'+inv.Name,
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c=inv.Name,
                    Issue_Record_Id__c=inv.id,
                    Comment__c = 'Record Type Mismatch : Invoice Type and record type is not matching'
                    ));                
            }
            if(hasError){
                retString += '\n' + 'Invoice type does not match record type for - ' + inv.id + '--RecordType= ' + inv.recordType.DeveloperName + '--Invoice Type= ' + inv.Invoice_Type__c;
                mapParam.put('ERROR','YES');
            }
        }

        if(issueRecordList.size()>0) {insert issueRecordList;}
        retString += '\n' + 'End of Invoice Type Check ---- ';

        return retString;
    }

    //Method to check if there is any mismatch between the Invoice Header Amount and sum of all Invoice Line Item amounts (if any)
    public list<String> invoiceAmtMismatchWithLineItemsAmtStart(map<string,string> mapParam){ 
        
        String qString = 'Select id, name, Invoiced_Amount_Formula__c, (select id, name, Total_Line_Amount__c from Invoice_line_items__r) from Invoice__c where Status__c not in (\'Cancelled\',\'Declined\')';
        return new list<String>{qString};  
    }
	public string invoiceAmtMismatchWithLineItemsAmtExecute(list<Invoice__c> listInvoice, map<string,string> mapParam){
        list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
        string parentId = mapParam.get('PARENT_ID');
        string processReqId = mapParam.get('PROCESS_REQ_ID');         
        string retString = 'Checking invoice header amount mismatch with the sum of line item amounts ---- ';  

        for(Invoice__c curInv: listInvoice){
            retString += '\n\n' + 'Checking for Invoice::' + curInv.id + ' Amount::' + Leasewareutils.zeroIfNull(curInv.Invoiced_Amount_Formula__c) + '\n';
            boolean hasError = false;
            Decimal sumLIAmount = 0;   
            
            for(Invoice_Line_Item__c curInvLI: curInv.invoice_line_items__r)
            {   
                retString += '\n' + 'Inv LI::' + curInvLI.id + ' Amount::' + LeasewareUtils.zeroIfNull(curInvLI.Total_Line_Amount__c);                
                sumLIAmount += LeasewareUtils.zeroIfNull(curInvLI.Total_Line_Amount__c);
            }
            //Checking for a difference more than 10 cents there by ignoring cases that could have been modified manually to address the rounding issues
            if(Math.abs(sumLIAmount - Leasewareutils.zeroIfNull(curInv.Invoiced_Amount_Formula__c)) > 0.1) 
            {
                hasError = true;
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name= 'HeaderAmountMismatch--' + curInv.Name,
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c=curInv.Name,
                    Issue_Record_Id__c=curInv.id,
                    Comment__c = 'Invoiced Amount is not equal to the sum of the Invoice Line Items.' + ' Expected= ' + Leasewareutils.zeroIfNull(curInv.Invoiced_Amount_Formula__c) + '; Actual= ' + sumLIAmount
                    ));                
            }
            if(hasError){
                retString += '\n' + 'Invoiced Amount is not equal to the sum of the Invoice Line Items   - ' + curInv.id + '--Expected= ' + Leasewareutils.zeroIfNull(curInv.Invoiced_Amount_Formula__c) + '--Actual= ' + sumLIAmount;
                mapParam.put('ERROR','YES');
            }
        }
        
        if(issueRecordList.size()>0) {insert issueRecordList;}
        retString += '\n' + 'End of Invoice header amount mismatch Check ---- ';

        return retString;
    }

     //Methods to return  MR invoice line item if the SR lookup is null or SR utilization lookup null
     public list<String> mrInvoiceLISRSRUtlznNullStart(map<string,string> mapParam){ 
        String qString = 'Select id, name,invoice__r.recordType.DeveloperName,Assembly_MR_Info__c,Assembly_Utilization__c,invoice__r.Utilization_Report__c ' 
                        +' from invoice_line_item__c where (Assembly_MR_Info__c = null or Assembly_Utilization__c = null )and Invoice_Type__c = \'Aircraft MR\' and Invoice_Status__c NOT IN (\'Cancelled\',\'Declined\')';
        return new list<String>{qString};  
    }
    //Inserts the record with MR invoice line item if the SR lookup  null or SR utilization lookup null or both
    public string mrInvoiceLISRSRUtlznNullExecute(list<invoice_line_item__c> listInvoiceLI, map<string,string> mapParam){

        list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
        string parentId = mapParam.get('PARENT_ID');
        string processReqId = mapParam.get('PROCESS_REQ_ID'); 
        string retString =  'Start of MR Invoice LI, SR lookup check----\n\n' ;         
        
        retString = 'Size of Error record:: '+listInvoiceLI.size()+'\n\n';  
        for(invoice_line_item__c invLI : listInvoiceLI){
                if(invLI.invoice__r.RecordType.DeveloperName == 'Monthly_Utilization' && invLI.Assembly_Utilization__c == null && invLI.Assembly_MR_Info__c == null  ){
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= ('Invoice Line Item SR and SR Utilization lookup null-' + invLI.Name).left(80),
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=invLI.Name,
                        Issue_Record_Id__c=invLI.id,
                        Comment__c = 'Invoice Line Item '+invLI.name+' does not have a link to the Supplemental Rent and Supplemental Rent Utilization'
                        ));
                        retString += 'Invoice line Item with out SR and SR utilization look up::'+invLi.id+'\n\n';
                }
                else if(invLI.Assembly_MR_Info__c == null){
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= ('Invoice Line Item SR lookup null-' + invLI.Name).left(80),
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=invLI.Name,
                        Issue_Record_Id__c=invLI.id,
                        Comment__c = 'Invoice Line Item '+invLI.name+' does not have a link to the Supplemental Rent'
                        ));

                        retString += 'Invoice line Item with out SR look up::'+invLi.id+'\n\n';
                }
                //adding the condition for utilization as well, when an utilization is unapproved the associated invoice LI SR utilization is also removed
                else if(invLI.invoice__r.RecordType.DeveloperName == 'Monthly_Utilization' && invLI.Assembly_Utilization__c == null && invLI.invoice__r.Utilization_Report__c != null ){
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= ('Invoice Line Item SR utilization lookup null-' + invLI.Name).left(80),
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=invLI.Name,
                        Issue_Record_Id__c=invLI.id,
                        Comment__c = 'Invoice Line Item '+invLI.name+' does not have a link to the Supplemental Rent utilization'
                        ));

                        retString += 'Invoice line Item with out SR utilization look up::'+invLI.id+'\n\n';
                }
        }
        
        if(issueRecordList.size()>0) {
            mapParam.put('ERROR','YES');
            insert issueRecordList;
        }
        retString += '\n' + 'End of invoice line item SR lookup check ---- ';

        return retString;
    }

    //Methods to return all the payment studio records except PrePayments
    public list<String> pyAmtPaidAmtAllocatedMismatchStart(map<string,string> mapParam){ 
        String qString = 'select id,name,Amount_Paid__c,Payment_Amount_Cur__c from Payment_Receipt__c where Prepayment__c = false and Available_Balance_Cur__c!=0';
        return new list<String>{qString};  
    }
    //Inserts the record when Payment amount and Allocated amount is having mismatch 
    public string pyAmtPaidAmtAllocatedMismatchExecute(list<Payment_Receipt__c> listPyStds, map<string,string> mapParam){

        list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
        string parentId = mapParam.get('PARENT_ID');
        string processReqId = mapParam.get('PROCESS_REQ_ID'); 
        string retString =  'Start of Payment Studio Amount check----\n\n' ;         
        
        retString = 'Size of records list:: '+listPyStds.size()+'\n\n';  
        for(Payment_Receipt__c pyStd : listPyStds){
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name= ('Payment Amount-Amount Paid Mismatch -' + pyStd.Name).left(80),
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c=pyStd.Name,
                    Issue_Record_Id__c=pyStd.id,
                    Comment__c = 'Payment Amount is not equal to the sum of Amount Paid of the Invoices Allocated. Expected = '+pyStd.Amount_Paid__c+' Actual = '+pyStd.Payment_Amount_Cur__c
                    ));
                    retString += 'Error payment studio record::' +pyStd.id+'\n\n';
        }
        if(issueRecordList.size()>0) {
            mapParam.put('ERROR','YES');
            insert issueRecordList;
        }
        retString += '\n' + 'End of Payment Studio Amount check ---- ';

        return retString;
    }

     //Methods to return  MR payment line item if the SR lookup is null or SR utilization lookup null
    public list<String> mrPaymentLISRSRUtlznNullStart(map<string,string> mapParam){ 
        String qString = 'Select id, name,payment__r.invoice__r.recordType.DeveloperName,Assembly_MR_Info__c,Assembly_Utilization__c,payment__r.invoice__r.Utilization_Report__c '+
                        '  from  payment_line_item__c where (Assembly_MR_Info__c = null or Assembly_Utilization__c = null )and Invoice_Type__c = \'Aircraft MR\' and Status__c NOT IN(\'Cancelled\')';
        return new list<String>{qString}; 
    } 
    //Inserts the record with MR payment line item if the SR lookup  null or SR utilization lookup null or both
    public string mrPaymentLISRSRUtlznNullExecute(list<payment_line_item__c> listPaymentLI, map<string,string> mapParam){

        list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
        string parentId = mapParam.get('PARENT_ID');
        string processReqId = mapParam.get('PROCESS_REQ_ID'); 
        string retString =  'Start of MR payment LI, SR lookup check----\n\n' ;         
        
        retString = 'Size of Error record:: '+listPaymentLI.size()+'\n\n';  
        for(payment_line_item__c pyLI : listPaymentLI){
                if(pyLI.payment__r.invoice__r.RecordType.DeveloperName == 'Monthly_Utilization' && pyLI.Assembly_Utilization__c == null && pyLI.Assembly_MR_Info__c == null  ){
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= ('Payment Line Item SR and SR Utilization lookup null-' + pyLI.Name).left(80),
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=pyLI.Name,
                        Issue_Record_Id__c=pyLI.id,
                        Comment__c = 'Payment Line Item '+pyLI.name+' does not have a link to the Supplemental Rent and Supplemental Rent Utilization'
                        ));
                        retString += 'Payment line Item without SR and SR utilization look up::'+pyLI.id+'\n\n';
                }
                else if(pyLI.Assembly_MR_Info__c == null){
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= ('Payment Line Item SR lookup null-' + pyLI.Name).left(80),
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=pyLI.Name,
                        Issue_Record_Id__c=pyLI.id,
                        Comment__c = 'Payment Line Item '+pyLI.name+' does not have a link to the Supplemental Rent'
                        ));

                        retString += 'Payment line Item without SR look up::'+pyLI.id+'\n\n';
                }
                //supplemental rent utilization is required only if utilization is available.
                else if(pyLI.payment__r.invoice__r.RecordType.DeveloperName == 'Monthly_Utilization' && pyLI.Assembly_Utilization__c == null && pyLI.payment__r.invoice__r.Utilization_Report__c!=null){
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= ('Payment Line Item SR utilization lookup null-' + pyLI.Name).left(80),
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=pyLI.Name,
                        Issue_Record_Id__c=pyLI.id,
                        Comment__c = 'Payment Line Item '+pyLI.name+' does not have a link to the Supplemental Rent utilization'
                        ));
                        retString += 'Payment line Item without SR utilization look up::'+pyLI.id+'\n\n';
                } 
        }
        
        if(issueRecordList.size()>0) {
            mapParam.put('ERROR','YES');
            insert issueRecordList;
        }
        retString += '\n' + 'End of payment line item SR lookup check ---- ';

        return retString;
    }
    
    //Methods to check if there is any record type and assembly type mismatch on the assembly
    public list<String> assemblyRecordTypeMismatchStart(map<string,string> mapParam){ 
        
        String qString = 'Select id, name, recordType.DeveloperName, recordType.Name ,type__c  from Constituent_Assembly__c ';
        return new list<String>{qString};  
    }
    //Method inserts records if record type is blank or has a mismatch 
    public string assemblyRecordTypeMismatchExecute(list<Constituent_Assembly__c> listAssembly, map<string,string> mapParam){

        list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
        string parentId = mapParam.get('PARENT_ID');
        string processReqId = mapParam.get('PROCESS_REQ_ID'); 
        string retString = 'Checking Assembly record type ---- ';  

        map<string, string> mapRecTypeToAssemblyType  = new map<string, string>
        {
            'Airframe' => 'Airframe',
            'APU' => 'APU',
            'Engine' => 'Engine',
            'Landing_Gear' => 'Landing',
            'Propeller' =>'Propeller' 
        };

        for(Constituent_Assembly__c assembly : listAssembly){
            
            if(String.IsBlank(assembly.RecordType.DeveloperName)){
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name= 'Blank Record Type' + assembly.Name,
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c=assembly.Name,
                    Issue_Record_Id__c=assembly.id,
                    Comment__c = 'Record Type is Blank'
                    ));
                    retString += '\n' + 'Record type is blank for - ' + assembly.id ;
            }
            //we are not checking for the Other type and thrust record type
            else if(mapRecTypeToAssemblyType.get(assembly.RecordType.DeveloperName) !=null && !assembly.Type__c?.startsWithIgnoreCase(mapRecTypeToAssemblyType.get(assembly.RecordType.DeveloperName))){
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name='Mismatch RecordType-'+assembly.Name,
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c=assembly.Name,
                    Issue_Record_Id__c=assembly.id,
                    Comment__c = 'Assembly Type and Record Type do not match.Assembly Type is '+ assembly.Type__c + ' and the Record Type is '+ assembly.RecordType.Name
                    ));  
                    retString += '\n' + 'Assembly type does not match record type for - ' + assembly.id + '--RecordType= ' + assembly.recordType.DeveloperName + '--Assembly Type= ' + assembly.Type__c;              
            }
        }
        if(issueRecordList.size()>0) {
            mapParam.put('ERROR','YES');
            insert issueRecordList;
        }
        retString += '\n' + 'End of Assembly Type Check ---- ';

        return retString;
    }
  
    //Methods to check if there is any Supplemental Rent Balance and Running Balance mismatch
    public list<String> supplementRentBalMismatchStart(map<string,string> mapParam){ 
        
        String qString = 'Select Id,Name, Transactions_Total_Invoiced__c,Transactions_Total_Cash__c,'+
                          +'(Select Id,Name,Running_Balance_Cash_Collected__c,Running_Balance_Invoiced__c,Sequence_Number__c'+
                          +' from Supplemental_Rent_Transactions__r order by Sequence_Number__c DESC  limit 1 ) from Assembly_MR_Rate__c ';
        return new list<String>{qString};  
    }
    //Method inserts records if Supplemental Rent Balance and Running Balance has a mismatch 
    public string supplementRentBalMismatchExecute(list<Assembly_MR_Rate__c> listSR, map<string,string> mapParam){

        list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
        string parentId = mapParam.get('PARENT_ID');
        string processReqId = mapParam.get('PROCESS_REQ_ID'); 
        string retString = 'Checking Supplemental Rent Balance and Running Balance mismatch ---- ';  

        for(Assembly_MR_Rate__c suppRent: listSR){
            retString += '\n\n' + 'Checking for Supplement Rent::' + suppRent.id + ' Transactions Total (Invoiced)::' + Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Invoiced__c) + '\n';
            retString += '\n\n' + 'Checking for Supplement Rent::' + suppRent.id + ' Transactions Total (Cash Collected)::' + Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Cash__c) + '\n';
            boolean hasErrorInvoiced = false;
            boolean hasErrorCash = false;
            decimal runningBalInvoicd ;
            decimal runningBalCash ;
            
            for(Supplemental_Rent_Transactions__c suppRentTrans: suppRent.Supplemental_Rent_Transactions__r)
            {   
                retString += '\n\n' + 'Supplemental Rent Transc::' + suppRentTrans.id + ' Running Balance (Invoiced)::' + LeasewareUtils.zeroIfNull(suppRentTrans.Running_Balance_Invoiced__c)+ '\n';                
                runningBalInvoicd = LeasewareUtils.zeroIfNull(suppRentTrans.Running_Balance_Invoiced__c);
                retString += '\n\n' + 'Supplemental Rent Transc::' + suppRentTrans.id + ' Running Balance (Cash Collected)::' + LeasewareUtils.zeroIfNull(suppRentTrans.Running_Balance_Cash_Collected__c)+ '\n';                
                runningBalCash = LeasewareUtils.zeroIfNull(suppRentTrans.Running_Balance_Cash_Collected__c);
            }
            //Checking for TransactionsTotal(Invoiced) not equal to the latest RunningBalance(Invoiced) record 
            if( Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Invoiced__c) != Leasewareutils.zeroIfNull(runningBalInvoicd))
            {
                    hasErrorInvoiced = true;
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= 'SR_BalanceMismatch_InvoicedTransactions--',
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=suppRent.Name,
                        Issue_Record_Id__c=suppRent.id,
                        Comment__c = 'Supplemental Rent '+suppRent.Name+' mismatch in running balance and sum of transactions.'+ 
                                    +'Transactions Total (Invoiced): Expected= ' + Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Invoiced__c) + '; Running Balance (Invoiced): Actual= ' + runningBalInvoicd
                   )); 
           }
            
            if(hasErrorInvoiced){
                retString +=  '\n' + 'Supplemental Rent '+suppRent.Name+' mismatch in running balance and sum of transactions   - ' + suppRent.id + 
                             +'\n  Transactions Total (Invoiced): --Expected= ' + Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Invoiced__c) + 'Running Balance (Invoiced): --Actual= ' + Leasewareutils.zeroIfNull(runningBalInvoicd);
                                
                mapParam.put('ERROR','YES');
            }
            
            //Checking for TransactionsTotal(Cash Collected) is not equal to the latest RunningBalance(Cash Collected) record
            if(Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Cash__c) != Leasewareutils.zeroIfNull(runningBalCash) )
            {
                    hasErrorCash = true;
                    issueRecordList.add(new Admin_Data_Validation_Result__c(
                        name= 'SR_BalanceMismatch_CashTransactions--',
                        Admin_Data_Validation__c=parentId,
                        Request_Id__c = processReqId,
                        Issue_Record_Name__c=suppRent.Name,
                        Issue_Record_Id__c=suppRent.id,
                        Comment__c = 'Supplemental Rent '+suppRent.Name+' mismatch in running balance and sum of transactions.'+ 
                                    +'Transactions Total (Cash Collected): Expected= ' + Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Invoiced__c) + '; Running Balance (Cash Collected): Actual= ' + runningBalInvoicd
					));                
                }
            
            if(hasErrorCash){
                retString +=  '\n' + 'Supplemental Rent '+suppRent.Name+' mismatch in running balance and sum of transactions  - ' + suppRent.id + 
                             +'\n  Transactions Total (Cash Collected): --Expected= ' + Leasewareutils.zeroIfNull(suppRent.Transactions_Total_Invoiced__c) + 'Running Balance (Cash Collected): --Actual= ' + Leasewareutils.zeroIfNull(runningBalInvoicd) ;
                                
                mapParam.put('ERROR','YES');
            }
            
                
        }
        
        if(issueRecordList.size()>0) {insert issueRecordList;}
        retString += '\n' + 'End of Supplemental Rent Balance and Running Balance mismatch check ---- ';

        return retString;
    }
  
         //Method to check if the Notification Setup is missing
         public list<String> notificationSubscriptionInErrorStart(map<string,string> mapParam){ 
        
            String qString = 'Select id, name,Error_Message__c from Notifications_Subscription__c where Status__c =\'Error\'';
            return new list<String>{qString};  
        }
        public String notificationSubscriptionInErrorExecute(list<Notifications_Subscription__c> notificationSubscriptionList, map<string,string> mapParam){
            system.debug('notificationSubscriptionInErrorExecute-----'+notificationSubscriptionList);
            list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
            string parentId = mapParam.get('PARENT_ID');
            string processReqId = mapParam.get('PROCESS_REQ_ID'); 
            string retString = 'Checking Notification Subscription Records in Error  ----\n\n ' +notificationSubscriptionList ;  
            retString = 'Size of Error record:: '+notificationSubscriptionList.size()+'\n\n';  
            for(Notifications_Subscription__c ns : notificationSubscriptionList){
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                            name= ('Notification Subscription In Error Status -' + ns.Name).left(80),
                            Admin_Data_Validation__c=parentId,
                            Request_Id__c = processReqId,
                            Issue_Record_Name__c=ns.Name,
                            Issue_Record_Id__c=ns.id,
                            Comment__c = 'Notification Subscription failed to run ---'+ ns.Error_Message__c
                            ));
            }
            if(issueRecordList.size()>0) {
                mapParam.put('ERROR','YES');
                insert issueRecordList;
            }
            retString += '\n' + 'End of Notitifcation Subscription In Error  check ---- ';
            return retString;
        }
    
        //Method to check if the Notification Setup is missing
        //If notification subscription record is present and notificatin set up is missing
        public list<String> notificationSetupMissingStart(map<string,string> mapParam){ 
            
            String qString = 'Select id, name from Notifications_Subscription__c';
            if(!test.isRunningTest()){
                qString += ' limit 1';
            }
            return new list<String>{qString};  
        }
    
        public String notificationSetupMissingExecute(list<Notifications_Subscription__c> notificationSubscriptionList, map<string,string> mapParam){
            system.debug('notificationSetupMissingExecute-----'+notificationSubscriptionList);
            list<Admin_Data_Validation_Result__c> issueRecordList= new list<Admin_Data_Validation_Result__c>();
            string parentId = mapParam.get('PARENT_ID');
            string processReqId = mapParam.get('PROCESS_REQ_ID'); 
            string retString = 'Checking Notification Setting Record  ---- ' +notificationSubscriptionList ;
            List<Notification_Setting__c> notifSetup = [select id,Name from Notification_Setting__c ];  
            if(notifSetup.size()==0){
                issueRecordList.add(new Admin_Data_Validation_Result__c(
                    name= 'Notification Setup Missing',
                    Admin_Data_Validation__c=parentId,
                    Request_Id__c = processReqId,
                    Issue_Record_Name__c='',
                    Issue_Record_Id__c='',
                    Comment__c = 'Notification Setting record should be created to use Notification Subscriptions'
                    ));
                    retString += '\n' + 'Notification Setting Record  ----  is missing' ;
            }
            if(issueRecordList.size()>0) {
                mapParam.put('ERROR','YES');
                insert issueRecordList;
            }
            retString += '\n' + 'End of Checking Notification Setting Record  ---- ---- ';
    
            return retString;
        }
    
}