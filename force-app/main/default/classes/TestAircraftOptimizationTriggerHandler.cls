/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAircraftOptimizationTriggerHandler {

 
	@isTest(seeAllData=true)
    static void myUnitTest() {
        
        Aircraft__c aircraft1 = new Aircraft__c(Name='AC1',MSN_Number__c='1234509',Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available');
        LeaseWareUtils.clearFromTrigger();
        insert aircraft1;
        
        Aircraft__c aircraft2 = new Aircraft__c(Name='AC2',MSN_Number__c='12345598',Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available');
        LeaseWareUtils.clearFromTrigger();
        insert aircraft2;
        
        list<Aircraft_Optimization__c> listAO = new list<Aircraft_Optimization__c>();
        
        Aircraft_Optimization__c AO1 = new Aircraft_Optimization__c();
        AO1.Aircraft__c=aircraft1.Id;
        AO1.Name='AO1';
        AO1.Date_of_Analysis__c=system.today();
        listAO.add(AO1);
        
        Aircraft_Optimization__c AO2 = new Aircraft_Optimization__c();
        AO2.Aircraft__c=aircraft2.Id;
        AO2.Name='AO2';
        AO2.Date_of_Analysis__c=system.today();
        listAO.add(AO2);
        
        Aircraft_Optimization__c AO3 = new Aircraft_Optimization__c();
        AO3.Aircraft__c=aircraft2.Id;
        AO3.Name='AO3';
        AO3.Date_of_Analysis__c=system.today();
        listAO.add(AO3);
        
        Aircraft_Optimization__c AO4 = new Aircraft_Optimization__c();
        AO4.Aircraft__c=aircraft2.Id;
        AO4.Name='AO4';
        AO4.Date_of_Analysis__c=system.today();
        listAO.add(AO4);
        
        Aircraft_Optimization__c AO5 = new Aircraft_Optimization__c();
        AO5.Aircraft__c=aircraft2.Id;
        AO5.Name='AO3';
        AO5.Date_of_Analysis__c=system.today();
        listAO.add(AO5);
        
        insert listAO;
        
        for(Aircraft_Optimization__c curRec: listAO){
            curRec.Name='AO';
            curRec.Date_of_Analysis__c=system.today()-1;
        }
        
        update listAO;
        
    }
        
    
}