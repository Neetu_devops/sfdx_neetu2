global class PDPDelivieryBatch implements Database.Batchable<sObject> , Database.Stateful{
    
    global Set<Id> deliIds = new Set<Id>();
    String exMsg='';
    
    global PDPDelivieryBatch(Set<Id> allIds){
        deliIds = allIds;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        string query = 'select id FROM Pre_Delivery_Payment__c WHERE Id IN: deliIds';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,  List<sObject> scope) {
        Savepoint spStart = Database.setSavepoint();
        try{
            PDPHandler.comesFromBatch = true;
            update scope;
        }catch(Exception ex){
            Database.rollback(spStart);
            exMsg += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
            LeaseWareUtils.createExceptionLog(ex, 'Exception in PDPDeliveryBatch.', null, null,'PDP Delivery Batch',false);  
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
      if(String.isNotEmpty(exMsg)){
        LeasewareUtils.insertExceptionLogs();
        LeaseWareUtils.sendEmail('PDP Delivery Batch', 'There was an unexpected error. Please contact your LeaseWorks System Administrator. \n \n \n  <br/><br/>Reason :'+ exMsg, UserInfo.getUserEmail());
      }
     
        
    }
}