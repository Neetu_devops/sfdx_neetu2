public with sharing class CashFlowMaintenanceChartController {
	// Bar Chart and Drill down
    @auraEnabled
    public static String getCashFlowBarData(Id recordId) {
        List<Monthly_Scenario__c> lstMonthlyScenarioWithEvent = [SELECT Id,Event_Date__c, Claim_Month_Period__c,Fx_Component_Output__r.type__c, Fx_Component_Output__r.Assembly_Event_Type__c 
                                                                 FROM Monthly_Scenario__c WHERE Fx_Component_Output__r.Fx_General_Input__c = : recordId  
                                                                 AND Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL AND Event_Date__c != NULL 
                                                                 ORDER BY Start_Date__c,Fx_Component_Output__r.Type__c];
        
        Set<String> setType = new Set<String>{'Engines','LandingGear','APU','Airframe','GearIcon'}; 
        Map<Date,Monthly_Scenario__c> mapClaimMonth = new Map<Date,Monthly_Scenario__c>(); 
        Map<String,StaticResource> mapStaticResourceWithType = new Map<String,StaticResource>();
        Map<String,List<String>> mapClaimMonthAssemblyType = new Map<String,List<String>>();
        Map<String,List<Monthly_Scenario__c>> mapMonthlyScenario = new Map<String,List<Monthly_Scenario__c>>();
        for(Monthly_Scenario__c ms : lstMonthlyScenarioWithEvent ){
            mapClaimMonth.put(ms.Claim_Month_Period__c,ms);
            String claimDate = Utility.formatDate(ms.Claim_Month_Period__c);
            if(!mapClaimMonthAssemblyType.containsKey(claimDate))
            	mapClaimMonthAssemblyType.put(claimDate,new List<String>());
            mapClaimMonthAssemblyType.get(claimDate).add(ms.Fx_Component_Output__r.Assembly_Event_Type__c );
        }
        
        
        List<Monthly_Scenario__c> lstMonthlyScenario = [SELECT Id,Event_Date__c, Fx_Component_Output__r.Event_Type__c,Start_Date__c,Fx_Component_Output__r.Type__c,
                                                        Fx_Component_Output__r.Assembly_Event_Type__c,
                                                        Fx_Component_Output__r.Name,Lessor_Contribution_Utilized__c,Airline_Contribution__c,MR_Contribution__c,Claim_Month_Period__c 
                                                        FROM Monthly_Scenario__c WHERE Fx_Component_Output__r.Fx_General_Input__c = : recordId  
                                                        AND Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL AND Start_Date__c IN: mapClaimMonth.keySet()
                                                        ORDER BY Start_Date__c,Fx_Component_Output__r.Type__c];
           
                    
        if(lstMonthlyScenario.size()>0){
            for(Monthly_Scenario__c ms : lstMonthlyScenario ){
                if( ms.Fx_Component_Output__r.Assembly_Event_Type__c != 'General'){
                     List<String> lstClaimType= new List<String>();
                     lstClaimType.addAll(mapClaimMonthAssemblyType.get(Utility.formatDate(ms.Start_Date__c)));
                    
                     if(mapClaimMonth.containsKey(ms.Start_Date__c) && lstClaimType.contains(ms.Fx_Component_Output__r.Assembly_Event_Type__c)){
                        if(!mapMonthlyScenario.containsKey(Utility.formatDate(mapClaimMonth.get(ms.Start_Date__c).Event_Date__c))){
                            mapMonthlyScenario.put(Utility.formatDate(mapClaimMonth.get(ms.Start_Date__c).Event_Date__c),new List<Monthly_Scenario__c>());
                        }
                        mapMonthlyScenario.get(Utility.formatDate(mapClaimMonth.get(ms.Start_Date__c).Event_Date__c)).add(ms);
                        System.debug(ms.Start_Date__c+'lstMonthlyScenario>> :'+mapClaimMonth.get(ms.Start_Date__c).Fx_Component_Output__r.Assembly_Event_Type__c +'******'+ms.Fx_Component_Output__r.Type__c);
                    }
                }
            }
        }
        
        List<StaticResource> lstStaticResource = [SELECT Id,Name, SystemModStamp
                                                  FROM StaticResource 
                                                  WHERE Name =: setType];
        
        
        Map<String, StaticResource> mapTypeWithStaticResource = new Map<String, StaticResource>();
        if(lstStaticResource.size()>0 ){
            for(StaticResource staticRes : lstStaticResource){
                mapTypeWithStaticResource.put(staticRes.Name,staticRes);
            }
        }
        
        List<CumulativeData> listLeasedata = new List<CumulativeData>();
        List<CumulativeData> listAirlineData = new List<CumulativeData>();
        List<CumulativeData> listMRData = new List<CumulativeData>();
        List<CumulativeData> markerData = new List<CumulativeData>();
       
        for(String monthlyData : mapMonthlyScenario.keySet()){
             Decimal totalAirline = 0.0, totalMR =0.0, totalLessor= 0.0;
             String type ='', assemblyType='';
             Decimal total = 0;
             Integer count = 0,count1=0;
             Date startDate ;
           
           
            for(Monthly_Scenario__c ms: mapMonthlyScenario.get(monthlyData)){
                totalLessor += (ms.Lessor_Contribution_Utilized__c == null? 0.0:ms.Lessor_Contribution_Utilized__c);
                totalAirline += (ms.Airline_Contribution__c == null? 0.0:ms.Airline_Contribution__c);
                totalMR += (ms.MR_Contribution__c == null? 0.0:ms.MR_Contribution__c);
                startDate = ms.Start_Date__c;
                System.debug(monthlyData+'monthlyData>>>'+ms);
            }
            total += totalAirline +totalMR + totalLessor;
            Set<String> lstAssemblies = new Set<String>();
                if(mapClaimMonth.containsKey(startDate)){
                    lstAssemblies.add(mapClaimMonth.get(startDate).Fx_Component_Output__r.Type__c);
                    if(lstAssemblies.contains(mapClaimMonth.get(startDate).Fx_Component_Output__r.Type__c)){
                        assemblyType = mapClaimMonth.get(startDate).Fx_Component_Output__r.Type__c;
                    }else{
                        count1 = 1;
                    }
                }
            
            if(lstAssemblies.size()> 0){
                     System.debug('mapMonthlyScenario>> :'+lstAssemblies);
                    //Set Static Resource
                    if( assemblyType.contains('Engine')){
                        type = 'Engines';
                    }else if(assemblyType.contains('Landing Gear')){
                        type = 'LandingGear';
                    }else if(assemblyType.contains('APU')){
                        type = 'APU';
                    }else if(assemblyType.contains('Airframe')){
                        type = 'Airframe';
                    } 
                }
           System.debug(monthlyData+count1+'lstAssemblies>>>'+lstAssemblies);
            if(lstAssemblies.size()== 1 && (total != null && total > 0)){
                System.debug('total if'+total);
                markerData.add(new CumulativeData(monthlyData,fetchStaticResource(mapTypeWithStaticResource, type),(total+4000)/1000,''));
            }else if(lstAssemblies.size()> 1  && (total != null && total > 0)){
                System.debug(totalLessor+'totalLessor'+totalMR+'totalMR'+totalAirline+'total else'+total);
                markerData.add(new CumulativeData(monthlyData,fetchStaticResource(mapTypeWithStaticResource, 'GearIcon'),(total+4000)/1000,''));
            }else{
               markerData.add(new CumulativeData(monthlyData,'',null,'')); 
            }
             
            
                listLeasedata.add(new CumulativeData(monthlyData,'',Math.round(totalLessor)/1000,''));
                listAirlineData.add(new CumulativeData(monthlyData,'',Math.round(totalAirline)/1000,''));
                listMRData.add(new CumulativeData(monthlyData,'',Math.round(totalMR)/1000,''));
        }
        //System.debug('listLeasedata Size---->>'+listLeasedata.size());
        String leasordata = System.json.serialize(listLeasedata);
        String mrdata = System.json.serialize(listMRData);
        String leaseedata = System.json.serialize(listAirlineData);
        String marker = System.json.serialize(markerData);
        
        String JSON = '{'+'"leasordata"'+':'+leasordata+','+'"mrdata"'+':'+mrdata+','+'"leaseedata"'+':'+leaseedata+','+'"marker"'+':'+marker+'}';
        return JSON;
    }
    
    
    //BAR Chart Table
     @auraEnabled
    public static List<CashMaintenanceTableWrapper> getCashFlowBarTableData(Id recordId,String startDate) {
        List<CashMaintenanceTableWrapper> lstCashMaintenanceTableWrapper = new List<CashMaintenanceTableWrapper>();
        for(Monthly_Scenario__c ms : ForecastSOQLServices.getMonthlyScenarioForBarChartTable(recordId,startDate)){
            CashMaintenanceTableWrapper cmt = new CashMaintenanceTableWrapper();
            cmt.assemblyName = ms.Fx_Component_Output__r.Assembly_Display_Name__c;
            cmt.svAndLLPReason = ms.Event_Reason__c;
            cmt.eventDate = ms.Event_Date__c;
            cmt.lastPerformanceRestauration = ms.Month_Since_Last_Event__c  ;
            cmt.nextPerformanceRestauration = ms.Month_Since_Next_Overhaul_F__c;
            cmt.cost = ms.Event_Expense_Amount__c;
            cmt.averageUtilization = ms.AVG_FC__c;
            cmt.FH = ms.AVG_FH__c;
           lstCashMaintenanceTableWrapper.add(cmt); 
        }
        return lstCashMaintenanceTableWrapper;
    }
    
    
    
    //To get the static Resource
    private Static String fetchStaticResource(Map<String,StaticResource> mapTypeWithSaticResource, String Type){
        String markerJSON = '';
        //To get the package prefix
        
        if(mapTypeWithSaticResource.containsKey(Type)){
            String org_Url = String.valueOF(System.URL.getSalesforceBaseUrl().toExternalForm());
            
            String url_file_ref = '/resource/'
                + String.valueOf(((DateTime)mapTypeWithSaticResource.get(Type).SystemModStamp).getTime())
                + '/' 
                + leasewareutils.getNamespacePrefix()+mapTypeWithSaticResource.get(Type).Name
                ;
            org_Url += url_file_ref;
            
            markerJSON =  'url' + '(' + org_Url + ')' ;
        }
        return markerJSON;
    }
    
    
    //Wrapper for Bar char table
    public class CashMaintenanceTableWrapper {
        @AuraEnabled public String assemblyName { get; set; }
        @AuraEnabled public String svAndLLPReason { get; set; }
        @AuraEnabled public Date eventDate { get; set; }
        @AuraEnabled public Decimal lastPerformanceRestauration { get; set; }
        @AuraEnabled public Decimal cost { get; set; }
        @AuraEnabled public Decimal nextPerformanceRestauration { get; set; }
        @AuraEnabled public Decimal averageUtilization { get; set; }
        @AuraEnabled public Decimal FH { get; set; }
    }
    
    // Wrapper class
    public class CumulativeData {
        @AuraEnabled
        public String month { get; set; }
        @AuraEnabled
        public MarkerWrapper marker { get; set; }
        @AuraEnabled
        public Decimal y { get; set; }
        @AuraEnabled
        public String stack { get; set; }
        
        public CumulativeData(String month,String marker,Decimal y,String stack) {
            this.month = month;
            this.marker = new MarkerWrapper(marker);
            this.y = y;
            this.stack = stack;
        }
    }
    
       
    //Wrapper class for showing marker with data
    public class MarkerWrapper{
        @AuraEnabled
        public String symbol{get;set;}
        public MarkerWrapper(String symbol){
            this.symbol = symbol;
        }
        
    }
}