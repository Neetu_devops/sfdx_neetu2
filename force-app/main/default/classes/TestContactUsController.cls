@isTest
public class TestContactUsController {
    @isTest
    public static void testGetContacts(){
        Lessor__c setup=new Lessor__c();
        setup.Name='test lessor';
        insert setup;
        
        Counterparty__c testCounterparty=new  Counterparty__c();
        testCounterparty.Name=setup.Name;
        insert testCounterparty;
        
        Contact conRecord=new Contact();
        conRecord.LastName='test';
        conRecord.Lessor__c=testCounterparty.Id;
        conRecord.Department='test';
        conRecord.Email='test@gmail.com';
        conRecord.Description='test text';
        insert conRecord;
        Test.startTest();
        ContactUsController.getContacts();
        Test.stopTest();
    }
}