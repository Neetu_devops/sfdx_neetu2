public with sharing class NewsArticleLWCClass {

	public static List<NewsArticleWrapper> newsArticles = new List<NewsArticleWrapper> ();
	public static final String SERVICENAME = 'News_Feed_CAPA';  //used for authentication in CAPA

	@AuraEnabled
	public static String getNamespace(){
		return LeaseWareUtils.getNamespacePrefix();
	}

	@AuraEnabled
	public static List<DataTableWrapper> getNewsFeed(String newsSource,Id recordId,Integer pageNumber, Integer pageSize,
													 String searchKeywrd, String startDt,String endDt,String fieldName,String region, String category){
		System.debug('NewsArticleLWCClass.getNewsFeed(+)');
		System.debug('newsSource:' + newsSource);
		System.debug('recordId:' + recordId);
		System.debug('pageNumber:' + pageNumber);
		System.debug('pageSize:' + pageSize);
		System.debug('fieldName:' + fieldName);
		System.debug('Region value in LWC :' + region);
		System.debug('Category value in LWC :' + category);
		NewsComponentCntrl newsComponentCntrl = new NewsComponentCntrl();

		String newsString = newsComponentCntrl.getNewFeeds(newsSource,recordId,pageNumber, pageSize,searchKeywrd,startDt,endDt,fieldName,region,category);
		newsArticles = (List<NewsArticleWrapper>) System.Json.deserialize(newsString, List<NewsArticleWrapper>.class);
		System.debug('newsArticles :' + newsArticles.size());
		List<DataTableWrapper> response = new List<DataTableWrapper>();
		if(newsArticles.size() == 0) {
			// enter a dummy data
			DataTableWrapper obj = new DataTableWrapper();
			//response.add(obj);
		}else{
			for(NewsArticleWrapper news : newsArticles) {
				DataTableWrapper obj = new DataTableWrapper();
				obj.newsId = news.newsId;
				obj.articleId = news.articleId;
				obj.link = news.link;
				obj.articleUrl = news.link;
				obj.article = news.article;
				obj.articleDate = news.articleDate;
				obj.type = news.type;
				obj.source = news.newsSource;
				obj.fullArticle = news.fullArticle;
				obj.richTextArticle = news.fullArticle;
				obj.title = news.title;
				obj.isLastPage = news.isLastPage;
				obj.totalEntries = news.totalEntries;
				obj.page = news.page;
				obj.totalPages = news.totalPages;         
				response.add(obj);
			}
		}
		if(newsArticles.size()>0 && response.size()>0){
			response[0].articlesNotReturned = newsArticles[0].articlesNotReturned ;
		}
		System.debug('Search response:' + response.size());
		System.debug('NewsArticleLWCClass.getNewsFeed(-)');
		return response;
	}
	// This method is used in LWC component to get the redirect url for auto login
	@AuraEnabled
	public static ResponseRedirectWrapper  getRedirectUrl(String url){
		System.debug('NewsArticleLWCClass.getRedirectURl (+)');

		ResponseRedirectWrapper responseWrapper ;
		DateTime logoutTime ;
		
		HttpResponse response ;
		List<LW_Setup__c> listRedirectToken = [Select Value__c,Disable__c,CreatedDate from LW_Setup__c where Name like 'CAPAToken%' and Disable__c = false];
		List<LW_Setup__c> listLogoutTime = [Select Value__c,Disable__c,CreatedDate from LW_Setup__c where Name = 'ExpiryTimeForCAPAToken' and Disable__c = false];

			if(listLogoutTime.size()>0){
				System.debug('sessionValidity : ' + listLogoutTime[0]);
				try{
					String time1 = listLogoutTime[0].Value__c.substringAfter('---');
					logoutTime = Datetime.valueOfGmt(time1);
				}catch(Exception e){
					logoutTime = null;
					system.debug('Error in LWSetup ExpiryTimeForCAPAToken value.'+e.getMessage()); 
					LeaseWareUtils.createExceptionLog(e, 'Error in LWSetup ExpiryTimeForCAPAToken value. ', null , null ,'News Article', false);					
				}
				System.debug('logoutTime : ' + logoutTime);
			}
			if(logoutTime != null && listRedirectToken.size()>0  && System.now()< logoutTime){     // session is still valid , return the same token back. 
				System.debug('-----SESSION still valid------');
				responseWrapper = new ResponseRedirectWrapper();
				responseWrapper.statusCode = '302';	
				responseWrapper.restResponse += 'https://centreforaviation.com/shared?access_token=';
				for(LW_Setup__c setup :listRedirectToken){
					responseWrapper.restResponse +=setup.Value__c;
				}
				String expirySec = listLogoutTime[0].Value__c.substringBefore('---');
				responseWrapper.restResponse += '&expires_in='+expirySec+'&redirect=' ;
				responseWrapper.expiryInsec = Integer.valueOf(expirySec);
			}else{
				System.debug('-----SESSION not valid , getting from API ------');
				responseWrapper = getHttpResponse(url);				
				logoutTime = System.now().addSeconds(responseWrapper.expiryInsec);
				System.debug('logoutTime:' + logoutTime);

				setTokenValuesAndInsert(listRedirectToken ,listLogoutTime,responseWrapper
				,String.valueOfGmt(logoutTime),String.valueOf(responseWrapper.expiryInsec));		 
			}

		LeaseWareUtils.insertExceptionLogs();
		system.debug(System.JSON.serializePretty(responseWrapper));
		System.debug('NewsArticleLWCClass.getRedirectURl (-)');
		return responseWrapper;
	}

	private static void setTokenValuesAndInsert(List<LW_Setup__c> listRedirectToken ,List<LW_Setup__c> listSessionValidity,ResponseRedirectWrapper responseWrapper
											,String logoutTime,String expirySec){

		//delete the old token first and recreate new records.
		
		try{
			if(listRedirectToken.size()>0){delete listRedirectToken ;}
			if(listSessionValidity.size()>0){delete listSessionValidity;}
		}catch(Exception e){
			system.debug('exception in deleting old tokens =='+e.getMessage()); 
			LeaseWareUtils.createExceptionLog(e, 'exception in deleting old tokens ', null , null ,'News Article', false);
		}
		listRedirectToken.clear();
		listSessionValidity.clear();

		// creating setup record for logout time
		if(String.isNotBlank(logoutTime)){
			listSessionValidity.add(new LW_Setup__c( Name = 'ExpiryTimeForCAPAToken' ,
													 Value__c = expirySec+'---'+logoutTime,		
													 Disable__c = false,
													 Description__c = 'Time till which the CAPA redirect token is valid'));
		}
		System.debug('listSessionValidity:' + listSessionValidity);
		
		// creating setup record for token
		if(String.isNotBlank(responseWrapper.restResponse)){
			String tokenAfter = responseWrapper.restResponse.substringAfter('https://centreforaviation.com/shared?access_token=');
			String token = tokenAfter.substringBefore('&expires_in');

			System.debug('token.length():' + token.length());
			integer i =0;
			do{
				String token1 = '';
				if(token.length()>255){ token1  = token.substring(0, 255); }
				else{token1 = token ;}
				System.debug('token1 :' + token1.length());
				LW_Setup__c tokenSetup1 =  new LW_Setup__c(Name = 'CAPAToken'+i ,
													  Value__c = token1,		
													  Disable__c = false,
													  Description__c = 'Redirect Token for CAPA autologin');
				listRedirectToken.add(tokenSetup1);
				i++;
				if(token.length()>255){token = token.substring(255,token.length());}
				else{token = '';} // this should be after the last token whose length is less than 255
				System.debug('token.length() from loop after reset :' + token.length());
			}while(token.length()>0);
		 	System.debug('listRedirectToken 1 ...' + listRedirectToken.size());
	  }

		try{
			if(listRedirectToken.size()>0){insert listRedirectToken ;}
			if(listSessionValidity.size()>0){insert listSessionValidity;}
		}catch(Exception e){
			system.debug('exception in inserting tokens =='+e.getMessage()); 
			LeaseWareUtils.createExceptionLog(e, 'exception in inserting tokens ', null , null ,'News Article', false);
		}
	}

	// Method for Automatic Authentication in CAPA
	private static  ResponseRedirectWrapper getHttpResponse(String url){
		
		ResponseRedirectWrapper responseWrapper = new ResponseRedirectWrapper();
		String staticResponse = '';
		String parsedResponse = '';
		Integer expiresInSec = 28800;
		Http http = new Http();
		HttpResponse response ;
		HttpRequest request = new HttpRequest();

		String token = CAPANewsFeedService.requestCAPAApiToken();
		request.setEndpoint('https://centreforaviation.com/login.php');
		request.setMethod('POST');
		request.setHeader('Content-Type','application/x-www-form-urlencoded');
		request.setHeader( 'Authorization', 'Bearer ' + token );

		String userCred = urlEncodeUserCred();
		if(String.isBlank(userCred)){
			System.debug('Username or Password for CAPA login is missing.');
			throw new AuraHandledException ('Username or Password for CAPA login is missing.');
		}
		userCred += '&url='+ url ;
		request.setBody(userCred);
		System.debug('===request===' + request);
		try{
			response = new System.Http().send(request);
		}catch(System.CalloutException exCall){
			  System.debug(exCall.getMessage()); 
			  throw new AuraHandledException ('There was an error redirecting to CAPA.Please check service settings or remote site settings.');
		}
		system.debug('Response === '+response.getBody());

		if(response != null && String.isNotBlank(response.getBody())){
			parsedResponse = response.getBody();
		}

		if(String.isNotBlank(parsedResponse) && (parsedResponse.contains('<p>Found'))){
			parsedResponse = parsedResponse.replace('<p>Found. Redirecting to <a href="','');
			parsedResponse = parsedResponse.substringBefore('">');
			parsedResponse = parsedResponse.replaceAll('&amp;','&');
			staticResponse = parsedResponse.substringBefore('redirect=');
			expiresInSec = Integer.valueOf(parsedResponse.substringBetween('expires_in=','&'));
		}

		system.debug('staticResponse 2=== '+staticResponse);		
		
		responseWrapper.statusCode = string.valueOf(response.getStatusCode());
		responseWrapper.restResponse = staticResponse+'redirect=';
		responseWrapper.expiryInsec = expiresInSec;
		return responseWrapper;
	}

	@AuraEnabled(cacheable=true)
	public static List<String> getAviatorCategories(){
		list <String> listLWAviatorCategories = new list <String> ();
		Map <string, LW_Aviator_News_Categories__c > mapLWAviatorCategories = LW_Aviator_News_Categories__c.getAll();
		listLWAviatorCategories.addAll(mapLWAviatorCategories.keyset());
		System.debug('values : ' + listLWAviatorCategories);
		return listLWAviatorCategories;
	}

	@AuraEnabled(cacheable=true)
	public static List<String> getAviatorRegions(){
		list <String> listLWAviatorRegions = new list <String> ();
		Map <string, LW_Aviator_News_Regions__c > mapLWAviatorRegions = LW_Aviator_News_Regions__c.getAll();
		listLWAviatorRegions.addAll(mapLWAviatorRegions.keyset());
		System.debug('values : ' + listLWAviatorRegions);
		return listLWAviatorRegions;
	}

	// This is required only for CAPA auto login
	private static String urlEncodeUserCred() {
        String result = '';
        ServiceSetting__c lwServiceSetup = ServiceSetting__c.getValues(SERVICENAME); 
        if( lwServiceSetup!=null && lwServiceSetup.username__c !=null && lwServiceSetup.password__c != null){
			result += '' 
			 + 'username=' + EncodingUtil.urlEncode(lwServiceSetup.Username__c, 'UTF-8') 
			 + '&password=' + EncodingUtil.urlEncode(lwServiceSetup.Password__c, 'UTF-8') +'&';
         }else{
			return result;
		 }
        return result.removeEnd('&');    
	}
	
	@AuraEnabled
	public static Boolean getOperatorFilterValue(){

		String operatorNameFilter = LeaseWareUtils.getLWSetup_CS('OPERATOR_NAME_FILTER');
		if(String.isNotBlank(operatorNameFilter)){
			return (operatorNameFilter.equalsIgnoreCase('ON') ? true : false);
		}
		return false;
	}

	@TestVisible
	private class DataTableWrapper {
		@AuraEnabled
		public Id newsId {get; set;}
		@AuraEnabled
		public String articleId {get; set;}
		@AuraEnabled
		public String link {get; set;}
		@AuraEnabled
		public String articleUrl {get; set;}
		@AuraEnabled
		public String article {get; set;}
		@AuraEnabled
		public String fullArticle {get; set;}
		@AuraEnabled
		public String richTextArticle {get; set;}
		@AuraEnabled
		public Date articleDate {get; set;}
		@AuraEnabled
		public String type {get; set;}
		@AuraEnabled
		public String source {get; set;}
		@AuraEnabled
		public String title {get; set;}
		@AuraEnabled
		public Boolean isLastPage {get; set;}
		@AuraEnabled
		integer totalEntries {get;set;}
		@AuraEnabled
		integer page {get;set;}
		@AuraEnabled
		integer totalPages{get;set;}
		@AuraEnabled
        integer articlesNotReturned{get;set;}


		public DataTableWrapper(){newsId = null;articleId = null;
									link = null;
									articleUrl = 'No Records Found';
									article = 'No Records Found';
									articleDate = null;
									type = '';
									fullArticle = '';
									source = '';
									title = '';
									isLastPage = false;
									totalEntries = 0;
									page = 0;
									totalPages= 0;
								}

	}

	@testVisible
	public class NewsArticleWrapper {
        String name {get;set;}
        Id operator {get;set;}
        Id lessor {get;set;}
        Id setup {get;set;}
        Date articleDate {get;set;}
        String article {get;set;}
        String tag {get;set;}
        String link {get;set;}
        String title {get;set;}
        String articleId {get;set;}
        string newsSource {get;set;}
		string fullArticle {get;set;}
		boolean isLastPage {get;set;}
		String type {get; set;}
		Id newsId {get; set;}
		integer totalEntries {get;set;}
        integer page {get;set;}
		integer totalPages{get;set;}
		integer articlesNotReturned{get;set;}
    }

	@TestVisible
	public class ResponseRedirectWrapper {
		@AuraEnabled
		public String statusCode {get; set;}
		@AuraEnabled
		public String restResponse {get; set;}
		@AuraEnabled
		public Integer expiryInsec {get; set;}
	

		public ResponseRedirectWrapper(){
			statusCode = '';
			restResponse = '';
			expiryInsec = 0;
		}

	}
}