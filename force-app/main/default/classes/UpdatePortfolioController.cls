public with sharing class UpdatePortfolioController {
    
    @AuraEnabled
    public static String getColumns(String recordId, String fieldSetName){
        System.debug('getColumns fieldSetName ' + fieldSetName);
        if(String.isBlank(fieldSetName)) {
            //String defaultFieldSet = LeaseWareUtils.getNamespacePrefix()+'AssetInDealEditor';
            String defaultFieldSet = getFieldSetBasedOnType(recordId);
            System.debug('getColumns defaultFieldSet ' + defaultFieldSet);
            if(defaultFieldSet == null)
                return null;
            List<FieldStructure> fieldList = getFieldSets(defaultFieldSet);
            System.debug('getColumns fieldList ' + fieldList);
            if(fieldList == null || fieldList.size() <= 0) 
                return null;//JSON.serialize(defaultColumns());
            fieldList = addNameField(fieldList);
            return JSON.serialize(fieldList);
        }
        else {
            List<FieldStructure> fieldList = getFieldSets(fieldSetName);
            if(fieldList == null || fieldList.size() <= 0) 
                return null;JSON.serialize(defaultColumns());
            fieldList = addNameField(fieldList);
            return JSON.serialize(fieldList);
        }
    }
    
    public static List<FieldStructure> addNameField(List<FieldStructure> fieldList){
        FieldStructure field = new FieldStructure();
        field.label = 'Name';
        field.apiName = 'Name';
        field.fieldType = 'String';
        fieldList.add(0,field);
        return fieldList;
    }
    
    public static List<FieldStructure> defaultColumns(){
        List<FieldStructure> fieldList = new List<FieldStructure>();
        FieldStructure field = new FieldStructure();
        field.label = 'Name';
        field.apiName = 'Name';
        field.fieldType = 'String';
        fieldList.add(field);
        return fieldList;
    }
    
    @AuraEnabled
    public static String getAIDDealType(String parentId) {
        List<Aircraft_Proposal__c> aidList = [select Marketing_Activity__r.Deal_Type__c from Aircraft_Proposal__c where Marketing_Activity__c =:parentId limit 1];
        if(aidList.size() > 0) { return aidList[0].Marketing_Activity__r.Deal_Type__c; }
        return null;
    }
    
    @AuraEnabled
    public static String getFieldSetBasedOnType(String recordId) {
        List<Marketing_Activity__c> deals = [ select Deal_Type__c from Marketing_Activity__c 
                                             where 
                                             Id = :recordId ];
        if(deals.size() > 0) {
            String dealType = deals[0].Deal_Type__c;
            if(String.isNotEmpty(dealType)) {
                dealType = dealType.deleteWhitespace();
                String fieldSetName = 'Updateportfolio'+dealType;
                return fieldSetName;
            }
        }
        return null;
    }
    
    public static List<FieldStructure> getFieldSets(String fieldSetName) {
        String objectName = LeaseWareUtils.getNamespacePrefix() + 'Aircraft_Proposal__c';
        
        System.debug('getFieldSets objectName: '+objectName + ' fieldSetName: '+fieldSetName);
        
        Schema.DescribeSObjectResult describe = RLUtility.describeObject(objectName);
        Schema.FieldSet fieldSetObj = RLUtility.describeFieldSet(describe,fieldSetName);
        
        System.debug('getFieldSets describe:'+describe +' fieldSetObj:'+fieldSetObj);
        
        if(fieldSetObj == null)
            return null;
        List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields();
        List<FieldStructure> fieldList = new List<FieldStructure>();
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
        {
            FieldStructure field = new FieldStructure();
            field.apiName = fieldSetMemberObj.getFieldPath();
            field.label = fieldSetMemberObj.getLabel();
            field.fieldType = String.valueOf(fieldSetMemberObj.getType()).toLowerCase();
            if(!field.apiName.equalsIgnoreCase('Name'))
            	fieldList.add(field);
            system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
        }
        system.debug('getFieldSetNames fieldList' + fieldList); //api name
        return fieldList;
    }

   

    @AuraEnabled
    public static String getAIDList(String recordId, String fieldSetName){
        system.debug('Get AID List------' + recordId);
        
        if(String.isBlank(recordId)) return null;
        
        List<FieldStructure> fieldStructure;
        if(String.isBlank(fieldSetName)) {
            fieldSetName = getFieldSetBasedOnType(recordId);
            if(fieldSetName == null)
                return null;
            fieldStructure = getFieldSets(fieldSetName);
        }
        else 
            fieldStructure = getFieldSets(fieldSetName);
        		
        if(fieldStructure == null || fieldStructure.size() <= 0)
            return null;
        else{
            fieldStructure = addNameField(fieldStructure);
        }

        Set<String> fields = new Set<String>();
        for(FieldStructure field: fieldStructure) {
            if('Reference'.equalsIgnoreCase(field.fieldType)) {
                string fieldApi = Utility.replaceRefField(field.apiName, 'c', 'r.name');
                field.apiName = fieldApi;
                fields.add(fieldApi);      
            }
            else {
                fields.add(field.apiName);
            }
        }
        fields.add('Id');
        
        
        String field =  String.join(new List<String>(fields),',');
        String queryStr = 'select '+field+' from Aircraft_Proposal__c where Inactive_AT__c= false and  Marketing_Activity__c = :recordId';
        
        System.debug('getAssetInDeal queryStr: '+queryStr);
        List<Aircraft_Proposal__c> assetInDeals = database.query(queryStr);
        
        System.debug('getAssetInDeal assetInDeals: '+assetInDeals.size());
        if(assetInDeals.size() > 0) {
            List<AssetInDealWrapper> assetInDealList = new List<AssetInDealWrapper>();
            for(Aircraft_Proposal__c assetDeal : assetInDeals) {
                AssetInDealWrapper aid = new AssetInDealWrapper();
                List<FieldStructure> fieldStructData = new List<FieldStructure>();
                for(FieldStructure fieldSt: fieldStructure) {
                    FieldStructure fieldStr = new FieldStructure();
                    if(fieldSt.apiName.contains('__r')) {
                        String data = fieldSt.apiName;
                        String[] refData = data.split('\\.');
                        if(assetDeal.getSObject(refData[0]) != null) {
                            String value = String.valueOf(assetDeal.getSObject(refData[0]).get(refData[1]));
                            fieldStr.value = value;
                            if('Name'.equalsIgnoreCase(refData[1])) {
                                fieldStr.referenceId =  String.valueOf(assetDeal.getSObject(refData[0]).get('Id'));
                            }
                        }
                    }
                    else {
                        fieldStr.value = assetDeal.get(fieldSt.apiName);
                    }
                    fieldStr.apiName = fieldSt.apiName;
                    fieldStr.label = fieldSt.label;
                    fieldStr.fieldType = fieldSt.fieldType;
                    fieldStructData.add(fieldStr);
                }
                aid.fields = fieldStructData;
                aid.aidId = assetDeal.Id;
                assetInDealList.add(aid);
            }
            return JSON.serialize(assetInDealList);
        }
        return null;
    }
    
    
    
 /***********************************************Shweta code Start******************************************************* */
   

   /******************************************************************************************* */
    //This method is called on click of Update Portfolio button on Deal Page
    //Currently on the specified deals this action is allowed : Lease, Lease Extension,SLB,Purchase,Sale
    //If by any chance this button is put on any of the layout other than these validation error message needs to be shown
    //And also based on the custom setting DEAL_STATUS_FOR_AUTO_DEAL_CONVERSION value. The Deal Status before this custom setting value is not allowed to perform an action
   /******************************************************************************************* */
    @AuraEnabled
    public static String checkValidDealForUpdatePort(Id dealId){
       
        Marketing_Activity__c deal = [select Id,Name,RecordType.name,Deal_Type__c, Deal_Status__c from Marketing_Activity__c where Id =:dealId];
        system.debug('deal.Deal_Type__c----'+deal.Deal_Type__c);
        if(deal.Deal_Type__c=='Lease' || deal.Deal_Type__c=='Lease Extension' || deal.Deal_Type__c=='SLB' || deal.Deal_Type__c=='Purchase' || deal.Deal_Type__c=='Sale'){
            String customSettingDealStatus = LeaseWareUtils.getLWSetup_CS('DEAL_STATUS_FOR_AUTO_DEAL_CONVERSION');
            if(String.isEmpty(customSettingDealStatus)){
                customSettingDealStatus = 'Documentation';
            }
            boolean isvalid =validateOnDealStatus(customSettingDealStatus,deal.Deal_Status__c);
            system.debug('Is Valid Deal Status ----' + isValid);
            if(isValid){
                return '';
            }else{
                return 'You cannot update the portfolio if the Deal status is not '+ customSettingDealStatus;
            }
        }else{
            return 'This is not applicable for this Deal Type. Please contact your admin to update the layout';
        }
    }
    
    /******************************************************************************************* */
    // This method creates/updates(Asset and Lease) and updates Asset In deal Based On Deal type
    
    /******************************************************************************************* */
    @AuraEnabled
    public static ResultSetWrapper savePortfolio(String recordTypeName ,String aid){
       
        ResultSetWrapper resWrap = new ResultSetWrapper();
       
        system.debug('record type name-----' + recordTypeName);
        resWrap.aidId = aid;
        resWrap.status = 'SUCCESS';
        resWrap.errorMessage ='';
        

        //Get the mapping records from Custom Lookup for the given recordType
        map<String,List<String>> mapSourceObjFields = new map<String,List<String>>();// ex: map<Asset In Deal, {Rent Amount,Current Rent}> which is used to build AID Query
        map<String,String> mapSrcTarget = new map<String,String>();	//map<AID:Rent Amount, Lease:Current Rent>// src and target obj and field mapping
        getMappingRecords(recordTypeName,mapSourceObjFields,mapSrcTarget);//get mapping records for given recordtype
        String aidObjectName = LeasewareUtils.getNamespacePrefix() + 'Aircraft_Proposal__c';

        //Get the details of AID 
        String fieldListForQuery = getFieldsToBeAddedForQuery(mapSourceObjFields);
        if(!fieldListForQuery.contains(LeaseWareUtils.getNamespacePrefix()+'Asset_Type__c')){
            fieldListForQuery = fieldListForQuery+','+LeaseWareUtils.getNamespacePrefix()+'Asset_Type__c';
        }
       
        Aircraft_Proposal__c curRec;
        try{
            String queryStr = 'select Id,Name, ' +fieldListForQuery+ ' from '+ aidObjectName +' where ID = :aid';
            system.debug('queryStr----' + queryStr);
            curRec = Database.query(queryStr);
            
        }catch(Exception e){
            system.debug('e.getMessage()-----'+ e.getMessage());
            String exMessage = e.getMessage();
            if(exMessage.startsWith('No such column')){
                throw new AuraHandledException('The field set up in the'+ exMessage.substringBetween('No such column','.') +' is not available. Please check the api details.');
            }else{
                throw new AuraHandledException('Something is wrong with mapping records.Please check');
            }
        }
       // In case of Deal Type Lease/Sale/Purchase/SLB and By Quantity and Type is chosen on Deal Wizard
        if(recordTypeName =='Lease' || recordTypeName =='Sale'){
            if(curRec.Aircraft__c ==null){
                resWrap.aidId = curRec.id;
                resWrap.aidName= curRec.name;
                resWrap.status = 'ERROR';
                resWrap.errorMessage = 'Asset is not selected on the AiD';
                return resWrap;
            }
           
        }
        if(recordTypeName =='SLB' || recordTypeName =='Purchase'){
            if(String.isBlank(curRec.Serial_Number__c)){
                resWrap.aidId = curRec.id;
                resWrap.aidName= curRec.name;
                resWrap.status = 'ERROR';
                resWrap.errorMessage = 'Serial Number is not updated in the AiD';
                return resWrap;
            }
            
        }

        
        String leaseId ;
        String acId;
        Lease__c leaseRec;
        Aircraft__c ac;

        if(curRec.Lease__c!=null){ // In case Lease is already present on AID, update needs to be done
            leaseId=curRec.Lease__c;
        }
        if(curRec.Aircraft__c!=null){
            acId= curRec.Aircraft__c;
            if(leaseId==null){ leaseId= curRec.Aircraft__r.Lease__c;}// In case of Lease extension
        }
        if(leaseId!=null){
            leaseRec = [select Id, Name,Lease_Start_Date_new__c,Lease_End_date_new__c from Lease__c where ID = :leaseId];
            resWrap.leaseId = leaseRec.Id;
            resWrap.leaseName = leaseRec.name;
        }
        if(acId !=null){
            ac = [select Id,Name,MSN_Number__c,Lease__c from Aircraft__c where ID =:acId];
            resWrap.assetId = ac.Id;
            resWrap.assetName = ac.Name;
           
        }
        system.debug('leaseRec----' + leaseRec);
        /********************Default Behaviour as per Spec***************************** */
        try{

            
            if(recordTypeName == 'Lease' || recordTypeName =='SLB' || recordTypeName == 'Purchase'){
                    
                   
                    if(recordTypeName == 'SLB'  ||  recordTypeName == 'Purchase'){ //In case of SLB and Purchase , create an asset if not exists
                       
                        if(curRec.Aircraft__c == null && (curRec.World_Fleet_Aircraft__c!=null || curRec.Serial_Number__c!=null)){
                           
                            //Create an Asset 
                            ac= new Aircraft__c();
                            ac.MSN_Number__c = curRec.Serial_Number__c;
                            if(curRec.World_Fleet_Aircraft__c==null && String.isNotEmpty(curRec.Asset_Type__c)){
                                List<String> typeVariant = curRec.Asset_Type__c.split('-');
                                ac.Aircraft_Type__c = typeVariant[0];
                                ac.Aircraft_Variant__c = typeVariant.size()>1?typeVariant[1]:'';
                            }else{
                                ac.Aircraft_Type__c = curRec.World_Fleet_Aircraft__r.AircraftType__c;
                                ac.Aircraft_Variant__c = curRec.World_Fleet_Aircraft__r.AircraftVariant__c;
                            }
                            
                            ac.Date_of_Manufacture__c = curRec.Date_of_Manufacture__c;
                            
                        }
                        
                    }
                    //In case of Lease and SLB Create Lease if not exists
                    if(recordTypeName == 'SLB' || recordTypeName == 'Lease'){
                        leaseRec = new Lease__c();
                        leaseRec.Lease_Start_Date_New__c =system.today();
                        Date todayDate = system.today();
                        if(curRec.Term_Mos__c==null){
                            resWrap.status = 'ERROR';
                            resWrap.errorMessage = 'Term (Mos) is missing for the asset';
                        }else{
                            leaseRec.Lease_End_Date_New__c =todayDate.addMonths(Integer.valueOf(curRec.Term_Mos__c));
                        }
                        
                        leaseRec.Aircraft__c = curRec.Aircraft__c;
                        leaseRec.Operator__c= curRec.Marketing_Activity__r.Prospect__c;
                    
                }
            }
            if(recordTypeName == 'Lease Extension'){
                if(curRec.Lease__c ==null){  // If the Lease is not linked on AID which means Lease End Date has not been extended
                   
                    if(leaseRec!=null){
                        if(curRec.Term_Mos__c==null){ 
                            resWrap.status = 'ERROR';
                            resWrap.errorMessage = 'Term (Mos) is missing for the asset';
                            leaseRec = null;
                            resWrap.leaseId = null;
                            resWrap.leaseName = null;
                        }else{
                            leaseRec.Lease_End_Date_new__c =  leaseRec.Lease_End_Date_new__c.addMonths(Integer.valueOf(curRec.Term_Mos__c));
                        }
                    }
                }else{ //Lease End Date extended once will be prevented from updating again
                    resWrap.status = 'WARNING';
                    resWrap.errorMessage = 'Existing Lease End Date was already updated';
                }
               
            }
            if(recordTypeName == 'Sale'){
                system.debug('Sale ----> ac ----> '+ac + '-------Lease----' + leaseRec);
                if(leaseRec!=null){
                    leaseRec.Lease_Type_Status__c= 'Lease Expired/Terminated';
                }
                if(ac!=null){
                    ac.Sold__c =true;
                }
            }

            /*******************Based on Mapping Records updation of fields ************************************************************************************ */
            system.debug('mapSrcTarget---'+mapSrcTarget);
            for(String key : mapSrcTarget.keySet()){    
                    //If SOurce/Target Object or Source/Target FIeld is empty skip those
                    String sourceObj = key.substringBefore(':');
                    String sourceField= key.substringAfter(':');
                    if(String.isEmpty(sourceObj) || String.isEmpty(sourceField) ) continue;

                    sourceField = sourceField.trim();
                    system.debug('sourceField----'+ sourceField);
                    
                    //Considering Source only to be from Aircraft_Proposal__c
                    if(sourceObj.trim() == LeaseWareUtils.getNamespacePrefix() +'Aircraft_Proposal__c'){
                       
                        String targetObjAndField = mapSrcTarget.get(key);
                        String targetObject = targetObjAndField.subStringBefore(':');
                        String targetField = targetObjAndField.substringAfter(':');
                        if(String.isEmpty(targetObject) || String.isEmpty(targetField)) continue;

                        targetField = targetField.trim();
                        system.debug('targetObject----'+ targetObject);

                        if( targetObject.trim() == LeaseWareUtils.getNamespacePrefix()+'Aircraft__c'){

                           
                            if(ac !=null){
                                try{
                                    system.debug('targetField-----' + targetField);
                                    system.debug('source field---' + sourceField);
                                    if(sourceField.contains('__r')){
                                        String key1 = sourceField.substringBefore('.');
                                        String key2 = sourceField.substringAfter('.');
                                        if(curRec.getSObject(key1) !=null) {
                                            ac.put(targetField,curRec.getSObject(key1).get(key2));
                                        } 
                                    }else{
                                         system.debug('curRec--'+curRec);
                                         ac.put(targetField,curRec.get(sourceField));
                                    }
                                }catch(Exception e){
                                    resWrap.errorMessage += '\n'+ targetObjAndField +':'+e.getMessage();
                                   
                                }
                                
                            }
                        }
                        system.debug('curRec--'+curRec);
                        if(targetObject.trim() == LeaseWareUtils.getNamespacePrefix()+'Lease__c'){
                            system.debug('targetField Inside Lease-----' + targetField);
                            try{
                                if(sourceField.contains('__r')){
                                    String key1 = sourceField.substringBefore('.');
                                    String key2 = sourceField.substringAfter('.');
                                    if(curRec.getSObject(key1) !=null) {
                                        system.debug('curRec.getSObject(key1).get(key2)----'+curRec.getSObject(key1).get(key2));
                                        leaseRec.put(targetField,curRec.getSObject(key1).get(key2));
                                    } 
                                }else{
                                    system.debug('sourceField----'+sourceField);
                                    system.debug('curRec.get(sourceField)----'+curRec.get(sourceField));
                                    if(curRec.get(sourceField)!=null)
                                    leaseRec.put(targetField,curRec.get(sourceField));
                                    
                                }
                            }catch(Exception e){
                                system.debug('Error----------------'+ e.getMessage());
                                resWrap.status='ERROR';
                                resWrap.errorMessage += '\n'+ targetObjAndField +' : '+e.getMessage();
                               
                            }
                            
                        }
                        
                    }
                    
            }
            
                
                if(ac!=null ) {
                    system.debug('asset ---'+ac);
                    try{
                       
                        Leasewareutils.clearFromTrigger();
                        upsert ac; 
                        system.debug('Asset Inserted----'+ac.id);
                        curRec.Aircraft__c = ac.Id;
                        if(leaseRec!=null){
                            leaseRec.Aircraft__c = ac.id;
                        }
                       
                        resWrap.assetName =ac.Id;
                        
                        system.debug('curRec.Aircraft__c---'+ curRec.Aircraft__c);
                    }catch(Exception e){
                        system.debug('Exception e----'+ e.getMessage());
                        String exMessage = e.getMessage();
                        //In case of duplicate value add the same asset link on AID
                        if(exMessage.contains('DUPLICATE_VALUE')){
                            String assetId = exMessage.substringBetween('record with id: ',':');
                            system.debug('asset Id ----' + assetId);
                            curRec.Aircraft__c = assetId;
                            if(leaseRec!=null){
                                leaseRec.Aircraft__c = assetId;
                            }

                        }else{
                            resWrap.status='ERROR';
                            String customValidationString = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
                            if(exMessage.contains(customValidationString)){
                                Integer index1 = exMessage.lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                                Integer index2 = exMessage.indexOf(': [');
                                exMessage= exMessage.substring(index1+customValidationString.length()+1,index2);
                            }
                            else if(exMessage.startswith('Invalid field')){
                               exMessage ='The field set up in the' + exMessage.substringAfter('Invalid field')  +' is not available. Please check the api details.';
                            }
                            resWrap.errorMessage += '\n Error in Asset Insert/Update: '+exMessage;
                            

                        }
                    }
                }
               
                
                try{
                    system.debug('Upsert Lease----');
                    if(leaseRec !=null){
                        upsert leaseRec; 
                        system.debug('leaseRec----'+ leaseRec);
                        curRec.Lease__c = leaseRec.id;
                        resWrap.leaseId = leaseRec.id;
                        
                    }
                }catch(Exception e){
                   
                    resWrap.status = 'ERROR';
                    String exMessage = e.getMessage();
                    String customValidationString = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
                    if(exMessage.contains(customValidationString)){
                        Integer index1 = exMessage.lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                        Integer index2 = exMessage.indexOf(': [');
                        exMessage= exMessage.substring(index1+customValidationString.length()+1,index2);
                    }
                    if(exMessage.startswith('Invalid field')){
                        exMessage =' The field set up in the' + exMessage.substringAfter('Invalid field')  +' is not available. Please check the api details.';
                    }
                    if(exMessage.contains('REQUIRED_FIELD_MISSING')){
                        exMessage =' Required Field Missing: '+ exMessage.substringBetween('Required fields are missing:', ':');
                    }
                    if(exMessage.contains('Leases cannot overlap for more than one day')){
                        exMessage = ' Leases cannot overlap for more than one day. Conflict between existing lease and the new lease to be created';
                    }
                    resWrap.leaseName = '';
                    resWrap.errorMessage += '\n Error in Lease Insert/Update :'+exMessage;
                }

                
                                   
                system.debug('leaseRec----'+ leaseRec);
                LeaseWareUtils.setTriggersAndWFsOff();
                update curRec; // This is to stamp lease id, asset id on AID   // Update AID
                LeaseWareUtils.setTriggersAndWFsOn();

                Aircraft_Proposal__c aidRec = [select Id, Name,Lease__c,Lease__r.Name,Aircraft__c,Aircraft__r.Name from Aircraft_Proposal__c where Id = :aid];
                resWrap.aidName = aidRec.name;
                resWrap.leaseId = aidRec.Lease__c;
               
                if( aidRec.Lease__c!=null){
                    resWrap.leaseName = aidRec.Lease__r.Name;
                   
                }
                if( aidRec.Aircraft__c!=null){
                    resWrap.assetId = aidRec.Aircraft__c;
                    resWrap.assetName = aidRec.Aircraft__r.Name;
                     
                    
                }
               system.debug('resWrap------'+ resWrap);
            }catch(Exception e){
                system.debug('Exeption ----' + e.getMessage() +'-'+ e.getLineNumber());
                resWrap.status='ERROR';
                resWrap.errorMessage = e.getMessage();
                LeaseWareUtils.createExceptionLog( e, 'Exception Occured :' + e.getMessage(), null, null,'CONVERT DEALS TO LEASES: ',false);  
            }
            LeaseWareUtils.insertExceptionLogs();
           
            return resWrap;
        
    }
    
    
    
    private static void getMappingRecords(String recordTypeName, map<String,List<String>> mapSourceObjFields, map<String,String> mapSrcTarget ){
        List<Custom_Lookup__c> clList = [SELECT id, Name, Lookup_Type__c, Sub_LookupType__c, Source__c,Target__c,Details__c, Description__c, Sub_LookupType2__c, Sub_LookupType3__c FROM Custom_Lookup__c WHERE Lookup_Type__c = 'CONVERT_DEALS_TO_LEASES' AND Sub_LookupType__c= :recordTypeName and Active__c=true];
        system.debug('cllist----' + clList.size());
        List<String> srcFieldList;
        
        for(Custom_Lookup__c cl : clList){
            system.debug('Source----' + cl.Source__c + '------Target-------' + cl.Target__c);
            if(String.isBlank(cl.Source__c) || String.isBlank(cl.Target__c)) continue;
            mapSrcTarget.put(cl.Source__c,cl.Target__c);
            String srcObject = cl.Source__c.subStringBefore(':');
            String srcField = cl.Source__c.subStringAfter(':');
            if(String.isEmpty(srcObject) || String.isEmpty(srcField))continue;
            srcObject = srcObject.trim();
            srcField = srcField.trim();
           
            //Map of Source Object and Fields
            if(mapSourceObjFields.containsKey(srcObject)){
                srcFieldList = mapSourceObjFields.get(srcObject);
            }else{
                srcFieldList = new List<String>();
            }
            srcFieldList.add(srcField);
            mapSourceObjFields.put(srcObject , srcFieldList);
        }
    }


    private static String getFieldsToBeAddedForQuery(map<String,List<String>> mapSourceObjFields){
        Set<String> fieldsToBeAddForQuery= new Set<String>();
        String defaultFieldListForDealType= 'Aircraft__c, Type_Variant__c,World_Fleet_Aircraft__c,World_Fleet_Aircraft__r.SerialNumber__c,World_Fleet_Aircraft__r.AircraftType__c,World_Fleet_Aircraft__r.AircraftVariant__c,Serial_Number__c,Date_of_Manufacture__c,Aircraft__r.Lease__c,Marketing_Activity__r.Prospect__c,Term_Mos__c,Lease__c' ;//The default field details to be captured from AID
        
        String fieldListForQuery;//  defaultFieldListForDealType + fields mentioned in mapping table
        List<String> srcFieldsList = new List<String>();
        String aidObjectName = LeasewareUtils.getNamespacePrefix() + 'Aircraft_Proposal__c';
        if(mapSourceObjFields.containsKey(aidObjectName)){
            //add the fields to the defaultFieldListForLeaseDealType
            srcFieldsList = mapSourceObjFields.get(aidObjectName);
        } 

        if(srcFieldsList.isEmpty()) {
            fieldListForQuery = defaultFieldListForDealType;
        }
        else  {
            fieldsToBeAddForQuery.addAll(defaultFieldListForDealType.split(','));
            fieldsToBeAddForQuery.addAll(srcFieldsList);
            fieldListForQuery = String.join(new List<String>(fieldsToBeAddForQuery), ',');
            
        }
        system.debug('fieldListForQuery----' + fieldListForQuery);
        return fieldListForQuery;
    }


    

    public static boolean validateOnDealStatus(String customSettingDealStatus,String currentDealStatus){
        system.debug('customSettingDealStatus----'+ customSettingDealStatus);
        system.debug('currentDealStatus----'+ currentDealStatus);
        boolean updatePortfolioAllowed=false;
        try {
            String sobjectName = leasewareutils.getNamespacePrefix()+'marketing_Activity__c';
            String fieldName = leasewareutils.getNamespacePrefix()+'deal_status__c';
            map<String,Integer> mapDealStatus = new map<String,Integer>();
            List<PicklistEntry> picklistValues = Schema.getGlobalDescribe().get(sobjectName).getDescribe().fields.getMap().get(fieldName).getDescribe().getPicklistValues();
            system.debug('picklistValues--------'+picklistValues);
            Integer count=0;
            for( Schema.PicklistEntry pickListVal : picklistValues){
                count++;
                System.debug(pickListVal.getLabel() +' -------------'+pickListVal.getValue());
                mapDealStatus.put(pickListVal.getLabel(),count);
            }  
            system.debug('mapDealStatus.get(customSettingDealStatus)----'+mapDealStatus.get(customSettingDealStatus));
            system.debug('mapDealStatus.get(currentDealStatus)----'+mapDealStatus.get(currentDealStatus));
            if(mapDealStatus.get(customSettingDealStatus)<= mapDealStatus.get(currentDealStatus)){
                updatePortfolioAllowed = true;
            }
            system.debug('updatePortfolioAllowed----'+updatePortfolioAllowed);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return updatePortfolioAllowed;
    }

    /***********************************Shweta  Code End*********************************************** */
    public class FieldStructure{
        @AuraEnabled public String apiName {get; set;}
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public Object value {get; set;}
        @AuraEnabled public String referenceId {get; set;}
        @AuraEnabled public String fieldType {get; set;}
        
        public FieldStructure() {
            value = null;
        }
    }

    public class AssetInDealWrapper {
        @AuraEnabled public String aidId {get; set;}
        @AuraEnabled public List<FieldStructure> fields {get; set;}
        @AuraEnabled public boolean isSelect {get; set;}
        public AssetInDealWrapper(){
            isSelect = false;
        }
    }
    
    public class ResultSetWrapper {
        @AuraEnabled public String aidId {get; set;}
        @AuraEnabled public String aidName {get; set;}
        @AuraEnabled public String assetId {get; set;}
        @AuraEnabled public String assetName {get; set;}
        @AuraEnabled public String leaseId {get; set;}
        @AuraEnabled public String leaseName {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String errorMessage {get; set;}
    }
}