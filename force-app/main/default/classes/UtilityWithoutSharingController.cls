public without sharing class UtilityWithoutSharingController {

    public static final String encryptionKey = 'leaseworks@sfdc1';
    
    //to encrypt the url parameters.
    public static String encodeData(String param){
        Blob key = Blob.valueOf(encryptionKey);
        string encodedCipherText = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES128', key, Blob.valueOf(param)));
        return encodingUtil.URLEncode(encodedCipherText,'UTF-8');
    }
    
    //to decrypt the url parameters.
    public static String decodeData(String param){
        Blob key = Blob.valueOf(encryptionKey);
        Blob blobData = EncodingUtil.base64Decode(param);		
        Blob decryptedBlob = Crypto.decryptWithManagedIV('AES128', key, blobData);		
        return decryptedBlob.toString();
    }
    
    //get community prefix if any.
    public static String getCommunityPrefix(){
        String communityPrefix = '/';
        //Try catch added for cases when code is executed outside of Community context
        try {
            communityPrefix = Site.getPathPrefix().removeEndIgnoreCase('/s');
        } catch (Exception e) {}
        return communityPrefix;
    }
}