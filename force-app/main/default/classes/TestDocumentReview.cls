/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDocumentReview {

    static testMethod void CRUDTest() {

    	map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        TestLeaseworkUtil.insertAircraft(mapInOut);       	
        //1 Create Document template
        Document_Template__c DT1 = new Document_Template__c(Name='Dummy',Organization_Name__c= (Id) mapInOut.get('Lessor__c.Id'), Type__c = 'Sale');
        LeaseWareUtils.clearFromTrigger();insert DT1;

        Document_Template_Checklist__c DTC1 = new Document_Template_Checklist__c(Name='Dummy',Document_Template__c= DT1.Id
        			, Category__c = 'Aircraft Maintenance and Status 1'
        			,Sub_Category__c = 'Specifaction Status Sheet 1.1');
        LeaseWareUtils.clearFromTrigger();insert DTC1;
        Document_Template_Checklist__c DTC2 = new Document_Template_Checklist__c(Name='Dummy',Document_Template__c= DT1.Id
        			, Category__c = 'Aircraft Maintenance and Status 1'
        			,Sub_Category__c = 'Specifaction Status Sheet 1.2');
        LeaseWareUtils.clearFromTrigger();insert DTC2;
        Document_Template_Checklist__c DTC3 = new Document_Template_Checklist__c(Name='Dummy',Document_Template__c= DT1.Id
        			, Category__c = 'Aircraft Maintenance and Status 3'
        			,Sub_Category__c = 'Specifaction Status Sheet 3.1');
        LeaseWareUtils.clearFromTrigger();insert DTC3;                
        
        AC_Document_Review__c DR1 = new AC_Document_Review__c(Name='Dummy',Aircraft__c = (Id)mapInOut.get('Aircraft__c.Id') 
        			,Review_Type__c = 'Sale'
        			,Document_Template__c =DT1.Id );
        LeaseWareUtils.clearFromTrigger();insert 	DR1;
        
        DR1.Document_Template__c = null;
        LeaseWareUtils.clearFromTrigger();update 	DR1;	
        
        DR1.Document_Template__c = DT1.Id;
        LeaseWareUtils.clearFromTrigger();update 	DR1;       
        
        LeaseWareUtils.clearFromTrigger();delete  DR1;
        
        LeaseWareUtils.clearFromTrigger(); undelete DR1;
        
    }
}