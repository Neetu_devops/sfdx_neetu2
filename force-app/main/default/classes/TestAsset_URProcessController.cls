@isTest
public class TestAsset_URProcessController {
	
    @testSetup 
        static void setup() {
            
			Date StartDate = Date.newInstance(2009,01,01);
            
            Lessor__c testLessor = new Lessor__c(Name='TestLessor', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
				Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
				Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today()
                                     ,Utilization_Staging_Purge_After__c='1');
            insert testLessor;
            
            Operator__c testOperator = new Operator__c(
                Name = 'SA Airlines',                   
                Status__c = 'Approved',                          
                Rent_Payments_Before_Holidays__c = false,         
                Revenue__c = 0.00,                                
                Current_Lessee__c = true                         
            );
            insert testOperator;
            
            Lease__c leaseRecord = new Lease__c(
                Name = 'Testing',
                Lease_Start_Date_New__c = StartDate,
                Lease_End_Date_New__c = StartDate.addYears(2),
                Lessee__c = testOperator.Id
            );
            insert leaseRecord;
            
            Aircraft__c testAircraft = new Aircraft__c(
                Name = 'Test',                    
                MSN_Number__c = '2821',                       
                Date_of_Manufacture__c = system.today(),
                Aircraft_Type__c = '737',                         
                Aircraft_Variant__c = '700',                    
                Marked_For_Sale__c = false,                  
                Alert_Threshold__c = 10.00,                   
                Status__c = 'Available',                  
                Awarded__c = false,                                              
                TSN__c = 1.00,                               
                CSN__c = 1,
                Last_CSN_Utilization__c = 1.00,
               Last_TSN_Utilization__c = 1
            );
            insert testAircraft;
            
            testAircraft.Lease__c = leaseRecord.Id;
            update testAircraft;
            
            leaseRecord.Aircraft__c = testAircraft.ID;
            update leaseRecord;  
            
            
            List<Constituent_Assembly__c> constituentList = new List<Constituent_Assembly__c>();
            for(Integer i=0;i<3;i++){
                Constituent_Assembly__c consituentRecord = new Constituent_Assembly__c();
                consituentRecord.Serial_Number__c = '2';
                //consituentRecord.CSLV__c = 2+i;
                consituentRecord.CSN__c = 3;
                //consituentRecord.TSLV__c = 4+i;
                consituentRecord.TSN__c = 5;
                consituentRecord.Last_CSN_Utilization__c = 3;
                consituentRecord.Last_TSN_Utilization__c = 5;
                consituentRecord.Attached_Aircraft__c = testAircraft.Id;
                consituentRecord.Name ='Testing';
                if (i == 0){
                    consituentRecord.Type__c ='Airframe'; 
                }
                else if (i == 1){
                    consituentRecord.Type__c ='Engine 1'; 
                }
                else{
                    consituentRecord.Type__c ='Landing Gear - Right Main'; 
                }
                
                constituentList.add(consituentRecord);
            }
            LWGlobalUtils.setTriggersflagOn(); 
            insert constituentList;
            LWGlobalUtils.setTriggersflagOff();
            
            
            // Set up Maintenance Program
            Maintenance_Program__c mp = new Maintenance_Program__c(
                name='Test',Current_Catalog_Year__c='2020');
            insert mp;
            // map of Maintenance Program Event
            map<string,id> mapMPE = new map<string,id>();       
            for(Maintenance_Program_Event__c curMPE: [select id,Assembly__c from Maintenance_Program_Event__c where Maintenance_Program__c =: mp.Id]){
                 mapMPE.put(curMPE.Assembly__c,curMPE.id);
            }
            // Set up Project Event
            List<Assembly_Event_Info__c> ListProjEvent = new List<Assembly_Event_Info__c>();       
            for(Integer i=0;i<3;i++){
                Assembly_Event_Info__c ProjEvent = new Assembly_Event_Info__c();
                if (i == 0){
                    ProjEvent.Name= 'Airframe';
                    ProjEvent.Event_Cost__c= 100;
                    ProjEvent.Constituent_Assembly__c= constituentList[0].id;
                    ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[0].Type__c);
                }
                else if (i == 1){
                    ProjEvent.Name= 'Engine 1';
                    ProjEvent.Event_Cost__c= 100;
                    ProjEvent.Constituent_Assembly__c= constituentList[1].id;
                    ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[1].Type__c);
                }
                else{
                    ProjEvent.Name= 'Landing Gear - Right Main';
                    ProjEvent.Event_Cost__c= 100;
                    ProjEvent.Constituent_Assembly__c= constituentList[2].id;
                    ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[2].Type__c);
                }
                ListProjEvent.add(ProjEvent);
            }
            
            LWGlobalUtils.setTriggersflagOn(); 
            insert ListProjEvent;
            LWGlobalUtils.setTriggersflagOff();
			
            Date URDate1 =  StartDate.addMonths(3).addDays(-1);
            Date URDate2 = URDate1.addMonths(1);
            Date URDate3 = URDate2.addMonths(1);
            Date URDate4 = URDate3.addMonths(1);
            System.debug(' URDate1 '+URDate1);
            System.debug(' URDate2 '+URDate2);
            System.debug(' URDate3 '+URDate3);
            System.debug(' URDate4 '+URDate4);
			List <Utilization_Report_Staging__c> URList = new List<Utilization_Report_Staging__c>();
             Utilization_Report_Staging__c UR1 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '200',
                                                                             Lease_UR_Staging__c = leaseRecord.Id,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                            
																			 Month_Ending__c = URDate1,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            
        	URList.add(UR1);
            Utilization_Report_Staging__c UR2 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
																			 Month_Ending__c = URDate2,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            URList.add(UR2);
            Utilization_Report_Staging__c UR3 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '100',
																			 Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
                                                                             Month_Ending__c = URDate3,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
			URList.add(UR3);	
            Utilization_Report_Staging__c UR4 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
																			 Month_Ending__c = URDate4,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            URList.add(UR4);
			insert URList;
            
    }
    
    
    @isTest
    static void callProcessUtilization () {
        String expectedResult = 'Processing of the utilization is complete.';
        Test.startTest();
        String actualResult = Asset_URProcessController.processUtilization();
        Test.stopTest();
        System.assertEquals(actualResult, expectedResult);
    }
}