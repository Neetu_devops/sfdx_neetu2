@isTest
private class ForecastDashboardControllerTest {
    @isTest
    static void testMethod1(){
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'LOC',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January',
            Ratio_Table__c = ratioTable.ID);
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Rent_Escalation_Rate__c = 50;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Return_Condition_Applicability__c ='From the last event';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSLV__c = 100;
        constitutentAssembly.CSLV__c =200;
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        constitutentAssembly.CSO__c = 14;
        constitutentAssembly.TSO__c = 12;
        constitutentAssembly.Auto_Sync__c = 'NO';
        insert constitutentAssembly;
        
        Assembly_RC_Info__c recAssembly = new Assembly_RC_Info__c(
            name='Test',
            RC_FC__c=23, 
            Event_Type__c= 'Performance Restoration',
            RC_FH__c =12,
            RC_Months__c = 2,
            Assembly_Type__c = 'Engine 1',
            Return_Condition__c = rec.Id
        );
        insert recAssembly;
        
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
        
        Assembly_Event_Info__c assemblyEventInfo = new Assembly_Event_Info__c( 
            Name = 'Test Info',
            Event_Cost__c = 20,
            Maintenance_Program_Event__c =  mpe.Id,
            Constituent_Assembly__c = constitutentAssembly.Id);
        insert assemblyEventInfo;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Type__c = 'Engine 1', 
            Event_Type__c = 'Performance Restoration', 
            Base_MRR__c =20,
            Assembly_Event_Info__c = assemblyEventInfo.Id,
            Outstanding_Receipts__c=30,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
            Last_Escalation_Date__c = System.today());
        insert assemblyMR;
        
        try{
            ForcastDashboardController.getConstituentAssembly(aircraft.Id,'Forecast',false);
            String si = JSON.serialize(scenarioInp);
            //String sci = JSON.serialize(scenarioComponentInp);
            ForcastDashboardController.saveTimeSpan(si);
            //ForcastDashboardController.saveScenarioCmpInp(si,sci);
            ForcastDashboardController.getScenarioInput(scenarioInp.Id);
            ForcastDashboardController.getAllFieldLabels();
            ForcastDashboardController.ScenarioInputWrapper scenrioInpWrapper = new ForcastDashboardController.ScenarioInputWrapper(scenarioInp);
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
    
    @isTest
    static void testMethodForLLp(){
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'LOC',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January',
            Ratio_Table__c = ratioTable.ID);
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Rent_Escalation_Rate__c = 50;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Return_Condition_Applicability__c ='From the last event';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Airframe').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Assembly_RC_Info__c recAssembly = new Assembly_RC_Info__c(
            name='Test',
            RC_FC__c=23, 
            Event_Type__c= '4C/6Y',
            RC_FH__c =12,
            RC_Months__c = 2,
            Assembly_Type__c = 'Airframe',
            Return_Condition__c = lease.Return_Condition__c
        );
        
        insert recAssembly;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Airframe';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSLV__c = 100;
        constitutentAssembly.CSLV__c =200;
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        constitutentAssembly.CSO__c = 14;
        constitutentAssembly.TSO__c = 12;
        constitutentAssembly.Auto_Sync__c = 'NO';
        insert constitutentAssembly;
        
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c=mp.Id,
            Event_Type_Global__c = '4C/6Y'
        );
        insert mpe;
        
        Assembly_Event_Info__c assemblyEventInfo = new Assembly_Event_Info__c( 
            Name = 'Test Info', 
            Event_Cost__c = 20,
            Maintenance_Program_Event__c = mpe.Id,
            Constituent_Assembly__c = constitutentAssembly.Id);
        insert assemblyEventInfo;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Type__c = 'Airframe', 
            Event_Type__c = '4C/6Y', 
            Base_MRR__c =20,
            Assembly_Event_Info__c = assemblyEventInfo.Id,
            Outstanding_Receipts__c=30,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
            Last_Escalation_Date__c = System.today());
        insert assemblyMR;
        
        constitutentAssembly.TSO__c = 30;
        update constitutentAssembly;
        
        try{
            ForcastDashboardController.getConstituentAssembly(aircraft.Id,'Forecast',true);
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
    
    @isTest
    static void testMethodForAirframe(){
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'LOC',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January',
            Ratio_Table__c = ratioTable.ID);
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Rent_Escalation_Rate__c = 50;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Return_Condition_Applicability__c ='From the last event';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSLV__c = 100;
        constitutentAssembly.CSLV__c =200;
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        constitutentAssembly.CSO__c = 14;
        constitutentAssembly.TSO__c = 12;
        constitutentAssembly.Auto_Sync__c = 'NO';
        insert constitutentAssembly;
        
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c=mp.Id,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
        
        Assembly_Event_Info__c assemblyEventInfo = new Assembly_Event_Info__c( 
            Name = 'Test Info', 
            Event_Cost__c = 20,
            Maintenance_Program_Event__c = mpe.Id,
            Constituent_Assembly__c = constitutentAssembly.Id);
        insert assemblyEventInfo;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Type__c = 'Engine 1 LLP', 
            Event_Type__c = 'Replacement', 
            Base_MRR__c =20,
            Assembly_Event_Info__c = assemblyEventInfo.Id,
            Outstanding_Receipts__c=30,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
            Last_Escalation_Date__c = System.today());
        insert assemblyMR;
        
        constitutentAssembly.TSO__c = 30;
        update constitutentAssembly;
        
        try{
            ForcastDashboardController.getConstituentAssembly(aircraft.Id,'Forecast',true);
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
    
    @isTest
    static void testMethod2(){
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;

        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'LOC',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January',
            Ratio_Table__c = ratioTable.ID);
        insert lease;

        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Rent_Escalation_Rate__c = 50;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Return_Condition_Applicability__c ='From the last event';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Scenario_Component_Input__c scenarioComponentInp = new Scenario_Component_Input__c();
        scenarioComponentInp.Name = 'Test Scenario Component Input';
        scenarioComponentInp.FH__c = 20;
        scenarioComponentInp.FC__c =21;
        scenarioComponentInp.Forecast_Type__c = 'Current Lease';
        scenarioComponentInp.Type__c ='Engine';
        scenarioComponentInp.User_Defined_Utilization_FH__c =True;
        scenarioComponentInp.Scenario_Input__c = scenarioInp.Id;
        scenarioComponentInp.Operating_Environment_Impact__c = True;
        insert scenarioComponentInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSLV__c = 100;
        constitutentAssembly.CSLV__c =200;
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        constitutentAssembly.CSO__c = 14;
        constitutentAssembly.TSO__c = 12;
        constitutentAssembly.Auto_Sync__c = 'NO';
        insert constitutentAssembly;
        
        Fx_Assembly_Event_Input__c assemblyEvent = new Fx_Assembly_Event_Input__c(); 
        assemblyEvent.Name = 'Assembly Evnt Input Test';
        assemblyEvent.Interval_1_Hours__c = 18; 
        assemblyEvent.Interval_2_Cycles__c = 32;
        assemblyEvent.Event_cost__c = 50;
        assemblyEvent.Event_Type__c = 'Performance Restoration';
        assemblyEvent.Interval_2_Hours__c = 11; 
        assemblyEvent.Interval_1_Cycles__c = 12; 
        assemblyEvent.Interval_1_Months__c = 80;
        assemblyEvent.Last_Escalation_Date__c = System.today();
        assemblyEvent.Current_MRR__c = 30;
        assemblyEvent.Base_MRR__c = 11;
        assemblyEvent.Interval_2_Months__c = 2; 
        assemblyEvent.RC_FC__c = 21; 
        assemblyEvent.RC_FH__c = 12; 
        assemblyEvent.RC_Months__c = 21;
        assemblyEvent.Fx_Assembly_Input__c = scenarioComponentInp.Id;
        assemblyEvent.Annual_Escalation__c =1; 
        assemblyEvent.Starting_MR_Balance__c = 12;
        assemblyEvent.Assembly__c = constitutentAssembly.Id;
        insert assemblyEvent;
        
        
        Assembly_Event_Info__c assemblyEventInfo = new Assembly_Event_Info__c( 
            Name = 'Test Info',
            Event_Cost__c = 20,
            Constituent_Assembly__c = constitutentAssembly.Id);
        insert assemblyEventInfo;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Type__c = 'Engine 1', 
            Event_Type__c = 'Replacement', 
            Base_MRR__c =20,
            Assembly_Event_Info__c = assemblyEventInfo.Id,
            Outstanding_Receipts__c=30,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
            Last_Escalation_Date__c = System.today());
        
        Fx_SV_Override__c sv = new Fx_SV_Override__c();
        sv.Fx_Assembly_Event_Input__c = assemblyEvent.Id;
        sv.Name = 'Test';
        sv.SV_Number__c =20;
        insert sv;
        
        String si = JSON.serialize(scenarioInp);
        String sci = JSON.serialize(scenarioComponentInp);
        
        try{
            ForcastDashboardController.saveTimeSpan(si);
            ForcastDashboardController.getScenarioInput(scenarioInp.Id);
            ForcastDashboardController.saveScenarioCmpInp(si,sci);
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
        scenarioComponentInp.Forecast_Type__c = 'Lease Extension';
        update scenarioComponentInp;
        
        try{           
            ForcastDashboardController.deleteScenarioRecord(scenarioInp.Id);
            ForcastDashboardController.getAllFieldLabels();
            ForcastDashboardController.ScenarioInputWrapper scenrioInpWrapper = new ForcastDashboardController.ScenarioInputWrapper(scenarioInp);
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
    
    @isTest
    static void testMethod3(){
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;

        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'LOC',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January',
            Ratio_Table__c = ratioTable.ID);
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Rent_Escalation_Rate__c = 50;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Return_Condition_Applicability__c ='From the last event';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Scenario_Component_Input__c scenarioComponentInp = new Scenario_Component_Input__c();
        scenarioComponentInp.Name = 'Engine';
        scenarioComponentInp.FH__c = 20;
        scenarioComponentInp.FC__c =21;
        scenarioComponentInp.Forecast_Type__c = 'Current Lease';
        scenarioComponentInp.Type__c ='Engine';
        scenarioComponentInp.User_Defined_Utilization_FH__c =True;
        scenarioComponentInp.Scenario_Input__c = scenarioInp.Id;
        scenarioComponentInp.Operating_Environment_Impact__c = True;
        insert scenarioComponentInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSLV__c = 100;
        constitutentAssembly.CSLV__c =200;
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        constitutentAssembly.CSO__c = 14;
        constitutentAssembly.TSO__c = 12;
        constitutentAssembly.Auto_Sync__c = 'NO';
        insert constitutentAssembly;
        
        Fx_Assembly_Event_Input__c assemblyEvent = new Fx_Assembly_Event_Input__c(); 
        assemblyEvent.Name = 'Engine 1 Performance Restoration';
        assemblyEvent.Interval_1_Hours__c = 18; 
        assemblyEvent.Interval_2_Cycles__c = 32;
        assemblyEvent.Event_cost__c = 50;
        assemblyEvent.Event_Type__c = 'Performance Restoration';
        assemblyEvent.Interval_2_Hours__c = 11; 
        assemblyEvent.Interval_1_Cycles__c = 12; 
        assemblyEvent.Interval_1_Months__c = 80;
        assemblyEvent.Last_Escalation_Date__c = System.today();
        assemblyEvent.Current_MRR__c = 30;
        assemblyEvent.Base_MRR__c = 11;
        assemblyEvent.Interval_2_Months__c = 2; 
        assemblyEvent.RC_FC__c = 21; 
        assemblyEvent.RC_FH__c = 12; 
        assemblyEvent.RC_Months__c = 21;
        assemblyEvent.Fx_Assembly_Input__c = scenarioComponentInp.Id;
        assemblyEvent.Annual_Escalation__c =1; 
        assemblyEvent.Starting_MR_Balance__c = 12;
        assemblyEvent.Assembly__c = constitutentAssembly.Id;
        insert assemblyEvent;
        
        
        Assembly_Event_Info__c assemblyEventInfo = new Assembly_Event_Info__c( 
            Name = 'Test Info',
            Event_Cost__c = 20,
            Constituent_Assembly__c = constitutentAssembly.Id);
        insert assemblyEventInfo;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Type__c = 'Engine 1', 
            Event_Type__c = 'Replacement', 
            Base_MRR__c =20,
            Assembly_Event_Info__c = assemblyEventInfo.Id,
            Outstanding_Receipts__c=30,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
            Last_Escalation_Date__c = System.today());
        
        Fx_SV_Override__c sv = new Fx_SV_Override__c();
        sv.Fx_Assembly_Event_Input__c = assemblyEvent.Id;
        sv.Name = 'Test';
        sv.SV_Number__c =20;
        insert sv;
        
        try{
            ForcastDashboardController.getScenarioInput(scenarioInp.Id);
            
            scenarioComponentInp.Forecast_Type__c = 'Lease Extension';
            update scenarioComponentInp;
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
        
        try{
            ForcastDashboardController.updateCostPerFH(scenarioInp.Id);            
            //ForcastDashboardController.createMonthlyDataForAssembly(scenarioInp.Id,constitutentAssembly.Id);
            ForcastDashboardController.ScenarioInputWrapper scenrioInpWrapper = new ForcastDashboardController.ScenarioInputWrapper(scenarioInp);
            ForcastDashboardController.deleteShopVisitRec(sv.Id,assemblyEvent.Id);
            ForcastDashboardController.getAssembliesForMonthlyData(scenarioInp.Id);
            ForcastDashboardController.restoreAssemblyEventInputData(scenarioInp.Id);
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
    
    @isTest
    static void testMethod4(){
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;

        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'LOC',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January',
            Ratio_Table__c = ratioTable.ID);
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;

        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Rent_Escalation_Rate__c = 50;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Return_Condition_Applicability__c ='From the last event';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Scenario_Component_Input__c scenarioComponentInp = new Scenario_Component_Input__c();
        scenarioComponentInp.Name = 'Engine';
        scenarioComponentInp.FH__c = 20;
        scenarioComponentInp.FC__c =21;
        scenarioComponentInp.Forecast_Type__c = 'Current Lease';
        scenarioComponentInp.Type__c ='Engine';
        scenarioComponentInp.User_Defined_Utilization_FH__c =True;
        scenarioComponentInp.Scenario_Input__c = scenarioInp.Id;
        scenarioComponentInp.Operating_Environment_Impact__c = True;
        insert scenarioComponentInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSLV__c = 100;
        constitutentAssembly.CSLV__c =200;
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        constitutentAssembly.CSO__c = 14;
        constitutentAssembly.TSO__c = 12;
        constitutentAssembly.Auto_Sync__c = 'NO';
        insert constitutentAssembly;
        
        Fx_Assembly_Event_Input__c assemblyEvent = new Fx_Assembly_Event_Input__c(); 
        assemblyEvent.Name = 'Engine 1 Performance Restoration';
        assemblyEvent.Interval_1_Hours__c = 18; 
        assemblyEvent.Interval_2_Cycles__c = 32;
        assemblyEvent.Event_cost__c = 50;
        assemblyEvent.Event_Type__c = 'Performance Restoration';
        assemblyEvent.Interval_2_Hours__c = 11; 
        assemblyEvent.Interval_1_Cycles__c = 12; 
        assemblyEvent.Interval_1_Months__c = 80;
        assemblyEvent.Last_Escalation_Date__c = System.today();
        assemblyEvent.Current_MRR__c = 30;
        assemblyEvent.Base_MRR__c = 11;
        assemblyEvent.Interval_2_Months__c = 2; 
        assemblyEvent.RC_FC__c = 21; 
        assemblyEvent.RC_FH__c = 12; 
        assemblyEvent.RC_Months__c = 21;
        assemblyEvent.Fx_Assembly_Input__c = scenarioComponentInp.Id;
        assemblyEvent.Annual_Escalation__c =1; 
        assemblyEvent.Starting_MR_Balance__c = 12;
        assemblyEvent.Assembly__c = constitutentAssembly.Id;
        insert assemblyEvent;
        
        
        Assembly_Event_Info__c assemblyEventInfo = new Assembly_Event_Info__c( 
            Name = 'Test Info',
            Event_Cost__c = 20,
            Constituent_Assembly__c = constitutentAssembly.Id);
        insert assemblyEventInfo;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Type__c = 'Engine 1', 
            Event_Type__c = 'Replacement', 
            Base_MRR__c =20,
            Assembly_Event_Info__c = assemblyEventInfo.Id,
            Outstanding_Receipts__c=30,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
            Last_Escalation_Date__c = System.today());
        
        try{
            ForcastDashboardController.isForcastAlreadyGenerated(scenarioInp.Id);
            ForcastDashboardController.getScenarioInput(scenarioInp.Id);
            ForcastDashboardController.restoreAssemblyEventInputData(scenarioInp.Id);
            //ForcastDashboardController.addShopVisitRow(assemblyEvent.Id, 0);
        }
        catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
}