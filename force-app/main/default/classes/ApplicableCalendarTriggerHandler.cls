public class ApplicableCalendarTriggerHandler implements ITrigger{
    
    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'ApplicableCalendarTriggerHandlerBefore';
    private final string triggerAfter = 'ApplicableCalendarTriggerHandlerAfter';
    
    private static set<Id> setLeasaeIDStatic = new set<Id>(); 
    public ApplicableCalendarTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
               
    }
     
    public void beforeUpdate()
    {
        
        
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
    	list<Applicable_Calendar__c> listAppCal = (list<Applicable_Calendar__c>)trigger.Old;
		//set LeaseID
		
		for(Applicable_Calendar__c curRec: listAppCal){
			setLeasaeIDStatic.add(curRec.Lease__c);
		}
        

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter); 
        HolidayInvoiceDueDateChange();       
         
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter); 
        HolidayInvoiceDueDateChange();
          
    }
     
    public void afterDelete()
    {
           updateDueDateonInvandRent(setLeasaeIDStatic);
    }

    public void afterUnDelete()
    {
            
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
    
    //After Update, After insert
	private void HolidayInvoiceDueDateChange(){
		system.debug('Holiday+');
		
		set<Id> listeffectedLeaseaforHoliday = new set<Id>();
		list<Applicable_Calendar__c> listAppCal = (list<Applicable_Calendar__c>)trigger.new;
		
		for(Applicable_Calendar__c curRec: listAppCal){
			listeffectedLeaseaforHoliday.add(curRec.Lease__c);
		}
		
		if(listeffectedLeaseaforHoliday.size()>0)
			updateDueDateonInvandRent(listeffectedLeaseaforHoliday);
		system.debug('Holiday-');
	}
    
    private void updateDueDateonInvandRent(set<Id> listeffectedLeaseaforHoliday)
	{
		//1. For Rent Schedule Updation
		list<Rent__c> listRentSchedule = [select Id from Rent__c where RentPayments__c in: listeffectedLeaseaforHoliday];
        try{
            if(!listRentSchedule.isEmpty()) Database.update(listRentSchedule,true);
        }catch(exception ex){
            LeaseWareUtils.createExceptionLog(ex,' Rent record could not be updated due to exception' ,'Rent__c','','Update',true);
        }
		
		//2. For Invoice Updation
		list<Invoice__c> listInvoice=[select Id from Invoice__c where Status__c NOT IN( 'Cancelled','Declined') and payment_status__c NOT IN ('Paid','Credited') And Lease__c in: listeffectedLeaseaforHoliday];
        try{
            if(!listInvoice.isEmpty()) Database.update(listInvoice,true);
		}catch(exception ex){
            LeaseWareUtils.createExceptionLog(ex,' Invoice record could not be updated due to exception' ,'Invoice__c','','Update',true);
        }
	}
	
   
       
}