@isTest
public class TestUnApproveUtilization {
	@isTest
    private static void validateUnApproveAction(){
        String aircraftMSN = '12345';
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                    Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                    Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
                    LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
         Aircraft__c aircraft = new Aircraft__c(MSN_Number__c=aircraftMSN,Aircraft_Type__c='737', Aircraft_Variant__c='300',
                                                TSN__c=100, CSN__c=50, 
            									Status__c='Available');
        insert aircraft; 
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        list<Custom_Lookup__c> listCustomLookup = [select id,name from Custom_Lookup__c] ;
        system.debug('listCustomLookup----'+listCustomLookup);
		
         Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
       
       	constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
       	constitutentAssembly.TSN__c = 100;
        constitutentAssembly.CSN__c = 200; 
      	insert constitutentAssembly;
        Date startDate = Date.newInstance(2018, 01, 01);
        Date endDate = Date.newInstance(2022, 12, 31);
        Id recordId = Schema.SObjectType.Lease__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        Lease__c lease = new Lease__c(Name='TestLease',Aircraft__c=aircraft.Id,Lease_Start_Date_New__c=startDate,
                                     Lease_End_Date_New__c=endDate,recordtypeid=recordId);
        insert lease;
        test.startTest();
     	Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='10',
            Airframe_Cycles_Landing_During_Month__c= 10,
            Aircraft__c = aircraft.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2018,01,31)
           
        );
       
       insert utilizationRecord;
         
        LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
        LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
        Utilization_Report__c trueUpUtilization = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='10',
            Airframe_Cycles_Landing_During_Month__c= 10,
            Aircraft__c = aircraft.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'True up Gross',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2018,01,31)
           
        );
        insert trueUpUtilization;
        LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
        LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
        test.stopTest();
        UnApproveUtilizationController.unapproveAction(trueUpUtilization.Id);
        
        Aircraft__c asset = [select Id,TSN__c,CSN__c from Aircraft__c ];
        system.assertEquals(110, asset.TSN__c);
        system.assertEquals(60, asset.CSN__c);
        
        //Ref:Reconciliation Studio
       
        Reconciliation_Schedule__c rsObj = new Reconciliation_Schedule__c();
        rsObj.Name = startDate.year()+' '+ datetime.newInstance(startDate.year(), startDate.month(),startDate.day()).format('MMM')+' to '+ endDate.addDays(-1).year() +' '+datetime.newInstance(endDate.addDays(-1).year(), endDate.addDays(-1).month(),endDate.addDays(-1).day()).format('MMM');
        rsObj.From_Date__c = Date.newInstance(2017,01,31);
        rsObj.To_Date__c = Date.newInstance(2018,01,31);
        rsObj.Status__c = 'Complete';
        rsObj.Disable_Invoice_Auto_Generation__c = true;
        rsObj.Lease__c = lease.Id;
        rsObj.Asset__c = aircraft.Id;
        rsObj.Reconciliation_Period__c = '12';
       
        insert rsObj;
        try{
    	UnApproveUtilizationController.unapproveAction(utilizationRecord.Id);     
        }catch(exception  e){
            System.debug(e.getMessage());
    }  
    }  
@IsTest
    public static void validateForLatestUtilization(){
      
        String aircraftMSN = '12345';
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                    Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                    Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
                    LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
         Aircraft__c aircraft = new Aircraft__c(MSN_Number__c=aircraftMSN,Aircraft_Type__c='737', Aircraft_Variant__c='300',
                                                TSN__c=100, CSN__c=50, 
            									Status__c='Available');
        insert aircraft; 
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        list<Custom_Lookup__c> listCustomLookup = [select id,name from Custom_Lookup__c] ;
        system.debug('listCustomLookup----'+listCustomLookup);
		
         Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
       
       	constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
       	constitutentAssembly.TSN__c = 100;
        constitutentAssembly.CSN__c = 200; 
      	insert constitutentAssembly;
        Date startDate = Date.newInstance(2018, 01, 01);
        Date endDate = Date.newInstance(2022, 12, 31);
        Id recordId = Schema.SObjectType.Lease__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        Lease__c lease = new Lease__c(Name='TestLease',Aircraft__c=aircraft.Id,Lease_Start_Date_New__c=startDate,
                                     Lease_End_Date_New__c=endDate,recordtypeid=recordId);
        insert lease;
     	Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='10',
            Airframe_Cycles_Landing_During_Month__c= 10,
            Aircraft__c = aircraft.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2018,01,31)
           
        );
       
       insert utilizationRecord;
         
        LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
        LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
        system.debug('utilizationRecord----'+ utilizationRecord.id);
        Utilization_Report__c trueUpUtilization = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='10',
            Airframe_Cycles_Landing_During_Month__c= 10,
            Aircraft__c = aircraft.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'True up Gross',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2018,01,31)
           
        );
        insert trueUpUtilization;
        LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
        LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
         system.debug('trueUpUtilization----'+ trueUpUtilization.id);
    	String message = UnApproveUtilizationController.validateFunc(utilizationRecord.Id);
		//system.assertEquals('Only Latest Utilizations can be made unapproved', message);
		
        }
    
    
    @isTest
       public static void validateForAPUType(){
      
        String aircraftMSN = '12345';
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                    Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                    Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
                    LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
         Aircraft__c aircraft = new Aircraft__c(MSN_Number__c=aircraftMSN,Aircraft_Type__c='737', Aircraft_Variant__c='300',
                                                TSN__c=100, CSN__c=50, 
            									Status__c='Available');
        insert aircraft; 
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        list<Custom_Lookup__c> listCustomLookup = [select id,name from Custom_Lookup__c] ;
        system.debug('listCustomLookup----'+listCustomLookup);
		
         Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
      	Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
       
       	constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'APU';
        constitutentAssembly.Serial_Number__c = 'Test 12';
       	constitutentAssembly.TSN__c = 100;
        constitutentAssembly.CSN__c = 200; 
        constitutentAssembly.APU_TSN__c = 100;
        constitutentAssembly.APU_CSN__c = 100;
      	insert constitutentAssembly;
        Date startDate = Date.newInstance(2018, 01, 01);
        Date endDate = Date.newInstance(2022, 12, 31);
        Id recordId = Schema.SObjectType.Lease__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        Lease__c lease = new Lease__c(Name='TestLease',Aircraft__c=aircraft.Id,Lease_Start_Date_New__c=startDate,
                                     Lease_End_Date_New__c=endDate,recordtypeid=recordId);
        insert lease;
     	Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='10',
            Airframe_Cycles_Landing_During_Month__c= 10,
            Aircraft__c = aircraft.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2018,01,31)
           
        );
       
       insert utilizationRecord;
         
        LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
        LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
        system.debug('utilizationRecord----'+ utilizationRecord.id);
        Utilization_Report__c trueUpUtilization = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='10',
            Airframe_Cycles_Landing_During_Month__c= 10,
            Aircraft__c = aircraft.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'True up Gross',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2018,01,31)
           
        );
        insert trueUpUtilization;
        LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
        LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
         system.debug('trueUpUtilization----'+ trueUpUtilization.id);
         
        String profileName = [select Id,Name from Profile where Id=: UserInfo.getProfileId() ].Name;
    	system.debug('Profile Name------'+ profileName);
        
        Map <string,LW_Setup__c > mapLWSetup = LW_Setup__c.getAll();
        LW_Setup__c lwSetUpP ;     
        
        if (mapLWSetup.containsKey('PROFILES_FOR_UNAPPROVE_DEL_UTILIZATION')) {
            lwSetUpP = mapLWSetup.get('PROFILES_FOR_UNAPPROVE_DEL_UTILIZATION');
            lwSetUpP.Value__c = profileName;
            update lwSetUpP;
        }
           
    	String message = UnApproveUtilizationController.validateFunc(utilizationRecord.Id);
		//system.assertEquals('Only Latest Utilizations can be made unapproved', message);
		
        }

}