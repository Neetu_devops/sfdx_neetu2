@isTest
public class TestRentManagerWizardController {
    @isTest
    static void loadTestCases(){
        
        //Aircraft Data
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Aircraft',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'V2524',
            MSN_Number__c = '1230',
            MTOW_Leased__c = 134455,
            Aircraft_Type__c = 'A320',
            Aircraft_Variant__c = '400',
            Date_of_Manufacture__c = System.Date.today());
        LeaseWareUtils.clearFromTrigger();
        LeaseWareUtils.TriggerDisabledFlag=true;
        insert aircraft;
        LeaseWareUtils.TriggerDisabledFlag=false;
        
        //Assembly Data
        Constituent_Assembly__c assembly1 = new Constituent_Assembly__c();
        assembly1.Name = 'TEst Assembly';
        assembly1.Engine_Model__c = 'CFM56';
        assembly1.TSN__c = 1000;
        assembly1.Attached_Aircraft__c = aircraft.Id;
        assembly1.Serial_Number__c = '575910';
        assembly1.CSN__c = 2000;
        assembly1.Type__c = 'Airframe';
        assembly1.Last_CSN_Utilization__c = 2000;
        assembly1.Last_TSN_Utilization__c = 2000;
        LeaseWareUtils.clearFromTrigger();
        LeaseWareUtils.TriggerDisabledFlag=true;
        insert assembly1;
        LeaseWareUtils.TriggerDisabledFlag=false;
        
        //Lease Data
        Lease__c l = new Lease__c(Name='LeaseName', Aircraft__c=aircraft.Id,
                                  Month_Thresholds__c='0,6,12', PBH_Rates__c='145,125,100', 
                                  UR_Due_Day__c='10',Lease_Start_Date_New__c=date.valueOf('2015-12-31'),
                                  Lease_End_Date_New__c=date.valueOf('2026-12-31'), Rent_Date__c=system.today(), 
                                  Rent_End_Date__c=system.today().addYears(2));
        LeaseWareUtils.TriggerDisabledFlag=true;
        insert l;
        LeaseWareUtils.TriggerDisabledFlag=false;
        
        //Interest Rate Index Data
        Interest_Rate_Index__c FR = new Interest_Rate_Index__c();
        FR.Name = 'Test Interest Rate Index';
        FR.Index_Type__c = 'USD LIBOR';
        FR.Maturity__c = '3';
        insert FR;
        
        //MultiRent record Data for Fixed Rent
        Stepped_Rent__c SR = new Stepped_Rent__c();
        SR.Name = 'name';
        SR.Rent_Type__c = 'Fixed Type';
        SR.Rent_Period__c = 'Daily';
        SR.Rent_Comment__c = 'comment';
        SR.Base_Rent__c = 475648;
        SR.Off_Wing_Rent__c = 56876;
        SR.Escalation__c = 45;
        SR.Escalation_Month__c = '3';
        SR.Start_Date__c = system.today();
        SR.Rent_End_Date__c = system.today().addYears(1);
        SR.Invoice_Generation_Day__c = '2';
        SR.Rent_Period_End_Day__c = '5';
        SR.Rent_Due_Type__c = 'Fixed';
        SR.Rent_Due_Day__c = 4;
        SR.Lease__c = l.Id;
        insert SR;
        
        //Stepped Rent Data
        SteppedMultiRent__c SMR = new SteppedMultiRent__c();
        SMR.Name = 'test name';
        SMR.Stepped_Rent_Start_Date__c = system.today();
        SMR.Rent_Percent__c = 45;
        SMR.Rent_Amount__c = 543534;
        SMR.Multi_Rent_Schedule__c = SR.Id;
        insert SMR;
        
        //Rent Schedule Data
        Rent__c RentObj = new Rent__c(
            RentPayments__c = l.Id ,
            Stepped_Rent__c= SR.Id,
            Name='x', 
            For_Month_Ending__c=system.today(), 
            Start_Date__c= System.today(),
            Assumed_Interest_Rate__c= 23);
        insert RentObj;
        
        
        
        
        RentManagerWizardController.getLeaseInfo(l.Id);
        RentManagerWizardController.getRentSchedulesForLease(l.Id,SR.Id);
        RentManagerWizardController.getRentSchedulesForLease(l.Id,null);
        String namespace = RentManagerWizardController.getNamespacePrefix();
        RentManagerWizardController.getPickListValues(namespace+'Stepped_Rent__c', namespace+'Rent_Type__c');
        
        //Case 1 : Generate Normal Fixed Rent
        String jsonFString = '{"nameStr":"Test Fixed","rentTypeStr":"Fixed Rent","periodStr":"Monthly","includeCommentStr":false,"baseRentStr":200000,"offWingRentStr":"","extendedRentTypeStr":null,"steppedStartDateStr":null,"rentEscalationStr":0,"rentEscalationMonthStr":"","SLRGenerateCheckStr":false,"SLRStartDateStr":"2019-09-19","SLREndDateStr":"2020-11-19","rentStartStr":"2019-09-19","rentEndStr":"2020-11-19","differentStartStr":false,"invoiceDayStr":"1","rentDueTypeStr":"Fixed","rentDueSettingStr":"Due Date in Advance","rentDueDayStr":"1","prorataStr":"","dueDateCorrectionStr":"","steppedMapListStr":null,"leaseIdStr":"'+l.Id+'","multiRentIdStr":"'+SR.Id+'"}';
        RentManagerWizardController.generateFixedRentSchedule(jsonFString);
        
        
        // Case 2 : Generate Normal Fixed Rent with Stepped Rent
        Map<String,String> steppedMap = new Map<String,String>();
        steppedMap.put('stData','2018-12-23');
        steppedMap.put('stAmtCheck','true');
        steppedMap.put('stPer','0.02');
        steppedMap.put('stAmt','1200');
        List<Map<String,String>> listSteppedmap = new List<Map<String,String>>();
        listSteppedmap.add(steppedMap);
        String  steppedjson = JSON.serialize(listSteppedmap);
        
        Map<String,String> jsonStringMap = new Map<String,String>();
        jsonStringMap.put('nameStr','Test Fixed Extended');jsonStringMap.put('rentTypeStr','Fixed Rent');jsonStringMap.put('periodStr','Monthly');jsonStringMap.put('isRentExtendedStr','false');jsonStringMap.put('commentStr','Test Fixed Extended');
        jsonStringMap.put('baseRentStr','780000');jsonStringMap.put('offWingRentStr','');jsonStringMap.put('extendedRentTypeStr',null);jsonStringMap.put('steppedStartDateStr','');jsonStringMap.put('rentEscalationStr','12');
        jsonStringMap.put('rentEscalationMonthStr','September');jsonStringMap.put('SLRGenerateCheckStr','true');jsonStringMap.put('SLROpeningBalanceStr','5600');jsonStringMap.put('SLRStartDateStr','2016-11-20');jsonStringMap.put('SLREndDateStr','2020-07-22');
        jsonStringMap.put('rentStartStr','2016-11-20');jsonStringMap.put('rentEndStr','2020-07-22');jsonStringMap.put('differentStartStr','false');jsonStringMap.put('secondRentStartStr','');jsonStringMap.put('invoiceDayStr','1');
        jsonStringMap.put('rentDueTypeStr','Fixed');jsonStringMap.put('rentDueSettingStr','Due Date in Advance');jsonStringMap.put('rentDueDayStr','1');jsonStringMap.put('prorataStr','2');jsonStringMap.put('dueDateCorrectionStr','');
        jsonStringMap.put('steppedMapListStr',steppedjson);jsonStringMap.put('leaseIdStr',l.Id);jsonStringMap.put('multiRentIdStr',SR.iD);
        String  jsonF1String = JSON.serialize(jsonStringMap);
       
        RentManagerWizardController.generateFixedRentSchedule(jsonF1String);
        
        //Case 3 : Generate Seasonal Rent
        Date InRentStartDate= system.today().addMonths(-1), InRentEndDate=system.today().addMonths(1);
        Stepped_Rent__c SR1 = new Stepped_Rent__c (
                                            Name='Dummy for SteppedRentRec5',
                                            Rent_Type__c='Seasonal Rent',
                                            Rent_Period__c='Semi-Annual',
                                            Base_Rent__c=10200.00,
                                            Low_Season_Rent__c=10000.00,
                                            Off_Wing_Rent__c=1000.00,
                                            Rent_Amount__c=8000.00,
                                            Start_Date__c=InRentStartDate , // YYYY-MM-DD
                                            Rent_End_Date__c=InRentEndDate,  // YYYY-MM-DD
                                            Invoice_Generation_Day__c='31',
                                            Rent_Period_End_Day__c='6',
                                            Rent_Due_Type__c='Relative Based On Invoice Date',
                                            Rent_Due_Day__c=20,
                                            Lease__c=l.Id,
                                            status__c=true,
                                            
                                            Low_Season_Start_Year__c= string.valueof(system.today().addMonths(-1).year()),
                                            Low_Season_Start_Month__c= datetime.newInstance(InRentStartDate.year(),InRentStartDate.month(),InRentStartDate.day()).format('MMMMM'),
                                            Low_Season_End_Year__c= string.valueof(system.today().addMonths(1).year()),
                                            Low_Season_End_Month__c= datetime.newInstance(InRentEndDate.year(),InRentEndDate.month(),InRentEndDate.day()).format('MMMMM'),
                                            Low_Season_Based_On__c = 'Rent Period Start Date' 
                                            );
        insert SR1;
        
        String jsonVString = '{"nameStr":"Test Variable","rentTypeStr":"Seasonal Rent","periodStr":"Monthly","isRentExtendedStr":false,"commentStr":"","extendedRentTypeStr":null,"includeCommentStr":false,"baseRentStr":123000,"lowSeasonRentStr":9000,"offWingRentStr":"","rentEscalationStr":"","rentEscalationMonthStr":"","SLRGenerateCheckStr":true,"SLROpeningBalanceStr":78000,"SLRStartDateStr":"2020-11-19","SLREndDateStr":"2021-11-19","rentStartStr":"2020-11-19","rentEndStr":"2021-11-19","lowStartMonthStr":"December","lowEndMonthStr":"October","lowStartYearStr":2020,"lowEndYearStr":"2021","lowSeaPeriodStr":"Rent Period Start Date","invoiceDayStr":"1","periodEndDayStr":"","rentDueTypeStr":"Fixed","rentDueSettingStr":"Due Date in Advance","rentDueDayStr":1,"differentStartStr":false,"secondRentStartStr":null,"prorataStr":"","dueDateCorrectionStr":"","leaseIdStr":"'+l.Id+'","multiRentIdStr":"'+SR1.Id+'"}';
        RentManagerWizardController.generateVariableRentSchedule(jsonVString);
        Test.startTest();
        
        //Case 4 : Generate Floating Rent
        
        Stepped_Rent__c SR3 = new Stepped_Rent__c (
                                            Name='Rent Schedule Floating',
                                            Lease__c=l.id,
                                            Rent_Type__c='Floating Rent',
                                            Rent_Period__c='Monthly',
                                            Start_Date__c=date.valueOf('2019-04-01') , // YYYY-MM-DD
                                            Rent_End_Date__c=date.valueOf('2019-09-30') ,  // YYYY-MM-DD
                                            Invoice_Generation_Day__c='1',

                                            Base_Rent__c=20000.00,
											Interest_Rate_Index__c= FR.Id,	
											Rate_Reset_Frequency__c=3,
											First_Rent_Rate_Reset_Date__c=date.valueOf('2019-03-28'),
                                            Business_Days_Prior__c=2,
											
											Rental_Adjustment_Factor__c=26512,
											Manufacturer_Escalation_Factor__c=1.0356,
                                            Assumed_Interest_Rate__c=1.45,                                            
											Rental_Credit_Debit__c=2000,	
											Cost_of_Funds__c=2,
                                            
                                            Rent_Due_Type__c='Fixed',
                                            Rent_Due_Day__c=6, 
											Rent_Due_Day_Setting__c='Due Date in Advance',
                                            Pro_rata_Number_Of_Days__c='Actual number of days in month/year'
                                        );
        insert SR3;
        String jsonFLString = '{"nameStr":"Test Float","rentTypeStr":"Floating Rent","periodStr":"Monthly","isRentExtendedStr":false,"commentStr":"","extendedRentTypeStr":null,"includeCommentStr":false,"baseRentStr":900000,"adjustStr":10000,"manufactureStr":1.02,"assInterestRateStr":0.06,"rentCreditdebitStr":"2500","costFundStr":"2","interestRateIndexStr":"'+FR.Id+'","rateResetFreqStr":3,"firstResetDateStr":"2020-11-21","businessDayPriorStr":3,"rentEscalationStr":2,"rentEscalationMonthStr":"September","rentStartStr":"2020-11-19","rentEndStr":"2021-11-19","apprResetDateStr":"","differentStartStr":false,"secondRentStartStr":null,"invoiceDayStr":"1","periodEndDayStr":"","rentDueTypeStr":"Fixed","rentDueSettingStr":"Due Date in Advance","rentDueDayStr":1,"prorataStr":"","dueDateCorrectionStr":"","leaseIdStr":"'+l.Id+'","multiRentIdStr":"'+SR3.Id+'","isFromLE":false}';
        RentManagerWizardController.generateFloatingRentSchedule(jsonFLString);
     
        //Case 5 : Generate PBH Rent
        Stepped_Rent__c SR4 = new Stepped_Rent__c (
                                            Name='Test- PBH Monthly',
                                            Lease__c=l.id,
                                            Rent_Type__c='Power By The Hour',
                                            Rent_Period__c='Monthly',
                                            
                                            Base_Rent__c=10000.00,
                                            Start_Date__c=date.valueOf('2019-01-01') , // YYYY-MM-DD
                                            Rent_End_Date__c=date.valueOf('2019-03-31') ,  // YYYY-MM-DD
                                            Invoice_Generation_Day__c='15',
                                            Rent_Due_Type__c='Fixed',
                                            Rent_Due_Day__c=15,
                                            Escalation__c=3,
                                            Escalation_Month__c='April',
                                            Pro_rata_Number_Of_Days__c='Actual number of days in month/year'
                                            );
        insert SR4;
        
        // PBH Rent Data
        PBH_Rent_Rate__c pbh = new PBH_Rent_Rate__c();
        pbh.Name = 'Pbh Rent';
        pbh.Multi_Rent_Schedule__c = SR4.Id;
        pbh.PBH_Rate__c = 900;
        pbh.Selection__c = True;
        pbh.Associated_Assembly__c = assembly1.Id;
        insert pbh;
        
        String jsonPBHString = '{"nameStr":"Test PBH","rentTypeStr":"Power By The Hour","periodStr":"Monthly","isRentExtendedStr":false,"commentStr":"","extendedRentTypeStr":null,"includeCommentStr":false,"rentStartStr":"2020-07-01","rentEndStr":"2021-08-08","invoiceDayStr":"1","baseRentStr":890000,"offWingRentStr":9000,"rentEscalationStr":"5","rentEscalationMonthStr":"May","minTotalStr":"7800","maxTotalStr":"90000","minPBHStr":"9000","maxPBHStr":"600000","rentDueTypeStr":"Fixed","rentDueSettingStr":"Due Date in Advance","rentDueDayStr":1,"prorataStr":"","dueDateCorrectionStr":"","assemblyMapListStr":null,"leaseIdStr":"'+l.Id+'","multiRentIdStr":"'+SR4.Id+'"}';
        RentManagerWizardController.generatePBHRentSchedule(jsonPBHString); 
        
        Map<String,String> PBHMap = new Map<String,String>();
        PBHMap.put('Id',pbh.Id);
        PBHMap.put('AssemblyId',assembly1.Id);
        PBHMap.put('Name','DevDataMSN1-E');
        PBHMap.put('Attached_Aircraft__c',aircraft.id);
        PBHMap.put('Type__c','Airframe');
        PBHMap.put('RateBasis','Cycle');
        PBHMap.put('Selection','True');
        PBHMap.put('FromValue','200');
        PBHMap.put('PBHrate','20');
        PBHMap.put('stId',SR4.Id);
        List<Map<String,String>> listPBHmap = new List<Map<String,String>>();
        listPBHmap.add(PBHMap);
        String  PBHjson = JSON.serialize(listPBHmap);
        
        
        Map<String,String> jsonStringPBHMap = new Map<String,String>();
        jsonStringPBHMap.put('nameStr','Test PBH 2');
        jsonStringPBHMap.put('rentTypeStr','Power By The Hour');
        jsonStringPBHMap.put('periodStr','Monthly');
        jsonStringPBHMap.put('isRentExtendedStr','false');
        jsonStringPBHMap.put('commentStr','Test PBH Extended');
        jsonStringPBHMap.put('baseRentStr','780000');jsonStringPBHMap.put('offWingRentStr','');jsonStringPBHMap.put('extendedRentTypeStr',null);jsonStringPBHMap.put('rentEscalationStr','2');
        jsonStringPBHMap.put('rentEscalationMonthStr','May');jsonStringPBHMap.put('SLRGenerateCheckStr','true');jsonStringPBHMap.put('SLROpeningBalanceStr','5600');jsonStringPBHMap.put('SLRStartDateStr','2027-11-20');jsonStringPBHMap.put('SLREndDateStr','2019-07-22');
        jsonStringPBHMap.put('rentStartStr','2017-11-20');jsonStringPBHMap.put('rentEndStr','2019-07-22');jsonStringPBHMap.put('differentStartStr','false');jsonStringPBHMap.put('secondRentStartStr',null);jsonStringPBHMap.put('invoiceDayStr','1');
        jsonStringPBHMap.put('rentDueTypeStr','Fixed');jsonStringPBHMap.put('rentDueSettingStr','Due Date in Advance');jsonStringPBHMap.put('rentDueDayStr','1');jsonStringPBHMap.put('prorataStr','2');jsonStringPBHMap.put('dueDateCorrectionStr','');
        jsonStringPBHMap.put('leaseIdStr',l.Id);jsonStringPBHMap.put('multiRentIdStr',SR4.iD);jsonStringPBHMap.put('assemblyMapListStr',PBHjson);
        jsonStringPBHMap.put('minTotalStr','1200');jsonStringPBHMap.put('maxTotalStr','1200000');jsonStringPBHMap.put('minPBHStr','1600');jsonStringPBHMap.put('maxPBHStr','200000');
                
        System.debug('jsonStringPBHMap : '+jsonStringPBHMap);
        String  jsonPBH2String = JSON.serialize(jsonStringPBHMap);
        System.debug('jsonPBH2String : '+jsonPBH2String);
        LeaseWareUtils.TriggerDisabledFlag=true;
         RentManagerWizardController.generatePBHRentSchedule(jsonPBH2String); 
        LeaseWareUtils.TriggerDisabledFlag=false;
        
        Lessor__c setup1 = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                          Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                                          Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
        LeaseWareUtils.clearFromTrigger();
        insert  setup1;
    	Holiday_Schedule__c HS1= new Holiday_Schedule__c(name='Dummy', Lessor__c  = setup1.Id,Year_New__c='2017');
    	LeaseWareUtils.clearFromTrigger();insert HS1;
    	
        Applicable_Calendar__c ACal = new Applicable_Calendar__c(name= 'Dummy',Holiday_Schedule__c=HS1.Id,Lease__c=l.Id );        
		LeaseWareUtils.clearFromTrigger();
        insert ACal;

        RentManagerWizardController.generatePendingPeriods(SR.Id);
        RentManagerWizardController.getOutputData(SR.Id);
        RentManagerWizardController.getSLROutputData(SR.Id);
        RentManagerWizardController.getSteppedMultiData(SR.Id);
        RentManagerWizardController.fetchInvoiceStatus(SR.Id);
        RentManagerWizardController.getAssemblyList(l.Id);
        LeaseWareUtils.TriggerDisabledFlag=true;
        RentManagerWizardController.getPBHRentData(SR.Id,l.Id);
        LeaseWareUtils.TriggerDisabledFlag=false;
        LeaseWareUtils.TriggerDisabledFlag=true;
        RentManagerWizardController.getMultiRentScheduleRecord(SR.Id,l.Id);
        LeaseWareUtils.TriggerDisabledFlag=false;
        RentManagerWizardController.updateMultiRentStatus(SR.Id, true);
       
        
        LeaseWareUtils.TriggerDisabledFlag=true;
        RentManagerWizardController.deleteMultiRent(SR.Id);
        LeaseWareUtils.TriggerDisabledFlag=false;
        RentManagerWizardController.deleteMultiRentRecord(SR.Id);
           Test.stopTest();                             
    }
}