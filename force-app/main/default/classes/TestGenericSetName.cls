/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class TestGenericSetName {
    
    static testMethod void myUnitTest() {

//        ApexPages.currentPage().getParameters().put('id', [][0].id);
        genericSetNameParam controller = new genericSetNameParam(new ApexPages.StandardController(new Aircraft__c()));
        controller.url();
        Constituent_Assembly__c CA1 = [select id,name from Constituent_Assembly__c where name = 'TestSeededMSN1-E' limit 1 ];
        
        ApexPages.currentPage().getParameters().put('id',CA1.Id);
        //NewAssembyUtilizationReport AsmblyController = new NewAssembyUtilizationReport();
        

        
    }

    public class PmtSetRecTypeOnNewCtrlTester extends PaymentNewControllerBase {

        public PmtSetRecTypeOnNewCtrlTester(ApexPages.StandardController ingored) {
            super(Invoice__c.SObjectType, Payment__c.SObjectType);
        }

        public PmtSetRecTypeOnNewCtrlTester(ApexPages.StandardController ingored, string tmp) {
            super(Invoice__c.SObjectType, Payment__c.SObjectType, tmp);
        }
        public PmtSetRecTypeOnNewCtrlTester(ApexPages.StandardController ingored, string tmp1, string tmp2) {
            super(Invoice__c.SObjectType, Payment__c.SObjectType, tmp1, tmp2);
        }


        public void Test(){
            try{
                super.url();
            }catch(exception e){
                system.debug('Unknown error - ' + e.getMessage());
            }
        }
    }


    static testMethod void myUnitTest2(){
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);      

        mapInOut.put('Utilization_Report__c.True_Up__c',null); 
        mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(-2)); 
        mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');
        TestLeaseworkUtil.createUR(mapInOut);
        Id leaseId ;
        leaseId = (Id)mapInOut.get('Aircraft__c.lease__c');

       
        // Insert Invoice 
        TestLeaseworkUtil.setRecordTypeOfInvoice(mapInOut);
        Utilization_Report_List_Item__c[] asmblyURs = [select Id, Cycles_During_Month__c, Running_Hours_During_Month__c from Utilization_Report_List_Item__c where Utilization_Report__c = :(Id)mapInOut.get('Utilization_Report__c.Id')];
        system.debug('asmblyURs.size()'+asmblyURs.size());

        
        // Insert Invoice 
        TestLeaseworkUtil.setRecordTypeOfInvoice(mapInOut);
        TestLeaseworkUtil.setRecordTypeOfPayment(mapInOut);
        // Insert Invoice
        TestLeaseworkUtil.createRent(mapInOut) ;
        
        system.debug('Done-1');
        
        Invoice__c newRentInv=new Invoice__c(Lease__c=leaseId, Name='Test UR Inv', RecordTypeId=(Id)mapInOut.get('Invoice__c.Rent'),Invoice_date__c=Date.today(), 
                Invoice_Type__c='Rent', Rent__c=(Id)mapInOut.get('Rent__c.Id'));
        insert newRentInv;
        system.debug('Done0');

        
        ApexPages.currentPage().getParameters().put('id', newRentInv.id);
        PmtSetRecTypeOnNewCtrlTester tstPctr = new PmtSetRecTypeOnNewCtrlTester(new ApexPages.StandardController(newRentInv),'Invoice_Type__c','Acquisition');
        tstPctr.Test();

    tstPctr = new PmtSetRecTypeOnNewCtrlTester(new ApexPages.StandardController(newRentInv));
        tstPctr.Test();

        tstPctr = new PmtSetRecTypeOnNewCtrlTester(new ApexPages.StandardController(newRentInv), 'Acquisition');
        tstPctr.Test();
        
    }
}