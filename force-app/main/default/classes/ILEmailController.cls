public without sharing class ILEmailController {
    
    @AuraEnabled 
    public static void sendMailMethod(String recordId){
        Customer_Interaction_Log__c logData = [select Y_Hidden_Send_Email__c 
                                               from Customer_Interaction_Log__c where Id =:recordId
                                              ];
		logData.Y_Hidden_Send_Email__c = true;
        update logData;
    }
    
    @AuraEnabled
    public static String sendMailToMany(Id recordId,List<User> userList, List<String> emailAddress, String userType) {
        System.debug('sendMailToMany recordId '+recordId );
        System.debug('sendMailToMany userList '+userList );
        System.debug('sendMailToMany emailAddress '+emailAddress );
        
        Set<Id> userIds = new Set<Id>(); 
        if(userList != null && userList.size() > 0) {
            for(User ur: userList) {
                userIds.add(ur.Id);
            }
        }
        if('group'.equals(userType)) {
            List<Group> thisGroup = [Select Id from Group where Id in :userIds];
            if(thisGroup.size() > 0) {
                userIds.clear();
                Set<Id> grpUserId = new Set<Id>();
                for(Group grp : thisGroup) {
                    grpUserId = LeaseWareUtils.GetUserIdsFromGroup(grp.Id);
                    userIds.addall(grpUserId);
                }
            }
        }
        System.debug('sendMailToMany userIds '+userIds );
        Id currentUserId = UserInfo.getUserId();
        userIds.add(currentUserId);
        if(userIds != null && userIds.size() > 0) {
            if(emailAddress == null) {
                emailAddress = new List<String>();
            }
            for(User ur: [select Id, Email from User where Id in :userIds]) {
                emailAddress.add(ur.Email);
            }
        }
        
        System.debug('sendMailToMany with userList emailAddress '+emailAddress );
        List<EmailTemplate> emailTemplate = [ Select Id,Subject,Description,HtmlValue,DeveloperName,Body 
                                       from EmailTemplate where name = 'IL Email_Template'];
        if(emailTemplate.size() <= 0) {
            return null;
        }
        
        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(emailTemplate[0].Id, currentUserId, recordId);
        
        String emailSubject = email.getSubject();
        String emailTextBody = email.getPlainTextBody();
        
        email.setTargetObjectId(currentUserId);
        email.setSubject(emailSubject);
        email.setPlainTextBody(emailTextBody);
        email.saveAsActivity = false;
        email.toAddresses = emailAddress;
        if(!Test.isRunningTest()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage [] {email});
            
            if (results[0].success) 
            {
                System.debug('The email was sent successfully.');
                return 'Success';
            } else {
                System.debug('The email failed to send: ' +  results[0].errors[0].message);
                return results[0].errors[0].message;
            }    
        }
        return null;
    }
    
}