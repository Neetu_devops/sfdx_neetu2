@isTest
public class TestAssetScorecardController {
    
    @TestSetup
    static void makeData(){
        list<Recommendation_Factor__c> rfList = new list<Recommendation_Factor__c>();
        rfList.add(new Recommendation_Factor__c(name = 'Asset Type',Type__c='Asset Scorecard', Weightage_Number__c=60));
        rfList.add(new Recommendation_Factor__c(name = 'Lease Term Remaining',Type__c='Asset Scorecard', Weightage_Number__c=20));
        rfList.add(new Recommendation_Factor__c(name = 'Asset Specification',Type__c='Asset Scorecard', Weightage_Number__c=10));
        rfList.add(new Recommendation_Factor__c(name = 'MR EOLA',Type__c='Asset Scorecard', Weightage_Number__c=10));
       

        insert rfList;
        list<Custom_Lookup__c> listLooupks = new list<Custom_Lookup__c>();
		listLooupks.add(new Custom_Lookup__c(Name='Score 1',Lookup_Type__c ='ASSET_SCORECARD' ,Sub_LookupType__c='LEASE_TERM_REMAINING_SCORE',Sub_LookupType2__c='1',Sub_LookupType3__c='1',Details__c='120'));
        listLooupks.add(new Custom_Lookup__c(Name='Score 2',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='LEASE_TERM_REMAINING_SCORE',Sub_LookupType2__c='2',Sub_LookupType3__c='2',Details__c='96'));
        listLooupks.add(new Custom_Lookup__c(Name='Score 3',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='LEASE_TERM_REMAINING_SCORE',Sub_LookupType2__c='3',Sub_LookupType3__c='3',Details__c='72'));
        listLooupks.add(new Custom_Lookup__c(Name='Score 4',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='LEASE_TERM_REMAINING_SCORE',Sub_LookupType2__c='4',Sub_LookupType3__c='4',Details__c='48'));
        listLooupks.add(new Custom_Lookup__c(Name='Score 5',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='LEASE_TERM_REMAINING_SCORE',Sub_LookupType2__c='5',Sub_LookupType3__c='5',Details__c='24'));
        listLooupks.add(new Custom_Lookup__c(Name='Score 6',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='LEASE_TERM_REMAINING_SCORE',Sub_LookupType2__c='6',Sub_LookupType3__c='6',Details__c='0'));
         
        listLooupks.add(new Custom_Lookup__c(Name='Favourable',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='COUNTRY_REPOSESSION_RISK_SCORE',Details__c='1'));
        listLooupks.add(new Custom_Lookup__c(Name='Cautionary',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='COUNTRY_REPOSESSION_RISK_SCORE',Details__c='3'));
        listLooupks.add(new Custom_Lookup__c(Name='Unfavourable',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='COUNTRY_REPOSESSION_RISK_SCORE',Details__c='6'));
          
        listLooupks.add(new Custom_Lookup__c(Name='Low',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='GOVERNMENT_SUPPORT_SCORE',Details__c='6'));
        listLooupks.add(new Custom_Lookup__c(Name='Moderate',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='GOVERNMENT_SUPPORT_SCORE',Details__c='3'));
        listLooupks.add(new Custom_Lookup__c(Name='High',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='GOVERNMENT_SUPPORT_SCORE',Details__c='1'));

        listLooupks.add(new Custom_Lookup__c(Name='Low',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='FINANCIAL_RISK_PROFILE_SCORE',Details__c='1'));
        listLooupks.add(new Custom_Lookup__c(Name='Moderate',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='FINANCIAL_RISK_PROFILE_SCORE',Details__c='3'));
        listLooupks.add(new Custom_Lookup__c(Name='High',Lookup_Type__c ='ASSET_SCORECARD',Sub_LookupType__c='FINANCIAL_RISK_PROFILE_SCORE',Details__c='6'));
          
		insert listLooupks;
        
        
         
        Aircraft_Type__c newAT = new Aircraft_Type__c(Name='737-200', Aircraft_Type__c='737',Aircraft_Variant__c='200',
        Residual_Value_Rentention__c=4,Value_for_Money__c=2,Remarketing_Potential__c=2, Operational_Performance__c=2);
        LeaseWareUtils.clearFromTrigger();insert newAT;

        Aircraft_Type__c newAT2 = new Aircraft_Type__c(Name='737-300', Aircraft_Type__c='737',Aircraft_Variant__c='300', 
        Residual_Value_Rentention__c=3,Value_for_Money__c=2,Remarketing_Potential__c=1, Operational_Performance__c=2,Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56');
        LeaseWareUtils.clearFromTrigger();insert newAT2;
       

        Country__c country = new Country__c (name='India',Country_Repossession_Risk__c='Favourable',Region__c='Asia');
        LeaseWareUtils.clearFromTrigger();
        insert country;
    }
 
    
     @IsTest
    private static void testAssetTypeAndLeaseScore(){
        Country__c country =[select Id,Name from Country__c where Name='India'];
		Operator__c opr = new Operator__c(name='Test Operator',Country_Lookup__c = country.id);
        LeaseWareUtils.clearFromTrigger();
        insert opr;

        Credit_Score__c cs = new Credit_Score__c(name='Test CS', Period_New__c='2019',Airline__c = opr.Id,
        Financial_Risk_Profile__c='Moderate',
        COVID_19_Government_Support__c='Low');
        LeaseWareUtils.clearFromTrigger();
        insert cs;


        Aircraft__c ac = new Aircraft__c(name='Dummy', MSN_Number__c='ABC-12345' ,Aircraft_Type__c='737',Aircraft_Variant__c='300',TSN__c=400,CSN__c=500);
        LeaseWareUtils.clearFromTrigger();
        insert ac;

        Lease__c lease = new Lease__c(Name='TestLease', Operator__c=opr.id,Lease_End_Date_New__c=Date.today().addYears(12),Lease_Start_Date_New__c=Date.today());
        LeaseWareUtils.clearFromTrigger();
        insert lease;

        Aircraft__c aac = [select Id, Lease__c from Aircraft__c where Id = : ac.Id];
        LeaseWareUtils.clearFromTrigger();
        aac.Lease__c = lease.id;
        update aac;

        Operator__c opr1 = [select Id,Name,Financial_Risk_Profile__c,Government_Support1__c,Country_Repossession_Risk__c from operator__c where id =: opr.id];
        system.debug('opr1-----' + opr1);
        AssetScorecardController.AssetScorecardWrapper data = AssetScorecardController.getAssetScoreDetails(aac.Id);
        String dataString = JSON.serialize(data);
        
 		AssetScorecardController.AssetRiskAssessmentWrapper wrapperRes =  AssetScorecardController.getAssetAssessmentDetails(aac.Id);
         if(wrapperRes != null) {
            AssetScorecardController.AssetScoreWrapper scoreWrapper = wrapperRes.scoreWrapper;
            system.assertEquals(String.valueOf(2),wrapperRes.scoreWrapper.assetTypeScore,'Asset Type Score');
        	system.assertEquals(String.valueOf(1),wrapperRes.scoreWrapper.leaseTermRemainingScore,'Lease Term Remaining Score');
            scoreWrapper.assetTypeScore = '1';
            scoreWrapper.leaseTermRemainingScore = '2';
            scoreWrapper.assetSpecScore = '3';
            scoreWrapper.MREOLAScore = '4'; 
            String dataString1 = JSON.serialize(scoreWrapper);
            AssetScorecardController.updateAsssessmentDetails(dataString1);
        }
        AssetScorecardController.getPandamicDetails(ac.Id);
        Aircraft__c fetchAC = [select Id,Name,Asset_Risk_Assessment_Score__c,Asset_Overall_Score__c from Aircraft__c where Id =: ac.Id]; 
        system.assertEquals(1.7, fetchAC.Asset_Risk_Assessment_Score__c,'Asset Risk Assessment Score ');
         
        system.assertEquals(2.9, fetchAC.Asset_Overall_Score__c,'Asset Overall Score');
    }

  
    @IsTest
    private static void testWhenAssetTypeValuesAbsent(){

        Aircraft__c ac = new Aircraft__c(name='Dummy', MSN_Number__c='ABC-12345' ,Aircraft_Type__c='737',Aircraft_Variant__c='500',TSN__c=400,CSN__c=500);
        LeaseWareUtils.clearFromTrigger();
        insert ac;

        AssetScorecardController.AssetScorecardWrapper data = AssetScorecardController.getAssetScoreDetails(ac.Id);
        String dataString = JSON.serialize(data);
        
 		AssetScorecardController.AssetRiskAssessmentWrapper wrapperRes =  AssetScorecardController.getAssetAssessmentDetails(ac.Id);
        if(wrapperRes != null) {
            AssetScorecardController.AssetScoreWrapper scoreWrapper = wrapperRes.scoreWrapper;
            system.assertEquals(null,wrapperRes.scoreWrapper.assetTypeScore,'Asset Type Score');
            system.assertEquals(null,wrapperRes.scoreWrapper.leaseTermRemainingScore,'Lease Term Remaining Score');
        }

    }
    
    
    @isTest
    private static void testLeaseScores(){
        Country__c country =[select Id,Name from Country__c where Name='India'];
		Operator__c opr = new Operator__c(name='Test Operator',Country_Lookup__c = country.id);
        LeaseWareUtils.clearFromTrigger();
        insert opr;
        
        
        Aircraft__c ac = new Aircraft__c(name='Dummy', MSN_Number__c='ABC-12345' ,Aircraft_Type__c='737',Aircraft_Variant__c='300',TSN__c=400,CSN__c=500);
        LeaseWareUtils.clearFromTrigger();
        insert ac;

        Lease__c lease = new Lease__c(Name='TestLease', Operator__c=opr.id,Lease_End_Date_New__c=Date.today().addMonths(13),Lease_Start_Date_New__c=Date.today());
        LeaseWareUtils.clearFromTrigger();
        insert lease;

        Aircraft__c aac = [select Id, Lease__c from Aircraft__c where Id = : ac.Id];
        LeaseWareUtils.clearFromTrigger();
        aac.Lease__c = lease.id;
        update aac;
        
        AssetScorecardController.AssetRiskAssessmentWrapper wrapperRes =  AssetScorecardController.getAssetAssessmentDetails(aac.Id);
         if(wrapperRes != null) {
            AssetScorecardController.AssetScoreWrapper scoreWrapper = wrapperRes.scoreWrapper;
            system.assertEquals(String.valueOf(2),wrapperRes.scoreWrapper.assetTypeScore,'Asset Type Score');
        	system.assertEquals(String.valueOf(6),wrapperRes.scoreWrapper.leaseTermRemainingScore,'Lease Term Remaining Score');
         }
    }
   

    @isTest 
    private static void testPandemicInfoData(){
        List<Country__c> countryList = new List<Country__c>();
      
        Country__c country2 = new Country__c(name='Indonesia',Region__c='Asia');
        Country__c country3 = new Country__c(name='Iran',Region__c='Asia');
        Country__c country4 = new Country__c(name='Iraq',Region__c='Asia');
        Country__c country5 = new Country__c(name='Japan',Region__c='Asia');
        Country__c country11 = new Country__c(name='Belgium',Region__c='Europe');
        Country__c country12 = new Country__c(name='Germany',Region__c='Europe');
        Country__c country13 = new Country__c(name='France',Region__c='Europe');
        Country__c country14 = new Country__c(name='Greece',Region__c='Europe');
        Country__c country15 = new Country__c(name='Finland',Region__c='Europe');
        countryList.add(country2);countryList.add(country3);countryList.add(country4);countryList.add(country5);
        countryList.add(country11); countryList.add(country12);countryList.add(country13);countryList.add(country14);countryList.add(country15);
        insert countryList;
        List<Country__C> fetchCountryList = [select Id,Name from Country__c];
        map<String,String> countryMap = new map<String,String>();
        for(Country__c cou : fetchCountryList){
            countryMap.put(cou.Name, cou.Id);
        }

        Operator__c opr = new Operator__c(name='Test Operator',Country_Lookup__c = countryMap.get('India'));
        LeaseWareUtils.clearFromTrigger();
        insert opr;
        
        
        Aircraft__c ac = new Aircraft__c(name='Dummy', MSN_Number__c='ABC-12345' ,Aircraft_Type__c='737',Aircraft_Variant__c='300',TSN__c=400,CSN__c=500);
        LeaseWareUtils.clearFromTrigger();
        insert ac;

        Lease__c lease = new Lease__c(Name='TestLease', Operator__c=opr.id,Lease_End_Date_New__c=Date.today().addMonths(13),Lease_Start_Date_New__c=Date.today());
        LeaseWareUtils.clearFromTrigger();
        insert lease;

        Aircraft__c aac = [select Id, Lease__c from Aircraft__c where Id = : ac.Id];
        LeaseWareUtils.clearFromTrigger();
        aac.Lease__c = lease.id;
        update aac;

        Pandemic_INfo__c info1 = new Pandemic_Info__c(name='India' ,Data_As_Of__c=system.today(), Total_Deaths__c =10 ,Recovered_Cases__c=1000 ,
                                                   Confirmed_Cases__c= 100000,Country__c=countryMap.get('India'));
        Pandemic_INfo__c info2 = new Pandemic_Info__c(name='Indonesia' ,Data_As_Of__c=system.today(), Total_Deaths__c= 20, Recovered_Cases__c = 2000,
                                                   Confirmed_Cases__c= 200000,Country__c=countryMap.get('Indonesia'));
        Pandemic_INfo__c info3 = new Pandemic_Info__c(name='Iran' , Data_As_Of__c=system.today(), Total_Deaths__c= 30, Recovered_Cases__c =3000,
                                                   Confirmed_Cases__c= 300000,Country__c=countryMap.get('Iran'));
        Pandemic_INfo__c info4 = new Pandemic_Info__c(name='Iraq' , Data_As_Of__c=system.today(), Total_Deaths__c= 40, Recovered_Cases__c = 4000,
                                                   Confirmed_Cases__c= 400000,Country__c=countryMap.get('Iraq'));
        Pandemic_INfo__c info5 = new Pandemic_Info__c(name='Japan' , Data_As_Of__c=system.today(), Total_Deaths__c= 50, Recovered_Cases__c = 5000,
                                                   Confirmed_Cases__c= 500000,Country__c=countryMap.get('Japan'));

        Pandemic_INfo__c info6 = new Pandemic_Info__c(name='Belgium' ,Data_As_Of__c=system.today(),  Total_Deaths__c= 60, Recovered_Cases__c = 60000,
                                                   Confirmed_Cases__c= 600000,Country__c=countryMap.get('Belgium'));
        Pandemic_INfo__c info7 = new Pandemic_Info__c(name='Germany' ,Data_As_Of__c=system.today(), Total_Deaths__c= 70, Recovered_Cases__c = 7000,
                                                   Confirmed_Cases__c= 700000,Country__c=countryMap.get('Germany'));
        Pandemic_INfo__c info8 = new Pandemic_Info__c(name='France' , Data_As_Of__c=system.today(), Total_Deaths__c= 80, Recovered_Cases__c = 8000,
                                                   Confirmed_Cases__c= 800000,Country__c=countryMap.get('France'));
        Pandemic_INfo__c info9 = new Pandemic_Info__c(name='Greece' ,Data_As_Of__c=system.today(),  Total_Deaths__c= 90, Recovered_Cases__c = 9000,
                                                   Confirmed_Cases__c= 900000,Country__c=countryMap.get('Greece'));
        Pandemic_INfo__c info10 = new Pandemic_Info__c(name='Finland' ,Data_As_Of__c=system.today(), Total_Deaths__c= 10, Recovered_Cases__c = 10000,
                                                   Confirmed_Cases__c= 100000,Country__c=countryMap.get('Finland'));

        List<Pandemic_Info__c> pandemicInfoList = new List<Pandemic_Info__c>();
        pandemicInfoList.add(info1);
        pandemicInfoList.add(info2);
        pandemicInfoList.add(info3);
        pandemicInfoList.add(info4);
        pandemicInfoList.add(info5);
        pandemicInfoList.add(info6);
        pandemicInfoList.add(info7);
        pandemicInfoList.add(info8);
        pandemicInfoList.add(info9);
        pandemicInfoList.add(info10);
        insert pandemicInfoList;

        List<Pandemic_Info__c> pandInfoList = [select Id,Name, Country__r.Name,Country__r.Region__c, Confirmed_Cases__c, Total_Deaths__C, Recovered_Cases__c,
                                               Fatality_Rate__c,Recovery_per__c from Pandemic_Info__c] ;
        system.debug('pandInfoList----'+pandInfoList);
        AssetScorecardController.CovidWrapper wrapperRes =  AssetScorecardController.getPandamicDetails(aac.Id);
        system.debug('wrapperRes----'+ wrapperRes);
        system.assertEquals(0, Decimal.valueOf(wrapperRes.regionFatalityRate), 'Region Fatality Rate');
        system.assertEquals(1, Integer.valueOf(wrapperRes.regionRecoveryRate), 'Region Recovery Rate');
        system.assertEquals(0, Decimal.valueOf(wrapperRes.worldwideFatalityRate),' Worldwide Fatality Rate');
        system.assertEquals(3, Decimal.valueOf(wrapperRes.worldwideRecoveryRate),'Worldwide Recovery Rate');
                                                        

                                                   
                                                   
                                                       
    }
}