@isTest
public class TestLLPPriceCatalogController {

    @isTest
    public static void loadTestCases() {
        Custom_Lookup__c lc = new Custom_Lookup__c();
        lc.Lookup_Type__c = 'Engine';
        lc.active__c = true;
        lc.name = 'Default';
        insert lc;
        Custom_Lookup__c[] listThrust = [select id from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true and name ='Default'];
        Engine_Template__c et1 = new Engine_Template__c(name='RB211',Manufacturer__c ='Rolls Royce'
                            ,Model__c='RB211',Thrust_LP__c=listThrust[0].Id,Current_Catalog_Year__c='2015');
        
        LeaseWareUtils.clearFromTrigger();
        insert et1;
        LLP_Catalog_Summary__c llp = new LLP_Catalog_Summary__c();
        llp.Engine_Template__c = et1.Id;
        insert llp;
        
        LLPPriceCatalogController.processAction(llp.Id);
        try{
        	LLPPriceCatalogController.processAction(lc.Id);
        }
        catch(Exception e) {
            
        }
    }
}