@isTest
public class TestAuditSchedulerController {
    @isTest
    static void loadTestCases(){
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Aircraft',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
        Date_of_Manufacture__c = System.Date.today());
        LeaseWareUtils.clearFromTrigger();
        LeaseWareUtils.TriggerDisabledFlag=true;
         insert aircraft;
         LeaseWareUtils.TriggerDisabledFlag=false;
        
        Lease__c lease = new Lease__c(Name='LeaseName', Aircraft__c=aircraft.Id,
                                  Month_Thresholds__c='0,6,12', PBH_Rates__c='145,125,100', 
                                  UR_Due_Day__c='10',Lease_Start_Date_New__c=system.today().addMonths(-2),
                                  Lease_End_Date_New__c=system.today().addYears(4), Rent_Date__c=system.today(),
                                  Rent_End_Date__c=system.today().addYears(2),
                                  Contractual_Delivery_Date__c =  system.today().addYears(2),
                                  Contractual_Redelivery_Date__c = system.today().addYears(4)   );
        LeaseWareUtils.TriggerDisabledFlag=true;
        insert lease;
        LeaseWareUtils.TriggerDisabledFlag=false;
        
        Date auditStartValue = Date.newInstance(2019, 04, 12);
        Integer auditPeriodicity = 12;
        Boolean isdelete = true;
        Date contractDate = system.today().addYears(2);
        Id recordId = lease.Id;
   
        AuditSchedulerController.InsertAuditRecs(auditStartValue,auditPeriodicity,isdelete,contractDate,recordId);
        AuditSchedulerController.getLeaseData(lease.Id);
    }

}