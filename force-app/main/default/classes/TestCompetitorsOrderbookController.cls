@isTest
public class TestCompetitorsOrderbookController {
	
    @isTest
    static void loadTestCases(){
        List<Global_Fleet__c> listOfFleets = new List<Global_Fleet__c>();
        
        Country__c country =new Country__c(Name='United States');
        insert country;

    	Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
    //  opr.Country__c = 'United States';
        opr.Country_Lookup__c = country.id;
        opr.Region__c = 'Asia';
        insert opr;
        System.assert(opr.Id != null);
        
        Global_Fleet__c gf = new Global_Fleet__c();
        gf.Name = 'testWorldFleet';
        gf.Aircraft_External_ID__c = 'type-msn';
        gf.MTOW__c = 134453;
        gf.BuildYear__c = '1998';
        gf.SeatTotal__c = 12;
        gf.AircraftType__c = 'A320';
        gf.AircraftVariant__c = '400';
        gf.EngineType__c = 'V2524';
        gf.AircraftSeries__c = 'AStest';
        gf.EngineVariant__c = 'EVtest';
        gf.OriginalOperator__c ='testOpr';
        gf.Aircraft_Operator__c = opr.Id;
        gf.AircraftStatus__c = 'On Order';
        listOfFleets.add(gf);
        insert gf;
        
        System.assert(gf.Id != null);
        
        Global_Fleet__c gf1 = new Global_Fleet__c();
        gf1.Name = 'testWorldFleet1';
        gf1.Aircraft_External_ID__c = 'type1-msn1';
        gf1.MTOW__c = 134498;
        gf1.BuildYear__c = '1998';
        gf1.SeatTotal__c = 120;
        gf1.AircraftType__c = 'A320';
        gf1.AircraftVariant__c = '400';
        gf1.SerialNumber__c ='101';
        gf1.EngineType__c = 'V2524';
        gf1.AircraftSeries__c = 'AStest1';
        gf1.OriginalOperator__c ='testOpr';
        gf1.EngineVariant__c = 'EVtest1';
        gf1.Aircraft_Operator__c = opr.Id;
        gf1.AircraftStatus__c = 'Sale';
        listOfFleets.add(gf1);
        insert gf1;
        System.assert(gf1.Id != null);
        
        Lease__c lease1 = new Lease__c(
        Reserve_Type__c= 'Maintenance Reserves',
        Base_Rent__c = 21,
        Lease_Start_Date_New__c = System.today()-2,
        Lease_End_Date_New__c = System.today(),
        Operator__c = opr.id);
        LeaseWareUtils.clearFromTrigger();
        insert lease1;
        
        Marketing_Activity__c m2= new Marketing_Activity__c(Name='Dummy',Prospect__c =lease1.Operator__c,
                                                            Deal_Type__c='Purchase' ,Deal_Status__c = 'Pipeline',
                                                            Marketing_Rep_c__c = 'N/A',Description__c='111', 
                                                            Asset_Type__c = 'Aircraft');
        LeaseWareUtils.clearFromTrigger();
        insert m2;
        
        Aircraft_Proposal__c AT2 = new Aircraft_Proposal__c();
        AT2.World_Fleet_Aircraft__c = gf1.Id;
        AT2.Name = 'test';
        AT2.Marketing_Activity__c = m2.Id;
        insert AT2;
        
        CompetitorsOrderbookController.getNameSpacePrefix();
        CompetitorsOrderbookController.getDependentMap('Aircraft__c', 'Aircraft_Type__c', 'Engine_Type__c');
        CompetitorsOrderbookController.getAssetDetails(AT2.Id);
        CompetitorsOrderbookController.getDatePicklist(1998);
        CompetitorsOrderbookController.getLessor();
        CompetitorsOrderbookController.getCompetitorsOrderbook('A320', 'V2524', '1998', 3, 'ALL','New');
        CompetitorsOrderbookController.getCompetitorsOrderbook('A320', 'V2524', '1992', 2, 'ALL','Used');
        CompetitorsOrderbookController.groupBasedOnLessor(listOfFleets, 0, 1);
        
    }
}