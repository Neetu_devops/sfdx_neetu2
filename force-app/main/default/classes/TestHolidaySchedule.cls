/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestHolidaySchedule {

	@isTest(seeAllData=true)
    static  void CRUD_HolidaySchedule() {
        // TO DO: implement unit test
    	Lease__c newLeasae=new Lease__c();
    	newLeasae.Name='L1';
    	newLeasae.Lease_Start_Date_New__c=system.today();
    	newLeasae.Lease_End_Date_New__c=system.today()+50;
    	LeaseWareUtils.clearFromTrigger();insert newLeasae;
    	
    	Lessor__c setup1 = [select id from Lessor__c limit 1];
    	Holiday_Schedule__c HS1= new Holiday_Schedule__c(name='Dummy', Lessor__c  = setup1.Id,Year_New__c='2017');
    	LeaseWareUtils.clearFromTrigger();insert HS1;
    	
    	
    	
    	Applicable_Calendar__c ACal = new Applicable_Calendar__c(name= 'Dummy',Holiday_Schedule__c=HS1.Id,Lease__c=newLeasae.Id );        
		LeaseWareUtils.clearFromTrigger();insert ACal;
		LeaseWareUtils.clearFromTrigger();update ACal;
		LeaseWareUtils.clearFromTrigger();delete ACal;

    }
}