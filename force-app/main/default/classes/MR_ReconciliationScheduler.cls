global class MR_ReconciliationScheduler implements Database.Batchable<sObject> , Database.AllowsCallouts, Database.Stateful, Schedulable{

    private List<ResultWrapper> resultList;
    private Process_Request_Log__c ReqLog;

    public MR_ReconciliationScheduler(){       
        system.debug('Default Constructor+');
        this.resultList = new List<ResultWrapper>();
        system.debug('Default Constructor-');
    }
    

   //START METHOD    
   public Database.QueryLocator start(Database.BatchableContext BC){  

        ReqLog = new Process_Request_Log__c(Status__c ='Success',Type__c = 'MR Reconciliation'); 

        String strQuery = 'Select id, Name, Lease__c, Lease__r.Name, ';
               strQuery += 'To_Date__c, From_Date__c, Ratio_Variance__c ' ;
               strQuery += 'from Reconciliation_Schedule__c ';
               strQuery += 'where Eligible__c = true And Status__c = \'Open\''; /* And Type__c = \'Assumed Ratio\''; */     
        
        ReqLog.Exception_Message__c = strQuery ;        
                    
        return Database.getQueryLocator(StrQuery);
   }   


   public void execute(Database.BatchableContext BC, List<sObject> scope){        
       
        Reconciliation_Schedule__c reconciliationSchedule = (Reconciliation_Schedule__c)scope[0];

        map<id,Assembly_MR_Rate__c> srMap = new Map<id,Assembly_MR_Rate__c>( [select id from Assembly_MR_Rate__c where Lease__c =: reconciliationSchedule.Lease__c
                                                AND RecordTypeId=: Schema.SObjectType.Assembly_MR_Rate__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId()
                                                ]); 
           
        try{ 
        
            MRReconciliationInvoiceController.generateMRReconciliationUR(reconciliationSchedule,srMap.keySet());
            MRReconciliationInvoiceController.MRReconciliationInvoiceContainer invoiceData = MRReconciliationInvoiceController.generateInvoiceData(reconciliationSchedule, 'Assumed Ratio',srMap.keySet());
            MRReconciliationInvoiceController.saveData(JSON.serialize(invoiceData), System.today(), null);
            resultList.add(new ResultWrapper(reconciliationSchedule.Lease__c, reconciliationSchedule.Lease__r.Name + ' ' + reconciliationSchedule.Name, 'SUCCESS', ''));
            
        } catch(Exception ex){
                
            resultList.add(new ResultWrapper(reconciliationSchedule.Lease__c, reconciliationSchedule.Lease__r.Name + ' ' + reconciliationSchedule.Name, 'FAIL', ex.getMessage()));
            LeaseWareUtils.createExceptionLog(ex, 'Unexpected Exception while processing MR Reconciliation: ', null, null,null,true);
                
            //update status as 'Fail' only once if any error.
            if(ReqLog.Status__c == 'Success'){
                ReqLog.Status__c = 'Fail';
            }
                }               

            
       } 

   // Logic to be Executed at finish
   public void finish(Database.BatchableContext BC)
   {   
        system.debug('finish +');
        
        //insert request log.
        insert ReqLog;

        String emailSubject = 'Subject ---';
        String emailBody = 'Body ---';
        string outputStr ='Lease Id,Name,Status,Comments' + '\n';

        for(ResultWrapper er: resultList){
            outputStr += er.LeaseId + ',' + er.LeaseName + ',' + er.Status  + ',' + er.Comments  + '\n';
        }
        
        EmailTemplate[] emailTemplatelist = [ SELECT Id,Body,Subject,TemplateType,HTMLValue,DeveloperName FROM EmailTemplate 
                                        WHERE DeveloperName in ('MR_Recon_Fail_Notification','MR_Recon_Notification') 
                                        order by DeveloperName];
        
        if(!'Fail'.equals(ReqLog.status__c)){ // SUCCESS dtatus


            if(emailTemplatelist.size()>1){ //
                emailSubject = emailTemplatelist[1].Subject;
                emailBody    = emailTemplatelist[1].Body;
                
            }
        }
        else{// fail case
            system.debug('Failed status...');
            if(emailTemplatelist.size()>1){ //Fail_Notification
                emailSubject = emailTemplatelist[0].Subject;
                emailBody    = emailTemplatelist[0].Body.replace('{ERROR_MESSAGE_TOKEN}', ReqLog.Comments__c==null?'Unexpected Error': ReqLog.Comments__c);
            }           
        }   

            setProcessRequestLog(outputStr,ReqLog.Id,'ResultsLog.csv','ResultsLog.csv');
            
        list<Document> documents = new list<Document>();    
            Document AttachdocOutput = new Document();
            AttachdocOutput.AuthorId = UserInfo.getUserId();
            AttachdocOutput.FolderId = UserInfo.getUserId();
            AttachdocOutput.Body = Blob.valueOf(outputStr);
            AttachdocOutput.name = 'ResultsLog.csv' ; 
            documents.add(AttachdocOutput);
            
        // sending email
        list<string> toAddresses = new list<string>();
        Lessor__c setupRec = [select id,Notification_Emails__c from Lessor__c limit 1];
        
        toAddresses.add(UserInfo.getUserEmail());
        if(setupRec.Notification_Emails__c!=null){
            toAddresses.addAll(setupRec.Notification_Emails__c.split(','));
        }
        
        LeaseWareUtils.sendEmailTo(emailSubject, emailBody, toAddresses , null ,'TEXT',documents);      
   }

    public static string runMRReconciliation() {
        string retMessage = 'SUCCESS; Your request has been queued and the email will be sent shortly. You may close this window.';
        Database.executeBatch(new MR_ReconciliationScheduler(),1);    // running a batch with batch size 1 record
        return retmessage;
    }

    public void  execute(SchedulableContext SC) {

        runMRReconciliation();
    }
                                                
    public class ResultWrapper{
        public string LeaseId {get;set;}
        public string LeaseName {get;set;}
        public string Status {get;set;}
        public string comments {get;set;}
        // for UR
        public ResultWrapper(string LeaseId,string LeaseName ,string Status,string comments){
            this.LeaseId = LeaseId; this.LeaseName = LeaseName; this.Status = Status; this.comments = comments;
        }
                                                  
    }                                             

    @AuraEnabled
    public static string runMR_Recon_apex() {
        return runMRReconciliation();
    }

    public static void setProcessRequestLog(string resBody,ID ReqLogId,String Title,String Filename){
        LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.

        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVer.PathOnClient = Filename; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = Title; // Display name of the files
        //conver.FileType = 'PDF';
        conVer.VersionData = blob.valueof(resBody); //blob.toPDF(resBody) ; Blob.valueof(resBody); // converting your binary string to Blog
		conVer.Description = 'Leaseworks Internal';
		

        insert conVer;
        
        ContentVersion cover = [select ContentDocumentId from ContentVersion where id = :conVer.Id limit 1];
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = cover.ContentDocumentId;
        cDe.LinkedEntityId = ReqLogId; // you can use objectId,GroupId etc
        cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cDe.Visibility = 'AllUsers';
        insert cDe;
        LeaseWareUtils.TriggerDisabledFlag = false;
    }

    
}
