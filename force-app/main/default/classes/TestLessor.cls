/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestLessor {

    @isTest(seeAllData=true)
    static void myUnitTest() {
        Test.startTest();

		Lessor__c newLessor;
		try{
			newLessor = new Lessor__c(Name='New Lessor', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
				Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
				Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
			insert  newLessor;
			insert  newLessor; //Second insertion so that exception will be thrown.
		}catch(exception e){
			system.debug('Expected. Only one lease. ' + e);
			newLessor=[select id from Lessor__c limit 1]; 
		}
		newLessor.Number_Of_Months_For_Narrow_Body__c=24;
		newLessor.Number_Of_Months_For_Wide_Body__c=24;
		newLessor.Narrowbody_Checked_Until__c=date.today();
		newLessor.Widebody_Checked_Until__c=date.today();
		newLessor.Auto_Create_Campaigns__c=true;
		update newLessor;

		newLessor.Auto_Create_Campaigns__c=false;
        newLessor.Notification_Emails__c = 'abc@xyz.com';
		update newLessor;
		


		leasewareUtils.CalculateIRR(2.5, 25.7, 3.6, 2.5, 150, 3.6);
		leasewareUtils.CalculateIRR(2.5, 3.5, 5, 3.6, 4.5, 4.7);
		leasewareUtils.IsNotSame('a', 'b');
		leasewareUtils.IsNotSame(1.5, 1.5);
		leasewareUtils.IsNotSame((decimal)(1.5), (decimal)(1.5));
		leasewareUtils.IsNotSame(Date.today(), Date.today());
		leasewareUtils.IsNotSame(true, false);
		leasewareUtils.IsNotSame((object)(5), (object)(5), Schema.DisplayType.Double);
		
		
        Test.stopTest();        
        
    }
    
    
    static testmethod void myUnitTest2() {
        Test.startTest();

		Lessor__c newLessor;
		try{
			newLessor = new Lessor__c(Name='New Lessor', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
				Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
				Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today()
                                     ,Notification_Emails__c = 'abc@com');
			insert  newLessor;
		}catch(exception e){
			system.debug('Expected. Email pattern. ' + e);
		}
		
        Test.stopTest();        
        
    }   
   
    static testmethod void myUnitTest3() {
        Test.startTest();

		Lessor__c newLessor;
			newLessor = new Lessor__c(Name='New Lessor', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
				Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
				Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today()
                                     ,Notification_Emails__c = 'abc@xyz.com');
			insert  newLessor;
        try{
			LeaseWareUtils.clearFromTrigger();delete newLessor;
        }catch(exception e){}
        
        try{
			LeaseWareUtils.clearFromTrigger();undelete newLessor;
        }catch(exception e){}        

		
        Test.stopTest();        
        
    }      
}