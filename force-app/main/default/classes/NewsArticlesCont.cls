//@updated 2016.10.20 11pm: Arjun : Operator News Feed page controller
public class NewsArticlesCont
{ 
    //This class not required now. Resuable News Component has been created.
    /*private final Operator__c operator;
    public string headerFont                           {get;set;}
    public string contentFont                          {get;set;}
    public list<NewsArticle__c> articles               {get;set;}
    public string defaultMsg                           {get;set;}
    public string aviatortoken                         {get;set;}
    public boolean showMore                            {get;set;}
    public NewsArticlesCont(ApexPages.StandardController stdController) 
    {
        showMore = false;
        aviatorToken = null;
        articles     = new list<NewsArticle__c>();
        defaultMsg = NewsFeedServices.NONEWSFOUNDMSG;
        if(!Test.isRunningTest()) 
        {
            stdController.addFields(new List<String> {'Name'});
        }
        this.operator = (Operator__c)stdController.getRecord();        
        aviatorToken = NewsFeedServices.requestAviatorApiToken();
    }
    
    public void getNewFeeds()
    {   
        system.debug('showMore=='+showMore);
        NewsFeedServices newsService  = new NewsFeedServices();
        if( aviatorToken!=null && aviatorToken!='' )    {
             articles = newsService.getNewsArticles( operator.Id , null , aviatorToken , showMore );
        }           
        
        if( articles.isEmpty() )
        {           
            NewsArticle__c newtnTmp = new NewsArticle__c( Article__c = defaultMsg ,
                                                          Tip__c = '', Date__c = null, link__c=null );
            articles.add(newtnTmp);
        }  
        
        System.debug('articles : '+articles.size());
   
        if( NewsFeedServices.isSF1() ) {
            headerFont  = '0.9em';
            contentFont = '0.8em';
        }
        else {          
            headerFont  = '1.1em';
            contentFont = '1.0em';
        }
    }*/ 
}