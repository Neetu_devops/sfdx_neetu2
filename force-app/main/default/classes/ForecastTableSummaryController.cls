public class ForecastTableSummaryController {
	
    // Load data to be displayed on DeliveryReturnCondition Table on Summary Tab.
    @auraEnabled
    public static ForecastTableSummaryWrapper getReserveType(Id scenarioInp){
        try{
            List<Scenario_Input__c> scenarioInputList = [SELECT Id, Ext_Reserve_Type__c,EOLA__c
                                                            FROM Scenario_Input__c
                                                            WHERE Id =: scenarioInp LIMIT 1];
            ForecastTableSummaryWrapper forecastWrapper = new ForecastTableSummaryWrapper();
                    if(!scenarioInputList.isEmpty()){
                        Scenario_Input__c sc =scenarioInputList[0];
                        forecastWrapper.isEOLA = sc.EOLA__c;
                        forecastWrapper.lessor = checkLessor();
                        forecastWrapper.extType = sc.Ext_Reserve_Type__c != null ? sc.Ext_Reserve_Type__c : '';
                        if(forecastWrapper.isEOLA && forecastWrapper.extType != 'Monthly MR'){ forecastWrapper.reserveType = 'EOLA'; }
                        else if(forecastWrapper.isEOLA && forecastWrapper.extType == 'Monthly MR'){ forecastWrapper.reserveType = 'BOTH'; }
                        else if(!forecastWrapper.isEOLA && forecastWrapper.extType != 'EOLA'){ forecastWrapper.reserveType = 'MR'; }
                        else if(!forecastWrapper.isEOLA && forecastWrapper.extType == 'EOLA'){ forecastWrapper.reserveType = 'BOTH'; }
                    }
                
            
            //System.debug('List-->>'+JSON.serialize(lstRCW));
            return forecastWrapper;
        }catch(Exception e){
            throw new AuraHandledException('An internal Server Error has Occured: '+e.getMessage()+' Line Number: '+e.getLineNumber());
        }
    }
    
    @AuraEnabled
    public static String checkLessor(){
        return Utility.checkAirline();
    }
    
    @AuraEnabled
    public static String isForecastGenerated(String recordId) {
        return Utility.checkForecastStatus(recordId);
    }
    

    public class ForecastTableSummaryWrapper{
        @auraEnabled public Boolean isEOLA{get; set;}
        @auraEnabled public String lessor{get; set;}
        @auraEnabled public String extType{get; set;}
        @auraEnabled public String reserveType{get; set;}
    }
}