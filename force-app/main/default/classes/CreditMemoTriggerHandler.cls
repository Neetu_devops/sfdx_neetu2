public without sharing class CreditMemoTriggerHandler implements ITrigger {

    private final string triggerBefore = 'CreditMemoTriggerHandlerBefore';
    private final string triggerAfter = 'CreditMemoTriggerHandlerAfter';
    @testvisible
    private static boolean bIsUndoCancellation = false;
    @testvisible
    private static boolean bIsCancelCm = false;

    
    public CreditMemoTriggerHandler() {}
    public void bulkBefore(){}
     
   
    public void bulkAfter(){}

    public void beforeInsert(){

        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
    }
    
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        list<credit_memo__c> listCms =(list<credit_memo__c>)trigger.new;
        set<string> setSkipFields = RLUtility.getAllFieldsFromFieldset('credit_memo__c','SkipFieldsFromValidations');//add fields that should be skipped always
        
        if(listCms[0]!=null && 
                listCms[0].getQuickActionName() == Schema.credit_memo__c.QuickAction.Undo_Cancellation){bisUndoCancellation = true;}
        else if(listCms[0]!=null && 
                listCms[0].getQuickActionName() == Schema.credit_memo__c.QuickAction.Cancel_Credit_Memo){bIsCancelCm = true;}
        if(bIsUndoCancellation || bIsCancelCm){
            quickActionCall();
            return;
        }
        boolean isError = updateValidations(setSkipFields);
        if(!isError){
            cancelCreditMemo();
        }
    }
    
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){ 
       if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        System.debug('CreditMemoTriggerHandler-after Delete(+)');
        set<id> memosToDel = new set<id>();
        list<credit_memo__c> delMemos  = (list<credit_memo__c>)trigger.old;
        list<payment__c> creditsTodelete = new list<payment__c>();
        list<journal_entry__c> journalsTodelete = new list<journal_entry__c>();
        try{
           
            for(credit_memo__c delcm : delMemos){
                if(!delcm.status__c.equals('Pending') ){
                    delcm.addError('Only pending credit memos can be deleted.');
                }
                else {
                    memosToDel.add(delcm.id);
                }
            }
            if(memosToDel.size()>0){
                list<credit_memo__c> creditMemos = new list< credit_memo__c>([ select id, (select id,payment_status__c from View_Payments__r),(select id from journal_entries__r)
                                from credit_memo__c where id in :memosToDel]);
                for(credit_memo__c cm :creditMemos){
                    system.debug('cm.View_Payments__r:::'+cm.View_Payments__r);
                    system.debug('cm.journal_entries__r:::'+cm.journal_entries__r);
                    if(cm.View_Payments__r!=null && cm.View_Payments__r.size()>0){
                        creditsTodelete.addAll(cm.View_Payments__r);
                    }
                    if(cm.journal_entries__r!=null && cm.journal_entries__r.size()>0){
                        journalsTodelete.addAll(cm.journal_entries__r);
                    }
                }
            }
            if(!creditsTodelete.isEmpty()){
                try{
                    delete creditsTodelete;
                }
                catch(DmlException ex){
                    System.debug(ex);
                    string errorMessage='';
                    for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                        // Process exception here
                        errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                    }
                    system.debug('errorMessage=='+errorMessage);
                    for(credit_memo__c cm : delMemos){
                        cm.addError(errorMessage);
                    }
                }
            }
            if(!journalsTodelete.isEmpty()){
                try{
                    delete journalsTodelete;
                }
                catch(DmlException ex){
                    System.debug(ex);
                    string errorMessage='';
                    for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                        // Process exception here
                        errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                    }
                    system.debug('errorMessage=='+errorMessage);
                    for(credit_memo__c cm : delMemos){
                        cm.addError(errorMessage);
                    }
                   
                }
            }
        }
        Catch(Exception e){
            system.debug('Exception while deleting creditmemo::'+e+'::'+e.getlineNumber());
            throw new LeaseWareException(e.getMessage());    
        }

        createDeleteParallelReportingRecord(); // delete the parallel reporting record
            
        System.debug('CreditMemoTriggerHandler-before Delete(-)');   
    }
    
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        System.debug('CreditMemoTriggerHandler-after Insert(+)');
        list<credit_memo__c> newMemos  = (list<credit_memo__c>)trigger.new;
        createAccountingJournals(newMemos);
        createDeleteParallelReportingRecord();
        System.debug('CreditMemoTriggerHandler-after Insert(-)');
    }

    /***
     * @Desc : Method creates journal entries for Credit Memo Created
     * @Called from : After Insert, updateJournals
     * @param : list of credit memos
     */
    private void createAccountingJournals(list<credit_memo__c> newMemos){
        
        system.debug('createAccountingJournals(+)');  
        list<credit_memo__c> cmWithLease = new list<credit_memo__c>();
        list<credit_memo__c> cmWithoutLease = new list<credit_memo__c>();
        list<journal_entry__c> insertJournals =  new List<Journal_Entry__c>();
        set<id> leaseIds = new set<Id>();
        Date startDate = null;
        Date endDate = null;
        map<id,calendar_period__c> calPMap = null;
        for(credit_memo__c cm :newMemos){
            if(cm.Y_Hidden_IsStandalone__c == false) continue;
            if(cm.lease__c == null){
                cmWithoutLease.add(cm);
            }
            else if(cm.lease__c !=null && cm.Y_Hidden_Acc_Enabled__c){
                cmWithLease.add(cm);
                leaseIds.add(cm.lease__c);
            }
            if(startdate==null || startdate > cm.Credit_Memo_Date__c.toStartOfMonth())startDate = cm.Credit_Memo_Date__c.toStartOfMonth();
            if(endDate == null || endDate < cm.Credit_Memo_Date__c.addMonths(1).toStartOfMonth().addDays(-1)) endDate = cm.Credit_Memo_Date__c.addMonths(1).toStartOfMonth().addDays(-1) ;
        }
        if(!cmWithoutLease.isEmpty() || !cmWithLease.isempty()){
            calpMap = new map<id,Calendar_Period__c>([select id,name,closed__c,start_date__c,end_date__c from calendar_period__c where start_date__c >= :startDate and end_date__c <=:endDate]);
        }
        if(cmWithoutLease.size()>0){
            Lessor__c lessor = [select Accounting_enabled__c,CM_Rev_Reduction__r.GL_Account_Description__c,CM_Rev_Reduction__r.GL_Code__c,
                                    Unapplied_cm__r.GL_Account_Description__c, Unapplied_cm__r.GL_Code__c from lessor__c limit 1];
            if(Lessor!=null && lessor.Accounting_enabled__c){
                insertJournals.addAll(createJourals(calpMap,lessor,cmWithoutLease,null,null));
            }
        }
        if(cmWithLease.size()>0){
            map<id, Lease__c> mapLeases = new map<Id, Lease__c>([select id, Accounting_Setup_P__c,legal_entity__r.name,line_company__r.name from Lease__c where id in :leaseIds]);

            set<Id> setParentAcSetupIds = new set<Id>();
            set<Id> setAcSetupIds = new set<Id>();
            for(Lease__c curLease : mapLeases.values()){
                setParentAcSetupIds.add(curLease.Accounting_Setup_P__c);
            }
            list<accounting_setup__c> listAcc = [select id from accounting_setup__c where parent_accounting_setup__c in :setParentAcSetupIds];
            for(accounting_setup__c acc :listAcc){
                setAcSetupIds.add(acc.id);
            }
            list<GL_Account_Code__c> listGLACCodes = [select Id, Accounting_Setup__c,Accounting_Setup__r.parent_accounting_setup__c, Account_description__c, GL_Account_Code_F__c, Transaction_Type__c, Sign__c, 
                        Account_Classification__c, JE_Description__c,Transaction_Subtype__c,Intercompany__c
                        from GL_Account_Code__c
                        where Accounting_Setup__c in :setAcSetupIds
                        and ((Transaction_Type__c = 'Credit Memo' /*AND Transaction_Subtype__c = 'Payment'*/)) ];

            system.Debug('listGLACCodes:::'+listGLACCodes.size());
            if(listGLACCodes!=null && listGLACCodes.size()>0 ){
                insertJournals.addAll(createJourals(calpMap,null,cmWithLease,mapLeases,listGLACCodes));
            }
           
        }
        if(!insertJournals.isempty()){
            try{
                insert insertJournals;
            }
            catch(DmlException ex){
                System.debug(ex);
                string errorMessage='';
                for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                    // Process exception here
                    errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                }
                system.debug('errorMessage=='+errorMessage);
                for(credit_memo__c cm : newMemos){
                    cm.addError(errorMessage);
                }
            }
           
        } 
        system.debug('createAccountingJournals(-)');  
    }
    /***
     * @Desc : Method creates journal entries for Credit Memo Created at Lessee snf lease level
     * @Called from : createAccountingJournals()
     * @param : Map of Calendar Period, Setup Object(Lessor),The list of credit memos,map of lease,
     *            list of GL account codes
     * @Return : list of journal entry object
     */
    private list<journal_entry__c> createJourals(map<id,Calendar_period__c> calpMap,Lessor__c lessor,list<credit_memo__c> cmList,
                                                        map<id,lease__c> mapLeases,list<GL_Account_Code__c> listGLACCodes){

        system.debug('createJourals(+)');                                                    
        String status ='';
        list<journal_entry__c> insertJournals = new list<journal_entry__c> ();
        for(credit_memo__c  memo :cmList ){
            Calendar_Period__c CalP = null;
            Lease__c curLease = null;
            if(mapLeases!=null){
                curLease = mapLeases.get(memo.Lease__c);
                if(curLease==null)continue;
            }
            Status = getJournalStatus(memo.status__c);
            if(calpMap!=null ) {
                for(Calendar_Period__c curCalPrd: calpMap.Values()){
                    if(memo.Credit_Memo_Date__c>=curCalPrd.Start_Date__c && memo.Credit_Memo_Date__c<=curCalPrd.End_Date__c){
                        CalP=curCalPrd;
                        break;
                    }
                }
            }
            if(CalP == null){
                memo.addError('Accounting Period is not found for '+memo.Credit_Memo_Date__c);
                continue;
            }
            else if(memo.status__c.equals('Approved')&& CalP.closed__c== true){
                memo.addError('Accounting Period for '+CalP.name+ ' is closed. Please reopen the period or choose another date');
                continue;
            }
            system.debug('Lessor:::'+lessor);
            if(lessor !=null && listGLACCodes == null){
                insertJournals.add(new Journal_Entry__c(Name=lessor.CM_Rev_Reduction__r.GL_Account_Description__c,
                                    credit_memo__c = memo.id,
                                    GL_Account_Code__c=lessor.CM_Rev_Reduction__r.GL_Code__c, Calendar_Period__c=CalP.id, Journal_Date__c=system.today(),
                                    Transaction_Type__c='Credit Memo', journal_status__c = status,Legal_Entity__c = memo.Y_Hidden_Company_Name__c,
                                    Amount__c=memo.amount__c, JE_Description__c='Credit Memo Revenue Reduction',Sign__c='D', 
                                    Debit__c= memo.amount__c, Credit__c=0.0));
                insertJournals.add(new Journal_Entry__c(Name=lessor.Unapplied_cm__r.GL_Account_Description__c,
                                    credit_memo__c = memo.id,
                                    GL_Account_Code__c=lessor.Unapplied_cm__r.GL_Code__c, Calendar_Period__c=CalP.id, Journal_Date__c=system.today(),
                                    Transaction_Type__c='Credit Memo', journal_status__c = status,Legal_Entity__c = memo.Y_Hidden_Company_Name__c,
                                    Amount__c=-1 * memo.amount__c, JE_Description__c='Unapplied Credit Memo',
                                    Sign__c='C', credit__c= memo.amount__c, debit__c=0.0));
            }
            if(mapLeases !=null && listGLACCodes!=null){
                integer intSign ;
                String strLE,strInterCoLE,strGLAc, strGLDec;
                for(GL_Account_Code__c curGLCode: listGLACCodes){
                    intSign = 'D'.equals(curGLCode.Sign__c)?1:-1;
                    strLE = curLease.legal_entity__r.name;
                    strInterCoLE = curLease.line_company__r.name;
                    strGLAc = curGLCode.GL_Account_Code_F__c;
                    strGLDec =  curGLCode.Account_description__c;

                    insertJournals.add(new Journal_Entry__c(Name=strGLDec,  credit_memo__c = memo.id,
                                        GL_Account_Code__c=strGLAc, Calendar_Period__c=calP.id, Journal_Date__c=system.today(),
                                        Transaction_Type__c='Credit Memo', journal_status__c = status,
                                        Lease__c = curLease.id,
                                        Amount__c=intSign*memo.amount__c, JE_Description__c=curGLCode.JE_Description__c,
                                        Sign__c=curGLCode.Sign__c, Legal_Entity__c=strLE, Intercompany__c=strInterCoLE, Debit__c=(intSign==1?memo.amount__c:0), 
                                        Credit__c=(intSign==-1?memo.amount__c:0)));
                }

            }
        }
        system.debug('createJourals(-)');  
        return insertJournals;

    }
    
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        System.debug('CreditMemoTriggerHandler-after update(+)');
        if(bIsUndoCancellation || bIsCancelCm){return;}
        updateCreditMemoIssuance();
        updateJournals();
        approveJournals();
        System.debug('CreditMemoTriggerHandler-after update(-)');
    }

        /***
     * @Desc : Method updates child Credit memo Issuance
     * @Called from : After update
     * 
     */
    private void updateCreditMemoIssuance(){
        system.debug('updateCreditMemoIssuance(+)');
        list<credit_memo__c> listCreditMemos = (list<credit_memo__c>)trigger.new ;
       update [Select id, name from Credit_Memo_Issuance__c where Credit_Memo__c in: listCreditMemos];
       system.debug('updateCreditMemoIssuance(+)');
    }

    /***
     * @Desc : Method creates journal entries for Credit Memo Created
     * @Called from : After update
     * 
     */
    private void updateJournals(){
        system.debug('updateJournals(+)');
        list<credit_memo__c> changedCMs = new list<credit_memo__c>();
        for (credit_memo__c cm : (list<credit_memo__c>)trigger.new){
            Credit_Memo__c oldcm = (credit_memo__c)trigger.oldMap.get(cm.id);
            //if(cm.lease__c!=null && !cm.Y_Hidden_Acc_Enabled__c)continue;
            if(oldcm.status__c !='Pending')continue;
            if(!oldcm.Y_Hidden_IsStandalone__c)continue;
           
            if(oldcm.Credit_Memo_Date__c !=cm.Credit_Memo_Date__c||oldcm.amount__c!=cm.amount__c||oldcm.Lessee__c !=cm.lessee__c ||
                oldcm.lease__c !=cm.lease__c){
                    changedCMs.add(cm);
                }
        }
        if(changedCMs.size()>0){
            system.debug('Changed CMs::'+changedCMs.size());
            list<journal_entry__c> journals = [select id from journal_entry__c where credit_memo__c in :changedCMs];

            if(null != journals && journals.size()>0){
                try{
                    delete journals;
                }
                catch(DmlException ex){
                    System.debug(ex);
                    System.debug(ex);
                    string errorMessage='';
                    for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                        // Process exception here
                        errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                    }
                    system.debug('errorMessage=='+errorMessage);
                    for(credit_memo__c cm : changedCMs){
                        cm.addError(errorMessage);
                    }
                }
                
            }
                createAccountingJournals(changedCMs);
        }
        system.debug('updateJournals(+)');
    }

     /***
     * @Desc : Method approved the journals.
     * @Called from : After update
     * 
     */
    private void ApproveJournals(){
        system.debug('ApproveJournals(+)');
        set<id> cmIds = new set<id>();
        map<id,credit_memo__c> approvedMap= new map<id,credit_memo__c>();
        list<credit_memo__c> newList = (list<credit_memo__c>)trigger.new;
        for(credit_memo__c creditMemo : newList){
            if(!creditMemo.status__c.equals('Approved'))continue;
            if('Approved'.equals(((credit_memo__c)trigger.oldMap.get(creditMemo.Id)).status__c))continue;
            cmIds.add(creditMemo.id);
            approvedMap.put(creditMemo.id,creditmemo);
        }
        if(cmIds.size()>0){
            list<journal_entry__c> journalsToUpdate = new  list<journal_entry__c>();
            list<payment__c> pysToUpdate = new list<payment__c>();
            list<credit_memo__c> creditMemos = [select id,status__c,
                (select id,Journal_Status__c,calendar_period__r.name,Calendar_period__r.closed__c
                    from Journal_Entries__r),(select id,payment_status__c from view_payments__r) FROM credit_memo__c where id in :cmIds];
                    String status;
            for(credit_memo__c cm : creditMemos){
                status = getJournalStatus(cm.status__c);
                Credit_memo__c errorMemo = approvedMap.get(cm.id);
                for(journal_entry__c je :cm.Journal_Entries__r ){
                        if(je.Calendar_period__r.closed__c == true){
                            if(errorMemo!=null)errorMemo.addError('Accounting Period for '+je.Calendar_period__r.name+ ' is closed. Please reopen the period or choose another date');
                            break;
                        }
                        else{
                            je.Journal_Status__c = status;
                            journalsToUpdate.add(je);
                        }
                }
                for(payment__c py :cm.view_payments__r ){
                    if(cm.status__c.equals('Approved')){
                        py.payment_status__c ='Approved';
                        pysToUpdate.add(py);
                    }
                }   
            }
                if(journalsToUpdate.size() >0) {
                    try{
                        update journalsToUpdate; 
                    }
                    catch(DmlException ex){
                        System.debug(ex);
                        string errorMessage='';
                        for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                            // Process exception here
                            errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                        }
                        system.debug('errorMessage=='+errorMessage);
                        for(credit_memo__c cm : newList){
                            cm.addError(errorMessage);
                        }
                       
                    }
                }

                if(pysToUpdate.size()>0){
                    try{
                        LeaseWareUtils.TriggerDisabledFlag=true;
                        update pysToUpdate;
                        LeaseWareUtils.TriggerDisabledFlag=false;
                    }
                    catch(DmlException ex){
                        System.debug(ex);
                        string errorMessage='';
                        for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                            // Process exception here
                            errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                        }
                        system.debug('errorMessage=='+errorMessage);
                        for(credit_memo__c cm : newList){
                            cm.addError(errorMessage);
                        }
                    }
                }
        }
        system.debug('ApproveJournals(-)');

    }
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(){
      
    }
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterUnDelete(){} 
    
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){}

    private String getJournalStatus(String status){
        String jestatus ='';
        switch on status {
            when  'Pending'{
                jestatus = 'Draft - Pending Approval';
            }
            when  'Approved'{
                jestatus = 'Posted';
            }
            
        }  
        return jestatus; 
    }

    /**
     * validates the updates on CM and shows error messages
     * called from before update
     */
    private boolean updateValidations(Set<String> setSkipFields){
        system.debug('Inside updateValidations:::');
        credit_memo__c oldMemo = new credit_memo__c();
        Map<String, Schema.SObjectField> mapInvField = Schema.SObjectType.credit_memo__c.fields.getMap();
        String thisFieldDesc =  null;
        boolean isError = false;
        for(credit_memo__c memo:(list<credit_memo__c>)trigger.new){
            oldMemo = (credit_memo__c)trigger.oldMap.get(memo.id);
            if(('Pending'.equals(oldmemo.status__c)||'Approved'.equals(oldMemo.status__c))&&
                    oldMemo.status__c != memo.status__c && ('Cancelled'.equals(memo.status__c)||'Cancelled-Pending'.equals(memo.status__c))){
                        memo.addError('Manual update to Cancelled/Cancelled-Pending is not allowed');
                        isError = true;
                        break;
            }
            else if('Approved'.equals(oldMemo.Status__c)||'Cancelled'.equals(oldMemo.Status__c)||'Cancelled-Pending'.equals(oldMemo.Status__c) 
                        ||('Pending'.equals(oldMemo.Status__c)&& memo.Y_Hidden_IsStandalone__c==false)){
                for(String Field:mapInvField.keyset()){
                    thisFieldDesc = (mapInvField.get(Field).getDescribe()).getLocalName();
                    if(thisFieldDesc!=null){
                        //status update is only allowed for non standalone cm
                        if(setSkipFields.contains(leasewareutils.getNamespacePrefix() + thisFieldDesc)) continue;
                        if('Approved'.equals(Memo.Status__c)&&'Pending'.equals(oldMemo.Status__c) && thisFieldDesc.equalsIgnoreCase('status__c'))continue;
                        if('Cancelled'.equals(Memo.Status__c)&&'Cancelled-Pending'.equals(oldMemo.Status__c) && thisFieldDesc.equalsIgnoreCase('status__c'))continue;
                        //if( thisFieldDesc.equalsIgnoreCase('status__c')&& memo.Y_Hidden_IsStandalone__c==false)continue;
                        if( (thisFieldDesc.equalsIgnoreCase('Allocated_Amount__c') ||thisFieldDesc.equalsIgnoreCase('Available_Credit_Memo_Amount_F__c'))
                                   /* && memo.Y_Hidden_IsStandalone__c==true*/)continue;
                        if(memo.get(thisFieldDesc) !=  oldMemo.get(thisFieldDesc)){ 
                            if(memo.Y_Hidden_IsStandalone__c==false) {
                                memo.addError('This user action is not permitted. This is a system generated credit memo; only the status can be changed. To change the information on this credit memo, please delete and generate a new one.');
                                isError = true;
                                break;
                            }
                            else if('Approved'.equals(oldMemo.Status__c)) {
                                memo.addError('Update not allowed on Approved Credit Memos');
                                isError = true;
                                break;
                            }
                            else if('Cancelled-Pending'.equals(oldMemo.Status__c)){
                                 memo.addError('Update not allowed on Cancelled-Pending Credit Memos');
                                 isError = true;
                                 break;
                             }
                            else if('Cancelled'.equals(oldMemo.Status__c)) {
                                memo.addError('Update not allowed on Cancelled Credit Memos');
                                isError = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        system.debug('Inside updateValidation(-)'); 
        return isError;  
    }

    /***
     * Calle from the before update
     */
     private static void quickActionCall(){
        list<credit_memo__c> cms = (list<credit_memo__c>)trigger.new;
        //expecting only one record on quick action
        if(cms!=null && cms[0]!=null){
            credit_memo__c oldCm = (credit_memo__c)trigger.oldMap.get(cms[0].id);
            boolean isError = checkAndSetStatus(cms[0],oldCm);
            if(isError && bIsCancelCm){
                cms[0].addError('Only Approved credit memos can be Cancelled');
                return;
            }
            if(isError && bIsUndoCancellation){
                cms[0].addError('This action can only be performed on credit memos in Cancelled-Pending status');
                return;
            }
        }
     }
    /***
     * This method is to check the invoice status before moving the status to Cancelled-Terminated/Approved.
     * called from quickactionCall.
     */
    private static boolean  checkAndSetStatus(credit_memo__c cm,credit_memo__c oldcm){
        System.debug('checkAndSetStatus:(+)');
        
        boolean isError = false;
        system.debug('oldCm.status__c::'+oldCm.status__c);
        if(oldCm!=null){
            if(bIsCancelCm){
                if(!'Approved'.equals(oldCm.status__c)){
                    system.debug('Error in status');
                    isError = true;
                }
                else {
                    cm.status__c ='Cancelled-Pending';
                }
            }
            if(bIsUndoCancellation){
                if(cm.status__c != oldCm.status__c || !'Cancelled-Pending'.equals(oldCm.status__c)){
                    system.debug('Error in status');
                    isError = true;
                }
                else {
                    cm.status__c ='Approved';
                }
            }
        }
        System.debug('checkAndSetStatus:(-)');
        return isError;
    }
    /***
     * Cancels the credit memos and corresponding credit
     */
    private void cancelCreditMemo(){
        system.debug('Inside cancelCreditMemo(+)');
        list<credit_memo__c> newList = (list<credit_memo__c>)trigger.new;
        map<id,credit_memo__c>  cancelledCmsMap = new map<id,credit_memo__c>();
        list<payment__c> creditsToUpdate = new list<payment__c>();
        list<credit_memo__c> cmToUpdate = new list<credit_memo__c>();
        for(credit_memo__c cm : newList){
            if('Cancelled'.equals(cm.status__c)){
                cancelledCmsMap.put(cm.id,cm);  
            }
        }
        if(cancelledCmsMap.size()>0){
            Decimal amount = 0.0;
            list<credit_memo__c> cancelledList = [select id,Allocated_Amount__c,(select id,amount__c,payment_status__c,status__c from View_Payments__r where payment_status__c != 'Cancelled')
                                                    from credit_memo__c where id IN :cancelledCmsMap.keySet()];
            for(credit_memo__c cm :cancelledList){
                credit_Memo__c creditMemo = cancelledCmsMap.get(cm.id);
                amount = 0.0;
                for(payment__c py : cm.View_Payments__r){
                    amount += py.amount__c; 
                    py.payment_status__c = 'Cancelled';
                    creditsToUpdate.add(py);
                }//end of py loop
                creditMemo.Allocated_Amount__c = LeaseWareUtils.zeroIfNull(creditMemo.Allocated_Amount__c) - amount;
            }//end of credit memo
            if(!creditsToUpdate.isEmpty()){
                try{
                    LeaseWareUtils.setFromTrigger('CM_CREDIT_UPDATE');
                    update creditsToUpdate;
                    LeaseWareUtils.setFromTrigger('CM_CREDIT_UPDATE');
                }
                catch(DmlException ex){
                    System.debug(ex);
                    string errorMessage='';
                    for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                        // Process exception here
                        errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                    }
                    system.debug('errorMessage=='+errorMessage);
                    for(credit_memo__c cm : cancelledCmsMap.values()){
                        cm.addError(errorMessage);
                    }
                }
            }
        }//end of if
        system.debug('Inside cancelCreditMemo(-)');

    }
     /*Method to create Prallel Reporting object on afterInsert and delete from beforeDelete     */
   private void createDeleteParallelReportingRecord(){
        System.debug('CreditMemoTriggerHandler.createDeleteParallelReportingRecord(+)');
        list<credit_memo__c> listToUpdate = (list<credit_memo__c>)(trigger.isDelete?trigger.old:trigger.new);   
        List<Operational_Transaction_Register__c> listToDelete = new List<Operational_Transaction_Register__c>();
        Set<Id> setIds = new Set<Id>();
        for(credit_memo__c cm : listToUpdate){
            setIds.add(cm.Id);
        }
        if(trigger.isDelete){   
            System.debug('Deleting record ---- ');
            // delete, cant handle that from future event , as record is already deleted by the time platform event is fired
            for(Operational_Transaction_Register__c rec: [Select id, Name from Operational_Transaction_Register__c where Credit_Memo__c in:setIds]){   
                listToDelete.add(rec);
            }
        }else{
            LeasewareUtils.CreatePlatformEvent('insertParallelReportingObject', JSON.serialize(setIds));  
        }         
        if(listToDelete.size()>0){
            try{
                System.debug('listToDelete:' + listToDelete);
                 delete listToDelete;   // dont have any trigger for this or no other related trigger will be called, so not disabling trigger here 
            }    
            catch(Exception e){
                 System.debug('Exception e: ' + e.getMessage());
                 LeasewareUtils.createExceptionLog(e,e.getMessage(),'Operational_Transaction_Register__c','', 'Delete Operational_Transaction_Register__c',true);
            }  
        }
        System.debug('CreditMemoTriggerHandler.createDeleteParallelReportingRecord(-)');
    }

}