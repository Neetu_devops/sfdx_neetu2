public class IncomingDataTriggerHandler implements ITrigger{
    
    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
    
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
    
   // private Map<String,String> listToMap;  
//	private List<Mapping__c> listVariantMap;    
    private static string leaseNameType = LeaseWareUtils.getLWSetup_CS('LEASE_NAME_TYPE');
    private static string doNotStripBFromBoeing = LeaseWareUtils.getLWSetup_CS('DO_NOT_STRIP_B_FROM_BOEING');
	private static boolean bStripBFromBoeing = !('Yes'.equalsIgnoreCase(doNotStripBFromBoeing));
    private static string includeZeroInMSN = LeaseWareUtils.getLWSetup_CS('INCLUDE_ZERO_IN_SN');
    private static boolean bIncludeZeroInMSN = ('Yes'.equalsIgnoreCase(includeZeroInMSN));
    @testvisible
    private static string syncPortalAssets = LeaseWareUtils.getLWSetup_CS('Sync Portal Assets');
    
    // Constructor
    private final string  triggerBefore = 'IncomingDataTriggerHandlerBefore';
    private final string  triggerAfter = 'IncomingDataTriggerHandlerAfter';
    public IncomingDataTriggerHandler()
    {
    }
    
    /**
* bulkBefore
*
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
    
    public void bulkAfter()
    {
    }
    
    public void beforeInsert(){}

    
    public void beforeUpdate(){}

    
    /**
* beforeDelete
*
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete(){}

    
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('IncomingDataTriggerHandler.afterInsert(+)');
        
        IncomingDataProcessInfo();
        
        system.debug('IncomingDataTriggerHandler.afterInsert(-)');     	
    }
    
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('IncomingDataTriggerHandler.afterUpdate(+)');
        
        IncomingDataProcessInfo();
        
        system.debug('IncomingDataTriggerHandler.afterUpdate(-)');     	
    }
    
    public void afterDelete(){}

    
    public void afterUnDelete(){}

    
    /**
* andFinally
*
* This method is called once all records have been processed by the trigger. Use this
* method to accomplish any final operations such as creation or updates of other records.
*/
    public void andFinally()
    {
        // insert any audit records
        
    }
    
    //after Insert,after Update)
    private void IncomingDataProcessInfo(){
        
        boolean bDisableLesseesAsOps=false;
        LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
        if(lwSettings!=null){
            bDisableLesseesAsOps=lwSettings.Disable_Lessees_As_Ops__c;
        }
        
        list<Aircraft_Stage__c> listAircraftToUpdate = new list<Aircraft_Stage__c>();
        list<Operator_Stage__c> listOperatorsToUpdate = new list<Operator_Stage__c>();
        list<Lease_Stage__c> listLeasesToUpdate = new list<Lease_Stage__c>();
        list<Portal_Asset_Staging__c> listPortalAssetToUpdate =  new list<Portal_Asset_Staging__c>();
        list<sObject> listStagingToDelete = new list<sObject>();
        map<Id, Incoming_Data_Processing_Log__c> mapIncomingDataLogs = new map<Id, Incoming_Data_Processing_Log__c>();
        Map<String, Schema.SObjectField> mapIncomingDataFields = Schema.SObjectType.Incoming_Data__c.fields.getMap();
           
        set<string> setContacts = new set<string>();
        set<string> setOperators = new set<string>();

        Database.delete([select id from Aircraft_Stage__c where IsDeleted = false],false);
        Database.delete([select id from Lease_Stage__c where IsDeleted = false],false);
        Database.delete([select id from Operator_Stage__c where IsDeleted = false],false);
        Database.delete([select id from Portal_Asset_Staging__c where IsDeleted = false],false);
        Database.delete([select id from Incoming_Data_Processing_Log__c where createdDate < :Date.today().addDays(-2) and IsDeleted = false limit 10000],false);
       
        
        string strName;      
        Incoming_Data__c[] listNew = (list<Incoming_Data__c>)trigger.new ;
        for(Incoming_Data__c curInfo : listNew){
            Incoming_Data_Processing_Log__c incoming_log = new Incoming_Data_Processing_Log__c(Error_Message__c = 'PROCESSING');
            for(String Field:mapIncomingDataFields.keyset()) {
                Schema.DescribeFieldResult thisFieldDesc = mapIncomingDataFields.get(Field).getDescribe();
                if(!thisFieldDesc.isUpdateable())continue;
                if(!thisFieldDesc.isCustom() && !Field.equalsIgnoreCase('Name') )continue; //skip standard field other than Name
                try{
                    incoming_log.put(thisFieldDesc.getName(), curInfo.get(Field));
                }catch(exception e){
                    system.debug('Processing Log : Could not insert ' + Field + ' - ' + curInfo.get(Field));
                }
            }
            
            try{    
                
                //Process Aircraft
                //Processing Aircraft records
                
                string strCurMSN = curInfo.MSN__c;
                
                //If MSN is null and Production is Not, then copy the value of Production to strCurMSN.
                if(curInfo.Production_Number__c != null && strCurMSN==null){
                    strCurMSN = curInfo.Production_Number__c;
                }
                
                if(strCurMSN==null) continue;	
                // Removing leading zero from MSN if custom setting INCLUDE_ZERO_IN_SN is NO or '' or null
                if(!bIncludeZeroInMSN){
                    while('0'.equals(strCurMSN.left(1))){
                    strCurMSN=strCurMSN.right(strCurMSN.length()-1);
                    }
                }
                System.debug('MSN::' + strCurMSN);
                System.debug('Variant::' + curInfo.Variant__c);
                if(''.equals(strCurMSN))continue;
                List<String> listOfTypeandVariant = new List<String>();
                if(curInfo.Variant__c==null)continue;
                listOfTypeandVariant = setTypeandVariant(curInfo.Variant__c,listOfTypeandVariant);
                string strAcType = listOfTypeandVariant[0];
                String variant = listOfTypeandVariant[1];
                System.debug('Final Type from Main: ' + listOfTypeandVariant[0]);
                System.debug('Final Variant from Main: ' + listOfTypeandVariant[1]);
                System.debug('New Variant::' + strAcType +'-'+ variant);
                String nameWithTypeAndMSN = strCurMSN+' (' + strAcType + ')';
                string stringAcId = strAcType + '-' + strCurMSN;
                Aircraft_Stage__c thisAc = new Aircraft_Stage__c(Name=nameWithTypeAndMSN,
                                                                 Aircraft_Id__c=stringAcId, 
                                                                 Aircraft_Type__c = strAcType, 
                                                                 Aircraft_Variant__c=variant,
                                                                 MSN_Number__c=strCurMSN,
                                                                 Engine_Type__c=curInfo.Engine__c,
                                                                 Date_of_Manufacture__c=curInfo.AircraftVintage__c,
                                                                 Asset_Owner__c=curInfo.Portfolio__c,
                                                                 Date_Acquired__c=curInfo.DateofPurchase__c,
                                                                 Maximum_Operational_Take_Lbs__c=curInfo.MTOWlbs__c,
                                                                 Registration_Number__c=curInfo.Registration__c,                                                       
                                                                 Model_Code__c=curInfo.Model_Code__c, 
                                                                 Production_Number__c= curInfo.Production_Number__c,
                                                                 Incident__c=curInfo.Incident__c,
                                                                 Utilization_Current_As_Of__c = curInfo.Utilization_Current_As_Of__c,
                                                                 In_Service_Date__c = curInfo.In_Service_Date__c,
                                                                 TSN__c=curInfo.TSN__c,
                                                                 CSN__c=curInfo.CSN__c,
                                                                 Delivery_Date_new__c=curInfo.Delivery_Date_new__c,
                                                                 NBV__c=curInfo.NBV__c,
                                                                 Contractual_Status__c = curInfo.Contractual_Status__c,
                                                                 Asset_Class__c = curInfo.Asset_Class__c,
                                                                 Asset_Id_External__c = curInfo.Asset_Id_External__c,
                                                                 Incoming_Data__c = curInfo.Id                                               
                                                                );
                listAircraftToUpdate.add(thisAc);
                 system.debug('curInfo.Available_For_Portal__c---'+curInfo.Available_For_Portal__c);
                if(curInfo.Available_For_Portal__c && 'ON'.equals(syncPortalAssets) ){
                    system.debug('Asset_Id_External__c----'+curInfo.Asset_Id_External__c);
                    system.debug('internal_id__c----'+stringAcId);
                    Portal_Asset_Staging__c portalAssetStage = new Portal_Asset_Staging__c(Name=nameWithTypeAndMSN,
                                                                                           internal_id__c = stringAcId,
                                                                                           external_id__c = curInfo.Asset_Id_External__c,
                                                                                           Asset_Class__c = curInfo.Asset_Class__c, 
                                                                                           Asset_Type__c = strAcType,
                                                                                           Asset_Variant__c =  curInfo.Variant__c,
                                                                                           Available_From__c = curInfo.LeaseMaturity__c == null?null:curInfo.LeaseMaturity__c.addDays(1),
                                                                                           Serial_Number__c = strCurMSN,
                                                                                           Current_Location__c = curInfo.Current_Location__c,
                                                                                           Registration__c = curInfo.Registration__c,
                                                                                           Vintage__c = curInfo.AircraftVintage__c,
                                                                                           tso__c = curInfo.TSO__c,
                                                                                           cso__c = curInfo.CSO__c,
                                                                                           Limiter__c = curInfo.Limiter__c,
                                                                                           Limiter_Component_Name__c = curInfo.Limiter_Component_Name__c,
                                                                                           TSN__c = curInfo.TSN__c,
                                                                                           csn__c = curInfo.CSN__c,
                                                                                           Incoming_Data__c = curInfo.Id  
                                                                                            );
                    listPortalAssetToUpdate.add(portalAssetStage);                                                                        
                }
                
                //Process Operators
                Operator_Stage__c thisOp, thisLessee;
                string thisOpName, thisLesseeName;
                thisOpName = curInfo.OperatorShortName__c;
                if(thisOpName==null){thisOpName = curInfo.Operator__c;}
                if(thisOpName!=null){
                    thisOpName=thisOpName.left(80);
                    if(!setOperators.contains(thisOpName)){
                        thisOp = new Operator_Stage__c(Name=thisOpName, Lookup_Technical_Representative_Contact__c=curInfo.TechnicalRep__c,
                                                       Lessee_Legal_Name__c = curInfo.Operator__c,
                                                       Lookup_Marketing_Representative_Contact__c=curInfo.MarketingRep__c, 
                                                       Lookup_Contracts_Representative_Contact__c=curInfo.ContractRep__c, Incoming_Data__c = curInfo.Id);
                        listOperatorsToUpdate.add(thisOp);
                        setOperators.add(thisOpName);
                    }
                }
                thisLesseeName=curInfo.CurrentLesseeShortName__c;
                if(thisLesseeName !=null ){
                    thisLesseeName=thisLesseeName.left(80);
                    if(!thisLesseeName.equals(thisOpName)){
                        //TODO - Reps could be different for Lessees. Info unavailable, setting to Op's Reps.
                        if(!bDisableLesseesAsOps && !setOperators.contains(thisLesseeName)){
                            thisLessee = new Operator_Stage__c(Name=thisLesseeName, Lookup_Technical_Representative_Contact__c=curInfo.TechnicalRep__c,
                                                               Lessee_Legal_Name__c = curInfo.CurrentLessee__c,
                                                               Lookup_Marketing_Representative_Contact__c=curInfo.MarketingRep__c,
                                                               Lookup_Contracts_Representative_Contact__c=curInfo.ContractRep__c, Incoming_Data__c = curInfo.Id);
                            listOperatorsToUpdate.add(thisLessee);
                            setOperators.add(thisLesseeName);
                        }
                    }
                }

                //Process Leases
                System.debug('Custom setting value :' + leaseNameType);
                string strLeaseName = strCurMSN + ' | ' + (thisLesseeName==null?'-':thisLesseeName.left(50));
                if(!String.isBlank(leaseNameType)){
                    if(leaseNameType.equalsIgnoreCase('Type')){
                        strLeaseName += ' | ' + strAcType;
                    }else if(leaseNameType.equalsIgnoreCase('Variant')){
                        strLeaseName += ' | '  +
                            ((thisAc.Aircraft_Variant__c==null||''.equals(thisAc.Aircraft_Variant__c))?'': thisAc.Aircraft_Variant__c);
                    }else{
                        strLeaseName += ' | ' + strAcType +
                            ((thisAc.Aircraft_Variant__c==null||''.equals(thisAc.Aircraft_Variant__c))?'': ('-' + thisAc.Aircraft_Variant__c));
                    }
                }else{
                    strLeaseName += ' | ' + strAcType +
                        ((thisAc.Aircraft_Variant__c==null||''.equals(thisAc.Aircraft_Variant__c))?'': ('-' + thisAc.Aircraft_Variant__c));
                }                
                if(strLeaseName!=null){
                    strLeaseName=strLeaseName.left(80);
                    
                    system.debug('AOG Date - Incoming data' + curInfo.AOG_Date__c);
                    
                    Lease_Stage__c thisLease = new Lease_Stage__c(Name=strLeaseName,
                                                                  Rent__c=curInfo.LastMonthBilledRent__c,
                                                                  Lease_End_Date_New__c=curInfo.LeaseMaturity__c,
                                                                  Security_Deposit__c=curInfo.SecurityDepositBalanceCash__c,
                                                                  Total_Non_Cash_Transactions_Security__c=curInfo.LOCMaint__c,
                                                                  Lease_Start_Date_New__c=curInfo.LeaseStart__c,
                                                                  Rent_Type__c=curInfo.RateType__c,
                                                                  Status__c=curInfo.Status__c,
                                                                  Rent_Period__c=('1'.equals(curInfo.FrequencyInMonths__c)?'Monthly':curInfo.FrequencyInMonths__c),
                                                                  Lease_Type_Legal__c=curInfo.ContractSummCategory__c,
                                                                  Lease_Novation_Date__c=curInfo.NovationDate__c,
                                                                  Lookup_Aircraft__c = stringAcId,
                                                                  Lookup_Operator__c = thisOpName,
                                                                  Lookup_Lessee__c = thisLesseeName,
                                                                  Reserve_Type__c = curInfo.ReserveType__c,
                                                                  Guaranty_Type__c = curInfo.GuarantyType__c,
                                                                  Bank_Guaranty__c = curInfo.LetterofCreditAmount__c,
                                                                  AOG_Date__c=curInfo.AOG_Date__c,
                                                                  Lease_Id_External__c = curInfo.Lease_Id_External__c,
                                                                  Incoming_Data__c = curInfo.Id
                                                                 );
                    System.debug('From LEASE staging setup Pkg : ' + strLeaseName);
                    listLeasesToUpdate.add(thisLease);
                }
                //No exceptions so far. Insert the log record in to the map.        
                mapIncomingDataLogs.put(curInfo.Id, incoming_log);
                
            }catch(exception e){
                incoming_log.Error_Message__c = 'ERROR: '+ e.getMessage() + '\n' + e.getStackTraceString();
                mapIncomingDataLogs.put(curInfo.Id, incoming_log);
            }
        }
        
        system.debug('listAircraftToUpdate.size()' + listAircraftToUpdate.size());
        Database.SaveResult[] lsr;
        String errormsg;
        boolean found;
        if(listAircraftToUpdate.size()>0) {
            lsr = Database.insert(listAircraftToUpdate, false);
            for (Database.SaveResult sr:lsr) {
                if (!sr.isSuccess()) {
                    Incoming_Data_Processing_Log__c inc = mapIncomingDataLogs.get(listAircraftToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c);
                    if(inc != null){
                        errormsg = 'ERROR: Aircraft insert failed for ' + sr.getId() +' .\n';
                        for (Database.Error err:sr.getErrors()) {
                            errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                            errormsg += ('Aircraft fields that affected this error: ' + err.getFields() + '\n');
                        }
                        inc.Error_Message__c = errormsg;
                        mapIncomingDataLogs.put(listAircraftToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c, inc);
                    }
                }
            }
        }
        
        if(listOperatorsToUpdate.size()>0) {
            lsr.clear();
            lsr = Database.insert(listOperatorsToUpdate, false);
            for(Database.SaveResult sr:lsr){
                if (!sr.isSuccess()){
                    Incoming_Data_Processing_Log__c inc = mapIncomingDataLogs.get(listOperatorsToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c);
                    if (inc != null) {
                        errormsg = 'ERROR: Operator insert failed for ' + sr.getId() +' .\n';
                        for (Database.Error err:sr.getErrors()) {
                            errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                            errormsg += ('Operator fields that affected this error: ' + err.getFields() + '\n');
                        }
                        inc.Error_Message__c = ('PROCESSING'.equals(inc.Error_Message__c)?'':(inc.Error_Message__c + ' \n')) + errormsg;
                        mapIncomingDataLogs.put(listOperatorsToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c, inc);
                    }
                }
            }     
        }    
        
        system.debug('listLeasesToUpdate.size()' + listLeasesToUpdate.size());
        if(listLeasesToUpdate.size()>0) {
            lsr.clear();
            lsr = Database.insert(listLeasesToUpdate, false);
            for (Database.SaveResult sr:lsr) {
                if (!sr.isSuccess()) {
                    Incoming_Data_Processing_Log__c inc = mapIncomingDataLogs.get(listLeasesToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c);
                    if (inc != null) {
                        errormsg = 'ERROR: Lease insert failed for ' + sr.getId() +' .\n';
                        for (Database.Error err:sr.getErrors()) {
                            errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                            errormsg += ('Lease fields that affected this error: ' + err.getFields() + '\n');
                        }
                        inc.Error_Message__c =  ('PROCESSING'.equals(inc.Error_Message__c)?'':(inc.Error_Message__c + ' \n')) + errormsg;
                        mapIncomingDataLogs.put(listLeasesToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c, inc);
                    }
                }
            }
        }
        system.debug('listPortalAssetToUpdate::'+listPortalAssetToUpdate.size());
        if(listPortalAssetToUpdate.size()>0) {
            lsr.clear();
            lsr = Database.insert(listPortalAssetToUpdate, false);
            for (Database.SaveResult sr:lsr) {
                if (!sr.isSuccess()) {
                    Incoming_Data_Processing_Log__c inc = mapIncomingDataLogs.get(listPortalAssetToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c);
                    if(inc != null){
                        errormsg = 'ERROR: Asset Portal stage insert failed for ' + sr.getId() +' .\n';
                        for (Database.Error err:sr.getErrors()) {
                            errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                            errormsg += ('Asset Portal stage  fields that affected this error: ' + err.getFields() + '\n');
                        }
                        inc.Error_Message__c = errormsg;
                        mapIncomingDataLogs.put(listPortalAssetToUpdate.get(lsr.indexOf(sr)).Incoming_Data__c, inc);
                    }
                }
            }
        }

        for(Id curID : mapIncomingDataLogs.keySet()){
            Incoming_Data_Processing_Log__c curLog = mapIncomingDataLogs.get(curID);
            if('PROCESSING'.equals(curLog.Error_Message__c)){
                curLog.Error_Message__c = 'SUCCESS';
                mapIncomingDataLogs.put(curID, curLog);
            }
        }
        
        if(mapIncomingDataLogs.size()>0)insert mapIncomingDataLogs.values();
    }
    public List<String> setTypeandVariant(String variant,List<String> listOfTypeandVariant){
        if(variant != null){
                string strAcType;
                list<string> strTypeVariant = new List<String>();
                if(String.isNotBlank(variant)){
                    strTypeVariant = variant.split('-',0);
                    strTypeVariant.add(null);
                    strTypeVariant.add(null);
                    strAcType = strTypeVariant[0];
                    if(String.isNotBlank(strAcType)){
                        if(strAcType.startsWith('B7') && bStripBFromBoeing){
                                //Strip off B for Boeing Aircraft. Use B7 to identify so that Bombardier is unaffected.
                        		strAcType=strAcType.right(strAcType.length()-1);
                        }                    
                        //Convert E to Embraer E
                        if('E'.equals(strAcType)){
                            strAcType='Embraer E';
                        }
                    }
                }else{
                    strTypeVariant.add(null);
                    strTypeVariant.add(null);
                }
                listOfTypeandVariant.add(strAcType);
                listOfTypeandVariant.add(strTypeVariant[1]==null?'':strTypeVariant[1]);
        }  
        System.debug('Inside setTypeandVariant Method : Type: ' + listOfTypeandVariant[0]);
        System.debug('Inside setTypeandVariant Method : Variant: ' + listOfTypeandVariant[1]);
        
        return listOfTypeandVariant ;
    }
}