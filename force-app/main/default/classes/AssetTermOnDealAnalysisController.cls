public class AssetTermOnDealAnalysisController {
    
    @AuraEnabled
    public static List<AssetTermWrapper> getDealAnalysisData(Id dealAnalysisId){
        
        List<AssetTermWrapper> lstManagedAssetType = new List<AssetTermWrapper>();
        for(Pricing_Output_New__c pricingOutput : [SELECT 
                                                   	Id, 
                                                   	Asset_Term__c, 
                                                   	Asset_Term__r.Name 
                                                   FROM 
                                                   	Pricing_Output_New__c 
                                                   WHERE 
                                                   Pricing_Run__c =: dealAnalysisId
                                                  AND
                                                   Asset_Term__c != NULL]){
        	AssetTermWrapper aw = new AssetTermWrapper(pricingOutput.Asset_Term__c,pricingOutput.Asset_Term__r.Name);
            lstManagedAssetType.add(aw);
        }
        return lstManagedAssetType;
    }

   
    public class AssetTermWrapper{
        @AuraEnabled public Id assetTermId{get; set;}
        @AuraEnabled public String assetTermName{get; set;}
        public AssetTermWrapper(Id assetTermId,String assetTermName ){
            this.assetTermId = assetTermId;
            this.assetTermName = assetTermName;
        }
    }
    
}