@isTest
public class TestAirlineUtils {

    @isTest
    static void loadTestCases(){
        
        Recommendation_Factor__c rec = new Recommendation_Factor__c(
            Type__c = 'Airline',
            Factor__c = 'Aircraft Type',
            Weightage_Number__c = 75,
            range__c = 0,
            Inactive__c = false
        );
		insert rec;

        Recommendation_Factor__c rec1 = new Recommendation_Factor__c(
            Type__c = 'Airline',
            Factor__c = 'Aircraft Variant',
            Weightage_Number__c = 50,
            range__c = 0,
            Inactive__c = false
        );
		insert rec1;

        Recommendation_Factor__c rec2 = new Recommendation_Factor__c(
            Type__c = 'Airline',
            Factor__c = 'Engine',
            Weightage_Number__c = 50,
            range__c = 0,
            Inactive__c = false
        );
		insert rec2;

        Recommendation_Factor__c rec3 = new Recommendation_Factor__c(
            Type__c = 'Airline',
            Factor__c = 'Vintage',
            Weightage_Number__c = 30,
            range__c = 3,
            Inactive__c = false
        );
		insert rec3;

        Recommendation_Factor__c rec4 = new Recommendation_Factor__c(
            Type__c = 'Airline',
            Factor__c = 'MTOW',
            Weightage_Number__c = 20,
            range__c = 1000,
            Inactive__c = false
        );
		insert rec4;
        
        Recommendation_Factor__c rec5 = new Recommendation_Factor__c(
            Type__c = 'Airline',
            Factor__c = 'Existing Customer',
            Weightage_Number__c = 50,
            range__c = 0,
            Inactive__c = false
        );
		insert rec5;
        
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Test',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400'
        );
        insert aircraft;
        
        Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
        opr.Country__c = 'United States';
        opr.Region__c = 'Asia';
        insert opr;
        System.assert(opr.Id != null);
    
        Global_Fleet__c gf = new Global_Fleet__c();
        gf.Name = 'testWorldFleet';
        gf.Aircraft_External_ID__c = 'type-msn';
        gf.MTOW__c = 134453;
        gf.BuildYear__c = '1996';
        gf.SeatTotal__c = 12;
        gf.AircraftType__c = 'A320';
        gf.AircraftVariant__c = '400';
        gf.EngineType__c = 'V2524';
        gf.AircraftSeries__c = 'AStest';
        gf.EngineVariant__c = 'EVtest';
        gf.OriginalOperator__c ='testOpr';
        gf.Aircraft_Operator__c = opr.Id;
        
        insert gf;
        System.assert(gf.Id != null);
        
        Global_Fleet__c gf1 = new Global_Fleet__c();
        gf1.Name = 'testWorldFleet1';
        gf1.Aircraft_External_ID__c = 'type1-msn1';
        gf1.MTOW__c = 134498;
        gf1.BuildYear__c = '1998';
        gf1.SeatTotal__c = 120;
        gf1.AircraftType__c = 'A320';
        gf1.AircraftVariant__c = '400';
        gf1.SerialNumber__c ='101';
        gf1.EngineType__c = 'V2524';
        gf1.AircraftSeries__c = 'AStest1';
        gf1.OriginalOperator__c ='testOpr';
        gf1.EngineVariant__c = 'EVtest1';
        gf1.Aircraft_Operator__c = opr.Id;
        insert gf1;
        System.assert(gf1.Id != null);
        
        Lease__c lease = new Lease__c(
        Reserve_Type__c= 'LOC',
        Base_Rent__c = 21,
        Lease_Start_Date_New__c = System.today()-2,
        Lease_End_Date_New__c = System.today(),
        Operator__c = opr.id);
        insert lease;
        
		List<AirlineRecommendation.Properties> dataList = AirlineUtils.getAirline('A320', '400', 'V2524', 0,0,0,0, null,null, 'Credit Score',0,0);
        List<AirlineRecommendation.Properties> dataList1 = AirlineUtils.getAirline('A320', '400', 'V2524', 0,0,0,0, null,null, 'Active Deals',6,400);
        AirlineUtils.populateXYAxis(datalist);      
    }
}