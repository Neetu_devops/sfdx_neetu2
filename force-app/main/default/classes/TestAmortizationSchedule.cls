/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAmortizationSchedule {

    static testMethod void CRUDTest() {
        // TO DO: implement unit test
        Financing__c insertRec = new Financing__c(Name = 'Dummy'
        		,Initial_Loan_Amount__c = 10000
        		,Interest_Rate__c = 10
        		,Term_of_the_Loan__c = 10
        		,Generate_Amortization__c= true);
        insert 	insertRec;	
		
	    LeaseWareUtils.clearFromTrigger();        
        
        Amortization_Schedule__c[] ASList = [select Id from Amortization_Schedule__c where Financing__c = :insertRec.Id];
        system.assertEquals(ASList.size()==10,true);
        
        ASList[0].Payment__c = 2222;
        try{
        	Update ASList[0];
        	system.assertEquals(1,2);
        }catch(Exception ex){
        	system.debug('ex'+ex);
        }
        
		
	    LeaseWareUtils.clearFromTrigger();
	            
        try{
        	delete ASList[1];
        	system.assertEquals(1,2);
        }catch(Exception ex){
        	system.debug('ex'+ex);
        }
      
        
    }
}