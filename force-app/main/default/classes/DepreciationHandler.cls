public class DepreciationHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'DepreciationHandlerBefore';
    private final string triggerAfter = 'DepreciationHandlerAfter';
    public DepreciationHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('DepreciationHandler.beforeInsert(+)');

	    system.debug('DepreciationHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('DepreciationHandler.beforeUpdate(+)');
    	Depreciation_schedule__c[] listNew = (list<Depreciation_schedule__c>)trigger.new ;

		
  	
    	for(Depreciation_schedule__c curRec:listNew){
    		Depreciation_schedule__c oldDS = ((Depreciation_schedule__c)trigger.oldMap.get(curRec.Id));
    		if(curRec.Status__c != oldDS.Status__c  && 'Active'.equals(curRec.Status__c) && (curRec.Depreciation_Method__c==null || curRec.Depreciation_Method__c=='Other' ||curRec.Depreciation_Method__c=='MACRS') ){// By default MRRCS
    			// check and validate 
    			Depreciation_Rate__c[] cheildList = [select Id,Depreciation_schedule__c,Period_Sequence__c,Depreciation_Rate__c
    					from Depreciation_Rate__c where Depreciation_schedule__c = :curRec.Id
    						ORDER BY Period_Sequence__c NULLS FIRST ];
				boolean verifyFlag;
				Decimal i=1, sumPercent=0;
				for(Depreciation_Rate__c curChildRec:cheildList){
					if(curChildRec.Period_Sequence__c== null || curChildRec.Period_Sequence__c != i) curRec.status__c.addError('Depreciation Rates are not in sequence.' + i + ' is missing.');
					sumPercent = sumPercent + curChildRec.Depreciation_Rate__c ;
					i++;
				}    
				if(sumPercent > 100 || sumPercent < 100 ){
					curRec.status__c.addError('Sum of Depreciation Rates should be 100%. Sum is ' + sumPercent);
				}						
    		}
    		if(curRec.Status__c != oldDS.Status__c  && 'Active'.equals(oldDS.Status__c)){
    			Aircraft__c[] AC = [select Depreciation_Schedule__c  from Aircraft__c 
    				where Depreciation_Schedule__c = :curRec.Id];
    			if(AC.size() > 0)	curRec.adderror('This Depreciation Schedule is already associated with an Aircraft. You are not allowed to modify the status.');
    		}
    	}
    	
    	system.debug('DepreciationHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('DepreciationHandler.beforeDelete(+)');
    	
    	
    	Depreciation_schedule__c[] listNew = (list<Depreciation_schedule__c>)trigger.old ;
    	AggregateResult[] listResult = [ select Depreciation_Schedule__c Id,count(name) tot from Aircraft__c 
    			where Depreciation_Schedule__c in :listNew
    			group by Depreciation_Schedule__c ];
    	
    	for(AggregateResult resultCur :listResult ){
    		if((Integer)resultCur.get('tot')> 0) 
    			((Depreciation_schedule__c)trigger.newMap.get((Id)resultCur.get('Id'))).addError('This Depreciation Schedule is already associated with an Aircraft. You are not allowed to delete this record.');
    	}
    	
    	system.debug('DepreciationHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationHandler.afterInsert(+)');
    	
    	system.debug('DepreciationHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationHandler.afterUpdate(+)');
    	
    	
    	system.debug('DepreciationHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationHandler.afterDelete(+)');
    	
    	
    	system.debug('DepreciationHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('DepreciationHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


}