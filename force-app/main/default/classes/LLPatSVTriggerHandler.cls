public class LLPatSVTriggerHandler implements ITrigger {
	
    private final string triggerBefore = 'LLPatSVTriggerHandlerBefore';
    private final string triggerAfter = 'LLPatSVTriggerHandlerAfter';
    	
	public LLPatSVTriggerHandler(){
    
    }
    
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
    
    
    public void beforeInsert()
    {
  		system.debug('Before Insert(+++++)');
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);  		
  		
  		setMRBals();
  		
  		system.debug('Before Insert(------)');
    }
     
    public void beforeUpdate()
    {
    	system.debug('Before Update(+)');
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);  
         
        system.debug('Before changeStatus(+)'); 	
		changeStatus();  //change status flat    
		system.debug('After changeStatus(+)');	
		
		setMRBals();
		
  		LLP_SV__c[] listNew = (list<LLP_SV__c>)trigger.new ;
  		LLP_SV__c oldRec;
  		set<Id> setEngTempIds = new set<Id>();
  		set<string> setPartNumberIds = new set<string>();
    	for(LLP_SV__c currRec: listNew){
    		oldRec = (LLP_SV__c)trigger.oldmap.get(currRec.id);
    		if('Open'.equals(currRec.status__c) && oldRec.part_number__c!=currRec.part_number__c){
				setEngTempIds.add(currRec.Y_Hidden_CA_Engine_DB__c);
				setPartNumberIds.add(currRec.part_number__c);
    		}
    	}  		
  		
  		
		system.debug('LLP @ SV__c Method started');
		system.debug('Event Trigger new data====='+listNew);
		
		
		list<LLP_Template__c> LLPTempList=[select id, Name, Part_Number__c,Engine_Template__c, Life_Limit_Cycles__c
											from LLP_Template__c
											where   Engine_Template__c in :setEngTempIds
											and  Part_Number__c in :setPartNumberIds];
		system.debug('LLP Template list='+ LLPTempList);
		map<string,LLP_Template__c> mapLLPtemplate = new map<string,LLP_Template__c>();		
		for(LLP_Template__c currRec:LLPTempList ){
			system.debug('Name coming from LLP DB====='+currRec.Name);
			mapLLPtemplate.put(currRec.Engine_Template__c + '-' + currRec.Part_Number__c + '-' + currRec.Name,currRec);
			system.debug('Key for future matching ====='+ currRec.Engine_Template__c + '-' + currRec.Part_Number__c + '-' + currRec.Name);
		}
		


		LLP_Template__c LLPTemplateRef;
		for (LLP_SV__c currRec: listNew){
    		oldRec = (LLP_SV__c)trigger.oldmap.get(currRec.id);
    		if('Open'.equals(currRec.status__c) && oldRec.part_number__c!=currRec.part_number__c){
    			system.debug('Key that is looking for matching ====='+currRec.Y_Hidden_CA_Engine_DB__c + '-' + currRec.Part_Number__c + '-' + currRec.Y_Hidden_LLP_Name__c);
    			LLPTemplateRef = mapLLPtemplate.get(currRec.Y_Hidden_CA_Engine_DB__c + '-' + currRec.Part_Number__c + '-' + currRec.Y_Hidden_LLP_Name__c);
				if(LLPTemplateRef==null){
					currRec.addError('LLP DB not found for changed Part Number : '+currRec.Part_Number__c);
				}else{
					currRec.Y_Hidden_LLP_DB__c = LLPTemplateRef.Id;
				} 	 
    		}
		}


  		system.debug('Before Update(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
    	system.debug('Before Delete(+)');
  		
  		system.debug('Before Delete(-)');

    }
     
    public void afterInsert()
    {
    	system.debug('After Insert(+)');
  		
  		system.debug('After Insert(-)');
    	     	
    }
     
    public void afterUpdate()
    {
    	system.debug('After Update(+)');
  		
  		system.debug('After Update(-)');
    	   	
    }
     
    public void afterDelete()
    {
    	system.debug('After Delete(+)');
  		
  		system.debug('After Ddelete(-)');
    	     	
    }

    public void afterUnDelete()
    {
    	system.debug('After Undelete(+)');
  		
  		system.debug('After Undelete(-)');
    	   	
    }
    
     public void andFinally()
    {
        // insert any audit records

    }
    
    
    //Change the status of Flag to OPEN if there is any changes on custom fields.
    public static void changeStatus(){
    	LLP_SV__c[] listNew = (list<LLP_SV__c>)trigger.new ;
    	LLP_SV__c oldRec;
    	list<LLP_SV__c> updLLSV = new list<LLP_SV__c>();
    	for(LLP_SV__c currRec: listNew){
    		oldRec = (LLP_SV__c)trigger.oldMap.get(currRec.Id);
    		if(currRec.CSN__c!=oldRec.CSN__c || currRec.TSN__c!=oldRec.TSN__c	|| currRec.Part_Number__c!=oldRec.Part_Number__c  || currRec.Serial_Number__c!=oldRec.Serial_Number__c ){
    			currRec.Status__c='Open';
    			updLLSV.add(currRec);
    		}
    	}
    	if(updLLSV.size()>1)update updLLSV;
    	
    }
/*    
    //update Life Limit Cycle if they change part number
    public static void updateLifeLimit(){
    	LLP_SV__c[] listNew = (list<LLP_SV__c>)trigger.new ;
    	system.debug('new list of llp at sv ====='+listNew);
    	LLP_SV__c oldRec;
    	Decimal newLifeLimet;
    	list<LLP_SV__c> updLLSV1 = new list<LLP_SV__c>();
    	
    	string newPartNumber='';
    				
    	for(LLP_SV__c currRec: listNew){
    		oldRec = (LLP_SV__c)trigger.oldMap.get(currRec.Id);

    		if(currRec.Part_Number__c!=oldRec.Part_Number__c){
    			newPartNumber=currRec.Part_Number__c;
    			//update life limimt
    			//currRec.Life_Limit_Cycles__c=newLifeLimet;
    			//updLLSV.add(currRec);
    		}
    	}
    	
    	//new code
    	list<LLP_Template__c> llptemplate=[select id,Part_Number__c, name, Life_Limit_Cycles__c from LLP_Template__c where Part_Number__c=:newPartNumber];
    	system.debug('Tilak llptemplate====='+llptemplate);	
    	
    	for(LLP_Template__c curRec: llptemplate){
    		newLifeLimet=curRec.Life_Limit_Cycles__c;
    		system.debug('New life limit updation ====='+curRec.Life_Limit_Cycles__c);
    	}
    	
    	
    	
    	for(LLP_SV__c currRec1: listNew){
    		oldRec = (LLP_SV__c)trigger.oldMap.get(currRec1.Id);

    		if(currRec1.Part_Number__c!=oldRec.Part_Number__c){
    			//update life limeLimt
    			system.debug('Vlue of New life Limit====='+newLifeLimet);
    			currRec1.Life_Limit_Cycles__c=newLifeLimet;
    			updLLSV1.add(currRec1);
    		}
    	}
    	
    	
    	if(updLLSV1.size()>1)update updLLSV1;
    	
    	
    }        
    //If they made any changes on Part# and Serial#, enforce them to enter TSN and CSN value.
    public static void enforceTSNCSN(){
    
    	LLP_SV__c[] listNew = (list<LLP_SV__c>)trigger.new ;
		
		for(LLP_SV__c currRec :listNew ){
			if(currRec.Part_Number__c != currRec.Previous_Part_Number__c || currRec.Serial_Number__c != currRec.Previous_Serial_Number__c){
					system.debug('Part # or  Serial # is changed');
					
					if(currRec.CSN__c == null){
						system.debug('CSN is null');
						currRec.addError('Please ensure that CSN is entered.');
					}
					
					if(currRec.TSN__c == null){
						system.debug('TSN is null');
						currRec.addError('Please ensure that TSN is entered.');
					}
			}
		}
    }
 */
 	private void setMRBals(){
   		for(LLP_SV__c curLLPAtSV : (list<LLP_SV__c>)trigger.new){
	  		if(curLLPAtSV.Life_Limited_Part_LLP__c != null){
	  			if(curLLPAtSV.Y_Hidden_Induction_Date__c!=null)
	  				curLLPAtSV.MR_Available_On_Induction_Date__c = leasewareUtils.getLLPMRBalanceAsOf(curLLPAtSV.Assembly_ID__c, curLLPAtSV.Life_Limited_Part_LLP__c, curLLPAtSV.Y_Hidden_Induction_Date__c);
	  			if(curLLPAtSV.Y_Hidden_Release_Date__c!=null)
	  				curLLPAtSV.MR_Available_On_Release_Date__c = leasewareUtils.getLLPMRBalanceAsOf(curLLPAtSV.Assembly_ID__c, curLLPAtSV.Life_Limited_Part_LLP__c, curLLPAtSV.Y_Hidden_Release_Date__c);
	  		}
  		}
 	}
    
}