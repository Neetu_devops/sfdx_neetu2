public class MatchingAircraftTriggerHandler implements ITrigger{

    // Constructor
    private final string triggerBefore = 'MatchingACTriggerHandlerBefore';
    private final string triggerAfter = 'MatchingACTriggerHandlerAfter';

/*    private list<Aircraft_Type_Market_Intelligence__c> listATMIs = new list<Aircraft_Type_Market_Intelligence__c>();
    private set<Id> setMIToDel = new set<Id>();
    private map<Id, Aircraft_Type_Market_Intelligence__c> mapATMIs = new map<Id, Aircraft_Type_Market_Intelligence__c>();
*/
    public MatchingAircraftTriggerHandler()
    {
    }
 
    public void bulkBefore()
    {
    }
     
    public void bulkAfter()
    {
/*
    	if(trigger.isUpdate){
    		list<Aircraft_Type_Market_Intelligence__c> listExATMIs = [select id, Name, Aircraft_Type__c, Matching_Aircraft__c 
    			from Aircraft_Type_Market_Intelligence__c where Matching_Aircraft__c in 
    			:(list<Matching_Aircraft__c>)trigger.new];
    		for(Aircraft_Type_Market_Intelligence__c curATMI : listExATMIs){
    			mapATMIs.put(curATMI.Matching_Aircraft__c, curATMI);
    		}	
    	}
*/
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('MatchingACTriggerHandler.beforeInsert(+)');

	    system.debug('MatchingACTriggerHandler.beforeInsert(-)');   	
    }
     
    public void beforeUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('MatchingACTriggerHandler.beforeUpdate(+)');
    	
    	system.debug('MatchingACTriggerHandler.beforeUpdate(-)');
    }
     
    public void beforeDelete()
    {  

    	system.debug('MatchingACTriggerHandler.beforeDelete(+)');
    	system.debug('MatchingACTriggerHandler.beforeDelete(-)');         
    }
     
    public void afterInsert()
    {
    	system.debug('MatchingACTriggerHandler.afterInsert(+)');
    	system.debug('MatchingACTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	system.debug('MatchingACTriggerHandler.afterUpdate(+)');
    	system.debug('MatchingACTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	system.debug('MatchingACTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('MatchingACTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	system.debug('MatchingACTriggerHandler.afterUnDelete(+)');
    	
    	system.assert(false);
    	
    	system.debug('MatchingACTriggerHandler.afterUnDelete(-)');     	
    }
     
    public void andFinally()
    {
    }
 
 /*
 	// after insert, after update, before delete
	private void CRUDAircraftTypeMarketIntel(){
		list<Matching_Aircraft__c> cuMIList = (list<Matching_Aircraft__c>)(trigger.isDelete?trigger.old:trigger.new);
		
		try{
	
			Matching_Aircraft__c oldMapMI;
			
			for(Matching_Aircraft__c curMI : cuMIList){
					
				if(trigger.isInsert){

					if(curMI.Aircraft_Type_New__c==null)continue;
					Id ACTypeId;
					try{
						ACTypeId = [select id from Aircraft_Type__c where Aircraft_Type__c = :curMI.Aircraft_Type_New__c 
							AND Aircraft_Variant__c = null AND Engine_Type__c = null AND Engine_Variant__c =null order by 
							CreatedDate desc limit 1].id;
					}catch(exception e){
						Aircraft_Type__c newACType = new Aircraft_Type__c(Name=curMI.Aircraft_Type_New__c, 
									Aircraft_Type__c = curMI.Aircraft_Type_New__c);
						insert newACType;
						ACTypeId = newACType.id;
					}
					listATMIs.add(new Aircraft_Type_Market_Intelligence__c(Name='Dummy', Aircraft_Type__c=ACTypeId, 
								Matching_Aircraft__c=curMI.Id));

				}else if(trigger.isUpdate){
					oldMapMI = (Matching_Aircraft__c)trigger.oldMap.get(curMI.Id);
					
					if(oldMapMI.Aircraft_Type_New__c != null && curMI.Aircraft_Type_New__c == null)
						setMIToDel.add(oldMapMI.Id);
					if(curMI.Aircraft_Type_New__c==null || oldMapMI.Aircraft_Type_New__c == curMI.Aircraft_Type_New__c)continue;

					Id ACTypeId;
					try{
						ACTypeId = [select id from Aircraft_Type__c where Aircraft_Type__c = :curMI.Aircraft_Type_New__c 
							AND Aircraft_Variant__c = null AND Engine_Type__c = null AND Engine_Variant__c =null order by 
							CreatedDate desc limit 1].id;
					}catch(exception e){
						Aircraft_Type__c newACType = new Aircraft_Type__c(Name=curMI.Aircraft_Type_New__c, 
									Aircraft_Type__c = curMI.Aircraft_Type_New__c);
						insert newACType;
						ACTypeId = newACType.id;
					}

					Aircraft_Type_Market_Intelligence__c exATMI = mapATMIs.get(curMI.Id);
					if(exATMI == null){
						listATMIs.add(new Aircraft_Type_Market_Intelligence__c(Name='Dummy', Aircraft_Type__c=ACTypeId, 
								Matching_Aircraft__c=curMI.Id));
					}else{
						exATMI.Aircraft_Type__c=ACTypeId;
						exATMI.Matching_Aircraft__c=curMI.Id;
						listATMIs.add(exATMI);
					}
					
				}else{ //Delete
					if(curMI.Aircraft_Type_New__c!=null)setMIToDel.add(curMI.Id);
				}
			}
			if(setMIToDel.size()>0)delete [select id from Aircraft_Type_Market_Intelligence__c where Matching_Aircraft__c in :setMIToDel];
			if(listATMIs.size()>0)upsert listATMIs;
	
		}catch(exception e){
			system.debug('CRUD on AircraftTypeMarketIntel failed due to exception '+ e.getMessage() + '\n' + e.getStackTraceString());
		}		
	}
 */
    
}