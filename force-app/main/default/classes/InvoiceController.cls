public class InvoiceController{

    public String recordId {get;set;}
    
    public InvoiceController(ApexPages.StandardController std) {    
        String retUrl = ApexPages.currentPage().getUrl().substringAfter('lkid'); 
        recordId = String.isNOTBlank(retUrl) && retUrl.length() >= 16 ? retUrl.substring(1,16) : retUrl; 
    }
}