public class NonLeaseAgrmntTriggerHandler implements ITrigger {
    private final string triggerBefore = 'NonLeaseAgrmntTriggerHandlerBefore';
    private final string triggerAfter = 'NonLeaseAgrmntTriggerHandlerAfter';
    public NonLeaseAgrmntTriggerHandler(){}
     public void bulkBefore(){}
     
   
    public void bulkAfter(){}
     
    public void beforeInsert(){
        system.debug('NonLeaseAgrmntTriggerHandler.beforeInsert(++++)');
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        findRentAmount();
        system.debug('NonLeaseAgrmntTriggerHandler.beforeInsert(---)');
        
    }
    public void beforeUpdate(){ }
    public void beforeDelete(){}
    
    public void afterInsert(){
        system.debug('NonLeaseAgrmntTriggerHandler.afterInsert(++++)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        createFeesSchedule();
        system.debug('NonLeaseAgrmntTriggerHandler.afterInsert(---)');
    }
    public void afterUpdate(){}
 
    
    public void afterDelete(){}
    
    
    public void afterUnDelete(){}    
 
    public void andFinally(){}
    /**
     * Desc : Finds the Rent Amount from Actual Closing date to Economic closing date
     * 
     * Called From : beforeInsert
     * */
    private void findRentAmount(){
        system.debug('findRentAmount(++)');
        list<Non_Lease_Agreements__c> SalePurList = new list<Non_Lease_Agreements__c>();
        set<id> leaseIds = new set<id>();
        Date startDate = null;
        Date endDate = null;
        String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Sales_Purchase_Agreement').getRecordTypeId();
        for(Non_Lease_Agreements__c nl : (list<Non_Lease_Agreements__c>)trigger.New ){
            if(nl.RecordTypeId.equals(recordType)){
                if(nl.Economic_Closing_Date__c == null ||nl.Actual_Closing_Date__c ==null )continue;
                SalePurList.add(nl);
                leaseIds.add(nl.Lease__c);
                 if(startDate == null || nl.Economic_Closing_Date__c <  startDate ) startDate = nl.Economic_Closing_Date__c.addMonths(-1);
                 if(EndDate == null|| nl.Actual_Closing_Date__c > EndDate )EndDate = nl.Actual_Closing_Date__c.addMonths(1);
      
            }
        } 	
        if(SalePurList.size()>0){
            list<rent__c> rents = [select start_date__c ,RentPayments__c, For_Month_Ending__c,Rent_Due_Amount__c,Days_in_Rental_Period__c,Stepped_Rent__r.Pro_rata_Number_Of_Days__c from rent__c
                       where RentPayments__c IN :leaseIds
                       and start_date__c >= :startDate and For_Month_Ending__c<= :endDate];
            
            Decimal tempRent = 0.0;
            Decimal totalAmount = 0;
            Date tempStartDate = null;
            Date tempEndDate = null;
            Integer noOfdays = 0;
            for(Non_Lease_Agreements__c nla :SalePurList){
                totalAmount = 0;
                list<rent__c> nlaRents = getRentSchedules(nla,rents);
                for(Rent__c rent : nlaRents){
                    tempStartDate = rent.Start_Date__c;
                    tempEndDate = rent.For_Month_Ending__c;
                    tempRent = rent.Rent_Due_Amount__c;
                    if(rent.Start_Date__c < nla.Economic_Closing_Date__c){
                        system.debug('First Sch');
                        tempStartDate = nla.Economic_Closing_Date__c;
                        noOfdays = tempStartDate.daysBetween(tempEndDate)+1;
                        tempRent = findPartialRent(tempStartDate,tempEndDate,rent);
                        
                    }
                    if(rent.For_Month_Ending__c > nla.Actual_Closing_Date__c){
                        system.debug('Last Sch');
                        tempEndDate = nla.Actual_Closing_Date__c;
                        noOfdays = tempStartDate.daysBetween(tempEndDate)+1;
                        tempRent = findPartialRent(tempStartDate,tempEndDate,rent);

                    }
                    system.debug('tempStartDate:::'+tempStartDate);
                    system.debug('tempEndDate:::'+tempEndDate);
 					system.debug('tempRent:::'+tempRent);
                    totalAmount = totalAmount + tempRent;
                    
                }
                system.debug('totalAmount:::'+totalAmount);
                nla.Current_Rental_amount__c = totalAmount;
                
            }
        }
    }
     /**
     * Desc : Finds the partial Rent
     * 
     * Called From : findRentAmount
     * 
     * @param : Non_Lease_Agreements list
     * */
    private Decimal findPartialRent(Date tempStartDate,Date tempEndDate,Rent__c rent){
        Decimal rentAmt =0.0;
        String proRata = rent.Stepped_Rent__r.Pro_rata_Number_Of_Days__c;
        Integer noOfdays = tempStartDate.daysBetween(tempEndDate)+1;
        Integer rentPeriod = rent.Start_Date__c.daysBetween(rent.For_Month_Ending__c)+1;
        try{
            if(rent.Days_in_Rental_Period__c >=30 || (rent.Days_in_Rental_Period__c >=28 && rent.For_Month_Ending__c.month()==2)){
                if(proRata == NULL ||'Actual number of days in month/year'.equals(proRata)||'None'.equals(proRata)){
                    rentAmt = (rent.Rent_Due_Amount__c/rentPeriod)*noOfDays;
                }
                else if(prorata.equals('Fixed - 30/360')){
                    rentAmt = (rent.Rent_Due_Amount__c/30)*noOfDays;
                }
            }
            else{
                Decimal PerDayRent = rent.Rent_Due_Amount__c/rent.Days_in_Rental_Period__c;
                rentAmt = PerDayRent * noOfDays;
            }
        }
        catch(Exception e){
            rentAmt = 0.0;
            LeasewareUtils.createExceptionLog(e,'Error in finding rent amount', 'Non_lease_agreement__c',''+rent.Id, 'Sales and Purchase', true);
            
        }
        return rentAmt;
    }
    /**
     * Desc : Retrieves the Rent Schedule based on the Non Lease Agreement.
     * 
     * Called From : findRentAmount
     * 
     * @param : Non_Lease_Agreements list
     * */
    private list<rent__c> getRentSchedules(Non_Lease_Agreements__c nla,list<rent__c> rents){
        list<rent__c> rentList = new list<rent__c>();
        
        for(rent__c rent :rents ){
            if(nla.Lease__c!=rent.RentPayments__c) continue;
             if( (rent.start_date__c >= nla.Economic_Closing_Date__c  || nla.Economic_Closing_Date__c <= rent.For_Month_Ending__c) &&
							(nla.actual_closing_date__c >=rent.For_Month_Ending__c|| nla.actual_closing_date__c >= rent.start_date__c)){
							rentlist.add(rent);
			
			}
        }
        system.debug('Rent List:::'+RentList.size()+':::'+RentList);
        return rentList;
        
    }
    
    /**
     * Desc :  Creates the Fees Schedule for the Fees Record type;
     * 
     * Called From : afterInsert
     * */
    private void createFeesSchedule(){
        
        system.debug('createFeesSchedule(++)');
        list<Non_Lease_Agreements__c> feesList = new list<Non_Lease_Agreements__c>();
        list<Fees_Schedule__c> schList = new list<Fees_Schedule__c>();
        String Periodicity='';
        Date startDate =  null;
        Date endDate = null;
        Decimal Amount = 0.0;
        Date tempStartDate =null;
        Date tempEndDate = null ;
        Boolean isFinished = false;
        integer noOfDays = 0;
        Integer DaysInMonth = 0;
        Integer count = 1;
        String Name;
        String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Fees').getRecordTypeId();
        for(Non_Lease_Agreements__c nl : (list<Non_Lease_Agreements__c>)trigger.New ){
            if(nl.RecordTypeId.equals(recordType)){
                feesList.add(nl);
            }
        } 	
        if(feesList.size()>0){
            for(Non_Lease_Agreements__c nla : feesList){
                startDate = nla.Start_Date__c;
                endDate = nla.End_Date__c;
                tempStartDate = startDate;
                tempEndDate = endDate;
                if(nla.Periodicity__c!=null)Periodicity = nla.Periodicity__c;
                Amount = nla.Periodic_Amount__c;
                count = 1;
                while(tempEndDate < = endDate && !isFinished){
                    if(Periodicity.equals('Monthly')){
                        tempEndDate = tempStartDate.addMonths(1).addDays(-1);
                        //DaysInMonth = date.daysInMonth(tempEndDate.year(), tempEndDate.month());
                        DaysInMonth = 30;
                    }
                    else if(Periodicity.equals('Annual')){
                        tempEndDate = tempStartDate.addyears(1).addDays(-1);
                        DaysInMonth = 360;
                        
                    }
                    else if(Periodicity.equals('Quarterly')){
                        tempEndDate = tempStartDate.addMonths(3).addDays(-1);  
                        DaysInMonth = 90;
                    }
                    if(tempEndDate >= endDate){
                        if(tempEndDate > endDate){
                            tempEndDate = endDate;
                            noOfDays = tempStartDate.daysBetween(tempEndDate)+1;
                            amount = (amount/DaysInMonth)*noOfDays;
                        }
                        
                        isFinished = true;
                    }
                    
                    system.debug('START DATE:'+tempStartDate+'END DATE:'+tempEndDate+'Amt:'+amount);
                    //add to list
                    name = 'Schedule '+count;
                    schList.add(new Fees_Schedule__c(Name = name,Amount_Due__c = amount,Y_Hidden_End_Date__c=tempEndDate,
                                                 Y_Hidden_Start_Date__c= tempStartDate,Fees__c =nla.id ));
                    tempStartDate =  tempEndDate.addDays(1);
                    count ++;
                    
                    
                }
            }
            if(!schList.isEmpty())
                insert schList;
        }
        system.debug('createFeesSchedule(--)');
    }
    
    
}