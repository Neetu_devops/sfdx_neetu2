@isTest
public class TestReconciliationController {
    @testSetup
    public static void testSetup(){
        Date dateField = System.today();
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month()- 1);
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month() - 1, numberOfDays);
        Date minStartDate = Date.newInstance(dateField.year()-4, dateField.month(), 1);
        Date maxEndDate = lastDayOfMonth;
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                  
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,        
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                        
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = minStartDate,
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = minStartDate,
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        
        insert testAircraft;
        
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.Id; 
        update leaseRecord;
        
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                          Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                                          Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
        LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
        Constituent_Assembly__c consituentAssemblyRecord = new Constituent_Assembly__c();
        consituentAssemblyRecord.Serial_Number__c = '2';
        consituentAssemblyRecord.CSN__c = 3;
        consituentAssemblyRecord.TSN__c = 5;
        consituentAssemblyRecord.Last_CSN_Utilization__c = 3;
        consituentAssemblyRecord.Last_TSN_Utilization__c = 5;
        consituentAssemblyRecord.Attached_Aircraft__c = testAircraft.Id;
        consituentAssemblyRecord.Name ='Testing';
        consituentAssemblyRecord.Type__c ='Airframe'; 
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert consituentAssemblyRecord ;
        LWGlobalUtils.setTriggersflagOff();
        
        Ratio_Table__c testRatioTable = new Ratio_Table__c(Name = 'Test Ratio Table');
        insert testRatioTable;
        
        Derate_Adjustment__c interpolationRecord = new Derate_Adjustment__c(Name = '2.0');
        interpolationRecord.Ratio_Table__c = testRatioTable.Id;
        interpolationRecord.FH_To_FC_Ratio__c = 2.0;
        interpolationRecord.Derate_Percentage__c = 10;
        interpolationRecord.Dollars_per_Flight_Hour__c = 1000;
        
        insert interpolationRecord;
        
        Assembly_Event_Info__c assemblyEventInfoObj=new Assembly_Event_Info__c(
            Name='Airframe-Airframe-Heavy 1',
            Constituent_Assembly__c = consituentAssemblyRecord.Id
        );
        insert assemblyEventInfoObj;
        
        Assembly_MR_Rate__c assemblyMrRecord = new Assembly_MR_Rate__c();
        assemblyMrRecord.Name ='TestMrRecord';
        assemblyMrRecord.Lease__c =leaseRecord.Id;
        assemblyMrRecord.Base_MRR__c =200;
        assemblyMrrecord.Escalation_Type__c='Advanced';
        assemblyMrRecord.Rate_Basis__c='Monthly';
        assemblyMrRecord.Assembly_Type__c ='Engine 3';
        assemblyMrRecord.Assembly_Lkp__c = consituentAssemblyRecord.Id;
        assemblyMrRecord.Assembly_Event_Info__c = assemblyEventInfoObj.Id;
        assemblyMrRecord.Ratio_Table__c = testRatioTable.Id;
        assemblyMrRecord.Annual_Escalation__c = 1;
        
        insert assemblyMrRecord;  
        
        Utilization_Report__c firstUtilizationRecord = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = minStartDate.toStartOfMonth().addMonths(1).addDays(-1),
            Status__c='Approved By Lessor'
        );
        insert firstUtilizationRecord;
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = lastDayOfMonth,
            Status__c='Approved By Lessor'
        );
        insert utilizationRecord;
        
        Date startDate = Date.newInstance(lastDayOfMonth.year() - 1, lastDayOfMonth.month() + 1, 1);
        Date endDate = lastDayOfMonth;
        
        Id recordTypeId = Schema.SObjectType.Reconciliation_Schedule__c.getRecordTypeInfosByDeveloperName().get('Default').getRecordTypeId();
        
        
        Reconciliation_Schedule__c rsObj = new Reconciliation_Schedule__c();
        rsObj.Name = startDate.year()+' '+ datetime.newInstance(startDate.year(), startDate.month(),startDate.day()).format('MMM')+' to '+ endDate.addDays(-1).year() +' '+datetime.newInstance(endDate.addDays(-1).year(), endDate.addDays(-1).month(),endDate.addDays(-1).day()).format('MMM');
        rsObj.From_Date__c = startDate;
        rsObj.To_Date__c = endDate;
        rsObj.Status__c = 'Open';
        rsObj.RecordTypeId = recordTypeId;
        rsObj.Disable_Invoice_Auto_Generation__c = true;
        rsObj.Lease__c = leaseRecord.Id;
        rsObj.Asset__c = testAircraft.Id;
        rsObj.Reconciliation_Period__c = '12';
        
        insert rsObj;
    }
    
    @isTest public static void testMethod1() {
        
        Date dateField = System.today();
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month()- 1);
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month() - 1, numberOfDays);
        
        Test.startTest();
        
        Lease__c leaseRecord = [Select Id From Lease__c Limit 1];
        
        Aircraft__c testAircraft = [Select Id From Aircraft__c Limit 1];
        
        
        Utilization_Report__c utilizationRecord3 = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Assumed FH and FC',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = lastDayOfMonth,
            Status__c='Approved By Lessor'
        );
        insert utilizationRecord3;
        
        
        Reconciliation_Schedule__c reconciliationScheduleRecord = [Select Id, Status__c From Reconciliation_Schedule__c Limit 1];
        
        ReconciliationScheduleController.ReconciliationScheduleDataWrapper wrapperObj = ReconciliationController.getReconciliationRecordData(reconciliationScheduleRecord.Id);
        wrapperObj.reconciliationType = 'Assumed FH and FC';
        
        ReconciliationController.validateWrapper validWrapperData = ReconciliationController.validateReconciliationRecord(JSON.serialize(wrapperObj), reconciliationScheduleRecord.Id);
        
        wrapperObj.reconciliationType = 'Assumed Ratio';
        
        validWrapperData = ReconciliationController.validateReconciliationRecord(JSON.serialize(wrapperObj), reconciliationScheduleRecord.Id);
        
        List<Utilization_Report__c> uRList = [Select Id From Utilization_Report__c];
        leasewareUtils.TriggerDisabledFlag = true;
        delete uRList;
        leasewareUtils.TriggerDisabledFlag = false;
        
        validWrapperData = ReconciliationController.validateReconciliationRecord(JSON.serialize(wrapperObj), reconciliationScheduleRecord.Id);
        
        Test.stopTest();
    }
    //Ref: Reconciliation Studio
    @isTest public static void testMethod2() {
        Date dateField = System.today();
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month()- 1);
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month() - 1, numberOfDays);
        
        Test.startTest();
        try {
            Lease__c leaseRecord = [Select Id From Lease__c Limit 1];
            
            Aircraft__c testAircraft = [Select Id From Aircraft__c Limit 1];
            
            Date startDate = Date.newInstance(lastDayOfMonth.year() - 1, lastDayOfMonth.month() + 1, 1);
            Date endDate = lastDayOfMonth;
            
            Reconciliation_Schedule__c reconciliationScheduleRecord = [Select Id, Status__c From Reconciliation_Schedule__c Limit 1];
            reconciliationScheduleRecord.Status__c = 'Complete';
          
            update reconciliationScheduleRecord;
            
            Utilization_Report__c utilizationRecord = new Utilization_Report__c(
                        Name='Testing 2',
                        Airframe_Flight_Hours_Month__c = 20.0,
                        Airframe_Cycles_Landing_During_Month__c= 20.0,
                        FH_String__c ='20',
                        Aircraft__c = testAircraft.Id,
                        Y_hidden_Lease__c = leaseRecord.Id,
                        Type__c = 'True Up Gross',
                        Escalation_Factor__c = 3,
                        Escalation_Factor_LLP__c = 2,
                        Month_Ending__c = lastDayOfMonth,
                        Status__c='Approved By Lessor'
                    );
        	insert utilizationRecord;
            
            LeaseWareUtils.clearFromTrigger();
            
            List<Utilization_Report__c> uRList = [Select Id From Utilization_Report__c];
            system.debug('uRList1--- '+uRList.size());
            delete uRList;
            
        }
        catch (DmlException e) {
            System.debug(e.getMessage());
        } 
        
        Test.stopTest();
        
    }
}