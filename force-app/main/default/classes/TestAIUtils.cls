@isTest
public class TestAIUtils {
    
    @isTest
    public static void loadTestCases() {
        Date myDate1 = Date.newInstance(2010, 2, 17);
        Aircraft__c aircraft = new Aircraft__c(Name= '1123 Test', MSN_Number__c = '1230', Aircraft_Type__c = 'A320', Aircraft_Variant__c = '100', 
                                               Date_of_Manufacture__c = myDate1);
        
        LeaseWareUtils.clearFromTrigger();
        insert aircraft;
        
        Operator__c opr = new Operator__c(Name = 'testOpr', Country__c = 'United States', Region__c = 'Asia');
        insert opr;
        
        Lease__c lease1 = new Lease__c(Reserve_Type__c= 'Maintenance Reserves', Base_Rent__c = 21, Lease_Start_Date_New__c = System.today()- 400, 
                                       Lease_End_Date_New__c = System.today(), Rent__c = 454546, Operator__c = opr.id);
        insert lease1;
        
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =lease1.Operator__c, Deal_Type__c='Purchase' ,
                                                            Deal_Status__c = 'Pipeline',Marketing_Rep_c__c = 'N/A',Description__c='111');
        LeaseWareUtils.clearFromTrigger();
        insert m1;
        
        Lease__c lease2 = new Lease__c(Reserve_Type__c= 'Maintenance Reserves', Base_Rent__c = 2100, Lease_Start_Date_New__c = System.today()-990,
                                       Rent__c = 454546, Lease_End_Date_New__c = System.today(),Aircraft__c = aircraft.Id, Operator__c = opr.id);
        LeaseWareUtils.clearFromTrigger();
        insert lease2;
        
        testSourcePortfolio(lease1, m1);
        testSourceClosedDeals(lease1, aircraft);
        testSourceAllDeal(lease1, m1);
        testSourceLease();
        testSourceIntelMarket(opr);
        testSourceWorldFleet(); 
    }

    public static void testSourcePortfolio(Lease__c lease1, Marketing_Activity__c m1) {
        Date myDate = Date.newInstance(2010, 2, 17);
        List<Aircraft_Proposal__c> aidList = new List<Aircraft_Proposal__c> ();
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c= lease1.Aircraft__c, Engine_Type_new__c='V2524',Rent__c = 509090, 
                                                            Asset_Type__c  = 'A320-100', Lease_Term__c = '2 Years', Term_Mos__c = 60,
                                                            Date_of_Manufacture__c = myDate);
        aidList.add(AT1);
        Aircraft_Proposal__c AT2 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c= lease1.Aircraft__c, Engine_Type_new__c='V2524',Rent__c = 509090, 
                                                            Asset_Type__c  = 'A320-100', Lease_Term__c = '2 Years', Term_Mos__c = 72,
                                                            Date_of_Manufacture__c = myDate);
        aidList.add(AT2);
        Aircraft_Proposal__c AT3 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c= lease1.Aircraft__c, Engine_Type_new__c='V2524',Rent__c = 509090, 
                                                            Asset_Type__c  = 'A320-100', Lease_Term__c = '2 Years', Term_Mos__c = 120,
                                                            Date_of_Manufacture__c = myDate);
        aidList.add(AT3);
        Aircraft_Proposal__c AT4 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c= lease1.Aircraft__c, Engine_Type_new__c='V2524',Rent__c = 509090, 
                                                            Asset_Type__c  = 'A320-100', Lease_Term__c = '2 Years', Term_Mos__c = 125,
                                                            Date_of_Manufacture__c = myDate);
        aidList.add(AT4);
        insert aidList;
        
        list<DealAnalysisData.Data> data = AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 1, 0, 'Aircraft','100');
        if(data != null) {
            system.assertEquals(4, data[0].Recs.size());
        }
    }
    
    public static void testSourceClosedDeals (Lease__c lease1, Aircraft__c aircraft ){
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =lease1.Operator__c,
                                                            Deal_Type__c='Purchase' ,Deal_Status__c = 'LOI Signed',
                                                            Marketing_Rep_c__c = 'N/A',Description__c='111');
        LeaseWareUtils.clearFromTrigger();
        insert m1;
        LeaseWareUtils.clearFromTrigger();
        Date myDate = Date.newInstance(2010, 2, 17);
        List<Aircraft_Proposal__c> aidList = new List<Aircraft_Proposal__c> ();
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c = aircraft.Id, Engine_Type_new__c='V2524',
                                                            Asset_Type__c  = 'A320-100', Term_Mos__c = 125, Rent__c = 509090, 
                                                           Date_of_Manufacture__c = myDate);
        insert AT1;
        
        list<DealAnalysisData.Data> data = AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 2, 0, 'Aircraft','100');
        
    }
    
    public static void testSourceAllDeal(Lease__c lease1, Marketing_Activity__c m1 ){
        Date myDate = Date.newInstance(2010, 2, 17);
        Aircraft_Proposal__c AT = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c= lease1.Aircraft__c, Engine_Type_new__c='V2524',Rent__c = 509090, 
                                                            Asset_Type__c  = 'A320-100', Lease_Term__c = '2 Years', Term_Mos__c = 68,
                                                            Date_of_Manufacture__c = myDate);
        insert AT;
        list<DealAnalysisData.Data> data = AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 3, 0, 'Aircraft','100');
        if(data != null) {
            system.assertEquals(2, data[3].Recs.size());
        }
    }

    public static void testSourceLease() {
        list<DealAnalysisData.Data> data = AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 4, 0, 'Aircraft','100');
        if(data != null) {
            system.assertEquals(1, data[1].Recs.size());
        }
    }
    
    public static void testSourceIntelMarket(Operator__c opr){
        Aircraft_Need__c myACNeed = new Aircraft_Need__c(Operator__c=opr.Id, Aircraft_Type__c='A320',
                                                         Vintage_From__c = '2010', Term_Mos__c = 72 
                                                        );
        insert myACNeed;
        Aircraft_Type__c airCraftType = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320', Aircraft_Variant__c ='100',
                                                             Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56', Engine_Variant__c = '56');
        insert airCraftType;
        Consolidated_Intel__c Intel = new Consolidated_Intel__c (Aircraft_Need__c = myACNeed.Id, Aircraft_Type__c = airCraftType.Id);
        insert Intel;
        
        list<DealAnalysisData.Data> data = AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 5, 0, 'Aircraft','100');
        if(data != null) {
            system.assertEquals(0, data[1].Recs.size());
        }
        list<DealAnalysisData.Data> dataEngine = AIUtils.GetDealAnalysisData('CFM56', 2010, 30, null, 5, 0, 'Engine','56');
        if(dataEngine != null) {
            system.assertEquals(0, dataEngine[1].Recs.size());
        }
    }
    
    public static void testSourceWorldFleet() {
        
        Date myDa = Date.newInstance(2029, 2, 17);
        Global_Fleet__c gf = new Global_Fleet__c( Name = 'testWorldFleet', BuildYear__c = '2010', CurrentMarketLeaseRate_m__c = 6,
                                                 AircraftType__c = 'A320', AircraftVariant__c = '100', Lease_Expiry__c = myDa,
                                                 Aircraft_External_ID__c = 'test-msn1');
        LeaseWareUtils.clearFromTrigger();
        insert gf; 
    
        Global_Fleet__c gfEng = new Global_Fleet__c( Name = 'testWorldFleet', BuildYear__c = '2010', CurrentMarketLeaseRate_m__c = 78, 
                                                    EngineType__c = 'V2524', EngineVariant__c = 'EVtest',  Lease_Expiry__c = myDa,
                                                    Aircraft_External_ID__c = 'test-msn2');
        insert gfEng;
     	
        AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 6, 0, 'Aircraft','100');
        AIUtils.GetDealAnalysisData('V2524', 2010, 30, null, 6, 0, 'Engine','EVtest');
        list<DealAnalysisData.Data> data = AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 5, 0, 'Aircraft','100');
        if(data != null) {
            system.assertEquals(0, data[2].Recs.size());
        }
        list<DealAnalysisData.Data> dataEngine = AIUtils.GetDealAnalysisData('A320', 2010, 30, null, 5, 0, 'Aircraft','100');
        if(dataEngine != null) {
            system.assertEquals(0, dataEngine[2].Recs.size());
        }
    }
    
}