/* This batch class is used to create Supplemental Rent Transactions for Invoices, Paym ents, Adjustments , Fund Codes and Fund Transfers
   It is executed from :  
   		1) AssemblyMRRateTRiggerHandler
   		2) InvoiceAdjLineItemTriggerHandler
   		3) GenerateUtilizationsFromURStaging
   		4) LeaseWareDataFixScript
   		5) SupplementalRentTransactionsHandler */

public with sharing class SupplementalRentTransactionsBatch implements Database.Batchable<sObject>,Database.Stateful {

	private Process_Request_Log__c ReqLog;
	integer totalRecords = 0;
	integer failedRecordCount = 0;
	private transient static integer batchSize = 10;
	Map<Id,SupplementalRentTransactionsHandler.FailedRecord> mapFailedRecods = new Map<Id,SupplementalRentTransactionsHandler.FailedRecord>();
	private Set<Id> setSupplementalRentsFinal = new Set<Id>();
	Set<Id> setLeaseId = new Set<Id>();
	Boolean callFromRefreshTransactionsButton  = false;
	string exceptionmsg = '' ;

	public SupplementalRentTransactionsBatch(Set<Id> setLeaseId,Boolean callFromRefreshTransactionsButton ) {
		this.setLeaseId = setLeaseId;
		this.callFromRefreshTransactionsButton  = callFromRefreshTransactionsButton ;
	}
	
	//START METHOD
	public Database.QueryLocator start(Database.BatchableContext BC)                // Start Method
	{
		System.debug('SupplementalRentTransactionsBatch.Start(+)');
		boolean errorFlg=false;
		
		Set<Id> setLeaseId2 = new Set<Id>();
		setLeaseId2 = this.setLeaseId;  // Need to copy it in this local variable else it gives System.QueryException: unexpected token: '{'  .
		
		string strSessionId = string.valueOf(datetime.now());
		ReqLog = new Process_Request_Log__c(Status__c ='Success',Type__c = 'Supplemental Rent Transactions',
		                                    Comments__c=strSessionId, Exception_Message__c='');
		insert ReqLog;
		System.debug('leaseID in start :' + setLeaseId2);
		String strQuery = getQuery(setLeaseId2);

		System.Debug('StrQuery=='+strQuery);
		System.debug('SupplementalRentTransactionsBatch.Start(-)');
		return Database.getQueryLocator(strQuery);
	}

	//EXECUTE METHOD
	public void execute(Database.BatchableContext BC, List<sObject> scope)          // Execute Logic
	{
		System.debug('SupplementalRentTransactionsBatch.Execute(+)');
		try{
			mapFailedRecods = createTransaction(scope);
			if(callFromRefreshTransactionsButton) {updateTotalOutstanding(scope);} // so that this field update happens only from scheduler or RefreshMRTransactions button
		}catch(Exception e){
			System.debug('ERROR:' + e.getMessage());
			exceptionmsg += e.getMessage();
		}
		System.debug('mapFailedRecods --' + mapFailedRecods);
		System.debug('SupplementalRentTransactionsBatch.Execute(-)');
	}

	// Logic to be Executed at finish
	public void finish(Database.BatchableContext BC)
	{
		System.debug('SupplementalRentTransactionsBatch.Finish(+)');
		System.debug('totalRecords:' + totalRecords);
		UpdateSupplementalRentTransactions(this.setLeaseId);
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
		                  TotalJobItems, CreatedBy.Email
		                  FROM AsyncApexJob WHERE Id =:BC.getJobId()];

		String emailSubject = '[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] -';
		string strMsg = '';
		 if( mapFailedRecods.values().size()>0) {
			emailSubject += 'Supplemental Rent Transaction creation failed \n';
			strMsg += createEmailBody( mapFailedRecods.values(),strMsg);
		}else if(String.isNotBlank(exceptionmsg)){
			emailSubject += 'Supplemental Rent Transaction creation failed \n';
			strMsg += createEmailBody( mapFailedRecods.values(),exceptionmsg);
		}
		else{
			emailSubject += ' Supplemental Rent Transaction updated successfully \n';
			strMsg +=   '<b>Total records processed : </b>'+ totalRecords + '<br/>'+
						'<b>Total records suceeded : </b>'+ (totalRecords - failedRecordCount) ;
		}
		if(this.callFromRefreshTransactionsButton ){
			try{
				LeaseWareUtils.sendEmail(emailSubject , strMsg, UserInfo.getUserEmail());
			}catch(EmailException e){
				LeaseWareUtils.sendEmail('Error sending email for Failed Transaction Records' ,'Unexpected Exception', UserInfo.getUserEmail());
			}
		}

		System.debug('SupplementalRentTransactionsBatch.Finish(-)');
	}

	public String getQuery(Set<Id> setLeaseId2){

		String strQuery = 'SELECT Id, Name,Fund_Code__c,Lease__c,Lease__r.Lease_Start_Date_New__c,createdDate,Opening_Balance_Date_Override__c,'
		+'(Select id , Name ,createdDate,Amount_Invoiced__c,Amount_Cash__c,Transaction_Date__c,'
		+'Document_Number__c, Document_Status__c,Fund_Code_Lkp__c ,Supplemental_Rent_Adjustment__c,Invoice_Adjustment_Line_Item__c,RecordId__c,'
		+'Transaction_Type__c ,Assembly_MR_Fund_Transfer__c,Credit_Memo_Issuance__c,Event_Payable__c,Event_Payout__c,Invoice_Line_Item__c,MR_Fund_Transfer__c,Payment_Line_Item__c,Previous_Transaction__c,'
		+'Sequence_Number__c,Supplemental_Rent__c '
		+ 'FROM Supplemental_Rent_Transactions__r order by Transaction_Date__c asc),'
		+'(select id, Name,createdDate,Payment_Date__c, Amount_Paid__c,Payment__r.Payment_Type__c,Payment__r.Name,Payment__r.Credit_Memo__c,'
		+'Payment__r.Credit_Memo__r.Name,Payment__r.Credit_Memo__r.Y_Hidden_IsStandalone__c,'
		+'Payment__r.RecordTypeId ,status__c,Assembly_MR_Info__r.Name from Payment_Line_Items__r'
		+' order by Payment_Date__c asc),'
		+'(select id, Name,createdDate,Assembly_Utilization__c,Invoice_Date__c,Invoice__c,Invoice__r.Status__c, Invoice__r.Name,Invoice__r.RecordTypeId,Assembly_MR_Info__r.Name from Invoice_Line_Items__r'
		+' order by Invoice_Date__c asc),'
		+'(select id,Name,createdDate, Payable_Balance__c,Status__c,Payment_Status__c,Payable_Date__c,Lessor_Contribution_Committed__c,Supplemental_Rent__r.Name,'
		+' Payable_Amount__c, Last_Payout_Date__c'
		+' from Payables__r where Payable_Type__c = \'MR Claim Event\' order by Payable_Date__c asc),'
		+'(select id,Name,createdDate, Payout_Date__c,Status__c,Payout_Type__c,Supplemental_Rent__r.Name,'
		+' Payout_Amount__c'
		+' from Payouts__r where Payables_Type__c = \'MR Claim Event\' order by Payout_Date__c asc),'
		+'(select id, Name,createdDate, MR_Fund_Transfer__c, Target_Assembly_MR_Lookup__r.Name, MR_Fund_Transfer__r.Name,MR_Fund_Transfer__r.Status__c,MR_Fund_Transfer__r.Date__c from Assembly_MR_Fund_Transfers1__r ),' //Target
		+'(select id, Name,createdDate,MR_Fund_Transfer__c, Source_Assembly_MR_Lookup__r.Name, MR_Fund_Transfer__r.Name,MR_Fund_Transfer__r.Status__c,MR_Fund_Transfer__r.Date__c from Assembly_MR_Fund_Transfers__r ),' // Source
		+'(select id, name,Date__c,Amount__c,Supplemental_Rent__c,Supplemental_Rent__r.Name ,createdDate from Supplemental_Rent_Adjustments__r),'
		+'(select id, name,Credit_Memo__c,Credit_Memo__r.Name,Credit_Memo__r.Credit_Memo_Date__c,Credit_Memo__r.Status__c,Amount__c,Supplemental_Rent__c,Supplemental_Rent__r.Name,createdDate from Credit_Memo_Issuances__r)'
		+'FROM Assembly_MR_Rate__c ';
		if(setLeaseId2 != null && setLeaseId2.size()>0){
			strQuery = strQuery + 'where Lease__c IN:setLeaseId2 AND RecordType.DeveloperName = \'MR\' order by Lease__c'+ (Test.isRunningTest() ? ' LIMIT 5' : '');
		}else{
			strQuery = strQuery + ' where RecordType.DeveloperName = \'MR\' '+ (Test.isRunningTest() ? ' LIMIT 5' : '');
		}
		return strQuery;
	}
	public Map<Id,SupplementalRentTransactionsHandler.FailedRecord> createTransaction(list<Assembly_MR_Rate__c> scope){
		
		List<Supplemental_Rent_Transactions__c> listSSWithErrors = new List<Supplemental_Rent_Transactions__c>();
		Map<Id,Assembly_MR_Rate__c> mapSupplementalRents = new  Map<Id,Assembly_MR_Rate__c>();
		Map<Id,Invoice_Line_Item__c> mapInvoiceLineItems = new  Map<Id,Invoice_Line_Item__c>();
		Map<Id,Invoice_Adjustment_Line_Item__c> mapInvoiceAdjstmntLineItems = new  Map<Id,Invoice_Adjustment_Line_Item__c>();
		Map<Id,List<Invoice_Adjustment_Line_Item__c>> mapSRIdToInvAdjstmntLineItems = new  Map<Id,List<Invoice_Adjustment_Line_Item__c>>();
		Map<Id,Payment_Line_Item__c> mapPaymentLineItems = new  Map<Id,Payment_Line_Item__c>();
		Map<Id,Payable__c> mapEventPayables = new  Map<Id,Payable__c>();
		Map<Id,Payout__c> mapEventPayouts = new  Map<Id,Payout__c>();
		Map<Id,Assembly_MR_Fund_Transfer__c> mapSourceTransfers = new  Map<Id,Assembly_MR_Fund_Transfer__c>();
		Map<Id,Assembly_MR_Fund_Transfer__c> mapTargetTransfers = new  Map<Id,Assembly_MR_Fund_Transfer__c>();
		Map<Id,Credit_Memo_Issuance__c> mapCreditMemo = new  Map<Id,Credit_Memo_Issuance__c>();
		Map<Id,Supplemental_Rent_Adjustment__c> mapAdjustments = new  Map<Id,Supplemental_Rent_Adjustment__c>();
		Map<Id,List<Assembly_MR_Rate__c> > mapSRToLease = new Map<Id,List<Assembly_MR_Rate__c> >();
		List<Supplemental_Rent_Transactions__c> listSRTToInsert = new List<Supplemental_Rent_Transactions__c>();
		Set<Assembly_MR_Rate__c> setSupplementalRents = new Set<Assembly_MR_Rate__c>();
		Map<Id,String> mapFailedReason = new  Map<Id,String>();

		listSRTToInsert.clear();
		database.SaveResult[] res = new List<database.SaveResult>();
		List<Supplemental_Rent_Transactions__c> finalListToUpsert = new List<Supplemental_Rent_Transactions__c>();
			for(Assembly_MR_Rate__c curAMR: scope) {
				System.debug('SR in execute  :' + curAMR);
				createChildRecordsMaps(curAMR,mapSupplementalRents,mapInvoiceLineItems, mapPaymentLineItems, mapEventPayables,mapEventPayouts,
										mapSourceTransfers, mapTargetTransfers,mapCreditMemo, mapAdjustments,mapInvoiceAdjstmntLineItems,mapSRIdToInvAdjstmntLineItems);
				setSupplementalRents.add(curAMR);
				setSupplementalRentsFinal.add(curAMR.Id);
				//System.debug('mapSRIdToInvAdjstmntLineItems:' + mapSRIdToInvAdjstmntLineItems);
				//System.debug('mapInvoiceAdjstmntLineItems:' + mapInvoiceAdjstmntLineItems);
				try{
					listSRTToInsert.addAll(SupplementalRentTransactionsHandler.createTransactions(setSupplementalRents,mapSRIdToInvAdjstmntLineItems));
				}catch(Exception e){
					System.debug('ERROR in execute:' + e.getMessage());
					exceptionmsg += e.getMessage();
				}

				if(listSRTToInsert.size()>0) {
					System.debug('List size After :' + listSRTToInsert.size());
					finalListToUpsert.addAll(listSRTToInsert);
					System.debug('finalListToUpsert :' + finalListToUpsert.size());
					totalRecords += finalListToUpsert.size();
					res = database.insert(finalListToUpsert,false);
				}
				listSRTToInsert.clear();
				setSupplementalRents.clear();
				System.debug('execute : totalRecords:' + totalRecords);
				if(finalListToUpsert.size()>0) {
					for(Integer i=0; i < res.size(); i++) {
						if(res.get(i).isSuccess()) {
							System.debug(' record updated... : ' + finalListToUpsert[i].Id );
						}else{
							Database.Error errors =  res.get(i).getErrors().get(0);
							listSSWithErrors.add(finalListToUpsert[i]);
							failedRecordCount =  failedRecordCount + listSSWithErrors.size();
							mapFailedReason.put(finalListToUpsert[i].RecordId__c,errors.getMessage());
							string errormsg = 'ERROR: Sanpshot Update failed for ' + res.get(i) +' .\n';
							System.debug(errormsg);
							for (Database.Error err:res.get(i).getErrors()) {
								errormsg += err.getStatusCode() + ': ' + err.getMessage() + '\n';
								errormsg += ('Fields that affected this error: ' + err.getFields() + '\n');
							}
							if(ReqLog !=null) {ReqLog.Exception_Message__c += errormsg;}
							LeasewareUtils.createExceptionLog(null, errormsg,'Supplemental Rent Transactions', null, 'Snapshot', false);
						}
					}
				}
				finalListToUpsert.clear();
			}
			
		// gets all the transactions for a SR and recalculates the running balance . 
		SupplementalRentTransactionsHandler.updatePreviousTransaction(setSupplementalRentsFinal);
		LeaseWareUtils.insertExceptionLogs();
		if(ReqLog !=null && ReqLog.Exception_Message__c!=null) {
			ReqLog.Exception_Message__c = ReqLog.Exception_Message__c.left(2000);
			update ReqLog;
		}
		System.debug('FAILED RECORDS:' + listSSWithErrors.size() +'::'+ listSSWithErrors);
		if(listSSWithErrors.size()>0){
			for(Supplemental_Rent_Transactions__c curRec:listSSWithErrors) {
				System.debug('FAILED:' + curRec.RecordId__c);
				System.debug('mapsObjects:' + mapFailedReason);
				Date recordDate;
				String recordStatus = '';
				String failedReason = '';
				String name = '';
				String srName = '';
				SupplementalRentTransactionsHandler.FailedRecord failedRec =  new SupplementalRentTransactionsHandler.FailedRecord();
				if(curRec.Transaction_Type__c.equals('Opening Balance')) {
					Assembly_MR_Rate__c record = mapSupplementalRents.get(Id.valueOf(curRec.RecordId__c));
					//failedRec.DocumentName = record.Id;
					failedRec.SupplementalRentId = record.Id;
					failedRec.SupplementalRentName = record.Name;
					name = record.Name;
					recordDate = record.Lease__r.Lease_Start_Date_New__c;
					failedReason = mapFailedReason.get(record.Id);
				}else if(curRec.Transaction_Type__c.contains('Payment') || curRec.Transaction_Type__c.equals('MR Invoice Credit (Adjustment)')) {
					Payment_Line_Item__c record = mapPaymentLineItems.get(Id.valueOf(curRec.RecordId__c));
					failedRec.DocumentName = record.Id;
					recordDate = record.Payment_Date__c;
					recordStatus = record.Status__c;
					name = record.Payment__r.Name;
					srName = record.Assembly_MR_Info__r.Name;
					failedRec.SupplementalRentId = curRec.Supplemental_Rent__c;
					failedReason = mapFailedReason.get(record.Id);
				}else if(curRec.Transaction_Type__c.equals('MR Claim') ||curRec.Transaction_Type__c.contains('Contribution')) {
					Payable__c record = mapEventPayables.get(Id.valueOf(curRec.RecordId__c));
					failedRec.DocumentName = record.Id;
					recordDate = record.Payable_Date__c;
					recordStatus = record.Status__c;
					name = record.Name;
					srName = record.Supplemental_Rent__r.Name;
					failedReason = mapFailedReason.get(record.Id);
				}
				else if(curRec.Transaction_Type__c.equals('MR Claim Refund') ||curRec.Transaction_Type__c.equals('MR Claim Offset')) {
					Payout__c record = mapEventPayouts.get(Id.valueOf(curRec.RecordId__c));
					failedRec.DocumentName = record.Id;
					recordDate = record.Payout_Date__c;
					recordStatus = record.Status__c;
					name = record.Name;
					srName = record.Supplemental_Rent__r.Name;
					failedReason = mapFailedReason.get(record.Id);
				}
				else if(curRec.Transaction_Type__c.equals('MR Invoice') || curRec.Transaction_Type__c.equals('MR Invoice Reconciliation')) {
					Invoice_Line_Item__c record = mapInvoiceLineItems.get(Id.valueOf(curRec.RecordId__c));
					failedRec.DocumentName = record.Id;
					recordDate = record.Invoice_Date__c;
					recordStatus = record.Invoice__r.Status__c;
					name = record.Invoice__r.Name;
					srName = record.Assembly_MR_Info__r.Name;
					failedReason = mapFailedReason.get(record.Id);
				}else if(curRec.Transaction_Type__c.contains('Transfer') ) {
					Assembly_MR_Fund_Transfer__c record  = new Assembly_MR_Fund_Transfer__c();
					if(mapSourceTransfers.containsKey(Id.valueOf(curRec.RecordId__c))){
						record = mapSourceTransfers.get(Id.valueOf(curRec.RecordId__c));
						srName = record.Source_Assembly_MR_Lookup__r.Name;
					}else If(mapTargetTransfers.containsKey(Id.valueOf(curRec.RecordId__c))){
						record = mapTargetTransfers.get(Id.valueOf(curRec.RecordId__c));
						srName = record.Target_Assembly_MR_Lookup__r.Name;
					}
					failedRec.DocumentName = record.Id;
					recordDate = record.MR_Fund_Transfer__r.Date__c;
					recordStatus = record.MR_Fund_Transfer__r.Status__c;
					name = record.MR_Fund_Transfer__r.Name;				
					failedReason = mapFailedReason.get(record.Id);
				}else if(curRec.Transaction_Type__c.equals('Adjustment') ) {
					Supplemental_Rent_Adjustment__c record = mapAdjustments.get(Id.valueOf(curRec.RecordId__c));
					failedRec.DocumentName = record.Id;
					recordDate = record.Date__c;
					name = record.Name;
					srName = record.Supplemental_Rent__r.Name;
					failedReason = mapFailedReason.get(record.Id);
				}else if(curRec.Transaction_Type__c.equals('MR Invoice Overpayment Credit') ) {
					Credit_Memo_Issuance__c record = mapCreditMemo.get(Id.valueOf(curRec.RecordId__c));
					failedRec.DocumentName = record.Id;
					recordDate = record.Credit_memo__r.Credit_memo_Date__c;
					recordStatus = record.Credit_memo__r.Status__c;
					name = record.Credit_Memo__r.Name;	
					srName = record.Supplemental_Rent__r.Name;				
					failedReason = mapFailedReason.get(record.Id);
				}else if(curRec.Transaction_Type__c.equals('MR Invoice Adjustment') ) {
					Invoice_Adjustment_Line_Item__c record = mapInvoiceAdjstmntLineItems.get(Id.valueOf(curRec.RecordId__c));
					failedRec.DocumentName = record.Id;
					recordDate = record.Invoice_Adjustment__r.Adjustment_Date__c;
					recordStatus = record.Invoice_Adjustment__r.Approval_Status__c;
					name = record.Name;
					srName = record.Linked_Invoice_Line_Item__r.Assembly_MR_Info__r.Name;
					failedReason = mapFailedReason.get(record.Id);
				}
				failedRec.DocumentDate = String.valueOf(recordDate);
				if(!curRec.Transaction_Type__c.equals('Opening Balance')){
					failedRec.SupplementalRentId = curRec.Supplemental_Rent__c;
					failedRec.SupplementalRentName = srName;
				}		
				failedRec.DocumentStatus = recordStatus;
				failedRec.Type = curRec.Transaction_Type__c;
				failedRec.Name = name;
				failedRec.FailedReason = failedReason;
				mapFailedRecods.put(failedRec.DocumentName,failedRec);
			}
		}
		return mapFailedRecods;
	}

	/*Method to get sum of Line Amount Balance Due  on all Invoice Line Items for any Invoice where Overdue is TRUE,
		 and update that value on {Total Outstanding (Past due)} field on SR
	  ---> This gets updated only when batch is called from UpdateRentOnLeaseScheduler scheduler  OR 
	  ---> RefreshMRTransactions button on Related List of Supplemental Rent on Lease */
	public void updateTotalOutstanding(list<Assembly_MR_Rate__c> scope){
		System.debug('SupplementalRentTransactionsBatch.updateTotalOutstanding(+)');
		List<Assembly_MR_Rate__c> listToUpdate = new List<Assembly_MR_Rate__c>();
		Id srId;
		Decimal balanceDue = 0.0;
		for(AggregateResult result : [ select SUM(Balance_Due__c) balDue, Assembly_MR_Info__c srId from Invoice_Line_Item__c where Invoice__r.Overdue__c = true 
		                              AND Assembly_MR_Info__c in :scope AND 
									  Invoice__r.status__c  IN ('Approved','Pending','Cancelled-Pending') AND Invoice__r.payment_status__c IN('Partially Paid','Open')
									  GROUP BY Assembly_MR_Info__c ]){
		    System.debug('Result---> ' + result);
			srId = (Id) result.get('srId');
			balanceDue = (Decimal) result.get('balDue');
			Assembly_MR_Rate__c suppRent = new Assembly_MR_Rate__c(Id = srId,
											Total_Outstanding_Past_Due__c = balanceDue 
											);
			listToUpdate.add(suppRent);
		}

		if(listToUpdate.size()>0){
			LeaseWareUtils.TriggerDisabledFlag = true; // disabling the trigger to avoid any scenario of transaction batch being called again from SR trigger. 
			try{
				System.debug('listToUpdate---' + listToUpdate);
				update listToUpdate;
			}catch(DmlException e){
				System.debug('Error updating SR: ' + e.getMessage());
				LeaseWareUtils.createExceptionLog(e,'Error updating {Total Outstanding (Past due)}  field on SR ','Supplemental Rent','','Update SR from SRT batch', true);
			}
			LeaseWareUtils.TriggerDisabledFlag = false;
		}
		System.debug('SupplementalRentTransactionsBatch.updateTotalOutstanding(-)');
	}

	public static Integer getBatchSize(){
		//ServiceSetting__c lwServiceSettings = ServiceSetting__c.getInstance('SnapshotBatch');
		//if(lwServiceSettings != null && lwServiceSettings.Batch_Size__c != null) batchSize = integer.valueOf(lwServiceSettings.Batch_Size__c);
		System.debug('batchSize' + batchSize) ;
		return batchSize;
	}

	@TestVisible
	private string createEmailBody(List<SupplementalRentTransactionsHandler.FailedRecord> listFailedRecords,String strMsg ){
		String emailBody = strMsg;
		String url = URL.getSalesforceBaseUrl().toExternalForm();
		System.debug('createEmailBody : totalRecords:' + totalRecords);
		emailBody+=  '<b>Total records processed : </b>'+ totalRecords + '<br/>'+
					'<b>Total records suceeded : </b>'+ Math.abs((totalRecords - listFailedRecords.size())) + '<br/>';
		if(listFailedRecords.size()>0){
			emailBody+= '<br/> <table border="1" style="border-collapse: collapse"><caption><b>Snapshot creation failed for records with following data :</b> </caption>'+
			'<tr><th><b>Date</b></th><th><b>Supplemental Rent</b></th><th><b>Document Name</b></th>'+
			'<th><b>Document Status</b></th><th><b>Type</b></th><th><b>Failed Reason</b></th></tr>';
			for (SupplementalRentTransactionsHandler.FailedRecord str: listFailedRecords) {
				//1
				emailBody += '<tr><td>' + str.DocumentDate + '</td>';

				//2	
				if(str.SupplementalRentId == null) {
					emailBody += ('<td><div style="background-color:yellow;text-align:centre;">'+str.SupplementalRentName + '<a><u></u></a></div></td>');
				}else{
					emailBody += ('<td><a href="'  + url + '/' + str.SupplementalRentId + '">' +str.SupplementalRentName + '</a></td>');
				}
	
				//3
				if(str.DocumentName == null) {
					emailBody += ('<td>'+''+'</td>');
				}else {
					emailBody += ('<td><a href="'  + url + '/' + str.DocumentName + '">' +str.Name + '</a></td>');
				}
	
				//4
				if(String.isBlank(str.DocumentStatus)) {
					emailBody += ('<td><div style="background-color:yellow;text-align:centre;">'+str.DocumentStatus + '<a><u></u></a></div></td>');
				}else{
					emailBody += ('<td>'+str.DocumentStatus + '</td>');
				}
	
				//5
				if(str.Type == null) {
					emailBody += ('<td><div style="background-color:yellow;text-align:centre;">'+str.Type + '<a><u></u></a></div></td>');
				}else{
					emailBody += ('<td>'+str.Type + '</td>');
				}
	
	
				//6
				if(String.isNotBlank(str.FailedReason)) {
					emailBody += ('<td>'+str.FailedReason + '</td>');
				}
			}
			emailBody += '</table>';
		}else{
			emailBody += '<b>Exception Occured: </b>'+ strMsg + '<br/>';
		}
		return emailBody;
	}

	private void createChildRecordsMaps(Assembly_MR_Rate__c suppRent,Map<Id,Assembly_MR_Rate__c> mapSupplementalRents,
		Map<Id,Invoice_Line_Item__c> mapInvoiceLineItems,Map<Id,Payment_Line_Item__c> mapPaymentLineItems,Map<Id,Payable__c> mapEventPayables,
		Map<Id,Payout__c> mapEventPayouts,
		Map<Id,Assembly_MR_Fund_Transfer__c> mapSourceTransfers, Map<Id,Assembly_MR_Fund_Transfer__c> mapTargetTransfers,Map<Id,Credit_Memo_Issuance__c> mapCreditMemo,
		Map<Id,Supplemental_Rent_Adjustment__c> mapAdjustments,Map<Id,Invoice_Adjustment_Line_Item__c> mapInvoiceAdjstmntLineItems ,
		Map<Id,List<Invoice_Adjustment_Line_Item__c>> mapSRIdToInvAdjstmntLineItems){

		mapSupplementalRents.put(suppRent.Id,suppRent);
		for(Payment_Line_Item__c curPaymnt : suppRent.Payment_Line_Items__r) {
			mapPaymentLineItems.put(curPaymnt.Id,curPaymnt);
		}
		for(Invoice_Line_Item__c curInv : suppRent.Invoice_Line_Items__r) {
			mapInvoiceLineItems.put(curInv.Id,curInv);
		}
		for(Payable__c curPayable : suppRent.Payables__r) {
			mapEventPayables.put(curPayable.Id,curPayable);
		}
		for(Payout__c curPO : suppRent.Payouts__r) {
			mapEventPayouts.put(curPO.Id,curPO);
		}
		for(Assembly_MR_Fund_Transfer__c curTransfer: suppRent.Assembly_MR_Fund_Transfers1__r) {
			mapTargetTransfers.put(curTransfer.Id,curTransfer);
		}
		for(Assembly_MR_Fund_Transfer__c curTransfer: suppRent.Assembly_MR_Fund_Transfers__r) {
			mapSourceTransfers.put(curTransfer.Id,curTransfer);
		}
		for(Supplemental_Rent_Adjustment__c curAdjustm: suppRent.Supplemental_Rent_Adjustments__r) {
			mapAdjustments.put(curAdjustm.Id,curAdjustm);
		}
		for(Credit_Memo_Issuance__c curCreditMemo : suppRent.Credit_Memo_Issuances__r) {
			mapCreditMemo.put(curCreditMemo.Id,curCreditMemo);
		}
			// Get a map InvoiceAdjustmentLine Items for all InvoiceLineItems on supplemental Rent
		List<Invoice_Adjustment_Line_Item__c> listInvoiceAdjstmntLIneItems = [Select Id, Name,createdDate, Invoice_Adjustment__c,Invoice_Adjustment__r.Approval_Status__c,
		Invoice_Adjustment__r.Adjustment_Date__c,Line_Adjustment_Amount__c,Linked_Invoice_Line_Item__r.Assembly_MR_Info__c,Linked_Invoice_Line_Item__r.Assembly_MR_Info__r.Name,
		Linked_Invoice_Line_Item__r.Assembly_MR_Info__r.Fund_Code__c from Invoice_Adjustment_Line_Item__c where Linked_Invoice_Line_Item__c in : suppRent.Invoice_Line_Items__r];
		
		System.debug('listInvoiceAdjstmntLIneItems:' + listInvoiceAdjstmntLIneItems);
		
		for(Invoice_Adjustment_Line_Item__c invAdjstmntLineItem : listInvoiceAdjstmntLIneItems){
				mapInvoiceAdjstmntLineItems.put(invAdjstmntLineItem.Id,invAdjstmntLineItem);
		}
		List<Invoice_Adjustment_Line_Item__c> listInvAdjLI ;
		for(Invoice_Adjustment_Line_Item__c curInvAdjLI : listInvoiceAdjstmntLIneItems){
			if(mapSRIdToInvAdjstmntLineItems.containsKey(curInvAdjLI.Linked_Invoice_Line_Item__r.Assembly_MR_Info__c)){
				listInvAdjLI = mapSRIdToInvAdjstmntLineItems.get(curInvAdjLI.Linked_Invoice_Line_Item__r.Assembly_MR_Info__c);
			}else{
				listInvAdjLI = new List<Invoice_Adjustment_Line_Item__c>();			
			}
			listInvAdjLI.add(curInvAdjLI);
			mapSRIdToInvAdjstmntLineItems.put(curInvAdjLI.Linked_Invoice_Line_Item__r.Assembly_MR_Info__c,listInvAdjLI);
		}
	}

	/*Method to update transactions and call WF and Running balance if (Amount_Invoiced__c != Amount_Invoiced_Sum__c) OR \
	   (Amount_Cash__c != Amount_Cash_Sum__c)
        called from : finish of batch
    */
    public void UpdateSupplementalRentTransactions(Set<Id> leaseId ){
		System.debug('From batch UpdateSupplementalRentTransactions : Lease id :' + leaseId);
        List<Supplemental_Rent_Transactions__c> listSRTToUpdate = new List<Supplemental_Rent_Transactions__c>();
		Set<Id> setAsmblyMRInfosId = new  Set<Id>();
        for(Supplemental_Rent_Transactions__c curTransac :[Select id , Name ,createdDate,Transaction_Date__c, Lease_Start_Date__c,          
															Fund_Code_Lkp__c ,Amount_Invoiced__c,Amount_Invoiced_Sum__c,
															Amount_Cash__c,Amount_Cash_Sum__c,
															Transaction_Type__c ,Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,
															Supplemental_Rent__r.Lease__r.Lease_Start_Date_New__c
															FROM Supplemental_Rent_Transactions__c where Supplemental_Rent__r.Lease__c in :leaseId ]){
			
			setAsmblyMRInfosId.add(curTransac.Supplemental_Rent__c);
			if((curTransac.Amount_Invoiced__c != curTransac.Amount_Invoiced_Sum__c) || 
			(curTransac.Amount_Cash__c != curTransac.Amount_Cash_Sum__c) ||
			(curTransac.Lease_Start_Date__c != curTransac.Supplemental_Rent__r.Lease__r.Lease_Start_Date_New__c)){
				//update the lease start date when not matching with the start date on the lease record. Not a very likely scenario but to be on safer side
				if(curTransac.Lease_Start_Date__c != curTransac.Supplemental_Rent__r.Lease__r.Lease_Start_Date_New__c){
					curTransac.Lease_Start_Date__c = curTransac.Supplemental_Rent__r.Lease__r.Lease_Start_Date_New__c;
				}
				listSRTToUpdate.add(curTransac);
			}
		}
        System.debug('listSRTToUpdate::' + listSRTToUpdate);
        if(listSRTToUpdate.size()>0){
            update listSRTToUpdate;
        }
        // updating Running balance on Transactions.
       SupplementalRentTransactionsHandler.updatePreviousTransaction(setAsmblyMRInfosId);
    }
}