@isTest
public class TestAircraftRecommendationController {

    @isTest
    static void loadTestCases(){
        Country__c country =new Country__c(Name='United States');
        insert country;

        Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
      //opr.Country__c = 'United States';
        opr.Country_Lookup__c = country.id;
        opr.Region__c = 'Asia';
        insert opr;
        System.assert(opr.Id != null);
    
        Global_Fleet__c gf = new Global_Fleet__c();
        gf.Name = 'testWorldFleet';
        gf.Aircraft_External_ID__c = 'type-msn';
        gf.MTOW__c = 134453;
        gf.BuildYear__c = '1996';
        gf.SeatTotal__c = 12;
        gf.AircraftType__c = 'A320';
        gf.AircraftVariant__c = '400';
        gf.EngineType__c = 'V2524';
        gf.AircraftSeries__c = 'AStest';
        gf.EngineVariant__c = 'EVtest';
        gf.OriginalOperator__c ='testOpr';
        gf.Aircraft_Operator__c = opr.Id;
        
        insert gf;
        System.assert(gf.Id != null);
        
        Global_Fleet__c gf1 = new Global_Fleet__c();
        gf1.Name = 'testWorldFleet1';
        gf1.Aircraft_External_ID__c = 'type1-msn1';
        gf1.MTOW__c = 134498;
        gf1.BuildYear__c = '1998';
        gf1.SeatTotal__c = 120;
        gf1.AircraftType__c = 'A320';
        gf1.AircraftVariant__c = '400';
        gf1.SerialNumber__c ='101';
        gf1.EngineType__c = 'V2524';
        gf1.AircraftSeries__c = 'AStest1';
        gf1.OriginalOperator__c ='testOpr';
        gf1.EngineVariant__c = 'EVtest1';
        gf1.Aircraft_Operator__c = opr.Id;
        insert gf1;
        System.assert(gf1.Id != null);
        
        
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Test',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
        Contractual_Status__c = 'Managed'    
        );
        insert aircraft;
        
        Lease__c lease = new Lease__c(
        Reserve_Type__c= 'Maintenance Reserves',
        Base_Rent__c = 21,
        Lease_Start_Date_New__c = System.today()-2,
        Lease_End_Date_New__c = System.today(),
        Operator__c = opr.id,
        Aircraft__c =  aircraft.id);
        insert lease;
        
        
        Aircraft__c aircraft1 = new Aircraft__c(
        Name= '1123 Test1',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1240',
        MTOW_Leased__c = 134455,
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
		Contractual_Status__c = 'Managed'            
        );
        insert aircraft1;
        
        Recommendation_Factor__c rec = new Recommendation_Factor__c(
            Type__c = 'Aircraft',
            Factor__c = 'Aircraft Type',
            Weightage_Number__c = 75,
            range__c = 0,
            Inactive__c = false
        );
		insert rec;

        Recommendation_Factor__c rec1 = new Recommendation_Factor__c(
            Type__c = 'Aircraft',
            Factor__c = 'Aircraft Variant',
            Weightage_Number__c = 50,
            range__c = 0,
            Inactive__c = false
        );
		insert rec1;

        Recommendation_Factor__c rec2 = new Recommendation_Factor__c(
            Type__c = 'Aircraft',
            Factor__c = 'Engine',
            Weightage_Number__c = 50,
            range__c = 0,
            Inactive__c = false
        );
		insert rec2;

        Recommendation_Factor__c rec3 = new Recommendation_Factor__c(
            Type__c = 'Aircraft',
            Factor__c = 'Vintage',
            Weightage_Number__c = 30,
            range__c = 3,
            Inactive__c = false
        );
		insert rec3;

        Recommendation_Factor__c rec4 = new Recommendation_Factor__c(
            Type__c = 'Aircraft',
            Factor__c = 'MTOW',
            Weightage_Number__c = 20,
            range__c = 1000,
            Inactive__c = false
        );
		insert rec4;
        
        Recommendation_Factor__c rec5 = new Recommendation_Factor__c(
            Type__c = 'Aircraft',
            Factor__c = 'Existing Customer',
            Weightage_Number__c = 50,
            range__c = 0,
            Inactive__c = false
        );
		insert rec5;
        
        List<AircraftWrapper.Properties> aircraftList = AircraftRecommendationController.getAircraftsRecommended(opr.Id, 'A320', '400', 
                                                                                                                'V2524', 0,0,0,0);
        AircraftRecommendationController.getVintagePicklist();
        AircraftRecommendationController.getNameSpacePrefix();
        
        AircraftRecommendationController.getDependentMap('Aircraft__c', 'Aircraft_Type__c', 'Engine_Type__c');
		AircraftRecommendationController.isDealAlreadyCreated(opr.Id, aircraft1.Id);
        AircraftRecommendationController.newMarkActivity(opr.Id, '3245', aircraft1.Id);
        AircraftRecommendationController.getDealType();
        if(aircraftList != null && aircraftList.size() > 0) {
            for(AircraftWrapper.Properties aircraftRec: aircraftList) {
                if(aircraftRec.operatorId == opr.Id) {
                    String jsonStr = JSON.serialize(aircraftRec);
                    AircraftRecommendationController.getAircraftCriteria(jsonStr);
                }
            }
        }
        
        
    }
}