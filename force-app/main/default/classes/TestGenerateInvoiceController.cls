@isTest
public class TestGenerateInvoiceController{
    @testSetup 
     static void setup() {
        List<Invoice__c> InvoiceList = new List<Invoice__c>();
        Aircraft__c aircraftObject = new Aircraft__c(Name='new Lessor',MSN_Number__c='234234234234', Date_of_Manufacture__c=System.today(), Engine_Type__c='BR700', Aircraft_Variant__c='700', CSN__c=23454, TSN__c= 11246.00);
        insert aircraftObject;
        lease__c leaseObject =  new lease__c(Name='New Lease',Lease_Start_Date_New__c = System.today(),Lease_End_Date_New__c = System.today().addMonths(12),Aircraft__c=aircraftObject.Id,MR_Invoice_Payment_Due_Day_Type__c='Fixed');
        insert leaseObject;
         
        Stepped_Rent__c SteppedRentRec1 = new Stepped_Rent__c (
                                            Name='Dummy for SteppedRentRec1',
                                            Rent_Type__c='Fixed Rent',
                                            Rent_Period__c='Monthly',
                                            Base_Rent__c=7000.00,
                                            Rent_Amount__c=6000.00,
                                            Start_Date__c=system.today(), // YYYY-MM-DD
                                            //Start_Date__c=listleaserec[0].Lease_Start_Date_New__c,
                                            Rent_End_Date__c=system.today().addMonths(2) ,  // YYYY-MM-DD
                                            //Rent_End_Date__c=listleaserec[0].Lease_End_Date_New__c,
                                            Invoice_Generation_Day__c='31',
                                            Rent_Period_End_Day__c='6',
                                            Rent_Due_Type__c='Relative Based On Invoice Date',
                                            Rent_Due_Day__c=20,
                                            Lease__c=leaseObject.id,
                                            status__c=true
                                            );
		insert SteppedRentRec1 ;          
        rent__c rentObject = new rent__c(RentPayments__c= leaseObject.id,Name='New Rent',For_Month_Ending__c=System.today(),Stepped_Rent__c =SteppedRentRec1.Id);
        insert rentObject;
        Lessor__c lessorObject = new Lessor__c(Name='new Lessor');
        insert lessorObject;
        		
		// Set up the Constt Assmbly record
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= aircraftObject.Id, Description__c ='test description', TSN__c=0, CSN__c=0, Type__c ='Airframe', Serial_Number__c='123');
        LeaseWareUtils.clearFromTrigger();
		insert consttAssembly;
        // Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
		    Interval_2_Hours__c=20,
            Event_Type_Global__c = '4C/6Y'
        );
        insert mpe;
        // Set up Project Event
        Assembly_Event_Info__c eventAF = new Assembly_Event_Info__c();
        eventAF.Name = 'Test Info';
        eventAF.Event_Cost__c = 20;
        eventAF.Constituent_Assembly__c = consttAssembly.Id;
        eventAF.Maintenance_Program_Event__c = mpe.Id;
        insert eventAF;

        Date endDate = leaseObject.Lease_Start_Date_New__c.addMonths(1).toStartOfMonth().addDays(-1);
        Utilization_Report__c utilizationObject = new Utilization_Report__c(Name='New Utilization',Y_hidden_Lease__c=leaseObject.id,Aircraft__c = aircraftObject.id,Airframe_Cycles_Landing_During_Month__c = 5,FH_String__c = '22', Airframe_Flight_Hours_Month__c = 3.42,Month_Ending__c = endDate, Status__c='Approved By Lessor');
        insert utilizationObject;
        Cash_Security_Deposit__c cashdepositobj = new Cash_Security_Deposit__c(Name='cashdepostired',Lease__c = leaseObject.Id);
        insert cashdepositobj;
        Assembly_MR_Rate__c assemblyMrInfoObj = new Assembly_MR_Rate__c(Name='assembly Mr',Lease__c=leaseObject.id,Rate_Basis__c='Cycles',Assembly_Lkp__c=consttAssembly.id,Base_MRR__c=7.12,Assembly_Event_Info__c=eventAF.id);
        insert assemblyMrInfoObj;
        Account newAccount = new Account(Name='New Account');
        insert newAccount;
     }
    @isTest
    static void testInvoice() {
        List<Rent__c> rentLIst = [SELECT Id,RentPayments__c FROM Rent__c];
        List<Utilization_Report__c> utilizationLIst = [SELECT Id FROM Utilization_Report__c];
        List<Cash_Security_Deposit__c> cashSecurityObj = [SELECT Id FROM Cash_Security_Deposit__c];
        List<lease__c> leaseList = [SELECT Id FROM lease__c];
        Account oAccount = [SELECT Id,Name FROM Account LIMIT 1];
        //String assemblyMRInfoList = [SELECT Id FROM Assembly_MR_Rate__c];
        Invoice__c invoice = new Invoice__c();
        invoice.rent__c =  rentLIst[0].Id;
        invoice.Invoice_Date__c = system.today();
        Test.startTest();

        invoice = GenerateInvoiceController.saveRentInvoice(invoice,'Rent');
        GenerateInvoiceController.getAssemblyMRInfo(rentLIst[0].RentPayments__c);
        
        Invoice__c invoice2 = new Invoice__c();
        invoice2.Invoice_Date__c = system.today();
        invoice2.Utilization_Report__c = utilizationLIst[0].Id;
        GenerateInvoiceController.saveMRInvoice(invoice2,'Monthly_Utilization');
        
        Invoice__c invoice3 = new Invoice__c();
        invoice3.Invoice_Date__c = system.today();
        invoice3.Cash_Security_Deposit__c = cashSecurityObj[0].Id;
        GenerateInvoiceController.saveSDInvoice(invoice3,'Security_Deposit');
        
        GenerateInvoiceController.DataContainerWrapper recordTypeObject = GenerateInvoiceController.getData(rentLIst[0].Id);
        Invoice__c invoice4 = new Invoice__c();
        invoice4.Invoice_Date__c = system.today();
        invoice4.Utilization_Report__c = utilizationLIst[0].Id;
        invoice4.lease__c = leaseList[0].Id;
        List<GenerateInvoiceController.AssemblyMRInfoWrapper> mrinfoList = GenerateInvoiceController.getAssemblyMRInfo(leaseList[0].Id);
        
        for(GenerateInvoiceController.AssemblyMRInfoWrapper mrinObject : mrinfoList){
            mrinObject.amount = 0;
        }
        GenerateInvoiceController.saveInvoice(invoice4,'Monthly_Utilization',JSON.serialize(mrinfoList));
		
                
        PageReference pageRef = new PageReference('lkid=0011U000006vUuJQAU');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(oAccount);        
        InvoiceController invoceController = new InvoiceController(sc);

        
        Test.stopTest();
        //System.assertEquals(expected, actual)
    }
}