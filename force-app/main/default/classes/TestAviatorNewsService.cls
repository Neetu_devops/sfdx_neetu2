@isTest
public class TestAviatorNewsService {
	static PageReference pageRefOperatorNewsArticle = Page.NewsArticles;
	static PageReference pageRefLessorNewsArticle   = Page.LessorNewsArticles;
	static Operator__c operator1                        {get; set;}
	static Operator__c operator2                        {get; set;}
	static Operator__c operator3                        {get; set;}
	static Operator__c newOperator                        {get; set;}
	static Aircraft_Type__c airCraftType                {get; set;}
	static Aircraft_Type_Operator__c ao1                {get; set;}
	static Aircraft_Type_Operator__c ao2                {get; set;}
	static Aircraft_Type_Operator__c ao3                {get; set;}
	static Counterparty__c cp                       {get; set;}
	static Lessor__c setupRecord                        {get; set;}
	static NewsArticle__c article1                      {get; set;}
	static LW_Setup__c lwSetup;
	static ServiceSetting__c lwSettings;
	static LeaseWorksSettings__c lwSettings1;
	static NewsArticle__c article;
	static NewsFeedServices.Entries data1;

	static void dataSetup()
	{

		lwSettings = new ServiceSetting__c();
		lwSettings.Name = 'News_Feed_Aviator';
		lwSettings.Token__c       = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwSettings.EndPointUrl__c  = 'http://api.aviator.aero/news_articles/find.json';
		insert lwSettings;

		lwSetup = new LW_Setup__c();
		lwSetup.Name = 'OPERATOR_NAME_FILTER';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'ON';
		insert lwSetup;

		LeaseWareSeededData.SeedLWServiceSetup();

		map<String,Object> mapInOut = new map<String,Object>();
		// Create Operator1
		//Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		mapInOut.put('Operator__c.Name','Garuda Indonesia');
		mapInOut.put('Operator__c.City__c','Faridabad');
		mapInOut.put('Operator__c.Zip_Code__c','121008');
		mapInOut.put('Operator__c.Country__c','Indonesia');
		mapInOut.put('Operator__c.Alias__c','GI');
		mapInOut.put('Operator__c.World_Fleet_Operator_Name__c', 'Garuda');
		mapInOut.put('Operator__c.Lessee_Legal_Name__c','Garuda Ind');
		TestLeaseworkUtil.createOperator(mapInOut);
		operator1 = (Operator__c)mapInOut.get('Operator__c');

		LeaseWareUtils.unsetTriggers('OperatorTrigger');

		// Create Operator2
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		mapInOut.put('Operator__c.Name','Air China');
		mapInOut.put('Operator__c.Location__Latitude__s', 23.45555555);
		mapInOut.put('Operator__c.Location__Longitude__s',73.23233232);
		mapInOut.put('Operator__c.Alias__c','airChina');
		TestLeaseworkUtil.createOperator(mapInOut);
		operator2 = (Operator__c)mapInOut.get('Operator__c');

		LeaseWareUtils.unsetTriggers('OperatorTrigger');
		// Create Operator3
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		mapInOut.put('Operator__c.Name','Jetscape');
		mapInOut.put('Operator__c.Alias__c','Jetscape');
		TestLeaseworkUtil.createOperator(mapInOut);
		operator3 = (Operator__c)mapInOut.get('Operator__c');

		airCraftType = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320', Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56');
		insert airCraftType;

		// Create airCraftType Operators
		mapInOut.put('Aircraft_Type_Operator__c.Aircraft_Type__c',airCraftType.Id);
		mapInOut.put('Aircraft_Type_Operator__c.Operator__c',operator1.Id);
		TestLeaseworkUtil.createAircraftTypeOperator(mapInOut);
		ao1 = (Aircraft_Type_Operator__c)mapInOut.get('Aircraft_Type_Operator__c');

		mapInOut.put('Aircraft_Type_Operator__c.Operator__c',operator2.Id);
		TestLeaseworkUtil.createAircraftTypeOperator(mapInOut);
		ao2 = (Aircraft_Type_Operator__c)mapInOut.get('Aircraft_Type_Operator__c');

		mapInOut.put('Aircraft_Type_Operator__c.Operator__c',operator3.Id);
		TestLeaseworkUtil.createAircraftTypeOperator(mapInOut);
		ao3 = (Aircraft_Type_Operator__c)mapInOut.get('Aircraft_Type_Operator__c');

		LeaseWareUtils.unsetTriggers('CounterpartyTrg');
		cp = new Counterparty__c(Name = 'BBAM',Address_Street__c='1240/29',City__c='Faridabad',Zip_Code__c='121008',Country__c='India',
		                             Fleet_Aircraft_Of_Interest__c='A320',Status__c='Approved');
		//Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		insert cp;

		setupRecord = new Lessor__c(Name='BBAM', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
				Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
				Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
				LeaseWareUtils.clearFromTrigger();
		insert  setupRecord;

		mapInOut.put('Aircraft__c',null);
		TestLeaseworkUtil.createAircraft(mapInOut);
		Aircraft__c aircraft1 = (Aircraft__c)mapInOut.get('Aircraft__c');

		mapInOut.put('Aircraft__c',null);
		mapInOut.put('Aircraft__c.msn','123');
		TestLeaseworkUtil.createAircraft(mapInOut);
		Aircraft__c aircraft2 = (Aircraft__c)mapInOut.get('Aircraft__c');

		mapInOut.put('Lease__c.Aircraft__c',aircraft1.Id);
		mapInOut.put('Lease__c.Operator__c',operator3.Id);
		mapInOut.put('Lease__c',null);
		TestLeaseworkUtil.createLease(mapInOut);
		Lease__c lease = (Lease__c)mapInOut.get('Lease__c');

		newOperator = [Select id, name, Alias__c,World_Fleet_Operator_Name__c from Operator__c where Name like: 'Garuda Indonesia'];
	}

	@isTest
	static void TestCreateArticlesWithRegionCategory()
	{
		datasetup();
		Test.setMock(HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen());

		Test.startTest();
		NewsFeedServices service = new NewsFeedServices();
		String response = service.getNewsArticles(operator1.id,'',lwSettings.EndPointUrl__c,lwSettings.Token__c,1,10,'',
		'', '','World Fleet Name/Name','Asia Pacific','Earnings + Fundings');
		List<NewsFeedServices.NewsArticleWrapper> articles = (List<NewsFeedServices.NewsArticleWrapper>) System.Json.deserialize(response, List<NewsFeedServices.NewsArticleWrapper>.class);
		System.assertEquals(1, articles.size(), 'number of articles returned');
		if(articles.size()>0){System.assertEquals('150710', articles[0].articleId,'selected article id ');}
		Test.stopTest();
	}


	@isTest
	static void TestCreateArticlesWithWFName()
	{
		datasetup();
		Test.setMock(HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen());

		Test.startTest();
		NewsFeedServices service = new NewsFeedServices();
		String response = service.getNewsArticles(operator1.id,'',lwSettings.EndPointUrl__c,lwSettings.Token__c,1,10,'',
		'', '','World Fleet Name/Name','','');
		List<NewsFeedServices.NewsArticleWrapper> articles = (List<NewsFeedServices.NewsArticleWrapper>) System.Json.deserialize(response, List<NewsFeedServices.NewsArticleWrapper>.class);
		System.assertEquals(1, articles.size(), 'number of articles returned');
		if(articles.size()>0){System.assertEquals('150710', articles[0].articleId,'selected article id ');}
		Test.stopTest();
	}

	@isTest
	static void TestCreateArticlesWithNameAndAlias()
	{
		datasetup();
		Test.setMock(HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen());

		Test.startTest();
		NewsFeedServices service = new NewsFeedServices();
		String response = service.getNewsArticles(operator1.id,'',lwSettings.EndPointUrl__c,lwSettings.Token__c,1,10,'',
		'', '','All Names','','');
		List<NewsFeedServices.NewsArticleWrapper> articles = (List<NewsFeedServices.NewsArticleWrapper>) System.Json.deserialize(response, List<NewsFeedServices.NewsArticleWrapper>.class);
		System.assertEquals(3, articles.size(), 'number of articles returned');
		if(articles.size()>0){System.assertEquals('150826', articles[0].articleId,'selected article id ');}
		Test.stopTest();
	}

	@isTest
	static  void TestKeywordSearchForArticles()
	{
		//dataSetup();
		lwSettings = new ServiceSetting__c();
		lwSettings.Name = 'News_Feed_Aviator';
		lwSettings.Token__c       = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwSettings.EndPointUrl__c  = 'http://api.aviator.aero/news_articles/find.json';
		insert lwSettings;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createOperator(mapInOut);
		operator1 = (Operator__c)mapInOut.get('Operator__c');
		mapInOut.put('NewsArticle__c.Operator__c',operator1.Id);
		TestLeaseworkUtil.createNewsArticles(mapInOut);
		article1 = (NewsArticle__c)mapInOut.get('NewsArticle__c');

		Test.setCurrentPage( pageRefOperatorNewsArticle );
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen());

		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		NewsFeedServices service = new NewsFeedServices();
		
		String response = service.getNewsArticles(operator1.id,'',lwSettings.EndPointUrl__c,lwSettings.Token__c,1,10,'financial',
		'', '','World Fleet Name/Name','','');
		List<NewsFeedServices.NewsArticleWrapper> articles = (List<NewsFeedServices.NewsArticleWrapper>) System.Json.deserialize(response, List<NewsFeedServices.NewsArticleWrapper>.class);
		System.assertEquals('150712',articles[0].articleId);
		System.assertEquals(3,articles[0].totalPages);
		Test.stopTest();
	}

	@isTest
	static void TestControllerForArticles()
	{
		Test.setCurrentPage( pageRefOperatorNewsArticle );
		Test.setMock(HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen());
		dataSetup();
		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		NewsComponentCntrl cont = new NewsComponentCntrl();
		cont.recordId = operator1.id;
		Test.startTest();
		cont.getNewFeeds('Aviator',operator1.id,1,10,'', '','','World Fleet Name/Name','','');
		Test.stopTest();
	}

	@isTest
	static void TestCreateArticlesWithOperator()
	{
		dataSetup();
		NewsFeedServices service = new NewsFeedServices();
		Test.setCurrentPage( pageRefOperatorNewsArticle );
		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		Test.startTest();
		NewsFeedServices.NewsArticleWrapper article1 = service.createArticle(setData(), operator1, null, null, null);
		System.assertEquals(true, article1.newsSource.equals('Aviator'));
		Test.stopTest();
	}

	@isTest
	static void TestCreateArticlesWithLessor()
	{
		Test.setCurrentPage( pageRefLessorNewsArticle );
		dataSetup();
		ApexPages.currentPage().getParameters().put('Id', setupRecord.id);
		NewsComponentCntrl cont = new NewsComponentCntrl();
		cont.recordId = setupRecord.id;
		Test.setMock( HttpCalloutMock.class, new TestNewsArticlesMockHttpResGen() );
		Test.startTest();
		cont.getNewFeeds('Aviator',setupRecord.id,1,10,'','', '','World Fleet Name/Name','','');
		Test.stopTest();
	}


	private static NewsFeedServices.Entries setData(){
		data1 = new NewsFeedServices.Entries();
		data1.id =911056;
		data1.source_url_1 ='https://www.reuters.com/article/garuda-indonesia/new-chief-of-indonesias-garuda-urged-to-revamp-work-culture-at-state-carrier-idUSL4N29R1XL';
		data1.Trans_date ='2020-01-23';
		data1.article ='<p><strong> Air India <p><strong>';
		return data1;
	}

	@IsTest
	public static void testHomeServiceForAviator(){

		dataSetup();
		NewsComponentCntrl cont = new NewsComponentCntrl();
		cont.capaToken = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		cont.capaURL = 'https://api.centreforaviation.com';
		cont.showMore =false;
		cont.recordId = operator1.id;
		Test.setMock( HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen() );
		Test.startTest();
		String response = cont.getNewFeeds('Aviator',null,1,10,'', '','','World Fleet Name/Name','','');
		List<NewsComponentCntrl.NewsArticleWrapper>  articles = (List<NewsComponentCntrl.NewsArticleWrapper>) System.Json.deserialize(response, List<NewsComponentCntrl.NewsArticleWrapper>.class);   
		Test.stopTest();
	}



	static testMethod void TestServiceForArticles()
	{
		//dataSetup();
		lwSettings = new ServiceSetting__c();
		lwSettings.Name = 'News_Feed_Aviator';
		lwSettings.Token__c       = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwSettings.EndPointUrl__c  = 'http://api.aviator.aero/news_articles/find.json';
		insert lwSettings;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createOperator(mapInOut);
		operator1 = (Operator__c)mapInOut.get('Operator__c');
		mapInOut.put('NewsArticle__c.Operator__c',operator1.Id);
		TestLeaseworkUtil.createNewsArticles(mapInOut);
		article1 = (NewsArticle__c)mapInOut.get('NewsArticle__c');

		Test.setCurrentPage( pageRefOperatorNewsArticle );
		Test.setMock(HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen());

		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		NewsFeedServices service = new NewsFeedServices();
		Test.startTest();
		service.getNewsArticles(operator1.id,'',lwSettings.EndPointUrl__c,lwSettings.Token__c,1,10,'',
		'', '','World Fleet Name/Name','','');
		Test.stopTest();
	}


	static testMethod void TestServiceForArticlesWithType()
	{
		//dataSetup();
		lwSettings = new ServiceSetting__c();
		lwSettings.Name = 'News_Feed_Aviator';
		lwSettings.Token__c       = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwSettings.EndPointUrl__c  = 'http://api.aviator.aero/news_articles/find.json';
		insert lwSettings;

		Operator__c operatorDummy = new Operator__c(Name = 'JetAirBlu',Alias__c = 'JetB');
		insert operatorDummy;


		Test.setCurrentPage( pageRefOperatorNewsArticle );
		Test.setMock(HttpCalloutMock.class, new TestNewsArtcileOPAliasMockHttpResGen());

		ApexPages.currentPage().getParameters().put('Id', operatorDummy.id);
		NewsFeedServices service = new NewsFeedServices();
		Test.startTest();
		service.getNewsArticles(operatorDummy.id,'V2533,GE90',lwSettings.EndPointUrl__c,lwSettings.Token__c,1,10,'','', '','World Fleet Name/Name','','');
		Test.stopTest();
	}

	static testMethod void TestOperatorPageWOArticles()
	{
		Test.setCurrentPage( pageRefOperatorNewsArticle );
		dataSetup();
		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		NewsComponentCntrl cont = new NewsComponentCntrl();
		cont.recordId = operator1.id;
		Test.setMock( HttpCalloutMock.class, new TestNewsArticlesMockHttpResGen() );
		cont.aviatortoken = null;
		Test.startTest();
		cont.getNewFeeds('Aviator',operator1.id,1,10,'','', '','World Fleet Name/Name','','');
		Test.stopTest();
	}

	static testMethod void TestCPPageWArticles()
	{
		Test.setCurrentPage( pageRefLessorNewsArticle );
		dataSetup();
		ApexPages.currentPage().getParameters().put('Id', cp.id);
		NewsComponentCntrl cont = new NewsComponentCntrl();
		cont.recordId = cp.id;
		Test.setMock( HttpCalloutMock.class, new TestNewsArticlesMockHttpResGen() );
		Test.startTest();
		cont.getNewFeeds('Aviator',cp.id,1,10,'','', '','World Fleet Name/Name','','');
		Test.stopTest();
	}

	static testMethod void TestForCodeCoverage(){
		HttpRequest request1 = new HttpRequest();
		request1.setEndpoint( 'https://aviator.aero/api/news_articles/find.json?key=Please+set+Token+Id&page=1&q=Garuda+Indonesia');
		request1.setMethod( 'GET' );
		request1.setHeader( 'content-type','application/json' );
		request1.setHeader( 'accept','application/json' );
		TestNewsArtcileOPAliasMockHttpResGen mock1 = new TestNewsArtcileOPAliasMockHttpResGen();
		HTTPResponse response1 = mock1.respond(request1);
		HttpRequest request2 = new HttpRequest();
		request2.setEndpoint( 'https://aviator.aero/api/news_articles/find.json?key=Please+set+Token+Id&page=1&q=GI');
		request2.setMethod( 'GET' );
		request2.setHeader( 'content-type','application/json' );
		request2.setHeader( 'accept','application/json' );
		TestNewsArtcileOPAliasMockHttpResGen mock2 = new TestNewsArtcileOPAliasMockHttpResGen();
		HTTPResponse response2 = mock2.respond(request2);
		HttpRequest request3 = new HttpRequest();
		request3.setEndpoint( 'https://aviator.aero/api/news_articles/find.json?key=Please+set+Token+Id&page=1&q=Garuda');
		request3.setMethod( 'GET' );
		request3.setHeader( 'content-type','application/json' );
		request3.setHeader( 'accept','application/json' );
		TestNewsArtcileOPAliasMockHttpResGen mock3 = new TestNewsArtcileOPAliasMockHttpResGen();
		HTTPResponse response3 = mock3.respond(request3);
		HttpRequest request4 = new HttpRequest();
		request4.setEndpoint( 'https://aviator.aero/api/news_articles/find.json?key=Please+set+Token+Id&page=1&q=TestOperator%2Bfinancial');
		request4.setMethod( 'GET' );
		request4.setHeader( 'content-type','application/json' );
		request4.setHeader( 'accept','application/json' );
		TestNewsArtcileOPAliasMockHttpResGen mock4 = new TestNewsArtcileOPAliasMockHttpResGen();
		HTTPResponse response4 = mock4.respond(request4);
	}
}