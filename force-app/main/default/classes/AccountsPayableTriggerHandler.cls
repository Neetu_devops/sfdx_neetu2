public with sharing class AccountsPayableTriggerHandler implements ITrigger {
    private final string triggerBefore = 'AccountsPayableTriggerHandlerBefore';
    private final string triggerAfter = 'AccountsPayableTriggerHandlerAfter';
    public AccountsPayableTriggerHandler() {

    }

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){}
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){}
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){}
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AccountsPayableTriggerHandlerBefore.afterInsert(+)');
        createPayoutLineItem();
        updatePayable();
        system.debug('AccountsPayableTriggerHandlerBefore.afterInsert(-)');
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){}
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(){}
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterUnDelete(){}    
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){}

    /***
     * create payout line item
     * Called from after insert
     * 
     */
    private void createPayoutLineItem(){
        system.debug('createPayoutLineItem(+)');
        set<id> setPayoutIds = new set<id>();
        for(payable_line_item__c ap : (list<payable_line_item__c>)trigger.new){
            setPayoutIds.add(ap.Payout__c);
        }
        list<payout__c> payouts = [select id,name,Payout_Amount__c, company__c from payout__c where id IN :setPayoutIds];
        list<payout_line_item__c> listPayoutLI =  new list<payout_line_item__c>(); 
        for(payout__c po : payouts){
            listPayoutLI.add( new payout_line_item__c(name = po.name+'-1',Payouts__c = po.id,Payout_Line_Amount__c =po.Payout_Amount__c,company__c = po.company__c ));
        }  
        if(!listPayoutLI.isEmpty()){
            try{
                insert listPayoutLI;
            }
            catch(DmlException ex){
                string errorMessage='';
                for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                    // Process exception here
                    errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                }
                system.debug('errorMessage=='+errorMessage);
                LeasewareUtils.createExceptionLog(ex,ex.getMessage(),'payout','insert payout line item', '',true);
            }
        }
      
        system.debug('createPayoutLineItem(-)');
    }

    /************
     * Updates the Payable  Claim if inserted in Approved status
     * Called from After insert
     */
    private void updatePayable(){
        set<id> setPayoutId = new set<id>();
        //map<id,payout__c> approvedSDPos = new  map<id,payout__c> (); 
        for(payable_line_item__c ap:  (list<payable_line_item__c>)trigger.new ){
            setPayoutId.add(ap.payout__c);
        }
        map<id,payout__c> approvedPos = new map<id,payout__c>([select id,name,Status__c,Claim__c,Supplemental_Rent__c,Company__c,Lease__c,Lessee__c,Payables_Type__c,Payout_Amount__c,Payout_Date__c,Payout_Type__c,Security_Deposit__c
                                        from payout__c where id IN :setPayoutId and Status__c = 'Approved']);
        for(payout__c po :approvedPos.values()){
                /*if('Security Deposit Refund'.equals(po.Payables_Type__c)){
                    approvedSDPos.put(po.id,po);
                }  */
        }
        if(approvedPos.size()>0){
            //In case of event claim payables, the below method updates the payable record whose trigger updates the related claim record.
            PayoutTriggerHandler.updatePayableOnApproval(approvedPos);
        }
        /* Commenting this section as, for SD the payouts are not created in approved status : Uncomment if there is such a req.
        if(approvedSDPos.size()>0){            
            try{
                CashSecurityDepositUtil.updateCashSDOnPayoutApproval(approvedSDPos.keySet(),approvedSDPos);
                CashSecurityDepositUtil.createSdHistoryPayout(approvedSDPos.keySet());
            }
            catch(DmlException ex){
                System.debug(ex);
                string errorMessage='';
                for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                    // Process exception here
                    errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                }
                system.debug('errorMessage=='+errorMessage);
                LeasewareUtils.createExceptionLog(ex,ex.getMessage(),'payable','update payable line item', '',true);
               
            }
            
        }*/

    }
    

}