global without sharing class CovidFeedService implements Schedulable{

    /*
        CovidFeedService.execute(null);

        Post deployment Steps:
        1. Add remote setting.
            Covid1_rapidapi	 ;  https://covid-19-tracking.p.rapidapi.com
            Covid2_rapidapi	;  https://covid-193.p.rapidapi.com
            Covid3_rapidapi	;  ;https://coronavirus-monitor.p.rapidapi.com
        2. Add Panedemic as related list of Country.
        3. Schedule class CovidFeedService every 4 hours.

        todo:
            Testcase
    */

    global static void execute(SchedulableContext sc) {
        callCovidAPIFuture();
    }
  
    @future (callout=true)
    public static void callCovidAPIFuture(){
        CovidFeedService service = new CovidFeedService();
        service.callCovidAPI();
    }
    

    private final integer REQUESTTIMEOUT = 60000;    //max request timeout set as 60 seconds
	private map<string,Pandemic_Info__c> mapRecsToAdd = new map<string,Pandemic_Info__c>();
    private map<string, id> mapCountryNameToId;
    private map<string, string> mapCountryMappings;
    private map<id, Country__c> mapCountries;


    public void callCovidAPI(){
        //calling 3 apis, to make sure we get data atleast from one incase of failure.
        callCovidAPIThird();
        callCovidAPISecond();
        callCovidAPIFirst();
        upsertRecordsToPandemic();
    }

    @testvisible
    private void upsertRecordsToPandemic(){
        system.debug('Number of records ='+mapRecsToAdd.size());
        if(!mapRecsToAdd.isEmpty()){
            upsert mapRecsToAdd.values() Name;
        }        
    }


    @testvisible
    private void callCovidAPIFirst(){
    
        HttpRequest request = new HttpRequest();
        Http http = new Http();
        HttpResponse response;
        
        String theUrl = 'https://covid-19-tracking.p.rapidapi.com/v1';
        request.setHeader( 'x-rapidapi-host', 'covid-19-tracking.p.rapidapi.com');
        request.setHeader( 'x-rapidapi-key', '8e6fd0de0emsh4d609974cfd0414p1e4a7bjsn6c51e2c5c00f');
        request.setEndpoint(theUrl);
        request.setMethod( 'GET' );
        request.setTimeout( REQUESTTIMEOUT );
        request.setHeader( 'content-type','application/json' );   
        request.setHeader( 'accept','application/json' );
        try{
            response = http.send( request );
system.debug('Response1 ' + response.getBody());            
            list<Object> mapCovidDataAll =(list<Object>) System.JSON.deserializeUntyped(response.getBody());
            for(Object curObj : mapCovidDataAll){
				Map<String, Object> mapCovidData =(Map<String, Object>)curObj;
                processCovidDataFirst(mapCovidData);
            }
		}catch( System.CalloutException exCall ){
            system.debug('exception in HTTPResponse call=='+exCall.getMessage()); 
        }catch(exception ex){
            system.debug('exception in HTTPResponse call=='+ ex.getStackTraceString() + ex.getLineNumber()+ex.getMessage()) ; 
            leasewareutils.createExceptionLog(ex, 'Exception while covid api call callCovidAPIAll().', null, null, 'callCovidAPIAll', true);
        }
    }

    @testvisible
    private void callCovidAPISecond(){
        //https://rapidapi.com/api-sports/api/covid-193?endpoint=apiendpoint_2feca6f0-0f58-40b7-9196-98c45c7d5083
        HttpRequest request = new HttpRequest();
        Http http = new Http();
        HttpResponse response;
        
        String theUrl = 'https://covid-193.p.rapidapi.com/statistics';
        request.setHeader( 'x-rapidapi-host', 'covid-193.p.rapidapi.com');
        request.setHeader( 'x-rapidapi-key', '8e6fd0de0emsh4d609974cfd0414p1e4a7bjsn6c51e2c5c00f');
        request.setEndpoint(theUrl);
        request.setMethod( 'GET' );
        request.setTimeout( REQUESTTIMEOUT );
        request.setHeader( 'content-type','application/json' );   
        request.setHeader( 'accept','application/json' );
        try{
            response = http.send( request );
system.debug('Response2 ' + response.getBody());
            map<String, Object>  mapResponse = (map<String, Object>)System.JSON.deserializeUntyped(response.getBody());
            list<Object> listCovidDataAll = (list<Object>)mapResponse.get('response');
            for(Object curObj : listCovidDataAll){
				Map<String, Object> mapCovidData =(Map<String, Object>)curObj;
                processCovidDataSecond(mapCovidData);
            }
		}catch( System.CalloutException exCall ){
            system.debug('exception in HTTPResponse call=='+exCall.getMessage()); 
        }catch(exception ex){
            system.debug('exception in HTTPResponse call=='+ ex.getStackTraceString() + ex.getLineNumber()+ex.getMessage()) ; 
            leasewareutils.createExceptionLog(ex, 'Exception while covid api call callCovidAPIAll().', null, null, 'callCovidAPIAll', true);
        }
    }

    @testvisible
    private void callCovidAPIThird(){
        // https://rapidapi.com/astsiatsko/api/coronavirus-monitor?endpoint=apiendpoint_745c45af-47e5-4b99-b962-9f77e164c054
        HttpRequest request = new HttpRequest();
        Http http = new Http();
        HttpResponse response;
        
        String theUrl = 'https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php';
        request.setHeader( 'x-rapidapi-host', 'coronavirus-monitor.p.rapidapi.com');
        request.setHeader( 'x-rapidapi-key', 'ae942d13e5msh7d5bf58c0e98732p17ef65jsn04a560009d93');
        request.setEndpoint(theUrl);
        request.setMethod( 'GET' );
        request.setTimeout( REQUESTTIMEOUT );
        request.setHeader( 'content-type','application/json' );   
        request.setHeader( 'accept','application/json' );
        try{
            response = http.send( request );
            system.debug('Response3 ' + response.getBody());
            map<String, Object>  mapResponse = (map<String, Object>)System.JSON.deserializeUntyped(response.getBody());
            list<Object> listCovidDataAll = (list<Object>)mapResponse.get('countries_stat');
            system.debug('all keys='+mapResponse.keySet());

            Date dateAsOf = getDateData(mapResponse,'statistic_taken_at');

            for(Object curObj : listCovidDataAll){
				Map<String, Object> mapCovidData =(Map<String, Object>)curObj;
                processCovidDataThird(mapCovidData,dateAsOf);
            }
		}catch( System.CalloutException exCall ){
            system.debug('exception in HTTPResponse call=='+exCall.getMessage()); 
        }catch(exception ex){
            system.debug('exception in HTTPResponse call=='+ ex.getStackTraceString() + ex.getLineNumber()+ex.getMessage()) ; 
            leasewareutils.createExceptionLog(ex, 'Exception while covid api call callCovidAPIAll().', null, null, 'callCovidAPIAll', true);
        }
    }

    private boolean isNewCovidInfo(Pandemic_Info__c newRec,Decimal confirmedCases,Decimal newCases){
        if(confirmedCases < leasewareutils.ZeroIfNull(newRec.Confirmed_Cases__c)) { return false; }
        else if(confirmedCases > leasewareutils.ZeroIfNull(newRec.Confirmed_Cases__c)) { return true; }
        else if(newCases > leasewareutils.ZeroIfNull(newRec.New_Cases__c)) { return true; }
        return false;
    }
    

    private void processCovidDataFirst(Map<String, Object> mapCovidData){
        Pandemic_Info__c newRec;
        string countryNameString;
        string keyString;
        string countryId;
        Decimal confirmedCases;
        Decimal newCases;
        date dateAsOf;
        map<string, Object> mapCases;

        dateAsOf = getDateData(mapCovidData,'Last Update');

        if(mapCovidData.get('Country_text')==null) {return;}
        
        countryNameString = (String)mapCovidData.get('Country_text');
        countryId =  getCountryId(countryNameString);
        if(countryId==null) {return;} // no country mapping found;

        countryNameString = getCountryName(countryId);

        //1. Get confirmed cases:
        confirmedCases = getDecimalData(mapCovidData,'Total Cases_text');
        newCases = getDecimalData(mapCovidData,'New Cases_text'); 
        //2. find if any record Exist
        keyString = (countryNameString + ' - ' + leasewareutils.getDatetoString(dateAsOf,'YYYY-MM-DD'));
        if(mapRecsToAdd.containsKey(keyString)){
            newRec = mapRecsToAdd.get(keyString);
            //3. if confirmed cases are more than existing, then override.
            if( !isNewCovidInfo(newRec,confirmedCases,newCases) ) {return; } // no need to process further. The fetched record might be stale.
        }else{
            newRec = new Pandemic_Info__c(name=keyString ,Data_As_Of__c=dateAsOf,Country__c=countryId);
        }

        
        //4. If everyting is good, set all the fields.
        //covid cases
        newRec.Confirmed_Cases__c = confirmedCases;
        newRec.Active_Cases__c = getFinalDecimal(newRec.Active_Cases__c,getDecimalData(mapCovidData,'Active Cases_text'));
        newRec.New_Cases__c = newCases; //getDecimalData(mapCovidData,'New Cases_text');
        //newRec.Total_Cases_1M_Population__c = getDecimalData(mapCovidData,'total_cases_per_1m_population');
        newRec.Recovered_Cases__c = getFinalDecimal(newRec.Recovered_Cases__c,getDecimalData(mapCovidData,'Total Recovered_text'));
        //death
        newRec.Total_Deaths__c = getFinalDecimal(newRec.Total_Deaths__c,getDecimalData(mapCovidData,'Total Deaths_text'));
        newRec.New_Deaths__c = getFinalDecimal(newRec.New_Deaths__c,getDecimalData(mapCovidData,'New Deaths_text'));
        //newRec.Deaths_1M_Population__c = getDecimalData(mapCovidData,'deaths_per_1m_population');

        //5. Add to the list
        mapRecsToAdd.put(newRec.name,newRec);
    }

    private void processCovidDataSecond(Map<String, Object> mapCovidData){

        Pandemic_Info__c newRec;
        string countryNameString;
        string keyString;
        string countryId;
        Decimal confirmedCases;
        Decimal newCases;
        date dateAsOf;
        map<string, Object> mapCases;

        dateAsOf = getDateData(mapCovidData,'day');

        if(mapCovidData.get('country')==null) {return;}
        
        countryNameString = (String)mapCovidData.get('country');
        countryId =  getCountryId(countryNameString);
        if(countryId==null) {return;} // no country mapping found;

        countryNameString = getCountryName(countryId);
 
        //1. Get confirmed cases:
        mapCases = (map<string, Object>)mapCovidData.get('cases');	
        confirmedCases = getDecimalData(mapCases,'total');
        newCases = getDecimalData(mapCases,'new');

        //2. find if any record Exist
        keyString = (countryNameString + ' - ' + leasewareutils.getDatetoString(dateAsOf,'YYYY-MM-DD'));
        if(mapRecsToAdd.containsKey(keyString)){
            newRec = mapRecsToAdd.get(keyString);
            //3. if confirmed cases are more than existing, then override.
            if( !isNewCovidInfo(newRec,confirmedCases,newCases) ) {return; }  // no need to process further. The fetched record might be stale.
        }else{
            newRec = new Pandemic_Info__c(name=keyString ,Data_As_Of__c=dateAsOf,Country__c=countryId);
        }

        
        //4. If everyting is good, set all the fields.
        // Covid cases
        mapCases = (map<string, Object>)mapCovidData.get('cases');		
        newRec.Active_Cases__c = getFinalDecimal(newRec.Active_Cases__c,getDecimalData(mapCases,'active'));
        newRec.New_Cases__c = newCases; //getDecimalData(mapCases,'new');
        newRec.Recovered_Cases__c = getFinalDecimal(newRec.Recovered_Cases__c,getDecimalData(mapCases,'recovered'));
        newRec.Total_Cases_1M_Population__c = getFinalDecimal(newRec.Total_Cases_1M_Population__c,getDecimalData(mapCases,'1M_pop'));
        newRec.Confirmed_Cases__c = getFinalDecimal(newRec.Confirmed_Cases__c,getDecimalData(mapCases,'total'));
        
        // death cases
        mapCases = (map<string, Object>)mapCovidData.get('deaths');
        newRec.Total_Deaths__c = getFinalDecimal(newRec.Total_Deaths__c,getDecimalData(mapCases,'total'));
        newRec.New_Deaths__c = getFinalDecimal(newRec.New_Deaths__c,getDecimalData(mapCases,'new'));
        newRec.Deaths_1M_Population__c = getFinalDecimal(newRec.Deaths_1M_Population__c,getDecimalData(mapCases,'1M_pop'));


        //5. Add to the list
        mapRecsToAdd.put(newRec.name,newRec);

    }
        
    private void processCovidDataThird(Map<String, Object> mapCovidData,date dateAsOf){

            Pandemic_Info__c newRec;
            string countryNameString;
            string keyString;
            string countryId;
            Decimal confirmedCases;
            Decimal newCases;
            if(mapCovidData.get('country_name')==null) {return;}
            
            countryNameString = (String)mapCovidData.get('country_name');
            countryId =  getCountryId(countryNameString);
            if(countryId==null) {return;} // no country mapping found;

            countryNameString = getCountryName(countryId);

            //1. Get confirmed cases:
            confirmedCases = getDecimalData(mapCovidData,'cases');
            newCases = getDecimalData(mapCovidData,'new_cases');

            //2. find if any record Exist
            keyString = (countryNameString + ' - ' + leasewareutils.getDatetoString(dateAsOf,'YYYY-MM-DD'));
            if(mapRecsToAdd.containsKey(keyString)){
                newRec = mapRecsToAdd.get(keyString);
                //3. if confirmed cases are more than existing, then override.
                if( !isNewCovidInfo(newRec,confirmedCases,newCases) ) {return; }  // no need to process further. The fetched record might be stale.
            }else{
                newRec = new Pandemic_Info__c(name=keyString ,Data_As_Of__c=dateAsOf,Country__c=countryId);
            }

            
            //4. If everyting is good, set all the fields.
            //covid cases
            newRec.Confirmed_Cases__c = confirmedCases;
            newRec.Active_Cases__c = getFinalDecimal(newRec.Active_Cases__c,getDecimalData(mapCovidData,'active_cases'));
            newRec.New_Cases__c = newCases;//getDecimalData(mapCovidData,'new_cases');
            newRec.Total_Cases_1M_Population__c = getFinalDecimal(newRec.Total_Cases_1M_Population__c,getDecimalData(mapCovidData,'total_cases_per_1m_population'));
            newRec.Recovered_Cases__c = getFinalDecimal(newRec.Recovered_Cases__c,getDecimalData(mapCovidData,'total_recovered'));
            //death
            newRec.Total_Deaths__c =  getFinalDecimal(newRec.Total_Deaths__c,getDecimalData(mapCovidData,'deaths'));
            newRec.New_Deaths__c =  getFinalDecimal(newRec.New_Deaths__c,getDecimalData(mapCovidData,'new_deaths'));
            newRec.Deaths_1M_Population__c =  getFinalDecimal(newRec.Deaths_1M_Population__c,getDecimalData(mapCovidData,'deaths_per_1m_population'));

            //5. Add to the list
            mapRecsToAdd.put(newRec.name,newRec);
    }

    private Decimal getFinalDecimal(Decimal oldValue,Decimal newValue){
        return newValue>0?newValue:oldValue;
    }

    private Decimal getDecimalData(Map<String, Object> mapCovidData,string getkey){
        try{
            if(mapCovidData.containsKey(getkey) && mapCovidData.get(getkey)!=null) { return Decimal.valueOf(((String)mapCovidData.get(getkey)).remove(','));}
        }catch(System.TypeException e){
            //system.debug('TypeException:' + e + '==' + getkey + '=='  + mapCovidData.get(getkey));
            if(e.getMessage().contains('Invalid conversion from runtime type Integer to String')){
                try{return Decimal.valueOf((Integer)mapCovidData.get(getkey));
                }catch(System.TypeException e2){
                    system.debug('second time TypeException:' + e2 );
                }
            }else{
                system.debug('Some other exception TypeException:' + e + '==' + getkey + '=='  + mapCovidData.get(getkey));
            }
        }
        return 0;
    }

    private Date getDateData(Map<String, Object> mapCovidData,string getkey){
        try{
            if(mapCovidData.containsKey(getkey) && mapCovidData.get(getkey)!=null) { return date.valueOf(((String)mapCovidData.get(getkey)).remove(','));}
        }catch(System.TypeException e){
            system.debug('TypeException:' + e + '==' + getkey + '=='  + mapCovidData.get(getkey));
        }
        return date.today().addDays(-1);
    }

    private id getCountryId(string strCountryName){

        if(strCountryName==null) {return null;}
        if(mapCountryNameToId == null || mapCountryMappings == null){
            mapCountryNameToId = new map<string, id>();
            mapCountryMappings = new map<string, string>();
            if(mapCountries==null) {mapCountries = new map<id, Country__c>([select id, Name from Country__c]);} //Id to Name

            for(Country__c curCtry: mapCountries.values()){
                mapCountryNameToId.put(curCtry.Name, curCtry.Id);
                mapCountryMappings.put(curCtry.Name, curCtry.Name); //Include all country names in the map.
            }

            for(Mapping__c curMap : [select id, Name, Map_To__c from Mapping__c where IsActive__c = true]){
                mapCountryMappings.put(curMap.Name, curMap.Map_To__c);//Add all mapping records.
            }
        }
        return mapCountryNameToId.get(mapCountryMappings.get(strCountryName));
    }

    private string getCountryName(id countryId){
        if(countryId==null) {return 'Unknown';}
        if(mapCountries==null) {mapCountries = new map<id, Country__c>([select id, Name from Country__c]);}
        return mapCountries.get(countryId).Name;
    }
}