/**
*******************************************************************************
* Class: TestImageUploadController
* @author Created by AKG, Leaseworks 01/11/2021
* @version 1.0
* ----------------------------------------------------------------------------------
* @description Purpose/Methods:
*  - call domain business logic to test ImageUploadController
*
*
*/
@isTest
public class TestImageUploadController {
    /**
    * @description save image as a Document when there is no record Id
    *
    * @return null
    */
    @isTest static void saveChosenImageFileTest() {
        Account account = new Account(Name = 'Test Account');
        insert account;
        
        String selectedImage = '8PGVBqcP37uqcAAAAASUVORK5CYII=';
		Test.startTest();        
        String result = ImageUploadController.saveChosenImage(selectedImage, 'TestImage.jpg', account.Id, true, 'Account', 'Website');
        Test.stopTest(); 
        System.assertEquals('Image Save Successfully', result);
    }
    
    
    /**
    * @description ImageSizeOptions list from custom metadata
    *
    * @return null
    */
    @isTest static void getImageOptionsTest() {
        Test.startTest();        
        List<ImageSizeOptions__mdt> resultList = ImageUploadController.getImageOptions();
        Test.stopTest(); 
        System.assertEquals(true, resultList.size() > 0);
    }
}