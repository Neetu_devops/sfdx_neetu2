public class LLPCatSumTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'LLPCatSumTriggerHandlerBefore';
    private final string triggerAfter = 'LLPCatSumTriggerHandlerAfter';
    
    
    public LLPCatSumTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
     
    private void setSOQLStaticMAPOrList(){

    }     
     
    public void bulkBefore()
    {
        setSOQLStaticMAPOrList();
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('LLPCatSumTriggerHandler.beforeInsert(+)');
        
		calculateCostPerCycle();
        system.debug('LLPCatSumTriggerHandler.beforeInsert(+)');      
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('LLPCatSumTriggerHandler.beforeUpdate(+)');
        calculateCostPerCycle();
        system.debug('LLPCatSumTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        system.debug('LLPCatSumTriggerHandler.beforeDelete(+)');
    
	
		
		system.debug('LLPCatSumTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LLPCatSumTriggerHandler.afterInsert(+)');
        
        system.debug('LLPCatSumTriggerHandler.afterInsert(-)');       
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LLPCatSumTriggerHandler.afterUpdate(+)');
        updateMRRates();
		
        system.debug('LLPCatSumTriggerHandler.afterUpdate(-)');       
    }
     
    public void afterDelete()
    {
        system.debug('LLPCatSumTriggerHandler.afterDelete(+)');
        
        
        system.debug('LLPCatSumTriggerHandler.afterDelete(-)');       
    }

    public void afterUnDelete()
    {
        system.debug('LLPCatSumTriggerHandler.afterUnDelete(+)');
        

        
        system.debug('LLPCatSumTriggerHandler.afterUnDelete(-)');         
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	//before insert update
 	private void calculateCostPerCycle(){
 		LLP_Catalog_Summary__c[] listNew = (list<LLP_Catalog_Summary__c>)trigger.new ;
 		set<string> setEngDBIds = new set<string>();
 		for(LLP_Catalog_Summary__c curRec:listNew){
 			setEngDBIds.add(currec.Engine_Template__c);
 		}

		map<String,AggregateResult> mapLLPCatl = new map<String,AggregateResult>();
		list<AggregateResult> agrResList = [select LLP_Template__r.Engine_Template__c DBID,name YR,sum(Life_Limit_F__c) TotCyc , Sum(Catalog_Price__c) TotCost from LLP_Catalog__c 
												where LLP_Template__r.Engine_Template__c = :setEngDBIds
												group by LLP_Template__r.Engine_Template__c,Name ];
		string strYear,StrEngId;
        for ( AggregateResult cRec : agrResList ) {
        	strYear = (String)cRec.get('YR');
        	StrEngId = (String)cRec.get('DBID');
        	mapLLPCatl.put(StrEngId + strYear,cRec);
        }			

		AggregateResult tempAr;
 		for(LLP_Catalog_Summary__c curRec:listNew){
 			if(mapLLPCatl.containsKey(curRec.Engine_Template__c + curRec.name)){
 				tempAr = mapLLPCatl.get(curRec.Engine_Template__c + curRec.name);
 				curRec.Total_Cost__c = (Decimal)tempAr.get('TotCost');
 				curRec.Total_Cycle__c = (Decimal)tempAr.get('TotCyc');
 			}
 		} 		
 		
 	}

	//Only after update
 	private void updateMRRates(){
 		LLP_Catalog_Summary__c[] listNew = (list<LLP_Catalog_Summary__c>)trigger.new ;
 		set<string> setEngDBIds = new set<string>();
 		
 		LLP_Catalog_Summary__c oldrec;
 		set<string> setLLPCatSum = new set<string>();
 		for(LLP_Catalog_Summary__c curRec:listNew){
 			oldrec = (LLP_Catalog_Summary__c)trigger.oldmap.get(curRec.Id);
 			if(oldrec.status__c !=curRec.status__c  && curRec.status__c=='Approved'){
 				setEngDBIds.add(currec.Engine_Template__c);
 				setLLPCatSum.add(curRec.Engine_Template__c +curRec.Name);
 			}
 		}

		list<MR_Rate__c> updateMRrateList = new list<MR_Rate__c>();
    	list<Assembly_MR_Rate__c> listMRrate = [Select Lease__c,id,Annual_Escalation__c,
                                                Ratio_Table__c,Rounding_Method_On_Esc_MR_Rate__c,
                                               Rounding_Method_on_Interpolated_MR_Rate__c,Rounding_Decimals_Interpolated_MR_Rate__c,
                                               Rounding_Decimals_Escalated_MR_Rate__c,Escalation_Type__c,
                                                Engine_Template__c
										,Esc_Periodicity_Mos__c
    								   ,(select id ,name,Esc_Factor__c,Esc_Per__c,MR_Rate_Start_Date__c ,MR_Rate_End_Date__c,Y_Hidden_LeaseId__c,MR_Rate__c,Override__c   
    		                           	from MR_Rates__r order by MR_Rate_Start_Date__c desc)
    		                           from Assembly_MR_Rate__c	
    		                           where Engine_Template__c in :setEngDBIds
    		                           and Escalation_Type__c = 'LLP Catalog'];
    	Date firstDate,SeconDate;

		set<id> setLeaseIds = new set<id>();
		for(Assembly_MR_Rate__c curRec:listMRrate){
			setLeaseIds.add(curRec.Lease__c);
		}    	
    	
    	set<string> setFindURS = new set<string>();
        for(Utilization_Report__c curURRec:[select id,Y_hidden_Lease__c,Month_Ending__c from Utilization_Report__c where Y_hidden_Lease__c = :setLeaseIds]){
        		setFindURS.add(curURRec.Y_hidden_Lease__c + leasewareutils.getDatetoString(curURRec.Month_Ending__c,'MMMYYYY') );
        }    

		map<string,map<String,Decimal>> mapLLPCatl = new map<string,map<String,Decimal>>();
		list<AggregateResult> agrResList = [select Engine_Template__c DBID,name YR,sum(Total_Cycle__c) TotCyc , Sum(Total_Cost__c) TotCost 
												from LLP_Catalog_Summary__c 
												where Engine_Template__c = :setEngDBIds and Status__c = 'Approved'
												group by Engine_Template__c,Name ];
		string strYear,EngDB;
		Decimal TotCycle,TotCost;
		map<String,Decimal> tempMap;
        for ( AggregateResult cRec : agrResList ) {
        	EngDB = (String)cRec.get('DBID');
        	strYear = (String)cRec.get('YR');
        	TotCycle = (Decimal)cRec.get('TotCyc');
        	TotCost = (Decimal)cRec.get('TotCost');
        	if(TotCycle==0 || TotCost==0) continue;
        	
        	if(mapLLPCatl.containsKey(EngDB)){
        		tempMap = mapLLPCatl.get(EngDB);
        	}else{
        		tempMap =new map<String,Decimal>();
        	}
        	tempMap.put(strYear,TotCost/TotCycle);
        	mapLLPCatl.put(EngDB,tempMap);
        }        	
        	
        //reelease heap memory
        setLeaseIds = null;
        setEngDBIds=null;
        	
        string strKeyCat;
        LLP_Catalog_Summary__c tempLLPCatrec;
        Decimal newEscRate,EscFactor;	
		Integer EscPeriodicity = 12;	                           		
    	for(Assembly_MR_Rate__c curRec:listMRrate){
            EscPeriodicity = 12;    
            if(curRec.Esc_Periodicity_Mos__c!=null ) EscPeriodicity = curRec.Esc_Periodicity_Mos__c.intValue();

    		newEscRate = null;
    		EscFactor =1.0;
			system.debug('Ratio Table----'+ curRec.Ratio_Table__c);
            System.RoundingMode rModeDefault = RoundingMode.HALF_UP;
            
         // System.RoundingMode rModeForInterpolated= curRec.Rounding_Method_on_Interpolated_MR_Rate__c==null?rModeDefault:LeasewareUtils.getRoundingMode(curRec.Rounding_Method_on_Interpolated_MR_Rate__c);
            System.RoundingMode rMode = curRec.Rounding_Method_On_Esc_MR_Rate__c==null?rModeDefault:LeasewareUtils.getRoundingMode(curRec.Rounding_Method_On_Esc_MR_Rate__c);
            
            
            /* Here rndValue is not being used anywhere so commenting ******/ 
	    	/*Integer rndValue,rndValueInterpolated ;
            rndValue = Integer.valueOf(curRec.Rounding_Decimals_Escalated_MR_Rate__c == null ? 
                      '2' :curRec.Rounding_Decimals_Escalated_MR_Rate__c) ;

			rndValueInterpolated  = Integer.valueOf(curRec.Rounding_Decimals_Interpolated_MR_Rate__c == null ?
								   '2' : curRec.Rounding_Decimals_Interpolated_MR_Rate__c );
		    /**************Comment End******** */
	    	 		
    		for(MR_Rate__c curChildRec :curRec.MR_Rates__r ){// in descending order
    			if(curChildRec.Override__c) continue; // if manual override , then do not update.
    			firstDate = curChildRec.MR_Rate_Start_Date__c;SeconDate = curChildRec.MR_Rate_End_Date__c;
				if(checkAnyURsUsages( curChildRec,setFindURS,firstDate,SeconDate)){
					system.debug('UR Usages found , hence no update for this=' + firstDate.format() + '=='+ SeconDate.format());  
					break;// if any usages no need to update any records
				}
				
				strKeyCat = curRec.Engine_Template__c + String.ValueOf(curChildRec.MR_Rate_Start_Date__c.Year());	    			
    			if(setLLPCatSum.contains(strKeyCat) || newEscRate!=null){
					//'LLP Catalog')
					system.debug('found record =' + strKeyCat); 
					EscFactor = leasewareutils.getEscFactor(curChildRec.MR_Rate_Start_Date__c,mapLLPCatl.get(curRec.Engine_Template__c),EscPeriodicity);
					if(EscFactor==null) continue;
    				newEscRate = (curChildRec.MR_Rate__c * EscFactor).setScale(2,rMode);
    				curChildRec.MR_Rate__c = newEscRate;
    				curChildRec.Esc_Factor__c = EscFactor;
    				curChildRec.Esc_Per__c = ((EscFactor - 1.0)*100).setScale(2,rMode);
    				updateMRrateList.add(curChildRec);
    			}else {system.debug('Not found record =' + strKeyCat); }
    		}
    		
    	}				
    	system.debug('No of records=' + updateMRrateList.size());   
    	LeaseWareUtils.TriggerDisabledFlag=true;//trigger disabled here,  else need to set LeaseWareUtils.isFromTrigger('AssemblyMRRateTriggerHandlerAfter')
    	if(updateMRrateList.size()>0) update updateMRrateList;
		LeaseWareUtils.TriggerDisabledFlag=false; 		
 	}




	private boolean checkAnyURsUsages(MR_Rate__c curRec,set<string> setFindURS,Date firstDate,Date secondDate){
		if(curRec.Utilization__c){
			return true;
		}		
		for(Date dt_St=firstDate.toStartOfMonth();dt_St<=secondDate;dt_St=dt_St.addMonths(1)){
			if(setFindURS.contains(curRec.Y_Hidden_LeaseId__c + leasewareutils.getDatetoString(dt_St,'MMMYYYY') ) ){
				return true;
			}						
		}
		return false;						
	}
	
}