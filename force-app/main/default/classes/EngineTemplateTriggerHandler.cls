public class EngineTemplateTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'EngineTemplateTriggerHandlerBefore';
    private final string triggerAfter = 'EngineTemplateTriggerHandlerAfter';
    
    private static list<Engine_Template__c> listExistingEngTmpl;
    public EngineTemplateTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
     
    private void setSOQLStaticMAPOrList(){
      Engine_Template__c[] listNew = (list<Engine_Template__c>)(trigger.isDelete?trigger.old:trigger.new) ;
      
      if(listExistingEngTmpl==null){
        listExistingEngTmpl = [select id,Model__c,Variant__c,Thrust_LP__c,Y_Hidden_Thrust_Name__c  
         from Engine_Template__c
         where id not in :listNew];
      }
    } 
    public void bulkBefore()
    {
        setSOQLStaticMAPOrList();
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('EngineTemplateTriggerHandler.beforeInsert(+)');
        checkDuplicateTemplate();

        system.debug('EngineTemplateTriggerHandler.beforeInsert(+)');      
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('EngineTemplateTriggerHandler.beforeUpdate(+)');
        
        
        checkDuplicateTemplate();
        system.debug('EngineTemplateTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
		//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('EngineTemplateTriggerHandler.beforeDelete(+)');
    
		Engine_Template__c[] listNew = (list<Engine_Template__c>)trigger.old ;
		Constituent_Assembly__c[] CAList = [select id,Engine_Template_CA__c
						from Constituent_Assembly__c
						where Engine_Template_CA__c in :listNew
						and type__c like 'Engine%'
						];
						
		
		Engine_Template__c OldEnftemplrec;
		for(Constituent_Assembly__c curCA:CAList){
			OldEnftemplrec = (Engine_Template__c)trigger.oldMap.get(curCA.Engine_Template_CA__c);
			if(OldEnftemplrec!=null){
				OldEnftemplrec.addError('This Engine DB is already associated with Engine.You are not allowed to delete this record.');
			}

		}

		
        system.debug('EngineTemplateTriggerHandler.beforeDelete(-)');        

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EngineTemplateTriggerHandler.afterInsert(+)');
        createChildIfClone();
        system.debug('EngineTemplateTriggerHandler.afterInsert(-)');       
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EngineTemplateTriggerHandler.afterUpdate(+)');
        
   	 	updateRelatedEngines();
   	 	updateLLPSummary();
      system.debug('EngineTemplateTriggerHandler.afterUpdate(-)');       
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EngineTemplateTriggerHandler.afterDelete(+)');
        
        
        system.debug('EngineTemplateTriggerHandler.afterDelete(-)');       
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EngineTemplateTriggerHandler.afterUnDelete(+)');
        

        
        system.debug('EngineTemplateTriggerHandler.afterUnDelete(-)');         
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

  // afterInsert only
  private void createChildIfClone(){

    Engine_Template__c[] newCAs = (list<Engine_Template__c>)trigger.new;
        list<LLP_Template__c> listNewAssetHist = new list<LLP_Template__c>();
    map<ID,id> IdsNeedToCloneMap = new map<ID,id>();
    set<ID> SourceIdsSet = new set<ID>();
    for(Engine_Template__c curRec: newCAs){
      if(curRec.isClone()){
        SourceIdsSet.add(curRec.getCloneSourceId());
        IdsNeedToCloneMap.put(curRec.getCloneSourceId(),curRec.Id);
      }
    }
    
    if(!IdsNeedToCloneMap.isEmpty()){
      Engine_Template__c[] ParentCloneList = [select id 
                      ,(select Engine_Template__c,name,Date_Of_Load__c
                        ,Lead_Time__c,Life_Limit_Cycles__c,Module_N__c,Part_Name__c,Part_Number__c
                        ,Replacement_Cost__c,Description__c,Disk_Stage__c,Replacement_Cost_Reset_Date__c
                       from LLP_Template__r
                      )
                     from Engine_Template__c
                     where id in :SourceIdsSet ];  

      list<LLP_Template__c> listInsertChilds = new list<LLP_Template__c>();
      for(Engine_Template__c curParent:ParentCloneList){
        for(LLP_Template__c curChild:curParent.LLP_Template__r){
            LLP_Template__c cloneChild = curChild.clone(false, true, false, false);
            cloneChild.Engine_Template__c = IdsNeedToCloneMap.get(curParent.Id);
            listInsertChilds.add(cloneChild);
        }
      }
      if(!listInsertChilds.isEmpty()) insert listInsertChilds;
      system.debug('listInsertChilds='+listInsertChilds);
    }    
  }  


  // before insert,before update
   private void checkDuplicateTemplate(){
     Engine_Template__c[] listNew = (list<Engine_Template__c>)trigger.new ;
     
     set<string> setEngTemplateKey = new set<string>();
     
     string keyStr;
     for(Engine_Template__c curEngTmpl: listExistingEngTmpl){
       keyStr = leasewareUtils.blankIfNull(curEngTmpl.Model__c) 
           +'-'+ leasewareUtils.blankIfNull(curEngTmpl.Variant__c) + '-'+ leasewareUtils.blankIfNull(curEngTmpl.Thrust_LP__c);
       setEngTemplateKey.add(keyStr);
     }
         
     for(Engine_Template__c curEngTmpl: listNew){
       keyStr = leasewareUtils.blankIfNull(curEngTmpl.Model__c) 
           +'-'+ leasewareUtils.blankIfNull(curEngTmpl.Variant__c) + '-'+ leasewareUtils.blankIfNull(curEngTmpl.Thrust_LP__c);
           
       if(setEngTemplateKey.contains(keyStr)){ curEngTmpl.addError('Duplicate record found for '
           +'Model {' + curEngTmpl.Model__c + (curEngTmpl.Variant__c==null?'':'}, Variant {' + curEngTmpl.Variant__c)   + '}, Thrust {' + curEngTmpl.Y_Hidden_Thrust_Name__c +'}.');
       }
       setEngTemplateKey.add(keyStr);
    
     }
     
     
   }

	// after update
	private void updateLLPSummary(){
		Engine_Template__c[] listNew = (list<Engine_Template__c>)trigger.new ;
		
		LLP_Catalog_Summary__c[] listLLPSum = [select Engine_Template__c,id,name,status__c from LLP_Catalog_Summary__c where Engine_Template__c = :listNew];
		map<string,LLP_Catalog_Summary__c> mapLLPCatSummary = new map<string,LLP_Catalog_Summary__c>();
		
		for(LLP_Catalog_Summary__c currec:listLLPSum){
			mapLLPCatSummary.put(currec.Engine_Template__c + currec.name,currec);
		}

		map<string,LLP_Catalog_Summary__c> mapUpsertLLPCatSummary = new map<string,LLP_Catalog_Summary__c>();

		list<AggregateResult> agrResList = [select LLP_Template__r.Engine_Template__c DBID,name YR,sum(Life_Limit_F__c) TotCyc , Sum(Catalog_Price__c) TotCost from LLP_Catalog__c 
												where LLP_Template__r.Engine_Template__c = :listNew
												group by LLP_Template__r.Engine_Template__c,Name ];
		
		string strKey,strYear,StrEngId;										
        for ( AggregateResult cRec : agrResList ) {
        	strYear = (String)cRec.get('YR');
        	StrEngId = (String)cRec.get('DBID');
			strKey = StrEngId + strYear;
			if(mapLLPCatSummary.containsKey(strKey) && !mapUpsertLLPCatSummary.containsKey(strKey)){
				//means  update on Old catalog
				mapUpsertLLPCatSummary.put(strKey,mapLLPCatSummary.get(strKey));
			}else if(!mapUpsertLLPCatSummary.containsKey(strKey)){
				//means New catalog
				mapUpsertLLPCatSummary.put(strKey,new LLP_Catalog_Summary__c(name=strYear,Engine_Template__c =StrEngId ));
			}        	
        }												
		
		if(mapUpsertLLPCatSummary.size()>0) upsert mapUpsertLLPCatSummary.values(); // rest of the calcuation will happen in trigger code
		
		
		// Update prev LLP catalog record
		Engine_Template__c[] listEngineLLPSum = [select id
								,(select Engine_Template__c,id,name,status__c from LLP_Catalog_Summary__r order by name )// asc order
								from Engine_Template__c
								where id = :listNew];
		Id prevId;	
		listLLPSum = new list<LLP_Catalog_Summary__c>();					
		for(Engine_Template__c curRec:listEngineLLPSum){
			prevId =null;						
			for(LLP_Catalog_Summary__c curChildrec: curRec.LLP_Catalog_Summary__r){
				curChildrec.Prev_LLP_Cat_Sum__c = prevId;
				prevId = curChildrec.Id;
				listLLPSum.add(curChildrec);
			}
		}
		if(listLLPSum.size()>0){
			LeaseWareUtils.TriggerDisabledFlag=true;
		 	upsert listLLPSum; 
		 	LeaseWareUtils.TriggerDisabledFlag=false;
		}
	}


 
   //only after update
  private void updateRelatedEngines(){
    
    Engine_Template__c[] listNew = (list<Engine_Template__c>)trigger.new ;
    map<string,string> mapEngineDBToYear = new map<string,string>();
    Engine_Template__c OldRec;
   
    for(Engine_Template__c curEngtempl:listNew){
            OldRec = (Engine_Template__c)trigger.oldMap.get(curEngtempl.Id);
            if(curEngtempl.Current_Catalog_Year__c!=null && OldRec.Current_Catalog_Year__c!=curEngtempl.Current_Catalog_Year__c){
                mapEngineDBToYear.put(curEngtempl.Id,curEngtempl.Current_Catalog_Year__c);
            }    
            
    }
    
    if(mapEngineDBToYear.size()>0){
        LLP_Template__c[] LLPTempList=[select Id,Name,Engine_Template__c,Engine_Template__r.Current_Catalog_Year__c 
                                          ,Part_Number__c
                                        ,(select Name,Catalog_Price__c
                                            from LLP_Catalog__r
                                            where name in :mapEngineDBToYear.values())
                                        from LLP_Template__c
                                        where Engine_Template__c in :mapEngineDBToYear.Keyset()];  
                LLP_Catalog__c tempCat;                            
        for(LLP_Template__c curLLPTempl:LLPTempList){
          tempCat =null;
                  for(LLP_Catalog__c curCat:curLLPTempl.LLP_Catalog__r){
                      if(curLLPTempl.Engine_Template__r.Current_Catalog_Year__c==curCat.Name)tempCat=curCat;
                  }
                  if(tempCat==null){
                      listNew[0].addError('Catalog year (' + curLLPTempl.Engine_Template__r.Current_Catalog_Year__c + ') not found for ' +curLLPTempl.Name +':' + curLLPTempl.Part_Number__c + ' .');
                  }else{
                      curLLPTempl.Replacement_Cost__c= tempCat.Catalog_Price__c;
                 
                  }          
        }
        
            try{
                if(LLPTempList.size()>0)update LLPTempList;
            }catch(System.DmlException ex){
                listnew[0].addError(ex.getDmlMessage(0));
            }
            catch(Exception ex){
                listnew[0].addError(ex);
            }   

    }

          system.debug('MaintenanceProgramEventHandler.afterUpdate(-)');     
  }
}