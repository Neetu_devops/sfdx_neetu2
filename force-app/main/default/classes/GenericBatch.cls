/*
    This is a genericBatch class to spawn batches for any methods.
    Example:
        Check class GenericScriptExecute


*/
public without sharing class GenericBatch implements Database.Batchable<Object>, Database.Stateful {

    string scriptMethodName;
    string infoMessage='';
    string exceptionMessage='';
    Integer numberOfRecs;
    Integer noOfProcessedRec=0;
    boolean updateMode=false;
    map<string,string> mapParam;
    map<string,string> mapParamInitial;    
    GenericScriptExecute scriptExecRec;
    Package_Script_History__c scriptReq;

    public GenericBatch(string methodName,boolean updateMode_p,map<string,string> mapParam_p,Package_Script_History__c scriptReq_p){
        system.debug('GenericBatch const Param =' + ' methodName ' + methodName + ' mapParam_p ' + mapParam_p + ' updateMode_p ' + updateMode_p );
        scriptMethodName = methodName;
        updateMode = updateMode_p;
        mapParam = mapParam_p;
        if(mapParam==null) {mapParam = new map<string,string>();}// to make the map pass by reference.
        mapParamInitial = mapParam_p;
        scriptExecRec = new GenericScriptExecute();
        scriptReq = scriptReq_p;
    }


    public Iterable<Object> start(Database.BatchableContext bc) {
        system.debug('GenericBatch ScriptName =' + scriptMethodName);
        string scriptStatus = 'Processing';
        if(scriptMethodName.endsWithIgnoreCase(GenericScriptExecute.C_VERIFY)) {scriptStatus = 'Verification Processing';}
        updatePackageScriptRec(scriptReq,scriptStatus);

        List<Object> retList = scriptExecRec.callStartMethod(scriptMethodName,updateMode,mapParam);

        if(retList==null){
            exceptionMessage = 'Could not find the method for this script. Script name:' + scriptMethodName;
            retList = new List<Object>();
        }else if(scriptExecRec.isDependentScriptVerified(scriptReq)){
            exceptionMessage =  'Dependent script has not been completed. Scriptname=' + scriptMethodName;
            retList = new List<Object>();            
        }else if(Test.isRunningTest() && retList.size()>1){
            retList = new List<Object>{retList[0]};
        }
        numberOfRecs = retList.size();
        return retList;
    }
    
    
    public void execute(Database.BatchableContext bc, List<Object> scopeList)
    {
        system.debug('mapParam= ' + mapParam );
        infoMessage = infoMessage +  '\n================================================================================================';
        infoMessage = infoMessage +  '\n - Number of records=' + scopeList.size();
        system.debug('GenericBatch ' + scriptMethodName + '(+) - Number of records=' + scopeList.size());
        Savepoint spStart = Database.setSavepoint();
        try{
            if(String.IsBlank(exceptionMessage)){
                infoMessage = infoMessage +  '\n' + scriptExecRec.callExecuteMethod(scopeList,scriptMethodName,updateMode,mapParam,scriptReq);
            }else{
                infoMessage = infoMessage +  '\n - Not Executed this batch. Reason is ' + exceptionMessage;
            }
        }catch(Exception ex){
            Database.rollback(spStart);
            exceptionMessage += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
            system.debug('Exception : ' + ex +  + '===' +exceptionMessage);
        }
        NoOfProcessedRec += scopeList.size();
        system.debug('mapParam1= ' + mapParam );
        system.debug('GenericBatch ' + scriptMethodName + '(-)' );
    } 
    
    public void finish(Database.BatchableContext bc) {
        try{
            

            if(!scriptMethodName.endsWithIgnoreCase(GenericScriptExecute.C_VERIFY)) {// for script
                finishMethodOnScriptRun();
                GenericScriptExecute.callVerifyBatch(scriptReq,mapParamInitial);
            }else{//for verify script
                finishMethodOnVreifyScriptRun();
            }
        }catch(Exception ex){
            LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),'Unexpected Error' + ' : Script Executed :'+scriptMethodName ,ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage(),null);
        }
        
    }
    

    public void finishMethodOnVreifyScriptRun(){

        string scriptStatus= 'Verified';
        if(exceptionMessage!='' || (mapParam!=null && mapParam.containsKey('ERROR'))){
            scriptStatus= 'Verification Error';
        }

        string outputStr ='[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] - ';
        if(LeaseWareUtils.isRunningInDevInstance()){
            outputStr += ' Running in Developer Instance : \n';
        }else {
            outputStr +=  ' Package Info=' +  system.requestVersion()   + '\n';
        }
        
        outputStr += '=> Error Message if any: \n' + exceptionMessage; 
        outputStr += '\n => Information \n' + infoMessage;
        
        updatePackageScriptRec(scriptReq,scriptStatus);
        // attach file to the record
        try{
            LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
            LeaseWareStreamUtils.addFileToRecord(scriptMethodName,scriptReq.id,outputStr,'.log');
            LeaseWareUtils.TriggerDisabledFlag = false;
        }catch(Exception ex){
            LeaseWareUtils.TriggerDisabledFlag = false;
            outputStr =   '------------------------------------------\n' + ' Got error while atdding the log file to record.'+ex +
                        '------------------------------------------\n' + outputStr;
        }
        //sending email
        string subjectName = scriptStatus + ' : Script Executed :'+scriptMethodName +' on Date '+ system.today().format() ;
        string emailBody = 'Please find the attachment.';
        list<Document> documents = new list<Document>();
        //log file
        documents.add(LeaseWareStreamUtils.createDoc(outputStr,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
        
        LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),subjectName ,emailBody,documents);

    }    
    public void finishMethodOnScriptRun(){



        string scriptStatus= 'Succeeded';
        if(numberOfRecs != NoOfProcessedRec || exceptionMessage!=''){
            scriptStatus= 'Error';
        }else if(mapParam!=null && mapParam.containsKey('ERROR')){
            scriptStatus= 'Warning';
        }  
        string outputStr ='[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] - ';
        if(LeaseWareUtils.isRunningInDevInstance()){
            outputStr += ' Running in Developer Instance : \n';
        }else {
            outputStr +=  ' Package Info=' +  system.requestVersion()   + '\n';
        }
        
        outputStr += '=> Error Message if any: \n' + exceptionMessage; 
        outputStr += '\n => Information \n' + infoMessage;
        
        updatePackageScriptRec(scriptReq,scriptStatus);
        String aditionalCSVFile;
        if(mapParam!=null && mapParam.containsKey('LOAD_CSV_FILE')){
            aditionalCSVFile = mapParam.get('LOAD_CSV_FILE');
            
            try{
                LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
                LeaseWareStreamUtils.addFileToRecord(scriptMethodName,scriptReq.id,aditionalCSVFile,'.csv');
                LeaseWareUtils.TriggerDisabledFlag = false;
            }catch(Exception ex){
                LeaseWareUtils.TriggerDisabledFlag = false;
                outputStr =   '------------------------------------------\n' + ' Got error while atdding the csv file to record.'+ex +
                            '------------------------------------------\n' + outputStr;
            }                
        }
       

        // attach file to the record
        try{
            LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
            LeaseWareStreamUtils.addFileToRecord(scriptMethodName,scriptReq.id,outputStr,'.log');
            LeaseWareUtils.TriggerDisabledFlag = false;
        }catch(Exception ex){
            LeaseWareUtils.TriggerDisabledFlag = false;
            outputStr =   '------------------------------------------\n' + ' Got error while atdding the log file to record.'+ex +
                        '------------------------------------------\n' + outputStr;
        }
        //sending email
        string subjectName = scriptStatus + ' : Script Executed :'+scriptMethodName + ' processed (' + NoOfProcessedRec+ '/' + numberOfRecs +  ') records on Date '+ system.today().format() ;
        string emailBody = 'Please find the attachment.';
        list<Document> documents = new list<Document>();
        if(aditionalCSVFile!=null){
            documents.add(LeaseWareStreamUtils.createDoc(aditionalCSVFile,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.csv'));
        }
        //log file
        documents.add(LeaseWareStreamUtils.createDoc(outputStr,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
        
        LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),subjectName ,emailBody,documents);

    }

    public static void updatePackageScriptRec(Package_Script_History__c scriptReq,string status){
        scriptReq.status__c = status;
        if(status=='Succeeded') {
            scriptReq.status__c = 'Success';
            if(!LeaseWareUtils.isRunningInDevInstance()){
                scriptReq.Executed_Package_Version__c = ''+system.requestVersion();
            }  
            scriptReq.Execution_Date__c = system.today();           
        }   
        update scriptReq;     
    }
}