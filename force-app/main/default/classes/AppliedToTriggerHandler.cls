/* This is for Applied To ( old name Funded with)

	Create Payment in +ve value similar to Cash payment
*/  
public class AppliedToTriggerHandler{}/*
implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'AppliedToTriggerHandlerBefore';
    private final string triggerAfter = 'AppliedToTriggerHandlerAfter';
    Applied_To__c[] listExistingAppliedTo;
    
    
    public AppliedToTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     ******
    public void bulkBefore()
    {
        set<string> setCNIds = new set<string>();
        list<Applied_To__c> newList =  (list<Applied_To__c>)((trigger.isdelete)?trigger.Old:trigger.New) ;  
    	for(Applied_To__c currec: newList){
    		setCNIds.add(currec.Credit_Note__c);
    	}         
        
        listExistingAppliedTo = [select id,Credit_Note__c ,Invoice__c
        								from Applied_To__c
        								where Credit_Note__c in :setCNIds
        								and id not in :newList];
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('AppliedToTriggerHandler.beforeInsert(+)');
        validateDuplicate();
		UpdateFundedWithInvRefBucket();

        system.debug('AppliedToTriggerHandler.beforeInsert(-)');     
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AppliedToTriggerHandler.beforeUpdate(+)');
        validateDuplicate();
		UpdateFundedWithInvRefBucket();
        
        system.debug('AppliedToTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     ******
    public void beforeDelete()
    {  

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AppliedToTriggerHandler.beforeDelete(+)');
            list<Applied_To__c> newList =  (list<Applied_To__c>)trigger.old ; 
            for(Applied_To__c curInv:newList){
            	
                if('Approved'.equals(curInv.Status__c)){
                    curInv.addError('You can not delete once it is approved..');
                    return;
                }
            } 
        system.debug('AppliedToTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AppliedToTriggerHandler.afterInsert(+)');


        system.debug('AppliedToTriggerHandler.afterInsert(-)');      
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AppliedToTriggerHandler.afterUpdate(+)');

		UpdateApplyToInvoiceParentBucket();
        
        system.debug('AppliedToTriggerHandler.afterUpdate(-)');      
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AppliedToTriggerHandler.afterDelete(+)');
        
        
        system.debug('AppliedToTriggerHandler.afterDelete(-)');      
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AppliedToTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('AppliedToTriggerHandler.afterUnDelete(-)');        
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     ******
    public void andFinally()
    {
        // insert any audit records

    }


    //beforeIns,BefUpdate
    private void validateDuplicate(){
    	
    	set<string> checkDup = new set<string>();
    	for(Applied_To__c existRec :listExistingAppliedTo){
    		checkDup.add(''+existRec.Credit_Note__c + existRec.Invoice__c);
    	}
    	list<Applied_To__c> newList =  (list<Applied_To__c>)(trigger.New) ;  
    	for(Applied_To__c currec: newList){
    		if(checkDup.contains(''+currec.Credit_Note__c + currec.Invoice__c)){
    			currec.Invoice__c.addError('This Invoice is already part of Funded With.');
    		}
    		checkDup.add(''+currec.Credit_Note__c + currec.Invoice__c);
    	}
  	
    	
    }

    
    //  after update
    private void UpdateApplyToInvoiceParentBucket(){
    	
    	// Update parent record once approved.
     	list<Applied_To__c> newList =  (list<Applied_To__c>)trigger.New ;  
    	set<id> creditNoteIdsSet = new set<id>();
    	Applied_To__c oldRec;
    	for(Applied_To__c currec: newList){
    		if('Credit Note Rent'.equals(currec.Type_F__c)) break;
    		oldRec = (Applied_To__c)trigger.oldMap.get(currec.id);	
    		if('Approved'.equals(currec.Status__c) ){    		
    			creditNoteIdsSet.add(currec.Credit_Note__c);
    		}
    	}  
    	FundedWithTriggerHandler.UpdateCNRec(creditNoteIdsSet);
    }    
    
    private void UpdateFundedWithInvRefBucket(){
    	
    	system.debug('UpdateFundedWithInvRefBucket+');
    	
    	list<Applied_To__c> newList =  (list<Applied_To__c>)trigger.New ;  
    	set<id> invoiceIdsSet = new set<id>();
    	for(Applied_To__c currec: newList){
    		invoiceIdsSet.add(currec.Invoice__c);
    	}       
    	map<id,Invoice__c> invMap = new map<id,Invoice__c>([select id 
    						,Maintenance_Reserve_Engine_1__c,Maintenance_Reserve_Engine_2__c,Maintenance_Reserve_Engine_3__c,Maintenance_Reserve_Engine_4__c
    						,Maintenance_Reserve_Engine_1_LLP__c,Maintenance_Reserve_Engine_2_LLP__c,Maintenance_Reserve_Engine_3_LLP__c,Maintenance_Reserve_Engine_4_LLP__c
    						,Maint_Reserve_Heavy_Maint_1_Airframe__c,Maint_Reserve_Heavy_Maint_2_Airframe__c,Maint_Reserve_Heavy_Maint_3_Airframe__c,Maint_Reserve_Heavy_Maint_4_Airframe__c
    						,Maintenance_Reserve_LG_Left_Main__c,Maintenance_Reserve_LG_Left_Wing__c,Maintenance_Reserve_LG_Nose__c,Maintenance_Reserve_LG_Right_Main__c,Maintenance_Reserve_LG_Right_Wing__c
    						,Maintenance_Reserve_Propeller_1__c,Maintenance_Reserve_Propeller_2__c,Maintenance_Reserve_Propeller_1_LLP__c,Maintenance_Reserve_Propeller_2_LLP__c
    						,Maintenance_Reserve_APU__c,MR_20_Month_C_Check__c
    						,Invoice_Type__c,Amount__c    					
    					
    					from Invoice__c where id in :invoiceIdsSet]);

		Decimal proprateWeigtage ; 
		Invoice__c OrigInv;
		Applied_To__c oldRec;
		list<Payment__c> insertPaymentList = new list<Payment__c>();
		Id RecordTypeId;
    	for(Applied_To__c currec: newList){
    		
  		
    		OrigInv = invMap.get(currec.Invoice__c);
    		if('Credit Note MR'.equals(currec.Type_F__c)){
	    		if(currec.Prorate__c){
	                        currec.Maintenance_Reserve_APU__c = 0;
	                        currec.Maintenance_Reserve_Engine_1__c = 0;
	                        currec.Maintenance_Reserve_Engine_1_LLP__c = 0;
	                        currec.Maintenance_Reserve_Engine_2__c = 0;
	                        currec.Maintenance_Reserve_Engine_2_LLP__c = 0;
	                        currec.Maintenance_Reserve_Engine_3__c = 0;
	                        currec.Maintenance_Reserve_Engine_3_LLP__c = 0;
	                        currec.Maintenance_Reserve_Engine_4__c = 0;
	                        currec.Maintenance_Reserve_Engine_4_LLP__c = 0;
	
	                        currec.Maintenance_Reserve_Propeller_1__c = 0;
	                        currec.Maintenance_Reserve_Propeller_1_LLP__c = 0;
	                        currec.Maintenance_Reserve_Propeller_2__c = 0;
	                        currec.Maintenance_Reserve_Propeller_2_LLP__c = 0;
	                        
	                        currec.MR_20_Month_C_Check__c = 0;
	                        currec.Maint_Reserve_Heavy_Maint_1_Airframe__c = 0;
	                        currec.Maint_Reserve_Heavy_Maint_2_Airframe__c =0;
	                        currec.Maint_Reserve_Heavy_Maint_3_Airframe__c = 0;
	                        currec.Maint_Reserve_Heavy_Maint_4_Airframe__c = 0;
	                        currec.Maintenance_Reserve_LG_Left_Main__c = 0;
	                        currec.Maintenance_Reserve_LG_Left_Wing__c = 0;
	                        currec.Maintenance_Reserve_LG_Nose__c = 0;
	                        currec.Maintenance_Reserve_LG_Right_Main__c =0;
	                        currec.Maintenance_Reserve_LG_Right_Wing__c = 0;  
	                if(currec.Credit_Amount__c==null){ 
	                	// default
	                	currec.Credit_Amount__c = (leasewareUtils.zeroIfNull(currec.Invoice_Paid_Amount__c)<=0?currec.Invoiced_Amount__c:leasewareUtils.zeroIfNull(currec.Invoice_Paid_Amount__c));
	                	currec.Credit_Amount__c = currec.Credit_Amount__c<0?0:currec.Credit_Amount__c;
	                }            			
	    			
	    			if(OrigInv.Amount__c==null || OrigInv.Amount__c==0)proprateWeigtage=0;
	    			else proprateWeigtage = currec.Credit_Amount__c/OrigInv.Amount__c;
	    				    			
	    			
	                        currec.Maintenance_Reserve_APU__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_APU__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_1__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_1__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_1_LLP__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_1_LLP__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_2__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_2__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_2_LLP__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_2_LLP__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_3__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_3__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_3_LLP__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_3_LLP__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_4__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_4__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Engine_4_LLP__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Engine_4_LLP__c  )  * proprateWeigtage;
	
	                        currec.Maintenance_Reserve_Propeller_1__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Propeller_1__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Propeller_1_LLP__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Propeller_1_LLP__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Propeller_2__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Propeller_2__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_Propeller_2_LLP__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_Propeller_2_LLP__c  )  * proprateWeigtage;
	
	                        currec.MR_20_Month_C_Check__c += leasewareUtils.zeroIfNull(OrigInv.MR_20_Month_C_Check__c  )  * proprateWeigtage;
	                        currec.Maint_Reserve_Heavy_Maint_1_Airframe__c += leasewareUtils.zeroIfNull(OrigInv.Maint_Reserve_Heavy_Maint_1_Airframe__c  )  * proprateWeigtage;
	                        currec.Maint_Reserve_Heavy_Maint_2_Airframe__c += leasewareUtils.zeroIfNull(OrigInv.Maint_Reserve_Heavy_Maint_2_Airframe__c  )  * proprateWeigtage;
	                        currec.Maint_Reserve_Heavy_Maint_3_Airframe__c += leasewareUtils.zeroIfNull(OrigInv.Maint_Reserve_Heavy_Maint_3_Airframe__c  )  * proprateWeigtage;
	                        currec.Maint_Reserve_Heavy_Maint_4_Airframe__c += leasewareUtils.zeroIfNull(OrigInv.Maint_Reserve_Heavy_Maint_4_Airframe__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_LG_Left_Main__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_LG_Left_Main__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_LG_Left_Wing__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_LG_Left_Wing__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_LG_Nose__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_LG_Nose__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_LG_Right_Main__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_LG_Right_Main__c  )  * proprateWeigtage;
	                        currec.Maintenance_Reserve_LG_Right_Wing__c += leasewareUtils.zeroIfNull(OrigInv.Maintenance_Reserve_LG_Right_Wing__c  )  * proprateWeigtage;    			
	    			
	    		}// Prorate if check
	    		else{
	                    if((trigger.isInsert && LeaseWareUtils.zeroIfNull(currec.Credit_Amount__c)!=0) 
	                    		|| (trigger.isUpdate && LeaseWareUtils.zeroIfNull(currec.Credit_Amount__c)!=0 && currec.Credit_Amount__c!=((Applied_To__c)trigger.oldMap.get(currec.id)).Credit_Amount__c)
	                    		){
	                        currec.Credit_Amount__c.addError('Please don\'t enter Amount if prorate is unchecked.');
	                        continue;
	                    }    			
							currec.Credit_Amount__c = (
		         				leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_APU__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_1__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_1_LLP__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_2__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_2_LLP__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_3__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_3_LLP__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_4__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Engine_4_LLP__c  ) +
		
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Propeller_1__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Propeller_1_LLP__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Propeller_2__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_Propeller_2_LLP__c  ) +
		                        
		                        leasewareUtils.zeroIfNull(  currec.MR_20_Month_C_Check__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maint_Reserve_Heavy_Maint_1_Airframe__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maint_Reserve_Heavy_Maint_2_Airframe__c ) +
		                        leasewareUtils.zeroIfNull(  currec.Maint_Reserve_Heavy_Maint_3_Airframe__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maint_Reserve_Heavy_Maint_4_Airframe__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_LG_Left_Main__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_LG_Left_Wing__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_LG_Nose__c  ) +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_LG_Right_Main__c)  +
		                        leasewareUtils.zeroIfNull(  currec.Maintenance_Reserve_LG_Right_Wing__c  )
		                        ) ; 	
		    		if(MATH.abs(LeaseWareUtils.zeroIfNull(currec.Credit_Amount__c)) > MATH.abs(currec.Invoice_Balance_Due__c)){
		                        currec.Credit_Amount__c.addError('Applied Amount can not be more than Invoice Balance Due.');
		                        continue;
		            } 	                        			
	    		}// Prorate else check
    		}
    		else if('Credit Note Rent'.equals(currec.Type_F__c)){
		    		if(MATH.abs(LeaseWareUtils.zeroIfNull(currec.Credit_Amount__c)) > MATH.abs(currec.Invoice_Balance_Due__c)){
		                        currec.Credit_Amount__c.addError('Applied Amount can not be more than Invoice Balance Due.');
		                        continue;
		            }   
		    			            			
    		}    		
	
    		
    		if(trigger.isUpdate){
    			
    			oldRec = (Applied_To__c)trigger.oldMap.get(currec.id);
    			if('Approved'.equals(currec.Status__c) && !'Approved'.equals(oldRec.Status__c)){
    				system.debug('OrigInv.id='+OrigInv.id);
    				system.debug('OrigInv.Invoice_Type__c='+OrigInv.Invoice_Type__c);

    				// create payment in -ve amount
    				string RTType =OrigInv.Invoice_Type__c.replace(' ','_') ;
    				
    				Payment__c Creditpmt = new Payment__c(Name='Payment'
    					, Invoice__c=OrigInv.id, payment_date__c=currec.Application_Date__c
    					, RecordTypeId=Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get(RTType).getRecordTypeId()
    					, Invoice_Type__c=OrigInv.Invoice_Type__c
    						,Prorate__c = false
    						,Credit_Note__c = currec.Credit_Note__c	
    						, Payment_Type__c = 'CN_Funded',
		                       Twenty_Month_C_Check__c= (+1) * leasewareUtils.zeroIfNull(currec.MR_20_Month_C_Check__c) ,
		                       APU__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_APU__c) ,
		                       Engine_1__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_1__c ) ,
		                       Engine_1_LLP__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_1_LLP__c) ,
		                       Engine_2__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_2__c ) ,
		                       Engine_2_LLP__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_2_LLP__c) ,
		                       Engine_3__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_3__c ) ,
		                       Engine_3_LLP__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_3_LLP__c) ,
		                       Engine_4__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_4__c ) ,
		                       Engine_4_LLP__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Engine_4_LLP__c) ,
		                       Propeller_1__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Propeller_1__c) ,
		                       Propeller_1_LLP__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Propeller_2_LLP__c) ,
		                       Propeller_2__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Propeller_2__c) ,
		                       Propeller_2_LLP__c= (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_Propeller_2_LLP__c) ,
		                       Heavy_Maint_1_Airframe__c= (+1) * leasewareUtils.zeroIfNull(currec.Maint_Reserve_Heavy_Maint_1_Airframe__c) ,
		                       Heavy_Maint_2_Airframe__c= (+1) * leasewareUtils.zeroIfNull(currec.Maint_Reserve_Heavy_Maint_2_Airframe__c) ,
		                       Heavy_Maint_3_Airframe__c= (+1) * leasewareUtils.zeroIfNull(currec.Maint_Reserve_Heavy_Maint_3_Airframe__c) ,
		                       Heavy_Maint_4_Airframe__c= (+1) * leasewareUtils.zeroIfNull(currec.Maint_Reserve_Heavy_Maint_4_Airframe__c) ,
		        
		
		                       LG_Left_Main__c = (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_LG_Left_Main__c) , 
		                       LG_Nose__c = (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_LG_Left_Wing__c) , 
		                       LG_Right_Main__c = (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_LG_Nose__c) , 
		                       LG_Left_Wing__c = (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_LG_Right_Main__c), 
		                       LG_Right_Wing__c = (+1) * leasewareUtils.zeroIfNull(currec.Maintenance_Reserve_LG_Right_Wing__c),
    						   Amount__c = currec.Credit_Amount__c
    					);
    					
    				if('Credit Note Rent'.equals(currec.Type_F__c)){
    					
    					Creditpmt.Amount__c=currec.Credit_Amount__c;
    					system.debug('Should be Rent type recordId ==='+OrigInv.Invoice_Type__c);
    				}
    				insertPaymentList.add(Creditpmt);    				
    			}
    			
    		}// insert payment record on approved
    	}  // end of for loop
    	
    	if(!insertPaymentList.isEmpty()) insert insertPaymentList;
    	system.debug('insertPaymentList size='+insertPaymentList.size());
    	
    	system.debug('UpdateFundedWithInvRefBucket-');
    }
    
}*/