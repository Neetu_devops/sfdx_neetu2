public class LookupController {

    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, String criteriaFieldName,String criteriaValue) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        System.debug('searchKey :'+searchKey);
        List < sObject > returnList = new List < sObject > ();
        Boolean criteriaFlag;
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        
        System.debug('fetchLookUpValues criteriaFieldName '+criteriaFieldName + ' criteriaValue: '+criteriaValue );
             
        if(String.isNotBlank(criteriaFieldName) &&  (criteriaValue != 'True' || criteriaValue != 'False')){
            sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey and '+ 
            criteriaFieldName +' = : criteriaValue order by createdDate DESC limit 5';
        }
        if(String.isNotBlank(criteriaFieldName) && criteriaValue == 'True'){
            criteriaFlag = True;
             sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey and '+ 
            criteriaFieldName +' = : criteriaFlag order by createdDate DESC limit 5';
        }
        else if(String.isNotBlank(criteriaFieldName) && criteriaValue == 'False'){
               criteriaFlag = False;
             sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey and '+ 
            criteriaFieldName +' = : criteriaFlag order by createdDate DESC limit 5';
        }
           
          
        System.debug('fetchLookUpValues sQuery '+sQuery);
        
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        System.debug('returnList :'+returnList);
        return returnList;
    }
}