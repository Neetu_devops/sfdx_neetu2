@isTest
public class TestAssetPortalController {

    
    @isTest
    static void testAssetPortalController() {
        Test.startTest();
        String namespace = AssetPortalController.getNameSpacePrefix();
        String resBody = '{"controllerValues":{},"defaultValue":null,"url":"/services/data/v41.0/ui-api/object-info/'+namespace+'Aircraft__c/picklist-values/0123h000000Edn5AAC/'+namespace+'Aircraft_Type__c","values":[{"attributes":null,"label":"A320","validFor":[],"value":"A320"},{"attributes":null,"label":"BR700","validFor":[],"value":"BR700"}]}';
        Map<String,String> responseHeader = new Map<String,String>{ 'Authorization' => 'OAuth '+UserInfo.getSessionId() };            
            
        TestSingleRequestMock fakeResponse = new TestSingleRequestMock(200,resBody,responseHeader);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        List<String> picklistRecordType = AssetPortalController.getPicklistBasedOnRecordType('Aircraft__c','Asset_Type__c', 'Aircraft');
        System.assertEquals(picklistRecordType.size(), 2);
        Map<String, List<String>> depMap = AssetPortalController.getDependentMap('Aircraft__c', 'Asset_Type__c', 'Aircraft_Variant__c');
        List<String> picklist = AssetPortalController.getPickListValues('Portal_Asset_Request__c','Security_Deposit_Type__c');
        System.assertEquals(picklist.size(), 2);
        Test.stopTest();
    } 

    public class TestSingleRequestMock implements HttpCalloutMock {
        protected Integer code;
        protected String bodyAsString;
        protected Map<String, String> responseHeaders;
        
        public TestSingleRequestMock(Integer code, String body, Map<String, String> responseHeaders) {
            this.code = code;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setBody(bodyAsString);
            
            if (responseHeaders != null) {
                for (String key : responseHeaders.keySet()) {
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            return resp;
        }
    }
    
    @isTest
    static void testSearchAssetsWithNoLeaseOverlap()
    {
        LeaseWareUtils.clearFromTrigger(); 

        Custom_Lookup__c lookup = new Custom_Lookup__c(Name = 'Default', Lookup_Type__c = 'Engine', Active__c = true);        
        insert lookup;        
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1', Aircraft_Type__c = 'CFM56', TSN__c = 500, CSN__c = 200);
        insert aircraft;
        Operator__c operator = new Operator__c(Name='TestOperator');
        insert operator;
        Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,
                        Lease_End_Date_New__c=System.Today().addmonths(1),Lease_Start_Date_New__c=Date.parse('01/01/2018'));
        insert lease;

        Id engineRecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();

        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(
			Name='12345', Engine_Model__c ='CFM56', Attached_Aircraft__c= aircraft.id,
				TSN__c=500, CSN__c=200, Type__c = 'Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1',
            Manufacturer_List__c = null,Current_TS__c = lookup.id, RecordTypeId = engineRecordTypeId);
        insert consttAssembly;
        
        Test.StartTest();

        LeasewareUtils.clearFromTrigger();
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
            Interval_2_Hours__c=200,            
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;

        // Set up Project Event
        Assembly_Event_Info__c eventPR = new Assembly_Event_Info__c();
        eventPR.Name = 'Test PR Event Info';
        eventPR.Event_Cost__c = 20000;
        eventPR.Constituent_Assembly__c = consttAssembly.Id;
        eventPR.Maintenance_Program_Event__c = mpe.Id;
        eventPR.Event_Due_Date_Override__c = System.today().addmonths(-1);
        insert eventPR;
        
        Portal_Asset_Request__c testRequest = new Portal_Asset_Request__c(Name = 'TestRequest', Asset_Type__c = 'CFM56', Date_Start__c = System.Today().addmonths(2), Date_End__c = System.Today().addmonths(6));

        List<AssetPortalController.PortalAssetSelectedWrapper> lstPASWrapper = PortalAssetHelper.searchAssets(testRequest);
        System.assertEquals(lstPASWrapper.size(), 1);

        List<Portal_Asset_Selected__c> lstPAS = new List<Portal_Asset_Selected__c>{lstPASWrapper[0].PortalAssetSelected};

        map<string,string> mapResult = PortalAssetHelper.savePortalAssetsSelected(testRequest, lstPAS);
        System.assertEquals(mapResult.get('code'), '200');
        
        
        AssetPortalController.SearchRequestWrapper searchWrapper = new AssetPortalController.SearchRequestWrapper();
        searchWrapper.assetRequest = testRequest;
        String searchString = JSON.serialize(searchWrapper);
        List<AssetPortalController.PortalAssetSelectedWrapper> resultWrapper = AssetPortalController.getAssetRequest(searchString);
        System.debug('testAssetPortalController resultWrapper: '+resultWrapper);
        System.assertEquals(resultWrapper.size(), 1);
        if(resultWrapper != null) {
            AssetPortalController.saveRequestData(JSON.serialize(resultWrapper),searchString);
        }

        Test.StopTest();
    }

    @isTest
    static void testSearchAssetsWithMultiLease()
    {
        LeaseWareUtils.clearFromTrigger(); 

        Custom_Lookup__c lookup = new Custom_Lookup__c(Name = 'Default', Lookup_Type__c = 'Engine', Active__c = true);        
        insert lookup;        
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1', Aircraft_Type__c = 'CFM56', TSN__c = 500, CSN__c = 200);
        insert aircraft;
        Operator__c operator = new Operator__c(Name='TestOperator');
        insert operator;
        Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,
                        Lease_End_Date_New__c=System.Today().addMonths(1).addDays(15),Lease_Start_Date_New__c=System.Today().addmonths(1));
        insert lease;

        Lease__c lease2 = new Lease__c(Name='TestLease2',aircraft__c =aircraft.id, Lessee__c=operator.Id,
                        Lease_End_Date_New__c=System.today().addMonths(3).addDays(15),Lease_Start_Date_New__c=System.today().addMonths(3));
        insert lease2;

        Id engineRecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();

        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(
			Name='12345', Engine_Model__c ='CFM56', Attached_Aircraft__c= aircraft.id,
				TSN__c=500, CSN__c=200, Type__c = 'Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1',
            Manufacturer_List__c = null,Current_TS__c = lookup.id, RecordTypeId = engineRecordTypeId);
        insert consttAssembly;
        
        Test.StartTest();

        LeasewareUtils.clearFromTrigger();
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
            Interval_2_Hours__c=200,            
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;

        // Set up Project Event
        Assembly_Event_Info__c eventPR = new Assembly_Event_Info__c();
        eventPR.Name = 'Test PR Event Info';
        eventPR.Event_Cost__c = 20000;
        eventPR.Constituent_Assembly__c = consttAssembly.Id;
        eventPR.Maintenance_Program_Event__c = mpe.Id;
        eventPR.Event_Due_Date_Override__c = System.today();
        insert eventPR;
        
        Portal_Asset_Request__c testRequest = new Portal_Asset_Request__c(Name = 'TestRequest', Asset_Type__c = 'CFM56', Date_Start__c = System.Today().addMonths(2), Date_End__c = System.Today().addMonths(2).addDays(15));

        List<AssetPortalController.PortalAssetSelectedWrapper> lstPASWrapper = PortalAssetHelper.searchAssets(testRequest);
        System.assertEquals(lstPASWrapper.size(), 1);

        List<Portal_Asset_Selected__c> lstPAS = new List<Portal_Asset_Selected__c>{lstPASWrapper[0].PortalAssetSelected};

        map<string,string> mapResult = PortalAssetHelper.savePortalAssetsSelected(testRequest, lstPAS);
        System.assertEquals(mapResult.get('code'), '200');

        Test.StopTest();
    }

    @isTest
    static void testSearchAssetsWithProjEvnt()
    {
        LeaseWareUtils.clearFromTrigger(); 

        Custom_Lookup__c lookup = new Custom_Lookup__c(Name = 'Default', Lookup_Type__c = 'Engine', Active__c = true);        
        insert lookup;        
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1', Aircraft_Type__c = 'CFM56', TSN__c = 500, CSN__c = 200);
        insert aircraft;
        Operator__c operator = new Operator__c(Name='TestOperator');
        insert operator;
        Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,
                        Lease_End_Date_New__c=System.Today().addMonths(1).addDays(15),Lease_Start_Date_New__c=System.Today().addmonths(1));
        insert lease;

        Lease__c lease2 = new Lease__c(Name='TestLease2',aircraft__c =aircraft.id, Lessee__c=operator.Id,
                        Lease_End_Date_New__c=System.today().addMonths(5).addDays(15),Lease_Start_Date_New__c=System.today().addMonths(5));
        insert lease2;

        Id engineRecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();

        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(
			Name='12345', Engine_Model__c ='CFM56', Attached_Aircraft__c= aircraft.id,
				TSN__c=500, CSN__c=200, Type__c = 'Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1',
            Manufacturer_List__c = null,Current_TS__c = lookup.id, RecordTypeId = engineRecordTypeId);
        insert consttAssembly;
        
        Test.StartTest();

        LeasewareUtils.clearFromTrigger();
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
            Interval_2_Hours__c=200,            
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;

        // Set up Project Event
        Assembly_Event_Info__c eventPR = new Assembly_Event_Info__c();
        eventPR.Name = 'Test PR Event Info';
        eventPR.Event_Cost__c = 20000;
        eventPR.Constituent_Assembly__c = consttAssembly.Id;
        eventPR.Maintenance_Program_Event__c = mpe.Id;
        eventPR.Event_Due_Date_Override__c = System.today().addMonths(2);
        insert eventPR;
        
        Portal_Asset_Request__c testRequest = new Portal_Asset_Request__c(Name = 'TestRequest', Asset_Type__c = 'CFM56', Date_Start__c = System.Today().addMonths(3), Date_End__c = System.Today().addMonths(3).addDays(15));

        List<AssetPortalController.PortalAssetSelectedWrapper> lstPASWrapper = PortalAssetHelper.searchAssets(testRequest);
        System.assertEquals(lstPASWrapper.size(), 1);

        List<Portal_Asset_Selected__c> lstPAS = new List<Portal_Asset_Selected__c>{lstPASWrapper[0].PortalAssetSelected};

        map<string,string> mapResult = PortalAssetHelper.savePortalAssetsSelected(testRequest, lstPAS);
        System.assertEquals(mapResult.get('code'), '200');

        Test.StopTest();
    }
   
}