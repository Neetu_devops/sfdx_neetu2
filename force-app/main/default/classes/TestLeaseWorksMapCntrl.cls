@isTest
private class TestLeaseWorksMapCntrl {

	static Address__c contact1,contact2;
	static Operator__c operator1                        {get; set;}
	static Operator__c operator2                        {get; set;}
	static Operator__c operator3                        {get; set;}
	static Aircraft_Type__c airCraftType                {get; set;}
	static Aircraft_Type_Operator__c ao1                {get; set;}
	static Aircraft_Type_Operator__c ao2                {get; set;}
	static Aircraft_Type_Operator__c ao3                {get; set;}
	static NewsArticle__c article1                      {get; set;}
	static NewsArticle__c article2                      {get; set;}
	static Deal_Proposal__c prospect                    {get; set;}
	static Counterparty__c lessor                       {get; set;}
	static Lessor__c setupRecord                        {get; set;}
	static list<Customer_Interaction_Log__c> Ilogs      {get; set;}
	static LW_Setup__c lwSetup;

	static void dataSetup()
	{
		ServiceSetting__c lwSettings = new ServiceSetting__c();
		LeaseWorksSettings__c lwSettings1 = new LeaseWorksSettings__c();
		lwSettings.Name = 'News';
		lwSettings.Token__c       = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwSettings.EndPointUrl__c  = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
		insert lwSettings;
		insert lwSettings1;

		setupRecord = new Lessor__c(Name='Emirates',Phone_Number__c='999999999',Email_Address__c='test@lws.com',Lease_Logo__c='https://www.logo.com',Address_Street__c='House No.1240,Sector-29',City__c='Faridabad',Zip_Code__c='121008',Country__c='India');
		insert setupRecord;

		/* lwSetup = new LW_Setup__c();
		   lwSetup.Name = 'OPERATOR_NAME_FILTER';
		   lwSetup.Disable__c = false;
		   lwSetup.Value__c = 'OFF';
		   insert lwSetup;*/

		LeaseWareSeededData.SeedLWServiceSetup();

		map<String,Object> mapInOut = new map<String,Object>();

		// Create Operator1
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		mapInOut.put('Operator__c.Name','Air India1');
		mapInOut.put('Operator__c.City__c','Faridabad');
		mapInOut.put('Operator__c.Zip_Code__c','121008');
		mapInOut.put('Operator__c.Country__c','India');
		mapInOut.put('Operator__c.Alias__c','airIndia');
		TestLeaseworkUtil.createOperator(mapInOut);
		operator1 = (Operator__c)mapInOut.get('Operator__c');

		LeaseWareUtils.unsetTriggers('OperatorTrigger');
		// Create Operator2
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		mapInOut.put('Operator__c.Name','Air China');
		mapInOut.put('Operator__c.Location__Latitude__s', 23.45555555);
		mapInOut.put('Operator__c.Location__Longitude__s',73.23233232);
		mapInOut.put('Operator__c.Alias__c','airChina');
		TestLeaseworkUtil.createOperator(mapInOut);
		operator2 = (Operator__c)mapInOut.get('Operator__c');

		LeaseWareUtils.unsetTriggers('OperatorTrigger');
		// Create Operator3
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		mapInOut.put('Operator__c.Name','Jetscape');
		mapInOut.put('Operator__c.Alias__c','Jetscape');
		TestLeaseworkUtil.createOperator(mapInOut);
		operator3 = (Operator__c)mapInOut.get('Operator__c');

		airCraftType = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320', Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56');
		insert airCraftType;

		// Create airCraftType Operators
		mapInOut.put('Aircraft_Type_Operator__c.Aircraft_Type__c',airCraftType.Id);
		mapInOut.put('Aircraft_Type_Operator__c.Operator__c',operator1.Id);
		TestLeaseworkUtil.createAircraftTypeOperator(mapInOut);
		ao1 = (Aircraft_Type_Operator__c)mapInOut.get('Aircraft_Type_Operator__c');

		mapInOut.put('Aircraft_Type_Operator__c.Operator__c',operator2.Id);
		TestLeaseworkUtil.createAircraftTypeOperator(mapInOut);
		ao2 = (Aircraft_Type_Operator__c)mapInOut.get('Aircraft_Type_Operator__c');

		mapInOut.put('Aircraft_Type_Operator__c.Operator__c',operator3.Id);
		TestLeaseworkUtil.createAircraftTypeOperator(mapInOut);
		ao3 = (Aircraft_Type_Operator__c)mapInOut.get('Aircraft_Type_Operator__c');

		LeaseWareUtils.unsetTriggers('CounterpartyTrg');
		lessor = new Counterparty__c(Address_Street__c='1240/29',City__c='Faridabad',Zip_Code__c='121008',Country__c='India',
		                             Fleet_Aircraft_Of_Interest__c='A320',Status__c='Approved',Location__Latitude__s = 23.45555555,
		                             Location__Longitude__s = 73.23233232);
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		insert lessor;

		mapInOut.put('Aircraft__c',null);
		TestLeaseworkUtil.createAircraft(mapInOut);
		Aircraft__c aircraft1 = (Aircraft__c)mapInOut.get('Aircraft__c');

		mapInOut.put('Aircraft__c',null);
		mapInOut.put('Aircraft__c.msn','123');
		TestLeaseworkUtil.createAircraft(mapInOut);
		Aircraft__c aircraft2 = (Aircraft__c)mapInOut.get('Aircraft__c');

		mapInOut.put('Lease__c.Aircraft__c',aircraft1.Id);
		mapInOut.put('Lease__c.Operator__c',operator3.Id);
		mapInOut.put('Lease__c',null);
		TestLeaseworkUtil.createLease(mapInOut);
		Lease__c lease = (Lease__c)mapInOut.get('Lease__c');

	

		Deal__c deal = new Deal__c(Name = 'Test Deal',Campaign_Active__c=true);
		insert deal;

		Aircraft_In_Deal__c acInDeal = new Aircraft_In_Deal__c( Marketing_Deal__c = deal.Id,Name='My AC in Deal',
		                                                        Aircraft__c=aircraft1.Id );
		insert acInDeal;

		prospect = new Deal_Proposal__c( Deal__c = deal.id, Name = 'My proposal', Prospect__c=operator1.Id );
		insert prospect;

		MSN_s_Of_Interest__c proposedTerm = new MSN_s_Of_Interest__c(Name='x', Aircraft__c = acInDeal.Id, Deal_Proposal__c = prospect.Id,
		                                                             Rent_000__c=250000, Term_Mos__c=72, Investment_Required_000__c=3000000, Evaluation_Approach__c='IRR',
		                                                             Upfront__c=true, End_Of_Lease__c=true, Amortize__c=true);
		insert proposedTerm;

		TestLeaseworkUtil.insertTransaction(mapInOut);

		mapInOut.put('Aircraft_In_Transaction__c',null);
		TestLeaseworkUtil.insertAircraft_In_Transaction(mapInOut);

		Aircraft_In_Transaction__c Ait = (Aircraft_In_Transaction__c)mapInOut.get('Aircraft_In_Transaction__c');


	

		mapInOut.put('Customer_Interaction_Log__c.Related_To_Operator__c',operator1.Id);
		mapInOut.put('Customer_Interaction_Log__c.Aircraft_Type__c','A320');
		mapInOut.put('Customer_Interaction_Log__c.Type_Of_Request__c','Aircraft Lease');
		TestLeaseworkUtil.createInteractionlog(mapInOut);

		contact1 = new Address__c(name='Ron',Last_Name__c='Test',Email_Address__c='test1@lws.com',Operator__c=operator1.Id);
		contact2 = new Address__c(name='Ron2',Last_Name__c='Test2',Email_Address__c='test1@new.com',Operator__c=operator1.Id);
		insert new Address__c[] {contact1,contact2};
	}





	static testMethod void TestUpdateOperator()
	{
		dataSetup();
		operator1.city__c     = 'New York';
		operator1.Zip_Code__c = '44111';
		operator1.country__c  = 'United States';
		
		LeaseWareUtils.clearFromTrigger();
		Test.startTest();
		update operator1;
		Test.stopTest();

		operator1.Zip_Code__c = '44111';
		
		LeaseWareUtils.clearFromTrigger();
		update operator1;

		operator1.country__c  = 'United States';
		
		LeaseWareUtils.clearFromTrigger();
		update operator1;
	}

	static testMethod void TestUpdateLessor()
	{
		dataSetup();
		lessor.city__c        = 'San Jose';
		LeaseWareUtils.unsetTriggers('CounterpartyTrg');
		Test.startTest();
		update lessor;
		Test.stopTest();

		lessor.Zip_Code__c = '12109';
		
		LeaseWareUtils.clearFromTrigger();
		update lessor;

		lessor.country__c  = 'United States';
		
		LeaseWareUtils.clearFromTrigger();
		update lessor;

		lessor.Location__Latitude__s   = null;
		lessor.Location__Longitude__s  = null;
		
		LeaseWareUtils.clearFromTrigger();
		update lessor;
	}


	//Unit test for LeaseWorksEmailHandler
	static testMethod void TestinBoundEmail1()
	{
		dataSetup();
		// create a new email and envelope object
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		email.toAddresses = new string[] {'test3@lws.com','test@lws2.com'};
		email.ccAddresses = new string[] {contact1.Email_Address__c,contact2.Email_Address__c};

		// setup the data for the email
		email.subject = 'Air India1 aka airIndia,airchina name in testing for subject with more than 30 characters';
		email.fromAddress = 'test@lws.com';
		email.plainTextBody = 'email body\n2225256325\nTitle';

		// add an Binary attachment
		Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
		attachment.body = blob.valueOf('my attachment text');
		attachment.fileName = 'textfileone.txt';
		attachment.mimeTypeSubType = 'text/plain';
		email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

		// add an Text atatchment

		Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
		attachmenttext.body = 'my attachment text';
		attachmenttext.fileName = 'textfiletwo3.txt';
		attachmenttext.mimeTypeSubType = 'texttwo/plain';
		email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };

		// call the email service class and test it with the data in the testMethod
		LeaseWorksEmailHandler testInbound = new LeaseWorksEmailHandler();
		testInbound.handleInboundEmail(email, env);
	}

	static testMethod void TestAerDataServiceBatch()
	{
		LeaseWorksSettings__c lwSettings = new LeaseWorksSettings__c();
		lwSettings.setupOwnerId = UserInfo.getUserId();
		lwSettings.Package_Prefix__c = '';
		lwSettings.Token__c       = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwSettings.EndPointUrl__c  = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
		insert lwSettings;

		setupRecord = new Lessor__c(Name='Emirates',Phone_Number__c='999999999',Email_Address__c='test@lws.com',Lease_Logo__c='https://www.logo.com',Address_Street__c='House No.1240,Sector-29',City__c='Faridabad',Zip_Code__c='121008',Country__c='India');
		insert setupRecord;

		LeaseWareSeededData.SeedLWServiceSetup();

		ServiceSetting__c serviceSettings = new ServiceSetting__c();
		serviceSettings.Name = 'Aerdata';
		serviceSettings.Token__c = 'abcededeededededededed';
		serviceSettings.EndPointUrl__c  = 'https://dashboard.aerdata.com/CMS-API/v1/';
		serviceSettings.CustomerSlug__c = 'customerSlug';
		insert serviceSettings;

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());

		Test.startTest();
		ID batchprocessid = Database.executeBatch(new AerDataServiceBatch(),1);       // running a batch with batch size 1
		Test.stopTest();

		System.schedule('AerDataServiceBatchJob', '0 15 * * * ?', new AerDataServiceBatch());

	}


	static testMethod void TestAerDataService()
	{
		ServiceSetting__c serviceSettings = new ServiceSetting__c();
		serviceSettings.Name = 'Aerdata';
		serviceSettings.Token__c = 'abcededeededededededed';
		serviceSettings.EndPointUrl__c  = 'https://dashboard.aerdata.com/CMS-API/v1/';
		serviceSettings.CustomerSlug__c = 'customerSlug';
		insert serviceSettings;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);

		AerDataService cont = new AerDataService();

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());

		Test.startTest();
		Process_Request_Log__c ReqLog = new Process_Request_Log__c(Status__c ='Success',Type__c = 'Aerdata Service');
		AerDataService.refreshData(ReqLog);
		Test.stopTest();

		AerDataService.executeAerDataBatch();
	}

	@IsTest
	static void AerDataServiceCodeCoverageTest1(){

		ServiceSetting__c serviceSettings = new ServiceSetting__c();
		serviceSettings.Name = 'Aerdata';
		serviceSettings.Token__c = 'abcededeededededededed';
		serviceSettings.EndPointUrl__c  = 'https://dashboard.aerdata.com/CMS-API/v1/';
		serviceSettings.CustomerSlug__c = 'customerSlug';
		insert serviceSettings;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		AerDataService cont = new AerDataService();

		Test.startTest();

		AerDataService.displayContractResults();
		AerDataService.displayMajorComponent();


		Test.stopTest();
	}

	@IsTest
	static void AerDataServiceCodeCoverageTest2(){

		ServiceSetting__c serviceSettings = new ServiceSetting__c();
		serviceSettings.Name = 'Aerdata';
		serviceSettings.Token__c = 'abcededeededededededed';
		serviceSettings.EndPointUrl__c  = 'https://dashboard.aerdata.com/CMS-API/v1/';
		serviceSettings.CustomerSlug__c = 'customerSlug';
		insert serviceSettings;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());
		AerDataService cont = new AerDataService();

		Test.startTest();
		List<AerDataService.ContractItemResource > listContractItem = new List<AerDataService.ContractItemResource >();
		Process_Request_Log__c ReqLog = new Process_Request_Log__c(Status__c ='Success',Type__c = 'Aerdata Service');
		AerDataService.refreshDataUsingAssetsContracts(ReqLog);
		AerDataService.LeaseItem leaseItem = new AerDataService.LeaseItem();
		leaseItem.contractualDeliveryDate ='';
		leaseItem.contractualRedeliveryDate = '';
		leaseItem.leaseTypeLegal = '';
		leaseItem.location = '';

		AerDataService.AssemblyItem assemblyItem = new AerDataService.AssemblyItem();
		assemblyItem.Name = '';
		assemblyItem.Description = '';
		assemblyItem.manufacturer = new AerDataService.ManufacturerInfo();
		assemblyItem.SerialNumber = '';
		assemblyItem.TSN = 0.0;
		assemblyItem.CSN = 0.0;
		assemblyItem.CSO = 0.0;

		AerDataService.ContractItemResource contractItem = new AerDataService.ContractItemResource();
		contractItem.contractType = 'test1';
		contractItem.leaseStructure = 'testLease';
		contractItem.lastModificationDateUtc = 'testDate';
		contractItem.identifier ='';
		contractItem.assetId ='';
		contractItem.contract = new AerDataService.ContractResource();
		contractItem.totalMrFundAmounts = new List<AerDataService.BalanceItem>();
		listContractItem.add(contractItem);

		AerDataService.AssetContractResource contractAssetItem = new AerDataService.AssetContractResource();
		contractAssetItem.assetModel = '';
		contractAssetItem.assetStatus= new AerDataService.AssetStatus();
		contractAssetItem.comment = '';
		contractAssetItem.description = '';
		contractAssetItem.contractItems = listContractItem;
		contractAssetItem.isEnabled = true;
		contractAssetItem.isReleased = true;
		contractAssetItem.dateOfManufacture = '';
		contractAssetItem.serialNumber = '';
		contractAssetItem.assetType = '';
		contractAssetItem.registration = '';
		contractAssetItem.identifier = '';
		contractAssetItem.slug ='';
		contractAssetItem.engineDesignation = '';
		contractAssetItem.name = '';
		contractAssetItem.portfolio = new AerDataService.CompanyResource();
		contractAssetItem.manufacturer = new AerDataService.CompanyResource();
		contractAssetItem.status = new AerDataService.AssetStatusResource();

		AerDataService.CompanyResource companyResource = new AerDataService.CompanyResource();
		companyResource.name = '';
		companyResource.customerSlug ='';
		companyResource.tradingAlias = new AerDataService.tradingAlias();

		AerDataService.ContractResource contractRes = new  AerDataService.ContractResource();
		contractRes.startDate ='';
		contractRes.endDate = '';
		contractRes.identifier ='';
		contractRes.isEnabled = true;
		contractRes.isReleased = true;
		contractRes.lastModificationDateUtc ='';
		contractRes.lessee =new AerDataService.CompanyResource();
		contractRes.lessor = new AerDataService.CompanyResource();
		contractRes.status =new AerDataService.LeaseStatus();
		contractRes.portfolio = new AerDataService.CompanyResource();
		contractRes.startDateLabel = '';
		Test.stopTest();

	}



	static testMethod void testRentScheduler()
	{
		dataSetup();
		Test.startTest();
		RentDueCheckScheduler rentCheck = new  RentDueCheckScheduler();
		ID batchprocessid = Database.executeBatch(rentCheck);
		Test.stopTest();

	}

	static testMethod void testThresholdScheduler()
	{
		dataSetup();
		Test.startTest();
		ThresholdCheckScheduler thrCheck = new ThresholdCheckScheduler();
		ID batchprocessid = Database.executeBatch(thrCheck);
		Test.stopTest();

	}
}