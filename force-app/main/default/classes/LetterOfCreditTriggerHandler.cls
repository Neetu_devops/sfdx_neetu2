public class LetterOfCreditTriggerHandler implements ITrigger {
    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'LetterOfCreditTriggerHandlerBefore';
    private final string triggerAfter = 'LetterOfCreditTriggerHandlerAfter';
    public LetterOfCreditTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('LetterOfCreditTriggerHandler.beforeInsert(+)');
		populateLOCName();
	    system.debug('LetterOfCreditTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
		before_update_call();
    	system.debug('LetterOfCreditTriggerHandler.beforeUpdate(+)');
   		populateLOCName();
    	system.debug('LetterOfCreditTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
        
    	system.debug('LetterOfCreditTriggerHandler.beforeDelete(+)');
    	setLocExpiryOnLease();
    	system.debug('LetterOfCreditTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert(){
        system.debug('LetterOfCreditTriggerHandler.afterInsert(+)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        setLocExpiryOnLease();
        try{
            CashSecurityDepositUtil.createLoCSdHistory((list<letter_of_credit__c>)trigger.new,true,null);
        }
        catch(DmlException ex){
            System.debug(ex);
            string errorMessage='';
            for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                // Process exception here
                errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
            }
            system.debug('errorMessage=='+errorMessage);
            for(letter_of_credit__c loc : (list<letter_of_credit__c>)trigger.new){
                loc.addError(errorMessage);
            }
        }
        system.debug('LetterOfCreditTriggerHandler.afterInsert(-)');
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LetterOfCreditTriggerHandler.afterUpdate(+)');
        setLocExpiryOnLease();
        try{
            CashSecurityDepositUtil.createLoCSdHistory((list<letter_of_credit__c>)trigger.new,false,(Map<Id,letter_of_credit__c>)trigger.oldMap);
        }
        catch(DmlException ex){
            System.debug(ex);
            string errorMessage='';
            for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                // Process exception here
                errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
            }
            system.debug('errorMessage=='+errorMessage);
            for(letter_of_credit__c loc : (list<letter_of_credit__c>)trigger.new){
                loc.addError(errorMessage);
            }
        }
        system.debug('LetterOfCreditTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LetterOfCreditTriggerHandler.afterDelete(+)');
    	
    	system.debug('LetterOfCreditTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LetterOfCreditTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('LetterOfCreditTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

    
    private void populateLOCName() {
        
        List<Letter_Of_Credit__c> newLOC = (List<Letter_Of_Credit__c>)trigger.new;
        Map<ID, Letter_Of_Credit__c> lOCOldmap = (Map<ID, Letter_Of_Credit__c>)trigger.oldMap;
        
        List<Letter_Of_Credit__c> locToUpdate = new List<Letter_Of_Credit__c>();
        Set<Id> lesseeIds = new Set<ID>();
        
        for(Letter_Of_Credit__c loc : newLOC) {
            if(lOCOldmap == null || 
               lOCOldmap.get(loc.Id).Lessee__c != loc.Lessee__c || lOCOldmap.get(loc.Id).LC_Number__c != loc.LC_Number__c) {
                   locToUpdate.add(loc);
                   
                   if(loc.Lessee__c != null) {
                       lesseeIds.add(loc.Lessee__c);
                   }
            }
        }
        if(locToUpdate.size() > 0) {
            
            Map<Id, Operator__c> lesseeMap ;
            if(lesseeIds.size() > 0) {
                lesseeMap = new Map<Id, Operator__c>([SELECT ID, Name FROM Operator__c WHERE ID IN :lesseeIds]);
            }
            
            for(Letter_Of_Credit__c loc : locToUpdate) {
                loc.Name = '';
                loc.Name += loc.LC_Number__c != null ? loc.LC_Number__c + ' ' : '';
                loc.Name += loc.Lessee__c != null ? lesseeMap.get(loc.Lessee__c).Name : '';
            }
        }
    }
    
    private void before_update_call() {
        
        //restricting user to edit LoC Amount when LoC is active.
        for (Letter_Of_Credit__c locRecord : (List<Letter_Of_Credit__c>)trigger.new ) {
            if(locRecord.LC_Amount_LC_Currency__c != ((Letter_Of_Credit__c)trigger.oldMap.get(locRecord.Id)).LC_Amount_LC_Currency__c
                && locRecord.Status__c == 'Active') {
                locRecord.addError('The Letter of Credit Amount cannot be changed once the status is \'Active\'.');
            }
        }
    }

    /***
     * set the Loc expiry date on lease to the earliest loc expiry date 
     * called from :after update , after insert,before delete
     */
    private void setLocExpiryOnLease(){
        system.debug('setLocExpiryOnLease(+)');
        set<id> leaseIds = new set<id>();
        set<id> deletedLocs = new set<id>();
        letter_of_credit__c oldLoc = new letter_of_credit__c();
        list<letter_of_credit__c> newList = (list<letter_of_credit__c>)(trigger.isDelete?trigger.old:trigger.new);

        for(letter_of_credit__c loc:newList){
            if((trigger.isInsert || trigger.isdelete) && 
                ('All Lease Obligation'.equals(loc.LC_Coverage_Type__c) || 'Security Deposit'.equals(loc.LC_Coverage_Type__c))){
                leaseIds.add(loc.lease__c);
                if(trigger.isDelete){deletedLocs.add(loc.id);}
            }
            else if(trigger.isUpdate){
                oldLoc = (letter_of_credit__c)trigger.oldmap.get(loc.id);
                if(oldloc.status__c!=loc.status__c || oldLoc.LC_Coverage_Type__c != loc.LC_Coverage_Type__c || oldLoc.expiry_date__c !=loc.expiry_date__c){
                    leaseIds.add(loc.lease__c);
                }
            }
            
        }
        if(leaseIds.size()>0){
            list<lease__c> leaseList = [select id,Letter_of_Credit_Expiry__c,
                                        (select id,expiry_date__c from Letter_Of_Credit__r where status__c ='Active' and id NOT IN :deletedLocs and LC_Coverage_Type__c IN ('All Lease Obligation','Security Deposit') order by expiry_date__c )
                                         from lease__c where id in :leaseIds];

            for(lease__c curLease:leaseList){
               if(curLease.letter_of_credit__r!=null && curLease.letter_of_credit__r.size()>0){
                    curLease.Letter_of_Credit_Expiry__c = curLease.letter_of_credit__r[0].expiry_date__c;
               }
               else{
                    curLease.Letter_of_Credit_Expiry__c = null;
               }
            }
            if(!leaseList.isEmpty()){
                    LeaseWareUtils.TriggerDisabledFlag=true;
                    update leaseList;
                    LeaseWareUtils.TriggerDisabledFlag=false;
            }
        }
        system.debug('setLocExpiryOnLease(-)');
    }
}