public class InterpolationRateTableController {
    
    @auraEnabled
    public static RatioTableInfoWrapper loadData(Id recordId){
    
        String prefix = LeaseWareUtils.getNamespacePrefix();
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        
        if(sObjName == prefix + 'Ratio_Table__c'){
            
            return getInterpolationRateData(recordId, null, null,6);// the ratio table cmp on the Ratio Table will always have rounding and precision of 6 decimal palces
            
        }else if(sObjName == prefix + 'MR_Rate__c'){
        
            List<MR_Rate__c> mrRateList = [Select id, Assembly_MR_Info__c, MR_Rate_Start_Date__c,
                                           Assembly_MR_Info__r.Rounding_Decimals_Escalated_MR_Rate__c,
                                           Assembly_MR_Info__r.Rounding_Method_On_Esc_MR_Rate__c,
                                           Assembly_MR_Info__r.Ratio_Table__c 
                                           from MR_Rate__c Where Id =: recordId];
            
            List<MR_Rate__c> allMRRateList = [Select id, Assembly_MR_Info__c, MR_Rate_Start_Date__c, 
                                              Esc_Per__c, Esc_Factor_F__c, MR_Rate_End_Date__c,
                                              Assembly_MR_Info__r.Ratio_Table__c from MR_Rate__c 
                                              Where Assembly_MR_Info__c =: mrRateList[0].Assembly_MR_Info__c 
                                              AND MR_Rate_Start_Date__c <=: mrRateList[0].MR_Rate_Start_Date__c
                                              order by MR_Rate_Start_Date__c];
            Decimal NewEscFactor = 1 ;
            
            for(MR_Rate__c curMRRec : allMRRateList){
                if(curMRRec.Esc_Per__c != null && curMRRec.Esc_Per__c != 0){
                    NewEscFactor += NewEscFactor * curMRRec.Esc_Per__c/100;
                }
            }
            
            Integer roundingNum  =  (mrRateList[0].Assembly_MR_Info__r.Rounding_Decimals_Escalated_MR_Rate__c == null|| 'None'.equals(mrRateList[0].Assembly_MR_Info__r.Rounding_Decimals_Escalated_MR_Rate__c) )?2:Integer.ValueOf(mrRateList[0].Assembly_MR_Info__r.Rounding_Decimals_Escalated_MR_Rate__c);
            return getInterpolationRateData(mrRateList[0].Assembly_MR_Info__r.Ratio_Table__c, NewEscFactor, mrRateList[0].Assembly_MR_Info__r.Rounding_Method_On_Esc_MR_Rate__c,roundingNum);
        }else if(sObjName == prefix+'Utilization_Report_List_Item__c'){
            List<Utilization_Report_List_Item__c> assemblyUtilizationRecord = [Select id, Effective_FH_FC_Ratio__c, Effective_Derate__c,
                                                                               Effective_Escalation_Factor__c, 
                                                                               Assembly_MR_Info__r.Rounding_Method_On_Esc_MR_Rate__c,
                                                                               Assembly_MR_Info__r.Rounding_Decimals_Escalated_MR_Rate__c
                                                                   from Utilization_Report_List_Item__c 
                                                                               Where Id = : recordId ]; 
            
            Integer rndgFctInterpolated = Integer.valueOf(assemblyUtilizationRecord[0].Assembly_MR_Info__r.Rounding_Decimals_Escalated_MR_Rate__c == null ? '2' : assemblyUtilizationRecord[0].Assembly_MR_Info__r.Rounding_Decimals_Escalated_MR_Rate__c);
            
            List<ContentDocumentLink> conDocList = [SELECT ContentDocumentId, LinkedEntityId, Id
                                                    FROM ContentDocumentLink
                                                    where LinkedEntityId =: recordId
                                                    AND ContentDocument.Title = 'LW_UR_RatioTable_Snapshot'];
            if(conDocList.size() > 0){
                List<ContentVersion> contentList = [select versionData from contentVersion
                                                    where ContentDocumentId =: conDocList[0].ContentDocumentId];
              if(contentList.size() > 0){
                  return getDataFromJSON(contentList[0].versionData.toString(),
                                         assemblyUtilizationRecord[0].Effective_FH_FC_Ratio__c,
                                         assemblyUtilizationRecord[0].Effective_Derate__c,
                                         assemblyUtilizationRecord[0].Effective_Escalation_Factor__c,
                                         assemblyUtilizationRecord[0].Assembly_MR_Info__r.Rounding_Method_On_Esc_MR_Rate__c,
                                         rndgFctInterpolated
                                        );
                  
              }
       	   }
        }
        return null;
    }   
    
    private static RatioTableInfoWrapper getDataFromJSON(String ratioTableRecord, Decimal hcRatio, Decimal deratePercent, Decimal escFactor, String roundUpOrDown, Integer roundingDecimal){
        
        RatioTableJSONContainer interpolationTableValues = (RatioTableJSONContainer)System.JSON.deserialize(ratioTableRecord,RatioTableJSONContainer.class);
        
        List<Ratio_Table__c> ratioTableList = [Select Id, Name From Ratio_Table__c Where Id =: interpolationTableValues.RatioTable];
        
        RatioTableInfoWrapper ratioTableInfo = new RatioTableInfoWrapper();
        
        List<InterpolationTableWrapper> interPolationTableList = new List<InterpolationTableWrapper>();
        
        Set<Decimal> rowLabelSet = new Set<Decimal>();
        List<Decimal> hCRatioList = new List<Decimal>();
        
        Map<String, InterpolationJSONWrapper> hCRationMap = new Map<String, InterpolationJSONWrapper>();
        Map<String, List<InterpolationDataWrapper>> dataMap = new Map<String, List<InterpolationDataWrapper>>();
        dataMap.put('H:C Ratio',new List<InterpolationDataWrapper>());
        
        for(InterpolationJSONWrapper interpolationRecord : interpolationTableValues.InterpolationList){
            
            rowLabelSet.add(interpolationRecord.Derate_Percentage);
            
            if(!hCRatioList.contains(interpolationRecord.FH_To_FC_Ratio)){
                hCRatioList.add(interpolationRecord.FH_To_FC_Ratio);
                //dataMap.get('H:C Ratio').add(new InterpolationDataWrapper(null, interpolationRecord.FH_To_FC_Ratio, null));
            }
            String key = String.valueOf(interpolationRecord.Derate_Percentage+'-'+interpolationRecord.FH_To_FC_Ratio);
            hCRationMap.put(key,interpolationRecord);
        }
        
        hCRatioList.sort();
       
        for(Decimal columnLabel : hCRatioList){
            dataMap.get('H:C Ratio').add(new InterpolationDataWrapper(null, columnLabel, null));
        }
       
        List<Decimal> rowLabelList = new List<Decimal>(rowLabelSet);
        rowLabelList.sort();
        Decimal hcRatioTest;
        Decimal deratePercentTest;
		
		//to consider derate as 0 if its null.
		if(deratePercent == null){
			deratePercent = 0;
		}
		
        if(hcRatio != null && deratePercent != null){
            hcRatioTest = hcRatio;
            deratePercentTest = deratePercent;
        }
        integer j = 0;
        
        for(Decimal rowLabel : rowLabelList){
            integer i = 0;
            for(Decimal columnLabel : hCRatioList){
               
                    String bgColor;
                      if(hcRatioTest == columnLabel ||
                            (i + 1 < hCRatioList.size() && hcRatioTest > columnLabel && hcRatioTest < hCRatioList[i + 1]) ||
                            (i - 1 > -1 && hcRatioTest < columnLabel && hcRatioTest > hCRatioList[i - 1]) ||
                            (i + 1== hCRatioList.size() && hcRatioTest > columnLabel) ||
                            (i == 0 && hcRatioTest < columnLabel)){
                            
                                if(deratePercentTest == rowLabel ||
                                (j + 1 < rowLabelList.size() && deratePercentTest > rowLabel && deratePercentTest < rowLabelList[j + 1]) ||
                                (j - 1 > -1 && deratePercentTest < rowLabel && deratePercentTest > rowLabelList[j - 1]) ||
                                (j + 1== rowLabelList.size() && deratePercentTest > rowLabel) ||
                                (j == 0 && deratePercentTest < rowLabel)) bgColor = '#b7d4e4';
                                
                       }
           if(hCRationMap.containsKey(String.valueOf(rowLabel+'-'+columnLabel))){
                    
                    Decimal rate;
                    if(escFactor != null){
                        //escFactor = escFactor.setScale(roundingDecimal);
                        rate = hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Dollars_per_Flight_Hour != null ? hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Dollars_per_Flight_Hour * escFactor : null;
                         if(rate != null ){
                            rate = rate.setScale(roundingDecimal, leasewareUtils.getRoundingMode(roundUpOrDown));
                        }
                    }
                    if(!dataMap.containsKey(String.valueOf(rowLabel))){
                        dataMap.put(String.valueOf(rowLabel),new List<InterpolationDataWrapper>());
                    }
                    dataMap.get(String.valueOf(rowLabel)).add(new InterpolationDataWrapper(hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Id, rate != null ? rate  : hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Dollars_per_Flight_Hour, bgColor));
                    
                }else{
                    if(!dataMap.containsKey(String.valueOf(rowLabel))){
                        dataMap.put(String.valueOf(rowLabel),new List<InterpolationDataWrapper>());
                    }
                    dataMap.get(String.valueOf(rowLabel)).add(new InterpolationDataWrapper(null, null, bgColor));
                }
                i++;
            }
            j++;
        }
        
        for(String rowLabel: dataMap.keySet()){
            InterpolationTableWrapper ratioObj = new InterpolationTableWrapper();
            ratioObj.interPolationField = rowLabel;
            ratioObj.interpolationDataList = dataMap.get(rowLabel);
            interPolationTableList.add(ratioObj);
         }
         if(escFactor != null){
                ratioTableInfo.escFactor = (Decimal)escFactor;
            }
        
        ratioTableInfo.ratioTableName = ratioTableList[0].Name;
        ratioTableInfo.ratioTableId = ratioTableList[0].Id;
        ratioTableInfo.isShownInAssemblyUtilization = true;
        ratioTableInfo.ratioTableColumnSize = dataMap.get('H:C Ratio').size() + 1;
        ratioTableInfo.interpolationTableValues = interPolationTableList;
        
        return ratioTableInfo;
    }
    
    private static RatioTableInfoWrapper getInterpolationRateData(String ratioTableId, Decimal escFactor, String roundUpOrDown,Integer rdecimals){
        
        List<Derate_Adjustment__c>  interpolationAdjustmentList = [Select Id, Ratio_Table__r.Name, FH_To_FC_Ratio__c,
                                                                   Derate_Percentage__c, Dollars_per_Flight_Hour__c 
                                                                   from Derate_Adjustment__c 
                                                                   where Ratio_Table__c =: ratioTableId order by FH_To_FC_Ratio__c];
        if(interpolationAdjustmentList.size() > 0){
            RatioTableInfoWrapper ratioTableInfo = new RatioTableInfoWrapper();
            
            List<InterpolationTableWrapper> interPolationTableList = new List<InterpolationTableWrapper>();
            
            Set<Decimal> rowLabelSet = new Set<Decimal>();
            List<Decimal> hCRatioList = new List<Decimal>();
            
            Map<String, Derate_Adjustment__c> hCRationMap = new Map<String, Derate_Adjustment__c>();
            Map<String, List<InterpolationDataWrapper>> dataMap = new Map<String, List<InterpolationDataWrapper>>();
            dataMap.put('H:C Ratio',new List<InterpolationDataWrapper>());
            
            for(Derate_Adjustment__c interpolationRecord : interpolationAdjustmentList){
                
                rowLabelSet.add(interpolationRecord.Derate_Percentage__c);
                
                if(ratioTableInfo.ratioTableName == null){
                    ratioTableInfo.ratioTableName = interpolationRecord.Ratio_Table__r.Name;
                    ratioTableInfo.ratioTableId = interpolationRecord.Ratio_Table__c;
                }
                
                if(!hCRatioList.contains(interpolationRecord.FH_To_FC_Ratio__c)){
                    hCRatioList.add(interpolationRecord.FH_To_FC_Ratio__c);
                    dataMap.get('H:C Ratio').add(new InterpolationDataWrapper(null, interpolationRecord.FH_To_FC_Ratio__c, null));
                }
                String key = String.valueOf(interpolationRecord.Derate_Percentage__c+'-'+interpolationRecord.FH_To_FC_Ratio__c);
                hCRationMap.put(key,interpolationRecord);
            }
            
            List<Decimal> rowLabelList = new List<Decimal>(rowLabelSet);
            rowLabelList.sort();
            
            
            for(Decimal rowLabel : rowLabelList){
                
                for(Decimal columnLabel : hCRatioList){
                    if(hCRationMap.containsKey(String.valueOf(rowLabel+'-'+columnLabel))){
                        
                        Decimal rate;
                        if(escFactor != null){
                            escFactor = escFactor.setScale(6);
                            rate = hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Dollars_per_Flight_Hour__c != null ? hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Dollars_per_Flight_Hour__c * escFactor : null;
                            if(rate != null ){
                                //conveting the rate to decimal places specified on SR and scaling to  a precision of 6
                                rate = (rate.setScale(rdecimals,leasewareUtils.getRoundingMode(roundUpOrDown))).setScale(6);
							}
                        }
                        
                        if(!dataMap.containsKey(String.valueOf(rowLabel))){
                            dataMap.put(String.valueOf(rowLabel),new List<InterpolationDataWrapper>());
                        }
                        dataMap.get(String.valueOf(rowLabel)).add(new InterpolationDataWrapper(hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Id, rate != null ? rate  : hCRationMap.get(String.valueOf(rowLabel+'-'+columnLabel)).Dollars_per_Flight_Hour__c, null));
                        
                    }else{
                        if(!dataMap.containsKey(String.valueOf(rowLabel))){
                            dataMap.put(String.valueOf(rowLabel),new List<InterpolationDataWrapper>());
                        }
                        dataMap.get(String.valueOf(rowLabel)).add(new InterpolationDataWrapper(null, null, null));
                    }
                }
            }
            
            for(String rowLabel: dataMap.keySet()){
                InterpolationTableWrapper ratioObj = new InterpolationTableWrapper();
                ratioObj.interPolationField = rowLabel;
                ratioObj.interpolationDataList = dataMap.get(rowLabel);
                interPolationTableList.add(ratioObj);
            }
            if(escFactor != null){
                ratioTableInfo.escFactor = (Decimal)escFactor;
            }
           /* if(roundUpOrDown!=null){
                ratioTableInfo.roundUpOrDown = roundUpOrDown;
            }*/
            
            ratioTableInfo.ratioTableColumnSize = dataMap.get('H:C Ratio').size();
            ratioTableInfo.interpolationTableValues = interPolationTableList;
            
            return ratioTableInfo;
        }
        return null;
    }
    
    @auraEnabled
    public static void saveInterpolationRateData(String interpolationDataList){
        
        List<InterpolationDataWrapper> interpolationTableValues = (List<InterpolationDataWrapper>)System.JSON.deserialize(interpolationDataList,List<InterpolationDataWrapper>.class);
        List<Derate_Adjustment__c>  interpolationAdjustmentListToUpdate = new List<Derate_Adjustment__c>();
        
        for(InterpolationDataWrapper obj : interpolationTableValues){
            
            Derate_Adjustment__c derateAdjustmentRecord = new Derate_Adjustment__c(Id = obj.id);
            derateAdjustmentRecord.Dollars_per_Flight_Hour__c = (Decimal)obj.rate;
            
            interpolationAdjustmentListToUpdate.add(derateAdjustmentRecord);
        }
        
        if(interpolationAdjustmentListToUpdate.size() > 0){
            update interpolationAdjustmentListToUpdate;
        }
    }
    
    public class RatioTableInfoWrapper{
        @AuraEnabled public String ratioTableName{get;set;}
        @AuraEnabled public Id ratioTableId{get;set;}
        @AuraEnabled public Integer ratioTableColumnSize{get;set;}
        @AuraEnabled public Decimal escFactor{get;set;}
        //@AuraEnabled public String roundUpOrDown{get; set;}
        @AuraEnabled public Boolean isShownInAssemblyUtilization{get; set;}
        @auraEnabled public List<InterpolationTableWrapper> interpolationTableValues {get;set;}
    }
    public class InterpolationTableWrapper{
        @AuraEnabled public String interPolationField{get;set;}
        @auraEnabled public List<InterpolationDataWrapper> interpolationDataList {get;set;}
    }
    
    public class InterpolationDataWrapper {
        @auraEnabled public Id id {get;set;}
        @auraEnabled public String bgColor {get;set;}
        @auraEnabled public Decimal rate {get;set;}
        
        public InterpolationDataWrapper (String id, Decimal rate, String bgColor){
            this.id =  id;
            this.rate =  rate;
            this.bgColor =  bgColor;
        }
    }
    
    public class RatioTableJSONContainer {
        public String RatioTable;
        public String AssemblyUtilization;
        public String ModifiedBy;
        public DateTime ModifiedDate;
        public Double Version;
        public List<InterpolationJSONWrapper> InterpolationList;
        
        public RatioTableJSONContainer(){
            InterpolationList = new List<InterpolationJSONWrapper>();
        }
    }
    
    public class InterpolationJSONWrapper {
        public String Id {get;set;}
        public Decimal FH_To_FC_Ratio {get;set;}
        public Decimal Derate_Percentage {get;set;}
        public Decimal Dollars_per_Flight_Hour {get;set;}
    }
}