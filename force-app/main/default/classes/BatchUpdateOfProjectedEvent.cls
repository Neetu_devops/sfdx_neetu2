public class BatchUpdateOfProjectedEvent implements Database.Batchable<sObject>{
    
    Set<Id> mpeList = new Set<Id>();
    Set<Id> assemblyList = new Set<Id>();
    public BatchUpdateOfProjectedEvent(Set<Id> listOfAssembly,Set<Id> listOfMPE)
    {
       assemblyList = listOfAssembly;
       mpeList = listOfMPE;
     }
       
   public Database.QueryLocator start(Database.BatchableContext bc) {
        
       if(assemblyList !=null){	
             return Database.getQueryLocator(
               [select id,Name from Assembly_Event_Info__c where Constituent_Assembly__c IN: assemblyList]
               );
       }
       else {
           return Database.getQueryLocator(
               [select id,Name from Assembly_Event_Info__c where Maintenance_Program_Event__c IN: mpeList]
                   );
       }
     }
       
   public void execute(Database.BatchableContext BC, List<Assembly_Event_Info__c> projEventList)
   {
     
      try{
          update projEventList;
      }catch(Exception e){
           LeaseWareUtils.createExceptionLog(e, 'Exception while updating projected event', null, null,'Projected Event Update Batch',true);
      }
   }
   public void finish(Database.BatchableContext BC) {
     
   }
}