@isTest
public class TestSpecMassTriggerHandler {

        
    @testSetup
     static void CRUD_massBalanceRecord(){
        
		Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Test',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Maximum_Operational_Take_Lbs__c= 144455,   
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
        Contractual_Status__c = 'Managed'    
        );
        LeaseWareUtils.clearFromTrigger();
        insert aircraft;
        
        
       Spec_Mass_and_Balance__c rec1=new Spec_Mass_and_Balance__c (Period__c='Last Delivered',Weight_Type__c='MTOW (Maximum Takeoff Weight)',Asset__c=aircraft.id,Unit__c='lbs',Weight__c=40000);
		LeaseWareUtils.clearFromTrigger();
        insert rec1;      
         
        //Read
        Spec_Mass_and_Balance__c massRec = [select id,name from Spec_Mass_and_Balance__c  where id =:rec1.id];
        
        //Update
        massRec.Weight__c=40500 ;
        LeaseWareUtils.clearFromTrigger(); 
        Update massRec;

        //Delete
        LeaseWareUtils.clearFromTrigger(); 
        delete massRec;            
 
    }
    
    
    static testMethod void UpdateWeightonAsset() {
             
            List<Spec_Mass_and_Balance__c> massList =new List<Spec_Mass_and_Balance__c>();
            List<Spec_Mass_and_Balance__c> massList1 =new List<Spec_Mass_and_Balance__c>();
            Aircraft__c assetRec = [select id from Aircraft__c];
           //Create  Adjusted Interval(Ratio) 
           Spec_Mass_and_Balance__c massRec1=new Spec_Mass_and_Balance__c();
           massRec1.Period__c='Last Delivered';
           massRec1.Weight_Type__c='MTOW (Maximum Takeoff Weight)';
           massRec1.Asset__c=assetRec.id;
           massRec1.Unit__c='lbs';
           massRec1.Weight__c=40000;
           massList1.add(massRec1);
           
        
           Spec_Mass_and_Balance__c massRec2=new Spec_Mass_and_Balance__c();
           massRec2.Period__c='Current';
           massRec2.Weight_Type__c='MTOW (Maximum Takeoff Weight)';
           massRec2.Asset__c=assetRec.id;
           massRec2.Unit__c='lbs';
           massRec2.Weight__c=40500;
           //LeaseWareUtils.clearFromTrigger(); 
           massList1.add(massRec2);
        
           Spec_Mass_and_Balance__c massRec3=new Spec_Mass_and_Balance__c();
           massRec3.Period__c='Current';
           massRec3.Weight_Type__c='MLW (Maximum Landing Weight)';
           massRec3.Asset__c=assetRec.id;
           massRec3.Unit__c='lbs';
           massRec3.Weight__c=40000;
           massList1.add(massRec3);
           LeaseWareUtils.clearFromTrigger(); 
           insert  massList1;
        
           massList=[Select id,Weight__c from Spec_Mass_and_Balance__c where Weight_Type__c='MTOW (Maximum Takeoff Weight)'] ;
               System.debug('Number of weight records #'+ massList.size());
         for (Spec_Mass_and_Balance__c rec : massList){
             rec.Weight__c=rec.Weight__c+10000;
        }
        LeaseWareUtils.clearFromTrigger(); 
           update  massList;   
        Aircraft__c aircraftRec = [Select id ,Maximum_Operational_Take_Lbs__c,MTOW_Leased__c,Maximum_Landing_Weight_Operational__c from Aircraft__c where id=:assetRec.id ];
        System.debug('Aircraft # '+ aircraftRec);
        System.assertEquals(50500, aircraftRec.Maximum_Operational_Take_Lbs__c,'MTOW(Operational)Weight on Asset & related list do not match');
        System.assertEquals(50000, aircraftRec.MTOW_Leased__c,'MTOW(Last Delivered) Weight on Asset & related list do not match');
        System.assertEquals(40000, aircraftRec.Maximum_Landing_Weight_Operational__c,'MLW(Operational) weight on Asset & related list do not match');
    } 
}