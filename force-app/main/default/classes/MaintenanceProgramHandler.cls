public class MaintenanceProgramHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'MaintenanceProgramHandlerBefore';
    private final string triggerAfter = 'MaintenanceProgramHandlerAfter';
    public MaintenanceProgramHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('MaintenanceProgramHandler.beforeInsert(+)');
	    system.debug('MaintenanceProgramHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
		
    	system.debug('MaintenanceProgramHandler.beforeUpdate(+)');

    	system.debug('MaintenanceProgramHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('MaintenanceProgramHandler.beforeDelete(+)');
    	
    	
    	system.debug('MaintenanceProgramHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('MaintenanceProgramHandler.afterInsert(+)');
    	
    	system.debug('MaintenanceProgramHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('MaintenanceProgramHandler.afterUpdate(+)');
    	
		Maintenance_Program__c[] listNew = (list<Maintenance_Program__c>)trigger.new ;
		map<string,string> mapMPToYear = new map<string,string>();
		Maintenance_Program__c OldRec;
		for(Maintenance_Program__c curMP:listNew){
			OldRec = (Maintenance_Program__c)trigger.oldMap.get(curMP.Id);
			if(curMP.Current_Catalog_Year__c!=null && OldRec.Current_Catalog_Year__c!=curMP.Current_Catalog_Year__c){
				mapMPToYear.put(curMP.Id,curMP.Current_Catalog_Year__c);
			}
		}
    	if(mapMPToYear.size()>0){
    		
    		Maintenance_Program_Event__c[] MPEList=[select Id,Maintenance_Program__c,Maintenance_Program__r.Current_Catalog_Year__c 
    									,Assembly__c
    									,(select Name,Never_Exceed_Period_Cycles__c,Never_Exceed_Period_Hours__c,Never_Exceed_Period_Months__c
    										,Interval_2_Cycles__c,Interval_2_Hours__c,Interval_2_Months__c,Event_Cost__c 
    										from Maintenance_Program_Event_Catalog__r
    										where name in :mapMPToYear.values())
    									from Maintenance_Program_Event__c
    									where Maintenance_Program__c in :mapMPToYear.Keyset()];
    		Maintenance_Program_Event_Catalog__c tempCat;
    		for(Maintenance_Program_Event__c curMPE:MPEList){
    			tempCat =null;
    			for(Maintenance_Program_Event_Catalog__c curCat:curMPE.Maintenance_Program_Event_Catalog__r){
    				if(curMPE.Maintenance_Program__r.Current_Catalog_Year__c==curCat.Name)tempCat=curCat;
    			}
                // MPE Event Cost Escalation - Start
                // Removing the below validation to allow updation of current cataloy year in MP even
                // if a MPE catalog record doesnt exist.
    			/*if(tempCat==null){
    				listNew[0].addError('Catalog year (' + curMPE.Maintenance_Program__r.Current_Catalog_Year__c + ') not found for ' +curMPE.Assembly__c + ' .');
    			}else*/
                if(tempCat != null)
                {
                //MPE Event Cost Escalation - End
					curMPE.Never_Exceed_Period_Cycles__c= tempCat.Never_Exceed_Period_Cycles__c;
					curMPE.Never_Exceed_Period_Hours__c= tempCat.Never_Exceed_Period_Hours__c;
					curMPE.Never_Exceed_Period_Months__c= tempCat.Never_Exceed_Period_Months__c;
					curMPE.Interval_2_Cycles__c= tempCat.Interval_2_Cycles__c;
					curMPE.Interval_2_Hours__c= tempCat.Interval_2_Hours__c;
					curMPE.Interval_2_Months__c= tempCat.Interval_2_Months__c;
					curMPE.Event_Cost__c= tempCat.Event_Cost__c;    				
    			}
    		}
            try{
                if(MPEList.size()>0)update MPEList;
            }catch(System.DmlException ex){
                listnew[0].addError(ex.getDmlMessage(0));
            }
            catch(Exception ex){
                listnew[0].addError(ex);
            } 
    	}
    	
    	
    	
    	system.debug('MaintenanceProgramHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	system.debug('MaintenanceProgramHandler.afterDelete(+)');
    	
    	
    	system.debug('MaintenanceProgramHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	system.debug('MaintenanceProgramHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('MaintenanceProgramHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


}