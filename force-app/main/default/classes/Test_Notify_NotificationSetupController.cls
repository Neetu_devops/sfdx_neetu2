@isTest
public class Test_Notify_NotificationSetupController {
    
    @TestSetup
    public static void testData() {
        
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Lease__c lease = new Lease__c();
        lease.Name = 'Test';
        Date startDate = System.today().addDays(-20);
        Date endDate = System.today().addDays(-10);
        lease.Lease_Start_date_new__c  = startDate;
        lease.Lease_end_date_new__c  = endDate;
       
        insert lease;
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        Profile prof = [SELECT Id FROM profile WHERE name = 'System Administrator'];
        User tuser = new User(firstname = 'Test',
                              lastName = 'Name',
                              email = uniqueName + '@test' + orgId + '.org',
                              Username = uniqueName + '@test' + orgId + '.org',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = uniqueName.substring(18, 23),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = prof.Id);
        insert tuser;
        
        System.runAs(tuser){
            Notify_NotificationSetupController.checkNotificationSetting();
            
            Notification_Setting__c nset = new Notification_Setting__c();
            nset.Name = 'SetUp'; 
            nset.Filter_By_Namespace_Prefix__c='leaseworks';
            nset.Email_Footer__c = 'Owner of the Notification;Date and Time when the notification was sent out;Title of the notification and hyperlink to the related Notification Subscription record';
            insert nset;   
        }
        
        Notifications_Subscription__c ns = new Notifications_Subscription__c();
        ns.Field_API_Name__c = 'Bank_Account_Close_Date__c';
        ns.Field_Name__c = 'Resale Certificate Expiration Date';
        ns.Record_Filter_Criteria__c = 'AccountSource = \'Email\'';
        ns.Record_Filter_Criteria_Text__c = 'AccountSource = \'Email\'';
        ns.Message__c = 'Hello';
        ns.Notification_Type__c = 'On Date';
        ns.Notify_Days__c = 10;
        ns.Object_API_Name__c = 'Account';
        ns.Object_Name__c = 'Account';
        ns.Title__c = 'Test {{name}}';
        ns.UserIds__c = tuser.Id;
        ns.Users__c = 'Abc';
        ns.Notify_Day_Type__c = 'After';
        ns.Active__c = true;
        ns.Status__c = 'Running';
        ns.ownerId = tuser.Id;

        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns;
        
    }
    @isTest
    public static void testDeployMetadata() {
        
        Test.StartTest();
        Test.setMock(WebServiceMock.class, new Notify_MetadataServiceMock());
        Notify_NotificationSetupController.deployMetadata('','');
        Test.StopTest();
        
    }
    @isTest
    public static void testgetLoadData() {
        
        Notifications_Subscription__c ns = [SELECT Id, UserIds__c FROM Notifications_Subscription__c LIMIT 1];
        
        Notify_NotificationSetupController.getLoadData('');
        Notify_NotificationSetupController.getLoadData(ns.Id);
        Notify_NotificationSetupController.existingTriggerCheck('');
        Notify_NotificationSetupController.existingTriggerCheck('Account');
        Notify_NotificationSetupController.checkNotificationSetting();
        Notify_NotificationSetupController.validateQuery('Account', 'AccountSource=Email');
        Notify_NotificationSetupController.checkNameUpdateable('Account');
       
        
        User tuser = [SELECT Id, Name FROM User LIMIT 1];

        Notify_Utility.NotificationSetupWrapper nsw = new Notify_Utility.NotificationSetupWrapper();
        nsw.selectedObjectLabel = 'Account';
        nsw.selectedObjectName = 'Account';
        nsw.selectedObjectName = 'Account';
        nsw.selectedUserGroup = 'Kim Brown';
        nsw.selectedUserRecords = '[{"sObjectType":"User","name":"Kim Brown","id":"0051I0000048LZmQAM"}]';
        nsw.recordFilterCriteriaSize = 1000;
        nsw.recordFilterCriteria = 'AccountSource= \'Email\'';
        nsw.notifyDays = 10;
        nsw.notifyDayType = 'After';
        nsw.notificationType = 'On Date';
        nsw.messageBody = '<p><span style="font-size: 13px;">{{name}} Cert Is expiring Soon. Please Recharge.</span></p>';
        nsw.isActive = true;
        nsw.selectedUserRecords = tuser.id;
        nsw.fieldName = 'Resale Cert Expiration Date';
        nsw.fieldAPIName = 'Bank_Account_Close_Date__c';
        nsw.deploymentMsg = 'Running';
        nsw.selectedUserGroupIds = '00e1I000000S7RgQAK';
        nsw.title = 'Account Cert Is expiring Soon Test {{name}}';
        nsw.userEditAccess = false;
        
        Test.StartTest();
        Notify_NotificationSetupController.saveNotificationData('', JSON.serialize(nsw),false);
        Notify_NotificationSetupController.testNotificationData(JSON.serialize(nsw));
        Notify_NotificationSetupController.validateMergeFieldValues(nsw.messageBody,nsw.title,'Account');
        Test.StopTest();
        
    }
    
    @isTest
    static void testNotifcationData() {
        
        Aircraft__c aircraft = new Aircraft__c(
            Name = 'TestAircraft',
            MSN_Number__c = '0J54-E190-80972',
            Aircraft_Type__c = '175',
            TSN__c = 100,
            CSN__c = 100
        );
        insert aircraft;
        
        Lease__c lease = new Lease__c(name='Test Lease', aircraft__c=aircraft.Id, Lease_Start_Date_New__c= System.today(), Lease_End_Date_New__c = System.today().addDays(10));
        insert lease;
        
        User tuser = [SELECT Id, Name FROM User LIMIT 1];

        Notify_Utility.NotificationSetupWrapper nsw = new Notify_Utility.NotificationSetupWrapper();
        nsw.selectedObjectLabel = 'Lease';
        nsw.selectedObjectName = 'Lease__c';
        nsw.selectedUserGroup = 'Kim Brown';
        nsw.selectedUserRecords = '[{"sObjectType":"User","name":"Kim Brown","id":"0051I0000048LZmQAM"}]';
        nsw.recordFilterCriteriaSize = 1000;
        
        nsw.notifyDays = 0;
        nsw.notifyDayType = 'After';
        nsw.notificationType = 'On Create';
        nsw.messageBody = '<p><span style="font-size: 13px;">{{name}} ,{{aircraft__r.name}}</span></p>';
        nsw.isActive = true;
        nsw.selectedUserRecords = tuser.id;
        
        nsw.deploymentMsg = 'Running';
        nsw.selectedUserGroupIds = '00e1I000000S7RgQAK';
        nsw.title = 'Lease  {{name}}    {{aircraft__r.name}}';
        nsw.userEditAccess = false;
        
        Test.StartTest();
        Notify_NotificationSetupController.saveNotificationData('', JSON.serialize(nsw),false);
        Notify_NotificationSetupController.testNotificationData(JSON.serialize(nsw));
        Notify_NotificationSetupController.existingTriggerCheck('Lease__c');
        Test.StopTest();
        
    }
    
    @isTest
    public static void testNotifyScheduler() {
        
        Test.startTest();
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notifications_Subscription__c ns = new Notifications_Subscription__c();
        ns.Field_API_Name__c = 'Lease_End_Date_New__c';
        ns.Field_Name__c = 'Lease End Date';
        ns.Message__c = 'Lease is Expiring  . ';
        ns.Notification_Type__c = 'On Date';
        ns.Notify_Days__c = 5;
        ns.Object_API_Name__c = 'Lease__c';
        ns.Object_Name__c = 'Lease';
        ns.Title__c = 'Test {{name}}';
        ns.UserIds__c = tuser.Id;
        ns.Users__c = 'Abc';
        ns.Notify_Day_Type__c = 'Before';
        ns.Active__c = true;
        ns.Status__c = 'Running';
        ns.ownerId = tuser.Id;
        ns.Last_execution_date__c = null;
        
        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns;
        
        Date startDate = Date.newInstance(2001,1,1);
        Date endDate = System.today().adddays(-5);
        
        Lease__c leaseRec = new Lease__c(name='test lease',Lease_Start_date_new__c = startDate,Lease_end_date_new__c=endDate);
        insert leaseRec;
        Notify_NotificationScheduler sc = new Notify_NotificationScheduler();
        sc.setNotificationRecord(ns);
        sc.execute(null);
        Test.stopTest();
        
    }
    
    @isTest
    public static void testNotifySchedulerError() {
        
        Test.startTest();
        try {
            User tuser = [SELECT Id, Name FROM User LIMIT 1];
            
            Notifications_Subscription__c ns = new Notifications_Subscription__c();
            ns.Field_API_Name__c = 'Lease_End_Date_New__c';
            ns.Field_Name__c = 'Lease End Date';
            ns.Message__c = 'Lease is Expiring  . ';
            ns.Notification_Type__c = 'On Date';
            ns.Notify_Days__c = 5;
            ns.Object_API_Name__c = 'Lease__c';
            ns.Object_Name__c = 'Lease';
            ns.Title__c = 'Test {{name}}';
            ns.UserIds__c = tuser.Id;
            ns.Users__c = 'Abc';
            ns.Notify_Day_Type__c = 'Before';
            ns.Active__c = true;
            ns.Status__c = 'Running';
            ns.ownerId = tuser.Id;
            ns.Last_execution_date__c = null;
            
            Notify_Utility.setSourceOfCall('INTERNAL_API');
            insert ns;
            
            Notifications_Subscription__c nsRecord = [select Id ,Message__c from Notifications_Subscription__c limit 1];
            
            Date startDate = Date.newInstance(2001,1,1);
            Date endDate = System.today().adddays(-5);
            
            Lease__c leaseRec = new Lease__c(name='test lease',Lease_Start_date_new__c = startDate,Lease_end_date_new__c=endDate);
            insert leaseRec;
            
            Notify_NotificationScheduler sc = new Notify_NotificationScheduler();
            sc.setNotificationRecord(ns);
            
            sc.execute(null);
        }
        catch(Exception e) {
            System.debug('Exception expected as field does not exist.....');
        }
        Test.stopTest();
        
    }
    
    @isTest
    public static void testNSValidationInsertActive() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
           
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease__c';
            ns1.Notification_Type__c = 'On Create';
            ns1.Active__c = true;
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as active cannot be true');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationObjNameEmpty() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
           
            ns1.Object_API_Name__c = null;
            ns1.Object_Name__c = null;
            ns1.Active__c = false;
            ns1.Users__c = null;
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationOnDateApiNameEmpty() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
           
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Active__c = false;
            ns1.Users__c = null;
            ns1.Notification_Type__c= 'On Date';
            ns1.Field_Api_name__c = null;
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }

    @isTest
    public static void testNSValidationOnDateNDayTypeEmpty() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
           
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Active__c = false;
            ns1.Users__c = null;
            ns1.Notification_Type__c= 'On Date';
            ns1.Field_Api_name__c = 'CreatedDate';
            ns1.Notify_Day_Type__c =null;
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationOnDateNDaysEmpty() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
           
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Active__c = false;
            ns1.Users__c = null;
            ns1.Notification_Type__c= 'On Date';
            ns1.Field_Api_name__c = 'CreatedDate';
            ns1.Notify_Day_Type__c ='Before';
            ns1.Notify_Days__c = null;
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationRecordFilterCriteria() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Active__c = false;
            ns1.Users__c = null;
            ns1.Notification_Type__c= 'On Date';
            ns1.Field_Api_name__c = 'CreatedDate';
            ns1.Notify_Day_Type__c ='Before';
            ns1.Notify_Days__c = null;
            ns1.Record_Filter_Criteria_Text__c = 'active__c=true';
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationTitleEmoty() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Active__c = false;
            ns1.Users__c = null;
            ns1.Notification_Type__c= 'On Date';
            ns1.Field_Api_name__c = 'CreatedDate';
            ns1.Notify_Day_Type__c ='Before';
            ns1.Notify_Days__c = 10;
            ns1.Title__c = null;
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }

    @isTest
    public static void testTitleMessage() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Active__c = false;
            ns1.Users__c = null;
            ns1.Notification_Type__c= 'On Date';
            ns1.Field_Api_name__c = 'CreatedDate';
            ns1.Notify_Day_Type__c ='Before';
            ns1.Notify_Days__c = 10;
            ns1.Title__c = '';
            ns1.Message__c = 'Dear user, Lease has been created . <a href="{{aircraft__c}}" target="blank">{{aircraft__r.name}}</a>';
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationFieldAPINameEmpty() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
            ns1.Async_JobId__c = '0123455678';
            ns1.Notification_Type__c ='On Change';
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Field_API_Name__c = null;
            ns1.Object_Name__c = 'Lease';
           
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as obj name cannot be empty');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationInsertUsers() {
        
        try {
            User tuser = [SELECT Id, Name FROM User LIMIT 1];
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
           
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Notification_type__c = 'On Create';
            ns1.Users__c = tuser.id;
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----as users are not allowed to be added ');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationInsertAsyncId() {
        
        try {
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
           
            ns1.Object_API_Name__c = 'Lease__c';
            ns1.Object_Name__c = 'Lease';
            ns1.Notification_type__c = 'On Create';
            ns1.Async_JobId__c = '0123455678';
            insert ns1;
        }
        catch(Exception e) {
            System.debug('exception is expected----async Id cannot be filled');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    
   
    
    @isTest
    public static void testNSValidationUpdateObjAPIChange() {
        
        try {
            User tuser = [SELECT Id, Name FROM User LIMIT 1];
            
            Notification_Setting__c setup = [SELECT Id, Filter_By_Namespace_Prefix__c FROM Notification_Setting__c LIMIT 1];
            setup.Filter_By_Namespace_Prefix__c= 'leaseworks';
            
            Notifications_Subscription__c ns = [SELECT Id, Object_API_Name__c,Object_Name__c, Notification_type__c, Field_API_Name__c FROM Notifications_Subscription__c LIMIT 1];
            system.debug('ns----');
            ns.Object_API_Name__c = 'Bank__c';
            update ns;
        }
        catch(Exception e) {
            System.debug('exception is expected---api name cannot be updated');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationUpdateNTotificationType() {
        
        try {
            User tuser = [SELECT Id, Name FROM User LIMIT 1];
            
            Notification_Setting__c setup = [SELECT Id, Filter_By_Namespace_Prefix__c FROM Notification_Setting__c LIMIT 1];
            setup.Filter_By_Namespace_Prefix__c= 'leaseworks';
            
            Notifications_Subscription__c ns = [SELECT Id, Object_API_Name__c,Object_Name__c ,Notification_type__c, Field_API_Name__c FROM Notifications_Subscription__c LIMIT 1];
            system.debug('ns----');
            ns.Notification_type__c = 'On Delete';
            ns.Active__c = false;
            ns.Users__c = null;
            update ns;
        }
        catch(Exception e) { System.debug('exception is expected---api name cannot be updated'); }
           
           
       
        
    }
    @isTest
    public static void testNSValidationUpdateFieldAPIChange() {
        
        try {
            User tuser = [SELECT Id, Name FROM User LIMIT 1];
            
            Notification_Setting__c setup = [SELECT Id, Filter_By_Namespace_Prefix__c FROM Notification_Setting__c LIMIT 1];
            setup.Filter_By_Namespace_Prefix__c= 'leaseworks';
            
            Notifications_Subscription__c ns = [SELECT Id, Object_API_Name__c, Notification_type__c, Field_API_Name__c FROM Notifications_Subscription__c LIMIT 1];
            ns.Field_API_Name__c = 'CreatedDate';
           
            ns.Active__c = false;
            ns.Users__c = null;
            update ns;
        }
        catch(Exception e) {
            System.debug('exception is expected----field api cannotbe updaed');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    @isTest
    public static void testNSValidationUpdateNotificationTypeChange() {
        
        try {
            User tuser = [SELECT Id, Name FROM User LIMIT 1];
            
            Notification_Setting__c setup = [SELECT Id, Filter_By_Namespace_Prefix__c FROM Notification_Setting__c LIMIT 1];
            setup.Filter_By_Namespace_Prefix__c= 'leaseworks';
            Notifications_Subscription__c ns = [SELECT Id, Object_API_Name__c, Notification_type__c, Field_API_Name__c FROM Notifications_Subscription__c LIMIT 1];
            ns.Notification_type__c = 'On Delete';
            ns.Active__c = false;
            ns.Users__c = null;
            update ns;
        }
        catch(Exception e) {
            System.debug('exception is expected----notification type cannotbe updaed');
            System.debug('Exception Message -------' + e.getMessage());
        }
        
    }
    
    @isTest
    public static  void  testNotificationSettingMultipleRecords() {
        
        try {
            Notification_Setting__c setup = new Notification_Setting__c();
            setup.name = 'Testing Duplicate';
            insert setup ;
        }
        catch(exception e) {
            System.debug('Exception expected --------');
        }
        
    }
    
    @isTest
    public static void testOnChangeOfRecord() {
        
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notifications_Subscription__c ns = new Notifications_Subscription__c();
        ns.Field_API_Name__c = 'Lease_end_date_new__c';
        ns.Field_Name__c = 'Lease End Date';
        ns.Message__c = 'Lease End Date  Changed';
        ns.Notification_Type__c = 'On Change';
        
        ns.Object_API_Name__c = 'Lease__c';
        ns.Object_Name__c = 'Lease';
        ns.Title__c = 'Lease {{name}}';
        ns.UserIds__c = tuser.Id;
        ns.Users__c = 'Abc';
        
        ns.Active__c = true;
        ns.Status__c = 'Running';
        ns.ownerId = tuser.Id;
        
        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns;
        
        Lease__c lease = [SELECT Id, Name FROM Lease__c LIMIT 1];
        lease.name = 'Lease End Date  Changed name ';
        update lease;
        
    }
    
    @isTest
    public static void testDeleteOpr() {
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notifications_Subscription__c ns = new Notifications_Subscription__c();
        ns.Message__c = 'Account Deleted';
        ns.Notification_Type__c = 'On Delete';
        
        ns.Object_API_Name__c = 'Account';
        ns.Object_Name__c = 'Account';
        ns.Title__c = 'ACcount {{name}}';
        ns.UserIds__c = tuser.Id;
        ns.Users__c = 'Abc';
        
        ns.Active__c = true;
        ns.ownerId = tuser.Id;
        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns;
        
        Lease__c lease = [select Id,Name from Lease__c limit 1];
        delete lease;
        
    }
    
    @isTest
    public static void testNSStatusScheduler() {
        Test.startTest();
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notifications_Subscription__c ns = new Notifications_Subscription__c();
        ns.Message__c = 'Contact Creation';
        ns.Notification_Type__c = 'On Create';
        
        ns.Object_API_Name__c = 'Contact';
        ns.Object_Name__c = 'Contact';
        ns.Title__c = 'Contact {{name}}';
        ns.UserIds__c = tuser.Id;
        ns.Users__c = 'Abc';
        
        ns.Active__c = true;
        ns.ownerId = tuser.Id;
        ns.Async_JobId__c ='01hw1073456';
        ns.Status__c = 'Pending';
        
        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns;
        
       
        Test.setMock(WebServiceMock.class, new Notify_CheckStatusSchedulerMockSuccess());
        Notify_SubscriptionStatusScheduler sch = new Notify_SubscriptionStatusScheduler();
        sch.execute(null);
        Test.stopTest();
        
    }
    
    @isTest
    static void TestUpdateSubscriptionStatus() {
        
        String nextFireTime = System.now().addMinutes(Notify_Utility.NOTIFICATION_STATUS_CHECK_SCH_TIME).format('ss mm HH dd MM ? yyyy');
        
        //String cronExcp= '0 5 * * * * ';
        String jobId = System.schedule('Update Subscription Status',nextFireTime , new Notify_NotificationScheduler());
        Test.StartTest();
        
        System.debug('Job Id----'+ jobId);
        Test.setMock(WebServiceMock.class, new Notify_CheckStatusSchedulerMockSuccess());
        
    }
    
    @isTest
    public static void testNSStatusSchedulerError() {
        
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notifications_Subscription__c ns = new Notifications_Subscription__c();
        ns.Message__c = 'Contact Creation';
        ns.Notification_Type__c = 'On Create';
        
        ns.Object_API_Name__c = 'Contact';
        ns.Object_Name__c = 'Contact';
        ns.Title__c = 'Contact {{name}}';
        ns.UserIds__c = tuser.Id;
        ns.Users__c = 'Abc';
        
        ns.Active__c = true;
        ns.ownerId = tuser.Id;
        ns.Async_JobId__c ='01hw1073456';
        ns.Status__c = 'Pending';
        
        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new Notify_CheckStatusSchedulerMockError());
        Notify_SubscriptionStatusScheduler sch = new Notify_SubscriptionStatusScheduler();
        sch.execute(null);
        Test.stopTest();
        
    }
    
    @isTest
    static void testAsyncResult() {
        
        System.debug('testAsyncResult-----');
        String nextFireTime = System.now().addMinutes(Notify_Utility.NOTIFICATION_STATUS_CHECK_SCH_TIME).format('ss mm HH dd MM ? yyyy');
        
        //String cronExcp= '0 5 * * * * ';
        String jobId = System.schedule('Test', nextFireTime, new Notify_NotificationScheduler());
        System.debug('Job Id----'+ jobId);
        
        Test.setMock(WebServiceMock.class, new Notify_MetadataServiceMock());
        // Notify_NotificationSetupController.checkAsyncRequest(jobId);
        
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notify_Utility.NotificationSetupWrapper nsw = new Notify_Utility.NotificationSetupWrapper();
        nsw.selectedObjectLabel = 'Contact';
        nsw.selectedObjectName = 'Contact';
        nsw.selectedObjectName = 'Account';
        nsw.selectedUserGroup = 'Kim Brown';
        nsw.selectedUserRecords = '[{"sObjectType":"User","name":"Kim Brown","id":"0051I0000048LZmQAM"}]';
        
        nsw.notificationType = 'On Create';
        nsw.messageBody = '<p><span style="font-size: 13px;">{{name}} Cert Is expiring Soon. Please Recharge.</span></p>';
        nsw.isActive = true;
        nsw.selectedUserRecords = tuser.id;
        
        nsw.selectedUserGroupIds = '00e1I000000S7RgQAK';
        nsw.title = 'Contact {{name}}';
        nsw.userEditAccess = false;
        nsw.asyncJobId = jobId;
        
        Test.StartTest();
        Test.setMock(WebServiceMock.class, new Notify_CheckStatusSchedulerMockError());
        Notify_NotificationSetupController.saveNotificationData('', JSON.serialize(nsw),false);
        Notify_NotificationSetupController.checkSchedulerIsScheduled();
        Test.StopTest();
        
    }
    
    
    @isTest
    static void testDuplicateNotifSubscriptionChk() {
        
        try {
            User tuser = [SELECT Id, Name FROM User LIMIT 1];
            
            Notifications_Subscription__c ns = new Notifications_Subscription__c();
            ns.Field_API_Name__c = 'Name';
            ns.Field_Name__c = 'Name';
            ns.Message__c = 'Contact Name Changed';
            ns.Notification_Type__c = 'On Change';
            ns.Object_API_Name__c = 'Contact';
            ns.Object_Name__c = 'Contact';
            ns.Title__c = 'Contact {{name}}';
            ns.UserIds__c = tuser.Id;
            ns.Users__c = 'Abc';
            
            ns.Active__c = true;
            ns.Status__c = 'Pending';
            ns.ownerId = tuser.Id;
            
            Notify_Utility.setSourceOfCall('INTERNAL_API');
            insert ns;
            
            Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
            ns1.Field_API_Name__c = 'Name';
            ns1.Field_Name__c = 'Name';
            ns1.Message__c = 'Contact Name Changed';
            ns1.Notification_Type__c = 'On Change';
            ns1.Object_API_Name__c = 'Contact';
            ns1.Object_Name__c = 'Contact';
            ns1.Title__c = 'Contact {{name}}';
            ns1.UserIds__c = tuser.Id;
            ns1.Users__c = 'Abc';
            
            ns1.Active__c = true;
            ns1.Status__c = 'Pending';
            ns1.ownerId = tuser.Id;
            Notify_Utility.setSourceOfCall('INTERNAL_API');
            insert ns1;
            
        }
        catch(Exception e) {
            System.debug('Exception is expected------------');
        }
        
    }
    
    @isTest
    static void testNSNotActive() {
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notify_Utility.NotificationSetupWrapper nsw = new Notify_Utility.NotificationSetupWrapper();
        nsw.selectedObjectLabel = 'Contact';
        nsw.selectedObjectName = 'Contact';
        nsw.selectedObjectName = 'Account';
        nsw.selectedUserGroup = 'Kim Brown';
        nsw.selectedUserRecords = '[{"sObjectType":"User","name":"Kim Brown","id":"0051I0000048LZmQAM"}]';
        
        nsw.notificationType = 'On Create';
        nsw.messageBody = '<p><span style="font-size: 13px;">{{name}} Cert Is expiring Soon. Please Recharge.</span></p>';
        nsw.isActive = false;
        nsw.selectedUserRecords = tuser.id;
        
        nsw.selectedUserGroupIds = '00e1I000000S7RgQAK';
        nsw.title = 'Contact {{name}}';
        nsw.userEditAccess = false;
        
        Test.StartTest();
        Notify_NotificationSetupController.saveNotificationData('', JSON.serialize(nsw),false);
        
        List<Notifications_Subscription__c> nsList = [select Id,Object_Name__c, Status__c from Notifications_Subscription__c where Object_Name__c='Contact'];
        System.debug(nsList);
        System.assertEquals(nsList[0].Status__c,'Not Running');
        Test.stopTest();
    }
    
    @isTest
    static void testNSInError() {
        
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        
        Notifications_Subscription__c ns = new Notifications_Subscription__c();
        ns.Field_API_Name__c = 'Lease_End_Date_New__c';
        ns.Field_Name__c = 'Lease End Date';
        ns.Message__c = 'Lease is Expiring {{Testing__c}} ';
        ns.Notification_Type__c = 'On Create';
        ns.Notify_Days__c = 0;
        ns.Object_API_Name__c = 'Lease__c';
        ns.Object_Name__c = 'Lease';
        ns.Title__c = 'Test {{name}}';
        ns.UserIds__c = tuser.Id;
        ns.Users__c = 'Abc';
        
        ns.Active__c = true;
        ns.Status__c = 'Running';
        ns.ownerId = tuser.Id;
        ns.Last_execution_date__c = null;
        
        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns;
        
        Lease__c lease = new Lease__c(name='Test Lease', Lease_Start_Date_New__c= System.today() , Lease_End_Date_New__c = System.today().addDays(10));
        insert lease;
        
    }
    
    //Metadata API web service mock class
    private class WebServiceMockImpl implements WebServiceMock {
        
        public void doInvoke(Object stub, Object request, Map<String, Object> response,
                             String endpoint, String soapAction, String requestName,
                             String responseNS, String responseName, String responseType) {
                                 
                                 if(request instanceof Notify_MetadataService.retrieve_element) {
                                     response.put('response_x', new Notify_MetadataService.retrieveResponse_element());
                                 }
                                 else if(request instanceof Notify_MetadataService.checkDeployStatus_element) {
                                     response.put('response_x', new Notify_MetadataService.checkDeployStatusResponse_element());
                                 }
                                 else if(request instanceof Notify_MetadataService.deploy_element) {
                                     response.put('response_x', new Notify_MetadataService.deployResponse_element());
                                 }
                                 return;
                             }
        
    }

    @IsTest
    static void codeCoverage(){
        User tuser = [SELECT Id, Name FROM User LIMIT 1];
        Notify_NotificationSetupController.checkIsSysAdmin(tuser.id);
    }
    
    @IsTest
    private static void coverGeneratedCodeFileBasedOperations1() {
        
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        Notify_MetadataService Notify_MetadataService = new Notify_MetadataService();
        
        Test.startTest();
        Notify_MetadataService.MetadataPort metaDataPort = new Notify_MetadataService.MetadataPort();
        metaDataPort.retrieve(null);
        metaDataPort.checkDeployStatus(null, false);
        metaDataPort.deploy(null, null);
        metaDataPort.checkDeployStatus(null, false);
        Test.stopTest();
        
    }
    
    @IsTest
    private static void coverGeneratedCodeTypes() {
        
        Test.startTest();
        new Notify_MetadataService();
        new Notify_MetadataService.checkDeployStatus_element();
        new Notify_MetadataService.CodeCoverageWarning();
        new Notify_MetadataService.LogInfo();
        new Notify_MetadataService.CallOptions_element();
        new Notify_MetadataService.DeployMessage();
        new Notify_MetadataService.CodeCoverageResult();
        new Notify_MetadataService.ProfileObjectPermissions();
        new Notify_MetadataService.RetrieveResult();
        new Notify_MetadataService.retrieve_element();
        new Notify_MetadataService.RunTestSuccess();
        new Notify_MetadataService.deployResponse_element();
        new Notify_MetadataService.FileProperties();
        new Notify_MetadataService.AsyncResult();
        new Notify_MetadataService.RetrieveRequest();
        new Notify_MetadataService.DebuggingHeader_element();
        new Notify_MetadataService.RunTestFailure();
        new Notify_MetadataService.MetadataWithContent();
        new Notify_MetadataService.Metadata();
        new Notify_MetadataService.RetrieveMessage();
        new Notify_MetadataService.SessionHeader_element();
        new Notify_MetadataService.DeployOptions();
        new Notify_MetadataService.PackageTypeMembers();
        new Notify_MetadataService.deploy_element();
        new Notify_MetadataService.retrieveResponse_element();
        new Notify_MetadataService.PackageVersion();
        new Notify_MetadataService.RunTestsResult();
        new Notify_MetadataService.CodeLocation();
        new Notify_MetadataService.DebuggingInfo_element();
        new Notify_MetadataService.Package_x();
        new Notify_MetadataService.DeployResult();
        new Notify_MetadataService.checkDeployStatusResponse_element();
        new Notify_MetadataService.DeployDetails();
        new Notify_MetadataService.AllOrNoneHeader_element();
        Test.stopTest();
        
    }
}