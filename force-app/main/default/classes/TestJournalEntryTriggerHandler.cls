@isTest
public class TestJournalEntryTriggerHandler {
    
    @testSetup
    static void setup(){
        
        
	    LeaseWareUtils.clearFromTrigger(); 
        Parent_Accounting_Setup__c accountingSetup = new Parent_Accounting_Setup__c(Name='TestAS');
        insert accountingSetup;
        
        
	    LeaseWareUtils.clearFromTrigger(); 
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
        insert aircraft;
	        
 		
	    LeaseWareUtils.clearFromTrigger();
        Dimension__c dim = new Dimension__c(name ='MSN/ESN');
        insert dim;
      
        Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
        insert legalEntity;
        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;
       
        
	    LeaseWareUtils.clearFromTrigger(); 
        Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,Legal_Entity__c=legalEntity.Id,Lease_End_Date_New__c=Date.today().addYears(10),Lease_Start_Date_New__c=Date.today());
        insert lease;
   		
        
        
	    LeaseWareUtils.clearFromTrigger(); 
        Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                                            Name='Rent Schedule Fixed',
                                            Lease__c=lease.Id,
                                            Rent_Type__c='Fixed Rent',
                                            Rent_Period__c='Monthly',
                                            Base_Rent__c=200000.00,
    										Start_Date__c=Date.today() , // YYYY-MM-DD
                                           	Rent_End_Date__c=Date.today().addYears(10) ,  // YYYY-MM-DD
                                            Invoice_Generation_Day__c='15',
                                            Rent_Period_End_Day__c='14',
    										Rent_Due_Type__c='Fixed',
                                            Rent_Due_Day__c=15,
    										Escalation__c=3,
    										Escalation_Month__c='January',
    										Pro_rata_Number_Of_Days__c='Actual number of days in month/year');
	    insert MultiRentSch;
        
        
	    LeaseWareUtils.clearFromTrigger(); 
        rent__c rent2 = new rent__c(RentPayments__c= lease.id,Name='New Rent 1',For_Month_Ending__c=system.today().addMonths(1).toStartOfMonth().addDays(-1),start_date__c=system.today().toStartOfMonth(),Stepped_Rent__c =MultiRentSch.id,Rent_Due_Amount__c = 10000);
        insert rent2;
        
         Calendar_Period__c CP1 = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1));
         Calendar_Period__c CP2 = new Calendar_Period__c(Name = 'Dummy2',Start_Date__c =system.today().addMonths(1).toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(2).toStartOfMonth().addDays(-1));
        
        list<Calendar_Period__c> cp = new list<Calendar_Period__c>{cp1,cp2};
        
        LeaseWareUtils.clearFromTrigger();
        insert cp;

        LeaseWare_SequenceId__c lw = new LeaseWare_SequenceId__c(Active__c =true,Initial_Value__c=1,Sequence_Name__c='Journal',Sequence_Number_New__c=1,name ='Sequence_Number_Journal_Entry');
        
        LeaseWareUtils.clearFromTrigger();
        insert lw;
        Invoice__c invoice2 = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rent2.id,amount__c = rent2.Rent_Due_Amount__c,status__c ='Pending',
                                            Date_of_MR_Payment_Due__c = Date.valueOf(datetime.newInstance(system.today().year(),system.today().month(), 17)),
                                           invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
        
	    LeaseWareUtils.clearFromTrigger(); 
        insert invoice2;
        
    }  
     @isTest
    static void testJournalDimension(){
        Test.startTest();
         
        list<Invoice__c> invoices = [Select Id,Status__c From invoice__c];
        System.assertEquals(1, invoices.size());
       	invoices.get(0).status__c ='Approved';
        
        
	    LeaseWareUtils.clearFromTrigger();
        update invoices;
        
        list<journal_entry__c> jes = [select id from Journal_Entry__c where
                                     invoice__c =:invoices.get(0).id ];
        System.assert(jes.size()>0);
        list<journal_dimension__c> jedimList =[Select id,name from journal_dimension__c where journal_entry__c =:jes.get(0).id];
        System.assertEquals('MSN/ESN', jedimList.get(0).name);
        
        Test.stopTest();
    }
    

}