@isTest
public class TestMSNController {
    @isTest
    static void testMSNController() {
        Project__c project = new Project__c(Name = 'Test',
                                            Economic_Closing_Date__c = System.today(),
                                            Project_Deal_Type__c = 'Sale');
        insert project;
        
        Ratio_Table__c ratioTable = new Ratio_Table__c(Name= 'Test Ratio');
        insert ratioTable;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
    //      Escalation__c = 43,
    //      Escalation_Month__c = 'January',
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '11237 Test',
            Contractual_Status__c= 'Sold',
            Ownership_Status__c = 'Managed',
            Aircraft_Type__c = 'Engine Type',
            Aircraft_Variant__c = '700',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '12930',
            MTOW_Leased__c=10,
            AOG_Date__c = Date.today(),
            Asset_Owner__c = 'abctest',
            TSLV__c = 1
        );
        insert aircraft;
        
        Aircraft__c aircraft1 = new Aircraft__c(
            Name= '113 Test',
            Contractual_Status__c= 'Sold',
            Ownership_Status__c = 'Managed',
            Aircraft_Type__c = 'Engine Type',
            Aircraft_Variant__c = '500',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '123',
            MTOW_Leased__c=10,
            AOG_Date__c = Date.today(),
            Asset_Owner__c = 'abctest',
            TSLV__c = 1
        );
        insert aircraft1;
        
        aircraft.Lease__c = lease.Id;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;
        
        Counterparty__c counterParty = new Counterparty__c(
            Name = 'Leassor Test');
        insert counterParty;
        
        Operator__c operator = new Operator__c (
            Name = 'Airline Test2');
        insert operator;
        
        Operator__c operator1 = new Operator__c (
            Name = 'Airline Test1');
        insert operator1;
        
        Marketing_Activity__c marketingActivity = new Marketing_Activity__c(
            Name = 'Test MA',
            Prospect__c = operator.Id,
            Lessor__c = counterParty.Id,
            Deal_Type__c = 'Sale',
            Inactive_MA__c = False,
            Deal_Status__c = 'Pipeline',
            campaign__c = project.Id);
        
        insert marketingActivity; 
        
        
        Marketing_Activity__c marketingActivity1 = new Marketing_Activity__c(
            Name = 'Test MA',
            Prospect__c = operator1.Id,
            Lessor__c = counterParty.Id,
            Deal_Type__c = 'Sale',
            Inactive_MA__c = False,
            Deal_Status__c = 'Pipeline',
            campaign__c = project.Id);
        
        insert marketingActivity1; 
        
        Aircraft_Proposal__c bidAssetTerm = new Aircraft_Proposal__c(Name= 'Test Asset',
                                                                     Project_Status__c = 'Bid',
                                                                     sale_price__c = 20,
                                                                     Marketing_Activity__c  = marketingActivity.ID,
                                                                     aircraft__c = aircraft.Id);
        insert bidAssetTerm;
        
        Aircraft_Proposal__c dataRoomAssetTerm = new Aircraft_Proposal__c(Name= 'Test Asset',
                                                                          Project_Status__c = 'Data Room',
                                                                          sale_price__c = 20,
                                                                          Marketing_Activity__c  = marketingActivity.ID,
                                                                          aircraft__c = aircraft1.Id);
        insert dataRoomAssetTerm;
        
        
        Global_Fleet__c gf = new Global_Fleet__c();
        gf.Name = 'testWorldFleet';
        gf.Aircraft_External_ID__c = 'type-msn';
        gf.MTOW__c = 134453;
        gf.BuildYear__c = '1996';
        gf.SeatTotal__c = 12;
        gf.AircraftType__c = 'A320';
        gf.AircraftVariant__c = '400';
        gf.EngineType__c = 'V2524';
        gf.AircraftSeries__c = 'AStest';
        gf.EngineVariant__c = 'EVtest';
        gf.OriginalOperator__c ='testOpr';
        gf.Aircraft_Operator__c = operator.Id;
        
        insert gf;
        
        List<String> assetList = new List<String> {aircraft1.Id};
            
        Test.startTest();      
        MSNController.getDefaultValues(leasewareutils.getNamespacePrefix() + 'Asset_Type_2__c', leasewareutils.getNamespacePrefix() + 'Aircraft_Engine_Type__c', marketingActivity.Id);
        MSNController.loadPicklistValues();
        MSNController.getResultColumns('Aircraft__c');
        MSNController.searchMSN(JSON.serialize(aircraft),1992,2,04,'kgs',2,2,2,1,10,2);
        MSNController.searchEngines(JSON.serialize(aircraft),2.0,2,2,2,1,0,0);
        MSNController.searchGlobalAssets(JSON.serialize(gf),1996,5,2,2,2,1,0,0);
        MSNController.addSelectedAssetsToRecord(marketingActivity1.Id, assetList, 'Aircraft__c');
        MSNController.getControllingField('Aircraft__c','Engine_Type__c');
        Test.stopTest();       
    }
}