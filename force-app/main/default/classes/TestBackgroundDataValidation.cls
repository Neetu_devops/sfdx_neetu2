@isTest
private class TestBackgroundDataValidation {

    static void createData(map<String,Object> mapInOut){
        
        mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id AircraftId1 = (string) mapInOut.get('Aircraft__c.Id');
        
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id');
        
         // Set up the Lease record.
        Lease__c tempLease;
        mapInOut.put('Lease__c.Aircraft__c',AircraftId1);
        mapInOut.put('Lease__c.Operator__c',OperatorId1);
        mapInOut.put('Lease__c.Name','Lease1');  
        mapInOut.put('Lease__c',null);      
        TestLeaseworkUtil.createLease(mapInOut);
        tempLease = (Lease__c)mapInOut.get('Lease__c');
        
        Aircraft__c AC = [select id, lease__c from Aircraft__c where id = :AircraftID1];
        AC.Lease__c = tempLease.Id;
        leasewareUtils.clearFromTrigger();
        update AC;

        mapInOut.put('Aircraft__c.lease__c', AC.Lease__c);
    }
    /* this testcase to cover the code coverage for batch processing and scheduler */
    
    static testMethod void basicTest() {

        map<String,Object> mapInOut = new map<String,Object>();
        createData(mapInOut);
        BackgroundDataValidationHelper helper = new  BackgroundDataValidationHelper();  
		helper.insertRec('Invoice Record Type Mismatch','Contract','Invoice','Medium','Dummy','Dummy',1000,'FIN_INV_RECORDTYPE_MISMATCH' );
        helper.insertScripts();
        
        Test.startTest();
        BackgroundDataValidation b= new BackgroundDataValidation();
        b.execute(null);
        Test.stopTest();

    }
    /* this testcase to cover the code coverage for batch processing and scheduler */
    @isTest(seeAllData=true)
    static void basicTest2() {

        BackgroundDataValidationHelper helper = new  BackgroundDataValidationHelper();  
		helper.insertRec('Invoice Record Type Mismatch','Contract','Invoice','Medium','Dummy','Dummy',1000,'FIN_INV_RECORDTYPE_MISMATCH' );
        helper.insertScripts();
        
        Test.startTest();
        BackgroundDataValidation b= new BackgroundDataValidation();
        b.execute(null);
        Test.stopTest();

    }  
    
    @isTest
   static void testInvoiceHeaderAmtMismatch(){
        
        map<String,Object> mapInOut = new map<String,Object>();
        createData(mapInOut);

        system.debug('Create Rent');
        TestLeaseworkUtil.createRent(mapInOut) ;
        TestLeaseworkUtil.setRecordTypeOfInvoice(mapInOut);
        
        Invoice__c newRentInv= new Invoice__c(
            Lease__c=(Id)mapInOut.get('Aircraft__c.lease__c'), Name='Test Rent Inv', RecordTypeId=(Id)mapInOut.get('Invoice__c.Rent'),
            Invoice_date__c=Date.today(), Invoice_Type__c='Rent', 
             Rent__c = (Id)mapInOut.get('Rent__c.Id'));
        leasewareUtils.clearFromTrigger();
        insert newRentInv;   

        //Modify the invoice header amount
        leasewareUtils.triggerDisabledFlag = true;
        newRentInv.Amount__c = LeasewareUtils.zeroIfNull(newRentInv.Amount__c) + 100;
        update newRentInv;
        leasewareUtils.triggerDisabledFlag = false;

        BackgroundDataValidationHelper helper = new  BackgroundDataValidationHelper();  
		helper.insertRec('Invoiced Header Amount and Invoice Line Items','Contract','Invoice','High',null,'Invoice Header Amount is not equal to sum of all Invoice Line Items (if any)',1000,'FIN_INV_HEADERAMT_MISMATCH' );
        helper.insertScripts();
        
        Test.startTest();
        BackgroundDataValidation b= new BackgroundDataValidation();
        b.execute(null);
        Test.stopTest();

        //The rent invoice has to be in the issue list
        List<Admin_Data_Validation_Result__c> lstIssueList = [select id from Admin_Data_Validation_Result__c where Issue_Record_Id__c = :newRentInv.id];
        System.assertEquals(lstIssueList.size(), 1);
   }
    @istest
    static void mrInvoiceLISRSRUtlznNull(){
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today().addYears(-5),
            Lease_End_Date_New__c = system.today().addYears(5),
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  
        Constituent_Assembly__c consituentAssemblyRecord = new Constituent_Assembly__c(Serial_Number__c = '2',CSN__c = 3,TSN__c = 5,
                            Last_CSN_Utilization__c = 3,Last_TSN_Utilization__c = 5, Attached_Aircraft__c = testAircraft.Id,Asset__c = testAircraft.Id,Name ='Testing', Type__c ='Airframe');
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert consituentAssemblyRecord ;
        LWGlobalUtils.setTriggersflagOff();

        Assembly_Event_Info__c assemblyEventInfoObj=new Assembly_Event_Info__c(Name='Airframe-Airframe-Heavy 1', Constituent_Assembly__c = consituentAssemblyRecord.Id);
        insert assemblyEventInfoObj;
        
        Assembly_MR_Rate__c assemblyMrRecord = new Assembly_MR_Rate__c(Name ='TestMrRecord',Lease__c =leaseRecord.Id,Base_MRR__c =200,Escalation_Type__c='Advanced',
                    Rate_Basis__c='Monthly', Assembly_Type__c ='Engine 3',Assembly_Lkp__c = consituentAssemblyRecord.Id,Assembly_Event_Info__c = assemblyEventInfoObj.Id,Annual_Escalation__c = 1);
        
        insert assemblyMrRecord; 
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Month_Ending__c =  system.today().addMonths(-2).toStartOfMonth().addDays(-1),
            Status__c = 'Approved By Lessor'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();

        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Amount__c =200,
            Status__c = 'Approved',
            payment_status__c ='Open',
            invoice_type__c = 'Aircraft MR',
            RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId()
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff();

        Utilization_Report_List_Item__c utilizationReportListItemObj=new Utilization_Report_List_Item__c(
            Constituent_Assembly__c=consituentAssemblyRecord.Id,
            Utilization_Report__c=utilizationRecord.Id,
            Event_Name__c=assemblyEventInfoObj.Id,
            Cycles_During_Month__c=1,
            Running_Hours_During_Month__c=2,
            Assembly_MR_Info__c=assemblyMrRecord.Id
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationReportListItemObj;
        LWGlobalUtils.setTriggersflagOff();
        

        list<invoice_line_item__c> invLIList = new list<invoice_line_item__c>();
        
        invLIList.add(new Invoice_Line_Item__c (name = 'test inv LI',invoice__c = invoiceRecord.id, Amount_Due__c = 200,
                                    RecordTypeId=Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId() ) );
        invLIList.add(new Invoice_Line_Item__c (name = 'test inv LI',invoice__c = invoiceRecord.id, Amount_Due__c = 200,
                                    RecordTypeId=Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId(),Assembly_MR_Info__c = assemblyMrRecord.id ) );
        invLIList.add(new Invoice_Line_Item__c (name = 'test inv LI',invoice__c = invoiceRecord.id, Amount_Due__c = 200,
                                    RecordTypeId=Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId(),Assembly_Utilization__c = utilizationReportListItemObj.id  ) );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invLIList;
        LWGlobalUtils.setTriggersflagOff();

        Test.startTest();
        Admin_Data_Validation__c adm = new Admin_Data_Validation__c(name = 'Test',Category__c = 'Finance',Object_Name__c = 'Invoice Line Item',Impact__c = 'High',Desc__c = null
                             ,Condition__c = null,Batch_Size__c = 100,Script_Method_Name__c  = 'Test',Active__c = true );
        insert adm;
        map<string,string> paramMap = new map<string,string>();
        paramMap.put('PARENT_ID',adm.id);
        BackgroundDataValidationScriptHelper bgdValScrHelp = new BackgroundDataValidationScriptHelper();
        list<invoice_line_item__c> listInvoiceLI = database.query(bgdValScrHelp.mrInvoiceLISRSRUtlznNullStart(paramMap)[0]);
        system.assertEquals(3, listInvoiceLI.size(), 'No of records returned by the validation script query is wrong');
        bgdValScrHelp.mrInvoiceLISRSRUtlznNullExecute(listInvoiceLI,paramMap);
        list<Admin_Data_Validation_Result__c> adList = [select id from Admin_Data_Validation_Result__c];
        system.assertEquals(3, adList.size(), 'No of records returned by the Admin Data Validation Result is wrong');
        Test.stopTest();
    }

    @isTest
    public static void  testPyStdAmtPaidAmtAllocatedMismatch(){
        LeaseWareUtils.clearFromTrigger(); 
        Operator__c operator = new Operator__c(Name='TestOperator', Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;

	    LeaseWareUtils.clearFromTrigger(); 
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft', MSN_Number__c='TestMSN1');
        insert aircraft;

	    LeaseWareUtils.clearFromTrigger(); 
        Lease__c lease = new Lease__c(Name='TestLease', aircraft__c = aircraft.id, Lessee__c = operator.Id, Lease_End_Date_New__c = Date.today().addYears(10),Lease_Start_Date_New__c = Date.today());
        insert lease; 

        Id MutliRentIDFixed = TestLeaseworkUtil.createmultiRentFixed(lease.id, 'Fixed 30/360',
                                lease.lease_start_date_new__c, lease.Lease_End_Date_New__c);

        List<Rent__c> rentList = [SELECT Id, For_Month_Ending__c, Start_date__c FROM Rent__c WHERE RentPayments__c =: lease.id AND Stepped_Rent__c =: MutliRentIDFixed ORDER BY start_date__c];
        Rent__c rent2 = rentList.get(0); 
        
        Invoice__c invoiceRecord = new Invoice__c(Invoice_Date__c = System.today(), lease__c = lease.id, rent__c =  rent2.id, status__c = 'Approved', Amount__c = 20000,
                                             invoice_type__c = 'Rent', recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
        

        
        LeaseWareUtils.TriggerDisabledFlag = true;
        insert invoiceRecord;
        LeaseWareUtils.TriggerDisabledFlag = false;

        Payment_Receipt__c paymentReciptRecord = new Payment_Receipt__c(
            Name='Testing',
            Approval_Status__c = 'Approved',
            Payment_Amount_Cur__c =  20000
        );
        insert paymentReciptRecord; 
        LeasewareUtils.clearfromTrigger();
        Payment_Receipt_Line_Item__c paymentLineITemREcord = new Payment_Receipt_Line_Item__c(
            Payment_Receipt__c = paymentReciptRecord.Id,
            Invoice__c = invoiceRecord.Id,
            Amount_Paid__c = 19000,
            Bank_Charges__c = 0
        );
        insert paymentLineITemREcord;
        Test.startTest();
        Admin_Data_Validation__c adm = new Admin_Data_Validation__c(name = 'Test',Category__c = 'Finance',Object_Name__c = 'Payment Studio',Impact__c = 'High',Desc__c = null
                             ,Condition__c = null,Batch_Size__c = 100,Script_Method_Name__c  = 'Test',Active__c = true );
        insert adm;
        map<string,string> paramMap = new map<string,string>();
        paramMap.put('PARENT_ID',adm.id);
        BackgroundDataValidationScriptHelper bgdValScrHelp = new BackgroundDataValidationScriptHelper();
        list<payment_Receipt__c> listPyStd = database.query(bgdValScrHelp.pyAmtPaidAmtAllocatedMismatchStart(paramMap)[0]);
        system.assertEquals(1, listPyStd.size(), 'No of records returned by the validation script query is wrong');
        bgdValScrHelp.pyAmtPaidAmtAllocatedMismatchExecute(listPyStd,paramMap);
        list<Admin_Data_Validation_Result__c> adList = [select id from Admin_Data_Validation_Result__c];
        system.assertEquals(1, adList.size(), 'No of records returned by the Admin Data Validation Result is wrong');
        Test.stopTest(); 
    }

    @istest
    static void paymentInvoiceLISRSRUtlznNull(){
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today().addYears(-5),
            Lease_End_Date_New__c = system.today().addYears(5),
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  
        Constituent_Assembly__c consituentAssemblyRecord = new Constituent_Assembly__c(Serial_Number__c = '2',CSN__c = 3,TSN__c = 5,
                            Last_CSN_Utilization__c = 3,Last_TSN_Utilization__c = 5, Attached_Aircraft__c = testAircraft.Id,Asset__c = testAircraft.Id,Name ='Testing', Type__c ='Airframe');
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert consituentAssemblyRecord ;
        LWGlobalUtils.setTriggersflagOff();

        Assembly_Event_Info__c assemblyEventInfoObj=new Assembly_Event_Info__c(Name='Airframe-Airframe-Heavy 1', Constituent_Assembly__c = consituentAssemblyRecord.Id);
        insert assemblyEventInfoObj;
        
        Assembly_MR_Rate__c assemblyMrRecord = new Assembly_MR_Rate__c(Name ='TestMrRecord',Lease__c =leaseRecord.Id,Base_MRR__c =200,Escalation_Type__c='Advanced',
                    Rate_Basis__c='Monthly', Assembly_Type__c ='Engine 3',Assembly_Lkp__c = consituentAssemblyRecord.Id,Assembly_Event_Info__c = assemblyEventInfoObj.Id,Annual_Escalation__c = 1);
        
        insert assemblyMrRecord; 
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Month_Ending__c =  system.today().addMonths(-2).toStartOfMonth().addDays(-1),
            Status__c = 'Approved By Lessor'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();

        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Amount__c =600,
            Status__c = 'Approved',
            payment_status__c ='Open',
            invoice_type__c = 'Aircraft MR',
            RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId()
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff();

        Utilization_Report_List_Item__c utilizationReportListItemObj=new Utilization_Report_List_Item__c(
            Constituent_Assembly__c=consituentAssemblyRecord.Id,
            Utilization_Report__c=utilizationRecord.Id,
            Event_Name__c=assemblyEventInfoObj.Id,
            Cycles_During_Month__c=1,
            Running_Hours_During_Month__c=2,
            Assembly_MR_Info__c=assemblyMrRecord.Id
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationReportListItemObj;
        LWGlobalUtils.setTriggersflagOff();
        

        list<invoice_line_item__c> invLIList = new list<invoice_line_item__c>();
        list<payment_line_item__c> pyLIList = new list<payment_line_item__c>();
        
        
        invLIList.add(new Invoice_Line_Item__c (name = 'test inv LI',invoice__c = invoiceRecord.id, Amount_Due__c = 200,
                                    RecordTypeId=Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId() ) );
        invLIList.add(new Invoice_Line_Item__c (name = 'test inv LI',invoice__c = invoiceRecord.id, Amount_Due__c = 200,
                                    RecordTypeId=Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId(),Assembly_MR_Info__c = assemblyMrRecord.id ) );
        invLIList.add(new Invoice_Line_Item__c (name = 'test inv LI',invoice__c = invoiceRecord.id, Amount_Due__c = 200,
                                    RecordTypeId=Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId(),Assembly_Utilization__c = utilizationReportListItemObj.id  ) );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invLIList;
        LWGlobalUtils.setTriggersflagOff();

        Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoiceRecord.id, payment_date__c=date.today(), RecordTypeId=Schema.SObjectType.payment__c.getRecordTypeInfosByDeveloperName().get('Aircraft_MR').getRecordTypeId(), Amount__c=600
                                              );
        LWGlobalUtils.setTriggersflagOn(); 
            insert newPayment;
        LWGlobalUtils.setTriggersflagOff();	

        pyLIList.add(new Payment_Line_Item__c(Name='Test',Payment__c=newPayment.Id, Amount_Paid__c=200 ,invoice_line_item__c = invLIList[0].id ,RecordTypeId=Schema.SObjectType.payment_Line_Item__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId() ));
        pyLIList.add( new Payment_Line_Item__c(Name='Test',Payment__c=newPayment.Id, Amount_Paid__c=200,Assembly_MR_Info__c = assemblyMrRecord.id ,invoice_line_item__c = invLIList[0].id,RecordTypeId=Schema.SObjectType.payment_Line_Item__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId() ));
        pyLIList.add( new Payment_Line_Item__c(Name='Test',Payment__c=newPayment.Id, Amount_Paid__c=200,Assembly_Utilization__c = utilizationReportListItemObj.id ,invoice_line_item__c = invLIList[0].id,RecordTypeId=Schema.SObjectType.payment_Line_Item__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId() ));
        LeaseWareUtils.clearFromTrigger();
        LWGlobalUtils.setTriggersflagOn(); 
            insert pyLIList;
        LWGlobalUtils.setTriggersflagOff();	
        Test.startTest();

        Admin_Data_Validation__c adm = new Admin_Data_Validation__c(name = 'Test',Category__c = 'Finance',Object_Name__c = 'Payment Line Item',Impact__c = 'High',Desc__c = null
                             ,Condition__c = null,Batch_Size__c = 100,Script_Method_Name__c  = 'Test',Active__c = true );
        insert adm;
        map<string,string> paramMap = new map<string,string>();
        paramMap.put('PARENT_ID',adm.id);
        BackgroundDataValidationScriptHelper bgdValScrHelp = new BackgroundDataValidationScriptHelper();
        list<payment_line_item__c> listPyLI = database.query(bgdValScrHelp.mrPaymentLISRSRUtlznNullStart(paramMap)[0]);
        system.assertEquals(3, listPyLI.size(), 'No of records returned by the validation script query is wrong');
        bgdValScrHelp.mrPaymentLISRSRUtlznNullExecute(listPyLI,paramMap);
        list<Admin_Data_Validation_Result__c> adList = [select id from Admin_Data_Validation_Result__c];
        system.assertEquals(3, adList.size(), 'No of records returned by the Admin Data Validation Result is wrong');
        Test.stopTest();
    }

    @istest
    static void assemblyRecordTypeMismatch(){
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today().addYears(-5),
            Lease_End_Date_New__c = system.today().addYears(5),
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  

        list<Constituent_Assembly__c> assemblyList = new list<Constituent_Assembly__c>();

        assemblyList.add(new Constituent_Assembly__c(Serial_Number__c = '2',CSN__c = 3,TSN__c = 5,RecordTypeId=Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('APU').getRecordTypeId(),
                            Last_CSN_Utilization__c = 3,Last_TSN_Utilization__c = 5, Attached_Aircraft__c = testAircraft.Id,Asset__c = testAircraft.Id,Name ='Testing', Type__c ='Airframe'));
        
        assemblyList.add(new Constituent_Assembly__c(Serial_Number__c = '2',CSN__c = 3,TSN__c = 5,RecordTypeId=Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId(),
                            Last_CSN_Utilization__c = 3,Last_TSN_Utilization__c = 5, Attached_Aircraft__c = testAircraft.Id,Asset__c = testAircraft.Id,Name ='Testing', Type__c ='APU'));
        assemblyList.add(new Constituent_Assembly__c(Serial_Number__c = '2',CSN__c = 3,TSN__c = 5,RecordTypeId=Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId(),
                            Last_CSN_Utilization__c = 3,Last_TSN_Utilization__c = 5, Attached_Aircraft__c = testAircraft.Id,Asset__c = testAircraft.Id,Name ='Testing', Type__c ='Engine 1'));
        LWGlobalUtils.setTriggersflagOn(); 
        insert assemblyList ;
        LWGlobalUtils.setTriggersflagOff();

       	
        Test.startTest();

        Admin_Data_Validation__c adm = new Admin_Data_Validation__c(name = 'Test',Category__c = 'Technical',Object_Name__c = 'Assembly',Impact__c = 'Medium',Desc__c = null
                             ,Condition__c = null,Batch_Size__c = 100,Script_Method_Name__c  = 'Test',Active__c = true );
        insert adm;
        map<string,string> paramMap = new map<string,string>();
        paramMap.put('PARENT_ID',adm.id);
        BackgroundDataValidationScriptHelper bgdValScrHelp = new BackgroundDataValidationScriptHelper();
        list<Constituent_Assembly__c> listAssemblyLI = database.query(bgdValScrHelp.assemblyRecordTypeMismatchStart(paramMap)[0]);
        for(Constituent_Assembly__c ass :listAssemblyLI){
            system.debug('Assembly type:'+ass.Type__c);
            system.debug('Record type dev name '+ass.recordType.DeveloperName);
            system.debug('Record type name '+ass.recordType.Name);
        }
        system.assertEquals(3, listAssemblyLI.size(), 'No of records returned by the validation script query is wrong');
        bgdValScrHelp.assemblyRecordTypeMismatchExecute(listAssemblyLI,paramMap);
        list<Admin_Data_Validation_Result__c> adList = [select id from Admin_Data_Validation_Result__c];
        system.assertEquals(2, adList.size(), 'No of records returned by the Admin Data Validation Result is wrong');
        Test.stopTest();
    }
    
    @isTest
    static void supplementRentBalanceMismatch(){
        Date dateField = System.today();
		Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
		Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);

		Lease__c leaseRecord = new Lease__c(
			Name = 'Testing',
			Lease_Start_Date_New__c = Date.valueOf('2020-03-01'),
			Lease_End_Date_New__c = Date.valueOf('2021-11-12'),
			Threshold_Claim_Amount__c = 5000
			);
        insert leaseRecord;
        
        // Set up the Aircraft record
		Aircraft__c asset = new Aircraft__c(Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available', Derate__c=20, MSN_Number__c = '28216');
		insert asset;
        
		// update lease reocrd with aircraft
		leaseRecord.Aircraft__c=asset.id;
		LeaseWareUtils.clearFromTrigger();
		update leaseRecord;

		Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
		insert legalEntity;

		Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
		insert operator;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);

		List<Constituent_Assembly__c> assemblyList = new List<Constituent_Assembly__c>();
		assemblyList.add(new Constituent_Assembly__c(Name='Airframe', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
		                                             TSLV__c=0, CSLV__c= 0, Type__c ='Airframe', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId(),
		                                             Serial_Number__c='Airframe'));
		assemblyList.add(new Constituent_Assembly__c(Name='Engine 1', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
		                                             TSLV__c=0, CSLV__c= 0, Type__c ='Engine 1', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId(),
		                                             Serial_Number__c='Engine 1'));

		LeaseWareUtils.clearFromTrigger();
		insert assemblyList;

		// Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(name='Test',Current_Catalog_Year__c='2020');
		insert mp;

		List<Maintenance_Program_Event__c> mpeList = new List<Maintenance_Program_Event__c>();
		mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Airframe', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = '4C/6Y'));
		mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Engine', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = 'Performance Restoration'));
		insert mpeList;

		// Set up Project Event for Airframe
		List<Assembly_Event_Info__c> projectedEventList = new List<Assembly_Event_Info__c>();
		projectedEventList.add(new Assembly_Event_Info__c(Name = 'Airframe - 4C/6Y', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[0].Id, Maintenance_Program_Event__c = mpeList[0].Id));
		projectedEventList.add(new Assembly_Event_Info__c(Name = 'Engine 1 - PR', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[1].Id, Maintenance_Program_Event__c = mpeList[1].Id));
		insert projectedEventList;

		List<Assembly_MR_Rate__c> supplementalRentList = new List<Assembly_MR_Rate__c>();
		supplementalRentList.add(new Assembly_MR_Rate__c(Name='Airframe - 4C/6Y', Assembly_Lkp__c = assemblyList[0].id, Assembly_Event_Info__c = projectedEventList[0].id,
		                                                 Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Induction Date', Rate_Basis__c='Cycles',
		                                                 Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
		supplementalRentList.add(new Assembly_MR_Rate__c(Name='Engine 1 - PR', Assembly_Lkp__c = assemblyList[1].id, Assembly_Event_Info__c = projectedEventList[1].id,
		                                                 Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Removal Date', Rate_Basis__c='Cycles',
		                                                 Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
		LWGlobalUtils.setTriggersflagOn(); 
        insert supplementalRentList;
        LWGlobalUtils.setTriggersflagOff(); 
        
        Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing 1',
            FH_String__c ='61',
            Airframe_Cycles_Landing_During_Month__c= 23,
            Aircraft__c = asset.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
			Type__c = 'Actual',
			Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2020,03,31)          
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord1;
        LWGlobalUtils.setTriggersflagOff(); 
        
		
		Constituent_Assembly__c ConsttAssembly =[select id,name from Constituent_Assembly__c where Attached_Aircraft__r.Lease__c =: leaseRecord.id and 
														Type__c='Airframe'];  // Checking for APU
		Assembly_MR_Rate__c assemblyMRInfo1 = [select id,Name,Lease__c from Assembly_MR_Rate__c where Lease__c =:leaseRecord.Id 
															AND RecordType.DeveloperName = 'MR' AND Assembly_Lkp__c =:ConsttAssembly.Id limit 1];
	
		Utilization_Report__c utilization = [Select id, name from Utilization_Report__c where Aircraft__r.Lease__c =:leaseRecord.Id];
		System.debug('assemblyMRInfo1 NAME:' + assemblyMRInfo1.Name);
		System.debug('utilization:' + utilization);

		// create Invoice record
		Invoice__c invoiceRecord = new Invoice__c(
			Name='Testing',
			Utilization_Report__c = utilization.Id,
			Lease__c = assemblyMRInfo1.Lease__c,
			Amount__c = 10000,
			status__c = 'Approved'
			);
        LWGlobalUtils.setTriggersflagOn(); 
		insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff(); 
		
        // create Payment record
           Payment__c paymentRecord = new Payment__c(
			Name = 'Payment Testing',
			Payment_Date__c = date.valueOf('2020-12-08'),
			Invoice__c = invoiceRecord.Id,
			Payment_Status__c = 'Approved',
			Amount__c = 200
			);
        LWGlobalUtils.setTriggersflagOn(); 
		insert paymentRecord;
        LWGlobalUtils.setTriggersflagOff(); 
        
		SupplementalRentTransactionsBatch snapshotBatch =new SupplementalRentTransactionsBatch(new set<Id>{leaseRecord.Id},false);
		Database.executeBatch(snapshotBatch,SupplementalRentTransactionsBatch.getBatchSize());

		
		Supplemental_Rent_Transactions__c[] listCurSS = [Select id, Name, Transaction_Type__c,Sequence_Number__c,
		                                                 Amount_Cash__c,Amount_Cash_Sum__c,Amount_Invoiced_Sum__c,Amount_Invoiced__c,Supplemental_Rent__r.Name,
		                                                 Transaction_Date__c,Running_Balance_Cash_Collected__c,Running_Balance_Invoiced__c,Running_Balance_Cash__c,
		                                                 Running_Balance_Invoice__c
		                                                 from
		                                                 Supplemental_Rent_Transactions__c where Supplemental_Rent__c =: assemblyMRInfo1.Id order by Sequence_Number__c asc];

		System.debug('TEST: MR Payment : :' + listCurSS.size() +'::'+ listCurSS);
       
        Test.startTest();
        Admin_Data_Validation__c adm = new Admin_Data_Validation__c(name = 'Supplemental Rent Balance and Running Balance mismatch',Category__c = 'Finance',Object_Name__c = 'Supplement Rent',Impact__c = 'High',Desc__c = null
                             ,Condition__c = null,Batch_Size__c = 100,Script_Method_Name__c  = 'FIN_SR_BAL_RUNNING_BAL_MISMATCH',Active__c = true );
        insert adm;
        map<string,string> paramMap = new map<string,string>();
        paramMap.put('PARENT_ID',adm.id);
        BackgroundDataValidationScriptHelper bgdValScrHelp = new BackgroundDataValidationScriptHelper();
        list<Assembly_MR_Rate__c> listSR = database.query(bgdValScrHelp.supplementRentBalMismatchStart(paramMap)[0]);
        for(Assembly_MR_Rate__c SR :listSR){
             System.debug('SR Total Trans Balance(Invoiced) '+SR.Transactions_Total_Invoiced__c);
             System.debug('SR Total Trans Balance(Cash) '+SR.Transactions_Total_Cash__c);
        }
        system.assertEquals(2, listSR.size(), 'No of records returned by the validation script query is wrong');
        
        bgdValScrHelp.supplementRentBalMismatchExecute(listSR,paramMap);
        list<Admin_Data_Validation_Result__c> adList = [select id from Admin_Data_Validation_Result__c];
        System.debug('adList'+adList.size());
        Test.stopTest();
		
    }

    @Istest
    static void notificationSetupRecordMissing(){
        Test.startTest();

        Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
        ns1.Object_API_Name__c = leasewareutils.getNamespacePrefix() + 'Lease__c';
        ns1.Object_Name__c = 'Lease';
        ns1.Notification_Type__c = 'On Create';
        insert ns1;

        Admin_Data_Validation__c adm = new Admin_Data_Validation__c(name = 'Test',Category__c = 'Technical',Object_Name__c = 'Notification Setting',Impact__c = 'Medium',Desc__c = null
                             ,Condition__c = null,Batch_Size__c = 100,Script_Method_Name__c  = 'Test',Active__c = true );
        insert adm;
        map<string,string> paramMap = new map<string,string>();
        paramMap.put('PARENT_ID',adm.id);
        BackgroundDataValidationScriptHelper bgdValScrHelp = new BackgroundDataValidationScriptHelper();
        list<Notifications_Subscription__c> listNS = database.query(bgdValScrHelp.notificationSetupMissingStart(paramMap)[0]);
       
        bgdValScrHelp.notificationSetupMissingExecute(listNS,paramMap);
        list<Admin_Data_Validation_Result__c> adList = [select id from Admin_Data_Validation_Result__c];
        system.assertEquals(1, adList.size(), 'Notification Setting Record Missing');
        Test.stopTest();
    }

    @Istest
    static void notificationSubscriptionInError(){
        Test.startTest();
        Notification_Setting__c ns = new Notification_Setting__c(name='Setup');
        insert ns;


        Notifications_Subscription__c ns1 = new Notifications_Subscription__c();
        ns1.Object_API_Name__c = 'Lease__c';
        ns1.Object_Name__c = 'Lease';
        ns1.Notification_Type__c = 'On Create';
        ns1.Active__c = true;
        ns1.Status__c='Error';
        Notify_Utility.setSourceOfCall('INTERNAL_API');
        insert ns1;
       
        Admin_Data_Validation__c adm = new Admin_Data_Validation__c(name = 'Test',Category__c = 'Technical',Object_Name__c = 'Notification Subscription',Impact__c = 'Medium',Desc__c = null
                             ,Condition__c = null,Batch_Size__c = 100,Script_Method_Name__c  = 'Test',Active__c = true );
        insert adm;
        map<string,string> paramMap = new map<string,string>();
        paramMap.put('PARENT_ID',adm.id);
        BackgroundDataValidationScriptHelper bgdValScrHelp = new BackgroundDataValidationScriptHelper();
        list<Notifications_Subscription__c> listNS = database.query(bgdValScrHelp.notificationSubscriptionInErrorStart(paramMap)[0]);
        system.assertEquals(1, listNS.size(), 'Notification Subscription In Error');
        bgdValScrHelp.notificationSubscriptionInErrorExecute(listNS,paramMap);
        list<Admin_Data_Validation_Result__c> adList = [select id from Admin_Data_Validation_Result__c];
        system.assertEquals(1, adList.size(), 'Notification Subscription In Error');
        Test.stopTest();
    }

}