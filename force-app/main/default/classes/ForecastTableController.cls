public class ForecastTableController {
    
    
    /** 
    * @description: Return the status of the Scenario_Input__c record.
    * @param: recordId : Scenario_Input__c record Id
    */
    @AuraEnabled
    public static String isForecastGenerated(String recordId) {
        return Utility.checkForecastStatus(recordId);
    }
    
    //this method is for demo purpose
    @remoteAction
    public static ForecastWrapper getForecastTableData(String recordId, Boolean loadHData) {
        return getForecastData(recordId, loadHData);
    }
    
    @AuraEnabled(cacheable=true)
    public static ForecastWrapper getForecastData(String recordId, Boolean loadHData) {
        try{
            List<Scenario_Component__c> listSC = [SELECT Id, Type__c, Assembly_Display_Name__c, Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c,
                                                  Fx_General_Input__r.Investment_Required_Purchase_Price__c
                                                  FROM Scenario_Component__c
                                                  Where Fx_General_Input__c =: recordId 
                                                  AND Life_Limited_part_LLP__c = NULL 
                                                  AND Type__c != 'General' ORDER BY Type__c]; 
            
            Scenario_Input__c scInp = [Select Id, Lease__c, historyMRCheck__c from Scenario_Input__c where id =: recordId];
            
            List<Monthly_Scenario__c> lstMonthlyScenario = [SELECT id,Fx_Component_Output__r.Id,Shortfall_FH__c,End_Date__c,Fx_Component_Output__r.First_Event__c,Return_Month__c, Shortfall_Month__c, Fx_Component_Output__r.Fx_General_Input__r.EOLA__c, Fx_Component_Output__r.Name,
                                                            Fx_Component_Output__r.Assembly__c,Shortfall_FC__c, Fx_Component_Output__r.Assembly_Display_Name__c, Fx_Component_Output__r.Rate_Basis__c,Fx_Component_Output__r.RC_Applicability__c,
                                                            Start_Date__c, Cash_Flow_F__c, Fx_Component_Output__r.Type__c, Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c,
                                                            Fx_Component_Output__r.Total_PR_Cost_F__c, Fx_Component_Output__r.Total_MR_EOLA_Amount_F__c, Fx_Component_Output__r.Total_CF_F__c,
                                                            Fx_Component_Output__r.Total_Claim_F__c, Lowest_Limiter_LLP_Name__c, Lowest_Limiter_LLP_Cycle_Remaining__c, Eola_Amount__c, 
                                                            MR_OR_EOLA_Amount__c, Acc_MR_F__c, Claim_F__c, MR_Amount__c, PR_Cost_F__c, Event_Date__c, Rent_F__c, SD_F__c, Hours_Remaining__c, 
                                                            Cycles_Remaining__c, Months_Remaining__c, RateBasis_Value__c, RateBasis_Label__c,TSO__c,CSO__c, Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c,
                                                            Fx_Component_Output__r.Fx_General_Input__r.Asset__r.Lease__r.Lease_Start_Date_New__c, Historical_Month__c, Historical_Event__c,
                                                            Fx_Component_Output__r.Assembly__r.Serial_Number__c, AVG_FC__c,AVG_FH__c ,Fx_Component_Output__r.Last_Event_Date__c, 
                                                            Fx_Component_Output__r.Event_Type__c, Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c, Fx_Component_Output__r.Life_Limit_Interval_1_Hours__c, 
                                                            Fx_Component_Output__r.Life_Limit_Interval_1_Months__c, Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c, 
                                                            Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c, Fx_Component_Output__r.Life_Limit_Interval_2_Months__c,Month_Since_Overhaul__c,Fx_Component_Output__r.LLP_Build_Standard__c,FH__c,FC__c,Return_Condition_Status__c,
                                                            InterpolatedMRRate__c,Effective_Escalation_Factor_MR__c,Current_Rate_Escalated__c,Fx_Component_Output__r.RC_Months__c,Fx_Component_Output__r.RC_FC__c,Fx_Component_Output__r.RC_FH__c,Fx_Component_Output__r.Return_Condition_Status__c
                                                            FROM Monthly_Scenario__c
                                                            WHERE Fx_Component_Output__r.Fx_General_Input__c =: recordId  
                                                            AND Fx_Component_Output__r.Life_Limited_Part_LLP__c = null
                                                            order by Start_Date__c, Fx_Component_Output__r.Assembly_Display_Name__c];
            
            
            
            return fetchForecastData(recordId, listSC, lstMonthlyScenario, loadHData, scInp);
        }catch(Exception e){
            system.debug('Error: '+e.getMessage()+','+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
            throw new AuraHandledException('An Error has Occurred: '+e.getMessage()+','+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
    }
    
    private static ForecastWrapper fetchForecastData(String recID, List<Scenario_Component__c> listSC, List<Monthly_Scenario__c> lstMonthlyScenario, Boolean loadHData, Scenario_Input__c scInp) {
        //To find weather the record are of Airline or Lessor
        String val = Utility.checkAirline();
        
        ForecastWrapper fw = new ForecastWrapper();
        List<ComponentWrapper> lstComponentWrapper = new List<ComponentWrapper>();
        Map<String, List<Monthly_Scenario__c>> mapAssemblyMonthlyData = new Map<String, List<Monthly_Scenario__c>>();
        //LW-AKG: 07/01/2020
        Map<String, List<Monthly_Scenario__c>> mapAssemblyNameMonthlyScenario = new Map<String, List<Monthly_Scenario__c>>();
        //LW-AKG: 07/01/2020
        Set<String> assemblies = new Set<String>();
        List<AssemblyWrapper> lstAW = new List<AssemblyWrapper>();
        String leaseDate = '';
        Decimal investmentRequired = 0;
        
        //Added on 08/04/2019 to show investment as first row on table - Start
        if(!listSC.isEmpty()){
            leaseDate = Utility.formatDate(listSC[0].Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c.addMonths(-1));
            
            if(!String.isBlank(leaseDate) && listSC[0].Fx_General_Input__r.Investment_Required_Purchase_Price__c != null){
                investmentRequired = listSC[0].Fx_General_Input__r.Investment_Required_Purchase_Price__c > 0 ? listSC[0].Fx_General_Input__r.Investment_Required_Purchase_Price__c * (-1) : listSC[0].Fx_General_Input__r.Investment_Required_Purchase_Price__c;
                
                if(loadHData && investmentRequired != 0){
                    for(Scenario_Component__c sc : listSC){
                        if (!mapAssemblyMonthlyData.containskey(leaseDate)){
                            mapAssemblyMonthlyData.put(leaseDate, new List<Monthly_Scenario__c>());
                        }
                        mapAssemblyMonthlyData.get(leaseDate).add(new Monthly_Scenario__c(Fx_Component_Output__c = sc.Id, Fx_Component_Output__r = sc));
                    }
                }
            }
        }
        //Added on 08/04/2019 to show investment as first row on table - End
        
        for (Monthly_Scenario__c ms: lstMonthlyScenario){
            String finalAssemblyName = ms.Fx_Component_Output__r.type__c + ms.Fx_Component_Output__r.Event_Type__c;
            if(!loadHData){
                if(!ms.Historical_Month__c){
                    string key = Utility.formatDate(ms.Start_Date__c);
                    if (!mapAssemblyMonthlyData.containskey(key)){
                        mapAssemblyMonthlyData.put(key, new List<Monthly_Scenario__c>());
                    }
                    //LW-AKG: 07/01/2020
                    if(!mapAssemblyNameMonthlyScenario.containskey(ms.Fx_Component_Output__r.Assembly_Display_Name__c)){
                        mapAssemblyNameMonthlyScenario.put(ms.Fx_Component_Output__r.Assembly_Display_Name__c,new List<Monthly_Scenario__c>());
                    }
                    mapAssemblyNameMonthlyScenario.get(ms.Fx_Component_Output__r.Assembly_Display_Name__c).add(ms);
                    //LW-AKG: 07/01/2020
                    mapAssemblyMonthlyData.get(key).add(ms);
                    
                    // for assembly header and sub headers
                    if (ms.Fx_Component_Output__r.Type__c != 'General' && !assemblies.contains(finalAssemblyName)){
                        AssemblyWrapper aw = new AssemblyWrapper();
                        aw.assemblyId = ms.Fx_Component_Output__r.Assembly__c;
                        aw.assemblyName = ms.Fx_Component_Output__r.Assembly_Display_Name__c;
                        aw.serialNumber = ms.Fx_Component_Output__r.Assembly__r.Serial_Number__c;
                        aw.AVGFC = ms.AVG_FC__c;
                        aw.AVGFH = ms.AVG_FH__c;
                        aw.lastEventDate = ms.Fx_Component_Output__r.Last_Event_Date__c;
                        aw.eventType = ms.Fx_Component_Output__r.Event_Type__c;
                        aw.interval_1_Months = LeaseWareUtils.zeroIfNull(ms.Fx_Component_Output__r.Life_Limit_Interval_1_Months__c);
                        aw.interval_1_Hours = LeaseWareUtils.zeroIfNull(ms.Fx_Component_Output__r.Life_Limit_Interval_1_Hours__c);
                        aw.interval_1_Cycles = LeaseWareUtils.zeroIfNull(ms.Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c); 
                        aw.interval_2_Months = LeaseWareUtils.zeroIfNull(ms.Fx_Component_Output__r.Life_Limit_Interval_2_Months__c);
                        aw.interval_2_Hours = LeaseWareUtils.zeroIfNull(ms.Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c);
                        aw.interval_2_Cycles = LeaseWareUtils.zeroIfNull(ms.Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c);
                        
                        if(ms.Fx_Component_Output__r.Type__c.contains('Engine')){
                            if(ms.Fx_Component_Output__r.Event_Type__c.contains('LLP')){
                                aw.staticResourceVal = 'LLP.svg';
                            }else{
                                aw.staticResourceVal = 'ENGINE.svg';
                            }          
                        }else if(ms.Fx_Component_Output__r.Assembly_Display_Name__c.contains('LG')){
                            if(ms.Fx_Component_Output__r.Assembly_Display_Name__c.contains('Nose')){
                                aw.staticResourceVal = 'NLG.svg';
                            }else{
                                aw.staticResourceVal = 'LDG.svg';
                            }    
                        }  
                        else if(ms.Fx_Component_Output__r.Assembly_Display_Name__c.contains('Propeller')){
                            aw.staticResourceVal = 'PROPELLER.svg';
                        }
                        else{
                            aw.staticResourceVal = ms.Fx_Component_Output__r.Type__c + '.svg';
                        }
                        if(aw.staticResourceVal == null || aw.staticResourceVal == ''){
                            aw.staticResourceVal = 'OTHERS.svg';
                        }
                        
                        String assemblyTypeVal = ms.Fx_Component_Output__r.Assembly_Display_Name__c.trim();
                        assemblyTypeVal = assemblyTypeVal.trim();
                        assemblyTypeVal = assemblyTypeVal.replaceAll('(\\s+)','_');
                        assemblyTypeVal = assemblyTypeVal.replaceAll('[^\\w((?<=\')(.*)(?=\'))]','_');
                        aw.assemblyType = assemblyTypeVal;
                        
                        aw.scenarioComponent = ms.Fx_Component_Output__r;
                        List<AssemblyColumnsWrapper> lstACW = setAssemblyColumns(ms, scInp);
                        aw.lstAssemblyColumnsWrapper = lstACW;
                        // adding 1 because CF column is hard coded on page
                        
                        integer columnCount = 0;
                        for(AssemblyColumnsWrapper acw : lstACW){
                            if(acw.showHeader){ columnCount ++; }
                        }
                        aw.assemblyColumnsCount = columnCount + 1;
                        
                        String bgClass = 'even';
                        if(Math.mod(lstAW.size(), 2) == 0){ bgClass = 'odd'; }
                        aw.bgClass = bgClass;
                        
                        lstAW.add(aw);
                        assemblies.add(finalAssemblyName);
                    }
                }
                else{
                    if(ms.Fx_Component_Output__r.Type__c == 'General'){
                        fw.totalRent += ms.Rent_F__c;
                        fw.totalSecurityDeposit += ms.SD_F__c;
                        fw.totalcashFlowPerMonth +=  (ms.Rent_F__c == null ? 0 : ms.Rent_F__c)  + (ms.SD_F__c == null ? 0 : ms.SD_F__c);
                        fw.previousCashFlowAcc += (ms.Rent_F__c == null ? 0 : ms.Rent_F__c) + (ms.SD_F__c == null ? 0 : ms.SD_F__c);
                    }else{
                        fw.totalcashFlowPerMonth += (ms.Cash_Flow_F__c == null ? 0 : ms.Cash_Flow_F__c);
                        fw.previousCashFlowAcc += (ms.Cash_Flow_F__c == null ? 0 : ms.Cash_Flow_F__c);
                    }
                }
            }
            else{
                string key = Utility.formatDate(ms.Start_Date__c);
                if (!mapAssemblyMonthlyData.containskey(key)){
                    mapAssemblyMonthlyData.put(key, new List<Monthly_Scenario__c>());
                }
                //LW-AKG: 07/01/2020
                if(!mapAssemblyNameMonthlyScenario.containskey(ms.Fx_Component_Output__r.Assembly_Display_Name__c)){
                    mapAssemblyNameMonthlyScenario.put(ms.Fx_Component_Output__r.Assembly_Display_Name__c,new List<Monthly_Scenario__c>());
                }
                mapAssemblyNameMonthlyScenario.get(ms.Fx_Component_Output__r.Assembly_Display_Name__c).add(ms);
                //LW-AKG: 07/01/2020
                mapAssemblyMonthlyData.get(key).add(ms); 
                
                // for assembly header and sub headers
                if (ms.Fx_Component_Output__r.Type__c != 'General' && !assemblies.contains(finalAssemblyName)){
                    AssemblyWrapper aw = new AssemblyWrapper();
                    aw.assemblyId = ms.Fx_Component_Output__r.Assembly__c;
                    aw.assemblyName = ms.Fx_Component_Output__r.Assembly_Display_Name__c.trim();
                    
                    aw.serialNumber = ms.Fx_Component_Output__r.Assembly__r.Serial_Number__c;
                    aw.AVGFC = ms.AVG_FC__c;
                    aw.AVGFH = ms.AVG_FH__c;
                    aw.lastEventDate = ms.Fx_Component_Output__r.Last_Event_Date__c;
                    aw.eventType = ms.Fx_Component_Output__r.Event_Type__c;
                    aw.interval_1_Months = ms.Fx_Component_Output__r.Life_Limit_Interval_1_Months__c;
                    aw.interval_1_Hours = ms.Fx_Component_Output__r.Life_Limit_Interval_1_Hours__c;
                    aw.interval_1_Cycles = ms.Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c;  
                    aw.interval_2_Months = ms.Fx_Component_Output__r.Life_Limit_Interval_2_Months__c;
                    aw.interval_2_Hours = ms.Fx_Component_Output__r.Life_Limit_Interval_2_Hours__c;
                    aw.interval_2_Cycles = ms.Fx_Component_Output__r.Life_Limit_Interval_2_Cycles__c;
                    
                    if(ms.Fx_Component_Output__r.Type__c.contains('Engine')){
                        if(ms.Fx_Component_Output__r.Event_Type__c.contains('LLP')){
                            aw.staticResourceVal = 'LLP.svg';
                        }else{
                            aw.staticResourceVal = 'ENGINE.svg';
                        }          
                    }else if(ms.Fx_Component_Output__r.Assembly_Display_Name__c.contains('LG')){
                        if(ms.Fx_Component_Output__r.Assembly_Display_Name__c.contains('Nose')){
                            aw.staticResourceVal = 'NLG.svg';
                        }else{
                            aw.staticResourceVal = 'LDG.svg';
                        }    
                    }  
                    else if(ms.Fx_Component_Output__r.Assembly_Display_Name__c.contains('Propeller')){
                        aw.staticResourceVal = 'PROPELLER.svg';
                    }
                    else{
                        aw.staticResourceVal = ms.Fx_Component_Output__r.Type__c + '.svg';
                    }
                    if(aw.staticResourceVal == null || aw.staticResourceVal == ''){
                        aw.staticResourceVal = 'OTHERS.svg';
                    }
                    
                    String assemblyTypeVal = ms.Fx_Component_Output__r.Assembly_Display_Name__c.trim();
                    assemblyTypeVal = assemblyTypeVal.trim();
                    assemblyTypeVal = assemblyTypeVal.replaceAll('(\\s+)','_');
                    assemblyTypeVal = assemblyTypeVal.replaceAll('[^\\w((?<=\')(.*)(?=\'))]','_');
                    aw.assemblyType = assemblyTypeVal;
                    
                    aw.scenarioComponent = ms.Fx_Component_Output__r;
                    List<AssemblyColumnsWrapper> lstACW = setAssemblyColumns(ms, scInp);
                    aw.lstAssemblyColumnsWrapper = lstACW;
                    // adding 1 because CF column is hard coded on page
                    
                    integer columnCount = 0;
                    for(AssemblyColumnsWrapper acw : lstACW){
                        if(acw.showHeader){ columnCount ++; }
                    }
                    aw.assemblyColumnsCount = columnCount + 1;
                    
                    String bgClass = 'even';
                    if(Math.mod(lstAW.size(), 2) == 0){ bgClass = 'odd'; }
                    aw.bgClass = bgClass;
                    lstAW.add(aw);
                    assemblies.add(finalAssemblyName);                    
                }
            }
        }
        
        Integer count = 0;
        Decimal cash = fw.previousCashFlowAcc;
        String lastHistoricalRecord = '';
        for (String s : mapAssemblyMonthlyData.keyset()){
            ComponentWrapper cw = new ComponentWrapper();
            cw.startDate = s;
            List<MonthlyScenarioWrapper> lstMSW = new List<MonthlyScenarioWrapper>();
            String bgClass = 'even';
            Boolean flag = true,isLastMontlyScenario = true,checkReserveType =true;
            Boolean investmentValueUsed = false;
            count ++;
            for (Monthly_Scenario__c ms : mapAssemblyMonthlyData.get(s)){
                //LW-AKG: 07/01/2020
                Integer length=0;
                if(mapAssemblyNameMonthlyScenario.get(ms.Fx_Component_Output__r.Assembly_Display_Name__c)!=null){
                   length =mapAssemblyNameMonthlyScenario.get(ms.Fx_Component_Output__r.Assembly_Display_Name__c).size();
                }
                //LW-AKG: 07/01/2020
                if(s == leaseDate && investmentRequired != 0){
                    if(!investmentValueUsed){
                        cw.cashFlow +=  investmentRequired;
                        cw.cashFlowAcc = cash + investmentRequired;
                        investmentValueUsed = true;
                    }
                    
                    bgClass = 'even';
                    if (Math.mod(lstMSW.size(),2) == 0){
                        bgClass = 'odd'; 
                    }
                    MonthlyScenarioWrapper msw = new MonthlyScenarioWrapper();
                    msw.bgClass = bgClass;
                    if(ms.Fx_Component_Output__r != null){
                        msw.assemblyType = ms.Fx_Component_Output__r.Assembly_Display_Name__c.trim();
                        msw.assemblyType = msw.assemblyType.replaceAll('(\\s+)','_');
                        msw.assemblyType = msw.assemblyType.replaceAll('[^\\w((?<=\')(.*)(?=\'))]','_');
                    }
                    lstMSW.add(msw);
                }
                else{
                    //String assemblies = '';
                    cw.msRecordId = ms.Id;
                    if(ms.Fx_Component_Output__r.Type__c == 'General'){
                        cw.rent = ms.Rent_F__c;
                        cw.securityDeposit =  ms.SD_F__c;
                        
                        cw.cashFlow =  (ms.MR_Amount__c == null ? 0 : ms.MR_Amount__c);
                        cw.cashFlowAcc = cash + (ms.Rent_F__c == null ? 0 : ms.Rent_F__c) + (ms.SD_F__c == null ? 0 : ms.SD_F__c);
                    }else{
                        
                        cw.cashFlowAcc = cash + (ms.Cash_Flow_F__c == null ? 0 : ms.Cash_Flow_F__c);
                        if (ms.Event_Date__c != null){
                            bgClass = 'hasevent';
                            if(flag){
                                cw.eventClass = 'hasevent';
                                flag = false;
                            }
                        }
                        else {
                            bgClass = 'even';
                            if (Math.mod(lstMSW.size(),2) == 0){
                                bgClass = 'odd'; 
                            }
                        }
                        MonthlyScenarioWrapper msw = new MonthlyScenarioWrapper(ms, bgClass);
                       
                        //Draw Red line
                        if(ms.Fx_Component_Output__r.Return_Condition_Status__c != null &&  Utility.formatDate(ms.Fx_Component_Output__r.Return_Condition_Status__c) == Utility.formatDate(ms.End_Date__c) && length != count){
                             msw.shortfallWaterMarkClass = 'shortfallWaterMark'; 
                        } 
                        //LW-AKG: 06/16/2020
                        if(fw.reserveType == null){
                            fw.reserveType = fetchReserveTypeVal(ms, scInp.historyMRCheck__c);
                        }
                        if(fw.reserveType != 'MR' && count == mapAssemblyMonthlyData.size()){
                            msw.lastNode = true;
                        }
                        lstMSW.add(msw);
                    }
                    if(ms.Fx_Component_Output__r.Type__c == 'General'){
                        fw.totalRent += cw.rent;
                        fw.totalSecurityDeposit += cw.securityDeposit;
                        fw.totalcashFlowPerMonth +=  (ms.Rent_F__c == null ? 0 : ms.Rent_F__c) + (ms.SD_F__c == null ? 0 : ms.SD_F__c);
                    }else{
                        fw.totalcashFlowPerMonth += (ms.Cash_Flow_F__c == null ? 0 : ms.Cash_Flow_F__c);
                    }
                }
                if(ms.Historical_Month__c){
                    cw.showHisoricalMonth = 'historicalMonth showHistoricalMonth';
                    lastHistoricalRecord = ms.Id;
                }
                else{
                    cw.showHisoricalMonth = '';
                }
                cash = cw.cashFlowAcc;
            }
            //System.debug('fw'+cw.eventClass+cw.startDate);
            cw.lstMSW = lstMSW;
            lstComponentWrapper.add(cw);
        }
        
        for(ComponentWrapper cw : lstComponentWrapper){
            if(cw.msRecordId == lastHistoricalRecord){
                cw.showHisoricalMonth = cw.showHisoricalMonth+' lastHistoricalRecord';
            }
        }
        
        fw.lstComponentWrapper = lstComponentWrapper;
        fw.lstAssemblyWrapper = lstAW;
        if(investmentRequired != 0){
            fw.totalcashFlowPerMonth += investmentRequired; 
        }
        return fw;
    }
    
    private static String fetchReserveTypeVal(Monthly_Scenario__c ms, Boolean historyMRCheck){
        Boolean isEOLA = ms.Fx_Component_Output__r.Fx_General_Input__r.EOLA__c;
        String extType = ms.Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c;
        String reserveType = '';
        if(isEOLA && extType != 'Monthly MR')
            reserveType = 'EOLA';
        else if(isEOLA && extType == 'Monthly MR')
            reserveType = 'BOTH';
        else if(isEOLA == false && extType != 'EOLA')
            reserveType = 'MR';
        else if(isEOLA == false && extType == 'EOLA')
            reserveType = 'BOTH';
        
        if(reserveType == 'EOLA' && historyMRCheck)
         	reserveType = 'BOTH';
        
        return reserveType;
    }
    
    static List<AssemblyColumnsWrapper> setAssemblyColumns(Monthly_Scenario__c ms, Scenario_Input__c scInp){
        List<AssemblyColumnsWrapper> lstACW = new List<AssemblyColumnsWrapper>();
        String reserveType = fetchReserveTypeVal(ms, scInp.historyMRCheck__c);
        if (!ms.Fx_Component_Output__r.Event_Type__c.contains('LLP')){
            if(reserveType == 'EOLA'){
                lstACW.add(new AssemblyColumnsWrapper('EOLA',true));
                lstACW.add(new AssemblyColumnsWrapper('Acc. MR',  false));
                lstACW.add(new AssemblyColumnsWrapper('PR Cost', true));
                lstACW.add(new AssemblyColumnsWrapper('Claim', false));
                //lstACW.add(new AssemblyColumnsWrapper('Remainning Life', true));
                lstACW.add(new AssemblyColumnsWrapper(ms.RateBasis_Label__c , true));
                lstACW.add(new AssemblyColumnsWrapper('LowestLimiter' , false));
            }else if(reserveType == 'Both'){
                lstACW.add(new AssemblyColumnsWrapper('MR/EOLA',true));
                lstACW.add(new AssemblyColumnsWrapper('Acc. MR',  true));
                lstACW.add(new AssemblyColumnsWrapper('PR Cost', true));
                lstACW.add(new AssemblyColumnsWrapper('Claim', true));
                //lstACW.add(new AssemblyColumnsWrapper('Remainning Life', true));
                lstACW.add(new AssemblyColumnsWrapper(ms.RateBasis_Label__c , true));
                lstACW.add(new AssemblyColumnsWrapper('LowestLimiter' , false));
            }else{
                lstACW.add(new AssemblyColumnsWrapper('MR',true));
                lstACW.add(new AssemblyColumnsWrapper('Acc. MR',  true));
                lstACW.add(new AssemblyColumnsWrapper('PR Cost', true));
                lstACW.add(new AssemblyColumnsWrapper('Claim', true));
                //lstACW.add(new AssemblyColumnsWrapper('Remainning Life', true));
                lstACW.add(new AssemblyColumnsWrapper(ms.RateBasis_Label__c , true));
                lstACW.add(new AssemblyColumnsWrapper('LowestLimiter' , false));
            }
        }
        else {
            if(reserveType == 'EOLA'){
                lstACW.add(new AssemblyColumnsWrapper('EOLA',true));
                lstACW.add(new AssemblyColumnsWrapper('Acc. MR', false));
                lstACW.add(new AssemblyColumnsWrapper('LLP Cost', true));
                lstACW.add(new AssemblyColumnsWrapper('Claim', false));
                //lstACW.add(new AssemblyColumnsWrapper('Remainning Life', true));
                lstACW.add(new AssemblyColumnsWrapper(ms.RateBasis_Label__c , false));
                lstACW.add(new AssemblyColumnsWrapper('LowestLimiter' , true));
            }else if(reserveType == 'Both'){
                lstACW.add(new AssemblyColumnsWrapper('MR/EOLA',true));
                lstACW.add(new AssemblyColumnsWrapper('Acc. MR', true));
                lstACW.add(new AssemblyColumnsWrapper('LLP Cost', true));
                lstACW.add(new AssemblyColumnsWrapper('Claim', true));
                //lstACW.add(new AssemblyColumnsWrapper('Remainning Life', true));
                lstACW.add(new AssemblyColumnsWrapper(ms.RateBasis_Label__c , false));
                lstACW.add(new AssemblyColumnsWrapper('LowestLimiter' , true));
            }else{
                lstACW.add(new AssemblyColumnsWrapper('MR',true));
                lstACW.add(new AssemblyColumnsWrapper('Acc. MR', true));
                lstACW.add(new AssemblyColumnsWrapper('LLP Cost', true));
                lstACW.add(new AssemblyColumnsWrapper('Claim', true));
                //lstACW.add(new AssemblyColumnsWrapper('Remainning Life', true));
                lstACW.add(new AssemblyColumnsWrapper(ms.RateBasis_Label__c , false));
                lstACW.add(new AssemblyColumnsWrapper('LowestLimiter' , true));
            }
        }
        return lstACW;        
    }
    
    public class AssemblyWrapper {
        @AuraEnabled public String assemblyId{get;set;}
        @AuraEnabled public String assemblyName{get;set;}
        @AuraEnabled public String serialNumber {get;set;}
        @AuraEnabled public string staticResourceVal {get;set;}
        @AuraEnabled public String bgClass {get;set;}
        @AuraEnabled public String assemblyType {get;set;}
        @AuraEnabled public Scenario_Component__c scenarioComponent {get;set;}
        @AuraEnabled public Integer assemblyColumnsCount{get;set;}
        @AuraEnabled public List<AssemblyColumnsWrapper> lstAssemblyColumnsWrapper{get;set;}
        @AuraEnabled public Decimal AVGFC {get;set;}
        @AuraEnabled public Decimal AVGFH {get;set;}
        @AuraEnabled public Date lastEventDate {get;set;}
        @AuraEnabled public String eventType {get;set;}
        @AuraEnabled public Decimal interval_1_Months {get;set;}
        @AuraEnabled public Decimal interval_1_Hours {get;set;}
        @AuraEnabled public Decimal interval_1_Cycles {get;set;}
        @AuraEnabled public Decimal interval_2_Months {get;set;}
        @AuraEnabled public Decimal interval_2_Hours {get;set;}
        @AuraEnabled public Decimal interval_2_Cycles {get;set;}
        
        public AssemblyWrapper(){
            this.scenarioComponent = new Scenario_Component__c();
        }
    }
    
    public class AssemblyColumnsWrapper {
        @AuraEnabled public String fieldLabel{get;set;}        
        @AuraEnabled public Boolean showHeader{get;set;}
        @AuraEnabled public string hideColumnClass {get;set;}
        
        public AssemblyColumnsWrapper(String fieldLabel, Boolean showHeader){
            this.fieldLabel = fieldLabel;
            this.showHeader = showHeader;
            if(!showHeader)
                hideColumnClass = 'hideColumn';
        }
    }
    
    public class ForecastWrapper {
        @AuraEnabled public String reserveType{get;set;}
        @AuraEnabled public List<ComponentWrapper> lstComponentWrapper{get;set;}
        @AuraEnabled public List<AssemblyWrapper> lstAssemblyWrapper{get;set;}
        @AuraEnabled public Decimal totalRent {get;set;}
        @AuraEnabled public Decimal totalSecurityDeposit {get;set;}
        @AuraEnabled public Decimal totalcashFlowPerMonth {get;set;}
        @AuraEnabled public Decimal previousCashFlowAcc {get;set;}
        
        public ForecastWrapper(){
            this.totalRent = 0;
            this.totalSecurityDeposit = 0;
            this.totalcashFlowPerMonth = 0;
            this.previousCashFlowAcc = 0;
        }
    }
    
    public class ComponentWrapper {
        @AuraEnabled public String msRecordId {get;set;}
        @AuraEnabled public string startDate {get;set;}
        @AuraEnabled public Decimal rent {get;set;}
        @AuraEnabled public Decimal securityDeposit {get;set;}
        @AuraEnabled public Decimal cashFlow {get;set;}
        @AuraEnabled public Decimal cashFlowAcc {get;set;}
        @AuraEnabled public List<MonthlyScenarioWrapper> lstMSW{get;set;}
        @AuraEnabled public String eventClass {get;set;}
        @auraEnabled public String showHisoricalMonth{get;set;}
        
        public ComponentWrapper(){
            this.cashFlow = 0 ;
            this.cashFlowAcc = 0;
            this.rent = 0;
            this.securityDeposit = 0;
            this.lstMSW = new List<MonthlyScenarioWrapper>();
            this.eventClass = '';
            this.showHisoricalMonth = '';
        }
    }
    
    public class MonthlyScenarioWrapper {
        @AuraEnabled public Monthly_Scenario__c ms {get;set;}
        @AuraEnabled public string showlowestLimiter {get;set;}
        @AuraEnabled public string shortfallWaterMarkClass {get;set;}
        @AuraEnabled public String reserveType {get;set;}
        @AuraEnabled public string showrateBasic {get;set;}
        @AuraEnabled public decimal remainingLife {get;set;}
        @AuraEnabled public string lowestLimiterName {get;set;}
        @AuraEnabled public decimal UtilizationSinceLastSV {get;set;}
        @AuraEnabled public string bgClass {get;set;}
        @AuraEnabled public string assemblyType {get;set;}
        @AuraEnabled public string hideColumnClass {get;set;}
        @AuraEnabled public Boolean lastNode{get;set;}
        @AuraEnabled public Decimal FHDivideFC{get;set;}
        @AuraEnabled public decimal cashFlow{get;set;}
        @AuraEnabled public decimal MR_EOLA_Amount{get;set;}
        @AuraEnabled public decimal accMR{get;set;}
        @AuraEnabled public decimal prCost{get;set;}
        @AuraEnabled public decimal claimAmount{get;set;}
        
        @AuraEnabled public String rateBasicLabel{get;set;}
        @AuraEnabled public Decimal mrRateTooltip{get;set;}
        
        public MonthlyScenarioWrapper(){
            this.ms = new Monthly_Scenario__c();
            this.showlowestLimiter = 'llpLimiter';
            this.reserveType = '';
            this.shortfallWaterMarkClass='';
            this.showrateBasic = '';
            this.remainingLife = 0;
            this.lowestLimiterName = '';
            this.UtilizationSinceLastSV = 0;
            this.bgClass = '';
            this.hideColumnClass = 'hideClass';
            this.lastNode = false;
            this.cashFlow = 0;
            this.MR_EOLA_Amount = 0;
            this.accMR = 0;
            this.prCost = 0;
            this.claimAmount = 0;
            this.assemblyType = '';
        }
        public MonthlyScenarioWrapper(Monthly_Scenario__c ms, String bgClass){
            if(ms != null){
                this.lastNode = false;
                this.ms = ms;
                this.shortfallWaterMarkClass='';
                this.bgClass = bgClass;
                this.bgClass = bgClass;
                
                hideColumnClass = 'hideClass';
                if(ms.Fx_Component_Output__r != null ){
                    this.assemblyType = ms.Fx_Component_Output__r.Assembly_Display_Name__c.trim();
                    this.assemblyType = this.assemblyType.replaceAll('(\\s+)','_');
                    this.assemblyType = this.assemblyType.replaceAll('[^\\w((?<=\')(.*)(?=\'))]','_');
                    if(ms.Fx_Component_Output__r.Event_Type__c.contains('LLP')){
                        this.lowestLimiterName = ms.Lowest_Limiter_LLP_Name__c; 
                        this.showlowestLimiter ='';
                        this.showrateBasic ='llpBasic';
                        this.remainingLife = ms.Lowest_Limiter_LLP_Cycle_Remaining__c;
                    }else{
                        this.showlowestLimiter ='llpLimiter';
                        this.showrateBasic ='slds-show';
                        this.UtilizationSinceLastSV =  ms.RateBasis_Value__c; 
                        if(ms.Fx_Component_Output__r.Rate_Basis__c == 'Hours'){
                            this.remainingLife = ms.Hours_Remaining__c;
                        }else if(ms.Fx_Component_Output__r.Rate_Basis__c == 'Cycles'){
                            this.remainingLife = ms.Cycles_Remaining__c;
                        }else{
                            this.remainingLife = ms.Months_Remaining__c;
                        }
                    }
                }
                
                ms.FH__c = LeaseWareUtils.zeroIfNull(ms.FH__c);
                
                if(ms.FC__c != null && ms.FC__c != 0.0){
                    this.FHDivideFC = ms.FH__c/ms.FC__c;
                }
                if(ms.InterpolatedMRRate__c!=null){
                    this.mrRateTooltip = ms.InterpolatedMRRate__c * LeaseWareUtils.zeroIfNull(ms.Effective_Escalation_Factor_MR__c);
                }else{
                    this.mrRateTooltip = LeaseWareUtils.zeroIfNull(ms.Current_Rate_Escalated__c);	 
                }
                
                
                
                this.rateBasicLabel = ms.RateBasis_Label__c;
                this.cashFlow = ms.Cash_Flow_F__c;
                this.MR_EOLA_Amount = ms.MR_OR_EOLA_Amount__c;
                this.accMR = ms.Acc_MR_F__c;
                this.prCost = ms.PR_Cost_F__c;
                this.claimAmount = ms.Claim_F__c;
            }
        }
    }
}