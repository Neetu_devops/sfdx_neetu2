public class AircraftOptimizationTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'AircraftOptimizationTriggerHandlerBefore';
    private final string triggerAfter = 'AircraftOptimizationTriggerHandlerAfter';
    public AircraftOptimizationTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
		if(leaseWareUtils.isFromTrigger(triggerBefore)) return;
        leaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('AircraftOptimizationTriggerHandler.beforeInsert(+)');

    	getCMLRFromWF();

        system.debug('AircraftOptimizationTriggerHandler.beforeInsert(-)');       
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(leaseWareUtils.isFromTrigger(triggerBefore)) return;
        leaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AircraftOptimizationTriggerHandler.beforeUpdate(+)');
        
    	getCMLRFromWF();
    	
        system.debug('AircraftOptimizationTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        //if(leaseWareUtils.isFromTrigger(triggerBefore)) return;
        //leaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AircraftOptimizationTriggerHandler.beforeDelete(+)');
        //UpdateMSNOnMarketingActivity_D();
        
        system.debug('AircraftOptimizationTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        //if(leaseWareUtils.isFromTrigger(triggerAfter)) return;
        //leaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftOptimizationTriggerHandler.afterInsert(+)');
		callAfterInsUpd();

        system.debug('AircraftOptimizationTriggerHandler.afterInsert(-)');        
    }
     
    public void afterUpdate()
    {
        //if(leaseWareUtils.isFromTrigger(triggerAfter)) return;
        //leaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftOptimizationTriggerHandler.afterUpdate(+)');
        callAfterInsUpd();
        
        system.debug('AircraftOptimizationTriggerHandler.afterUpdate(-)');        
    }
     
    public void afterDelete()
    {
        //if(leaseWareUtils.isFromTrigger(triggerAfter)) return;
        //leaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftOptimizationTriggerHandler.afterDelete(+)');
        
        
        system.debug('AircraftOptimizationTriggerHandler.afterDelete(-)');        
    }

    public void afterUnDelete()
    {
        //if(leaseWareUtils.isFromTrigger(triggerAfter)) return;
        //leaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AircraftOptimizationTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('AircraftOptimizationTriggerHandler.afterUnDelete(-)');      
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	//after Insert,after update
    private void callAfterInsUpd(){
        
        Aircraft_Optimization__c[] newList = (list<Aircraft_Optimization__c>)trigger.new;
		/* Mark other primary Timeline to false*/
        set<Id> setACId = new set<Id>();        
        for(Aircraft_Optimization__c curRec:newList){ 
            
            if(trigger.IsInsert && curRec.Primary__c){
                if(setACId.contains(curRec.Aircraft__c))curRec.Primary__c.addError('You can mark only one Primary.');
           		setACId.add(curRec.Aircraft__c);
            }
            else if(trigger.IsUpdate && curRec.Primary__c && !((Aircraft_Optimization__c)trigger.oldMap.get(curRec.Id)).Primary__c){
                if(setACId.contains(curRec.Aircraft__c))curRec.Primary__c.addError('You can mark only one Primary.');
         		setACId.add(curRec.Aircraft__c);
            }             
        }

        system.debug('setACId='+setACId.size());
        if(!setACId.isEmpty()){
            list<Aircraft_Optimization__c> updateACOpti= new list<Aircraft_Optimization__c>();
            map<id,Aircraft__c> mapATAll = new map<id,Aircraft__c>([select id
											,Y_Hidden_Value_To_Hold__c,Y_Hidden_Value_To_Sell__c
                                            ,(select id,Primary__c,Value_to_Hold__c,Value_To_Sell__c
                                                from Aircraft_Optimization__r where (id not in :newList) and Primary__c= true)
                                                 
                                        from  Aircraft__c where Id in :setACId]);
            
            for(Aircraft__c curAT:mapATAll.values()){
                
                for(Aircraft_Optimization__c curACOpti:curAT.Aircraft_Optimization__r){
                    curACOpti.Primary__c =false;
                    updateACOpti.add(curACOpti);
                }
                
            }
            system.debug('updateACOpti='+updateACOpti.size());
            if(!updateACOpti.IsEmpty()){
                LWGlobalUtils.setTriggersflagOn(); //TriggerDisabledFlag=true;
                update updateACOpti;
                LWGlobalUtils.setTriggersflagOff();   //TriggerDisabledFlag=false;
            }
            
            list<Aircraft__c> updateAircraft = new list<Aircraft__c>();
            Aircraft__c tempAT;            
            for(Aircraft_Optimization__c curACOpti:newList){
                system.debug('curACOpti.Primary__c='+curACOpti.Primary__c + curACOpti.id);
                if(curACOpti.Primary__c){
                    tempAT = mapATAll.get(curACOpti.Aircraft__c);
                    tempAT.Y_Hidden_Value_To_Hold__c = curACOpti.Value_to_Hold__c;
                    tempAT.Y_Hidden_Value_To_Sell__c = curACOpti.Value_To_Sell__c;
                    updateAircraft.add(tempAT);                    
                }            
            }
            if(!updateAircraft.IsEmpty()){
                LWGlobalUtils.setTriggersflagOn(); //TriggerDisabledFlag=true;
                update updateAircraft;
                LWGlobalUtils.setTriggersflagOff();   //TriggerDisabledFlag=false;
            }            
        }
        
    }

    
 	//before insert, before update
	private void getCMLRFromWF()
	{
		
		Aircraft_Optimization__c[] newACOptimization = (list<Aircraft_Optimization__c>)trigger.new;

		set<string> setMSNs = new set<string>();
        set<string> setACId = new set<string>();
        for(Aircraft_Optimization__c curRec: newACOptimization){
            if(trigger.IsInsert && !setACId.contains(curRec.Aircraft__c)){
                curRec.Primary__c = true; setACId.add(curRec.Aircraft__c);
            }
            setMSNs.add(curRec.Y_Hidden_MSN__c);
        }
		
		map<string, Global_Fleet__c> mapWordFleet = new map<string, Global_Fleet__c>();
		list<Global_Fleet__c> listWF = [select id, CurrentMarketLeaseRate_m__c
										, AircraftType__c, SerialNumber__c
			 						from Global_Fleet__c 
			 						where SerialNumber__c in :setMSNs];
		system.debug('setMSNs='+setMSNs);
		
		list<Custom_Lookup__c> listCustomLookup = [select id, name,Lookup_Type__c
										, Sub_LookupType__c, Sub_LookupType2__c
            					from Custom_Lookup__c
            					where Lookup_Type__c = 'AT_MAP'
            					and active__c =true
        				];
        map<string,string>	mapATType = new map<string,string>();			
		for(Custom_Lookup__c  curRec:listCustomLookup){
			mapATType.put(curRec.name,curRec.Sub_LookupType__c);
		}	 
		
		string AircraftTypeNew;
		for(Global_Fleet__c curWF : listWF){
			AircraftTypeNew = curWF.AircraftType__c;
			// converting AT based on Custom Lookup : e.g 737(NG) to 737
			if(mapATType.containsKey(AircraftTypeNew)) AircraftTypeNew = mapATType.get(AircraftTypeNew);
			mapWordFleet.put(AircraftTypeNew + '-' + curWF.SerialNumber__c, curWF);
		}
		system.debug('mapWordFleet='+mapWordFleet.size());
		system.debug('mapWordFleet full='+mapWordFleet);
		Global_Fleet__c WFMatchTemp;
		for(Aircraft_Optimization__c curRec: newACOptimization){
			if(curRec.Ascend_FLCMV__c == null || curRec.Ascend_FLCMV__c == 0){
				WFMatchTemp= mapWordFleet.get(curRec.Y_Hidden_Aircraft_Type__c + '-' + curRec.Y_Hidden_MSN__c);
				if(WFMatchTemp != null)curRec.Ascend_FLCMV__c = WFMatchTemp.CurrentMarketLeaseRate_m__c;
			}
		}		
	}
 
    

}