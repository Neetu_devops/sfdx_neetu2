global class PostSandboxRefresh implements SandboxPostCopy{

  	global void runApexClass(SandboxContext context) {
  		
  		set<Id> setLWAdmins = new set<Id>(); 
  		list<GroupMember> listLWAdmins = [select Id, UserOrGroupId, Group.Name from GroupMember where Group.Name = 'LW Admin Group'];
  		for(GroupMember curGM : listLWAdmins){
  			setLWAdmins.add(curGM.UserOrGroupId);
  		}
  		List<User> LWAdminUsers = [SELECT Id, Name, Email FROM User WHERE id in :setLWAdmins and IsActive = true];
  		string strBody = 'User emails updated : <BR>';
  		for(User curUsr : LWAdminUsers){
  			strBody += ((strBody==''?'':', ') + curUsr.Name);
  			curUsr.Email = curUsr.Email.replace('@example.com','');
			curUsr.Email = curUsr.Email.replace('=','@');
			curUsr.Email = curUsr.Email.replace('.invalid','');
  		}
  		update LWAdminUsers;
		  strBody+='<BR>';
		  strBody+='<BR> ----------------------------------------------------------------------------------------' ;
		  strBody+='<BR> Admin, please execute the following steps:' ;
		  strBody+='<BR> 1. Please re-create the Named Credentials {CallSFAPI}. Make sure you follow all the steps again. <BR>' ;
				
		LWGlobalutils.sendEmailToLWAdmins('Sanbox refresh completed - ' +  context.organizationId()
			+ ' ' + context.sandboxId() + context.sandboxName(),  strBody);
	}
    
}