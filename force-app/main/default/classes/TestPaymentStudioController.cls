@isTest
Public class TestPaymentStudioController{
    @testSetup static void methodName() {
        Date dateField = System.today(); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
            
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Month_Ending__c = lastDayOfMonth
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        utilizationRecord.Status__c = 'Approved By Lessor';
        update utilizationRecord;
        
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Amount__c = 100,
            Status__c = 'Approved',
            payment_status__c ='Open'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        System.debug('Invoice: '+invoiceRecord);
        
        Invoice__c invoiceRecord1 = new Invoice__c(
            Name='Testing 1',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Amount__c = 100,
            Status__c = 'Approved',
            payment_status__c ='Open'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord1;
        LWGlobalUtils.setTriggersflagOff();
        
        Payment__c paymentRecord = new Payment__c(
            Name = 'Payment Testing',
            Invoice__c = invoiceRecord.Id,
            Amount__c = invoiceRecord.Amount__c
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert paymentRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Payment_Receipt__c paymentReciptRecord = new Payment_Receipt__c(
            Name='Testing',
            Approval_Status__c = 'Approved'
        );
        insert paymentReciptRecord; 
        
        Account testAcc = new Account(
        	Name= 'Testing'
        );
        insert testAcc;
        
        Payment_Receipt_Line_Item__c paymentLineITemREcord = new Payment_Receipt_Line_Item__c(
            Payment_Receipt__c = paymentReciptRecord.Id,
            Invoice__c = invoiceRecord1.Id,
            Amount_Paid__c = 10,
            Bank_Charges__c = 0
        );
        insert paymentLineITemREcord;
        
        LeasewareUtils.clearfromTrigger();
        paymentReciptRecord.Name = 'Test 1';
        paymentReciptRecord.Payment_Date__c = System.today().addDays(-10);
        LWGlobalUtils.setTriggersflagOn();
        update paymentReciptRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Letter_Of_Credit__c locRecord = new Letter_Of_Credit__c(Name ='loc Domo Record', LC_Number__c ='012345678', Lease__c =leaseRecord.Id, Lessee__c =testOperator.Id, LC_Amount_LC_Currency__c =20000);
        insert locRecord;
        
        LOC_Drawn_Down__c drawnDownRecord = new LOC_Drawn_Down__c(Name = 'Test Drawn Down', LETTER_OF_CREDIT__C =locRecord.Id);
        insert drawnDownRecord;

        Calendar_Period__c CP = new Calendar_Period__c(Name = 'Dummy3',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1),closed__c =true);
        
        list<Calendar_Period__c> cpList = new list<Calendar_Period__c>{cp};
     	insert cpList;
    }
     
    
    @isTest public static void cashPaymentTest(){
        Lease__c[] leaselist = [select id,Name from Lease__c];
        Invoice__c[] invoicelist = [select id,Name,Status__c,Type__c from Invoice__c];
        System.debug('InvoiceList: '+invoicelist);
        Utilization_Report__c ur =[Select Id from Utilization_Report__c];
        Invoice__c invoiceRecord1 = new Invoice__c(
            Name='Testing 1',
            Utilization_Report__c = ur.Id,
            Lease__c = leaselist[0].Id,
            Amount__c = 100,
            Status__c = 'Approved',
            payment_status__c ='Open'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord1;
        LWGlobalUtils.setTriggersflagOff();
        
        Payment_Receipt__c paymentReceipt = new Payment_Receipt__c();
        Account[] accountlist = [select id,Name from Account];
        Payment_Receipt_Line_Item__c[] paymentReciptlist = [select id,Name from Payment_Receipt_Line_Item__c];
        LOC_Drawn_Down__c[] drawnDownList = [select id,Name from LOC_Drawn_Down__c];
        
        List<PaymentStudioController.InvoicePaymentWrapper > paymentWrapperList = new List<PaymentStudioController.InvoicePaymentWrapper>();
        List<PaymentStudioController.InitailLoadPaymentWrapper > InitailLoadPaymentWrapperList = new List<PaymentStudioController.InitailLoadPaymentWrapper>();
        PaymentStudioController.InitailLoadPaymentWrapper loadPaymentWrapper = new PaymentStudioController.InitailLoadPaymentWrapper();
        loadPaymentWrapper.paymentReceiptRecord = paymentReceipt;
        loadPaymentWrapper.paymentName = '';
        loadPaymentWrapper.paymentDesc = '';
        loadPaymentWrapper.paymentReceivedDate = System.today();
        loadPaymentWrapper.paymentBankAccount = accountlist[0];
        loadPaymentWrapper.paymentAmountReceived = 0.0;
        loadPaymentWrapper.paymentAmountToAllocate = 0.0;
        loadPaymentWrapper.paymentReceiptLineItems = paymentReciptlist;
        loadPaymentWrapper.paymentReceiptInvoiceList = paymentWrapperList;
        
        PaymentStudioController.InvoicePaymentWrapper invoiceWrapper = new PaymentStudioController.InvoicePaymentWrapper();
        invoiceWrapper.isAllocated = false; 
        invoiceWrapper.invoiceRecord = invoiceRecord1; 
        invoiceWrapper.amountPaid = 0.0;
        invoiceWrapper.amountRemaining = 0.0;
        invoiceWrapper.bankCharges = 0.0;
        invoiceWrapper.isOverPayed = false;
        invoiceWrapper.overDueStatus = '';
        paymentWrapperList.add(invoiceWrapper);
        
        PaymentStudioController.loadInitialLoadData('');
        PaymentStudioController.fetchInvoiceData(invoicelist[0].Name, leaselist[0].Id, invoicelist[0].Type__c, '', 'Cash');
        PaymentStudioController.savePaymentReceipt(JSON.serialize(loadPaymentWrapper), JSON.serialize(paymentWrapperList), 'Letter of Credit', drawnDownList[0].Id);
    }
    
    @isTest public static void cashPrepaymentTest(){
        Operator__c[] lesseelist = [select Id, Name from Operator__c];
        Lease__c[] leaselist = [select Id, Name from Lease__c];
        Account[] accountlist = [select Id, Name from Account];
        Calendar_Period__c[] aplist = [select Id, Name from Calendar_Period__c];
        
        List<PaymentStudioController.InvoicePaymentWrapper > paymentWrapperList = new List<PaymentStudioController.InvoicePaymentWrapper>();
        PaymentStudioController.InitailLoadPaymentWrapper loadPaymentWrapper = PaymentStudioController.loadInitialLoadData('');
        loadPaymentWrapper.paymentDesc = 'Prepayment Test';
        loadPaymentWrapper.paymentReceivedDate = System.today();
        loadPaymentWrapper.paymentBankAccount = accountlist[0];
        loadPaymentWrapper.paymentAmountReceived = 0.0;
        loadPaymentWrapper.paymentAmountToAllocate = 0.0;
        loadPaymentWrapper.paymentPrePaymentCKB = true;
        loadPaymentWrapper.paymentLease = leaselist[0];
        loadPaymentWrapper.paymentLessee = lesseelist[0];
        loadPaymentWrapper.paymentAccountingPeriod = aplist[0];
        loadPaymentWrapper.paymentReceiptLineItems = new list<Payment_Receipt_Line_Item__c>();
        loadPaymentWrapper.paymentReceiptInvoiceList = paymentWrapperList;
        
        loadPaymentWrapper = PaymentStudioController.updateAccountingPeriodData(JSON.serialize(loadPaymentWrapper));
        PaymentStudioController.savePaymentReceipt(JSON.serialize(loadPaymentWrapper), JSON.serialize(paymentWrapperList), 'Cash', null);
        PaymentStudioController.fetchRelatedLegalEntities(lesseelist[0].Id);
    }
}