@isTest
private class TestAttachmentUpload {

    static testMethod void testAttachments()
    {
    	
        Aircraft__c ac=new Aircraft__c(Name='LeaseWorks',Date_of_Manufacture__c = System.today(), MSN_Number__c='123', TSN__c=0, CSN__c=0, Status__c='Available');
        insert ac;

		Blob b = Blob.valueOf('LOPA Image');
		Attachment att = new Attachment(ParentId = ac.ID, Name = 'LOPA.jpg', Body = b);

		insert(att);
    	Lessor__c theLessor = new Lessor__c(Name='Test', Lease_Logo__c='http://www.leaseworks.com/logo.jpg', Phone_Number__c='234-456-2345', Email_Address__c='support@lw.com');
    	insert theLessor;

		
	    LeaseWareUtils.clearFromTrigger();		
		insert(att.clone());

	    LeaseWareUtils.clearFromTrigger();
		Attachment att2 = att.clone();
		insert(att2);

		
	    LeaseWareUtils.clearFromTrigger();
	    update att;

		
	    LeaseWareUtils.clearFromTrigger();		
		delete att;
		
		
	    LeaseWareUtils.clearFromTrigger();		
		delete att2;		

    }

}