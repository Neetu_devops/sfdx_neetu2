public class NewsFeedRestIntegrationHelper {

	//public List<Data> data;

	public class Data {
		public Integer id;
		public String title;
		public String url;
		public List<String> tags;
		public Integer dateOfNews;
		public String article;
	}
	public static List<Data> parse(String json) {
		Map<String, Object> m =(Map<String, Object>) System.JSON.deserializeUntyped(json);

		list<Object> dataList =(list<Object>)m.get('data');
		System.debug('dataList Val ' + dataList);
		List<Data> data = new List<Data>();
		if (dataList == null) {return data ;}
		for(object curData : dataList) {
			Map<String, Object> newsArt =(Map<String, Object>)curData;
			Data curNewsData = new Data();
			curNewsData.id = (integer) newsArt.get('id');
			curNewsData.article = (string)newsArt.get('article');
			curNewsData.dateOfNews = (integer)newsArt.get('date');
			curNewsData.tags = new List<String>();
			if((list<object>)newsArt.get('tags') != null) {
				for(object curObj: (list<object>)newsArt.get('tags')) {
					curNewsData.tags.add((string)curObj);
				}
			}
			curNewsData.title = (string)newsArt.get('title');
			curNewsData.url = (string)newsArt.get('url');
		//	System.debug('curNewsData is ' + curNewsData);
			data.add(curNewsData);
		}
		//system.debug('Data size ' + data.size() + '\n' + data);
		return data;
	}
	// public static NewsFeedRestIntegrationHelper parse(String json) {
	// 	return (NewsFeedRestIntegrationHelper) System.JSON.deserialize(json, NewsFeedRestIntegrationHelper.class);
	// }
}