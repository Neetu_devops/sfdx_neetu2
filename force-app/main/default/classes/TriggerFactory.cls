/**
 * Class TriggerFactory
 *
 * Used to instantiate and execute Trigger Handlers associated with sObjects.
 */
public class TriggerFactory
{
    /**
     * Public static method to create and execute a trigger handler
     *
     * Arguments:   Schema.sObjectType soType - Object type to process (SObject.sObjectType)
     *
     * Throws a TriggerException if no handler has been coded.
     */
    public static void createHandler(Schema.sObjectType soType)
    {
        // added this here so that even if we do not add at trigger level it will take care at this place.
        if(LeaseWareUtils.isTriggerDisabled(soType)) {return; }
        // Get a handler appropriate to the object being processed
        ITrigger handler = getHandler(soType);
         
        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
        if (handler == null)
        {
            //throw new LeaseWareException('No Trigger Handler registered for Object Type: ' + soType);
        }
         
        // Execute the handler to fulfil the trigger
        execute(handler,soType); 
    }
     
    /**
     * private static method to control the execution of the handler
     *
     * Arguments:   ITrigger handler - A Trigger Handler to execute
     */
    private static void execute(ITrigger handler,Schema.sObjectType soType)
    {
        // Before Trigger
        if (Trigger.isBefore)
        {
            // Call the bulk before to handle any caching of data and enable bulkification
            leasewareUtils.resetVariableIfSecondAttempt(''+soType);
            handler.bulkBefore();
             
            // Iterate through the records to be deleted passing them to the handler.
            if (Trigger.isDelete)
            {
                    handler.beforeDelete();
            }
            // Iterate through the records to be inserted passing them to the handler.
            else if (Trigger.isInsert)
            {
                    handler.beforeInsert();
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate)
            {

                    handler.beforeUpdate();
            }
        }
        else
        {
            // Call the bulk after to handle any caching of data and enable bulkification
            handler.bulkAfter();
             
            // Iterate through the records deleted passing them to the handler.
            if (Trigger.isDelete)
            {
                    handler.afterDelete();
            }
            // Iterate through the records inserted passing them to the handler.
            else if (Trigger.isInsert)
            {
                    handler.afterInsert();
            }
            // Iterate through the records updated passing them to the handler.
            else if (Trigger.isUpdate)
            {
                    handler.afterUpdate();
            }
            else if (Trigger.isUnDelete )
            {
                    handler.afterUnDelete();
            }            
        }
         
        // Perform any post processing
        handler.andFinally();
    }
     
    /**
     * private static method to get the appropriate handler for the object type.
     * Modify this method to add any additional handlers.
     *
     * Arguments:   Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
     *
     * Returns:     ITrigger - A trigger handler if one exists or null.
     */
    private static ITrigger getHandler(Schema.sObjectType soType)
    {
        if (soType == Maintenance_Program__c.sObjectType) return new MaintenanceProgramHandler();
        if (soType == Maintenance_Program_Event__c.sObjectType) return new MaintenanceProgramEventHandler();
        if (soType == Depreciation_schedule__c.sObjectType) return new DepreciationHandler();
        if (soType == Depreciation_Rate__c.sObjectType) return new DepreciationRateHandler();
  
        if (soType == Aircraft_Need__c.sObjectType) return new AircraftNeedHandler();
  
        if (soType == PDP_Payment_Schedule__c.sObjectType) return new PDPScheduleTriggerHandler();
        if (soType == Operator__c.sObjectType) return new OperatorHandler(); 
        if (soType == Counterparty__c.sObjectType) return new CounterpartyTrgHandler(); 
        if (soType == Aircraft__c.sObjectType) return new AircraftTriggerHandler();     
        if (soType == Invoice__c.sObjectType) return new InvoiceHandler();      
        if (soType == Invoice_Line_Item__c.sObjectType) return new InvoiceLineItemHandler();        
        if (soType == Financing__c.sObjectType) return new FinancingHandler();
        if (soType == Amortization_Schedule__c.sObjectType)  return new AmortizationScheduleHandler(); 
        if (soType == Aircraft_Depreciation_Schedule__c.sObjectType) return new AircraftDepreciationScheduleHandler();  
        if (soType == Lease__c.sObjectType) return new LeaseHandler();  
        if (soType == Lease_Depreciation_Schedule__c.sObjectType) return new LeaseDepreciationScheduleHandler();    
        if (soType == Capitalized_Cost__c.sObjectType) return new CapitalizedCostHandler(); 
        if (soType == Equipment_Depreciation_Schedule__c.sObjectType) return new EquipmentDepreciationScheduleHandler(); 
        if (soType == Deal_Proposal__c.sObjectType) return new DealProposalTriggerHandler();  
        if (soType == MSN_s_Of_Interest__c.sObjectType) return new ProspectTermTriggerHandler();
        if (soType == Asset_History__c.sObjectType) return new AssetHistoryTriggerHandler();
        /* commenting after discussion with Bobby 08 June 2016          
        if (soType == Calendar_Period__c.sObjectType) return new CalendarPeriodTriggerHandler();
        */          
        if (soType == Delivery_Timeline__c.sObjectType) return new DeliveryTimelineTriggerHandler();
        if (soType == Customer_Interaction_Log__c.sObjectType) return new CustomerInteractionLogHandler(); 
        if (soType == Aircraft_Type__c.sObjectType) return new AircraftTypeHandler();              
        if (soType == Lessor__c.sObjectType) return new LessorHandler();  
        if (soType == Fleet_Acquisition_Analysis__c.sObjectType) return new FleetAcquisitionAnalysisHandler(); 
        if (soType == Aircraft_In_Transaction__c.sObjectType) return new AITHandler(); 
        //anjani : not using now : if (soType == Aircraft_Eligible_Event__c.sObjectType) return new ACEETriggerHandler(); 
        if (soType == Assembly_Eligible_Event__c.sObjectType) return new CAEETriggerHandler(); 

        if (soType == Utilization_Report__c.sObjectType) return new URTriggerHandler(); 
        if (soType == Assembly_Utilization__c.sObjectType) return new AssemblyURTriggerHandler(); 
        if (soType == Utilization_Report_List_Item__c.sObjectType) return new URLITriggerHandler();
        if (soType == Assembly_MR_Rate__c.sObjectType) return new AssemblyMRRateTriggerHandler();
        if (soType == Payment__c.sObjectType) return new PaymentTriggerHandler(); 
        if (soType == Payment_Line_Item__c.sObjectType) return new PaymentLineItemHandler(); 
        //anjani : not using now : if (soType == Aircraft_Eligible_Event_Invoice__c.sObjectType) return new ACEEInvTriggerHandler();
        
        if (soType == Sub_Components_LLPs__c.sObjectType) return new LLPTriggerHandler();
        if (soType == Thrust_Setting__c.sObjectType) return new ThrustSettingTriggerHandler();
        if (soType == Constituent_Assembly__c.sObjectType) return new AssemblyTriggerHandler();
        if (soType == Assembly_Event_Info__c.sObjectType) return new AssemblyEventInfoTriggerHandler();
        if (soType == Rent__c.sObjectType) return new RentTriggerHandler();
        if (soType == Transaction__c.sObjectType) return new TransactionTriggerHandler();
        if (soType == Matching_Aircraft__c.sObjectType) return new MatchingAircraftTriggerHandler();

        if (soType == Marketing_Activity__c.sObjectType) return new MarketingActivityTriggerHandler();
        
        if (soType == Aircraft_Proposal__c.sObjectType) return new AssetTermTriggerHandler();
        if (soType == Incoming_Data__c.sObjectType) return new IncomingDataTriggerHandler();
        if (soType == Attendee_Contacts__c.sObjectType) return new AttendeesContactsTriggerHandler();
        if (soType == PDP_Interest__c.sObjectType) return new PDPInterestTriggerHandler();
        if (soType == Engine_Template__c.sObjectType) return new EngineTemplateTriggerHandler();

        if (soType == LLP_Template__c.sObjectType) return new LLPTemplateTriggerHandler();
        if (soType == Maintenance_Program_Event_Catalog__c.sObjectType) return new MPDCatalogTriggerHandler();
        if (soType == LLP_Catalog__c.sObjectType) return new LLPCatalogTriggerHandler();
        if (soType == Transaction_Terms__c.sObjectType) return new TrxTermsTriggerHandler();

        if (soType == Pricing_Output_New__c.sObjectType) return new PricingAnalysisTriggerHandler(); 
        if (soType == Aircraft_In_Deal__c.sObjectType) return new AICTriggerHandler();       
        if (soType == Transaction_Proposal__c.sObjectType) return new TrxProspectTriggerHandler();
        if (soType == Pricing_Run_New__c.sObjectType) return new PricingRunInputTriggerHandler();
        if (soType == Address__c.sObjectType) return new AddressBookTriggerHandler();
        if (soType == Issuance__c.sObjectType) return new IssuanceTriggerHandler();
        if (soType == Bank__c.sObjectType) return new CounterPartyNewTriggerHandler();
        if (soType == List_of_Investors__c.sObjectType) return new IssuanceWithInvestorTriggerHandler(); 
       // if (soType == Stepped_Rent__c.sObjectType) return new Stepped_RentHandler(); //Commented because - Stepped_RentHndler moved to one more down level - SteppedMultiRent Obj level 
		if (soType == SteppedMultiRent__c.sObjectType) return new Stepped_RentHandler();
		if (soType == Stepped_Rent__c.sObjectType) return new MultiRentHandler();//Added PriyankaC:For MidTErmLeaseUpdate Controller
        if(soType == Batch__c.sObjectType)	return new PDPBatchTriggerHelper();
		if(soType == Cost_Revision__c.sObjectType) return new PDPCostRevisionHandler();
		if(soType == Pre_Delivery_Payment__c.sObjectType) return new PDPHandler();
        if (soType == Applicable_Calendar__c.sObjectType) return new ApplicableCalendarTriggerHandler();
        if (soType == Deal_team__c.sObjectType) return new DealTeamTriggerHandler();    
        if (soType == Deal_And_Group_Assignment__c.sObjectType) return new DealGroupAssignmentHandler();    
        if (soType == Department_Default__c.sObjectType) return new DepartmentDefaultTriggerHandler();
        if (soType == Insurance_Stipulation__c.sObjectType) return new InsuranceStipulationTriggerHandler();
        if (soType == Insurance_Cert__c.sObjectType) return new InsuranceCertificateTriggerHandler();
        if (soType == Certificate_Detail__c.sObjectType) return new CertificateDetailTriggerHandler();
        if (soType == Aircraft_Optimization__c.sObjectType) return new AircraftOptimizationTriggerHandler ();
        if (soType == Project__c.sObjectType) return new ProjectTriggerHandler();

        if (soType == Scenario_Input__c.sObjectType) return new CashFlowTriggerHandler();
        if (soType == MR_Fund_Transfer__c.sObjectType) return new MRFundTransferTriggerHandler();
        if(soType == Assembly_MR_Fund_Transfer__c.sObjectType) return new AssemblyMRFundTransferTriggerHandler();
        if (soType == MR_Rate__c.sObjectType) return new MRRateTriggerHandler();
        if (soType == LLP_Catalog_Summary__c.sObjectType) return new LLPCatSumTriggerHandler();
        if (soType == Payment_Receipt__c.sObjectType) return new PaymentStudioTriggerHandler();
        if (soType == Letter_Of_Credit__c.sObjectType) return new LetterOfCreditTriggerHandler();
        if (soType == Delinquency_Staging__c.sObjectType) return new DelinquencyStagingTriggerHandler();
        if(soType == Purchase_Agreement_Milestone__c.sObjectType)return new PurchaseAgreementMilestoneTriggerHandler();
        if(soType == Order__c.sObjectType)return new PurchaseAgreementTriggerHandler();
        if(soType == Credit_Score__c.sObjectType)return new CreditScoreTriggerHandler();
        if(soType == Reconciliation_Schedule__c.sObjectType) return new ReconciliationScheduleTriggerHandler();
        if(soType == Adjusted_Interval_Ratio__c.sObjectType) return new AdjustedIntRatioTriggerHandler();
        if(soType == Assembly_Utilization_Snapshot_New__c.sObjectType) return new AssemblyUtilSnapshotTriggerHandler();
        if(soType == Utilization_Snapshot__c.sObjectType) return new UtilizationSnapshotTriggerHandler();
        if(soType == LLP_Utilization_Snapshot__c.sObjectType) return new LLPUtilSnapshotTriggerHandler();
        if(soType == Spec_Mass_and_Balance__c.sObjectType) return new SpecMassTriggerHandler();
        if(soType == Credit_Memo_Issuance__c.sObjectType) return new CreditMemoIssuanceTriggerHandler();
        if(soType == Fund_Code__c.sObjectType) return new FundCodeTriggerHandler();
        if(soType == Supplemental_Rent_Adjustment__c.sObjectType) return new SuppRentAdjustmentTriggerHandler();

        //Accounting related objects
        if(soType == Dimension__c.sObjectType) return new DimensionHandler();
        if(soType == Dimension_Application__c.sObjectType) return new DimensionApplicationTriggerHandler();
        if(soType == Journal_Entry__c.sObjectType) return new JournalEntryTriggerHandler();
        if(soType == Parent_Accounting_setup__c.sObjectType) return new AccountingSetupTriggerHandler();
        if(soType == Accounting_setup__c.sObjectType) return new SubAccSetupTriggerHandler();
	    if(soType == Maintenance_Event_Task__c.sObjectType) return new MaintenanceEventTaskTriggerHandler();
        if(soType == Supp_Rent_Event_Reqmnt__c.sObjectType) return new SuppmntlRentEventReqTriggerHandler();
        if(soType == Historical_Event_Task__c.sObjectType) return new HistoricalEventTaskTriggerHandler();
        if(soType == Claims__c.sObjectType) return new ClaimTriggerHandler();
        if(soType == Interest_Rate_Index_Daily__c.sObjectType) return new InterestRateIndexDailyTriggerHandler ();
         //Non-Lease Agreement object
        if(soType == Non_Lease_Agreements__c.sObjectType) return new NonLeaseAgrmntTriggerHandler();
        if(soType == payable__c.sObjectType) return new PayableTriggerHandler();
        if(soType == payout__c.sObjectType) return new payoutTriggerHandler();
        if(soType == payable_line_item__c.sObjectType) return new AccountsPayableTriggerHandler();
        if(soType == payout_line_item__c.sObjectType) return new PayoutLineItemTriggerHandler();
        if(soType == cash_security_deposit__c.sObjectType) return new CashSDTriggerHandler();        
        if(soType == credit_memo__c.sObjectType) return new CreditMemoTriggerHandler();
        if(soType == Invoice_Adjustment__c.sobjectType) return new InvoiceAdjustmentTriggerHandler();
        if(soType == Invoice_Adjustment_Line_Item__c.sobjectType) return new InvoiceAdjLineItemTriggerHandler();

        if(soType == IL_Operator__c.sObjectType) return new ILOperatorTriggerHandler();
        if(soType == IL_Contact__c.sObjectType) return new ILContactTriggerHandler();
        if(soType == PBH_Rent_Per_Assembly__c.sObjectType) return new PBHRentAssemblyTriggerHandler();


        return null;
    }
}