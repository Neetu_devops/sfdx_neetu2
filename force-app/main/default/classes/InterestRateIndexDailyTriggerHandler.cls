public class InterestRateIndexDailyTriggerHandler implements ITrigger{
    //Hint: Interest Rate Schedule Name field shoud be update based on Index + “,” + space + Interest Rate Date
    // Constructor
    private final string triggerBefore = 'InterestRateIndexDailyTriggerHandlerBefore';
    private final string triggerAfter =  'InterestRateIndexDailyTriggerHandlerAfter';
       
    
    public InterestRateIndexDailyTriggerHandler ()
    {   
    }
    //Making functionality more generic.
   
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {

    }
    public void bulkAfter()
    {

    }   
    public void beforeInsert()
    {   
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InterestRateIndexDailyTriggerHandler.beforeInsert(+)');

        UpdInterestRateIndexDailyName();
        
        system.debug('InterestRateIndexDailyTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InterestRateIndexDailyTriggerHandler.beforeUpdate(+)');
        
        UpdInterestRateIndexDailyName();
        
        system.debug('InterestRateIndexDailyTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        LeaseWareUtils.setFromTrigger(triggerBefore);
    
        system.debug('InterestRateIndexDailyTriggerHandler.beforeDelete(+)');

        system.debug('InterestRateIndexDailyTriggerHandler.beforeDelete(-)');
    }
     
    public void afterInsert()
    {

    }
     
    public void afterUpdate()
    {

    }
     
    public void afterDelete()
    {
           
    }

    public void afterUnDelete()
    {
            
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
    }
    
    //Before Insert, Before Update
    //When User create or Update Interest Rate Index Daily, Interest Rate Schedule Name field shoud be update based on Index + “,” + space + Interest Rate Date
    private void UpdInterestRateIndexDailyName(){
        list<Interest_Rate_Index_Daily__c> listIRID =(list<Interest_Rate_Index_Daily__c>)trigger.new;
        List<id> listIRI=new List<id>();
        set<date> setOldRentResetDt= new set<date>();
        for(Interest_Rate_Index_Daily__c curIRID: listIRID){
            listIRI.add(curIRID.Interest_Rate_Index__c);
            if(trigger.isUpdate){
                Interest_Rate_Index_Daily__c oldRec = (Interest_Rate_Index_Daily__c)trigger.oldMap.get(curIRID.id);
                setOldRentResetDt.add(oldRec.Interest_Rate_Date__c);
            }
        }
        map<id,string> mapIRIName= new map<id,string>();
        for(Interest_Rate_Index__c curIRI: [select id,name from Interest_Rate_Index__c where id in: listIRI]){
            mapIRIName.put(curIRI.id,curIRI.name);
        }
        
        set<string> setIRI_IRDateExistInv=new set<string>();
        for(Rent__c curRent: [select id,name,Rent_Reset_Date__c,Stepped_Rent__r.Interest_Rate_Index__c,Actual_Interest_Rate__c 
                from Rent__c
                where Link_to_Invoice__c!=Null and Rent_Reset_Date__c!=Null and Stepped_Rent__r.Interest_Rate_Index__c!=Null
                and Stepped_Rent__r.Interest_Rate_Index__c in : listIRI and Rent_Reset_Date__c in :setOldRentResetDt]){
            setIRI_IRDateExistInv.add(curRent.Stepped_Rent__r.Interest_Rate_Index__c+'-'+curRent.Rent_Reset_Date__c);
        }

        for(Interest_Rate_Index_Daily__c curIRID: listIRID){
            if(trigger.isInsert){
                curIRID.Name= mapIRIName.get(curIRID.Interest_Rate_Index__c)+', '+ curIRID.Interest_Rate_Date__c.day()+'-'+ LeaseWareUtils.mapMonthsName.get(curIRID.Interest_Rate_Date__c.month()) +'-'+ curIRID.Interest_Rate_Date__c.Year();
            }
            else if(trigger.isUpdate){
                system.debug('Update case');
                Interest_Rate_Index_Daily__c oldRec = (Interest_Rate_Index_Daily__c)trigger.oldMap.get(curIRID.id);
                if(curIRID.Interest_Rate_Date__c!= oldRec.Interest_Rate_Date__c){ //When Interest Rate Date value get change
                    if(setIRI_IRDateExistInv.contains(oldRec.Interest_Rate_Index__c+'-'+oldRec.Interest_Rate_Date__c)){ //if record is alrady exist for same Index and Interest Rate Date
                        curIRID.addError('Cannot change the Date because a Rent Invoice has already been created. Please cancel the invoice first and then change the date.');
                        continue;
                    }
                    curIRID.Name= mapIRIName.get(curIRID.Interest_Rate_Index__c)+', '+ curIRID.Interest_Rate_Date__c.day()+'-'+ LeaseWareUtils.mapMonthsName.get(curIRID.Interest_Rate_Date__c.month()) +'-'+ curIRID.Interest_Rate_Date__c.Year(); 
                }
                else if(curIRID.Interest_Rate_Prcntg__c!= oldRec.Interest_Rate_Prcntg__c){ //When Actual Interest Rate value get change
                    if(setIRI_IRDateExistInv.contains(oldRec.Interest_Rate_Index__c+'-'+oldRec.Interest_Rate_Date__c)){ // if record is alrady exist for same Index and Interest Rate Date
                        curIRID.addError('Cannot change Actual Interest % Rate because a Rent Invoice has already been created. Please cancel the invoice first and then change the interest rate.');
                        continue;
                    }
                    if(!curIRID.Override__c) curIRID.Override__c=True;
                }
            }
        }
    }    
}