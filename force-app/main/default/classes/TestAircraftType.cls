@isTest
private class TestAircraftType {

    static testMethod void testAircraftType1() {
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);      	
        Aircraft_Type__c newAT = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320');
        LeaseWareUtils.clearFromTrigger();insert newAT;

        Aircraft_Type__c newAT2 = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320', Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56');
        LeaseWareUtils.clearFromTrigger();insert newAT2;


		Aircraft_Type_Map__c newATM = new Aircraft_Type_Map__c(Name='A330', Aircraft_Type__c=newAT.id);
		try{
			LeaseWareUtils.clearFromTrigger();insert newATM;
		}catch(exception e){
			system.debug('Expected.' + e.getMessage());
			newATM.Type__c = 'A330';
			newATM.Engine_Type__c='CF6';
			LeaseWareUtils.clearFromTrigger();insert newATM;
		}
		
		newATM.Type__c = 'A320';
		newATM.Engine_Type__c='CFM56';
		LeaseWareUtils.clearFromTrigger();update newATM;
        
		newAT2.Fleet_Size_To_Consider__c=0;
		LeaseWareUtils.clearFromTrigger();update newAT2;
		

		LeaseWareUtils.clearFromTrigger();delete newATM;

    }

	@isTest(seeAllData=true)
    static void testAircraftType2() {
     	try{
    		
    		set<id> setAircraftMapOpIds = new set<id>();
    		setAircraftMapOpIds.add('a24G0000000Skbq');
    		setAircraftMapOpIds.add('a24G0000000Skc1');
    		setAircraftMapOpIds.add('a24G0000000SkcK');
    		leasewareUtils.ftrBulkUpdateAcTypeMapOps(setAircraftMapOpIds);

    		update [select id from Aircraft_Type_Map__c where id = 'a23G0000000QJFb'];
    		update [select id from Aircraft_Type__c limit 1];
    		
     	}catch(exception e){
     		system.debug('Ignore');
     	}
     }
    
}