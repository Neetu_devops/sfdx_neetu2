/*
    This batch is run in succession to ForecastBatch. After successfully creating the monthly scenarios,
    it performs some post processing actions such as updating certain fields on those records.
    Updating the scenario input record with NPV, Total Cost per FH etc. 
    
    Start - Updates the Month_Since_Next_Overhaul,
    cost per FH for the monthly scenarios. The total cash flow for a given month is summed up
    and stored in MR_Amount field of the "General" monthly scenario record.

    Finish - Update Scenario input record with NPV, XIRR etc. 

*/
public class ForecastBatch2 implements Database.batchable <Integer>, Database.Stateful { 
    
    string forecastId;
    private Process_Request_Log__c ReqLog;
    
    public ForecastBatch2(string forecastId, Process_Request_Log__c ReqLog)
    {
        this.forecastId = forecastId;
        this.ReqLog = ReqLog;             
    }
        
    public Iterable<Integer> start(Database.BatchableContext bc) 
    {   
        List<Integer> lstNumOfBatches = new List<Integer>();
        lstNumOfBatches.add(1);
        return lstNumOfBatches;
    }
    
   public void execute(Database.batchableContext info, List<Integer> scope)
   {    
        try
        {   
            System.debug('BATCH 2 - Execute');

            //Update month since next event, also Cost Per FH, Total Cash flow per month
            ScenarioHelper.UpdateMSNOForMonthlyScenarios(forecastId);
            
        }
        catch(Exception ex)
        {   
            updateRequestLog(ReqLog,'FAIL', LeaseWareUtils.cutString( ex.getMessage() , 0, 254 )
                    , 'Batch 2 - Failed to update the month since next overhaul for the monthly scenarios :' + ex.getMessage() + ex.getStackTraceString());	                                                
        }
   }

   public void finish(Database.batchableContext info)
   {     
        try
        {
            //Update the scenario input with NPV, IRR etc
            ScenarioHelper.UpdateScenarioInput(forecastId);

            //TODO: Invoke flow trigger to calculate XIRR
            //XIRR calculation cannot be done in apex. It is currently being done using a JS library.
        }
        catch(Exception ex)
        {   
            updateRequestLog(ReqLog,'FAIL', LeaseWareUtils.cutString( ex.getMessage() , 0, 254 )
                    , 'Batch 2 - Failed to update NPV, IRR on scenario input :' + ex.getMessage() + ex.getStackTraceString());	                                                
        }
       
        if('PROCESSING'.equals(ReqLog.status__c))
        {
            SendEmail(forecastId, true, ReqLog, new List<document>());
        }
        else //FAIL
        {   
            //Set the status of the forecast to "Complete"(Not "Draft" because component outputs are already created in first batch)
            Scenario_Input__c scenarioInpRec = [Select Id, Forecast_Status__c from Scenario_Input__c where id =: forecastId];
            scenarioInpRec.Forecast_Status__c = 'Completed';
            update scenarioInpRec;

            string outputStr = ' Status(Error/Warning) , Message ' + '\n';                
            outputStr += 'Error' + ',"' + ReqLog.Exception_Message__c +  '\n';	
            list<Document> documents = CreateAttachment(outputstr, ReqLog);				                
            SendEmail(forecastId, false, ReqLog, documents);
        }    
        
        if(Test.isRunningTest()) //For code coverage
        {   
            updateRequestLog(ReqLog, 'FAIL', 'TEST', 'TEST');
            list<Document> documents = CreateAttachment('TEST', ReqLog);				                
            SendEmail(forecastId, false, ReqLog, documents);
        }
       
   }

   public static List<Document> CreateAttachment(string outputstr, Process_Request_Log__c ReqLog)
    {   
        LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.

        List<document> documents = new List<document>();
        Document AttachdocOutput = new Document();
        AttachdocOutput.AuthorId = UserInfo.getUserId();
        AttachdocOutput.FolderId = UserInfo.getUserId();
        AttachdocOutput.Body = Blob.valueOf(outputStr);
        AttachdocOutput.name = 'ResultsLog.csv' ; 
        documents.add(AttachdocOutput);	
        
        ContentVersion contentVer = new ContentVersion();
        contentVer.ContentLocation = 'S'; 
        contentVer.PathOnClient = AttachdocOutput.name;
        contentVer.Title = AttachdocOutput.name;
        contentVer.VersionData = AttachdocOutput.Body;
		contentVer.Description = 'Leaseworks Internal';
		

        insert contentVer;

        // First get the content document Id from ContentVersion
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:contentVer.Id].ContentDocumentId;

        //Create ContentDocumentLink
        ContentDocumentLink docLink = new ContentDocumentLink();                
        docLink.ContentDocumentId = conDoc;                
        docLink.LinkedEntityId = ReqLog.Id;                
        docLink.ShareType = 'V';
        insert docLink; 
        LeaseWareUtils.TriggerDisabledFlag = false;  
        return documents;
    }

    public static void SendEmail(string forecastId, boolean success, Process_Request_Log__c ReqLog, list<Document> documents)
    {
        EmailTemplate[] emailTemplatelist = [ SELECT Id,Body,Subject,TemplateType,HTMLValue,DeveloperName FROM EmailTemplate 
        WHERE DeveloperName IN('Forecast_Fail_Notification', 'Forecast_Generated_Notification') order by DeveloperName];

        String emailSubject = 'Forecast';
        String emailBody = 'Forecast';
        
        if(success)
        {
            ReqLog.status__c = 'SUCCESS';
            update ReqLog;

            if(emailTemplatelist.size()>0){ //Forecast_Generated_Notification
                emailSubject = emailTemplatelist[1].Subject;
                String recordlink = URL.getSalesforceBaseUrl().toExternalForm() +'/'+ forecastId;
                emailBody  = emailTemplatelist[1].Body.replace('{FORECAST_OUTPUT_LINK}', recordlink);
            }
        }
        else
        {
            if(emailTemplatelist.size()>0){ //Forecast_Fail_Notification
                emailSubject = emailTemplatelist[0].Subject;
                emailBody    = emailTemplatelist[0].Body.replace('{ERROR_MESSAGE_TOKEN}', ReqLog.Comments__c==null?'Unexpected Error': ReqLog.Comments__c);
            }
        }

         // sending email
         List<Scenario_Input__c> forecastNames = [select Name from Scenario_Input__c where id = :forecastId];
         if(forecastNames.size() > 0)        
             emailSubject = emailSubject.replace('{FORECAST_ID_TOKEN}',forecastNames[0].Name);
                     
         list<string> toAddresses = new list<string>();
         toAddresses.add(UserInfo.getUserEmail());
 
         LeaseWareUtils.sendEmailTo(emailSubject, emailBody, toAddresses , null ,'TEXT',documents);         

    }

    public static void updateRequestLog(Process_Request_Log__c ReqLog,String status,String comments,String excepStr)
    {   
        ReqLog.status__c = status;
        ReqLog.Comments__c = comments;
        ReqLog.Exception_Message__c = excepStr;      
        update ReqLog;
    }

}