public without sharing class EngineDetailsController {
	
    @AuraEnabled
    public static ContainerWrapper getData(String assetId){
        
        //decode parameter.
        if(!Test.isRunningTest()){
            assetId = UtilityWithoutSharingController.decodeData(assetId);
        }
        
        ContainerWrapper container = new ContainerWrapper();
        
        List<Aircraft__c> assetList =  [select id, Asset_Status_External__c, Current_Location__c, Engine_Technology__c , MSN_Number__c, 
                                                    Aircraft_Series__c, Lease__r.Lease_Start_Date_New__c,
                                                    Lease__r.Lease_End_Date_New__c,
                                                    Lease__r.Lessee__r.Name
                                                    from Aircraft__c where id= : assetId]; 

        if(assetList.size()>0){
            
            String currentThrust;
            
            List<Constituent_Assembly__c> assemblyList = [select id,Current_TS__r.Name,
                                                                     Serial_Number__c, Asset__r.MSN_Number__c,
                                                                     TSN__c, CSN__c, TSO_External__c,
                                                                     CSO_External__c, Lowest_LLP_Limiter_Current_TS__c,
                                                                     Lowest_Limiter_LLP__r.Name, Limiting_Module__c, 
                                                                      (select id,Next_Scheduled_Event_Due_Date_F__c	
                                                                       from Eligible_Events__r)
                                                                     from Constituent_Assembly__c
                                                                     where Attached_Aircraft__c = :assetId
                                                                     and Replaced_External__c = false];
            
            
            String linkedAsset = '';
            
            if(assemblyList.size()>0){
                
				currentThrust = assemblyList[0].Current_TS__r.Name;
				container.esn = assemblyList[0].Serial_Number__c; 
                container.assemblyId = assemblyList[0].Id;
                
                Date lastShopVisitDate;
                
                if(assemblyList[0].Eligible_Events__r.size()>0){
                    lastShopVisitDate = assemblyList[0].Eligible_Events__r[0].Next_Scheduled_Event_Due_Date_F__c;
                }
                
        		container.llpSummary =  new LLPManagementSummaryWrapper(assemblyList[0].TSN__c,
                                                                        assemblyList[0].CSN__c,
                                                                        assemblyList[0].TSO_External__c,
                                                                        assemblyList[0].CSO_External__c,
                                                                        assemblyList[0].Lowest_LLP_Limiter_Current_TS__c,
                                                                        assemblyList[0].Lowest_Limiter_LLP__r.Name,
                                                                        assemblyList[0].Limiting_Module__c ,
                                                                        lastShopVisitDate );
            
            	linkedAsset  = assetList[0].MSN_Number__c != assemblyList[0].Asset__r.MSN_Number__c ?
                    		   assemblyList[0].Asset__r.MSN_Number__c : 'N/A'; 
            }
            String lessee;
            Date leaseStartDate;
            Date leaseEndDate;

            
            String mrFundQuery = 'Select Id, event_Code_Description__c, Maintenence_Reserve_Balance__c from Lease_Fund_Events_External__c where ';
            String msn = assetList[0].MSN_Number__c;
            if(linkedAsset != '' && linkedAsset != 'N/A'){
                Aircraft__c ast = [Select Id, Lease__c, Lease__r.Name, Lease__r.Lease_Start_Date_New__c ,Lease__r.Lease_End_Date_New__c, Lease__r.Lessee__r.Name from Aircraft__c where MSN_Number__c=: linkedAsset];
                if(ast.Lease__c != null){
                    lessee = ast.Lease__r.Lessee__r.Name;
                    leaseStartDate = ast.Lease__r.Lease_Start_Date_New__c;
                    leaseEndDate = ast.Lease__r.Lease_End_Date_New__c;
                }
                if(linkedAsset != container.esn){
                    mrFundQuery += 'eventSerialNumber__c =: msn';
                }
                else{
                    mrFundQuery += 'eventMsnEsn__c =: msn';
                }
            }
            else{
                mrFundQuery += 'eventMsnEsn__c =: msn';
            }
            container.engineSummary = new EngineSummaryWrapper(assetList[0].Aircraft_Series__c, currentThrust,
                                                               assetList[0].Asset_Status_External__c,
                                                              (lessee !='' && lessee !=null)? lessee : assetList[0].Lease__r.Lessee__r.Name,
                                                               //assetList[0].Linked_MSN__c,
                                                               linkedAsset,
                                                               leaseStartDate != null ? leaseStartDate : assetList[0].Lease__r.Lease_Start_Date_New__c,
                                                               leaseEndDate != null ? leaseEndDate : assetList[0].Lease__r.Lease_End_Date_New__c,
                                                               assetList[0].Engine_Technology__c , assetList[0].Current_Location__c);
            container.fundList = (List<Lease_Fund_Events_External__c>)Database.query(mrFundQuery);                      
        }
        
        container.llpList.addAll(getLLPManagementList(assetId));
        container.ShopVisitList.addAll(getShopVisitList(assetId));
        
        return container;
    }
    
    //to retrieve LLP list.
    private static List<LLPManagementWrapper> getLLPManagementList(String assetId){
        
        List<LLPManagementWrapper> llpList = new List<LLPManagementWrapper>();
        
        for(Sub_Components_LLPs__c llp : [Select id, Name, Part_Number__c, TSN_F__c,
                                                          Serial_Number__c, CSN_New__c, 
                                                          Life_Limit__c
                                                          from Sub_Components_LLPs__c
                                                         where Constituent_Assembly__r.Attached_Aircraft__c = : assetId]){
        
        		Decimal remaining = (llp.Life_Limit__c == null ? 0 : llp.Life_Limit__c)
                    				- (llp.CSN_New__c == null ? 0 : llp.CSN_New__c);
                                                             
            	llpList.add(new LLPManagementWrapper(llp.Name, llp.Part_Number__c, llp.Serial_Number__c,
                                                     llp.TSN_F__c, llp.CSN_New__c, remaining)); 
                                                             
         }
        
        return llpList;
    }
    
    //to retrieve shop visit list.
    private static List<ShopVisitWrapper> getShopVisitList(String assetId){
        
        List<ShopVisitWrapper> shopVisitList = new List<ShopVisitWrapper>();
        
        for(Assembly_Eligible_Event__c historicalEvent : [Select id, event_Type_external__c, Name_of_MRO__r.Name, 
                                                                      TSN__c,CSN__c, End_Date__c
                                                                      from Assembly_Eligible_Event__c
                                                                      where Constituent_Assembly__r.Attached_Aircraft__c = : assetId order by End_Date__c desc]){

        	shopVisitList.add(new ShopVisitWrapper(historicalEvent.event_Type_external__c, 
                                                   historicalEvent.End_Date__c, 
                                                   historicalEvent.Name_of_MRO__r.Name, historicalEvent.TSN__c,
                                                   historicalEvent.CSN__c)); 
        }
        
        return shopVisitList;
    } 
    
    
    public class ContainerWrapper{
        @AuraEnabled public String esn;
        @AuraEnabled public String assemblyId; //for utilization table.
        @AuraEnabled public EngineSummaryWrapper engineSummary;
        @AuraEnabled public LLPManagementSummaryWrapper llpSummary; 
        @AuraEnabled public List<LLPManagementWrapper> llpList;
        @AuraEnabled public List<ShopVisitWrapper> shopVisitList;
        @AuraEnabled public List<Lease_Fund_Events_External__c> fundList; 
        
        public ContainerWrapper(){
            llpList =  new List<LLPManagementWrapper>();
            shopVisitList = new List<ShopVisitWrapper>();
            fundList = new List<Lease_Fund_Events_External__c>();
        }
    }
    
    public class EngineSummaryWrapper{
        @AuraEnabled public String assetSeries;
        @AuraEnabled public String currentThrust;
        @AuraEnabled public String assetStatus;
        @AuraEnabled public String lessee;
        @AuraEnabled public String linkedMSN;
        @AuraEnabled public Date leaseStart;
        @AuraEnabled public Date leaseEnd;
        @AuraEnabled public String technology;
        @AuraEnabled public String currentLocation;

        public EngineSummaryWrapper(String assetSeries, String currentThrust, String assetStatus, String lessee, String linkedMSN, Date leaseStart,
                                   Date leaseEnd, String technology, String currentLocation){
        
        	this.assetSeries = assetSeries;
            this.currentThrust = currentThrust;
            this.assetStatus = assetStatus;
            this.lessee = lessee;
            this.linkedMSN = linkedMSN;   
            this.leaseStart = leaseStart;       
            this.leaseEnd =  (leaseEnd == Date.newInstance(2200, 01, 01) ? null : leaseEnd);                           
            this.technology = technology;
            this.currentLocation = currentLocation;
        }
    }
    
    public class LLPManagementSummaryWrapper{
        
        @AuraEnabled public Decimal tsn;
        @AuraEnabled public Decimal csn;
        @AuraEnabled public Decimal tso;
        @AuraEnabled public Decimal cso;
        @AuraEnabled public Decimal limiter;
        @AuraEnabled public String limitingPart;
        @AuraEnabled public String limitingModule;
        @AuraEnabled public Date lastShopVisit;
        
        public LLPManagementSummaryWrapper(Decimal tsn, Decimal csn, Decimal tso, Decimal cso, Decimal limiter,
                                          String limitingPart, String limitingModule, Date lastShopVisit){
            
        	this.tsn = tsn;
            this.csn = csn;
            this.tso = tso;
            this.cso = cso;
            this.limiter = limiter;
            this.limitingPart = limitingPart;
            this.limitingModule = limitingModule;
            this.lastShopVisit = lastShopVisit;                                  
                                                                                 
        }

    }
    
    public class LLPManagementWrapper{

        @AuraEnabled public String description;
        @AuraEnabled public String partNo;
        @AuraEnabled public String serialNo;
        @AuraEnabled public Decimal hours;
        @AuraEnabled public Decimal cycles;
        @AuraEnabled public Decimal remaining;
		
        public LLPManagementWrapper(String description, String partNo, String serialNo, Decimal hours, Decimal cycles, 
                                    Decimal remaining){
        
        	this.description = description;                                
            this.partNo = partNo;
            this.serialNo = serialNo;     
            this.hours = hours;        
            this.cycles = cycles; 
            this.remaining = remaining;                            
        }

    }
    
    public class ShopVisitWrapper{
        
        @AuraEnabled public String reason;
        @AuraEnabled public Date releaseDate;
        @AuraEnabled public String mro{get;set;}
        @AuraEnabled public Decimal tsn{get;set;} 
        @AuraEnabled public Decimal csn{get;set;} 
        
        ShopVisitWrapper(String reason, Date releaseDate, String mro, Decimal tsn, Decimal csn){
            this.reason = reason;
            this.releaseDate = releaseDate ;
            this.mro = mro;
            this.tsn = tsn;
            this.csn = csn;
        }
    }
}