global class UpdateRentOnLeaseScheduler implements Schedulable {
  
    //Safiya:28/4/2021 - The scheduler class has been updated and the following functions 
    //      have been moved to UpdateBalanceDueOnLeaseScheduler
    //      -UpdateLease()
    //      -FindMultiRentSch(list<lease__c> listLeaseToMultiRentRec)
    //      -CopyFieldMRSchToLease(list<lease__c> listLeaseToMultiRentRec,String TypeOfCall)
    //      -FetchRentRecToUpd(set<id> setMultiRentSchID, map<string,decimal> mapGetActIntRate, string strCalledFrom)
    //      -CreatListLeaseToMultiRentRec(string LeaseID)
    //      -sendEmailAlert(set<id> setMRSchID)
    
    global void execute(SchedulableContext ctx) {
        UpdateBalanceDueOnLeaseScheduler.callUpdateBalanceDueOnLeaseBatch();
        CertificateUpdateBatch.callCertificateUpdateBatch();//Hint: executing everyday to update correct certificate id in the current and next certificate on stipulation and variability schedule records
        OverdueOnInvoiceUpdateBatch.callOverdueUpdateBatch();//Hint : executing everyday to update the overdue field on invoice.
        /* Calling thetransaction batch from finish method of OverdueOnInvoiceUpdateBatch , so that all Invoices are updated with correct Amount Due and Overdue checkebox value 
        before processing the transactions*/
        //SupplementalRentTransactionsHandler.updateTransactions(); // Executing everyday to update the supplemental rent transactions on all Active leases
    }
}    
    
    
