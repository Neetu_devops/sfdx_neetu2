/*
    Created By: Anjani Prasad
    
    Decription :
        Date : 7th July 2016


*/


public class URLLPTriggerHandler implements ITrigger{

   	private final string triggerBefore = 'URLLPTriggerHandlerBefore';
    private final string triggerAfter = 'URLLPTriggerHandlerAfter';
        
    public URLLPTriggerHandler()
    {
    }
 
    public void bulkBefore()
    {
        system.debug('URLLPTriggerHandler.bulkBefore(+)');
        system.debug('URLLPTriggerHandler.bulkBefore(-)');
    }
     
    public void bulkAfter()
    {
		if(trigger.isUpdate){
    		set<Id> setURLIIds = new set<Id>();
            for(Utilization_Report_LLP__c curURLLP : (list<Utilization_Report_LLP__c>)trigger.new){
                if('Approved By Lessor'.equals((curURLLP.Status__c)))setURLIIds.add(curURLLP.Utilization_Report_List_Item__c);
            }
            if(!LeaseWareUtils.isfromFuture() && !LeaseWareUtils.isfromBatch() && setURLIIds.size()>0){
            	UpdateLLPMR(setURLIIds);
            }
		}
    	
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('URLLPTriggerHandler.beforeInsert(+)');

        system.debug('URLLPTriggerHandler.beforeInsert(+)');     
    }
     
    public void beforeUpdate()
    {

        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('URLLPTriggerHandler.beforeUpdate(+)');

        system.debug('URLLPTriggerHandler.beforeUpdate(-)');

    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        system.debug('URLLPTriggerHandler.beforeDelete(+)');
        
        system.debug('URLLPTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);

        system.debug('URLLPTriggerHandler.afterInsert(+)');
        
        system.debug('URLLPTriggerHandler.afterInsert(-)');      
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);        
        system.debug('URLLPTriggerHandler.afterUpdate(+)');
        
        system.debug('URLLPTriggerHandler.afterUpdate(-)');      
    }
     
    public void afterDelete()
    {
        system.debug('URLLPTriggerHandler.afterDelete(+)');        
        
        system.debug('URLLPTriggerHandler.afterDelete(-)');      
    }

    public void afterUnDelete()
    {
        system.debug('URLLPTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('URLLPTriggerHandler.afterUnDelete(-)');        
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
//        leasewareUtils.insertExceptionLogs();
    }
    
    @future
	public static void UpdateLLPMR(set<id> setURLIIds){


system.debug('Set URLI Ids ' + setURLIIds);

		set<Id> setLLPIds = new set<Id>();
		list<Utilization_Report_List_Item__c> listURLIs = [select id, For_The_Month_Ending__c,
    				(select id, LLP__c, Accumulated_MR__c from LLP_Utilization__r)
    				from Utilization_Report_List_Item__c where id in :setURLIIds];
system.debug('listURLIs ' + listURLIs);
    				
    	for(Utilization_Report_List_Item__c curURLI : listURLIs){
    		for(Utilization_Report_LLP__c curURLLP : curURLI.LLP_Utilization__r)setLLPIds.add(curURLLP.LLP__c);
    	}
system.debug('Set LLP Ids ' + setLLPIds);
		
    	map<id, Sub_Components_LLPs__c> mapLLPsToUpd = new map<id, Sub_Components_LLPs__c>([select id, Y_Hidden_MR_LLP_Utilization__c
    			 from Sub_Components_LLPs__c where id in :setLLPIds]);

system.debug('Map LLPs ' + mapLLPsToUpd);
    	for(Utilization_Report_List_Item__c curURLI : listURLIs){
    		for(Utilization_Report_LLP__c curURLLP : curURLI.LLP_Utilization__r){
    			mapLLPsToUpd.get(curURLLP.LLP__c).Y_Hidden_MR_LLP_Utilization__c=curURLLP.Accumulated_MR__c;
system.debug('Setting MR on ' + curURLLP.LLP__c + ' to ' + curURLLP.Accumulated_MR__c);

    		}
    	}
    	
		if(mapLLPsToUpd.size()>0)update mapLLPsToUpd.values();
	}

    
}