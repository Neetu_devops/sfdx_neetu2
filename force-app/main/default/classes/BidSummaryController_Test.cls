@isTest
public class BidSummaryController_Test {
    @isTest
    static void testMethod1(){
        Project__c project = new Project__c(Name = 'Test',
        Economic_Closing_Date__c = System.today(),
        Project_Deal_Type__c = 'Sale');
        insert project;
        
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Test',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'AE 3007',
        MSN_Number__c = '1230'
        );
        insert aircraft;
        
        Aircraft__c aircraft1 = new Aircraft__c(
        Name= '1234 Test',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'AE 3007',
        MSN_Number__c = '1234'
        );
        insert aircraft1;
        
        Ratio_Table__c ratioTable = new Ratio_Table__c(
        Name= 'Test Ratio');
        insert ratioTable;
        
        Counterparty__c counterParty = new Counterparty__c(
        Name = 'Leassor Test');
        insert counterParty;
        
        Operator__c operator = new Operator__c (
        Name = 'Airline Test');
        insert operator;
        
        Marketing_Activity__c marketingActivity = new Marketing_Activity__c(
        Name = 'Test MA',
        Prospect__c = operator.Id,
        Lessor__c = counterParty.Id,
        Deal_Type__c = 'Sale',
        Inactive_MA__c = False,
        Deal_Status__c = 'LOI',
        campaign__c = project.Id);
          
        insert marketingActivity;
       
        Aircraft_Proposal__c bidAssetTerm = new Aircraft_Proposal__c(Name= 'Test Asset',
                                                                 Project_Status__c = 'Bid',
                                                                 sale_price__c = 20,
                                                                 Marketing_Activity__c  = marketingActivity.ID,
                                                                 aircraft__c = aircraft.Id);
        insert bidAssetTerm;
        
        Aircraft_Proposal__c dataRoomAssetTerm = new Aircraft_Proposal__c(Name= 'Test Asset',
                                                                 Project_Status__c = 'Data Room',
                                                                 sale_price__c = 20,
                                                                 Marketing_Activity__c  = marketingActivity.ID,
                                                                 aircraft__c = aircraft1.Id);
        insert dataRoomAssetTerm;
 
        Lease__c lease = new Lease__c(
        Reserve_Type__c= 'Maintenance Reserves',
        Base_Rent__c = 21,
        Lease_Start_Date_New__c = System.today()-2,
        Lease_End_Date_New__c = System.today()
     // Escalation__c = 43,
    //  Escalation_Month__c = 'January',
        );
        insert lease;
        
        try{
            BidSummaryController.getAircraftData(project.Id);
        }catch(Exception ex){       }
        try{
            BidSummaryController.getProjectBidData(project.Id);
        }catch(Exception ex){       }        

        BidSummaryController.updateFilterStatus(project.Id, 'Filter');
        BidSummaryController.saveAssetTerm(bidAssetTerm);
        BidSummaryController.saveAssetTerm(dataRoomAssetTerm);
        BidSummaryController.AircraftContainer container = new BidSummaryController.AircraftContainer();

        BidSummaryController.updateBidPreferredField(project.Id, leasewareutils.getNamespacePrefix()+'Sale_Price__c');
        BidSummaryController.updateAssetDetailRecords(bidAssetTerm, null,null, null);
        BidSummaryController.updateMAAndAIDRecords(marketingActivity, new List<Aircraft_Proposal__c>{ bidAssetTerm });
        BidSummaryController.getBidSummary(project.Id);
        
         LW_Setup__c setting = new LW_Setup__c();
        setting.Name = 'Client Controller Name';
        setting.Value__c = 'DataSourceController';
        insert setting;
        
        try{
            BidSummaryController.validateTradingProfile(aircraft.Id, operator.Id);
        }catch(Exception e){
            System.debug('An exception occurred: ' + e.getMessage());
        }
        delete setting;
        
        try{
            BidSummaryController.validateTradingProfile(aircraft.Id, operator.Id);
        }catch(Exception e){
            System.debug('An exception occurred: ' + e.getMessage());
        }
        
    }
    
  
}