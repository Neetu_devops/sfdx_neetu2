public class NewsReportRestIntegrationHelper {

    //public List<Data> data;

	public class Data {
		public Integer id ;
		public Object price;  
		public Integer ad ;
		public Integer featured;  
		public Integer dateOfNews; 
		public String title ;
		public String description;  
		public Object keypoints {get;set;} 
		public String article {get;set;} // in json: abstract
		public Integer premium {get;set;} 
		public String source {get;set;} 
		public String url {get;set;} 
		public String image {get;set;} 
		public List<String> tags {get;set;} 
	//	public ReportType reportType {get;set;} // in json: type
		public Integer words {get;set;} 
	}

	public static List<Data> parse(String json) {
		System.debug('json--- \n');
		System.debug(json);
		Map<String, Object> m =(Map<String, Object>) System.JSON.deserializeUntyped(json);

		list<Object> dataList =(list<Object>)m.get('data');
		//System.debug('dataList Val ' + dataList);
		List<Data> data = new List<Data>();
		if (dataList == null) {return data ;}
		for(object curData : dataList) {
			//System.debug('curData---' + curData);
			Map<String, Object> newsArt =(Map<String, Object>)curData;
			Data curNewsData = new Data();
			curNewsData.id = (integer) newsArt.get('id');
			curNewsData.article = (string)newsArt.get('abstract');
			curNewsData.dateOfNews = (integer)newsArt.get('timestamp');				
			curNewsData.image = (string)newsArt.get('image');
			curNewsData.words = (Integer)newsArt.get('words');
			curNewsData.tags = new List<String>();
			if((list<object>)newsArt.get('tags') != null) {
				for(object curObj: (list<object>)newsArt.get('tags')) {
					curNewsData.tags.add((string)curObj);
				}
			}
			curNewsData.title = (string)newsArt.get('title');
			curNewsData.url = (string)newsArt.get('url');
			data.add(curNewsData);
		}
		//system.debug('Data size ' + data.size() + '\n' + data);
		return data;
	}

	
	// public class ReportType {
	// 	public Integer id {get;set;} 
	// 	public String name {get;set;} 

	// 	public ReportType(JSONParser parser) {
	// 		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
	// 			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
	// 				String text = parser.getText();
	// 				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
	// 					if (text == 'id') {
	// 						id = parser.getIntegerValue();
	// 					} else if (text == 'name') {
	// 						name = parser.getText();
	// 					} else {
	// 						System.debug(LoggingLevel.WARN, 'Type_Z consuming unrecognized property: '+text);
	// 						consumeObject(parser);
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	// public static void consumeObject(System.JSONParser parser) {
	// 	Integer depth = 0;
	// 	do {
	// 		System.JSONToken curr = parser.getCurrentToken();
	// 		if (curr == System.JSONToken.START_OBJECT || 
	// 			curr == System.JSONToken.START_ARRAY) {
	// 			depth++;
	// 		} else if (curr == System.JSONToken.END_OBJECT ||
	// 			curr == System.JSONToken.END_ARRAY) {
	// 			depth--;
	// 		}
	// 	} while (depth > 0 && parser.nextToken() != null);
	// }
}