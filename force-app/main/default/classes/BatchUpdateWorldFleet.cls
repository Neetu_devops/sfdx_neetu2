global class BatchUpdateWorldFleet implements Database.Batchable<sObject>, Database.Stateful{
    
    global map<String, Id> mapOps = new map<String, Id>();
    public map<String, Operator__c> mapOpsRec = new map<String, Operator__c>();
    public map<String, Counterparty__c> mapLessorRec = new map<String, Counterparty__c>();
    global map<String, Id> mapLessor = new map<String, Id>();
    public map<String, Id> mapCountries = new map<String, Id>();
    public map<String, Bank__c> mapCounterPartiesRec = new map<String, Bank__c>();
    global map<String, Id> mapCounterparties = new map<String, Id>();
    
    public map<string,map<string,CustomWrapper>> mapto = new map<string,map<string,CustomWrapper>>();
    public map<string,string> mapATgroup = new map<string,string>();
    global set<string> setOperatorsNotPresentinLW = new set<String>(); 
    integer iCount = 0; 
    
    
    //Feb 04, 2020
    public Set<String> setAircarftType = new set<String>();
    public Set<String> setAircraftTypeVarient = new set<String>();
    public Set<String> setAircraftTypeSeries = new set<String>();
    public Set<String> setEngineType = new set<String>();
    public Set<String> setEngineVariant = new set<String>();
    public Set<String> setManager = new set<String>();
    public Set<String> setOperatorRegion = new set<String>();
    public Map<String, Set<String>> aircarftVarientMap = new Map<String,Set<String>>();
    public Map<String, Set<String>> aircarftTypeEngineTypeMap = new Map<String,Set<String>>();
    public Map<String, Set<String>> engineTypeEngineVariantMap = new Map<String,Set<String>>();
    public Map<String,List<String>> fleetSlNoMap = new Map<String,List<String>>();
    public Map<String,String> acTypeMap = new Map<String,String>();
    public Set<Decimal> setBuildYear = new set<Decimal>();
    public Set<Decimal> setMTOW = new set<Decimal>();
    public Set<Decimal> setSeat = new set<Decimal>();
    
    Set<String> exMsgSet = new Set<String>();
    
    global BatchUpdateWorldFleet(){
        
        for(Operator__c curOp: [select id, Name,Fleet_Size__c,Aircraft_Type_Of_Interest_gp__c,Contacted_For_AC_Types_gp__c, Alias__c,Lessee_Legal_Name__c,World_Fleet_Operator_Name__c from Operator__c]){
            string opName = curOp.Name.toLowerCase();
            mapOps.put(opName, curOp.Id);
            mapOpsRec.put(curOp.Id,curOp);
            
            if(String.isNotBlank(curOp.Lessee_Legal_Name__c)){
                mapOps.put(curOp.Lessee_Legal_Name__c.toLowerCase(),curOp.Id);                    
            }
            if(String.isNotBlank(curOp.World_Fleet_Operator_Name__c)){
                mapOps.put(curOp.World_Fleet_Operator_Name__c.toLowerCase(),curOp.Id);
            }
            if(String.isNotBlank(curOp.Alias__c)){
                mapOps.put(curOp.Alias__c.toLowerCase(),curOp.Id);
            }
            
            if(curOp.Alias__c==null)continue;
            for(string curName : curOp.Alias__c.split(',')){
                opName = curName.trim().toLowerCase();
                if(opName!='')mapOps.put(opName, curOp.Id);
            }
        }
        for(Counterparty__c curCP: [select id, Name,Alias__c,Fleet_Size__c from Counterparty__c]){
            mapLessor.put(curCP.Name.toLowerCase(), curCP.Id);
            mapLessorRec.put(curCP.Id,curCP);
            if(curCP.Alias__c==null)continue; 
            mapLessor.put(curCP.Alias__c, curCP.Id); 
            for(string curName : curCP.Alias__c.split(',')){
                mapLessor.put(curName.trim().toLowerCase(), curCP.Id); 
            }          
        }
        
        for(Country__c curCountry: [select id, Name from Country__c]){
            mapCountries.put(curCountry.Name.toLowerCase().trim(), curCountry.Id);                    
        }
        for(Bank__c curCP: [select id, Name,Alias__c,Fleet_Size__c from Bank__c]){
            mapCounterPartiesRec.put(curCP.Id,curCP);
            string CPName = curCP.Name.toLowerCase();
            mapCounterparties.put(CPName, curCP.Id);
            if(curCP.Alias__c==null)continue;
            mapCounterparties.put(curCP.Alias__c, curCP.Id);
            for(string curName : curCP.Alias__c.split(',')){
                CPName = curName.trim().toLowerCase();
                if(CPName!='')mapCounterparties.put(CPName, curCP.Id);
            }
            
        }
        
        for(Mapping__c mapRec: [select Id, Name,Map_To__c from Mapping__c where IsActive__c=true]){
            acTypeMap.put(mapRec.name,mapRec.Map_To__c);
        }
        for(Aircraft_Type_Map__c curRec: [select id, Type__c,Variant__c,Engine_Type__c, Series__c,Aircraft_Group_F__c 
                                          from Aircraft_Type_Map__c
                                          where Type__c!=null and Aircraft_Group_F__c!=null]){
                                              mapATgroup.put(curRec.Type__c.toLowerCase(),curRec.Aircraft_Group_F__c);
                                              if(curRec.Variant__c!=null){
                                                  mapATgroup.put((curRec.Type__c+curRec.Variant__c).toLowerCase(),curRec.Aircraft_Group_F__c);
                                                  /*
if(curRec.Engine_Type__c!=null){
mapATgroup.put((curRec.Type__c+curRec.Variant__c+curRec.Engine_Type__c).toLowerCase(),curRec.Aircraft_Group_F__c);
}               */
                                              }/*
if(curRec.Engine_Type__c!=null){
mapATgroup.put((curRec.Type__c+curRec.Engine_Type__c).toLowerCase(),curRec.Aircraft_Group_F__c);
}  */
                                          }        
        for(Aircraft_Type__c curRec: [select id, Aircraft_Type__c,Engine_Type__c,Aircraft_Variant__c,Engine_Variant__c,Aircraft_Group_GP__c 
                                      from Aircraft_Type__c
                                      where Aircraft_Type__c!=null and Aircraft_Group_GP__c!=null]){
                                          mapATgroup.put(curRec.Aircraft_Type__c.toLowerCase(),curRec.Aircraft_Group_GP__c);
                                          if(curRec.Aircraft_Variant__c!=null){
                                              mapATgroup.put((curRec.Aircraft_Type__c+curRec.Aircraft_Variant__c).toLowerCase(),curRec.Aircraft_Group_GP__c);
                                          }
                                          
                                      }          
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        //delete [select id from Exception_Log__c where Trigger_Event_Type__c = 'World Fleet Update Batch'];
        resetFleetSizeField();//Method Call to reset Fleet Size field
        string query = 'select id, Operator__c, Aircraft_Type_Variant__c, Aircraft_Operator__c, AircraftType__c, EngineType__c, AircraftStatus__c,AircraftSeries__c,AircraftVariant__c, EngineVariant__c, ' +
            ' Owner__c, Manager__c, OperatorArea__c,SerialNumber__c, Related_Owner__c ,OperatorCountry__c,Related_Country__c,' + 
            ' MTOW__c, SeatTotal__c, BuildYear__c '+
            'from Global_Fleet__c '+ 
            'order by AircraftType__c' +
            (Test.isRunningTest()?' LIMIT 200':'');
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        iCount+=scope.size();
        system.debug('size='+iCount);
        System.debug('Operator not in LW:' + setOperatorsNotPresentinLW);
        executeHelper(scope);
        if(exMsgSet.isEmpty()){
            Savepoint spStart = Database.setSavepoint();
            try{
                update scope;
            }catch(Exception e){
                Database.rollback(spStart);
                if(!exMsgSet.contains(e.getMessage())){
                    exMsgSet.add(e.getMessage());
                }   
            }
        }
    }
    
    
    
    private void updateATGrpToOper(Operator__c opTempRec_p,Global_Fleet__c curGF_p){
        string ATGroupStr;
        if(curGF_p.AircraftType__c!=null ){
            ATGroupStr = mapATgroup.get(curGF_p.AircraftType__c.toLowerCase());
            if(curGF_p.AircraftSeries__c!=null){
                ATGroupStr = mapATgroup.get((curGF_p.AircraftType__c + curGF_p.AircraftSeries__c).toLowerCase());
            }
            if(curGF_p.AircraftVariant__c!=null && ATGroupStr==null){
                ATGroupStr = mapATgroup.get((curGF_p.AircraftType__c + curGF_p.AircraftVariant__c).toLowerCase());
            }
            if(ATGroupStr==null){
                LeaseWareUtils.createExceptionLog(null, 'Warning : No Aircraft Type Group found for {' + curGF_p.AircraftType__c + ','+ curGF_p.AircraftSeries__c + '/' + curGF_p.AircraftVariant__c + '}', null, null,'World Fleet Update Batch',false);
            }                       
        }
        
        if(ATGroupStr!=null){
            string[] strATGP;
            set<string> setAaircraftType = new set<string>();   
            
            setAaircraftType.add(ATGroupStr);
            if(opTempRec_p.Aircraft_Type_Of_Interest_gp__c!=null){
                strATGP = opTempRec_p.Aircraft_Type_Of_Interest_gp__c.split(';');
                setAaircraftType.addAll(new Set<string>(strATGP));
            }
            opTempRec_p.Aircraft_Type_Of_Interest_gp__c = LeasewareUtils.convertSetStrToMultiPick(setAaircraftType);
            
            opTempRec_p.Not_Contacted_For_AC_Types_gp__c =null;
            if(opTempRec_p.Contacted_For_AC_Types_gp__c!=null){
                strATGP = opTempRec_p.Contacted_For_AC_Types_gp__c.split(';');
                setAaircraftType.removeAll(new Set<string>(strATGP));
            }
            opTempRec_p.Not_Contacted_For_AC_Types_gp__c = LeasewareUtils.convertSetStrToMultiPick(setAaircraftType);
            
        }       
    }
    
    
    private void updateToATMap(map<string,map<string,CustomWrapper>>mapto,String AType_p,string AStatus_p,String ID_p){
        map<string,CustomWrapper> tempmap;
        CustomWrapper CWVar;
        if(mapto.containsKey(ID_p)){
            tempmap = mapto.get(ID_p);
            if(tempmap.containskey(AType_p)){
                CWVar = tempmap.get(AType_p);
                CWVar.addCount(AStatus_p) ; 
                tempmap.put(AType_p,CWVar);
            }
            else{
                CWVar= new CustomWrapper(AType_p,0,0);
                CWVar.addCount(AStatus_p) ;               
                tempmap.put(AType_p,CWVar);
            }
            
        }
        else{
            CWVar= new CustomWrapper(AType_p,0,0); //example Integer ii= new Integer(0);
            CWVar.addCount(AStatus_p); //example methodCount(Type,ii);
            tempmap = new map<string,CustomWrapper>();
            tempmap.put(AType_p,CWVar);
        }
        mapto.put(ID_p,tempmap);
        //system.debug('mapto' + mapto);
    }
    
    
    public class CustomWrapper implements Comparable{
        public integer InServiceCount=0;
        public integer OtherServiceCount=0;
        Public String AType;
        // Constructor
        public CustomWrapper(String AType,Integer InServiceCount,Integer OtherServiceCount ) {
            this.AType = AType;
            this.InServiceCount = InServiceCount;
            this.OtherServiceCount = OtherServiceCount;
        }
        
        public void addCount(String StatusType ) {
            if('In Service'.equalsIgnoreCase(StatusType) || 'Storage'.equalsIgnoreCase(StatusType)) this.InServiceCount +=1;
            else this.OtherServiceCount +=1;
        }    
        
        public Integer compareTo(Object compareTo) {
            // Cast argument to CustomWrapper
            CustomWrapper compareTo_mon_copy = (CustomWrapper)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (InServiceCount > compareTo_mon_copy.InServiceCount) {
                // Set return value to a negative value.
                returnValue = -1;
            } else if (InServiceCount < compareTo_mon_copy.InServiceCount) {
                // Set return value to a positive value.
                returnValue = 1;
            }
            return returnValue;      
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        if(exMsgSet.isEmpty()){
            
            postOperationsAfterWorldFleetUpdate();
            
            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                              TotalJobItems, CreatedBy.Email
                              FROM AsyncApexJob WHERE Id =:BC.getJobId()];
            string nWidth='300px';                     
            //AggregateResult agr = [select count(id) cnt from Exception_Log__c where Trigger_Event_Type__c = 'World Fleet Update Batch'];
            //Decimal countException = (Decimal)agr.get('cnt') ;
            string strMsg = '';
            
            strMsg +='\n Job with ID ' + BC.getJobId() + ' completed with status ' + a.Status 
                + '.\n' + a.TotalJobItems + ' batches with '+ a.JobItemsProcessed + ' processed  ' ;
            strMsg+=('\nTotal Number of records processed : ' + iCount +'\n' ) ; //+ ('\nTotal Number of warning/exception : ' + countException);
            strMsg+= '<br/> <table border=1><tr><td width= ' + nWidth + '><b> Operators not present in LeaseWorks </b></td></tr>';
            for (String str: setOperatorsNotPresentinLW){
                strMsg += '<tr><td>' + str + '</td></tr>';
            }
            strMsg += '</table>';
            
            LeaseWareUtils.sendEmailToLWAdmins('World Fleet Update', strMsg);
            LeaseWareUtils.sendEmail('World Fleet Update', strMsg, a.CreatedBy.Email);
            
            Database.executeBatch(new BatchUpdateWFLinkOnAsset(null), 100); 
        }else{
            LeaseWareUtils.sendEmail('World Fleet Update', 'There was an unexpected error updating all World Fleet references. Please contact your LeaseWorks System Administrator. \n \n \n  <br/><br/>Reason :'+ exMsgSet, UserInfo.getUserEmail());
            
        }
        
  }
    
    // A320 - 12, 727-8, ....
    // Output: Desc order of count :  A320:12,727:8
    // String should not be more than 255 character
    // Assumption: AircraftType size can never be more than 9999
    
    public static string getStringOfAT(map<string,CustomWrapper> mapOfATs){
        string strAcTypeCnt='',tempStr;
        list<CustomWrapper> listCW = new list<CustomWrapper>();
        for(string strKey:  mapOfATs.keySet()){
            listCW.add(mapOfATs.get(strKey));
        }
        listCW.sort();
        //system.debug('Sorted order = ' + listCW);
        for (CustomWrapper currec: listCW) {
            tempstr= currec.AType+': '+currec.InServiceCount +' (+'+currec.OtherServiceCount+' option/order)';
            if((strAcTypeCnt.length()+tempstr.length())<255 ){
                strAcTypeCnt+= currec.AType+': '+currec.InServiceCount +' (+'+currec.OtherServiceCount+' option/order)'+'\n';
            }
            else{ 
                System.debug('truncating the Fleet size field once its field size meet to 255 characters'); 
                break;  
            }
        }
        return strAcTypeCnt;
    }
    
    @AuraEnabled
    public static List<ListView> getListViews() {
        String sObjectname  = getNamespacePrefix()+ 'Global_Fleet__c';
        List<ListView> listviews =
            [SELECT Id, Name FROM ListView WHERE SobjectType = :sObjectname];
        return listViews;
    }
    @AuraEnabled
    public static string getNamespacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled
    public static void updateWorldFleetReference(){
        Database.executeBatch(new BatchUpdateWorldFleet());
    }
    
    
    public void updateWorldFleetFieldSummary() {
        
        try{
            WF_Field_Summary__c FleetSummaryRecord = new WF_Field_Summary__c();
            
            //get old record.   
            for(WF_Field_Summary__c oldFleetSummaryRecord : [select id from WF_Field_Summary__c limit 1]){
                FleetSummaryRecord = oldFleetSummaryRecord;
            }
            
            //delete old content document.
            if(FleetSummaryRecord.id != null) {
                Set<Id> documentIds = new Set<Id>();
                list<ContentDocumentLink> contentDocumentLinkList = [select id, ContentDocumentId from ContentDocumentLink where LinkedEntityId =:FleetSummaryRecord.Id and (ContentDocument.Title = 'AircraftType_Variant'  or ContentDocument.Title like 'AircraftType_EngineType%'  or ContentDocument.Title ='EngineType_EngineVariant' )]; 
                for(ContentDocumentLink contentLink : contentDocumentLinkList ) {
                    documentIds.add(contentLink.ContentDocumentId);
                }
                
                if(documentIds.size() > 0) {
                    List<ContentDocument> contentDocumentList = [select id from ContentDocument where id IN : documentIds];
                    if(contentDocumentList!=null && contentDocumentList.size()>0){
                        LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
                        delete contentDocumentList;
                        LeaseWareUtils.TriggerDisabledFlag = false; 
                    }
                }
            }
            
            //create world fleet summary record.
            
            String aircraftTypeStr = String.join(new List<String>(setAircarftType),',');
            if(aircraftTypeStr.length()>131000) aircraftTypeStr = aircraftTypeStr.left(131000) + '...';
            FleetSummaryRecord.Aircraft_Type__c = aircraftTypeStr ;
            
            String aircraftTypeVarStr = String.join(new List<String>(setAircraftTypeVarient),',');
            if(aircraftTypeVarStr.length()>131000) aircraftTypeVarStr = aircraftTypeVarStr.left(131000) + '...';
            FleetSummaryRecord.Aircraft_Type_Variant__c = aircraftTypeVarStr;
            
            
            String engineTypeStr = String.join(new List<String>(setEngineType),',');
            if(engineTypeStr.length()>131000) engineTypeStr = engineTypeStr.left(131000) + '...';
            FleetSummaryRecord.Engine_Type__c = engineTypeStr;
            
            String managerStr =  String.join(new List<String>(setManager),',');
            if(managerStr.length()>131000) managerStr = managerStr.left(131000) + '...';
            FleetSummaryRecord.Manager__c = managerStr;
            
            String oprAreaStr = String.join(new List<String>(setOperatorRegion),',');
            if(oprAreaStr.length()>32700) oprAreaStr = oprAreaStr.left(32700) + '...';
            FleetSummaryRecord.OperatorArea__c = oprAreaStr;
            
            FleetSummaryRecord.Name = DateTime.newInstance(date.today().year(),date.today().month(),date.today().day()).format('dd-MMM-YYYY');
            if(setMTOW != null && setMTOW.size() > 0) {
                List<Decimal> mtowList = new List<Decimal>(setMTOW);
                mtowList.sort();
                String mtowStr = String.join(mtowList,',');
                if(mtowStr.length()>32700) mtowStr = mtowStr.left(32700) + '...';
                FleetSummaryRecord.MTOW__c = mtowStr;
            }
            if(setSeat != null && setSeat.size() > 0) {
                List<Decimal> seatList = new List<Decimal>(setSeat);
                seatList.sort();
                String seatStr = String.join(seatList,',');
                if(seatStr.length()>32700) seatStr = seatStr.left(32700) + '...';
                FleetSummaryRecord.Seat__c = seatStr;
            }
            
            if(setBuildYear != null && setBuildYear.size() > 0) {
                List<Decimal> buildYearList = new List<Decimal>(setBuildYear);
                buildYearList.sort();
                String buildYearStr = String.join(buildYearList,',');
                if(buildYearStr.length()>32700) buildYearStr = buildYearStr.left(32700) + '...';
                FleetSummaryRecord.Build_Year__c = buildYearStr;
            }
            
            
            upsert FleetSummaryRecord;
            
            
            //store JSON files.  
            
            //To skip sharing with Library.
            LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
            
            ContentVersion contentversionVarient=new ContentVersion();
            contentversionVarient.Title='AircraftType_Variant';
            contentversionVarient.contentLocation='S';
            contentversionVarient.PathOnClient='AircraftType_Variant.json';
            contentversionVarient.FirstPublishLocationId=FleetSummaryRecord.Id;
            contentversionVarient.VersionData=Blob.valueOf(JSON.serialize(aircarftVarientMap));
            contentversionVarient.Description = 'Leaseworks Internal';
            
            insert contentversionVarient;
            
            ContentVersion contentversionEngineType=new ContentVersion();
            contentversionEngineType.Title='AircraftType_EngineType';
            contentversionEngineType.contentLocation='S';
            contentversionEngineType.PathOnClient='AircraftType_EngineType.json';
            contentversionEngineType.FirstPublishLocationId=FleetSummaryRecord.Id;
            contentversionEngineType.VersionData=Blob.valueOf(JSON.serialize(aircarftTypeEngineTypeMap));
            contentversionEngineType.Description = 'Leaseworks Internal';
            
            insert contentversionEngineType;
            
            ContentVersion contentversionEngineVariant=new ContentVersion();
            contentversionEngineVariant.Title='EngineType_EngineVariant';
            contentversionEngineVariant.contentLocation='S';
            contentversionEngineVariant.PathOnClient='EngineType_EngineVariant.json';
            contentversionEngineVariant.FirstPublishLocationId=FleetSummaryRecord.Id;
            contentversionEngineVariant.VersionData=Blob.valueOf(JSON.serialize(engineTypeEngineVariantMap));
            contentversionEngineVariant.Description = 'Leaseworks Internal';
            
            insert contentversionEngineVariant;
            LeaseWareUtils.TriggerDisabledFlag = false;
        }catch(Exception e){
            LeasewareUtils.createExceptionLog(e, e.getMessage(), null, null, 'Update World FLeet Field Summary', false);
            LeaseWareUtils.sendEmail('World Fleet Field Summary', 'There was an unexpected error updating World Fleet Field Summary. Please contact your LeaseWorks System Administrator. \n \n \n  <br/><br/>'+ e.getMessage(), UserInfo.getUserEmail());
            LeaseWareUtils.TriggerDisabledFlag = false;
        }
        
        
        
    }
    
    
    //Update World Fleet Field Summary
    //Update Fleet Summary for Operator/Lessor/Counterparty
    void postOperationsAfterWorldFleetUpdate(){
        system.debug(' Post Process of Batch Update World Fleet, World Fleet Field Summary........');
        String errorMessage='';
        try{
            updateWorldFleetFieldSummary();
            
            list<Operator__C> updateOPList = new list<Operator__C>();
            list<Counterparty__c> updateLSRList = new list<Counterparty__c>();
            list<Bank__c> updateCPList = new list<Bank__c>();
            Operator__C tempOp;
            Counterparty__c tempLSR;
            Bank__c tempCP;
            map<string,CustomWrapper> mapOfATs;
            
            for(string strAirCraTyp : mapto.keySet()){// this iteration for every oprator and Lessor
                
                tempOp = mapOpsRec.get(strAirCraTyp);  // get that particular Operator record to upate fleet_size field
                tempLSR = mapLessorRec.get(strAirCraTyp);  // get that particular Lessor record to upate fleet_size field
                tempCP = mapCounterPartiesRec.get(strAirCraTyp);
                mapOfATs = mapto.get(strAirCraTyp); // get all aircraft type and crossponding count of that particular operator/Lessor 
                if(tempOp!=null){ // to check aircrfat type and count is for operator or lessor
                    //system.debug('Oper Id and Name: ' + tempOp.name +' - '+ tempOp.id +' and AC Type and Count(Desc): ' +getStringOfAT(mapOfATs));
                    tempOp.Fleet_Size__c = getStringOfAT(mapOfATs);
                    updateOPList.add(tempOp);
                }
                if(tempLSR!=null){
                    //system.debug('Lessor Id and Name: ' + tempLSR.name +' - '+ tempLSR.id +' and AC Type and Count(Desc): ' +getStringOfAT(mapOfATs));
                    tempLSR.Fleet_Size__c = getStringOfAT(mapOfATs);
                    updateLSRList.add(tempLSR);
                }
                if(tempCP!=null){
                    tempCP.Fleet_Size__c = getStringOfAT(mapOfATs);
                    updateCPList.add(tempCP);
                }
                
            }
            if(updateOPList.size()>0){
                LeaseWareUtils.TriggerDisabledFlag=true;
                update updateOPList;
                LeaseWareUtils.TriggerDisabledFlag=false;
            }
            if(updateLSRList.size()>0){
                LeaseWareUtils.TriggerDisabledFlag=true;
                update updateLSRList;
                LeaseWareUtils.TriggerDisabledFlag=false;
            }
            if(updateCPList.size()>0){
                LeaseWareUtils.TriggerDisabledFlag=true;
                update updateCPList;
                LeaseWareUtils.TriggerDisabledFlag=false;
            }
        }catch(Exception e){
            LeaseWareUtils.TriggerDisabledFlag=false;
            LeasewareUtils.createExceptionLog(e, e.getMessage(), null, null, 'Fleet Summary Update For Operator/Lessor/Counterparty', false);
            LeaseWareUtils.sendEmail('Fleet Summary Update For Operator/Lessor/Counterparty', 'There was an unexpected error updating Fleet Summary. Please contact your LeaseWorks System Administrator. \n \n \n  <br/><br/>'+ e.getMessage(), UserInfo.getUserEmail());
        }
        
    }
    
    
    public void executeHelper(List<Global_Fleet__c> scope){
        Operator__c opTempRec;
        for(Global_Fleet__c curGF : (Global_Fleet__c[])scope){ 
            try{
                Id OpId = null;
                curGF.Aircraft_Operator__c = null;
                if(curGF.Operator__c != null){                                
                    string strOp=curGF.Operator__c.toLowerCase();
                    OpId=mapOps.get(strOp);
                    if(OpId == null) setOperatorsNotPresentinLW.add(curGF.Operator__c);
                    if(OpId!=null){
                        curGF.Aircraft_Operator__c = OpId;
                        updateToATMap(mapto,curGF.AircraftType__c,curGF.AircraftStatus__c,curGF.Aircraft_Operator__c); // This is for Opreator records
                        opTempRec = mapOpsRec.get(OpId);
                        updateATGrpToOper(opTempRec,curGF);
                    }                
                }
                
                //get unique items
                //Feb 03, 2020.
                
                if(curGF.AircraftType__c != null) {
                    setAircarftType.add(curGF.AircraftType__c);
                }
                
                if(curGF.Aircraft_Type_Variant__c != null) {
                    setAircraftTypeVarient.add(curGF.Aircraft_Type_Variant__c);
                }
                
                if(curGF.EngineType__c != null) {
                    setEngineType.add(curGF.EngineType__c);
                }
                
                if(curGF.OperatorArea__c != null) {
                    setOperatorRegion.add(curGF.OperatorArea__c);
                }
                if(curGf.AircraftType__c != null && curGF.AircraftSeries__c !=null){
                    if(!aircarftVarientMap.containsKey(curGf.AircraftType__c)){
                        aircarftVarientMap.put(curGf.AircraftType__c,new Set<String>());
                    }
                    aircarftVarientMap.get(curGf.AircraftType__c).add(curGF.AircraftSeries__c);
                }
                
                if(curGf.AircraftType__c != null && curGF.EngineType__c !=null){
                    if(!aircarftTypeEngineTypeMap.containsKey(curGf.AircraftType__c)){
                        aircarftTypeEngineTypeMap.put(curGf.AircraftType__c,new Set<String>());
                    }
                    aircarftTypeEngineTypeMap.get(curGf.AircraftType__c).add(curGF.EngineType__c);
                }
                if(curGf.AircraftType__c != null && curGF.EngineType__c !=null && curGF.EngineVariant__c !=null){
                    if(!engineTypeEngineVariantMap.containsKey(curGf.AircraftType__c+'_'+curGf.EngineType__c)){
                        engineTypeEngineVariantMap.put(curGf.AircraftType__c+'_'+curGf.EngineType__c,new Set<String>());
                    }
                    engineTypeEngineVariantMap.get(curGf.AircraftType__c+'_'+curGf.EngineType__c).add(curGF.EngineVariant__c);
                }
                try {
                    if(curGF.MTOW__c != null && curGf.MTOW__c > 0) 
                        setMTOW.add(curGF.MTOW__c.setscale(0));
                }
                catch(Exception e){}
                
                try {
                    if(curGF.SeatTotal__c != null && curGf.SeatTotal__c > 0) 
                        setSeat.add(curGF.SeatTotal__c.setscale(0));
                }
                catch(Exception e){}
                
                if(curGf.BuildYear__c != null) {
                    try{
                        setBuildYear.add(Decimal.valueOf(curGf.BuildYear__c));
                    }
                    catch(Exception e){}
                }
                
                
                
                
                Id CountryId=null;
                if(curGF.OperatorCountry__c != null) {
                    CountryId=mapCountries.get(curGF.OperatorCountry__c.toLowerCase().trim());
                    curGF.Related_Country__c=CountryId;               
                }
                
                //Related to COunterparty renamed as Related To COunterparty Manager - This reference is automatically filled by the system with a Counterparty matching by Name or Alias the value in the field 'Manager'
                //Related Owner renamed as Related To Lessor Manager - This reference is automatically filled by the system with a Lessor matching by Name or Alias the value in the field 'Manager'
                
                Id CPIdManager = null;
                Id lessorIdManager=null;
                curGF.Related_Owner__c =null;
                curGF.Related_To_Counterparty__c=null;
                if(curGF.Manager__c != null){
                    setManager.add(curGF.Manager__c);
                    CPIdManager=mapCounterparties.get(curGF.Manager__c.toLowerCase());
                    if(CPIdManager!=null) {
                        curGF.Related_To_Counterparty__c = CPIdManager;
                        updateToATMap(mapto,curGF.AircraftType__c,curGF.AircraftStatus__c,curGF.Related_To_Counterparty__c); // This is for Counterparty records
                    }else{
                        lessorIdManager=mapLessor.get(curGF.Manager__c.toLowerCase());
                        if(lessorIdManager !=null){
                            curGF.Related_Owner__c = lessorIdManager;
                            updateToATMap(mapto,curGF.AircraftType__c,curGF.AircraftStatus__c,curGF.Related_Owner__c); // This is for Lessor records
                            
                        }
                    }
                }
                
                //Related To Counterparty(Owner) field is newly created
                //Related To Lessor(Owner) field is newly created
                Id CPIdOwner = null;
                Id lessorIdOwner = null;
                curGF.Related_To_Lessor_Owner__c = null;
                curGF.Related_To_Counterparty_Owner__c=null;
                if(curGF.Owner__c != null){
                    CPIdOwner=mapCounterparties.get(curGF.Owner__c.toLowerCase());
                    if(CPIdOwner!=null) {
                        curGF.Related_To_Counterparty_Owner__c = CPIdOwner;
                        updateToATMap(mapto,curGF.AircraftType__c,curGF.AircraftStatus__c,curGF.Related_To_Counterparty_Owner__c); // This is for Counterparty records
                    }else{
                        lessorIdOwner=mapLessor.get(curGF.Owner__c.toLowerCase());
                        if(lessorIdOwner !=null){
                            curGF.Related_To_Lessor_Owner__c = lessorIdOwner;
                            updateToATMap(mapto,curGF.AircraftType__c,curGF.AircraftStatus__c,curGF.Related_To_Lessor_Owner__c); // This is for Lessor records
                            
                        }
                        
                    }
                }
            }catch(Exception e){
                
                if(!exMsgSet.contains(e.getMessage())){
                    exMsgSet.add('\n ' + e.getMessage());
                }
            }
            
        }
        
    }
        // Method to reset the Fleet Size field on Operator/Lessor/Counterparty
    void resetFleetSizeField(){
            
        list< Operator__c> resetOpsList = new list< Operator__c>();
        list< Bank__c> resetCpList = new list< Bank__c>();
        list< Counterparty__c> resetLessorList = new list< Counterparty__c>();
        for (String curkey  :mapOpsRec.keySet()){
            Operator__c  curOp=mapOpsRec.get(curkey);
            if(String.isNotBlank(curOp.Fleet_Size__c))
            {
              curOp.Fleet_Size__c=null;
              resetOpsList.add(curOp); 
            }
        }
               
         for(String curkey  :mapLessorRec.keySet()){
            Counterparty__c curLessor=mapLessorRec.get(curkey);
            if(String.isNotBlank(curLessor.Fleet_Size__c))
            {
              curLessor.Fleet_Size__c=null;
              resetLessorList.add(curLessor); 
            }
         }   
         for(String curkey  :mapCounterPartiesRec.keySet()){
            Bank__c curCP=mapCounterPartiesRec.get(curkey);
            if(String.isNotBlank(curCP.Fleet_Size__c))
            {
              curCP.Fleet_Size__c=null;
              resetCpList.add(curCP); 
            }
         }   
        LeaseWareUtils.TriggerDisabledFlag=true;                
            if(resetOpsList.size()>0) update resetOpsList;
            if(resetCpList.size()>0) update resetCpList;
            if(resetLessorList.size()>0) update resetLessorList;
        LeaseWareUtils.TriggerDisabledFlag=false;

    }
}