/*
    This is a GenericBatchQL class to spawn batches for any methods.
    Example:
        Check class GenericScriptExecute


*/
public without sharing class GenericBatchQL implements Database.Batchable<sObject>, Database.Stateful {

    string scriptMethodName;
    string infoMessage='';
    string exceptionMessage='';
    Integer numOfProcessedRec=0;
    boolean updateMode=false;
    map<string,string> mapParam;
    map<string,string> mapParamInitial;
    GenericScriptExecute scriptExecRec;
    Package_Script_History__c scriptReq;

    public GenericBatchQL(string methodName,boolean updateMode_p,map<string,string> mapParam_p,Package_Script_History__c scriptReq_p){
        system.debug('GenericBatchQL const Param =' + ' methodName ' + methodName + ' mapParam_p ' + mapParam_p + ' updateMode_p ' + updateMode_p );
        scriptMethodName = methodName;
        updateMode = updateMode_p;
        mapParam = mapParam_p;
        if(mapParam==null) {mapParam = new map<string,string>();}// to make the map pass by reference.
        mapParamInitial = mapParam_p;
        scriptExecRec = new GenericScriptExecute();
        scriptReq = scriptReq_p;
    }


    public Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('GenericBatchQL ScriptName =' + scriptMethodName);
        string scriptStatus = 'Processing';
        if(scriptMethodName.endsWithIgnoreCase(GenericScriptExecute.C_VERIFY)) {scriptStatus = 'Verification Processing';}
        GenericBatch.updatePackageScriptRec(scriptReq,scriptStatus);
        string queryString;
        list<object> retSObject = scriptExecRec.callStartMethod(scriptMethodName,updateMode,mapParam);
        if(retSObject!=null && retSObject.size()>0){
            queryString = (String)retSObject[0];// first element
        }

        if(queryString==null){
            exceptionMessage = 'Could not find the method for this script. Script name:' + scriptMethodName;
            queryString='select id,name from ApexClass where name= \'DoNotMatchAnyClassDummy\'  ';
        }else if(queryString.startsWith('ERROR')){
            exceptionMessage = queryString;
            queryString='select id,name from ApexClass where name= \'DoNotMatchAnyClassDummy\'  ';            
        }else if(scriptExecRec.isDependentScriptVerified(scriptReq)){
            exceptionMessage = 'Dependent script has not been completed. Scriptname=' + scriptMethodName;
            queryString='select id,name from ApexClass where name= \'DoNotMatchAnyClassDummy\'  ';            
        }else if(Test.isRunningTest()){
            queryString += ' Limit 5 ';
        }
        return Database.getQueryLocator(queryString);
    }
    
    
    public void execute(Database.BatchableContext bc, List<Object> scopeList)
    {
        system.debug('mapParam= ' + mapParam );
        infoMessage = infoMessage +  '\n================================================================================================';
        infoMessage = infoMessage +  '\n - Number of records=' + scopeList.size();
        system.debug('GenericBatchQL ' + scriptMethodName + '(+) - Number of records=' + scopeList.size());
        Savepoint spStart = Database.setSavepoint();
        try{
            if(String.IsBlank(exceptionMessage)){
                infoMessage = infoMessage +  '\n' + scriptExecRec.callExecuteMethod(scopeList,scriptMethodName,updateMode,mapParam,scriptReq);
            }else{
                infoMessage = infoMessage +  '\n - Not Executed this batch. Reason is ' + exceptionMessage;
            }
        }catch(Exception ex){
            Database.rollback(spStart);
            exceptionMessage += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
            system.debug('Exception : ' + ex +  + '===' +exceptionMessage);
        }
        numOfProcessedRec += scopeList.size();
        system.debug('mapParam1= ' + mapParam );
        system.debug('GenericBatchQL ' + scriptMethodName + '(-)' );
    } 
 
    public void finish(Database.BatchableContext bc) {
        try{
            

            if(!scriptMethodName.endsWithIgnoreCase(GenericScriptExecute.C_VERIFY)) {// for script
                finishMethodOnScriptRun();
                GenericScriptExecute.callVerifyBatch(scriptReq,mapParamInitial);
            }else{//for verify script
                finishMethodOnVreifyScriptRun();
            }
        }catch(Exception ex){
            LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),'Unexpected Error' + ' : Script Executed :'+scriptMethodName ,ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage(),null);
        }
        
    }

    public void finishMethodOnVreifyScriptRun(){

        string scriptStatus= 'Verified';
        if(exceptionMessage!='' || (mapParam!=null && mapParam.containsKey('ERROR'))){
            scriptStatus= 'Verification Error';
        }

        string outputStr ='[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] - ';
        if(LeaseWareUtils.isRunningInDevInstance()){
            outputStr += ' Running in Developer Instance : \n';
        }else {
            outputStr +=  ' Package Info=' +  system.requestVersion()   + '\n';
        }
        
        outputStr += '=> Error Message if any: \n' + exceptionMessage; 
        outputStr += '\n => Information \n' + infoMessage;
        
        GenericBatch.updatePackageScriptRec(scriptReq,scriptStatus);
        // attach file to the record
        try{
            LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
            LeaseWareStreamUtils.addFileToRecord(scriptMethodName,scriptReq.id,outputStr,'.log');
            LeaseWareUtils.TriggerDisabledFlag = false;
        }catch(Exception ex){
            LeaseWareUtils.TriggerDisabledFlag = false;
            outputStr =   '------------------------------------------\n' + ' Got error while atdding the log file to record.'+ex +
                        '------------------------------------------\n' + outputStr;
        }
        //sending email
        string subjectName = scriptStatus + ' : Script Executed :'+scriptMethodName +' on Date '+ system.today().format() ;
        string emailBody = 'Please find the attachment.';
        list<Document> documents = new list<Document>();
        //log file
        documents.add(LeaseWareStreamUtils.createDoc(outputStr,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
        
        LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),subjectName ,emailBody,documents);

    }    
    public void finishMethodOnScriptRun(){


        string scriptStatus= 'Succeeded';

        if(exceptionMessage!=''){
            scriptStatus= 'Error';
        }else if(mapParam!=null && mapParam.containsKey('ERROR')){
            scriptStatus= 'Warning';
        }  
        string outputStr ='[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] - ';
        if(LeaseWareUtils.isRunningInDevInstance()){
            outputStr += ' Running in Developer Instance : \n';
        }else {
            outputStr +=  ' Package Info=' +  system.requestVersion()   + '\n';
        }
        
        outputStr += '=> Error Message if any: \n' + exceptionMessage; 
        outputStr += '\n => Information \n' + infoMessage;
        
        GenericBatch.updatePackageScriptRec(scriptReq,scriptStatus);
        String aditionalCSVFile;
        if(mapParam!=null && mapParam.containsKey('LOAD_CSV_FILE')){
            aditionalCSVFile = mapParam.get('LOAD_CSV_FILE');
            
            try{
                LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
                LeaseWareStreamUtils.addFileToRecord(scriptMethodName,scriptReq.id,aditionalCSVFile,'.csv');
                LeaseWareUtils.TriggerDisabledFlag = false;
            }catch(Exception ex){
                LeaseWareUtils.TriggerDisabledFlag = false;
                outputStr =   '------------------------------------------\n' + ' Got error while atdding the csv file to record.'+ex +
                            '------------------------------------------\n' + outputStr;
            }                
        }
       

        // attach file to the record
        try{
            LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
            LeaseWareStreamUtils.addFileToRecord(scriptMethodName,scriptReq.id,outputStr,'.log');
            LeaseWareUtils.TriggerDisabledFlag = false;
        }catch(Exception ex){
            LeaseWareUtils.TriggerDisabledFlag = false;
            outputStr =   '------------------------------------------\n' + ' Got error while atdding the log file to record.'+ex +
                        '------------------------------------------\n' + outputStr;
        }
        //sending email
        string subjectName = scriptStatus + ' : Script Executed :'+scriptMethodName + ' processed (' + numOfProcessedRec+ ') records on Date '+ system.today().format() ;
        string emailBody = 'Please find the attachment.';
        list<Document> documents = new list<Document>();
        if(aditionalCSVFile!=null){
            documents.add(LeaseWareStreamUtils.createDoc(aditionalCSVFile,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.csv'));
        }
        //log file
        documents.add(LeaseWareStreamUtils.createDoc(outputStr,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
        
        LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),subjectName ,emailBody,documents);

    }

    


  
}