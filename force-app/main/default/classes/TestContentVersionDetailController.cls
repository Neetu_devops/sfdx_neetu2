@isTest
public class TestContentVersionDetailController {
    @isTest
    public static void testContentVersion(){
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion; 
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
    	ContentVersionDetailController.getContentVersionDetails(documents[0].Id);
    }
}