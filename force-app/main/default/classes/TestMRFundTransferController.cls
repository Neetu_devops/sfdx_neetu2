@isTest
private class TestMRFundTransferController {

    @testSetup
    static void setup() {
    
        Id recordId = Schema.SObjectType.Lease__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();

        // Set up the Aircraft record
		String aircraftMSN = '12345';
        Aircraft__c a1 = new Aircraft__c(MSN_Number__c=aircraftMSN, Date_of_Manufacture__c = System.today(),TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available', Derate__c=20);
        LeaseWareUtils.clearFromTrigger();
        insert a1;

        // Set up the Aircraft record
		String aircraftMSN1 = '54321';
        Aircraft__c a2 = new Aircraft__c(MSN_Number__c=aircraftMSN1, Date_of_Manufacture__c = System.today(),TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available', Derate__c=20);
        LeaseWareUtils.clearFromTrigger();
        insert a2;        

        


        Lease__c lease1 = new Lease__c(Name='TestLease1',Aircraft__c=a1.id , Lease_End_Date_New__c=Date.today()+20, Lease_Start_Date_New__c=Date.today()-10, recordtypeid=recordId);
        LeaseWareUtils.clearFromTrigger();insert lease1;
        Lease__c lease2 = new Lease__c(Name='TestLease2',Aircraft__c=a2.id , Lease_End_Date_New__c=Date.today()+20, Lease_Start_Date_New__c=Date.today()-20, recordtypeid=recordId);
        LeaseWareUtils.clearFromTrigger();insert lease2;


        map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);


        // Set up the Constt Assmbly record
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= a1.Id, Description__c ='test description', TSN__c=0, CSN__c=0,  Type__c ='Engine 1', Serial_Number__c='123');
        LeaseWareUtils.clearFromTrigger();
		insert consttAssembly;

		Constituent_Assembly__c consttAssembly1 = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= a2.Id, Description__c ='test description', TSN__c=0, CSN__c=0,  Type__c ='Airframe', Serial_Number__c='123');
        LeaseWareUtils.clearFromTrigger();
		insert consttAssembly1;

		// Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
		    Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
		
		Maintenance_Program_Event__c mpe1= new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
			Interval_2_Hours__c=20,
            Event_Type_Global__c = '4C/6Y'
        );
        insert mpe1;
		
        // Set up Project Event for engine
        Assembly_Event_Info__c event = new Assembly_Event_Info__c();
        event.Name = 'Test Info';
        event.Event_Cost__c = 20;
        event.Constituent_Assembly__c = consttAssembly.Id;
        event.Maintenance_Program_Event__c = mpe.Id;
        insert event;
		
		// Set up Project Event for Airframe
        Assembly_Event_Info__c event1 = new Assembly_Event_Info__c();
        event1.Name = 'Test Info';
        event1.Event_Cost__c = 20;
        event1.Constituent_Assembly__c = consttAssembly1.Id;
        event1.Maintenance_Program_Event__c = mpe1.Id;
        insert event1;
            
        
        List<Assembly_MR_Rate__c> assemblyRates = new List<Assembly_MR_Rate__c> { 
            new Assembly_MR_Rate__c(Lease__c = lease2.Id, Name = 'RateInfo 1', Base_MRR__c = 500,Rate_Basis__c='Cycles', Assembly_Lkp__c = consttAssembly1.id, Assembly_Event_Info__c = event1.id),
            new Assembly_MR_Rate__c(Lease__c = lease1.Id, Name = 'RateInfo 2', Base_MRR__c = 1500,Rate_Basis__c='Hours', Assembly_Lkp__c = consttAssembly.id , Assembly_Event_Info__c = event.id),
            new Assembly_MR_Rate__c(Lease__c = lease2.Id, Name = 'RateInfo 3', Base_MRR__c = 300, Rate_Basis__c='Cycles',Assembly_Lkp__c = consttAssembly1.id, Assembly_Event_Info__c = event1.id),
            new Assembly_MR_Rate__c(Lease__c = lease1.Id, Name = 'RateInfo 4', Base_MRR__c = 1200,Rate_Basis__c='Hours', Assembly_Lkp__c = consttAssembly.id, Assembly_Event_Info__c = event.id)
        };
        insert assemblyRates;
 
 /* Need to check with Sagar, why e are creating below date:

        Id assetReportType = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();
        Aircraft__c asset = new Aircraft__c(Name='asset1', MSN_Number__c='1234', Date_of_Manufacture__c=date.today(),
                                            Aircraft_Type__c='3DAero', TSN__c=0.00, CSN__c=0,
                                           recordtypeid = assetReportType);
        insert asset;
        
        lease1.Aircraft__c = asset.id;
        lease2.Aircraft__c = asset.id;
        //update lease1;update lease2;
        

        Id assemblyRecordId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId();
        Constituent_Assembly__c assembly = new Constituent_Assembly__c(Name='test', Aircraft_Type__c='737-300',
         Aircraft_Engine__c='737-300 @ CFM56', Engine_Model__c ='CFM56', Attached_Aircraft__c= asset.Id, 
         Description__c ='testtt', TSN__c=0, CSN__c=0, TSLV__c=0, CSLV__c= 0, Type__c ='Engine 1', Serial_Number__c='123',
         Engine_Thrust__c='-3B1');
        insert assembly;
        
        
        // Set up Project Event
        Assembly_Event_Info__c event3 = new Assembly_Event_Info__c();
        event3.Name = 'Test Info';
        event3.Event_Cost__c = 20;
        event3.Constituent_Assembly__c = assembly.Id;
        event3.Maintenance_Program_Event__c = mpe.Id;
        insert event3;

        List<Assembly_Eligible_Event__c> historicalEvent = new List<Assembly_Eligible_Event__c> {
            new Assembly_Eligible_Event__c(Name='event1', Constituent_Assembly__c=assembly.id, Projected_Event__c=event3.id,Start_Date__c=date.today()+2,TSN_CSN_Certified__c=true,TSN__c=5.00,CSN__c=5),
            new Assembly_Eligible_Event__c(Name='event2', Constituent_Assembly__c=assembly.id, Projected_Event__c=event3.id,End_Date__c=date.today(),TSN_CSN_Certified__c=true,TSN__c=5.00,CSN__c=5),
            new Assembly_Eligible_Event__c(Name='event3', Constituent_Assembly__c=assembly.id, Projected_Event__c=event3.id,Start_Date__c=date.today()-6,End_Date__c=date.today(),TSN_CSN_Certified__c=true,TSN__c=5.00,CSN__c=5)
        };
        insert historicalEvent;

        Id invoiceRecordId = Schema.SObjectType.Constt_Assembly_Eligible_Event_Invoice__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId();        
        List<Constt_Assembly_Eligible_Event_Invoice__c> eventInvoice = new List<Constt_Assembly_Eligible_Event_Invoice__c> { 
            new Constt_Assembly_Eligible_Event_Invoice__c(Constt_Assembly_Eligible_Event__c=historicalEvent[0].id, Invoice_Date__c=date.today(), Status__c='Approved', RecordTypeId=invoiceRecordId, Payment_Date__c=date.today(), Assembly_Amount__c=500), 
            new Constt_Assembly_Eligible_Event_Invoice__c(Constt_Assembly_Eligible_Event__c=historicalEvent[1].id, Invoice_Date__c=date.today(), Status__c='Approved', RecordTypeId=invoiceRecordId, Payment_Date__c=date.today(), Assembly_Amount__c=300), 
            new Constt_Assembly_Eligible_Event_Invoice__c(Constt_Assembly_Eligible_Event__c=historicalEvent[2].id, Invoice_Date__c=date.today(), Status__c='Approved', RecordTypeId=invoiceRecordId, Payment_Date__c=date.today(), Assembly_Amount__c=200)                 
        };
        try{
        	insert eventInvoice;
        }catch(Exception ex){
            system.debug('Unexpected error');
        }
        
        eventInvoice[0].Status__c = 'Paid';
        eventInvoice[1].Status__c = 'Paid';
        eventInvoice[2].Status__c = 'Paid';
        try{
        	update eventInvoice;
        }catch(Exception ex){
            system.debug('Unexpected error');
        }

        */
        
    }
    
    @isTest
    static void MRFundTransferTest() {
        
        Test.startTest();
        
        List<Lease__c> leases = [SELECT Id,Name,Serial_Number__c,Lease_End_Date_New__c,Lease_Start_Date_New__c FROM Lease__c];
        //getLeaseRecord(recordId)
        MRFundTransferController.getLeaseRecord(leases[0].Id);
        
        Date selectDate = Date.today();
        
        //getRecordData(sourceId, targetId)
        //for different lease
        MRFundTransferController.getRecordData(leases[0].Id, leases[1].Id, leases[0].Name, leases[1].Name, selectDate);
        //for same lease
        MRFundTransferController.getRecordData(leases[0].Id, leases[0].Id, leases[0].Name, leases[0].Name, selectDate);
        
        //saveData(recordsToInsert)
        List<Assembly_MR_Rate__c> assemblyRates = [SELECT Id, Name, Lease__c, Starting_MR_Balance__c, Assembly_Lkp__c,Assembly_Event_Info__c
                                ,Lease__r.Aircraft__c, Assembly_Lkp__r.Attached_Aircraft__c FROM Assembly_MR_Rate__c];
        for(Assembly_MR_Rate__c curRec:assemblyRates){
            system.debug('check issue: Name' + curRec.name + '==' + curRec.Lease__r.Aircraft__c + '==' + curRec.Assembly_Lkp__r.Attached_Aircraft__c);
        }
        List<MRFundTransferController.AssemblyMRInfoWrapper> recordWrappers = new List<MRFundTransferController.AssemblyMRInfoWrapper>();
        for(Assembly_MR_Rate__c aRate : assemblyRates) {
            
            MRFundTransferController.AssemblyMRInfoWrapper recordWrapper = new MRFundTransferController.AssemblyMRInfoWrapper(aRate.Id, aRate.Name, 250.00, aRate.Lease__c, '1234');
            recordWrappers.add(recordWrapper);
        }
        
        List<MRFundTransferController.MRFundContainerWrapper> childWrappers = new List<MRFundTransferController.MRFundContainerWrapper>{
            new MRFundTransferController.MRFundContainerWrapper(recordWrappers[0], 50.00, 250.00, leases[0].Id, leases[1].Id, leases[0].Name, leases[1].Name),
            new MRFundTransferController.MRFundContainerWrapper(recordWrappers[1], 50.00, 750.00, leases[0].Id, leases[1].Id, leases[0].Name, leases[1].Name)
        };
            
        childWrappers[0].targetRecord = recordWrappers[2];
        childWrappers[0].targetAmountAfterTransfer = 550;
        childWrappers[1].targetRecord = recordWrappers[3];
        childWrappers[1].targetAmountAfterTransfer = 1950;
        
        String jsonData = JSON.serialize(childWrappers);
        
        Id mrTransferRecord = MRFundTransferController.saveData(jsonData,system.today());
        
        //To get data and show on record home page
        MRFundTransferController.getSavedData(mrTransferRecord);
        
        //After inserting data
        List<Assembly_MR_Fund_Transfer__c> fundTransfers = [SELECT Id, Name FROM Assembly_MR_Fund_Transfer__c];
        System.assertEquals(2, fundTransfers.size());
        
        //To cover code of Approved MRFundTransfers.
        List<MR_Fund_Transfer__c> transfers = [SELECT ID, Status__c FROM MR_Fund_Transfer__c];
        transfers[0].Status__c = 'Approved';
        update transfers;
        
        MRFundTransferController.getRecordData(leases[0].Id, leases[1].Id, leases[0].Name, leases[1].Name, selectDate);
        Test.stopTest();
    }
}