public class AddTermsApexController {

    public class TermWPClass{
        @AuraEnabled public Integer SI_Num{get;set;}
        @AuraEnabled public string id{get;set;}
        @AuraEnabled public string name{get;set;}
        @AuraEnabled public string date_Of_Manufacture{get;set;}
        @AuraEnabled public string new_Used{get;set;}
        @AuraEnabled public boolean isRecExist{get;set;}
        @AuraEnabled public string isRecExistText{get;set;}
        public TermWPClass(Integer SI_Num,string id,string name,boolean isRecExist,string date_Of_Manufacture
                          ,string new_Used){
            this.SI_Num = SI_Num;
            this.Id = ID;this.name = name;this.isRecExist = isRecExist;
            
            this.date_Of_Manufacture = date_Of_Manufacture;
            this.new_Used = new_Used;
            
            
            if(this.isRecExist) isRecExistText = '  	Already exist.' ;
            else isRecExistText = '  	' ;
        }
    }    
    
	@AuraEnabled
    public static list<Pricing_Run_New__c> getPricingRunDetail(Id recId) {
        system.debug(' getPricingRunDetail recId-->'+recId);
		return [SELECT Id, name
        		FROM Pricing_Run_New__c 
                where Id = :recId
                ];
   
    }    
	@AuraEnabled
    public static List<TermWPClass> getTerms(Id recId) {
        system.debug('getTerms recId-->'+recId);
        
        list<TermWPClass> returnList = new list<TermWPClass>();
        
        list<Pricing_Run_New__c> PRrecordList = [SELECT Id, name ,Marketing_Activity__c
        		,(select id,Asset_Term__c from Pricing_Output_New__r)
        		FROM Pricing_Run_New__c 
                where Id = :recId 
                	and status__c in ('Open','PCM Review Rejected')
                ];
        if(PRrecordList.size()==0) return returnList;       
        set<string> alredayAddedIds = new set<string>();
        for(Pricing_Output_New__c curRec:PRrecordList[0].Pricing_Output_New__r){
        	alredayAddedIds.add(curRec.Asset_Term__c);
        }
        
		list<Aircraft_Proposal__c> allATList = [SELECT Id, name
                      ,Date_of_Manufacture__c,New_Used__c                          		
        		FROM Aircraft_Proposal__c 
                where Marketing_Activity__c = :PRrecordList[0].Marketing_Activity__c
                ORDER BY createdDate ASC];
        
        Integer i=1;
        /*
        // if exist
        for(Aircraft_Proposal__c curRec:allATList ){
            if(alredayAddedIds.contains(curRec.Id)){
            	 returnList.add(new TermWPClass(i,curRec.Id,curRec.Name,true));
            	 i++;
            }
            
        }
        */
        // if not exist
        string DOM_str;
        for(Aircraft_Proposal__c curRec:allATList ){
            if(!alredayAddedIds.contains(curRec.Id)){
            	DOM_str=null;
				if(curRec.Date_of_Manufacture__c!=null) DOM_str = curRec.Date_of_Manufacture__c.format();
            	returnList.add(new TermWPClass(i,curRec.Id,curRec.Name,false,DOM_str ,curRec.New_Used__c));
            	i++;
            }
            
        }        
        
		return    returnList;     
    }

	@AuraEnabled
    public static void callSaveApex(string recId,map<string,boolean> selectedIds_p) {
        system.debug(' callSaveApex+');
		
		system.debug('param' + recId+ selectedIds_p);
		list<Pricing_Output_New__c > PricingOutputInsUpd = new list<Pricing_Output_New__c >();	
		for(string ATids : selectedIds_p.keySet()){
			if(selectedIds_p.get(ATids)){
				PricingOutputInsUpd.add( new Pricing_Output_New__c(
								name = 'System Generated'
								,Asset_Term__c =ATids
								,Pricing_Run__c = recId
							)
						);
			}			
		}
		
		if(PricingOutputInsUpd.size()>0) insert PricingOutputInsUpd;
		
		system.debug(' callSaveApex+');
   
    } 
    
    
}