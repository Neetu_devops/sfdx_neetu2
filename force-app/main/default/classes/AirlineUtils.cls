public without sharing class AirlineUtils {
    
    /* Algo for the recommendation
     * 
     * Input : AssetID
     * Output: List of operators
     * 
     * 1) Get weight from Recommendation Factor object for Airline type
     * 2) Get asset details(Aircraft_Type, Aircraft_Variant, Engine, Vintage, MTOW,) for the asset Id 
     * 3) Get Operators having same Aircraft_type from WorldFleet
     * 4) For each operator list compare the variant , engine, vintage, mtow and have a weightage
     * 5) In the above list operators will be duplicated since same operator have many aircrafts.
     * 6) Need to add all weightage for one particular operator and remove dulpicate of that operator
     * 7) Repeat Step 6 for all operators
     * 8) Rank the unique operator list based on weightage
     * 9) Return top 10 or requested list of operators 
     * 
    */
    
    public static integer typeWeight = 75;
    public static integer variantWeight = 0;
    public static integer engineWeight = 0;
    public static integer vintageWeight = 0;
    public static integer mtowWeight = 0;
    public static integer vintageRange = 0;
    public static integer mtowRange = 0;
    public static integer existingCustomerWeight = 0;
    public static Set<String> existingOperatorList = new Set<String>();
    public static Map<String,AirlineRecommendation.OperatorWrapper> operatorWrapperList;
    public static integer numOfRecords = 10;
    
    
    public static String AircraftType;
    public static String AircraftVariant;
    public static String EngineType;
    public static Integer Vintage;
    public static Integer vintageFilterRange;
    public static Integer MTOWLeased;
    public static Integer mtowFilterRange;
    public static String Manager;
    public static String Region;
    
    //public static List<AirlineRecommendation.Properties> getAirline(String assetId, Integer leaseExpiry ){
    public static List<AirlineRecommendation.Properties> getAirline(String aircraftType, String variant, 
                                                                    String engine, integer vintage, 
                                                                    integer vintageRange, integer mtow, integer mtowRange,string manager, string region, String scoreFilter, Integer seatFrom, Integer seatTo){
        //assetId = 'a0R6F00000a1bqwUAA';
        system.debug('getAirline aircraftType '+aircraftType);
		system.debug('getAirlinesRecommended '+aircraftType + ', '+variant+ ', '+engine+ ', '
					+vintage+ ', '+vintageRange + ', '+mtow+ ', '+mtowRange+ ','+manager+ ', scoreFilter= '+scoreFilter+ ', '+seatFrom+', '+seatTo);
		operatorWrapperList = new Map<String,AirlineRecommendation.OperatorWrapper>();                                                                        
        getWeightage();
       // Aircraft__c asset = getAssetData(assetId);
		AircraftType = aircraftType;
		AircraftVariant = variant;
		EngineType = engine;
        Vintage = vintage;
        vintageFilterRange = vintageRange;
        MTOWLeased = mtow;
        mtowFilterRange = mtowRange;
        Manager = manager;
        Region = region;
        
        getExistingCustomer();
                                                       
       List<Global_Fleet__c> operatorList = getOperator(AircraftType,Manager,Region, seatFrom, seatTo);
      // System.debug('returned items----'+populateWeightage(operatorList, aircraftType));
       return populateWeightage(operatorList, aircraftType, ScoreFilter);                                                   
        
    }
    
    public static void getWeightage(){
        List<Recommendation_Factor__c> recommendationList = [
            select Type__c, Factor__c, Weightage_Number__c, range__c from Recommendation_Factor__c 
            where Type__c = 'Airline' and Inactive__c = false
        ];
      //  system.debug('getWeightage recommendationList '+recommendationList);
        if(recommendationList != null && recommendationList.size() > 0 ){
            for(Recommendation_Factor__c recommendation: recommendationList) {
                if(recommendation.Factor__c.equalsIgnoreCase('Aircraft Type') && recommendation.Weightage_Number__c != null)
                    typeWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                if(recommendation.Factor__c.equalsIgnoreCase('Aircraft Variant') && recommendation.Weightage_Number__c != null)
                    variantWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                if(recommendation.Factor__c.equalsIgnoreCase('Engine') && recommendation.Weightage_Number__c != null)
                    engineWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                
                if(recommendation.Factor__c.equalsIgnoreCase('Vintage') && recommendation.Weightage_Number__c != null) {
                    vintageWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                    if(recommendation.range__c != null)
                        vintageRange = Integer.valueOf(recommendation.range__c);
                }
       //         system.debug('recom '+vintageRange+' vintageWeight: '+vintageWeight);
                if(recommendation.Factor__c.equalsIgnoreCase('MTOW') && recommendation.Weightage_Number__c != null) {
                    mtowWeight = Integer.valueOf(recommendation.Weightage_Number__c); 
                    if(recommendation.range__c != null)
                        mtowRange = Integer.valueOf(recommendation.range__c);
                }                
                if(recommendation.Factor__c.equalsIgnoreCase('Existing Customer') && recommendation.Weightage_Number__c != null)
                    existingCustomerWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                
            }
        }
        recommendationList = null;
    }
    
    public static Aircraft__c getAssetData(String assetId){
      //  system.debug('getOperatorData asset id ' +assetId);
        List<Aircraft__c> asset = [
            select Id, Aircraft_Type__c, Aircraft_Variant__c, Engine_Type__c, 
            Vintage__c, MTOW_Leased__c, MSN_Number__c
            from Aircraft__c  where id =: assetId
        ];
        
        //System.debug('asset '+asset);
        if(asset.size() > 0)
            return asset[0];
        return null;
    }
    
    public static List<Global_Fleet__c> getOperator(String aircraftType, String manager, string region, Integer seatFrom, Integer seatTo) {
        String airType = aircraftType +'%';
        system.debug('manager is-----'+manager);
        List<Global_Fleet__c> allFleet = new List<Global_Fleet__c>();
        String query = 'select AircraftType__c, AircraftVariant__c, EngineType__c, MTOW__c , SerialNumber__c , Lease_Expiry__c,Operator__c, Operator_ID__c, Aircraft_Operator__r.Name, Manager__c, BuildYear__c,OperatorArea__c FROM Global_Fleet__c WHERE (AircraftType__c LIKE :airType)';
        if(manager != null && region !=null){
            query += ' AND (Manager__c =:manager) AND (OperatorArea__c =:region)';
        }
        if(manager == null && region !=null){
            query += ' AND (OperatorArea__c =:region)';
        }
        if(manager != null && region == null){
            query += ' AND (Manager__c =:manager)';
        }
        if(seatFrom >= 0 && seatTo > 0){
            query += ' AND (SeatTotal__c >= :seatFrom and SeatTotal__c <= :seatTo)';
        }
        
        System.debug(LoggingLevel.DEBUG,'query: '+query);
        allFleet = Database.query(query);
  
        //system.debug('----------operator-------'+json.serialize(allFleet));
        //system.debug('allFleet list '+allFleet.size());
        return allFleet;
    }
    
    public static void getExistingCustomer(){
        List<Lease__c> existingOperator = [
            select Operator__r.Name from Lease__c
            where 
            Active__c = true
        ];
        if(existingOperator == null) return;
        for(Lease__c lease: existingOperator ) {
            if(lease.Operator__c != null)
        		existingOperatorList.add(lease.Operator__r.Name);
        }
        existingOperator = null;
      //  system.debug('getExistingCustomer list '+existingOperatorList);
    }
    
    public static List<AirlineRecommendation.Properties> populateWeightage(List<Global_Fleet__c> operatorList, String aircraftType, String ScoreFilter){
        if(operatorList == null) return null;
        List<AirlineRecommendation.Properties> airlineWrapperList = new List<AirlineRecommendation.Properties>();
        
        boolean isVariantMatched = false;
        boolean isEngineMatched = false;
        boolean isExistingCustomer = false;
        integer weight = 1; 
        for(Global_Fleet__c fleet: operatorList) {
            try{
                weight = 1; 
                isVariantMatched = false;
                isEngineMatched = false;
                isExistingCustomer = false;
                //type is not checked because query is based on type
                if(AircraftVariant != null && fleet.AircraftVariant__c != null && 
                   fleet.AircraftVariant__c.equalsIgnoreCase(AircraftVariant)) {
                       weight = weight + variantWeight;
                       isVariantMatched = true;
                   }
                if(EngineType!= null && fleet.EngineType__c != null && 
                   fleet.EngineType__c.equalsIgnoreCase(EngineType)) {
                       weight = weight + engineWeight;
                       isEngineMatched = true;
                   }
                integer operatorBuildYr = 0 ;
                if(fleet.BuildYear__c != null && Vintage > 0) {
                    operatorBuildYr = Integer.valueOf(fleet.BuildYear__c);
                    try {
                        //String[] buildYearStr = asset.Vintage__c.split('-');
                        //system.debug('populateWeightage '+aircraft.Vintage__c + ' buildYearStr '+buildYearStr);
                        /*integer buildYear = 0 ;
                        if(buildYearStr != null && buildYearStr.size() > 0)
                            buildYear = Integer.valueOf(buildYearStr[0]);
                        */
                        if((Vintage > (operatorBuildYr - vintageFilterRange))  && 
                           (Vintage < (operatorBuildYr + vintageFilterRange)))
                            weight = weight + vintageWeight;
                    }
                    catch(Exception e){}
                }
                if((MTOWLeased > 0 && fleet.MTOW__c  != null) && 
                   (MTOWLeased> (fleet.MTOW__c - mtowFilterRange)) && 
                   (MTOWLeased < (fleet.MTOW__c + mtowFilterRange)))
                    weight = weight + mtowWeight;
                
                if(existingOperatorList != null && fleet.Operator__c != null && existingOperatorList.contains(fleet.Operator__c)) {
                    weight = weight + existingCustomerWeight;
                    isExistingCustomer = true;
                }
                
                AirlineRecommendation.OperatorWrapper operatorWrapper;
                if(fleet.Operator_ID__c != null && operatorWrapperList.get(fleet.Operator_ID__c) == null) {
                    operatorWrapper = new AirlineRecommendation.OperatorWrapper();
                    operatorWrapper.operatorName = fleet.Aircraft_Operator__r.Name;
                    operatorWrapper.weight = weight;
                    operatorWrapper.aircraftTypeCount = 1;
                    if(operatorBuildYr > 0) {
                        operatorWrapper.vintageMinRange = (operatorBuildYr - vintageFilterRange);
                        operatorWrapper.vintageMaxRange = (operatorBuildYr + vintageFilterRange);
                    }
                    else {
                        operatorWrapper.vintageMinRange = 0;
                        operatorWrapper.vintageMaxRange = 0;
                    }
                    operatorWrapper.aircraftVariantCount = 0;
                    operatorWrapper.aircraftEngineCount = 0;
                    if(isVariantMatched)
                        operatorWrapper.aircraftVariantCount = 1;
                    operatorWrapper.isExistingCustomer = isExistingCustomer;
                }
                else if(fleet.Operator_ID__c != null) {
                    operatorWrapper = operatorWrapperList.get(fleet.Operator_ID__c);
                    operatorWrapper.weight = operatorWrapper.weight + weight;
                    operatorWrapper.aircraftTypeCount = operatorWrapper.aircraftTypeCount + 1;
                    operatorWrapper.isExistingCustomer = isExistingCustomer;
                    if(isVariantMatched)
                        operatorWrapper.aircraftVariantCount = operatorWrapper.aircraftVariantCount + 1;
                    if(isEngineMatched)
                        operatorWrapper.aircraftEngineCount = operatorWrapper.aircraftEngineCount + 1;
                }
                //system.debug('isVariantMatched '+isVariantMatched + ' id:'+fleet.Operator_ID__c + ' vintageRange '+operatorBuildYr + ' vintageRange '+vintageRange);
                if(fleet.Operator_ID__c != null)
                operatorWrapperList.put(fleet.Operator_ID__c, operatorWrapper);
            }
            catch(Exception e){ system.debug('Exception populateWeightage: '+e);}
        }
        operatorList = null;
        //system.debug('operatorWrapperList size ' +JSON.serializePretty(operatorWrapperList));
        if(operatorWrapperList != null && operatorWrapperList.size() > 0){
            for(String id : operatorWrapperList.keySet()){
                AirlineRecommendation.OperatorWrapper operatorWrapper = operatorWrapperList.get(id);
                AirlineRecommendation.Properties wrapper = new AirlineRecommendation.Properties();
                wrapper.operatorId = id;
                if(operatorWrapper.operatorName != null)
                    wrapper.operatorName = operatorWrapper.operatorName; 
                wrapper.weight = operatorWrapper.weight;
                wrapper.aircraftTypeCount = operatorWrapper.aircraftTypeCount;
                wrapper.aircraftVariantCount = operatorWrapper.aircraftVariantCount;
                wrapper.aircraftEngineCount = operatorWrapper.aircraftEngineCount;
                wrapper.vintageMinRange = operatorWrapper.vintageMinRange;
                wrapper.vintageMaxRange = operatorWrapper.vintageMaxRange;
                wrapper.aircraftType = aircraftType;
                //system.debug('populateWeightage type: '+wrapper.aircraftType+  ' == '+AircraftType);
                wrapper.aircraftVariant = AircraftVariant;
                wrapper.aircraftEngine = EngineType;
                wrapper.isExistingCustomer = operatorWrapper.isExistingCustomer;
                airlineWrapperList.add(wrapper);
            }
            operatorWrapperList = null;
        }
       // system.debug('populateWeightage airlineWrapperList '+ airlineWrapperList.size());
        if(airlineWrapperList != null && airlineWrapperList.size() > 0) {
            airlineWrapperList.sort();
         //   system.debug('populateWeightage '+ airlineWrapperList.size());
           
            List<AirlineRecommendation.Properties> airlineWrapperListLimited = new List<AirlineRecommendation.Properties>();
            for(integer i = 0; i < airlineWrapperList.size() && i < 50; i++)
                airlineWrapperListLimited.add(airlineWrapperList.get(i)); 
            System.debug('ScoreFilter '+ScoreFilter);
            if(ScoreFilter.equalsIgnoreCase('Active Deals'))
            	airlineWrapperListLimited = getWeightageBasedOnDeals(airlineWrapperListLimited);
            else
                airlineWrapperListLimited = getWeightageBaseOnCreditRating(airlineWrapperListLimited);
            airlineWrapperListLimited = ICAOCodeCalc(airlineWrapperListLimited);
            airlineWrapperList = null;
            return airlineWrapperListLimited;   
        }
        return null;
    }
    
    public static List<AirlineRecommendation.Properties> populateXYAxis(List<AirlineRecommendation.Properties> dataList){
        integer highestWeight = dataList[0].weight;
        List<AirlineRecommendation.Properties> updatedDataList = new List<AirlineRecommendation.Properties>();
        
        set<id> setOpIds = new set<id>();
        for(AirlineRecommendation.Properties curData : dataList){
            setOpIds.add(curData.operatorId);
        }
        
        list<AggregateResult> agGFCountByOps = [select Aircraft_Operator__c OpId, count(Id) FleetCount from Global_Fleet__c where
                                         	Aircraft_Operator__c in :setOpIds group by Aircraft_Operator__c];
        map<id, integer> mapFleetCountByOps = new map<id, integer>();
        for(AggregateResult curOp : agGFCountByOps){
            mapFleetCountByOps.put((id)curOp.get('OpId'), (integer)curOp.get('FleetCount'));
        }

        list<AggregateResult> agGFCountByLeased = [select Aircraft_Operator__c OpId,  count(Id) FleetCount from Global_Fleet__c where
                                         	Aircraft_Operator__c in :setOpIds and IsLeased__c = TRUE
                                            group by Aircraft_Operator__c];
        map<id, integer> mapFleetCountByLeased = new map<id, integer>();
        for(AggregateResult curOp : agGFCountByLeased){
            mapFleetCountByLeased.put((id)curOp.get('OpId'), (integer)curOp.get('FleetCount'));
      //      System.debug('leasedCount '+(integer)curOp.get('FleetCount'));
        }

        integer totalCount, leasedCount;
        for(AirlineRecommendation.Properties data : dataList) {

            totalCount = mapFleetCountByOps.get(data.operatorId); 
            leasedCount = mapFleetCountByLeased.get(data.operatorId);
            if(leasedCount == null)
                leasedCount = 0;
            if(totalCount == null)
                totalCount = 0;
            Decimal xRatio = 0;
            if(totalCount != 0)
            	xRatio = ((leasedCount * 100 ) /totalCount);
            data.x_axis = Integer.valueOf(xRatio);
           
            Decimal yRatio = (data.weight * 100 / highestWeight);
            data.y_axis =  Integer.valueOf(yRatio);
            if(data.x_axis == 0 && data.y_axis == 0)
                system.debug('');
            else
            	updatedDataList.add(data);
            
         /*   system.debug('populateXYAxis '+highestWeight+' leasedCount:'+leasedCount +
                         ' totalCount:'+totalCount+' weight:'+ data.weight + ' opId:' +data.operatorId+
                         '(data.weight / highestWeight) '+data.x_axis + ' '+data.y_axis + ' ' + data.rank); */
        }
       // system.debug('populateXYAxis updatedDataList '+updatedDataList.size());
        
        return updatedDataList;
    }
    
    public static List<AirlineRecommendation.Properties> getWeightageBaseOnCreditRating(List<AirlineRecommendation.Properties> dataList){
        List<String> operatorList = new List<String>();
        for(AirlineRecommendation.Properties data: dataList){
            if(data.operatorId != null)
                operatorList.add(data.operatorId);
        }
        
        List<Operator__c> creditDataList = [
            select Id, Financial_Risk_Profile__c, Name from Operator__c where 
            Id in :operatorList
        ]; 
        
        //System.debug('getWeightageBaseOnCreditRating '+creditDataList );
        Map<String,String> creditMap = new Map<String,String>();
        for(Operator__c operator: creditDataList)  {
            String creditRating= operator.Financial_Risk_Profile__c == null? '':operator.Financial_Risk_Profile__c ;
            creditMap.put(operator.Name, creditRating);
        }
        
        System.debug('getWeightageBaseOnCreditRating creditMap '+creditMap );
        for(AirlineRecommendation.Properties data: dataList) {
            String creditRating = creditMap.get(data.operatorName);
        	system.debug('creditRating-----'+creditRating);
        	if(creditRating =='High')
                data.rank = 3;
			else if(creditRating == 'Moderate')
                data.rank = 2;
            else if(creditRating == 'Low')
                data.rank = 1;
            else 
            data.rank = 0;
            
        }
        System.debug('getWeightageBaseOnCreditRating dataList: '+JSON.serializePretty(dataList)); 
        return dataList;
    }
    
    
    public static List<AirlineRecommendation.Properties> getWeightageBasedOnDeals(List<AirlineRecommendation.Properties> dataList){
        List<String> operatorList = new List<String>();
        for(AirlineRecommendation.Properties data: dataList){
            if(data.operatorId != null)
                operatorList.add(data.operatorId);
        }
        
        List<Marketing_Activity__c> dealDataList = [
            select Prospect__c, Prospect__r.Name, Inactive_MA__c, Name from Marketing_Activity__c where 
            Prospect__c in :operatorList 
        ]; 
        if(dealDataList.size() <= 0)
            return dataList;
        System.debug('getWeightageBasedOnDeals '+JSON.serializePretty(dealDataList ));
        
        //1-green ,3-red ,4-grey
        for(AirlineRecommendation.Properties data: dataList) {
            data.rank = 4;
            boolean operatorExists = false;
            for(Marketing_Activity__c deal: dealDataList) {
                if(data.operatorId == deal.Prospect__c) {
                    operatorExists = true;
                    if(deal.Inactive_MA__c == false) {
                        data.rank = 1;
                        break;
                    }
                    else if(deal.Inactive_MA__c == true) 
                        data.rank = 3;
                }
            }
        }
        return dataList;
    }
   
     public static List<AirlineRecommendation.Properties> ICAOCodeCalc(List<AirlineRecommendation.Properties> dataList){
        List<String> operatorList = new List<String>();
        for(AirlineRecommendation.Properties data: dataList){
            if(data.operatorId != null)
                operatorList.add(data.operatorId);
        }
        List<Operator__c> operatorData = [select Id, ICAOCode__c from Operator__c 
                                          where Id in :operatorList];
        //system.debug('operatorList: ' +operatorList);
        for(Operator__c opr : operatorData) {
            for(AirlineRecommendation.Properties data: dataList){
                if(opr.Id == data.operatorId)
                    data.ICAO = opr.ICAOCode__c;
            }
        }
        
         List<Aircraft__c> activeLease = [ select Lease__c, Lease__r.Operator__c, Name 
                                          from Aircraft__c 
                                          where Status__c != 'Sold'
                                         ];
        //System.debug('ICAOCodeCalc activeLease '+activeLease);
        System.debug('ICAOCodeCalc activeLease size '+activeLease.size());
        if(activeLease.size() > 0) {
            Decimal totalActiveLease = activeLease.size();
            List<Aircraft__c> leaseDataList = [ select Lease__c, Lease__r.Operator__c, Name 
                                                        from Aircraft__c 
                                                        where Lease__r.Operator__c in :operatorList and  
                                                        Lease__r.Active__c = true
                                                       ];
            //System.debug('ICAOCodeCalc leaseDataList '+JSON.serializePretty(leaseDataList));
            if(leaseDataList.size() > 0) { 
                Map<Id, List<Aircraft__c>> operatorLeases = new Map<Id, List<Aircraft__c>>();
                for(Aircraft__c lease: leaseDataList) {
                    List<Aircraft__c> leases = null;
                    if(operatorLeases.get(lease.Lease__r.Operator__c) == null) 
                        leases = new List<Aircraft__c>();
                    else 
                        leases = operatorLeases.get(lease.Lease__r.Operator__c);
                    leases.add(lease);
                    operatorLeases.put(lease.Lease__r.Operator__c, leases);
                }
                //System.debug('ICAOCodeCalc operatorLeases '+JSON.serializePretty(operatorLeases));
                for(Aircraft__c asset : leaseDataList) {
                    for(AirlineRecommendation.Properties data: dataList) {
                        if(asset.Lease__r.Operator__c == data.operatorId) {
                            Decimal operatorLeaseCount = 0;
                            if(operatorLeases.get(asset.Lease__r.Operator__c) != null)
                                operatorLeaseCount = operatorLeases.get(asset.Lease__r.Operator__c).size();
                            data.ICAOPrecentage = ((operatorLeaseCount / totalActiveLease) * 100).setScale(1);
                        }
                    }
                }
            }
        }
        return dataList;
    }
   
}