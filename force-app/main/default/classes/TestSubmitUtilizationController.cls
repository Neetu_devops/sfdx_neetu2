@isTest
public class TestSubmitUtilizationController {
    @isTest
    public static void testLoadDataMethod(){
        Date lastDayOfMonth = System.today().addMonths(2).toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        Custom_Lookup__c lookup = new Custom_Lookup__c(Name = 'Default', Lookup_Type__c = 'Engine', Active__c = true);
        insert lookup;
        
        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        Lease__c leaseRecord=new Lease__c();
        leaseRecord.Security_Deposit__c=200;
        leaseRecord.Lessee__c=operatorRecord.Id;
        leaseRecord.Lease_Id_External__c='test local';
        leaseRecord.Lease_Start_Date_New__c=minStartDate;
        leaseRecord.Lease_End_Date_New__c=lastDayOfMonth;
        insert leaseRecord;
        
        Id recordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        
        Aircraft__c aircraftRecord=new Aircraft__c();
        aircraftRecord.Name='test aircraft';
        aircraftRecord.MSN_Number__c='123';
        aircraftRecord.Date_of_Manufacture__c = minStartDate;
        aircraftRecord.Aircraft_Type__c='737';
        aircraftRecord.Aircraft_Variant__c='700';
        aircraftRecord.TSN__c=1.00;
        aircraftRecord.CSN__c=1;
        aircraftRecord.RecordTypeId=recordTypeId;
        insert aircraftRecord;
        aircraftRecord.Lease__c = leaseRecord.Id;
        update aircraftRecord;
        
        leaseRecord.Aircraft__c = aircraftRecord.Id; 
        update leaseRecord;
        
        Id recordTypeId1 = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId();
        
        Constituent_Assembly__c assembly=new Constituent_Assembly__c();
        assembly.Name='test assembly';
        assembly.Serial_Number__c='1';
        assembly.RecordTypeId=recordTypeId1;
        assembly.TSO_External__c=2;
        assembly.CSN__c=4;
        assembly.TSN__c=5;
        assembly.CSO_External__c=3;
        assembly.Type__c='Airframe';
        assembly.Attached_Aircraft__c=aircraftRecord.Id;
        assembly.Asset__c = aircraftRecord.Id; 
        assembly.Replaced_External__c = false;
        insert assembly; 
        
        Utilization_Report_Staging__c utlizationReportStage=new Utilization_Report_Staging__c();
        utlizationReportStage.Name='test utlizationReportStage';
        utlizationReportStage.Utilization_Start_Date__c=System.today();
        utlizationReportStage.Lease_UR_Staging__c=leaseRecord.Id;
        utlizationReportStage.FH_String__c='2.001';
        utlizationReportStage.Airframe_Cycles_Landing_During_Month__c=2;
        utlizationReportStage.AF_CSN_as_per_Airline__c=2;
        utlizationReportStage.AF_TSN_as_per_Airline__c=3;
        utlizationReportStage.Aircraft__c=aircraftRecord.Id;
        utlizationReportStage.Status__c = 'Open';
        insert utlizationReportStage;
        Id recordTypeId2 = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();
        Id recordTypeId3 = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Landing_Gear').getRecordTypeId();
        
        Test.startTest();
        SubmitUtilizationController.ContainerWrapper containerWrap=new SubmitUtilizationController.ContainerWrapper();
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='APU';
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Engine 1';
        assembly.RecordTypeId=recordTypeId2;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Engine 2';
        assembly.RecordTypeId=recordTypeId2;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Engine 3';
        assembly.RecordTypeId=recordTypeId2;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Engine 4';
        assembly.RecordTypeId=recordTypeId2;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Landing Gear - Left Wing';
        assembly.RecordTypeId=recordTypeId3;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Landing Gear - Right Main';
        assembly.RecordTypeId=recordTypeId3;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Landing Gear - Left Main';
        assembly.RecordTypeId=recordTypeId3;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
       
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c='Landing Gear - Nose';
        assembly.RecordTypeId=recordTypeId3;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        
        assembly.Type__c=' Landing Gear - Right Wing';
        assembly.RecordTypeId=recordTypeId3;
        update assembly;
        containerWrap= SubmitUtilizationController.loadData(aircraftRecord.Id);
        SubmitUtilizationController.save(JSON.serialize(containerWrap),aircraftRecord.Id);
        Test.stopTest();
    }
}