public class MRRateTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'MRRateTriggerHandlerBefore';
    private final string triggerAfter = 'MRRateTriggerHandlerAfter';
    public MRRateTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('MRRateTriggerHandler.beforeInsert(+)');
        for(MR_Rate__c curRec : (list<MR_Rate__c>)trigger.new){
			if(!curRec.Override__c  && !LeaseWareUtils.isFromTrigger('AssemblyMRRateTriggerHandlerAfter')){
				curRec.Override__c = true;
			}
        }
        system.debug('MRRateTriggerHandler.beforeInsert(+)');     
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('MRRateTriggerHandler.beforeUpdate(+)');
        
        MR_Rate__c oldRec;
        set<id> setLeaseIds = new set<id>();
        set<id> setMRRateIds2 = new set<id>();
        for(MR_Rate__c curRec : (list<MR_Rate__c>)trigger.new){
        	oldRec = (MR_Rate__c)trigger.oldmap.get(curRec.id);
			if(!curRec.Override__c  && !LeaseWareUtils.isFromTrigger('AssemblyMRRateTriggerHandlerAfter')){
				curRec.Override__c = true;
			}
			if(oldRec.MR_Rate__c!=curRec.MR_Rate__c || oldRec.Esc_Per__c!=curRec.Esc_Per__c || oldRec.MR_Rate_Start_Date__c!=curRec.MR_Rate_Start_Date__c || oldRec.MR_Rate_End_Date__c!=curRec.MR_Rate_End_Date__c){
				setLeaseIds.add(curRec.Y_Hidden_LeaseId__c);
				setMRRateIds2.add(curRec.Assembly_MR_Info__c);
			}
			
        }	
        if(setLeaseIds.size()>0){
			set<string> setFindURS = new set<string>();
        	for(Utilization_Report__c curURRec:[select id,Y_hidden_Lease__c,Month_Ending__c from Utilization_Report__c where Y_hidden_Lease__c = :setLeaseIds]){
        		setFindURS.add(curURRec.Y_hidden_Lease__c + leasewareutils.getDatetoString(curURRec.Month_Ending__c,'MMMYYYY') );
        	}
        	boolean errorFlag=false;
        	Date firstDate,SeconDate;
        	String ErrorMessage ;
        	

	    	map<string,Assembly_MR_Rate__c> mapMRrate = new map<string,Assembly_MR_Rate__c>([Select id,Name,Annual_Escalation__c,
                                             Ratio_Table__c,Rounding_Method_On_Esc_MR_Rate__c,
                                               Rounding_Method_on_Interpolated_MR_Rate__c,Rounding_Decimals_Interpolated_MR_Rate__c,
                                               Rounding_Decimals_Escalated_MR_Rate__c,Base_MRR__c
	    								   ,(select id,MR_Rate__c 
	    		                           	from MR_Rates__r order by MR_Rate_Start_Date__c asc)
	    		                           from Assembly_MR_Rate__c	
	    		                           where id in :setMRRateIds2]); 
            MR_Rate__c prevRec;   
            Assembly_MR_Rate__c tempAssemblyMRrate;
            
                         	
	        for(MR_Rate__c curRec : (list<MR_Rate__c>)trigger.new){
	        	oldRec = (MR_Rate__c)trigger.oldmap.get(curRec.id);
	        	prevRec = null;
	        	tempAssemblyMRrate = mapMRrate.get(curRec.Assembly_MR_Info__c);
	        	
				System.RoundingMode rModeDefault = RoundingMode.HALF_UP;
            
			//	System.RoundingMode rModeForInterpolated= tempAssemblyMRrate.Rounding_Method_on_Interpolated_MR_Rate__c==null?rModeDefault:LeasewareUtils.getRoundingMode(tempAssemblyMRrate.Rounding_Method_on_Interpolated_MR_Rate__c);
				System.RoundingMode rMode = tempAssemblyMRrate.Rounding_Method_On_Esc_MR_Rate__c==null?rModeDefault:LeasewareUtils.getRoundingMode(tempAssemblyMRrate.Rounding_Method_On_Esc_MR_Rate__c);
				
           	Integer rndValue,rndValueInterpolated;
			   rndValue = Integer.valueOf(tempAssemblyMRrate.Rounding_Decimals_Escalated_MR_Rate__c == null ? 
			   '2' :tempAssemblyMRrate.Rounding_Decimals_Escalated_MR_Rate__c) ;

			//   rndValueInterpolated  = Integer.valueOf(tempAssemblyMRrate.Rounding_Decimals_Interpolated_MR_Rate__c == null ?
			//				'2' : tempAssemblyMRrate.Rounding_Decimals_Interpolated_MR_Rate__c );
		    	        	
	        	//find prev record
					for(MR_Rate__c curChildRec: tempAssemblyMRrate.MR_Rates__r){
						if(curRec.Id == curChildRec.Id){
							break;
						}
						prevRec = curChildRec;
					}	        	
	        	system.debug('=prevRec='+prevRec );
				if(oldRec.MR_Rate__c!=curRec.MR_Rate__c){
					if(curRec.Y_Hidden_Esc_Type__c=='LLP Catalog'){
						//EscFactor = getEscFactor(dt_St,mapLLPCatl);
						if(curRec.Esc_Per__c==null || curRec.Esc_Per__c==0){
							curRec.Esc_Per__c =0;curRec.Esc_Factor__c = 1.0;
						}
					}else if(curRec.Y_Hidden_Esc_Type__c=='Advanced'){
						curRec.Esc_Per__c = 0.0;
						curRec.Esc_Factor__c = 1.0 + curRec.Esc_Per__c;
					}					
					
					if(prevRec!=null && curRec.MR_Rate__c!=null && leasewareUtils.zeroIfnull(prevRec.MR_Rate__c)>0){
						Decimal EscPer = ((curRec.MR_Rate__c - prevRec.MR_Rate__c)/prevRec.MR_Rate__c);
                    
						curRec.Esc_Per__c = (EscPer *100 ).setScale(2,rMode);
					  //curRec.Esc_Factor__c = (1.0 + EscPer).setScale(rndValue,rMode);
					    curRec.Esc_Factor__c = (1.0 + EscPer);
					}					
					if(checkAnyURsUsages( curRec,setFindURS,curRec.MR_Rate_Start_Date__c,curRec.MR_Rate_End_Date__c)){
						curRec.addError('You can not change the MR Rate if Utilization is filled for that period and MR Rate is used in a calculation of MR invoice.');
						break;
					}
				}
				else if(oldRec.Esc_Per__c!=curRec.Esc_Per__c){
				
					if(curRec.Esc_Per__c != null){
                         // Shweta Rempved ----
						//curRec.Esc_Factor__c = (1.0 + curRec.Esc_Per__c/100).setScale(rndValue,rMode) ;
						curRec.Esc_Factor__c = (1.0 + curRec.Esc_Per__c/100);
					}
					if(curRec.Override__c && prevRec!=null && prevRec.MR_Rate__c!=null && prevRec.MR_Rate__c==0){// if override and prev rate is 0.
						// keep the same rate : no esc and esc factor
						curRec.Esc_Per__c = 0;curRec.Esc_Factor__c = 1.0;
					}else if(prevRec!=null && prevRec.MR_Rate__c!=null){
						curRec.MR_Rate__c =  (prevRec.MR_Rate__c * leasewareUtils.zeroIfnull(curRec.Esc_Factor__c)).setScale(rndValue,rMode);
					}else{
						curRec.MR_Rate__c = (leasewareUtils.zeroIfnull(mapMRrate.get(curRec.Assembly_MR_Info__c).Base_MRR__c) * leasewareUtils.zeroIfnull(curRec.Esc_Factor__c)).setScale(rndValue,rMode);
					}
					//system.debug('=prevRec.MR_Rate__c='+prevRec.MR_Rate__c + '=curRec.MR_Rate__c='+curRec.MR_Rate__c + '=curRec.Esc_Per__c='+curRec.Esc_Per__c + '=curRec.Esc_Factor__c='+curRec.Esc_Factor__c);
					if(checkAnyURsUsages( curRec,setFindURS,curRec.MR_Rate_Start_Date__c,curRec.MR_Rate_End_Date__c)){
						curRec.addError('You can not change the MR Rate if Utilization is filled for that period and MR Rate is used in a calculation of MR invoice.');
						break;
					}
				}		
						
				if( (oldRec.MR_Rate_Start_Date__c!=curRec.MR_Rate_Start_Date__c )){
					if(oldRec.MR_Rate_Start_Date__c>curRec.MR_Rate_Start_Date__c){ firstDate = curRec.MR_Rate_Start_Date__c;SeconDate = oldRec.MR_Rate_Start_Date__c;  }
					else { firstDate = oldRec.MR_Rate_Start_Date__c;SeconDate = curRec.MR_Rate_Start_Date__c;  }
					if(checkAnyURsUsages( curRec,setFindURS,firstDate,SeconDate)){
						curRec.addError('You can not change the MR Start Date if Utilization is filled for that period.');
						break;
					}					
				}	
				if( (oldRec.MR_Rate_End_Date__c!=curRec.MR_Rate_End_Date__c )){					
					if(oldRec.MR_Rate_End_Date__c>curRec.MR_Rate_End_Date__c){ firstDate = curRec.MR_Rate_End_Date__c.addMonths(1);SeconDate = oldRec.MR_Rate_End_Date__c;}
					else { firstDate = oldRec.MR_Rate_End_Date__c.addMonths(1);SeconDate = curRec.MR_Rate_End_Date__c;  }
					if(checkAnyURsUsages( curRec,setFindURS,firstDate,SeconDate, true)){
						curRec.addError('You can not change the MR End Date if Utilization is filled for that period.');
						break;
					}						
				}								        	
	        }        	
        }	
        
        system.debug('MRRateTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('MRRateTriggerHandler.beforeDelete(+)');
        
        
        system.debug('MRRateTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('MRRateTriggerHandler.afterInsert(+)');

		afterCall();
        system.debug('MRRateTriggerHandler.afterInsert(-)');      
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('MRRateTriggerHandler.afterUpdate(+)');
        
		afterCall();
        system.debug('MRRateTriggerHandler.afterUpdate(-)');      
    }
     
    public void afterDelete()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('MRRateTriggerHandler.afterDelete(+)');

      
        
        system.debug('MRRateTriggerHandler.afterDelete(-)');      
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        //system.debug('MRRateTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        //system.debug('MRRateTriggerHandler.afterUnDelete(-)');        
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	private boolean checkAnyURsUsages(MR_Rate__c curRec,set<string> setFindURS,Date firstDate,Date secondDate){
		if(curRec.Utilization__c){
			return true;
		}
		for(Date dt_St=firstDate.toStartOfMonth();dt_St<=secondDate;dt_St=dt_St.addMonths(1)){
			if(setFindURS.contains(curRec.Y_Hidden_LeaseId__c + leasewareutils.getDatetoString(dt_St,'MMMYYYY') ) ){
				return true;
			}						
		}
		return false;						
	}
	private boolean checkAnyURsUsages(MR_Rate__c curRec,set<string> setFindURS,Date firstDate,Date secondDate, boolean breakInPeriod)
	{	
		for(Date dt_St=firstDate.toStartOfMonth();dt_St<=secondDate;dt_St=dt_St.addMonths(1)){
			if(setFindURS.contains(curRec.Y_Hidden_LeaseId__c + leasewareutils.getDatetoString(dt_St,'MMMYYYY') ) ){
				return true;
			}						
		}
		return false;						
	}
    public void afterCall()
    {
        system.debug('MRRateTriggerHandler.afterCall(+)');
        
        set<id> setMRRateIds1 = new set<id>();
        set<id> setMRRateIds2 = new set<id>();
        MR_Rate__c oldrec;
        for(MR_Rate__c curRec : (list<MR_Rate__c>)trigger.new){
        	setMRRateIds1.add(curRec.Assembly_MR_Info__c);
        	if(trigger.IsUpdate){
	        	oldrec = (MR_Rate__c) trigger.oldmap.get(curRec.id);
	        	if(oldrec.MR_Rate__c!=null && oldrec.MR_Rate__c != curRec.MR_Rate__c){
	        		setMRRateIds2.add(curRec.Assembly_MR_Info__c);
	        	}
        	}
        }

		list<MR_Rate__c> updateMRrateList = new list<MR_Rate__c>();
    	map<string,Assembly_MR_Rate__c> mapMRrate = new map<string,Assembly_MR_Rate__c>([Select id,Name,Annual_Escalation__c,
                                                                                         Ratio_Table__c,Rounding_Method_On_Esc_MR_Rate__c,
                                               Rounding_Method_on_Interpolated_MR_Rate__c,Rounding_Decimals_Interpolated_MR_Rate__c,
                                               Rounding_Decimals_Escalated_MR_Rate__c,Escalation_Type__c
    								   ,(select id ,name,Esc_Factor__c,Esc_Per__c,MR_Rate_Start_Date__c,MR_Rate_End_Date__c
    		                           	from MR_Rates__r order by MR_Rate_Start_Date__c)
    		                           from Assembly_MR_Rate__c	
    		                           where id in :setMRRateIds1]);
    		                           
    	//validate overlapp	           
    	boolean errorFlag=false;                
    	for(Assembly_MR_Rate__c curRec:mapMRrate.values()){
    		MR_Rate__c prevRec;
    		for(MR_Rate__c curChildRec :curRec.MR_Rates__r ){
    			if(prevRec!=null && prevRec.MR_Rate_End_Date__c>=curChildRec.MR_Rate_Start_Date__c){
    				if(trigger.newmap.containsKey(curChildRec.Id)) trigger.newmap.get(curChildRec.Id).addError('MR Rate period '+ prevRec.name +' overlaps with the '+ curChildRec.name +'. Please change the dates to ensure there\'s no overlap of periods.');
    				else if(trigger.newmap.containsKey(prevRec.Id)) trigger.newmap.get(prevRec.Id).addError('MR Rate period '+ prevRec.name +' overlaps with the '+ curChildRec.name +'. Please change the dates to ensure there\'s no overlap of periods.');
    				errorFlag =true; break;
    			}
    			prevRec = curChildRec;
    		}
    		if(errorFlag) break;
    	}
    	
    		                           
    	if(errorFlag || trigger.IsInsert) return;
    	
    	// only for Update case	                           
    		                           
    	Decimal newEscRate,ratePercent;
    	Decimal EscFactor =1;	                           
    	for(Assembly_MR_Rate__c curRec:mapMRrate.values()){
    		if(!setMRRateIds2.contains(curRec.id)) continue;
    		newEscRate = null;
    		ratePercent = 0;
    		if(curRec.Annual_Escalation__c!=null) ratePercent = curRec.Annual_Escalation__c;
    		EscFactor =1.0;
			System.RoundingMode rModeDefault = RoundingMode.HALF_UP;
            
        //  System.RoundingMode rModeForInterpolated= curMRRParams.Rounding_Method_on_Interpolated_MR_Rate__c==null?rModeDefault:LeasewareUtils.getRoundingMode(curRec.Rounding_Method_on_Interpolated_MR_Rate__c);
            System.RoundingMode rMode = curRec.Rounding_Method_On_Esc_MR_Rate__c==null?rModeDefault:LeasewareUtils.getRoundingMode(curRec.Rounding_Method_On_Esc_MR_Rate__c);
            
             
	    	Integer rndValue,rndValueInterpolated ;
			rndValue = Integer.valueOf(curRec.Rounding_Decimals_Escalated_MR_Rate__c == null ? 
			'2' :curRec.Rounding_Decimals_Escalated_MR_Rate__c) ;

		//	rndValueInterpolated  = Integer.valueOf(curRec.Rounding_Decimals_Interpolated_MR_Rate__c == null ?
		//				 '2' : curRec.Rounding_Decimals_Interpolated_MR_Rate__c );
    	
	    	 		
    		for(MR_Rate__c curChildRec :curRec.MR_Rates__r ){
    			
    			if(newEscRate!=null){
					if(curRec.Escalation_Type__c=='LLP Catalog'){
						EscFactor = curChildRec.Esc_Factor__c;
					}else if(curRec.Escalation_Type__c=='Advanced'){
						EscFactor = (1.0 + ratePercent/100);
					}     				
    				
    				newEscRate = (newEscRate * leasewareUtils.zeroIfnull(EscFactor)).setScale(rndValue,rMode);
					system.debug('newEscRate::'+newEscRate);
					system.debug('EscFactor::'+EscFactor);
    				curChildRec.MR_Rate__c = newEscRate;
    				curChildRec.Esc_Factor__c = EscFactor;
    				if(EscFactor!=null) curChildRec.Esc_Per__c = ((EscFactor -1.0 )*100).setScale(2,rMode) ;
    				else curChildRec.Esc_Per__c =0;
    				updateMRrateList.add(curChildRec);
    			}
    			else if(trigger.newmap.containskey(curChildRec.id)){
					system.debug('newEscRate::'+newEscRate);
    				newEscRate = ((MR_Rate__c)trigger.newmap.get(curChildRec.id)).MR_Rate__c;  				
    			}
    		}
    		
    	}
    	system.debug('No of records=' + updateMRrateList.size());   
    	LeaseWareUtils.TriggerDisabledFlag=true;
    	if(updateMRrateList.size()>0) update updateMRrateList;
		LeaseWareUtils.TriggerDisabledFlag=false;
        system.debug('MRRateTriggerHandler.afterCall(-)');      
    }	
	
}