public class ProjectTriggerHandler implements  ITrigger{

    private final string triggerBefore = 'ProjectTriggerHandlerBefore';
    private final string triggerAfter = 'ProjectTriggerHandlerAfter';

    public ProjectTriggerHandler (){}
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    public void bulkAfter(){}
    
    
    public void beforeInsert(){
    
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('ProjectTriggerHandler.beforeInsert(+)');
        
        validateProjectName(Trigger.new);
        setDefaultConfigurations(Trigger.new);
        

        system.debug('ProjectTriggerHandler.beforeInsert(-)');    
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('ProjectTriggerHandler.beforeUpdate(+)');

        validateProjectName(Trigger.new);
        
        system.debug('ProjectTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    { 

    }
     
    public void afterInsert()
    {
        
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        
        system.debug('ProjectTriggerHandler.afterUpdate(+)');
        
        recalculateBidSummary(Trigger.new);

        system.debug('ProjectTriggerHandler.afterUpdate(-)');         
    }
     
    public void afterDelete()
    {
     
    }

    public void afterUnDelete()
    {
           
    }
     
    public void andFinally(){
        // insert any audit records
        LeaseWareUtils.TriggerDisabledFlag = false; // to make Trigger disabled at apex level  
    }
    

    public static void validateProjectName(List<Project__c> projectList){
    
        List<Project__c> filteredProjects = new List<Project__c>();
        
        //filter records.
        for(Project__c projectObj : projectList){
                
            if((Trigger.isUpdate && projectObj.name != ((Project__c)Trigger.oldMap.get(projectObj.Id)).name && projectObj.name !=null) ||
            
                (Trigger.isInsert && projectObj.name != null)){
                
                    filteredProjects.add(projectObj);
                }
        }
        
        if(filteredProjects.size()>0){
        
            //query existing ones and exclude filtered ones.
        
            Set<String> existingProjects = new Set<String>();
        
            String qry = 'select id, name from project__c';
            
            if(Trigger.isUpdate){
                qry  +=  ' where id not IN : filteredProjects' ;
            }
                 
            for(Project__c projectObj : Database.query(qry)){
                existingProjects.add(projectObj.name.toLowerCase());
            }
            
            
            for(Project__c projectObj : filteredProjects){
                  
                if(existingProjects.contains(projectObj.name.toLowerCase())){
                    projectObj.addError('Campaign Name must be unique.');
                }else{
                    existingProjects.add(projectObj.name.toLowerCase());
                }      
            }     
        }
    }
    
    //to recalulate bid summary according to new bid field.
    public static void recalculateBidSummary(List<Project__c> projectList){
    
        Set<Id> projectIds = new Set<Id>();
        Boolean stopSummaryCalculation = LeaseWareUtils.isFromTrigger('StopSummaryCalculation');
        
        if(!stopSummaryCalculation ){
        for(Project__c projectObj : projectList){
        
            if(projectObj.bid_field__c != ((Project__c)Trigger.oldMap.get(projectObj.Id)).bid_field__c){
                projectIds.add(projectObj.Id);
            }
        }
        
        if(projectIds.size()>0){
            Utility.calculateBidSummary(projectIds) ;   
        }
    }
    }
    
    public static void setDefaultConfigurations(List<Project__c> projectList){
    
         Map<String, String> projectConfigurationMap = new Map<String,String>();
    
        // get default configurations.
        for(Project_Configuration__mdt configuration : [SELECT Deal_Type__c,Default_Bid_Field__c FROM Project_Configuration__mdt]){
            projectConfigurationMap.put(configuration.Deal_Type__c, configuration.Default_Bid_Field__c);
        }
        
        for(Project__c projectObj : projectList){
            if(projectConfigurationMap.containsKey(projectObj.Project_Deal_Type__c)){
                projectObj.bid_field__c = projectConfigurationMap.get(projectObj.Project_Deal_Type__c);          
            }
        }
    }
    
}