public without sharing class SnapshotController {
    
    public static String INDEX_FILE_NAME = 'Index';
    public static String SNAPSHOT_CONFIGURATION_FILE_NAME = 'SnapshotConfiguration';
    
    @AuraEnabled
    public static String getObjectName(Id recordId) {
        System.debug('getObjectName recordId: '+recordId);
        if(recordId == null)
            return null; 
        String sObjName = recordId.getSObjectType().getDescribe().getLabel();
        return sObjName;
    }
    
    @AuraEnabled
    public static void createSnapshotForRecord(Id recordId, String comment) {
        system.debug('SnapshotController createSnapshotForRecord '+recordId);
        SnapshotCreatorControllerIndex.createSnapshots(recordId, comment);
    }

    @AuraEnabled
    public static List<SnapshotViewerControllerIndex.SnapshotDetails> getSnapshot(Id snapshotId, Id recordId) {
        return SnapshotViewerControllerIndex.getSingleSnapshotData(snapshotId, recordId);
    }
    
    @AuraEnabled
    public static List<SnapshotCreatorControllerIndex.SnapshotObjectField> getSnapshotRecordDetail(Id snapshotId, Id recordId) {
        return SnapshotViewerControllerIndex.getSnapshotRecDetail(snapshotId, recordId);
    }
    
    
    @AuraEnabled
    public static SnapshotViewerControllerIndex.SnapshotWrapper getAllSnapshots(Id recordId, Integer pageNumber, Integer pageSize) {
        return SnapshotViewerControllerIndex.getAllSnapshots(recordId, pageNumber, pageSize);
    }
    
    
}