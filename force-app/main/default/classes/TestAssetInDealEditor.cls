@isTest
public class TestAssetInDealEditor {
	
    @isTest
    static void loadTestCases() {
               
        
        Date myDate1 = Date.newInstance(2010, 2, 17);
        
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Aircraft',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
        Date_of_Manufacture__c = myDate1 );
        LeaseWareUtils.clearFromTrigger();
        insert aircraft;
        
        
        Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
        opr.Country__c = 'United States';
        opr.Region__c = 'Asia';
        LeaseWareUtils.clearFromTrigger();
        insert opr;
        System.assert(opr.Id != null);
        
        Lease__c lease1 = new Lease__c(
        Reserve_Type__c= 'LOC',
        Base_Rent__c = 21,
        Lease_Start_Date_New__c = System.today()-2,
        Lease_End_Date_New__c = System.today(),
        Operator__c = opr.id);
        LeaseWareUtils.clearFromTrigger();
        insert lease1;
               
        
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =lease1.Operator__c,
                                                            Deal_Type__c='Purchase' ,Deal_Status__c = 'Pipeline',
                                                            Marketing_Rep_c__c = 'N/A',Description__c='111',Asset_Type__c = 'Aircraft');
        LeaseWareUtils.clearFromTrigger();
        insert m1;
        LeaseWareUtils.clearFromTrigger();
        Date myDate = Date.newInstance(2010, 2, 17);
        
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c= aircraft.Id, Engine_Type_new__c='V2524',
                                                            Asset_Type__c  = 'A320',
                                                           Date_of_Manufacture__c = myDate);
        
        
        LeaseWareUtils.clearFromTrigger();
        insert AT1;
        
        String fieldSet = 'Fields_For_Editing_On_Bid_Matrix';
        String namespace = LeaseWareUtils.getNamespacePrefix();
        if(!String.isBlank(namespace))
            fieldSet = namespace + fieldSet;
        AssetInDealEditor.getColumns(m1.Id,fieldSet, namespace + 'Date_of_Manufacture__c');
        AssetInDealEditor.getAssetInDeal(m1.Id, fieldSet, namespace + 'Date_of_Manufacture__c');
        AssetInDealEditor.getAssetInDeal(m1.Id, '', namespace + 'Date_of_Manufacture__c');
        AssetInDealEditor.getColumns(m1.Id, '', namespace + 'Date_of_Manufacture__c');
        AssetInDealEditor.updateAssetInDeal(new List<Aircraft_Proposal__c>{AT1});
        AssetInDealEditor.deleteAssetInDeal(AT1.Id);
        AssetInDealEditor.getAIDRecordType(m1.Id);
    }
    
    @isTest
    private static void testDuplicateAssetsIncluded(){
        Operator__c oper = new Operator__c(Name='Airlines');
        insert oper ;
        Id OperatorId1 = oper.Id; 
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= ' Test Asset 1',
          	Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230'
        );
        LeaseWareUtils.clearFromTrigger(); 
        insert aircraft;
        Id Aircraft1 = aircraft.Id;  
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
           	Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
        LeaseWareUtils.clearFromTrigger(); 
        insert aircraft2;
        Id Aircraft2Id = aircraft2.Id; 
        
        Date StartDate=  Date.newInstance(2020,1,1);
        Date EndDate=  Date.newInstance(2020,12,31);        
        
        
        LeaseWareUtils.clearFromTrigger(); 
        Lease__c lease = new Lease__c(Name='TestLease1',Rent__c=100, Security_Deposit__c=10000,
        aircraft__c =Aircraft1, Operator__c=OperatorId1,Lease_End_Date_New__c=EndDate,Lease_Start_Date_New__c=StartDate);
        system.debug('Insert Lease');
        
        insert lease;
        system.debug('Inserted Lease1');
        LeaseWareUtils.clearFromTrigger(); 
        
        Lease__c lease2 = new Lease__c(Name='TestLease2',Rent__c=100, Security_Deposit__c=10000,
        aircraft__c =Aircraft2Id, Operator__c=OperatorId1,Lease_End_Date_New__c=EndDate,Lease_Start_Date_New__c=StartDate);
        system.debug('Insert Lease2');
        insert lease2;
        
       
        Id idRecordType=Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByDeveloperName().get('Payment_Deferral_Request').getRecordTypeId();
        system.debug('Record Type ID=-'+ idRecordType);
        system.debug('Insert Deal');
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                            Deal_Type__c='Payment Deferral Request' ,
                                                            RecordTypeId = idRecordType, 
                                                            Deal_Status__c = 'Request Received',
                                                            Deal_Name_Format__c= 'Prospect + MSNs',
                                                            Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                            Asset_Type__c='Aircraft');
        
        
        LeaseWareUtils.clearFromTrigger();
        insert m1;
        LeaseWareUtils.clearFromTrigger();
        Id idRecordTypeAsset=Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByDeveloperName().get('Payment_Deferral_Request').getRecordTypeId();
        system.debug('Record Type ID=-'+ idRecordType);
        List<Aircraft_Proposal__c> assetInDealList  = new List<Aircraft_Proposal__c>();
        String unexpectedMsg;
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy',Percent_Of_Rent_Requested_To_Be_Deferred__c=50,Percent_Of_Rent_Granted_To_Be_Deferred__c=50,
                                                            Months_Granted_by_Lessor__c=1,RecordTypeId = idRecordTypeAsset,
                                                            Months_Requested_by_Lessee__c=2,Marketing_Activity__c = m1.Id,Lease__c=lease.id, Aircraft__c= Aircraft1);
        assetInDealList.add(AT1);
        Aircraft_Proposal__c AT2= new Aircraft_Proposal__c(Name='Dummy',Percent_Of_Rent_Requested_To_Be_Deferred__c=50,Percent_Of_Rent_Granted_To_Be_Deferred__c=50,
                                                            Months_Granted_by_Lessor__c=1,RecordTypeId = idRecordTypeAsset, 
                                                            Months_Requested_by_Lessee__c=2,Marketing_Activity__c = m1.Id,Lease__c=lease.id, Aircraft__c= Aircraft1);
        assetInDealList.add(AT2);

        try{
            insert assetInDealList;
        }catch(Exception e){
            unexpectedMsg = e.getMessage();
            system.debug('MSN 1230 is included more than once in this deal');
           
        }
        system.assert(unexpectedMsg.contains('MSN 1230 is included more than once in this deal'));
        
    }
    @istest
    public static void testAircraftTypeQtyField(){
        LW_Setup__c setting = new LW_Setup__c();
        setting.Name = 'UPDATE_LATEST_FILE_FIELDS';
        setting.Value__c = 'ON';
        insert setting;
        map<String,Object> mapInOut = new map<String,Object>();
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
        Id idRecordTypeAsset=Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= ' Test Asset 1',
            Aircraft_Type__c='3DAero',
            RecordTypeId = idRecordTypeAsset, 
            MSN_Number__c = '111'
        );
        LeaseWareUtils.clearFromTrigger(); 
        insert aircraft;
        Id Aircraft1 = aircraft.Id;  
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Aircraft_Type__c='CFM56',
            RecordTypeId = idRecordTypeAsset,
            MSN_Number__c = '222'
        );
        LeaseWareUtils.clearFromTrigger(); 
        insert aircraft2;
        Id Aircraft2Id = aircraft2.Id; 
        
        Aircraft__c aircraft3 = new Aircraft__c(
            Name= ' Test Asset 3',
            Aircraft_Type__c='CFM56',
            RecordTypeId = idRecordTypeAsset,
            MSN_Number__c = '333'
        );
        LeaseWareUtils.clearFromTrigger(); 
        insert aircraft3;
        Id Aircraft3Id = aircraft3.Id; 
        Date StartDate=  Date.newInstance(2020,1,1);
        Date EndDate=  Date.newInstance(2020,12,31);        
        
        Id idRecordType=Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByDeveloperName().get('Lease').getRecordTypeId();
        system.debug('Record Type ID=-'+ idRecordType);
        system.debug('Insert Deal');
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
        Deal_Type__c='Lease' ,
        RecordTypeId = idRecordType, 
        Deal_Status__c = 'Pipeline',
        Deal_Name_Format__c= 'Prospect + MSNs',
        Asset_Type__c='Aircraft');


        LeaseWareUtils.clearFromTrigger();
        insert m1;
        Id idRecordTypeAID=Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByDeveloperName().get('Lease').getRecordTypeId();
        system.debug('Record Type ID=-'+ idRecordType);
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy',RecordTypeId = idRecordTypeAID,  Asset_Type__c='737-300',Rent__c=10000,Marketing_Activity__c = m1.id);
        Aircraft_Proposal__c AT2 = new Aircraft_Proposal__c(Name='Dummy2',RecordTypeId = idRecordTypeAID,Rent__c=50000,Asset_Type__c='737-300',Marketing_Activity__c = m1.id);
        Aircraft_Proposal__c AT3 = new Aircraft_Proposal__c(Name='Dummy2',RecordTypeId = idRecordTypeAID,Rent__c=50000,Asset_Type__c='737-500',Marketing_Activity__c = m1.id);
        Aircraft_Proposal__c AT4 = new Aircraft_Proposal__c(Name='Dummy2',RecordTypeId = idRecordTypeAID,Rent__c=50000,Asset_Type__c='737-500',Marketing_Activity__c = m1.id);
        Aircraft_Proposal__c AT5 = new Aircraft_Proposal__c(Name='Dummy2',RecordTypeId = idRecordTypeAID,Rent__c=50000,Asset_Type__c='737-500',Marketing_Activity__c = m1.id);
        Aircraft_Proposal__c AT6 = new Aircraft_Proposal__c(Name='Dummy2',RecordTypeId = idRecordTypeAID,Rent__c=50000,Asset_Type__c='737-800',Marketing_Activity__c = m1.id);
        Aircraft_Proposal__c AT7 = new Aircraft_Proposal__c(Name='Dummy2',RecordTypeId = idRecordTypeAID,Rent__c=50000,Asset_Type__c='737-200',Marketing_Activity__c = m1.id);
        LeaseWareUtils.clearFromTrigger();
        insert new list<Aircraft_Proposal__c>{AT1,AT2,AT3,AT4,AT5,AT6,AT7};

        m1 = [select id,Aircraft_Type_Qty_List__c from Marketing_Activity__c where id=:m1.id];
        system.assertEquals(m1.Aircraft_Type_Qty_List__c,'1 x 737-200\n2 x 737-300\n3 x 737-500\n1 x 737-800','Incorrect Aircraft Type & Qty field value');
    
        Aircraft_Proposal__c AT8 = new Aircraft_Proposal__c(Name='Dummy2',RecordTypeId = idRecordTypeAID,Rent__c=50000,Asset_Type__c='737-200',Marketing_Activity__c = m1.id);
        LeaseWareUtils.clearFromTrigger();
        insert AT8;
        m1 = [select id,Aircraft_Type_Qty_List__c from Marketing_Activity__c where id=:m1.id];
        system.assertEquals(m1.Aircraft_Type_Qty_List__c,'2 x 737-200\n2 x 737-300\n3 x 737-500\n1 x 737-800','Incorrect Aircraft Type & Qty field value');
    
        LeaseWareUtils.clearFromTrigger();
        delete AT8;
        m1 = [select id,Aircraft_Type_Qty_List__c from Marketing_Activity__c where id=:m1.id];
        system.assertEquals(m1.Aircraft_Type_Qty_List__c,'1 x 737-200\n2 x 737-300\n3 x 737-500\n1 x 737-800','Incorrect Aircraft Type & Qty field value');
    
       
    }
}