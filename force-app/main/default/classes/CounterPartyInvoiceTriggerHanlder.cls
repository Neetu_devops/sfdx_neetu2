public with Sharing class CounterPartyInvoiceTriggerHanlder implements ITrigger{

    
    // Constructor
    private final string triggerBefore = 'CounterPartyInvoiceTriggerHanlderBefore';
    private final string triggerAfter = 'CounterPartyInvoiceTriggerHanlderAfter';
    public CounterPartyInvoiceTriggerHanlder()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
       
            
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        
       
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

          

    }
     
    public void afterInsert()
    {
              
    }
     
    public void afterUpdate()
    {    
    	system.debug('afterUpdate CounterPartyInvoiceTriggerHanlder ++');
    	//Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
        
        updateVendorInvoice(); 
        system.debug('afterUpdate CounterPartyInvoiceTriggerHanlder --');  
    }
     
    public void afterDelete()
    {
            
    }

    public void afterUnDelete()
    {
             
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
    
    //After Update 
    public void updateVendorInvoice(){
    		system.debug('updateVendorInvoice ++');
    	    boolean bDisabled=false;
		    LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
		    if(lwSettings!=null)
		       if((lwSettings.Disable_All_Triggers__c==true ) )bDisabled=true;
		    if(bDisabled)return;
		   
		    //Variable Declaration and Initialization 
		    list<Counterparty_Invoice__c> listCINew=(list<Counterparty_Invoice__c>)trigger.New;
		    set<id>  setCIIds = new set<id>();  
		    
		    //putting the id of CI in set.
		    for(Counterparty_Invoice__c currCINew : listCINew ){ 
		           setCIIds.add(currCINew.Id); 
		    }
		    
		    //Fetching all vendor invoice whose CI is belog to setCIIds 
		    list<Vendor_Invoice__c> listVI =[select id 
		                                     from Vendor_Invoice__c 
		                                     where Counterparty_Invoice__c  in :setCIIds];
		    
		    //updating Vendor Invoice list
		    if(listVI.size() > 0)  update listVI;
		    system.debug('updateVendorInvoice --');
    }
    
    
    

}