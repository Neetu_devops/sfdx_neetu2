public class DepartmentalFlagsController {
    
   @auraEnabled
    public static List<Deal_Team__c> getDepartmentValues(Id earId ){
        List<Deal_Team__c> lstDealTeam = new List<Deal_Team__c>();
        for(Deal_Team__c dealTeam : [SELECT 
                                            Id,
                                            Complete__c,
                                            Comment__c,
                                            Marketing_Activity__c,
                                            Name 
                                        FROM 
                                            Deal_Team__c
                                        WHERE 
                                            Marketing_Activity__c =: earId
                                         AND Final_Notification__c = false
                                        ORDER BY
                                           Sort_Order__c]){
                                               
                                                   lstDealTeam.add(dealTeam);
                                                     
                                           } 
        return lstDealTeam;
    }
    
    @auraEnabled
    public static List<Deal_Team__c> saveDepartmentStatus(List<Deal_Team__c> lstDeptStatusRec){
        if(lstDeptStatusRec.size()>0)
            update lstDeptStatusRec;
        return lstDeptStatusRec;
    }
}