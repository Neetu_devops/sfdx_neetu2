/**
****************************************************************************************************************************************************************
* Class: CreatePayoutsController
* @author Created by Aman, Lease-Works, 
* @date 09/09/2020
* @version 1.0
* --------------------------------------------------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to create a Payout using quick action on Payable record page.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*	1.0       A.K.G                 17.08.2020      Initial Data & Payout creation
****************************************************************************************************************************************************************
*/
public class CreatePayoutsController {
    /**
    * @description: Check the callng sObject via recordId and fetches all the required data to render on the UI.
    *
    * @param recordId - String, contains record Id of the Object loading the CMP.
    *
    * @return PayableWrapper - Wrapper contianing required data for Invoice and Credit memo.
    */
    @AuraEnabled
    public static PayableWrapper fetchInitData(String recordId) {
        try {
            PayableWrapper payableWrap = new PayableWrapper();
            if (recordId != null && recordId != '') {
                for (Payable__c payable : [SELECT Id, Lease__c, Lease__r.Name, Lessee__c, Lessee__r.Name, Status__c,
                                            Payable_Type__c, Payable_Amount_Paid_Cash__c, Payable_Balance__c,company__c,claim__c,Supplemental_Rent__c, Cash_Security_Deposit__c
                                            FROM Payable__c 
                                            WHERE Id =: recordId]) {
                    payableWrap.payableRec = payable;
                    
                    if (payable.Lease__c != null) {
                        payableWrap.payoutRec.Lease__c = payable.Lease__c;
                        payableWrap.payoutRec.Lease__r = payable.Lease__r;
                    }
                    if (payable.Lessee__c != null) {
                        payableWrap.payoutRec.Lessee__c= payable.Lessee__c;
                        payableWrap.payoutRec.Lessee__r = payable.Lessee__r;
                    }
                    payableWrap.payoutRec.Payout_Date__c = System.today();
                    payableWrap.payoutRec.Payout_Type__c = 'Cash';
                    payableWrap.payoutRec.Status__c = 'Pending';
                    payableWrap.payoutRec.company__c = payable.company__c;
                    payableWrap.payoutRec.Payables_Type__c = payable.Payable_Type__c;
                    if('Security Deposit Refund'.equals(payable.Payable_Type__c)){
                        payableWrap.payoutRec.security_deposit__c = payable.Cash_Security_Deposit__c;
                    }
                    else if('MR Claim Event'.equals(payable.Payable_Type__c)){
                        payableWrap.payoutRec.claim__c = payable.claim__c;
                        payableWrap.payoutRec.Supplemental_Rent__c = payable.Supplemental_Rent__c;
                    }
                    break;
                }
            }
            return payableWrap;
        }
        catch (Exception ex) {
            System.debug(loggingLevel.ERROR,'Error: ' + ex.getMessage() + ' --- > ' + ex.getStackTraceString() + '@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }

    /**
    * @description: Creates the Payout and payable line item Record
    *
    * @param payableData - String, Wrapper object containing all the information entered by the user.
    *
    * @return null
    */
    @AuraEnabled
    public static void savePayout(String payableData) {
        try {
            PayableWrapper payWrap = (PayableWrapper) JSON.deserialize(payableData, PayableWrapper.class);
            System.debug(loggingLevel.DEBUG, 'payWrap: ' + payWrap);
           

            if (payWrap != null && payWrap.payableRec != null && payWrap.payoutRec != null) {
                if(payWrap.payableRec.Payable_Balance__c < payWrap.payoutRec.Payout_Amount__c ){
                    throw new AuraHandledException('Payout Amount is greater than Payable Balance');
                }
                if(payWrap.payableRec.status__c.equals('Pending')||(payWrap.payableRec.status__c.equals('Declined'))){
                    throw new AuraHandledException('Payouts can only be created on Approved Payables');
                }
                try{
                    insert payWrap.payoutRec;
                }
                catch(Exception ex){
                    System.debug(loggingLevel.ERROR,'Error: ' + ex.getMessage() + ' --- > ' + ex.getStackTraceString() + '@ Line: '+ex.getLineNumber());
                    throw new AuraHandledException(Utility.getErrorMessage(ex));
                }
                Payable_Line_Item__c pli = new Payable_Line_Item__c();
                pli.Payable__c = payWrap.payableRec.Id;
                pli.Payout__c = payWrap.payoutRec.Id;
                try{
                    insert pli;
                }
                catch(Exception ex){
                    System.debug(loggingLevel.ERROR,'Error: ' + ex.getMessage() + ' --- > ' + ex.getStackTraceString() + '@ Line: '+ex.getLineNumber());
                    throw new AuraHandledException(Utility.getErrorMessage(ex));
                }


            }
        }catch(Exception ex){
            System.debug(loggingLevel.ERROR,'Error: ' + ex.getMessage() + ' --- > ' + ex.getStackTraceString() + '@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
     }

    /**
    * @description: PayableWrapper to hold data related to Payables and Payouts 
    */
    public class PayableWrapper {
        @AuraEnabled public Payable__c payableRec { get; set; }
        @AuraEnabled public Payout__c payoutRec { get; set; }

        public PayableWrapper(){
            this.payableRec = new Payable__c();
            this.payoutRec = new Payout__c();
        }
    }
    
}