/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMarketingDeal {

    @isTest(SeeAllData=true)
    static void myUnitTest() {
       
        Deal__c myDeal = new Deal__c(Name = 'Test Deal');
        insert myDeal;
        
        Lease__c lease1 = [select id,name,aircraft__c,operator__c from Lease__c where name like '%TestSeededMSN2%' limit 1];
        
        Aircraft_In_Deal__c myACInDeal = new Aircraft_In_Deal__c(Marketing_Deal__c=myDeal.Id,Name='My AC in Deal',Aircraft__c=lease1.aircraft__c);
        LeaseWareUtils.clearFromTrigger();insert myACInDeal;

        try{
            Aircraft_In_Deal__c dupACInDeal = new Aircraft_In_Deal__c(Marketing_Deal__c=myDeal.Id,Name='My AC in Deal',Aircraft__c=myACInDeal.Aircraft__c);
            LeaseWareUtils.clearFromTrigger();insert dupACInDeal;
            
        }catch(exception e){
            system.debug('Expected - One AC can be added to the deal only once.'+e);
        }
        
        Deal_Proposal__c myDP = new Deal_Proposal__c(Deal__c = myDeal.id, Name = 'My proposal', Prospect__c=lease1.operator__c);
        LeaseWareUtils.clearFromTrigger();insert myDP;
        
        LeaseWareUtils.clearFromTrigger();update myDeal;
        LeaseWareUtils.clearFromTrigger();update myACInDeal;
        
        MSN_s_Of_Interest__c myMSNofInt;
        try{
            myMSNofInt = new MSN_s_Of_Interest__c(Name='x', Aircraft__c=myACInDeal.Id, Deal_Proposal__c=myDP.Id, 
            Rent_000__c=250000, Term_Mos__c=72, Investment_Required_000__c=3000000, Evaluation_Approach__c='IRR', 
            Upfront__c=true, End_Of_Lease__c=true, Amortize__c=true);
            LeaseWareUtils.clearFromTrigger();insert myMSNofInt;
            
            myMSNofInt.Evaluation_Approach__c='NPV';
            LeaseWareUtils.clearFromTrigger();update myMSNofInt;
        }catch(exception e){
            system.debug('Expected - One AC can be added to the deal only once.'+e);
        }


        MSN_s_Of_Interest__c myMSNofIntAutoAdded = [select id, Rent_000__c,
                            Term_Mos__c,
                            Investment_Required_000__c,
                            Evaluation_Approach__c,
                            Upfront__c,
                            End_Of_Lease__c,
                            Amortize__c from MSN_s_Of_Interest__c where Deal_Proposal__c=:myDP.Id limit 1];
        try{
            myMSNofIntAutoAdded.Rent_000__c=250000;
            myMSNofIntAutoAdded.Term_Mos__c=72;
            myMSNofIntAutoAdded.Investment_Required_000__c=3000000;
            myMSNofIntAutoAdded.Evaluation_Approach__c='IRR';
            myMSNofIntAutoAdded.Upfront__c=true;
            myMSNofIntAutoAdded.End_Of_Lease__c=true;
            myMSNofIntAutoAdded.Amortize__c=true;
            update myMSNofIntAutoAdded; 

            myMSNofIntAutoAdded.Evaluation_Approach__c='NPV';
            LeaseWareUtils.clearFromTrigger();update myMSNofIntAutoAdded;
            LeaseWareUtils.clearFromTrigger();delete myMSNofIntAutoAdded;
        }catch(exception e){
            system.debug('Unexpected. '+e);
        }


        MSN_s_Of_Interest__c myMSNofIntDup = new MSN_s_Of_Interest__c(Name='x', Aircraft__c=myACInDeal.Id, Deal_Proposal__c=myDP.Id);
        try{
            LeaseWareUtils.clearFromTrigger();insert myMSNofIntDup;
        }catch(exception e){
            system.debug('Expected' + e);
        }
        myMSNofIntDup = new MSN_s_Of_Interest__c(Name='x', Aircraft__c=myACInDeal.Id, Deal_Proposal__c=[select Id from Deal_Proposal__c limit 1].Id);
        try{
            LeaseWareUtils.clearFromTrigger();insert myMSNofIntDup;
        }catch(exception e){
            system.debug('Expected' + e);
        }

        Deal_Propsoal_Activity__c myDPAct = new Deal_Propsoal_Activity__c(Name='x', Deal_Proposal__c=myDP.Id, Deal_Proposal_Comments__c='Some Comment');
        LeaseWareUtils.clearFromTrigger();insert myDPAct;
        
        
        try{
            LeaseWareUtils.clearFromTrigger();delete myACInDeal;
        }catch(exception e){
            system.debug('Delete might fail if insert went into exception due to expected issues. Nevermind!'+e);
        }
        try{
            LeaseWareUtils.clearFromTrigger();delete myDPAct;
        }catch(exception e){
            system.debug('Delete might fail if insert went into exception due to expected issues. Nevermind!'+e);
        }
        try{
            LeaseWareUtils.clearFromTrigger();delete myMSNofInt;
        }catch(exception e){
            system.debug('Delete might fail if insert went into exception due to expected issues. Nevermind!'+e);
        }
        
    }
    
    static testMethod void testAwardedprospectMSNList() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        // Add 3 aircraft
        mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft1 = (string) mapInOut.get('Aircraft__c.Id');
        Test.startTest();
        mapInOut.put('Aircraft__c.msn','MSN2');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft2 = (string) mapInOut.get('Aircraft__c.Id');
//        mapInOut.put('Aircraft__c.msn','MSN3');     
//        TestLeaseworkUtil.insertAircraft(mapInOut); 
//        Id Aircraft3 =(string)  mapInOut.get('Aircraft__c.Id');
        
        // Create Campaign      
        LeaseWareUtils.clearFromTrigger();  
        TestLeaseworkUtil.createDeal(mapInOut);
        
        // Create Aircraft in Campaign
        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft1);
        mapInOut.put('Aircraft_In_Deal__c.Marketing_Deal__c',(String)mapInOut.get('Deal__c.Id'));
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        
        Id AICId1 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
	    
        
        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft2);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
         
        Id AICId2 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
//        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft3);
//        LeaseWareUtils.clearFromTrigger();
//        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
//        Id AICId3 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId = (String)mapInOut.get('Operator__c.Id');
        
        // Crete Prospect
        mapInOut.put('Deal_Proposal__c.Deal__c',(String)mapInOut.get('Deal__c.Id'));
        mapInOut.put('Deal_Proposal__c.Prospect__c',OperatorId);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createDealProsposal(mapInOut); 
       
        // Create Prospect Term : automatic creation now onwards March 2018
      
        System.debug('Prospect Id'+(String) mapInOut.get('Deal_Proposal__c.Id'));
        System.debug('Aircraft in Camp Id'+ AICId1);
		Test.stopTest();
        MSN_s_Of_Interest__c[] listMSNOfInt = [select id from MSN_s_Of_Interest__c where Deal_Proposal__c = :(String) mapInOut.get('Deal_Proposal__c.Id')
        									and (Aircraft__c =:AICId1 or Aircraft__c =:AICId2 ) ];
        for(MSN_s_Of_Interest__c currec:listMSNOfInt){									
        	currec.Awarded__c = true; // Awarding MSN1
        }
        LeaseWareUtils.clearFromTrigger();
        update listMSNOfInt;        
        
       
        // Validate MSN list in Prospect
        
        Deal_Proposal__c[] fetchedProspect = [select  id,MSN__c from Deal_Proposal__c where Id = :(String) mapInOut.get('Deal_Proposal__c.Id')  ];
        // insert Utilization_Report__c
        
        //System.debug(fetchedProspect[0].MSN__c);
        system.assertEquals(fetchedProspect[0].MSN__c.contains('MSN1'), true) ;
        system.assertEquals(fetchedProspect[0].MSN__c.contains('MSN2'), true) ;
    }
    static testMethod void testAwardedprospectMSNListDelete() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        // Add 3 aircraft
        mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft1 = (string) mapInOut.get('Aircraft__c.Id');
        Test.StartTest();
        mapInOut.put('Aircraft__c.msn','MSN2');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft2 = (string) mapInOut.get('Aircraft__c.Id');
//        mapInOut.put('Aircraft__c.msn','MSN3');     
//        TestLeaseworkUtil.insertAircraft(mapInOut); 
//        Id Aircraft3 =(string)  mapInOut.get('Aircraft__c.Id');
        
        // Create Campaign        
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createDeal(mapInOut);
        
        // Create Aircraft in Campaign
        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft1);
        mapInOut.put('Aircraft_In_Deal__c.Marketing_Deal__c',(String)mapInOut.get('Deal__c.Id'));
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        
        Id AICId1 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');

        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft2);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        Id AICId2 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
//        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft3);
//        LeaseWareUtils.clearFromTrigger();
//        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
//        Id AICId3 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId = (String)mapInOut.get('Operator__c.Id');
        
        // Crete Prospect
        mapInOut.put('Deal_Proposal__c.Deal__c',(String)mapInOut.get('Deal__c.Id'));
        mapInOut.put('Deal_Proposal__c.Prospect__c',OperatorId);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createDealProsposal(mapInOut); 
        
        // Create Prospect Term
        
        System.debug('Prospect Id'+(String) mapInOut.get('Deal_Proposal__c.Id'));
        System.debug('Aircraft in Camp Id'+ AICId1);
        Test.StopTest();
        MSN_s_Of_Interest__c[] listMSNOfInt = [select id from MSN_s_Of_Interest__c where Deal_Proposal__c = :(String) mapInOut.get('Deal_Proposal__c.Id')
        									and (Aircraft__c =:AICId1 or Aircraft__c =:AICId2 ) ];
        for(MSN_s_Of_Interest__c currec:listMSNOfInt){									
        	currec.Awarded__c = true; // Awarding MSN1
        }
        LeaseWareUtils.clearFromTrigger();
        update listMSNOfInt;   
        
//        mapInOut.put('MSN_s_Of_Interest__c.Aircraft__c',AICId3);
//        LeaseWareUtils.clearFromTrigger();
//        TestLeaseworkUtil.createMSN(mapInOut); 
//        MSN_s_Of_Interest__c M3 = (MSN_s_Of_Interest__c)mapInOut.get('MSN_s_Of_Interest__c'); 
         
//        LeaseWareUtils.unsetTriggers('MSNofInterestNoDupsAndOnlyValidAC'); 
//        LeaseWareUtils.clearFromTrigger();
//        delete M3;

        // Validate MSN list in Prospect
        Deal_Proposal__c[] fetchedProspect = [select  id,MSN__c from Deal_Proposal__c where Id = :(String) mapInOut.get('Deal_Proposal__c.Id')  ];

        system.assertEquals(fetchedProspect[0].MSN__c.contains('MSN1'), true) ;
        system.assertEquals(fetchedProspect[0].MSN__c.contains('MSN2'), true) ; 
                
        LeaseWareUtils.unsetTriggers('MSNofInterestNoDupsAndOnlyValidAC');
        LeaseWareUtils.clearFromTrigger();
        delete listMSNOfInt[0];
        LeaseWareUtils.unsetTriggers('MSNofInterestNoDupsAndOnlyValidAC');
        LeaseWareUtils.clearFromTrigger();
        delete listMSNOfInt[1];
        fetchedProspect = [select  id,MSN__c from Deal_Proposal__c where Id = :(String) mapInOut.get('Deal_Proposal__c.Id')  ];
        system.assertEquals(true ,fetchedProspect[0].MSN__c == null || ''.equals(fetchedProspect[0].MSN__c)) ;
        
        // insert Utilization_Report__c
       
        //System.debug(fetchedProspect[0].MSN__c);
        
    }    
    static testMethod void testAwardedprospectMSNListBulkInsert() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        // Add 3 aircraft
        mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft1 = (string) mapInOut.get('Aircraft__c.Id');
        Test.StartTest();
        mapInOut.put('Aircraft__c.msn','MSN2');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft2 = (string) mapInOut.get('Aircraft__c.Id');
//        mapInOut.put('Aircraft__c.msn','MSN3');     
//        TestLeaseworkUtil.insertAircraft(mapInOut); 
//        Id Aircraft3 =(string)  mapInOut.get('Aircraft__c.Id');
        
        // Create Campaign        
        TestLeaseworkUtil.createDeal(mapInOut);
        
        // Create Aircraft in Campaign
        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft1);
        mapInOut.put('Aircraft_In_Deal__c.Marketing_Deal__c',(String)mapInOut.get('Deal__c.Id'));
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        
        Id AICId1 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');

        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft2);
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        Id AICId2 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
//        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft3);
//        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
//        Id AICId3 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId = (String)mapInOut.get('Operator__c.Id');
        
        // Create Prospect
        mapInOut.put('Deal_Proposal__c.Deal__c',(String)mapInOut.get('Deal__c.Id'));
        mapInOut.put('Deal_Proposal__c.Prospect__c',OperatorId);
        TestLeaseworkUtil.createDealProsposal(mapInOut); 
        Id ProspectId1 = (String) mapInOut.get('Deal_Proposal__c.Id') ;
        // Create Prospect Term
        Test.StopTest();
        MSN_s_Of_Interest__c[] listMSNOfInt = [select id from MSN_s_Of_Interest__c where Deal_Proposal__c = :(String) mapInOut.get('Deal_Proposal__c.Id')
        									and Aircraft__c =:AICId1];
        listMSNOfInt[0].Awarded__c = true; // Awarding MSN1
        LeaseWareUtils.clearFromTrigger();
        update listMSNOfInt[0];
        

        // Validate MSN list in Prospect
        Deal_Proposal__c[] fetchedProspect = [select  id,MSN__c from Deal_Proposal__c where Id = :(String) mapInOut.get('Deal_Proposal__c.Id')  ];
        // insert Utilization_Report__c
        
        System.debug(fetchedProspect[0].MSN__c);
        system.assertEquals(fetchedProspect[0].MSN__c.contains('MSN1')  , true) ;
        
    }    
    static testMethod void testAwardedprospectMSNListBulkDelete() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        // Add 3 aircraft
        mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft1 = (string) mapInOut.get('Aircraft__c.Id');
        Test.StartTest();
        mapInOut.put('Aircraft__c.msn','MSN2');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft2 = (string) mapInOut.get('Aircraft__c.Id');
//        mapInOut.put('Aircraft__c.msn','MSN3');     
//        TestLeaseworkUtil.insertAircraft(mapInOut); 
//        Id Aircraft3 =(string)  mapInOut.get('Aircraft__c.Id');
        
        // Create Campaign
        LeaseWareUtils.clearFromTrigger();        
        TestLeaseworkUtil.createDeal(mapInOut);
        
        // Create Aircraft in Campaign
        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft1);
        mapInOut.put('Aircraft_In_Deal__c.Marketing_Deal__c',(String)mapInOut.get('Deal__c.Id'));
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        
        Id AICId1 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');

        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft2);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        Id AICId2 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
//        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft3);
//        LeaseWareUtils.clearFromTrigger();
//        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
//        Id AICId3 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId = (String)mapInOut.get('Operator__c.Id');
        
        // Crete Prospect
        mapInOut.put('Deal_Proposal__c.Deal__c',(String)mapInOut.get('Deal__c.Id'));
        mapInOut.put('Deal_Proposal__c.Prospect__c',OperatorId);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createDealProsposal(mapInOut); 
        
        Id ProspectId1 = (String) mapInOut.get('Deal_Proposal__c.Id') ;
        // Create Prospect Term
        Test.StopTest();
        MSN_s_Of_Interest__c[] listMSNOfInt = [select id from MSN_s_Of_Interest__c where Deal_Proposal__c = :(String) mapInOut.get('Deal_Proposal__c.Id')
        									and (Aircraft__c =:AICId1 or Aircraft__c =:AICId2 ) ];
        for(MSN_s_Of_Interest__c currec:listMSNOfInt){									
        	currec.Awarded__c = true; // Awarding MSN1
        }
        LeaseWareUtils.clearFromTrigger();
        update listMSNOfInt;         
        

 
        LeaseWareUtils.unsetTriggers('MSNofInterestNoDupsAndOnlyValidAC');
        LeaseWareUtils.clearFromTrigger();
        delete listMSNOfInt;
        
        // Validate MSN list in Prospect
        Deal_Proposal__c[] fetchedProspect = [select  id,MSN__c from Deal_Proposal__c where Id = :(String) mapInOut.get('Deal_Proposal__c.Id')  ];
        // insert Utilization_Report__c
        
        //System.debug(fetchedProspect[0].MSN__c);
        system.assertEquals(true ,fetchedProspect[0].MSN__c == null || ''.equals(fetchedProspect[0].MSN__c)) ;
    } 
  
    static testMethod void testAwardedprospectMSNListBulkUpdate() {
       
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createLessor(mapInOut);
        // Add 3 aircraft
        mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft1 = (string) mapInOut.get('Aircraft__c.Id');
        Test.StartTest();
        mapInOut.put('Aircraft__c.msn','MSN2');
        TestLeaseworkUtil.insertAircraft(mapInOut); 
        Id Aircraft2 = (string) mapInOut.get('Aircraft__c.Id');
//        mapInOut.put('Aircraft__c.msn','MSN3');     
//        TestLeaseworkUtil.insertAircraft(mapInOut); 
//        Id Aircraft3 =(string)  mapInOut.get('Aircraft__c.Id');
        
        // Create Campaign    
        LeaseWareUtils.clearFromTrigger();    
        TestLeaseworkUtil.createDeal(mapInOut);
        
        // Create Aircraft in Campaign
        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft1);
        mapInOut.put('Aircraft_In_Deal__c.Marketing_Deal__c',(String)mapInOut.get('Deal__c.Id'));
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        
        Id AICId1 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');

        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft2);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
        
        Id AICId2 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
//        mapInOut.put('Aircraft_In_Deal__c.Aircraft__c',Aircraft3);
//        LeaseWareUtils.clearFromTrigger();
//        TestLeaseworkUtil.createAircraftInDeal(mapInOut);
//        Id AICId3 = (String) mapInOut.get('Aircraft_In_Deal__c.Id');
        
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId = (String)mapInOut.get('Operator__c.Id');
        
        // Crete Prospect
        mapInOut.put('Deal_Proposal__c.Deal__c',(String)mapInOut.get('Deal__c.Id'));
        mapInOut.put('Deal_Proposal__c.Prospect__c',OperatorId);
        LeaseWareUtils.clearFromTrigger();
        TestLeaseworkUtil.createDealProsposal(mapInOut); 
        Id ProspectId1 = (String) mapInOut.get('Deal_Proposal__c.Id') ;
        // Create Prospect Term
        Test.StopTest();
        MSN_s_Of_Interest__c[] listMSNOfInt = [select id from MSN_s_Of_Interest__c where Deal_Proposal__c = :(String) mapInOut.get('Deal_Proposal__c.Id')
        									and (Aircraft__c =:AICId1 or Aircraft__c =:AICId2 ) ];
        for(MSN_s_Of_Interest__c currec:listMSNOfInt){									
        	currec.Awarded__c = true; // Awarding MSN1
        }
        LeaseWareUtils.clearFromTrigger();
        update listMSNOfInt; 
        
        for(MSN_s_Of_Interest__c rec:listMSNOfInt){
            rec.Awarded__c = false;
        } 
        LeaseWareUtils.unsetTriggers('MSNofInterestNoDupsAndOnlyValidAC');
        LeaseWareUtils.clearFromTrigger();
        update listMSNOfInt;
        
        // Validate MSN list in Prospect
        Deal_Proposal__c[] fetchedProspect = [select  id,MSN__c from Deal_Proposal__c where Id = :(String) mapInOut.get('Deal_Proposal__c.Id')  ];
        system.assertEquals(null ,fetchedProspect[0].MSN__c) ;
        
        // Update back to true
        for(MSN_s_Of_Interest__c rec:listMSNOfInt){
            rec.Awarded__c = true;
        } 
        LeaseWareUtils.unsetTriggers('MSNofInterestNoDupsAndOnlyValidAC');
        LeaseWareUtils.clearFromTrigger();
        update listMSNOfInt;
        
        // Validate MSN list in Prospect
        fetchedProspect = [select  id,MSN__c from Deal_Proposal__c where Id = :(String) mapInOut.get('Deal_Proposal__c.Id')  ];

        system.assertEquals(fetchedProspect[0].MSN__c.contains('MSN1'), true) ;
        system.assertEquals(fetchedProspect[0].MSN__c.contains('MSN2'), true) ;        
        
       
        
    }     
}