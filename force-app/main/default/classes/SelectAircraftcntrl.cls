public with sharing class SelectAircraftcntrl {    
  
    @AuraEnabled
    public static ResultWrapper saveProject(Id projectId, List<ID> lstAircraft, List<ID> prospects){
    
        Savepoint sp = Database.setSavepoint();
        
        try{
            
            Integer countOfNewMA = 0;
            Integer countOfExistingMA = 0;
            
            //existing marketing activities that are going to be there.
            Set<Id> retainedMarketingActivities = new Set<Id>();
            
            List<Project__c> projectList = [select id,Inactive__c, Asset_Type__c, Project_Deal_Type__c from project__c where Id =: projectId];

            if(projectList.size()>0){
            

                Id activityRecordTypeId;
                Id asssetDetailRecordTypeId;
                
                // get record type ids for marketing activity and asset detail from configuration.
                                
                List<Project_Configuration__mdt> configurationList = [SELECT Marketing_Activity_Record_Type__c,Asset_Type__c,
                                                                      Deal_Status__c,Marketing_Rep__c,
                                                                      Marketing_Activity_Deal_Type__c
                                                                      FROM Project_Configuration__mdt
                                                                      WHERE Deal_Type__c =: projectList[0].Project_Deal_Type__c];
               
               
                if(configurationList.size()>0){
                
                    if(Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByDeveloperName().containsKey(configurationList[0].Marketing_Activity_Record_Type__c)){
                        activityRecordTypeId = Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByDeveloperName().get(configurationList[0].Marketing_Activity_Record_Type__c).getRecordTypeId();
                    }else{
                        return new ResultWrapper('Error','Deal record type is missing or incorrectly configured.');
                    }
                
                    if(Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByDeveloperName().containsKey(configurationList[0].Marketing_Activity_Record_Type__c)){
                        asssetDetailRecordTypeId = Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByDeveloperName().get(configurationList[0].Marketing_Activity_Record_Type__c).getRecordTypeId();
                    }else{
                        return new ResultWrapper('Error','Asset Detail record type is missing or incorrectly configured.');
                    }
                    
                }else{
                
                     return new ResultWrapper('Error', 'Missing Campaign Configuration metadata for Deal Type ' + projectList[0].Project_Deal_Type__c + '.');
                }
            
            
                Map<Id,Marketing_Activity__c> marketingActivityMap = new Map<Id,Marketing_Activity__c>();
                Map<Id,Set<Id>> prospectAircraftMap = new Map<Id,Set<Id>>();
                
                
                List<Marketing_Activity__c> marketingActivityList = [select id,name,prospect_Id__c,Campaign__c, Inactive_MA__c,
                                                                        (Select id, aircraft__c from Aircraft_Proposals__r) 
                                                                        from Marketing_Activity__c 
                                                                        where Campaign__c =: projectId
                                                                        and prospect_Id__c != null 
                                                                        and deal_type__c =: configurationList[0].Marketing_Activity_Deal_Type__c
                                                                        and recordtype.developerName =: configurationList[0].Marketing_Activity_Record_Type__c];

                for(Marketing_Activity__c activity : marketingActivityList){
                     
                    marketingActivityMap.put(activity.prospect_Id__c, activity); 
    
                    for(Aircraft_Proposal__c assetDetail : activity.Aircraft_Proposals__r){
                    
                        if(!prospectAircraftMap.containsKey(activity.prospect_Id__c)){
                            prospectAircraftMap.put(activity.prospect_Id__c, new Set<Id>());
                        }
                        prospectAircraftMap.get(activity.prospect_Id__c).add(assetDetail.aircraft__c);
                    }    
                }

                List<Aircraft_Proposal__c> assetDetailList = new List<Aircraft_Proposal__c>();
                Map<Id,Marketing_Activity__c> marketingActivitiesToBeCreated = new Map<Id,Marketing_Activity__c>();
                Map<Id,Aircraft__c> aircraftMap = new Map<Id, Aircraft__c>([select id, New_Used__c from aircraft__c where New_Used__c != null
                                                                                and id in :lstAircraft]);
                
                
                
                for(Id selectedId : prospects){
                
                    //check if any existing maketing activity there for the prospect or not.
                    if(!marketingActivityMap.containsKey(selectedId)){
                    
                        Marketing_Activity__c activity = new Marketing_Activity__c();                        
                        activity.Created_from_Project__c =  true;
                        activity.campaign__c = projectId;
                        activity.Inactive_MA__c = projectList[0].Inactive__c;
                        
                        // check if it is airline or lessor.
                        
                        if(selectedId.getSObjectType().getDescribe().getName().toLowercase() == String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'operator__c').toLowercase()){
                            activity.Prospect__c =  selectedId;
                        }else if(selectedId.getSObjectType().getDescribe().getName().toLowercase() == String.valueOf(LeaseWareUtils.getNamespacePrefix() + 'counterparty__c').toLowercase()){
                            activity.Lessor__c =  selectedId;
                        }else{
                            activity.counterparty__c = selectedId; 
                        }    
                        
                        //set MA values from configuration according to the campaign deal type.
                             
                        activity.Deal_Type__c = configurationList[0].Marketing_Activity_Deal_Type__c;
                        activity.Deal_Status__c = configurationList[0].Deal_Status__c;
                        
                        //populate asset type.
                        if(projectList[0].Asset_Type__c != null){
                            activity.Asset_Type__c = projectList[0].Asset_Type__c;
                        }else{
                            activity.Asset_Type__c = configurationList[0].Asset_Type__c;
                        }
                        
                        activity.Marketing_Rep_c__c = configurationList[0].Marketing_Rep__c;
                            
                        
                        if(activityRecordTypeId != null)
                            activity.RecordTypeId = activityRecordTypeId;
                        
                        marketingActivitiesToBeCreated.put(selectedId, activity);
                        
                        prospectAircraftMap.put(selectedId, new Set<Id>());
                        
                        countOfNewMA += 1;
                    
                    }else{
                    
                        Marketing_Activity__c activity = marketingActivityMap.get(selectedId);
                        
                        if(activity.campaign__c != projectId || activity.Inactive_MA__c != projectList[0].Inactive__c){
                            activity.campaign__c = projectId;
                            activity.Inactive_MA__c = projectList[0].Inactive__c;
                            marketingActivitiesToBeCreated.put(selectedId, activity);
                        }
                        
                        retainedMarketingActivities.add(activity.Id);
                        countOfExistingMA += 1;
                    }
                    
                    Set<Id> existingAircraftIds = prospectAircraftMap.get(selectedId);
                    
                    for(Id aircraftId : lstAircraft){
                      
                        //check if asset detail exists or not.
                        if(existingAircraftIds == null || !existingAircraftIds.contains(aircraftId)){
            
                            //if not exists then create asset detail with marketing activity.
                            Aircraft_Proposal__c assetDetail = new Aircraft_Proposal__c ();
                            
                            //if MA newly created then take its reference otherwise use existing one.
                           
                            if(marketingActivitiesToBeCreated.containsKey(selectedId) && marketingActivitiesToBeCreated.get(selectedId).Id == null){
                                assetDetail.Marketing_Activity__r = marketingActivitiesToBeCreated.get(selectedId);
                            }else{
                                assetDetail.Marketing_Activity__c = marketingActivityMap.get(selectedId).Id;
                            }

                            assetDetail.Aircraft__c = aircraftId;
                            assetDetail.Created_from_Project__c =  true;
                            //assetDetail.deal_type__c = configurationList[0].Marketing_Activity_Deal_Type__c;
                            
                            if(asssetDetailRecordTypeId != null){
                                assetDetail.RecordTypeId = asssetDetailRecordTypeId;
                            }
                            
                            //set aircraft based fields.
                            if(aircraftMap.containskey(aircraftId)){
                                assetDetail.New_Used__c = aircraftMap.get(aircraftId).New_Used__c;
                            }
                                
                            assetDetailList.add(assetDetail);
                        }
                    }
                } 
                //To Stop the Summary Calculation From Trigger
                LeaseWareUtils.setFromTrigger('StopSummaryCalculation');
                
                //delete asset details.
                List<Aircraft_Proposal__c> deletingAssetDetails = [select id from Aircraft_proposal__c 
                                                                      where marketing_activity__c in : retainedMarketingActivities 
                                                                      and aircraft__c Not In : lstAircraft];
                                                                      
                if(deletingAssetDetails.size()>0){
                    delete deletingAssetDetails;
                }
                                   
                LeaseWareUtils.clearFromTrigger();
                
                //To Stop the Summary Calculation From Trigger
                LeaseWareUtils.setFromTrigger('StopSummaryCalculation');

                //create marketing activities for airlines and lessors.
                if(marketingActivitiesToBeCreated.size()>0){
                
                    upsert marketingActivitiesToBeCreated.values();
                    
                    //collect overall associate deals ids.
                    for(Marketing_Activity__c activity : marketingActivitiesToBeCreated.values()){
                        retainedMarketingActivities.add(activity.Id);
                }
                }
                 
                if(assetDetailList.size()>0){ 
                
                    for(Aircraft_Proposal__c assetDetail : assetDetailList){
                        if(assetDetail.Id == null && assetDetail.Marketing_Activity__c == null){
                            assetDetail.Marketing_Activity__c = assetDetail.Marketing_Activity__r.Id;
                    }
                    }
                    
                    insert assetDetailList;
                }           
                
                LeaseWareUtils.unsetTriggers('StopSummaryCalculation');
            } 
            
            ResultWrapper result = new ResultWrapper('Success', '');
            result.existingDeals = countOfExistingMA;
            result.newDeals = countOfNewMA;
            result.associatedDeals = retainedMarketingActivities;
            return result ;  
            
          }catch(System.DmlException exp){
            
            Database.rollback( sp );
             
            String error = '';
            Integer count = 1;
            Set<String> errorSet = new Set<String>();
            for (Integer i = 0; i < exp.getNumDml(); i++) {
                String errorMsg = exp.getDmlMessage(i)+' - '+exp.getDmlFieldNames(i);
                if(!errorSet.contains(errorMsg)){
                    error += '[ERROR : ' + count +'] ' + exp.getDmlMessage(i)+' - '+exp.getDmlFieldNames(i) + ' ; ';
                    count++;
                    errorSet.add(errorMsg);
                }
            }
            
            return new ResultWrapper('Error', error ); 
            
         }catch (Exception exp) {
             return new ResultWrapper('Error',  exp.getStackTraceString() + ', ' + exp.getMessage()); 
         } 
    }
   
    // to delete or detach the deals from campaign as per the user input.
    @AuraEnabled 
    public static void detachDeals(Id campaignId, List<Id> associatedDeals, String answer){
   
        List<Marketing_Activity__c> deletingMarketingActivities = [select id from Marketing_Activity__c 
                                                                    where Campaign__c =: campaignId
                                                                    and Id Not in : associatedDeals];                                           

    
        //delete marketing activities
        if(deletingMarketingActivities.size()>0){
            LeaseWareUtils.setFromTrigger('StopSummaryCalculation');
            if(answer == 'Yes'){
            
                delete deletingMarketingActivities;
               
            }else if(answer == 'No'){
                
                for(Marketing_Activity__c marketingActivity : deletingMarketingActivities){
                    marketingActivity.Campaign__c = null;
                }
                
                update deletingMarketingActivities;
            }  
            LeaseWareUtils.unsetTriggers('StopSummaryCalculation');
                }
        
        //to calculate summaries.
        calculateSummary(campaignId);
    }
    
    @auraEnabled 
    public static void calculateSummary(Id campaignId){
        Utility.calculateBidSummary(new Set<Id>{campaignId});
    }
               
                
    @auraEnabled 
    public static void deleteProjectRecord(Id projectId, String answer){
            
        if(projectId != null){
      
            if(answer == 'Yes'){
     
                Delete [select id from Marketing_Activity__c where campaign__c =: projectId];
    
            }
        
            Project__c prjObj = new Project__c (Id = projectId);
            delete prjObj;
            }  
        }    
      
    @auraEnabled
    public static ProjectDataWrapper getData(Id projectId){
    
        
        ProjectDataWrapper dataWrapper = new ProjectDataWrapper();        
        dataWrapper.layoutSectionList =  explorLayout();
        
        string linkState = LeaseWareUtils.getLWSetup_CS('SHOW_OPERATOR_FINDER_LINK');
        if('ON'.equalsIgnoreCase(linkState)){
            dataWrapper.showOperatorFinderLink = true;
        }

        
        if(projectId != null){
        
            Set<Id> lessorsIds =  new Set<Id>();
            Set<Id> airlineIds = new Set<Id>();
            Set<Id> counterpartyIds = new Set<Id>();
            
            
            for(Marketing_Activity__c activity : [select Counterparty__c, lessor__c, Prospect__c
                                                     from Marketing_Activity__c
                                                     where campaign__c =: projectId]){
                                                     
                if(activity.prospect__c != null){
                    airlineIds.add(activity.prospect__c);
                }
                
                if(activity.lessor__c != null){
                    lessorsIds.add(activity.lessor__c);
                }
                
                if(activity .Counterparty__c != null){
                    counterpartyIds.add(activity.Counterparty__c);
                }
            }
                
            
            dataWrapper.aircrafts = LeaseWareUtils.getCampaignAssets(projectId);
            
            if(airlineIds.size()>0){
                dataWrapper.airlines = [select id, name from operator__c where id in : airlineIds order by Name];
            } 
             
            if(lessorsIds.size()>0){
                dataWrapper.lessors = [select id, name from counterParty__c where id in : lessorsIds order by Name];
            } 
            
            if(counterpartyIds.size()>0){
                dataWrapper.counterparties = [select id, name from bank__c where id in : counterpartyIds order by Name];
            }    
        }   
        
        //Get Custom Setting for Campaign
       
        LW_Setup__c selectedProspectTypes = LW_Setup__c.getInstance('Prospect Types');
        if(selectedProspectTypes != null && selectedProspectTypes.Value__c != null){
            dataWrapper.selectedProspect = selectedProspectTypes.Value__c;
        }
        
        LW_Setup__c tradingProfileFeature = LW_Setup__c.getInstance('Trading Profile Feature');
        if(tradingProfileFeature != null && tradingProfileFeature.Value__c != null && tradingProfileFeature.Value__c == '1'){
            dataWrapper.tradingProfileFeature = true;
        }
       return dataWrapper ;
    }
    
    @auraEnabled
    public static List<LayoutSection> explorLayout(){
        
        
        List<LayoutSection> lstSections = new List<LayoutSection>();
    
        List<Metadata.Metadata> layouts =  Metadata.Operations.retrieve(Metadata.MetadataType.Layout, 
                                                 new List<String> {leasewareutils.getNamespacePrefix() + 'Project__c-' + leasewareutils.getNamespacePrefix() + 'Project Layout'});
                                                 
        if(layouts != null && layouts.size()>0){
        
            Metadata.Layout layoutMd = (Metadata.Layout)layouts.get(0);
            
            for( Metadata.LayoutSection ls : layoutMd.layoutSections ) {
                
                if(ls.editHeading){
                
                    LayoutSection section = new LayoutSection( ls.label, ls.layoutColumns.size() );
                          
                    List<LayoutColumn> lstColumns = new List<LayoutColumn>();
                    Integer maxFieldsInColumn = 0;
                    
                    for( Metadata.LayoutColumn lc : ls.layoutColumns ) {
                        
                        LayoutColumn column = new LayoutColumn();
                        // check if there are fields available in that column
                        if( lc.layoutItems != null ) { 
                            // Get the max number of fields in a column to preserve the alignment 
                            if( maxFieldsInColumn < lc.layoutItems.size() ) {
                                maxFieldsInColumn = lc.layoutItems.size();
                            }
                            for( Metadata.LayoutItem li : lc.layoutItems ) {
                                
                                // Pass the LayoutItem object in the LayoutField consturctor        
                                column.lstFields.add( new LayoutField( li ) );
                            }
                        }
                        // No need to add a column in the section if there is no field available 
                        if( column.lstFields.size() > 0 ) {
                            lstColumns.add( column );
                        }
                    }
                    
                    // Now, we need to arrange the fields in section so we can use them in the iteration 
                    // on the component so we will have to arrange them in the order 
                    if( maxFieldsInColumn > 0 ) {
                        for( Integer i = 0; i < maxFieldsInColumn; i++ ) {
                            for( Integer j = 0; j < lstColumns.size(); j++ ){
                                if( lstColumns[j].lstFields.size() > i ) {
                                    section.lstFields.add( lstColumns[j].lstFields[i] );    
                                }    
                                else {
                                    section.lstFields.add( new LayoutField() );
                                }
                            }    
                        }    
                    }
                    
                    lstSections.add( section );
                }    
            }
        }
        
        return lstSections ;
    }
    
    public class ProjectDataWrapper {
    	@auraEnabled public Boolean showOperatorFinderLink{get; set;}
        @auraEnabled public List<Aircraft__c> aircrafts{get; set;}
        @auraEnabled public List<Operator__c> airlines {get;set;}
        @auraEnabled public List<CounterParty__c> lessors {get;set;}
        @auraEnabled public List<Bank__c> counterparties{get;set;}
        @auraEnabled public List<LayoutSection> layoutSectionList {get;set;}
        @auraEnabled public String selectedProspect {get;set;}
        @auraEnabled public Boolean tradingProfileFeature {get;set;}
        
        
        
        
        public ProjectDataWrapper(){
            aircrafts = new List<Aircraft__c>();
            airlines = new List<Operator__c>();
            lessors = new List<CounterParty__c>();
            counterparties = new List<Bank__c>();
        }
    } 
    
    public class ResultWrapper {
    
        @auraEnabled public String status {get;set;}
        @auraEnabled public String message {get;set;}
        @auraEnabled public Integer existingDeals {get;set;}
        @auraEnabled public Integer newDeals {get;set;}
        @auraEnabled public Set<Id> associatedDeals {get;set;}
        
        public ResultWrapper(String status, String message){
            this.status = status;
            this.message = message;
            associatedDeals = new Set<Id>();
        }
    }
 
    public class LayoutSection { 
      
        @AuraEnabled public String label;
        @AuraEnabled public List<LayoutField> lstFields;
        @AuraEnabled public Integer totalColumns;
        public LayoutSection( String label, Integer totalColumns ) {
        this.label = label;
            this.totalColumns = totalColumns;
            this.lstFields = new List<LayoutField>();
        }
    }
    
    private class LayoutColumn {
        private List<LayoutField> lstFields;    
        public LayoutColumn() {
            this.lstFields = new List<LayoutField>();
        }
    }
    
    public class LayoutField {
    
        @AuraEnabled public String fieldName;
        @AuraEnabled public Boolean isRequired;
        @AuraEnabled public Boolean isReadOnly;
        
        public LayoutField() {}
        
        public LayoutField( Metadata.LayoutItem li ) {
            
            this.fieldName = li.field;
            if( li.behavior == Metadata.UiBehavior.Required ) {
                this.isRequired = true;
            }
            else if( li.behavior == Metadata.UiBehavior.ReadOnly ) {
                this.isReadOnly = true;
            }    
        }
    }
  
}