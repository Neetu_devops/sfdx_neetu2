public class EquipmentTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'EquipmentTriggerHandlerBefore';
    private final string triggerAfter = 'EquipmentTriggerHandlerAfter';

	private static list<Equipment_DB__c> listEquipmentDB;
	private static map<string, Equipment_DB__c> mapEquipmentDBByPartNo;
	private static map<string, Equipment_DB__c> mapEquipmentDBByCode;
	private static map<string, Equipment_DB__c> mapEquipmentDBByFIN;

    public EquipmentTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        if(listEquipmentDB==null){
        	listEquipmentDB=new list<Equipment_DB__c>([select id, Name, ATA_Chapter__c, ATA_Chapter_Name__c, Code__c, Description__c, 
        		Family__c, FIN__c, Remark__c, Vendor__c, Zone__c
        		from Equipment_DB__c]);
        	mapEquipmentDBByPartNo = new map<string, Equipment_DB__c>();
        	mapEquipmentDBByCode = new map<string, Equipment_DB__c>();
        	mapEquipmentDBByFIN = new map<string, Equipment_DB__c>();
        }
        for(Equipment_DB__c curEq : listEquipmentDB){
        	mapEquipmentDBByPartNo.put(curEq.Name, curEq);
        	if(curEq.Code__c != null)mapEquipmentDBByCode.put(curEq.Code__c, curEq);
        	if(curEq.FIN__c != null)mapEquipmentDBByFIN.put(curEq.FIN__c, curEq);
        }
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('EquipmentTriggerHandler.beforeInsert(+)');
        EquipmentBefInsUpd();
        system.debug('EquipmentTriggerHandler.beforeInsert(+)');      
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('EquipmentTriggerHandler.beforeUpdate(+)');

        EquipmentBefInsUpd();
        system.debug('EquipmentTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('EquipmentTriggerHandler.beforeDelete(+)');
    

        system.debug('EquipmentTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EquipmentTriggerHandler.afterInsert(+)');

        system.debug('EquipmentTriggerHandler.afterInsert(-)');       
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EquipmentTriggerHandler.afterUpdate(+)');
        
        system.debug('EquipmentTriggerHandler.afterUpdate(-)');       
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EquipmentTriggerHandler.afterDelete(+)');
        
        
        system.debug('EquipmentTriggerHandler.afterDelete(-)');       
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('EquipmentTriggerHandler.afterUnDelete(+)');
        
        system.assert(true); 
        
        system.debug('EquipmentTriggerHandler.afterUnDelete(-)');         
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

    //before insert, before update
    private void EquipmentBefInsUpd(){

        list<Equipment__c> newList = (list<Equipment__c>)Trigger.new;

        for ( Equipment__c curEquipment : newList){
        	Equipment_DB__c tempEqDBRef;
        	if(trigger.isInsert){
	    		if(curEquipment.Part_Number__c != null)tempEqDBRef=mapEquipmentDBByPartNo.get(curEquipment.Part_Number__c);
	    		if(tempEqDBRef==null && curEquipment.Code__c != null)tempEqDBRef=mapEquipmentDBByCode.get(curEquipment.Code__c);
	    		if(tempEqDBRef==null && curEquipment.FIN__c != null)tempEqDBRef=mapEquipmentDBByFIN.get(curEquipment.FIN__c);
        	}else{
				Equipment__c prevRec = (Equipment__c)(trigger.oldMap.get(curEquipment.Id));
	    		if(curEquipment.Part_Number__c != null && prevRec.Part_Number__c != curEquipment.Part_Number__c)tempEqDBRef=mapEquipmentDBByPartNo.get(curEquipment.Part_Number__c);
	    		if(tempEqDBRef==null && curEquipment.Code__c != null && prevRec.Code__c != curEquipment.Code__c)tempEqDBRef=mapEquipmentDBByCode.get(curEquipment.Code__c);
	    		if(tempEqDBRef==null && curEquipment.FIN__c != null && prevRec.FIN__c != curEquipment.FIN__c)tempEqDBRef=mapEquipmentDBByFIN.get(curEquipment.FIN__c);
        	}
    		if(tempEqDBRef != null){
    			//Overwrite blindly all info with DB if a match is found.
    			curEquipment.Part_Number__c =  tempEqDBRef.Name;
    			if(tempEqDBRef.Code__c != null)curEquipment.Code__c=tempEqDBRef.Code__c; 
    			if(tempEqDBRef.FIN__c != null)curEquipment.FIN__c=tempEqDBRef.FIN__c; 
    			if(tempEqDBRef.ATA_Chapter__c != null)curEquipment.ATA_Text__c=tempEqDBRef.ATA_Chapter__c;
    			if(tempEqDBRef.ATA_Chapter_Name__c != null)curEquipment.ATA_Chapter_Name__c=tempEqDBRef.ATA_Chapter_Name__c;    			 
    			if(tempEqDBRef.Description__c != null)curEquipment.Description__c=tempEqDBRef.Description__c; 
    			if(tempEqDBRef.Family__c != null)curEquipment.Family__c=tempEqDBRef.Family__c; 
    			if(tempEqDBRef.Remark__c != null)curEquipment.Remark__c=tempEqDBRef.Remark__c; 
    			if(tempEqDBRef.Vendor__c != null)curEquipment.Vendor__c=tempEqDBRef.Vendor__c; 
    			if(tempEqDBRef.Zone__c != null)curEquipment.Zone__c=tempEqDBRef.Zone__c; 
    		}
        }
    }


}