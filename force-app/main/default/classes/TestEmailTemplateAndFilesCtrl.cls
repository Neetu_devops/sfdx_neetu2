/**
*******************************************************************************
* Class: TestEmailTemplateAndFilesCtrl
* @author Created by Aman, Lease-Works, 
* @date 17/08/2020
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
* Test all the Methods of EmailTemplateAndFilesCtrl.
*
* History:
* VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestEmailTemplateAndFilesCtrl {
    /**
    * @description Created Test Data to test all the Methods
    * @return null
    */
    @testSetup
    static void setup() {
        List<ContentVersion> cVersionList = new List<ContentVersion>();
        cVersionList.add(new ContentVersion(Title = 'Penguins1', PathOnClient = 'Penguins.jpg', 
                                            VersionData = Blob.valueOf('Test Content'), IsMajorVersion = true));
        cVersionList.add(new ContentVersion(Title = 'Penguins2', PathOnClient = 'Penguins.jpg', 
                                            VersionData = Blob.valueOf('Test Content'), IsMajorVersion = true));
        insert cVersionList;
        
        // Set up the Aircraft record
        Aircraft__c asset = new Aircraft__c(MSN_Number__c = '12345', Date_of_Manufacture__c = System.today(), TSN__c = 0, CSN__c = 0,
                                            Threshold_for_C_Check__c = 1000, Status__c = 'Available', Derate__c = 20, 
                                            Comments_Technical__c = '<p><img src="https//xyz.com/version/download/'+cVersionList[0].Id+'?test"></p><p><img src="https//xyz.com/version/download/'+cVersionList[1].Id+'?test"></p>');
        LeaseWareUtils.clearFromTrigger();
        insert asset;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        cdlList.add(new ContentDocumentLink(LinkedEntityId = asset.id, ContentDocumentId = documents[0].Id, shareType = 'V'));
        cdlList.add(new ContentDocumentLink(LinkedEntityId = asset.id, ContentDocumentId = documents[1].Id, shareType = 'V'));
        insert cdlList;
        
        ContentDistribution cd = new ContentDistribution();
        cd.Name = cVersionList[0].Title;
        cd.ContentVersionId = cVersionList[0].Id;
        cd.PreferencesAllowViewInBrowser = true;
        cd.PreferencesLinkLatestVersion = true;
        cd.PreferencesNotifyOnVisit = false;
        cd.PreferencesPasswordRequired = false;
        cd.PreferencesAllowOriginalDownload = true;
        insert cd;
    }
    
    @isTest static void testSendEmail() {
        try {
            Test.startTest();
            List<Aircraft__c> assetList = Database.query('SELECT Id, Comments_Technical__c FROM Aircraft__c');
            if (!assetList.isEmpty()) {
                assetList[0].put(EmailTemplateAndFilesCtrl.FIELD_API_NAME, 'Asset_Summary');
                update assetList[0];
            }
            
            String response = EmailTemplateAndFilesCtrl.sendEmail(assetList[0].Id);
            Test.stopTest();
        }
        catch(Exception ex) {
            System.debug('Error: '+ex);
        }
    }
    
    @isTest static void testEncodeDecodeImageURLs() {
        try {
            Test.startTest();
            List<Aircraft__c> assetList = Database.query('SELECT Id, Comments_Technical__c FROM Aircraft__c');
            if (!assetList.isEmpty()) {
                String response = EmailTemplateAndFilesCtrl.encodeDecodeImageURLs(assetList[0].Comments_Technical__c, assetList[0].Id);
            }
            Test.stopTest();
        }
        catch (Exception ex) {
            System.debug('Error: '+ex);
        }
    }
    
    @isTest static void testSendEmailNegativeScenario() {
        try {
            Test.startTest();
            String response = EmailTemplateAndFilesCtrl.sendEmail('');
            System.assertEquals('Something went wrong. Try Refreshing the page.', response);
            
            User user = [SELECT Id FROM User Where Id =: UserInfo.getUserId()];
            response = EmailTemplateAndFilesCtrl.sendEmail(user.Id);
            System.assertEquals('Email Template configuration not found, please configure a field on the record with API \''+EmailTemplateAndFilesCtrl.FIELD_API_NAME+'\'', response);
            
            List<Aircraft__c> assetList = Database.query('SELECT Id FROM Aircraft__c');
            if (!assetList.isEmpty()) {
                assetList[0].put(EmailTemplateAndFilesCtrl.FIELD_API_NAME, null);
                update assetList[0];
                
                response = EmailTemplateAndFilesCtrl.sendEmail(assetList[0].Id);
                System.assertEquals('Email Template configuration not found, please configure a field on the record with API \''+EmailTemplateAndFilesCtrl.FIELD_API_NAME+'\'', response);
                
                assetList[0].put(EmailTemplateAndFilesCtrl.FIELD_API_NAME, 'Test_Email_Template');
                update assetList[0];
                
                response = EmailTemplateAndFilesCtrl.sendEmail(assetList[0].Id);
                System.assertEquals('Email Template \'Test_Email_Template\' not found, please make sure it exists and you have access to it', response);
            }
            Test.stopTest();
        }
        catch (Exception ex) {
            System.debug('Error: '+ex);
        }
    }
}