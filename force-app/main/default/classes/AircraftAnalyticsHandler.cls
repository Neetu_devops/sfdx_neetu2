public class AircraftAnalyticsHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'AircraftAnalyticsHandlerBefore';
    private final string triggerAfter = 'AircraftAnalyticsHandlerAfter';
    public AircraftAnalyticsHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('AircraftAnalyticsHandler.beforeInsert(+)');
		generateComps();

	    system.debug('AircraftAnalyticsHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AircraftAnalyticsHandler.beforeUpdate(+)');
    	generateComps();
    	
    	system.debug('AircraftAnalyticsHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AircraftAnalyticsHandler.beforeDelete(+)');
    	
    	
    	system.debug('AircraftAnalyticsHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftAnalyticsHandler.afterInsert(+)');
    	
    	system.debug('AircraftAnalyticsHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftAnalyticsHandler.afterUpdate(+)');
    	
    	
    	system.debug('AircraftAnalyticsHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftAnalyticsHandler.afterDelete(+)');
    	
    	
    	system.debug('AircraftAnalyticsHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftAnalyticsHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('AircraftAnalyticsHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	private void generateComps(){

    	String SelectClause ='';
    	/* 'Select count(Id) cnt'
    		+ ', max(leaseworks__CurrentMarketLeaseRate_m__c) maxCMLR ,min(leaseworks__CurrentMarketLeaseRate_m__c) minCMLR,avg(leaseworks__CurrentMarketLeaseRate_m__c) avgCMLR' 
			+ ', max(leaseworks__CurrentMarketValue_m__c) maxCMV,min(leaseworks__CurrentMarketValue_m__c) minCMV,avg(leaseworks__CurrentMarketValue_m__c) avgCMV' 
			+ ', max(leaseworks__SeatTotal__c) maxST,min(leaseworks__SeatTotal__c) minST,avg(leaseworks__SeatTotal__c) avgST '
			+ ', max(leaseworks__CyclesCumulative__c) maxCC,min(leaseworks__CyclesCumulative__c) minCC,avg(leaseworks__CyclesCumulative__c) avgCC '
			+ ', max(leaseworks__HoursCumulative__c) maxHC,min(leaseworks__HoursCumulative__c) minHC,avg(leaseworks__HoursCumulative__c) avgHC ' 
			+ '  from leaseworks__Global_Fleet__c';*/

		String WhereClause = '';

    	Aircraft_Data_Mining__c[] listNew = (list<Aircraft_Data_Mining__c>)trigger.new ;
    	for(Aircraft_Data_Mining__c curRec:listNew){
    		WhereClause = ' where ';
    		//1.Aircraft Type
			if(curRec.Aircraft_Type__c!=null) WhereClause = WhereClause + ' AircraftType__c = \''+ curRec.Aircraft_Type__c + '\'';
			else WhereClause = WhereClause + ' AircraftType__c like \'%\''  ;
			// 2. Variant : Aircraft_Variant__c
			if(curRec.Aircraft_Variant__c!=null) WhereClause = WhereClause + ' AND AircraftVariant__c = \''+ curRec.Aircraft_Variant__c + '\'';
			// 3. Engine Type
			if(curRec.Engine_Type__c!=null) WhereClause = WhereClause + ' AND EngineType__c = \''+ curRec.Engine_Type__c + '\'';
			// 4. Aircrft Manufacturer
			if(curRec.Aircraft_Manufacturer__c!=null) WhereClause = WhereClause + ' AND AircraftManufacturer__c = \''+ curRec.Aircraft_Manufacturer__c + '\'';
			// 5. Aircrft Status
			WhereClause = WhereClause + ' AND (  ' 
					+ ' AircraftStatus__c = \'XXX\'  ' ; 
			if(curRec.Aircraft_In_Order__c) {
				WhereClause = WhereClause + ' OR AircraftStatus__c = \'Order\'  ' ; 
			}
			if(curRec.Aircraft_In_Service__c) {
				WhereClause = WhereClause + ' OR AircraftStatus__c = \'In Service\'  ' ; 
			}	
			if(curRec.Aircraft_In_Storage__c) {
				WhereClause = WhereClause + ' OR AircraftStatus__c = \'Storage\'  ' ; 
			}			
			WhereClause = WhereClause + ' )  ' ;		 			 
			
			// 6. Year of Manufacurer
			Integer vintageFrom,vintageTo,todayYear,ageFrom,ageTo;
			if(curRec.From_Year_of_manufacture__c!=null || curRec.To_Year_of_manufacture__c!=null) {
				todayYear = System.today().Year();
				vintageFrom = curRec.From_Year_of_manufacture__c == null?1900:Integer.valueOf(curRec.From_Year_of_manufacture__c);
				vintageTo = curRec.To_Year_of_manufacture__c == null?5000:Integer.valueOf(curRec.To_Year_of_manufacture__c);
				ageFrom = todayYear - vintageFrom;
				ageTo = todayYear - vintageTo ;
				system.debug('ageFrom='+ ageFrom);
				system.debug('ageTo='+ ageTo);
				WhereClause = WhereClause + ' AND ((Y_Hidden_YOM_F_c__c >= :vintageFrom AND Y_Hidden_YOM_F_c__c <= :vintageTo) '	;
				WhereClause = WhereClause + '   OR  (AircraftAge__c <= :ageFrom AND AircraftAge__c >= :ageTo AND Y_Hidden_YOM_F_c__c=null ) ) '	;
				
			}			
			// 7. Aircraft Usages : AND ( AircraftUsage__c = XXX OR AircraftUsage__c = yyy OR AircraftUsage__c = zzzz)
			WhereClause = WhereClause + ' AND (  ' 
					+ ' AircraftUsage__c = \'XXX\'  ' ; 
			if(curRec.Usage_Passenger__c) {
				WhereClause = WhereClause + ' OR AircraftUsage__c = \'Passenger\'  ' ; 
			}
			if(curRec.Usage_Other__c) {
				WhereClause = WhereClause + ' OR AircraftUsage__c != \'Passenger\'  ' ; 
			}	
			WhereClause = WhereClause + ' )  ' ;			
			
			system.debug('SelectClause='+ SelectClause);
			system.debug('WhereClause='+ WhereClause);
			
			//List<AggregateResult> listAR = Database.query(SelectClause + WhereClause);
			List<AggregateResult> listAR;
			SelectClause = 'Select '
	    		+ '  max(CurrentMarketLeaseRate_m__c) maxCMLR ,min(CurrentMarketLeaseRate_m__c) minCMLR,avg(CurrentMarketLeaseRate_m__c) avgCMLR' 
				+ '  from Global_Fleet__c';			
			listAR = Database.query(SelectClause + WhereClause + ' AND CurrentMarketLeaseRate_m__c!= null AND CurrentMarketLeaseRate_m__c!=0 ');
			curRec.Avg_Current_Market_Lease_Rate_mm__c = (Decimal)listAR[0].get('avgCMLR') ;
			curRec.Max_Current_Market_Lease_Rate_mm__c = (Decimal)listAR[0].get('maxCMLR') ;
			curRec.Min_Current_Market_Lease_Rate_mm__c = (Decimal)listAR[0].get('minCMLR') ;
			
			SelectClause = 'Select '
	    		+ ' max(CurrentMarketValue_m__c) maxCMV,min(CurrentMarketValue_m__c) minCMV,avg(CurrentMarketValue_m__c) avgCMV' 
				+ '  from Global_Fleet__c';			
			listAR = Database.query(SelectClause + WhereClause + ' AND CurrentMarketValue_m__c!= null AND CurrentMarketValue_m__c!=0 ');
			curRec.Avg_Current_Market_Value__c = (Decimal)listAR[0].get('avgCMV') ;
			curRec.Max_Current_Market_Value__c = (Decimal)listAR[0].get('maxCMV') ;
			curRec.Min_Current_Market_Value__c = (Decimal)listAR[0].get('minCMV') ;
			
			SelectClause = 'Select '
	    		+ ' max(SeatTotal__c) maxST,min(SeatTotal__c) minST,avg(SeatTotal__c) avgST ' 
				+ '  from Global_Fleet__c';			
			listAR = Database.query(SelectClause + WhereClause + ' AND SeatTotal__c!= null AND SeatTotal__c!=0 ');
			curRec.Avg_Seat_Total__c = (Decimal)listAR[0].get('avgST') ;
			curRec.Max_Seat_Total__c = (Decimal)listAR[0].get('maxST') ;
			curRec.Min_Seat_Total__c = (Decimal)listAR[0].get('minST') ;

			SelectClause = 'Select '
	    		+ ' max(CyclesCumulative__c) maxCC,min(CyclesCumulative__c) minCC,avg(CyclesCumulative__c) avgCC ' 
				+ '  from Global_Fleet__c';			
			listAR = Database.query(SelectClause + WhereClause + ' AND CyclesCumulative__c!= null AND CyclesCumulative__c!=0 ');			
			curRec.Avg_Cycles_Cumulative__c = (Decimal)listAR[0].get('avgCC') ;
			curRec.Max_Cycles_Cumulative__c = (Decimal)listAR[0].get('maxCC') ;
			curRec.Min_Cycles_Cumulative__c = (Decimal)listAR[0].get('minCC') ;	
			
			SelectClause = 'Select '
	    		+ ' max(HoursCumulative__c) maxHC,min(HoursCumulative__c) minHC,avg(HoursCumulative__c) avgHC ' 
				+ '  from Global_Fleet__c';			
			listAR = Database.query(SelectClause + WhereClause + ' AND HoursCumulative__c!= null AND HoursCumulative__c!=0 ');				
			curRec.Avg_Hours_Cumulative__c = (Decimal)listAR[0].get('avgHC') ;
			curRec.Max_Hours_Cumulative__c = (Decimal)listAR[0].get('maxHC') ;
			curRec.Min_Hours_Cumulative__c = (Decimal)listAR[0].get('minHC') ;

			SelectClause = 'Select count(Id) cnt'
				+ '  from Global_Fleet__c';			
			listAR = Database.query(SelectClause + WhereClause );				
			curRec.Matches_Found__c = (Integer)listAR[0].get('cnt') ;
			
    	} 	
		
	}

}