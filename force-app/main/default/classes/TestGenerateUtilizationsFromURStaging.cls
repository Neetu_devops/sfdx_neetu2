@isTest
public class TestGenerateUtilizationsFromURStaging {
    
        @testSetup 
        static void setup() {
            
			Date StartDate = Date.newInstance(2009,03,01);
            
            Lessor__c testLessor = new Lessor__c(Name='TestLessor', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
				Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
				Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today()
                                     ,Utilization_Staging_Purge_After__c='2');
            insert testLessor;
            
            Operator__c testOperator = new Operator__c(
                Name = 'SA Airlines',                   
                Status__c = 'Approved',                          
                Rent_Payments_Before_Holidays__c = false,         
                Revenue__c = 0.00,                                
                Current_Lessee__c = true                         
            );
            insert testOperator;
            
            Lease__c leaseRecord = new Lease__c(
                Name = 'Testing',
                Lease_Start_Date_New__c = StartDate,
                Lease_End_Date_New__c = StartDate.addYears(2),
                Lessee__c = testOperator.Id
            );
            insert leaseRecord;
            
            Aircraft__c testAircraft = new Aircraft__c(
                Name = 'Test',                    
                MSN_Number__c = '2821',                       
                Date_of_Manufacture__c = system.today(),
                Aircraft_Type__c = '737',                         
                Aircraft_Variant__c = '700',                    
                Marked_For_Sale__c = false,                  
                Alert_Threshold__c = 10.00,                   
                Status__c = 'Available',                  
                Awarded__c = false,                                              
                TSN__c = 1.00,                               
                CSN__c = 1,
                Last_TSN_utilization__c = 1.00,                               
                Last_CSN_utilization__c = 1
            );
            insert testAircraft;
            
            testAircraft.Lease__c = leaseRecord.Id;
            update testAircraft;
            
            
            leaseRecord.Aircraft__c = testAircraft.ID;
            update leaseRecord;  
            
                        
            List<Constituent_Assembly__c> constituentList = new List<Constituent_Assembly__c>();
            for(Integer i=0;i<7;i++){
                Constituent_Assembly__c consituentRecord = new Constituent_Assembly__c();
                consituentRecord.Serial_Number__c = '2';
                consituentRecord.CSN__c = 3;
                consituentRecord.Last_CSN_Utilization__c = 3;
                consituentRecord.TSN__c = 5;
                consituentRecord.Last_TSN_Utilization__c = 5;
                consituentRecord.Attached_Aircraft__c = testAircraft.Id;                
                consituentRecord.Asset__c = testAircraft.Id;
                consituentRecord.Name ='Testing';
                if (i == 0){
                    consituentRecord.Type__c ='Airframe'; 
                }
                else if (i == 1){
                    consituentRecord.Type__c ='Engine 1'; 
                }
                else if (i == 2){
                    consituentRecord.Type__c ='Landing Gear - Right Main'; 
                }
                else if (i == 3){
                    consituentRecord.Type__c ='Engine 2'; 
                }
                else if (i == 4){
                    consituentRecord.Type__c ='Engine 3'; 
                }
                else if (i == 5){
                    consituentRecord.Type__c ='Engine 4'; 
                }
                else{
                    consituentRecord.Type__c ='APU'; 
                }
                
                constituentList.add(consituentRecord);
            }
           // LWGlobalUtils.setTriggersflagOn(); 
            LeaseWareUtils.TriggerDisabledFlag = true;
            insert constituentList;
            LeaseWareUtils.TriggerDisabledFlag = false;
           // LWGlobalUtils.setTriggersflagOff();
            
            
            // Set up Maintenance Program
            Maintenance_Program__c mp = new Maintenance_Program__c(
                name='Test',Current_Catalog_Year__c='2020');
            insert mp;
            // map of Maintenance Program Event
            map<string,id> mapMPE = new map<string,id>();       
            for(Maintenance_Program_Event__c curMPE: [select id,Assembly__c from Maintenance_Program_Event__c where Maintenance_Program__c =: mp.Id]){
                 mapMPE.put(curMPE.Assembly__c,curMPE.id);
            }
            // Set up Project Event
            List<Assembly_Event_Info__c> ListProjEvent = new List<Assembly_Event_Info__c>();       
            for(Integer i=0;i<3;i++){
                Assembly_Event_Info__c ProjEvent = new Assembly_Event_Info__c();
                if (i == 0){
                    ProjEvent.Name= 'Airframe';
                    ProjEvent.Event_Cost__c= 100;
                    ProjEvent.Constituent_Assembly__c= constituentList[0].id;
                    ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[0].Type__c);
                }
                else if (i == 1){
                    ProjEvent.Name= 'Engine 1';
                    ProjEvent.Event_Cost__c= 100;
                    ProjEvent.Constituent_Assembly__c= constituentList[1].id;
                    ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[1].Type__c);
                }
                else{
                    ProjEvent.Name= 'Landing Gear - Right Main';
                    ProjEvent.Event_Cost__c= 100;
                    ProjEvent.Constituent_Assembly__c= constituentList[2].id;
                    ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[2].Type__c);
                }
                ListProjEvent.add(ProjEvent);
            }
            
            LWGlobalUtils.setTriggersflagOn(); 
            insert ListProjEvent;
            LWGlobalUtils.setTriggersflagOff();

            Test.StartTest();
			
            Id recordTypeIdPBH = Schema.SObjectType.Assembly_MR_Rate__c.getRecordTypeInfosByDeveloperName().get('PBH').getRecordTypeId();
            Assembly_MR_Rate__c assemblyRecord = new Assembly_MR_Rate__c(
            Name='Testing',
            Assembly_Lkp__c =constituentList[0].Id,
            Base_MRR__c = 200,
            Lease__c = leaseRecord.Id,
			Rate_Basis__c='Monthly',
            RecordTypeId = recordTypeIdPBH);
        	insert assemblyRecord;
            
            Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing-UR-Mar',
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='300',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = StartDate.addMonths(1).toStartOfMonth().addDays(-1));//Mar 
            LWGlobalUtils.setTriggersflagOn(); 
             insert utilizationRecord;
            LWGlobalUtils.setTriggersflagOff();
                
            Date URDate1 =  StartDate.addMonths(2).addDays(-1); //Apr 
            Date URDate2 = URDate1.addMonths(1); //May - Invalid month ending
            Date URDate3 = URDate2.addMonths(1); //June - Invalid type - assumed
            Date URDate4 = URDate3.addMonths(1); //July - Will fail due to gap
            System.debug(' URDate1 '+URDate1);
            System.debug(' URDate2 '+URDate2);
            System.debug(' URDate3 '+URDate3);
            System.debug(' URDate4 '+URDate4);
			List <Utilization_Report_Staging__c> URList = new List<Utilization_Report_Staging__c>();
             Utilization_Report_Staging__c UR1 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '200',                                                                            
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Hours_During_Month_APU__c = 200,
                                                                            Hours_During_Month_Engine1__c = 200,
                                                                            Hours_During_Month_Engine2__c = 200,
                                                                            Hours_During_Month_Engine3__c = 200,
                                                                            Hours_During_Month_Engine4__c = 200,
                                                                            Hours_During_Month_Landing_Gear__c = 200,
                                                                            Cycles_During_Month_APU__c = 10,
                                                                            Cycles_During_Month_Engine1__c = 10,
                                                                            Cycles_During_Month_Engine2__c = 10,
                                                                            Cycles_During_Month_Engine3__c = 10,
                                                                            Cycles_During_Month_Engine4__c = 10,
                                                                            Cycles_During_Month_Landing_Gear__c = 10,
                                                                            
																			 Month_Ending__c = URDate1,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            
        	URList.add(UR1);
            Utilization_Report_Staging__c UR2 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
																			 Month_Ending__c = URDate2,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            URList.add(UR2);
            Utilization_Report_Staging__c UR3 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '100',
																			 Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
                                                                             Month_Ending__c = URDate3,
                                                                             Type__c= 'Assumed/Estimated',
                                                                             status__c='Submitted',
                                                                             Staging_Status__c='Pending');
			URList.add(UR3);	
            Utilization_Report_Staging__c UR4 = new Utilization_Report_Staging__c(Aircraft__c = testAircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
																			 Month_Ending__c = URDate4,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            URList.add(UR4);
            
			insert URList;
            
            //Ref: Reconciliation Studio.
            Reconciliation_Schedule__c schedule = new Reconciliation_Schedule__c();
            schedule.from_date__c = URDate1.toStartOfMonth();
            schedule.to_date__c = URDate1;
            schedule.Disable_Invoice_Auto_Generation__c = true;
            schedule.lease__c = leaseRecord.Id; 
            insert schedule;

            Test.stopTest();
  
        }
    
        @isTest
    	static void callGenerateUtilizationfromURStaging() {
            
            Aircraft__c aircraft = [select id from Aircraft__c];
            Integer expectedURCount = 2 ; //Mar, Apr would succeed
            
            Test.startTest();
            Database.executeBatch(new GenerateUtilizationsFromURStaging(), 1);
            Test.stopTest();
           
            Integer actualURCount = [select count() from Utilization_Report__c where Aircraft__c = :aircraft.Id];
            Utilization_Report__c URlatest = [select id,Aircraft__c,Month_Ending__c,End_Date_F__c,TSN_At_Month_End__c,CSN_At_Month_End__c from Utilization_Report__c where Aircraft__c=:aircraft.Id ORDER BY End_Date_F__c Desc,CreatedDate Desc LIMIT 1];
        
            System.debug('Test: ' + URlatest);
            System.assertEquals(200, URlatest.TSN_At_Month_End__c);
            System.assertEquals(30, URlatest.CSN_At_Month_End__c);

            for(Assembly_Utilization__c curRec : [select id,name,TSN_At_Month_End__c,CSN_At_Month_End__c from Assembly_Utilization__c where Utilization_Report__c =:URlatest.id]){

                System.assertEquals(205, curRec.TSN_At_Month_End__c);
                System.assertEquals(13, curRec.CSN_At_Month_End__c);
            }

            Aircraft__c aircraft1 = [select id,TSN__c,CSN__c,Last_TSN_utilization__c,Last_CSN_utilization__c from Aircraft__c];
           
            System.assertEquals(200,aircraft1.TSN__c);
            System.assertEquals(30, aircraft1.CSN__c);
            System.assertEquals(200,aircraft1.Last_TSN_utilization__c);
            System.assertEquals(30, aircraft1.Last_CSN_utilization__c);

            for(Constituent_Assembly__c curAssembl :[select id,TSN__c,CSN__c,Last_TSN_utilization__c,Last_CSN_utilization__c from Constituent_Assembly__c]){
                System.assertEquals(205, curAssembl.TSN__c);
                System.assertEquals(13, curAssembl.CSN__c);
                System.assertEquals(205, curAssembl.Last_TSN_utilization__c);
                System.assertEquals(13, curAssembl.Last_CSN_utilization__c);
            }

            System.debug('expectedURCount = ' + expectedURCount);
            System.debug('actualURCount = ' + actualURCount);
            
            System.assertEquals(expectedURCount,actualURCount);
            
        }

        @isTest
    	static void callGenerateUtilizationfromURStaging2() {
            
            Aircraft__c aircraft = [select id from Aircraft__c];
            Integer expectedURCount = 1 ; 

            List<Utilization_Report_Staging__c> lstURStaging = [select id from Utilization_Report_Staging__c];
            delete lstURStaging;

            //Create new staging record for future period. So, would fail
            Date URFutureDate = System.Today().addMonths(2).toStartOfMonth().addDays(-1);
            Utilization_Report_Staging__c URTest = new Utilization_Report_Staging__c(Aircraft__c = aircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
																			 Month_Ending__c = URFutureDate,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            
            
            insert URTest;
            
            Test.startTest();
            Database.executeBatch(new GenerateUtilizationsFromURStaging(), 1);
            Test.stopTest();
           
            Integer actualURCount = [select count() from Utilization_Report__c where Aircraft__c = :aircraft.Id];
            System.debug('expectedURCount = ' + expectedURCount);
            System.debug('actualURCount = ' + actualURCount);
            
            System.assertEquals(expectedURCount,actualURCount);
            
        }
        
        @isTest
    	static void TestInvoiceAndPaymentGeneration() {
            
            Aircraft__c aircraft = [select id from Aircraft__c];
            Integer expectedURCount = 2 ; //Mar, Apr

            List<Utilization_Report_Staging__c> lstURStaging = [select id from Utilization_Report_Staging__c];
            delete lstURStaging;

            Date StartDate = Date.newInstance(2009,03,01);
            Date URDate = StartDate.addMonths(2).addDays(-1); //Apr

            Utilization_Report_Staging__c URTest = new Utilization_Report_Staging__c(Aircraft__c = aircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
                                                                             Invoice_Date__c = Date.newInstance(2009,05,02),
                                                                             Payment_Date__c = Date.newInstance(2009,06,03),
																			 Month_Ending__c = URDate,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            
            
            insert URTest;

            List<Reconciliation_Schedule__c> schedules = [select id, Disable_Invoice_Auto_Generation__c from Reconciliation_Schedule__c];
            if(schedules.size() > 0)
            {
                schedules[0].Disable_Invoice_Auto_Generation__c = false;            
                update schedules[0];
            }
            
            Test.startTest();
            Database.executeBatch(new GenerateUtilizationsFromURStaging(), 1);
            Test.stopTest();
           
            Integer actualURCount = [select count() from Utilization_Report__c where Aircraft__c = :aircraft.Id];
            System.debug('expectedURCount = ' + expectedURCount);
            System.debug('actualURCount = ' + actualURCount);
            
            System.assertEquals(expectedURCount,actualURCount);
            
    	}
        @isTest
    	static void TestPaymentGenerationFailure() {
            
            Aircraft__c aircraft = [select id from Aircraft__c];
            Integer expectedURCount = 1 ; //Mar

            List<Utilization_Report_Staging__c> lstURStaging = [select id from Utilization_Report_Staging__c];
            delete lstURStaging;

            Date StartDate = Date.newInstance(2009,03,01);
            Date URDate = StartDate.addMonths(2).addDays(-1); //Apr

            Utilization_Report_Staging__c URTest = new Utilization_Report_Staging__c(Aircraft__c = aircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
                                                                             Invoice_Date__c = Date.newInstance(2009,05,02),
                                                                             Payment_Date__c = System.Today().addMonths(1), //Future date - invalid input
																			 Month_Ending__c = URDate,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            
            
            insert URTest; //would fail due to invalid payment info

            List<Reconciliation_Schedule__c> schedules = [select id, Disable_Invoice_Auto_Generation__c from Reconciliation_Schedule__c];
            if(schedules.size() > 0)
            {
                schedules[0].Disable_Invoice_Auto_Generation__c = false;            
                update schedules[0];
            }
            
            Test.startTest();
            Database.executeBatch(new GenerateUtilizationsFromURStaging(), 1);
            Test.stopTest();
           
            Integer actualURCount = [select count() from Utilization_Report__c where Aircraft__c = :aircraft.Id];
            System.debug('expectedURCount = ' + expectedURCount);
            System.debug('actualURCount = ' + actualURCount);
            
            System.assertEquals(expectedURCount,actualURCount);            
        }
        @isTest
    	static void TestIncorrectInvoiceAmount() {
            
            Aircraft__c aircraft = [select id from Aircraft__c];
            Integer expectedURCount = 1 ; //Mar

            List<Utilization_Report_Staging__c> lstURStaging = [select id from Utilization_Report_Staging__c];
            delete lstURStaging;

            Date StartDate = Date.newInstance(2009,03,01);
            Date URDate = StartDate.addMonths(2).addDays(-1); //Apr

            Utilization_Report_Staging__c URTest = new Utilization_Report_Staging__c(Aircraft__c = aircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
                                                                             Invoice_Date__c = Date.newInstance(2009,05,02),
                                                                             Invoice_amount__c = 1111.23,
                                                                             Payment_Date__c = System.Today().addMonths(1), //Future date - invalid input
																			 Month_Ending__c = URDate,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            
            
            insert URTest; //would fail due to incorrect invoice amount

            List<Reconciliation_Schedule__c> schedules = [select id, Disable_Invoice_Auto_Generation__c from Reconciliation_Schedule__c];
            if(schedules.size() > 0)
            {
                schedules[0].Disable_Invoice_Auto_Generation__c = false;            
                update schedules[0];
            }
            
            Test.startTest();
            Database.executeBatch(new GenerateUtilizationsFromURStaging(), 1);
            Test.stopTest();
           
            Integer actualURCount = [select count() from Utilization_Report__c where Aircraft__c = :aircraft.Id];
            System.debug('expectedURCount = ' + expectedURCount);
            System.debug('actualURCount = ' + actualURCount);
            
            System.assertEquals(expectedURCount,actualURCount);            
        }
        @isTest
    	static void TestAccountingSetupValidations() {
            
            Aircraft__c aircraft = [select id, Lease__c from Aircraft__c];
            Lease__c lease = [select id,Accounting_Setup_P__c from Lease__c where id = :aircraft.lease__c];

            Integer expectedURCount = 1 ; //Mar

            List<Utilization_Report_Staging__c> lstURStaging = [select id from Utilization_Report_Staging__c];
            delete lstURStaging;

            parent_Accounting_setup__c tstAccSetup = new parent_Accounting_Setup__c(name='test');
            insert tstAccSetup;

            lease.Accounting_Setup_P__c = tstAccSetup.id;
            update lease;

            Date StartDate = Date.newInstance(2009,03,01);
            Date URDate = StartDate.addMonths(2).addDays(-1); //Apr

            Calendar_Period__c CP1 = new Calendar_Period__c(Name = 'TestCalPrd',Start_Date__c =Date.newInstance(2009,04,01),
                                                        End_Date__c = Date.newInstance(2009,04,30));
            insert CP1;

            Utilization_Report_Staging__c URTest = new Utilization_Report_Staging__c(Aircraft__c = aircraft.Id,
                                                                             FH_String__c = '100',
                                                                             Hours_During_Month_APU__c=100,
                                                                             Airframe_Cycles_Landing_During_Month__c = 10,
                                                                             Cycles_During_Month_APU__c= 10,
                                                                             Invoice_Date__c = Date.newInstance(2009,05,02),
                                                                             Payment_Date__c = Date.newInstance(2009,06,03),
																			 Month_Ending__c = URDate,
                                                                             Type__c= 'Actual',
                                                                             status__c='Approved By Lessor',
                                                                             Staging_Status__c='Pending');
            
            
            insert URTest; //would fail due to missing accounting period
 
            Test.startTest();
            Database.executeBatch(new GenerateUtilizationsFromURStaging(), 1);
            Test.stopTest();
           
            Integer actualURCount = [select count() from Utilization_Report__c where Aircraft__c = :aircraft.Id];
            System.debug('expectedURCount = ' + expectedURCount);
            System.debug('actualURCount = ' + actualURCount);
            
            System.assertEquals(expectedURCount,actualURCount);            
        }

        
}