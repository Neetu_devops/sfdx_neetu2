public class LLPTemplateTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'LLPTemplateTriggerHandlerBefore';
    private final string triggerAfter = 'LLPTemplateTriggerHandlerAfter';
    
    private static list<LLP_Template__c>  listExistingLLPTmpl;
    
    public LLPTemplateTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
     
    private void setSOQLStaticMAPOrList(){
    	LLP_Template__c[] listNew = (list<LLP_Template__c>)(trigger.isDelete?trigger.old:trigger.new) ;
    	
    	if(listExistingLLPTmpl==null){
    		listExistingLLPTmpl = [select id,Engine_Template__c,Part_Number__c,Y_Hidden_Engine_Template_Name__c,Life_Limit_Cycles__c
    				, Part_Name__c,Module_N__c,Replacement_Cost__c,Position_LLPT__c
 				from LLP_Template__c
 				where id not in :listNew];
    	}
    }     
     
    public void bulkBefore()
    {
        setSOQLStaticMAPOrList();
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('LLPTemplateTriggerHandler.beforeInsert(+)');
        checkDuplicateTemplate();

        system.debug('LLPTemplateTriggerHandler.beforeInsert(+)');      
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('LLPTemplateTriggerHandler.beforeUpdate(+)');
        boolean IsCalledFrmLLPCatalogTrigger=false;
        if(LeaseWareUtils.isFromTrigger(LLPCatalogTriggerHandler.triggerBefore) || LeaseWareUtils.isFromTrigger(LLPCatalogTriggerHandler.triggerAfter)){
            IsCalledFrmLLPCatalogTrigger=true;
        }
        system.debug('IsCalledFrmLLPCatalogTrigger='+IsCalledFrmLLPCatalogTrigger);
        if(!IsCalledFrmLLPCatalogTrigger) checkDuplicateTemplate();
        system.debug('LLPTemplateTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        system.debug('LLPTemplateTriggerHandler.beforeDelete(+)');
    
	
		
		system.debug('LLPTemplateTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LLPTemplateTriggerHandler.afterInsert(+)');
        
        createChildIfClone();
        UpdMETRecChangeLLPTempRec();
        
        system.debug('LLPTemplateTriggerHandler.afterInsert(-)');       
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LLPTemplateTriggerHandler.afterUpdate(+)');
        
		//updateRelatedLLPChanges();
        UpdMETRecChangeLLPTempRec();
        
        system.debug('LLPTemplateTriggerHandler.afterUpdate(-)');       
    }
     
    public void afterDelete()
    {
        system.debug('LLPTemplateTriggerHandler.afterDelete(+)');
        
        UpdMETRecChangeLLPTempRec();
        system.debug('LLPTemplateTriggerHandler.afterDelete(-)');       
    }

    public void afterUnDelete()
    {
        system.debug('LLPTemplateTriggerHandler.afterUnDelete(+)');
        

        
        system.debug('LLPTemplateTriggerHandler.afterUnDelete(-)');         
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

 	private void checkDuplicateTemplate(){
 		LLP_Template__c[] listNew = (list<LLP_Template__c>)trigger.new ;
		set<string> setCheckDuplicatePartNum = new set<string>();
		string positionStr;// anjani : converting Position to Integer then string. Through data Loader the value comes with decimal like 6.0
 		for(LLP_Template__c curRec:listExistingLLPTmpl){
			positionStr =  curRec.Position_LLPT__c==null?'':''+Integer.valueOf(curRec.Position_LLPT__c);
 			setCheckDuplicatePartNum.add(curRec.Engine_Template__c + curRec.Part_Number__c + positionStr);
 			setCheckDuplicatePartNum.add(curRec.Engine_Template__c + curRec.Part_Name__c + curRec.Part_Number__c + curRec.Module_N__c +positionStr);
 		}
 		for(LLP_Template__c curRec:listNew){
			positionStr =  curRec.Position_LLPT__c==null?'':''+Integer.valueOf(curRec.Position_LLPT__c);
 			if(setCheckDuplicatePartNum.contains(curRec.Engine_Template__c + curRec.Part_Number__c+positionStr)){
				curRec.addError('Duplicate Record : {'+curRec.Y_Hidden_Engine_Template_Name__c 
				+ '}, {' +curRec.Part_Name__c + '}, {'  +curRec.Part_Number__c+ '}, {' +curRec.Module_N__c + '}, {' +positionStr + '}.'  );
 			}else if(setCheckDuplicatePartNum.contains(curRec.Engine_Template__c + curRec.Part_Name__c + curRec.Part_Number__c + curRec.Module_N__c +positionStr)){
 				curRec.addError('Duplicate Record : {'+curRec.Y_Hidden_Engine_Template_Name__c 
 					+ '}, {' +curRec.Part_Name__c + '}, {'  +curRec.Part_Number__c+ '}, {' +curRec.Module_N__c + '}, {' +positionStr + '}.'  );
			}
			setCheckDuplicatePartNum.add(curRec.Engine_Template__c + curRec.Part_Number__c + positionStr); 
 			setCheckDuplicatePartNum.add(curRec.Engine_Template__c + curRec.Part_Name__c + curRec.Part_Number__c + curRec.Module_N__c +positionStr); 			
 		}
 	}

	// afterInsert only
	private void createChildIfClone(){

		LLP_Template__c[] newLLPTemplates = (list<LLP_Template__c>)trigger.new;
        list<LLP_Catalog__c> listNewAssetHist = new list<LLP_Catalog__c>();
		map<ID,id> IdsNeedToCloneMap = new map<ID,id>();
		set<ID> SourceIdsSet = new set<ID>();
		for(LLP_Template__c curRec: newLLPTemplates){
			if(curRec.isClone()){
				SourceIdsSet.add(curRec.getCloneSourceId());
				IdsNeedToCloneMap.put(curRec.getCloneSourceId(),curRec.Id);
			}
		}
		
		if(!IdsNeedToCloneMap.isEmpty()){
			LLP_Template__c[] ParentCloneList = [select id 
											,(select Name,Catalog_Price__c,Part_Number_New__c
											 from LLP_Catalog__r
											)
										 from LLP_Template__c
										 where id in :SourceIdsSet ];	

			list<LLP_Catalog__c> listInsertChilds = new list<LLP_Catalog__c>();
			for(LLP_Template__c curParent:ParentCloneList){
				for(LLP_Catalog__c curChild:curParent.LLP_Catalog__r){
						LLP_Catalog__c cloneChild = curChild.clone(false, true, false, false);
						cloneChild.LLP_Template__c = IdsNeedToCloneMap.get(curParent.Id);
						listInsertChilds.add(cloneChild);
				}
			}
			if(!listInsertChilds.isEmpty()) insert listInsertChilds;
			system.debug('listInsertChilds='+listInsertChilds);
		}		
	}	

    /* 
 	//after update
	private void updateRelatedLLPChanges(){
		
		LLP_Template__c[] listNew = (list<LLP_Template__c>)trigger.new ;
		map<string,LLP_Template__c> mapLLPTemplate = new map<string,LLP_Template__c>();
		set<string> setPartNumber = new set<string>();
		set<string> setEngTeml = new set<string>();
		LLP_Template__c oldrec;
 		for(LLP_Template__c curRec:listNew){
 			oldrec = (LLP_Template__c)trigger.oldMap.get(curRec.Id);
 			if(curRec.Life_Limit_Cycles__c!=oldrec.Life_Limit_Cycles__c || curRec.Replacement_Cost__c!=oldrec.Replacement_Cost__c ){
 				mapLLPTemplate.put(curRec.Engine_Template__c + '-'+curRec.Part_Number__c,curRec);
 				setPartNumber.add(curRec.Part_Number__c);
 				setEngTeml.add(curRec.Engine_Template__c);
 			}
 		}		
		
	}
	*/


    // After Insert, After Update, After Delete
    //Purpose of this Method Insert/Update/Delete Maintenance Event Task record based on LLP Template Record for that Engine Temp ID
    /* Additional Note:  
      If you don't want to recursive call any LLPTemplate method Before or After LLPCatlog call [example upate LLP_Catalog__c] 
      then use below logic.
        boolean IsCalledFrmLLPCatalogTrigger=false;
        if(LeaseWareUtils.isFromTrigger(LLPCatalogTriggerHandler.triggerBefore) || LeaseWareUtils.isFromTrigger(LLPCatalogTriggerHandler.triggerAfter)){
            IsCalledFrmLLPCatalogTrigger=true;
        }
        system.debug('IsCalledFrmLLPCatalogTrigger='+IsCalledFrmLLPCatalogTrigger);
        if(!IsCalledFrmLLPCatalogTrigger) call YourMethod.
    */
    private void UpdMETRecChangeLLPTempRec(){
        system.debug('LLPTemplateTriggerHandler.UpdMETRecChangeLLPTempRec(+)');
        LLP_Template__c[] listNew = (list<LLP_Template__c>)(trigger.isDelete?trigger.old:trigger.new) ;
        set<id> setEngLLPTempIds  = new set<id>();
        LLP_Template__c oldLLPRec;
        for(LLP_Template__c curLLPRec:listNew){
            if(trigger.IsInsert || trigger.IsDelete) setEngLLPTempIds.add(curLLPRec.Engine_Template__c);
            if(trigger.IsUpdate){
                oldLLPRec = (LLP_Template__c)trigger.oldMap.get(curLLPRec.Id);
                if(curLLPRec.Name!=oldLLPRec.Name || curLLPRec.Replacement_Cost__c!=oldLLPRec.Replacement_Cost__c ){
                    setEngLLPTempIds.add(curLLPRec.Engine_Template__c);
                }
            }
        }
        if(setEngLLPTempIds.size()>0) FindLLP_SumRepCost(setEngLLPTempIds);
        system.debug('LLPTemplateTriggerHandler.UpdMETRecChangeLLPTempRec(-)');
    }

    /* Purpose: Finding All Latest LLP Template changes based on EngineTempID
       Reason to reterive all LLPs again, based on new changes in LLPTemplate under EngTempId, Recalculate Sum of ReplacementCost
       for those LLPs for that EnginTempID and based on this will upate Task Cost Event %. Details updated in spec- Maintenance Event Workscope.   
     */
	public static void FindLLP_SumRepCost(set<Id> SetEngTempIDs){
        //Part 1: Finding All Latest LLP Template changes based on EngineTempID
        list<LLP_Template__c> listLLTemplate = new list<LLP_Template__c>();
        if(SetEngTempIDs.size()>0){
            listLLTemplate= [ select id,Name,Module_N__c,Part_Name__c,Replacement_Cost_Current_Catalog_Year__c,
							Engine_Template__c,Engine_Template__r.Name
							from LLP_Template__c 
							where Engine_Template__c IN :SetEngTempIDs
						];
        }
		system.debug('size of listLLTemplate='+listLLTemplate.size());
    
    
		map<ID, LLP_Template__c> mapLLPTempRec=new map<Id,LLP_Template__c>();
		map<id, decimal> mapSumpRepCost = new map<id, decimal>(); // <EngTemp ID, sum of Replacement Cost of all LLP Templates for that EngTemplate>
		map<ID, List<LLP_Template__c>> mapEngIDLLPTemp= new map<ID, List<LLP_Template__c>>();

		//Two things are doing here :
		// 1. Creating map<Id,LLPTemplateRec>
		// 2. sum of all Replacement_Cost for that Engine Temp id map<EngTemp ID, sum of LLP  Replacement Cost>
		
		if(listLLTemplate.size()>0) {
			for(LLP_Template__c curLLPTemp: listLLTemplate){
				mapLLPTempRec.put(curLLPTemp.ID,curLLPTemp);				
				if( !mapSumpRepCost.containsKey(curLLPTemp.Engine_Template__c)){
					mapSumpRepCost.put(curLLPTemp.Engine_Template__c,0);
				}
				mapSumpRepCost.put(curLLPTemp.Engine_Template__c,mapSumpRepCost.get(curLLPTemp.Engine_Template__c)+curLLPTemp.Replacement_Cost_Current_Catalog_Year__c);
	  			if( !mapEngIDLLPTemp.containsKey(curLLPTemp.Engine_Template__c)){
					mapEngIDLLPTemp.put(curLLPTemp.Engine_Template__c,new List<LLP_Template__c>());
				}
				mapEngIDLLPTemp.get(curLLPTemp.Engine_Template__c).add(curLLPTemp);
			}
		}
		system.debug('size of mapLLPTempRec='+mapLLPTempRec.size());
		system.debug('size of mapSumpRepCost='+mapSumpRepCost.size());
		//----End Part 1----
	  
		//----Start Part 2---- 
		/* Below is discription of this part
		  A. IF LLP Template ID is matched in Existing MET Record Then update with the change. i.e. Name or Replacement_Cost
		  B. IF Newly LLP Template Record then create MET Record crosspondingly
		  C. IF LLP Template Record get deleted then Delete MET Record if there is no link with Supplemental Rent Event Requirement else set Null value in the LLP Temp lkp field
		*/

		map<id, List<Maintenance_Event_Task__c>> mapEngTemp_METIds = new map<id, List<Maintenance_Event_Task__c>>(); // map<EngTempID,List Of METRec> based on EngTempID
		map<id, map<id,List<ID>>> mapEngTempMPEMETIds = new map<id, map<id,List<ID>>>(); //map<EngTempID,map<MPEID,List<MET's LLPTempID>>>
		map<id,List<ID>> mapMPEMETIds;
		//Find all MPE IDs which has These EngTemp ID and find list of it's MET records
		List<Maintenance_Event_Task__c> ListMETRec= 
			[ select id,Name,Remark__c,TaskCostMin__c,TaskCostEventPrcntg__c,Y_Hidden_TaskCostEvent__c,
				Y_Hidden_LLPTemplate__c, Y_Hidden_EngineTemplate__c,Y_Hidden_LLPTemplate__r.id,
				//MPE Fields
				Maintenance_Program_Event__c,Maintenance_Program_Event__r.name, 
				Maintenance_Program_Event__r.Engine_Template__c
			  from Maintenance_Event_Task__c 
			  where Y_Hidden_EngineTemplate__c in : SetEngTempIDs
			];
		if(ListMETRec!=Null && ListMETRec.size()>0){
			for(Maintenance_Event_Task__c curMETRec: ListMETRec){
				if( !mapEngTemp_METIds.containsKey(curMETRec.Y_Hidden_EngineTemplate__c)){
					mapEngTemp_METIds.put(curMETRec.Y_Hidden_EngineTemplate__c,new List<Maintenance_Event_Task__c>());
				}
				mapEngTemp_METIds.get(curMETRec.Y_Hidden_EngineTemplate__c).add(curMETRec);

				if(mapEngTempMPEMETIds.containsKey(curMETRec.Y_Hidden_EngineTemplate__c)){
					mapMPEMETIds = mapEngTempMPEMETIds.get(curMETRec.Y_Hidden_EngineTemplate__c);
				}else{
					mapMPEMETIds = new map<id,List<ID>>();
				}
				if(!mapMPEMETIds.containsKey(curMETRec.Maintenance_Program_Event__c)){
					mapMPEMETIds.put(curMETRec.Maintenance_Program_Event__c,new List<ID>());
				}
				mapMPEMETIds.get(curMETRec.Maintenance_Program_Event__c).add(curMETRec.Y_Hidden_LLPTemplate__c);
				mapEngTempMPEMETIds.put(curMETRec.Y_Hidden_EngineTemplate__c,mapMPEMETIds);

			}
		}
		system.debug('size of mapEngTemp_METIds='+mapEngTemp_METIds.size());
		system.debug('size of mapEngTempMPEMETIds='+mapEngTempMPEMETIds.size());

		LLP_Template__c curLLPTemp;
		map <ID, Maintenance_Event_Task__c > mapUpdMETRec = new map < ID, Maintenance_Event_Task__c > ();
		map<Id,Maintenance_Event_Task__c> mapDelMETRec= New map<Id, Maintenance_Event_Task__c>();
		List<Maintenance_Event_Task__c> listMainEvntTask=new List<Maintenance_Event_Task__c>();
		Boolean Flag=false;
		for(ID EngTempID : mapEngTemp_METIds.keyset()){
			curLLPTemp = new LLP_Template__c();
			for(Maintenance_Event_Task__c curMET: mapEngTemp_METIds.get(EngTempID)){
				Flag=False;

				/* Below IF case Update opertaion perform for three scenarios:
				  1. Update Existing MET Records when Engine LLP Template Record ID update like Current Catalog Year-> Catalog_Price__c got Update
				  2. Update Existing MET Records when LLP Template Record get updated like name or Replacement_Cost
				  3. Also those case when LLP Temp Rec Delete then other MET Records for that Eng TempId got effected, Need to be re-calculate again 
				*/
				
				// Part A. IF LLP Template ID is matched in Existing MET Record Then update with the change. i.e. Name or Replacement_Cost
				
				if(mapLLPTempRec.containsKey(curMET.Y_Hidden_LLPTemplate__c)){ 
				  Flag=True;
				  curLLPTemp= mapLLPTempRec.get(curMET.Y_Hidden_LLPTemplate__c);
					if(curMET.Name!= curLLPTemp.Name) curMET.Name=curLLPTemp.Module_N__c==Null? curLLPTemp.Part_Name__c : curLLPTemp.Module_N__c +'-'+curLLPTemp.Part_Name__c;
					curMET.Remark__c=curLLPTemp.Engine_Template__r.Name;
					curMET.TaskCostMin__c=curLLPTemp.Replacement_Cost_Current_Catalog_Year__c;
					curMET.TaskCostEventPrcntg__c=mapSumpRepCost.get(curLLPTemp.Engine_Template__c)!=0 ? ((curLLPTemp.Replacement_Cost_Current_Catalog_Year__c*100)/mapSumpRepCost.get(curLLPTemp.Engine_Template__c)).setscale(2):0; // case of Engine template
					curMET.Y_Hidden_TaskCostEvent__c= curLLPTemp.Replacement_Cost_Current_Catalog_Year__c;
				}
				else{  // Part C. IF LLP Template Record get deleted then Delete MET Record
				  //CurMET record is not available in LLPTemplate for that EngTemp, 
				  //It mean this LLPTemplate record get deleted but not yet deleted in MET. Needs to be delete
				 
				   mapDelMETRec.put(curMET.ID,curMET);
				}
				if(Flag) mapUpdMETRec.put(curMET.id,curMET); 
			}
			//Part B. IF Newly LLP Template Record then create MET Record crosspondingly
			if(mapEngIDLLPTemp.containsKey(EngTempID)){
				for(LLP_Template__c LLPTempOrgRec: mapEngIDLLPTemp.get(EngTempID)){
					for(ID curMPE: mapEngTempMPEMETIds.get(EngTempID).keyset()){
						if(!mapEngTempMPEMETIds.get(EngTempID).get(curMPE).contains(LLPTempOrgRec.id)){
							listMainEvntTask.add(new Maintenance_Event_Task__c(
								Remark__c=LLPTempOrgRec.Engine_Template__r.Name,
								Name=LLPTempOrgRec.Module_N__c==Null? LLPTempOrgRec.Part_Name__c : LLPTempOrgRec.Module_N__c +'-'+LLPTempOrgRec.Part_Name__c,
								TaskCostMin__c=LLPTempOrgRec.Replacement_Cost_Current_Catalog_Year__c,
								Maintenance_Program_Event__c=curMPE,
								TaskCostEventPrcntg__c=mapSumpRepCost.get(LLPTempOrgRec.Engine_Template__c)!=0 ? ((LLPTempOrgRec.Replacement_Cost_Current_Catalog_Year__c*100)/mapSumpRepCost.get(LLPTempOrgRec.Engine_Template__c)).setscale(2):0, // case of Engine template
								Y_Hidden_TaskCostEvent__c= LLPTempOrgRec.Replacement_Cost_Current_Catalog_Year__c, // case of Engine template - Internal purpose which ever is max(Task CostEven% in currency format,TaskCostMin) 
								Y_Hidden_EngineTemplate__c=LLPTempOrgRec.Engine_Template__c,
								Y_Hidden_LLPTemplate__c=LLPTempOrgRec.id
							));
						}
					}
				}
			}
		}

		system.debug('size of mapUpdMETRec='+mapUpdMETRec.size());
		if(mapUpdMETRec.values().size()>0) Update mapUpdMETRec.values(); // without triger off reason is to check duplicate MET Name
		system.debug('size of mapDelMETRec='+mapDelMETRec.size());
		LeaseWareUtils.TriggerDisabledFlag=true;

		/* Comments on below code
		  On delete LLP Template - check if there’s a Supplemental Rent Requirement associated to the Maintenance Event Task:
		  Yes - do not delete Maintenance Event Task, remove LLP Template lookup only
		  No - delete Maintenance Event Task
		*/

		List<Maintenance_Event_Task__c> ListUpdLLPTempIDInMETRec= New List<Maintenance_Event_Task__c>();
		List<Maintenance_Event_Task__c> ListDelMETRec = new List<Maintenance_Event_Task__c>();
		Maintenance_Event_Task__c METRec = New Maintenance_Event_Task__c();
		set<ID> setSRERMETIDContain= new Set<ID>();
		if(mapDelMETRec.size()>0){
			for(Supp_Rent_Event_Reqmnt__c currSRER : 
                    [select id,Maintenance_Event_Task__c from Supp_Rent_Event_Reqmnt__c 
                    where Maintenance_Event_Task__c in : mapDelMETRec.keyset()]){
				setSRERMETIDContain.add(currSRER.Maintenance_Event_Task__c);

			}  
		}
		for(ID CurMETID: mapDelMETRec.keyset()){
			if(setSRERMETIDContain.contains(CurMETID)){ //If Ture then set LLPTemplateID in MET as Null
				METRec=mapDelMETRec.get(CurMETID);
				METRec.Y_Hidden_LLPTemplate__c=Null;
				ListUpdLLPTempIDInMETRec.add(METRec);
			}
			else{ // remove MET record
				ListDelMETRec.add(mapDelMETRec.get(CurMETID));
			}
		}
		system.debug('size of ListDelMETRec='+ListDelMETRec.size());
		if(ListDelMETRec.size()>0) delete ListDelMETRec;
		system.debug('size of ListUpdLLPTempIDInMETRec='+ListUpdLLPTempIDInMETRec.size());
		if(ListUpdLLPTempIDInMETRec.size()>0) Update ListUpdLLPTempIDInMETRec;
		LeaseWareUtils.TriggerDisabledFlag=false;
		system.debug('size of listMainEvntTask='+listMainEvntTask.size());
		//system.debug('listMainEvntTask='+ JSON.serializePretty(listMainEvntTask));
		if (listMainEvntTask.size() > 0) Insert listMainEvntTask;
	}
}