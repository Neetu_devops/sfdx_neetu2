public without sharing class AssetInDealEditor {
        
    
    
    @AuraEnabled
    public static void deleteAssetInDeal(String recordId) {
        List<Aircraft_Proposal__c> deleteAID = [select Id	 from Aircraft_Proposal__c where Id =: recordId];
        if(deleteAID.size() > 0) {
            delete deleteAID[0];
        }
    }
    
    @AuraEnabled
    public static void updateAssetInDeal(List<Aircraft_Proposal__c> assetInDealList) {
        if(assetInDealList == null || assetInDealList.size() <= 0)
            return;
        update assetInDealList;
    }
	
    @AuraEnabled
    public static String getColumns(String recordId, String fieldSetName, String additionalField){
        System.debug('getColumns fieldSetName ' + fieldSetName);
        if(String.isBlank(fieldSetName)) {
            //String defaultFieldSet = LeaseWareUtils.getNamespacePrefix()+'AssetInDealEditor';
            String defaultFieldSet = getFieldSetBasedOnType(recordId);
            System.debug('getColumns defaultFieldSet ' + defaultFieldSet);
            if(defaultFieldSet == null)
                return null;
            List<FieldStructure> fieldList = getFieldSets(defaultFieldSet);
            System.debug('getColumns fieldList ' + fieldList);
            if(fieldList == null || fieldList.size() <= 0) 
                return null;//JSON.serialize(defaultColumns());
            fieldList = addNameField(fieldList);
            //LW-AKG: 12/1820 : Added to load any additional field when passed from Bid Matrix
            if (additionalField != null && additionalField != '') {
                fieldList = addAdditionalFields(fieldList, additionalField);
            }
            return JSON.serialize(fieldList); 
            
        }
        else {
            List<FieldStructure> fieldList = getFieldSets(fieldSetName);
            if(fieldList == null || fieldList.size() <= 0) 
                return null;JSON.serialize(defaultColumns());
            fieldList = addNameField(fieldList);
            if (additionalField != null && additionalField != '') {
                fieldList = addAdditionalFields(fieldList, additionalField);
            }
            return JSON.serialize(fieldList);
        }
    }
    
    public static List<FieldStructure> addNameField(List<FieldStructure> fieldList){
        FieldStructure field = new FieldStructure();
        field.label = 'Name';
        field.apiName = 'Name';
        field.fieldType = 'String';
        fieldList.add(0,field);
        return fieldList;
    }
    
    public static List<FieldStructure> defaultColumns(){
        List<FieldStructure> fieldList = new List<FieldStructure>();
        FieldStructure field = new FieldStructure();
        field.label = 'Name';
        field.apiName = 'Name';
        field.fieldType = 'String';
        fieldList.add(field);
        return fieldList;
    }
    
    @AuraEnabled
    public static String getAIDRecordType(String parentId) {
        String parentRTId = '';
        String recordTypeId = '';
        String relatedObjectName = LeaseWareUtils.getNamespacePrefix()+'Aircraft_Proposal__c';
        String parentObjectName = ID.valueOf(parentId).getSobjectType().getDescribe().getName();

        String query = 'SELECT id, recordTypeId FROM '+parentObjectName+' WHERE Id =:parentId';

        List<sObject> objRecords = Database.query(query);
        parentRTId = (String) objRecords[0].get('recordTypeId');

        if(parentRTId != null){
            if(Schema.getGlobalDescribe().get(parentObjectName).getDescribe().getRecordTypeInfosById().get(parentRTId) != null){
                String parentRTName = Schema.getGlobalDescribe().get(parentObjectName).getDescribe().getRecordTypeInfosById().get(parentRTId).getDeveloperName();
                
                if(Schema.getGlobalDescribe().get(relatedObjectName).getDescribe().getRecordTypeInfosByDeveloperName().containsKey(parentRTName) && Schema.getGlobalDescribe().get(relatedObjectName).getDescribe().getRecordTypeInfosByDeveloperName().get(parentRTName) != null){
                    recordTypeId = Schema.getGlobalDescribe().get(relatedObjectName).getDescribe().getRecordTypeInfosByDeveloperName().get(parentRTName).getRecordTypeId();
                }
            }
        }

        if(recordTypeId != null && recordTypeId != ''){
            return recordTypeId;
        }

        return 'Error';
    }
    
    @AuraEnabled
    public static String getFieldSetBasedOnType(String recordId) {
        List<Marketing_Activity__c> deals = [ select Deal_Type__c from Marketing_Activity__c 
                                             where 
                                             Id = :recordId ];
        if(deals.size() > 0) {
            String dealType = deals[0].Deal_Type__c;
            if(String.isNotEmpty(dealType)) {
                dealType = dealType.deleteWhitespace();
                String fieldSetName = 'AiDEditor'+dealType;
                return fieldSetName;
            }
        }
        return null;
    }
    
    @AuraEnabled
    public static String getAssetInDeal(String recordId, String fieldSetName, String additionalField) {
        if(String.isBlank(recordId)) return null;
        
        List<FieldStructure> fieldStructure;
        if(String.isBlank(fieldSetName)) {
            fieldSetName = getFieldSetBasedOnType(recordId);
            if(fieldSetName == null)
                return null;
            //String defaultFieldSet = LeaseWareUtils.getNamespacePrefix()+'AssetInDealEditor';
            fieldStructure = getFieldSets(fieldSetName);
        }
        else 
            fieldStructure = getFieldSets(fieldSetName);
        		
        if(fieldStructure == null || fieldStructure.size() <= 0)
            return null;//fieldStructure = null;//defaultColumns();
        else{
            fieldStructure = addNameField(fieldStructure);
            //LW-AKG: 12/1820 : Added to load any additional field when passed from Bid Matrix
            if (additionalField != null && additionalField != '') {
                fieldStructure = addAdditionalFields(fieldStructure, additionalField);
            }
        }

        Set<String> fields = new Set<String>();
        for(FieldStructure field: fieldStructure)
            fields.add(field.apiName);
        fields.add('Id');
        
        String field =  String.join(new List<String>(fields),',');
        String queryStr = 'select '+field+' from Aircraft_Proposal__c where Marketing_Activity__c = :recordId';
        
        System.debug('getAssetInDeal queryStr: '+queryStr);
        List<Aircraft_Proposal__c> assetInDeals = database.query(queryStr);
        System.debug('getAssetInDeal assetInDeals: '+assetInDeals.size());
        if(assetInDeals.size() > 0) {
            List<AssetInDealWrapper> assetInDealList = new List<AssetInDealWrapper>();
            for(Aircraft_Proposal__c assetDeal : assetInDeals) {
                AssetInDealWrapper aid = new AssetInDealWrapper();
                List<FieldStructure> fieldStructData = new List<FieldStructure>();
                for(FieldStructure fieldSt: fieldStructure) {
                    FieldStructure fieldStr = new FieldStructure();
                    fieldStr.value = assetDeal.get(fieldSt.apiName);
                    fieldStr.apiName = fieldSt.apiName;
                    fieldStr.label = fieldSt.label;
                    fieldStr.fieldType = fieldSt.fieldType;
                    fieldStructData.add(fieldStr);
                }
                aid.fields = fieldStructData;
                aid.aidId = assetDeal.Id;
                assetInDealList.add(aid);
            }
            return JSON.serialize(assetInDealList);
        }
        return null;
    }

    /**
    * LW-AKG: 12/1820 : Added to load any additional field when passed from Bid Matrix 
    * @description This function is used to add any additional field to be used other then the one available within the fieldSet.
    *
    * @param fieldList - List<FieldStructure>, liats of the fields Wrapper containing all the fields present in the fieldset.
    * @param additionalFields - String, String of comman seperated fields to be added to the list other then the fieldset fields.
    */
    public static List<FieldStructure> addAdditionalFields(List<FieldStructure> fieldList, String additionalFields){
        String objectName = LeaseWareUtils.getNamespacePrefix() + 'Aircraft_Proposal__c';
        Set<String> existingFields = new Set<String>();
        for (FieldStructure fs : fieldList) {
            existingFields.add(fs.apiName);
        }
        
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for (String key : additionalFields.deleteWhiteSpace().split(',')) {
            if (!existingFields.contains(key) && fieldMap.containsKey(key) && fieldMap.get(key) != null) {
                Schema.DescribeFieldResult fieldDesc = fieldMap.get(key).getDescribe();
                FieldStructure field = new FieldStructure();
                field.label = fieldDesc.getLabel();
                field.apiName = fieldDesc.getName();
                field.fieldType = String.valueOf(fieldDesc.getType()).toLowerCase();
                fieldList.add(1, field);
            }
        }
        return fieldList;
    }

    public static List<FieldStructure> getFieldSets(String fieldSetName) {
        String objectName = LeaseWareUtils.getNamespacePrefix() + 'Aircraft_Proposal__c';
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);
        
        System.debug('getFieldSets '+targetType + ' fieldSetName: '+fieldSetName);
        
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Schema.FieldSet fieldSetObj = describe.fieldSets.getMap().get(fieldSetName);
        
        System.debug('getFieldSets describe:'+describe +' fieldSetObj:'+fieldSetObj);
        
        if(fieldSetObj == null)
            return null;
        List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields();
        List<FieldStructure> fieldList = new List<FieldStructure>();
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
        {
            FieldStructure field = new FieldStructure();
            field.apiName = fieldSetMemberObj.getFieldPath();
            field.label = fieldSetMemberObj.getLabel();
            field.fieldType = String.valueOf(fieldSetMemberObj.getType()).toLowerCase();
            if(!field.apiName.equalsIgnoreCase('Name'))
            	fieldList.add(field);
            //system.debug('Required ====>' + fieldSetMemberObj.getRequired());
            //system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
            system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
        }
        system.debug('getFieldSetNames fieldList' + fieldList); //api name
        return fieldList;
    }

    public class FieldStructure{
        @AuraEnabled public String apiName {get; set;}
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public Object value {get; set;}
        @AuraEnabled public String fieldType {get; set;}
        
        public FieldStructure() {
            value = null;
        }
    }
    
    public class AssetInDealWrapper {
        @AuraEnabled public String aidId {get; set;}
        @AuraEnabled public List<FieldStructure> fields {get; set;}
        @AuraEnabled public boolean isSelect {get; set;}
        public AssetInDealWrapper(){
            isSelect = false;
        }
    }
}