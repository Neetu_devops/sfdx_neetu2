@isTest
private class TestCashFlowTriggerHandler {
    @isTest
    static void testMethord_InsertCheck(){
        Test.startTest();
        Ratio_Table__c ratioTable = new Ratio_Table__c(Name= 'Test Ratio');
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;
        
        List<Scenario_Input__c> scenarioInpList = new List<Scenario_Input__c>();
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.ID;
        scenarioInpList.add(scenarioInp);
        
        Scenario_Input__c scenarioInp2 = new Scenario_Input__c();
        scenarioInp2.Name ='Test Secanrio';
        scenarioInp2.Asset__c = aircraft.Id;
        scenarioInp2.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp2.UF_Fee__c = 1 ;
        scenarioInp2.Debt__c= 12;
        scenarioInp2.Operating_Environment_Val__c= 1;
        scenarioInp2.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp2.Interest_Rate__c = 23;
        scenarioInp2.Balloon__c =100;
        scenarioInp2.Ratio_Table__c =ratioTable.Id;
        scenarioInp2.Base_Rent__c = 40;
        scenarioInp2.Estimated_Residual_Value__c =60;
        scenarioInp2.RC_Type__c = 'Base Case';
        scenarioInp2.Investment_Required_Purchase_Price__c=12;
        scenarioInp2.Rent_Escalation_Month__c= 'January';
        scenarioInp2.Ascend__c = 10; 
        scenarioInp2.Avitas__c = 20; 
        scenarioInp2.IBA__c = 30; 
        scenarioInp2.Other__c = 40;
        scenarioInpList.add(scenarioInp2);
        
        if(!scenarioInpList.isEmpty()){
            try{
            	insert scenarioInpList;
            }
            catch(Exception e){
                System.debug('Error: '+e.getMessage()+', Line Number : '+e.getLineNumber());
            }
        }
        Test.stopTest();
    }
    
    @isTest
    static void testMethord_UpdateCheck(){
        Test.startTest();
        Ratio_Table__c ratioTable = new Ratio_Table__c(Name= 'Test Ratio');
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;
        
        List<Scenario_Input__c> scenarioInpList = new List<Scenario_Input__c>();
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.ID;
        scenarioInpList.add(scenarioInp);
        
        Scenario_Input__c scenarioInp2 = new Scenario_Input__c();
        scenarioInp2.Name ='Test Secanrio 2';
        scenarioInp2.Asset__c = aircraft.Id;
        scenarioInp2.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp2.UF_Fee__c = 1 ;
        scenarioInp2.Debt__c= 12;
        scenarioInp2.Operating_Environment_Val__c= 1;
        scenarioInp2.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp2.Interest_Rate__c = 23;
        scenarioInp2.Balloon__c =100;
        scenarioInp2.Ratio_Table__c =ratioTable.Id;
        scenarioInp2.Base_Rent__c = 40;
        scenarioInp2.Estimated_Residual_Value__c =60;
        scenarioInp2.RC_Type__c = 'Base Case';
        scenarioInp2.Investment_Required_Purchase_Price__c=12;
        scenarioInp2.Rent_Escalation_Month__c= 'January';
        scenarioInp2.Ascend__c = 10; 
        scenarioInp2.Avitas__c = 20; 
        scenarioInp2.IBA__c = 30; 
        scenarioInp2.Other__c = 40;
        scenarioInpList.add(scenarioInp2);
        
        if(!scenarioInpList.isEmpty()){
            try{
            	insert scenarioInpList;
                scenarioInpList[1].Name = 'Test Secanrio';
                update scenarioInpList;
            }
            catch(Exception e){
                System.debug('Error: '+e.getMessage()+', Line Number : '+e.getLineNumber());
            }
        }
        Test.stopTest();
    }
}