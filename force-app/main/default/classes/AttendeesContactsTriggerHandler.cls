public class AttendeesContactsTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'AttendeesContactsTriggerHandlerBefore';
    private final string triggerAfter = 'AttendeesContactsTriggerHandlerAfter';
    public AttendeesContactsTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('AttendeesContactsTriggerHandler.beforeInsert(+)');
		
	    system.debug('AttendeesContactsTriggerHandler.beforeInsert(-)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AttendeesContactsTriggerHandler.beforeUpdate(+)');
    	
    	
    	system.debug('AttendeesContactsTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AttendeesContactsTriggerHandler.beforeDelete(+)');
    	updateAttendeeContact();
    	
    	system.debug('AttendeesContactsTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AttendeesContactsTriggerHandler.afterInsert(+)');
    	updateAttendeeContact();
    	system.debug('AttendeesContactsTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AttendeesContactsTriggerHandler.afterUpdate(+)');
    	updateAttendeeContact();
    	
    	system.debug('AttendeesContactsTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AttendeesContactsTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('AttendeesContactsTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AttendeesContactsTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('AttendeesContactsTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	private void updateAttendeeContact(){
		
		
		list<Attendee_Contacts__c> newList = (list<Attendee_Contacts__c>)(trigger.isDelete?trigger.old:trigger.new);
		
		set<id> ILIds = new set<id>();
		set<id> OnDelNoSelection = new set<id>();
		for(Attendee_Contacts__c curRec:newList){
			ILIds.add(curRec.Interaction_Log__c);
			if(trigger.isDelete) OnDelNoSelection.add(curRec.Id);
		}

		
		map<id,Customer_Interaction_Log__c> mapIL = new map<id,Customer_Interaction_Log__c>(
														[select id,	Attendees_List__c from Customer_Interaction_Log__c where id in :ILIds]);
		system.debug('mapIL='+mapIL);												
		for(Customer_Interaction_Log__c currec:mapIL.values()){
			currec.Attendees_List__c =null;
		}
														
		Attendee_Contacts__c[] listAttendee = [select id,Interaction_Log__c,Contact__r.Name from Attendee_Contacts__c where Interaction_Log__c in :ILIds and (id not in :OnDelNoSelection)];
		system.debug('listAttendee='+listAttendee.size());
		system.debug('OnDelNoSelection='+OnDelNoSelection);
		for(Attendee_Contacts__c curRec:listAttendee){
			if(mapIL.get(curRec.Interaction_Log__c).Attendees_List__c==null) mapIL.get(curRec.Interaction_Log__c).Attendees_List__c = curRec.Contact__r.Name; 
			else mapIL.get(curRec.Interaction_Log__c).Attendees_List__c += ', ' +curRec.Contact__r.Name;
		}
		
		if(!mapIL.isEmpty()){
			// only update Attendee list fields
			LeaseWareUtils.TriggerDisabledFlag=true;
			update mapIL.values();
			LeaseWareUtils.TriggerDisabledFlag=false;
		}
	}
}