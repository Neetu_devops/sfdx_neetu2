@isTest
public class TestAircraftWrapper {
@AuraEnabled public Integer aircraftVariantCount {get; set;}
        @AuraEnabled public Integer aircraftEngineCount {get; set;}
        @AuraEnabled public Integer vintageMinRange {get; set;}
        @AuraEnabled public Integer vintageMaxRange {get; set;}
        
        @AuraEnabled public String aircraftType {get; set;}
        @AuraEnabled public String aircraftVariant {get; set;}
        @AuraEnabled public String aircraftEngine {get; set;}
        @AuraEnabled public String aircraftVintage {get; set;}
        @AuraEnabled public Decimal aircraftMtow {get; set;}
        
        @AuraEnabled public boolean isExistingCustomer {get; set;}
        
        @AuraEnabled public Map<String,String> leasesList {get; set;}
        @AuraEnabled public List<String> leasesVsOwned {get; set;}
        @AuraEnabled public Integer leasedCount {get; set;}
	@isTest
    public static void loadTestCases() {
        AircraftWrapper.Properties obj1 = new AircraftWrapper.Properties();
        obj1.aircraft = '';
        obj1.rank = 2;
        obj1.color = '';
        obj1.size = 4;
        obj1.fleetId = '';
        obj1.operatorId = '';
        obj1.operatorName = '';
        obj1.aircraftTypeCount = 8;
        obj1.aircraftVariantCount = 5;
        obj1.aircraftEngineCount = 6;
        obj1.vintageMinRange = 0;
        obj1.vintageMaxRange = 1000;
        obj1.aircraftType = '';
        obj1.aircraftVariant = '';
        obj1.aircraftEngine = '';
        obj1.aircraftVintage = '';
        obj1.aircraftMtow = 9;
        obj1.isExistingCustomer = false;
        obj1.leasesList = null;
        obj1.leasesVsOwned = null;
        obj1.weight = 10;
        obj1.leasedCount = 9;
        
        AircraftWrapper.Properties obj2 = new AircraftWrapper.Properties();
        obj2.weight = 8;
        
        obj1.compareTo(obj2);
        
        AircraftWrapper.AircraftWeightage aircraftWeight = new AircraftWrapper.AircraftWeightage();
        aircraftWeight.aircraftEngineCount = 9;
        aircraftWeight.aircraftTypeCount = 8;
        aircraftWeight.aircraftVariantCount = 5;
        aircraftWeight.isExistingCustomer = false;
        aircraftWeight.vintageMaxRange = 9;
        aircraftWeight.vintageMinRange = 0;
        aircraftWeight.weight =8;
    }
}