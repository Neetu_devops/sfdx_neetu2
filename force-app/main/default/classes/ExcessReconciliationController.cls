public class ExcessReconciliationController {
    @auraEnabled
    public static ExcessUtilizationDataWrapper getData(String recordId, String type){
        
        system.debug('default ---- > ' + Type);  
        
        ExcessUtilizationDataWrapper reconciliationSchedule = new ExcessUtilizationDataWrapper();
        
        List<Reconciliation_Schedule__c> rSRecordList = [Select Id, Not_Maintenance_Reserve__c, To_Date__c, Contractual_Ratio__c, From_Date__c,
                                                         Lease__c, Lease__r.Name, Status__c, Type__c, 
                                                         Lease__r.Contractual_FH_FC_Ratio__c,
                                                         Lease__r.Y_Hidden_Supplemental_Rent_Count__c,
                                                         Lease__r.Aircraft__c,
                                                         (select id from Invoices__r
                                                         where status__c = 'Pending')
                                                         from Reconciliation_Schedule__c Where Id =: recordId];
        
        
        Boolean isSRBased ;
        
        //this condition will be true when user switches from the UI.
        if(type != null){
            isSRBased =  type == 'SR' ? True : false;
            reconciliationSchedule.nonMR = !isSRBased;
        }else{
            //this case is for open schedules and don't have any previous record,
            //in that this condition will decide whether it is SR based or Assembly Based.
        	isSRBased = rSRecordList[0].Lease__r.Y_Hidden_Supplemental_Rent_Count__c > 0 ? True: false;
			
			if(rSRecordList[0].Status__c == 'Open' && !isSRBased){
				reconciliationSchedule.nonMR = true;
			}else{
				reconciliationSchedule.nonMR = rSRecordList[0].Not_Maintenance_Reserve__c;
			}
            
        }    
        														
		Map<Id, Reconciliation_Line_Item__c> existingRecordsMap = new Map<Id, Reconciliation_Line_Item__c>();
		
        // To populate default value for Excess Utlization Reconciliation
        if(rSRecordList[0].Status__c == 'Open' || rSRecordList[0].Status__c == 'Complete'){
            
            //Only for first time loading.
            if(type == null){	
            
                String leaseId = rSRecordList[0].Lease__c;
                Date startDate = rSRecordList[0].From_Date__c.toStartOfMonth();
                
                String query = 'Select Id, Not_Maintenance_Reserve__c, ';
                query += '(Select Id, Supplemental_Rent__c,Assembly__c, Excess_Rate_Unit__c, Usage_Limit__c, Excess_Rate__c, ';
                query += 'Comments__c from Reconciliation_Line_Items__r Order By Supplemental_Rent__r.Name) ';
                query += 'from Reconciliation_Schedule__c ';
                
                // if status open then check for previous record
                if(rSRecordList[0].Status__c == 'Open' ){
                    query += 'Where Lease__c =: leaseId AND To_Date__c <: startDate ';
                }else{
                    query += 'Where Id  =: recordId ';
                }
                query += 'AND Type__c = \'Excess Utilization\' Order By To_Date__c Desc Limit 1';
                
                //these records can be from previous Reconciliation or the current Reconciliation schedule.
                for(Reconciliation_Schedule__c previousRSRecordList : (List<Reconciliation_Schedule__c>)Database.query(query)){
                             
                    isSRBased = !previousRSRecordList.Not_Maintenance_Reserve__c; 
                    
                    for(Reconciliation_Line_Item__c recLineItem : previousRSRecordList.Reconciliation_Line_Items__r){
                        
                        Id key = previousRSRecordList.Not_Maintenance_Reserve__c ? recLineItem.Assembly__c :
                        recLineItem.Supplemental_Rent__c;						
                        existingRecordsMap.put(key, recLineItem);
                    }   
                }
        	}
        }
        
        Set<Date> monthEndingSet = new Set<Date>();
        
        //Map to hold total FH, FC during the period and Assembly level information.
        Map<Id, ExcessUtilizationItemWrapper> summaryDataMap = new Map<Id, ExcessUtilizationItemWrapper>();

        if(isSRBased){
            
            //calculate the total fh , fc etc from the Utilizations filed within the period.
            for(Assembly_MR_Rate__c sRObj : [select Id, Name, 
                                             Assembly_Lkp__c, Assembly_Lkp__r.Type__c,
                                             Assembly_Lkp__r.Serial_Number__c,
                                             (Select Id, FH_F__c, FC_F__c, 
                                              Utilization_Report__r.Month_Ending__c                                         
                                              from Assembly_Utilization__r where 
                                              (Type_F__c = 'Actual' Or Type_F__c = 'True Up Gross')
                                              AND Utilization_Report__r.Status__c = 'Approved By Lessor'
                                              AND Utilization_Report__r.Month_Ending__c <=: rSRecordList[0].To_Date__c.addMonths(1).toStartOfMonth().addDays(-1)
                                              AND Utilization_Report__r.Month_Ending__c >=: rSRecordList[0].From_Date__c.toStartOfMonth() 
                                              Order By CreatedDate Desc)
                                             from Assembly_MR_Rate__c Where Lease__c =: rSRecordList[0].Lease__c Order By Name]){
                   
                    
                
                for(Utilization_Report_List_Item__c urliObj : sRObj.Assembly_Utilization__r){
                
                    if(!monthEndingSet.contains(urliObj.Utilization_Report__r.Month_Ending__c)){
                        
                        if(!summaryDataMap.containsKey(sRObj.Id)){
                            summaryDataMap.put(sRObj.Id, new ExcessUtilizationItemWrapper(sRObj.Name, sRObj.Assembly_Lkp__c,sRObj.Assembly_Lkp__r.Type__c,
                                             												sRObj.Assembly_Lkp__r.Serial_Number__c,
                                                                                         	sRObj.Id));
                        }
                        
                        summaryDataMap.get(sRObj.Id).totalFH += urliObj.FH_F__c;
                        summaryDataMap.get(sRObj.Id).totalFC += urliObj.FC_F__c;
                        
                        monthEndingSet.add(urliObj.Utilization_Report__r.Month_Ending__c);
                    }	
                }
                monthEndingSet.clear();
            }
        }else{
            
             //calculate the total fh , fc etc from the Utilizations filed within the period.
            for(Constituent_Assembly__c assemblyObj : [select Id, Name, Type__c,Serial_Number__c,
                                                         (Select Id, Running_Hours_During_Month__c, Cycles_During_Month__c, 
                                                          Utilization_Report__r.Month_Ending__c                                         
                                                          from URA_List_Items__r where 
                                                          (Type_F__c = 'Actual' Or Type_F__c = 'True Up Gross')
                                                          AND Utilization_Report__r.Status__c = 'Approved By Lessor'
                                                          AND Utilization_Report__r.Month_Ending__c <=: rSRecordList[0].To_Date__c.addMonths(1).toStartOfMonth().addDays(-1)
                                                          AND Utilization_Report__r.Month_Ending__c >=: rSRecordList[0].From_Date__c.toStartOfMonth() 
                                                          Order By CreatedDate Desc)
                                                         from Constituent_Assembly__c Where Attached_Aircraft__c =: rSRecordList[0].Lease__r.Aircraft__c Order By Name]){
                   
	
                                                             	system.debug('entered--->');
                
                for(Assembly_Utilization__c urliObj : assemblyObj.URA_List_Items__r){
                
                    if(!monthEndingSet.contains(urliObj.Utilization_Report__r.Month_Ending__c)){
                        
                        if(!summaryDataMap.containsKey(assemblyObj.Id)){
                            summaryDataMap.put(assemblyObj.Id, new ExcessUtilizationItemWrapper(assemblyObj.Type__c, assemblyObj.Id, assemblyObj.Type__c,
                                                                                                 assemblyObj.Serial_Number__c, null));
                        }
                        summaryDataMap.get(assemblyObj.Id).totalFH += urliObj.Running_Hours_During_Month__c;
                        summaryDataMap.get(assemblyObj.Id).totalFC += urliObj.Cycles_During_Month__c;
                        
                        monthEndingSet.add(urliObj.Utilization_Report__r.Month_Ending__c);
                    }	
                }
                monthEndingSet.clear();
            }
        }
        

        reconciliationSchedule.hasSR = rSRecordList[0].Lease__r.Y_Hidden_Supplemental_Rent_Count__c > 0 ? True: false;
        reconciliationSchedule.periodStart = rSRecordList[0].From_Date__c;
        reconciliationSchedule.periodEnd = rSRecordList[0].To_Date__c;        
        reconciliationSchedule.reconciliationScheduleId = rSRecordList[0].Id; 
        reconciliationSchedule.leaseId = rSRecordList[0].Lease__c;
        reconciliationSchedule.lease = rSRecordList[0].Lease__r.Name;
        reconciliationSchedule.invoiceId = rSRecordList[0].Invoices__r.size() > 0 ?
                                                  rSRecordList[0].Invoices__r[0].Id : null;
        
        if(rSRecordList[0].Status__c == 'Open'){
            reconciliationSchedule.contractulRatio = rSRecordList[0].Lease__r.Contractual_FH_FC_Ratio__c;
        }else{
            reconciliationSchedule.contractulRatio = rSRecordList[0].Contractual_Ratio__c;
        }
        
        reconciliationSchedule.reconType = rSRecordList[0].Type__c;
        
        system.debug('summaryDataMap.length ---> ' + summaryDataMap.size());
          system.debug('summaryDataMap ---> ' + summaryDataMap);
        
        for(Id key : summaryDataMap.keySet()){
            
			if(existingRecordsMap.containsKey(key)){
				ExcessUtilizationItemWrapper lineItemValues = new ExcessUtilizationItemWrapper();
                lineItemValues = summaryDataMap.get(key);
				lineItemValues.selectedAssembly = false;
                lineItemValues.excessRate = existingRecordsMap.get(key).Excess_Rate__c;
                lineItemValues.excessUnitBase = existingRecordsMap.get(key).Excess_Rate_Unit__c;
                lineItemValues.maxUnits = existingRecordsMap.get(key).Usage_Limit__c;
                lineItemValues.comments = existingRecordsMap.get(key).Comments__c;
                
                if(existingRecordsMap.get(key).Excess_Rate_Unit__c == 'Hours'){
                    lineItemValues.excessUnits = summaryDataMap.get(key).totalFH - existingRecordsMap.get(key).Usage_Limit__c;
                }else{
                    lineItemValues.excessUnits = summaryDataMap.get(key).totalFC - existingRecordsMap.get(key).Usage_Limit__c;
                }
                
                if(lineItemValues.excessUnits < 0){
                    lineItemValues.excessUnits = null;
                }else{
                    lineItemValues.totalAmount = (lineItemValues.excessUnits * lineItemValues.excessRate).setScale(2, RoundingMode.HALF_UP);
                }

                reconciliationSchedule.excessUtilizationtableData.add(lineItemValues);
            }else{

            	reconciliationSchedule.excessUtilizationtableData.add(summaryDataMap.get(key));
        	}
        }
        system.debug('reconciliationSchedule.excessUtilizationtableData --->' + reconciliationSchedule.excessUtilizationtableData.size());
        return reconciliationSchedule;
    }
    
    @AuraEnabled
    public static Id saveData(String data, Date invoiceDate, Date paymentDate) {
      
        Map<Id,Reconciliation_Line_Item__c> existingLineItemsMap = new Map<Id,Reconciliation_Line_Item__c>();
        ExcessUtilizationDataWrapper reconciliationDataWrapper = (ExcessUtilizationDataWrapper)System.JSON.deserialize(data, ExcessUtilizationDataWrapper.class);
        List<ExcessUtilizationItemWrapper> wrapperData = reconciliationDataWrapper.excessUtilizationtableData;
        
        if(reconciliationDataWrapper.reconType != 'Open'){
        
        for(Reconciliation_Line_Item__c reconciliationLineItem : [Select id, Supplemental_Rent__c, Assembly__c,
                                                                  
                                                                      (Select id from Invoice_Line_items__r
                                                                      where Invoice__r.status__c = 'Pending')
                                                                  
                                                                  from Reconciliation_Line_Item__c
                                                                  where 
                                                                  Reconciliation_Schedule__c =: reconciliationDataWrapper.reconciliationScheduleId]){
                                                                          
                                                                  Id key = reconciliationDataWrapper.nonMR ? reconciliationLineItem.Assembly__c  :
                                                                      reconciliationLineItem.Supplemental_Rent__c;
                                                                  
                                                                  // SR lookup can be null in case of Non MR case.    
                                                                  if(key != null){
                                                                  	existingLineItemsMap.put(key,reconciliationLineItem);
                                                                  }  
                                                             }
        }
        
        Id InvRecordId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('MR_Reconciliation').getRecordTypeId();
        
        Invoice__c invoice = new Invoice__c(name= 'sys gen', Month_Ending__c = reconciliationDataWrapper.periodEnd.addMonths(1).toStartOfMonth().addDays(-1),
                                            Lease__c = reconciliationDataWrapper.leaseId,
                                            Reconciliation_Start_Date__c = reconciliationDataWrapper.periodStart.toStartOfMonth(), 
                                            Reconciliation_End_Date__c = reconciliationDataWrapper.periodEnd.addMonths(1).toStartOfMonth().addDays(-1),
                                            RecordTypeId= InvRecordId,
                                            Invoice_Type__c = (reconciliationDataWrapper.nonMR ? 'Non MR' : 'Aircraft MR') ,
                                            Invoice_Date__c = invoiceDate,
                                            Date_of_MR_Payment_Due__c = paymentDate,
                                            Id = reconciliationDataWrapper.invoiceId,
                                           Reconciliation_Schedule__c = reconciliationDataWrapper.reconciliationScheduleId);
        
        LeaseWareUtils.setFromTrigger('AllowUpdationOnInvoiceType');
        upsert invoice;
        LeaseWareUtils.unsetTriggers('AllowUpdationOnInvoiceType');
        
        String apiNameOfReconciliationRecordType;
        
        Id reconciliationRecordTypeId = Schema.SObjectType.Reconciliation_Schedule__c.getRecordTypeInfosByDeveloperName().get('Excess_Utilization').getRecordTypeId();
         
        Reconciliation_Schedule__c reconciliationRecord = new Reconciliation_Schedule__c();
        reconciliationRecord.Id = reconciliationDataWrapper.reconciliationScheduleId;
        reconciliationRecord.RecordTypeId = reconciliationRecordTypeId;
        reconciliationRecord.Reconciliation_Type__c = 'Excess Utilization';
        reconciliationRecord.Status__c = 'Complete';
        reconciliationRecord.Contractual_Ratio__c = reconciliationDataWrapper.contractulRatio;
        reconciliationRecord.Not_Maintenance_Reserve__c = reconciliationDataWrapper.nonMR;
        LeaseWareUtils.setFromTrigger('AllowUpdateOnReconciliationSchedule'); // to skip validation.
        update reconciliationRecord;
        LeaseWareUtils.unsetTriggers('AllowUpdateOnReconciliationSchedule');
        
        List<Invoice_Line_Item__c> invoiceItems = new List<Invoice_Line_Item__c>();
        List<Reconciliation_Line_Item__c> reconciliationLineItems = new List<Reconciliation_Line_Item__c>();
        
        Id InvLineRecordId = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('MR_Reconciliation').getRecordTypeId();
        Id reconciliationLineItemRecordTypeId = Schema.SObjectType.Reconciliation_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Excess_Utilization').getRecordTypeId();
        
        for(ExcessUtilizationItemWrapper record : reconciliationDataWrapper.excessUtilizationtableData) {
            
            Id existingInvoiceLineItemId;
            
            Reconciliation_Line_Item__c reconciliationLineItem = new
                Reconciliation_Line_Item__c(Name = record.name, 
                                            Reconciliation_Schedule__c = reconciliationDataWrapper.reconciliationScheduleId,
                                            Supplemental_Rent__c = record.assemblyMRId,
                                            Assembly__c = record.associatedAssemblyId,
                                            Excess_Rate__c = record.excessRate,
                                            Excess_Rate_Unit__c = record.excessUnitBase,
                                            Usage_Limit__c = record.maxUnits,
                                            Total_FH_for_the_Period__c = record.totalFH,
                                            Total_FC_for_the_Period__c = record.totalFC,
                                            Excess_Amount__c = record.totalAmount,
                                            Comments__c = record.comments,
                                            RecordTypeId = reconciliationLineItemRecordTypeId);
            
            //to update the record, get the Id on the basis of selected type i.e either SR or Assembly.
            if(reconciliationDataWrapper.nonMR){
                reconciliationLineItem.Id = (existingLineItemsMap.containsKey(record.associatedAssemblyId) ? existingLineItemsMap.get(record.associatedAssemblyId).Id :
                                                  null);
                
                 existingInvoiceLineItemId = existingLineItemsMap.containsKey(record.associatedAssemblyId)  && 
                                        existingLineItemsMap.get(record.associatedAssemblyId).Invoice_line_items__r.size() > 0 ? 
                                        existingLineItemsMap.get(record.associatedAssemblyId).Invoice_line_items__r[0].Id : null;
                
            }else{
                
               	reconciliationLineItem.Id = (existingLineItemsMap.containsKey(record.assemblyMRId) ? existingLineItemsMap.get(record.assemblyMRId).Id :
                                                  null) ;
                
                existingInvoiceLineItemId = existingLineItemsMap.containsKey(record.assemblyMRId)  && 
                    existingLineItemsMap.get(record.assemblyMRId).Invoice_line_items__r.size() > 0 ? 
                    existingLineItemsMap.get(record.assemblyMRId).Invoice_line_items__r[0].Id : null;
            }
            
            reconciliationLineItems.add(reconciliationLineItem);
            
            Invoice_Line_Item__c lineInvItem = new 
                Invoice_Line_Item__c(Name=record.name +' ('+ record.serialNumber + ')', 
                                     Invoice__c = invoice.id,
                                     Amount_Due__c = leasewareutils.ZeroIfNull(record.totalAmount),
                                     RecordTypeId=InvLineRecordId, Assembly__c = record.associatedAssemblyId,
                                     Id = existingInvoiceLineItemId,
                                     Comments__c = record.comments,
                                     Assembly_MR_Info__c = record.assemblyMRId,
                                     Reconciliation_Line_Item__r = reconciliationLineItem );
            
            invoiceItems.add(lineInvItem);
        }
        
        if(reconciliationLineItems.size() > 0) {
            upsert reconciliationLineItems;
        }
        
        //map inserted Invoice line item with Reconciliation line item.
        if(invoiceItems.size() > 0){
            for(Invoice_Line_Item__c invoiceLineItem : invoiceItems){
                invoiceLineItem.Reconciliation_Line_Item__c = invoiceLineItem.Reconciliation_Line_Item__r.Id;
            }
            upsert invoiceItems;
        }
        
        //delete the line items which not in used anymore.
        List<Reconciliation_Line_Item__c> reconciliationLineItemsToDelete = [Select id from Reconciliation_Line_Item__c
                                            where Reconciliation_Schedule__c =: reconciliationDataWrapper.reconciliationScheduleId
                                          	And Id Not in : reconciliationLineItems];
        
        if(reconciliationLineItemsToDelete.size()>0){
        
            List<Invoice_Line_Item__c> invoiceItemsToDelete = [Select id from Invoice_Line_item__c 
                                                               where Invoice__r.status__c = 'Pending' 
                                    					And Reconciliation_line_item__c in : reconciliationLineItemsToDelete];
            
            if(invoiceItemsToDelete.size()>0){
            	delete invoiceItemsToDelete;
            }
            
            delete reconciliationLineItemsToDelete;
        }
        
        return invoice.Id;
    }
    
    public class ExcessUtilizationDataWrapper{
        @AuraEnabled public Boolean nonMR{get;set;}
        @AuraEnabled public Boolean hasSR{get;set;}
        @AuraEnabled public Id leaseId{get;set;}
        @AuraEnabled public String lease{get; set;}
        @AuraEnabled public Id reconciliationScheduleId{get;set;}
        @AuraEnabled public Date periodStart{get; set;}
        @AuraEnabled public Date periodEnd{get; set;}
        @AuraEnabled public Id invoiceId{get; set;}
        @AuraEnabled public String reconType{get; set;}
        @AuraEnabled public Decimal totalAmountDue{get;set;}
        @AuraEnabled public Decimal contractulRatio{get;set;}
        
        @auraEnabled public List<ExcessUtilizationItemWrapper> excessUtilizationtableData {get;set;}
        
        
        Public ExcessUtilizationDataWrapper(){
            excessUtilizationtableData = new List<ExcessUtilizationItemWrapper>();
        }
    }    
    public class ExcessUtilizationItemWrapper{
        
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String assemblyMRId{get;set;}
        @AuraEnabled public String associatedAssemblyId{get;set;}
        @AuraEnabled public String serialNumber{get; set;}
        @AuraEnabled public Decimal totalFH{get; set;}
        @AuraEnabled public Decimal totalFC{get; set;}
        @auraEnabled public Decimal maxUnits {get;set;}
        @AuraEnabled public String excessUnitBase{get;set;}
        @AuraEnabled public Decimal excessUnits{get;set;}
        @AuraEnabled public Decimal excessRate{get;set;}
        @AuraEnabled public Decimal totalAmount{get;set;}
        @AuraEnabled public String comments{get;set;}
        @AuraEnabled public String associatedAssemblyType{get;set;}
        @AuraEnabled public Boolean selectedAssembly{get;set;}
        
        Public ExcessUtilizationItemWrapper(){
            selectedAssembly = false;
        }
        
        Public ExcessUtilizationItemWrapper(String name, String assemblyId, String assemblyType, String serialNumber,
                                           String supplementalRentId){
            
            this.associatedAssemblyId = assemblyId;
            this.serialNumber = serialNumber;
            this.associatedAssemblyType = assemblyType;
            this.name = name;
            this.assemblyMRId = supplementalRentId;
            selectedAssembly = false; 
            totalFH = 0.00;
            totalFC = 0;                                   
            
        }
    }
}