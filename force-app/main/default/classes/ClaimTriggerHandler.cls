public class ClaimTriggerHandler implements ITrigger{
    
    // Constructor
    private final string triggerBefore = 'ClaimTriggerHandlerBefore';
    private final string triggerAfter = 'ClaimTriggerHandlerAfter';
    
    public ClaimTriggerHandler()
    {   

    }
    //Making functionality more generic.
   
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        system.debug('ClaimTriggerHandler.bulkBefore(+)');
        system.debug('ClaimTriggerHandler.bulkBefore(-)');
    }
    public void bulkAfter()
    {
        system.debug('ClaimTriggerHandler.bulkAfter(+)');
        system.debug('ClaimTriggerHandler.bulkAfter(-)');
    }   
    public void beforeInsert()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerBefore)){ return; } 
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('ClaimTriggerHandler.beforeInsert(+)');
        callBeforeInsUpd();
        system.debug('ClaimTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)){ return; } 
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('ClaimTriggerHandler.beforeUpdate(+)');
        callBeforeInsUpd();
        system.debug('ClaimTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        if(LeaseWareUtils.isFromTrigger(triggerBefore)){ return; } 
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('ClaimTriggerHandler.beforeDelete(+)');
        
        List<Payable__c> lstPyToDel = new List<Payable__c>();
        List<Payable__c> lstPayables = [select id,Claim__c, Status__c from Payable__c where Claim__c in :trigger.oldMap.Keyset()];
        for(Payable__c py: lstPayables)
        {
            if(py.Status__c =='Pending' || py.Status__c =='Approved' || py.Status__c == 'Cancelled-Pending'){
                trigger.oldMap.get(py.Claim__c).addError('The claim cannot be deleted due to one or more Payables relating to the claim. Please Decline or Cancel the payable/s to proceed with the deletion.');
                continue;
            }
            else{                
                lstPyToDel.add(py);
            }
        }

        if(lstPyToDel.size() > 0){
            //If the Payable Deletion custom setting is ON, delete all the related payables,
            //else the payables and payouts will be delinked from the claim (i.e) claim lookup would be cleared on the records
            string deletePayable = LeaseWareUtils.getLWSetup_CS('PAYABLE_DELETION');        
            if(!(null==deletePayable || ''.equals(deletePayable)||'OFF'.equals(deletePayable))){
                LeasewareUtils.clearFromTrigger();
                //call the trigger to take care of deletion of declined/cancelled payouts and related account payable records if any            
                //and also update the supplemental rent transactions and operational transaction register 
                delete lstPyToDel;  
             }
        }
        
        system.debug('ClaimTriggerHandler.beforeDelete(-)');
    }
     
    public void afterInsert()
    {
		if(LeaseWareUtils.isFromTrigger(triggerAfter)){ return; } 
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('ClaimTriggerHandler.afterInsert(+)');
        UpdSREReqIdOnHET();
        system.debug('ClaimTriggerHandler.afterInsert(-)');

    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)){ return; } 
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('ClaimTriggerHandler.afterUpdate(+)');
        UpdSREReqIdOnHET();
        system.debug('ClaimTriggerHandler.afterUpdate(-)');
    }
     
    public void afterDelete()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)){ return; } 
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('ClaimTriggerHandler.afterDelete(+)');
        DelSREReqIdOnHET();
        system.debug('ClaimTriggerHandler.afterDelete(-)');
    }

    public void afterUnDelete()
    {
        system.debug('ClaimTriggerHandler.afterUnDelete(+)');
        system.debug('ClaimTriggerHandler.afterUnDelete(-)');
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
        
    }
    
    // Before Insert, Before Update
    private void callBeforeInsUpd(){
        // Implement Validation - A claim is already created for this Event. Please select a different Historical Event.
        system.debug('ClaimTriggerHandler.callBeforeInsUpd(+)');
        list<Claims__c> listClaims =(list<Claims__c>)trigger.new;
        map<string,id> mapSuppRentHisEvent= new map<string,id>();
        set<Id> setSuppRentId= new set<Id>();
        for(Claims__c curClaim: listClaims){
            setSuppRentId.add(curClaim.Supplemental_Rent__c);
        }
        for(Claims__c existClaim: [select id,name,Supplemental_Rent__c,Event__c from Claims__c where Supplemental_Rent__c IN : setSuppRentId]){
            mapSuppRentHisEvent.put(existClaim.Supplemental_Rent__c+''+existClaim.Event__c,existClaim.Event__c);
        }
        Claims__c oldClaimRec;
        for(Claims__c newClaim: listClaims){
            if(trigger.IsInsert && newClaim.Event__c!=Null){
                if(mapSuppRentHisEvent.containsKey(newClaim.Supplemental_Rent__c+''+newClaim.Event__c)){
                    newClaim.Event__c.adderror('A claim is already created for this Event. Please select a different Assembly Event.');
                }
            }
            else if(trigger.IsUpdate){
                system.debug('Claim- Update case');
                oldClaimRec = (Claims__c)trigger.oldMap.get(newClaim.id);
                if((oldClaimRec.Event__c==Null && newClaim.Event__c!=Null)|| (oldClaimRec.Event__c!=Null && newClaim.Event__c!=Null && oldClaimRec.Event__c!=newClaim.Event__c)){
                    if(mapSuppRentHisEvent.containsKey(newClaim.Supplemental_Rent__c+''+newClaim.Event__c)){
                        newClaim.Event__c.adderror('A claim is already created for this Event. Please select a different Assembly Event.');
                    }
                } 
                if(oldClaimRec.Approved_Claim_Amount__c != newClaim.Approved_Claim_Amount__c && 'Approved'.equals(newClaim.Claim_Status__c) && newClaim.Claim_Status__c == oldClaimRec.Claim_Status__c){
                    newClaim.addError('The claim cannot be modified once approved');
                }
            }
        }
        system.debug('ClaimTriggerHandler.callBeforeInsUpd(-)');
    }

    //after Insert, After Update
    //Update SuppRentEventReq and Claim ID on Historical Event Task - (reason: deprecated lease field on Historical Event Task)
    private void UpdSREReqIdOnHET(){
        system.debug('UpdSREReqIdOnHET(+)');
        list<Claims__c> listClaims =(list<Claims__c>) trigger.new;

        set<ID> SetSuppRentHEID = new set<ID>(); //Set of Supplmental Rent and Old Historical Event Ids
        map<Id,string> mapGetClaimSuppRentID = new map<Id,string>();
        Claims__c oldClaimRec;
        for(Claims__c curClaimRec: listClaims){
			if(trigger.IsInsert && curClaimRec.Event__c!=Null ) {
                system.debug('Claim- Insert case='+ curClaimRec.Event__c);
                mapGetClaimSuppRentID.put(curClaimRec.Event__c, curClaimRec.id+'=='+curClaimRec.Supplemental_Rent__c);
            }
            else if(trigger.IsUpdate){
                system.debug('Claim- Update case='+ curClaimRec.Event__c);
                oldClaimRec = (Claims__c)trigger.oldMap.get(curClaimRec.id);
                if((oldClaimRec.Event__c==Null && curClaimRec.Event__c!=Null)|| (oldClaimRec.Event__c!=Null && curClaimRec.Event__c!=Null && oldClaimRec.Event__c!=curClaimRec.Event__c) ){
                    mapGetClaimSuppRentID.put(curClaimRec.Event__c, curClaimRec.id+'=='+curClaimRec.Supplemental_Rent__c);
                }
                else if(oldClaimRec.Event__c!=Null && curClaimRec.Event__c==Null ){
                    system.debug('=Reset Case='+oldClaimRec.Event__c);
                    SetSuppRentHEID.add(oldClaimRec.Event__c); // Find those HET which want to nullify Supplement Rent Event Req and Claim ID
                }                
            }
            SetSuppRentHEID.add(curClaimRec.Supplemental_Rent__c);
        }
        system.debug('size of mapGetClaimSuppRentID='+mapGetClaimSuppRentID.size());
        system.debug('size of SetSuppRentHEID='+SetSuppRentHEID.size());
        List<Supp_Rent_Event_Reqmnt__c> ListSuppRentEvntReq = new List<Supp_Rent_Event_Reqmnt__c>();
        List<Historical_Event_Task__c> listHistEventTask=new List<Historical_Event_Task__c>(); 
        if(SetSuppRentHEID.size()>0){
            ListSuppRentEvntReq = [ Select id,Supplemental_Rent__c,Maintenance_Event_Task__c 
                                    from Supp_Rent_Event_Reqmnt__c
                                    where Supplemental_Rent__c in : SetSuppRentHEID
                                  ];
        }
        system.debug('size of ListSuppRentEvntReq='+ListSuppRentEvntReq.size());

        if(mapGetClaimSuppRentID.size()>0 || SetSuppRentHEID.size()>0){
            listHistEventTask=[ select id,name,SuppRentEventReqmnt__c,Historical_Event__c,Historical_Event__r.Projected_Event__c,Maintenance_Event_Task__c 
                                from Historical_Event_Task__c 
                                where Historical_Event__c IN: mapGetClaimSuppRentID.keySet() OR Historical_Event__c IN: SetSuppRentHEID]; 
        }
        system.debug('size of listHistEventTask='+listHistEventTask.size());
        
        map<string, ID> mapSRMET_SRERIds = new map<string, ID>(); // This mapSRMET_SRERIds used to stamp Supp Rent Event Req id in HistEventTask Record using SuppRentID+ METID
        for( Supp_Rent_Event_Reqmnt__c curSRER : ListSuppRentEvntReq){
            mapSRMET_SRERIds.put(curSRER.Supplemental_Rent__c+''+curSRER.Maintenance_Event_Task__c,curSRER.id);
        }
        system.debug('size of mapSRMET_SRERIds='+mapSRMET_SRERIds.size());
                
        List<Historical_Event_Task__c> ListUpdHistEventTask=new List<Historical_Event_Task__c>();
		if(listHistEventTask.size()>0){
            for(Historical_Event_Task__c curHistEventTask : listHistEventTask){
                system.debug('curHistEventTask='+curHistEventTask.id);
                if(mapGetClaimSuppRentID.containsKey(curHistEventTask.Historical_Event__c)){
                    if(mapSRMET_SRERIds.containsKey(mapGetClaimSuppRentID.get(curHistEventTask.Historical_Event__c).SubStringAfter('==')+''+curHistEventTask.Maintenance_Event_Task__c)){
                        curHistEventTask.SuppRentEventReqmnt__c= mapSRMET_SRERIds.get(mapGetClaimSuppRentID.get(curHistEventTask.Historical_Event__c).SubStringAfter('==')+''+ curHistEventTask.Maintenance_Event_Task__c);
                        curHistEventTask.Claim__c=mapGetClaimSuppRentID.get(curHistEventTask.Historical_Event__c).SubStringBefore('==');
                        ListUpdHistEventTask.add(curHistEventTask);
                    }
                }
                else if(SetSuppRentHEID.contains(curHistEventTask.Historical_Event__c)){
                    curHistEventTask.SuppRentEventReqmnt__c=Null;
                    curHistEventTask.Claim__c=Null;
                    ListUpdHistEventTask.add(curHistEventTask);
                }
            }
        }
        system.debug('size of ListUpdHistEventTask='+ListUpdHistEventTask.size());
        //system.debug('ListUpdHistEventTask='+ JSON.serializePretty(ListUpdHistEventTask));
        if(ListUpdHistEventTask.size() > 0) Update ListUpdHistEventTask;
        system.debug('UpdSREReqIdOnHET(-)');
    }
    
    //after Delete
    //Reset SuppRentEventReq and Claim ID to Null on Historical Event Task
    private void DelSREReqIdOnHET(){
        list<Claims__c> listClaims =(list<Claims__c>) trigger.old;
        set<ID> SetHistEvntID = new set<ID>(); //Set of Historical Event Ids
        for(Claims__c curClaimRec: listClaims){
            SetHistEvntID.add(curClaimRec.Event__c);
        }
        List<Historical_Event_Task__c> listHistEventTask=new List<Historical_Event_Task__c>(); 
        if(SetHistEvntID.size()>0){
            listHistEventTask=[ select id,name,SuppRentEventReqmnt__c,Claim__c from Historical_Event_Task__c 
                                where Historical_Event__c IN: SetHistEvntID ]; 
        }
        system.debug('size of listHistEventTask='+listHistEventTask.size());

		if(listHistEventTask.size()>0){
            for(Historical_Event_Task__c curHistEventTask : listHistEventTask){
                curHistEventTask.SuppRentEventReqmnt__c=Null;
                curHistEventTask.Claim__c=Null;
            }
        }
        if(listHistEventTask.size() > 0) Update listHistEventTask;
    }
    
}