public without sharing class SubmittedUtilizationPDFController{

    public SubmitUtilizationController.ContainerWrapper data {get;set;}

    public SubmittedUtilizationPDFController(){
    
        Id utilizationStagingId = Apexpages.currentPage().getParameters().get('id');
        Id assetId = Apexpages.currentPage().getParameters().get('assetId');
        system.debug('assetId'+assetId);
        //its call by reference so cannot pass data bcoz its being used on vf page. 
        SubmitUtilizationController.ContainerWrapper data1 = new SubmitUtilizationController.ContainerWrapper();
        
        if(assetId != null){
            List<Aircraft__c> assetList = [select id, RecordType.DeveloperName from Aircraft__c 
                                                       where id = : assetId];
            if(assetList.size()>0){
              data1.assetType = assetList[0].RecordType.DeveloperName;  
            }
        }
        
        SubmitUtilizationController.loadAssemblies(data1, assetId, utilizationStagingId);
        data = data1;
        System.debug('Data: '+data);

    }

}