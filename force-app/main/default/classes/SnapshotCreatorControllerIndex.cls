public without sharing class SnapshotCreatorControllerIndex {
    
	private static List<ContentVersion> contentVersionList = new List<ContentVersion>();
    private static Set<String> IdCheckSet;
    private static List<RecordDetailJson> recordDetailList;
    
    /**
     * Creates a snapshot record for the particualr record Id and for all its child data
     */ 
    @AuraEnabled
    public static void createSnapshots(Id recordId, String comment) { 
        System.debug('createSnapshots recordId '+recordId);       
        if(recordId == null)
            return;
        IdCheckSet = new Set<String>();
        contentVersionList = new List<ContentVersion>();
        recordDetailList = new List<RecordDetailJson>();
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        String sObjLabel = recordId.getSObjectType().getDescribe().getLabel();
        
        //Create Json for the parent record its done based on record Id
        List<SnapshotObjectField> objFieldList = getFieldsForObject(sObjName);
        objFieldList = getValueforParentRecord(recordId, sObjName, objFieldList);
        
        DateTime dt = DateTime.now();
        String dateTimeStr = dt.format('yyyy-MMM-dd');
        //insert snapshot 
        Snapshot__c snapshot = new Snapshot__c();
        snapshot.Name = sObjLabel + ' ' +dateTimeStr;
        snapshot.Type__c = sObjLabel;
        snapshot.Y_Hidden_Type_API__c = sObjName;
        snapshot.Comment__c = comment;
        insert snapshot;
        String title = sObjName + '-' + recordId; 
        createJson(snapshot.Id, title, objFieldList, recordId);
        
        //parsing child data and creating snapshots for it
        getJsonForRelatedObj(recordId,snapshot.Id);
        
        if(contentVersionList.size() > 0 ) 
            insert contentVersionList;
        
        //Create Indexing file
        if(recordDetailList != null && recordDetailList.size() > 0) {
            ContentVersion contentversionVarient = new ContentVersion();
            contentversionVarient.Title = SnapshotController.INDEX_FILE_NAME;
            contentversionVarient.contentLocation='S';
            contentversionVarient.PathOnClient = SnapshotController.INDEX_FILE_NAME + '.json';
            contentversionVarient.FirstPublishLocationId = snapshot.Id;
            contentversionVarient.VersionData = Blob.valueOf(JSON.serialize(recordDetailList));
            insert contentversionVarient;
        
            System.debug('createSnapshots recordDetailList: '+ JSON.serializePretty(recordDetailList));
        }
    }

    /***
     * Create parent record json and checks whether relatedObj json is there, if so creates json for all objects
     */ 
    public static void getJsonForRelatedObj(Id recordId, Id snapshotId){
        Snapshot_Configuration__c snapshotConf = getRelatedObjectFromSnapshotConf(recordId);
        //System.debug('getJsonForRelatedObj snapshotConf '+snapshotConf);
        if(snapshotConf == null || snapshotConf.Related_Object__c == null) 
            return;
        
        //system.debug('getJsonForRelatedObj snapshotConf: '+snapshotConf);
        if(snapshotConf.Related_Object__c != null) {
            RelatedObjectJson relatedObjectMap;
            try {
                relatedObjectMap = (RelatedObjectJson) JSON.deserialize(snapshotConf.Related_Object__c,RelatedObjectJson.class );
                //system.debug('getJsonForRelatedObj relatedObjectMap '+JSON.serializePretty(relatedObjectMap));
                
                //coping configuration json to snapshot object
                String title = 'SnapshotConfiguration';
                ContentVersion contentversionVarient = new ContentVersion();
                contentversionVarient.Title = title;
                contentversionVarient.contentLocation='S';
                contentversionVarient.PathOnClient = title +'.json';
                contentversionVarient.FirstPublishLocationId = snapshotId;
                contentversionVarient.VersionData = Blob.valueOf(snapshotConf.Related_Object__c);
                contentVersionList.add(contentversionVarient);
                
            }
            catch(Exception e1) {
                system.debug('getJsonForRelatedObj Exception: '+e1);
                String m = 'Invalid json ';
                AuraHandledException e = new AuraHandledException(m);
                e.setMessage(m);
                throw e;
            }
            
            if(relatedObjectMap != null) {
                List<RelatedObjectJson> childList = relatedObjectMap.child;
                List<Id> parentIdList = new List<Id>();
                parentIdList.add(recordId);
                
                createSnapshotForChild(childList, parentIdList, snapshotId);
            }
        }
    }
    
    /**
     * Parse the Configuration json and gets all the child objects and creates a json
     */ 
    public static void createSnapshotForChild (List<RelatedObjectJson> relatedObjectMapList, List<Id> parentIdList, Id snapshotId) {
        if(relatedObjectMapList != null) {
            for(RelatedObjectJson relatedObjectMap: relatedObjectMapList) {
               // system.debug('createSnapshotForChild relatedObjectMap '+relatedObjectMap);
               // system.debug('createSnapshotForChild parentIdList '+parentIdList);
                String childApi = relatedObjectMap.obj_api;
                String rel_obj_api = relatedObjectMap.related_obj_api;
                List<RelatedObjectJson> subChildList = relatedObjectMap.child;
                if(childApi == null || rel_obj_api == null) 
                    return;
                
                List<SnapshotObjectField> childFieldSnopObj = getFieldsForObject(childApi);
                if(childFieldSnopObj == null)
                    return;
                Set<String> fieldList = new Set<String>();
                for(SnapshotObjectField field : childFieldSnopObj) {
                    fieldList.add(field.ApiName);
                }
                String fields = String.join(new List<String>(fieldList),',');
                String queryStr = 'select '+fields+' from '+childApi+' where '+rel_obj_api+ ' in :parentIdList';
                
                List<SObject> objDataList = database.query(queryStr);
                
                //System.debug('getReferenceApiForChild queried data: '+ JSON.serializePretty(objDataList));
                //System.debug('createSnapshotForChild queried parentIdList: '+ parentIdList);
                Map<Id, List<RecordDetailChildJson>> parentChildMap = new Map<Id, List<RecordDetailChildJson>>();
                List<Id> childIdList = new List<Id>();
                for(SObject objData: objDataList) {
                    childIdList.add(objData.Id);
                    String name = null;
                    if(objData.get('Name') != null) 
                        name = String.valueOf(objData.get('Name'));
                    for(SnapshotObjectField snap: childFieldSnopObj) {
                        if(objData.get(snap.ApiName) != null) {
                            try {   
                                snap.Value = String.valueOf(objData.get(snap.ApiName));
                                //Is to store parent child ids for indexing
                                if(rel_obj_api.equalsIgnoreCase(snap.ApiName) && snap.Value != null) {
                                    RecordDetailChildJson childJson = new RecordDetailChildJson();
                                    childJson.childId = objData.Id;
                                    childJson.childName = name;
                                    if(parentChildMap.get(snap.Value) == null) {
                                        List<RecordDetailChildJson> childIds = new List<RecordDetailChildJson>();
                                        childIds.add(childJson);
                                        parentChildMap.put(snap.Value, childIds);
                                    } else {
                                        List<RecordDetailChildJson> childIds = parentChildMap.get(snap.Value);
                                        childIds.add(childJson);
                                        parentChildMap.put(snap.Value, childIds);
                                    }
                                }
                            }
                            catch(Exception e) {
                                snap.Value = null;
                            }
                        }
                        else 
                            snap.Value = null;
                    }
                    //System.debug('getReferenceApiForChild objFieldList: '+ JSON.serializePretty(objFieldList));
                    //System.debug('getReferenceApiForChild childFieldSnopObj: '+ childFieldSnopObj);
                    String title = childApi + '-' + objData.Id;
                    createJson(snapshotId, title, childFieldSnopObj, objData.Id); 
                }
                if(childIdList.size() > 0) {
                    if(parentChildMap.size() > 0) {
                        for (Id key : parentChildMap.keySet()) {
                            List<RecordDetailChildJson> localChildList = parentChildMap.get(key);
                            RecordDetailJson recordDetailJson = new RecordDetailJson();
                            recordDetailJson.child_api = childApi; 
                            recordDetailJson.related_obj_api = rel_obj_api;
                            recordDetailJson.parentId = key;
                            recordDetailJson.itemIdList = localChildList;
                            recordDetailList.add(recordDetailJson);
                        }
                    }
                    parentChildMap = null;
                }
                if(subChildList != null) {
                    createSnapshotForChild(subChildList, childIdList, snapshotId);
                }       
            }   
        }
    }
    
    /**
     * Gets relatedJson for the given object from Configuration record
     */ 
    @AuraEnabled
    public static Snapshot_Configuration__c getRelatedObjectFromSnapshotConf(Id recordId) {
        //System.debug('getRelatedObjectFromSnapshotConf recordId '+recordId);       
        if(recordId == null)
            return null;
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        //System.debug('getRelatedObjectFromSnapshotConf sObjName '+sObjName);       
        if(sObjName == null)
            return null;
		List<Snapshot_Configuration__c> snapshotConf = [select Id, Related_Object__c from 
                                                        Snapshot_Configuration__c where 
                                                        Primary_Object__c = :sObjName and 
                                                        Active__c = true order by  
                                                        LastModifiedDate desc limit 1 
                                                       ];
        if(snapshotConf.size() > 0) {
            return snapshotConf[0];
        }
        return null;
    }
    
    /***
     * Get all the fields for a specified object name and store it in SnapshotObjectField class.
     */ 
    public static List<SnapshotObjectField> getFieldsForObject(String objName){
        //System.debug('getFieldsForObject objName '+objName);       
        if(objName == null)
            return null;
        List<SnapshotObjectField> objFieldList = new List<SnapshotObjectField>();
        //if exception its a wrong object
        Map<string,SObjectField> fieldMap;
        try {
            fieldMap = schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        }
        catch(Exception e1){
            System.debug('getFieldsForObject Exception '+e1);
            String m = 'Invalid Object name '+objName +'. Please check Snapshot Configuration settings.';
            AuraHandledException e = new AuraHandledException(m);
            e.setMessage(m);
            throw e;
        }
        /* 
         * To avoid duplicate field error if someone create a local field with same name, 
         * created a fieldCheckSet to add unique field api name
		*/
        Set<String> fieldCheckSet = new Set<String>();
        for(string fieldName: fieldMap.keySet()) {
            string strFieldLocalName = fieldMap.get(fieldName).getDescribe().getLocalName();
            if(!fieldCheckSet.contains(strFieldLocalName)) {
                SnapshotObjectField objField = new SnapshotObjectField();
                objField.Label = fieldMap.get(fieldName).getDescribe().getLabel();
                objField.ApiName = fieldName;
                objField.FieldType = fieldMap.get(fieldName).getDescribe().getType();
                if(objField.FieldType == Schema.DisplayType.REFERENCE ){
                    Schema.DescribeFieldResult dfr1 = fieldMap.get(fieldName).getDescribe();  
                    String RelatedObjNameChild = String.valueOf(dfr1.getReferenceTo()).substringBetween('(',')');
                    objField.RelObjectName = RelatedObjNameChild;
                }
                objFieldList.add(objField);
                fieldCheckSet.add(strFieldLocalName);
            }
        }
        fieldCheckSet = null;
        //System.debug('getFieldsForObject objFieldList : '+ JSON.serializePretty(objFieldList));
        //System.debug('getFieldsForObject objFieldList : '+ objFieldList);
        return objFieldList;
    }
    
    /***
     * Query to get value for the record.
     */ 
    public static List<SnapshotObjectField> getValueforParentRecord(Id recordId, String objName,  List<SnapshotObjectField> objFieldList) {
        List<String> fieldList = new List<String>();
        for(SnapshotObjectField field : objFieldList)
        	fieldList.add(field.ApiName);
        
        String fields = String.join(new List<String>(fieldList),',');
        String queryStr = 'select '+fields+' from '+objName+' where Id =: recordId';
        
        //System.debug('getValueforRecord queryStr '+queryStr);
        fieldList = null;
        List<SObject> objData = database.query(queryStr);
        //System.debug('getValueforRecord queried data: '+ JSON.serializePretty(objData));
        //System.debug('getValueforRecord queried data: '+ objData);
        
        if(objData.size() > 0) {
            
            RecordDetailChildJson itemJson = new RecordDetailChildJson();
            itemJson.childId = recordId;
            for(SnapshotObjectField snap: objFieldList) {
                if(objData[0].get(snap.ApiName) != null) {
                    try {   
                        snap.Value = String.valueOf(objData[0].get(snap.ApiName));
                        if(snap.ApiName.equalsIgnoreCase('Name')) 
                            itemJson.childName = snap.Value;
                    }
                    catch(Exception e) {
                        snap.Value = null;
                    }
                }
                else 
                    snap.Value = null;
            }
            List<RecordDetailChildJson> itemList = new List<RecordDetailChildJson>();
            itemList.add(itemJson);
            RecordDetailJson recordDetailJson = new RecordDetailJson();
            recordDetailJson.child_api = objName; 
            recordDetailJson.related_obj_api = '';
            recordDetailJson.parentId = null;
            recordDetailJson.itemIdList = itemList;
            recordDetailList.add(recordDetailJson);
            
            
        }
        //System.debug('getValueforRecord objFieldList: '+ JSON.serializePretty(objFieldList));
        //System.debug('getValueforRecord objFieldList: '+ objFieldList);
        return objFieldList;
    }
    
    /***
     * Creates a json file for the record.
     */ 
    public static void createJson(Id recordId, String title, List<SnapshotObjectField> objData, Id objectId){
        //System.debug('createJson objFieldList: '+ objData);
        ContentVersion contentversionVarient = new ContentVersion();
        contentversionVarient.Title = title;
        contentversionVarient.contentLocation='S';
        contentversionVarient.PathOnClient = title +'.json';
        contentversionVarient.FirstPublishLocationId = recordId;
        contentversionVarient.VersionData = Blob.valueOf(JSON.serialize(objData));
        
        //To avoid adding duplicate record to snapshot objects
        if(objectId != null && !IdCheckSet.contains(objectId)) {
        	contentVersionList.add(contentversionVarient);
            IdCheckSet.add(objectId);
        }
        
        //to avoid heap size issue.. Insert records in bulk and empty the list memory.
        if(contentVersionList.size() > 20) {
            insert contentVersionList;
            contentVersionList = null;
            contentVersionList = new List<ContentVersion>();
        }
    }
    
    
    public class SnapshotObjectField implements Comparable{
        
        @AuraEnabled public String Label  {get; set;}
        @AuraEnabled public String ApiName  {get; set;}
        @AuraEnabled public String RelObjectName  {get; set;}
        @AuraEnabled public String RelObjectFieldValue  {get; set;}
        @AuraEnabled public Schema.DisplayType FieldType  {get; set;}
        @AuraEnabled public String Value  {get; set;}
        @AuraEnabled public String FieldCurrentStatus {get; set;} //To indicate z_unused, y_hidden
        
        public Integer compareTo(Object objToCompare) {
            SnapshotObjectField snap = (SnapshotObjectField)(objToCompare);
            if(snap == null)
                return 1;
            if (this.Label > snap.Label) 
                return 1;
            if (this.Label == snap.Label) 
                return 0;
            
            return -1;
        }    
    }
    
    public class RelatedObjectJson {
        @AuraEnabled public String obj_api  {get; set;}
        @AuraEnabled public String related_obj_api  {get; set;}
        @AuraEnabled public List<RelatedObjectJson> child  {get; set;}
    }
    
    public class RecordDetailJson {
        @AuraEnabled public String child_api  {get; set;}
        @AuraEnabled public String related_obj_api  {get; set;}
        @AuraEnabled public Id parentId {get; set;}
        @AuraEnabled public List<RecordDetailChildJson> itemIdList  {get; set;}
    }
    
    public class RecordDetailChildJson {
        @AuraEnabled public Id childId {get; set;}
        @AuraEnabled public String childName {get; set;}
    }
    
}