public class Notify_SubscriptionStatusScheduler implements Schedulable,Database.AllowsCallouts{
    public void execute(SchedulableContext SC) {
        setStatusOfNotiifcationSubscription();
        finish();//to abort the jpb once the status is updated
    }
    public void finish(){
        
        Id detailId = [SELECT Id FROM CronJobDetail WHERE Name='Update Subscription Status' limit 1]?.id;
        if (detailId != null) {
            Id jobId = [SELECT Id from CronTrigger WHERE CronJobDetailId = :detailId][0].Id;
            system.debug('Abort the Job-----'+jobId );
	        System.abortJob(jobId);
        }
       
    }

    @future(callout=true)
    public static void setStatusOfNotiifcationSubscription(){
        Map<String, String> asyncStatusMap = new Map<String,String>();
        Set<String> notifyIdSet = new Set<String>();
        String status;
        List<Notifications_Subscription__c> nsList = [Select Id,Object_API_Name__c,Active__c, Status__c,Async_JobId__c  from Notifications_Subscription__c where status__c like '%Pending%' and Async_JobId__c != null];
        for(Notifications_Subscription__c ns : nsList){
            status = '';
            List<ApexTrigger> trig = Notify_Utility.existingTriggerCheck(Notify_Utility.generateTriggerName(ns.Object_API_Name__c));
            if(trig.size()>0){
                ns.Status__c ='Running';
            }else{
                Notify_MetadataService.MetadataPort service = Notify_Utility.createService();
                Notify_MetadataService.DeployResult deployResult = service.checkDeployStatus(ns.Async_JobId__c, true);
                system.debug('deployRes--------'+ deployResult);
                if (deployResult.done && deployResult.status != 'Failed') {
                    //in case of component failures set the status to error with error message
                    
                    
                     if (deployResult.details != null && deployResult.details.componentFailures != null) { // Deployment errors?
                         for (Notify_MetadataService.DeployMessage deployMessage : deployResult.details.componentFailures) {
                             system.debug('deployMessage---'+ deployMessage);
                             if (deployMessage.problem != null) {
                                 system.debug('Error----'+ deployMessage.problem);
                                 ns.Status__c = 'Error';
                                 ns.Error_Message__c = deployMessage.fileName + ' (Line: ' + deployMessage.lineNumber + ': Column:' + deployMessage.columnNumber + ') : ' + deployMessage.problem;
                                 
                             }
                         }
                     }else{ns.Status__c =  'Running';}
                 }else{ ns.Status__c = ns.Active__c==true?'Pending':'Not Running' ;}
            }
            
        }
        
        if(!nsList.isEmpty()){
            Notify_Utility.setSourceOfCall('INTERNAL_API');
            update nsList;
        }
    }

   
}