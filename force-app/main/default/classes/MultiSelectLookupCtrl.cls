public class MultiSelectLookupCtrl {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, List<sObject> ExcludeitemsList, String whereClause) {
        List<SObject> returnList = new List<SObject>();
        List<String> lstExcludeitems = new List<String>();
        
        for(sObject item : ExcludeitemsList ){
            lstExcludeitems.add(item.id);
        }
        
        executeQuery(false, searchKeyWord, ObjectName, lstExcludeitems, whereClause, returnList);
        for (sObject obj: returnList) {
            lstExcludeitems.add(obj.Id);
        }      
        
        executeQuery(true, searchKeyWord, ObjectName, lstExcludeitems, whereClause, returnList);
        System.debug('returnList --->' + returnList);
        
        return returnList;
    }
    
    private static void executeQuery(Boolean isLikeQuery, String searchKeyWord, String ObjectName, List<String> lstExcludeitems, String whereClause, List<SObject> returnList){
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records  
        String sQuery = 'Select Id, Name';
        
        if(ObjectName == 'Aircraft__c') {
            sQuery += ', Next_Lessee__c, Assigned_To__c';
        }
        sQuery += ' from ' + ObjectName + ' where ';
        
        if(!isLikeQuery){
            sQuery += ' Name =: searchKeyWord ';
        }else{
            searchKeyWord = '%' + searchKeyWord + '%';
            sQuery += ' Name LIKE: searchKeyWord ';
        }
        sQuery += 'AND Id NOT IN : lstExcludeitems';
        
        if(String.isNotBlank(whereClause)){
            sQuery += ' AND ' + whereClause;
        }
        sQuery += ' order by createdDate DESC limit 10';
        System.debug('query --->' + sQuery);
        
        for (sObject obj: Database.query(sQuery)) {
            returnList.add(obj);
        }
    }
}