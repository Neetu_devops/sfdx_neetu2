/**
*******************************************************************************
* Class: TestGenericRelatedListController
* @author Created by Bhavna, Lease-Works, 
* @date  6/12/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Test all the Methods of GenericRelatedListController.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestGenericRelatedListController {
    
    /**
    * @description Created Test Data to test all the Methods of GenericRelatedListController.
    *    
    * @return null
    */
    @testSetup 
    static void setup() {
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        System.debug('What is the profile id ' + profile1);
        date tDate = date.today();
        date uDate = Date.today().addDays(30);
        
        User u = new User(
            ProfileId = profile1.Id,
            Username = 'testUser1111@leaseWorks.com',
            Alias = 'test',
            Email='testUser1@salesforce.com',
            EmailEncodingKey='UTF-8',
            Firstname='testing',
            Lastname='user',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago');
        insert u;
        
        System.runAs(u){            
            string dealRecType;
            string ctRecType;
            Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c');
            
            for(Schema.RecordTypeInfo rti : objectDescribe.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    dealRecType = rti.getRecordTypeId();
                }
            }
            
            Schema.DescribeSObjectResult objectDescribe1 = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
            
            for(Schema.RecordTypeInfo rti : objectDescribe1.getRecordTypeInfos()) {
                if(rti.getName()=='Engine Lease'){
                    ctRecType = rti.getRecordTypeId();
                }
            }
            
            Account acc = new Account(
                Name= 'testing'
            );
            insert acc;
            
            Operator__c testOperator = new Operator__c(
                Name = 'American Airlines',                   
                Status__c = 'Approved',                          
                Rent_Payments_Before_Holidays__c = false,         
                Revenue__c = 0.00,                                
                Current_Lessee__c = true                         
            );
            insert testOperator;
            
            Marketing_Activity__c testMarketingActivity = new Marketing_Activity__c(
                Name = 'Test'+1,
                Deal_Type__c = 'Lease',                                
                Deal_Status__c = 'Pipeline',                           
                Asset_Type__c = 'Engine',                              
                Prospect__c = testOperator.Id,                    
                Marketing_Rep_c__c = 'N/A',                            
                Description__c = '2',                                                
                Inactive_MA__c = false,
                RecordTypeId = dealRecType
            );                             
            insert testMarketingActivity;
            
            List<Aircraft__c> aircraftList = new List<Aircraft__c>();
            for(Integer i=0;i<2;i++){
                Aircraft__c testAircraft = new Aircraft__c();
                testAircraft.Name = 'Test'+i;                        
                testAircraft.MSN_Number__c = '28216'+i;                         
                testAircraft.Date_of_Manufacture__c = system.today();
                testAircraft.Aircraft_Type__c = '737'+i;                           
                testAircraft.Aircraft_Variant__c = '700'+i;                      
                testAircraft.Marked_For_Sale__c = false;                      
                testAircraft.Alert_Threshold__c = 10.00;                        
                testAircraft.Status__c = 'Available';                     
                testAircraft.Awarded__c = false;                                                  
                testAircraft.TSN__c = 1.00;                                 
                testAircraft.CSN__c = 1;
                aircraftList.add(testAircraft);
            }
            insert aircraftList;
            
            Pricing_Run_New__c testPricingRunNew = new Pricing_Run_New__c(
                Name='Test',
                Current_Rent__c= 2000,
                EBT__c = 20,
                IRR__c = 2,
                Lease_Rate_Factor__c = 2,
                Purchase_Price_For_SLBs_Only__c = 20,
                Rate_Type__c = 'Fixed/Fixed',
                Marketing_Activity__c = testMarketingActivity.Id,
                RecordTypeId = ctRecType
            );
            insert testPricingRunNew;
            
            Pricing_Run_New__c testPricingRun = new Pricing_Run_New__c(
                Name='Test1',
                Current_Rent__c= 3000,
                EBT__c = 20,
                IRR__c = 2,
                Lease_Rate_Factor__c = 2,
                Purchase_Price_For_SLBs_Only__c = 20,
                Rate_Type__c = 'Fixed/Fixed',
                Marketing_Activity__c = testMarketingActivity.Id,
                RecordTypeId = ctRecType
            );
            insert testPricingRun;
        }
    }
    
    /**
    * @description This Methods gives the code coverage of  getRelatedListData,getPageLayoutFields
    *   getRecordIdPrefix,getObjectRecTypeId method of GenericRelatedListController
    *
    * @return null
    */
    @isTest static void testGenericRelatedList(){
        Marketing_Activity__c[] newlist = [select id from Marketing_Activity__c];
        List<Aircraft__c> aircraftRecord = [select id, Name from Aircraft__c];
        List<Pricing_Output_New__c> pricingOutputNewList =[select id,Asset_Term__c, 
                                                           Asset_Term__r.Aircraft__c 
                                                           from Pricing_Output_New__c];
        Pricing_Run_New__c[] pricingRunNewList = [select id from Pricing_Run_New__c WHERE Name='Test1'];
        Account[] accountList = [select id from Account];
        String ctRecType;
        Schema.DescribeSObjectResult objectDescribe1 = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        
        for(Schema.RecordTypeInfo rti : objectDescribe1.getRecordTypeInfos()) {
            if(rti.getName()=='Engine Lease'){
                ctRecType = rti.getRecordTypeId();
            }
        }
        
        
        GenericRelatedListController.getRelatedListData(newlist[0].Id, 
                                                        leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c', 
                                                        leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c', 
                                                        leasewareutils.getNamespacePrefix() + 'CommercialTermFieldset', 'createddate');
        GenericRelatedListController.getPageLayoutFields(pricingRunNewList[0].Id, 
                                                         leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c', 
                                                         ctRecType);
        GenericRelatedListController.getRecordIdPrefix(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        GenericRelatedListController.getObjectRecTypeId(pricingRunNewList[0].Id, 
                                                        leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        GenericRelatedListController.getObjectRecTypeId(accountList[0].Id, 'Account');
        system.assertEquals(pricingRunNewList.size(),1);
        
        GenericRelatedListController.fetchMatchingRTId(newlist[0].Id, leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
    } 
    
    /**
    * @description This Methods gives the code coverage of deleteRecord  
    *   method of GenericRelatedListController 
    *
    * @return null
    */
    @isTest static void testdeleteRecord(){
        Pricing_Run_New__c[] pricingRunNew = [select id from Pricing_Run_New__c WHERE Name='Test1']; 
        GenericRelatedListController.deleteRecord(pricingRunNew[0].Id, leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        system.assertEquals(pricingRunNew.size(), 1);
    }
    
}