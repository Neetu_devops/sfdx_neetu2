public class ReconciliationScheduleController {
    
    @auraEnabled
    public static ReconciliationScheduleDataWrapper getDefaultData(String leaseId){
        
        List<Lease__c> leaseList = [Select Id, Name, Lease_Start_Date_New__c, 
                                    Lease_End_Date_New__c, Aircraft__c, 
                                    Aircraft__r.Name
                                    from Lease__c where Id =: leaseId];
        
        String prefix = LeaseWareUtils.getNamespacePrefix();
        
        List<Utility.Option> reconciliationPeriodoptions = Utility.getPicklistvalues(prefix + 'Reconciliation_Schedule__c', prefix + 'Reconciliation_Period__c');
        
        Date endDateOfLastReconcileSchedule = leaseList[0].Lease_Start_Date_New__c;
        
        Boolean isEndDateEqualLeaseEnd = false;
        
        List<Reconciliation_Schedule__c> rSObjList = [Select Id, To_Date__c from Reconciliation_Schedule__c Where Lease__c =: leaseId order By To_Date__c DESC Limit 1];
        
        if(rSObjList.size() > 0 && rSObjList[0].To_Date__c != leaseList[0].Lease_End_Date_New__c){
            endDateOfLastReconcileSchedule = rSObjList[0].To_Date__c.addDays(1);
        }else if(rSObjList.size() > 0 && rSObjList[0].To_Date__c == leaseList[0].Lease_End_Date_New__c){
            endDateOfLastReconcileSchedule = null;
            isEndDateEqualLeaseEnd = true;
        }
        
        ReconciliationScheduleDataWrapper reconciliationSchedule = new ReconciliationScheduleDataWrapper();
        reconciliationSchedule.startDate = endDateOfLastReconcileSchedule;
        reconciliationSchedule.endDate = endDateOfLastReconcileSchedule == null ? null : leaseList[0].Lease_End_Date_New__c;        
        reconciliationSchedule.leaseStartDate = leaseList[0].Lease_Start_Date_New__c;
        reconciliationSchedule.leaseEndDate = leaseList[0].Lease_End_Date_New__c;
        reconciliationSchedule.assetId = leaseList[0].Aircraft__c;
        reconciliationSchedule.reconciliationPeriodoptions = reconciliationPeriodoptions;
        reconciliationSchedule.asset = leaseList[0].Aircraft__r.Name;
        reconciliationSchedule.leaseId = leaseList[0].Id;
        reconciliationSchedule.lease = leaseList[0].Name;
        reconciliationSchedule.isEndDateEqualLeaseEnd = isEndDateEqualLeaseEnd;
        
        return reconciliationSchedule;
    }
    
    @auraEnabled
    public static void saveReconciliationSchedules(String reconciliationData){
        
        ReconciliationScheduleDataWrapper reconciliationScheduleValues = (ReconciliationScheduleDataWrapper)System.JSON.deserialize(reconciliationData,ReconciliationScheduleDataWrapper.class);
        
        List<Reconciliation_Schedule__c> reconciliationSchedulesToInsert = new List<Reconciliation_Schedule__c>();
        
        Date startDate= reconciliationScheduleValues.startDate.toStartOfMonth();
        Date startDateForNewReconciliations = startDate;
        
        if(startDate < reconciliationScheduleValues.leaseStartDate){
            startDate = reconciliationScheduleValues.leaseStartDate;
            startDateForNewReconciliations = reconciliationScheduleValues.leaseStartDate;
        }
        
        Date reconciliationEndDate = reconciliationScheduleValues.endDate != null ? reconciliationScheduleValues.endDate.toStartOfMonth().addMonths(1).addDays(-1) : reconciliationScheduleValues.leaseEndDate;
        Date endDateForNewReconciliations = reconciliationEndDate;
        Integer reconciliationFrequency;
        
        if(reconciliationEndDate > reconciliationScheduleValues.leaseEndDate){
            reconciliationEndDate = reconciliationScheduleValues.leaseEndDate;
            endDateForNewReconciliations = reconciliationScheduleValues.leaseEndDate;
            
        }
        
        if(reconciliationScheduleValues.reconciliationPeriod == 'Whole Lease Term'){
            reconciliationFrequency = startDate.monthsBetween(reconciliationScheduleValues.leaseEndDate) + 1;
        }else{
            reconciliationFrequency = Integer.valueOf(reconciliationScheduleValues.reconciliationPeriod);
        }
        
        if(reconciliationScheduleValues.reconciliationOverwrite){
            
            reconciliationEndDate = reconciliationScheduleValues.leaseEndDate;
            startDate = startDate.addMonths(1).addDays(-1);
        }
        
        List<Reconciliation_Schedule__c> rSRecordList = [Select Id, Name,To_Date__c, From_Date__c from Reconciliation_Schedule__c 
                                                         Where Lease__c =: reconciliationScheduleValues.leaseId 
                                                         AND (((To_Date__c >=: reconciliationEndDate
                                                                AND From_Date__c <=: reconciliationEndDate)
                                                               OR (To_Date__c >=: startDate AND From_Date__c <=: startDate)) 
                                                              OR ((To_Date__c <=: reconciliationEndDate
                                                                   AND To_Date__c >=: startDate)
                                                                  OR (From_Date__c <=: reconciliationEndDate
                                                                      AND From_Date__c >=: startDate))) order by From_Date__c];
        
        if(rSRecordList.size() > 0 && reconciliationScheduleValues.reconciliationOverwrite){
            
            delete rSRecordList;
            LeaseWareUtils.clearFromTrigger();
        }else if(rSRecordList.size() > 0){
            throw new AuraHandledException('Reconciliation Schedule already exists for the period selected. Please revise the dates.');
        }
        
        Id recordTypeId = Schema.SObjectType.Reconciliation_Schedule__c.getRecordTypeInfosByDeveloperName().get('Default').getRecordTypeId();
        
        while (startDateForNewReconciliations.addMonths(reconciliationFrequency) <= endDateForNewReconciliations) {
            
            Date tempDate = startDateForNewReconciliations.addMonths(reconciliationFrequency).addDays(-1);
            Date endDate;
            
            if(startDateForNewReconciliations.addMonths(reconciliationFrequency).Month() == tempDate.Month()){
                endDate = tempDate.toStartOfMonth().addDays(-1);
                //Date.newInstance(tempDate.year(), tempDate.month() - 1,date.daysInMonth(tempDate.year(), tempDate.month()-1));
            }else{
                endDate = tempDate.toStartOfMonth().addMonths(1).addDays(-1);
            }
            
            Reconciliation_Schedule__c rsObj = new Reconciliation_Schedule__c();
            rsObj.Name = startDateForNewReconciliations.year()+' '+ datetime.newInstance(startDateForNewReconciliations.year(), startDateForNewReconciliations.month(),startDateForNewReconciliations.day()).format('MMM')+' to '+ endDate.addDays(-1).year() +' '+datetime.newInstance(endDate.addDays(-1).year(), endDate.addDays(-1).month(),endDate.addDays(-1).day()).format('MMM');
            rsObj.From_Date__c = startDateForNewReconciliations;
            rsObj.To_Date__c = endDate;
            rsObj.Status__c = 'Open';
            rsObj.RecordTypeId = recordTypeId;
            rsObj.Disable_Invoice_Auto_Generation__c = reconciliationScheduleValues.disableActualMR;
            rsObj.Lease__c = reconciliationScheduleValues.leaseId;
            rsObj.Asset__c = reconciliationScheduleValues.assetId;
            rsObj.Reconciliation_Period__c = reconciliationScheduleValues.reconciliationPeriod;
            rsObj.Ratio_Variance__c = reconciliationScheduleValues.aActualratio;
            
            reconciliationSchedulesToInsert.add(rsObj);
            
            startDateForNewReconciliations = endDate.toStartOfMonth().addMonths(1);
        }
        
        
        if(startDateForNewReconciliations != endDateForNewReconciliations){
            
            Reconciliation_Schedule__c rsObj = new Reconciliation_Schedule__c();
            rsObj.Name = startDateForNewReconciliations.year()+' '+ datetime.newInstance(startDateForNewReconciliations.year(), startDateForNewReconciliations.month(),startDateForNewReconciliations.day()).format('MMM')+' to '+ endDateForNewReconciliations.year() +' '+datetime.newInstance(endDateForNewReconciliations.year(), endDateForNewReconciliations.month(),endDateForNewReconciliations.day()).format('MMM');
            rsObj.From_Date__c = startDateForNewReconciliations;
            rsObj.Status__c = 'Open';
            rsObj.RecordTypeId = recordTypeId;
            rsObj.To_Date__c = endDateForNewReconciliations;
            rsObj.Lease__c = reconciliationScheduleValues.leaseId;
            rsObj.Asset__c = reconciliationScheduleValues.assetId;
            rsObj.Reconciliation_Period__c = reconciliationScheduleValues.reconciliationPeriod;
            rsObj.Ratio_Variance__c = reconciliationScheduleValues.aActualratio;
            rsObj.Disable_Invoice_Auto_Generation__c = reconciliationScheduleValues.disableActualMR;
            
            reconciliationSchedulesToInsert.add(rsObj);
            
        }
        
        if(reconciliationSchedulesToInsert.Size() > 0){
            insert reconciliationSchedulesToInsert;
        }      
    }
    
    public class ReconciliationScheduleDataWrapper{
        @AuraEnabled public Date startDate{get;set;}
        @AuraEnabled public Date endDate{get;set;}
        @AuraEnabled public String reconciliationType{get;set;}
        @AuraEnabled public String reconciliationPeriod{get;set;}
        @AuraEnabled public Boolean reconciliationOverwrite{get;set;}
        @AuraEnabled public Decimal aActualratio{get;set;}
        @AuraEnabled public Id leaseId{get;set;}
        @AuraEnabled public String lease{get; set;}
        @AuraEnabled public Date leaseStartDate{get; set;}
        @AuraEnabled public Date leaseEndDate{get; set;}
        @AuraEnabled public Id assetId{get;set;}
        @auraEnabled public String asset {get;set;}
        @auraEnabled public Boolean disableActualMR {get;set;}
        @AuraEnabled public Boolean isEndDateEqualLeaseEnd{get;set;}
        @auraEnabled public List<Utility.Option> reconciliationPeriodoptions {get;set;}
        
        Public ReconciliationScheduleDataWrapper(){
            reconciliationPeriodoptions = new List<Utility.Option>();
            reconciliationOverwrite = false;
            disableActualMR = false;
        }
    }
}
