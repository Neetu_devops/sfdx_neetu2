@isTest
public class TestILEmailController {
	
    
    @isTest
    static void loadTestCases(){
        
        Country__c country =new Country__c(Name='United States');
        insert country;
        Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
     // opr.Country__c = 'United States';
        opr.Country_Lookup__c = country.id;
        opr.Region__c = 'Asia';
        insert opr;
                                               
        Customer_Interaction_Log__c ilog = new Customer_Interaction_Log__c(
            Name = 'Test',
            Related_To_Operator__c = opr.Id, 
            Type_Of_Interaction__c = 'Call',
            Discussion_Type__c = 'Marketing Intel',
            Type_Of_Event__c = 'ISTAT',
            Comments__c = 'Test',
            Summary__c = 'Test',
            Meeting_Notes__c = 'Test',
            Action_Recommendation__c = 'Test',
            Fleet_Plans__c = 'Test',
            Reason__c = 'Test',
            Type_Of_Request__c = 'Aircraft Lease',
            Aircraft_Type__c = 'A300',
            Variant__c = '600F', 
            Engine_Type_List__c = 'JT9D',
            Engine_Variant__c = '7R4E',
            Vintage_Range__c = '2',
            Lease_Start_Date__c = System.today(),
            Date_Needed__c =System.today(),
            Intel_Quality__c = 'Medium',
            MSN__c = '1234',
            Date_Available_if_Sale__c = system.today(),
            Rent_000__c = 2332423, 
            Term_Mos__c = 100, 
            Transition_and_Modifications_Cost_K__c = 3443, 
            Mx_Liabilities_During_Lease_K__c = 35345, 
            Security_Deposit_K__c = 4643634, 
            IRR__c = 23, 
            Form_of_Security_Deposit__c = 'Cash', 
            Notes_about_transition_and_mod_cost__c = '4634', 
            Comment__c = 'twet'
        );
		insert ilog;	
        
        ILEmailController.sendMailMethod(ilog.Id);
        List<User> userList = [select Id, Name from User limit 3];
        List<Group> groupList = [select Id from Group limit 3];
        if(groupList.size() > 0) {
            List<User> grpIds = new List<User>();
            for(Group grp: groupList) {
                String jsonStr = '{"Id" : "'+grp.Id+'"}';
                User ur = (User)JSON.deserialize(jsonStr, User.class);
                grpIds.add(ur);
            }
            ILEmailController.sendMailToMany(ilog.Id, grpIds, null, 'group');
        }
        ILEmailController.sendMailToMany(ilog.Id, userList, null, 'user');
    }
    
    
    
    
}