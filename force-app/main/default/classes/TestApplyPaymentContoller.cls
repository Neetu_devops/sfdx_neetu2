@isTest
    public class TestApplyPaymentContoller {

        @testSetup static void setup() {
            Parent_Accounting_Setup__c ASRec = new Parent_Accounting_Setup__c(Name='Dummy' );
            insert ASRec;
            TestLeaseworkUtil.setCoaPy();
         
            Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
            insert aircraft;

            Account bank = new Account(name='test',RecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Bank_Accounts').getRecordTypeId());
            insert bank;

            Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
            insert operator;

            Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,Lease_End_Date_New__c=Date.newInstance(2020,12,31),Lease_Start_Date_New__c= Date.NewInstance(2020,01,01)
                                     );
            insert lease;
            
            Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                    Name='Rent Schedule Fixed',
                    Lease__c=lease.Id,
                    Rent_Type__c='Fixed Rent',
                    Rent_Period__c='Monthly',
                    Base_Rent__c=200000.00,
                    Start_Date__c=Date.NewInstance(2020,01,01), // YYYY-MM-DD
                    Rent_End_Date__c=Date.newInstance(2020,12,31) ,  // YYYY-MM-DD
                    Invoice_Generation_Day__c='1',
                    Rent_Due_Type__c='Fixed',
                    Rent_Due_Day__c=1,
                    Escalation__c=3,
                    Escalation_Month__c='May',
                    Pro_rata_Number_Of_Days__c='Actual number of days in month/year');
                
            insert MultiRentSch;
           
            rent__c rent2 = new rent__c(RentPayments__c= lease.id,Name='New Rent 1',For_Month_Ending__c=system.today().addMonths(1).toStartOfMonth().addDays(-1),start_date__c=system.today().toStartOfMonth(),Stepped_Rent__c =MultiRentSch.id);
            insert rent2;
            
             Calendar_Period__c CP1 = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1));
            Calendar_Period__c CP2 = new Calendar_Period__c(Name = 'Dummy2',Start_Date__c =Date.NewInstance(2020,01,01),
                                                            End_Date__c = Date.NewInstance(2020,01,31));
            Calendar_Period__c CP3 = new Calendar_Period__c(Name = 'Dummy3',Start_Date__c =Date.NewInstance(2020,02,01),
                                                            End_Date__c = Date.NewInstance(2020,02,29));
            
            list<Calendar_Period__c> cp = new list<Calendar_Period__c>{cp1,cp2,cp3};
                LeaseWareUtils.clearFromTrigger();
            insert cp;

            Invoice__c invoice = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rent2.id,amount__c = rent2.Rent_Due_Amount__c,status__c ='Approved',
                                                Date_of_MR_Payment_Due__c = Date.valueOf(datetime.newInstance(system.today().year(),system.today().month(), 17)),
                                               invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
            insert invoice;

            Payment_Receipt__c payRecord1 = new Payment_Receipt__c(
                Name='P1',
                Approval_Status__c = 'Approved',
                Prepayment__c = true,
                Lease__c = lease.Id,
                Payment_Amount_Cur__c = 100000,
                Bank_Account_Lkp__c = bank.id,
                Accounting_Period__c = cp1.id
                
            );
            Payment_Receipt__c payRecord2 = new Payment_Receipt__c(
                Name='P2',
                Approval_Status__c = 'Approved',
                Prepayment__c = true,
                Lease__c = lease.Id,
                Payment_Amount_Cur__c = 200000,
                Bank_Account_Lkp__c = bank.id,
                Accounting_Period__c = cp1.id
                
            );
            list<Payment_Receipt__c> pylist = new list<Payment_Receipt__c>{payRecord1,payRecord2};
            insert pylist; 
        }
    @istest
    public static void testApplyPrepy(){
        Test.StartTest();
        Invoice__c invoice  = [select id,Balance_Due__c,Type__c from Invoice__c ];
        
        Payment_Receipt__c pr = [select id,accounting_period__c,prepayment__c,Payment_Amount_Cur__c,Available_Balance_Cur__c 
                                    from Payment_Receipt__c where name ='P1'];
        ApplyPaymentController.PrePaymentWrapper ppWrap = ApplyPaymentController.fetchInitData(invoice.id);
        System.assertEquals(ppWrap.invoiceRec.id,invoice.id);
        ppWrap.prePaymentRec = pr;
        ppWrap.prePaymentDate = system.today();
        ApplyPaymentController.applyPrePayment(JSON.serialize(ppWrap),100000);

        payment__c py = [select id,amount__c,(select id,name,credit__c,debit__c from journal_entries__r) from payment__c where invoice__r.id = :invoice.id];
        System.assertEquals(py.amount__c,100000);
        
        Payment_Receipt_Line_Item__c paymentReciptlist = [select id,Name,Amount_Paid__c,status__c,Payment_Receipt__r.Available_Balance_Cur__c from Payment_Receipt_Line_Item__c where Payment_Receipt__c = :pr.id];
        system.assertEquals(paymentReciptlist.Amount_Paid__c,100000);
        if('Pending'.equals(paymentReciptlist.status__c))
        	system.assertEquals(paymentReciptlist.Payment_Receipt__r.Available_Balance_Cur__c,100000);
        else if('Approved'.equals(paymentReciptlist.status__c))
        	system.assertEquals(paymentReciptlist.Payment_Receipt__r.Available_Balance_Cur__c,0);


        for(journal_entry__c je :py.journal_entries__r){
            system.debug('JE NAME:::'+je.name+' '+' '+je.credit__c+' '+je.debit__c);
            if(je.name=='AR - Rent')system.assertEquals(100000, je.credit__c);
            if(je.name=='Customer Prepayments')system.assertEquals(100000, je.debit__c);
           
        }
		
        if('Pending'.equals(paymentReciptlist.status__c)){
            LeaseWareUtils.clearFromTrigger();
            py.payment_status__c = 'Approved';
            update py;
        }
        paymentReciptlist = [select id,Name,Amount_Paid__c,status__c,Payment_Receipt__r.Available_Balance_Cur__c from Payment_Receipt_Line_Item__c where Payment_Receipt__c = :pr.id];
        system.assertEquals(paymentReciptlist.Amount_Paid__c,100000);
        system.assertEquals(paymentReciptlist.Payment_Receipt__r.Available_Balance_Cur__c,0.0);
        system.assertEquals(paymentReciptlist.status__c,'Approved');
		
        LeaseWareUtils.clearFromTrigger();
        pr = [select id,accounting_period__c,prepayment__c,Payment_Amount_Cur__c,Available_Balance_Cur__c 
                    from Payment_Receipt__c where name ='P2'];
        ppWrap = ApplyPaymentController.fetchInitData(invoice.id);
        System.assertEquals(ppWrap.invoiceRec.id,invoice.id);
        ppWrap.prePaymentRec = pr;
        ppWrap.prePaymentDate = system.today();
        ApplyPaymentController.applyPrePayment(JSON.serialize(ppWrap),50000);
		
        LeaseWareUtils.clearFromTrigger();
        paymentReciptlist = [select id,Name,Amount_Paid__c,Payment_Receipt__r.Available_Balance_Cur__c from Payment_Receipt_Line_Item__c where Payment_Receipt__c = :pr.id];
		LeaseWareUtils.clearFromTrigger();
        py = [select id,amount__c,payment_status__c from payment__c where invoice__r.id = :invoice.id and Y_Hidden_Payment_Std_Line_Item__c = :paymentReciptlist.id ];
        if(py.payment_status__c == 'Pending'){
            LeaseWareUtils.clearFromTrigger();
            py.payment_status__c = 'Approved';
            update py;
        }
          
        paymentReciptlist = [select id,Name,Amount_Paid__c,status__c,Payment_Receipt__r.Available_Balance_Cur__c from Payment_Receipt_Line_Item__c where Payment_Receipt__c = :pr.id];
        system.assertEquals(paymentReciptlist.Amount_Paid__c,50000);
        system.assertEquals(paymentReciptlist.status__c,'Approved');
        system.assertEquals(paymentReciptlist.Payment_Receipt__r.Available_Balance_Cur__c,150000);

        LeaseWareUtils.TriggerDisabledFlag=true;               
        py.payment_status__c = 'Cancelled-Pending';
            update py;
        LeaseWareUtils.TriggerDisabledFlag=false;
        LeaseWareUtils.clearFromTrigger();
        py.payment_status__c = 'Cancelled';
        update py;

        paymentReciptlist = [select id,Name,Amount_Paid__c,status__c,Payment_Receipt__r.Available_Balance_Cur__c from Payment_Receipt_Line_Item__c where Payment_Receipt__c = :pr.id];
        system.assertEquals(paymentReciptlist.Amount_Paid__c,50000);
        system.assertEquals(paymentReciptlist.Payment_Receipt__r.Available_Balance_Cur__c,200000);
        system.assertEquals(paymentReciptlist.status__C,'Cancelled');
        

        Test.stopTest();
    }   
}