public with sharing class AssetDealAnalysisController {
    
    private static map<string, integer> mapDataSources = new map<string, integer>{'Portfolio Acquisitions' => 1,
        'Closed Deals' => 2, 'All Deals' => 3, 'Current Leases' => 4, 'Market Intel' => 5, 'World Fleet' => 6};
    private static map<string, integer> mapCriteria = new map<string, integer>{'>=' => 1,
                '=' => 2, '<=' => 3};
    private static map<string, integer> mapRatings = new map<string, integer>{'Any' => 1, '1' => 2, '2' => 3, '3' => 4, 
                        '4' => 5, '5' => 6, '6' => 7, '7' => 8, '8' => 9, '9' => 10, '10' => 11, '11' => 12, '12' => 13};
    private static Lease__c AssetDetailsForLease;
    private static Aircraft__c Asset;        
    private static Aircraft_Proposal__c AssetDetails;
    private static Aircraft_Proposal__c AssetDetailsVariant;
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    
    @AuraEnabled
    public static Map<String, List<DealAnalysisData.Data>> getDataSources(decimal VintageRange, String FinancialRisk, String Source,String Variant,
                                                       String AircraftType, decimal YearOfManufacture, String AssetDetailId,
                                                       Integer leaseDateRange, String recordType){  
                                                           
                                                           
                                                           system.debug('getDataSources years '+VintageRange + ' FinanacialRisk '+FinancialRisk+ '  source '+source+ 'variant '+variant);
                                                           system.debug('getDataSources AircraftType '+AircraftType + ' YearOfManufacture '+YearOfManufacture+ ' AssetDetailId '+AssetDetailId);
                                                           system.debug('getDataSources leaseDateRange '+leaseDateRange + ' recordType '+recordType);
                                                           List<String> SourceList = null;
                                                           if(String.isNotBlank(Source)) { 
                                                               SourceList = Source.split(';');
                                                           }
                                                           List<String> financialList = null; 
                                                           if(String.isNotBlank(FinancialRisk)) { 
                                                               financialList = FinancialRisk.split(';');
                                                           }
                                                           system.debug('getDataSources SourceList '+SourceList+' financialList: '+financialList);
                                                           if(SourceList != null && SourceList.size() > 0) {
                                                               Map<String, List<DealAnalysisData.Data>> termsMap = new Map<String, List<DealAnalysisData.Data>>();
                                                               termsMap.put('All', new List<DealAnalysisData.Data>()); 
                                                               for(String src : SourceList) {
                                                                   List <DealAnalysisData.Data> data = AIUtils.GetDealAnalysisData(AircraftType, YearOfManufacture, VintageRange, financialList,
                                                                                                                                   mapDataSources.get(src),leaseDateRange, recordType,variant);
                                                                  // List <DealAnalysisData.Data> data = AIUtils.TempGetStaticData();
                                                                   system.debug('getDataSources result '+data);
                                                                   system.debug('getDataSources result termsMap all: '+termsMap);
                                                                   if(data != null && data.size() > 0) {
                                                                       for(Integer i =0 ; i < data.size(); i++) {
                                                                           DealAnalysisData.Data dataItem = data.get(i);
                                                                           
                                                                           if(!termsMap.containsKey(dataItem.Heading)) {
                                                                               termsMap.put(dataItem.Heading, new List<DealAnalysisData.Data>()); 
                                                                           }
                                                                           dataItem.Source = src;
                                                                           termsMap.get(dataItem.Heading).add(dataItem);
                                                                       }
                                                                   }
                                                                   system.debug('getDataSources terms '+termsMap);
                                                               }
                                                                 system.debug('getDataSources final terms '+termsMap);
                                                               return termsMap;
                                                           }
                                                           return null;
                                                       }
    
    
    @AuraEnabled
    public static List <String> getPicklist(){
        List <String> picklist = new List <String>();
        for(string curCrit : mapCriteria.keySet()){
            picklist.add(curCrit);
        }
        System.debug('picklist :'+picklist);
        return picklist;
    }
    
    @AuraEnabled
    public static List<String> getGradePicklist(){
        List<String> picklist = new List<String>();
        for(string curRating : mapRatings.keySet()){
            picklist.add(curRating);
        }
        return picklist;
    }
    
    @AuraEnabled
    public static List<String> getSourcePicklist(){
        List<String> picklist = new List<String>();
        for(string curOption : mapDataSources.keySet()){
            picklist.add(curOption);
        }
        return picklist;
    }
    
    @AuraEnabled
    public static Boolean getPageType(Id recordId,String sObjName){
        if(sObjName.contains('Aircraft_Proposal__c') || sObjName.contains('Pricing_Run_New__c')
           || sObjName.contains('Aircraft__c') || sObjName.contains('Lease__c')
           || sObjName.contains('Marketing_Activity__c')){
            return true;
        }
        else 
            return false;
    }
    
    @AuraEnabled
    public static Map<String, List<String>> getPicklistForRecords() {
        String prefix = leasewareutils.getNamespacePrefix();
        string objectName = prefix + 'Aircraft__c';
        List<RecordType> recordTypeList = [select Id,Name from RecordType where sObjectType=:objectName];
        System.debug('getPicklistForRecords recordTypeList: ' + recordTypeList);
        if(recordTypeList.size() > 0) {
            Map<String, List<String>> recordTypePickList = new  Map<String, List<String>>();
            string fieldName = prefix + 'Aircraft_Type__c';
            for(RecordType rcd: recordTypeList) {
                Map<string,string> picklistValueMap = LeaseWareUtils.getValues(objectName,rcd.id,fieldName);
                if(picklistValueMap != null) {
                    recordTypePickList.put(rcd.Name, picklistValueMap.values());
                }
            }
            return recordTypePickList;
        }    
        return null;
    } 
    
    @AuraEnabled
    public static String fetchRecordType(String aircraftType){
        List<Aircraft__c > assets = [SELECT  RecordType.Name 
                 FROM Aircraft__c WHERE Aircraft_Type__c =:aircraftType 
                ];
        System.debug('Asset Record Type'+assets);
        if(assets.size() > 0) {
            System.debug('Asset Record Type'+assets[0].RecordType.Name);
        	return assets[0].RecordType.Name;
        }
        return 'Aircraft';
    }
    
    @AuraEnabled
    public static List <String> getPickListOptions(String sobj, string fieldname) {
        string prefix = leasewareutils.getNamespacePrefix();
        
        string sobjNew = sobj;
        if(sobj.indexof(prefix) == -1) {
            sobjNew = prefix + sobj;
        }
        
        string fieldNameNew = fieldname;
        if(fieldname.indexof(prefix) == -1) {
            fieldNameNew = prefix + fieldname;
        }
        
        List <String> optionList = new list <String>();
        SObjectType objType = Schema.getGlobalDescribe().get(sobjNew);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list <Schema.PicklistEntry> values = fieldMap.get(fieldNameNew).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            optionList.add(a.getValue());
        }
        
        return optionList;
    }
    
    @AuraEnabled
    public static PresetData getPresetValues(Id recordId, String sObjName){
        System.debug('getPresetValues recordId  '+recordId + ' sObjName: '+sObjName);
        try{
            if(sObjName.contains('Aircraft_Proposal__c')) {
                List<Aircraft_Proposal__c> AP = [ select Aircraft_Type__c ,Marketing_Activity__r.Asset_Type__c ,Aircraft__r.RecordType.Name,
                                                 Y_Hidden_DOM__c, Aircraft__c ,Aircraft__r.Aircraft_Variant__c , 
                                                 World_Fleet_Aircraft__c ,World_Fleet_Aircraft__r.AircraftType__c ,
                                                 World_Fleet_Aircraft__r.AircraftVariant__c, World_Fleet_Aircraft__r.BuildYear__c, 
                                                 Engine__c, Engine__r.Engine_Model__c, Engine__r.Engine_Thrust__c, 
                                                 Engine__r.Date_of_Manufacture__c, Engine_Type_new__c , 
                                                 Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c 
                                                 from Aircraft_Proposal__c where Id =: recordId];
                System.debug('getPresetValues AP AssetInDeal '+AP);
                if(AP != null && AP.size() > 0) {
                    System.debug('getPresetValues AP not null '+AP);
                    if(AP[0].Marketing_Activity__r.Asset_Type__c == 'Aircraft') {
                        System.debug('getPresetValues AP : aircraft'+AP);
                        if(AP[0].Aircraft__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Aircraft_Type__c;
                            data.Variant = AP[0].Aircraft__r.Aircraft_Variant__c;
                            data.DateOfManf = AP[0].Y_Hidden_DOM__c;
                            data.RecordType = 'Aircraft';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for deal-assetType-Aircraft'+data);
                            return data;
                            
                        }
                        else if(AP[0].World_Fleet_Aircraft__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].World_Fleet_Aircraft__r.AircraftType__c;
                            data.Variant = AP[0].World_Fleet_Aircraft__r.AircraftVariant__c;
                            if(AP[0].World_Fleet_Aircraft__r.BuildYear__c != null) {
                                Integer year = Integer.valueOf(AP[0].World_Fleet_Aircraft__r.BuildYear__c);
                                data.DateOfManf = Date.newInstance((year - 1), 1, 1);
                            }
                            data.RecordType = 'Aircraft';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for Aircraft is null '+data);
                            return data;
                        }
                    }
                    else if(AP[0].Marketing_Activity__r.Asset_Type__c == 'Engine') {
                        System.debug('getPresetValues AP Engine '+AP);
                        if(AP[0].Engine__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Engine__r.Engine_Model__c;
                            data.Variant = AP[0].Engine__r.Engine_Thrust__c;
                            data.DateOfManf = AP[0].Engine__r.Date_of_Manufacture__c;
                            data.RecordType = 'Engine';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for deal-assetType-Engine '+data);
                            return data;
                        }
                        else if(AP[0].Engine_Type_new__c != null){
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Engine_Type_new__c;
                            data.Variant = null;
                            data.DateOfManf = date.today();
                            data.RecordType = 'Engine';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for Engine is null '+data);
                            return data;    
                        }
                        else if(AP[0].Aircraft__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Aircraft_Type__c;
                            data.Variant = AP[0].Aircraft__r.Aircraft_Variant__c;
                            data.DateOfManf = AP[0].Y_Hidden_DOM__c;
                            data.RecordType = 'Engine';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for deal-assetType-Aircraft'+data);
                            return data;
                            
                        }
                    }
                }
            }
            else if(sObjName.contains('Pricing_Run_New__c')) {
                List<Pricing_Output_New__c> PRN = [select Asset_Term__c from Pricing_Output_New__c where Pricing_Run__c = :recordId
                                                   order by CreatedDate limit 1];
                System.debug('getPresetValues PRN '+PRN);
                if(PRN != null && PRN.size() > 0) {
                    String term = PRN[0].Asset_Term__c;
                    List<Aircraft_Proposal__c> AP = [ select Aircraft_Type__c ,Marketing_Activity__r.Asset_Type__c ,
                                                     Y_Hidden_DOM__c, Aircraft__c ,Aircraft__r.Aircraft_Variant__c , 
                                                     World_Fleet_Aircraft__c ,World_Fleet_Aircraft__r.AircraftType__c ,
                                                     World_Fleet_Aircraft__r.AircraftVariant__c, World_Fleet_Aircraft__r.BuildYear__c, 
                                                     Engine__c, Engine__r.Engine_Model__c, Engine__r.Engine_Thrust__c, 
                                                     Engine__r.Date_of_Manufacture__c, Engine_Type_new__c,
                                                     Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c 
                                                     from Aircraft_Proposal__c where Id =: term];
                    if(AP != null && AP.size() > 0) {
                        System.debug('getPresetValues PRN '+AP[0]);
                        if(AP[0].Marketing_Activity__r.Asset_Type__c == 'Aircraft') {
                            if(AP[0].Aircraft__c != null) {
                                PresetData data = new PresetData();
                                data.AircraftType = AP[0].Aircraft_Type__c;
                                data.Variant = AP[0].Aircraft__r.Aircraft_Variant__c;
                                data.DateOfManf = AP[0].Y_Hidden_DOM__c;
                                data.RecordType = 'Aircraft';
                                data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                                return data;
                            }
                            else if(AP[0].World_Fleet_Aircraft__c != null) {
                                PresetData data = new PresetData();
                                data.AircraftType = AP[0].World_Fleet_Aircraft__r.AircraftType__c;
                                data.Variant = AP[0].World_Fleet_Aircraft__r.AircraftVariant__c;
                                if(AP[0].World_Fleet_Aircraft__r.BuildYear__c != null) {
                                    Integer year = Integer.valueOf(AP[0].World_Fleet_Aircraft__r.BuildYear__c);
                                    data.DateOfManf = Date.newInstance((year - 1), 1, 1);
                                }
                                data.RecordType = 'Aircraft';
                                data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                                return data;
                            }
                        }
                        else if(AP[0].Marketing_Activity__r.Asset_Type__c == 'Engine') {
                            if(AP[0].Engine__c != null) {
                                PresetData data = new PresetData();
                                data.AircraftType = AP[0].Engine__r.Engine_Model__c;
                                data.Variant = AP[0].Engine__r.Engine_Thrust__c;
                                data.DateOfManf = AP[0].Engine__r.Date_of_Manufacture__c;
                                data.RecordType = 'Engine';
                                data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                                return data;
                            }
                            else if(AP[0].Engine_Type_new__c != null){
                                PresetData data = new PresetData();
                                data.AircraftType = AP[0].Engine_Type_new__c;
                                data.Variant = null;
                                data.DateOfManf = date.today();
                                data.RecordType = 'Engine';
                                data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                                return data;    
                            }
                            else if(AP[0].Aircraft__c != null) {
                                PresetData data = new PresetData();
                                data.AircraftType = AP[0].Aircraft_Type__c;
                                data.Variant = AP[0].Aircraft__r.Aircraft_Variant__c;
                                data.DateOfManf = AP[0].Y_Hidden_DOM__c;
                                data.RecordType = 'Aircraft';
                                data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                                return data;
                            }
                        }
                        
                    }
                }
            }
            else if(sObjName.contains('Aircraft__c')){
                Aircraft__c asset = [ SELECT Aircraft_Type__c ,Aircraft_Variant__c,RecordType.Name,Date_of_Manufacture__c,
                                     Engine_Variant__c, Lease__r.Operator__r.Financial_Risk_Profile__c    
                                     FROM Aircraft__c WHERE Id =: recordId];
                if(asset != null ) {
                    if(asset.RecordType.Name == 'Aircraft') {
                        PresetData data = new PresetData();
                            data.AircraftType = asset.Aircraft_Type__c;
                            data.Variant = asset.Aircraft_Variant__c;
                            data.DateOfManf = asset.Date_of_Manufacture__c;
                            data.RecordType = 'Aircraft';
                        	data.FinancialRisk = asset.Lease__r.Operator__r.Financial_Risk_Profile__c;
                            System.debug('data for asset-assetType-Aircraft'+data);
                            return data;
                         
                    }
                    else if(asset.RecordType.Name == 'Engine') {
                        PresetData data = new PresetData();
                            data.AircraftType = asset.Aircraft_Type__c;
                            data.Variant = asset.Engine_Variant__c;
                            data.DateOfManf = asset.Date_of_Manufacture__c;
                            data.RecordType = 'Engine';
                        	data.FinancialRisk = asset.Lease__r.Operator__r.Financial_Risk_Profile__c;
                            System.debug('data for asset-assetType-Engine '+data);
                            return data;
                        }
                        
                    }
                }
            else if(sObjName.contains('Lease__c')){
                Lease__c lease = [Select Aircraft__c, Aircraft__r.RecordType.Name,Aircraft__r.Date_of_Manufacture__c,Aircraft__r.Aircraft_Type__c,
                                  Aircraft__r.Aircraft_Variant__c, Operator__r.Financial_Risk_Profile__c 
                                  from Lease__c where Id =: recordId];
                string recordType = lease.Aircraft__r.RecordType.Name;
                if(recordType == 'Aircraft'){
                   PresetData data = new PresetData();
                            data.AircraftType = lease.Aircraft__r.Aircraft_Type__c;
                            data.Variant = lease.Aircraft__r.Aircraft_Variant__c;
                            data.DateOfManf = lease.Aircraft__r.Date_of_Manufacture__c;
                            data.RecordType = 'Aircraft';
                    		data.FinancialRisk = lease.Operator__r.Financial_Risk_Profile__c;
                            System.debug('data for lease-assetType-Aircraft'+data);
                            return data; 
                }
                if(recordType == 'Engine'){
                    PresetData data = new PresetData();
                            data.AircraftType = lease.Aircraft__r.Aircraft_Type__c;
                            data.Variant = lease.Aircraft__r.Engine_Variant__c;
                            data.DateOfManf = lease.Aircraft__r.Date_of_Manufacture__c;
                            data.RecordType = 'Engine';
                    		data.FinancialRisk = lease.Operator__r.Financial_Risk_Profile__c;
                            System.debug('data for lease-assetType-Engine '+data);
                            return data;

                }
            }
            else if(sObjName.contains('Marketing_Activity__c')){
             List<Aircraft_Proposal__c> AP = [ select Aircraft_Type__c ,Marketing_Activity__r.Asset_Type__c ,Marketing_Activity__c,
                                                 Y_Hidden_DOM__c, Aircraft__c ,Aircraft__r.Aircraft_Variant__c , 
                                                 World_Fleet_Aircraft__c ,World_Fleet_Aircraft__r.AircraftType__c ,
                                                 World_Fleet_Aircraft__r.AircraftVariant__c, World_Fleet_Aircraft__r.BuildYear__c, 
                                                 Engine__c, Engine__r.Engine_Model__c, Engine__r.Engine_Thrust__c, 
                                                 Engine__r.Date_of_Manufacture__c, Engine_Type_new__c, 
                                                 Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c
                                                 from Aircraft_Proposal__c where Marketing_Activity__c =: recordId];
                System.debug('getPresetValues AP '+AP);
                if(AP != null && AP.size() > 0) {
                    if(AP[0].Marketing_Activity__r.Asset_Type__c == 'Aircraft') {
                        if(AP[0].Aircraft__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Aircraft_Type__c;
                            data.Variant = AP[0].Aircraft__r.Aircraft_Variant__c;
                            data.DateOfManf = AP[0].Y_Hidden_DOM__c;
                            data.RecordType = 'Aircraft';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for deal-assetType-Aircraft'+data);
                            return data;
                            
                        }
                        else if(AP[0].World_Fleet_Aircraft__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].World_Fleet_Aircraft__r.AircraftType__c;
                            data.Variant = AP[0].World_Fleet_Aircraft__r.AircraftVariant__c;
                            if(AP[0].World_Fleet_Aircraft__r.BuildYear__c != null) {
                                Integer year = Integer.valueOf(AP[0].World_Fleet_Aircraft__r.BuildYear__c);
                                data.DateOfManf = Date.newInstance((year - 1), 1, 1);
                            }
                            data.RecordType = 'Aircraft';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for deal -from WorldFleet-assetType : Aircraft '+data);
                            return data;
                        }
                    }
                    else if(AP[0].Marketing_Activity__r.Asset_Type__c == 'Engine') {
                        if(AP[0].Engine__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Engine__r.Engine_Model__c;
                            data.Variant = AP[0].Engine__r.Engine_Thrust__c;
                            data.DateOfManf = AP[0].Engine__r.Date_of_Manufacture__c;
                            data.RecordType = 'Engine';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for deal-assetType-Engine '+data);
                            return data;
                        }
                        else if(AP[0].Engine_Type_new__c != null){
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Engine_Type_new__c;
                            data.Variant = null;
                            data.DateOfManf = date.today();
                            data.RecordType = 'Engine';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for -from WorldFleet-assetType :Engine '+data);
                            return data;    
                        }
                        else if(AP[0].Aircraft__c != null) {
                            PresetData data = new PresetData();
                            data.AircraftType = AP[0].Aircraft_Type__c;
                            data.Variant = AP[0].Aircraft__r.Aircraft_Variant__c;
                            data.DateOfManf = AP[0].Y_Hidden_DOM__c;
                            data.RecordType = 'Engine';
                            data.FinancialRisk = AP[0].Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c;
                            System.debug('data for deal-assetType-Aircraft'+data);
                            return data;
                            
                        }
                    }
                }
            }   
            
            
            else{
                PresetData data = new PresetData();
                data.AircraftType = 'A320';
                data.Variant = '200';
                data.DateOfManf = Date.today();
                data.RecordType = 'Aircraft';
                return data;   
            }
         }
        catch(Exception e){
            System.debug('getPresetValues Exception '+e);
           // throw new AuraHandledException(Utility.getErrorMessage(e));
           // Throw an AuraHandledException with custom data
         // CustomExceptionData data = new CustomExceptionData('Record Data Missing');
         // throw new AuraHandledException(JSON.serialize(data));
        } 
        
        System.debug('Out of Try catch block');
        return null;
    }
    
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        
        Schema.sObjectType objType = objDetail.getSObjectType();
        if (objType==null){
            return objResults;
        }
        
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objResults;     
        }
        
        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        return objResults;
    }
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';
        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        
        return validForBits;
    }
    
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }
    
    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }
        
    }
    
    public class PresetData{
        @AuraEnabled public String AircraftType  {get; set;}
        @AuraEnabled public String Variant  {get; set;}
        @AuraEnabled public Date DateOfManf  {get; set;}
        @AuraEnabled public String RecordType  {get; set;}
        @AuraEnabled public String FinancialRisk  {get; set;}
    }
    
    // Wrapper class for my custom exception data
    public class CustomExceptionData {
    	public String name;
    	public String message;
    	public Integer code;

    public CustomExceptionData(String name) {
        this.message = message;
    }
}




    
}