/*
	Created By: Anjani Prasad
	
	Decription :
		Date : 10th June 2016

	






*/




public class IssuanceTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'IssuanceTriggerHandlerBefore';
    private final string triggerAfter = 'IssuanceTriggerHandlerAfter';
    

             
    // declaring private variable : accessing from before and after trigger both                
    
    public IssuanceTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('IssuanceTriggerHandler.beforeInsert(+)');

		

	    system.debug('IssuanceTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('IssuanceTriggerHandler.beforeUpdate(+)');

		

    	system.debug('IssuanceTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('IssuanceTriggerHandler.beforeDelete(+)');

    	system.debug('IssuanceTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceTriggerHandler.afterInsert(+)');
    	
 		//UpdateRatingAgencyPartyLookup();
    	
    	system.debug('IssuanceTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceTriggerHandler.afterUpdate(+)');
    	
		//UpdateRatingAgencyPartyLookup();
    	
    	system.debug('IssuanceTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('IssuanceTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('IssuanceTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('IssuanceTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

/*
	// after insert,after update
	private void UpdateRatingAgencyPartyLookup(){
		
		Issuance__c[] listNew = (list<Issuance__c>)trigger.new ;
		string ratAgecy1,ratAgecy2,ratAgecy3;
		Issuance__c oldRec;
		for(Issuance__c curRec:listNew){
			if(trigger.IsInsert){
				if(currec.Rating_Agency_1__c!=null)ratAgecy1 = currec.Rating_Agency_1__c;
				if(currec.Rating_Agency_2__c!=null)ratAgecy2 = currec.Rating_Agency_2__c;
				if(currec.Rating_Agency_3__c!=null)ratAgecy3 = currec.Rating_Agency_3__c;
			}else if(trigger.IsUpdate){
				oldRec = (Issuance__c)trigger.oldMap.get(curRec.Id);
				if((oldRec.Rating_1__c!=curRec.Rating_1__c || oldRec.Rating_Agency_1__c!=curRec.Rating_Agency_1__c)  && curRec.Rating_Agency_1__c!=null)ratAgecy1 = currec.Rating_Agency_1__c;
				if((oldRec.Rating_2__c!=curRec.Rating_2__c || oldRec.Rating_Agency_2__c!=curRec.Rating_Agency_2__c)  && curRec.Rating_Agency_2__c!=null)ratAgecy2 = currec.Rating_Agency_2__c;
				if((oldRec.Rating_3__c!=curRec.Rating_3__c || oldRec.Rating_Agency_3__c!=curRec.Rating_Agency_3__c)  && curRec.Rating_Agency_3__c!=null)ratAgecy3 = currec.Rating_Agency_3__c;
			}
		}
		map<string,string> mapIds = new map<string,string>();
		if(ratAgecy1!=null || ratAgecy2!=null || ratAgecy3!=null ){
			
			map<string,List_Of_Rating_Agency__c> mapTatAgency = new map<string,List_Of_Rating_Agency__c>();
			list<List_Of_Rating_Agency__c> RatingAgencyList;
			if(trigger.IsUpdate){
				RatingAgencyList=[select id,Counterparty__c,Issuance__c from List_Of_Rating_Agency__c where Issuance__c in :listNew ];
				for(List_Of_Rating_Agency__c curRatAgency :RatingAgencyList){
					mapTatAgency.put(curRatAgency.Counterparty__c+'-'+curRatAgency.Issuance__c,curRatAgency);
				}
			}
			
			
			Bank__c[] partyList = [select id,name
									from Bank__c 
									where Party_Type__c='Rating Agency'
									and (name in (:ratAgecy1,:ratAgecy2,:ratAgecy3))];
			for(Bank__c currec :partyList){
				mapIds.put(currec.name,currec.id);
			}						 
			list<List_Of_Rating_Agency__c> insUpdAgency = new list<List_Of_Rating_Agency__c>();
			List_Of_Rating_Agency__c RatingAgencyTemp;
			for(Issuance__c curRec:listNew){
				if(trigger.IsInsert){
					if(currec.Rating_Agency_1__c!=null) insUpdAgency.add(new List_Of_Rating_Agency__c(name=currec.Rating_Agency_1__c,Rating__c=currec.Rating_1__c,Issuance__c=currec.Id,Counterparty__c=mapIds.get(currec.Rating_Agency_1__c)));   
					if(currec.Rating_Agency_2__c!=null)insUpdAgency.add(new List_Of_Rating_Agency__c(name=currec.Rating_Agency_2__c,Rating__c=currec.Rating_2__c,Issuance__c=currec.Id,Counterparty__c=mapIds.get(currec.Rating_Agency_2__c)));
					if(currec.Rating_Agency_3__c!=null)insUpdAgency.add(new List_Of_Rating_Agency__c(name=currec.Rating_Agency_3__c,Rating__c=currec.Rating_3__c,Issuance__c=currec.Id,Counterparty__c=mapIds.get(currec.Rating_Agency_3__c)));
				}else if(trigger.IsUpdate){
					oldRec = (Issuance__c)trigger.oldMap.get(curRec.Id);
					if((oldRec.Rating_1__c!=curRec.Rating_1__c || oldRec.Rating_Agency_1__c!=curRec.Rating_Agency_1__c)  && curRec.Rating_Agency_1__c!=null){
						if(mapIds.containsKey(currec.Rating_Agency_1__c)){
							if(mapTatAgency.containsKey(mapIds.get(currec.Rating_Agency_1__c)+'-'+currec.id)){
								RatingAgencyTemp = mapTatAgency.get(mapIds.get(currec.Rating_Agency_1__c)+'-'+currec.id);
								RatingAgencyTemp.Rating__c=currec.Rating_1__c;
								insUpdAgency.add(RatingAgencyTemp);   //update
							}else{
								insUpdAgency.add(new List_Of_Rating_Agency__c(name=currec.Rating_Agency_1__c,Rating__c=currec.Rating_1__c,Issuance__c=currec.Id,Counterparty__c=mapIds.get(currec.Rating_Agency_1__c)));   
							}
						}
					}
					if((oldRec.Rating_2__c!=curRec.Rating_2__c || oldRec.Rating_Agency_2__c!=curRec.Rating_Agency_2__c)  && curRec.Rating_Agency_2__c!=null){
						if(mapIds.containsKey(currec.Rating_Agency_2__c)){
							if(mapTatAgency.containsKey(mapIds.get(currec.Rating_Agency_2__c)+'-'+currec.id)){
								RatingAgencyTemp = mapTatAgency.get(mapIds.get(currec.Rating_Agency_2__c)+'-'+currec.id);
								RatingAgencyTemp.Rating__c=currec.Rating_2__c;
								insUpdAgency.add(RatingAgencyTemp);   //update
							}else{
								insUpdAgency.add(new List_Of_Rating_Agency__c(name=currec.Rating_Agency_2__c,Rating__c=currec.Rating_2__c,Issuance__c=currec.Id,Counterparty__c=mapIds.get(currec.Rating_Agency_2__c)));   
							}
						}
					}
					if((oldRec.Rating_3__c!=curRec.Rating_3__c || oldRec.Rating_Agency_3__c!=curRec.Rating_Agency_3__c)  && curRec.Rating_Agency_3__c!=null){
						if(mapIds.containsKey(currec.Rating_Agency_3__c)){
							if(mapTatAgency.containsKey(mapIds.get(currec.Rating_Agency_3__c)+'-'+currec.id)){
								RatingAgencyTemp = mapTatAgency.get(mapIds.get(currec.Rating_Agency_3__c)+'-'+currec.id);
								RatingAgencyTemp.Rating__c=currec.Rating_3__c;
								insUpdAgency.add(RatingAgencyTemp);   //update
							}else{
								insUpdAgency.add(new List_Of_Rating_Agency__c(name=currec.Rating_Agency_3__c,Rating__c=currec.Rating_3__c,Issuance__c=currec.Id,Counterparty__c=mapIds.get(currec.Rating_Agency_3__c)));   
							}
						}
					}					
					
					
				}	
			}//end of for

			if(insUpdAgency.size()>0) upsert insUpdAgency;
		}//end of If
		
	}
*/

}