public without sharing class LesseeHomeController {

	@AuraEnabled
	public static DataWrapper getData(){

		DataWrapper data = new DataWrapper();
		if(!Test.isRunningTest())
		{
			data.lastRefreshedDate = '';
			ServiceSetting__c lwServiceSetup = ServiceSetting__c.getValues('External Data Source');
			if( lwServiceSetup!=null && lwServiceSetup.get('Token__c') != null)
			{
				data.lastRefreshedDate = String.valueof(lwServiceSetup.get('Token__c'));
			}
		}
			
		List<Operator__c> operators = UtilityWithSharingController.getOperators();

		for(AggregateResult result : [select sum(Outstanding_Amount__c) dueAmount from Invoice_External__c
		                              where lease__r.lessee__c in : operators and
		                              lease__r.Lease_Status_External__c = 'Active' and
		                                                                  Outstanding_Amount__c != null ]) {

			data.totalOutStanding += (Decimal) (result.get('dueAmount') == null ? 0 : result.get('dueAmount'));
		}

		data.noOfLeasesToBeExpired = [select COUNT() from lease__c
		                              where Lease_End_Date_New__c < NEXT_N_MONTHS:6
		                              and Lease_End_Date_New__c >= TODAY
		                              and lessee__c in : operators];


		/*Map<Id,List<Date>> monthlyUtilizationMap = new Map<Id,List<Date>>();

		   for(Utilization_Report_Staging__c urStaging : [select id, Utilization_Start_Date__c, Lease_UR_Staging__c
		                                                           from Utilization_Report_Staging__c
		                                                           where Lease_UR_Staging__r.lessee__c in : operators
		                                                           and Utilization_Start_Date__c != null
		                                                           and Lease_UR_Staging__r.Lease_Status_External__c = 'Active']){

		     if(!monthlyUtilizationMap.containsKey(urStaging.Lease_UR_Staging__c)){
		         monthlyUtilizationMap.put(urStaging.Lease_UR_Staging__c, new List<Date>());
		     }

		     monthlyUtilizationMap.get(urStaging.Lease_UR_Staging__c).add(urStaging.Utilization_Start_Date__c.toStartOfMonth());
		   } */

		// query leases with latest utilization filed.
		for(Lease__c lease : [select id, Lease_Start_Date_New__c,
		                                  Aircraft__r.MSN_Number__c,
		                                  Aircraft__r.recordType.developerName,
		                                  (select id, Period_End_Date__c, lease__c
		                                   from Assembly_Utilization_External__r
		                                   Where Period_End_Date__c < THIS_MONTH
		                                   order by Period_End_Date__c desc limit 1)
		                                  from Lease__c
		                                  where lessee__c in : operators
		                                  and Lease_Status_External__c = 'Active']) {

			data.totalActiveLeases += 1;

			//List<Date> expectedDates = getMonths(lease.Lease_Start_Date_New__c);
			String type = lease.Aircraft__r.recordType.developerName == 'Aircraft' ? 'MSN' : 'ESN';

			if(lease.Assembly_Utilization_External__r.size()>0) {
				Integer difference = lease.Assembly_Utilization_External__r[0].Period_End_Date__c.monthsBetween(System.Today());
				if(difference > 1) {
					String dateRange;
					if(difference == 2) {
						dateRange = Datetime.newInstance(lease.Assembly_Utilization_External__r[0].Period_End_Date__c.addMonths(1), Time.newInstance(0,0,0,0)).format('MMM, YYYY');
					}else{
						dateRange = Datetime.newInstance(lease.Assembly_Utilization_External__r[0].Period_End_Date__c.addMonths(1), Time.newInstance(0,0,0,0)).format('MMM, YYYY') + ' - ' + Datetime.newInstance(System.today().toStartOfMonth().addDays(-1), Time.newInstance(0,0,0,0)).format('MMM, YYYY');
					}

					data.openURList.add(new OpenUtilizationWrapper( type + ' ' + lease.Aircraft__r.MSN_Number__c, dateRange));
				}
			}else{

				// Either there is no UR filed as of now or Lease just started in the current month.
				Integer difference = lease.Lease_Start_Date_New__c.monthsBetween(System.Today());
				if(difference > 0) {
					String dateRange;
					if(difference == 1) {
						dateRange = Datetime.newInstance(lease.Lease_Start_Date_New__c, Time.newInstance(0,0,0,0)).format('MMM, YYYY');
					}else{
						dateRange = Datetime.newInstance(lease.Lease_Start_Date_New__c, Time.newInstance(0,0,0,0)).format('MMM, YYYY') + ' - ' + Datetime.newInstance(System.today().toStartOfMonth().addDays(-1), Time.newInstance(0,0,0,0)).format('MMM, YYYY');
					}
					data.openURList.add(new OpenUtilizationWrapper( type + ' ' + lease.Aircraft__r.MSN_Number__c, dateRange));
				}
			}

			/* if(expectedDates.size()>0){
			     for(Date urExpectedDate : expectedDates){
			         if(!monthlyUtilizationMap.containsKey(lease.Id) ||
			            !monthlyUtilizationMap.get(lease.Id).contains(urExpectedDate)){
			                data.openURList.add(new OpenUtilizationWrapper( type + ' ' + lease.Aircraft__r.MSN_Number__c,
			                                                        Datetime.newInstance(urExpectedDate, Time.newInstance(0,0,0,0)).format('MMM dd, YYYY') + ' - ' + Datetime.newInstance(urExpectedDate.addMonths(1).addDays(-1), Time.newInstance(0,0,0,0)).format('MMM dd, YYYY')));
			            }
			      }
			   }*/

		}

		return data;
	}

	/* private static String getMonths(Date startDate, Date endDate){

	     Integer difference = startDate.monthsBetween(endDate);
	     Date dateRange;

	     if(difference > 1){
	         if(difference == 2){
	             dateRange = Datetime.newInstance(startDate, Time.newInstance(0,0,0,0)).format('MMM, YYYY') + ' - ' + Datetime.newInstance(endDate, Time.newInstance(0,0,0,0)).format('MMM, YYYY');
	         }else{
	             dateRange = Datetime.newInstance(startDate, Time.newInstance(0,0,0,0)).format('MMM, YYYY') + ' - ' + Datetime.newInstance(endDate, Time.newInstance(0,0,0,0)).format('MMM, YYYY');
	         }
	     }
	   } */

	public class DataWrapper {

		@AuraEnabled public Integer totalActiveLeases {get; set;}
		@AuraEnabled public Decimal totalOutStanding {get; set;}
		@AuraEnabled public Integer noOfLeasesToBeExpired {get; set;} // in next 6 months.
		@AuraEnabled public List<OpenUtilizationWrapper> openURList {get; set;}
		@AuraEnabled public String lastRefreshedDate {get; set;}

		public DataWrapper(){
			totalActiveLeases = 0;
			totalOutStanding = 0.00;
			noOfLeasesToBeExpired = 0;
			openURList = new List<OpenUtilizationWrapper>();
		}
	}

	public class OpenUtilizationWrapper {

		@AuraEnabled public String title {get; set;}
		@AuraEnabled public String dateRange {get; set;}

		public OpenUtilizationWrapper(String title, String dateRange){
			this.title = title;
			this.dateRange = dateRange;
		}
	}

}