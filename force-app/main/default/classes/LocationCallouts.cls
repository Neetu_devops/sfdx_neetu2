//@lastupdated : 2016.04.27 6 PM
public class LocationCallouts {
     @future (callout=true)  // future method needed to run callouts from Triggers
      static public void getLocation( id recordId, String sobjectName )
      {
        String Namespaceprefix_temp = '';
        List<ApexClass> cls = [ SELECT NamespacePrefix FROM ApexClass where NamespacePrefix ='leaseworks' limit 1 ]; 
        if( cls.size()>0 ) 
        { 
            Namespaceprefix_temp  = cls[0].NamespacePrefix + '__'; 
        }

        List<sobject> sobjList = new List<sobject>();
        //gather info
        if( sobjectName.contains('Operator__c')  )
        {
          sobjList    = (list<Operator__c>)
                        [ SELECT Location__Latitude__s,Location__Longitude__s,
                          City__c,Country__c,Zip_Code__c,Address_Street__c 
                          FROM Operator__c WHERE id =:recordId ];
        }
        else if( sobjectName.contains('Counterparty__c')  ) {
          sobjList    = (list<Counterparty__c>)
                        [ SELECT Location__Latitude__s,Location__Longitude__s,
                          City__c,Country__c,Zip_Code__c,Address_Street__c 
                          FROM Counterparty__c WHERE id =:recordId ];     
        }
        else if( sobjectName.contains('Lessor__c')  ) {
          sobjList    = (list<Lessor__c>)
                        [ SELECT Y_Hidden_Location__Latitude__s,Y_Hidden_Location__Longitude__s,
                          City__c,Country__c,Zip_Code__c,Address_Street__c 
                          FROM Lessor__c WHERE id =:recordId ];     
        }
        // create an address string
        String address = '';
        if( sobjList.size()>0 ) 
        {
            if ( sobjList[0].get( Namespaceprefix_temp+'Address_Street__c' ) != null )
                address += sobjList[0].get( Namespaceprefix_temp+'Address_Street__c' ) +', ';
            if ( sobjList[0].get( Namespaceprefix_temp+'City__c' ) != null )
                address += sobjList[0].get( Namespaceprefix_temp+'City__c') +', ';
            if ( sobjList[0].get( Namespaceprefix_temp+'Zip_Code__c' ) != null )
                address += sobjList[0].get( Namespaceprefix_temp+'Zip_Code__c' ) +', ';
            if ( sobjList[0].get( Namespaceprefix_temp+'Country__c') != null )
                address += sobjList[0].get( Namespaceprefix_temp+'Country__c' );
      
            address = EncodingUtil.urlEncode(address, 'UTF-8');
            
            String apikey ='';
            string endPointUrl ='';

            LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
            if(lwSettings!=null){
                apikey       = lwSettings.Token__c;
                endPointUrl  = lwSettings.EndPointUrl__c;
            }

            // build callout
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endPointUrl+address+'&sensor=false&key='+apikey+'&language=no');
            request.setMethod('GET');
            request.setTimeout(120000);
     
            try {
                // callout
                HttpResponse response = http.send(request);
                system.debug('res.getBody()=='+response.getBody());
                // parse coordinates from response
                JSONParser parser = JSON.createParser(response.getBody());
                double lat = null;
                double lon = null;
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                        (parser.getText() == 'location'))   {
                           parser.nextToken(); // object start
                           while (parser.nextToken() != JSONToken.END_OBJECT)   {
                               String txt = parser.getText();
                               parser.nextToken();
                               if (txt == 'lat')
                                   lat = parser.getDoubleValue();
                               else if (txt == 'lng')
                                   lon = parser.getDoubleValue();
                           }     
                    }
                }

                // update coordinates if we get back
                if ( lat != null )    {
                    if( sobjectName.contains('Lessor__c') ) {
                        sobjList[0].put( Namespaceprefix_temp+'Y_Hidden_Location__Latitude__s',lat );
                        sobjList[0].put( Namespaceprefix_temp+'Y_Hidden_Location__Longitude__s',lon );
                    }
                    else {
                        sobjList[0].put( Namespaceprefix_temp+'Location__Latitude__s',lat );
                        sobjList[0].put( Namespaceprefix_temp+'Location__Longitude__s',lon );
                    }
                    update sobjList[0];
                }     
            } 
            catch (Exception ex) 
            {
                system.debug('exception ='+ex);
            }
        }        
    }
}