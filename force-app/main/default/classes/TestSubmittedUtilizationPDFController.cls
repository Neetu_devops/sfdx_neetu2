@isTest
public class TestSubmittedUtilizationPDFController {
    @isTest
    public static void testSubmitUtilPdfController(){
        Date lastDayOfMonth = System.today().addMonths(2).toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        
        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        Lease__c leaseRecord=new Lease__c();
        leaseRecord.Security_Deposit__c=200;
        leaseRecord.Lessee__c=operatorRecord.Id;
        leaseRecord.Lease_Start_Date_New__c=minStartDate;
        leaseRecord.Lease_End_Date_New__c=lastDayOfMonth;
        insert leaseRecord;
        
        Id recordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        
        Aircraft__c aircraftRecord=new Aircraft__c();
        aircraftRecord.Name='test aircraft';
        aircraftRecord.MSN_Number__c='123';
        aircraftRecord.Date_of_Manufacture__c = minStartDate;
        aircraftRecord.Aircraft_Type__c='737';
        aircraftRecord.Aircraft_Variant__c='700';
        aircraftRecord.TSN__c=1.00;
        aircraftRecord.CSN__c=1;
        aircraftRecord.RecordTypeId=recordTypeId;
        insert aircraftRecord;
        aircraftRecord.Lease__c = leaseRecord.Id;
        update aircraftRecord;
        
        leaseRecord.Aircraft__c = aircraftRecord.Id; 
        update leaseRecord;
        PageReference pageRef = Page.Submitted_Utilization_PDF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('assetId',aircraftRecord.Id);
        Test.startTest();
        SubmittedUtilizationPDFController subUtilPdfController=new SubmittedUtilizationPDFController();
        Test.stopTest();
        
    }
}