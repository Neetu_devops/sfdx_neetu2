public class InsuranceStipulationTriggerHandler implements ITrigger  {

    // Constructor
    private final string triggerBefore = 'InsuranceStipulationTriggerHandlerBefore';
    private final string triggerAfter = 'InsuranceStipulationTriggerHandlerAfter';
    
    
    public InsuranceStipulationTriggerHandler(){
    	
    }
    
    public void bulkBefore() {
		if (trigger.isDelete) return;
	}

    public void bulkAfter() {
        
    }

    public void beforeInsert() {

        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InsuranceStipulationTriggerHandler.beforeInsert(+)');
        system.debug('InsuranceStipulationTriggerHandler.beforeInsert(-)');
    }

    public void beforeUpdate() {
        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InsuranceStipulationTriggerHandler.beforeUpdate(+)');
        system.debug('InsuranceStipulationTriggerHandler.beforeUpdate(-)');
    }

    
    public void beforeDelete() {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InsuranceStipulationTriggerHandler.beforeDelete(+)');		
        system.debug('InsuranceStipulationTriggerHandler.beforeDelete(-)');
    }

    public void afterInsert() {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InsuranceStipulationTriggerHandler.afterInsert(+)');
        
        createvariabilitySchedule();
        system.debug('InsuranceStipulationTriggerHandler.afterInsert(-)');
    }

    public void afterUpdate() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InsuranceStipulationTriggerHandler.afterUpdate(+)');
        
        createvariabilitySchedule();
        system.debug('InsuranceStipulationTriggerHandler.afterUpdate(-)');
    }

    public void afterDelete() {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InsuranceStipulationTriggerHandler.afterDelete(+)');
		createvariabilitySchedule();
		system.debug('InsuranceStipulationTriggerHandler.afterDelete(-)');
    }

    public void afterUnDelete() {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InsuranceStipulationTriggerHandler.afterUnDelete(+)');
        // code here
        system.debug('InsuranceStipulationTriggerHandler.afterUnDelete(-)');
    }

    
    public void andFinally() {
        // insert any audit records

    }
    
	//After Insert and After Update and After Delete
	//Hint: InsertUpdate after Insert or Update of Insurance Stipulation  
    public void createvariabilitySchedule(){
    	system.debug('createvariabilitySchedule ++');    	
    	list<Insurance_Stipulation__c> listInsStipulation = (list<Insurance_Stipulation__c>) (trigger.isDelete ? trigger.old : trigger.new);
    	system.debug('listInsStipulation Size = '+listInsStipulation.size());
		set<id> setStupID=new set<id>();
		list<Insurance_Stipulation__c> listStipulation = new list<Insurance_Stipulation__c>();
		Insurance_Stipulation__c oldStipulation;
		set<id> setLeaseID =new set<id>(); //used when stip record get delete
        for(Insurance_Stipulation__c curStipulation: listInsStipulation){
            if(trigger.isInsert){
				system.debug('Insert case');
				setStupID.add(curStipulation.id);
                listStipulation.add(curStipulation);
            }
            else if(trigger.isUpdate){
				system.debug('Update case');
                oldStipulation= (Insurance_Stipulation__c)trigger.oldMap.get(curStipulation.Id);
				if(curStipulation.Start_Date__c!= oldStipulation.Start_Date__c || curStipulation.End_Date__c!= oldStipulation.End_Date__c || 
				   curStipulation.Stipulated_Requirement__c!= oldStipulation.Stipulated_Requirement__c || curStipulation.Variability__c!=  oldStipulation.Variability__c ||
				   curStipulation.Variability_Amount__c !=  oldStipulation.Variability_Amount__c || curStipulation.Variability_Percent__c != oldStipulation.Variability_Percent__c ||
				   curStipulation.Variability_Periodicity_in_months__c != oldStipulation.Variability_Periodicity_in_months__c ||
				   curStipulation.First_Renewal_Date__c != oldStipulation.First_Renewal_Date__c ||
				   curStipulation.First_Variable_Period__c != oldStipulation.First_Variable_Period__c){ 
				   if(!curStipulation.Dis_Var_Sch_Regen__c){
                    	listStipulation.add(curStipulation);
					}
					else{
						curStipulation.Dis_Var_Sch_Regen__c.addError('Changes will cause the Variability Schedules to regenerate. Please uncheck this checkbox to regenerate the schedules.');
                    	return;
					}
				}
				//Adding all the condition on which Variability Schedule Regeneration is allowed, to make Current and Next Certificate stamping
				if(curStipulation.Start_Date__c!= oldStipulation.Start_Date__c || curStipulation.End_Date__c!= oldStipulation.End_Date__c || 
				curStipulation.Insurance_Type__c!= oldStipulation.Insurance_Type__c ||  curStipulation.Variability_Periodicity_in_months__c != oldStipulation.Variability_Periodicity_in_months__c ||  
				curStipulation.Stipulated_Requirement__c != oldStipulation.Stipulated_Requirement__c  || curStipulation.Variability__c!= oldStipulation.Variability__c || 
				curStipulation.Variability_Amount__c!= oldStipulation.Variability_Amount__c || curStipulation.Variability_Percent__c!= oldStipulation.Variability_Percent__c ||
				curStipulation.First_Renewal_Date__c != oldStipulation.First_Renewal_Date__c || curStipulation.First_Variable_Period__c != oldStipulation.First_Variable_Period__c){
                    setStupID.add(curStipulation.id);
				}
            }
            else{ //Delete case
                system.debug('Delete case');
				setLeaseID.add(curStipulation.Lease__c);
            } 
		}
		system.debug('size of listStipulation='+listStipulation.size());
		
    	//Need to Create Readability Schedules 
    	list<Variability_Schedule__c> listReadabilityScheduleCreate = new list<Variability_Schedule__c>();
    	
    	
    	list<Variability_Schedule__c> listRSDelete = new list<Variability_Schedule__c>();
    	//Integer noOfReadabilitySchedule =0;
    	//decimal StipulatedRequirement;
    	decimal decreasedAmount;
    	decimal decreasePercent;
    	date startDate;
    	date endDate;
    	set<id> setIS= new set<id>();
		
		list<Variability_Schedule__c> listInsVarSch = new list<Variability_Schedule__c>();
    	for(Insurance_Stipulation__c Stipul: listStipulation ){
			startDate = Stipul.Start_Date__c;
			system.debug('Periodicity='+Stipul.Variability_Periodicity_in_months__c);
			setIS.add(Stipul.Id);
			//StipulatedRequirement = Stipul.Stipulated_Requirement__c;
			
			decreasedAmount = 0; //Stipul.Variability_Percent__c/100*Stipul.Stipulated_Requirement__c;
			decreasePercent = 0;
			integer Periodicity;
			
			if('1'.equals(Stipul.Variability_Periodicity_in_months__c)){
				Periodicity=1; //30days
				if(Stipul.Start_Date__c.daysBetween(Stipul.End_Date__c)+1 <= 30){
					listInsVarSch=setFieldOnVarSch(Stipul,30);
				}
				else{
					endDate = startDate.addMonths(Periodicity).adddays(-1);
					listInsVarSch=genrateVarSchedule(startDate, endDate, Stipul);
				}
			}else if('3'.equals(Stipul.Variability_Periodicity_in_months__c)){
				Periodicity=3; //90days
				if(Stipul.Start_Date__c.daysBetween(Stipul.End_Date__c)+1 <= 90){
					listInsVarSch=setFieldOnVarSch(Stipul,90);
				}
				else{
					endDate = startDate.addMonths(Periodicity).adddays(-1);
					listInsVarSch=genrateVarSchedule(startDate, endDate, Stipul);
				}
			}else if('6'.equals(Stipul.Variability_Periodicity_in_months__c)){
				Periodicity=6; //180days
				if(Stipul.Start_Date__c.daysBetween(Stipul.End_Date__c)+1 <= 180){
					listInsVarSch=setFieldOnVarSch(Stipul,180);
				}
				else{
					endDate = startDate.addMonths(6).adddays(-1);
					listInsVarSch=genrateVarSchedule(startDate, endDate, Stipul);
				}
			}else{ //Default Yearly=12
				 Periodicity=12; //365 days
				//Case: when no. of days between Stipulation start date and end date less than total days of the periodicity.
				if(Stipul.Start_Date__c.daysBetween(Stipul.End_Date__c)+1 <= 365){
					listInsVarSch=setFieldOnVarSch(Stipul,365);
				}
				else{
					endDate = startDate.addMonths(12).adddays(-1);
					listInsVarSch=genrateVarSchedule(startDate, endDate, Stipul);
				}
			}
		} //Loop Ending for Stipulation
		system.debug('size of listInsVarSch='+listInsVarSch.size());
		listRSDelete= [select id from Variability_Schedule__c where Insurance_Stipulations__c = :setIS];
		delete listRSDelete;    	
		
		if(listInsVarSch.size()>0){
			system.debug('insert Variability Schedule');
			insert listInsVarSch;
		}
		listInsVarSch.clear(); //clear list memory
		//Hint: marked as check this checkbox(Dis_Var_Sch_Regen__c) on Stipulation after generating Variability Schedule
		list<Insurance_Stipulation__c> listUpdInsStip= new list<Insurance_Stipulation__c>();
        for(Insurance_Stipulation__c curStip:[select id,name,Dis_Var_Sch_Regen__c from Insurance_Stipulation__c where id in : listStipulation]){
            if(!curStip.Dis_Var_Sch_Regen__c && trigger.isUpdate){
				curStip.Dis_Var_Sch_Regen__c=true;
				listUpdInsStip.add(curStip);
			}
		}
		system.debug('size of listUpdInsStip='+listUpdInsStip.size());
		listStipulation.clear();//clear list memory
		//update Stipulations records (Enable checkbox on only, no futher process, hence using TriggerDisabledFlag
		LeaseWareUtils.TriggerDisabledFlag=true;
		if(listUpdInsStip.size()>0) update listUpdInsStip;
		LeaseWareUtils.TriggerDisabledFlag=false;
		listUpdInsStip.clear(); //clear list memory		
		system.debug('size of setStupID='+setStupID.size());
        set<id> setAssetID =new set<id>();
		for(Insurance_Stipulation__c curstipul :[select id,name, Lease__r.Aircraft__c from Insurance_Stipulation__c where id in : setStupID]){
            setAssetID.add(curstipul.Lease__r.Aircraft__c);
		}
		for(lease__c curLease: [select id,name, Aircraft__c from lease__c where id in :setLeaseID]){
			setAssetID.add(curLease.Aircraft__c);
		}
		setStupID.clear(); //clear set memory
		system.debug('size of setAssetID='+setAssetID);
        // Calling Batch job to update Current and Next Certificate id on Stipulation and Variability Schedule
        if(!setAssetID.isEmpty() && !LeaseWareUtils.isfromFuture() && !LeaseWareUtils.isfromBatch() && !Test.isRunningTest()){			
			system.debug('calling CertificateUpdateBatch');
            Database.executeBatch(new CertificateUpdateBatch(setAssetID));
		}
		
		system.debug('createvariabilitySchedule --');
    	
	}    
	//Hint: set some default value when no. of days between Stipulation start date and end date less then total days of the periodicity. 	
	private list<Variability_Schedule__c> setFieldOnVarSch(Insurance_Stipulation__c curStip, integer noOfdaysinPeriod){
		//If the Start date and End date is less than a Month.
		List<Variability_Schedule__c> listVarScheduleCreated=new List<Variability_Schedule__c>();
		Variability_Schedule__c vsNew;
		if(curStip.First_Renewal_Date__c==Null){
			system.debug('When First Renewal Date is null');
			vsNew = createVarSch(curStip.Id, curStip.Start_Date__c, curStip.End_Date__c, curStip.Stipulated_Requirement__c, curStip.Min_Insurance_Req__c);
			if(vsNew!=null) listVarScheduleCreated.add(vsNew);
		}
		if(curStip.Start_Date__c.daysBetween(curStip.End_Date__c)+1 <= noOfdaysinPeriod && curStip.End_Date__c <= curStip.First_Renewal_Date__c){
			system.debug('When First Renewal Date is greater than or equal to Stipulation End date');
			vsNew = createVarSch(curStip.Id, curStip.Start_Date__c, curStip.End_Date__c, curStip.Stipulated_Requirement__c, curStip.Min_Insurance_Req__c);
			if(vsNew!=null) listVarScheduleCreated.add(vsNew);
		}
		else if(curStip.End_Date__c > curStip.First_Renewal_Date__c){
			system.debug('When First Renewal Date is less than Stipulation End date');
			vsNew = createVarSch(curStip.Id, curStip.Start_Date__c, curStip.First_Renewal_Date__c, curStip.Stipulated_Requirement__c, curStip.Min_Insurance_Req__c);
			if(vsNew!=null) listVarScheduleCreated.add(vsNew);
			vsNew=null;
			vsNew = createVarSch(curStip.Id, curStip.First_Renewal_Date__c.adddays(1), curStip.End_Date__c, curStip.Stipulated_Requirement__c, curStip.Min_Insurance_Req__c);
			if(vsNew!=null) listVarScheduleCreated.add(vsNew);
		}
		return listVarScheduleCreated; 
	}
	private Variability_Schedule__c createVarSch(Id StipId, Date StipStartDt , Date StipEndDt,  decimal StipulatedReq , decimal MinInsuranceReq){
		Variability_Schedule__c  vsNew = new Variability_Schedule__c();
		vsNew.Name='Variability Schedule '+ StipStartDt.Year()+ ' '+LeaseWareUtils.mapMonthsName.get(StipStartDt.month()) +' - '+ StipStartDt.Year()+ ' '+LeaseWareUtils.mapMonthsName.get(StipStartDt.month());
		vsNew.Insurance_Stipulations__c = StipId;
		vsNew.Stipulated_Requirement__c = StipulatedReq ;  //Stipulated Requirement
		vsNew.Decrease_Percent__c = 0;  
		vsNew.Decrease_Amount__c = 0;	//Constant
		vsNew.Insurance_Start_Date__c = StipStartDt;
		vsNew.Insurance_End_Date__c = StipEndDt ;
		vsNew.Min_Insurance_Req__c= MinInsuranceReq;
		return vsNew;
	}

	//Hint:Genrate variability Schedule based on selection types and values in Stipulation
	private list<Variability_Schedule__c> genrateVarSchedule(date StartDate, Date EndDate, Insurance_Stipulation__c curStip){
		system.debug('==stipulation Start Date=='+ StartDate+'=stipulation endDate='+ curStip.End_Date__c +'=First_Variable_Period='+curStip.First_Variable_Period__c+'=First_Renewal_Date=' + curStip.First_Renewal_Date__c+'=Variability=' + curStip.Variability__c);
		decimal StipulatedRequirement = curStip.Stipulated_Requirement__c;
		List<Variability_Schedule__c> listVarScheduleCreated=new List<Variability_Schedule__c>();
		Variability_Schedule__c  rsNew;
		decimal decreasedAmount=0;
		decimal decreasePercent=0;
		integer nMonths= Integer.valueOf(curStip.Variability_Periodicity_in_months__c);
		integer cntVarPrd=0, numberDays;
		Boolean flagFirstVarPrd=False, flagSetFRD=False;
		if(curStip.Variability__c != 'Fixed' && curStip.First_Variable_Period__c!=null){
			flagFirstVarPrd=true;
		}
		if(curStip.First_Renewal_Date__c!=Null &&  curStip.First_Renewal_Date__c < EndDate) EndDate=curStip.First_Renewal_Date__c;
		do{
			//Create New Variability Schedule
			rsNew = new Variability_Schedule__c();
			if(flagFirstVarPrd) cntVarPrd+=1;
			
			rsNew.Name = 'Variability Schedule '+ startDate.Year()+ ' '+LeaseWareUtils.mapMonthsName.get(startDate.month()) +' - '+ EndDate.Year()+ ' '+LeaseWareUtils.mapMonthsName.get(EndDate.month());  //Name of Variability Schedule
			rsNew.Insurance_Stipulations__c = curStip.Id;  //Master Details Relationship
			rsNew.Min_Insurance_Req__c= curStip.Min_Insurance_Req__c;
						
			if(flagFirstVarPrd && cntVarPrd==curStip.First_Variable_Period__c && cntVarPrd==1){
				if(curStip.Variability__c == 'Variable Percent' && curStip.Variability_Percent__c!=null){
					system.debug('Case Variable Percent and First Variable Period=1');
					decreasePercent = curStip.Variability_Percent__c; //Constant
					decreasedAmount = curStip.Variability_Percent__c/100*StipulatedRequirement; 
					StipulatedRequirement = StipulatedRequirement - (curStip.Variability_Percent__c/100*StipulatedRequirement);
				}
				else if (curStip.Variability__c == 'Variable Amount' && curStip.Variability_Amount__c != null){
					system.debug('Case Variable Amount and First Variable Period=1');
					decreasedAmount = curStip.Variability_Amount__c;  //Constant
					StipulatedRequirement = StipulatedRequirement - decreasedAmount;
					decreasePercent =  decreasedAmount/StipulatedRequirement*100;   		    	
				}
			}
			
			if(curStip.Variability__c == 'Fixed' || (flagFirstVarPrd && cntVarPrd<curStip.First_Variable_Period__c-1)){
				startDate = startDate;
				rsNew.Stipulated_Requirement__c = StipulatedRequirement ;  //Stipulated Requirement
				rsNew.Decrease_Percent__c = decreasePercent;  //Constant
				rsNew.Decrease_Amount__c = decreasedAmount;	//Constant
				rsNew.Insurance_Start_Date__c = startDate;
				rsNew.Insurance_End_Date__c = endDate ;
			
			}
			//for Variability Percent -- Percent = Const 
			else if(curStip.Variability__c == 'Variable Percent' && (flagFirstVarPrd && cntVarPrd>=curStip.First_Variable_Period__c-1)){
				startDate = startDate;
				rsNew.Stipulated_Requirement__c = StipulatedRequirement ;  //Stipulated Requirement
				rsNew.Decrease_Percent__c = decreasePercent;  //Constant
				rsNew.Decrease_Amount__c = decreasedAmount;
				rsNew.Insurance_Start_Date__c = startDate;
				rsNew.Insurance_End_Date__c = endDate ;
			
			}
			else if (curStip.Variability__c == 'Variable Amount' && (flagFirstVarPrd && cntVarPrd>=curStip.First_Variable_Period__c-1)){
				startDate = startDate;
				rsNew.Stipulated_Requirement__c = StipulatedRequirement;  //Stipulated Requirement
				rsNew.Decrease_Percent__c = decreasePercent ;
				rsNew.Decrease_Amount__c = decreasedAmount;  //Constant
				rsNew.Insurance_Start_Date__c = startDate;
				rsNew.Insurance_End_Date__c = endDate;
			}
			
			listVarScheduleCreated.add(rsNew);
			
			if(curStip.Variability__c == 'Variable Percent' && curStip.Variability_Percent__c!=null && (flagFirstVarPrd && cntVarPrd>=curStip.First_Variable_Period__c-1)){
				//Add Validation rules to enter the Variability_Percent__c if Variability__c is fixed
				decreasePercent = curStip.Variability_Percent__c; //Constant
				decreasedAmount = curStip.Variability_Percent__c/100*StipulatedRequirement; 
				
				
				StipulatedRequirement = StipulatedRequirement - (curStip.Variability_Percent__c/100*StipulatedRequirement);
				//	decreasedAmount = curStip.Variability_Percent__c/100*StipulatedRequirement;
			}
			else if (curStip.Variability__c == 'Variable Amount' && curStip.Variability_Amount__c != null && (flagFirstVarPrd && cntVarPrd>=curStip.First_Variable_Period__c-1)){
				//Add Validation rules to enter the Variability_Amount__c if Variability__c is Variable
				decreasedAmount = curStip.Variability_Amount__c;  //Constant
				// decreasePercent =  decreasedAmount/StipulatedRequirement*100;
				
				StipulatedRequirement = StipulatedRequirement - decreasedAmount;
				//	decreasedAmount = curStip.Variability_Percent__c/100*StipulatedRequirement; 
				decreasePercent =  decreasedAmount/StipulatedRequirement*100;   		    	
			}	
			if(curStip.First_Renewal_Date__c==Null || curStip.First_Renewal_Date__c>endDate){
				startDate = startDate.addmonths(nMonths);
			}
			else if(curStip.First_Renewal_Date__c<=endDate){
				startDate = endDate.adddays(1);
			}
			
			if(endDate.addmonths(nMonths) > curStip.End_Date__c && endDate != curStip.End_Date__c){
				endDate =curStip.End_Date__c;
			}
			else if(curStip.First_Renewal_Date__c!=Null && curStip.First_Renewal_Date__c >= endDate && !flagSetFRD){
				endDate= endDate.addmonths(nMonths);
				if(startDate<=curStip.First_Renewal_Date__c && curStip.First_Renewal_Date__c<= endDate){
					endDate=curStip.First_Renewal_Date__c;
					flagSetFRD=true;
					system.debug('flagSetFRD='+flagSetFRD);
				}				
			}
			else if(curStip.First_Renewal_Date__c!=Null && curStip.First_Renewal_Date__c >= endDate){
				endDate= endDate.addmonths(nMonths);
				numberDays =  Date.daysInMonth(endDate.year(), endDate.month());
				if(curStip.First_Renewal_Date__c.day() > numberDays){
					endDate= date.newinstance(endDate.year(),endDate.month(), numberDays);
				}
				else{
					endDate= date.newinstance(endDate.year(),endDate.month(), curStip.First_Renewal_Date__c.day());
				}
			} 
			else {
				endDate= endDate.addmonths(nMonths);
			}
		} while(endDate <= curStip.End_Date__c);//End of Inner while loop
		return listVarScheduleCreated; 
	}
}