/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class TestNewTransactionWizController {

    static testMethod void myUnitTest() {
        
        NewTransactionWizController NTWCtrlr = new NewTransactionWizController();
        
        Transaction__c thisTnx = NTWCtrlr.getTnx();
        
        thisTnx.Transaction_Type__c = 'Acquisition';
        thisTnx.Probability__c = 'High';
        thisTnx.Estimated_Closing__c = Date.parse('03/31/2016');
        
        Aircraft_In_Transaction__c thisAiT = NTWCtrlr.getAircraftInTnx();

        thisAiT.Name = '345';
        thisAiT.Lease_End_Date__c = Date.parse('10/31/2016');
        thisAiT.Data_as_of_Eng_1__c = Date.parse('02/27/2016');
        thisAiT.Engine1_TSN__c = 1000;
        thisAiT.Engine_1_Lowest_Limiter_Cycles__c = 2000;
        thisAiT.Engine_1_FH__c = 100;
        thisAiT.Engine_1_FC__c = 100;
        thisAiT.Life_Limit__c = 23000;
        thisAiT.Life_Limit_On_Lowest_Limiter__c = 25000;
        
        NTWCtrlr.step1();
        NTWCtrlr.step2();
        NTWCtrlr.step3();
        NTWCtrlr.step4();
        
        NTWCtrlr.cancel();
        
          try{
            NTWCtrlr.save();
        }catch(exception e){
            system.debug('Cannot save - ' + e.getMessage());
        }
    }
 }