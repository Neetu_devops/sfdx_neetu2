@isTest
public class TestDimensionApplicationTrigger {
    @isTest
    static void  testDimensionAppln() {
        Dimension__c dimension = new Dimension__c(name='Operator Type');
        insert dimension;
        
        Dimension_value__c dimVal = new Dimension_Value__c(name='Lessor',dimension__c = dimension.id);
        insert dimVal;
        
        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;
        
        Lease__c lease = new Lease__c(Name='TestLease',Lessee__c=operator.Id,Lease_End_Date_New__c=Date.today().addYears(1),Lease_Start_Date_New__c=Date.today());
        insert lease;
        
        Test.startTest();
        
		LeaseWareUtils.clearFromTrigger();
        list<dimension_value__c> dimValue = [select id,name,dimension__c from dimension_value__c ];
        System.assertEquals(1, dimValue.size());
        
        Dimension_application__c da1 = new Dimension_application__c(name='Operator Type',Dimension_Value__c=dimValue.get(0).id,lease__c =lease.id );
        insert da1;
        
		LeaseWareUtils.clearFromTrigger();
       
        Dimension_application__c da2 = new Dimension_application__c(name='Operator Type',Dimension_Value__c=dimValue.get(0).id,lease__c =lease.id );
        
        
		LeaseWareUtils.clearFromTrigger();
        try{
			insert da2;
		}catch(Exception e) {
			System.debug('ERROR:' + e.getMessage());
            Boolean expectedExceptionThrown =  e.getMessage().contains('Dimension Application with Operator Type  already exists') ? true : false ;
            System.AssertEquals(expectedExceptionThrown, true);     
		}
        Test.stopTest();
        
    }
}