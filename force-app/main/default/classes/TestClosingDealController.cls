/**
*******************************************************************************
* Class: TestClosingDealController
* @author Created by Bhavna, Lease-Works, 
* @date  9/18/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains methods to test ClosingDealController
*
* History:
* - VERSION   DEVELOPER NAME    	DATE         	DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestClosingDealController {
    /**
    * @description Created Test Data to test all the Methods of ClosingDealController.
    *  	 
    * @return null
    */
    @testSetup 
    static void setup() {
        Project__c project = new Project__c(Name = 'Test',
                                            Economic_Closing_Date__c = System.today(),
                                            Project_Deal_Type__c = 'Sale');
        insert project;
        
        Ratio_Table__c ratioTable = new Ratio_Table__c(Name= 'Test Ratio');
        insert ratioTable;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
        //  Escalation__c = 43,
        //  Escalation_Month__c = 'January',
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Contractual_Status__c= 'Sold',
            Ownership_Status__c = 'Managed',
            Aircraft_Type__c = 'Engine Type',
            Aircraft_Variant__c = '100',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230',
            MTOW_Leased__c=10,
            AOG_Date__c = Date.today(),
            Asset_Owner__c = 'abctest',
            TSLV__c = 1
        );
        insert aircraft;
        
        aircraft.Lease__c = lease.Id;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;
        
         Aircraft__c aircraft2 = new Aircraft__c(
            Name= '1123 Test',
            Contractual_Status__c= 'Sold',
            Ownership_Status__c = 'Managed',
            Aircraft_Type__c = 'Engine Type',
            Aircraft_Variant__c = '100',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1240',
            MTOW_Leased__c=10,
            AOG_Date__c = Date.today(),
            Asset_Owner__c = 'abctest',
            TSLV__c = 1
        );
        insert aircraft2;
        Counterparty__c counterParty = new Counterparty__c(
            Name = 'Leassor Test');
        insert counterParty;
        
        Operator__c operator = new Operator__c (
            Name = 'Airline Test');
        insert operator;
        
        Marketing_Activity__c marketingActivity = new Marketing_Activity__c(
            Name = 'Test MA',
            Prospect__c = operator.Id,
            Lessor__c = counterParty.Id,
            Deal_Type__c = 'Sale',
            Inactive_MA__c = False,
            Deal_Status__c = 'Pipeline',
            campaign__c = project.Id);
        
        insert marketingActivity;
        
        Aircraft_Proposal__c assetDeal = new Aircraft_Proposal__c(Name= 'Test Asset',
                                                                  Project_Status__c = 'Bid',
                                                                  sale_price__c = 20,
                                                                  Marketing_Activity__c  = marketingActivity.ID,
                                                                  aircraft__c = aircraft.Id);
        insert assetDeal;
        
        assetDeal = new Aircraft_Proposal__c(Name= 'Test Asset',
                                             Project_Status__c = 'Data Room',
                                             sale_price__c = 20,
                                             Marketing_Activity__c  = marketingActivity.ID,
                                             aircraft__c = aircraft2.Id);
        insert assetDeal;
    }
    
    
    /**
    * @description Testing all the methods of ClosingDealController
    *  	 
    * @return null
    */
    @isTest static void testUtilizationStudioTable() {
        Marketing_Activity__c marketingActivity = [Select Id from Marketing_Activity__c limit 1];
        List<Aircraft_Proposal__c> assetList = [Select Id, Name, Competitor_Rent__c, Competitor_Price__c from Aircraft_Proposal__c];
        
        String editableFields = leasewareutils.getNamespacePrefix() + 'Competitor_Rent__c,' + leasewareutils.getNamespacePrefix() + 'Competitor_Price__c';
        
        for(Aircraft_Proposal__c asset : assetList) {
            asset.Competitor_Rent__c = 345;
            asset.Competitor_Price__c = 200;
        }
        
        ClosingDealController.getAssetInDeals(leasewareutils.getNamespacePrefix() + 'ClosingDeal', editableFields, marketingActivity.Id);
    	ClosingDealController.saveAssetInDeals(assetList, false, true, 'Declined to pursue', 'Test', marketingActivity.Id);
        ClosingDealController.updateDeal(marketingActivity.Id);
    }
}