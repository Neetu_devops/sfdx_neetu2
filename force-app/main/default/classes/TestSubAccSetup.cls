@isTest
public class TestSubAccSetup {
    
    public static testMethod void testSubAcc(){
        
        LeaseWareUtils.clearFromTrigger();
       	test.startTest();
        parent_Accounting_setup__c accSetup1 = new parent_Accounting_Setup__c(name='test',default__c=true);
        insert accSetup1;
       	
        list<Accounting_setup__c> acclist =  [select id,name from accounting_setup__c where  parent_Accounting_Setup__c = :accSetup1.id];
        system.assertEquals(9, acclist.size(),'The List size doesnot match');
        
        list<gl_account_code__c> glList =[select id from gl_account_code__c where accounting_setup__r.name = :acclist.get(0).name]; 
         system.assert(glList.size()>0);
       
        
        
        LeaseWareUtils.clearFromTrigger();
        test.stopTest();
    }

}