public with sharing class LLPReplacementControllor {
    @AuraEnabled
    public Static List<llpwrapper> getrecords (Id recordId , String engineType){
        List<llpwrapper> llpWrapList = new List<llpwrapper>();
        List<Constituent_Assembly__c> assemblyList = [SELECT Id, Attached_Aircraft__c, Type__c
                                                      FROM Constituent_Assembly__c 
                                                      WHERE Attached_Aircraft__c =: recordId AND Type__c =: engineType];
        
        for(Sub_Components_LLPs__c LLPData : [SELECT Id, Name, Constituent_Assembly__c, Cycles_Used__c, Life_Limit__c, Cycles_Remaining_TS__c, Part_Number__c FROM Sub_Components_LLPs__c WHERE Constituent_Assembly__c IN: assemblyList ORDER BY Name]){
            llpWrapList.add(new llpwrapper(LLPData));
        }
        return llpWrapList;
    }
    public class llpwrapper{
        @AuraEnabled public boolean isReplaced {get;set;}
        @AuraEnabled public Sub_Components_LLPs__c llpRecord {get;set;}
        public llpwrapper(){
            this.isReplaced = false;
            this.llpRecord = new Sub_Components_LLPs__c();
        }
        public llpwrapper(Sub_Components_LLPs__c llpRecord){
            this.isReplaced = false;
            this.llpRecord = llpRecord;
        }
    }
}