/**
*******************************************************************************
* Class: TestMRClaimController
* @author Created by Bhavna, Lease-Works, 
* @date  8/17/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Test all the Methods of MRClaimController.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestMRClaimController {
    /**
    * @description Created Test Data to test all the Methods of claim panel.
    * @return null
    */
    @testSetup
    static void setup(){
        Date dateField = System.today(); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = Date.valueOf('2016-07-01'),
            Lease_End_Date_New__c = lastDayOfMonth,
            Threshold_Claim_Amount__c = 5000
        );
        insert leaseRecord;
        
        // Set up the Aircraft record
        Aircraft__c asset = new Aircraft__c(Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available', Derate__c=20, MSN_Number__c = '28216');
        LeaseWareUtils.clearFromTrigger();
        insert asset;
        
        // update lease reocrd with aircraft
        leaseRecord.Aircraft__c=asset.id;
        LeaseWareUtils.clearFromTrigger();
        update leaseRecord;
        
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createTSLookup(mapInOut);
        
        List<Constituent_Assembly__c> assemblyList = new List<Constituent_Assembly__c>();
        assemblyList.add(new Constituent_Assembly__c(Name='Airframe', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
                                                     Type__c ='Airframe', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId(), 
                                                     Serial_Number__c='Airframe'));
        assemblyList.add(new Constituent_Assembly__c(Name='Engine 1', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
                                                     Type__c ='Engine 1', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId(), 
                                                     Serial_Number__c='Engine 1'));
        assemblyList.add(new Constituent_Assembly__c(Name='APU', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0, 
                                                     Type__c ='APU', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('APU').getRecordTypeId(), 
                                                     Serial_Number__c='APU'));
        assemblyList.add(new Constituent_Assembly__c(Name='LGN', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0, 
                                                     Type__c ='Landing Gear - Nose', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Landing_Gear').getRecordTypeId(),
                                                     Serial_Number__c='LGN'));
        assemblyList.add(new Constituent_Assembly__c(Name='LGRM', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0, 
                                                     Type__c ='Landing Gear - Right Main', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Landing_Gear').getRecordTypeId(), 
                                                     Serial_Number__c='LGRM'));       
        LeaseWareUtils.clearFromTrigger();
        insert assemblyList;
        
        // Set up Maintenance Program
        Maintenance_Program__c mp = new Maintenance_Program__c(name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        List<Maintenance_Program_Event__c> mpeList = new List<Maintenance_Program_Event__c>();
        mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Airframe', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = '4C/6Y'));
        mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Engine', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = 'Performance Restoration'));
        mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'APU', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = 'Performance Restoration'));
        mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Landing Gear - Nose', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = 'Overhaul'));
        mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Landing Gear - Main', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = 'Overhaul'));
        insert mpeList;
        
        // Set up Project Event for Airframe
        List<Assembly_Event_Info__c> projectedEventList = new List<Assembly_Event_Info__c>();
        projectedEventList.add(new Assembly_Event_Info__c(Name = 'Airframe - 4C/6Y', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[0].Id, Maintenance_Program_Event__c = mpeList[0].Id));
        projectedEventList.add(new Assembly_Event_Info__c(Name = 'Engine 1 - PR', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[1].Id, Maintenance_Program_Event__c = mpeList[1].Id));
        projectedEventList.add(new Assembly_Event_Info__c(Name = 'APU - PR', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[2].Id, Maintenance_Program_Event__c = mpeList[2].Id));
        projectedEventList.add(new Assembly_Event_Info__c(Name = 'LGN - Overhaul', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[3].Id, Maintenance_Program_Event__c = mpeList[3].Id));
        projectedEventList.add(new Assembly_Event_Info__c(Name = 'LGN - Main', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[4].Id, Maintenance_Program_Event__c = mpeList[4].Id));
        insert projectedEventList;
        
        List<Assembly_MR_Rate__c> supplementalRentList = new List<Assembly_MR_Rate__c>();
        supplementalRentList.add(new Assembly_MR_Rate__c(Name='Airframe - 4C/6Y', Assembly_Lkp__c = assemblyList[0].id, Assembly_Event_Info__c = projectedEventList[0].id, 
                                                         Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Induction Date', Rate_Basis__c='Cycles', 
                                                         Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
        supplementalRentList.add(new Assembly_MR_Rate__c(Name='Engine 1 - PR', Assembly_Lkp__c = assemblyList[1].id, Assembly_Event_Info__c = projectedEventList[1].id , 
                                                         Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Removal Date', Rate_Basis__c='Cycles',
                                                         Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
        supplementalRentList.add(new Assembly_MR_Rate__c(Name='APU - PR', Assembly_Lkp__c = assemblyList[2].id, Assembly_Event_Info__c = projectedEventList[2].id , 
                                                         Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Installation Date', Rate_Basis__c='Cycles', 
                                                         Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
        supplementalRentList.add(new Assembly_MR_Rate__c(Name='LGN - Overhaul', Assembly_Lkp__c = assemblyList[3].id, Assembly_Event_Info__c = projectedEventList[3].id , 
                                                         Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Release Date', Rate_Basis__c='Cycles', 
                                                         Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
        insert supplementalRentList;
        
        List<MR_Share__c> mrShareList = new List<MR_Share__c>();
        mrShareList.add(new MR_Share__c(Name='Engine 1 - Airframe 4C/6Y', Assembly_MR_Info__c= supplementalRentList[1].Id, Assembly__c='Airframe', Maintenance_Event_Name__c = '4C/6Y'));
        mrShareList.add(new MR_Share__c(Name='APU - Airframe 4C/6Y', Assembly_MR_Info__c= supplementalRentList[1].Id, Assembly__c='Airframe', Maintenance_Event_Name__c = '4C/6Y'));
        mrShareList.add(new MR_Share__c(Name='LGN - LGRM Overhaul', Assembly_MR_Info__c= supplementalRentList[3].Id, Assembly__c='Landing Gear - Right Main', Maintenance_Event_Name__c = 'Overhaul'));
        insert mrShareList;
        
        List<Assembly_Eligible_Event__c> historicalEventList = new List<Assembly_Eligible_Event__c>();
        historicalEventList.add(new Assembly_Eligible_Event__c(Name ='Airframe 4C/6Y', Constituent_Assembly__c = assemblyList[0].Id, Projected_Event__c = projectedEventList[0].id,
                                                               Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
                                                               Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = '4C/6Y'));
        historicalEventList.add(new Assembly_Eligible_Event__c(Name ='Engine 1 PR', Constituent_Assembly__c = assemblyList[1].Id, Projected_Event__c = projectedEventList[1].id,
                                                               Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
                                                               Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = 'Performance Restoration'));
        historicalEventList.add(new Assembly_Eligible_Event__c(Name ='APU PR', Constituent_Assembly__c = assemblyList[2].Id, Projected_Event__c = projectedEventList[2].id,
                                                               Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
                                                               Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = 'Performance Restoration'));
        historicalEventList.add(new Assembly_Eligible_Event__c(Name ='LGN Overhaul', Constituent_Assembly__c = assemblyList[3].Id, Projected_Event__c = projectedEventList[3].id,
                                                               Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
                                                               Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = 'Overhaul'));
        historicalEventList.add(new Assembly_Eligible_Event__c(Name ='LGMR Overhaul', Constituent_Assembly__c = assemblyList[4].Id, Projected_Event__c = projectedEventList[4].id,
                                                               Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
                                                               Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = 'Overhaul'));
        LWGlobalUtils.setTriggersflagOn(); 
        insert historicalEventList;
        LWGlobalUtils.setTriggersflagOff();
        
        List<Supplemental_Rent_Transactions__c> mrHistoryList = new List<Supplemental_Rent_Transactions__c>();
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'Airframe SB', 
        Transaction_Type__c = 'Opening Balance', Supplemental_Rent__c = supplementalRentList[0].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'Airframe MRP', 
        Transaction_Type__c = 'MR Cash Payment', Supplemental_Rent__c = supplementalRentList[0].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'Airframe AE', 
        Transaction_Type__c = 'MR Claim', Supplemental_Rent__c = supplementalRentList[0].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'Engine SB', 
        Transaction_Type__c = 'Opening Balance', Supplemental_Rent__c = supplementalRentList[1].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'Engine MRP', 
        Transaction_Type__c = 'MR Cash Payment', Supplemental_Rent__c = supplementalRentList[1].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'Engine AE', 
        Transaction_Type__c = 'MR Claim', Supplemental_Rent__c = supplementalRentList[1].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'APU SB',
        Transaction_Type__c = 'Opening Balance',Supplemental_Rent__c = supplementalRentList[2].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'APU MRP',
        Transaction_Type__c = 'MR Cash Payment', Supplemental_Rent__c = supplementalRentList[2].Id));
        mrHistoryList.add(new Supplemental_Rent_Transactions__c(Name = 'APU AE', 
        Transaction_Type__c = 'MR Claim', Supplemental_Rent__c = supplementalRentList[2].Id));
        
        insert mrHistoryList;
        
        List<Claims__c> claimList = new List<Claims__c>();
        claimList.add(new Claims__c(Name ='Airframe 4C/6Y', Supplemental_Rent__c = supplementalRentList[0].Id, Approved_Claim_Amount__c = 20000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
        claimList.add(new Claims__c(Name ='Engine 1 PR', Supplemental_Rent__c = supplementalRentList[1].Id, Approved_Claim_Amount__c = 20000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
        claimList.add(new Claims__c(Name ='APU PR', Supplemental_Rent__c = supplementalRentList[2].Id, Approved_Claim_Amount__c = 20000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
        claimList.add(new Claims__c(Name ='LGN Overhaul', Supplemental_Rent__c = supplementalRentList[3].Id, Approved_Claim_Amount__c = 20000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
        insert claimList;
    }
    
    @isTest static void claimForSameBucket() {
        Constituent_Assembly__c assembly = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'APU' LIMIT 1];
        
        Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: assembly.Id LIMIT 1];
        
        Assembly_MR_Rate__c mrInfo = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c, Lessor_Contribution_Available__c, 
                                      Claim_Comments__c, Available_Reference_Date__c, MR_Claim_Method__c, Assembly_Event_Info__r.Event_Cost_F__c, Assembly_Lkp__r.Type__c,
                                      Assembly_Event_Info__r.Event_Type__c
                                      FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: assembly.Id LIMIT 1];
        
        Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];        
        
        Test.startTest();
        MRClaimController.getAssemblyMRInfo(claim.Id);
        MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
        MRClaimController.getAssemblyMRInfos(claim.Id, '');
        MRClaimController.findMRClaimAvailable(mrInfo, claim.Id);
        
        claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,
                Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c,Supplemental_Rent__r.Lease__r.Lessor__c,
                 Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c, 
                 Approved_Claim_Amount__c,  Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c
                 FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];
        
        MRClaimController.calculateTotalInvoiceAmount(claim.Id);
        MRClaimController.createEventInvoice(claim, mrInfo, 5000.00, '2019-08-08', 'Removal', 2000.45, 5000.00);
        MRClaimController.findObjectAPIName(mrInfo.Id);
        MRClaimController.getAssemblyMRInfo(claim.Id);
        MRClaimController.createEventInvoice(claim, mrInfo, 5000.00, '2019-08-08', 'Removal', 2000.45, 5000.00);
        Test.stopTest();
    }
    
    @isTest static void claimForSharedBucket() {
        Constituent_Assembly__c airframe_CA = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Airframe' LIMIT 1];
        Constituent_Assembly__c engine_CA = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Engine 1' LIMIT 1];
        
        Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: airframe_CA.Id LIMIT 1];
        
        Assembly_MR_Rate__c airframe_MR = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c, Lessor_Contribution_Available__c, 
                                      Claim_Comments__c, Available_Reference_Date__c, MR_Claim_Method__c, Assembly_Event_Info__r.Event_Cost_F__c, Assembly_Lkp__r.Type__c,
                                      Assembly_Event_Info__r.Event_Type__c
                                      FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: airframe_CA.Id LIMIT 1];
        
        Assembly_MR_Rate__c engine_MR = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c, Lessor_Contribution_Available__c, 
                                      Claim_Comments__c, Available_Reference_Date__c, MR_Claim_Method__c, Assembly_Event_Info__r.Event_Cost_F__c, Assembly_Lkp__r.Type__c,
                                      Assembly_Event_Info__r.Event_Type__c
                                      FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: engine_CA.Id LIMIT 1];
        
        Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: airframe_MR.Id LIMIT 1];        
        
        Test.startTest();
        MRClaimController.getAssemblyMRInfo(claim.Id);
        MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
        MRClaimController.getAssemblyMRInfos(claim.Id, '');
        MRClaimController.findMRClaimAvailable(engine_MR, claim.Id);
        
        claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,
                Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c,Supplemental_Rent__r.Lease__r.Lessor__c,
                 Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c, 
                 Approved_Claim_Amount__c, Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c
                 FROM Claims__c Where Supplemental_Rent__c =: airframe_MR.Id LIMIT 1];
        
        MRClaimController.calculateTotalInvoiceAmount(claim.Id);
        MRClaimController.createEventInvoice(claim, engine_MR, 5000.00, '2019-08-08', 'Removal', 2000.45, 5000.00);
        MRClaimController.findObjectAPIName(engine_MR.Id);
        MRClaimController.getAssemblyMRInfo(claim.Id);
        MRClaimController.createEventInvoice(claim, engine_MR, 5000.00, '2019-08-08', 'Removal', 2000.45, 5000.00);
        Test.stopTest();
    }
    
    @isTest static void claimForDiffrentHE() {
        Constituent_Assembly__c airframe_CA = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Airframe' LIMIT 1];
        Constituent_Assembly__c engine_CA = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Engine 1' LIMIT 1];
        
        Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: airframe_CA.Id LIMIT 1];
        
        Assembly_MR_Rate__c engine_MR = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c, Lessor_Contribution_Available__c, 
                                      Claim_Comments__c, Available_Reference_Date__c, MR_Claim_Method__c, Assembly_Event_Info__r.Event_Cost_F__c, Assembly_Lkp__r.Type__c,
                                      Assembly_Event_Info__r.Event_Type__c
                                      FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: engine_CA.Id LIMIT 1];
        
        Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: engine_MR.Id LIMIT 1];        
        
        Test.startTest();
        MRClaimController.getAssemblyMRInfo(claim.Id);
        MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
        MRClaimController.getAssemblyMRInfos(claim.Id, '');
        MRClaimController.findMRClaimAvailable(engine_MR, claim.Id);
        
        claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,
                Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c,Supplemental_Rent__r.Lease__r.Lessor__c,
                 Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c, 
                 Approved_Claim_Amount__c,  Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c
                 FROM Claims__c Where Supplemental_Rent__c =: engine_MR.Id LIMIT 1];
        
        MRClaimController.calculateTotalInvoiceAmount(claim.Id);
        MRClaimController.createEventInvoice(claim, engine_MR, 5000.00, '2019-08-08', 'Removal', 2000.45, 5000.00);
        MRClaimController.findObjectAPIName(engine_MR.Id);
        MRClaimController.getAssemblyMRInfo(claim.Id);
        MRClaimController.createEventInvoice(claim, engine_MR, 5000.00, '2019-08-08', 'Removal', 2000.45, 5000.00);
        Test.stopTest();
    }
}