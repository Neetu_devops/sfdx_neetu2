//@lastUpdated: 2016.10.16 1130 PM : Arjun : Unit test for SendURCntrl,SendMultipleInvoiceCntrl
@isTest
private class TestSendURCntrl {
    
    //static PageReference pageRefSendURAudit = Page.SendMonthlyUtilizationAudit;
    //static PageReference pageRefSendMonthlyInvoice = Page.SendMonthlyInvoices;
    //static PageReference pageRefURPDF  = Page.MonthlyUtilizationPDF;
    static Operator__c operator1                        {get;set;}
    static Operator__c operator2                        {get;set;}
    static Counterparty__c lessor1                      {get;set;}
    static Counterparty__c lessor2                      {get;set;}
    static Lessor__c lessor                             {get;set;}
    static lease__c lease1,lease2;
    static Aircraft__c aircraft1,aircraft2;
    static Utilization_Report__c ur1,ur2,ur3,ur4;
    static Rent__c incSchedule1,incSchedule2;
    static Invoice__c newInvoice1,newInvoice2;
    static void dataSetup() 
    {                   
        lessor = new Lessor__c(Name='Emirates',Phone_Number__c='999999999',
                                    Email_Address__c='test@lws.com',Lease_Logo__c='https://www.logo.com',
                                   
                                    Address_Street__c='House No.1240,Sector-29',City__c='Faridabad',
                                    Zip_Code__c='121008',Country__c='India');
        insert lessor;
        LeaseWareSeededData.SeedLWServiceSetup();
        map<String,Object> mapInOut = new map<String,Object>();
        Country__c country = new Country__c(name='India');
        insert country;
        // Create Operator1
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());      
        mapInOut.put('Operator__c.Name','Air India1');
        mapInOut.put('Operator__c.City__c','Faridabad');
        mapInOut.put('Operator__c.Zip_Code__c','121008');
        mapInOut.put('Operator__c.Country_Lookup___c',country.id);
        
        TestLeaseworkUtil.createOperator(mapInOut);
        operator1 = (Operator__c)mapInOut.get('Operator__c');
        
        LeaseWareUtils.unsetTriggers('OperatorTrigger');
        // Create Operator2
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());  
        mapInOut.put('Operator__c.Name','Air India2');
        mapInOut.put('Operator__c.Location__Latitude__s', 23.45555555);
        mapInOut.put('Operator__c.Location__Longitude__s',73.23233232);
        TestLeaseworkUtil.createOperator(mapInOut);
        operator2 = (Operator__c)mapInOut.get('Operator__c');
          
        LeaseWareUtils.unsetTriggers('CounterpartyTrg');
        lessor1 = new Counterparty__c(Address_Street__c='1240/29',City__c='Faridabad',Zip_Code__c='121008',Country_Lookup__c=country.id,
                                     Fleet_Aircraft_Of_Interest__c='A320',Status__c='Approved',Location__Latitude__s = 23.45555555,
                                     Location__Longitude__s = 73.23233232,
                                     Main_Email_Address__c='test1@lws.com',
                                     Subsidiary_Entity__c = true,
                                     Billing_Email_Address__c='test2@lws.com');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());  
        insert lessor1;
        
        LeaseWareUtils.unsetTriggers('CounterpartyTrg');
        lessor2 = new Counterparty__c(Address_Street__c='1240/29',City__c='Faridabad',Zip_Code__c='121008',Country_Lookup__c=country.id,
                                     Fleet_Aircraft_Of_Interest__c='A320',Status__c='Approved',Location__Latitude__s = 23.45555555,
                                     Location__Longitude__s = 73.23233232,
                                     Main_Email_Address__c='test1@lws.com',
                                     Subsidiary_Entity__c = true,
                                     Billing_Email_Address__c='test1@lws.com');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());  
        insert lessor2;
        
        mapInOut.put('Aircraft__c',null);
        TestLeaseworkUtil.createAircraft(mapInOut);
        aircraft1 = (Aircraft__c)mapInOut.get('Aircraft__c');
        
        mapInOut.put('Aircraft__c',null);
        mapInOut.put('Aircraft__c.msn','123');
        TestLeaseworkUtil.createAircraft(mapInOut);
        aircraft2 = (Aircraft__c)mapInOut.get('Aircraft__c');
        
        //lease with lesssee populated
        mapInOut.put('Lease__c.Lessor__c',lessor1.Id); 
        mapInOut.put('Lease__c.Lessee__c',operator2.Id); 
        mapInOut.put('Lease__c.Aircraft__c',aircraft1.Id); 
        mapInOut.put('Lease__c.Operator__c',operator2.Id);
        mapInOut.put('Lease__c.Name','Lease1');   
        mapInOut.put('Lease__c',null);          
        TestLeaseworkUtil.createLease(mapInOut);
        lease1 = (Lease__c)mapInOut.get('Lease__c');
        
        //lease with operator populated
        mapInOut.put('Lease__c.Lessor__c',lessor2.Id); 
        mapInOut.put('Lease__c.Aircraft__c',aircraft2.Id); 
        mapInOut.put('Lease__c.Operator__c',operator1.Id);
        mapInOut.put('Lease__c.Name','Lease2'); 
        mapInOut.put('Lease__c',null);          
        TestLeaseworkUtil.createLease(mapInOut);
        lease2 = (Lease__c)mapInOut.get('Lease__c');   
    }
    
    //Used as dataSetup for Bulk Invoice page
    static void dataSetup_SendInv()
    {
        map<String,Object> mapInOut = new map<String,Object>();
        
        Integer numberOfDays = Date.daysInMonth( system.today().year(), system.today().month() );
        Date monthEnding  = Date.newInstance( system.today().year(), system.today().month(), numberOfDays );
        
        mapInOut.put('Rent__c',null) ;
        mapInOut.put('Rent__c.For_Month_Ending__c',monthEnding);
        mapInOut.put('Aircraft__c.lease__c',lease1.Id);
        TestLeaseworkUtil.createRent(mapInOut);
        
        mapInOut.put('Invoice__c.Aircraft MR',null) ;
        mapInOut.put('Invoice__c.Assembly MR',null) ;
        mapInOut.put('Invoice__c.Rent',null);
        TestLeaseworkUtil.setRecordTypeOfInvoice(mapInOut);
        
        incSchedule1 = (Rent__c)mapInOut.get('Rent__c') ;   //Income Schedule with operator on lease
        
        ur1 = new Utilization_Report__c();
        
        LeaseWareUtils.clearFromTrigger();
        mapInOut.put('Utilization_Report__c',null) ;
        mapInOut.put('Aircraft__c.Id',Aircraft1.Id);
        mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(0));
        mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');
        TestLeaseworkUtil.createUR(mapInOut);      
        ur1 =(Utilization_Report__c) mapInOut.get('Utilization_Report__c');  // Monthly Utilization
        
        newInvoice1 = new Invoice__c();
        newInvoice1.Lease__c        = lease1.Id;
        newInvoice1.Invoice_Date__c = System.Today();
        newInvoice1.Invoice_Type__c = 'Aircraft MR'; 
        newInvoice1.Status__c       = 'Pending';             
        newInvoice1.RecordTypeId    = (Id)mapInOut.get('Invoice__c.Aircraft MR');
        newInvoice1.Month_Ending__c = monthEnding;
        newInvoice1.Utilization_Report__c = ur1.Id;
        
        newInvoice2 = new Invoice__c();
        newInvoice2.Lease__c        = lease1.Id;
        newInvoice2.Invoice_Date__c = System.Today();
        newInvoice2.Invoice_Type__c = 'Rent'; 
        newInvoice2.Status__c       = 'Pending';             
        newInvoice2.RecordTypeId    = (Id)mapInOut.get('Invoice__c.Rent');
        newInvoice2.Rent__c = incSchedule1.Id;
        list<Invoice__c> inv = new list<Invoice__c>();      
        inv.add( newInvoice1 ); inv.add( newInvoice2); insert inv;
        
                
        mapInOut.put('Rent__c',null) ;
        mapInOut.put('Rent__c.For_Month_Ending__c',monthEnding);
        mapInOut.put('Aircraft__c.lease__c',lease2.Id);
        TestLeaseworkUtil.createRent(mapInOut);
        incSchedule2 = (Rent__c)mapInOut.get('Rent__c') ;   //Income Schedule with lessee on lease
    }
    
    //Used as dataSetup for Send MonthlyUtil page
    static void dataSetup_SendUR() 
    {
        ur1 = new Utilization_Report__c();
        ur2 = new Utilization_Report__c();
        ur3 = new Utilization_Report__c();
        ur4 = new Utilization_Report__c();
        map<String,Object> mapInOut = new map<String,Object>();
        
        LeaseWareUtils.clearFromTrigger();
        mapInOut.put('Utilization_Report__c',null) ;
        mapInOut.put('Aircraft__c.Id',Aircraft1.Id);
        mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(-1));
        mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');
        TestLeaseworkUtil.createUR(mapInOut);      
        ur1 =(Utilization_Report__c) mapInOut.get('Utilization_Report__c');
        
        
        LeaseWareUtils.clearFromTrigger();
        mapInOut.put('Aircraft__c.Id',Aircraft2.Id);
        mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(-1));
        TestLeaseworkUtil.createUR(mapInOut);      
        ur2 =(Utilization_Report__c) mapInOut.get('Utilization_Report__c');
        
        
        LeaseWareUtils.clearFromTrigger();
        mapInOut.put('Aircraft__c.Id',Aircraft1.Id);        
        mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(0));
        TestLeaseworkUtil.createUR(mapInOut);      
        ur3 =(Utilization_Report__c) mapInOut.get('Utilization_Report__c');
        
        
        LeaseWareUtils.clearFromTrigger();
        mapInOut.put('Aircraft__c.Id',Aircraft2.Id);
        mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(0));
        TestLeaseworkUtil.createUR(mapInOut);      
        ur4 =(Utilization_Report__c) mapInOut.get('Utilization_Report__c');
    }   
    
    static testMethod void TestSendURCntrlOnload()
    {    /*
        dataSetup();
        
        User user = [ select id from user where profile.name='System Administrator' 
                      AND isactive = true limit 1 ];
		Test.startTest();                      
        system.runAs( user )
        {
            dataSetup_SendUR();
        }
        Test.stopTest();
        system.assert( [ SELECT Id
                        FROM Utilization_Report__c 
                        WHERE Calendar_Month(Month_Ending__c) =:system.today().month()
                        AND Calendar_Year(Month_Ending__c) =:system.today().year()
                        AND Type__c = 'Actual'
                        AND Status__c = 'Approved By Lessor' ].size()>0);
        Test.setCurrentPage(pageRefSendURAudit);  
        SendMonthlyUtilizationAuditCntrl cont;
        ApexPages.StandardController scontroller = new ApexPages.standardController( lessor );
        
            cont = new SendMonthlyUtilizationAuditCntrl(scontroller); 
     */   
    }
    
    static testMethod void TestSendURCntrlSendUr()
    {/*    
        dataSetup();
       
        User user = [ select id from user where profile.name='System Administrator' 
                      AND isactive = true limit 1 ];
		 Test.startTest();                      
        system.runAs( user )
        {
            dataSetup_SendUR();
        }
        Test.stopTest();
        system.assert( [ SELECT Id
                        FROM Utilization_Report__c 
                        WHERE Type__c = 'Actual'
                        AND Status__c = 'Approved By Lessor' ].size()>0);
        Test.setCurrentPage(pageRefSendURAudit);  
        ApexPages.StandardController scontroller = new ApexPages.standardController( lessor );
        SendMonthlyUtilizationAuditCntrl cont = new SendMonthlyUtilizationAuditCntrl(scontroller); 
        ApexPages.currentPage().getParameters().put('selectedLessors'
                                    ,string.valueof(lessor1.Id)+','+string.valueof(lessor2.Id));  
        ApexPages.currentPage().getParameters().put('selectedUtils'
                                    ,string.valueof(ur1.Id)+','+string.valueof(ur2.Id));        

        
            cont.sendURtoLessors();  
        */
    }
    
    static testMethod void TestMonthlyUtilizationPDF()
    {/*    
        dataSetup();
        Test.startTest();
        User user = [ select id from user where profile.name='System Administrator' 
                      AND isactive = true limit 1 ];
        system.runAs( user )
        {
            dataSetup_SendUR();
        }
        Test.setCurrentPage(pageRefURPDF);  
        MonthlyUtilizationPDFCntrl cont; 
        ApexPages.currentPage().getParameters().put('utils'
                                    ,string.valueof(ur1.Id)+','+string.valueof(ur2.Id));        

        
            cont = new MonthlyUtilizationPDFCntrl();  
        Test.stopTest();
		*/
    }
    
    //Testing Bulk Invoice page load
    static testMethod void TestSendURCntrlSendInv_Pageload()
    {/*    
        dataSetup();
        User user = [ select id from user where profile.name='System Administrator' 
                      AND isactive = true limit 1 ];
         Test.startTest();              
        system.runAs( user )
        {
            dataSetup_SendInv();
        }
        
        Test.setCurrentPage(pageRefSendMonthlyInvoice);

        ApexPages.StandardController scontroller = new ApexPages.standardController( lessor );
        
        SendMultipleInvoiceCntrl cont;
        
       
              cont = new SendMultipleInvoiceCntrl(scontroller); 
        Test.stopTest();
        
        SendMultipleInvoiceCntrl.generateNewInvoice( incSchedule1.Id, 'Rent' );
        
        SendMultipleInvoiceCntrl.generateNewInvoice( ur1.Id, 'Utilization' );
        
        SendMultipleInvoiceCntrl.generateNewUtil( aircraft1.Id, string.valueof(system.today().month()),string.valueof(system.today().year()),'23','22' );
    */
    }
}