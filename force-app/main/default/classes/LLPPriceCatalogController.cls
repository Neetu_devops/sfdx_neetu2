/* Created By A.K.G on 08/07/2019 */

public class LLPPriceCatalogController {
    
    @AuraEnabled
    public static void processAction(String recordId){
        System.debug('Approve LLP Catalog processAction+');
        string errorMsg;
        try{
            LLP_Catalog_Summary__c updRec = [select id,Prev_LLP_Cat_Sum__c,Prev_LLP_Cat_Sum__r.status__c,Prev_LLP_Cat_Sum__r.Name from LLP_Catalog_Summary__c where id =:recordId];
            
            if(updRec.Prev_LLP_Cat_Sum__c!=null && updRec.Prev_LLP_Cat_Sum__r.status__c!='Approved'){
            	errorMsg = 'Previous year (' + updRec.Prev_LLP_Cat_Sum__r.Name + ') LLP Price Catalog Summary record is not approved. Please approve prev year record first.';
            	System.assert(false,errorMsg);
            }else{
            	updRec.Status__c = 'Approved';
            	update updRec;
            }	
        }
        catch(Exception ex){
			System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(getErrorMessage(ex));
		}
        System.debug('Approve LLP Catalog processAction-');
    }
    
    public static String getErrorMessage(Exception ex){
        String error = '';
        if(ex instanceof DmlException){
            for(Integer i = 0; i < ex.getNumDml(); i++) {
                error = error == '' ? ex.getDmlMessage(i) : error+' \n '+ex.getDmlMessage(i);
            }
        }
        else{
            error = ex.getMessage()+' : Stack Trace: '+ex.getStackTraceString()+' : Line Number: '+ex.getLineNumber();
        }
        return error;
    }
}