@isTest
public class TestInterpolationRateTableController {
    @isTest public static void testMethod1(){
        Date dateField = System.today();
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                  
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,        
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                        
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id,
            Reserve_Type__c = 'LOC MR'
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  
       
               
        Constituent_Assembly__c consituentAssemblyRecord = new Constituent_Assembly__c();
        consituentAssemblyRecord.Serial_Number__c = '2';
        consituentAssemblyRecord.CSN__c = 3;
        consituentAssemblyRecord.TSN__c = 5;
        consituentAssemblyRecord.Last_CSN_Utilization__c = 3;
        consituentAssemblyRecord.Last_TSN_Utilization__c = 5;
        consituentAssemblyRecord.Attached_Aircraft__c = testAircraft.Id;
        consituentAssemblyRecord.Name ='Testing';
        consituentAssemblyRecord.Type__c ='Airframe'; 


        LWGlobalUtils.setTriggersflagOn(); 
        insert consituentAssemblyRecord ;
        LWGlobalUtils.setTriggersflagOff();
        
         Assembly_Event_Info__c assemblyEventInfoObj=new Assembly_Event_Info__c(
            Name='Airframe-Airframe-Heavy 1',
            Constituent_Assembly__c=consituentAssemblyRecord.Id
        );
        insert assemblyEventInfoObj;
        
        Ratio_Table__c testRatioTable = new Ratio_Table__c(Name = 'Test Ratio Table');
        insert testRatioTable;
        
        Derate_Adjustment__c interpolationRecord = new Derate_Adjustment__c(Name = '2.0');
        interpolationRecord.Ratio_Table__c = testRatioTable.Id;
        interpolationRecord.FH_To_FC_Ratio__c = 2.0;
        interpolationRecord.Derate_Percentage__c = 10;
        interpolationRecord.Dollars_per_Flight_Hour__c = 1000;
        
        insert interpolationRecord;
        
        Assembly_MR_Rate__c assemblyMrRecord = new Assembly_MR_Rate__c();
        assemblyMrRecord.Name ='TestMrRecord';
        assemblyMrRecord.Lease__c =leaseRecord.Id;
        assemblyMrRecord.Base_MRR__c =200;
        //assemblyMrRecord.Assembly_Type__c ='Engine 3';
        assemblyMrRecord.Assembly_Lkp__c = consituentAssemblyRecord.Id;
        assemblyMrRecord.Assembly_Event_Info__c = assemblyEventInfoObj.Id;
		assemblyMrRecord.Rate_Basis__c='Cycles';
        assemblyMrRecord.Ratio_Table__c = testRatioTable.Id;
        
        
        insert assemblyMrRecord;
        
        MR_Rate__c mrRateRecord = new MR_Rate__c(Name = 'Test MR Record');
        mrRateRecord.MR_Rate_Start_Date__c = dateField;
        mrRateRecord.MR_Rate_Start_Date__c = dateField;
        mrRateRecord.Assembly_MR_Info__c = assemblyMrRecord.Id;
        mrRateRecord.MR_Rate_End_Date__c = lastDayOfMonth;
        mrRateRecord.MR_Rate__c = 100;
        mrRateRecord.Rate_Basis__c = 'Monthly';
        insert mrRateRecord;
        
        Utilization_Report__c utilizationRecordSecond = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Provision',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = Date.newInstance(2020, 02, 29 ),
            Status__c='Approved By Lessor'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecordSecond;
        LWGlobalUtils.setTriggersflagOff();
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = Date.newInstance(2020, 02, 29 ),
            Status__c='Approved By Lessor',
            Original_Monthly_Utilization__c = utilizationRecordSecond.Id
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Utilization_Report_List_Item__c utilizationReportListItemObj=new Utilization_Report_List_Item__c(
            Constituent_Assembly__c=consituentAssemblyRecord.Id,
            Utilization_Report__c=utilizationRecord.Id,
            //Event_Name__c=assemblyEventInfoObj.Id,
            Cycles_During_Month__c=1,
            Running_Hours_During_Month__c=2,
            Assembly_MR_Info__c=assemblyMrRecord.Id
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationReportListItemObj;
        LWGlobalUtils.setTriggersflagOff();
        
        Utilization_Report_List_Item__c utilizationReportListItemObjSecond=new Utilization_Report_List_Item__c(
            Constituent_Assembly__c=consituentAssemblyRecord.Id,
            Utilization_Report__c=utilizationRecordSecond.Id,
            //Event_Name__c=assemblyEventInfoObj.Id,
            Cycles_During_Month__c=1,
            Running_Hours_During_Month__c=2,
            Assembly_MR_Info__c=assemblyMrRecord.Id
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationReportListItemObjSecond;
        LWGlobalUtils.setTriggersflagOff();
        
        InterpolationRateTableController.RatioTableJSONContainer ratioTablewithDerateList = new InterpolationRateTableController.RatioTableJSONContainer();
        
        ratioTablewithDerateList.RatioTable = testRatioTable.Id;
        ratioTablewithDerateList.Version = 0.1;
        
        InterpolationRateTableController.InterpolationJSONWrapper interpolationRecordWrapper = new InterpolationRateTableController.InterpolationJSONWrapper();
                            
        interpolationRecordWrapper.Id = interpolationRecord.Id;
        interpolationRecordWrapper.Derate_Percentage = interpolationRecord.Derate_Percentage__c;
        interpolationRecordWrapper.Dollars_per_Flight_Hour = interpolationRecord.Dollars_per_Flight_Hour__c;
        interpolationRecordWrapper.FH_To_FC_Ratio = interpolationRecord.FH_To_FC_Ratio__c;
        ratioTablewithDerateList.InterpolationList.add(interpolationRecordWrapper);
        //
        
        ContentVersion contentversionVarient=new ContentVersion();
        contentversionVarient.Title='LW_UR_RatioTable_Snapshot';
        contentversionVarient.contentLocation='S';
        contentversionVarient.PathOnClient='LW_UR_RatioTable_Snapshot.json';
        contentversionVarient.FirstPublishLocationId = utilizationReportListItemObjSecond.Id;
        contentversionVarient.VersionData=Blob.valueOf(JSON.serialize(ratioTablewithDerateList));
        
        insert contentversionVarient;
        
        InterpolationRateTableController.RatioTableInfoWrapper ratioTable = InterpolationRateTableController.loadData(mrRateRecord.Id);
        
        ratioTable = InterpolationRateTableController.loadData(testRatioTable.Id);
        
        ratioTable = InterpolationRateTableController.loadData(utilizationReportListItemObjSecond.Id);
        system.debug('ratioTable--- '+ratioTable);
        List<InterpolationRateTableController.InterpolationDataWrapper> interpolationTableValues = new List<InterpolationRateTableController.InterpolationDataWrapper>();
        
        for(Integer i=0; i < ratioTable.interpolationTableValues.Size(); i++){
            for(Integer j=0; j < ratioTable.interpolationTableValues[i].interpolationDataList.size(); j++){
                if(ratioTable.interpolationTableValues[i].interpolationDataList[j].id != null){
                    interpolationTableValues.add(ratioTable.interpolationTableValues[i].interpolationDataList[j]);
                }
            }
        }
        InterpolationRateTableController.saveInterpolationRateData(JSON.serialize(interpolationTableValues));
    }
}