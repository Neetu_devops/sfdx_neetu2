@isTest
public class TestLetterOfCreditTriggerHandler {
    
    @isTest public static void testMethod1(){
        Date dateField = System.today();
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                  
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,        
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                        
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id,
            Reserve_Type__c = 'LOC MR'
        );
        insert leaseRecord;
           
         Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
            
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  
        
                
        Constituent_Assembly__c consituentAssemblyRecord = new Constituent_Assembly__c();
        consituentAssemblyRecord.Serial_Number__c = '2';
        //consituentAssemblyRecord.CSLV__c = 2;
        consituentAssemblyRecord.CSN__c = 3;
        //consituentAssemblyRecord.TSLV__c = 4;
        consituentAssemblyRecord.TSN__c = 5;
        consituentAssemblyRecord.Last_CSN_Utilization__c = 3;
        consituentAssemblyRecord.Last_TSN_Utilization__c = 5;
        consituentAssemblyRecord.Attached_Aircraft__c = testAircraft.Id;
        consituentAssemblyRecord.Name ='Testing';
        consituentAssemblyRecord.Type__c ='Airframe'; 


        LWGlobalUtils.setTriggersflagOn(); 
        insert consituentAssemblyRecord ;
        LWGlobalUtils.setTriggersflagOff();
        
         Assembly_Event_Info__c assemblyEventInfoObj=new Assembly_Event_Info__c(
            Name='Airframe-Airframe-Heavy 1',
            Constituent_Assembly__c=consituentAssemblyRecord.Id
        );
        insert assemblyEventInfoObj;
        
        Assembly_MR_Rate__c assemblyMrRecord = new Assembly_MR_Rate__c();
        assemblyMrRecord.Name ='TestMrRecord';
        assemblyMrRecord.Lease__c =leaseRecord.Id;
        assemblyMrRecord.Base_MRR__c =200;
        //assemblyMrRecord.Assembly_Type__c ='Engine 3';
        assemblyMrRecord.Assembly_Lkp__c = consituentAssemblyRecord.Id;
        assemblyMrRecord.Assembly_Event_Info__c = assemblyEventInfoObj.Id;
		assemblyMrRecord.Rate_Basis__c='Cycles';
        
        
        insert assemblyMrRecord;
        
        
        Letter_Of_Credit__c locRecord=new Letter_Of_Credit__c();
        locRecord.Name='Loc Demo';
        locRecord.LC_Amount_LC_Currency__c=200;
        locRecord.LC_Coverage_Type__c = 'Maintenance';
        locRecord.Lease__c=leaseRecord.Id;
        locRecord.Lessee__c=testOperator.Id;
        locRecord.LC_Number__c='123';
        insert locRecord;
        
        LeaseWareUtils.clearFromTrigger();
        
        locRecord.LC_Number__c='321';
        locRecord.LC_Amount_LC_Currency__c =300;
        update locRecord;
        
        delete locRecord;
    }
    @istest
    public static void testLocExpiryOnLease(){
        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;

	    LeaseWareUtils.clearFromTrigger(); 
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
        insert aircraft;

	    LeaseWareUtils.clearFromTrigger(); 
        Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,Lease_End_Date_New__c=Date.today().addYears(10),Lease_Start_Date_New__c=Date.today());
        insert lease;    
        
        list<Letter_Of_Credit__c> locList = new list<Letter_Of_Credit__c>();
        locList.add(new Letter_Of_Credit__c(Name='Loc Demo',LC_Amount_LC_Currency__c=100000,LC_Coverage_Type__c = 'Security Deposit',Lease__c=lease.Id,
                    Lessee__c=operator.Id ,LC_Number__c='123',Expiry_date__c = system.today().addDays(3) ,status__c = 'Active'));
        locList.add(new Letter_Of_Credit__c(Name='Loc Demo',LC_Amount_LC_Currency__c=100000,LC_Coverage_Type__c = 'Maintenance',Lease__c=lease.Id,Lessee__c=operator.Id ,LC_Number__c='123',Expiry_date__c = system.today().addDays(2)
                    ,status__c = 'Active'));
        locList.add(new Letter_Of_Credit__c(Name='Loc Demo',LC_Amount_LC_Currency__c=100000,LC_Coverage_Type__c = 'All Lease Obligation',Lease__c=lease.Id,Lessee__c=operator.Id ,LC_Number__c='123',Expiry_date__c = system.today().addDays(4)
                    ,status__c = 'Active'));
        LeaseWareUtils.clearFromTrigger();
        insert locList;
        lease = [select id,Letter_of_Credit_Expiry__c from lease__c where id = :lease.id];
        system.assertEquals(lease.Letter_of_Credit_Expiry__c,system.today().addDays(3));
        
        locList[1].Expiry_date__c = system.today().addDays(1);
        LeaseWareUtils.clearFromTrigger();
        update locList[1];
       
        lease = [select id,Letter_of_Credit_Expiry__c from lease__c where id = :lease.id];
        system.assertEquals(lease.Letter_of_Credit_Expiry__c,system.today().addDays(3));

        locList[2].Expiry_date__c = system.today().addDays(2);
        LeaseWareUtils.clearFromTrigger();
        update locList[2];

        lease = [select id,Letter_of_Credit_Expiry__c from lease__c where id = :lease.id];
        system.assertEquals(lease.Letter_of_Credit_Expiry__c,system.today().addDays(2));

        LeaseWareUtils.clearFromTrigger();
        delete locList[0];
        lease = [select id,Letter_of_Credit_Expiry__c from lease__c where id = :lease.id];
        system.assertEquals(lease.Letter_of_Credit_Expiry__c,system.today().addDays(2));

        locList.remove(0);
        LeaseWareUtils.clearFromTrigger();
        delete locList;
        lease = [select id,Letter_of_Credit_Expiry__c from lease__c where id = :lease.id];
        system.assertEquals(lease.Letter_of_Credit_Expiry__c,null);
    }
}