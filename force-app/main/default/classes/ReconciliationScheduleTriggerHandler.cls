public class ReconciliationScheduleTriggerHandler implements ITrigger{

    private final string triggerBefore = 'ReconciliationScheduleTriggerHandlerBefore';
    private final string triggerAfter = 'ReconciliationScheduleTriggerHandlerAfter';
    
    
    public ReconciliationScheduleTriggerHandler(){}
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    public void bulkAfter(){}
    
    
    public void beforeInsert()
    {
         
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('ReconciliationScheduleTriggerHandler.beforeUpdate(+)');
        
        validateReconciliationSchedule(Trigger.New);
        
        system.debug('ReconciliationScheduleTriggerHandler.beforeUpdate(-)');      
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    { 
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('ReconciliationScheduleTriggerHandler.beforeDelete(+)');
        
        validateReconciliationSchedule(Trigger.Old);
        
        system.debug('ReconciliationScheduleTriggerHandler.beforeDelete(-)'); 
    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('ReconciliationScheduleTriggerHandler.afterInsert(+)');
        
        updateLastActualUROnReconSchedule(Trigger.New);
        
        system.debug('ReconciliationScheduleTriggerHandler.afterInsert(-)'); 
    }
     
    public void afterUpdate()
    {
         after_update_call(Trigger.new);
    }
     
    public void afterDelete()
    {
     
    }

    public void afterUnDelete()
    {
           
    }
     
    public void andFinally(){
       LeaseWareUtils.TriggerDisabledFlag = false; // to make Trigger disabled at apex level
    }
    
    public static void after_update_call(List<Reconciliation_Schedule__c> reconciliationScheduleList){

        //update warning on the true ups.
        
        Date minDate, maxDate;
        Set<Id> leaseIds = new Set<Id>();
        Set<Reconciliation_Schedule__c> filteredScheduleList = new Set<Reconciliation_Schedule__c>();
        Set<Id> recordTypeIds = new Set<Id>{Schema.SObjectType.Reconciliation_Schedule__c.getRecordTypeInfosByDeveloperName().get('Assumed_FH_and_FC').getRecordTypeId()};
        recordTypeIds.add(Schema.SObjectType.Reconciliation_Schedule__c.getRecordTypeInfosByDeveloperName().get('Assumed_Ratio').getRecordTypeId());
        
        for(Reconciliation_Schedule__c schedule : reconciliationScheduleList){
            
        	if(schedule.Status__c != ((Reconciliation_Schedule__c)Trigger.oldMap.get(schedule.id)).Status__c &&
               schedule.status__c == 'Reconciled' && recordTypeIds.contains(schedule.RecordTypeId)){
                
                    if(minDate == null || schedule.From_date__c < minDate){
                        minDate = schedule.From_date__c.toStartOfMonth();
                    }
                    
                    if(maxDate == null || schedule.To_date__c > maxDate ){
                        maxDate = schedule.To_date__c.addMonths(1).toStartOfMonth().addDays(-1) ;
                    }
                    
                    filteredScheduleList.add(schedule);
                    leaseIds.add(schedule.Lease__c);
            }    
        }

        if(leaseIds.size()>0){

            Map<Id,List<Utilization_Report__c>> leaseURMap = new Map<Id,List<Utilization_Report__c>>();
            
            for(Utilization_Report__c utilization : [Select id, Warning__c, Y_hidden_lease__c, Month_Ending__c 
                                                     		from Utilization_Report__c 
                                                            where Y_hidden_lease__c in : leaseIds 
                                                            And Month_Ending__c >= : minDate
                                                            And Month_Ending__c <= : maxDate
                                                            And Type__c = 'True Up Gross']){
                                                                
                 //collect only those records which already have warning set up.
                 if(utilization.Warning__c != null){  
                     if(!leaseURMap.containsKey(utilization.Y_hidden_lease__c)){
                     	leaseURMap.put(utilization.Y_hidden_lease__c, new List<Utilization_Report__c>());
                     }  
                     leaseURMap.get(utilization.Y_hidden_lease__c).add(utilization);                                                     
                 }                                               
        	}
            
            if(leaseURMap.size()>0){
                
                List<Utilization_Report__c> urToUpdate = new List<Utilization_Report__c>();
                
                for(Reconciliation_Schedule__c reconSchedule : filteredScheduleList){
            
                    if(leaseURMap.containsKey(reconSchedule.Lease__c)){
                    
                        for(Utilization_Report__c utilization : leaseURMap.get(reconSchedule.Lease__c)){
                        
                            if(utilization.Month_Ending__c >= reconSchedule.From_date__c.toStartOfMonth() && 
                                utilization.Month_Ending__c <= reconSchedule.To_date__c.addMonths(1).toStartOfMonth().addDays(-1)){
                            
                                    utilization.Warning__c = '<span style="color:orange;">This utilization is part of the reconciliation done for the period <a href="/' + reconSchedule.Id + '">' + reconSchedule.Name + '</a>.';
                                    utilization.Warning__c += ' The reconciled invoice is approved and hence users will have to raise a manual adjustment invoice to include the true up amount.</span>';
                                    urToUpdate.add(utilization);
                            }
                        }
                    }   
                
                }
                
                if(urToUpdate.size()>0){
                    LWGlobalUtils.setTriggersOff();
                    update urToUpdate;
                    LWGlobalUtils.setTriggersOn();
                }
            }
    	}
    }
    
    
    public static void validateReconciliationSchedule(List<Reconciliation_Schedule__c> reconciliationScheduleRecords) {
        
        Set<Id> leaseIds = new Set<Id>();
        List<Reconciliation_Schedule__c> rsList = new List<Reconciliation_Schedule__c>();
        Date startDate;
        Date endDate;
        Boolean allowUpdate = LeaseWareUtils.isFromTrigger('AllowUpdateOnReconciliationSchedule');
        
        for(Reconciliation_Schedule__c reconciliationScheduleRecord : reconciliationScheduleRecords) {
            
            if((Trigger.isDelete && (reconciliationScheduleRecord.Status__c == 'Complete' || 
                                    reconciliationScheduleRecord.Status__c == 'Reconciled')) ||
            
                      (!allowUpdate && Trigger.isUpdate && (((Reconciliation_Schedule__c)Trigger.oldMap.get(reconciliationScheduleRecord.Id)).status__c == 'Complete'
                       || ((Reconciliation_Schedule__c)Trigger.oldMap.get(reconciliationScheduleRecord.Id)).status__c == 'Reconciled'))
                         
                ){
            
                
                String errorMsg = Trigger.isDelete ? 'You cannot delete reconciliation schedules for which reconciliation is complete, reconciled or if there are utilization that are not invoiced.':
                                                     'Reconciliation Schedule cannot be modified if it has been marked as Complete or Reconciled.';  
                reconciliationScheduleRecord.addError(errorMsg);
                
            }else if(Trigger.isUpdate && 
                     (reconciliationScheduleRecord.To_Date__c != ((Reconciliation_Schedule__c)Trigger.oldMap.get(reconciliationScheduleRecord.Id)).To_Date__c 
                      || reconciliationScheduleRecord.From_Date__c != ((Reconciliation_Schedule__c)Trigger.oldMap.get(reconciliationScheduleRecord.Id)).From_Date__c)){
            
                //check if Actual Utilization filed for the period or not.
                //if so, no delete and update will be allowed.
                                  
                leaseIds.add(reconciliationScheduleRecord.lease__c);
                rsList.add(reconciliationScheduleRecord);
               
               
                Reconciliation_Schedule__c oldRecord = (Reconciliation_Schedule__c) Trigger.oldMap.get(reconciliationScheduleRecord.Id);
               
                }
            }
        
        if(leaseIds.size() > 0){
           
                Map<Id, List<Reconciliation_Schedule__c>> leaseReconciliationMap = new Map<Id, List<Reconciliation_Schedule__c>>();
                
                for(Lease__c leaseRec : [Select Id, (Select Id, From_Date__c, To_Date__c 
                                                     From Reconciliation_Schedules__r
                                                 Where Id NOT IN : rsList)
                                         from Lease__c where Id IN : leaseIds]){
                    if(leaseRec.Reconciliation_Schedules__r.Size() > 0){
                        leaseReconciliationMap.put(leaseRec.Id, leaseRec.Reconciliation_Schedules__r);
                    }
                }
                
                if(leaseReconciliationMap.size() > 0){
                    
                for(Reconciliation_Schedule__c reconciliationScheduleRecord : rsList) {
                    
                        Boolean hasError = false;
                        
                        if(leaseReconciliationMap.containsKey(reconciliationScheduleRecord.Lease__c)){

                            for(Reconciliation_Schedule__c rsRecord : leaseReconciliationMap.get(reconciliationScheduleRecord.Lease__c)){
                                
                                Date tempStartDate = rsRecord.From_Date__c.toStartOfMonth();
                                Date tempEndDate = rsRecord.To_Date__c.toStartOfMonth().addMonths(1).addDays(-1);
                                

                                
                                if(( tempEndDate >= reconciliationScheduleRecord.To_Date__c
                                    && tempStartDate <= reconciliationScheduleRecord.To_Date__c)
                                   || ( tempEndDate >=  reconciliationScheduleRecord.From_Date__c
                                       && tempStartDate <= reconciliationScheduleRecord.From_Date__c)){
                                           
                                           hasError = true;
                                           break;
                                           
                                }else if((tempEndDate <= reconciliationScheduleRecord.To_Date__c
                                         && tempEndDate >=  reconciliationScheduleRecord.From_Date__c)
                                        || ( tempStartDate <=  reconciliationScheduleRecord.To_Date__c
                                            && tempStartDate >=  reconciliationScheduleRecord.From_Date__c)){
                                                
                                                hasError = true;
                                                break;
                                                
                                }
                            } 
                        }
                        
                        if(hasError){
                            
                                String errorMsg ='Reconciliation Schedule already exists for the period selected. Please revise the dates.';  
                                reconciliationScheduleRecord.addError(errorMsg);
                                
                        }else{
                                    
                               //set the name and date override to true.
                               reconciliationScheduleRecord.Date_Override__c = true;
          
                               Datetime tempStartDateFormate = datetime.newInstance(reconciliationScheduleRecord.From_Date__c.year(), reconciliationScheduleRecord.From_Date__c.month(),reconciliationScheduleRecord.From_Date__c.day());
                               
                               Datetime tempEndDateFormate = datetime.newInstance(reconciliationScheduleRecord.To_Date__c.year(), reconciliationScheduleRecord.To_Date__c.month(),reconciliationScheduleRecord.To_Date__c.day());
                       
                               reconciliationScheduleRecord.Name = tempStartDateFormate.year()+' '+ tempStartDateFormate.format('MMM')+' to '+ tempEndDateFormate.year() +' '+tempEndDateFormate.format('MMM');
                            
                               if(!leaseReconciliationMap.containsKey(reconciliationScheduleRecord.Lease__c)){
                                   leaseReconciliationMap.put(reconciliationScheduleRecord.Lease__c, new List<Reconciliation_Schedule__c>());
                               }
                              
                               leaseReconciliationMap.get(reconciliationScheduleRecord.Lease__c).add(reconciliationScheduleRecord);
                         }
                        
                    }
                }
            }
        }
    
    private void updateLastActualUROnReconSchedule(List<Reconciliation_Schedule__c> reconciliationScheduleRecords) {
        
        Map<Id, Set<Date>> leaseWithMonthEndingMap = new Map<Id, Set<Date>>();
        
        for(Reconciliation_Schedule__c reconciliationScheduleRecord : reconciliationScheduleRecords) {
            if(!leaseWithMonthEndingMap.containsKey(reconciliationScheduleRecord.lease__c)){
                    leaseWithMonthEndingMap.put(reconciliationScheduleRecord.lease__c, new Set<Date>());
                }
                
                leaseWithMonthEndingMap.get(reconciliationScheduleRecord.lease__c).add(reconciliationScheduleRecord.To_Date__c);
        }
        
        if(leaseWithMonthEndingMap.size()>0){
            URTriggerHandler.updateLastActualUROnReconSchedule(leaseWithMonthEndingMap);
        }
    }
}
