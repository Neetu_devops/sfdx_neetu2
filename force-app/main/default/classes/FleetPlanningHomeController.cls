public without sharing class FleetPlanningHomeController {

    @AuraEnabled
    public static ContainerWrapper getData(){
        
        ContainerWrapper container = new ContainerWrapper();
        container.userName = Userinfo.getName();
        
        Set<Id> assetsOnLease = new Set<Id>();
        String communityPrefix = UtilityWithoutSharingController.getCommunityPrefix();
        List<Operator__c> operators = UtilityWithSharingController.getOperators();
        
        for(Lease__c lease : [select Id, Lease_Id_External__c,Lease_Start_Date_New__c,Lease_End_Date_New__c,
                                          Lease_Type_Status__c,Operator__r.Name, Base_Rent__c,
                                          Aircraft__r.MSN_Number__c, Aircraft__c,
                                          Aircraft__r.RecordType.DeveloperName, Greentime_Lease_External__c,
                                          Lease_Remaining_Months_External__c
                                          from Lease__c
                                          where lessee__c in : operators
                                          and Lease_Status_External__c = 'Active'  and Aircraft__c != null ]){
                                              
            assetsOnLease.add(lease.Aircraft__c);
                                              
            String msnURL, submitUR_URL;
                
            submitUR_URL = communityPrefix + '/s/submit-ur?recordId=' + UtilityWithoutSharingController.encodeData(lease.Aircraft__c);
                
            if(lease.Aircraft__r.RecordType.DeveloperName == 'Aircraft'){
               msnURL = communityPrefix + '/s/aircraft-details?recordId=';                                 
            }else{
               msnURL = communityPrefix + '/s/engine-details?recordId=';                                 
            }                                  
            
            msnURL += UtilityWithoutSharingController.encodeData(lease.Aircraft__c);                                  
                                              
            container.leaseOverview.add(new LeaseWrapper(lease.Lease_Id_External__c, lease.Id, lease.Aircraft__r.RecordType.DeveloperName, 
                                                         lease.Aircraft__r.MSN_Number__c, msnURL,
                                                         lease.Operator__r.Name,
                                                         lease.Lease_Type_Status__c,lease.Lease_Start_Date_New__c,
                                                         lease.Lease_End_Date_New__c, submitUR_URL,
                                                         lease.Greentime_Lease_External__c, lease.Base_Rent__c,
                                                         lease.Lease_Remaining_Months_External__c)); 
        }
        
        //get the Aircrafts and Engines on Lease.
        if(assetsOnLease.size()>0){
            
           	for(Aircraft__c asset : [select id, recordType.DeveloperName, Aircraft_Variant__c, 
                                                 Vintage__c,TSN__c, CSN__c,
                                                 MSN_Number__c, Engine_Type__c, Type_Variant__c,
                                                 Aircraft_Series__c,Lease__r.Lessee__r.Name,
                                                 (select id, Current_TS__r.Name, Lowest_LLP_Limiter_Current_TS__c,
                                                  TSO_External__c, CSO_External__c from Constituent_Assemblies__r 
                                                  where Replaced_External__c = false limit 1)
                                                 from Aircraft__c
                                                 where Status__c != 'Sold'
                                                 and id in : assetsOnLease]){
            
                if(asset.recordType.DeveloperName == 'Aircraft'){
                    
                    String url = communityPrefix + '/s/aircraft-details?recordId=' + UtilityWithoutSharingController.encodeData(asset.Id);
                        
                    container.aircraftsOnLease.add(new AircraftWrapper(asset.MSN_Number__c, url, asset.Type_Variant__c,
                                                                       asset.Aircraft_Variant__c, asset.Vintage__c, 
                                                                       asset.Engine_Type__c, asset.Lease__r.Lessee__r.Name, 
                                                                       asset.TSN__c, asset.CSN__c));   

                }else{
                    
                    String currentThrust;
                    Decimal tso, cso, limiter;
					String url = communityPrefix + '/s/engine-details?recordId=' +  UtilityWithoutSharingController.encodeData(asset.Id);
                    
                    if(asset.Constituent_Assemblies__r.size()>0){
                        limiter = asset.Constituent_Assemblies__r[0].Lowest_LLP_Limiter_Current_TS__c;
                        currentThrust = asset.Constituent_Assemblies__r[0].Current_TS__r.Name;
                        tso = asset.Constituent_Assemblies__r[0].TSO_External__c;
                        cso = asset.Constituent_Assemblies__r[0].CSO_External__c;
                    }
                    
                    container.enginesOnLease.add(new EngineWrapper(asset.MSN_Number__c, url,
                                                                   asset.Aircraft_Series__c, currentThrust, 
                                                                   asset.Lease__r.Lessee__r.Name, 
                                                                   tso, cso, limiter));
                    
                }                                         
            } 
        }
        
        return container;  
    }
    

    public class ContainerWrapper{
        @AuraEnabled public String userName ;
        @AuraEnabled public List<LeaseWrapper> leaseOverview ;
        @AuraEnabled public List<AircraftWrapper> aircraftsOnLease ;
        @AuraEnabled public List<EngineWrapper> enginesOnLease ;

            
        public ContainerWrapper(){
            leaseOverview =  new List<LeaseWrapper>();
            aircraftsOnLease =  new List<AircraftWrapper>();
            enginesOnLease =  new List<EngineWrapper>();
        }
    }
    
    public class LeaseWrapper{
        @AuraEnabled public String title; 
        @AuraEnabled public String isGTLease;
        @AuraEnabled public Decimal baseRent; 
        @AuraEnabled public Integer monthsRemaining;
        @AuraEnabled public String id; 
        @AuraEnabled public String msn; 
        @AuraEnabled public String msnURL;
        @AuraEnabled public String type; 
        @AuraEnabled public String operatorName; 
        @AuraEnabled public String status;
        @AuraEnabled public Date leaseStart;
        @AuraEnabled public Date leaseEnd;
        @AuraEnabled public String submitUR_URL;
        @AuraEnabled public String submitURTitle; 
        
        public LeaseWrapper(String title, String id, String type, String msn, String msnURL, String operatorName, String status, 
                            Date leaseStart, Date leaseEnd, String submitUR_URL, String isGTLease, Decimal baseRent,
                           Decimal monthsRemaining){
        
        	this.title = title;
            this.id = id;
            this.type = type;     
            this.msn = msn;   
            this.msnURL = msnURL;                    
            this.operatorName = operatorName;
            this.status = status;
            this.leaseStart = leaseStart;
            this.leaseEnd =  (leaseEnd == Date.newInstance(2200, 01, 01) ? null : leaseEnd);
            this.submitUR_URL = submitUR_URL;
            submitURTitle = 'Submit Utilization';
            this.isGTLease = isGTLease;
            this.baseRent = baseRent;
            this.monthsRemaining = monthsRemaining != null ? Integer.valueOf(monthsRemaining) : null;
                               
        }
    }
    
    public class AircraftWrapper{
        @AuraEnabled public String msn; 
        @AuraEnabled public String msnURL ; 
        @AuraEnabled public String aircraftType ;
        @AuraEnabled public String assetVariant; 
        @AuraEnabled public String vintage; 
        @AuraEnabled public String aircraftEngineType; 
        @AuraEnabled public String operatorName;
        @AuraEnabled public Decimal tsn;
        @AuraEnabled public Decimal csn;
        
        public AircraftWrapper(String msn, String msnURL, String aircraftType, String assetVariant, String vintage, String aircraftEngineType,
                              String operatorName, Decimal tsn, Decimal csn){
            
        	this.msn = msn;
            this.msnURL = msnURL;
            this.aircraftType = aircraftType;
            this.assetVariant = assetVariant;    
            this.vintage = (vintage != null ? Vintage.left(4) : null);
            this.aircraftEngineType = aircraftEngineType;
            this.operatorName = operatorName;
            this.tsn = tsn;
            this.csn = csn;
        }
    }
    
    public class EngineWrapper{
        @AuraEnabled public String esn;
        @AuraEnabled public String esnURL;
        @AuraEnabled public String assetSeries; 
        @AuraEnabled public String currentThrust; 
        @AuraEnabled public String operatorName;
        @AuraEnabled public Decimal tso; 
        @AuraEnabled public Decimal cso; 
        @AuraEnabled public Decimal limiter;
        
        public EngineWrapper(String esn, String esnURL, String assetSeries, String currentThrust, String operatorName, 
                             Decimal tso, Decimal cso, Decimal limiter){
            
        	this.esn = esn;
            this.esnURL = esnURL;
            this.assetSeries = assetSeries;    
            this.currentThrust = currentThrust;
            this.operatorName = operatorName;
            this.tso = tso;
            this.cso = cso;
            this.limiter = limiter;
        }
    }
}