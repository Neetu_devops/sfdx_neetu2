public class MPDCatalogTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'MPDCatalogTriggerHandlerBefore';
    private final string triggerAfter = 'MPDCatalogTriggerHandlerAfter';
    
    private static list<Maintenance_Program_Event_Catalog__c>  listExistingMPDCatalog;
    
    public MPDCatalogTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
     
    private void setSOQLStaticMAPOrList(){
    	Maintenance_Program_Event_Catalog__c[] listNew = (list<Maintenance_Program_Event_Catalog__c>)(trigger.isDelete?trigger.old:trigger.new) ;
    	
    	//if(listExistingMPDCatalog==null)
    	{
    		listExistingMPDCatalog = [select id,Name,Maintenance_Program_Event__c,Period__c,Event_Count__c,MP_Applicability__c
    				
 				from Maintenance_Program_Event_Catalog__c
 				where id not in :listNew];
    	}
        
    }     
     
    public void bulkBefore()
    {
        setSOQLStaticMAPOrList();
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('MPDCatalogTriggerHandler.beforeInsert(+)');
        checkDuplicateTemplate();

        system.debug('MPDCatalogTriggerHandler.beforeInsert(+)');      
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('MPDCatalogTriggerHandler.beforeUpdate(+)');
        checkDuplicateTemplate();
        system.debug('MPDCatalogTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        system.debug('MPDCatalogTriggerHandler.beforeDelete(+)');
    

 			
        system.debug('MPDCatalogTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('MPDCatalogTriggerHandler.afterInsert(+)');
        
        system.debug('MPDCatalogTriggerHandler.afterInsert(-)');       
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('MPDCatalogTriggerHandler.afterUpdate(+)');
        
		updateMPEOnChangeOfCurrentYrCatalog();
        system.debug('MPDCatalogTriggerHandler.afterUpdate(-)');       
    }
     
    public void afterDelete()
    {
        system.debug('MPDCatalogTriggerHandler.afterDelete(+)');
        
        
        system.debug('MPDCatalogTriggerHandler.afterDelete(-)');       
    }

    public void afterUnDelete()
    {
        system.debug('MPDCatalogTriggerHandler.afterUnDelete(+)');
        

        
        system.debug('MPDCatalogTriggerHandler.afterUnDelete(-)');         
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
    
    
 	private void checkDuplicateTemplate(){
 		Maintenance_Program_Event_Catalog__c[] listNew = (list<Maintenance_Program_Event_Catalog__c>)trigger.new ;
 		set<string> setCheckDuplicateYear = new set<string>();
 		for(Maintenance_Program_Event_Catalog__c curRec:listExistingMPDCatalog){
 			setCheckDuplicateYear.add(curRec.Maintenance_Program_Event__c+curRec.Name + (curRec.MP_Applicability__c==null?'':curRec.MP_Applicability__c) +(curRec.Event_Count__c==null?'':curRec.Event_Count__c) + (curRec.Period__c==null?'':curRec.Period__c));
 		}
 		for(Maintenance_Program_Event_Catalog__c curRec:listNew){
 			if(setCheckDuplicateYear.contains(curRec.Maintenance_Program_Event__c+curRec.Name + (((curRec.MP_Applicability__c==null?'':curRec.MP_Applicability__c))) +((curRec.Event_Count__c==null?'':curRec.Event_Count__c)) + ((curRec.Period__c==null?'':curRec.Period__c)))){
 				curRec.addError('Duplicate Catalog found : {'+ curRec.Y_Hidden_Parent_Name__c
 					 + '}, {'+ curRec.Name + ' ' + (curRec.MP_Applicability__c==null?'':curRec.MP_Applicability__c) + ' ' +(curRec.Event_Count__c==null?'':curRec.Event_Count__c) + ' ' + (curRec.Period__c==null?'':curRec.Period__c) + '}.');
 			}
 			setCheckDuplicateYear.add(curRec.Name + (curRec.MP_Applicability__c==null?'':curRec.MP_Applicability__c) + (curRec.Event_Count__c==null?'':curRec.Event_Count__c) + (curRec.Period__c==null?'':curRec.Period__c));
        }	
 	}
 
 	// after update
	private void updateMPEOnChangeOfCurrentYrCatalog(){

 		Maintenance_Program_Event_Catalog__c[] listNew = (list<Maintenance_Program_Event_Catalog__c>)trigger.new ;
 		map<string,Maintenance_Program_Event_Catalog__c> mapMPE = new map<string,Maintenance_Program_Event_Catalog__c>();
 		Maintenance_Program_Event_Catalog__c oldrec;
 		for(Maintenance_Program_Event_Catalog__c curRec:listNew){
 			oldrec = (Maintenance_Program_Event_Catalog__c)trigger.oldMap.get(curRec.Id);
 			if(curRec.Current__c && (oldrec.Event_Cost__c!=curRec.Event_Cost__c
 						|| oldrec.Never_Exceed_Period_Cycles__c!=curRec.Never_Exceed_Period_Cycles__c
 						|| oldrec.Never_Exceed_Period_Hours__c!=curRec.Never_Exceed_Period_Hours__c
 						|| oldrec.Never_Exceed_Period_Months__c!=curRec.Never_Exceed_Period_Months__c
 						|| oldrec.Interval_2_Cycles__c!=curRec.Interval_2_Cycles__c
 						|| oldrec.Interval_2_Hours__c!=curRec.Interval_2_Hours__c
 						|| oldrec.Interval_2_Months__c!=curRec.Interval_2_Months__c
 					)
 				){
 				mapMPE.put(curRec.Maintenance_Program_Event__c,curRec);
 			}
 		}

		if(mapMPE.size()>0){
			Maintenance_Program_Event__c[] updMPE_List = [select id 
									,Event_Cost__c
									,Never_Exceed_Period_Cycles__c
									,Never_Exceed_Period_Hours__c
									,Never_Exceed_Period_Months__c
									,Interval_2_Cycles__c
									,Interval_2_Hours__c
									,Interval_2_Months__c			
									from Maintenance_Program_Event__c
									where id in :mapMPE.keySet()];
			for(Maintenance_Program_Event__c curMPE:updMPE_List){
				oldrec = mapMPE.get(curMPE.Id);
				curMPE.Event_Cost__c = oldrec.Event_Cost__c;
				curMPE.Never_Exceed_Period_Cycles__c=oldrec.Never_Exceed_Period_Cycles__c;
				curMPE.Never_Exceed_Period_Hours__c=oldrec.Never_Exceed_Period_Hours__c;
				curMPE.Never_Exceed_Period_Months__c=oldrec.Never_Exceed_Period_Months__c;
				curMPE.Interval_2_Cycles__c=oldrec.Interval_2_Cycles__c;
				curMPE.Interval_2_Hours__c=oldrec.Interval_2_Hours__c;
				curMPE.Interval_2_Months__c=oldrec.Interval_2_Months__c;				
			}
			update updMPE_List;
		}		
						
	}


}