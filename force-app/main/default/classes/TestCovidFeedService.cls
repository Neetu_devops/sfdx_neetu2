@isTest
private class TestCovidFeedService {

    static testMethod void callCovidFeedServiceCodeCoverage(){
        Test.startTest();
        CovidFeedService covidServiceInst = new CovidFeedService();
        CovidFeedService.execute(null);
        Test.stopTest();
    }
	static testMethod void callCovidAPIFirst(){
        CovidFeedService covidServiceInst = new CovidFeedService();
        Country__c conIndia = new Country__c(Name='India');
        insert conIndia;
        
        Mapping__c mapRec1 = new Mapping__c(Name='Bharat',Map_To__c='India');
        insert mapRec1;       

        string jsonBody= '[{"Active Cases_text":"6,553,403","Country_text":"World","Last Update":"2020-08-21 15:26","New Cases_text":"+71,566","New Deaths_text":"+1,802","Total Cases_text":"22,921,668","Total Deaths_text":"798,178","Total Recovered_text":"15,570,087"},{"Active Cases_text":"2,479,939","Country_text":"India","Last Update":"2020-08-21 15:26","New Cases_text":"+7,787","New Deaths_text":"+169","Total Cases_text":"5,754,059","Total Deaths_text":"177,593","Total Recovered_text":"3,096,527"},{"Active Cases_text":"0","Country_text":"Anguilla","New Cases_text":"","New Deaths_text":"","Total Cases_text":"3","Total Deaths_text":"","Total Recovered_text":"3"},{"Last Update":"2020-08-21 15:26"}]';
    	CovidApiHttpMock mock = new CovidApiHttpMock(jsonBody);
       	Test.setMock(HttpCalloutMock.class, mock);
       	HTTPResponse res =null;        
        
        Test.startTest();
		covidServiceInst.callCovidAPIFirst();
        covidServiceInst.upsertRecordsToPandemic();
        Test.stopTest();
        Pandemic_Info__c[] pandemicList = [select id from Pandemic_Info__c where country__c =:conIndia.id];
        //assert
        system.debug('Number of record should be one' + pandemicList.size());
        system.assert(pandemicList.size()==1,true);
	}  

	static testMethod void callCovidAPISecond(){
        CovidFeedService covidServiceInst = new CovidFeedService();
        Country__c conIndia = new Country__c(Name='India');
        insert conIndia;
        
        string jsonBody= '{"get":"statistics","parameters":[],"errors":[],"results":227,"response":[{"continent":"Asia","country":"India","population":1439323776,"cases":{"new":"+22","active":491,"critical":20,"recovered":79792,"1M_pop":"59","total":84917},"deaths":{"new":null,"1M_pop":"3","total":4634},"tests":{"1M_pop":"62814","total":90410000},"day":"2020-08-21","time":"2020-08-21T16:15:06+00:00"},{"continent":"Europe","country":"Italy","population":60449180,"cases":{"new":"+947","active":16678,"critical":69,"recovered":204960,"1M_pop":"4253","total":257065},"deaths":{"new":"+9","1M_pop":"586","total":35427},"tests":{"1M_pop":"130069","total":7862592},"day":"2020-08-21","time":"2020-08-21T16:15:06+00:00"}]}';
    	CovidApiHttpMock mock = new CovidApiHttpMock(jsonBody);
       	Test.setMock(HttpCalloutMock.class, mock);
       	HTTPResponse res =null;        
        
        Test.startTest();
		covidServiceInst.callCovidAPISecond();
        covidServiceInst.upsertRecordsToPandemic();
        Test.stopTest();
        Pandemic_Info__c[] pandemicList = [select id from Pandemic_Info__c where country__c =:conIndia.id];
        //assert
        system.debug('Number of record should be one' + pandemicList.size());
        system.assert(pandemicList.size()==1,true);
    }  
    
    
	static testMethod void callCovidAPIThird(){
        CovidFeedService covidServiceInst = new CovidFeedService();
        Country__c conUSA = new Country__c(Name='USA');
        insert conUSA;
        
        string jsonBody= '{"countries_stat":[{"country_name":"USA","cases":"5,758,769","deaths":"177,660","region":"","total_recovered":"3,097,355","new_deaths":"236","new_cases":"12,497","serious_critical":"16,812","active_cases":"2,483,754","total_cases_per_1m_population":"17,384","deaths_per_1m_population":"536","total_tests":"74,028,814","tests_per_1m_population":"223,468"},{"country_name":"Brazil","cases":"3,505,361","deaths":"112,445","region":"","total_recovered":"2,653,407","new_deaths":"22","new_cases":"264","serious_critical":"8,318","active_cases":"739,509","total_cases_per_1m_population":"16,475","deaths_per_1m_population":"528","total_tests":"13,748,152","tests_per_1m_population":"64,616"}],"statistic_taken_at": "2020-08-21 16:16:02"} ';
    	CovidApiHttpMock mock = new CovidApiHttpMock(jsonBody);
       	Test.setMock(HttpCalloutMock.class, mock);
       	HTTPResponse res =null;        
        
        Test.startTest();
		covidServiceInst.callCovidAPIThird();
        covidServiceInst.upsertRecordsToPandemic();
        Test.stopTest();
        Pandemic_Info__c[] pandemicList = [select id from Pandemic_Info__c where country__c =:conUSA.id];
        //assert
        system.debug('Number of record should be one' + pandemicList.size());
        system.assert(pandemicList.size()==1,true);
	}      

    class CovidApiHttpMock implements HttpCalloutMock{
        private string jsonbody;

        CovidApiHttpMock(string jsonbody){
            this.jsonbody = jsonbody;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setHeader('Content-Type', 'application/json');
            res.setBody(jsonbody);
            return res;             
        }
    }
}