/**
***************************************************************************************************
* Class: GenericActionOverrideController
* @author Created by Aman K. Gupta, Lease-Works, 
* @date 01/04/2020
* @version 1.0
* -------------------------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to load data for generic action override component.
*
* History:
* VERSION   DEVELOPER_NAME      DATE        DETAIL
* 1.0       Aman K. Gupta       01/04/20    Initial Logic
* 1.1       Aman K. Gupta       30/04/20    Override Logic
**************************************************************************************************
*/
public class GenericActionOverrideController {
    /**
    * @description Retreives default mappings related to given object & recordtype and populates parentId whereever specified.
    *
    * @param sObjectName - String: name of the Object.
    * @param recordTypeId - String: recordTypeId selected by user if any.
    * @param parentRecordId - String, parentId if loaded from a related List.
    *
    * @return response - Map of all fields with default field values
    */
    @AuraEnabled
    public static String fetchInitData(String sObjectName, String recordTypeId, String parentRecordId){
        try{
            String nameSpacePrifix = LeaseWareUtils.getNamespacePrefix() == null ? '' : LeaseWareUtils.getNamespacePrefix();
            String objectNameString = '';
            Map<String, Object> response = new Map<String, Object>();
            Map<String, Set<String>> lookupFieldMap = new Map<String, Set<String>>();
            LW_Action_Override_Config__mdt laocData;

            laocData = getMetadataRecord(nameSpacePrifix, nameSpacePrifix+'Object_API_Name__c = \'Name\'');
            if(laocData != null && laocData.Field_Mapping__c != null && laocData.Field_Mapping__c != ''){
                Map<String, Object> jsonObjectMap = (Map<String, Object>) Json.deserializeUntyped(laocData.Field_Mapping__c.trim());
                for(String key : jsonObjectMap.keySet()){
                    response.put(key, jsonObjectMap.get(key));
                }
                laocData = null;
            }
            System.debug('response: '+response);

            if(sObjectName == null || sObjectName == '') {
                return JSON.serialize(response);
            }
            if(recordTypeId != null && recordTypeId != ''){
                objectNameString = sObjectName+'.'+getRTDeveloperName(sObjectName, recordTypeId);
            }
            System.debug('objectNameString: '+objectNameString);

            if(parentRecordId != null && parentRecordId != '') {
                lookupFieldMap = getLookupFieldMap(sObjectName, parentRecordId);
            }
            System.debug('lookupFieldMap: '+lookupFieldMap);

            if(objectNameString != ''){
            	laocData = getMetadataRecord(nameSpacePrifix, nameSpacePrifix+'Object_API_Name__c = \''+objectNameString+'\'');
            }
            if(laocData == null){
                laocData = getMetadataRecord(nameSpacePrifix, nameSpacePrifix+'Object_API_Name__c = \''+sObjectName+'\'');
            }
            System.debug('laocData: '+laocData);

            if(laocData != null && laocData.Field_Mapping__c != null && laocData.Field_Mapping__c != ''){
                Map<String, Object> jsonObjectMap = (Map<String, Object>) Json.deserializeUntyped(laocData.Field_Mapping__c.trim());
                for(String key : jsonObjectMap.keySet()){
                    Boolean addNameSpacePrefix = nameSpacePrifix != '' ? true : false;
                    if(key.indexOf(nameSpacePrifix) != -1){ addNameSpacePrefix = false; }
					
                    if(key == 'OVERRIDE' && parentRecordId != null){
                        addOverrideFields(nameSpacePrifix, sObjectName, parentRecordId, response);
                    }
                    else if(lookupFieldMap.containsKey('MASTER') && lookupFieldMap.get('MASTER') != null && lookupFieldMap.get('MASTER').contains(key) && jsonObjectMap.get(key) == '<SET_PARENT>'){
                        response.put((addNameSpacePrefix ? nameSpacePrifix+''+key : key), (parentRecordId != null && parentRecordId != '') ? parentRecordId : null);
                    }
                    else if(lookupFieldMap.containsKey('LOOKUP') && lookupFieldMap.get('LOOKUP') != null && lookupFieldMap.get('LOOKUP').contains(key) && jsonObjectMap.get(key) == '<SET_PARENT>'){
                        response.put((addNameSpacePrefix ? nameSpacePrifix+''+key : key), (parentRecordId != null && parentRecordId != '') ? parentRecordId : null);
                    }
                    else if(jsonObjectMap.get(key) == '<SET_PARENT>'){
                        continue;
                    }
                    else if(key == 'Name'){
                        response.put(key, jsonObjectMap.get(key));
                    }
                    else{
                        response.put((addNameSpacePrefix ? nameSpacePrifix+''+key : key), jsonObjectMap.get(key));
                    }
                }
            }
            
            if(lookupFieldMap.containsKey('MASTER') && lookupFieldMap.get('MASTER') != null){
                for(String key : lookupFieldMap.get('MASTER')){
                    Boolean addNameSpacePrefix = nameSpacePrifix != '' ? true : false;
                    if(key.indexOf(nameSpacePrifix) != -1){ addNameSpacePrefix = false; }
                    key = (addNameSpacePrefix ? nameSpacePrifix+''+key : key);
                    
                    if(!response.containsKey(key)){
                        response.put(key, (parentRecordId != null && parentRecordId != '') ? parentRecordId : null);
                    }
                }
            }
            if(lookupFieldMap.containsKey('LOOKUP') && lookupFieldMap.get('LOOKUP') != null){
                for(String key : lookupFieldMap.get('LOOKUP')){
                    Boolean addNameSpacePrefix = nameSpacePrifix != '' ? true : false;
                    if(key.indexOf(nameSpacePrifix) != -1){ addNameSpacePrefix = false; }
                    key = (addNameSpacePrefix ? nameSpacePrifix+''+key : key);
                    
                    if(!response.containsKey(key)){
                        response.put(key, (parentRecordId != null && parentRecordId != '') ? parentRecordId : null);
                    }
                }
            }
            System.debug('response: '+JSON.serialize(response));
            return JSON.serialize(response);
        }
        catch(Exception ex){
            System.debug('Error: '+ex.getMessage()+' : Line Number: '+ex.getLineNumber()+' : '+ex.getStackTraceString());
        }
        return '';
    }

    /**
    * @description Retreives default mappings records from LW_Action_Override_Config__mdt based on filter criteria.
    * 
    * @param nameSpacePrifix - String: namespaceprefic for the object and field if called within package.
    * @param whereClause - String: filter criteria to limit the records of LW_Action_Override_Config__mdt.
    *
    * @return LW_Action_Override_Config__mdt - records fetched from the dynamic SOQL query.
    */
    public static LW_Action_Override_Config__mdt getMetadataRecord(String nameSpacePrifix, String whereClause){
        if(whereClause != null && whereClause != ''){
            String query = 'SELECT MasterLabel, '+nameSpacePrifix+'Field_Mapping__c, '+nameSpacePrifix+'Object_API_Name__c FROM '+nameSpacePrifix+'LW_Action_Override_Config__mdt WHERE '+nameSpacePrifix+'Inactive__c = false AND ' + whereClause;
            List<LW_Action_Override_Config__mdt> laocDataList = Database.query(query);
            if(!laocDataList.isEmpty()){
                return laocDataList[0];
            }
        }
        return null;
    }

    /**
    * @description Retreives record type developer name using objectName and recordtypeId.
    *
    * @param sObjectName - String: name of the Object.
    * @param recordTypeId - String: recordTypeId selected by user if any.
    *
    * @return String: recordtype developer name.
    */
    public static String getRTDeveloperName(String sObjectName, String recordTypeId){
        if(Schema.getGlobalDescribe().get(sObjectName).getDescribe().getRecordTypeInfosById().containsKey(recordTypeId) && Schema.getGlobalDescribe().get(sObjectName).getDescribe().getRecordTypeInfosById().get(recordTypeId) != null){
            return Schema.getGlobalDescribe().get(sObjectName).getDescribe().getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
        }
        return '';
    }

    /**
    * @description Retreives all the matching Master Detail & Lookup field name based on the object name of the parent Id.
    *
    * @param sObjectName - String: name of the Object.
    * @param parentRecordId - String, parentId if loaded from a related List.
    *
    * @return Map<String, Set<String>>: Map of all the fields that matches the object name as of the parentRecordId.
    */
    public static Map<String, Set<String>> getLookupFieldMap(String sObjectName, String parentRecordId){
        Map<String, Set<String>> responseMap = new Map<String, Set<String>>();
        String parentObjectName = Id.valueOf(parentRecordId).getSobjectType().getDescribe().getName();
        System.debug('parentObjectName: '+parentObjectName);

        Map<String, Schema.SObjectField> field_map = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
        for(String key : field_map.keySet()){
            Schema.DescribeFieldResult field = field_map.get(key).getDescribe();

            if(String.valueOf(field.getType()) == 'REFERENCE'){
                if(String.valueOf(field.getRelationshipOrder()) != null){
                    if(String.valueOf(field.getReferenceTo()[0]) == parentObjectName){
                        if(!responseMap.containsKey('MASTER')){
                            responseMap.put('MASTER', new Set<String>());
                        }
                        responseMap.get('MASTER').add(String.valueOf(field.getSObjectField()));
                    }
                }
                else{
                    if(String.valueOf(field.getReferenceTo()[0]) == parentObjectName){
                        if(!responseMap.containsKey('LOOKUP')){
                            responseMap.put('LOOKUP', new Set<String>());
                        }
                        responseMap.get('LOOKUP').add(String.valueOf(field.getSObjectField()));
                    }
                }
            }
        }
        return responseMap;
    }
    
    /**
    * @description Set defaut value for a field if "OVERRIDE" keyword is found within Field MApping based on logic defined.
    *
    * @param nameSpacePrifix - String: namespaceprefic for the object and field if called within package.
    * @param sObjectName - String: name of the Object.
    * @param parentRecordId - String, parentId if loaded from a related List.
    * @param response - Map<String, Object>, field mapping along with the value to be used.
    */
    public static void addOverrideFields(String nameSpacePrifix, String sObjectName, String parentRecordId, Map<String, Object> response){
        String parentObjectName = Id.valueOf(parentRecordId).getSobjectType().getDescribe().getName();
        System.debug('parentObjectName: '+parentObjectName);

        if(sObjectName == nameSpacePrifix+'Assembly_MR_Rate__c' && parentObjectName == nameSpacePrifix+'Lease__c'){
            List<Lease__c> leaseList = [SELECT ID, Aircraft__c FROM Lease__c WHERE Id =: parentRecordId];
            if(!leaseList.isEmpty() && leaseList[0].Aircraft__c != null){
                response.put(nameSpacePrifix+'Y_Hidden_Asset__c', leaseList[0].Aircraft__c);
            }
        }
        else if(sObjectName == nameSpacePrifix+'Supp_Rent_Event_Reqmnt__c' && parentObjectName == nameSpacePrifix+'Assembly_MR_Rate__c'){
            List<Assembly_MR_Rate__c> listSuppRent= [select id,Assembly_Event_Info__r.Maintenance_Program_Event__c from Assembly_MR_Rate__c where ID =: parentRecordId];
            if(!listSuppRent.isEmpty() && listSuppRent[0].Assembly_Event_Info__r.Maintenance_Program_Event__c != null){
                response.put(nameSpacePrifix+'Y_Hidden_MPE_LKP__c', listSuppRent[0].Assembly_Event_Info__r.Maintenance_Program_Event__c);
            }
        }
        else if(sObjectName == nameSpacePrifix+'Payable__c' && parentObjectName == nameSpacePrifix+'Cash_Security_Deposit__c'){
            list<Cash_Security_Deposit__c> listSd = [select id,lease__c,lease__r.lessee__c,lease__r.operator__c,lease__r.lessor__c from Cash_Security_Deposit__c where id = :parentRecordId];
            if(!listSd.isEmpty()){
                Id lessee = listSd[0].lease__r.lessee__c !=null?listSd[0].lease__r.lessee__c:listSd[0].lease__r.operator__c;
                response.put(nameSpacePrifix+'Cash_Security_Deposit__c',listSd[0].id);
                response.put(nameSpacePrifix+'Lease__c',listSd[0].lease__c);
                response.put(nameSpacePrifix+'Lessee__c',lessee);
                response.put(nameSpacePrifix+'Company__c',listSd[0].lease__r.lessor__c);
            }
        }
		else if(sObjectName == nameSpacePrifix+'Credit_Memo_Issuance__c' && parentObjectName == nameSpacePrifix+'Credit_Memo__c'){
            list<Credit_Memo__c> listCM = [select id,Lessee__c from Credit_Memo__c where id = :parentRecordId];
            if(!listCM.isEmpty()&& listCM[0].Lessee__c != null){
             response.put(nameSpacePrifix+'Lessee__c',listCM[0].Lessee__c);               
            }
        }
        else if(sObjectName == nameSpacePrifix+'Insurance_Cert__c' && parentObjectName == nameSpacePrifix+'Lease__c'){
            List<Lease__c> listLease= [select id, Aircraft__c from Lease__c where ID =: parentRecordId];
            if(!listLease.isEmpty() && listLease[0].Aircraft__c!= null){
                response.put(nameSpacePrifix+'Asset__c', listLease[0].Aircraft__c);
            }
        }
		else if(sObjectName == nameSpacePrifix+'Insurance_Cert__c' && parentObjectName == nameSpacePrifix+'Aircraft__c'){
            List<Aircraft__c> listAc= [select id,Lease__c from Aircraft__c where id =: parentRecordId];
            if(!listAc.isEmpty() && listAc[0].Lease__c!= null){
                response.put(nameSpacePrifix+'Lease__c', listAc[0].Lease__c);
            }
        }
    }
}