@isTest
public class TestCreditMemoIssuanceTriggerHandler {

        
    @testSetup
     static void CRUD_creditMemoIssuanceRecord(){
         
        Custom_Lookup__c clookup = new Custom_Lookup__c();
        clookup.name ='Default';
        clookup.Lookup_Type__c = 'Engine';
        LeasewareUtils.clearFromTrigger();insert clookup;
		
        Parent_Accounting_Setup__c accountingSetup = new Parent_Accounting_Setup__c(Name='TestAS');
            LeasewareUtils.clearFromTrigger();insert accountingSetup;
            
        Operator__c operator1 = new Operator__c(Name='TestThisAirline1', Status__c='Approved');
            LeasewareUtils.clearFromTrigger();insert operator1;
        Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
            LeasewareUtils.clearFromTrigger();insert legalEntity;
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Test',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Maximum_Operational_Take_Lbs__c= 144455,   
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
        TSN__c=1000,
        CSN__c=1000,
        Contractual_Status__c = 'Managed'    
        );
        LeasewareUtils.clearFromTrigger();insert aircraft;
         
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name',Attached_Aircraft__c= aircraft.Id,Current_TS__c=clookup.id,
                                                                             TSN__c=10, CSN__c=10,Type__c ='Airframe', Serial_Number__c='123');
        LeaseWareUtils.clearFromTrigger();
		LeasewareUtils.clearFromTrigger();insert consttAssembly;
        
         Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        LeasewareUtils.clearFromTrigger();insert mp;
        
         
         Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
			Interval_2_Hours__c=20,
            Event_Type_Global__c = '4C/6Y'
        );
        LeasewareUtils.clearFromTrigger();insert mpe;
         
        
        Lease__c lease1 = new Lease__c(Name='TestLease1',Aircraft__c=aircraft.id , Lease_End_Date_New__c=Date.today()+20,Accounting_Setup_P__c =accountingSetup.id, Lease_Start_Date_New__c=Date.today()-10);
        LeaseWareUtils.clearFromTrigger();insert lease1;
        
        Assembly_Event_Info__c eventAF = new Assembly_Event_Info__c();
        eventAF.Name = 'Test Info';
        eventAF.Event_Cost__c = 20;
        eventAF.Constituent_Assembly__c = consttAssembly.Id;
        eventAF.Maintenance_Program_Event__c = mpe.Id;
        insert eventAF;
        Assembly_MR_Rate__c assemblyRecord = new Assembly_MR_Rate__c(
            Name='Testing',
            Assembly_Lkp__c =consttAssembly.Id,
            Base_MRR__c = 200,
            Lease__c = lease1.Id,
			Rate_Basis__c='Hours',
           Assembly_Event_Info__c= eventAF.id
        );
        LeasewareUtils.clearFromTrigger();insert assemblyRecord;
         
        Assembly_MR_Rate__c AssemblyMRInfo1 = [select id,Name from Assembly_MR_Rate__c where Lease__c=:lease1.id];       
        
        Calendar_Period__c cp = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1),closed__c=true);
        LeasewareUtils.clearFromTrigger(); insert cp;
		 
        Chart_of_accounts__c coa1 = new Chart_of_Accounts__c(name='Lessor COA',GL_Code__c='0001',GL_Account_Description__c='Credit Memo RR');
        Chart_of_accounts__c coa2 = new Chart_of_Accounts__c(name='Lessor COA',GL_Code__c='0002',GL_Account_Description__c='Credit Memo Unapplied');
        list<Chart_of_accounts__c> coalist = new list<Chart_of_accounts__c> {coa1,coa2};
        LeasewareUtils.clearFromTrigger();insert coalist;
            
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                              Phone_Number__c='2342', Email_Address__c='a@lease.com',Accounting_enabled__c = true,CM_Rev_Reduction__c = coa1.id,Unapplied_cm__c=coa2.id); 
        LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
		
        credit_memo__c creditMemo = new credit_memo__c(amount__c = 20000.00,
                           Credit_Memo_Date__c=system.today(),company__c =legalEntity.id,
                           lessee__c = operator1.id);
        LeasewareUtils.clearFromTrigger();insert creditMemo;
        Credit_Memo_Issuance__c cmIssuance = new Credit_Memo_Issuance__c(amount__c =10000.00,Credit_Memo__c=creditMemo.id,
                                                                        Supplemental_Rent__c=AssemblyMRInfo1.Id);    	
        
        LeaseWareUtils.clearFromTrigger(); insert cmIssuance;      
         
        //Read
        Credit_Memo_Issuance__c Rec = [select id,amount__c from Credit_Memo_Issuance__c  where id =:cmIssuance.id];
        
        //Update
        Rec.amount__c=15000;
        LeaseWareUtils.clearFromTrigger(); 
        Update Rec;

        //Delete
        LeaseWareUtils.clearFromTrigger(); 
        delete Rec;            
 
    }
    
    @isTest
    static void validateAmount() {
            Test.startTest();   
            List<Credit_Memo_Issuance__c> issuanceList =new List<Credit_Memo_Issuance__c>();
            List<Spec_Mass_and_Balance__c> massList1 =new List<Spec_Mass_and_Balance__c>();
            credit_memo__c cmRec = [select id from credit_memo__c];
            Assembly_MR_Rate__c srec =[select id,Name from Assembly_MR_Rate__c]; 
           //Create  Adjusted Interval(Ratio) 
            String expectedError = 'Cannot save Credit Memo Issuance with this Amount, sum of all Credit Memo Issuance';
            Credit_Memo_Issuance__c Rec1=new Credit_Memo_Issuance__c(amount__c=15000,Credit_Memo__c=cmRec.id,
                                                                        Supplemental_Rent__c=srec.Id);
            Credit_Memo_Issuance__c Rec2=new Credit_Memo_Issuance__c(amount__c=25000,Credit_Memo__c=cmRec.id,
                                                                        Supplemental_Rent__c=srec.Id);
           issuanceList.add(Rec1);
           issuanceList.add(Rec2); 
           Database.SaveResult[] srList = Database.insert(issuanceList, false);
           // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
          if (sr.isSuccess()) {
            System.debug('Successfully inserted record: ' + sr.getId());
            }
          else {                       
            for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.assert(err.getMessage().contains(expectedError));
              }
            }                    
        }
            List<Credit_Memo_Issuance__c> curRec = [select id,amount__c from Credit_Memo_Issuance__c ];
            System.assertEquals(1, curRec.size(), 'Number of records is not as expected');
         Test.stopTest();
        } 
    
}