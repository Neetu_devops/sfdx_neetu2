@isTest
public class TestAssetDealAnalysisController {

    @isTest
    public static void loadTestCases() {
        
        
        Date myDate1 = Date.newInstance(2010, 2, 17);
        Aircraft__c aircraft = new Aircraft__c(Name= '1123 Test', MSN_Number__c = '1230', Aircraft_Type__c = 'A320', Aircraft_Variant__c = '100', 
                                               Date_of_Manufacture__c = myDate1);
        
        LeaseWareUtils.clearFromTrigger();
        insert aircraft;
        
        Aircraft__c aircraft1 = new Aircraft__c(Name= '1123 Test1', MSN_Number__c = '1231', Aircraft_Type__c = 'A320', Aircraft_Variant__c = '100', 
                                               Date_of_Manufacture__c = myDate1);
        
        LeaseWareUtils.clearFromTrigger();
        insert aircraft1;
        
        Operator__c opr = new Operator__c(Name = 'testOpr', Country__c = 'United States', Region__c = 'Asia');
        insert opr;
        
        Lease__c lease1 = new Lease__c(Reserve_Type__c= 'Maintenance Reserves', Base_Rent__c = 21, Lease_Start_Date_New__c = System.today()- 400, 
                                       Lease_End_Date_New__c = System.today(), Rent__c = 454546, Operator__c = opr.id, Aircraft__c = aircraft1.Id);
        insert lease1;
        
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =lease1.Operator__c, Deal_Type__c='Purchase' ,
                                                            Deal_Status__c = 'Pipeline',Marketing_Rep_c__c = 'N/A',Description__c='111',
                                                            Asset_Type__c = 'Aircraft');
        LeaseWareUtils.clearFromTrigger();
        insert m1;
        
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            Aircraft__c= aircraft.Id, Asset_Type__c  = 'A320-100',
                                                            Date_of_Manufacture__c = myDate1);
        LeaseWareUtils.clearFromTrigger();
        insert AT1;
		Test.startTest();        
        Map<String, List<DealAnalysisData.Data>> data = AssetDealAnalysisController.getDataSources(30,null,'Portfolio Acquisitions;Closed Deals;All Deals;Current Leases;Market Intel;World Fleet','100','A320',2010,AT1.Id, 20, 'Aircraft');
        if(data != null) {
            system.assertEquals(1,data.get('0 - 48')[3].Recs.size());
        }
        
        testGeneralMethods(AT1);
        testPicklist();
        Test.stopTest();
        testPresetMethod(AT1, m1,lease1, aircraft1);
    }
    
    public static void testGeneralMethods(Aircraft_Proposal__c AT1) {
        
        List<String> picklist = AssetDealAnalysisController.getPicklist();
        system.assertEquals(3, picklist.size());
        
        List<String> picklistGrade = AssetDealAnalysisController.getGradePicklist();
        system.assertEquals(13, picklistGrade.size());
        
        List<String> picklistSource = AssetDealAnalysisController.getSourcePicklist();
        system.assertEquals(6, picklistSource.size());
        
        Boolean pageType = AssetDealAnalysisController.getPageType(AT1.Id,'Aircraft_Proposal__c');
        system.assertEquals(true, pageType);
        
        String recordType = AssetDealAnalysisController.fetchRecordType('A320');
        system.assertEquals('Aircraft', recordType);
        
        List<String> picklistOptions = AssetDealAnalysisController.getPickListOptions('Aircraft__c','Aircraft_Type__c');
        system.assertNotEquals(null,picklistOptions);
    }

    public static void testPicklist() {
        
        String namespace = LeaseWareUtils.getNamespacePrefix();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(namespace+'Aircraft__c'); 
        SObject myObj = targetType.newSObject();
		Map<String, List<String>> mapData = AssetDealAnalysisController.getDependentMap(myObj, 'Aircraft_Type__c', 'Engine_Type__c');
        system.assertNotEquals(null, mapData);
        
        String resBody = '{"controllerValues":{},"defaultValue":null,"url":"/services/data/v41.0/ui-api/object-info/'+namespace+'Aircraft__c/picklist-values/0123h000000Edn5AAC/'+namespace+'Aircraft_Type__c","values":[{"attributes":null,"label":"A320","validFor":[],"value":"A320"},{"attributes":null,"label":"BR700","validFor":[],"value":"BR700"}]}';
        Map<String,String> responseHeader = new Map<String,String>{ 'Authorization' => 'OAuth '+UserInfo.getSessionId() };            
            
        TestSingleRequestMock fakeResponse = new TestSingleRequestMock(200,resBody,responseHeader);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Map<String, List<String>> mapRecord = AssetDealAnalysisController.getPicklistForRecords();
        system.assertNotEquals(null, mapRecord);
       
    }
    
    public static void testPresetMethod(Aircraft_Proposal__c AT1, Marketing_Activity__c m1, Lease__c lease1, Aircraft__c aircraft) {
        AssetDealAnalysisController.PresetData aidData = AssetDealAnalysisController.getPresetValues(AT1.Id, 'Aircraft_Proposal__c');
        system.assertEquals('A320', aidData.AircraftType);
        
        AssetDealAnalysisController.PresetData astData = AssetDealAnalysisController.getPresetValues(aircraft.Id, 'Aircraft__c');
        system.assertEquals('100', astData.Variant);
        
        AssetDealAnalysisController.PresetData leaseData = AssetDealAnalysisController.getPresetValues(lease1.Id, 'Lease__c');
        system.assertEquals('Aircraft', leaseData.RecordType);
        
        AssetDealAnalysisController.PresetData pricingData = AssetDealAnalysisController.getPresetValues(lease1.Id, 'Pricing_Output_New__c');
        system.assertEquals(null, pricingData.FinancialRisk);
        
        //=================================================
        Date myDate1 = Date.newInstance(2010, 2, 17);
        Global_Fleet__c gf = new Global_Fleet__c(Name = 'testWorldFleet', Aircraft_External_ID__c = 'type-msn', BuildYear__c = '2010',
                                                 AircraftType__c = 'A320',AircraftVariant__c = '400');
        insert gf;
        Aircraft_Proposal__c AT2 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, 
                                                            World_Fleet_Aircraft__c = gf.Id, Asset_Type__c  = 'A320-100',
                                                            Date_of_Manufacture__c = myDate1);
        LeaseWareUtils.clearFromTrigger();
        insert AT2;
        AssetDealAnalysisController.PresetData aidData2 = AssetDealAnalysisController.getPresetValues(AT2.Id, 'Aircraft_Proposal__c');
        system.assertEquals('400', aidData2.Variant);
        
        //=================================================
        Marketing_Activity__c m2= new Marketing_Activity__c(Name='Dummy',Prospect__c =lease1.Operator__c,
                                                            Deal_Type__c='Purchase' ,Deal_Status__c = 'Pipeline',
                                                            Marketing_Rep_c__c = 'N/A',Description__c='111', 
                                                            Asset_Type__c = 'Engine');
		insert m2;        
        Aircraft_Proposal__c AT3 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m2.Id, 
                                                            Aircraft__c = aircraft.Id, Asset_Type__c  = 'A320-100',Engine_Type_new__c = 'CFM56',
                                                            Date_of_Manufacture__c = myDate1);
        LeaseWareUtils.clearFromTrigger();
        insert AT3;
        
        AssetDealAnalysisController.PresetData aidData3 = AssetDealAnalysisController.getPresetValues(AT3.Id, 'Aircraft_Proposal__c');
        system.assertEquals('CFM56', aidData3.AircraftType);
            
        AssetDealAnalysisController.PresetData miData = AssetDealAnalysisController.getPresetValues(m1.Id, 'Marketing_Activity__c');
        system.assertEquals('100', miData.Variant);
        
        AssetDealAnalysisController.PresetData miData2 = AssetDealAnalysisController.getPresetValues(m2.Id, 'Marketing_Activity__c');
        system.assertEquals(null, miData2.Variant);
                
        pricingObject(AT2, AT3);
        
        AT3.Engine_Type_new__c = null;
        update AT3;
        
        AssetDealAnalysisController.PresetData aidData4 = AssetDealAnalysisController.getPresetValues(AT3.Id, 'Aircraft_Proposal__c');
        system.assertEquals('Engine', aidData4.RecordType);
        
        AssetDealAnalysisController.PresetData miData3 = AssetDealAnalysisController.getPresetValues(m2.Id, 'Marketing_Activity__c');
        system.assertEquals('Engine', miData3.RecordType);
        
        pricingObject(AT1, AT3);
    }
    
    public static void pricingObject(Aircraft_Proposal__c aidAircraft, Aircraft_Proposal__c aidEngine){
        
        Pricing_Run_New__c CT = new Pricing_Run_New__c( Name = 'test', Current_Rent__c = 676, EBT__c= 909, IRR__c = 7,
                                                       Lease_Rate_Factor__c = 56, Purchase_Price_For_SLBs_Only__c = 45,Rate_Type__c = 'Fixed/Fixed');
        insert CT;
        
        Pricing_Output_New__c ET = new Pricing_Output_New__c(Name = 'Test', Asset_Term__c = aidEngine.Id, Pricing_Run__c = CT.Id);
        insert ET;
        
        AssetDealAnalysisController.PresetData prcData = AssetDealAnalysisController.getPresetValues(CT.Id, 'Pricing_Run_New__c');
        system.assertEquals(null, prcData.FinancialRisk);
        
        Pricing_Run_New__c CT1 = new Pricing_Run_New__c( Name = 'test1', Current_Rent__c = 676, EBT__c= 909, IRR__c = 7,
                                                       Lease_Rate_Factor__c = 56, Purchase_Price_For_SLBs_Only__c = 45,Rate_Type__c = 'Fixed/Fixed');
        insert CT1;
        Pricing_Output_New__c ET1 = new Pricing_Output_New__c(Name = 'Test', Asset_Term__c = aidAircraft.Id, Pricing_Run__c = CT1.Id);
        insert ET1;
        
        AssetDealAnalysisController.PresetData prcData2 = AssetDealAnalysisController.getPresetValues(CT1.Id, 'Pricing_Run_New__c');
        system.assertEquals(null, prcData2.FinancialRisk);
        
    }
    
    public class TestSingleRequestMock implements HttpCalloutMock {
        protected Integer code;
        protected String bodyAsString;
        protected Map<String, String> responseHeaders;
        
        public TestSingleRequestMock(Integer code, String body, Map<String, String> responseHeaders) {
            this.code = code;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setBody(bodyAsString);
            
            if (responseHeaders != null) {
                for (String key : responseHeaders.keySet()) {
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            return resp;
        }
    }
}