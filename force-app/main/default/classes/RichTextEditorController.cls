/**
***********************************************************************************************************************
* Class: RichTextEditorController
* @author Created by Aman, Lease-Works, 
* @date 17/08/2020
* @version 1.0
* ---------------------------------------------------------------------------------------------------------------------
* Purpose/Methods:
* Contains all custom business logic to read the Email Template body and create public link for the image files if included in the long Text area field.
*
* History:
* VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
* 1.0       A.K.G                 17.08.2020      Get & Set Field Value [Initial Code]
***********************************************************************************************************************
*/
public class RichTextEditorController {
    /**
    * @description get the field value to display the content on the UI.
    *
    * @param recordId - String, contains record Id.
    * @param fieldAPIName - String, API name of the field(longtextarea) to be used to read the data from.
    *
    * @return RichTextEditorWrapper - wrapper containg all the necessary information to display the content on UI.
    */
    @AuraEnabled
    public static RichTextEditorWrapper getFieldData (String recordId, String fieldAPIName) {
        try{
            if (recordId != null && recordId != '' && fieldAPIName != null && fieldAPIName != '') {
                return new RichTextEditorWrapper(recordId, fieldAPIName);
            }
        }
        catch (Exception ex) { throw new AuraHandledException(ex.getMessage()); }
        return null;
    }
	
    /**
    * @description Save the data in the field as per the modification done by the User.
    *
    * @param recordId - String, contains record Id.
    * @param fieldAPIName - String, API name of the field(longtextarea) to be used to read the data from.
    * @param fieldValue - String, Updated field value to be stored on the record.
    * @param filesToDelete - String, Set of ContentVersion Id's that are needed to be removed from the record.
    *
    * @return RichTextEditorWrapper - wrapper containg all the necessary information to display the content on UI.
    */
    @AuraEnabled
    public static RichTextEditorWrapper saveFieldData (String recordId, String fieldAPIName, String fieldValue, String filesToDelete) {
        try {
            RichTextEditorWrapper rteWrap = new RichTextEditorWrapper(recordId, fieldAPIName);
            rteWrap.recordData.put(fieldAPIName, fieldValue);
            rteWrap.fieldValue = fieldValue;

            update rteWrap.recordData;

            if (filesToDelete != null && filesToDelete != '') {
                Set<String> filesToDeleteSet = (Set<String>) JSON.deserializeStrict(filesToDelete, Set<String>.class);
                Set<Id> cdIds = new Set<Id>();
                if (!filesToDeleteSet.isEmpty()) {
                    for (ContentVersion cVersion : [SELECT Id, ContentDocumentId FROM Contentversion WHERE Id IN: filesToDeleteSet]) {
                        cdIds.add(cVersion.ContentDocumentId);
                    }
                }
                if (!cdIds.isEmpty()) {
                    delete [SELECT Id FROM ContentDocument WHERE Id IN: cdIds];
                }
            }
            return rteWrap;
        }
        catch (Exception ex) { throw new AuraHandledException(ex.getMessage()); }
    }

    /**
    * @description Delete the files if user does not save the chnages he did on the field.
    *
    * @param filesToDelete - String, Set of ContentVersion Id's that are needed to be removed from the record.
    *
    */
    @AuraEnabled
    public static void deleteFiles (String filesToDelete) {
        try {
            if (filesToDelete != null && filesToDelete != '') {
                Set<String> filesToDeleteSet = (Set<String>) JSON.deserializeStrict(filesToDelete, Set<String>.class);
                Set<Id> cdIds = new Set<Id>();
                if (!filesToDeleteSet.isEmpty()) {
                    for (ContentVersion cVersion : [SELECT Id, ContentDocumentId FROM Contentversion WHERE Id IN: filesToDeleteSet]) {
                        cdIds.add(cVersion.ContentDocumentId);
                    }
                }
                if (!cdIds.isEmpty()) {
                    delete [SELECT Id FROM ContentDocument WHERE Id IN: cdIds];
                }
            }
        }
        catch (Exception ex) { throw new AuraHandledException(ex.getMessage()); }
    }

    /**
    * @description RichTextEditorWrapper to hold data related to Field and rendering of Field Data
    */
    @TestVisible
    private class RichTextEditorWrapper{
        @AuraEnabled public Id recordId { get; set; }
        @AuraEnabled public Boolean fieldExists { get; set; }
        @AuraEnabled public String fieldAPIName { get; set; }
        @AuraEnabled public Boolean isfieldAccessible { get; set; }
        @AuraEnabled public Boolean isfieldUpdatable { get; set; }
        @AuraEnabled public Integer fieldLength { get; set; }
        @AuraEnabled public String fieldValue { get; set; }
        @AuraEnabled public SObject recordData { get; set; }

        public RichTextEditorWrapper(Id recordId, String fieldAPIName) {
            this.recordId = recordId;
            this.fieldExists = false;
            this.fieldAPIName = fieldAPIName;
            this.isfieldAccessible = false;
            this.isfieldUpdatable = false;
            this.fieldLength = 0;
            this.fieldValue = '';

            String objectName = Id.valueOf(recordId).getSobjectType().getDescribe().getName();
            if (objectName != null) {
                Map<String, SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
                for (String fieldName : fieldsMap.keySet()) {
                    if (fieldName == fieldAPIName) { 
                        this.fieldExists = true; 
                        this.fieldLength = fieldsMap.get(fieldName).getDescribe().getLength();
                        this.isfieldAccessible = fieldsMap.get(fieldName).getDescribe().isAccessible();
                        this.isfieldUpdatable = fieldsMap.get(fieldName).getDescribe().isUpdateable();
                        break;
                    }
                }
                if (this.fieldExists) {
                    this.recordData = (SObject) Database.query('SELECT Id, '+fieldAPIName+' FROM '+ objectName +' WHERE Id = \''+recordId+'\'');
                    if (recordData != null && recordData.get(fieldAPIName) != null) {
                        this.fieldValue = (String) recordData.get(fieldAPIName);
                    }
                }
            }
        }
    }
}