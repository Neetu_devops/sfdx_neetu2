public class CounterpartyTrgHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final  string triggerBefore = 'CounterpartyTrgHandlerBefore';
    private final  string triggerAfter = 'CounterpartyTrgHandlerAfter';
    private map<Id, String> mapCountry = new map<Id, String>();
    public CounterpartyTrgHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        
        if(Trigger.isDelete)return;
            
                Set<Id> countryIds = new Set<Id>();
                for(Counterparty__c lessor : (list<Counterparty__c>)trigger.new){
                    if(lessor.Country_Lookup__c!=null){
                        countryIds.add(lessor.Country_Lookup__c);
                    }
                    
                    if(lessor.Country_Of_Registration_Lookup__c!=null){
                        countryIds.add(lessor.Country_Of_Registration_Lookup__c);
                    }
                    
                }
                
                for(Country__c cou:  [select Id,Name from Country__c where ID IN: countryIds]){
                    mapCountry.put(cou.Id,cou.Name);
                }
            
       
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {

		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('CounterpartyTrgHandler.beforeInsert(+)');
        updateSearchableFieldOfCountry(trigger.new);
	    system.debug('CounterpartyTrgHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('OperatorHandler.beforeUpdate(+)');
    	
        List<Counterparty__c> lessorListToUpdateTextCountry = new List<Counterparty__c>();
        Counterparty__c[] listNew = (list<Counterparty__c>)trigger.new ;
        Counterparty__c oldRec;
        for(Counterparty__c curRec : listNew){
            oldRec = (Counterparty__c)trigger.oldMap.get(curRec.id);
            if(curRec.Country_Lookup__c != oldRec.Country_Lookup__c  
                || curRec.Country_Of_Registration_Lookup__c != oldRec.Country_Of_Registration_Lookup__c){
                    lessorListToUpdateTextCountry.add(curRec);
                }
        }
        if(lessorListToUpdateTextCountry.size()>0){
            updateSearchableFieldOfCountry(lessorListToUpdateTextCountry);
        }
    	
    	system.debug('OperatorHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
    	system.debug('CounterpartyTrgHandler.beforeDelete(+)');
    	
    	
    	system.debug('CounterpartyTrgHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CounterpartyTrgHandler.afterInsert(+)');
        createAccount();
        
    	system.debug('CounterpartyTrgHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
        system.debug('CounterpartyTrgHandler.afterUpdate(+)');
        
    	system.debug('CounterpartyTrgHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	system.debug('CounterpartyTrgHandler.afterDelete(+)');
    	
    	system.debug('CounterpartyTrgHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	system.debug('CounterpartyTrgHandler.afterUnDelete(+)');
    	
		system.assert(false); 
    	
    	system.debug('CounterpartyTrgHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

    private void createAccount(){
        if( 'ON'.equals(LeaseWareUtils.getLWSetup_CS('Auto_Creation_of_Accounts'))){
            list<Counterparty__c> listNew =  (list<Counterparty__c>)trigger.new;
            List<String> lessorNameList = new List<String>();
            for(Counterparty__c opr: listNew){
                lessorNameList.add(opr.Name);
            }
            LeasewareUtils.createAccount(lessorNameList, 'Lessor');
        }
    }
    
    private void updateSearchableFieldOfCountry(List<Counterparty__c> lessorListToUpdate){
        String countryName,countryOfRegName,textFieldCountry;
        for(Counterparty__c lessor : lessorListToUpdate){
            countryName = lessor.Country_Lookup__c==null?'':mapCountry.containsKey(lessor.Country_Lookup__c)?mapCountry.get(lessor.Country_Lookup__c):'';
            countryOfRegName = lessor.Country_Of_Registration_Lookup__c==null?'':mapCountry.containsKey(lessor.Country_Of_Registration_Lookup__c)?mapCountry.get(lessor.Country_Of_Registration_Lookup__c):'';
            system.debug('countryName----'+countryName);
            system.debug('countryOfRegName----'+countryOfRegName);
            textFieldCountry = countryName +','+ countryOfRegName ;
            if(textFieldCountry!=null && textFieldCountry.length()>255) lessor.Y_Hidden_Country__c = textFieldCountry.left(251) + '...'; 
            else   lessor.Y_Hidden_Country__c = textFieldCountry ;

            system.debug('Hidden Country---'+ lessor.Y_Hidden_Country__c);
        }
        
        

    }
}