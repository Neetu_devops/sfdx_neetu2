public class FloatingRentInputTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'FloatingRentInputTriggerHandlerBefore';
    private final string triggerAfter = 'FloatingRentInputTriggerHandlerAfter';
    public FloatingRentInputTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('FloatingRentInputTriggerHandler.beforeInsert(+)');

		befInsUpd();

	    system.debug('FloatingRentInputTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('FloatingRentInputTriggerHandler.beforeUpdate(+)');

 		befInsUpd();
    	
    	system.debug('FloatingRentInputTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('FloatingRentInputTriggerHandler.beforeDelete(+)');
    	
    	
    	system.debug('FloatingRentInputTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FloatingRentInputTriggerHandler.afterInsert(+)');

    	system.debug('FloatingRentInputTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FloatingRentInputTriggerHandler.afterUpdate(+)');
    	
    	
    	system.debug('FloatingRentInputTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FloatingRentInputTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('FloatingRentInputTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FloatingRentInputTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('FloatingRentInputTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	//bef ins,bef upd
	private void befInsUpd(){
		
		Floating_Rent_Inputs__c[] listnew = (list<Floating_Rent_Inputs__c>)trigger.new;
		set<string> setleaseids = new set<string>();
		
		Floating_Rent_Inputs__c oldrec;
		for(Floating_Rent_Inputs__c curRec:listnew){
			curRec.Approximate_Reset_Date__c =	curRec.Y_Hidden_Rent_Start_Date__c.addMonths(Integer.valueOf(curRec.From_Period__c) -1);
			if(trigger.IsInsert) setleaseids.add(curRec.Lease__c);
			else if(trigger.IsUpdate){
				oldrec = (Floating_Rent_Inputs__c)trigger.oldmap.get(curRec.Id);
				if(curRec.From_Period__c!=oldrec.From_Period__c || curRec.To_Period__c!=oldrec.To_Period__c) setleaseids.add(curRec.Lease__c);
			}
		}
		
		if(setleaseids.size()>0){
			map<id,set<Decimal>> mapLeaseToFloatingPeriod = new map<id,set<Decimal>>();
			map<id,Lease__c> mapExistingFloatingRentInpt = new map<id,Lease__c>([
						select id 
							,(select id,From_Period__c,To_Period__c 
								from Floating_Rent_Inputs__r where id not in :listnew 
								order by From_Period__c)
						from Lease__c
						where id in :setleaseids
						
						]);
						
			set<Decimal> setDecimalTemp ;			
			for(Lease__c curLease:mapExistingFloatingRentInpt.values()){
				setDecimalTemp = new set<Decimal>();
				for(Floating_Rent_Inputs__c curChild:curLease.Floating_Rent_Inputs__r){
					for(Decimal i=curChild.From_Period__c;i<=curChild.To_Period__c;i++){
						setDecimalTemp.add(i);
					}
				}
				mapLeaseToFloatingPeriod.put(curLease.Id,setDecimalTemp);
			}	
			
			boolean impactedFlag;
			for(Floating_Rent_Inputs__c curRec:listnew){
				impactedFlag = false;
				if(trigger.IsInsert) impactedFlag=true;
				else if(trigger.IsUpdate){
					oldrec = (Floating_Rent_Inputs__c)trigger.oldmap.get(curRec.Id);
					if(curRec.From_Period__c!=oldrec.From_Period__c || curRec.To_Period__c!=oldrec.To_Period__c) impactedFlag=true;
				}
				
				if(impactedFlag){
					setDecimalTemp = mapLeaseToFloatingPeriod.get(curRec.Lease__c);
					for(Decimal i=curRec.From_Period__c;i<=curRec.To_Period__c;i++){
						if(setDecimalTemp.contains(i)) curRec.addError('This period is already part of Floating rent Inputs. Please verify. Name:' + curRec.name + ' , Issue Period:'+i);
						else setDecimalTemp.add(i);
					}
				}
			}

					
		}
		
	}
	


}