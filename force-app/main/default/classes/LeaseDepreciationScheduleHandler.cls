public class LeaseDepreciationScheduleHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string  triggerBefore = 'LeaseDepreciationScheduleHandlerBefore';
    private final string  triggerAfter = 'LeaseDepreciationScheduleHandlerAfter';
    public LeaseDepreciationScheduleHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('LeaseDepreciationScheduleHandler.beforeInsert(+)');
    	Lease_Depreciation_Schedule__c[] listNew = (list<Lease_Depreciation_Schedule__c>)trigger.new ;
    	for(Lease_Depreciation_Schedule__c curRec:listNew){
    		if(!LeaseWareUtils.isFromTrigger('InsertedFromAircraftTriggerHandler')) {
    			curRec.addError('You are not allowed to create Lease Depreciation Schedule records.');
    		}
			
    	}    	
    	
		
	    system.debug('LeaseDepreciationScheduleHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('LeaseDepreciationScheduleHandler.beforeUpdate(+)');
    	Lease_Depreciation_Schedule__c[] listNew = (list<Lease_Depreciation_Schedule__c>)trigger.new ;
    	for(Lease_Depreciation_Schedule__c curRec:listNew){
    			curRec.addError('You are not allowed to modify Lease Depreciation Schedule records.');
    	}     	
    	
    	system.debug('LeaseDepreciationScheduleHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('LeaseDepreciationScheduleHandler.beforeDelete(+)');
    	Lease_Depreciation_Schedule__c[] listNew = (list<Lease_Depreciation_Schedule__c>)trigger.old ;
    	for(Lease_Depreciation_Schedule__c curRec:listNew){
    		if(!LeaseWareUtils.isFromTrigger('DeletedFromAircraftTriggerHandler')) {
    			curRec.addError('You are not allowed to delete Lease Depreciation Schedule records.');
    		}
			
    	}      	
    	
    	system.debug('LeaseDepreciationScheduleHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LeaseDepreciationScheduleHandler.afterInsert(+)');
    	
    	system.debug('LeaseDepreciationScheduleHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LeaseDepreciationScheduleHandler.afterUpdate(+)');
    	
    	
    	system.debug('LeaseDepreciationScheduleHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LeaseDepreciationScheduleHandler.afterDelete(+)');
    	
    	
    	system.debug('LeaseDepreciationScheduleHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LeaseDepreciationScheduleHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('LeaseDepreciationScheduleHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


}