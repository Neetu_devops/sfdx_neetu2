public with sharing class AircraftController2 {

    private final Aircraft__c parentAC;
    private List<Aircraft_Eligible_Event__c> ACEligible_Event= new List<Aircraft_Eligible_Event__c>();
    
    private List<Equipment__c> Equipment = new List<Equipment__c>();
    
    public List<Constituent_Assembly__c> ConstAssemb = new List<Constituent_Assembly__c>();
    public List<Constituent_Assembly__c> ConstAssembEngine = new List<Constituent_Assembly__c>();
    public List<Constituent_Assembly__c> ConstAssembGear = new List<Constituent_Assembly__c>();
    public List<Constituent_Assembly__c> ConstAssembApu = new List<Constituent_Assembly__c>();
     
   
    private final Lease__c Lease;
    private final Operator__c Operator;
    
    private ID parentAC_ID; 
    private ID ACEligible_Event_ID;
    private ID Lease_ID;
    private ID Lessor_ID;
    private ID OperatorID;
    
    public string Logo_URL;
    public String email {get;set;}
    public String sub {get;set;}
    public String ebody {get;set;}
     
    public AircraftController2 () {
    
        Id id = ApexPages.currentPage().getParameters().get('id');
        
        Logo_URL =  LeaseWareUtils.getLogoURL();
        
        
       
  
              
        List<Aircraft__c>   Ac_spec = [select id,Logo__c, Aircraft_Type__c, Assigned_To__c, Country_Of_Registration__c, 
                                                  CSN__c, Lease__c,Approach_Landing_Cat__c, 
                                                  Number_of_engines__c, Registration_Number__c,ETOPS__c,Line_Num__c, 
                                                  Time_Remaining_To_Check__c,TSN__c, Aircraft_Variant__c,Date_of_Manufacture__c,
                                                  Auxiliary_Tank_Capacity_Gallons__c, Auxiliary_Tank_Capacity_Kilograms__c, 
                                                  Auxillary_Tank_Capacity_Pounds__c, Maximum_Landing_Weight_Max__c,
                                                  Maximum_Landing_Weight_Operational__c, Maximum_Landing_Wt_Purchased_Leased__c,
                                                  Maximum_Operational_Take__c, Maximum_Take_Off_Wt_Max_Design__c,
                                                  Maximum_Take_Off_Weight_Operational__c, Maximum_Zero_Fuel_Wt_Max_Design__c,
                                                  Maximum_Zero_Fuel_Wt_Operational__c, Maximum_Zero_Fuel_Wt_Purchased_Leased__c,
                                                  Standard_Fuel_Capacity_Gallons__c, Standard_Fuel_Capacity_KG__c, 
                                                  Standard_Fuel_Capacity_Pounds__c 
                                             from Aircraft__c where id =  :ApexPages.currentPage().getParameters().get('id')];
                                                                                                          
       if (Ac_spec.size() > 0)
            {
                parentAC = Ac_spec.get(0);
                parentAC_ID = parentAC.id;
                
                Lease_ID = parentAC.Lease__c;
                              
            }
            
      
                  
       List<Lease__c>   Lease_List = [select id, Operator__c 
                                           from Lease__c where id =  :Lease_ID];
        
       if (Lease_List.size() > 0)
            {
                Lease = Lease_List.get(0);
                OperatorID = Lease.Operator__c;
                
            }  
                
      List<Operator__c>   OperatorList = [select id, Country__c,Name,Base_Of_Operations__c
                                           from Operator__c  where id =  :OperatorID];  
     
      if(OperatorList.size() > 0)
            {
               Operator = OperatorList.get(0);
             }
             
     ConstAssembEngine = getConstitutenAssemblyEngine(parentAC); 
     
     ConstAssembGear = getConstitutenAssemblyGear(parentAC);
     
     ConstAssembApu = getConstitutenAssemblyApu(parentAC);
     
     ACEligible_Event = getAircraft_Eligible_Event(parentAC);
     
     Equipment = getEquipment(parentAC);
     
     
   }
   
   
    
    public Aircraft__c getparentAC() {
        return parentAC;
    }
    
     public Lease__c getLease() {
        return Lease;
    }
    
     public Operator__c getOperator() {
        return Operator;
    }
    
   public List<Aircraft_Eligible_Event__c> getAircraft_Eligible_Event(Aircraft__c aircraft)
    {
        List<Aircraft_Eligible_Event__c> Eligible_Event= new List<Aircraft_Eligible_Event__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            Eligible_Event = [select id, Event_Type__c, CSN__c, End_Date__c, Start_Date__c, TSN_N__c from 
                           Aircraft_Eligible_Event__c where Aircraft__c = :aircraftID  
                           Order By LastModifiedDate Desc];  
                           
        }  
           
        return Eligible_Event;
    }
    
    public List<Aircraft_Eligible_Event__c> getACEligible_Event() {
        return ACEligible_Event;
    }
    
     public List<Equipment__c> getEquipment(Aircraft__c aircraft)
    {
        List<Equipment__c> Equipment = new List<Equipment__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            Equipment = [select id, ATA_Text__c, Description__c, Part_Number__c, Serial_Number__c, Vendor__c, 
            Family__c, Quantity__c 
            from Equipment__c where Aircraft__c = :aircraftID 
                           Order By ATA_Text__c Asc];  
// AND (ATA_Text__c = '21' or ATA_Text__c = '22' or ATA_Text__c = '23' or ATA_Text__c = '31'  or ATA_Text__c = '34' )
                                                      
        }  
           
        return Equipment;
    }
    
    public string getLogo_URL()
    {
      
        return Logo_URL;
    }
    
       
    public List<Equipment__c> getEquipment(){
        return Equipment;
    }
    
  
    
    
    public List<Constituent_Assembly__c> getConstitutenAssemblyEngine(Aircraft__c aircraft)
    {
        List<Constituent_Assembly__c> constAssemblies = new List<Constituent_Assembly__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            constAssemblies = [select id,name,Part_Number__c,Serial_Number__c, Model__c, Type__c, CSN__c, TSN__c, CSLV__c, TSLV__c, Life_Limit__c from 
                           Constituent_Assembly__c where Attached_Aircraft__c = :aircraftID AND
                           (Type__c = 'Engine 1' or Type__c = 'Engine 2' or Type__c = 'Engine 3' or Type__c = 'Engine 4')Order By LastModifiedDate Desc];  
                           
        }  
                 
       List<Constituent_Assembly__c> constAssembliesToReturn = new List<Constituent_Assembly__c>();
        Set<String> setType = new Set<String>();
        for(Constituent_Assembly__c obj : constAssemblies)
             {
                if(setType.add(obj.Type__c))
                     {
                         constAssembliesToReturn.add(obj);
                     }
             }
               
        return constAssembliesToReturn;
    }
    
    public List<Constituent_Assembly__c> getConstitutenAssemblyGear(Aircraft__c aircraft)
    {
        List<Constituent_Assembly__c> constAssemblies = new List<Constituent_Assembly__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            constAssemblies = [select id,name,Part_Number__c,Serial_Number__c, Model__c, Type__c, CSN__c, TSN__c, CSLV__c, TSLV__c, Life_Limit__c from 
                           Constituent_Assembly__c where Attached_Aircraft__c = :aircraftID AND
                           (Type__c = 'Landing Gear - Center' or Type__c = 'Landing Gear - Left Main'
                             or Type__c = 'Landing Gear - Right Main' or Type__c = 'Landing Gear - Nose' or Type__c = 'Landing Gear - Left Wing'
                             or Type__c = 'Landing Gear - Right Wing')
                             Order By LastModifiedDate Desc];  
   
        }           
        List<Constituent_Assembly__c> constAssembliesToReturn = new List<Constituent_Assembly__c>();
        Set<String> setType = new Set<String>();
        for(Constituent_Assembly__c obj : constAssemblies)
             {
                if(setType.add(obj.Type__c))
                     {
                         constAssembliesToReturn.add(obj);
                     }
             }
               
        return constAssembliesToReturn;
    }
   
   public List<Constituent_Assembly__c> getConstitutenAssemblyApu(Aircraft__c aircraft)
    {
        List<Constituent_Assembly__c> constAssemblies = new List<Constituent_Assembly__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            constAssemblies = [select id,name,Part_Number__c,Serial_Number__c, Model__c, Type__c, CSN__c, TSN__c, CSLV__c, TSLV__c, Life_Limit__c from 
                           Constituent_Assembly__c where Attached_Aircraft__c = :aircraftID AND
                           Type__c = 'APU' ];  
                           
        }  
                 
       List<Constituent_Assembly__c> constAssembliesToReturn = new List<Constituent_Assembly__c>();
        Set<String> setType = new Set<String>();
        for(Constituent_Assembly__c obj : constAssemblies)
             {
                if(setType.add(obj.Type__c))
                     {
                         constAssembliesToReturn.add(obj);
                     }
             }
               
        return constAssembliesToReturn;
    } 
    
   public List<Constituent_Assembly__c> getConstAssembEngine(){
   
    return ConstAssembEngine;
   }
   
   public List<Constituent_Assembly__c> getConstAssembGear(){
       return ConstAssembGear;
   } 
   
   public List<Constituent_Assembly__c> getConstAssembApu(){
       return ConstAssembApu;
   }  
   
   
   
    public PageReference sendPdf() {
     
    PageReference pdf = Page.AC_Spec_Sheet_B;
    // add parent id to the parameters for standardcontroller
    pdf.getParameters().put('id',parentAC.id);
 
    // the contents of the attachment from the pdf
    Blob body;
 
    // returns the output of the page as a PDF
    body = !Test.isRunningTest() ? pdf.getContent() : Blob.ValueOf('dummy text');
 
    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
    attach.setContentType('application/pdf');
    attach.setFileName('AC_Spec_Sheet_B.pdf');
    attach.setInline(false);
    attach.Body = body;
 
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setUseSignature(false);
    
//  Messaging.SendEmailResult[] results;
    try{


        String[] mailAddresses; 
        if(Test.isRunningTest()){
            mail.setToAddresses(new String[] {'a@lw.com'});
        }else{
            mailAddresses =email.deleteWhitespace().split('[,;]',0);
            mail.setToAddresses(mailAddresses);
        }
    
        mail.setSubject(sub);
        mail.setplainTextBody(ebody);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
     
        // Send the email
        Messaging.SendEmailResult[] results=Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }, false);
        if(!results[0].isSuccess()){
        
            System.Statuscode errStatus=results[0].getErrors()[0].getStatusCode();
                
            if(errStatus ==StatusCode.SINGLE_EMAIL_LIMIT_EXCEEDED){
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Email could not be sent as the limit has been reached. Please try after some time.' ));
            }else if(errStatus ==StatusCode.INVALID_EMAIL_ADDRESS){
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid email address. Multiple email ids can be separated by comma or semicolon.' ));
            }else{
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Email could not be sent - ' + results[0].getErrors()[0].Message));
            }
            return null;
        }
    }catch(exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Email could not be sent. Exception - ' + e));
            return null;
    }
 
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with spec sheet sent to '+email));
 
    return null;
 
  }

}