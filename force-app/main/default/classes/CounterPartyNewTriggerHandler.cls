/*
	Created By: Anjani Prasad
	
	Decription :
		Date : 10th June 2016

	






*/




public class CounterPartyNewTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'CounterPartyNewTriggerHandlerBefore';
    private final string triggerAfter = 'CounterPartyNewTriggerHandlerAfter';
    private  map<Id, String> mapCountry = new  map<Id, String>();

             
    // declaring private variable : accessing from before and after trigger both               
    
    public CounterPartyNewTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
       
        if(Trigger.isDelete)return;
    
            Set<Id> countryIds = new Set<Id>();
            for(Bank__c cp : (list<Bank__c>)trigger.new){
                if(cp.Country_Lookup__c!=null){
                    countryIds.add(cp.Country_Lookup__c);
                }
            }
            
            for(Country__c cou:  [select Id,Name from Country__c where ID IN: countryIds]){
                mapCountry.put(cou.Id,cou.Name);
            }
        
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {

		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('CounterPartyNewTriggerHandler.beforeInsert(+)');
        updateSearchableFieldOfCountry(trigger.new);
		

	    system.debug('CounterPartyNewTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('CounterPartyNewTriggerHandler.beforeUpdate(+)');
        List<Bank__c> cpListToUpdateTextCountry = new List<Bank__c>();
        Bank__c[] listNew = (list<Bank__c>)trigger.new ;
        Bank__c oldRec;
        for(Bank__c curRec : listNew){
            oldRec = (Bank__c)trigger.oldMap.get(curRec.id);
            if(curRec.Country_Lookup__c != oldRec.Country_Lookup__c){ 
               cpListToUpdateTextCountry.add(curRec);
            }
        }
        if(cpListToUpdateTextCountry.size()>0){
            updateSearchableFieldOfCountry(cpListToUpdateTextCountry);
        }

		

    	system.debug('CounterPartyNewTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	system.debug('CounterPartyNewTriggerHandler.beforeDelete(+)');

    	system.debug('CounterPartyNewTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {

		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CounterPartyNewTriggerHandler.afterInsert(+)');
    	
         //CreatePartyNew();
         createAccount();
        
    	
    	system.debug('CounterPartyNewTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CounterPartyNewTriggerHandler.afterUpdate(+)');
    	
    	
    	system.debug('CounterPartyNewTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {

    	system.debug('CounterPartyNewTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('CounterPartyNewTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {

    	system.debug('CounterPartyNewTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('CounterPartyNewTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

    private void createAccount(){
        if( 'ON'.equals(LeaseWareUtils.getLWSetup_CS('Auto_Creation_of_Accounts'))){
            list<Bank__c> listNew =  (list<Bank__c>)trigger.new;
            List<String> cpNameList = new List<String>();
            for(Bank__c opr: listNew){
                cpNameList.add(opr.Name);
            }
            LeasewareUtils.createAccount(cpNameList, 'Counterparty');
        }
   }

   private void updateSearchableFieldOfCountry(List<Bank__c> cpListToUpdate){
    for(Bank__c cp : cpListToUpdate){
        cp.Y_Hidden_Country__c = cp.Country_Lookup__c==null?'':mapCountry.containsKey(cp.Country_Lookup__c)?mapCountry.get(cp.Country_Lookup__c):'';
    }
        
  }
}