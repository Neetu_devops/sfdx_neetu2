public with sharing class AssetPortalController {
     
    
    @AuraEnabled
    public static List<String> getPicklistBasedOnRecordType (string objectName, string fieldName, string recordTypeName) {
        if(String.isBlank(objectName) || String.isBlank(fieldName)){
            return null;
        }     
        String prefix = getNameSpacePrefix();
      	map<string,string> returnMap = new map<string,string>();
		string objectNameNew = objectName;
        if(objectName.indexof(prefix) == -1) {
            objectNameNew = getNameSpacePrefix() + objectName;
        }
        
        string fieldNameNew = fieldName;
        if(fieldName.indexof(prefix) == -1) {
            fieldNameNew = prefix + fieldName;
        }
        
        String recordTypeId = '';
        if(Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByDeveloperName().containsKey(recordTypeName) && Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName) != null){
            recordTypeId = Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
            Map<string,string> picklistValueMap = LeaseWareUtils.getValues(objectNameNew, recordTypeId, fieldNameNew);
            return picklistValueMap.values();
        }
        return null;
    }
    
    @AuraEnabled
    public static List<PortalAssetSelectedWrapper> getAssetRequest(String searchRequest) {
        AssetPortalController.SearchRequestWrapper searchWrapper = (AssetPortalController.SearchRequestWrapper)System.JSON.deserialize(searchRequest, AssetPortalController.SearchRequestWrapper.class);
        System.debug('getAssetRequest searchWrapper : ' + Json.serializePretty(searchWrapper));

        
        List<PortalAssetSelectedWrapper> requestList = PortalAssetHelper.searchAssets(searchWrapper.assetRequest);
        if(requestList != null) {
            System.debug('getAssetRequest searchWrapper : ' + Json.serializePretty(requestList));
        } 
        return requestList;
    }
    
    @AuraEnabled
    public static Map<String,String> saveRequestData(String saveRequest, String requestData) {
        List<PortalAssetSelectedWrapper> saveWrapperList = (List<PortalAssetSelectedWrapper>)System.JSON.deserialize(saveRequest, List<PortalAssetSelectedWrapper>.class);
        System.debug('saveRequestData saveWrapper : ' + Json.serializePretty(saveWrapperList));

        AssetPortalController.SearchRequestWrapper searchWrapper = (AssetPortalController.SearchRequestWrapper)System.JSON.deserialize(requestData, AssetPortalController.SearchRequestWrapper.class);
        System.debug('getAssetRequest searchWrapper : ' + Json.serializePretty(searchWrapper));

        Map<String,String> resultMap = new Map<String,String>();
        if(saveWrapperList != null && searchWrapper != null && searchWrapper.assetRequest != null) {
            List<Portal_Asset_Selected__c> saveList = new List<Portal_Asset_Selected__c>();
            for(PortalAssetSelectedWrapper wrapper: saveWrapperList) {
                saveList.add(wrapper.portalAssetSelected);
            }
            System.debug('saveRequestData saveList: '+saveList);
            System.debug('getAssetRequest assetRequest : ' + Json.serializePretty(searchWrapper.assetRequest));
            resultMap = PortalAssetHelper.savePortalAssetsSelected(searchWrapper.assetRequest, saveList);
            resultMap.put('Id', searchWrapper.assetRequest.Id);
            return resultMap;
        }
        return null;
    }

    @AuraEnabled
    public static String getNameSpacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(String objDetail, string contrfieldApiName,string depfieldApiName) {
        String namespace = getNameSpacePrefix();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(namespace+objDetail); 
        SObject myObj = targetType.newSObject();
        
    	Map<String, List<String>> objRes = DependentFilters.getDependentMap(myObj,contrfieldApiName,depfieldApiName);
        return objRes;
    }
    
    @AuraEnabled
    Public static List<String> getPickListValues(String objectName, String fld) {
        String namespace = getNamespacePrefix();
        objectName = namespace + objectName;
        objectName = objectName.replace(namespace + namespace , namespace);
        
        if(!fld.equals('Name')) {
            fld = namespace + fld;
            fld = fld.replace(namespace + namespace , namespace);
        }
        
        system.debug('objObject --->' + objectName);
        system.debug('fld --->' + fld);
        List <String> allOpts = new list <String>();
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        return allOpts;
    }
        
    public class SearchRequestWrapper{
        @AuraEnabled public Decimal leaseTerm  {get; set;}
        @AuraEnabled public Decimal totalContractAmt  {get; set;}
        @AuraEnabled public Portal_Asset_Request__c assetRequest {get; set;}
    }
    
    public class PortalAssetSelectedWrapper
    {
        @AuraEnabled public Portal_Asset_Selected__c PortalAssetSelected{get;set;}
        @AuraEnabled public boolean HasSpecSheet{get;set;}
        @AuraEnabled public boolean HasMoreThanHalfLifeFC{get;set;}     
        @AuraEnabled public boolean IsAfterSV
        {   get
            {
                if(String.IsNotBlank(PortalAssetSelected.Engine_Condition__c) && PortalAssetSelected.Engine_Condition__c.contains('SV'))
                    return true;
                else 
                    return false;
            }
            set;
        }   

        //Formula fields on PAS will not be evaluated since the record is not yet inserted
        //So, compute the values for displaying in the UI
        @AuraEnabled public String EstRemLifeFHFC {get;set;}       
        @AuraEnabled public Integer EstRemLifeMO{get;set;}
        @AuraEnabled public String Location{get;set;}
        @AuraEnabled public boolean IsPreRequested{get;set;}

        public PortalAssetSelectedWrapper(Portal_Asset_Selected__c selectedAsset, boolean hasSpecSheet, boolean hasMoreThanHalfLifeFC)
        {
            this.PortalAssetSelected = selectedAsset;
            this.HasSpecSheet = hasSpecSheet;
            this.HasMoreThanHalfLifeFC = hasMoreThanHalfLifeFC;            
        }
    }

}