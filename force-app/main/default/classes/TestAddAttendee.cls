/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAddAttendee {

    @isTest(seeAllData=true)
    static void myUnitTest() {
        // TO DO: implement unit test
        Lease__c l1 = [select Operator__c from lease__c where name like '%TestSeededMSN1%' limit 1];
        
        Customer_Interaction_Log__c CL1 = new Customer_Interaction_Log__c(Name='Dummy',Related_To_Operator__c =l1.Operator__c );
        LeaseWareUtils.clearFromTrigger();insert CL1;
        
        Address__c add1 = new Address__c(name='Dummy',Last_Name__c='a',First_Name__c='b');
        LeaseWareUtils.clearFromTrigger();insert add1;
        
        
        Attendee_Contacts__c Acont = new Attendee_Contacts__c(name='Dummy',contact__c=add1.Id,Interaction_Log__c =CL1.Id );
        LeaseWareUtils.clearFromTrigger();insert Acont;
        
        LeaseWareUtils.clearFromTrigger();update Acont;
        
        LeaseWareUtils.clearFromTrigger();delete Acont;
        
        LeaseWareUtils.clearFromTrigger();undelete Acont;
    }
}