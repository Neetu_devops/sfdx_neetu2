/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestVendorInvoice {

    @isTest(seeAllData=false)
    static void WireSummaryCreation() {
        // TO DO: implement unit test
        
        //Creating Wire Summary
        Invoice_Summary__c InvSum= new Invoice_Summary__c();
        InvSum.Name='Wire Summary 1';
        InvSum.For_Month_Ending__c=system.today();
        insert InvSum;
        
        
        
        //Creating Bank__c Counterparty 
        Bank__c bankRec= new Bank__c ();
        bankRec.Name= 'Bank of TestData1';
        insert bankRec;
        
        //Creating Vendors
        Vendor_Invoice_Summary__c vendorRec = new Vendor_Invoice_Summary__c ();
        vendorRec.Invoice_Summary__c= InvSum.Id;  
        vendorRec.Name='Vendor 1';
        vendorRec.Vendor__c= bankRec.Id; //Lookup Counterparty
        insert vendorRec;
        
        //Creating Cost_Center_Grouping__c
        Cost_Center_Grouping__c ccgRec=new Cost_Center_Grouping__c();
        ccgRec.Name= 'CostCenter Grouping1';
        ccgRec.Vendor_Invoice_Summary__c=vendorRec.Id;  
        insert ccgRec;
        
        
        //Creating Counterparty_Invoice__c
        Counterparty_Invoice__c counterInvRec= new Counterparty_Invoice__c();
        counterInvRec.Name='Counter Party Inv 1';
        counterInvRec.Counterparty__c= bankRec.Id; //counterparty
        counterInvRec.Invoice_Date__c=system.today();
        insert counterInvRec;
        
        
        
        
        
        //Creating vendor wire instruction Vendor_Invoice__c
        Vendor_Invoice__c VendorWInsRec= new Vendor_Invoice__c();
        VendorWInsRec.Name= 'Vendor Wire Inst1';
        VendorWInsRec.Counterparty_Invoice__c = counterInvRec.Id; //Lookup to Counterparty Invoice
        VendorWInsRec.Cost_Center_Grouping__c = ccgRec.Id; 
        insert VendorWInsRec;
        
        
        //calling Create Invoice Method via js
        Date forMonthending= system.today();
        string month='June';
        string year = '2017';
        //string result = LWGlobalReportUtils.createInvoiceSummary(InvSum.Id, forMonthending, month , year);
        //system.debug(result );
        
        //update 
        InvSum.Name='Wire Summary 2';
        update InvSum;
        
        vendorRec.Name='Vendor 2';
        update vendorRec;
        
        //update 
        counterInvRec.Name='Counter Party Inv 2';
        counterInvRec.Invoice_Date__c=system.today()+2;
        update counterInvRec;
    }
    
    @isTest(seeAllData=false)
    static void VendorInvoiceTrigger() {
        // TO DO: implement unit test
    }
}