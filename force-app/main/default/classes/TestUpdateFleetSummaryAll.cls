@IsTest
public class TestUpdateFleetSummaryAll {
    @testSetup
    static Void testSetup(){
        List<Global_Fleet__c> fleetRecordList = new List<Global_Fleet__c>();
         Operator__c opr1 = new Operator__c(Name='Test Operator1');
        insert opr1;
        
        Operator__c opr2 = new Operator__c(Name='Test Operator2',Group_Parent__c=opr1.id);
        insert opr2;
        
        Global_Fleet__c gf = new Global_Fleet__c();
        gf.Name = 'testWorldFleet';
        gf.Aircraft_External_ID__c = 'type-msn';
        gf.MTOW__c = 134453;
        gf.BuildYear__c = '2010';
        gf.SeatTotal__c = 12;
        gf.AircraftType__c = 'A320';
        gf.AircraftVariant__c = '400';
        gf.EngineType__c = 'V2524';
        gf.AircraftSeries__c = 'AStest';
        gf.EngineVariant__c = 'EVtest';
        gf.OriginalOperator__c ='testOpr';
        gf.Aircraft_Operator__c = opr2.Id;
        
        insert gf;
        Fleet_Summary__c summary = new Fleet_Summary__c(name = 'Test', Aircraft_Type__c = gf.AircraftType__c, 
                                               Variant__c = gf.AircraftVariant__c,Controlling_Airline__c = opr2.ID);
        insert summary;
        
        Global_Fleet__c gf1 = new Global_Fleet__c();
        gf1.Name = 'testWorldFleet1';
        gf1.Aircraft_External_ID__c = 'type-msn1';
        gf1.MTOW__c = 134453;
        gf1.BuildYear__c = '2010';
        gf1.SeatTotal__c = 12;
        gf1.AircraftType__c = 'A319';
        gf1.AircraftVariant__c = '400';
        gf1.EngineType__c = 'CFM56';
        gf1.AircraftSeries__c = 'AStest';
        gf1.EngineVariant__c = 'EVtest';
        gf1.OriginalOperator__c ='testOpr';
        gf1.Aircraft_Operator__c = opr2.Id;
        
        insert gf1;
 }
    @isTest
    static void testUpdateFleetSummaryAll(){
       
        
        Test.startTest();
        Database.executeBatch(new BatchUpdateAllFleetSummary(),10);
        Test.stopTest();
        Operator__c opr = [select Id from Operator__c limit 1];
        List<Fleet_Summary__c> fleetSummary = [select Id,Aircraft_Type__c from Fleet_Summary__c where Controlling_Airline__c=:opr.id];
        system.assertEquals(2, fleetSummary.size());
        
        
    }
    
    @isTest
    static void testUpdateofWorldFleet(){
        Operator__c opr = [select Id from Operator__c limit 1];
       
        Global_Fleet__c gf = [select Id from Global_Fleet__c where name='testWorldFleet1'];
       	delete gf;
        Test.startTest();
        Database.executeBatch(new BatchUpdateAllFleetSummary(),10);
       Test.stopTest();
       
         List<Fleet_Summary__c> fleetSummary1 = [select Id,Aircraft_Type__c from Fleet_Summary__c where Controlling_Airline__c=:opr.id];
        system.assertEquals(1, fleetSummary1.size());
    }

    //Just for code coverage
	 @isTest
    static void testAuraMethod(){
        BatchUpdateAllFleetSummary.updateFleetSummaryAll();
        BatchUpdateAllFleetSummary.getNamespacePrefix();
        BatchUpdateAllFleetSummary.getListViews();
    }
           
}