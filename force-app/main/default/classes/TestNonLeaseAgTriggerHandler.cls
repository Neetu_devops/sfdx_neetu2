@isTest
public class TestNonLeaseAgTriggerHandler {
    @testSetup
    static void setup(){
        
        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;
        
        Lease__c lease = new Lease__c(Name='TestLease',Lessee__c=operator.Id,Lease_End_Date_New__c=Date.newInstance(2023, 12, 31),Lease_Start_Date_New__c=Date.newInstance(2019, 01, 01));
        
        LWGlobalUtils.setTriggersflagOn();
        insert lease;
        LWGlobalUtils.setTriggersflagOff();
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
        insert aircraft;
        
		aircraft.Lease__c = lease.Id;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;
        
        Id MutliRentIDFixed = TestLeaseworkUtil.createmultiRentFixed(lease.Id,'Fixed - 30/360',
                             Date.newInstance(2019, 05, 13),Date.newInstance(2020, 01, 12));
        Id MutliRentIDActual = TestLeaseworkUtil.createmultiRentFixed(lease.Id,'Actual number of days in month/year',
                             Date.newInstance(2020, 01, 15),Date.newInstance(2020, 08, 31));
    }

    @isTest
    public static void TestFeeMonthly(){
        Lease__c lease = [select id,Aircraft__c from lease__c limit 1];
    	String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Fees').getRecordTypeId();
        Non_Lease_agreements__c nla = new non_lease_agreements__c(name ='Test Fees',start_date__c =Date.newInstance(2020, 01, 01),end_date__c=Date.newInstance(2020, 06, 30),
                                                              periodicity__c = 'Monthly',RecordTypeId = recordType,Periodic_Amount__c =1500);
        insert nla;
        Test.startTest();
        list<Fees_Schedule__c> feesList = [select id,Amount_Due__c,start_date__c,end_date__c,Amount_Paid__c,Outstanding_Amount__c
                                          from Fees_Schedule__c where fees__r.name = 'Test Fees' order by start_date__c];
        system.assertEquals(feesList.size(),6);
        system.assertEquals(feesList.get(0).start_date__c,Date.newInstance(2020, 01, 01));
        system.assertEquals(feesList.get(0).end_date__c,Date.newInstance(2020, 01, 31));
        system.assertEquals(feesList.get(0).amount_due__c,1500);
        system.assertEquals(feesList.get(0).Outstanding_Amount__c,1500);
        system.assertEquals(feesList.get(5).start_date__c,Date.newInstance(2020, 06, 01));
        system.assertEquals(feesList.get(5).end_date__c,Date.newInstance(2020, 06, 30));
        system.assertEquals(feesList.get(5).amount_due__c,1500);
        
        
        LeaseWareUtils.clearFromTrigger(); 
        nla = new non_lease_agreements__c(name ='Test Fees 1',start_date__c =Date.newInstance(2020, 01, 15),end_date__c=Date.newInstance(2020, 06, 10),
                                                              periodicity__c = 'Monthly',RecordTypeId = recordType,Periodic_Amount__c =1500);
        insert nla;
     
        feesList = [select id,Amount_Due__c,start_date__c,end_date__c,Amount_Paid__c,Outstanding_Amount__c
                                          from Fees_Schedule__c where fees__r.name = 'Test Fees 1' order by start_date__c];
        
        system.assertEquals(feesList.size(),5);
        system.assertEquals(feesList.get(0).start_date__c,Date.newInstance(2020, 01, 15));
        system.assertEquals(feesList.get(0).end_date__c,Date.newInstance(2020, 02, 14));
        system.assertEquals(feesList.get(0).amount_due__c,1500);
        system.assertEquals(feesList.get(0).Outstanding_Amount__c,1500);
        system.assertEquals(feesList.get(4).start_date__c,Date.newInstance(2020, 05, 15));
        system.assertEquals(feesList.get(4).end_date__c,Date.newInstance(2020, 06, 10));
        system.assertEquals(feesList.get(4).amount_due__c,1350);
        Test.stopTest();
    
    }
    
    @isTest
    public static void TestFeeQuarterly(){
        Lease__c lease = [select id,Aircraft__c from lease__c limit 1];
    	String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Fees').getRecordTypeId();
        Non_Lease_agreements__c nla = new non_lease_agreements__c(name ='Test Fees',start_date__c =Date.newInstance(2020, 01, 01),end_date__c=Date.newInstance(2020, 06, 30),
                                                              periodicity__c = 'Quarterly',RecordTypeId = recordType,Periodic_Amount__c =1500);
        insert nla;
        Test.startTest();
        list<Fees_Schedule__c> feesList = [select id,Amount_Due__c,start_date__c,end_date__c,Amount_Paid__c,Outstanding_Amount__c
                                          from Fees_Schedule__c where fees__r.name = 'Test Fees' order by start_date__c];
        system.assertEquals(feesList.size(),2);
        system.assertEquals(feesList.get(0).start_date__c,Date.newInstance(2020, 01, 01));
        system.assertEquals(feesList.get(0).end_date__c,Date.newInstance(2020, 03, 31));
        system.assertEquals(feesList.get(0).amount_due__c,1500);
        system.assertEquals(feesList.get(0).Outstanding_Amount__c,1500);
        system.assertEquals(feesList.get(1).start_date__c,Date.newInstance(2020, 04, 01));
        system.assertEquals(feesList.get(1).end_date__c,Date.newInstance(2020, 06, 30));
        system.assertEquals(feesList.get(1).amount_due__c,1500);
        
       
        LeaseWareUtils.clearFromTrigger(); 
        nla = new non_lease_agreements__c(name ='Test Fees 1',start_date__c =Date.newInstance(2020, 01, 15),end_date__c=Date.newInstance(2020, 06, 10),
                                                              periodicity__c = 'Quarterly',RecordTypeId = recordType,Periodic_Amount__c =1500);
        insert nla;
     
        feesList = [select id,Amount_Due__c,start_date__c,end_date__c,Amount_Paid__c,Outstanding_Amount__c
                                          from Fees_Schedule__c where fees__r.name = 'Test Fees 1' order by start_date__c];
        
        system.assertEquals(feesList.size(),2);
        system.assertEquals(feesList.get(0).start_date__c,Date.newInstance(2020, 01, 15));
        system.assertEquals(feesList.get(0).end_date__c,Date.newInstance(2020, 04, 14));
        system.assertEquals(feesList.get(0).amount_due__c,1500);
        system.assertEquals(feesList.get(0).Outstanding_Amount__c,1500);
        system.assertEquals(feesList.get(1).start_date__c,Date.newInstance(2020, 4, 15));
        system.assertEquals(feesList.get(1).end_date__c,Date.newInstance(2020, 06, 10));
        system.assertEquals(feesList.get(1).amount_due__c.setScale(2),950.00);
        Test.stopTest();
    
    }
    
    @isTest
    public static void TestFeeAnnual(){
        Lease__c lease = [select id,Aircraft__c from lease__c limit 1];
    	String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Fees').getRecordTypeId();
        Non_Lease_agreements__c nla = new non_lease_agreements__c(name ='Test Fees',start_date__c =Date.newInstance(2020, 01, 01),end_date__c=Date.newInstance(2021, 12, 31),
                                                              periodicity__c = 'Annual',RecordTypeId = recordType,Periodic_Amount__c =1500);
        insert nla;
        Test.startTest();
        list<Fees_Schedule__c> feesList = [select id,Amount_Due__c,start_date__c,end_date__c,Amount_Paid__c,Outstanding_Amount__c
                                          from Fees_Schedule__c where fees__r.name = 'Test Fees' order by start_date__c];
        system.assertEquals(feesList.size(),2);
        system.assertEquals(feesList.get(0).start_date__c,Date.newInstance(2020, 01, 01));
        system.assertEquals(feesList.get(0).end_date__c,Date.newInstance(2020, 12, 31));
        system.assertEquals(feesList.get(0).amount_due__c,1500);
        system.assertEquals(feesList.get(0).Outstanding_Amount__c,1500);
        system.assertEquals(feesList.get(1).start_date__c,Date.newInstance(2021, 01, 01));
        system.assertEquals(feesList.get(1).end_date__c,Date.newInstance(2021, 12, 31));
        system.assertEquals(feesList.get(1).amount_due__c,1500);
        
        
        LeaseWareUtils.clearFromTrigger(); 
        nla = new non_lease_agreements__c(name ='Test Fees 1',start_date__c =Date.newInstance(2020, 01, 01),end_date__c=Date.newInstance(2021, 10, 10),
                                                              periodicity__c = 'Annual',RecordTypeId = recordType,Periodic_Amount__c =1500);
        insert nla;
     
        feesList = [select id,Amount_Due__c,start_date__c,end_date__c,Amount_Paid__c,Outstanding_Amount__c
                                          from Fees_Schedule__c where fees__r.name = 'Test Fees 1' order by start_date__c];
        
        system.assertEquals(feesList.size(),2);
        system.assertEquals(feesList.get(0).start_date__c,Date.newInstance(2020, 01, 01));
        system.assertEquals(feesList.get(0).end_date__c,Date.newInstance(2020, 12, 31));
        system.assertEquals(feesList.get(0).amount_due__c,1500);
        system.assertEquals(feesList.get(0).Outstanding_Amount__c,1500);
        system.assertEquals(feesList.get(1).start_date__c,Date.newInstance(2021, 01, 01));
        system.assertEquals(feesList.get(1).end_date__c,Date.newInstance(2021, 10, 10));
        system.assertEquals(feesList.get(1).amount_due__c.setScale(2),1179.17);
        Test.stopTest();
    
    }
    
    @isTest
    public static void TestFeeOneTime(){
        Lease__c lease = [select id,Aircraft__c from lease__c limit 1];
    	String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Fees').getRecordTypeId();
        Non_Lease_agreements__c nla = new non_lease_agreements__c(name ='Test Fees',start_date__c =Date.newInstance(2020, 01, 01),end_date__c=Date.newInstance(2021, 12, 31),
                                                              periodicity__c = 'One Time Payment',RecordTypeId = recordType,Periodic_Amount__c =1500);
        insert nla;
        Test.startTest();
        list<Fees_Schedule__c> feesList = [select id,Amount_Due__c,start_date__c,end_date__c,Amount_Paid__c,Outstanding_Amount__c
                                          from Fees_Schedule__c where fees__r.name = 'Test Fees' order by start_date__c];
        system.assertEquals(feesList.size(),1);
        system.assertEquals(feesList.get(0).start_date__c,Date.newInstance(2020, 01, 01));
        system.assertEquals(feesList.get(0).end_date__c,Date.newInstance(2021, 12, 31));
        system.assertEquals(feesList.get(0).amount_due__c,1500);
        Test.stopTest();
    
    }
    @isTest
    public static void TestSPFixed(){
        Lease__c lease = [select id,Aircraft__c from lease__c limit 1];
    	String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Sales_Purchase_Agreement').getRecordTypeId();
        Non_Lease_agreements__c nla = new non_lease_agreements__c(name ='Test SP',Economic_Closing_Date__c =Date.newInstance(2019, 05, 30),Actual_Closing_Date__c=Date.newInstance(2020, 01, 10),
                                                              RecordTypeId = recordType,Contracted_Interest__c = 6,Deposit_Paid__c=100000,Sale_Purchase_Price__c =45000000,lease__c = lease.id);
        insert nla;
        Test.startTest();
        nla = [select id,Contracted_Interest_Amount__c,Current_Rental_amount__c,Contracted_Interest__c,Deposit_Paid__c,Sale_Purchase_Price__c,Economic_Closing_Date__c,Actual_Closing_Date__c,
              Gross_Price__c,Net_Price__c from non_lease_agreements__c where name ='Test SP' limit 1];
        
        
        Decimal GrossPrice = nla.Sale_Purchase_Price__c+nla.Contracted_Interest_Amount__c-nla.Current_Rental_amount__c;
        Decimal NetPrice = nla.Gross_Price__c - nla.Deposit_Paid__c;
        system.assertEquals((nla.Current_Rental_amount__c).setScale(2),1486666.67);
        system.assertEquals((nla.Contracted_Interest_Amount__c).setScale(2),1687500.00);
        system.assertEquals((nla.Gross_Price__c).setScale(2),grossPrice.setScale(2));
        system.assertEquals((nla.Net_Price__c).setScale(2),Netprice.setScale(2));
        Test.stopTest();
    } 
    
    @isTest
    public static void TestSPActual(){
        Lease__c lease = [select id,Aircraft__c from lease__c limit 1];
    	String recordType = Schema.SObjectType.Non_Lease_Agreements__c.getRecordTypeInfosByDeveloperName().get('Sales_Purchase_Agreement').getRecordTypeId();
        Non_Lease_agreements__c nla = new non_lease_agreements__c(name ='Test SP 1',Economic_Closing_Date__c =Date.newInstance(2020, 01, 20),Actual_Closing_Date__c=Date.newInstance(2020, 08, 28),
                                                              RecordTypeId = recordType,Contracted_Interest__c = 6,Deposit_Paid__c=100000,Sale_Purchase_Price__c =45000000,lease__c = lease.id);
        insert nla;
        Test.startTest();
        nla = [select id,Contracted_Interest_Amount__c,Current_Rental_amount__c,Contracted_Interest__c,Deposit_Paid__c,Sale_Purchase_Price__c,Economic_Closing_Date__c,Actual_Closing_Date__c,
              Gross_Price__c,Net_Price__c from non_lease_agreements__c where name ='Test SP 1' limit 1];
        
        
        Decimal GrossPrice = nla.Sale_Purchase_Price__c+nla.Contracted_Interest_Amount__c-nla.Current_Rental_amount__c;
        Decimal NetPrice = nla.Gross_Price__c - nla.Deposit_Paid__c;
        system.assertEquals((nla.Current_Rental_amount__c).setScale(2),1458064.52);
        system.assertEquals((nla.Contracted_Interest_Amount__c).setScale(2),1657500.00);
        system.assertEquals((nla.Gross_Price__c).setScale(2),grossPrice.setScale(2));
        system.assertEquals((nla.Net_Price__c).setScale(2),Netprice.setScale(2));
        Test.stopTest();
    }
}