public class InstallAssemblyController {
    
    public class AssemblyWrapper {
        @AuraEnabled
        public Id assemblyId {get;set;}
        @AuraEnabled
        public Id assetId {get;set;}
        @AuraEnabled
        public Id leaseId {get; set;}
        @AuraEnabled
        public String assemblyName {get;set;}
        @AuraEnabled
        public String assetName {get;set;}
        @AuraEnabled
        public List<String> approvalStatus {get;set;}
        @AuraEnabled
        public String selectedApprovalStatus {get; set;}
        @AuraEnabled
        public Decimal assemblyTSN {get;set;}
        @AuraEnabled
        public Decimal assetTSN {get;set;}
        @AuraEnabled
        public Decimal assemblyCSN {get;set;}
        @AuraEnabled
        public Decimal assetCSN {get;set;}
        @AuraEnabled
        public String reason {get;set;}
        @AuraEnabled
        public String whereClause {get; set;}
    }
    
    @AuraEnabled
    public static AssemblyWrapper getAssemblyData(Id recordId, Date todayDate) {
        AssemblyWrapper assemblyData = new AssemblyWrapper();
        
        Id recordTypeIdAssembly = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();
        Id recordTypeIdAssetAircraft = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        Id recordTypeIdAssetEngine = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId();
        
        Constituent_Assembly__c assembly = [SELECT Id, RecordTypeId, Asset__c, Name, Asset__r.Name, Asset__r.Lease__c,
                                            TSN__c, CSN__c, Asset__r.TSN__c, Asset__r.CSN__c 
                                            FROM Constituent_Assembly__c WHERE ID = :recordId];

        List<AggregateResult> history = [SELECT Assembly__c, MAX(Assembly_CSN__c)maxAssemCsn, MAX(Assembly_TSN__c)maxAssemTsn, 
                                         MAX(Aircraft_CSN__c)maxAssetCsn, MAX(Aircraft_TSN__c)maxAssetTsn
                                         FROM Asset_History__c 
                                         WHERE Assembly__c = :recordId 
                                         AND Status__c ='Removal' 
                                         AND Status_Change_Date__c = :todayDate 
                                         GROUP BY Assembly__c LIMIT 1];
        if(history.size() > 0) {
            assemblyData.assemblyTSN = (Decimal)history[0].get('maxAssemTsn');
            assemblyData.assetTSN = (Decimal)history[0].get('maxAssetTsn');
            assemblyData.assemblyCSN = (Decimal)history[0].get('maxAssemCsn');
            assemblyData.assetCSN = (Decimal)history[0].get('maxAssetCsn');
        } else {
            assemblyData.assemblyTSN = assembly.TSN__c;
            assemblyData.assetTSN = assembly.Asset__r.TSN__c;
            assemblyData.assemblyCSN = assembly.CSN__c;
            assemblyData.assetCSN = assembly.Asset__r.CSN__c;
        }
        assemblyData.assemblyId = assembly.Id;
        assemblyData.assetId = assembly.Asset__c;
        assemblyData.leaseId = assembly.Asset__r.Lease__c;
        assemblyData.assemblyName = assembly.Name;
        assemblyData.assetName = assembly.Asset__r.Name;
        List<String> approvalStatus = new List<String>{'Approved','Not Required'};
        assemblyData.approvalStatus = approvalStatus;
        assemblyData.reason = '';
        
        if(recordTypeIdAssembly == assembly.RecordTypeId) 
            assemblyData.whereClause = '(RecordTypeId = \''+recordTypeIdAssetAircraft+'\' OR RecordTypeId = \''+recordTypeIdAssetEngine+'\')';
        else
            assemblyData.whereClause = 'RecordTypeId = \''+recordTypeIdAssetAircraft+'\'';
        
        return assemblyData;
    }
    
    @AuraEnabled
    public static AssemblyWrapper getTsnCsnValues(String assemblyData, Id assetChangeId, Date userSelectedDate) {
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        
        Double assetTSNFromUR = null;
        Double assetCSNFromUR = null;
        
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
        Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        Aircraft__c aC = [SELECT Id, Name, TSN__c, CSN__c, Lease__c FROM Aircraft__c WHERE Id=:assetChangeId];
        
        wrapperData.assetId = aC.Id;
        wrapperData.leaseId = aC.Lease__c;
        

        for(Utilization_Report__c utilization : [SELECT TSN_At_Month_End__c, CSN_At_Month_End__c 
                                                 FROM Utilization_Report__c 
                                                 WHERE Aircraft__c = :wrapperData.assetId 
                                                 AND (Month_Ending__c = :lastDayOfMonth 
                                                      OR Month_Ending__c = :lastDayOfPreviousMonth)
                                                 AND Type__c = 'Actual' 
                                                 AND TSN_At_Month_End__c != null 
                                                 AND CSN_At_Month_End__c != null order by End_Date_F__c desc LIMIT 1]) {
                                                     
            assetTSNFromUR = utilization.TSN_At_Month_End__c; 
            assetCSNFromUR = utilization.CSN_At_Month_End__c;
  
        }
        
        if(assetTSNFromUR == null || assetCSNFromUR == null) {
            wrapperData.assetTSN = aC.TSN__c;
            wrapperData.assetCSN = aC.CSN__c;
        }else{
            wrapperData.assetTSN = assetTSNFromUR ;
            wrapperData.assetCSN = assetCSNFromUR ;
        }
        
        return wrapperData;
    }
    
    @AuraEnabled
    public static void saveData(String assemblyData, Date userSelectedDate) {
        
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
        Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        List<Utilization_Report__c> utilizations = [SELECT Current_TSN__c, TSN_At_Month_End__c, Current_CSN__c, CSN_At_Month_End__c 
                                                    FROM Utilization_Report__c 
                                                    WHERE Aircraft__c = :wrapperData.assetId 
                                                    AND (Month_Ending__c = :lastDayOfMonth 
                                                    OR Month_Ending__c = :lastDayOfPreviousMonth)
                                                    AND Type__c = 'Actual' order by End_Date_F__c desc];
        Boolean isACTSNValid = false;
        Boolean isACCSNValid = false;
        
        if(utilizations.size() > 0) {
            
            for(Utilization_Report__c utilization : utilizations) {
                if(wrapperData.assetTSN <= utilization.TSN_At_Month_End__c 
                    && wrapperData.assetTSN >= utilization.Current_TSN__c) {
                       isACTSNValid = true;
                }
                if(wrapperData.assetCSN <= utilization.CSN_At_Month_End__c
                   && wrapperData.assetCSN >= utilization.Current_CSN__c) {
                       isACCSNValid = true;
                }
            }
            
        } else {
            Aircraft__c aC = [SELECT Id, Name, TSN__c, CSN__c FROM Aircraft__c WHERE Id=:wrapperData.assetId];
            
            if(wrapperData.assetTSN == aC.TSN__c) {
                isACTSNValid = true;
            }
            if(wrapperData.assetCSN == aC.CSN__c) {
                isACCSNValid = true;
            }
        }
        if(!isACTSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Install AC TSN');  
        }
        if(!isACCSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Install AC CSN');
        }
        
    }
     @AuraEnabled
    public static String proceedToSave(String assemblyData, Date userSelectedDate){
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        
        if(wrapperData.leaseId == null){
            List<Aircraft__c> aircraftList = [SELECT Lease__c FROM Aircraft__c WHERE Id =: wrapperData.assetId];
            if(aircraftList.size()>0){
                wrapperData.leaseId = aircraftList[0].Lease__c;
            }  
        }
        
        AssemblyTriggerHandler CAInst = new AssemblyTriggerHandler();
        
        CAInst.installAssembly(wrapperData.assemblyId,wrapperData.assemblyName,wrapperData.assetId,userSelectedDate,
                               wrapperData.leaseId,wrapperData.assemblyTSN,wrapperData.assemblyCSN,wrapperData.assetTSN,
                               wrapperData.assetCSN,wrapperData.reason,wrapperData.selectedApprovalStatus);
        
        return wrapperData.assemblyId;
    }
    
    @AuraEnabled
    public static AssemblyWrapper getTsnCsnValuesOnDateChange(String assemblyData, Date userSelectedDate) {
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
        Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        Decimal oldTSN = wrapperData.assetTSN;
        Decimal oldCSN = wrapperData.assetCSN;
        
        wrapperData.assetTSN = null;
        wrapperData.assetCSN = null;
            
        for(Utilization_Report__c utilization : [SELECT TSN_At_Month_End__c, CSN_At_Month_End__c 
                                                 FROM Utilization_Report__c 
                                                 WHERE Aircraft__c = :wrapperData.assetId 
                                                 AND (Month_Ending__c = :lastDayOfMonth 
                                                 OR Month_Ending__c = :lastDayOfPreviousMonth)
                                                 AND Type__c = 'Actual' 
                                                 AND TSN_At_Month_End__c != null 
                                                 AND CSN_At_Month_End__c != null order by End_Date_F__c desc LIMIT 1]) {
            wrapperData.assetTSN = utilization.TSN_At_Month_End__c; 
            wrapperData.assetCSN = utilization.CSN_At_Month_End__c;
               
        }
        if(wrapperData.assetTSN == null || wrapperData.assetCSN == null) {
            wrapperData.assetTSN = oldTSN;
            wrapperData.assetCSN = oldCSN;
        }
        
        return wrapperData;
    }
}