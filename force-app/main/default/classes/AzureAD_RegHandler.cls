global class AzureAD_RegHandler implements Auth.RegistrationHandler {
    
    global User createUser(Id portalId, Auth.UserData data) {
        system.debug('Azure_AD Auth. Provider createUser called');
        //The 'identifier' is where the user ID from Azure AD comes.
        //So search the SF user by that custom field
        User[] users = [SELECT ID, Username FROM User Where AzureId__c = : data.identifier];
        if (users.size() > 0) {
            system.debug('User found, Username = ' + users[0].Username);
            return users[0];
        }
        return null;
    }
    
    global void updateUser(Id userId, Id portalId, Auth.UserData data) {
        system.debug('Azure_AD Auth. Provider updateUser called');
        // Nothing to do here, we don´t want to update fields in SF automatically from Azure AD fields... at least for now.
    }
}