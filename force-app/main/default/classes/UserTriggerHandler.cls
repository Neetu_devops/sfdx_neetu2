public class UserTriggerHandler implements  ITrigger {

    private final string triggerBefore = 'UserTriggerHandlerBefore';
    private final string triggerAfter = 'UserTriggerHandlerAfter';

    public UserTriggerHandler (){}
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    public void bulkAfter(){}
    
    
    public void beforeInsert(){
    }
     
    public void beforeUpdate(){
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){
    }
     
    public void afterInsert(){
        
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        
        system.debug('UserTriggerHandler.afterInsert(+)');
        
		after_insert_call();
        
        system.debug('UserTriggerHandler.afterInsert(-)');    
    }
     
    public void afterUpdate(){
    }
     
    public void afterDelete(){
    }

    public void afterUnDelete(){
    }
     
    public void andFinally(){
    }
    
    private void after_insert_call(){
        try{
            //query Content library.
            String workspaceId;
            
            for(ContentWorkspace workspace :  [select Id from ContentWorkspace where Name = 'All Files' limit 1]){
                workspaceId = workspace.Id;
            }
            
            if(workspaceId != null || Test.isRunningTest()){
                
                //query library permission
                List<ContentWorkspacePermission> permissionList = [SELECT Id FROM ContentWorkspacePermission where Type = 'Author' limit 1]; 
                
                if(permissionList.size()>0){
                    
                    List<ContentWorkspaceMember> libraryMemberList = new List<ContentWorkspaceMember>();
                    
                    
                    for(User userRecord : (List<User>) Trigger.new){
                        libraryMemberList.add(new ContentWorkspaceMember(ContentWorkspaceId = workspaceId,
                                                                         MemberId = userRecord.Id,
                                                                         ContentWorkspacePermissionId = permissionList[0].Id));
                    }
    
                    if(libraryMemberList.size()>0){
                        insert libraryMemberList;
                    }
                }    
            }
        }catch(Exception exp){
        	leasewareUtils.createExceptionLog(exp, 'Exception in User Trigger while creating library member', null, null,null,true); 
        }    
    }      
}