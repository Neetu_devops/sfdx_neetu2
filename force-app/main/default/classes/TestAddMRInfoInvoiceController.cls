/**
    *******************************************************************************
    * Class: TestAddMRInfoInvoiceController
    * @author Created by Bhavna, Lease-Works, 
    * @date  6/13/2019
    * @version 1.0
    * ----------------------------------------------------------------------------------
    * Purpose/Methods:
    *  - Test all the Methods of AddMRInfoInvoiceController.
    *
    * History:
    * - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
    *
    ********************************************************************************
*/
@isTest
public class TestAddMRInfoInvoiceController {
    //deprecated        
}