@isTest
public class TestLLPReplacementController {	   
    @testSetup
    static void testData(){
        //Insert "Aircraft__c" record
        Aircraft__c ac = new Aircraft__c();
        ac.MSN_Number__c = '2416';
        //Handle triggers on insert of record
        LWGlobalUtils.setTriggersflagOn(); 
        insert ac;
        LWGlobalUtils.setTriggersflagOff();
        
        //Engine type for getrecords() 
        String engineType = 'Engine 1';
        
        //Insert "Constituent_Assembly__c" record
        Constituent_Assembly__c ca = new Constituent_Assembly__c();
        ca.Type__c = engineType;
        ca.Attached_Aircraft__c = ac.Id;
        //ca.CSLV__c = 1;
        ca.CSN__c = 2;
        ca.Serial_Number__c = '2416';
        //ca.TSLV__c = 1;
        ca.TSN__c = 2;
        ca.Last_CSN_Utilization__c = 1;
        ca.Last_TSN_Utilization__c = 2;
        //Handle triggers on insert of record
        LWGlobalUtils.setTriggersflagOn(); 
        insert ca;
        LWGlobalUtils.setTriggersflagOff();
        
        //Insert "Scenario_Input__c" record
        Scenario_Input__c si = new Scenario_Input__c();
        insert si;
        
        //Insert "Scenario_Component_Input__c" record
        Scenario_Component_Input__c sci = new Scenario_Component_Input__c();
        sci.Scenario_Input__c = si.Id;
        sci.Name = 'Engine';
        sci.Forecast_Type__c = 'Current Lease';
        sci.Type__c = 'Engine';
        insert sci;
        
        //Insert "Fx_Assembly_Event_Input__c" record
        Fx_Assembly_Event_Input__c aei = new Fx_Assembly_Event_Input__c();
        aei.Fx_Assembly_Input__c = sci.Id;
        aei.Event_Type__c = 'Performance Restoration';
        aei.Assembly__c = ca.Id;
        aei.Name = 'Engine 1 Performance Restoration';
        insert aei;
        
        //Insert "Fx_SV_Override__c" record
        Fx_SV_Override__c sv = new Fx_SV_Override__c();
        sv.Fx_Assembly_Event_Input__c = aei.Id;
        sv.LLP_Replacement_Ids__c = '[{"Id":"a337F000001mDszQAE","LLP_Cost":1000},{"Id":"a337F000001mDt0QAE","LLP_Cost":1000},{"Id":"a337F000001mDt1QAE","LLP_Cost":1000},{"Id":"a337F000001mDt2QAE","LLP_Cost":1000}]';
        insert sv;
        
        //Insert "Sub_Components_LLPs__c" record
        Sub_Components_LLPs__c scLLP = new Sub_Components_LLPs__c();
        scLLP.Name = 'LP TURBINE STAGE 3 ROTOR (LPT)';
        scLLP.Constituent_Assembly__c = ca.Id;
        scLLP.Serial_Number__c = '2416';
        //Text
        scLLP.Part_Number__c = '';
        insert scLLP;
        
        //Insert "Custom_Lookup__c" record
        Custom_Lookup__c cl = new Custom_Lookup__c();
        cl.Lookup_Type__c = 'Engine';
        cl.Active__c = true;
        insert cl;
        
        //Insert "Thrust_Setting__c" record
        Thrust_Setting__c ts = new Thrust_Setting__c(); 
        ts.Remaining_Cycle__c = 1; //(Number)
        ts.Cycle_Used__c = 2; //(Number)
        ts.Cycle_Limit__c = 3; //(Number)
        ts.Life_Limited_Part__c = scLLP.Id;
        ts.Thrust_Setting_Name__c = cl.Id;
        //Handle triggers on insert of record
        LWGlobalUtils.setTriggersflagOn(); 
        insert ts;
        LWGlobalUtils.setTriggersflagOff();
    }
    
    @isTest
    public static void testMethodforLLPRT(){
        String engineType = 'Engine 1';
        List<Aircraft__c> a = [Select id from Aircraft__c LIMIT 1];
        Scenario_Input__c sc = [Select id from Scenario_Input__c];
        
        System.assertEquals(a.size(), 1);

        //Call getrecords() method of LLPReplacementControllor class
        LLPReplacementControllor.getrecords (a[0].Id, engineType);
    }
}