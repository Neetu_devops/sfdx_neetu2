@isTest
private class TestUtility {
    @isTest
    static void testMethod1(){
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;

        Lessor__c setup = new Lessor__c();
        setup.Name= 'Test';
        insert setup;
        
        Operator__c prospect = new Operator__c();
        prospect.Airline_Type_M__c= 'Full Service';
        insert prospect;
		
        Project__c project = new Project__c();
        project.Bid_Field__c = 'Project_Status__c';
        project.Project_Deal_Type__c = 'Lease';
        insert project;
        
        Marketing_Activity__c ma = new Marketing_Activity__c(name ='test MA' , prospect__c =prospect.id, Deal_Status__c = 'LOI', Campaign__c = project.Id);
        insert ma;
        
        Aircraft_Proposal__c testAircraftProposal = new Aircraft_Proposal__c(
            Name='Test',
            Marketing_Activity__c = ma.Id,
            Aircraft__c = aircraft.Id
        );
        insert testAircraftProposal;
        
        Department_Default__c departmentDefault = new Department_Default__c();
        departmentDefault.Name= 'Test';
        departmentDefault.Recipients_on_Complete__c = 'test@gmail.com';
        departmentDefault.Recipients_on_Incomplete__c = 'testincomplete@gmail.com';
        departmentDefault.Setup__c= setup.Id;
        insert departmentDefault;
        
        Deal_Team__c dteam2 = new Deal_Team__c (name ='test deal team final', Marketing_Activity__c = ma.id,final_notification__c =  true);
        dteam2.complete__c = true;
        dteam2.Complete_Message_Enabled__c = true;
        dteam2.Recipients_on_Complete__c = 'test@test.com';
        dteam2.Email_Message_on_Complete__c = 'test';
        dteam2.InComplete_Message_Enabled__c = true;
        dteam2.Recipients_on_InComplete__c = 'test@test.com';
        dteam2.Email_Message_on_InComplete__c = 'test';
        dteam2.final_notification__c = false;
        insert dteam2;
        
        Deal_Team__c dteam3= new Deal_Team__c (name ='test deal team final', Marketing_Activity__c = ma.id,final_notification__c =  true);
        dteam3.complete__c = true;
        dteam3.Complete_Message_Enabled__c = true;
        dteam3.Recipients_on_Complete__c = 'test@test.com';
        dteam3.Email_Message_on_Complete__c = 'test';
        dteam3.InComplete_Message_Enabled__c = true;
        dteam3.Recipients_on_InComplete__c = 'test@test.com';
        dteam3.Email_Message_on_InComplete__c = 'test';
        dteam3.final_notification__c = true;
        insert dteam3;
         
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__r = lease;
        scenarioInp.Lease__c = lease.ID;
        insert scenarioInp;
        
        Scenario_Component__c scenarioComponent = new Scenario_Component__c(
            Name = 'Test Scenario Component',
            Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'Hours', 
            Type__c = 'Engine 1',
            Event_Type__c = 'Performance Restoration', 
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent;
        
        Scenario_Component__c scenarioComponent2 = new Scenario_Component__c(
            Name = 'Test Scenario Component',
            Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'Monthly', 
            Type__c = 'Engine 2',
            Event_Type__c = 'Performance Restoration', 
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent2;

        Scenario_Component__c scenarioComponent3 = new Scenario_Component__c(
            Name = 'Test Scenario Component',
            Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'Cycles', 
            Type__c = 'Airframe',
            Event_Type__c = '4C/6Y', 
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent3;
        
        Monthly_Scenario__c monthlyScenario = new Monthly_Scenario__c(
            Name = 'Test Monthly Scenario',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            CSO__c=1,
            Event_Date__c= System.today(),
            Rent_Amount__c =12, 
            Fx_Component_Output__c = scenarioComponent.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            TSO__c = 2,
            End_Date__c = System.today(),
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 20,
            FH__c = 3,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FC__c =4,
            FC_At_SV__c = 10,
            FH_At_SV__c = 5,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40);
        insert monthlyScenario;
        
        Monthly_Scenario__c monthlyScenario2 = new Monthly_Scenario__c(
            Name = 'Test Monthly Scenario2',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            CSO__c=1,
            End_Date__c = System.today(),
            Event_Date__c= System.today(),
            Rent_Amount__c =11, 
            Fx_Component_Output__c = scenarioComponent2.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            TSO__c = 22,
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 220,
            FH__c = 32,
            FC__c = 14,
            FC_At_SV__c = 1,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FH_At_SV__c = 52,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40);
        insert monthlyScenario2;

        Monthly_Scenario__c monthlyScenario3 = new Monthly_Scenario__c(
            Name = 'Test Monthly Scenario3',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            CSO__c=1,
            End_Date__c = System.today(),
            Rent_Amount__c =11, 
            Fx_Component_Output__c = scenarioComponent3.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            TSO__c = 22,
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 220,
            FH__c = 32,
            FC__c = 14,
            FC_At_SV__c = 1,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FH_At_SV__c = 52,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40);
        insert monthlyScenario3;
        
        List<Monthly_Scenario__c> lstMonthlyScenario = [SELECT Id,Name,SD_F__c,Cash_Flow_F__c ,MR_OR_EOLA_AMount__c,Hours_Remaining__c,Cycles_Remaining__c,Fx_Component_Output__r.Assembly_Display_Name__c,
                Months_Remaining__c,Net_MR__c,Cost_Per_FH__c,Event_Expense_Amount__c,Fx_Component_Output__r.Cost_Per_FH__c,
                Event_Date__c,RateBasis_Value__c,End_Date__c,Start_Date__c,Rent_F__c, Fx_Component_Output__r.Event_Type__c,
                Fx_Component_Output__r.Fx_General_Input__r.EOLA__c,Shortfall_Month__c,Net_EOLA_Payment_Amount__c, MR_At_Return__c,
                Fx_Component_Output__r.FC__c,Return_Month__c,Return_Condition_Status__c, Fx_Component_Output__r.Rate_Basis__c,Fx_Component_Output__r.FH__c, 
                Claim_F__c,Month_Since_Overhaul__c, PR_Cost_F__c, Acc_MR_F__c,Eola_Amount__c,Lowest_Limiter_LLP_Name__c,
                MR_F__c,Fx_Component_Output__r.Name,FH__c,TSN__c,Shortfall_FC__c,Lowest_Limiter_LLP_Cycle_Remaining__c,
                Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c,Eola_Rate__c,
                Fx_Component_Output__r.Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c, EOLA_FH_TSO__c,
                Shortfall_FH__c, CSN__c, FC__c, Fx_Component_Output__r.Type__c,TSO__c,CSO__c,Historical_Month__c,
                Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c, FC_At_SV__c, Total_FH_Cumulative__c,FH_At_SV__c,Current_Rate_Escalated__c,MR_Amount__c FROM Monthly_Scenario__c];
        
        Test.startTest();
        String prefix = LeaseWareUtils.getNamespacePrefix() ;
        Utility.checkAirline();
        Utility.getPicklistvalues(prefix + 'Aircraft_Proposal__c', prefix + 'Project_Status__c');
        Utility.formatDate(system.today());
        Utility.formatYear(system.today());
        Utility.getFieldLabels( new List<String>{'Scenario_Input__c','Fx_Assembly_Event_Input__c'});
        try {
            Utility.setMonthlyData(scenarioInp,lstMonthlyScenario);
        }
        catch(Exception e) {
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
        
        Utility.sendEmailToAllDepartments(new List<Id>{ dteam2.Id });
        Utility.isForecastGenerated(scenarioInp.Id);
        Utility.calculateBidSummary(new Set<Id>{project.Id});
        Utility.EMAIL_MSG_ON_New_Revision(new List<Id>{ dteam2.Id, dteam3.Id});
	
	//Added by Bhavna, 09/30/2019
        Utility.getMARecordType('Aircraft', 'Lease', leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c');
        Utility.getMARecordType('Engine', 'Lease', leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c');
        Test.stopTest();
		
		Account acc = new Account(name= 'Test');
        insert acc;
        delete acc;
        
        Database.DeleteResult[] result = Database.Delete(new List<Account>{acc}, false);
        Utility.generateExceptionLogs(result,'Account');
    }
}