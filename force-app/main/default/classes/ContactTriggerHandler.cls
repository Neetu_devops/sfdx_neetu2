/*
    Created By: Bobby Mathew
    
    Decription : Trigger Handler for standard object Contact.
        Date : 2018/08/14

*/


public class ContactTriggerHandler implements ITrigger{

    // Constructor
    private final string triggerBeforeIns = 'ContactTriggerHandlerBeforeIns';
    private final string triggerBeforeUpd = 'ContactTriggerHandlerBeforeUpd';
    private final string triggerBeforeDel = 'ContactTriggerHandlerBeforeDel';
    private final string triggerAfterIns = 'ContactTriggerHandlerAfterIns';
    private final string triggerAfterUpd = 'ContactTriggerHandlerAfterUpd';
    
    private static map<string, Id> mapOps; //Operators
    private static map<string, Id> mapLessors;//Lessors
    private static map<string, Id> mapCounterparties;//Counterparties

    private static boolean bCompanyChaged;
             
    public ContactTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        bCompanyChaged = false;
        
        if(trigger.isDelete)return;
        /************************Update the Company if Account Id is not null********************************************* */
        Set<Id> accountIds = new Set<Id>();
        map<Id,Account> accountMap;
        for(Contact curContact :  (list<Contact>) trigger.new){
            if(curContact.AccountId!=null) accountIds.add(curContact.AccountId);
        }
        if(accountIds!=null && accountIds.size()>0){
            accountMap = new map<Id,Account>([select Id,Name from Account where ID IN : accountIds]);
            system.debug('accountMap---'+accountMap);
        }   
        Contact oldContact;
        for(Contact curCt : (list<Contact>) trigger.new){
            If(Trigger.IsUpdate) oldContact= (Contact)Trigger.oldMap.get(curCt.Id);
            if(Trigger.isInsert || (Trigger.isUpdate && curCt.AccountId!=oldContact.AccountId)){
               
                if(curCt.AccountId!=null){
                    curCt.Company__c = accountMap==null?null:accountMap.get(curCt.AccountId)==null?null:accountMap.get(curCt.AccountId).Name;
                    system.debug('Company__c------'+curCt.Company__c );
                }
            }
        }
        
        if(trigger.isUpdate){
            for(Contact curContact : (list<Contact>) trigger.new){
                string oldCompany = ((Contact)trigger.oldMap.get(curContact.Id)).Company__c;
                if((oldCompany == null && curContact.Company__c != null) || (oldCompany != null && curContact.Company__c == null) || (oldCompany != curContact.Company__c)){
                    bCompanyChaged=true;
                    break;
                }
            }
        }
        if(!(trigger.isInsert || bCompanyChaged))return;

        mapOps = new map<string, Id>(); //Operators
        mapLessors = new map<string, Id>();//Lessors
        mapCounterparties= new map<string, Id>();//Counterparties

        list<Operator__c> listOps = [select id, Name, Alias__c from Operator__c];
        for(Operator__c curOp : listOps){
            string opName = curOp.Name.toLowerCase();
            mapOps.put(opName, curOp.Id);
            if(curOp.Alias__c==null)continue;
            for(string curName : curOp.Alias__c.split(',')){
                opName = curName.trim().toLowerCase();
                if(opName!='')mapOps.put(opName, curOp.Id);
            }
        }
        list<Counterparty__c> listLessors = [select id, Name from Counterparty__c];
        for(Counterparty__c curLessor : listLessors){
            mapLessors.put(curLessor.Name.toLowerCase(), curLessor.Id);
        }
        list<Bank__c> listCPs = [select id, Name,Alias__c from Bank__c];
        for(Bank__c curCP : listCPs){
            String cpName = curCP.Name.toLowerCase();
            mapCounterparties.put(cpName, curCP.Id);
            if(curCP.Alias__c == null)continue;
            for(string curName : curCP.Alias__c.split(',')){
                cpName = curName.trim().toLowerCase();
                if(cpName!='')mapCounterparties.put(cpName, curCP.Id);
            }
        }
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBeforeIns);
        
        system.debug('ContactTriggerHandler.beforeInsert(+)');
        
        setCompany();

        system.debug('ContactTriggerHandler.beforeInsert(+)');      
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBeforeUpd)) return;
        LeaseWareUtils.setFromTrigger(triggerBeforeUpd);
        system.debug('ContactTriggerHandler.beforeUpdate(+)');

        if(bCompanyChaged) setCompany();// If company is changed, set Operator/Lessor/CP
        else updateCmpOnChangeofOprLessorCP();//If Opr/Lessor/CP is changed update Company

        system.debug('ContactTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        system.debug('ContactTriggerHandler.beforeDelete(+)');

        system.debug('ContactTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfterIns);
        system.debug('ContactTriggerHandler.afterInsert(+)');
        
        system.debug('ContactTriggerHandler.afterInsert(-)');       
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfterUpd)) return;
        LeaseWareUtils.setFromTrigger(triggerAfterUpd);
        system.debug('ContactTriggerHandler.afterUpdate(+)');
        

        
        system.debug('ContactTriggerHandler.afterUpdate(-)');       
    }
     
    public void afterDelete()
    {
        system.debug('ContactTriggerHandler.afterDelete(+)');
        
        
        system.debug('ContactTriggerHandler.afterDelete(-)');       
    }

    public void afterUnDelete()
    {
        system.debug('ContactTriggerHandler.afterUnDelete(+)');
        
        system.debug('ContactTriggerHandler.afterUnDelete(-)');         
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {

    }

    private void setCompany(){

        if(!(trigger.isInsert || bCompanyChaged))return;
       
        list<contact> listCts = (list<contact>)trigger.new;
        for(contact curCt: listCts){
           // When Company is filled/changed, search for an Operator/Lessor/Counterparty (in that order) and fill the appropriate "Related To" field
            if(curCt.Company__c!=null){
            
            if((Trigger.isInsert && curCt.Operator__c == null) || bCompanyChaged)  {        
                Id OpId=mapOps.get(curCt.Company__c.toLowerCase());
                curCt.Operator__c= OpId;
               
            }
            if((Trigger.isInsert && curCt.Counterparty__c == null) || bCompanyChaged)  {     
             
                Id CPId=mapCounterparties.get(curCt.Company__c.toLowerCase()); 
                curCt.Counterparty__c = CPId;
                
            }
            if((Trigger.isInsert && curCt.Lessor__c == null) || bCompanyChaged)  { 
           
                Id LessorId=mapLessors.get(curCt.Company__c.toLowerCase()); 
                curCt.Lessor__c = LessorId;
                
            }
            
        }else {
           // When Operator, Lessor or Counterparty is filled/changed, modify the name in Company with the value in Operator (if any), else Lessor (if any), else Counterparty.
           updateCompanyField(curCt);
        }
        }
    }

   
    private void updateCmpOnChangeofOprLessorCP(){
        system.debug('Update Company Based on Opr/Lessor/CP present');
		list<Contact> listCts = (list<contact>)trigger.new;
       
        boolean oprlessorCPUpdated;
		for(Contact curCt : listCts){
            oprlessorCPUpdated = false;
            if(Trigger.isUpdate){
                Contact oldCt = ((Contact)trigger.oldMap.get(curCt.Id));
                if(oldCt.Operator__c!=curCt.Operator__c || oldCt.Lessor__c!=curCt.Lessor__c || oldCt.Counterparty__c!=curCt.CounterParty__c){
                    oprlessorCPUpdated = true;
                }
            }
			
			if(Trigger.IsInsert || oprlessorCPUpdated){
                updateCompanyField(curCt);
			}            
		}


    }
    private void updateCompanyField(Contact curCt){
        curCt.Company__c ='';
        if(curCt.Operator__c!=null){
            curCt.Company__c = curCT.Y_Hidden_Operator_Name__c;
            return ;
        }
        if(curCt.Lessor__c!=null){
            curCt.Company__c = curCT.Y_Hidden_Lessor_Name__c;
            return;
        }
        if(curCt.Counterparty__c!=null){
            curCt.Company__c = curCT.Y_Hidden_Counter_Party_Name__c;
            return;
        }

    }

}