@isTest
public class TestUpdatePortfolio {

  static void testGeneralMethods(Marketing_Activity__c m1, Aircraft_Proposal__c AT1){
      String fieldSet = 'Fields_For_Editing_On_Bid_Matrix';
      String namespace = LeaseWareUtils.getNamespacePrefix();
      if(!String.isBlank(namespace))
          fieldSet = namespace + fieldSet;
      String fieldst = UpdatePortfolioController.getColumns(AT1.Id,fieldSet);
      system.assertNotEquals(fieldst,null);
      String ft = UpdatePortfolioController.getColumns(AT1.Id, '');
      system.assertEquals(ft,null);
      UpdatePortfolioController.getColumns(m1.Id, '');
      String type = UpdatePortfolioController.getAIDDealType(m1.Id);
      system.assertEquals(type,'Lease');
      UpdatePortfolioController.getFieldSetBasedOnType(m1.Id);
      String aidList = UpdatePortfolioController.getAIDList(m1.Id, fieldSet);
      system.assertNotEquals(aidList,null);
      UpdatePortfolioController.getAIDList(m1.Id,'');
  }  
    
  @TestSetup
  static void makeData(){
    list<Custom_Lookup__c> listLooupks = new list<Custom_Lookup__c>();
		listLooupks.add(new Custom_Lookup__c(Name=' 1',Active__c=true,Lookup_Type__c ='CONVERT_DEALS_TO_LEASES' ,Sub_LookupType__c='Lease',Source__c='Aircraft_Proposal__c:Deal_Analysis__r.Base_Rent__c',Target__c='Lease__c:Base_Rent__c'));
    listLooupks.add(new Custom_Lookup__c(Name=' 2',Active__c=true,Lookup_Type__c ='CONVERT_DEALS_TO_LEASES',Sub_LookupType__c='Lease',Source__c='Aircraft_Proposal__c:Deposit__c' ,Target__c='Lease__c:Securit_Deposit__c'));
    
    listLooupks.add(new Custom_Lookup__c(Name=' 4',Active__c=true,Lookup_Type__c ='CONVERT_DEALS_TO_LEASES',Sub_LookupType__c='SLB',Source__c='Aircraft_Proposal__c:Purchase_Price__c',Target__c='Aircraft__c:Purchase_Price__c'));
    listLooupks.add(new Custom_Lookup__c(Name=' 5',Active__c=true,Lookup_Type__c ='CONVERT_DEALS_TO_LEASES',Sub_LookupType__c='SLB',Source__c='Aircraft_Proposal__c:Engine_Type_new__c',Target__c='Aircraft__c:Engine_Type__c'));

    insert listLooupks;
  }


  @IsTest
  static void testAidList(){
    map<String,Object> mapInOut = new map<String,Object>();
    // Create Operator
    mapInOut.put('Operator__c.Name','Air India');
    TestLeaseworkUtil.createOperator(mapInOut);
    Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
    
    mapInOut.put('Aircraft__c.msn','MSN1');
    mapInOut.put('Aircraft__c.New_Used__c','New');
    TestLeaseworkUtil.createAircraft(mapInOut);
    Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id');        
           
    Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                        Deal_Type__c='Lease' , Deal_Status__c = 'LOI',
                                                        Deal_Name_Format__c= 'Prospect + MSNs',
                                                        Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                       
                                                        Asset_Type__c='Aircraft');
    insert m1;
    system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
    
    Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, Aircraft__c= Aircraft1,Est_All_In_Cost__c =12,Quoted_Date__c = system.today(),Est_LRF__c = 1, Annual_EBT__c= 12, IRR__c= 1, Rent__c=123, Term_Mos__c=1);
    insert AT1;

    //UpdatePortfolioController.getAIDList(m1.id);
    String s = UpdatePortfolioController.checkValidDealForUpdatePort(m1.id);
    system.assert(String.isEmpty(s));
    Id idRecordType=Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByDeveloperName().get('Payment_Deferral_Request').getRecordTypeId();
        
    Marketing_Activity__c m2= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
    Deal_Type__c='Payment Deferral Request' , Deal_Status__c = 'Request Received', RecordTypeId = idRecordType, 
    Deal_Name_Format__c= 'Prospect + MSNs',
    Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
   
    Asset_Type__c='Aircraft');
    insert m2;
   
    String s1 = UpdatePortfolioController.checkValidDealForUpdatePort(m2.id);
    system.assert(String.isNotEmpty(s1));
      
    testGeneralMethods(m1, AT1);  
  }
    @isTest
    static void testUpdatePortfolioLease(){
        // TO DO: implement unit test
        map<String,Object> mapInOut = new map<String,Object>();
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
        
        mapInOut.put('Aircraft__c.msn','MSN1');
        mapInOut.put('Aircraft__c.New_Used__c','New');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id');        
               
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                            Deal_Type__c='Lease' , Deal_Status__c = 'Pipeline',
                                                            Deal_Name_Format__c= 'Prospect + MSNs',
                                                            Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                           
                                                            Asset_Type__c='Aircraft');
        insert m1;
        system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
        
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, Aircraft__c= Aircraft1,Est_All_In_Cost__c =12,Quoted_Date__c = system.today(),Est_LRF__c = 1, Annual_EBT__c= 12, IRR__c= 1, Rent__c=123, Term_Mos__c=1);
        insert AT1;

        Id idRecordTypeCommercialTerm=Schema.SObjectType.Pricing_Run_New__c.getRecordTypeInfosByDeveloperName().get('Lease').getRecordTypeId();
        
        Pricing_Run_New__c testPricingRunNew = new Pricing_Run_New__c(
                Name='Test',
                Current_Rent__c= 2000,
                EBT__c = 20,
                IRR__c = 2,
                Lease_Rate_Factor__c = 2,
                Base_Rent__c =1000,
                Purchase_Price_For_SLBs_Only__c = 20,
                Rate_Type__c = 'Fixed/Fixed',
                Marketing_Activity__c = m1.Id,
                RecordTypeId = idRecordTypeCommercialTerm
            );
        insert testPricingRunNew;

        AT1.Deal_Analysis__c = testPricingRunNew.id;
        LeasewareUtils.clearFromTrigger();
        update AT1;

        UpdatePortfolioController.savePortfolio('Lease',AT1.id);
        Aircraft_Proposal__c AT2 = [select Id,Lease__c,Lease__r.Aircraft__c from  Aircraft_Proposal__c where Id = : AT1.id];
        system.assert(AT2.Lease__c!=null);
        system.assertEquals(AT2.Lease__r.Aircraft__c,aircraft1);
        
      
      }
    

      @isTest
      static void testUpdatePortfolioLeaseExtension(){
          // TO DO: implement unit test
          map<String,Object> mapInOut = new map<String,Object>();
          // Create Operator
          mapInOut.put('Operator__c.Name','Air India');
          TestLeaseworkUtil.createOperator(mapInOut);
          Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
          
          mapInOut.put('Aircraft__c.msn','MSN1');
          mapInOut.put('Aircraft__c.New_Used__c','New');
          TestLeaseworkUtil.createAircraft(mapInOut);
          Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id');   
          
          Date StartDate=  Date.newInstance(2020,1,1);
          Date EndDate=  Date.newInstance(2020,12,31);  

          Lease__c lease = new Lease__c(name='Test',Lease_End_Date_New__c=EndDate,Lease_Start_Date_New__c=StartDate,Aircraft__c=Aircraft1);
          insert lease;

          Aircraft__c ac1 = [select Id,Name from Aircraft__c where ID =:Aircraft1];
          ac1.Lease__c = lease.id;
          update ac1;
                 
          Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                              Deal_Type__c='Lease Extension' , Deal_Status__c = 'Pipeline',
                                                              Deal_Name_Format__c= 'Prospect + MSNs',
                                                              Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                             
                                                              Asset_Type__c='Aircraft');
          insert m1;
          system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
          
          Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, Aircraft__c= Aircraft1,Est_All_In_Cost__c =12,Quoted_Date__c = system.today(),Est_LRF__c = 1, Annual_EBT__c= 12, IRR__c= 1, Rent__c=123);
          insert AT1;
  		 
          UpdatePortfolioController.savePortfolio('Lease Extension',AT1.id);
          Aircraft_Proposal__c aid = [select Id,Lease__c from Aircraft_Proposal__c ];
          system.assertEquals(aid.Lease__c,null,'Lease will not be attached on AID due to Terms of Mos being empty');
          aid.Term_Mos__c =12;
          update aid;
          Aircraft_Proposal__c aid1 = [select Id,Lease__c,Term_Mos__c from Aircraft_Proposal__c ];
          
          UpdatePortfolioController.savePortfolio('Lease Extension',aid1.id);
          Aircraft_Proposal__c AT2 = [select Id,Lease__c,Lease__r.Aircraft__c,Aircraft__r.Lease__c from  Aircraft_Proposal__c where Id = : AT1.id];
          Date d = lease.Lease_End_Date_New__c;
          Lease__c lease1 = [select Id, Name,Lease_End_date_new__c from Lease__c where Id = :lease.Id];
          system.assertEquals(lease1.Lease_End_Date_New__c,d.addMonths(Integer.valueOf(aid1.Term_Mos__c)));
          //calling again Update Portfolio will not extend the Lease End Date but will be same as it was 
          UpdatePortfolioController.savePortfolio('Lease Extension',AT1.id);
          Lease__c lease2 = [select Id, Name,Lease_End_date_new__c from Lease__c where Id = :lease1.Id];
          system.assertEquals(lease2.Lease_End_Date_New__c,d.addMonths(Integer.valueOf(aid1.Term_Mos__c)));
          
       }


       @isTest
       static void testUpdatePortfolioSLB(){
          // TO DO: implement unit test
        map<String,Object> mapInOut = new map<String,Object>();
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                            Deal_Type__c='SLB' , Deal_Status__c = 'Pipeline',
                                                            Deal_Name_Format__c= 'Prospect + MSNs',
                                                            Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                            Asset_Type__c='Aircraft');
        insert m1;
        system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
        
        Global_Fleet__c gf = new Global_Fleet__c();
        gf.Name = 'testWorldFleet';
        gf.Aircraft_External_ID__c = 'type-msn';
        gf.MTOW__c = 134453;
        gf.BuildYear__c = '1996';
        gf.SeatTotal__c = 12;
        gf.AircraftType__c = 'A320';
        gf.AircraftVariant__c = '400';
        gf.EngineType__c = 'V2524';
        gf.AircraftSeries__c = 'AStest';
        gf.EngineVariant__c = 'EVtest';
        gf.OriginalOperator__c ='testOpr';
        gf.Aircraft_Operator__c = OperatorId1;
        gf.SerialNumber__c ='TESTMSN';
        
        insert gf;

        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', World_Fleet_Aircraft__c = gf.id,Marketing_Activity__c = m1.Id,Est_All_In_Cost__c =12,Quoted_Date__c = system.today(),Est_LRF__c = 1, Annual_EBT__c= 12, IRR__c= 1, Rent__c=123, Term_Mos__c=1);
        insert AT1;

        UpdatePortfolioController.savePortfolio('SLB',AT1.id);

        Aircraft_Proposal__c AT2 = [select Id,Aircraft__c,Lease__c,Lease__r.Aircraft__c from  Aircraft_Proposal__c where Id = : AT1.id];
        system.assert(AT2.Lease__c!=null);
        system.assert(AT2.Aircraft__c!=null);
       

       }


       @IsTest
       static void testUpdatePortfolioSale(){

        map<String,Object> mapInOut = new map<String,Object>();
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
        
        mapInOut.put('Aircraft__c.msn','MSN1');
        mapInOut.put('Aircraft__c.New_Used__c','New');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id');   
        
        Date StartDate=  Date.newInstance(2020,1,1);
        Date EndDate=  Date.newInstance(2020,12,31);  

        Lease__c lease = new Lease__c(name='Test',Lease_End_Date_New__c=EndDate,Lease_Start_Date_New__c=StartDate,Aircraft__c=Aircraft1);
        insert lease;

        Aircraft__c ac1 = [select Id,Name from Aircraft__c where ID =:Aircraft1];
        ac1.Lease__c = lease.id;
        update ac1;
               
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                            Deal_Type__c='Sale' , Deal_Status__c = 'Pipeline',
                                                            Deal_Name_Format__c= 'Prospect + MSNs',
                                                            Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                           
                                                            Asset_Type__c='Aircraft');
        insert m1;
        system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
        
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, Aircraft__c= Aircraft1,Est_All_In_Cost__c =12,Quoted_Date__c = system.today(),Est_LRF__c = 1, Annual_EBT__c= 12, IRR__c= 1, Rent__c=123, Term_Mos__c=10);
        insert AT1;

        UpdatePortfolioController.savePortfolio('Sale',AT1.id);
        Aircraft__c ac2 = [select Id,Name,Sold__c from Aircraft__c where ID =:Aircraft1];

        Lease__c lease1 = [select Id,Name,Lease_Type_Status__c from Lease__c where ID =:lease.id];
        system.assertEquals('Lease Expired/Terminated',lease1.Lease_Type_Status__c);
        system.assertEquals(true,ac2.Sold__c);

       }

       @IsTest
       private static void testTermMosEmptyLease(){
          // TO DO: implement unit test
        map<String,Object> mapInOut = new map<String,Object>();
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
        
        mapInOut.put('Aircraft__c.msn','MSN1');
        mapInOut.put('Aircraft__c.New_Used__c','New');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id');        
            
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                            Deal_Type__c='Lease' , 
                                                             Deal_Status__c = 'Pipeline',
                                                            Deal_Name_Format__c= 'Prospect + MSNs',
                                                            Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                           
                                                            Asset_Type__c='Aircraft');
        insert m1;
        system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
        
        Aircraft_Proposal__c AT1 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m1.Id, Aircraft__c= Aircraft1,Est_All_In_Cost__c =12,Quoted_Date__c = system.today(),Est_LRF__c = 1, Annual_EBT__c= 12, IRR__c= 1, Rent__c=123);
        insert AT1;

        UpdatePortfolioController.savePortfolio('Lease',AT1.id);
        LeasewareUtils.clearFromTrigger();
        Id  idRecordType=Schema.SObjectType.Marketing_activity__c.getRecordTypeInfosByDeveloperName().get('Lease_Extension').getRecordTypeId();
        
        Marketing_Activity__c m2= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
        Deal_Type__c='Lease Extension' , Deal_Status__c = 'Pipeline', RecordTypeId = idRecordType,
        Deal_Name_Format__c= 'Prospect + MSNs',
        Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
       
        Asset_Type__c='Aircraft');
        insert m2;
        system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
        Id idRecordTypeAID=Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByDeveloperName().get('Lease_Extension').getRecordTypeId();
           
        Aircraft_Proposal__c AT2 = new Aircraft_Proposal__c(Name='Dummy', Marketing_Activity__c = m2.Id,
        RecordTypeId = idRecordTypeAID, Aircraft__c= Aircraft1,Est_All_In_Cost__c =12,Quoted_Date__c = system.today(),Est_LRF__c = 1, Annual_EBT__c= 12, IRR__c= 1, Rent__c=123);
        insert AT2;
        
        UpdatePortfolioController.savePortfolio('Lease Extension',AT2.id);
       }


       @IsTest
       private static void testCSExceptionCase(){
        LW_Setup__c setup = new LW_Setup__c(name = 'DEAL_STATUS_FOR_AUTO_DEAL_CONVERSION', Value__c = 'Documentation', Disable__c = false, Description__c = 'When ON: a script will be ran to create all existing Operators/Lessors/Counterparties as Account and the workflow will be activated (to create an account every time a new Operator/Lessor/Counterparty is created by the user.Default Value: OFF');
        insert setup;

        map<String,Object> mapInOut = new map<String,Object>();
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id'); 
        
        mapInOut.put('Aircraft__c.msn','MSN1');
        mapInOut.put('Aircraft__c.New_Used__c','New');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id');   
        
        Date StartDate=  Date.newInstance(2020,1,1);
        Date EndDate=  Date.newInstance(2020,12,31);  

        Lease__c lease = new Lease__c(name='Test',Lease_End_Date_New__c=EndDate,Lease_Start_Date_New__c=StartDate,Aircraft__c=Aircraft1);
        insert lease;

        Aircraft__c ac1 = [select Id,Name from Aircraft__c where ID =:Aircraft1];
        ac1.Lease__c = lease.id;
        update ac1;
               
        Marketing_Activity__c m1= new Marketing_Activity__c(Name='Dummy',Prospect__c =OperatorId1,
                                                            Deal_Type__c='Sale' , Deal_Status__c = 'Market Intel',
                                                            Deal_Name_Format__c= 'Prospect + MSNs',
                                                            Aircraft_Engine_Type__c='A320',Asset_Type_2__c='A321',
                                                           
                                                            Asset_Type__c='Aircraft');
        insert m1;
        system.debug('DealName Format Test Class----'+ m1.Deal_Name_Format__c);
        
      
        UpdatePortfolioController.checkValidDealForUpdatePort(m1.id);
       }
       
      
  }