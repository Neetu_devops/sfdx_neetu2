public class ReconciliationController {

    @auraEnabled
    public static ReconciliationScheduleController.ReconciliationScheduleDataWrapper getReconciliationRecordData(String recordId){
       
        //get the current reconciliation Schedule.
        List<Reconciliation_Schedule__c> rSRecordList = [Select Id, To_Date__c, From_Date__c, Lease__c, Type__c, Status__c,
                                                         Lease__r.Name, Lease__r.Aircraft__c, Lease__r.Aircraft__r.Name,
                                                         Lease__r.Lease_Start_Date_New__c, Lease__r.Lease_End_Date_New__c
                                                         from Reconciliation_Schedule__c Where Id =: recordId];
        
        ReconciliationScheduleController.ReconciliationScheduleDataWrapper reconciliationSchedule = new ReconciliationScheduleController.ReconciliationScheduleDataWrapper();
        reconciliationSchedule.reconciliationType = rSRecordList[0].Type__c;
        
        //get the reconciliation Schedule just before the current schedule.
        
        if(rSRecordList[0].Status__c == 'Open'){
        List<Reconciliation_Schedule__c> previousRSRecordList = [Select Id, Type__c
                                                                 from Reconciliation_Schedule__c 
                                                                 Where Lease__c =: rSRecordList[0].Lease__c
                                                                AND (Status__c = 'Complete' OR Status__c = 'Reconciled')
                                                                Order By To_Date__c Desc Limit 1];
            if(previousRSRecordList.Size() > 0){
                reconciliationSchedule.reconciliationType = previousRSRecordList[0].Type__c;
            }
        }
        
        String prefix = LeaseWareUtils.getNamespacePrefix();
        
        List<Utility.Option> reconciliationPeriodoptions = Utility.getPicklistvalues(prefix + 'Reconciliation_Schedule__c', prefix + 'Reconciliation_Type__c');
        
        reconciliationSchedule.startDate = rSRecordList[0].From_Date__c;
        reconciliationSchedule.endDate = rSRecordList[0].To_Date__c;        
        reconciliationSchedule.leaseStartDate = rSRecordList[0].Lease__r.Lease_Start_Date_New__c;
        reconciliationSchedule.leaseEndDate = rSRecordList[0].Lease__r.Lease_End_Date_New__c;
        reconciliationSchedule.assetId = rSRecordList[0].Lease__r.Aircraft__c;
        reconciliationSchedule.reconciliationPeriodoptions = reconciliationPeriodoptions;
        reconciliationSchedule.asset = rSRecordList[0].Lease__r.Aircraft__r.Name;
        reconciliationSchedule.leaseId = rSRecordList[0].Lease__c;
        reconciliationSchedule.lease = rSRecordList[0].Lease__r.Name;
        
        
        return reconciliationSchedule;
    }
    @auraEnabled
    public static validateWrapper validateReconciliationRecord(String reconciliationData, String recordId){
        
        ReconciliationScheduleController.ReconciliationScheduleDataWrapper reconciliationScheduleValues = (ReconciliationScheduleController.ReconciliationScheduleDataWrapper)System.JSON.deserialize(reconciliationData,ReconciliationScheduleController.ReconciliationScheduleDataWrapper.class);
        
        List<Reconciliation_Schedule__c> rSRecordList = [Select Id
                                                         from Reconciliation_Schedule__c Where Id =: recordId 
                                                         AND Status__c = 'Reconciled'];
        
        
        if(rSRecordList.size()>0 ){
            throw new AuraHandledException('Reconciliation Schedule is already Reconciled.');
        }
        
        Id leaseId = reconciliationScheduleValues.leaseId;
        Date endDate = reconciliationScheduleValues.endDate.toStartOfMonth().addMonths(1).addDays(-1);
        
        String query = 'Select Id, Type__c from Utilization_Report__c Where Y_hidden_Lease__c =: leaseId AND (Status__c = \'Approved By Lessor\' AND Month_Ending__c =: endDate) ';
        if(reconciliationScheduleValues.reconciliationType == 'Assumed FH and FC'){
           
            query += 'AND (Type__c = \'Actual\' OR Type__c = \'Assumed/Estimated\')';
        }else{
            query += 'AND Type__c = \'Actual\'';
        }
        system.debug('query--- '+query);
        
        Set<String> typeSet = new Set<String>();
        for(Utilization_Report__c uRObj : (List<Utilization_Report__c>)Database.query(query)){
            if(typeSet.size() < 2){
                typeSet.add(uRObj.Type__c);
            }else{
                break;
            }
        }
        
        String msg;
        Boolean displayFooter = false;
        
        if(reconciliationScheduleValues.reconciliationType == 'Assumed FH and FC' && typeSet.size() == 2){
            msg = 'Actual and Assumed Utilization are filed for the selected period and ready for reconciliation. <br/> ';
            msg += 'Do you wish to continue?';
            displayFooter = true;
        }else if(reconciliationScheduleValues.reconciliationType != 'Assumed FH and FC' && typeSet.size() == 1){
            msg ='Actual Utilization are filed for the selected period and ready for reconciliation. <br/> ';
            msg += 'Do you wish to continue?';
            displayFooter = true;
        }else if(reconciliationScheduleValues.reconciliationType == 'Assumed FH and FC' && typeSet.size() != 2){
            msg = ' Actual and Assumed Utilization are missing for the selected period. <br/> ';
            msg += 'Please Utilize "Generate Assumed Utilization" on the Reconciliation record to create assumed utilization for the selected period. <br/> ';
            msg += 'Also record the Actual utilization for the selected period using Utilization Studio before performing the reconciliation. ';
        }else if(reconciliationScheduleValues.reconciliationType != 'Assumed FH and FC' && typeSet.size() == 0){
            msg = 'Actual Utilization are missing for the selected period. <br/> ';
            msg += 'Please record the Actual utilization for the selected period using Utilization Studio before performing the reconciliation.';
        }
        
        return new validateWrapper(msg, displayFooter);
    }
    
    public class validateWrapper{
        @AuraEnabled public String confirmationMsg{get;set;}
        @AuraEnabled public Boolean displayConfirmationFooter{get; set;}
        
        Public validateWrapper(String confirmationMsg, Boolean displayConfirmationFooter){
            this.confirmationMsg = confirmationMsg;
            this.displayConfirmationFooter = displayConfirmationFooter;
        }
    }
}