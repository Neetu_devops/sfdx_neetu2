public class AITHandler implements ITrigger {


    // Constructor
    private final string triggerBefore = 'AITHandlerBefore';
    private final string triggerAfter = 'AITHandlerAfter';

    private list < Consolidated_Intel__c > listATMIs = new list < Consolidated_Intel__c > ();
    private set < Id > setAiTToDel = new set < Id > ();
    private map < Id, Consolidated_Intel__c > mapATMIs = new map < Id, Consolidated_Intel__c > ();

    private map < id, Aircraft__c > mapAircraft;
    Id IdDisposalRecType;
    // Sample comment
    public AITHandler() {}

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {

        if (trigger.isDelete) return;

        IdDisposalRecType = Schema.SObjectType.Aircraft_In_Transaction__c.getRecordTypeInfosByDeveloperName().get('Disposal').getRecordTypeId();
        Aircraft_In_Transaction__c[] listNew = (list < Aircraft_In_Transaction__c > ) trigger.new;
        set < Id > setACIds = new set < Id > ();
        for (Aircraft_In_Transaction__c curAiT: listNew) {
            if (curAiT.Aircraft__c != null) setACIds.add(curAiT.Aircraft__c);
        }
        mapAircraft = new map < id, Aircraft__c > ([select id, MSN_Number__c, Aircraft_Type__c, Aircraft_Variant__c,
            Engine_Type__c, Date_of_Manufacture__c, Lease__r.Operator__c
            from Aircraft__c where id in : setACIds
        ]);

    }

    public void bulkAfter() {
        IdDisposalRecType = Schema.SObjectType.Aircraft_In_Transaction__c.getRecordTypeInfosByDeveloperName().get('Disposal').getRecordTypeId();
        if (trigger.isUpdate) {
            list < Consolidated_Intel__c > listExATMIs = [select id, Name, Aircraft_Type__c, Aircraft_In_Transaction__c
                from Consolidated_Intel__c where Aircraft_In_Transaction__c in
                : (list < Aircraft_In_Transaction__c > ) trigger.new
            ];
            for (Consolidated_Intel__c curATMI: listExATMIs) {
                mapATMIs.put(curATMI.Aircraft_In_Transaction__c, curATMI);
            }
        }
    }

    public void beforeInsert() {
        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);

        system.debug('AITHandler.beforeInsert(+)');
        //  Aircraft_In_Transaction__c[] listNew = (list<Aircraft_In_Transaction__c>)trigger.new ;

        defaultingFieldValues();

        system.debug('AITHandler.beforeInsert(+)');
    }

    public void beforeUpdate() {
        //Before check  : keep code here which does not have issue with multiple call .
        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AITHandler.beforeUpdate(+)');

        //  Aircraft_In_Transaction__c[] listNew = (list<Aircraft_In_Transaction__c>)trigger.new ;

        defaultingFieldValues();

        system.debug('AITHandler.beforeUpdate(-)');
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete() {

        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AITHandler.beforeDelete(+)');

        afterCall();

        system.debug('AITHandler.beforeDelete(-)');

    }

    public void afterInsert() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AITHandler.afterInsert(+)');
        afterCall();
        system.debug('AITHandler.afterInsert(-)');
    }

    public void afterUpdate() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AITHandler.afterUpdate(+)');

        if (LeaseWareUtils.isFromTrigger('CalledFromTxnTermsRefreshProspectSummary')) return;

        afterCall();
        AITRefreshProspectSummary();
        refreshAICOnChangeOfMSN();
        system.debug('AITHandler.afterUpdate(-)');
    }

    public void afterDelete() {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AITHandler.afterDelete(+)');

        system.debug('AITHandler.afterDelete(-)');
    }

    public void afterUnDelete() {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AITHandler.afterUnDelete(+)');

        // code here

        system.debug('AITHandler.afterUnDelete(-)');
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        // insert any audit records

    }

	// before insert,before update
    private void defaultingFieldValues() {
        Aircraft_In_Transaction__c[] listNew = (list < Aircraft_In_Transaction__c > ) trigger.new;

		Aircraft_In_Transaction__c OldAITRec;
        for (Aircraft_In_Transaction__c curAiT: listNew) {

			if(trigger.IsUpdate) OldAITRec = (Aircraft_In_Transaction__c)trigger.oldmap.get(curAiT.id);
            if (curAiT.RecordTypeId == IdDisposalRecType) { //Applies only to Disposal txns.
                system.debug('Disposal..');
                boolean isAicraftChanged = false;
                if (trigger.isUpdate) { //IsUpdate. Check if aircraft assignment changed
                    Aircraft_In_Transaction__c oldAiT = (Aircraft_In_Transaction__c)(trigger.oldMap.get(curAiT.Id));
                    if ((oldAiT.Aircraft__c != null && curAiT.Aircraft__c == null) || (oldAiT.Aircraft__c == null && curAiT.Aircraft__c != null) || (oldAiT.Aircraft__c != curAiT.Aircraft__c)) isAicraftChanged = true;
                } else if (curAiT.Aircraft__c != null) isAicraftChanged = true; //IsInsert and Aircraft is assigned.

                if (isAicraftChanged) {
                    system.debug('Is Changed..');
                    curAiT.Aircraft_Type_New__c = null;
                    curAiT.Aircraft_Variant__c = null;
                    curAiT.Engine__c = null;
                    curAiT.YoM__c = null;
                    curAiT.Operator__c = null;
                    curAiT.Name = curAiT.Y_Hidden_MSN__c;                    
                    if (curAiT.Aircraft__c != null) {
                        Aircraft__c newAC = mapAircraft.get(curAiT.Aircraft__c);
                        curAiT.Aircraft_Type_New__c = newAC.Aircraft_Type__c;
                        curAiT.Aircraft_Variant__c = newAC.Aircraft_Variant__c;
                        curAiT.Engine__c = newAC.Engine_Type__c;
                        curAiT.YoM__c = string.valueOf(newAC.Date_of_Manufacture__c.year());

                        curAiT.Operator__c = newAC.Lease__r.Operator__c;
                    }
                }
            } else if ('Acquisition'.equals(curAiT.Type_F__c) && curAiT.World_Fleet_Assignment__c != null) {
            	
                curAiT.Aircraft_Type_New__c = curAiT.Y_Hidden_Aircraft_Type__c;
                curAiT.Aircraft_Variant__c = curAiT.Y_Hidden_Variant__c;
                curAiT.Name = curAiT.Y_Hidden_MSN__c;
                if (curAiT.Engine__c==null || (OldAITRec!=null && OldAITRec.World_Fleet_Assignment__c != curAiT.World_Fleet_Assignment__c)) curAiT.Engine__c = curAiT.Y_Hidden_Engine_Type__c;
                if(curAiT.YoM__c==null || (OldAITRec!=null && OldAITRec.World_Fleet_Assignment__c != curAiT.World_Fleet_Assignment__c)) curAiT.YoM__c = curAiT.Y_Hidden_YOM__c;
				if(curAiT.Engine_Variant__c==null || (OldAITRec!=null && OldAITRec.World_Fleet_Assignment__c != curAiT.World_Fleet_Assignment__c))curAiT.Engine_Variant__c= curAiT.Y_Hidden_Engine_Variant__c;
				if(curAiT.Operator__c==null || (OldAITRec!=null && OldAITRec.World_Fleet_Assignment__c != curAiT.World_Fleet_Assignment__c))curAiT.Operator__c= curAiT.Y_Hidden_Operator_Id__c;
            }
            //Calculate Equity %
            if (curAit.Equity__c != null && (curAit.Price__c != null && curAit.Price__c > 0)) {
                curAit.Equity_Percentage__c = (curAit.Equity__c / curAit.Price__c) * 100;

            } else if (curAit.Equity__c == null && curAit.Equity_Percentage__c != null && curAit.Price__c != null) {
                curAit.Equity__c = (curAit.Equity_Percentage__c / 100) * curAit.Price__c;
            }

            //Calculate FH:FC ratio and update the mssing field with the value -->Engine 1
            if (curAit.Engine_1_FH__c != null && (curAit.Engine_1_FC__c != null && curAit.Engine_1_FC__c > 0)) {
                curAit.FH_FC_Engine_1__c = curAit.Engine_1_FH__c / curAit.Engine_1_FC__c;
            } else if (curAit.Engine_1_FH__c == null && (curAit.Engine_1_FC__c != null && curAit.FH_FC_Engine_1__c != null)) {
                curAit.Engine_1_FH__c = curAit.Engine_1_FC__c * curAit.FH_FC_Engine_1__c;
            } else if (curAit.Engine_1_FC__c == null && (curAit.Engine_1_FH__c != null && (curAit.FH_FC_Engine_1__c != null && curAit.FH_FC_Engine_1__c > 0))) {
                curAit.Engine_1_FC__c = curAit.Engine_1_FH__c / curAit.FH_FC_Engine_1__c;
            }

            //Calculate FH:FC ratio and update the mssing field with the value -->Engine 2
            if (curAit.Engine_2_FH__c != null && (curAit.Engine_2_FC__c != null && curAit.Engine_2_FC__c > 0)) {
                curAit.FH_FC_Engine_2__c = curAit.Engine_2_FH__c / curAit.Engine_2_FC__c;
            } else if (curAit.Engine_2_FH__c == null && (curAit.Engine_2_FC__c != null && curAit.FH_FC_Engine_2__c != null)) {
                curAit.Engine_2_FH__c = curAit.Engine_2_FC__c * curAit.FH_FC_Engine_2__c;
            } else if (curAit.Engine_2_FC__c == null && (curAit.Engine_2_FH__c != null && (curAit.FH_FC_Engine_2__c != null && curAit.FH_FC_Engine_2__c > 0))) {
                curAit.Engine_2_FC__c = curAit.Engine_2_FH__c / curAit.FH_FC_Engine_2__c;
            }

            //Cycles check -->Engine 1
            if (curAiT.Engine_1_Lowest_Limiter_Cycles__c != null && curAiT.Life_Limit_On_Lowest_Limiter__c != null) {
                curAiT.Cycles_Remaining_Engine_1__c = curAiT.Life_Limit_On_Lowest_Limiter__c - curAiT.Engine_1_Lowest_Limiter_Cycles__c;
            } else if (curAiT.Engine_1_Lowest_Limiter_Cycles__c == null && (curAiT.Cycles_Remaining_Engine_1__c != null &&
                    curAiT.Life_Limit_On_Lowest_Limiter__c != null)) {
                curAiT.Engine_1_Lowest_Limiter_Cycles__c = curAiT.Life_Limit_On_Lowest_Limiter__c - curAiT.Cycles_Remaining_Engine_1__c;
            } else if (curAiT.Life_Limit_On_Lowest_Limiter__c == null && (curAiT.Cycles_Remaining_Engine_1__c != null &&
                    curAiT.Engine_1_Lowest_Limiter_Cycles__c != null)) {
                curAiT.Life_Limit_On_Lowest_Limiter__c = curAiT.Cycles_Remaining_Engine_1__c + curAiT.Engine_1_Lowest_Limiter_Cycles__c;
            }

            //Cycles check -->Engine 2
            if (curAiT.Engine_2_Lowest_Limiter_Cycles__c != null && curAiT.Life_Limit_On_Lowest_Limiter_Eng_2__c != null) {
                curAiT.Cycles_Remaining_Engine_2__c = curAiT.Life_Limit_On_Lowest_Limiter_Eng_2__c - curAiT.Engine_2_Lowest_Limiter_Cycles__c;

            } else if (curAiT.Engine_2_Lowest_Limiter_Cycles__c == null && (curAiT.Cycles_Remaining_Engine_2__c != null &&
                    curAiT.Life_Limit_On_Lowest_Limiter_Eng_2__c != null)) {
                curAiT.Engine_2_Lowest_Limiter_Cycles__c = curAiT.Life_Limit_On_Lowest_Limiter_Eng_2__c - curAiT.Cycles_Remaining_Engine_2__c;
            } else if (curAiT.Life_Limit_On_Lowest_Limiter_Eng_2__c == null && (curAiT.Cycles_Remaining_Engine_2__c != null &&
                    curAiT.Engine_2_Lowest_Limiter_Cycles__c != null)) {
                curAiT.Life_Limit_On_Lowest_Limiter_Eng_2__c = curAiT.Cycles_Remaining_Engine_2__c + curAiT.Engine_2_Lowest_Limiter_Cycles__c;
            }

            // calculate PMT
            try {

                curAiT.Debt_Amortization_Payment__c = 1000000 * leasewareUtils.calculatePMT(leasewareUtils.zeroIfNull(curAiT.Interest_Percentage__c) / (12 * 100),
                    integer.valueOf(leasewareUtils.zeroIfNull(curAiT.Term_Mo__c)), leasewareUtils.zeroIfNull(curAiT.Debt_Amount_M__c), leasewareUtils.zeroIfNull(curAiT.Balloon_Payment__c));
            } catch (exception e) {
                system.debug('Exception ' + e.getMessage());
                curAiT.Debt_Amortization_Payment__c = null;
            }

            // calculate IRR
            if (curAiT.Net_Equity_M__c != null && curAiT.Rent__c != null && curAiT.Term_Mo__c > 0) {
                Integer TermLen = curAiT.Term_Mo__c.intValue();
                //  CF = ((Rent - Management fee ) - Loan Payement )
                Double CF = (leasewareUtils.zeroIfNull(curAiT.Rent__c) - leasewareUtils.zeroIfNull(curAiT.Management_Fee__c)) - leasewareUtils.zeroIfNull(curAiT.Debt_Amortization_Payment__c);
                double[] CashFlow = new double[TermLen + 1];
                system.debug('Rent' + CF);
                system.debug('Capital' + (-1 * leasewareUtils.zeroIfNull(curAiT.Net_Equity_M__c) * 1000000));
                system.debug('Last' + (CF + ((leasewareUtils.zeroIfNull(curAiT.Net_Residual_Value_M__c) * 1000000) - (leasewareUtils.zeroIfNull(curAiT.Balloon_Payment__c) * 1000000))));
                for (Integer i = 0; i <= TermLen; i++) {

                    cashFlow[i] = CF;
                    // Iinitial investment
                    if (i == 0) cashFlow[i] = -1 * leasewareUtils.zeroIfNull(curAiT.Net_Equity_M__c) * 1000000;
                    if (i == TermLen) cashFlow[i] = CF + ((leasewareUtils.zeroIfNull(curAiT.Net_Residual_Value_M__c) * 1000000) - (leasewareUtils.zeroIfNull(curAiT.Balloon_Payment__c) * 1000000));
                }

                curAiT.Y_Hidden_IRR__c = (math.pow(1 + (leasewareUtils.zeroIfNull(leasewareUtils.ggetIRR(cashFlow, 0))), 12) - 1) * 100;
            }

        }

    }

    // after insert, after update, before delete
    private void afterCall() {
    	
    	system.debug('afterCall()+');
        list < Aircraft_In_Transaction__c > cuAiTList = (list < Aircraft_In_Transaction__c > )(trigger.isDelete ? trigger.old : trigger.new);

        try {

            set < id > setTxnIds = new set < id > ();
            set<id> deletedIDs = new set<id>();
            for (Aircraft_In_Transaction__c curAiT: cuAiTList) {
                setTxnIds.add(curAiT.Transaction__c);
                if(trigger.isDelete) deletedIDs.add(curAiT.Id);
            }
            map < id, Transaction__c > mapTxns = new map < id, Transaction__c > ([select Id, MSNs_Marketed_In_This_Transaction__c 
            															,(select name from Aircraft_In_Transaction__r where Id not in :deletedIDs)
            															from Transaction__c where Id in : setTxnIds]);

            Transaction__c parentTxn;
            Aircraft_In_Transaction__c oldMapAIT;
			string nameStr;	
			map<string,Decimal> DuplicateCountCheck = new map<string,Decimal>();
			string keyStr;
			for(Transaction__c curTrx: mapTxns.values()){
				nameStr = null;
				for(Aircraft_In_Transaction__c curAiT:curTrx.Aircraft_In_Transaction__r){
					if(nameStr==null)  nameStr = curAiT.Name;
					else nameStr += ', ' + curAiT.Name;
					
					// set count
					keyStr = curTrx.Id+ curAiT.Name;
					if(DuplicateCountCheck.containskey(keyStr)) DuplicateCountCheck.put(keyStr,DuplicateCountCheck.get(keyStr)+1);
					else DuplicateCountCheck.put(keyStr,1);
				}
				if(nameStr!=null && nameStr.length()>254) nameStr = nameStr.left(251) + '...';
				curTrx.MSNs_Marketed_In_This_Transaction__c=nameStr;
				system.debug('curTrx.MSNs_Marketed_In_This_Transaction__c=='+curTrx.MSNs_Marketed_In_This_Transaction__c);	
			}
			
			LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled
			update mapTxns.values();
			LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger disabled
			
			
            for (Aircraft_In_Transaction__c curAiT: cuAiTList) {
            	keyStr = curAiT.Transaction__c+ curAiT.Name;
            	system.debug('keyStr='+keyStr);
            	system.debug('DuplicateCountCheck='+DuplicateCountCheck);
            	if(DuplicateCountCheck.containsKey(keyStr) && DuplicateCountCheck.get(keyStr)>1){
            		curAiT.addError('MSN ' + curAiT.Name + ' is already present in the transaction.');
                    continue;
            	}

                //parentTxn = mapTxns.get(curAiT.Transaction__c);
                if (trigger.isInsert) {
                	
                    //system.debug(parentTxn.MSNs_Marketed_In_This_Transaction__c + ', ' + curAiT.Name);
                    //if ((parentTxn.MSNs_Marketed_In_This_Transaction__c + ', ').contains(curAiT.Name + ', ')) {
                        //curAiT.addError('MSN ' + curAiT.Name + ' is already present in the transaction.');
                        //continue;
                    //}
                    //parentTxn.MSNs_Marketed_In_This_Transaction__c = (parentTxn.MSNs_Marketed_In_This_Transaction__c == null ? '' : (parentTxn.MSNs_Marketed_In_This_Transaction__c + ', ')) + curAiT.Name;
                    //mapTxns.put(parentTxn.id, parentTxn);

                    if (curAiT.Aircraft_Type_New__c == null || curAiT.RecordTypeId == IdDisposalRecType) continue;
                    Id ACTypeId;
                    try {
                        ACTypeId = [select id from Aircraft_Type__c where Aircraft_Type__c = : curAiT.Aircraft_Type_New__c
                            AND Aircraft_Variant__c = null AND Engine_Type__c = null AND Engine_Variant__c = null order by
                            CreatedDate desc limit 1
                        ].id;
                    } catch (exception e) {
                        Aircraft_Type__c newACType = new Aircraft_Type__c(Name = curAiT.Aircraft_Type_New__c,
                            Aircraft_Type__c = curAiT.Aircraft_Type_New__c);
                        insert newACType;
                        ACTypeId = newACType.id;
                    }
                    listATMIs.add(new Consolidated_Intel__c(Name = 'Dummy', Aircraft_Type__c = ACTypeId,
                        Aircraft_In_Transaction__c = curAiT.Id));

                } else if (trigger.isUpdate) {
                    oldMapAIT = (Aircraft_In_Transaction__c) trigger.oldMap.get(curAiT.Id);
                    //if (!oldMapAIT.Name.equals(curAiT.Name) && (parentTxn.MSNs_Marketed_In_This_Transaction__c + ', ').contains(curAiT.Name + ', ')) {
                        //curAiT.addError('MSN ' + curAiT.Name + ' is already present in the transaction.');
                        //continue;
                    //}
                    //parentTxn.MSNs_Marketed_In_This_Transaction__c = (parentTxn.MSNs_Marketed_In_This_Transaction__c == null ? '' : (parentTxn.MSNs_Marketed_In_This_Transaction__c.replace(oldMapAIT.Name, curAiT.Name)));
                    //mapTxns.put(parentTxn.id, parentTxn);

                    if (oldMapAIT.Aircraft_Type_New__c != null && curAiT.Aircraft_Type_New__c == null)
                        setAiTToDel.add(oldMapAIT.Id);
                    if (curAiT.RecordTypeId == IdDisposalRecType || curAiT.Aircraft_Type_New__c == null || oldMapAIT.Aircraft_Type_New__c == curAiT.Aircraft_Type_New__c) continue;

                    Id ACTypeId;
                    try {
                        ACTypeId = [select id from Aircraft_Type__c where Aircraft_Type__c = : curAiT.Aircraft_Type_New__c
                            AND Aircraft_Variant__c = null AND Engine_Type__c = null AND Engine_Variant__c = null order by
                            CreatedDate desc limit 1
                        ].id;
                    } catch (exception e) {
                        Aircraft_Type__c newACType = new Aircraft_Type__c(Name = curAiT.Aircraft_Type_New__c,
                            Aircraft_Type__c = curAiT.Aircraft_Type_New__c);
                        insert newACType;
                        ACTypeId = newACType.id;
                    }

                    Consolidated_Intel__c exATMI = mapATMIs.get(curAiT.Id);
                    if (exATMI == null) {
                        listATMIs.add(new Consolidated_Intel__c(Name = 'Dummy', Aircraft_Type__c = ACTypeId,
                            Aircraft_In_Transaction__c = curAiT.Id));
                    } else {
                        exATMI.Aircraft_Type__c = ACTypeId;
                        exATMI.Aircraft_In_Transaction__c = curAiT.Id;
                        listATMIs.add(exATMI);
                    }

                } else { //Delete
                    /*if (parentTxn.MSNs_Marketed_In_This_Transaction__c != null) {
                        if (parentTxn.MSNs_Marketed_In_This_Transaction__c.equals(curAiT.Name)) {
                            parentTxn.MSNs_Marketed_In_This_Transaction__c = null;
                        } else if (parentTxn.MSNs_Marketed_In_This_Transaction__c.startsWith(curAiT.Name + ', ')) {
                            parentTxn.MSNs_Marketed_In_This_Transaction__c = parentTxn.MSNs_Marketed_In_This_Transaction__c.remove(curAiT.Name + ', ');
                        } else {
                            parentTxn.MSNs_Marketed_In_This_Transaction__c = parentTxn.MSNs_Marketed_In_This_Transaction__c.remove(', ' + curAiT.Name);
                        }
                        mapTxns.put(parentTxn.id, parentTxn);
                    }
                    */
                    if (curAiT.Aircraft_Type_New__c != null) setAiTToDel.add(curAiT.Id);
                }
            }
            if (setAiTToDel.size() > 0) delete[select id from Consolidated_Intel__c where Aircraft_In_Transaction__c in : setAiTToDel];
            if (listATMIs.size() > 0) upsert listATMIs;
            //update mapTxns.values();
			system.debug('afterCall()-');
        } catch (exception e) {
            system.debug('Parent transaction couldn\'t be updated due to exception ' + e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    // after update
    private void AITRefreshProspectSummary() {

        system.debug('AITRefreshProspectSummary+');
        list < Aircraft_In_Transaction__c > listNew = trigger.new;
        set < Id > setProspects = new set < Id > ();
        Aircraft_In_Transaction__c oldRec;
        
        
        
        map <id,Aircraft_In_Transaction__c> mapAITs = new map<id,Aircraft_In_Transaction__c>([
        		select id 
        			,(select Id, Transaction_Prospect__c
                    from Transaction_Terms__r
                    where Transaction_Prospect__c != null)
                    from Aircraft_In_Transaction__c
                    where id in :listNew
                ]);
        
        list < Transaction_Terms__c > listTTs;
        for (Aircraft_In_Transaction__c curAiT: listNew) {
        	listTTs =null;
        	if(mapAITs.get(curAiT.Id)!=null)   	listTTs = mapAITs.get(curAiT.Id).Transaction_Terms__r;
        	system.debug('listTTs+'+listTTs);
        	if(listTTs==null)continue;
            oldRec = (Aircraft_In_Transaction__c) trigger.oldmap.get(curAiT.Id);
            if (curAiT.Max_Bid__c != oldRec.Max_Bid__c) {
                for (Transaction_Terms__c curTT: listTTs) {
                    setProspects.add(curTT.Transaction_Prospect__c);
                }
            }
        }
        if (setProspects.size() > 0 && !leasewareUtils.isfromFuture()) LeaseWareUtils.RefreshProspectSummary(setProspects);
        system.debug('AITRefreshProspectSummary-');

    }
    
    // after update
    private void refreshAICOnChangeOfMSN() {

        system.debug('refreshAICOnChangeOfMSN+');
        list < Aircraft_In_Transaction__c > listNew = trigger.new;
        set < Id > setAiT_IDs = new set < Id > ();
        Aircraft_In_Transaction__c oldRec;
        
        
        


        for (Aircraft_In_Transaction__c curAiT: listNew) {
            oldRec = (Aircraft_In_Transaction__c) trigger.oldmap.get(curAiT.Id);
            if (curAiT.Name != oldRec.Name) setAiT_IDs.add(curAiT.Id);
        }
        if(setAiT_IDs.size()>0){
        	list<Aircraft_In_Deal__c> listAICs = [select Id, Name, MSN__c,Aircraft_In_Transaction__r.Name
	        			from Aircraft_In_Deal__c where Aircraft_In_Transaction__c in :setAiT_IDs
	        			and Marketing_Deal__r.Campaign_Active__c=true];  
	        			
	        for(Aircraft_In_Deal__c curAICs:listAICs){
	        	curAICs.name = curAICs.Aircraft_In_Transaction__r.Name;
	        }	
	        LeaseWareUtils.setFromTrigger('UpdateProspectOnChangeOf_MSN_Name');
	        Database.SaveResult[] srList = Database.update(listAICs, false);
            LeaseWareUtils.unsetTriggers('UpdateProspectOnChangeOf_MSN_Name');
	      	for (Database.SaveResult sr : srList) {
	      		if (!sr.isSuccess()) {// on failure
	      			for(Database.Error err : sr.getErrors()) {
	      				system.debug('refreshAICOnChangeOfMSN : MSN could not be updated due to exception '+ + err.getStatusCode() + ': ' + err.getMessage() + err.getFields());
	      				LeaseWareUtils.createExceptionLog(null,' refreshAICOnChangeOfMSN : MSN could not be updated due to exception' + err.getStatusCode() + ': ' + err.getMessage() + err.getFields() ,'Aircraft_In_Transaction__c','','Update',false);
	      			}
	      		}
	      	}	
	      	// if any error
	      	LeaseWareUtils.insertExceptionLogs();
        }
        system.debug('refreshAICOnChangeOfMSN-');

    }    

}