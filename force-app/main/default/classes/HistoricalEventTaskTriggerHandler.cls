public class HistoricalEventTaskTriggerHandler implements ITrigger{
    
    // Constructor
    private final string triggerBefore = 'HistoricalEvntTaskTriggerHandlerBefore';
    private final string triggerAfter = 'HistoricalEvntTaskTriggerHandlerAfter';
       
    public HistoricalEventTaskTriggerHandler ()
    {   
    }
    //Making functionality more generic.
   
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {

    }
    public void bulkAfter()
    {

    }   
    public void beforeInsert()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('HistoricalEventTaskTriggerHandler.beforeInsert(+)');
        list<Historical_Event_Task__c> ListHET =(list<Historical_Event_Task__c>)trigger.new;
        for(Historical_Event_Task__c curHET: ListHET){
            curHET.Y_Hidden_Key__c=curHET.Historical_Event__c+curHET.Name; // Used in Duplicate Rule
        }
        system.debug('HistoricalEventTaskTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('HistoricalEventTaskTriggerHandler.beforeUpdate(+)');
        list<Historical_Event_Task__c> ListHET =(list<Historical_Event_Task__c>)trigger.new;
        for(Historical_Event_Task__c curHET: ListHET){
            curHET.Y_Hidden_Key__c=curHET.Historical_Event__c+curHET.Name; // Used in Duplicate Rule
        }
        system.debug('HistoricalEventTaskTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        
    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('HistoricalEventTaskTriggerHandler.afterInsert(+)');
        //Calculate Qualified for Claim and Eligible Claim Amount in the Historical Event Record After creation/Updation of Historical Event task records for that Historical Event
        Historical_Event_Task__c[] ListHistoricalEventTaskIds = (list < Historical_Event_Task__c >)trigger.New ;
        
        CalQualifiedEligibleClaim(ListHistoricalEventTaskIds);
        system.debug('HistoricalEventTaskTriggerHandler.afterInsert(-)'); 
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('HistoricalEventTaskTriggerHandler.afterUpdate(+)');
        //Calculate Qualified for Claim and Eligible Claim Amount in the Historical Event Record After creation/Updation of Historical Event task records for that Historical Event
        Historical_Event_Task__c[] ListHistoricalEventTaskIds = (list < Historical_Event_Task__c >)trigger.New ;
        
        CalQualifiedEligibleClaim(ListHistoricalEventTaskIds);
        system.debug('HistoricalEventTaskTriggerHandler.afterUpdate(-)'); 
    }
     
    public void afterDelete()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('HistoricalEventTaskTriggerHandler.afterDelete(+)');
        //Calculate Qualified for Claim and Eligible Claim Amount in the Historical Event Record After deletion of Historical Event task records for that Historical Event
        Historical_Event_Task__c[] ListHistoricalEventTaskIds = (list < Historical_Event_Task__c >)trigger.old ;
        
        CalQualifiedEligibleClaim(ListHistoricalEventTaskIds);
        system.debug('HistoricalEventTaskTriggerHandler.afterDelete(-)');
    }

    public void afterUnDelete()
    {
        system.debug('AssemblyMRRateTriggerHandler.afterUnDelete(+)');
        
        system.debug('AssemblyMRRateTriggerHandler.afterUnDelete(-)'); 
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
    }
    //After Insert, After Update, After Delete
    //Purpose: Calculate Qualified for Claim and Eligible Claim Amount in the Historical Event Record After creation/Updation/Deletion of Historical Event task records for that Historical Event
    // Input parameter: List<Historical_Event_Task__c>
    private void CalQualifiedEligibleClaim(List<Historical_Event_Task__c> ListHistoricalEventTaskIds){
        system.debug('HistoricalEventTaskTriggerHandler.CalQualifiedEligibleClaim(+)');
        
        system.debug('size of ListHistoricalEventTaskIds='+ListHistoricalEventTaskIds.size());
        set<id> setHistEventIds= new set<id>();
        map<id, List<Historical_Event_Task__c>> mapHisEvent_HETIds = new map<id, List<Historical_Event_Task__c>>();  // map<Historical Event id, List<Historical Event Task Ids>>  
		map<id, Assembly_Eligible_Event__c> mapHisEvent= new map<id, Assembly_Eligible_Event__c>();
		
        
        for(Historical_Event_Task__c curHistEvntTaskRec :ListHistoricalEventTaskIds){
            setHistEventIds.add(curHistEvntTaskRec.Historical_Event__c);
        }
        system.debug('size of setHistEventIds='+setHistEventIds.size());
        if(setHistEventIds.size()>0){
            for(Assembly_Eligible_Event__c curHistoricalEventRec:[  select id,Y_Hidden_QualifiedClaim__c,Y_Hidden_Eligible_Claim_Amount__c,
                                                                        (select id,name, Performed__c, Qualified_Amount_F__c, Required__c
                                                                        from Historical_Event_Tasks__r)
                                                                    from Assembly_Eligible_Event__c
                                                                    where id in : setHistEventIds ]){
                for(Historical_Event_Task__c curHETRec: curHistoricalEventRec.Historical_Event_Tasks__r){
                    if( !mapHisEvent_HETIds.containsKey(curHistoricalEventRec.id)){
                        mapHisEvent_HETIds.put(curHistoricalEventRec.id,new List<Historical_Event_Task__c>());
                    }
                    mapHisEvent_HETIds.get(curHistoricalEventRec.id).add(curHETRec);
                }
                mapHisEvent.put(curHistoricalEventRec.id,curHistoricalEventRec);
            }
        }
        system.debug('size of mapHisEvent_HETIds='+mapHisEvent_HETIds.size());
        Boolean FlagReqArePerform=False;
        Decimal sumQualifiedAmount;
        for(ID curHistEventRecID : mapHisEvent_HETIds.keyset()){
            FlagReqArePerform=True;
            sumQualifiedAmount=0;
            for(Historical_Event_Task__c curHistEventTask :mapHisEvent_HETIds.get(curHistEventRecID)){
                if(FlagReqArePerform && curHistEventTask.Required__c){ //can't be combined both if because once 1st if true then only proceed to check 2nd if otherwise skip
                    if(!curHistEventTask.Performed__c){
                        FlagReqArePerform=False;
                        break;
                    }
                }
                sumQualifiedAmount+=curHistEventTask.Qualified_Amount_F__c;
            }
            system.debug('Assembly Event Id='+ curHistEventRecID+'=FlagReqArePerform='+ FlagReqArePerform+'=sumQualifiedAmount='+sumQualifiedAmount);
            if(FlagReqArePerform){
                mapHisEvent.get(curHistEventRecID).Y_Hidden_QualifiedClaim__c=True;
                mapHisEvent.get(curHistEventRecID).Y_Hidden_Eligible_Claim_Amount__c=sumQualifiedAmount;
            }
            else{
                mapHisEvent.get(curHistEventRecID).Y_Hidden_QualifiedClaim__c=False;
                mapHisEvent.get(curHistEventRecID).Y_Hidden_Eligible_Claim_Amount__c=0;
            }
        }

        system.debug('size of mapHisEvent='+mapHisEvent.size());
        
        LeaseWareUtils.TriggerDisabledFlag=true;
        if (mapHisEvent.values().size() > 0) Update mapHisEvent.values();
        LeaseWareUtils.TriggerDisabledFlag=false;
       
        system.debug('HistoricalEventTaskTriggerHandler.CalQualifiedEligibleClaim(-)');
    }
	
}