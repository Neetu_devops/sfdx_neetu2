@IsTest
public class TestNewsFeedService {

	static PageReference pageRefOperatorNewsArticle = Page.NewsArticles;
	static NewsArticle__c article1                      {get; set;}
	static Operator__c operator1                        {get; set;}
	static Operator__c newOperator                        {get; set;}
	static Counterparty__c counterparty                       {get; set;}
	static Lessor__c lessor                        {get; set;}
	static LeaseWorksSettings__c lwSettings;
	static ServiceSetting__c lwServiceSetupProfile;
	static LW_Setup__c lwSetup;
	static NewsArticle__c article;
	static NewsFeedRestIntegrationHelper.Data data1;
	static NewsReportRestIntegrationHelper.Data data2;


	@IsTest
	public static void testDate(){
		Long timestamp = Long.valueOf('156689994');
		String idate = String.valueOf(timestamp);
		System.debug('after right pad:' +idate.rightPad(13,'0'));
		//CAPANewsFeedService service = new CAPANewsFeedService();
		DateTime gmtDatetime = LeasewareUtils.unixDateToDate(timestamp);
		System.assertEquals(gmtDatetime.dateGmt(),  Date.newInstance(2019, 08, 27));
	}

	@IsTest
	public static void testCAPAApiToken(){
		dataSetup();
		ServiceSetting__c lwServiceSetup = ServiceSetting__c.getValues('News_Feed_CAPA');
		System.assertEquals(lwServiceSetup.EndPointUrl__c!=null,true);
	}

	@isTest
	static void TestNewsLWCClass()
	{
		dataSetup();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		Test.startTest();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		
		List<NewsArticleLWCClass.DataTableWrapper> articles = NewsArticleLWCClass.getNewsFeed('CAPA-News',newOperator.id,0, 10,'','','','World Fleet Name/Name','','');
		//List<NewsArticleLWCClass.DataTableWrapper> articles = (List<NewsArticleLWCClass.DataTableWrapper>) System.Json.deserialize(response, List<NewsArticleLWCClass.DataTableWrapper>.class);
		System.assertEquals(1,articles.size());
		Test.stopTest();
	}

	@isTest
	static void TestWorldFleetName()
	{
		dataSetup();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		Test.startTest();
		CAPANewsFeedService service = new CAPANewsFeedService();
		NewsComponentCntrl cont = new NewsComponentCntrl();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		
		String response = service.getNewsAndReportsFeed(newOperator.id, lwServiceSetupProfile.EndPointUrl__c, lwServiceSetupProfile.Token__c,'CAPA-News',1,15,'World Fleet Name/Name','','');
		List<CAPANewsFeedService.NewsArticleWrapper> articles = (List<CAPANewsFeedService.NewsArticleWrapper>) System.Json.deserialize(response, List<CAPANewsFeedService.NewsArticleWrapper>.class);
		System.assertEquals(2,articles.size());
		Test.stopTest();
	}
	@isTest
	static void TestWorldFleetNameContrl()
	{
		dataSetup();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		Test.startTest();
		NewsComponentCntrl cont = new NewsComponentCntrl();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );

		String response2 = cont.getNewFeeds('CAPA-News',newOperator.id,1,15,'', '','','World Fleet Name/Name','','');
		List<NewsComponentCntrl.NewsArticleWrapper> articles2 = (List<NewsComponentCntrl.NewsArticleWrapper>) System.Json.deserialize(response2, List<NewsComponentCntrl.NewsArticleWrapper>.class);
		System.assertEquals(1,articles2.size());
		Test.stopTest();
	}
	@isTest
	static void TestNameAndAlias()
	{
		dataSetup();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		CAPANewsFeedService service = new CAPANewsFeedService();
		NewsComponentCntrl cont = new NewsComponentCntrl();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		String response = service.getNewsAndReportsFeed(newOperator.id, lwServiceSetupProfile.EndPointUrl__c, lwServiceSetupProfile.Token__c,'CAPA-News',1,15,'All Names','','');
		List<CAPANewsFeedService.NewsArticleWrapper> articles = (List<CAPANewsFeedService.NewsArticleWrapper>) System.Json.deserialize(response, List<CAPANewsFeedService.NewsArticleWrapper>.class);
		System.assertEquals(4,articles.size());
		Test.stopTest();
	}

	@isTest
	static void TestNameAndAliasContrl()
	{
		dataSetup();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		NewsComponentCntrl cont = new NewsComponentCntrl();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		String response2 = cont.getNewFeeds('CAPA-News',newOperator.id,1,15,'', '','','All Names','','');
		List<NewsComponentCntrl.NewsArticleWrapper> articles2 = (List<NewsComponentCntrl.NewsArticleWrapper>) System.Json.deserialize(response2, List<NewsComponentCntrl.NewsArticleWrapper>.class);
		System.assertEquals(2,articles2.size());
		Test.stopTest();
	}


	@isTest
	static void TestOperatorPageWithArticles()
	{
		dataSetup();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		CAPANewsFeedService service = new CAPANewsFeedService();
		Test.setCurrentPage( pageRefOperatorNewsArticle );
		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		String response = service.getNewsAndReportsFeed(operator1.id, lwServiceSetupProfile.EndPointUrl__c, lwServiceSetupProfile.Token__c,'CAPA-News',1,15,'World Fleet Name/Name','','');
		List<CAPANewsFeedService.NewsArticleWrapper> articles = (List<CAPANewsFeedService.NewsArticleWrapper>) System.Json.deserialize(response, List<CAPANewsFeedService.NewsArticleWrapper>.class);
		System.assertEquals(2,articles.size());
		for(CAPANewsFeedService.NewsArticleWrapper article : articles){
			System.assertEquals(true, article.isLastPage);
		}	
		Test.stopTest();
	}

	@IsTest
	public static void testHomeService(){

		dataSetup();
		// setData();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;
		CAPANewsFeedService service = new CAPANewsFeedService();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		System.debug('' + lwServiceSetupProfile.EndPointUrl__c);
		Test.startTest();
		String response = service.getNewsAndReportsFeed(null, lwServiceSetupProfile.EndPointUrl__c, lwServiceSetupProfile.Token__c,'CAPA-News',1,12,'World Fleet Name/Name','','');
		List<CAPANewsFeedService.NewsArticleWrapper> articles = (List<CAPANewsFeedService.NewsArticleWrapper>) System.Json.deserialize(response, List<CAPANewsFeedService.NewsArticleWrapper>.class);
		System.assertEquals(true,articles.size()>0);
		Test.stopTest();
	}

	@IsTest
	public static void testReportService(){

		dataSetup();
		// setData();
		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;
		CAPANewsFeedService service = new CAPANewsFeedService();
		Test.setMock( HttpCalloutMock.class, new TestNewsReportMockHttpResGen() );
		System.debug('' + lwServiceSetupProfile.EndPointUrl__c);
		Test.startTest();
		String response = service.getNewsAndReportsFeed(operator1.id, lwServiceSetupProfile.EndPointUrl__c, lwServiceSetupProfile.Token__c,'CAPA-Reports',1,15,'World Fleet Name/Name','','');
		List<CAPANewsFeedService.NewsArticleWrapper> articles = (List<CAPANewsFeedService.NewsArticleWrapper>) System.Json.deserialize(response, List<CAPANewsFeedService.NewsArticleWrapper>.class);
		System.assertEquals(true,articles.size()>0);
		Test.stopTest();
	}

	@isTest
	static void TestCreateArticlesWithOperator()
	{
		dataSetup();
		CAPANewsFeedService service = new CAPANewsFeedService();
		Test.setCurrentPage( pageRefOperatorNewsArticle );
		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		Test.startTest();
		service.createArticle(setData(), operator1, null, null, null);
		//System.assertEquals(true, article.Article__c != null);
		Test.stopTest();
	}

	@IsTest
	public static void testHomeServiceForReport(){

		dataSetup();
		NewsComponentCntrl cont = new NewsComponentCntrl();
		cont.capaToken = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		cont.capaURL = 'https://api.centreforaviation.com';
		cont.showMore =false;
		cont.recordId = operator1.id;
		Test.setMock( HttpCalloutMock.class, new TestNewsReportMockHttpResGen() );
		Test.startTest();
		cont.getNewFeeds('CAPA-Reports',null,1,10,'', '','','World Fleet Name/Name','','');
		Test.stopTest();
	}

	@IsTest
	public static void testService(){

		dataSetup();
		NewsComponentCntrl cont = new NewsComponentCntrl();
		cont.capaToken = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		cont.capaURL = 'https://api.centreforaviation.com';
		cont.showMore =false;
		cont.recordId = operator1.id;
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		String response = cont.getNewFeeds('CAPA-News',operator1.id,1,2,'', '','','World Fleet Name/Name','','');
		List<NewsComponentCntrl.NewsArticleWrapper> articles = (List<NewsComponentCntrl.NewsArticleWrapper>) System.Json.deserialize(response, List<NewsComponentCntrl.NewsArticleWrapper>.class);
		System.assertEquals(true,articles.size()>0);
		System.assertEquals(1,  articles[0].articlesNotReturned,'articles not returned after operator name match');
		for(NewsComponentCntrl.NewsArticleWrapper article : articles){
			System.assertEquals(false, article.isLastPage);
		}
		Test.stopTest();
	}

	@IsTest
	public static void testCAPANewsService(){

		dataSetup();
		NewsComponentCntrl cont = new NewsComponentCntrl();	
		cont.capaToken = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		cont.capaURL = 'https://api.centreforaviation.com';
		cont.showMore =false;
		cont.recordId = operator1.id;
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		String response = cont.getNewFeeds('CAPA-News',operator1.id,1,10,'','2020-08-07','2020-08-07','World Fleet Name/Name','' ,'');
		List<NewsComponentCntrl.NewsArticleWrapper> articles = (List<NewsComponentCntrl.NewsArticleWrapper>) System.Json.deserialize(response, List<NewsComponentCntrl.NewsArticleWrapper>.class);
		System.assertEquals(true,articles.size()>0);

		System.assertEquals('2020-08-07', String.valueOf(articles[0].articleDate), 'article date returned should match the date filter');
		Test.stopTest();
	}

	static void dataSetup()
	{
		LeaseWorksSettings__c lwSettings = new LeaseWorksSettings__c();
		insert lwSettings;

		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		ServiceSetting__c lwServiceSetup =  new ServiceSetting__c();
		lwServiceSetup.Name = 'News_Feed_CAPA';
		lwServiceSetup.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetup.Authorization_Token2__c = 'A';
		lwServiceSetup.Authorization_Token3__c = 'A';
		lwServiceSetup.Authorization_Token4__c = 'A';
		lwServiceSetup.Authorization_Token5__c = 'A';
		lwServiceSetup.Username__c = 's.jha@lw.com';
		lwServiceSetup.Password__c = 'djdhi';
		lwServiceSetup.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetup;

		lwSetup = new LW_Setup__c();
		lwSetup.Name = 'OPERATOR_NAME_FILTER';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'ON';
		insert lwSetup;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createOperator(mapInOut);
		operator1 = (Operator__c)mapInOut.get('Operator__c');
		mapInOut.put('NewsArticle__c.Operator__c',operator1.Id);
		//  TestLeaseworkUtil.createNewsArticles(mapInOut);
		// article1 = (NewsArticle__c)mapInOut.get('NewsArticle__c');

		
		Operator__c newOp = new Operator__c (Name = 'Qantas Air', Alias__c = 'QA' , World_Fleet_Operator_Name__c = 'Qantas');

		LeaseWareUtils.TriggerDisabledFlag = true;
			insert newOp;
		LeaseWareUtils.TriggerDisabledFlag = false;

		newOperator = [Select id, name, Alias__c,World_Fleet_Operator_Name__c from Operator__c where Name like: 'Qantas Air'];
	}


	@IsTest
	public static void testJSONParser(){
		String jsonString = '{\n\t\"data\": [\n\t\t{\n\t\t\t\"rank\": 1,\n\t\t\t\"id\": 503,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"name\": \"Airline\"\n\t\t\t},\n\t\t\t\"iata\": \"QF\",\n\t\t\t\"icao\": \"QFA\",\n\t\t\t\"name\": \"Qantas Airways\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airlines/qantas-airways-qf\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": \"Australia\",\n\t\t\t\t\"code\": \"AU\"\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/1-503/logo.png?1477525965\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 1,\n\t\t\t\"id\": 13701,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"name\": \"Airline\"\n\t\t\t},\n\t\t\t\"iata\": \"\",\n\t\t\t\"icao\": \"\",\n\t\t\t\"name\": \"QantasLink\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airlines/qantaslink\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": \"Australia\",\n\t\t\t\t\"code\": \"AU\"\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/1-13701/logo.png?1418087767\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 2,\n\t\t\t\"id\": 69,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 10,\n\t\t\t\t\"name\": \"Airline Group\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Group\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airline-groups/qantas-group\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/10-69/logo.png?1477525982\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 4,\n\t\t\t\"id\": 2495,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 3,\n\t\t\t\t\"name\": \"Lessor\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Leasing Pty Ltd\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/lessors/qantas-leasing-pty-ltd\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t}\n\t\t},\n\t\t{\n\t\t\t\"rank\": 8,\n\t\t\t\"id\": 640,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 12,\n\t\t\t\t\"name\": \"Supplier\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Ground Services (QGS)\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/suppliers/qantas-ground-services-qgs\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"subclass\": \"Ground Handling\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 9,\n\t\t\t\"id\": 1264,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 21,\n\t\t\t\t\"name\": \"MRO\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Airways MRO\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/maintenance-repair-and-overhaul/qantas-airways-mro\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/21-1264/logo.png?1474513036\"\n\t\t}\n\t]\n}';
		NewsFeedProfileRestIntegrationHelper resHelper = (NewsFeedProfileRestIntegrationHelper)JSON.deserialize(jsonString, NewsFeedProfileRestIntegrationHelper.class);
		System.debug(resHelper);
		List<Integer> profileIds = new List<Integer>();
		for(NewsFeedProfileRestIntegrationHelper.Data data: resHelper.Data ) {
			if(data!=null && data.type!=null) {
				if(data.type.id == 1) {
					profileIds.add(data.id);
				}
				System.debug('profileIds:'+ profileIds);
			}
		}
		for(Integer id: profileIds) {
			System.assertEquals(503, id);
			break;
		}

		System.debug('Data=' + resHelper.Data);
		NewsFeedProfileRestIntegrationHelper.Data data = resHelper.Data[0];
		System.assertEquals(503, data.id);
		System.assertEquals(1, data.rank);
		System.assertEquals('AU', data.country.code);
		System.assertEquals('Australia', data.country.name);
		System.assertEquals(1, data.type.id);
		System.assertEquals('Airline', data.type.name);
		System.assertEquals('QF', data.iata);
		System.assertEquals('QFA', data.icao);
		System.assertEquals('Qantas Airways', data.name);
		System.assertEquals('https://centreforaviation.com/data/profiles/airlines/qantas-airways-qf', data.url);

		//For code coverage
		JSONParser parser = JSON.createParser(jsonString);
		NewsFeedProfileRestIntegrationHelper resHelper1 = new NewsFeedProfileRestIntegrationHelper(parser);
	}

	@IsTest
	public static void testNewsJSONParser(){

		NewsFeedRestIntegrationHelper.Data data = setData();
		System.assertEquals(911056, data.id);
		System.assertEquals('Khanty-Mansiysk Autonomous Area not planning to sell UTair shares', data.title);
		System.assertEquals('https://centreforaviation.com/news/khanty-mansiysk-autonomous-area-not-planning-to-sell-utair-shares-911056', data.url);
		System.assertEquals(1560134880, data.dateOfNews);
		System.assertEquals('russia',data.tags[0]);
		System.debug(data);

		//For code coverage
		String jsonString = '{\n\t\"data\": [\n\t\t{\n\t\t\t\"rank\": 1,\n\t\t\t\"id\": 503,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"name\": \"Airline\"\n\t\t\t},\n\t\t\t\"iata\": \"QF\",\n\t\t\t\"icao\": \"QFA\",\n\t\t\t\"name\": \"Qantas Airways\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airlines/qantas-airways-qf\",\n\t\t\t \"tags\": [\n\"freight\",\n\"canada\",\n\"leasing\"],\n\t\t\t\"country\": {\n\t\t\t\t\"name\": \"Australia\",\n\t\t\t\t\"code\": \"AU\"\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/1-503/logo.png?1477525965\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 1,\n\t\t\t\"id\": 13701,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"name\": \"Airline\"\n\t\t\t},\n\t\t\t\"iata\": \"\",\n\t\t\t\"icao\": \"\",\n\t\t\t\"name\": \"QantasLink\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airlines/qantaslink\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": \"Australia\",\n\t\t\t\t\"code\": \"AU\"\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/1-13701/logo.png?1418087767\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 2,\n\t\t\t\"id\": 69,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 10,\n\t\t\t\t\"name\": \"Airline Group\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Group\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airline-groups/qantas-group\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/10-69/logo.png?1477525982\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 4,\n\t\t\t\"id\": 2495,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 3,\n\t\t\t\t\"name\": \"Lessor\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Leasing Pty Ltd\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/lessors/qantas-leasing-pty-ltd\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t}\n\t\t},\n\t\t{\n\t\t\t\"rank\": 8,\n\t\t\t\"id\": 640,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 12,\n\t\t\t\t\"name\": \"Supplier\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Ground Services (QGS)\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/suppliers/qantas-ground-services-qgs\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"subclass\": \"Ground Handling\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 9,\n\t\t\t\"id\": 1264,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 21,\n\t\t\t\t\"name\": \"MRO\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Airways MRO\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/maintenance-repair-and-overhaul/qantas-airways-mro\",\r\n\"tags\": [\r\n\"freight\",\r\n\"canada\"],\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/21-1264/logo.png?1474513036\"\n\t\t}\n\t]\n}';
		NewsFeedRestIntegrationHelper.parse(jsonString);
	}

	public static void testReportJSONParser(){

		NewsReportRestIntegrationHelper.Data data = setReportData();
		System.assertEquals(911056, data.id);
		System.assertEquals('Europes long haul recovery lags domestic and international short haul', data.title);
		System.assertEquals('https://centreforaviation.com/analysis/reports/europes-long-haul-recovery-lags-domestic-and-international-short-haul-530411', data.url);
		System.assertEquals(1560134880, data.dateOfNews);
		System.assertEquals('russia',data.tags[0]);
		System.debug(data);

		//For code coverage
		String jsonString = '{\n\t"data\":[{\"id\":532754,\"price\":null,\"ad\":1,\"featured\":0,\"timestamp\":1596775440,'+
		'\"title\":\"Airports: Groupe ADP outlines plans based on 1H2020 results\",\"description\":\"\",\"keypoints\":null,'+
		'\"abstract\":\"<p>First half 2020 financial results are being published by airport operators, '+
		'Among them are commercial revenues from retail concessions, which at some airports in Europe were quick to get going again. Data released by the Spanish operator AENA also adds weight to that theory.",'+
		 '\"premium\":1,\"source\":\"\",'+
		 '\"url\":\"https://centreforaviation.com/analysis/reports/airports-groupe-adp-outlines-plans-based-on-1h2020-results-532754\",'+
		 '\"tags\":[\"france\",\"germany\",\"aig\",\"airport retail\",\"airport concessions\",\"aena aeropuertos internacional\",\"groupe adp\",\"tav airports\",\"aena\",\"covid 19\",\"pandemic\"],\"type\":{\"id\":1,\"name\":\"Analysis\"},\"words\":2802}]}';
		NewsReportRestIntegrationHelper.parse(jsonString);
	}

	private static NewsFeedRestIntegrationHelper.Data setData(){
		data1 = new NewsFeedRestIntegrationHelper.Data();
		data1.id =911056;
		data1.title ='Khanty-Mansiysk Autonomous Area not planning to sell UTair shares';
		data1.url ='https://centreforaviation.com/news/khanty-mansiysk-autonomous-area-not-planning-to-sell-utair-shares-911056';
		data1.tags = new List<String> {'russia'};
		data1.dateOfNews =1560134880;
		data1.article ='<p><strong> Air India <p><strong>';
		return data1;
	}

	private static NewsReportRestIntegrationHelper.Data setReportData(){
		data2 = new NewsReportRestIntegrationHelper.Data();
		data2.id =911056;
		data2.title ='Europes long haul recovery lags domestic and international short haul';
		data2.url ='https://centreforaviation.com/analysis/reports/europes-long-haul-recovery-lags-domestic-and-international-short-haul-530411';
		data2.tags = new List<String> {'russia'};
		data2.dateOfNews =1560134880;
		data2.article ='<p><strong> Air India <p><strong>';
		return data2;
	}

	@isTest
	static void TestArticlesWithOperatorFilterOFF()
	{
		LeaseWorksSettings__c lwSettings = new LeaseWorksSettings__c();
		insert lwSettings;

		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetupProfile.Authorization_Token2__c = 'A';
		lwServiceSetupProfile.Authorization_Token3__c = 'A';
		lwServiceSetupProfile.Authorization_Token4__c = 'A';
		lwServiceSetupProfile.Authorization_Token5__c = 'A';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		ServiceSetting__c lwServiceSetup =  new ServiceSetting__c();
		lwServiceSetup.Name = 'News_Feed_CAPA';
		lwServiceSetup.Token__c = 'AIzaSyBHkFZfpkciAckm2Nrh1n2OnA5U6LjfY24';
		lwServiceSetup.Authorization_Token2__c = 'A';
		lwServiceSetup.Authorization_Token3__c = 'A';
		lwServiceSetup.Authorization_Token4__c = 'A';
		lwServiceSetup.Authorization_Token5__c = 'A';

		lwServiceSetup.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetup;

		lwSetup = new LW_Setup__c();
		lwSetup.Name = 'OPERATOR_NAME_FILTER';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'OFF';
		insert lwSetup;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createOperator(mapInOut);
		operator1 = (Operator__c)mapInOut.get('Operator__c');
		mapInOut.put('NewsArticle__c.Operator__c',operator1.Id);
		//   TestLeaseworkUtil.createNewsArticles(mapInOut);
		//   article1 = (NewsArticle__c)mapInOut.get('NewsArticle__c');

		CAPANewsFeedService service = new CAPANewsFeedService();
		Test.setCurrentPage( pageRefOperatorNewsArticle );
		ApexPages.currentPage().getParameters().put('Id', operator1.id);
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		String response = service.getNewsAndReportsFeed(operator1.id, lwServiceSetupProfile.EndPointUrl__c, lwServiceSetupProfile.Token__c,'CAPA-News',1,10,'World Fleet Name/Name','','');
		List<CAPANewsFeedService.NewsArticleWrapper> articles = (List<CAPANewsFeedService.NewsArticleWrapper>) System.Json.deserialize(response, List<CAPANewsFeedService.NewsArticleWrapper>.class);
		System.assertEquals(true,articles.size()>0);
		Test.stopTest();
	}

	@isTest
	private static void testRedirectUrl(){
		dataSetup();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		NewsArticleLWCClass.ResponseRedirectWrapper responseWrapper = NewsArticleLWCClass.getRedirectUrl('https://centreforaviation.com/news/air-india-to-resume-services-connecting-mumbai-to-seven-domestic-destinations-in-sep-2020-1022622');
		Test.stopTest();
		System.assertEquals('302', responseWrapper.statusCode);
		System.assertEquals(true, responseWrapper.restResponse.contains('https://centreforaviation.com/shared?access_token=eyJ0eXAiOiJKV1QiLCJhb'));
		
		List<LW_Setup__c> listRedirectToken = [Select Value__c,Disable__c from LW_Setup__c where Name like 'CAPAToken%' and Disable__c = false];
		List<LW_Setup__c> listLogoutTime = [Select Value__c,Disable__c from LW_Setup__c where Name = 'ExpiryTimeForCAPAToken' and Disable__c = false];
		
		System.debug('Test : number of tokens created:' + listRedirectToken.size());
		System.assertEquals(3,listRedirectToken.size());
		for(LW_Setup__c stup : listRedirectToken){
			System.debug('Test : size :' + stup.Value__c.length());
		}

		System.assertEquals(255, listRedirectToken[0].Value__c.length());
		String logoutTime = String.valueOfGmt(System.now().addSeconds(28800));
		System.assertEquals('28800---'+logoutTime, listLogoutTime[0].value__c);
	}

	@isTest
	private static void testRedirectUrlSessionValid(){
		dataSetup();

		List<LW_Setup__c> listSessionValidity = new List<LW_Setup__c>();

		listSessionValidity.add(new LW_Setup__c( Name = 'ExpiryTimeForCAPAToken' ,
													 Value__c = '28800---'+String.valueOfGmt(System.now().addSeconds(28800)),		
													 Disable__c = false,
													 Description__c = 'Time till which the CAPA redirect token is valid'));

		listSessionValidity.add(new LW_Setup__c( Name = 'CAPAToken1' ,
													 Value__c = 'ABGHGY.FYGYHHJHIUHHFTR.GHUHU',		
													 Disable__c = false,
													 Description__c = 'Redirect Token for CAPA autologin'));

		insert listSessionValidity;
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		NewsArticleLWCClass.ResponseRedirectWrapper responseWrapper = NewsArticleLWCClass.getRedirectUrl('https://centreforaviation.com/news/air-india-to-resume-services-connecting-mumbai-to-seven-domestic-destinations-in-sep-2020-1022622');
		Test.stopTest();
		System.assertEquals('302', responseWrapper.statusCode);
		System.assertEquals(true, responseWrapper.restResponse.contains('https://centreforaviation.com/shared?access_token=ABGHGY.FYGYHHJHIUHHFTR.GHUHU'));
		
		List<LW_Setup__c> listRedirectToken = [Select Value__c,Disable__c from LW_Setup__c where Name like 'CAPAToken%' and Disable__c = false];
		List<LW_Setup__c> listLogoutTime = [Select Value__c,Disable__c from LW_Setup__c where Name = 'ExpiryTimeForCAPAToken' and Disable__c = false];		
		System.assertEquals(true, listRedirectToken[0].Value__c.length()>0);
		String logoutTime = String.valueOfGmt(System.now().addSeconds(28800));
		System.assertEquals('28800---'+logoutTime, listLogoutTime[0].value__c);
	}

	@isTest
	private static void testEmptyToken(){

		LeaseWorksSettings__c lwSettings = new LeaseWorksSettings__c();
		insert lwSettings;

		ServiceSetting__c lwServiceSetupProfile = new ServiceSetting__c();
		lwServiceSetupProfile.Name = 'News_Feed_CAPA';
		lwServiceSetupProfile.Token__c = '';

		lwServiceSetupProfile.EndPointUrl__c = 'https://api.centreforaviation.com';
		insert lwServiceSetupProfile;

		lwSetup = new LW_Setup__c();
		lwSetup.Name = 'OPERATOR_NAME_FILTER';
		lwSetup.Disable__c = false;
		lwSetup.Value__c = 'ON';
		insert lwSetup;
		Operator__c newOp = new Operator__c (Name = 'Qantas Air', Alias__c = 'QA' , World_Fleet_Operator_Name__c = 'Qantas');

		LeaseWareUtils.TriggerDisabledFlag = true;
			insert newOp;
		LeaseWareUtils.TriggerDisabledFlag = false;

		newOperator = [Select id, name, Alias__c,World_Fleet_Operator_Name__c from Operator__c where Name like: 'Qantas Air'];

		CAPANewsFeedService service = new CAPANewsFeedService();
		Test.setMock( HttpCalloutMock.class, new TestNewsFeedMockHttpResGen() );
		Test.startTest();
		String response = service.getNewsAndReportsFeed(newOperator.id, lwServiceSetupProfile.EndPointUrl__c, lwServiceSetupProfile.Token__c,'CAPA-News',1,10,'World Fleet Name/Name','','');
		//List<CAPANewsFeedService.NewsArticleWrapper> articles = (List<CAPANewsFeedService.NewsArticleWrapper>) System.Json.deserialize(response, List<CAPANewsFeedService.NewsArticleWrapper>.class);
		System.assertEquals('',response);
	}

	@isTest
	private static void TestForCodeCoverage(){
		String jsonString = '{\n\t\"data\": [\n\t\t{\n\t\t\t\"rank\": 1,\n\t\t\t\"id\": 503,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"name\": \"Airline\"\n\t\t\t},\n\t\t\t\"iata\": \"QF\",\n\t\t\t\"icao\": \"QFA\",\n\t\t\t\"name\": \"Qantas Airways\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airlines/qantas-airways-qf\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": \"Australia\",\n\t\t\t\t\"code\": \"AU\"\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/1-503/logo.png?1477525965\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 1,\n\t\t\t\"id\": 13701,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"name\": \"Airline\"\n\t\t\t},\n\t\t\t\"iata\": \"\",\n\t\t\t\"icao\": \"\",\n\t\t\t\"name\": \"QantasLink\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airlines/qantaslink\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": \"Australia\",\n\t\t\t\t\"code\": \"AU\"\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/1-13701/logo.png?1418087767\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 2,\n\t\t\t\"id\": 69,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 10,\n\t\t\t\t\"name\": \"Airline Group\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Group\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/airline-groups/qantas-group\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/10-69/logo.png?1477525982\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 4,\n\t\t\t\"id\": 2495,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 3,\n\t\t\t\t\"name\": \"Lessor\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Leasing Pty Ltd\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/lessors/qantas-leasing-pty-ltd\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t}\n\t\t},\n\t\t{\n\t\t\t\"rank\": 8,\n\t\t\t\"id\": 640,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 12,\n\t\t\t\t\"name\": \"Supplier\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Ground Services (QGS)\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/suppliers/qantas-ground-services-qgs\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"subclass\": \"Ground Handling\"\n\t\t},\n\t\t{\n\t\t\t\"rank\": 9,\n\t\t\t\"id\": 1264,\n\t\t\t\"type\": {\n\t\t\t\t\"id\": 21,\n\t\t\t\t\"name\": \"MRO\"\n\t\t\t},\n\t\t\t\"iata\": null,\n\t\t\t\"icao\": null,\n\t\t\t\"name\": \"Qantas Airways MRO\",\n\t\t\t\"url\": \"https://centreforaviation.com/data/profiles/maintenance-repair-and-overhaul/qantas-airways-mro\",\n\t\t\t\"country\": {\n\t\t\t\t\"name\": null,\n\t\t\t\t\"code\": null\n\t\t\t},\n\t\t\t\"image\": \"https://images.cdn.centreforaviation.com/profiles/21-1264/logo.png?1474513036\"\n\t\t}\n\t]\n}';
		NewsFeedProfileRestIntegrationHelper resHelper = (NewsFeedProfileRestIntegrationHelper)JSON.deserialize(jsonString, NewsFeedProfileRestIntegrationHelper.class);
		NewsFeedProfileRestIntegrationHelper.parse(jsonString);
		JSONParser parser = JSON.createParser(jsonString);
		NewsFeedProfileRestIntegrationHelper.consumeObject(parser);
	}
}