public class TrxProspectTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'TrxProspectTriggerHandlerBefore';
    private final string triggerAfter = 'TrxProspectTriggerHandlerAfter';
    public TrxProspectTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('TrxProspectTriggerHandler.beforeInsert(+)');


		TxnProposalBfrInsrtUpdate();

        system.debug('TrxProspectTriggerHandler.beforeInsert(-)');       
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('TrxProspectTriggerHandler.beforeUpdate(+)');
        
		TxnProposalBfrInsrtUpdate();
        system.debug('TrxProspectTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('TrxProspectTriggerHandler.beforeDelete(+)');
        //TxnTermsRefreshProspectSummary();
        
        system.debug('TrxProspectTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxProspectTriggerHandler.afterInsert(+)');
		//TxnTermsRefreshProspectSummary();
        system.debug('TrxProspectTriggerHandler.afterInsert(-)');        
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxProspectTriggerHandler.afterUpdate(+)');
		//TxnTermsRefreshProspectSummary();
        system.debug('TrxProspectTriggerHandler.afterUpdate(-)');        
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxProspectTriggerHandler.afterDelete(+)');
        
        
        system.debug('TrxProspectTriggerHandler.afterDelete(-)');        
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxProspectTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('TrxProspectTriggerHandler.afterUnDelete(-)');      
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
    
    // bef ins/bef upd
    private void TxnProposalBfrInsrtUpdate(){

		list<Transaction_Proposal__c> listNew = (list<Transaction_Proposal__c>) trigger.new;
	    set<Id> setTxnIds = new set<Id>();
	    for(Transaction_Proposal__c curTxnProp:listNew){
	        setTxnIds.add(curTxnProp.Transaction__c);
	    }
	    
	    map<Id, Transaction__c> mapParentTxns = new map<Id, Transaction__c>([select Id, Ascend_Value__c 
	                                from Transaction__c where id in :setTxnIds]);
	    
	    for(Transaction_Proposal__c curTxnProp:listNew){
	        if(curTxnProp.Purchase_Price__c!=null ){
	            Transaction__c parentTxn=mapParentTxns.get(curTxnProp.Transaction__c);
	
	            curTxnProp.IRR__c=LeaseWareUtils.CalculateIRR(curTxnProp.Purchase_Price__c, curTxnProp.Rent__c, 
	                                curTxnProp.Term_Months__c, null, null, leasewareUtils.ZeroIfNull(parentTxn.Ascend_Value__c)*1000000)*100;
				//Annualize the IRR                                
	            curTxnProp.IRR__c=(math.pow((1+double.valueOf(curTxnProp.IRR__c/100)),12)-1)*100;
	        }
	    }

    	
    }

}