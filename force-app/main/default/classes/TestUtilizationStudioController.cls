@isTest
public class TestUtilizationStudioController{
    @testSetUp
    public static void testBulkUtilizationData(){
        
        UtilizationStudioController.getAPUCounterStatus();
        
        Date dateField = System.today().addmonths(-6); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today().addmonths(-6),
            Lease_End_Date_New__c = system.today().addmonths(6),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id
        );
        insert leaseRecord;
 
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false, 
            RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Aircraft').getRecordTypeId(),
            TSN__c = 1.00,                               
            CSN__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;

        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;         
        
        List<Constituent_Assembly__c> constituentList = new List<Constituent_Assembly__c>();
        for(Integer i=0;i<3;i++){
            Constituent_Assembly__c consituentRecord = new Constituent_Assembly__c();
            consituentRecord.Serial_Number__c = '2';
            //consituentRecord.CSLV__c = 2+i;
            consituentRecord.CSN__c = 3;
            consituentRecord.Last_CSN_Utilization__c = 3;
            //consituentRecord.TSLV__c = 4+i;
            consituentRecord.TSN__c = 5;
            consituentRecord.Last_TSN_Utilization__c = 5;
            consituentRecord.Attached_Aircraft__c = testAircraft.Id;
            consituentRecord.Name ='Testing';
            if (i == 0){
                consituentRecord.Type__c ='Airframe'; 
            }
            else if (i == 1){
                consituentRecord.Type__c ='Engine 1'; 
            }
            else{
                consituentRecord.Type__c ='APU'; 
            }
            
            constituentList.add(consituentRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert constituentList;
        LWGlobalUtils.setTriggersflagOff();
        
        
        // Set up Maintenance Program
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        // map of Maintenance Program Event
        map<string,id> mapMPE = new map<string,id>();       
        for(Maintenance_Program_Event__c curMPE: [select id,Assembly__c from Maintenance_Program_Event__c where Maintenance_Program__c =: mp.Id]){
            mapMPE.put(curMPE.Assembly__c,curMPE.id);
        }
        // Set up Project Event
        List<Assembly_Event_Info__c> ListProjEvent = new List<Assembly_Event_Info__c>();       
        for(Integer i=0;i<3;i++){
            Assembly_Event_Info__c ProjEvent = new Assembly_Event_Info__c();
            if (i == 0){
                ProjEvent.Name= 'Airframe';
                ProjEvent.Event_Cost__c= 100;
                ProjEvent.Constituent_Assembly__c= constituentList[0].id;
                ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[0].Type__c);
            }
            else if (i == 1){
                ProjEvent.Name= 'Engine 1';
                ProjEvent.Event_Cost__c= 100;
                ProjEvent.Constituent_Assembly__c= constituentList[1].id;
                ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[1].Type__c);
            }
            else{
                ProjEvent.Name= 'APU';
                ProjEvent.Event_Cost__c= 100;
                ProjEvent.Constituent_Assembly__c= constituentList[2].id;
                ProjEvent.Maintenance_Program_Event__c=mapMPE.get(constituentList[2].Type__c);
            }
            ListProjEvent.add(ProjEvent);
        }
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert ListProjEvent;
        LWGlobalUtils.setTriggersflagOff();
        
        Assembly_MR_Rate__c assemblyRecord = new Assembly_MR_Rate__c(
            Name='Testing',
            Assembly_Lkp__c =constituentList[0].Id,
            Base_MRR__c = 200,
            Lease__c = leaseRecord.Id,
            Assembly_Event_Info__c= ListProjEvent[0].id,
            Rate_Basis__c = 'Monthly'
        );
        insert assemblyRecord;
        
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                    Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                    Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
                    LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = lastDayOfMonth//Date.newInstance(2020,02,29)
        );
        //LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
       // LWGlobalUtils.setTriggersflagOff();
        
        Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'True Up Gross',
            Original_Monthly_Utilization__c =  utilizationRecord.Id,           
            Status__c='Approved By Lessor',
            Month_Ending__c = lastDayOfMonth
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord1;
        LWGlobalUtils.setTriggersflagOff();
        
        //utilizationRecord.Status__c = 'Approved By Lessor';
        //update utilizationRecord;
        
        Assembly_Utilization__c assemblyUtilization = new Assembly_Utilization__c();
        assemblyUtilization.Name = 'Test Assembly Utilization';
        assemblyUtilization.Constituent_Assembly__c = constituentList[0].Id;
        assemblyUtilization.Utilization_Report__c = utilizationRecord.Id;
        assemblyUtilization.Unknown_FH_FC__c = true;
        LWGlobalUtils.setTriggersflagOn(); 
        insert assemblyUtilization;
        LWGlobalUtils.setTriggersflagOff();
        
        List<Utilization_Report_List_Item__c> utilizationReportList = new List<Utilization_Report_List_Item__c>();
        for(Integer i=0;i<3;i++){
            Utilization_Report_List_Item__c utilizationReportRecord = new Utilization_Report_List_Item__c();
            utilizationReportRecord.Comments__c = 'Testing'+i;
            utilizationReportRecord.Utilization_Report__c = utilizationRecord.Id;
            utilizationReportRecord.Cycles_During_Month__c = 2;
            utilizationReportRecord.Running_Hours_During_Month__c = 3;
            if(i == 0){
                utilizationReportRecord.Constituent_Assembly__c = constituentList[0].Id;
                //utilizationReportRecord.Event_Name__c=ListProjEvent[0].Id;
            }
            else if(i == 1){
                utilizationReportRecord.Constituent_Assembly__c = constituentList[1].Id;
                //utilizationReportRecord.Event_Name__c=ListProjEvent[1].Id;
            }
            else{
                utilizationReportRecord.Constituent_Assembly__c = constituentList[2].Id;
                //utilizationReportRecord.Event_Name__c=ListProjEvent[2].Id;
            }
            utilizationReportRecord.Effective_Unit__c = 'Monthly';
            utilizationReportRecord.Threshold_Units__c = 2;
            utilizationReportRecord.Rate_Up_To_Threshold__c = 3;
            utilizationReportRecord.Threshold_Amount__c = 4 ;
            utilizationReportRecord.Monthly_Threshold_Guaranteed__c = true;
            //utilizationReportRecord.Event_Name__c = '23';
            utilizationReportRecord.Net_Units__c = 3;
            utilizationReportRecord.Rate_Beyond_Threshold__c = 6;
            utilizationReportRecord.TSN_At_Month_Start__c = 4;
            utilizationReportRecord.CSN_At_Month_Start__c = 6;
            utilizationReportRecord.Y_Hidden_MR__c = 9;
            utilizationReportList.add(utilizationReportRecord);
        }
        LWGlobalUtils.setTriggersflagOn();  
        insert utilizationReportList;
        LWGlobalUtils.setTriggersflagOff();
        
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Amount__c = 100,
            Status__c = 'Approved'
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Payment__c paymentRecord = new Payment__c(
            Name = 'Payment Testing',
            Invoice__c = invoiceRecord.Id,
            Amount__c = invoiceRecord.Amount__c
        );
        insert paymentRecord;
        
        List<Payment_Line_Item__c> paymentLineList = new List<Payment_Line_Item__c>();
        for(Integer i=0;i<3;i++){
            Payment_Line_Item__c paymentLineRecord = new Payment_Line_Item__c();
            if(i == 0){
                paymentLineRecord.Name = 'Airframe-23';
                paymentLineRecord.Assembly_Utilization__c = utilizationReportList[0].Id;
            }
            else if(i == 1){
                paymentLineRecord.Name = 'Engine-23';
                paymentLineRecord.Assembly_Utilization__c = utilizationReportList[1].Id;
            }
            else{
                paymentLineRecord.Name = 'Landing Gear - Right Main-23';
                paymentLineRecord.Assembly_Utilization__c = utilizationReportList[2].Id;
            }
            paymentLineRecord.Amount_Paid__c = 9;
            paymentLineRecord.Payment__c = paymentRecord.Id;
            
            paymentLineList.add(paymentLineRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert paymentLineList;
        LWGlobalUtils.setTriggersflagOff();
        
        
        List<Invoice_Line_Item__c> invoiceList = new List<Invoice_Line_Item__c>();
        for(Integer i=0;i<3;i++){
            Invoice_Line_Item__c invoiceLineItemRecord = new Invoice_Line_Item__c();
            invoiceLineItemRecord.Name='Testing'+i;
            if(i == 0){
                invoiceLineItemRecord.Assembly_Utilization__c = utilizationReportList[0].Id;  
            }
            else if(i == 1){
                invoiceLineItemRecord.Assembly_Utilization__c = utilizationReportList[1].Id;  
            }
            else{
                invoiceLineItemRecord.Assembly_Utilization__c = utilizationReportList[2].Id;  
            }
            
            invoiceLineItemRecord.Invoice__c = invoiceRecord.Id;
            invoiceLineItemRecord.Amount_Due__c = 20;
            invoiceList.add(invoiceLineItemRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceList;
        LWGlobalUtils.setTriggersflagOff();
        
        
        List<Utilization_Report_LLP__c> utilizationReportLLPList = new List<Utilization_Report_LLP__c>();
        for(Integer i=0;i<3;i++){
            Utilization_Report_LLP__c utilizationReportLLPRecord = new Utilization_Report_LLP__c();
            utilizationReportLLPRecord.Y_Hidden_MR__c = 2;
            if(i == 0){
                utilizationReportLLPRecord.Utilization_Report_List_Item__c = utilizationReportList[0].Id;  
            }
            else if(i == 1){
                utilizationReportLLPRecord.Utilization_Report_List_Item__c = utilizationReportList[1].Id;  
            }
            else{
                utilizationReportLLPRecord.Utilization_Report_List_Item__c = utilizationReportList[2].Id;  
            }
            utilizationReportLLPRecord.Cycles_During_Month__c =  3;
            utilizationReportLLPList.add(utilizationReportLLPRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationReportLLPList;
        LWGlobalUtils.setTriggersflagOff();
    }
    
    
    @isTest static void testfetchUtilization(){
        Test.startTest();
        
        Aircraft__c aircraftList = [select id,Name from Aircraft__c LIMIT 1];
        
        Operator__c op = [select id,Name from Operator__c LIMIT 1];
        Constituent_Assembly__c constituentAssembly = [select id from Constituent_Assembly__c LIMIT 1];
        Utilization_Report__c utilizationlist = [select id,Status__c from Utilization_Report__c where Type__c ='Actual'];
        Utilization_Report_List_Item__c urListItem = [Select id, name from Utilization_Report_List_Item__c LIMIT 1];
        
        Aircraft__c asset = new Aircraft__c();
        asset.Id= aircraftList.Id;
        asset.Name= 'Test';
        
        Operator__c Operator = new Operator__c();
        Operator.Id= op.Id; 
        
        searchFiltersWrapper sfw = new searchFiltersWrapper();
        sfw.Status ='All';
        
        List<String> IgnoreIdsList = new List<String>();  
        List<String> PreviousRecordsList= new List<String>(); 
        List<String> PinnedIdsList= new List<String>();
        UtilizationStudioController.fetchUtilizationData(JSON.serialize(asset),JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        sfw.Status ='Create New';
        UtilizationStudioController.fetchUtilizationData('',JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        sfw.Status ='All';
        UtilizationStudioController.fetchUtilizationData('','',JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        
        PinnedIdsList.add(asset.Id);
        UtilizationStudioController.fetchUtilizationData(JSON.serialize(asset),JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        
        UtilizationStudioController.getRecordsonLoad(aircraftList.Id);
        UtilizationStudioController.getRecordsonLoad(utilizationlist.Id);
        UtilizationStudioController.getRecordsonLoad(op.Id);
        UtilizationStudioController.getAssetRecordsValidations(aircraftList.Id);
        UtilizationStudioController.saveCommentOnListItems(JSON.serialize(urListItem), 'Utilization_Report_List_Item__c');
        UtilizationStudioController.saveCommentOnListItems('abc', 'Utilization_Report_List_Item__c');
        UtilizationStudioController.updateStatus(JSON.serialize(utilizationlist), '','','');
        utilizationlist.Status__c = 'Submitted To Lessor'; 
        UtilizationStudioController.updateStatus(JSON.serialize(utilizationlist), '','','REJECT');
        LWGlobalUtils.setTriggersflagOff();
        Test.stopTest();
        
    }   
    public class searchFiltersWrapper{
        @AuraEnabled public string Status{get;set;}
        @AuraEnabled public string DateOption{get;set;}
        @AuraEnabled public Date InputDate{get;set;}
        @AuraEnabled public String AssetType{get;set;}
    }
    
    @isTest static void testUtilizationStatusUpdate(){
        Test.startTest();        
        Date dateField = System.today().addmonths(-5); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Aircraft__c aircraftList = [select id,Name,RecordTypeId, RecordType.Name from Aircraft__c LIMIT 1];
        
        Operator__c op = [select id,Name from Operator__c LIMIT 1];
        
        Utilization_Report__c ur = [select id,Name from Utilization_Report__c LIMIT 1];
        List<Constituent_Assembly__c> constituentList = [Select id from Constituent_Assembly__c];
        List<Assembly_Event_Info__c> ListProjEvent = [Select id from Assembly_Event_Info__c];
        
        
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing1',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = aircraftList.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = lastDayOfMonth //Date.newInstance(2020,01,31)
        );
        LWGlobalUtils.setTriggersflagOff(); 
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOn();
        
        UtilizationStudioController.updateStatus(JSON.serialize(utilizationRecord), '','','');

        dateField = System.today().addmonths(-4); 
        numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing1',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = aircraftList.Id,
            Type__c = 'Actual',
            Status__c='Open',
            Month_Ending__c = lastDayOfMonth//Date.newInstance(2020,03,31)
        );
       // LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord1;
       // LWGlobalUtils.setTriggersflagOff();
       Assembly_Utilization__c assemblyUtilization = new Assembly_Utilization__c();
        assemblyUtilization.Name = 'Test Assembly Utilization';
        assemblyUtilization.Constituent_Assembly__c = constituentList[1].Id;
        assemblyUtilization.Utilization_Report__c = utilizationRecord1.Id;
        assemblyUtilization.Unknown_FH_FC__c = true;
        LWGlobalUtils.setTriggersflagOn(); 
        insert assemblyUtilization;
        LWGlobalUtils.setTriggersflagOff();
        
       AircraftUtilizationController.getUtilizationRelatedData(aircraftList.Id);    
        
        Utilization_Report__c trueuputilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = aircraftList.Id,
            Type__c = 'True Up Gross',
            Original_Monthly_Utilization__c =  utilizationRecord1.Id,           
            Status__c='Approved By Lessor',
            Month_Ending__c = lastDayOfMonth
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert trueuputilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        
        List<Utilization_Report_List_Item__c> utilizationReportList = new List<Utilization_Report_List_Item__c>();
        for(Integer i=0;i<3;i++){
            Utilization_Report_List_Item__c utilizationReportRecord = new Utilization_Report_List_Item__c();
            utilizationReportRecord.Comments__c = 'Testing'+i;
            utilizationReportRecord.Utilization_Report__c = utilizationRecord1.Id;
            utilizationReportRecord.Cycles_During_Month__c = 2;
            utilizationReportRecord.Running_Hours_During_Month__c = 3;
            if(i == 0){
                utilizationReportRecord.Constituent_Assembly__c = constituentList[0].Id;
                //utilizationReportRecord.Event_Name__c=ListProjEvent[0].Id;
            }
            else if(i == 1){
                utilizationReportRecord.Constituent_Assembly__c = constituentList[1].Id;
                //utilizationReportRecord.Event_Name__c=ListProjEvent[1].Id;
            }
            else{
                utilizationReportRecord.Constituent_Assembly__c = constituentList[2].Id;
                //utilizationReportRecord.Event_Name__c=ListProjEvent[2].Id;
            }
            utilizationReportRecord.Effective_Unit__c = 'Monthly';
            utilizationReportRecord.Threshold_Units__c = 2;
            utilizationReportRecord.Rate_Up_To_Threshold__c = 3;
            utilizationReportRecord.Threshold_Amount__c = 4 ;
            utilizationReportRecord.Monthly_Threshold_Guaranteed__c = true;
            utilizationReportRecord.Net_Units__c = 3;
            utilizationReportRecord.Rate_Beyond_Threshold__c = 6;
            utilizationReportRecord.TSN_At_Month_Start__c = 4;
            utilizationReportRecord.CSN_At_Month_Start__c = 6;
            utilizationReportRecord.Y_Hidden_MR__c = 9;
            utilizationReportList.add(utilizationReportRecord);
        }
        LWGlobalUtils.setTriggersflagOn();  
        insert utilizationReportList;
        LWGlobalUtils.setTriggersflagOff();
        
        UtilizationStudioController.updateStatus(JSON.serialize(utilizationRecord1), '','','');



        dateField = System.today().addmonths(-3); 
        numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Utilization_Report__c utilizationRecord2 = new Utilization_Report__c(
            Name='Testing1',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = aircraftList.Id,
            Type__c = 'Actual',
            Status__c='Rejected By Lessor',
            Month_Ending__c = lastDayOfMonth
        );
        LWGlobalUtils.setTriggersflagOff(); 
        insert utilizationRecord2;
        LWGlobalUtils.setTriggersflagOn();
        
        UtilizationStudioController.updateStatus(JSON.serialize(utilizationRecord2), '','','');
        UtilizationStudioController.updateStatus('abc', '','','');

        dateField = System.today().addmonths(-2); 
        numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Utilization_Report__c utilizationRecord3 = new Utilization_Report__c(
            Name='Testing1',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = aircraftList.Id,
            Type__c = 'Actual',
            Status__c='Submitted To Lessor',
            Month_Ending__c = lastDayOfMonth
        );
        LWGlobalUtils.setTriggersflagOff(); 
        insert utilizationRecord3;
        LWGlobalUtils.setTriggersflagOn();
        
        UtilizationStudioController.updateStatus(JSON.serialize(utilizationRecord3), '','','');
        
        UtilizationStudioController.AssetDataWrapper adw = new UtilizationStudioController.AssetDataWrapper();
        List<UtilizationStudioController.ComponentWrapper> cwList = new List<UtilizationStudioController.ComponentWrapper>();
        for(Integer i=0; i<3;i++){
            UtilizationStudioController.ComponentWrapper cw = new UtilizationStudioController.ComponentWrapper();
            if (i == 0){
                cw.assembly ='Airframe';
                cw.flightHM = '10'; // Changed from FlightH to FlightHM.
                cw.flightC = 10;
            }
            else if (i == 1){
                cw.assembly ='Engine 1'; 
                cw.flightHM = '10'; // Changed from FlightH to FlightHM.
                cw.flightC = 10;
            }
            else{
                cw.assembly ='APU'; 
                cw.flightHM = '10'; // Changed from FlightH to FlightHM.
                cw.flightC = 10;
            }
            cwList.add(cw);
        }
        
        aircraftList.RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Aircraft').getRecordTypeId();
        adw.Asset = aircraftList;
        adw.Utilization = utilizationRecord1;
        adw.totalFH = 0;
        adw.lstComponentWrapper = cwList;
        UtilizationStudioController.saveFHdata(JSON.serialize(adw));
        
        Aircraft__c testAssest = new Aircraft__c();
        testAssest.Name = 'Test Assest';
        testAssest.MSN_Number__c = 'abc';
        testAssest.Aircraft_Type__c = '727';
        insert testAssest;
        
        UtilizationStudioController.AssemblyWrapper assemblyWrapperRecord = new UtilizationStudioController.AssemblyWrapper();
        assemblyWrapperRecord.cwList = cwList;
        assemblyWrapperRecord.assetId = testAssest.Id;
		
        UtilizationStudioController.getUtilizationRelatedData(testAssest.Id, utilizationRecord1.Id);
        UtilizationStudioController.getTrueUpData(testAssest.Id, utilizationRecord1.Id);
        Test.stopTest();
    } 
    
    @isTest 
    public static void testPinAndPrevious(){
        Test.startTest();
        Invoice__c inv = [Select id,name, Status__c from Invoice__c];
        UtilizationStudioController.sendInvoices(JSON.serialize(inv));
        Aircraft__c aircraftList = [select id,Name from Aircraft__c LIMIT 1];
        
        Operator__c op = [select id,Name from Operator__c LIMIT 1];
        Utilization_Report__c utilizationlist = [select id,Status__c, Month_Ending__c from Utilization_Report__c where Type__c ='Actual'];
        Utilization_Report_List_Item__c urListItem = [Select id, name from Utilization_Report_List_Item__c LIMIT 1];
        
        Aircraft__c asset = new Aircraft__c();
        asset.Id= aircraftList.Id;
        asset.Name= 'Test';
        
        Operator__c Operator = new Operator__c();
        Operator.Id= op.Id;
        
        searchFiltersWrapper sfw = new searchFiltersWrapper();
        sfw.Status ='All';
        sfw.DateOption = 'On Date';
        sfw.InputDate = System.today();
        sfw.AssetType = 'Aircraft';
        List<String> IgnoreIdsList = new List<String>();  
        List<String> PreviousRecordsList= new List<String>(); 
        List<String> PinnedIdsList= new List<String>();
        UtilizationStudioController.getAdditionalUtilization(aircraftList.Id, utilizationlist.Id, String.valueof(utilizationlist.Month_Ending__c));
        UtilizationStudioController.fetchUtilizationData(JSON.serialize(asset),JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'Utilization','');
        sfw.Status ='Open';
        UtilizationStudioController.fetchUtilizationData(JSON.serialize(asset),JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'Utilization','');
        UtilizationStudioController.fetchUtilizationData(JSON.serialize(asset),JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'Utilization','');
        
        sfw.Status ='Create New';
        PinnedIdsList.add(asset.Id);
        
        UtilizationStudioController.fetchUtilizationData(JSON.serialize(asset),'',JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        PreviousRecordsList.add(asset.Id);
        PinnedIdsList.clear();
        UtilizationStudioController.fetchUtilizationData('','',JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        sfw.Status ='Open';
        sfw.DateOption = 'Before Date';
        sfw.InputDate = System.today();
        sfw.AssetType = 'Aircraft';
        PinnedIdsList.clear();
        UtilizationStudioController.fetchUtilizationData('',JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        ShortVersionFH_FC_Controller.getUtilizationData(utilizationlist.Id);
        Test.stopTest();
    }   
    @isTest
    public static void testOperatorSelected(){
        Test.startTest();
        Operator__c testOperator = [Select id from Operator__c];
        Aircraft__c testAircraft = [Select id from Aircraft__c];
        Lease__c leaseRecord1 = new Lease__c(
            Name = 'Testing1',
            Lease_Start_Date_New__c = Date.valueof('2020-08-01'),
            Lease_End_Date_New__c = Date.valueof('2020-08-15'),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id,
            Aircraft__c = testAircraft.Id
        );
        LWGlobalUtils.setTriggersflagOn();
        insert leaseRecord1;
        LWGlobalUtils.setTriggersflagOff();
        
        Lease__c leaseRecord2 = new Lease__c(
            Name = 'Testing2',
            Lease_Start_Date_New__c = Date.valueof('2020-08-16'),
            Lease_End_Date_New__c = Date.valueof('2020-08-20'),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id,
            Aircraft__c = testAircraft.Id
        );
        LWGlobalUtils.setTriggersflagOn();
        insert leaseRecord2;
        LWGlobalUtils.setTriggersflagOff();
        
        Lease__c leaseRecord3 = new Lease__c(
            Name = 'Testing3',
            Lease_Start_Date_New__c = Date.valueof('2020-08-20'),
            Lease_End_Date_New__c = Date.valueof('2020-08-30'),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id,
            Aircraft__c = testAircraft.Id
        );
        LWGlobalUtils.setTriggersflagOn();
        insert leaseRecord3;
        LWGlobalUtils.setTriggersflagOff();
        
        Lease__c leaseRecord4 = new Lease__c(
            Name = 'Testing4',
            Lease_Start_Date_New__c = Date.valueof('2020-08-31'),
            Lease_End_Date_New__c = Date.valueof('2020-09-20'),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id,
            Aircraft__c = testAircraft.Id
        );
        LWGlobalUtils.setTriggersflagOn();
        insert leaseRecord4;
        LWGlobalUtils.setTriggersflagOff();
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.valueof('2019-04-30')//Date.newInstance(2020,02,29)
        );
        //LWGlobalUtils.setTriggersflagOn(); 
         LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord1.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.valueof('2019-05-31')//Date.newInstance(2020,02,29)
        );
        LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord1; 
        LWGlobalUtils.setTriggersflagOff();
        Utilization_Report__c utilizationRecord2 = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord2.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.valueof('2019-05-31')//Date.newInstance(2020,02,29)
        );
        LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord2; 
        Utilization_Report__c utilizationRecord3 = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord3.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.valueof('2020-08-31')//Date.newInstance(2020,02,29)
        );
        LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord3;
        Utilization_Report__c utilizationRecord4 = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord4.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.valueof('2020-08-31')//Date.newInstance(2020,02,29)
        );
        LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord4;
        List<String> IgnoreIdsList = new List<String>();  
        List<String> PreviousRecordsList= new List<String>(); 
        List<String> PinnedIdsList= new List<String>();
        searchFiltersWrapper sfw = new searchFiltersWrapper();
        sfw.Status ='Create New';
        sfw.DateOption = 'All';
        sfw.InputDate = System.today();
        sfw.AssetType = 'Aircraft';
        Operator__c Operator = new Operator__c();
        Operator.Id= testOperator.Id;
        UtilizationStudioController.fetchUtilizationData('',JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
        sfw.Status ='All';
        UtilizationStudioController.fetchUtilizationData('',JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
		UtilizationStudioController.getAssemblyData(testAircraft.Id);
        UtilizationStudioController.updateEmail(testOperator.Id,'test@abc.com');
        UtilizationStudioController.getReconciliationCondition(utilizationRecord2.Id,leaseRecord2.Id);
        UtilizationStudioController.getWarningMsg(utilizationRecord2.Id);
   		Test.stopTest();
    }
     @isTest
    public static void testMultipleLeases(){
        Test.startTest();
        Operator__c testOperator = [Select id,MR_Billing_Contact__c from Operator__c];
        Aircraft__c testAircraft = [Select id from Aircraft__c];
        Lease__c leaseRecord1 = new Lease__c(
            Name = 'Testing1',
            Lease_Start_Date_New__c = Date.valueof('2019-08-01'),
            Lease_End_Date_New__c = Date.valueof('2019-08-15'),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id,
            Aircraft__c = testAircraft.Id
        );
        LWGlobalUtils.setTriggersflagOn();
        insert leaseRecord1;
        LWGlobalUtils.setTriggersflagOff();
        
        Lease__c leaseRecord2 = new Lease__c(
            Name = 'Testing2',
            Lease_Start_Date_New__c = Date.valueof('2019-08-16'),
            Lease_End_Date_New__c = Date.valueof('2019-08-20'),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id,
            Aircraft__c = testAircraft.Id
        );
        LWGlobalUtils.setTriggersflagOn();
        insert leaseRecord2;
        LWGlobalUtils.setTriggersflagOff();
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Y_hidden_Lease__c = leaseRecord1.Id,
            Aircraft__c = testAircraft.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.valueof('2019-08-31')//Date.newInstance(2020,02,29)
        );
         
         LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord2.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = Date.valueof('2019-08-31')//Date.newInstance(2020,02,29)
        );
        LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord1; 
        LWGlobalUtils.setTriggersflagOff();
         
       
        List<String> IgnoreIdsList = new List<String>();  
        List<String> PreviousRecordsList= new List<String>(); 
        List<String> PinnedIdsList= new List<String>();
        searchFiltersWrapper sfw = new searchFiltersWrapper();
        sfw.Status ='Create New';
        sfw.DateOption = 'All';
        sfw.InputDate = System.today();
        sfw.AssetType = 'Aircraft';
        Operator__c Operator = new Operator__c();
        Operator.Id= testOperator.Id;
        UtilizationStudioController.fetchUtilizationData('',JSON.serialize(Operator),JSON.serialize(sfw),IgnoreIdsList,true,PreviousRecordsList,PinnedIdsList,'','');
		
   		Test.stopTest();
    }
}