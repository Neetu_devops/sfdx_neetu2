public without sharing class CurrentLeasesController {
	
    @AuraEnabled
    public static List<LeaseWrapper> getCurrentLeases(){
        
        List<LeaseWrapper> wrapperList = new List<LeaseWrapper>();
        String communityPrefix = UtilityWithoutSharingController.getCommunityPrefix();
        List<Operator__c> operators = UtilityWithSharingController.getOperators();
        
        for(Lease__c lease : [select Id, Lease_Id_External__c, Lessee__r.Name, Lease_Start_Date_New__c,
                                          Lease_End_Date_New__c, Aircraft__r.MSN_Number__c,
                                          Type__c, Lease_Type_Status__c,Operator__r.Name,
                                          Greentime_Lease_External__c, Aircraft__r.RecordType.DeveloperName, Aircraft__c
                                          from Lease__c where lessee__c in : operators]){
            
            String msnURL, submitUR_URL;
                                             
            submitUR_URL = communityPrefix + '/s/submit-ur?recordId=' + UtilityWithoutSharingController.encodeData(lease.Aircraft__c);
                
            if(lease.Aircraft__r.RecordType.DeveloperName == 'Aircraft'){
               msnURL = communityPrefix + '/s/aircraft-details?recordId=';                                  
            }else{
               msnURL = communityPrefix + '/s/engine-details?recordId=';                                     
            }   
                                              
            msnURL += UtilityWithoutSharingController.encodeData(lease.Aircraft__c);
                                                                              
            wrapperList.add(new LeaseWrapper(lease.Lease_Id_External__c, lease.Lessee__r.Name, lease.Operator__r.Name, 
                                             lease.Type__c, lease.Aircraft__r.MSN_Number__c, 
                                             lease.Lease_Start_Date_New__c,lease.Lease_End_Date_New__c,
                                             lease.Greentime_Lease_External__c, lease.Lease_Type_Status__c, msnURL, submitUR_URL)); 
        }
        
        return wrapperList;
    } 
    
    public class LeaseWrapper{
        @AuraEnabled public String title {get;set;}
        @AuraEnabled public String lesseeName {get;set;}
        @AuraEnabled public String operatorName {get;set;}
        @AuraEnabled public String type {get;set;}
        @AuraEnabled public String msn {get;set;}
        @AuraEnabled public String msnURL {get;set;}
        @AuraEnabled public Date leaseStart {get;set;}
        @AuraEnabled public Date leaseEnd {get;set;}
        @AuraEnabled public Boolean greentimeLease {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String submitUR_URL{get;set;}
        @AuraEnabled public String submitURTitle{get;set;}
        
        public LeaseWrapper(String title, String lesseeName, String operatorName, String type, String msn, Date leaseStart,
                           Date leaseEnd, String greentimeLease, String status, String msnURL, String submitUR_URL){
                               
        	this.title = title;
            this.lesseeName = lesseeName;
            this.operatorName = operatorName;
            this.type = type;          
            this.msn = msn;
            this.leaseStart = leaseStart;
            this.leaseEnd = leaseEnd;
            this.greentimeLease = greentimeLease == 'Yes' ? True : false;  
            this.status = status;       
            this.msnURL = msnURL;
            this.submitUR_URL = submitUR_URL;
            submitURTitle = 'Submit Utilization';
        }
    }
}