public class ApplyPaymentController {

    @AuraEnabled
    public static PrePaymentWrapper fetchInitData(Id invoiceId){
        try{
            PrePaymentWrapper ppWrap = new PrePaymentWrapper();
            ppWrap.invoiceRec = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,invoice_type__c from Invoice__c WHERE Id =: invoiceId];

            return ppWrap;
        }
        catch(Exception ex){
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }

    public class PrePaymentWrapper{
        @AuraEnabled public Invoice__c invoiceRec {get;set;}
        @AuraEnabled public Payment_Receipt__c prePaymentRec {get;set;}
        @AuraEnabled public Date prePaymentDate {get;set;}

        public PrePaymentWrapper(){
            this.invoiceRec = new Invoice__c();
            this.prePaymentRec = new Payment_Receipt__c();
            this.prePaymentDate = System.today();
        }
    }
    
    /**
    * @description : Applys the prepayment to the invoice
                     Creates a Payment Reciept Line Item
                     Creates a payment and its journal entries.
    *
    * @param prePaymentData - String, Wrapper object containing all the information entered by the user.
    * @param prePaymentAmountApplied - Double, Amount applied by the User from PrePayment record.
    */
    @AuraEnabled
    public static void applyPrePayment(String prePaymentData, Double prePaymentAmountApplied){
        try{
            PrePaymentWrapper ppWrap = (PrePaymentWrapper) JSON.deserialize(prePaymentData, PrePaymentWrapper.class);
            System.debug('ppWrap: ' + ppWrap);
            
            Decimal amount = 0.0;
            amount = prePaymentAmountApplied;

            Payment_Receipt_Line_Item__c prli = new Payment_Receipt_Line_Item__c();
            prli.Invoice__c = ppWrap.invoiceRec.Id;
            prli.Amount_Paid__c = amount;
            //prli.Bank_Charges__c = ipWrap.bankCharges;
            prli.Payment_Receipt__c = ppWrap.prePaymentRec.Id;
            prli.Prepayment__c = true;
            prli.status__c = 'Pending';
            insert prli;
            
            Payment__c payment = new Payment__c();

            //Removing the code to set the record type since record type of payment is set in the trigger
            
            payment.Invoice__c = prli.Invoice__c;
            payment.Payment_Date__c = ppWrap.prePaymentDate;
            payment.Amount__C = (prli.Amount_Paid__c);
            payment.PrePayment__c = true;
            payment.Prepayment_lkp__c = ppWrap.prePaymentRec.Id;
            payment.Y_Hidden_Payment_Std_Line_Item__c = prli.id;
            insert payment;
	    
            //payment.Bank_Charges__c = prli.Bank_Charges__c;// for Accounting
        }catch(Exception ex){
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }
}