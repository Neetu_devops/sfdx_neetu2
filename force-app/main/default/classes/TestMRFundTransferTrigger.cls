@isTest
private class TestMRFundTransferTrigger {

    @testSetup
    static void setup() {
        Id recordId = Schema.SObjectType.Lease__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        
        List<Lease__c> leases = new List<lease__c>();
        Lease__c lease1 = new Lease__c(Name='TestLease1', Lease_End_Date_New__c=Date.today()+20, Lease_Start_Date_New__c=Date.today()-10, recordtypeid=recordId);
        Lease__c lease2 = new Lease__c(Name='TestLease2', Lease_End_Date_New__c=Date.today()+20, Lease_Start_Date_New__c=Date.today()-20, recordtypeid=recordId);
        Lease__c lease3 = new Lease__c(Name='TestLease3', Lease_End_Date_New__c=Date.today()+20, Lease_Start_Date_New__c=Date.today()-5, recordtypeid=recordId);
        leases.add(lease1);
        leases.add(lease2);
        leases.add(lease3);
        insert leases;

        // Set up the Aircraft record
		String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN, Date_of_Manufacture__c = System.today(),TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available', Derate__c=20);
        LeaseWareUtils.clearFromTrigger();
        insert a;

         // Set up the Aircraft record
		String aircraftMSN1 = '54321';
        Aircraft__c ac = new Aircraft__c(MSN_Number__c=aircraftMSN1, Date_of_Manufacture__c = System.today(),TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available', Derate__c=20);
        LeaseWareUtils.clearFromTrigger();
        insert ac;

        //update lease reocrd with aircraft
        lease1.Aircraft__c=a.id;
        LeaseWareUtils.clearFromTrigger();
        update lease1;

        //update lease reocrd with aircraft
        lease2.Aircraft__c=ac.id;
        LeaseWareUtils.clearFromTrigger();
        update lease2;

        map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);

        // Set up the Constt Assmbly record
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= a.Id, Description__c ='test description', TSN__c=0, CSN__c=0, Type__c ='Engine 1', Serial_Number__c='123');
        LeaseWareUtils.clearFromTrigger();
		insert consttAssembly;
		
		Constituent_Assembly__c consttAssembly1 = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= ac.Id, Description__c ='test description', TSN__c=0, CSN__c=0, Type__c ='Airframe', Serial_Number__c='123');
        LeaseWareUtils.clearFromTrigger();
		insert consttAssembly1;
		
		// Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
		    Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
		
		Maintenance_Program_Event__c mpe1= new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
			Interval_2_Hours__c=20,
            Event_Type_Global__c = '4C/6Y'
        );
        insert mpe1;
		
        // Set up Project Event for engine
        Assembly_Event_Info__c event = new Assembly_Event_Info__c();
        event.Name = 'Test Info';
        event.Event_Cost__c = 20;
        event.Constituent_Assembly__c = consttAssembly.Id;
        event.Maintenance_Program_Event__c = mpe.Id;
        insert event;
		
		// Set up Project Event for Airframe
        Assembly_Event_Info__c event1 = new Assembly_Event_Info__c();
        event1.Name = 'Test Info';
        event1.Event_Cost__c = 20;
        event1.Constituent_Assembly__c = consttAssembly1.Id;
        event1.Maintenance_Program_Event__c = mpe1.Id;
        insert event1;
                    
        List<Assembly_MR_Rate__c> assemblyRates = new List<Assembly_MR_Rate__c> { 
                new Assembly_MR_Rate__c(Lease__c = leases[1].Id, Name = 'RateInfo 1', Base_MRR__c = 500,Rate_Basis__c='Cycles', Assembly_Lkp__c = consttAssembly1.Id, Assembly_Event_Info__c = event1.id),
                new Assembly_MR_Rate__c(Lease__c = leases[0].Id, Name = 'RateInfo 2', Base_MRR__c = 1500,Rate_Basis__c='Hours', Assembly_Lkp__c = consttAssembly.Id, Assembly_Event_Info__c = event.id)
        };
        insert assemblyRates;
        
    }
    @isTest
    private static void testSumTransferAmount() {
        
        List<Lease__c> leases = [SELECT Id FROM Lease__c];
        List<Assembly_MR_Rate__c> assemblyMRInfo = [SELECT Id FROM Assembly_MR_Rate__c];
        Test.startTest();
        MR_Fund_Transfer__c mrFundTransfer = new MR_Fund_Transfer__c(Name='TestMRFund1', Source_Lease__c=leases[0].Id,
                                                                     Target_Lease__c=leases[1].Id, Status__c='Pending',
                                                                     Date__c = Date.today());
        insert mrFundTransfer;
        List<Assembly_MR_Fund_Transfer__c> assemblyMRFundTransfer = new List<Assembly_MR_Fund_Transfer__c>{
            new Assembly_MR_Fund_Transfer__c(MR_Fund_Transfer__c=mrFundTransfer.Id, Source_Assembly_MR_Lookup__c=assemblyMRInfo[0].Id,
            Target_Assembly_MR_Lookup__c=assemblyMRInfo[1].Id, Transfer_Amount__c=100),
            new Assembly_MR_Fund_Transfer__c(MR_Fund_Transfer__c=mrFundTransfer.Id, Source_Assembly_MR_Lookup__c=assemblyMRInfo[0].Id,
            Target_Assembly_MR_Lookup__c=assemblyMRInfo[1].Id, Transfer_Amount__c=100)};
                
        insert assemblyMRFundTransfer;
        
        mrFundTransfer.Status__c='Approved';
        LeaseWareUtils.clearFromTrigger();
        update mrFundTransfer;
        Test.stopTest();

        List<Assembly_MR_Rate__c> approvedAssemblyMRInfo = [SELECT Id, Total_Transfers__c FROM Assembly_MR_Rate__c];

        Supplemental_Rent_Transactions__c[] listCurSS = [Select id, Name, Transaction_Type__c,Sequence_Number__c,
        Amount_Cash__c,Amount_Cash_Sum__c,Amount_Invoiced_Sum__c,Amount_Invoiced__c,Supplemental_Rent__r.Name,
        Transaction_Date__c,Running_Balance_Cash_Collected__c,Running_Balance_Invoiced__c,Running_Balance_Cash__c,
        Running_Balance_Invoice__c from Supplemental_Rent_Transactions__c where
        Supplemental_Rent__c =: approvedAssemblyMRInfo[0].Id order by Sequence_Number__c asc];

        System.debug('Transaction : ' + listCurSS );
        System.debug('Transaction : ' + listCurSS.size() );

        for(Assembly_MR_Rate__c assembly : approvedAssemblyMRInfo) {
            if(assembly.Id == assemblyMRInfo[0].Id) {
                System.debug('Total_Transfers__c =' + approvedAssemblyMRInfo[0].Total_Transfers__c);
                System.assertEquals(-200, approvedAssemblyMRInfo[0].Total_Transfers__c);
            }
            if(assembly.Id == assemblyMRInfo[1].Id) {
                System.debug('Total_Transfers__c 2 = ' + approvedAssemblyMRInfo[1].Total_Transfers__c);
                System.assertEquals(200, approvedAssemblyMRInfo[1].Total_Transfers__c);
            }
        } 
    }
}