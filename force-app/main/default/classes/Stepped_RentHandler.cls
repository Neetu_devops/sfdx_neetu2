public class Stepped_RentHandler implements ITrigger{
	/* Hint This Handler file is stepped_RentHandler but this is used for SteppedMultiRentScheduler object. */
    
    // Constructor
    private final string triggerBefore = 'Stepped_RentHandlerBefore';
    private final string triggerAfter = 'Stepped_RentHandlerAfter';
    private map<ID,Stepped_Rent__c> mapMRSchToStepMultiRent = new map<ID,Stepped_Rent__c>();
    
    public Stepped_RentHandler()
    {   
    }
    //Making functionality more generic.
    private void setSOQLStaticMAPOrList(){
        system.debug('Stepped_RentHandler::setSOQLStaticMAPOrList - Enter');
        SteppedMultiRent__c[] newSteppedMultiRentList = (list<SteppedMultiRent__c>)(trigger.IsDelete?trigger.old:trigger.new);
    
        //Set LeaseID of new
        set<Id> setMultiRentId = new set<Id>(); // this set is for MulitRentSchedule id
        set<Id> setDelMRSchID = new set<Id>(); // this set for deleted records
                
        //Iterating over each Stepped Rent
        for(SteppedMultiRent__c curSR: newSteppedMultiRentList){
            setMultiRentId.add(curSR.Multi_Rent_Schedule__c);
            If(Trigger.isDelete){// for deletion, keeping track of deleted steppeed rent.
                setDelMRSchID.add(curSR.Id);
            }
            
        }//End of For
        // anjani : removed the conditional fetch. As the data can change from Before to After.
        //if (listLease == NULL || listLease.isEmpty() )  
        {

			mapMRSchToStepMultiRent = new map<ID,Stepped_Rent__c>([select id, name,Base_Rent__c,Start_Date__c,Rent_End_Date__c,off_wing_rent__c
                                        ,(select id,name,Multi_Rent_Schedule__c,Stepped_Rent_Start_Date__c,Rent_Amount__c,Applicable_Rent__c,Rent_Percent__c 
                                            from SteppedMultiRentSch__r 
                                            where id not in: setDelMRSchID
                                            order by Stepped_Rent_Start_Date__c asc) // exclude in case of deletion
                                    from Stepped_Rent__c 
                                    where id in:setMultiRentId ]);
		}
        
        system.debug('Stepped_RentHandler::setSOQLStaticMAPOrList - Exit');
    }
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
        setSOQLStaticMAPOrList();
    }
    public void bulkAfter()
    {
        setSOQLStaticMAPOrList();
    }   
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);  
        system.debug('RentTriggerHandler.beforeInsert(+)');

		//Task - SteppedMulti Rent Schedule : User is allowed to edit Rent Amount
        //The check box Amount increase/decrease?, if it is checked then user has to enter %increase/Decrease and Rent Amount is calculated,
        //if it is unchecked then user has to enter Rent Amount and  %increase/Decrease? calculated
        
        
        SteppedMultiRent__c[] newSteppedRentList = (list<SteppedMultiRent__c>)(trigger.IsDelete?trigger.old:trigger.new);
        
        for (SteppedMultiRent__c curSteprent:newSteppedRentList){
            Stepped_Rent__c curMultiRentSch = mapMRSchToStepMultiRent.get(curSteprent.Multi_Rent_Schedule__c);
        
            
            if( curSteprent.Amount_Incr_Decr__c == TRUE) {
                if (curSteprent.Rent_Amount__c == NULL) continue;
                if (curSteprent.Rent_Amount__c !=NULL && curMultiRentSch.Base_Rent__c !=NULL )
                    curSteprent.Rent_Percent__c=((curSteprent.Rent_Amount__c - curMultiRentSch.Base_Rent__c ) /curMultiRentSch.Base_Rent__c)*100;
                else 
                    curSteprent.Rent_Percent__c= NULL;
                
            }else {
                if (curSteprent.Rent_Percent__c == NULL) continue;
                if (curSteprent.Rent_Percent__c !=NULL && curMultiRentSch.Base_Rent__c !=NULL )
                    curSteprent.Rent_Amount__c=curMultiRentSch.Base_Rent__c*(1+curSteprent.Rent_Percent__c/100);
                else 
                    curSteprent.Rent_Amount__c= NULL;
            }
        }
                     
        system.debug('RentTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);  
        system.debug('RentTriggerHandler.beforeUpdate(+)');
       
        //Task - SteppedMulti Rent Schedule
        //A check box is provided Amount increase/decrease?, if it is checked then user has to enter %Increase/Decrease? and Rent Amount is calculated,
        //if it is unchecked then user has to enter Rent Amount and %Increase/Decrease? is calculated
        
        SteppedMultiRent__c[] newSteppedRentList = (list<SteppedMultiRent__c>)(trigger.IsDelete?trigger.old:trigger.new);
        
        for (SteppedMultiRent__c curSteprent:newSteppedRentList){
            Stepped_Rent__c curMultiRentSch = mapMRSchToStepMultiRent.get(curSteprent.Multi_Rent_Schedule__c);
            SteppedMultiRent__c oldStepRent =  (SteppedMultiRent__c)Trigger.oldMap.get(curSteprent.Id);
 
            if (curSteprent.Amount_Incr_Decr__c != oldStepRent.Amount_Incr_Decr__c ||
                curSteprent.Rent_Percent__c!=oldStepRent.Rent_Percent__c || 
                curSteprent.Rent_Amount__c!=oldStepRent.Rent_Amount__c) {
                    if (curSteprent.Rent_Amount__c == NULL) continue;
                if( curSteprent.Amount_Incr_Decr__c == TRUE) {
                    if (curSteprent.Rent_Amount__c == NULL) continue;
                    if (curSteprent.Rent_Amount__c !=NULL && curMultiRentSch.Base_Rent__c !=NULL )
                        curSteprent.Rent_Percent__c=((curSteprent.Rent_Amount__c - curMultiRentSch.Base_Rent__c ) /curMultiRentSch.Base_Rent__c)*100;
                    else 
                        curSteprent.Rent_Percent__c= NULL;
                    
                }else {
                     if (curSteprent.Rent_Percent__c == NULL) continue;
                     if (curSteprent.Rent_Percent__c !=NULL && curMultiRentSch.Base_Rent__c !=NULL )
                        curSteprent.Rent_Amount__c=curMultiRentSch.Base_Rent__c*(1+curSteprent.Rent_Percent__c/100);
                    else 
                        curSteprent.Rent_Amount__c= NULL;
                                
                }
            }

        }
        system.debug('RentTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
		//UpdateApplicableRent();
    }
     
    public void afterInsert()
    {
		if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);   
        system.debug('RentTriggerHandler.afterInsert(+)');
        //UpdateApplicableRent()    ; 
        system.debug('RentTriggerHandler.afterInsert(-)');

    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter); 
        system.debug('RentTriggerHandler.afterUpdate(+)');
    	//UpdateApplicableRent()   ;  
		system.debug('RentTriggerHandler.afterUpdate(-)');
    }
     
    public void afterDelete()
    {
           
    }

    public void afterUnDelete()
    {
            
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
    }
/*	
	//After Update, After Insert, Before Delete
	public void UpdateApplicableRent(){
		
		SteppedMultiRent__c[] newSteppedRentList = (list<SteppedMultiRent__c>)(trigger.IsDelete?trigger.old:trigger.new);
				
		//Set LeaseID of new
		set<Id> newSRMRSchIdSet = new set<Id>(); // this is for Lease id set
		set<Id> delSRID = new set<Id>(); // this set for deleted records
				
		//Iterating over each Stepped Rent
		for(SteppedMultiRent__c curSR: newSteppedRentList){
			newSRMRSchIdSet.add(curSR.Multi_Rent_Schedule__c);
			If(Trigger.isDelete){// for deletion, keeping track of deleted steppeed rent.
				delSRID.add(curSR.Id);
			}
			
		}//End of For Loop
		
		String newApplicableRentStr='';
		string EndDatestr;
		Decimal BaseRent;
		for(Stepped_Rent__c curMultiRentSch: mapMRSchToStepMultiRent.values()){
			newApplicableRentStr=' ';
			
			BaseRent = curMultiRentSch.Base_Rent__c==null?0:curMultiRentSch.Base_Rent__c;
			
			if(curMultiRentSch.SteppedMultiRentSch__r.size()>0){
				newApplicableRentStr += ' From '+
					curMultiRentSch.Start_Date__c.format() +' To '+ curMultiRentSch.SteppedMultiRentSch__r[0].Stepped_Rent_Start_Date__c.adddays(-1).format() + ': $'+ BaseRent.format() + '\n'+' ';
			}else{
				newApplicableRentStr += ' From '+
					curMultiRentSch.Start_Date__c.format() +' To '+ curMultiRentSch.Rent_End_Date__c.format() + ': $'+ BaseRent.format() + '\n'+' ';
			}
			
			system.debug('curMultiRentSch.SteppedMultiRentSch__r size='+curMultiRentSch.SteppedMultiRentSch__r.size());
			EndDatestr = null;
			for(integer i=0; i<curMultiRentSch.SteppedMultiRentSch__r.size(); i++){
				
				if( (i+1) >= curMultiRentSch.SteppedMultiRentSch__r.size() ){
					EndDatestr=curMultiRentSch.Rent_End_Date__c.format();
				}else{
					EndDatestr=curMultiRentSch.SteppedMultiRentSch__r[i+1].Stepped_Rent_Start_Date__c.adddays(-1).format();
				}
                system.debug('++++++ newApplicableRentStr' +newApplicableRentStr);
				system.debug('++++++ Stepped_Rent_Start_Date__c' +curMultiRentSch.SteppedMultiRentSch__r[i].Stepped_Rent_Start_Date__c);
                system.debug('++++++ EndDatestr ' +EndDatestr);
                system.debug('++++++ Rent_Amount__c' +curMultiRentSch.SteppedMultiRentSch__r[i].Rent_Amount__c);
                try{
					newApplicableRentStr += 'From '+  
						curMultiRentSch.SteppedMultiRentSch__r[i].Stepped_Rent_Start_Date__c.format() + ' To '+ EndDatestr +': $'+curMultiRentSch.SteppedMultiRentSch__r[i].Rent_Amount__c.format() + '\n'+' ';
				}catch(exception e){	
					system.debug('Ignore (old existing records) - exception e ' + e);
                }
			}
            curMultiRentSch.Applicable_Rent_MRSch__c= newApplicableRentStr;
		}
		
		LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled
		update mapMRSchToStepMultiRent.values();
		LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger enable
    }
 */   
}