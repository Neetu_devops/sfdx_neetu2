/**
*******************************************************************************
* Class: CommercialTermRelatedListController
* @author Created by Bhavna, Lease-Works, 
* @date 27/05/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to create CommercialTerm Related List with Assets MultiSelectLookup 
*   and one can Edit, Delete and Clone CommercialTerms.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*
********************************************************************************
*/
public with sharing class CommercialTermRelatedListController {
    /**
    * @description Retreives all details related to CommercialTerms and its Related Asstes 
    *   for given Deal with respect to given fieldset 
    *
    * @param dealId - String, contains selected Deal's Id
    *
    * @return ctw - CommercialTermWrapper, CommercialTermWrapper record
    */
    @AuraEnabled
    public static CommercialTermWrapper getDealCommercialTerms(String dealId, String fieldSetName, String sortOrderField) {
        Map<String, Pricing_Run_New__c> commercialTermById = new Map<String, Pricing_Run_New__c>();
        Map<String, List<Aircraft__c>> assetsByCTId = new Map<String, List<Aircraft__c>>();
        Map<String, List<Aircraft_Proposal__c>> assetsMSNByCTId = new Map<String, List<Aircraft_Proposal__c>>();
        Map<String, List<Pricing_Output_New__c>> eaByCTId = new Map<String, List<Pricing_Output_New__c>>();
        
        List<ColumnWrapper> colList = new List<ColumnWrapper>();
        List<ChildWrapper> cwList = new List<ChildWrapper>();    
        
        //Retrieving CommercialTerm fieldset
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        Schema.FieldSet ctFieldSet = RLUtility.describeFieldSet(objectDescribe, fieldSetName);
        
        //Dynamic query for CommercialTerm
        String ctQuery = 'Select Id';        
        for(Schema.FieldSetMember fsm : ctFieldSet.getFields()) {
            if (fsm.getType() == Displaytype.reference){
                if (fsm.getFieldPath().contains('__c')){
                    ctQuery += ',' + Utility.replaceRefField(String.valueOf(fsm.getFieldPath()), 'c', 'r.name');
                }
                else {
                    ctQuery += ',' + fsm.getFieldPath().left(String.valueOf(fsm.getFieldPath()).length() - 2) + '.Name' ;
                }                
            }
            
            ctQuery += ', ' + fsm.getFieldPath();   
            
            //creating columnwrapper list for DataTable headers
            ColumnWrapper cw = new ColumnWrapper(fsm.getLabel(), fsm.getFieldPath(), String.valueOf(fsm.getType()));            
            colList.add(cw);
        }       
        ctQuery += ' from ' + leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c where ' + leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c = :dealId';
        
        if (sortOrderField != null && sortOrderField != ''){
            ctQuery += ' order by '+sortOrderField;
        }
        else{
            ctQuery += ' order by CreatedDate';
        }

        //Querying Commercial Terms
        for(Pricing_Run_New__c ctRecord :  Database.query(ctQuery)) {
            commercialTermById.put(ctRecord.Id, ctRecord);
        }
        
        
        //Query Economic Analysis Records for queried Commercial Terms for given DealId
        for(Pricing_Output_New__c eaRecord :  [Select Id, Name, Pricing_Run__c, Asset_Term__c, Asset_Term__r.Name, 
                                               Asset_Term__r.Aircraft__c, Asset_Term__r.Aircraft__r.Name, 
                                               Asset_Term__r.MSN_E__c 
                                               from Pricing_Output_New__c 
                                               where Pricing_Run__c IN : commercialTermById.keySet()]) {
            if(eaRecord.Asset_Term__c != null && eaRecord.Asset_Term__r.Aircraft__c != null) {
                //Assets Map 
                Aircraft__c asset = new Aircraft__c(Id = eaRecord.Asset_Term__r.Aircraft__c, 
                                                    Name = eaRecord.Asset_Term__r.Aircraft__r.Name);
                
                if(!assetsByCTId.containsKey(eaRecord.Pricing_Run__c)) {
                    assetsByCTId.put(eaRecord.Pricing_Run__c, new List<Aircraft__c>());
                }
                assetsByCTId.get(eaRecord.Pricing_Run__c).add(asset);
                 
                //Economic Analysis Map 
                if(!eaByCTId.containsKey(eaRecord.Pricing_Run__c)) {
                     eaByCTId.put(eaRecord.Pricing_Run__c, new List<Pricing_Output_New__c>());                                   
                }
                eaByCTId.get(eaRecord.Pricing_Run__c).add(eaRecord);
            }    
            else if(eaRecord.Asset_Term__c != null && eaRecord.Asset_Term__r.MSN_E__c != null && eaRecord.Asset_Term__r.MSN_E__c != '') {
                Aircraft_Proposal__c assetMSN = new Aircraft_Proposal__c(Id=eaRecord.Asset_Term__c, 
                                                                         Name=eaRecord.Asset_Term__r.Name, 
                                                                         MSN_E__c=eaRecord.Asset_Term__r.MSN_E__c);
                
                if(!assetsMSNByCTId.containsKey(eaRecord.Pricing_Run__c)) {
                    assetsMSNByCTId.put(eaRecord.Pricing_Run__c, new List<Aircraft_Proposal__c>());
                }
                assetsMSNByCTId.get(eaRecord.Pricing_Run__c).add(assetMSN);               
            }
        }
              
        //creating Assets List and Economic Analysis List
        if(commercialTermById.keySet().size() > 0) {
            for(String ctId : commercialTermById.keySet()) {
                List<Aircraft__c> assetList = new List<Aircraft__c>();
                List<Pricing_Output_New__c> eaList = new List<Pricing_Output_New__c>();
                List<Aircraft_Proposal__c> msnList = new List<Aircraft_Proposal__c>();
                    
                if(assetsByCTId.containsKey(ctId)) {
                    assetList.addAll(assetsByCTId.get(ctId));
                }
                
                if(eaByCTId.containsKey(ctId)) {
                    eaList.addAll(eaByCTId.get(ctId));
                }
                
                if(assetsMSNByCTId.containsKey(ctId)) {
                    msnList.addAll(assetsMSNByCTId.get(ctId));
                }
                
                //creating ChildWrapper
                ChildWrapper cw = new ChildWrapper(commercialTermById.get(ctId), assetList, eaList, msnList);
                cwList.add(cw);
            }
        }
        
        
        //creating RelatedListWrapper
        CommercialTermWrapper ctw = new CommercialTermWrapper(colList, cwList, objectDescribe.getLabelPlural());
        
        return ctw;
    }
    
    
    /**
    * @description Deletes given CommercialTerm 
    *
    * @param commercialTermId - String, contains selected CommercialTerm's Id
    *
    * @return String, message of success or failure
    */
    @AuraEnabled
    public static void deleteCommercialTerm(String commercialTermId, Boolean showAssetsInDealSelector,  Boolean deleteAIDOnCTDeletion) {
        Savepoint sp = Database.setSavepoint();
        try {
            List<Aircraft_Proposal__c> assetDelList = new List<Aircraft_Proposal__c>();
            Set<String> assetIds = new Set<String>();
            
            //Querying Commercial Term record to be deleted
            Pricing_Run_New__c ctRecord = [Select Id, Marketing_Activity__c 
                                           from Pricing_Run_New__c 
                                           where Id = : commercialTermId];
            
            //Querying Assests in Deal related to Commercial Term
            List<Pricing_Output_New__c> economicAnalysis = [Select Id, Asset_Term__c 
                                                   from Pricing_Output_New__c 
                                                   where Pricing_Run__c =: commercialTermId];

            for(Pricing_Output_New__c eaRecord : economicAnalysis) {
                if(eaRecord.Asset_Term__c != null) {
                    assetIds.add(eaRecord.Asset_Term__c);
                }
                
            }
            
            if(assetIds.size() > 0) {
                Set<String> assetDealIds = new Set<String>();
                List<Aircraft_Proposal__c> assetList = new List<Aircraft_Proposal__c>();
                
                for(Pricing_Output_New__c eaRecord : [Select Id, Asset_Term__c 
                                                      from Pricing_Output_New__c 
                                                      where Asset_Term__c IN :assetIds 
                                                      AND Pricing_Run__c != :commercialTermId 
                                                      AND Pricing_Run__r.Marketing_Activity__c = :ctRecord.Marketing_Activity__c]) {
                    if(assetIds.contains(eaRecord.Asset_Term__c)) {
                        assetIds.remove(eaRecord.Asset_Term__c);
                    }
                }
                
                if(showAssetsInDealSelector && deleteAIDOnCTDeletion && assetIds.size() > 0) {
                    //delete [select id from Aircraft_Proposal__c where id in: assetIds];
                }
                else if(!showAssetsInDealSelector && assetIds.size() > 0){
                    //delete [select id from Aircraft_Proposal__c where id in: assetIds];
                }
            }
            
            //Deleting Commercial Term
            delete ctRecord;
        } catch(Exception ex) {
            Database.rollback(sp);

            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }      
    
    /**
    * @description Deletes given CommercialTerm 
    *
    * @param assetsInDeal - List of all selected Asset and MSN records
    * @param mapAssetsMSNAid - Map with Asset/ MSn Id with AssetInDeal recordid.
    * @return Map of Asset/MSN id to AID record ID.
    */
    public static Map<String, String> updateMapValues(List<Aircraft_Proposal__c> assetsInDeal, Map<String, String> mapAssetsMSNAid){            
        for (Aircraft_Proposal__c ap : assetsInDeal){
            if (ap.Aircraft__c != null){
                mapAssetsMSNAid.put(ap.Aircraft__c, ap.Id);
            }
            else {
                mapAssetsMSNAid.put(ap.MSN_E__c, ap.Id);
            }
        }
        return mapAssetsMSNAid;
    }

    /**
    * @description creates Assets in Deal records for related deal and EconomicAnalysis records for given CommercialTerm 
    *   from the given list of Assets and also deletes Assets and Economic Analysis records which are removed from selection
    *
    * @param commercialTermId - Id, contains selected CommercialTerm's Id
    * @param dealId - Id, given Deal Id
    * @param assetsList - List<Aircraft_Proposal__c>, Assets in Deal record list
    * @param eaList - List<Pricing_Output_New__c>, EconomicAnalysis record list
    * @param serialNumber - String, Manual Serial Number for Assets in Deal
    * @param msnList - List<String>, List of MSN Ids
    * @param actionType - String, action which is performed like Clone, Edit or New
    * @param assetInDealList - List of Asset In Deal Record Selected
    * @param isAISSelected is AssetIn Deal Lookup visible
    * @return String, message of success or failure
    */
    @AuraEnabled
    public static void processRelatedRecords(Id commercialTermId, Id dealId, List<Aircraft__c> assetsList, 
                                               String serialNumber, List<String> msnList, String actionType, 
                                               List<Aircraft_Proposal__c> assetInDealList, Boolean isAISSelected) {
        
        Savepoint sp = Database.setSavepoint();
        try {
            Marketing_Activity__c deal = [Select Id, RecordType.Name from Marketing_Activity__c where Id = :dealId];
            Pricing_Run_New__c commercialTerm = [Select Id, RecordType.Name from Pricing_Run_New__c where Id = :commercialTermId];

            String assetRecordType;
            if (Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByName().get(deal.RecordType.Name) != null) {    
                assetRecordType = Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByName().get(deal.RecordType.Name).getRecordTypeId();
            }
            System.debug('assetRecordType'+assetRecordType);

            String eaRecordType; 
            if (Schema.SObjectType.Pricing_Output_New__c.getRecordTypeInfosByName().get(commercialTerm.RecordType.Name) != null) {
                eaRecordType = Schema.SObjectType.Pricing_Output_New__c.getRecordTypeInfosByName().get(commercialTerm.RecordType.Name).getRecordTypeId();
            }
            System.debug('eaRecordType'+eaRecordType);
            
            //String dealId, String commercialTermId, string action
            if(isAISSelected){
                createEAFromAiDRecords(commercialTermId, dealId, assetInDealList, assetRecordType, eaRecordType);
            }
            else{
                createAssetsInDealRecords(commercialTermId, dealId, assetsList, serialNumber, msnList, actionType,  assetRecordType, eaRecordType);
            }
        }
        catch(Exception ex) {
            Database.rollback(sp);
            
            System.debug('Error: '+ex.getMessage()+' --- > '+ex.getStackTraceString()+'@ Line: '+ex.getLineNumber());
            throw new AuraHandledException(Utility.getErrorMessage(ex));
        }
    }


    public static void createEAFromAiDRecords(Id commercialTermId, Id dealId, List<Aircraft_Proposal__c> assetInDealList, String assetRecordType, String eaRecordType){
        List<String> assetsInDealIds = new List<String>();
        for(Aircraft_Proposal__c key : assetInDealList){
            assetsInDealIds.add(key.Id);
        } 
        
        Set<String> existingEArecords = new Set<string>();
        List<Pricing_Output_New__c> economicAnalysis = new List<Pricing_Output_New__c>();
        Map<String,string> mapAiDEAId = new Map<String,String>();

        for (Pricing_Output_New__c ea: [select id, Asset_term__c from Pricing_Output_New__c 
                                        where Pricing_Run__c =: commercialTermId]){
            mapAiDEAId.put(ea.Asset_term__c, ea.Id);
        }
        for (String aid : assetsInDealIds){
            if (!mapAiDEAId.containsKey(aid)){
                Pricing_Output_New__c eaRecord = new Pricing_Output_New__c();
                eaRecord.Pricing_Run__c = commercialTermId;
                eaRecord.Asset_Term__c = aid;
                
                if(eaRecordType != null) { eaRecord.RecordTypeId = eaRecordType; }
                economicAnalysis.add(eaRecord);
            }
        }
        if(!economicAnalysis.isEmpty()){
            insert economicAnalysis;
        }
        
        Set<String> itemsToDelete = new Set<String>();
        for (string s : mapAiDEAId.keyset()){ //4 {a,b,c,d}
            if (!assetsInDealIds.contains(s)){ // {a,b,d,e}
                itemsToDelete.add(mapAiDEAId.get(s));
            }
        }
        if (itemsToDelete.size() > 0){
            delete [select id from Pricing_Output_New__c where Pricing_Run__c =: commercialTermId AND
                   Id in: itemsToDelete];
        }
    }


    public static void createAssetsInDealRecords(Id commercialTermId, Id dealId, List<Aircraft__c> assetsList, String serialNumber, List<String> msnList, String actionType, String assetRecordType, String eaRecordType){
        Set<String> selectedAssets = new Set<String>();
        for(Aircraft__c key : assetsList){
            selectedAssets.add(key.Id);
        }
        List<Aircraft_Proposal__c> assetsInDeal = new List<Aircraft_Proposal__c>();
        List<Pricing_Output_New__c> economicAnalysis = new List<Pricing_Output_New__c>();
        // check for existing AID related to Deal
        Map<String, String> mapAssetsMSNAid = new Map<String, String>();
        List<Aircraft_Proposal__c> existingAssetsInDeal = [select id,Aircraft__c,MSN_E__c from Aircraft_Proposal__c 
                        where Marketing_Activity__c =: dealId ];
        
        mapAssetsMSNAid = updateMapValues(existingAssetsInDeal, mapAssetsMSNAid);
        string keyPrefix = Aircraft__c.sobjecttype.getdescribe().getKeyPrefix();
        
        List<String> manualSerialNumbers = new List<String>();
        actionType = actionType.toLowerCase();
        
        if (actionType == 'new' ){
            if(serialNumber != null && serialNumber != ''){
                for(String s : serialNumber.split(',')){
                    manualSerialNumbers.add(s.trim());
                }
            }
            selectedAssets.addAll(manualSerialNumbers);
            for (String assetId : selectedAssets){
                // if selected asset doesn't exist in Asset in deal then create AID
                if (!mapAssetsMSNAid.containskey(assetId)){
                    Aircraft_Proposal__c aid = new Aircraft_Proposal__c();
                    aid.Marketing_Activity__c = dealId;

                    if(assetRecordType != null){ aid.RecordTypeId = assetRecordType; }

                    if (assetId.startsWith(keyPrefix)){ aid.Aircraft__c = assetId; }
                    else { 
                        aid.MSN_E__c = assetId; 
                    }
                    assetsInDeal.add(aid);
                }
            }
            if(!assetsInDeal.isEmpty()){
                insert assetsInDeal;
            }

            // update the map with new asset in deal
            mapAssetsMSNAid = updateMapValues(assetsInDeal,mapAssetsMSNAid);
            
            for (String assetId : selectedAssets){
                Pricing_Output_New__c eaRecord = new Pricing_Output_New__c();
                eaRecord.Pricing_Run__c = commercialTermId;
                eaRecord.Asset_Term__c = mapAssetsMSNAid.get(assetId);
                
                if(eaRecordType != null) { eaRecord.RecordTypeId = eaRecordType; }
                economicAnalysis.add(eaRecord);
            }
            insert economicAnalysis;
        }
        if (actionType == 'edit' || actionType == 'clone') {
            for(String key : msnList){
                manualSerialNumbers.add(key);
            }
            if(serialNumber != null && serialNumber != ''){
                for(String s : serialNumber.split(',')){
                    manualSerialNumbers.add(s.trim());
                }
            }
            selectedAssets.addAll(manualSerialNumbers);
            // new or existing
            
            //existingAssetsInDeal //mapAssetsMSNAid
            Map<String, string> mapAiDName = new Map<String,string>();
            for (string s : mapAssetsMSNAid.keyset()){
                mapAiDName.put(mapAssetsMSNAid.get(s), s);
            }
            
            // selectedAssets (AssetId, MSN, AIDId)
            //mapAssetsMSNAid>>>{MSN1=AIDId, MSN2=AIDId, AssetId=AIDId}
            for (String assetId: selectedAssets){
                if (!mapAssetsMSNAid.containskey(assetId) && !mapAssetsMSNAid.values().contains(assetId)){
                    // new asset
                    Aircraft_Proposal__c aid = new Aircraft_Proposal__c();
                    aid.Marketing_Activity__c = dealId;

                    if(assetRecordType != null){ aid.RecordTypeId = assetRecordType; }

                    if (assetId.startsWith(keyPrefix)){ aid.Aircraft__c = assetId; }
                    else { 
                        String assetName = assetId;
                        if (mapAiDName.containskey(assetId)){
                            assetName = mapAiDName.get(assetId);
                        }
                        aid.MSN_E__c = assetName;
                    }
                    assetsInDeal.add(aid);
                }
            }
            insert assetsInDeal;
            
            // update the map with new asset in deal
            mapAssetsMSNAid = updateMapValues(assetsInDeal,mapAssetsMSNAid);
            
            //Set<String> existingEArecords = new Set<string>();
            Map<String,string> mapAiDEAId = new Map<String,String>();
            for (Pricing_Output_New__c ea: [select id, Asset_term__c from Pricing_Output_New__c 
                where Pricing_Run__c =: commercialTermId]){
                
                //existingEArecords.add(ea.Asset_term__c);
                mapAiDEAId.put(ea.Asset_term__c, ea.Id);
            }
            
            for (String assetId : selectedAssets){
                
                if (actionType=='clone' && mapAiDName.containskey(assetId)){
                    Pricing_Output_New__c eaRecord = new Pricing_Output_New__c();
                    eaRecord.Pricing_Run__c = commercialTermId;
                    eaRecord.Asset_Term__c = assetId;
                    
                    if(eaRecordType != null) { eaRecord.RecordTypeId = eaRecordType; }
                    economicAnalysis.add(eaRecord);
                }
                if (mapAssetsMSNAid.containskey(assetId)){
                    if (!mapAiDEAId.keyset().contains(mapAssetsMSNAid.get(assetId))){
                        Pricing_Output_New__c eaRecord = new Pricing_Output_New__c();
                        eaRecord.Pricing_Run__c = commercialTermId;
                        eaRecord.Asset_Term__c = mapAssetsMSNAid.get(assetId);
                        
                        if(eaRecordType != null) { eaRecord.RecordTypeId = eaRecordType; }
                        economicAnalysis.add(eaRecord);
                    } 
                }
            }
            if(!economicAnalysis.isEmpty()){
                insert economicAnalysis;
            }
            
            
            // remove EA records - which were removed during Edit - removed Asset Id/ MSN Id
            Set<String> itemsToDelete = new Set<String>();

            for (String key : mapAssetsMSNAid.keyset()){
                if(selectedAssets.contains(key)){
                    selectedAssets.remove(key);
                    selectedAssets.add(mapAssetsMSNAid.get(key));
                }
            }
            

            for (String s : mapAssetsMSNAid.values()){ //4 {a,b,c,d}
                if (!selectedAssets.contains(s) && mapAiDEAId.containsKey(s) && mapAiDEAId.get(s) != null){ // {a,b,d,e}
                   itemsToDelete.add(mapAiDEAId.get(s));
                }
            }
            
            
            // delete EA records
            if (itemsToDelete.size() > 0){
                delete [select id from Pricing_Output_New__c where Pricing_Run__c =: commercialTermId and 
                       Id in: itemsToDelete];
                
                // commented the logic to delete AID automatically
                /*for (Pricing_Output_New__c ea: [select id,Asset_Term__c from Pricing_Output_New__c where Pricing_Run__r.Marketing_Activity__c =: dealId AND 
                                                Pricing_Run__c !=: commercialTermId and 
                                                Asset_Term__c in: itemsToDelete]){
                    if (itemsToDelete.contains(ea.Asset_Term__c)){
                        itemsToDelete.remove(ea.Asset_Term__c);
                    }
                }
                // delete Aid records
                if (itemsToDelete.size() > 0){
                    //delete [select id from Aircraft_Proposal__c where id in: itemsToDelete];
                }*/
            }
        }
    }
    
    /**
    * @description Retreives details of the Page layout assigned to logged in user for CommercialTerm record 
    *   when CommercialTerm is inserted, edited or cloned
    *
    * @param recordId - String, contains Deal's Id for New CommercialTerm or selected CommercialTerm's Id for 
    *   Edit or Cloning of CommercialTerm
    *
    * @return plw - PageLayoutWrapper, PageLayoutWrapper record
    */
    @AuraEnabled 
    public static PageLayoutWrapper getPageLayoutFields(String recordId, String dealTypeField) {
        Pricing_Run_New__c dealAnalysis =  new Pricing_Run_New__c();
        
        //Finding RecordId prefix
        LOISectionWrapper loiWrapper  = getRecordIdPrefix(recordId);
        String prefix = loiWrapper.prefix;
        String commercialTermType = '';
        Boolean errorMsg = false;
        //Querying CommercialTerm if RecordId is of CommercialTerm
        if(recordId != null) {
            if(prefix == String.valueOf(recordId).left(3)) {
                Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
                Map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap();
                
                String query = 'Select Id';         
                for(String fieldName : fieldMap.keySet()) { 
                    if(fieldName != 'Id') {
                        query += ', ' + fieldName;
                    }            
                }
                query += ' from ' + leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c where Id = : recordId';
                       
                dealAnalysis = Database.query(query);
                
                recordId = dealAnalysis.Marketing_Activity__c;                
            }
            else {
                if(dealTypeField != null && dealTypeField != '') {
                    String dealQuery = 'Select Id, ' + dealTypeField + ' from ' + leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c where Id = :recordId';
                    Marketing_Activity__c deal = database.query(dealQuery);
                    commercialTermType = deal.get(dealTypeField) != null ? String.valueOf(deal.get(dealTypeField)) : '';
                }            
            }
        }
        
        //Finding Deal's RecordTypeId        
        Id idRecType = getDealAnalysisRecTypeId(recordId);   
        
        System.debug('idRecType:' + idRecType);
        String layoutname;
        
        //Finding PageLayout name for given RecordTypeId for current user
        if(!Test.isRunningTest()) {
            try {
            
                layoutname =  LeaseWareStreamUtils.getLayoutNameForCurrentUserProfile(idRecType); 
            
            if(layoutname != null) {
                String objectName = leasewareutils.getNamespacePrefix() +'Pricing_Run_New__c';
                String objectId = [Select Id, DurableId from EntityDefinition where QualifiedApiName = :objectName].DurableId;                
                String body =  LeaseWareStreamUtils.toolingAPISOQL('Select ManageableState from Layout where TableEnumOrId=\'' + objectId + '\' AND Layout.Name=\'' + layoutname + '\''); 
                String packageType = body.substringBetween('"ManageableState":"', '"');
                
                if(packageType == 'unmanaged') {
                    layoutname = objectName + '-' + layoutname; 
                }
                else {
                    layoutname = objectName + '-' + leasewareutils.getNamespacePrefix() + layoutname; 
                }  
            }

              }catch(System.CalloutException exp) {
                     //leasewareUtils.createExceptionLog(exp,exp.getMessage(), null, null, null, true);
                    throw new AuraHandledException('API error, please contact your LeaseWorks administrator [Exception: ' + exp.getMessage() + ']');
            }
                    
        }
        else {
            layoutname = leasewareutils.getNamespacePrefix() +'Pricing_Run_New__c-' + leasewareutils.getNamespacePrefix() + 'Economic Analysis Request - MA Layout'; 
        }
        
        system.debug('getPageLayoutFields Layoutname=' + layoutname);
        
        //You can give multiple page layout names here as well
        List<String> componentNameList = new List<String>{layoutname};
        List<LayoutSection> lstSections = new List<LayoutSection>();

        //Retrieve Page layout details 
        List<Metadata.Metadata> components = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, componentNameList);
        if(!components.isEmpty()){
            Metadata.Layout economicLayout = (Metadata.Layout) components.get(0);
        
            //We are going to find the fields names and will keep them according to columns so, 
            //we can show them as per page layout 
            for(Metadata.LayoutSection ls : economicLayout.layoutSections) {           
                LayoutSection section = new LayoutSection(ls.layoutColumns.size());
                section.label = ls.label;
                System.debug('Name****** '+section.label);
                
                List<LayoutColumn> lstColumns = new List<LayoutColumn>();
                Integer maxFieldsInColumn = 0;
                
                for(Metadata.LayoutColumn lc : ls.layoutColumns) {                
                    LayoutColumn column = new LayoutColumn();
                    
                    //check if there are fields available in that column
                    if(lc.layoutItems != null) { 
                        //Get the max number of fields in a column to preserve the alignment 
                        if(maxFieldsInColumn < lc.layoutItems.size()) {
                            maxFieldsInColumn = lc.layoutItems.size();
                        }
                        
                        for(Metadata.LayoutItem li : lc.layoutItems) {
                            //if(li.behavior != Metadata.UiBehavior.ReadOnly ) {
                                //Pass the LayoutItem object in the LayoutField consturctor     
                                column.lstFields.add( new LayoutField( li ) );
                            //}  
                        }
                    }
                    
                    //No need to add a column in the section if there is no field available 
                    if(column.lstFields.size() > 0) {
                        lstColumns.add( column );
                    }
                }
                
                //Now, we need to arrange the fields in section so we can use them in the iteration 
                //on the component so we will have to arrange them in the order 
                if(maxFieldsInColumn > 0) {
                    for(Integer i = 0; i < maxFieldsInColumn; i++) {
                        for(Integer j = 0; j < lstColumns.size(); j++) {
                            if(lstColumns[j].lstFields.size() > i) {
                                section.lstFields.add(lstColumns[j].lstFields[i]);    
                            }    
                            else {
                                section.lstFields.add(new LayoutField());
                            }
                        }    
                    }    
                }
                
                Leasewareutils.insertExceptionLogs();
                System.debug('lstSections');
                lstSections.add(section);
            }
        }
        else{
            errorMsg = true;
        }
        
        PageLayoutWrapper plw = new PageLayoutWrapper(lstSections, dealAnalysis, idRecType, commercialTermType, errorMsg);
        return plw;
    }
    
    @AuraEnabled
    public static List<Aircraft_Proposal__c> getAssetsInDeal(Id recId){
        List<Aircraft_Proposal__c> response = new List<Aircraft_Proposal__c>();

        for(Pricing_Output_New__c eaRecord : [SELECT Id, Asset_Term__c, Asset_Term__r.Name FROM Pricing_Output_New__c 
                                              WHERE Pricing_Run__c =: recId AND Asset_Term__c != null]) {
            if(eaRecord.Asset_Term__c != null){
                response.add(eaRecord.Asset_Term__r);
            }
            
        }
        return response;
    }
    
    
    /**
    * @description find given recordId object prefix to check either record is of Deal or CommercialTerm
    *
    * @param recordId - Id, contains Deal's Id or selected CommercialTerm's Id
    *
    * @return loiWrapper - LOISectionWrapper, LOISectionWrapper record
    */
    public static LOISectionWrapper getRecordIdPrefix(Id recordId) {
        LOISectionWrapper loiWrapper = new LOISectionWrapper();
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        String keyPrefix = objectDescribe.getKeyPrefix();
        
        System.debug('keyPrefix:' + leasewareutils.getNamespacePrefix() + '--' + recordId);
        if(keyPrefix == String.valueOf(recordId).left(3)) { 
            Pricing_Run_New__c dealAnalysis = [Select Id, Name 
                                               from Pricing_Run_New__c 
                                               where Id = :recordId];
            loiWrapper.dealAnalysisName = dealAnalysis.Name;
        }
        else {
            loiWrapper.dealAnalysisName = '';
        }
        
        loiWrapper.prefix = keyPrefix;
        
        return loiwrapper;
    }
    
    
    /**
    * @description find CommercialTerm's RecordTypeId with respect to given Deal's RecordTypeId
    *
    * @param MAID - String, contains selected Deal's Id
    *
    * @return loiWrapper - LOISectionWrapper, LOISectionWrapper record
    */
    public static id getDealAnalysisRecTypeId(String recordId) {
        Id idRecType;
        
        String recordTypeName = [Select Id, RecordType.Name from Marketing_Activity__c where Id = :recordId].RecordType.Name;
        
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
         
        for(Schema.RecordTypeInfo rti : objectDescribe.getRecordTypeInfos()) {
            if(rti.getName() == recordTypeName) {
                idRecType = rti.getRecordTypeId();
                
                break;
            }
        }
        system.debug('IdRecType=' + idRecType);
        
        return idRecType;
    }    
    
    
    /**
    * @description LOISectionWrapper to hold details related to CommercialTerm Name and given RecordId's object prefix
    *
    */
    public class LOISectionWrapper {
        @AuraEnabled public String dealAnalysisName {get; set;}
        @AuraEnabled public String prefix {get; set;}
    }
    
    
    /**
    * @description PageLayoutWrapper to hold details of selected CommercialTerm PageLayout and 
    *   CommercialTerm Record if CommercialTerm is getting cloned
    *
    */
    public class PageLayoutWrapper {
        @AuraEnabled public List<LayoutSection> lstSections {get; set;}
        @AuraEnabled public sObject selectedRecord {get; set;}
        @AuraEnabled public String recordTypeId {get; set;}
        @AuraEnabled public String commercialTermType {get; set;}
        @AuraEnabled public Boolean errorMsg {get;set;}
        
        public PageLayoutWrapper(List<LayoutSection> lstSections, sObject selectedRecord, String recordTypeId, String commercialTermType, Boolean errorMsg) {
            this.lstSections = lstSections;
            this.selectedRecord = selectedRecord;
            this.recordTypeId = recordTypeId;
            this.commercialTermType = commercialTermType;
            this.errorMsg = errorMsg;
        }
    }
    
    
    /**
    * @description LayoutSection to hold details related to selected CommercialTerm PageLayout Sections and 
    *   associated fields with each sections
    *
    */
    public class LayoutSection {
        @AuraEnabled public String label;
        @AuraEnabled public List<LayoutField> lstFields;       
        @AuraEnabled public Integer totalColumns;
        
        public LayoutSection(Integer totalColumns) {
            this.totalColumns = totalColumns;
            this.lstFields = new List<LayoutField>();            
        }
    }
    
    
    /**
    * @description LayoutColumn to hold details related to fields of each Section of CommercialTerm Page Layout
    *
    */
    private class LayoutColumn {
        private List<LayoutField> lstFields;  
        
        public LayoutColumn() {
            this.lstFields = new List<LayoutField>();
        }
    }
    
    
    /**
    * @description LayoutField to hold details related to each field on CommercialTerm Page Layout
    *
    */
    public class LayoutField {
        @AuraEnabled public String fieldName;
        @AuraEnabled public Boolean isRequired;
        @AuraEnabled public Boolean isReadOnly;
        @AuraEnabled public String fieldValue;
        
        public LayoutField() {}
        
        public LayoutField(Metadata.LayoutItem li) {
            System.debug('this.fieldName>>' + li.field + '----'+ Leasewareutils.getNamespacePrefix());
            this.fieldName = li.field;
                       
            if(li.behavior == Metadata.UiBehavior.Required) {
                this.isRequired = true;
            }
            else if(li.behavior == Metadata.UiBehavior.ReadOnly ) {
                this.isReadOnly = true;
            }   
        }
    }   
    
    
    /**
    * @description CommercialTermWrapper to hold CommercialTerm and Assets details 
    *
    */
    public class CommercialTermWrapper {
        @AuraEnabled public List<ColumnWrapper> headers {get; set;}
        @AuraEnabled public List<ChildWrapper> cwList {get; set;}
        @AuraEnabled public String objectLabel {get; set;}
        
        public CommercialTermWrapper(List<ColumnWrapper> headers, List<ChildWrapper> cwList, String objectLabel) {
            this.headers = headers;
            this.cwList = cwList;
            this.objectLabel = objectLabel;
        }
    }
    
    
    /**
    * @description ColumnWrapper to hold details related to headers of the CommercialTerm Data
    *
    */
    public class ColumnWrapper {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public String sortIcon {get; set;}
        
        public ColumnWrapper(String label, String fieldName, String type) {
            this.label = label;
            this.fieldName = fieldName;
            this.type = type.toLowerCase();
            this.sortIcon = 'arrowdown';
        }
    }
    
    
    /**
    * @description ChildWrapper to hold details related to CommercialTerm, its EconomicAnalysis and Assets object
    *
    */
    public class ChildWrapper {
        @AuraEnabled public sObject objRecord {get; set;}
        @AuraEnabled public List<Aircraft__c> mslList {get; set;}
        @AuraEnabled public List<Pricing_Output_New__c> eaList {get; set;}
        @AuraEnabled public List<Aircraft_Proposal__c> msnList {get; set;}
        
        public ChildWrapper(sObject objRecord, List<Aircraft__c> mslList, List<Pricing_Output_New__c> eaList, 
                            List<Aircraft_Proposal__c> msnList) {
            this.objRecord = objRecord;
            this.mslList = mslList;
            this.eaList = eaList;
            this.msnList = msnList;
        }
    }
}