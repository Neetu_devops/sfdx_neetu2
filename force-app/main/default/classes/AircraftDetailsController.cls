public without sharing class AircraftDetailsController {

    @AuraEnabled
    public static ContainerWrapper getData(String assetId){
        
        //decode parameter.
        if(!Test.isRunningTest()){
            assetId = UtilityWithoutSharingController.decodeData(assetId);
        }
        String communityPrefix = UtilityWithoutSharingController.getCommunityPrefix();
        ContainerWrapper container = new ContainerWrapper();
        Set<String> assemblyTypes = new Set<String>{'Engine', 'Airframe'};
        
        List<Aircraft__c> assetList =  [select id, Name, Vintage__c, Engine_Type__c, 
                                                    Registration_Number__c, Aircraft_Series__c,
                                                    Asset_Status_External__c, Aircraft_Variant__c, MSN_Number__c,
                                                    Lease__r.Lease_Start_Date_New__c,
                                                    Lease__r.Lease_End_Date_New__c, CSN__c,
                                                    Lease__r.Operator__r.Name, TSN__c,
                                                    Lease__r.Lessee__r.Name, Type_Variant__c,
                                                    (select id, Type__c, Serial_Number__c,
                                                      Attached_Aircraft__c,
                                                      Asset__c from Assemblies__r 
                                                      where RecordType.DeveloperName in : assemblyTypes
                                                      and Replaced_External__c = false
                                                      order by  Type__c)
                                                    from Aircraft__c where id= : assetId]; 

        if(assetList.size()>0){
               
            container.msn = assetList[0].MSN_Number__c;
            container.engineSummary = new EngineSummaryWrapper(assetList[0].Type_Variant__c, assetList[0].Vintage__c,
                                                               assetList[0].Engine_Type__c, assetList[0].Registration_Number__c,
                                                               assetList[0].Asset_Status_External__c, 
                                                               assetList[0].Lease__r.Lessee__r.Name,
                                                               assetList[0].Lease__r.Operator__r.Name,
                                                               assetList[0].Lease__r.Lease_Start_Date_New__c,
                                                               assetList[0].Lease__r.Lease_End_Date_New__c,
                                                               assetList[0].TSN__c, assetList[0].CSN__c);
            
        	//get linked Assets (Engines) and Airfrmae.
            for(Constituent_Assembly__c assembly : assetList[0].Assemblies__r){
                
                if(assembly.Type__c == 'Airframe'){
                    container.assemblyId = assembly.Id;
                }else{
                    
                	container.linkedAssets.add(new LinkedAssetWrapper(assembly.Type__c, assembly.Serial_Number__c,
                                                                assembly.Attached_Aircraft__c == null ? '' : communityPrefix + '/s/engine-details?recordId='+ UtilityWithoutSharingController.encodeData(assembly.Attached_Aircraft__c))); 
            
                }
            }
            System.debug(container.linkedAssets);
            Set<String> serialNo = new Set<String>();
            if(container.linkedAssets.size() > 0){
                for(LinkedAssetWrapper law : container.linkedAssets){
                    serialNo.add(law.value);
                }
                for(Lease_Fund_Events_External__c fund : [Select Id, event_Code_Description__c,eventSerialNumber__c, Maintenence_Reserve_Balance__c from Lease_Fund_Events_External__c where eventMsnEsn__c =: assetList[0].MSN_Number__c]){
                    if(!serialNo.contains(fund.eventSerialNumber__c)){
                        container.fundList.add(fund);
                    }
                }
            } 
            else{
                container.fundList = [Select Id, event_Code_Description__c,eventSerialNumber__c, Maintenence_Reserve_Balance__c from Lease_Fund_Events_External__c where eventMsnEsn__c =: assetList[0].MSN_Number__c];
            }
        }
        
        loadShopVisitList(assetId, container);
        return container;
    }
    
    private static void loadShopVisitList(String assetId, ContainerWrapper container){
        
         Set<String> recordTypes = new Set<String>{'Airframe', 'APU', 'Landing_Gear'};
             
         for(Assembly_Eligible_Event__c  hisEvent : [select id, Name, event_Type_external__c, End_Date__c, 
                                                                       	TSN__c, CSN__c,
                                                                        Name_of_MRO__r.Name, 
                                                                       	Next_Scheduled_Event_Due_Date__c,
                                                                       	Constituent_Assembly__r.Type__c,
                                                                        Constituent_Assembly__r.TSN__c,
                                                                        Constituent_Assembly__r.CSN__c,
                                                                        Constituent_Assembly__r.TSO_External__c,
                                                                        Constituent_Assembly__r.CSO_External__c,
                                                                        Constituent_Assembly__r.Serial_Number__c,
                                                                        Constituent_Assembly__r.Part_Number__c,
                                                                        Constituent_Assembly__r.RecordType.developerName
                                                                       	from Assembly_Eligible_Event__c
                                                                      	where Constituent_Assembly__r.Attached_Aircraft__c =: assetId
                                                                      	and Constituent_Assembly__r.RecordType.developerName in : recordTypes
                                                                      	order by /*Constituent_Assembly__r.RecordType.developerName,*/ End_Date__c desc]){
                                                                         
         	if(hisEvent.Constituent_Assembly__r.RecordType.developerName == 'Airframe'){
                
            	container.shopVistList_Airframe.add(new AirframeShopVisitWrapper(hisEvent.event_Type_external__c, hisEvent.End_Date__c,
                                                                               hisEvent.Name_of_MRO__r.Name,
                                                                               hisEvent.TSN__c,
                                                                               hisEvent.TSN__c,
                                                                               hisEvent.Next_Scheduled_Event_Due_Date__c));                                                                    
           
            }else {
                
                ShopVisitWrapper shopVisit = new ShopVisitWrapper(hisEvent.Constituent_Assembly__r.Type__c,
                                                                    hisEvent.Constituent_Assembly__r.Serial_Number__c, 
                                                                    hisEvent.Constituent_Assembly__r.TSN__c, 
                                                                    hisEvent.Constituent_Assembly__r.CSN__c, 
                                                                    hisEvent.Constituent_Assembly__r.TSO_External__c, 
                                                                    hisEvent.Constituent_Assembly__r.CSO_External__c,
                                         							hisEvent.End_Date__c, hisEvent.Name_of_MRO__r.Name);
            	
                if(hisEvent.Constituent_Assembly__r.RecordType.developerName == 'APU'){
                    shopVisit.tsnOH = hisEvent.TSN__c;
                    shopVisit.csnOH = hisEvent.CSN__c;
                    container.shopVistList_APU.add(shopVisit);
                }else{
                    shopVisit.lastOHHours = hisEvent.TSN__c;
                    shopVisit.lastOHCycles = hisEvent.CSN__c;
                    shopVisit.partNo = hisEvent.Constituent_Assembly__r.Part_Number__c;
                    container.shopVistList_LG.add(shopVisit);
                }
            }
                                                                                 
         }     
    }

    public class ContainerWrapper{
        @AuraEnabled public String msn;
        @AuraEnabled public String assemblyId; //for utilization table.
        @AuraEnabled public EngineSummaryWrapper engineSummary;
        @AuraEnabled public List<LinkedAssetWrapper> linkedAssets;
        @AuraEnabled public List<AirframeShopVisitWrapper> shopVistList_Airframe;
        @AuraEnabled public List<ShopVisitWrapper> shopVistList_APU; 
        @AuraEnabled public List<ShopVisitWrapper> shopVistList_LG;
        @AuraEnabled public List<Lease_Fund_Events_External__c> fundList;
        
        public ContainerWrapper(){
            linkedAssets = new List<LinkedAssetWrapper>();
            shopVistList_Airframe = new List<AirframeShopVisitWrapper>(); 
            shopVistList_APU = new List<ShopVisitWrapper>();
            shopVistList_LG = new List<ShopVisitWrapper>();
            fundList = new List<Lease_Fund_Events_External__c>();
        }
    }
    
    
    public class EngineSummaryWrapper{
        
        @AuraEnabled public String aircraftType;
        @AuraEnabled public String vintage;
        @AuraEnabled public String aircraftEngineType;
        @AuraEnabled public String registration;
        @AuraEnabled public String assetStatus;
        @AuraEnabled public String lessee;
        @AuraEnabled public String operator;
        @AuraEnabled public Date leaseStart;
        @AuraEnabled public Date leaseEnd;
        @AuraEnabled public Decimal airframeTSN;
        @AuraEnabled public Decimal airframeCSN;
        
        public EngineSummaryWrapper(String aircraftType, String vintage, String aircraftEngineType, String registration,
                                   String assetStatus, String lessee, String operator, Date leaseStart, Date leaseEnd,
                                   Decimal airframeTSN, Decimal airframeCSN){
        
        	this.aircraftType = aircraftType;
            this.vintage = (vintage != null ? vintage.left(4) : null);
            this.aircraftEngineType = aircraftEngineType;
            this.registration = registration;
            this.assetStatus = assetStatus;    
            this.lessee = lessee;    
            this.Operator = Operator;      
            this.leaseStart = leaseStart;      
            this.leaseEnd =  (leaseEnd == Date.newInstance(2200, 01, 01) ? null : leaseEnd);
            this.airframeTSN = airframeTSN;   
            this.airframeCSN = airframeCSN;                           
    
        }
    }
    
    public class LinkedAssetWrapper{
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public String url;
        
        public LinkedAssetWrapper(String label, String value, String url){
            this.label = label;
            this.value = value;
            this.url = url;
        }
    }
    
    public class AirframeShopVisitWrapper{
        
        @AuraEnabled public String shopType;
        @AuraEnabled public Date releaseDate;
        @AuraEnabled public String mro;
        @AuraEnabled public Decimal tsn;
        @AuraEnabled public Decimal csn;
        @AuraEnabled public Date nextCheckDate;

        public AirframeShopVisitWrapper(String shopType, Date releaseDate, String  mro, Decimal tsn, Decimal csn,
                                       Date nextCheckDate){
            this.shopType = shopType;
            this.releaseDate = releaseDate;
            this.mro = mro;
            this.tsn = tsn;
            this.csn = csn;
            this.nextCheckDate = nextCheckDate;                               
                                                                          
        }
    }
    
     //for APU and LG
     public class ShopVisitWrapper{
         
		@AuraEnabled public String description;
     	@AuraEnabled public String partNo;
        @AuraEnabled public String serialNo;
        @AuraEnabled public Decimal tsn;
        @AuraEnabled public Decimal csn;
        @AuraEnabled public Decimal tso;	
        @AuraEnabled public Decimal cso;	
        @AuraEnabled public Date lastOHDate;
        @AuraEnabled public Decimal tsnOH; 
        @AuraEnabled public Decimal csnOH; 
        @AuraEnabled public Decimal lastOHHours;	
        @AuraEnabled public Decimal lastOHCycles;	
        @AuraEnabled public String mro;
         
         public ShopVisitWrapper(String description, String serialNo, Decimal tsn, Decimal csn, Decimal tso, Decimal cso,
                                        Date lastOHDate, String mro){
         
         	this.description = description;
            this.serialNo = serialNo;
            this.tsn = tsn;
            this.csn = csn;
            this.tso = tso;  
            this.cso = cso;  
            this.lastOHDate = lastOHDate;
            this.mro = mro;                                
                                            
         }
     }    
}