/**
* This class contains unit tests for validating the behavior of EmailMessage trigger
*/
@isTest
private class TestEmailMessageTrigger {
    
    static testMethod void testEmailToIL() {
        
        Operator__c newOp;
        Counterparty__c newLessor;
        Bank__c newCP;

        LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;
        
        try{
            newOp = new Operator__c(Name='Operator For Contact Testing 1', Alias__c='Alias 1 for Op 1, Alias 2 for Op 1');
            insert newOp;
        }catch(exception e){
            try{
                system.debug('Exception ' + e);
                newOp = [select id from Operator__c where Name = 'Operator For Contact Testing 1' limit 1];
            }catch(exception e2){
                system.debug('Operator creation failed. Exception ' + e2);
            }
        }
        
        try{
            newLessor = new Counterparty__c(name='Lessor For Contact Testing 1');
            insert newLessor;
        }catch(exception e){
            try{
                system.debug('Exception ' + e);
                newLessor = [select id from Counterparty__c where Name = 'Lessor For Contact Testing 1' limit 1];
            }catch(exception e2){
                system.debug('Lessor creation failed. Exception ' + e2);
            }
        }
        
        LeaseWareUtils.clearFromTrigger();
        try{
            newCP = new Bank__c(name='Counterparty For Contact Testing 1');
            insert newCP;
        }catch(exception e){
            try{
                system.debug('Exception ' + e);
                newCP = [select id from Bank__c where Name = 'Counterparty For Contact Testing 1' limit 1];
            }catch(exception e2){
                system.debug('Counterparty creation failed. Exception ' + e2);
            }
        }
        
        system.debug('Operator, Lessor and CP Ids ' + newOP.id + ', ' + newLessor.Id + ', ' + newCP.id);
        
        LeaseWareUtils.clearFromTrigger();
        
        EmailMessage em = new EmailMessage(Subject = 'Test', RelatedToId = newOp.id, TextBody = 'Text Test body', HtmlBody ='Html Test body');
        insert em;
        delete em;
        
    }
    
       
    @isTest
    private static void checkILDeleteOnDel(){
        Operator__c opr = new Operator__c(Name='Test OPerator');
        insert opr;
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
        LeaseWareUtils.clearFromTrigger(); 
        insert aircraft2;
        LW_Setup__c lwSetup;
        lwSetup = new LW_Setup__c();
        lwSetup.Name = 'CREATE IL FOR NEW EMAILS';
        lwSetup.Disable__c = false;
        lwSetup.Value__c = 'Active';
        insert lwSetup;
        LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;
        
        EmailMessage em = new EmailMessage(Subject='Test Subject',RelatedToId = opr.id);
        insert em;
        
        List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c where name= :em.Subject];
        system.assertEquals(1, il.size());
        
        
        LeasewareUtils.clearFromTrigger();
        delete em;
        List<Customer_Interaction_Log__c> il1 = [select Id,Name from Customer_Interaction_Log__c where name= :em.Subject];
        system.assertEquals(0, il1.size());
        
        
    }
    
    @isTest
    private static void checkILDeleteOnDeleteTask(){
        Operator__c opr = new Operator__c(Name='Test OPerator');
        insert opr;
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
       
        LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;

        EmailMessage em = new EmailMessage(Subject='Test Subject',RelatedToId = opr.id);
        insert em;
        
        List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c where name= :em.Subject];
        system.assertEquals(1, il.size());
        
        Task task = [select Id,WhatID from task];
        system.debug('task----'+task);
        LeasewareUtils.clearFromTrigger();
        delete task;
        List<Customer_Interaction_Log__c> il1 = [select Id,Name from Customer_Interaction_Log__c where name= :em.Subject];
        system.assertEquals(0, il1.size());
        
        
    }
    
     //Test Case Scenario :AUTO IL FOR NEW EMAILS = 'ALWAYS'
    //Related To ;Blank
    @IsTest
    private static void testAutoILNewEmailALWAYS(){
        Operator__c opr = new Operator__c(Name='Test OPerator');
        insert opr;
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
       

        LeaseWareUtils.clearFromTrigger(); 
        LW_Setup__c lwSetup = new LW_Setup__c();
        
        lwSetup.Name = 'AUTO IL FOR NEW EMAILS';
        lwSetup.Disable__c = false;
        lwSetup.Value__c = 'ALWAYS INCLUDING LW ADMINS';
        insert lwSetup;
		LeaseWareUtils.clearFromTrigger(); 
         LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;
        LeaseWareUtils.clearFromTrigger(); 
        LeaseWorksSettings__c lwSettings = LeaseWorksSettings__c.getInstance();
        if(lwSettings==null) lwSettings = new LeaseWorksSettings__c(SetupOwnerId=System.UserInfo.getUserId());//anjani: this will never get called, as above line will return.
        lwSettings.Disable_All_Triggers__c=true;
        //upsert lwSettings;
        
        EmailMessage em = new EmailMessage(Subject='Test Subject',RelatedToId = opr.id);
        insert em;
        lwSettings.Disable_All_Triggers__c=false;
        upsert lwSettings;
        //IL is now created because we have removed the Validation Rule OperatorOrLessorRequired
        List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c ];
        system.assertEquals(1, il.size());
        
    }
    
    //Test Case Scenario :AUTO IL FOR NEW EMAILS = 'ALWAYS'
    //Related To ;Not Blank
    @IsTest
    private static void testAutoILNewEmailALWAYSWithRelatedTo(){
        Operator__c opr = new Operator__c(Name='Test OPerator');
        insert opr;
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
       

        LeaseWareUtils.clearFromTrigger(); 
        LW_Setup__c lwSetup = new LW_Setup__c();
        
        lwSetup.Name = 'AUTO IL FOR NEW EMAILS';
        lwSetup.Disable__c = false;
        lwSetup.Value__c = 'ALWAYS INCLUDING LW ADMINS';
        insert lwSetup;
        
        LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;
        
        EmailMessage em = new EmailMessage(Subject='Test Subject',RelatedToId = opr.id);
        insert em;
        
        List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c ];
        system.assertEquals(1, il.size());
        
    }
    //Test Case Scenario :AUTO IL FOR NEW EMAILS = 'ALWAYS'
    //Related To ; Blank
    //Validation Rule : InActive
    @IsTest
    private static void testAutoILNewEmailALWAYSWithValidationActive(){
        Operator__c opr = new Operator__c(Name='Test OPerator');
        insert opr;
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
       

        LeaseWareUtils.clearFromTrigger(); 
        LW_Setup__c lwSetup = new LW_Setup__c();
        
        lwSetup.Name = 'AUTO IL FOR NEW EMAILS';
        lwSetup.Disable__c = false;
        lwSetup.Value__c = 'ALWAYS INCLUDING LW ADMINS';
        insert lwSetup;

        LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;

		String unexpectedMsg;
        try{
        EmailMessage em = new EmailMessage(Subject='Test Subject');
        insert em;
        }catch(Exception e){
            system.debug('Exception occured.......');
            unexpectedMsg = e.getMessage();
        }
        system.debug('unexpectedMsg-------'+ unexpectedMsg);
        List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c ];
        system.assertEquals(1, il.size());
        
    }
    
    //Test Case Scenario  AUTO IL FOR NEW EMAILS = 'OFF'
    //Related To ;Not Blank
    @IsTest
    private static void testAutoILEmailWithOFF(){
        Bank__c cp = new Bank__c(Name='Test CP');
        insert cp;
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
       
        LeaseWareUtils.clearFromTrigger(); 
        LW_Setup__c lwSetup = new LW_Setup__c();
        lwSetup.Name = 'AUTO IL FOR NEW EMAILS';
        lwSetup.Disable__c = false;
        lwSetup.Value__c = 'OFF';
        insert lwSetup;

        
        EmailMessage em = new EmailMessage(Subject='Test Subject',RelatedToId = cp.id);
        insert em;
        
        List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c ];
        system.assertEquals(0, il.size());
        
    }

    //Test Case Scenario : AUTO IL FOR NEW EMAILS = 'ALWAYS'
    //Related To ;Not Blank
    @IsTest
    private static void testAutoILNewEmailONLYWITHRELATEDTO(){
        Operator__c opr = new Operator__c(Name='Test OPerator');
        insert opr;
        
        Aircraft__c aircraft2 = new Aircraft__c(
            Name= ' Test Asset 2',
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = 'Test Asset 2'
        );
        LeaseWareUtils.clearFromTrigger(); 
       
        LW_Setup__c lwSetup = new LW_Setup__c();
        lwSetup.Name = 'AUTO IL FOR NEW EMAILS';
        lwSetup.Disable__c = false;
        lwSetup.Value__c = 'ONLY WITH RELATED TO';
        insert lwSetup;

        LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;
        EmailMessage em = new EmailMessage(Subject='Test Subject',RelatedToId = opr.id);
        insert em;
        
        List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c ];
        system.assertEquals(1, il.size());
        
    }

    //Test Case Scenario : Outgoing Email and AUTO IL FOR NEW EMAILS = 'ALWAYS'/'ALWAYS INCLUDING LW ADMINS' 
    @IsTest
    private static void testAutoILNewOutgoingEmail(){
        // Create Setting
        LW_Setup__c lwSetup = new LW_Setup__c();
        lwSetup.Name = 'AUTO IL FOR NEW EMAILS';
        lwSetup.Disable__c = false;
        lwSetup.Value__c = 'ALWAYS';
        insert lwSetup;
        LW_Setup__c lwSetup1 = new LW_Setup__c();
        lwSetup1.Name = 'Email Threading In Integration';
        lwSetup1.Disable__c = false;
        lwSetup1.Value__c = 'OFF';
        insert lwSetup1;
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@sampleorg.com');
        insert user1;
        
        System.runAs ( thisUser ) {
         //Create Group   
            Group grp = new Group();
            grp.Name = 'LW_Admin_Group';
            grp.Type = 'Regular';
            insert grp;
            LeaseWareUtils.clearFromTrigger(); 
       //Create Group Member
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = thisUser.id;
            grpMem1.GroupId = grp.Id;
            Insert grpMem1;
            LeaseWareUtils.clearFromTrigger(); 
         }
        
         String retString='Testing';
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         mail.setToAddresses( new String[] { UserInfo.getUserEmail() } );
         mail.setSubject('Test creation of IL for outgoing email'+ ' : Dated '+ system.today().format() );
         mail.setHtmlBody(retString ); 
        
        // When setting is Always and user doesnot belong to LW_Admin group
        system.runAs(user1){
         Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } ); 
        }
       
         List<Customer_Interaction_Log__c> il = [select Id,Name from Customer_Interaction_Log__c ];
         system.assertEquals(1, il.size(),'IL was not created for Outgoing mail sent by non LW_Admin user');
         system.debug('IL Size'+il.size());
        
         // When setting is Always and user belongs to LW_Admin group
         Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } ); 
         List<Customer_Interaction_Log__c> il_new = [select Id,Name from Customer_Interaction_Log__c where CreatedById=:thisuser.id ];
         system.assertEquals(0, il_new.size(),'IL was created for Outgoing mail sent by LW_Admin user');
         system.debug('New IL Size'+il_new.size());
        
         lwSetup.Value__c = 'ALWAYS INCLUDING LW ADMINS';
         update lwSetup;
        
        // When setting is ALWAYS INCLUDING LW ADMINS and user belongs to LW_Admin group
         Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } ); 
         List<Customer_Interaction_Log__c> il_new1 = [select Id,Name from Customer_Interaction_Log__c where CreatedById=:thisuser.id ];
         system.assertEquals(1, il_new1.size(),'IL was not created for Outgoing mail sent by LW_Admin user');
         system.debug('New IL Size'+il_new1.size());
    }

}