@isTest
private class TestProspectExplorerController {
    @testSetup
    static void testData(){
        
        Country__c country =new Country__c(Name='United States');
        insert country;

        Operator__c opr = new Operator__c();
        opr.Name = 'testOpr';
      //  opr.Country__c = 'United States';
        opr.Country_Lookup__c = country.id;
        opr.Region__c = 'Asia';
        insert opr;
        System.assert(opr.Id != null);
        
        Operator__c opr1 = new Operator__c();
        opr1.Name = 'testOpr1';
    //  opr1.Country__c = 'United States';
        opr1.Country_Lookup__c = country.id;
        opr1.Region__c = 'Asia';
        insert opr1;
        System.assert(opr1.Id != null);
        
        Global_Fleet__c gf = new Global_Fleet__c();
        gf.Name = 'testWorldFleet';
        gf.Aircraft_External_ID__c = 'type-msn';
        gf.MTOW__c = 134453;
        gf.BuildYear__c = '1996';
        gf.SeatTotal__c = 12;
        gf.AircraftType__c = 'ATtest';
        gf.EngineType__c = 'ETtest';
        gf.AircraftSeries__c = 'AStest';
        gf.EngineVariant__c = 'EVtest';
        gf.OriginalOperator__c ='testOpr';
        gf.Aircraft_Operator__c = opr.Id;
        
        insert gf;
        System.assert(gf.Id != null);
        
        Global_Fleet__c gf1 = new Global_Fleet__c();
        gf1.Name = 'testWorldFleet1';
        gf1.Aircraft_External_ID__c = 'type1-msn1';
        gf1.MTOW__c = 134498;
        gf1.BuildYear__c = '2018';
        gf1.SeatTotal__c = 120;
        gf1.AircraftType__c = '727';
        gf1.SerialNumber__c ='101';
        gf1.EngineType__c = 'ETtest1';
        gf1.AircraftSeries__c = 'AStest1';
        gf1.OriginalOperator__c ='testOpr';
        gf1.EngineVariant__c = 'EVtest1';
        gf1.Aircraft_Operator__c = opr.Id;
        insert gf1;
        System.assert(gf1.Id != null);
        
        Aircraft__c ac = new Aircraft__c();
        ac.Name = 'testAircraft';
        ac.Aircraft_Type__c = '727';
        ac.Engine_Type__c = 'JT8D';
        ac.Aircraft_Variant__c = '100';
        ac.MSN_Number__c = '101';
        ac.Date_of_Manufacture__c = system.today();
        ac.CSN__c = 831;
        ac.TSN__c = 2.3;
        insert ac;
        
        Deal__c deal = new Deal__c();
        deal.Name = 'testDeal';
        insert deal;
        System.assert(deal.Id != null);
        
        Aircraft_In_Deal__c AID = new Aircraft_In_Deal__c();
        AID.Name = 'testACDeal';
        AID.Marketing_Deal__c = deal.Id;
        AID.Aircraft__c = ac.Id;
        insert AID;
        System.assert(AID.Id != null);
        
        Aircraft_Type__c at = new Aircraft_Type__c();
        at.Name = 'testAc';
        //insert at;
        //System.assert(at.Id != null);
        
        
        Deal_Proposal__c dp = new Deal_Proposal__c();
        dp.Name = 'testDealProp';
        dp.Deal__c = deal.Id;
        dp.Prospect__c = opr.Id;
        insert dp;
        System.assert(dp.Id != null);
        
        WF_Field_Summary__c fleet = new WF_Field_Summary__c();
        fleet.Build_Year__c = '2020';
        fleet.MTOW__c = '1200, 12002, 21312';
        fleet.Seat__c = '9,67';
        fleet.Aircraft_Type__c='ATTest,727';
        fleet.Exclude_Aircraft_Type__c='727';
        insert fleet;
    }
    
    @isTest 
    static void loadDefaultDataTest() {
        Id marketCampId = [Select Id from Deal__c limit 1].Id;
        System.assert(marketCampId != null);
        
        ProspectExplorerController.loadDefaultData(marketCampId );
        
        ProspectExplorerController.searchProspects('ATtest','AStest','ETtest','12','120','1996','2018','134453','134498','EVtest',marketCampId,'United States','Asia');
        
        ProspectExplorerController.searchSistersships(marketCampId, '101', '2018', '727');
        
        ProspectExplorerController.addProspects('test',marketCampId);
        
        //@Bhavna Updated on 19th Sept, 2019 for OperatorFinder
        List<Operator__c> operatorList = [Select Id, Name from Operator__c];
        ProspectExplorerController.createDeals(operatorList);
    }
}