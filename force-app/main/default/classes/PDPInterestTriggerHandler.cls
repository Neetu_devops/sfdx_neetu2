public with sharing class PDPInterestTriggerHandler implements ITrigger {
	final Integer MAX = 9999999;
	public Map<Id,Map<Date,PDP_Interest__c>> previousInterests;
	public Map<Id,List<PDP_Interest__c>> unaccumulatedInterests;


	public PDPInterestTriggerHandler() {
		previousInterests = getPreviousInterest(Trigger.new);
		unaccumulatedInterests = getUnaccumulatedInterests(Trigger.new);
	}

	public void  bulkBefore() {
		setPDPPaid(Trigger.new);
    }

    public void bulkAfter() {

    } 

    public void beforeDelete() {

    }

    public void afterDelete() {

    }

    public void afterUnDelete() {

    }

    public void andFinally() {

    }

    public void beforeInsert() {
		setInitialDate(Trigger.new);
    	setNumberOfDays(Trigger.new);
		setPDPPaid(Trigger.new);
    	setDeferral(Trigger.new);
    	setCumulativeDeferral(Trigger.new);
    	setInterestOnPDP(Trigger.new);
	}

	public void beforeUpdate() {

        setNumberOfDays(Trigger.new);
        setDeferral(Trigger.new);
        setPDPPaid(Trigger.new);
        modifyNextInterests(Trigger.new);
	}

    public void afterInsert() {

    }
    public void afterUpdate() {


    }
    
    private void setInitialDate(List<PDP_interest__c> interestToInsert){
        
        PDP_interest__c pdp = interestToInsert[0];
        List<PDP_interest__c> pdpInterestList = [SELECT ID, Final_Paymant_Date__c FROM PDP_interest__c WHERE Delivery__c= :pdp.Delivery__c ORDER BY Final_Paymant_Date__c DESC ];
        if ( pdpInterestList.size() > 0 ) {
        	Date MaxDate = pdpInterestList[0].Final_Paymant_Date__c;
            date newDate = maxDate.addDays(1);
            pdp.Initial_Payment_Date__c = newDate;
        }
        if( pdp.Initial_Payment_Date__c > pdp.Final_Paymant_Date__c ){
                pdp.Final_Paymant_Date__c.addError('Invalid Date');
        }
        
    }

	private void modifyNextInterests(List<PDP_Interest__c> interests) {
		PDP_Interest__c modifiedInterest = interests[0];
		PDP_Interest__c prevInterest = (PDP_Interest__c) Trigger.oldMap.get(modifiedInterest.Id);
		List<PDP_Interest__c> relatedInterests = [SELECT Orig_PDP__c, PDP_Schedule__c,Cumulative_Deferral__c,PDP_Percentage__c,Accumulate__c,Accumulated__c, ID,Orig_PDP_Amount__c ,Number_of_Days__c,PDP_Paid__c ,Date__c,Deferral__c, Interest_on_PDP__c,Initial_Payment_Date__c,Final_Paymant_Date__c, Delivery__c,Rate__c FROM PDP_Interest__c	WHERE Delivery__c = :modifiedInterest.Delivery__c ORDER  by Final_Paymant_Date__c, Initial_Payment_Date__c ASC ];
		List<PDP_Interest__c> listToInsert = relatedInterests.deepClone(false,true,true);

		//Find modified record by old FinalPaymentDate.
		Integer index;
		for(PDP_Interest__c toInterest : listToInsert ) {
			if(toInterest.Final_Paymant_Date__c == prevInterest.Final_Paymant_Date__c) {
				 index = listToInsert.indexOf(toInterest);
			}
            toInterest.Accumulated__c = false;

		}
        Integer dbIndex;
        for(PDP_Interest__c DBinterest : relatedInterests ) {
            if(DBinterest.Id ==modifiedInterest.Id) {
                dbIndex = relatedInterests.indexOf(DBinterest);
            }
        }
        if(dbIndex != null ) {
            relatedInterests.remove(dbIndex);
        }
		List<PDP_Interest__c> itarateList = new List<PDP_Interest__c>();
		itarateList.addAll(listToInsert);
		if(index != null ) {
			modifiedInterest.Accumulated__c = false;
			itarateList.set(index,modifiedInterest);
			listToInsert.remove(index);
		}
		if( !validateModifiedDate(itarateList,modifiedInterest )) {


			for (PDP_Interest__c toInsert : itarateList) {

				if (toInsert.Accumulate__c && itarateList.indexOf(toInsert) < index) {
					accumulate(listToInsert.indexOf(toInsert), listToInsert, true);
				} else if (itarateList.indexOf(toInsert) == index) {
					toInsert.Cumulative_Deferral__c = toInsert.Deferral__c;
					if (index != 0) {
						toInsert.Cumulative_Deferral__c += calculateDeferral(itarateList.indexOf(toInsert), itarateList, toInsert.Accumulate__c);
					}
					individualnterestCalc(toInsert);

				} else if (itarateList.indexOf(toInsert) > index) {
					toInsert.Cumulative_Deferral__c = toInsert.Deferral__c;
					toInsert.Cumulative_Deferral__c += calculateDeferral(itarateList.indexOf(toInsert), itarateList, toInsert.Accumulate__c);
					individualnterestCalc(toInsert);
				}
			}
			
			LeaseWareUtils.TriggerDisabledFlag=true;
			delete relatedInterests;
			insert listToInsert;
			LeaseWareUtils.TriggerDisabledFlag=false;
		}


    }

	private Boolean validateModifiedDate(List<PDP_Interest__c> interests, PDP_Interest__c modifedInterest ) {
		Boolean withError = false;
		if(modifedInterest.Initial_Payment_Date__c > modifedInterest.Final_Paymant_Date__c ) {
			modifedInterest.addError('Initial Payment Date grater than FinalPayment Date');
			withError = true;
		}else{
			for(PDP_Interest__c i : interests ) {
				if((i.Initial_Payment_Date__c < modifedInterest.Initial_Payment_Date__c && i.Final_Paymant_Date__c> modifedInterest.Initial_Payment_Date__c) || (i.Initial_Payment_Date__c < modifedInterest.Final_Paymant_Date__c && i.Final_Paymant_Date__c> modifedInterest.Final_Paymant_Date__c)  ) {
					modifedInterest.addError('The modified Dates overlap with the following Dates: '+i.Initial_Payment_Date__c+' - '+ i.Final_Paymant_Date__c);
					withError = true;
				}
			}

		}
		return withError;
	}

	private Decimal calculateDeferral(Integer index, List<PDP_Interest__c> listToInsert, Boolean accumulate) {
		Set<Integer> accumulatedIndexs = accumulate(index, listToInsert,false);
		Decimal sum = 0;
		for (Integer i = index - 1; i >= index - accumulatedIndexs.size(); i--) {
                if (i == (index - 1)) {
                    sum += listToInsert.get(i).Cumulative_Deferral__c;
                }
                if(accumulate) {
                    sum += listToInsert.get(i).Interest_on_PDP__c;
                    PDP_Interest__c ref = listToInsert.get(i);
                    ref.Accumulated__c = true;
                    listToInsert.set(i,ref);
                }
		}
		return sum;
	}

	private Set<Integer> accumulate(Integer index, List<PDP_Interest__c> l,Boolean accumulate) {
		Set<Integer> ret = new Set<Integer>();
		for(PDP_Interest__c intr: l ) {
			if(l.indexOf(intr)<index) {
                if(accumulate) {
                    intr.Accumulated__c = true;
                }
                if(!intr.Accumulated__c) {
                    ret.add(l.indexOf(intr));
                }
			}
		}
		return  ret;
	}



    private void setNumberOfDays(List<PDP_Interest__c> interests) {

    	for(PDP_Interest__c interest:(List<PDP_Interest__c>) interests ) {

    		interest.Number_of_Days__c = interest.Initial_Payment_Date__c.daysBetween(interest.Final_Paymant_Date__c)+1;
    	}
    }

	private void setPDPPaid(List<PDP_Interest__c> interests) {

		List<Id> allSechedId = new List<Id>();
		Map<Id,PDP_Payment_Schedule__c> mapIdSched = new Map<Id,PDP_Payment_Schedule__c>();

		for ( PDP_Interest__c interest:(List<PDP_Interest__c>) interests ) {
			if ( String.isNotEmpty(interest.PDP_Schedule__c) ) {
				allSechedId.add(interest.PDP_Schedule__c);
			}
		}

		for ( PDP_Payment_Schedule__c pdpSched: [ SELECT Id,Percentage_of_PDPRP__c,Paid_Amount_M__c FROM PDP_Payment_Schedule__c WHERE Id IN:allSechedId ] ){
			mapIdSched.put(pdpSched.Id, pdpSched);
		}

		for (PDP_Interest__c interest:(List<PDP_Interest__c>) interests ) {
			if ( String.isNotEmpty(interest.PDP_Schedule__c) ) {
				interest.PDP_Paid__c = mapIdSched.get(interest.PDP_Schedule__c).Paid_Amount_M__c;
				interest.PDP_Percentage__c = mapIdSched.get(interest.PDP_Schedule__c).Percentage_of_PDPRP__c;
			}
		}
	}

    private void setDeferral(List<PDP_Interest__c> interests) {

    	for(PDP_Interest__c interest:(List<PDP_Interest__c>) interests ) {
    		if(interest.Orig_PDP_Amount__c == null && interest.PDP_Paid__c != null) {
    			interest.Deferral__c = - interest.PDP_Paid__c;
    		}else if( interest.PDP_Paid__c == null && interest.Orig_PDP_Amount__c != null) {
    			interest.Deferral__c = interest.Orig_PDP_Amount__c ;
    		}else if(interest.PDP_Paid__c == null && interest.Orig_PDP_Amount__c == null ) {
    			interest.Deferral__c = 0;
    		}else{

    			interest.Deferral__c = interest.Orig_PDP_Amount__c - interest.PDP_Paid__c;
    		}
            interest.Deferral__c = interest.Deferral__c.setScale(5,System.RoundingMode.HALF_UP);
    	}
    }

    private void setCumulativeDeferral(List<PDP_Interest__c> interests) {
    	List<PDP_Interest__c> unnAccumulatedList = new List<PDP_Interest__c>();
    	for(PDP_Interest__c interest:(List<PDP_Interest__c>) interests) {
    		if( previousInterests.get(interest.Delivery__c) != null){
				PDP_Interest__c previousInterest = previousInterests.get(interest.Delivery__c).get(interest.Final_Paymant_Date__c);
				if(previousInterest!= null){
	    			Decimal cumDef = previousInterest.Cumulative_Deferral__c;
	    			Decimal interestPDP = previousInterest.Interest_on_PDP__c;
	    			interest.Cumulative_Deferral__c = cumDef + interestPDP + interest.Deferral__c;
	    			if ( interest.Accumulate__c ) {
	    				List<PDP_Interest__c> unaccumulated = unaccumulatedInterests.get(interest.Delivery__c);
	    				Decimal sum = 0;
	    				Boolean previousInterestAdded = false;
	    				if(unaccumulated != null ) {
		    				for(PDP_Interest__c unn : unaccumulated ) {	
		    					if(unn.Interest_on_PDP__c != null && unn.Final_Paymant_Date__c < interest.Initial_Payment_Date__c && unn.Id != previousInterest.Id ){
		    						sum += unn.Interest_on_PDP__c;
		    						if(!unn.Accumulated__c) {
		    							unn.Accumulated__c = true;
		    							System.debug('unAcumulated:  ' + unn);
		    							unnAccumulatedList.add(unn);
		    						}
		    				
		    					}else if(unn.Id == previousInterest.Id ) {
		    						previousInterestAdded = true;
		    						if(!unn.Accumulated__c) {
		    							unn.Accumulated__c = true;
		    							System.debug('unAcumulated:  ' + unn);
		    							unnAccumulatedList.add(unn);
		    						}
		    					}
		    				}
	    				}

	    				if(!previousInterestAdded){
	    					if(!previousInterest.Accumulated__c){
	    						previousInterest.Accumulated__c = true;
	    						unnAccumulatedList.add(previousInterest);
	    					}
		    				System.debug('PrevInterest:  ' + previousInterest);
	    				}
	    				interest.Cumulative_Deferral__c = cumDef + interestPDP + interest.Deferral__c + sum;
	    			}else {
	    				if(previousInterest.Accumulated__c) {
		    				previousInterest.Accumulated__c = false;
		    				System.debug('PrevInterest:  ' + previousInterest);

		    				unnAccumulatedList.add(previousInterest);
	    				}
						interest.Cumulative_Deferral__c =interest.Deferral__c + cumDef;

	    			}
				}else{
	    			interest.Cumulative_Deferral__c =  interest.Deferral__c;

				}
			}
            interest.Cumulative_Deferral__c = interest.Cumulative_Deferral__c.setScale(5,System.RoundingMode.HALF_UP);
    	}
        LeaseWareUtils.TriggerDisabledFlag=true;
    	update unnAccumulatedList;
		LeaseWareUtils.TriggerDisabledFlag=false;
    }

    private Map<Id,List<PDP_Interest__c>> getUnaccumulatedInterests(List<PDP_Interest__c> interests) {
    	Set<Id> modifiedInterest = new Set<Id>();
    	Set<Id> DeliveriesID = new Set<Id>();
    	Map<Id,List<PDP_Interest__c>> deliveriesMap = new Map<Id,List<PDP_Interest__c>>();
    	List<Date> dates = new List<Date>();
    	for(PDP_Interest__c interest : (List<PDP_Interest__c>) interests) {
    		if(interest.Id != null ) {
				modifiedInterest.add(interest.Id);
    		}
    		DeliveriesID.add(interest.Delivery__c);
    	}

    	List<PDP_Interest__c> allInterests;
    	if(modifiedInterest.size() == 0 ) {
    		allInterests = [SELECT Cumulative_Deferral__c,Accumulated__c,ID, Interest_on_PDP__c, Final_Paymant_Date__c , Initial_Payment_Date__c ,Delivery__c From PDP_Interest__c WHERE Delivery__c IN :DeliveriesID AND  Accumulated__c = false ];
    	}else{
    		allInterests = [SELECT Cumulative_Deferral__c, Accumulated__c,ID, Interest_on_PDP__c, Final_Paymant_Date__c , Initial_Payment_Date__c ,Delivery__c From PDP_Interest__c WHERE Delivery__c IN :DeliveriesID AND  Accumulated__c = false AND id Not In :modifiedInterest];
    	}
    	for(PDP_Interest__c interest : allInterests ) {

    		List<PDP_Interest__c> aux = deliveriesMap.get(interest.Delivery__c);
    		if(aux==null) {
    			aux = new List<PDP_Interest__c>();
    		}
    		aux.add(interest);
    		deliveriesMap.put(interest.Delivery__c,aux);
    	}
    	return deliveriesMap;
    }

    private void setInterestOnPDP (List<PDP_Interest__c> interests) {

    	for ( PDP_Interest__c interest : ( List<PDP_Interest__c>) interests  ) {

    		individualnterestCalc(interest);
    	}
    }

	private void individualnterestCalc(PDP_Interest__c interest) {
		if (interest.Cumulative_Deferral__c != null && interest.Rate__c != null && interest.Number_of_Days__c != null && interest.Cumulative_Deferral__c > 0) {
			System.debug('Interest Calc:');
			System.debug('Rate:' + interest.Rate__c);
			System.debug('Cumulative_Deferral__c:' + interest.Cumulative_Deferral__c);
			System.debug('Number_of_Days__c:' + interest.Rate__c);
			System.debug('interest.Rate__c/365:' + (interest.Rate__c / 365));
			interest.Interest_on_PDP__c = ((interest.Cumulative_Deferral__c * (interest.Rate__c / 365) * interest.Number_of_Days__c) / 100).setScale(5, System.RoundingMode.HALF_UP);
		} else {
			interest.Interest_on_PDP__c = 0;
		}
	}

	private Map<Id, Map<Date, PDP_Interest__c>> getPreviousInterest(List<PDP_Interest__c> interests) {
    	Set<Id> modifiedInterest = new Set<Id>();
    	Set<Id> DeliveriesID = new Set<Id>();
    	Map<Id,Map<Date,PDP_Interest__c>> deliveriesMap = new Map<Id,Map<Date,PDP_Interest__c>>();
    	for(PDP_Interest__c interest : (List<PDP_Interest__c>) interests) {
    		if(interest.Id != null ) {
				modifiedInterest.add(interest.Id);
    		}
    		deliveriesMap.put(interest.Delivery__c, new Map<Date,PDP_Interest__c>());
    		DeliveriesID.add(interest.Delivery__c);
    	}
    	List<PDP_Interest__c> allInterests;
    	if(modifiedInterest.size() == 0 ) {
    		allInterests = [SELECT Cumulative_Deferral__c,Accumulated__c, ID, Interest_on_PDP__c,Final_Paymant_Date__c, Delivery__c From PDP_Interest__c WHERE Delivery__c IN :DeliveriesID  Order BY Final_Paymant_Date__c];
    	}else {
			allInterests = [SELECT Cumulative_Deferral__c,Accumulated__c, ID, Interest_on_PDP__c,Final_Paymant_Date__c, Delivery__c From PDP_Interest__c WHERE Delivery__c IN :DeliveriesID  AND Id Not in :modifiedInterest  Order BY Final_Paymant_Date__c];
    	}
    	for(PDP_Interest__c interest : allInterests ) {
    		Map<Date,PDP_Interest__c> aux = deliveriesMap.get(interest.Delivery__c);
    		aux.put(interest.Final_Paymant_Date__c,interest);
    		deliveriesMap.put(interest.Delivery__c,aux);
    	}

    	Map<Id,Map<Date,PDP_Interest__c>> ret = new Map<Id,Map<Date,PDP_Interest__c>>();
		if(Trigger.isUpdate) {
			for (PDP_Interest__c interest : interests) {
				deliveriesMap.get(interest.Delivery__c).put(interest.Final_Paymant_Date__c,interest);
			}
		}

    	for(PDP_Interest__c interest :(List<PDP_Interest__c>) interests ){
    		Date newInterestDate = interest.Final_Paymant_Date__c;
    		Integer difference = MAX; 
    		Date neighbourDate;
    		for(Date oldInterestDate : deliveriesMap.get(interest.Delivery__c).keySet() ) {
    			if(oldInterestDate.daysBetween(newInterestDate) < difference && oldInterestDate.daysBetween(newInterestDate) > 0 ) {
    				difference = oldInterestDate.daysBetween(newInterestDate);
    				neighbourDate = oldInterestDate;
    			}
    		}
     		PDP_Interest__c previousInterst = deliveriesMap.get(interest.Delivery__c).get(neighbourDate);
    		Map<Date,PDP_Interest__c> aux ;
    		if ( ret.get(interest.Delivery__c) == null ) {
    			aux = new Map<Date,PDP_Interest__c>();
    		}else {
    			aux = ret.get(interest.Delivery__c);
    		}
			aux.put(newInterestDate,previousInterst);
			ret.put(interest.Delivery__c, aux );
    	}
    	return ret;
    }
}