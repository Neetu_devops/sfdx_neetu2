public  without sharing class CashSDTriggerHandler implements ITrigger {
    private final string triggerBefore = 'PCashSDTriggerHandlerBefore';
    private final string triggerAfter = 'CashSDTriggerHandlerAfter';
    public CashSDTriggerHandler() {
        
    }
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){}
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){}
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){}
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('CashSDTriggerHandler.afterInsert(+)');
        list<cash_security_deposit__c> newList = (list<cash_security_deposit__c>)trigger.new;
            set<id> leaseIds = new set<id>();
            for(cash_security_deposit__c sd: newList){
                leaseIds.add(sd.lease__c);
            }
            if(leaseIds.size()>0){
                setIsDepositReceivedOnlease(leaseIds,newList);
            }
       
        system.debug('CashSDTriggerHandler.afterInsert(-)');
    }
 
    /****
     * updates the Deposit Received field on the lease.
     * called from : afterInsert,afterDelete
     * @param : list of lease Ids
     */
    private void setIsDepositReceivedOnlease(set<id> leaseIds,list<cash_security_deposit__c> sdList){
        system.debug('setDepositReceivedDate(+)');
        Boolean isAllPaid = true;
        List<lease__c> leaseList = new List<lease__c>();
        map<id,lease__c> mapLease = new map<id,lease__c>([select id,Is_Deposit_Received__c,(select id,Deposit_Paid__c from cash_security_deposit__r)
                                                            from lease__c where id in :leaseIds]);
        for(lease__c curLease :mapLease.values() ){
            if( curLease.cash_security_deposit__r!=null &&  curLease.cash_security_deposit__r.size()>0){
                for(cash_security_deposit__c sd : curLease.cash_security_deposit__r){
                    if(sd.deposit_paid__c == false){
                        isAllPaid = false;
                        break;
                    }
                }
            }
            else{
               isAllPaid = false; 
            }
            curLease.Is_Deposit_Received__c = isAllPaid;
            leaseList.add(curLease);
        }
        if(!leaseList.isEmpty()){
            try{
                LeaseWareUtils.TriggerDisabledFlag=true;
                update leaseList;
                LeaseWareUtils.TriggerDisabledFlag=false;
            }
            catch(DmlException ex){
                System.debug(ex);
                string errorMessage='';
                for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                    // Process exception here
                    errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                }
                system.debug('errorMessage=='+errorMessage);
                for(cash_security_deposit__c sd : sdList){
                    sd.addError(errorMessage);
                }
            }
        }
        
        system.debug('setDepositReceivedDate(-)');
    }
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){}
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('CashSDTriggerHandler.afterDelete(+)');
        list<cash_security_deposit__c> oldList = (list<cash_security_deposit__c>)trigger.old; 
            set<id> leaseIds = new set<id>();
            for(cash_security_deposit__c sd:oldList ){
                leaseIds.add(sd.lease__c);
            }
            if(leaseIds.size()>0){
                setIsDepositReceivedOnlease(leaseIds,oldList);
            }
       
        system.debug('CashSDTriggerHandler.afterInsert(-)');
    }
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterUnDelete(){}    
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){}
}