public without sharing class PDPBatchTriggerHelper implements ITrigger {




    public void bulkBefore() {

    }
     
    public void bulkAfter() {

    }
     
    public  void beforeInsert() {

    }
     
    public void beforeUpdate() {

    }

    public void beforeDelete() {

    }

    public void afterInsert() {

    }

    public void afterUpdate() {
        deleteChangeStructure((List<Batch__c>) Trigger.new,(Map<Id,Batch__c>)Trigger.oldMap);
    }
 
    public void afterDelete() {

    }
    
    public void afterUnDelete() {

    }
 
    public void andFinally() {
    
    }


    public static void deleteChangeStructure( List<Batch__c> batchList , Map<Id,Batch__c> oldMap ) {
        Set<Id> batchIds= new Set<Id>();
        Map<Id,Pre_Delivery_Payment__c> pdps = new Map<Id,Pre_Delivery_Payment__c >();
        for (Batch__c batch: batchList ) {
            if ( oldMap.get(batch.Id).PDP_Structure__c != batch.PDP_Structure__c) {
                batchIds.add(batch.Id);
            }
        }
        
        for ( Pre_Delivery_Payment__c pdp: [ SELECT Id FROM Pre_Delivery_Payment__c WHERE Batch__c IN: batchIds AND Batch__c != null ] ) {
            pdps.put(pdp.Id,pdp);
 
        } 
        
        if ( pdps.keySet().size() > 0 ) {
            delete [ SELECT Id FROM PDP_Payment_Schedule__c WHERE PDP__c IN: pdps.keySet() ];
            
            PDPDelivieryBatch bcn = new PDPDelivieryBatch (pdps.keySet()) ;
            ID batchprocessid = Database.executeBatch(bcn);
            update pdps.values() ;
        }

    }

}