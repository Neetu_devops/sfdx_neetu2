/**
*******************************************************************************
* Class: TestRLUtility
* @author Created by Bhavna, Lease-Works,
* @date  6/13/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Test all the Methods of RLUtility.
*
* History:
* - VERSION   DEVELOPER NAME    	DATE         	DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestRLUtility {
    /**
    * @description null
    *  
    * @return null
    */
    @testSetup 
    static void setup(){
        
    }
    
    
    /**
    * @description This method gives the code coverage of  describeObject,describeFieldSet
    *  	findIconName method of RLUtility
    *
    * @return null
    */
    @isTest static void testRLUtilityController(){
        Schema.DescribeSObjectResult objectDescribe1 = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        
        RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        RLUtility.describeFieldSet(objectDescribe1, leasewareutils.getNamespacePrefix() + 'CommercialTermFieldset');
        string accountIcon = RLUtility.findIconName('Account');
        string pricingIcon = RLUtility.findIconName(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        string marketingIcon = RLUtility.findIconName(leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c');
        system.assertEquals(accountIcon, 'standard:account');
        system.assertEquals(pricingIcon, null);
        system.assertEquals(marketingIcon, 'custom:custom30');
        
    }
}