@isTest
public class TestEngineTemplateManagerController {
    
    @isTest
    static void loadTestCases() {
        
		Custom_Lookup__c CL = new Custom_Lookup__c();
        CL.Lookup_Type__c = 'Engine';
        CL.Name = 'Default';
        insert CL;
    	
        Engine_Template__c ET = new Engine_Template__c();
        ET.Name = 'CFM56-5B';
        ET.Current_Catalog_Year__c = '2019';
        ET.Manufacturer__c = 'CFMI';
        ET.Model__c = 'CFM56-5B';
        ET.Thrust_LP__c = CL.Id;
        insert ET;
        
        LLP_Template__c LLPT = new LLP_Template__c();
        LLPT.Engine_Template__c = ET.Id;
        LLPT.Name = 'FAN DISK';
        LLPT.Part_Number__c = '338-001-504-0';
        LLPT.Part_Name__c = 'FAN DISK';
        LLPT.Life_Limit_Cycles__c = 30000;
        LLPT.Replacement_Cost__c = 235345;
        insert LLPT;
        
        LLP_Catalog__c LLPC = new LLP_Catalog__c();
        LLPC.Name = '2019';
        LLPC.Catalog_Price__c = 235345;
        LLPC.LLP_Template__c = LLPT.Id;
        LLPC.Part_Number_New__c = '2353';
        insert LLPC;
        
        LLP_Limit_Catalog__c LLPLC = new LLP_Limit_Catalog__c();
        LLPLC.Name = '2019';
        LLPLC.Life_Limit_Cycles__c = 235345;
        LLPLC.LLP_Template__c = LLPT.Id;
        LLPLC.Part_Number__c = '2353';
        LLPLC.Thrust__c = CL.Id;
        insert LLPLC;
        
        
        //----------------LLP Limit dat------------------------------
        List<EngineTemplateManagerController.LLPLimitWrapper> llpLimitList = new List<EngineTemplateManagerController.LLPLimitWrapper>();
        EngineTemplateManagerController.LLPLimitWrapper llpLimitWrapper = new EngineTemplateManagerController.LLPLimitWrapper();
        llpLimitWrapper.Id = LLPLC.Id;
        llpLimitWrapper.Name = '2019';
        llpLimitWrapper.LifeLimitCycles = 3243;
        llpLimitWrapper.PartNumber = '2353';
        llpLimitWrapper.ThrustId = CL.Id; 
        llpLimitList.add(llpLimitWrapper);
        
        List<EngineTemplateManagerController.LLPLimitWrapper> llpLimitListN = new List<EngineTemplateManagerController.LLPLimitWrapper>();
        EngineTemplateManagerController.LLPLimitWrapper llpLimitWrapperN = new EngineTemplateManagerController.LLPLimitWrapper();
        llpLimitWrapperN.Name = '2019';
        llpLimitWrapperN.LifeLimitCycles = 3288;
        llpLimitWrapperN.PartNumber = '2388';
        llpLimitWrapperN.ThrustId = CL.Id; 
        llpLimitListN.add(llpLimitWrapperN);
        
        String llpLimitWrapperStr = JSON.serialize(llpLimitList);
        String llpLimitWrapperStrN = JSON.serialize(llpLimitListN);
        
        //-----------------------------LLP Price Data------------------------------------
        List<EngineTemplateManagerController.LLPCatalogWrapper> llpCatalogList = new List<EngineTemplateManagerController.LLPCatalogWrapper>();
        EngineTemplateManagerController.LLPCatalogWrapper llpCatalogWrapper = new EngineTemplateManagerController.LLPCatalogWrapper();
        llpCatalogWrapper.Id = LLPC.Id;
        llpCatalogWrapper.Name = '2019';
        llpCatalogWrapper.LeadTimeDays = 32;
        llpCatalogWrapper.PartNumber = '2353';
        llpCatalogWrapper.CatalogPrice = 3431;
        llpCatalogList.add(llpCatalogWrapper);
        
        List<EngineTemplateManagerController.LLPCatalogWrapper> llpCatalogListN = new List<EngineTemplateManagerController.LLPCatalogWrapper>();
        EngineTemplateManagerController.LLPCatalogWrapper llpCatalogWrapperN = new EngineTemplateManagerController.LLPCatalogWrapper();
        llpCatalogWrapperN.Name = '2019';
        llpCatalogWrapperN.LeadTimeDays = 38;
        llpCatalogWrapperN.PartNumber = '2388';
        llpCatalogWrapperN.CatalogPrice = 7831;
        llpCatalogListN.add(llpCatalogWrapperN);
        
        String llpCatalogWrapperStr = JSON.serialize(llpCatalogList);
        String llpCatalogWrapperStrN = JSON.serialize(llpCatalogListN);
        
        
        //------------------------LLP Template Data----------------------------------------------------
        List<EngineTemplateManagerController.LLPTemplateWrapper> llpTempList = new List<EngineTemplateManagerController.LLPTemplateWrapper>();
        EngineTemplateManagerController.LLPTemplateWrapper llpTempWrapper = new EngineTemplateManagerController.LLPTemplateWrapper();
        llpTempWrapper.Id = LLPT.Id;
        llpTempWrapper.Name = '2019';
        llpTempWrapper.IIN = '32';
        llpTempWrapper.DefaultPn = '2353';
        llpTempWrapper.LifeLimit = 23;
        llpTempWrapper.LLPDesc = 'test';
        llpTempList.add(llpTempWrapper);
        
        List<EngineTemplateManagerController.LLPTemplateWrapper> llpTempListN = new List<EngineTemplateManagerController.LLPTemplateWrapper>();
        EngineTemplateManagerController.LLPTemplateWrapper llpTempWrapperN = new EngineTemplateManagerController.LLPTemplateWrapper();
        llpTempWrapperN.Name = '2019';
        llpTempWrapperN.IIN = '38';
        llpTempWrapperN.DefaultPn = '2388';
        llpTempWrapperN.LifeLimit = 23;
        llpTempWrapperN.LLPDesc = 'test';
        llpTempListN.add(llpTempWrapperN);
        
        String llpTempWrapperStr = JSON.serialize(llpTempList);
        String llpTempWrapperStrN = JSON.serialize(llpTempListN);
        
        
        EngineTemplateManagerController.getLLPLimitData(LLPT.Id, '2019');
        EngineTemplateManagerController.updateLLDLimitData(llpLimitWrapperStr,llpLimitWrapperStrN, LLPT.Id, '2019');
        
        EngineTemplateManagerController.getLLPCatalogYearTemplate(LLPT.Id);
        EngineTemplateManagerController.getLLPCatalogData(LLPT.Id, '2019');
        try{
        	EngineTemplateManagerController.updateLLDCatalogData(llpCatalogWrapperStr,llpCatalogWrapperStrN, LLPT.Id, '2019');
        }
        catch(Exception e){}
        
        //EngineTemplateManagerController.updateTemplateDefaultPN(LLPT.Id, '234523');
        //EngineTemplateManagerController.addNewLLPCatalog(llpCatalogWrapperStrN, LLPT.Id, '2019', null);
        
        EngineTemplateManagerController.getTemplateBasicDetails(ET.Id);
        EngineTemplateManagerController.getTempData(ET.Id);
        EngineTemplateManagerController.updateLLDData(llpTempWrapperStr,llpTempWrapperStrN, ET.Id);
        
        EngineTemplateManagerController.getNamespacePrefix();
        EngineTemplateManagerController.getPickListValues('LLP_Template__c', 'Module_N__c');

        EngineTemplateManagerController.deleteLLPLimit(LLPLC.Id);
        EngineTemplateManagerController.deleteLLPCatalog(LLPC.Id);
        EngineTemplateManagerController.deleteLLP(LLPT.Id);  
    }
    
    
    
}