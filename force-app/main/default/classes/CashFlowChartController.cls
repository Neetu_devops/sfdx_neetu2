public with sharing class CashFlowChartController {
    @AuraEnabled
    public static String checkLessor(){
        return Utility.checkAirline();
    }
    
    @AuraEnabled
    public static Scenario_Input__c getScenarioInput(String recordId){
        return [select Id, comments__c from Scenario_Input__c where id =: recordId];
    }
    
    @AuraEnabled
    public static void updateScenarioInput(Scenario_Input__c scenarioInput){
        update scenarioInput;
    }
    
    @AuraEnabled
    public static String isForecastGenerated(String recordId) {
        return Utility.checkForecastStatus(recordId);
    }
    
    //Cash Flow Table Popup
    @auraEnabled
    public static ReturnConditionWrapper.CashFlowPerMonthMainWrapper getCashFlowDataTable(Id recordId, String startDate) {
        ReturnConditionWrapper.CashFlowPerMonthMainWrapper cashFlowMainWrapper = new ReturnConditionWrapper.CashFlowPerMonthMainWrapper();
        List<ReturnConditionWrapper.CashFlowPerMonthWrapper> lstCashFlowPerMonth = new List<ReturnConditionWrapper.CashFlowPerMonthWrapper> ();
        
        List<Monthly_Scenario__c> lstMonthlyScenario = ForecastSOQLServices.getMonthlyScenarioAsPerDate(recordId,startDate);
        //System.debug('lstMonthlyScenario-->>'+lstMonthlyScenario);
        Set<String> setHeaderName  = new Set<String>();
        Set<String> setfirstColumn  = new Set<String>{'Rent','SD','MR','PR','EOLA','Claims','Net Cash Flow'};
            Map<String,Monthly_Scenario__c> mapMonthlyScenarioPerType = new Map<String,Monthly_Scenario__c>();
        
        for(Monthly_Scenario__c ms : lstMonthlyScenario){
            if(ms.Fx_Component_Output__r.Type__c.contains('General')){
                mapMonthlyScenarioPerType.put(ms.Fx_Component_Output__r.Type__c, ms);  
            }
            else{
                if(ms.Fx_Component_Output__r.Type__c.contains('Airframe')){
                    setHeaderName.add(ms.Fx_Component_Output__r.Event_Type__c);
                    mapMonthlyScenarioPerType.put(ms.Fx_Component_Output__r.Event_Type__c, ms); 
                }else if(ms.Fx_Component_Output__r.Type__c.contains('Landing Gear')){
                    String name = ms.Fx_Component_Output__r.Type__c.replace('Landing Gear', 'LG');
                    setHeaderName.add(name);
                    mapMonthlyScenarioPerType.put(name, ms); 
                }else{
                    setHeaderName.add( ms.Fx_Component_Output__r.Type__c);
                    mapMonthlyScenarioPerType.put(ms.Fx_Component_Output__r.Type__c, ms);  
                }  
            } 
        }
        cashFlowMainWrapper.lstHeader = new List<String>(setHeaderName);
        Decimal totalNetCashFlow = 0.0;
        for(String firstCol : setfirstColumn){
            ReturnConditionWrapper.CashFlowPerMonthWrapper cfwrapper = new ReturnConditionWrapper.CashFlowPerMonthWrapper(); 
            List<Decimal> lstValAsPerRow = new List<Decimal>();
            cfwrapper.total = 0.0;
            for(String val :setHeaderName){
                if(firstCol.contains('Rent')){
                    if(mapMonthlyScenarioPerType.containsKey('General')){
                        cfwrapper.name = firstCol;
                        cfwrapper.total = (mapMonthlyScenarioPerType.get('General').Rent_F__c);
                        cfwrapper.lstvalue.add(0);
                    }
                }  
                else if(firstCol.contains('SD')){
                    if(mapMonthlyScenarioPerType.containsKey('General')){
                        cfwrapper.name = firstCol;
                        cfwrapper.total = (mapMonthlyScenarioPerType.get('General').SD_F__c);
                        cfwrapper.lstvalue.add(0);
                    }
                }  
                else if(firstCol == 'MR'){
                    cfwrapper.name = firstCol;
                    if(mapMonthlyScenarioPerType.containsKey(val) ){
                        cfwrapper.total += mapMonthlyScenarioPerType.get(val).MR_F__c == null ? 0.0 : mapMonthlyScenarioPerType.get(val).MR_F__c ;
                        cfwrapper.lstvalue.add(mapMonthlyScenarioPerType.get(val).MR_F__c == null ? 0 : mapMonthlyScenarioPerType.get(val).MR_F__c);
                    }
                } else if(firstCol == 'PR'){
                    cfwrapper.name = firstCol;
                    if(mapMonthlyScenarioPerType.containsKey(val) ){
                        cfwrapper.total += mapMonthlyScenarioPerType.get(val).PR_Cost_F__c == null ? 0.0 : mapMonthlyScenarioPerType.get(val).PR_Cost_F__c ;
                        cfwrapper.lstvalue.add(mapMonthlyScenarioPerType.get(val).PR_Cost_F__c == null ? 0 : mapMonthlyScenarioPerType.get(val).PR_Cost_F__c );
                    }
                }else if(firstCol == 'Eola'){
                    cfwrapper.name = firstCol;
                    if(mapMonthlyScenarioPerType.containsKey(val) ){
                        cfwrapper.total += mapMonthlyScenarioPerType.get(val).Eola_Amount__c == null ? 0.0 : mapMonthlyScenarioPerType.get(val).Eola_Amount__c ;
                        cfwrapper.lstvalue.add(mapMonthlyScenarioPerType.get(val).Eola_Amount__c == null ? 0 : mapMonthlyScenarioPerType.get(val).Eola_Amount__c );
                    }
                }else if(firstCol == 'Claims'){
                    cfwrapper.name = firstCol;
                    if(mapMonthlyScenarioPerType.containsKey(val) ){
                        cfwrapper.total += mapMonthlyScenarioPerType.get(val).Claim_F__c == null ? 0.0 : mapMonthlyScenarioPerType.get(val).Claim_F__c ;
                        cfwrapper.lstvalue.add(mapMonthlyScenarioPerType.get(val).Claim_F__c == null ? 0 : mapMonthlyScenarioPerType.get(val).Claim_F__c);
                    }
                }else if(firstCol == 'Net Cash Flow'){
                    cfwrapper.name = firstCol;
                    if(mapMonthlyScenarioPerType.containsKey(val) ){
                        cfwrapper.total = totalNetCashFlow ;
                        cfwrapper.lstvalue.add(mapMonthlyScenarioPerType.get(val).Cash_Flow_F__c == null ? 0 : mapMonthlyScenarioPerType.get(val).Cash_Flow_F__c);
                    }
                }
            }
            totalNetCashFlow += cfwrapper.total;
            //System.debug(firstCol+'<<---totalNetCashFlow-->>'+totalNetCashFlow); 
            cashFlowMainWrapper.lstCashFlowPerMonthWrapper.add(cfwrapper);
        }
        //System.debug('cashFlowMainWrapper-->>'+cashFlowMainWrapper);
        return cashFlowMainWrapper;
    }
    
    //Cash Flow Chart Line(1st Chart)
    @auraEnabled
    public static String getCashFlowDataforLine1(Id recordId) {
        
        Scenario_Input__c scenarioInp = ForecastSOQLServices.getScenarioInput(recordId);
        List<Scenario_Component__c> lstSecnarioCmp =  ForecastSOQLServices.getScenarioOutput(scenarioInp.Id);
        List<Monthly_Scenario__c> lstMonthlyScenario = ForecastSOQLServices.getMonthlyScenario(lstSecnarioCmp);
        
        StaticResource static_resource = [SELECT Id,Name, SystemModStamp
                                          FROM StaticResource 
                                          WHERE Name = 'APU'
                                          LIMIT 1];
        String org_Url = String.valueOF(System.URL.getSalesforceBaseUrl().toExternalForm());
        
        String url_file_ref = '/resource/'
            + String.valueOf(((DateTime)static_resource.get('SystemModStamp')).getTime())
            + '/' 
            + leasewareutils.getNamespacePrefix()+static_resource.get('Name')
            ;
        org_Url += url_file_ref;
        Map<String, Decimal> mapMonthlyScenario = calculateTotalAsPerType(lstMonthlyScenario,false);
        String min ='' ,max ='';
        for(Monthly_Scenario__c ms : lstMonthlyScenario){
            if(min == ''){
                min = Utility.formatDate(ms.Start_Date__c);
            }
            max = Utility.formatDate(ms.Start_Date__c);
        }
        
        String markerJSON =  'url' + '(' + org_Url + ')' ;
        List<CashFlowData> data = new List<CashFlowData>();
        
        for(String key : mapMonthlyScenario.keySet()){
            if(key == min || key == max){
                data.add(new CashFlowData(key,markerJSON,Math.round(mapMonthlyScenario.get(key)/1000)));  
            }else{
                data.add(new CashFlowData(key,'',Math.round(mapMonthlyScenario.get(key)/1000)));
            }
        }
        //return System.json.serialize(data);
        return System.Json.serialize(data);   
    }
    
    //Calculate Total as per Type
    public static Map<String,Decimal> calculateTotalAsPerType(List<Monthly_Scenario__c> lstMonthlyScenario, Boolean isInvestmentRequired){
        Map<String, Decimal> mapMonthlyScenario = new Map<String, Decimal>();
        String leaseDate;
        Decimal investmentReq =0;
        leaseDate= Utility.formatDate(lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c.addMonths(-1));
        if(isInvestmentRequired && !String.isBlank(leaseDate) && lstMonthlyScenario.size()>0 && lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c != null){
            investmentReq = lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c > 0 ? lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c * (-1) : lstMonthlyScenario[0].Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c;
            mapMonthlyScenario.put(leaseDate,investmentReq);
        }
        
        
        //System.debug('lstMonthlyScenario'+lstMonthlyScenario.size());
        for(Monthly_Scenario__c ms : lstMonthlyScenario){
            if(ms.Fx_Component_Output__r.Type__c == 'General'){
                mapMonthlyScenario.put(Utility.formatDate(ms.Start_Date__c),ms.MR_Amount__c);
            }
        }
        System.debug('<<<mapMonthlyScenario>>'+mapMonthlyScenario);
        return mapMonthlyScenario;
    }
    
    // Wrapper class
    public class CashFlowData {
        @AuraEnabled
        public String month { get; set; }
        @AuraEnabled
        public MarkerWrapper marker { get; set; }
        @AuraEnabled
        public Decimal y { get; set; }
        
        public CashFlowData(String month,String marker,Decimal y) {
            this.month = month;
            this.marker = new MarkerWrapper(marker);
            this.y = y;
        }
        
    }
    
    //Wrapper class for showing marker with data
    public class MarkerWrapper{
        @AuraEnabled
        public String symbol{get;set;}
        public MarkerWrapper(String symbol){
            this.symbol = symbol;
        }
        
    }
}