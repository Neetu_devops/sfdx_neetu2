@isTest
private class TestBatchDeleteWorldFleet {

    @testSetup
    static Void testSetup(){
        List<Global_Fleet__c> fleetRecordList = new List<Global_Fleet__c>();
        Operator__c opr = new Operator__c(Name='Test Operator');
        insert opr;
        for(Integer i=0;i<100;i++){
            Global_Fleet__c fleetRecord = new Global_Fleet__c();
            fleetRecord.Name = 'Test Fleet '+i;
            fleetRecord.Aircraft_External_ID__c= '1-2'+i;
            fleetRecord.Aircraft_Operator__c = opr.Id;	
            fleetRecordList.add(fleetRecord);
        }
        insert fleetRecordList;
        
        Fleet_Summary__c fleetSummary = new Fleet_Summary__c(name='Test.....',
               Operator__c =  opr.id);
        insert fleetSummary;
         
    }
   
    @isTest
    static void batchDelete(){
      
       	Test.startTest();
        Database.executeBatch(new BatchDeleteWorldFleet(),101);
        Test.stopTest();
       
        List<Global_Fleet__c> fleets = [select Id from Global_Fleet__c ];
        System.assertEquals(fleets.size(), 0);
         
    }
    
    @isTest
    static void batchDeleteFailureScenario(){
      
       	Test.startTest();
        
         Operator__c airline = new Operator__c();
        airline.Name =  'testopera';
        insert airline; 
        System.debug('airline :'+airline);
        
        Marketing_Activity__c deal = new Marketing_Activity__c();
        deal.Marketing_Rep_c__c = 'N/A';
        deal.Deal_Type__c ='Lease';
        deal.Deal_Status__c = 'Spec Sent';
        deal.Description__c = 'Test Deal';
        deal.Prospect_Type__c = 'Airline';
        deal.Prospect__c = airline.Id;
        insert deal;
        System.debug('deal :'+deal);

        Global_Fleet__c aircraft  = new Global_Fleet__c();
        aircraft.Aircraft_External_ID__c = 'abc';
        aircraft.Name    = 'testWF';
        aircraft.AircraftType__c = 'A320';
        insert aircraft;
        
        Aircraft_Proposal__c assetinDeal = new Aircraft_Proposal__c();
        assetinDeal.Marketing_Activity__c = deal.Id;
        assetinDeal.Name = 'Test AssetInDeal1';
        assetinDeal.New_Used__c = 'New';
        assetinDeal.World_Fleet_Aircraft__c = aircraft.id;
        insert assetinDeal;
        System.debug('assetinDeal :'+assetinDeal);
       
        Database.executeBatch(new BatchDeleteWorldFleet(),101);
        Test.stopTest();
       
        List<Global_Fleet__c> fleets = [select Id from Global_Fleet__c ];
        System.assertEquals(fleets.size(), 1);
         
    }
}