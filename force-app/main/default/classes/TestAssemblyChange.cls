@isTest
private class TestAssemblyChange 
{  
    
	@isTest(seeAllData=true)
    static void testAssembly_CRUD_2() 
    {     
		Date tempDate = system.today(); 
        string NewUsed='New';
        string ACName = 'TestMSN1';
        string AircraftType ='737';
        Aircraft__c AC1;
        string Variant = '200';
        try{
            AC1 = [select id from Aircraft__c where MSN_Number__c = :ACName limit 1];
        }catch(Exception ex){
            AC1 = new Aircraft__c(MSN_Number__c=ACName, TSN__c=200, CSN__c=200, Threshold_for_C_Check__c=1000
                                                ,New_Used__c = NewUsed,Aircraft_Type__c = AircraftType,	Aircraft_Variant__c = Variant
                                                , Status__c='Available',Date_of_Manufacture__c=tempDate.toStartOfMonth());
                                                        
            LeaseWareUtils.clearFromTrigger();insert AC1;
        }
        
        map<String,Object> mapEngineRecTypes = new map<String,Object>();
        TestLeaseworkUtil.setRecordTypeOfEngine(mapEngineRecTypes);
        ID recTypeId=(ID)(mapEngineRecTypes.get('Other'));

		list<Custom_Lookup__c> listCustomLookup = [select id,name from Custom_Lookup__c] ;
		for(Custom_Lookup__c curCL  :listCustomLookup){
			mapEngineRecTypes.put('Custom_Lookup_Name.'+ curCL.Name,curCL.Id);
		}

        
        		
		list<Aircraft__c> listAC = [select id,MSN_Number__c from Aircraft__c where MSN_Number__c like 'TestMSN1'];	
    // 2.1 Add Assembly
     	list<Constituent_Assembly__c> listCA = new list<Constituent_Assembly__c>();
     	string AssemblyName;
     	string CurrentTS = '5A1';string Current_TS_Id = (Id)mapEngineRecTypes.get('Custom_Lookup_Name.'+CurrentTS);
     	string EngineModel='CFM56';
     	string ManufacturerList = null;
     	string CAtype;
	    for(integer i=0;i<listAC.size();i++){
	    	AssemblyName = listAC[i].MSN_Number__c + '-E';
	    	
	    	CAtype = 'Engine 1';
	        listCA.add(new Constituent_Assembly__c(
	            Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
	            TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today(), //TSLV__c=0, CSLV__c= 0,
	             Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
	            ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
	        		
	    	CAtype = 'Engine 2';
	        listCA.add(new Constituent_Assembly__c(
	            Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
	            TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today()
	            , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
	            ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));

	    	CAtype = 'Engine 3';
	        listCA.add(new Constituent_Assembly__c(
	            Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
	            TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today()
	            , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
	            ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
	        		
	    	CAtype = 'Engine 4';
	        listCA.add(new Constituent_Assembly__c(
	            Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
	            TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today()
	            ,  Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
	            ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));

	            
	    	CAtype = 'APU';
	        listCA.add(new Constituent_Assembly__c(
	            Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
	            TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today()
	            , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
	            ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
	            
	    	CAtype = 'Landing Gear - Left Wing';
	        listCA.add(new Constituent_Assembly__c(
	            Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
	            TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today()
	            , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
	            ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
	            
	    	CAtype = 'Landing Gear - Right Main';
	        listCA.add(new Constituent_Assembly__c(
	            Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
	            TSN__c=0, CSN__c=0, Date_of_Manufacture__c=system.today()
	            ,  Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
	            ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));	            	            	            			
		}
		LeaseWareUtils.clearFromTrigger();insert  listCA; 
		Test.startTest();
		list<Sub_Components_LLPs__c> insLLPs = new list<Sub_Components_LLPs__c>();
		for(Constituent_Assembly__c curCA   :listCA){
			if(curCA.Type__c.contains('Engine')){
				insLLPs.add(new Sub_Components_LLPs__c(Name='LLPTest', Serial_Number__c='1234'
						, Constituent_Assembly__c=curCA.Id,part_number__c=curCA.Id
						, Life_Limit_Cycles__c=1000, TSN__c=0, CSN__c=0));
			}
		}

		LeaseWareUtils.clearFromTrigger();insert  insLLPs;     

		LeaseWareUtils.clearFromTrigger();update  listCA;  
		Test.stopTest();   		
		LeaseWareUtils.clearFromTrigger();delete  listCA;       
    }

	@isTest(seeAllData=true)
    static  void testAssembly3() 
    {     

        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN, Date_of_Manufacture__c = System.today(),Aircraft_Type__c='737', Aircraft_Variant__c='300', TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Engine_1_Check__c=1, Status__c='Available');
        insert a;

        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='Name', Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56', Engine_Model__c ='CFM56', Attached_Aircraft__c= a.Id, Description__c ='Desc', TSN__c=0, CSN__c=0,  Type__c ='Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1');
        insert consttAssembly;

        a.Aircraft_Type__c='A320';
        a.Aircraft_Variant__c='232';
        update a;
        consttAssembly.Aircraft_Type__c='A320-232';
        consttAssembly.Engine_Thrust__c='V2527-A5';
        consttAssembly.Aircraft_Engine__c='A320-232 @ V2500';
        update consttAssembly;
        
        a.Aircraft_Type__c='A320';
        a.Aircraft_Variant__c='233';
        update a;
        consttAssembly.Aircraft_Type__c='A320-233';
        consttAssembly.Engine_Thrust__c='V2527E-A5';
        consttAssembly.Aircraft_Engine__c='A320-233 @ V2500';
        update consttAssembly;
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='131';
        update a;
        consttAssembly.Aircraft_Type__c='A321-131';
        consttAssembly.Engine_Thrust__c='V2530-A5';
        consttAssembly.Aircraft_Engine__c='A321-131 @ V2500';
        update consttAssembly;
        
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='231';
        update a;
        consttAssembly.Aircraft_Type__c='A321-231';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A321-231 @ V2500';
        update consttAssembly;
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='232';
        update a;
        consttAssembly.Aircraft_Type__c='A321-232';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A321-232 @ V2500';
        update consttAssembly;
        
        // Set up the Constt Assmbly record.
        Sub_Components_LLPs__c LLP = new Sub_Components_LLPs__c(Name='LLPTest', Serial_Number__c='1234', Constituent_Assembly__c=consttAssembly.Id, Life_Limit_Cycles__c=1000, TSN__c=0, CSN__c=0);
        insert LLP; 
        
//        LeaseWareUtils.updateAllSubCompsThrustSettings(consttAssembly);
                // Verify that the results are as expected.
        Constituent_Assembly__c ca = [SELECT Name, Description__c
             FROM Constituent_Assembly__c
             WHERE Id = :consttAssembly.Id];
        String caDesc2=ca.Description__c; 
        System.assertEquals('Desc', caDesc2);
        
    } 
    
         
    static testMethod void testAssemblyNextEventwithoutMP()  {
    	
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);     	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A1');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        
        
	    LeaseWareUtils.clearFromTrigger();
        
		//mapInOut.put('Utilization_Report__c.True_Up__c',null); 
		mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addyears(-2));//lease start
		mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');
		TestLeaseworkUtil.createUR(mapInOut);    	
    	
    } 
    
    @isTest(seeAllData=true)
    static  void testAssemblyNextEventwithMP()  {
    	map<String,Object> mapInOut = new map<String,Object>();
		string ACName = 'TestSeededMSN1';
        Aircraft__c aircraftRec1 = [select id,lease__c 
        							,(select Id,Y_Hidden_Est_Next_Shop_Visit_RU1__c,type__c from Constituent_Assemblies__r)
        							from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',aircraftRec1.Id);
        mapInOut.put('Aircraft__c.lease__c',aircraftRec1.lease__c);
            	
    	
    	mapInOut.put('Maintenance_Program__c.Historical_Utilization_Months__c',10) ;
    	TestLeaseworkUtil.createMP(mapInOut); 
		Id MPId = (Id)mapInOut.get('Maintenance_Program__c.Id');
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Engine') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
    	TestLeaseworkUtil.createMPDetail(mapInOut); 
    	
		
	    LeaseWareUtils.clearFromTrigger();    	
	     	
    	 
    	aircraftRec1.Maintenance_Program__c = 	MPId;
    	system.debug('aircraftRec1.Maintenance_Program__c'+ aircraftRec1.Maintenance_Program__c);
    	update aircraftRec1;
        
        
	    LeaseWareUtils.clearFromTrigger();
        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');   	    
    	Constituent_Assembly__c[] CAReclist = [select Id,Y_Hidden_Est_Next_Shop_Visit_RU1__c,type__c from Constituent_Assembly__c
    										where Attached_Aircraft__c =:aircraftRec1.Id];
		 
		List<Maintenance_Program_Event__c> listMPE =  [select id from Maintenance_Program_Event__c where Maintenance_Program__r.Name like 'TestSeededMPN%'];
		
		List<Assembly_Event_Info__c> listAssemblyVar= [Select Assembly_Type__c, Event_Type__c from Assembly_Event_Info__c where Maintenance_Program_Event__c in :listMPE];
		
		map<string,string> mapprojevnt = new map<string,string> ();
		for(Assembly_Event_Info__c curProjEvent: listAssemblyVar) {
			mapprojevnt.put(curProjEvent.Assembly_Type__c, curProjEvent.Event_Type__c);
		}
		list<Assembly_Eligible_Event__c> assEEList = new list<Assembly_Eligible_Event__c>();
        Id recordTypeIdVar;
		
    	for(Constituent_Assembly__c curRec:CAReclist){
            if(curRec.Type__c =='Airframe') recordTypeIdVar= Schema.SObjectType.Assembly_Eligible_Event__c.getRecordTypeInfosByName().get('Airframe').getRecordTypeId();
            else recordTypeIdVar= Schema.SObjectType.Assembly_Eligible_Event__c.getRecordTypeInfosByName().get('Assemblies').getRecordTypeId();
		
	        assEEList.add(new Assembly_Eligible_Event__c( recordTypeId = recordTypeIdVar,Event_Type__c = mapprojevnt.get(curRec.Type__c),
	        Constituent_Assembly__c=curRec.Id, Start_Date__c=dateStart, End_Date__c=dateEnd, TSN__c = 100, CSN__c = 100));
        }

	    LeaseWareUtils.clearFromTrigger();
		insert assEEList;
		System.debug('6.Number of Queries testAssemblyNextEventwithMP: ' + Limits.getQueries());  
		
		LeaseWareUtils.clearFromTrigger();		
        //mapInOut.put('Utilization_Report__c.True_Up__c','1'); 
		mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(-2));
		mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');
		TestLeaseworkUtil.createUR(mapInOut);    	
 		System.debug('7.Number of Queries testAssemblyNextEventwithMP: ' + Limits.getQueries()); 
        
	    LeaseWareUtils.clearFromTrigger();
        Test.startTest();
		mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(-1));
		mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');
		TestLeaseworkUtil.createUR(mapInOut);  
    	System.debug('8.Number of Queries testAssemblyNextEventwithMP: ' + Limits.getQueries()); 
    	Test.stopTest();
    	
    	Constituent_Assembly__c[] CARec = [select Id,Y_Hidden_Est_Next_Shop_Visit_RU1__c,type__c from Constituent_Assembly__c
    										where Attached_Aircraft__c =:aircraftRec1.Id];
    	for(Constituent_Assembly__c curRec:CARec){
    		system.debug('type__c' + curRec.type__c);
    		system.debug('Y_Hidden_Est_Next_Shop_Visit_RU1__c' + curRec.Y_Hidden_Est_Next_Shop_Visit_RU1__c);
    		
    	}
	}  
	@isTest(seeAllData=true)
    static void testThrustUpdateValidation() {
        
        Custom_Lookup__c lookup1 = new Custom_Lookup__c(name = '5A51', Lookup_Type__c = 'Engine', Sub_LookupType__c = 'CFM56', Description__c = 'Test data');
        Custom_Lookup__c lookup2 = new Custom_Lookup__c(name = '5A52', Lookup_Type__c = 'Engine', Sub_LookupType__c = 'CFM56', Description__c = 'Test data');
        
        insert lookup1; 
        insert lookup2;
        
        Date tempDate = system.today();
        Aircraft__c aircraft = new Aircraft__c(MSN_Number__c='TestMsn1', TSN__c=200, CSN__c=200, Threshold_for_C_Check__c=1000
                                                ,New_Used__c = 'New', Aircraft_Type__c = '737',	Aircraft_Variant__c = '200'
                                                , Status__c='Available',Date_of_Manufacture__c=tempDate.toStartOfMonth());
        insert aircraft;
        
        Constituent_Assembly__c assembly = new Constituent_Assembly__c(
	            Name='AssemblyName', Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c ='CFM56', Attached_Aircraft__c= aircraft.ID, Description__c ='Test', 
	            TSN__c=100, CSN__c=100, TSLV__c=0,Date_of_Manufacture__c=system.today()
	            , CSLV__c= 0, Type__c = 'Engine 1', Serial_Number__c='AssemblyName', Engine_Thrust__c='-3B1', 
            	Current_Thrust_Setting__c=lookup1.Id
	            ,Current_TS__c =  lookup1.Id);
	    
        Constituent_Assembly__c assemblyWithdiffThrust = new Constituent_Assembly__c(
	            Name='AssemblyName', Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
	            , Engine_Model__c ='CFM56', Attached_Aircraft__c= aircraft.ID, Description__c ='Test', 
	            TSN__c=100, CSN__c=100, TSLV__c=0,Date_of_Manufacture__c=system.today()
	            , CSLV__c= 0, Type__c = 'Engine 2', Serial_Number__c='AssemblyName', Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=lookup2.Id
	            ,Current_TS__c =  lookup2.Id);
        
        Insert assembly;
        Insert assemblyWithdiffThrust;
        
        Sub_Components_LLPs__c llpWithThrust = new Sub_Components_LLPs__c(Name='LLPTest', Serial_Number__c='1234'
						, Constituent_Assembly__c=assembly.Id,part_number__c=assembly.Id
						, Life_Limit_Cycles__c=1000, TSN__c=100, CSN__c=100, TSLV__c=0, CSLV__c= 0);
        insert llpWithThrust;
        
        Thrust_Setting__c thrustUsuage = new Thrust_Setting__c(Name = lookup1.Name ,Cycle_Limit__c=2500,Cycle_Used__c=0
							,Life_Limited_Part__c = llpWithThrust.Id,Thrust_Setting_Name__c=lookup1.Id
							,Thrust__c = 2500
							,Current_Thrust__c = true );
       
        Thrust_Setting__c thrustUsuage1 = new Thrust_Setting__c(Name = lookup2.Name ,Cycle_Limit__c=200,Cycle_Used__c=0
                                                                ,Life_Limited_Part__c = llpWithThrust.Id,Thrust_Setting_Name__c=lookup2.Id
                                                                ,Thrust__c = 2500);
        insert thrustUsuage1;
        
        Sub_Components_LLPs__c llpWithoutThrust = new Sub_Components_LLPs__c(Name='LLPTestWithoutThrust', Serial_Number__c='12342'
						, Constituent_Assembly__c=assemblyWithdiffThrust.Id,part_number__c=assemblyWithdiffThrust.Id
						, Life_Limit_Cycles__c=1000, TSN__c=100, CSN__c=100, TSLV__c=0, CSLV__c= 0);
        insert llpWithoutThrust;
        
       
        
        assembly.Current_TS__c = lookup2.Id;
        LeaseWareUtils.clearFromTrigger();
        update assembly;
        
        assemblyWithdiffThrust.Current_TS__c = lookup1.Id;
        LeaseWareUtils.clearFromTrigger();
        try{
            update assemblyWithdiffThrust;
        }catch(Exception e) {
            system.debug('e.getMessage()::::'+e.getMessage());
            Boolean expectedExceptionThrown =  e.getMessage().contains('Some LLPs do not have Limits for newly selected Thrust Setting. Please use LLP Disk sheet component to change the current Thrust Setting ') ? true : false ;
            System.AssertEquals(expectedExceptionThrown, true); 
        }
            
    }   
      
}