/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest(SeeAllData=true)
private class TestURSequence {
    
    static testMethod void myUnitTest() {
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        insert operator;
        // Set up the Lease record
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10', Lease_Start_Date_New__c =Date.parse('01/01/2006') ,Lease_End_Date_New__c=Date.parse('12/31/2016'));
        insert lease;
        
        // Set up the Aircraft record.
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN,Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
                                        Status__c='Available');
        
        LeaseWareUtils.clearFromTrigger();        
        insert a;
        
        
        lease.aircraft__c=a.id;
        
        LeaseWareUtils.clearFromTrigger();        
        update lease;
        
        
        // Set up the Constt Assmbly record.
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= a.Id, Description__c ='test description', TSN__c=0, CSN__c=0,  Type__c ='Engine 1', Serial_Number__c='123');
        insert consttAssembly;
        
        Utilization_Report__c testURForCMR, testURForCMR2;
        
        //Insert, update and delete URs to cover CMR updates
        testURForCMR = new Utilization_Report__c(Name= 'thisURForCMR',Type__c = 'Actual', Aircraft__c=a.Id, FH_String__c='0', Airframe_Cycles_Landing_During_Month__c=0, Month_Ending__c=Date.parse('01/31/2006'), MR_20_Month_C_Check__c=1, Maintenance_Reserve_Engine_1__c=1, Maintenance_Reserve_Engine_2__c=1, Maintenance_Reserve_Engine_3__c=1, Maintenance_Reserve_Engine_4__c=1, Maintenance_Reserve_Engine_1_LLP__c=1, Maintenance_Reserve_Engine_2_LLP__c=2, Maintenance_Reserve_Engine_3_LLP__c=3, Maintenance_Reserve_Engine_4_LLP__c=4, Maintenance_Reserve_APU__c=1, Status__c='Open');
        try{
            
            LeaseWareUtils.clearFromTrigger();			
            insert testURForCMR;
        }catch(exception e){
            // lease.MR_Escalation_Month__c='January'; 
            lease.MR_Escalation__c=1;
            
            LeaseWareUtils.clearFromTrigger();
            update lease;
            
            LeaseWareUtils.clearFromTrigger();	        
            insert testURForCMR;
        }
        
        /* Utilization_Report_List_Item__c testURLI;
testURLI=[select Id, Cycles_During_Month__c, Running_Hours_During_Month__c  from Utilization_Report_List_Item__c where Utilization_Report__c=:testURForCMR.id limit 1];
testURLI.Cycles_During_Month__c=20;
testURLI.Running_Hours_During_Month__c=25;
update testURLI;*/
        
        testURForCMR.Status__c='Approved By Lessor';
        update testURForCMR;
        
        testURForCMR2 = new Utilization_Report__c(Name= 'thisURForCMR2',Type__c = 'Actual', Aircraft__c=a.Id, FH_String__c='0', Airframe_Cycles_Landing_During_Month__c=0, Month_Ending__c=Date.parse('02/28/2006'), MR_20_Month_C_Check__c=1, Maintenance_Reserve_Engine_1__c=1, Maintenance_Reserve_Engine_2__c=1, Maintenance_Reserve_Engine_3__c=1, Maintenance_Reserve_Engine_4__c=1, Maintenance_Reserve_Engine_1_LLP__c=1, Maintenance_Reserve_Engine_2_LLP__c=2, Maintenance_Reserve_Engine_3_LLP__c=3, Maintenance_Reserve_Engine_4_LLP__c=4, Maintenance_Reserve_APU__c=1);
        try{
            insert testURForCMR2;
        }catch(exception e){
            system.debug('Expected, ignoring.. ');
            //lease.MR_Escalation_Month__c='January';
            lease.MR_Escalation__c=10;
            update lease;
            testURForCMR2 = new Utilization_Report__c(Name= 'thisURForCMR2', Type__c = 'Actual',Aircraft__c=a.Id, FH_String__c='0', Airframe_Cycles_Landing_During_Month__c=0, Month_Ending__c=Date.parse('02/28/2006'), MR_20_Month_C_Check__c=1, Maintenance_Reserve_Engine_1__c=1, Maintenance_Reserve_Engine_2__c=1, Maintenance_Reserve_Engine_3__c=1, Maintenance_Reserve_Engine_4__c=1, Maintenance_Reserve_Engine_1_LLP__c=1, Maintenance_Reserve_Engine_2_LLP__c=2, Maintenance_Reserve_Engine_3_LLP__c=3, Maintenance_Reserve_Engine_4_LLP__c=4, Maintenance_Reserve_APU__c=1);
            insert testURForCMR2;
        }
        
        
        /*= new Utilization_Report_List_Item__c(Constituent_Assembly__c = consttAssembly.id, Utilization_Report__c=testURForCMR2.Id, Cycles_During_Month__c=0, Running_Hours_During_Month__c=0, Report_Item__c='Engine 2');
insert testURLI;
delete testURLI;
*/
        consttAssembly.Type__c='Engine 2';
        update consttAssembly;
        /*     try{
testURLI = new Utilization_Report_List_Item__c(Constituent_Assembly__c = consttAssembly.id, Utilization_Report__c=testURForCMR2.Id, Cycles_During_Month__c=0, Running_Hours_During_Month__c=0, Report_Item__c='Engine 2');
insert testURLI;
delete testURLI;
}catch(exception e){
System.debug('Expected exception - ' +e);
}


consttAssembly.Type__c='Engine 3';
update consttAssembly;
testURLI = new Utilization_Report_List_Item__c(Constituent_Assembly__c = consttAssembly.id, Utilization_Report__c=testURForCMR2.Id, Cycles_During_Month__c=0, Running_Hours_During_Month__c=0, Report_Item__c='Engine 3');
insert testURLI;
delete testURLI;

consttAssembly.Type__c='Engine 4';
update consttAssembly;
testURLI = new Utilization_Report_List_Item__c(Constituent_Assembly__c = consttAssembly.id, Utilization_Report__c=testURForCMR2.Id, Cycles_During_Month__c=0, Running_Hours_During_Month__c=0, Report_Item__c='Engine 4');
insert testURLI;
delete testURLI;

consttAssembly.Type__c='APU';
update consttAssembly;
testURLI = new Utilization_Report_List_Item__c(Constituent_Assembly__c = consttAssembly.id, Utilization_Report__c=testURForCMR2.Id, Cycles_During_Month__c=0, Running_Hours_During_Month__c=0, Report_Item__c='APU');
insert testURLI;
delete testURLI;

consttAssembly.Type__c='Landing Gear - Center';
update consttAssembly;
testURLI = new Utilization_Report_List_Item__c(Constituent_Assembly__c = consttAssembly.id, Utilization_Report__c=testURForCMR2.Id, Cycles_During_Month__c=0, Running_Hours_During_Month__c=0, Report_Item__c='Landing Gear - Center');
insert testURLI;
delete testURLI;
*/        
        /*      Utilization_Report_List_Item__c newlyCreatedURLI = [select id from Utilization_Report_List_Item__c where id=:testURLI.id];
delete newlyCreatedURLI;
*/
        
        testURForCMR2.Status__c='Approved By Lessor';
        update testURForCMR2;
        
        
        try{
            Date dateToday = Date.parse('03/31/2008');      
            Utilization_Report__c testUR = new Utilization_Report__c(Name= 'thisUR',Type__c = 'Actual', Aircraft__c=a.Id, FH_String__c='0', Airframe_Cycles_Landing_During_Month__c=0, Month_Ending__c=dateToday, MR_20_Month_C_Check__c=1, Maintenance_Reserve_Engine_1__c=1, Maintenance_Reserve_Engine_2__c=1, Maintenance_Reserve_Engine_3__c=1, Maintenance_Reserve_Engine_4__c=1, Maintenance_Reserve_Engine_1_LLP__c=1, Maintenance_Reserve_Engine_2_LLP__c=2, Maintenance_Reserve_Engine_3_LLP__c=3, Maintenance_Reserve_Engine_4_LLP__c=4, Maintenance_Reserve_APU__c=1);      
            insert testUR;
            
        }catch(System.DmlException e){
            System.debug('System.DmlException expected. Ignore');
        }
        
        
        try{
            delete testURForCMR;
            delete testURForCMR2;
        }catch(System.DmlException e){
            System.debug('Expected. Ignore');
        }
        
    }
    //When assembly MR Info is inactive, the supplemental rent utilization is not created.
    //Once Utilization is created, supplemental rent status is not considered for supplemental rent utilization
    static testMethod void ActiveAsmblyMRInfo() {
        system.debug('==Case-Active==');
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN3';
        Aircraft__c AC1 = [select id,lease__c,lease__r.Lease_Start_Date_new__c 
                           ,(select id from Constituent_Assemblies__r where recordType.Name ='Airframe')
                           ,(select id from Leases__r)
                           from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        System.debug('1.Number of Queries : ' + Limits.getQueries());
        
        
        Date ForMonthEnding = AC1.lease__r.Lease_Start_Date_new__c.toStartOfMonth().addMonths(1).addDays(-1); // Lease start Date date.newinstance(2013, 8, 17)
        
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
		    Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Heavy 1'
        );
        insert mpe;
        
        //Assembly_Event_Info__c AEI = new Assembly_Event_Info__c ( Name='Heavy 1',Event_Cost__c = 2000,Constituent_Assembly__c=AC1.Constituent_Assemblies__r[0].Id, Maintenance_Program_Event__c=mpe.id);
        //insert AEI;
        
        // set AsmblyMRRate.Inactive__c=True
        Assembly_MR_Rate__c AsmblyMRRate=[select id,name,Inactive__c,Assembly_Lkp__c,Assembly_Event_Info__c from Assembly_MR_Rate__c where lease__c=:AC1.lease__c limit 1];
        system.debug('=AsmblyMRRate='+JSON.serializePretty(AsmblyMRRate));
        
        // Actual UR
        Utilization_Report__c URList = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
            Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
            Status__c='Open'
            ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
            ,Maintenance_Reserve_Engine_1__c = 20000
            ,Airframe_Flight_Hours_Month__c = 150
        );
        
        
        LeaseWareUtils.clearFromTrigger();
        insert URList;
        system.debug('=URList='+JSON.serializePretty(URList));
        system.debug('=URList.Id='+URList.Id+'=Assembly ID='+AsmblyMRRate.Assembly_Lkp__c +'=Projected Event id='+AsmblyMRRate.Assembly_Event_Info__c );
        Utilization_Report_List_Item__c[] testURLI1=[select id,name,Utilization_Report__c,Constituent_Assembly__c,Assembly_MR_Info__c, Total_Maintenance_Reserve__c from Utilization_Report_List_Item__c where Utilization_Report__c =:URList.Id and Constituent_Assembly__c=:AsmblyMRRate.Assembly_Lkp__c and Assembly_MR_Info__c=:AsmblyMRRate.id limit 1];
        system.debug('=size testURLI1='+testURLI1.size());
        system.debug('=testURLI1='+JSON.serializePretty(testURLI1));

        Decimal oldMRValue = testURLI1[0].Total_Maintenance_Reserve__c;
        
        AsmblyMRRate.Inactive__c=True;
        
        LeaseWareUtils.clearFromTrigger();
        update AsmblyMRRate;
        system.debug('=AsmblyMRRate='+JSON.serializePretty(AsmblyMRRate));
        
        
        //LeaseWareUtils.clearFromTrigger();
        Update testURLI1;
        testURLI1=[select id,name,Utilization_Report__c,Constituent_Assembly__c,Assembly_MR_Info__c,Total_Maintenance_Reserve__c  from Utilization_Report_List_Item__c where Utilization_Report__c =:URList.Id and Constituent_Assembly__c=:AsmblyMRRate.Assembly_Lkp__c  and Assembly_MR_Info__c=:AsmblyMRRate.id limit 1];
        system.debug('=testURLI1='+JSON.serializePretty(testURLI1));  
        
        //Expected behavior: The MR total of the URLI should not change though the supplemental rent is inactive
        System.assertEquals(oldMRValue, testURLI1[0].Total_Maintenance_Reserve__c);
    }
    
    //Test the scenario - Do not create SRU for inactive supplemental rent
    static testMethod void InActiveAsmblyMRInfo() {
        system.debug('==Case- InActive==');
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN3';
        Aircraft__c AC1 = [select id,lease__c,lease__r.Lease_Start_Date_new__c 
                           ,(select id from Constituent_Assemblies__r where recordType.Name ='Airframe')
                           ,(select id from Leases__r)
                           from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        System.debug('1.Number of Queries : ' + Limits.getQueries());
        
        
        Date ForMonthEnding = AC1.lease__r.Lease_Start_Date_new__c.toStartOfMonth().addMonths(1).addDays(-1); // Lease start Date date.newinstance(2013, 8, 17)
        
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
			Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Heavy 1'
        );
        insert mpe;
        
        //Assembly_Event_Info__c AEI = new Assembly_Event_Info__c ( Name='Heavy 1',Event_Cost__c = 2000,Constituent_Assembly__c=AC1.Constituent_Assemblies__r[0].Id, Maintenance_Program_Event__c=mpe.id);
        //insert AEI;
        
        // set AsmblyMRRate.Inactive__c=True
        Assembly_MR_Rate__c AsmblyMRRate=[select id,name,Inactive__c,Assembly_Lkp__c,Assembly_Event_Info__c from Assembly_MR_Rate__c where lease__c=:AC1.lease__c limit 1];
        AsmblyMRRate.Inactive__c=True;
        
        
        LeaseWareUtils.clearFromTrigger();
        update AsmblyMRRate;
        system.debug('=AsmblyMRRate='+JSON.serializePretty(AsmblyMRRate));
        
        // Actual UR
        Utilization_Report__c URList = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
            Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
            Status__c='Open'
            ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
            ,Maintenance_Reserve_Engine_1__c = 20000
            ,Airframe_Flight_Hours_Month__c = 150
        );
        
        
        LeaseWareUtils.clearFromTrigger();
        insert URList;
        system.debug('=URList='+JSON.serializePretty(URList));
        
        //There should be no URLI created for the supplemental rent
        Utilization_Report_List_Item__c[] testURLI1=[select id,name,Utilization_Report__c,Constituent_Assembly__c,Assembly_MR_Info__c,Total_Maintenance_Reserve__c  from Utilization_Report_List_Item__c where Utilization_Report__c =:URList.Id and Constituent_Assembly__c=:AsmblyMRRate.Assembly_Lkp__c  and Assembly_MR_Info__c=:AsmblyMRRate.id limit 1];
        system.debug('=size testURLI1='+testURLI1.size());
        System.assertEquals(testURLI1.size(), 0);
        
    }
    @isTest(seeAllData=true)
    static  void UtilizationDeletionForCreditedInvoice() {
          Lease__c lease = [select id,Name,Lease_Start_Date_New__c,Lease_End_Date_New__c,Aircraft__c,(Select MR_Rate_End_Date__c,MR_Rate_Start_Date__c from Assembly_MR_Rates__r)  from lease__c where name like '%TestSeededMSN3%' limit 1];
        
        lease.Lease_Start_Date_New__c=date.valueOf('2019-01-01');
        lease.Lease_End_Date_New__c=date.valueOf('2022-12-31');
        update lease;
        for(Assembly_MR_Rate__c rec:lease.Assembly_MR_Rates__r)  
            {
                rec.MR_Rate_Start_Date__c =lease.Lease_Start_Date_New__c;
                rec.MR_Rate_End_Date__c=lease.Lease_End_Date_New__c;
            }
			update lease.Assembly_MR_Rates__r;
        Test.startTest();
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            FH_String__c ='100',
            Airframe_Cycles_Landing_During_Month__c= 50.0,
            Aircraft__c = lease.Aircraft__c,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'Actual',
            Month_Ending__c = Date.newInstance(2019,01,31),
            Status__c='Approved By Lessor'
        );
        insert utilizationRecord;
        Test.stopTest();
        
        system.debug('Utilization Record Id----'+utilizationRecord);
        
        Invoice__c invRecord = new Invoice__c(
            name='system generated'
            ,Utilization_Report__c = utilizationRecord.Id
            ,Invoice_Type__c = 'Aircraft MR'
            ,Status__c='Declined'
            ,Lease__c = lease.id   
        );
        insert invRecord;
        system.debug('Invoice-------------'+ invRecord);
        LeaseWareUtils.unsetTriggers('InvoiceHandlerBefore');
        LeaseWareUtils.unsetTriggers('InvoiceHandlerAfter');
       
        try{
            
            LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
            LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
            boolean isAllowed = LeaseWareUtils.isAllowedToDeleteUnApproveUtil();
            system.debug('Delete Utilization');
            delete utilizationRecord ;
            if(isAllowed){
            	List<Utilization_Report__c> util1 = [select Id from Utilization_Report__c where Id=:utilizationRecord.Id];
            	system.assertEquals(0, util1.size());
            }
            
        }catch(Exception ex){
            String unexpectedMessage = ex.getMessage();
            system.debug('unexpectedMessage---'+unexpectedMessage);
            System.assert(unexpectedMessage.contains('You do not have sufficient privileges to delete the Utilizations for the Invoice whose status is Credited or Declined'));
        }
        
    }
    
    
    
    
}