/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAircraftAnalytics {

    static testMethod void AircraftAnalyticsCRUDTest() {
        // TO DO: implement unit test
        Aircraft_Data_Mining__c AA1= new Aircraft_Data_Mining__c(
        	name='Dummy'
        	,Aircraft_In_Order__c=true
        	,Aircraft_In_Service__c=true
        	,Aircraft_In_Storage__c =true
        	,Aircraft_Manufacturer__c = 'Airbus'
        	,Aircraft_Type__c = 'A320'
        	,Usage_Other__c = true
        	,Engine_Type__c = 'V2500'
        	,From_Year_of_manufacture__c = '1980'
        	,Usage_Passenger__c = true
        	,To_Year_of_manufacture__c = '2222'
        	//,Aircraft_Variant__c = ''
        	
        );
        insert AA1;
        
       	LeaseWareUtils.unsetFromTrigger();
	    LeaseWareUtils.clearFromTrigger();
	    AA1.Aircraft_Variant__c ='210 (CFM)';
	    update AA1;

       	LeaseWareUtils.unsetFromTrigger();
	    LeaseWareUtils.clearFromTrigger();
	    delete 	AA1;
	    undelete AA1;    
        
    }
}