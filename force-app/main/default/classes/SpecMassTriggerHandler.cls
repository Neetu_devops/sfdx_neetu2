public class SpecMassTriggerHandler implements ITrigger{
    /* This is trigger handler class for Adjusted Interval (Ratio) object.*/
    
    
    //Contructor
    private final string triggerBefore = 'SpecMassTriggerHandlerBefore';
    private final string triggerAfter = 'SpecMassTriggerHandlerAfter';

    public void bulkBefore(){}
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){
              
        system.debug('SpecMassTriggerHandler.beforeInsert(+)');
        
         //UpdateWeightonAsset();
        system.debug('SpecMassTriggerHandler.beforeInsert(-)'); 
    }
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){
         
    }
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){}
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        system.debug('SpecMassTriggerHandler.afterInsert(+)');        
        UpdateWeightonAsset();
        system.debug('SpecMassTriggerHandler.afterInsert(-)'); 
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        system.debug('SpecMassTriggerHandler.afterUpdate(+)');        
        UpdateWeightonAsset();
        system.debug('SpecMassTriggerHandler.afterUpdate(-)'); 
    }
    
    public void afterDelete(){}
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
   public  void afterUnDelete(){}   
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
  
    public void andFinally(){}
    
    /* Function to update MTOW ( Delivered,Operational) on Asset record based on child record values
       Called in After Insert/Update */
        
    private void UpdateWeightonAsset(){
       Spec_Mass_and_Balance__c[] SpecMasstList = (list<Spec_Mass_and_Balance__c>)trigger.new ;
           
      set<Id> AssetIdSet = new set<Id>(); // this is for Asset id set 
	  
      for(Spec_Mass_and_Balance__c curRec: SpecMasstList)  {
	  
        if( Trigger.IsInsert || (Trigger.IsUpdate && ((Spec_Mass_and_Balance__c)trigger.oldMap.get(curRec.Id)).Weight__c!= curRec.Weight__c )
		&& (curRec.Weight_Type__c=='MTOW (Maximum Takeoff Weight)' || curRec.Weight_Type__c=='MLW (Maximum Landing Weight)')
		&&( curRec.Period__c=='Current' || curRec.Period__c=='Last Delivered' ) ){
		      AssetIdSet.add(curRec.Asset__c);
			}  
        }  
      list<Aircraft__c> listAsset=[select id,MTOW_Leased__c,Maximum_Operational_Take_Lbs__c,(select id,Weight__c,Weight_Type__c,Period__c,Unit__c,Weight_Alt__c
                      from Spec_Masses_and_Balances__r where (Weight_Type__c ='MTOW (Maximum Takeoff Weight)' OR Weight_Type__c='MLW (Maximum Landing Weight)')
					  AND (Period__c ='Current' OR Period__c='Last Delivered') AND ID in:SpecMasstList )from Aircraft__c where id in : AssetIdSet];
      for(Aircraft__c CurAsset : listAsset)    {
            System.Debug('Asset Id #' +CurAsset.id); 
               
            for(Spec_Mass_and_Balance__c curchild:CurAsset.Spec_Masses_and_Balances__r ){
                System.debug('Period'+ curchild.Period__c);
                if(curchild.Weight_Type__c == 'MTOW (Maximum Takeoff Weight)'&& curchild.Period__c =='Current'){
                     if(curchild.Unit__c=='lbs')
                      CurAsset.Maximum_Operational_Take_Lbs__c= curchild.Weight__c;
                     else
                      CurAsset.Maximum_Operational_Take_Lbs__c= curchild.Weight_Alt__c;     
                    }
                else if (curchild.Weight_Type__c == 'MTOW (Maximum Takeoff Weight)' && curchild.Period__c =='Last Delivered'){
                     if( curchild.Unit__c=='lbs')                        
                       CurAsset.MTOW_Leased__c = curchild.Weight__c;
                     else
                       CurAsset.MTOW_Leased__c = curchild.Weight_Alt__c;
                    }                       
                else if(curchild.Weight_Type__c == 'MLW (Maximum Landing Weight)' && curchild.Period__c =='Current'){
					 if(curchild.Unit__c=='lbs')
					   CurAsset.Maximum_Landing_Weight_Operational__c= curchild.Weight__c;
			         else
					  CurAsset.Maximum_Landing_Weight_Operational__c= curchild.Weight_Alt__c;                
					}                                                                   
            }
    
      }
        
        LeaseWareUtils.TriggerDisabledFlag = true;
              update listAsset;
        LeaseWareUtils.TriggerDisabledFlag = false;       

   }
}