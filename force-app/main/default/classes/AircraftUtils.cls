public without sharing class AircraftUtils {
    
    /* Algo for Aircraft recommendation
     * 
     * Input : Operator Id 
     * Output: List of Aircraft (MSN)
     * 
     * 1) Get the weightage from Recommendation Factor object for type Aircraft
     * 2) Get operator details AircraftType, AircraftVariant, EngineType, Vintage, MTOW from World fleet 
     * 3) Get list of Asset from Asset object whose lease is getting over in specified time period
     * 4) For each asset compare with all the operator data and calculate the weight
     * 5) In above step the operator will be duplicated since 
     * one operator can have many aircraft of same type but aircraft vintage or mtow may differ. 
     * 6) Rank the Asset based on the weight 
     * 7) Return top 25 or requested number of records
     * 
    */
    
    public static integer typeWeight = 75;
    public static integer variantWeight = 0;
    public static integer engineWeight = 0;
    public static integer vintageWeight = 0;
    public static integer mtowWeight = 0;
    public static integer vintageRange = 0;
    public static integer mtowRange = 0;
    public static integer numOfRecords = 25;
   // public static Integer leaseLimit = 0;
    public static Integer existingCustomerWeight = 0;
    public static Set<String> existingOperatorList = new Set<String>();
    
    
    public static String AircraftType;
    public static String AircraftVariant;
    public static String EngineType;
    public static Integer Vintage;
    public static Integer vintageFilterRange;
    public static Integer MTOWLeased;
    public static Integer mtowFilterRange;
    
    public static List<AircraftWrapper.Properties> getAircraft(String operatorId, String aircraftType1, String variant, 
                                                                    String engine, integer vintage1, 
                                                                    integer vintageRange, integer mtow, integer mtowRange){
        
        //operatorId = 'a2w6F000001V25QQAS';
        //leaseLimit = Integer.valueOf(leaseExpiry);    
        
        system.debug('getAircraft operatorId '+operatorId);
		system.debug('getAircraft '+aircraftType1 + ', '+variant+ ', '+engine+ ', '
					+vintage1+ ', '+vintageRange + ', '+mtow+ ', '+mtowRange);
		AircraftType = aircraftType1;
		AircraftVariant = variant;
		EngineType = engine;
        Vintage = vintage1;
        vintageFilterRange = vintageRange;
        MTOWLeased = mtow;
        mtowFilterRange = mtowRange;
                                                                        
        getWeightage();
		getExistingCustomer();
		if(AircraftType == null) {
        	List<Global_Fleet__c> operatorAircraftList = getOperatorData(operatorId);
			return populateWeightage(operatorId, operatorAircraftList);
		}
		else
        	return populateWeightageBasedOnFilter(operatorId);		
    }
    
    public static void getWeightage(){
        List<Recommendation_Factor__c> recommendationList = [
            select Type__c, Factor__c, Weightage_Number__c, range__c from Recommendation_Factor__c 
            where Type__c = 'Aircraft' and Inactive__c = false
        ];
        system.debug('getWeightage recommendationList '+recommendationList);
        if(recommendationList != null && recommendationList.size() > 0 ){ 
            for(Recommendation_Factor__c recommendation: recommendationList) {
                if(recommendation.Factor__c.equalsIgnoreCase('Aircraft Type') && recommendation.Weightage_Number__c != null)
                    typeWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                if(recommendation.Factor__c.equalsIgnoreCase('Aircraft Variant') && recommendation.Weightage_Number__c != null)
                    variantWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                if(recommendation.Factor__c.equalsIgnoreCase('Engine') && recommendation.Weightage_Number__c != null)
                    engineWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                if(recommendation.Factor__c.equalsIgnoreCase('Vintage') && recommendation.Weightage_Number__c != null) {
                    vintageWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                    if(recommendation.range__c != null)
                        vintageRange = Integer.valueOf(recommendation.range__c);
                }
                if(recommendation.Factor__c.equalsIgnoreCase('MTOW') && recommendation.Weightage_Number__c != null) {
                    mtowWeight = Integer.valueOf(recommendation.Weightage_Number__c); 
                    if(recommendation.range__c != null)
                        mtowRange = Integer.valueOf(recommendation.range__c);
                }
                if(recommendation.Factor__c.equalsIgnoreCase('Existing Customer') && recommendation.Weightage_Number__c != null)
                    existingCustomerWeight = Integer.valueOf(recommendation.Weightage_Number__c);
                
//                system.debug('getWeightage '+recommendation);
            }
        }
    }
    
    public static List<Global_Fleet__c> getOperatorData(String operatorId){

		String queryStr = 'select AircraftType__c, AircraftVariant__c, EngineType__c, BuildYear__c, MTOW__c, Operator__c, Operator_ID__c '+
            				+' from Global_Fleet__c where '+
            				+' Aircraft_Operator__r.id = :operatorId ';
       /* if(AircraftType != null)
            queryStr = queryStr + ' and AircraftType__c =: AircraftType ';
        if(EngineType != null)
            queryStr = queryStr + ' and EngineType__c =: EngineType ';
        if(AircraftVariant != null)
            queryStr = queryStr + ' and AircraftVariant__c =: AircraftVariant ';
        */
        try{
        	List<Global_Fleet__c> operatorAircraftList = database.query(queryStr);
      		
            system.debug('getOperatorData queryStr ' +queryStr);
            system.debug('getOperatorData operatorAircraftList ' +operatorAircraftList);    
            system.debug('getOperatorData operatorAircraftList size ' +operatorAircraftList.size());    
            return operatorAircraftList;
        }
        catch(Exception e) {
            system.debug('getOperatorData: Operators not found for the given criteria: Exception '+e);
        }
        return null;
      
    }
    
    public static void getExistingCustomer(){
        List<Lease__c> existingOperator = [
            select Operator__r.Name from Lease__c
            where 
            Active__c = true
        ];
        if(existingOperator == null) return;
        for(Lease__c lease: existingOperator ) {
            if(lease.Operator__c != null)
        		existingOperatorList.add(lease.Operator__r.Name);
        }
        system.debug('getExistingCustomer list '+existingOperatorList);
    }
    
    public static List<AircraftWrapper.Properties> populateWeightageBasedOnFilter(String operatorId){
        system.debug('populateWeightageBasedOnFilter ');
        /*leaseLimit = 365;
        Date endDate = Date.today().addDays(Integer.valueOf(leaseLimit));
        Date todayDate = Date.today();*/
        List<Aircraft__c> aircraftList = [
            select Id, Aircraft_Type__c, Aircraft_Variant__c, Engine_Type__c, 
            Vintage__c, MTOW_Leased__c, MSN_Number__c, Date_of_Manufacture__c 
            from Aircraft__c where 
            Lease__r.Operator__c = null and 
            Contractual_Status__c IN ('Owned', 'Managed', 'On Order', 'New', 'Operational', 'Operational 1', 'Operational 2') and 
            Contractual_Status__c NOT IN ('Inoperative 1', 'Inoperative 2')
        ];
        
        List<Global_Fleet__c> fleet = [select Operator__c, Operator_ID__c 
            				 from Global_Fleet__c where 
            				 Aircraft_Operator__r.id = :operatorId Limit 1];
        String operatorName = null;
        if(fleet.size() > 0)
            operatorName = fleet[0].Operator__c;
        
        //where Lease__r.Lease_End_Date__c < :endDate and  Lease__r.Lease_End_Date__c > :todayDate
            
        //system.debug('populateWeightage list: '+aircraftList);
        //system.debug('populateWeightage list size : '+aircraftList.size());
        
        List<AircraftWrapper.Properties> aircraftWrapperList = new List<AircraftWrapper.Properties>();
        Map<String,AircraftWrapper.AircraftWeightage> aircraftWeightageList = new Map<String,AircraftWrapper.AircraftWeightage>();
        
        for(Aircraft__c aircraft: aircraftList) {
            integer weight = 0; 
            boolean isExistingCustomer = false;
            
            try {
                if(AircraftType != null && aircraft.Aircraft_Type__c != null && 
                   AircraftType.equalsIgnoreCase(aircraft.Aircraft_Type__c)) {
                       weight = weight + typeWeight;
                       if(AircraftVariant != null && aircraft.Aircraft_Variant__c != null && 
                          AircraftVariant.equalsIgnoreCase(aircraft.Aircraft_Variant__c)) {
                              weight = weight + variantWeight;
                          }
                       if(EngineType != null && aircraft.Engine_Type__c != null && 
                          EngineType.equalsIgnoreCase(aircraft.Engine_Type__c)) {
                              weight = weight + engineWeight;
                          }
                       
                       if(Vintage > 0 && aircraft.Vintage__c != null){
                           String[] buildYearStr = aircraft.Vintage__c.split('-');
                           // system.debug('populateWeightage '+aircraft.Vintage__c + ' buildYearStr '+buildYearStr);
                           integer buildYear = 0 ;
                           try{
                               if(buildYearStr != null && buildYearStr.size() > 0)
                                   buildYear = Integer.valueOf(buildYearStr[0]);
                               if((buildYear > (Vintage - vintageFilterRange))  && 
                                  (buildYear < (Vintage + vintageFilterRange)))
                                   weight = weight + vintageWeight;
                           }
                           catch(Exception e){
                               
                           }
                       }
                       if(MTOWLeased > 0 && (aircraft.MTOW_Leased__c != null && MTOWLeased != null) && 
                          (aircraft.MTOW_Leased__c > (MTOWLeased - mtowFilterRange)) && 
                          (aircraft.MTOW_Leased__c < (MTOWLeased + mtowFilterRange)))
                       		weight = weight + mtowWeight;
                       
                       if(existingOperatorList != null && operatorId != null && 
                          existingOperatorList.contains(operatorId)) {
                              weight = weight + existingCustomerWeight;
                              isExistingCustomer = true;
                       }
                   }   
            }
            catch(Exception e){ system.debug('Exception populateWeightage: '+e);}
            
            if(weight > 0) {
                AircraftWrapper.Properties wrapper = new AircraftWrapper.Properties();
                wrapper.fleetId = aircraft.Id;
                wrapper.operatorId = operatorId;
                wrapper.aircraft = aircraft.MSN_Number__c;
                wrapper.operatorName = operatorName;
                wrapper.weight = weight;
                
                if(vintage > 0){
                    wrapper.vintageMinRange = Vintage - vintageFilterRange;
                    wrapper.vintageMaxRange = Vintage + vintageFilterRange;
                }
                else {
                    wrapper.vintageMinRange = 0;
                    wrapper.vintageMaxRange = 0;
                }
                wrapper.isExistingCustomer = isExistingCustomer;
                wrapper.aircraftType = aircraft.Aircraft_Type__c;
                if(aircraft.Date_of_Manufacture__c != null )
                    wrapper.aircraftDom = aircraft.Date_of_Manufacture__c.year();
                wrapper.aircraftVariant = aircraft.Aircraft_Variant__c;
                wrapper.aircraftEngine = aircraft.Engine_Type__c;
                wrapper.aircraftVintage = aircraft.Vintage__c;
                wrapper.aircraftMtow = aircraft.MTOW_Leased__c;
                wrapper.operatorName = operatorName; 
                aircraftWrapperList.add(wrapper);
            }
           // system.debug('populateWeightage '+ aircraft +' '+weight );
        }
        
        
        if(aircraftWrapperList != null && aircraftWrapperList.size() > 0 ){
            aircraftWrapperList.sort();
            
            //system.debug('populateWeightage ----------------------------- ');
            integer previousWeight = 0;
            integer rank = 1;
            for (AircraftWrapper.Properties wrapper : aircraftWrapperList)  {
                if(previousWeight == 0){
                    previousWeight = wrapper.weight;
                    wrapper.rank = rank;
                }
                else if(previousWeight == wrapper.weight)
                    wrapper.rank = rank;
                else{
                    rank++;
                    wrapper.rank = rank;
                    previousWeight = wrapper.weight;
                }
            //    system.debug('populateWeightage ' +wrapper);    
            }
            
            //system.debug('populateWeightage '+ aircraftWrapperList.size());
            List<AircraftWrapper.Properties> aircraftWrapperListLimited = new List<AircraftWrapper.Properties>();
            for(integer i = 0; i < aircraftWrapperList.size() && i < numOfRecords; i++)
                aircraftWrapperListLimited.add(aircraftWrapperList.get(i));
            //system.debug('populateWeightage aircraftWrapperListLimited '+ aircraftWrapperListLimited.size());
            //
            //aircraftWrapperListLimited = populateAxis(operatorId, operatorAircraftList, aircraftWrapperListLimited);
            return aircraftWrapperListLimited;
        }
        else 
            system.debug('populateWeightage no item ');
        return null;
    } 
    
    
    public static List<AircraftWrapper.Properties> populateWeightage(String operatorId, List<Global_Fleet__c> operatorAircraftList){
        system.debug('populateWeightage ');
        /*leaseLimit = 365;
        Date endDate = Date.today().addDays(Integer.valueOf(leaseLimit));
        Date todayDate = Date.today();*/
        List<Aircraft__c> aircraftList = [
            select Id, Aircraft_Type__c, Aircraft_Variant__c, Engine_Type__c, 
            Vintage__c, MTOW_Leased__c, MSN_Number__c, Date_of_Manufacture__c 
            from Aircraft__c where
            Lease__r.Operator__c = null and 
            Contractual_Status__c IN ('Owned', 'Managed', 'On Order', 'New', 'Operational', 'Operational 1', 'Operational 2') and 
            Contractual_Status__c NOT IN ('Inoperative 1', 'Inoperative 2')
        ];
        //where Lease__r.Lease_End_Date__c < :endDate and  Lease__r.Lease_End_Date__c > :todayDate
            
        //system.debug('populateWeightage list: '+aircraftList);
        //system.debug('populateWeightage list size : '+aircraftList.size());
        
        List<AircraftWrapper.Properties> aircraftWrapperList = new List<AircraftWrapper.Properties>();
        Map<String,AircraftWrapper.AircraftWeightage> aircraftWeightageList = new Map<String,AircraftWrapper.AircraftWeightage>();
        String operatorName;
        for(Aircraft__c aircraft: aircraftList) {
            integer weight = 0; 
            //system.debug('populateWeightage for aircraft '+aircraft);
            boolean isVariantMatched = false;
            boolean isEngineMatched = false;
        	boolean isExistingCustomer = false;
            integer minVintage = 0;
            integer maxVintage = 0;
            for(Global_Fleet__c operatorAircraft : operatorAircraftList) {
                isVariantMatched = false;
            	isExistingCustomer = false;
                isEngineMatched = false;
                try{
                    operatorName = operatorAircraft.Operator__c;
                    if(operatorAircraft.AircraftType__c != null && aircraft.Aircraft_Type__c != null && 
                       operatorAircraft.AircraftType__c.equalsIgnoreCase(aircraft.Aircraft_Type__c)) {
                           weight = weight + typeWeight;
                           if(operatorAircraft.AircraftVariant__c != null && aircraft.Aircraft_Variant__c != null && 
                              operatorAircraft.AircraftVariant__c.equalsIgnoreCase(aircraft.Aircraft_Variant__c)) {
                                  weight = weight + variantWeight;
                                  isVariantMatched = true;
                              }
                           if(operatorAircraft.EngineType__c != null && aircraft.Engine_Type__c != null && 
                              operatorAircraft.EngineType__c.equalsIgnoreCase(aircraft.Engine_Type__c)) {
                                  weight = weight + engineWeight;
                                  isEngineMatched = true;
                              }
                           integer operatorBuildYr = 0;
                           if(operatorAircraft.BuildYear__c != null)
                               operatorBuildYr = Integer.valueOf(operatorAircraft.BuildYear__c);
                           
                           if(minVintage == 0 || minVintage > operatorBuildYr)
                               minVintage = operatorBuildYr;
                           if(maxVintage < operatorBuildYr)
                               maxVintage = operatorBuildYr;
                           
                           if(aircraft.Vintage__c != null){
                               String[] buildYearStr = aircraft.Vintage__c.split('-');
                               // system.debug('populateWeightage '+aircraft.Vintage__c + ' buildYearStr '+buildYearStr);
                               integer buildYear = 0 ;
                               try{
                                   if(buildYearStr != null && buildYearStr.size() > 0)
                                       buildYear = Integer.valueOf(buildYearStr[0]);
                                   if((buildYear > (operatorBuildYr - vintageRange))  && 
                                      (buildYear < (operatorBuildYr + vintageRange)))
                                       weight = weight + vintageWeight;
                               }
                               catch(Exception e){
                                   
                               }
                           }
                           if((aircraft.MTOW_Leased__c != null && operatorAircraft.MTOW__c != null) && 
                              (aircraft.MTOW_Leased__c > (operatorAircraft.MTOW__c - mtowRange)) && 
                              (aircraft.MTOW_Leased__c < (operatorAircraft.MTOW__c + mtowRange)))
                               weight = weight + mtowWeight;

                           
                           if(existingOperatorList != null && operatorAircraft.Operator__c != null && 
                              existingOperatorList.contains(operatorAircraft.Operator__c))
                               weight = weight + existingCustomerWeight;
                           
                           AircraftWrapper.AircraftWeightage aircraftWrapper;
                           if(aircraftWeightageList.get(aircraft.Id) == null) {
                               aircraftWrapper = new AircraftWrapper.AircraftWeightage();
                               aircraftWrapper.weight = weight;
                               aircraftWrapper.aircraftTypeCount = 1;
                               //aircraftWrapper.vintageMinRange = (operatorBuildYr - vintageRange);
                               //aircraftWrapper.vintageMaxRange = (operatorBuildYr + vintageRange);
                               aircraftWrapper.aircraftVariantCount = 0;
                               aircraftWrapper.aircraftEngineCount = 0;
                               if(isVariantMatched)
                                   aircraftWrapper.aircraftVariantCount = 1;
                               aircraftWrapper.isExistingCustomer = isExistingCustomer;
                           }
                           else{
                               aircraftWrapper = aircraftWeightageList.get(aircraft.Id);
                               aircraftWrapper.weight = aircraftWrapper.weight + weight;
                               aircraftWrapper.aircraftTypeCount = aircraftWrapper.aircraftTypeCount + 1;
                               aircraftWrapper.isExistingCustomer = isExistingCustomer;
                               if(isVariantMatched)
                                   aircraftWrapper.aircraftVariantCount = aircraftWrapper.aircraftVariantCount + 1;
                               if(isEngineMatched)
                                   aircraftWrapper.aircraftEngineCount = aircraftWrapper.aircraftEngineCount + 1;
                           }
                           aircraftWrapper.vintageMinRange = minVintage;
                           aircraftWrapper.vintageMaxRange = maxVintage;
                           //system.debug('minVintage:' + minVintage +' maxVintage:'+maxVintage+ ' operatorBuildYr:'+operatorBuildYr);
                           //system.debug('isVariantMatched '+isVariantMatched + ' id:'+fleet.Operator_ID__c + ' vintageRange '+operatorBuildYr + ' vintageRange '+vintageRange);
                           aircraftWeightageList.put(aircraft.Id, aircraftWrapper);
                       }
                }
                catch(Exception e){ system.debug('Exception populateWeightage: '+e);}
            }
            if(weight > 0) {
                AircraftWrapper.Properties wrapper = new AircraftWrapper.Properties();
                wrapper.fleetId = aircraft.Id;
                wrapper.operatorId = operatorId;
                wrapper.aircraft = aircraft.MSN_Number__c;
                wrapper.operatorName = operatorName;
                //wrapper.weight = weight;
                
                AircraftWrapper.AircraftWeightage aircraftWeightage = aircraftWeightageList.get(aircraft.Id);
                wrapper.weight = aircraftWeightage.weight;
                wrapper.aircraftTypeCount = aircraftWeightage.aircraftTypeCount;
                wrapper.aircraftVariantCount = aircraftWeightage.aircraftVariantCount;
                //wrapper.vintageMinRange = aircraftWeightage.vintageMinRange;
                //wrapper.vintageMaxRange = aircraftWeightage.vintageMaxRange;
                if(vintage > 0){
                    wrapper.vintageMinRange = Vintage - vintageFilterRange;
                    wrapper.vintageMaxRange = Vintage + vintageFilterRange;
                }
                else {
                    wrapper.vintageMinRange = 0;
                    wrapper.vintageMaxRange = 0;
                }
                wrapper.isExistingCustomer = aircraftWeightage.isExistingCustomer;
                wrapper.aircraftEngineCount = aircraftWeightage.aircraftEngineCount;
                wrapper.aircraftType = aircraft.Aircraft_Type__c;
                if(aircraft.Date_of_Manufacture__c != null )
                    wrapper.aircraftDom = aircraft.Date_of_Manufacture__c.year();
                wrapper.aircraftVariant = aircraft.Aircraft_Variant__c;
                wrapper.aircraftEngine = aircraft.Engine_Type__c;
                wrapper.aircraftVintage = aircraft.Vintage__c;
                wrapper.aircraftMtow = aircraft.MTOW_Leased__c;
                
                aircraftWrapperList.add(wrapper);
            }
           // system.debug('populateWeightage '+ aircraft +' '+weight );
        }
        
        
        if(aircraftWrapperList != null && aircraftWrapperList.size() > 0 ){
            aircraftWrapperList.sort();
            
            //system.debug('populateWeightage ----------------------------- ');
            integer previousWeight = 0;
            integer rank = 1;
            for (AircraftWrapper.Properties wrapper : aircraftWrapperList)  {
                if(previousWeight == 0){
                    previousWeight = wrapper.weight;
                    wrapper.rank = rank;
                }
                else if(previousWeight == wrapper.weight)
                    wrapper.rank = rank;
                else{
                    rank++;
                    wrapper.rank = rank;
                    previousWeight = wrapper.weight;
                }
            //    system.debug('populateWeightage ' +wrapper);    
            }
            
            //system.debug('populateWeightage '+ aircraftWrapperList.size());
            List<AircraftWrapper.Properties> aircraftWrapperListLimited = new List<AircraftWrapper.Properties>();
            for(integer i = 0; i < aircraftWrapperList.size() && i < numOfRecords; i++)
                aircraftWrapperListLimited.add(aircraftWrapperList.get(i));
            //system.debug('populateWeightage aircraftWrapperListLimited '+ aircraftWrapperListLimited.size());
            //
            //aircraftWrapperListLimited = populateAxis(operatorId, operatorAircraftList, aircraftWrapperListLimited);
            return aircraftWrapperListLimited;
        }
        else 
            system.debug('populateWeightage no item ');
        return null;
    } 
    
}