public class Asset_URProcessController {
    @AuraEnabled
	public static String processUtilization()
    {
        String result = 'Processing of the utilization is complete.';
        System.debug('In Asset_URProcessController : processUtilization');
        System.debug('Call GenerateUtilizationsFromURStaging batch - begin');
        try {
       		 Database.executeBatch(new GenerateUtilizationsFromURStaging(), 1);
        }
        catch(Exception e) {
            System.debug('error in batch  '+ e);
            result = 'Error in processing the utilizations';
        }
        System.debug('Call GenerateUtilizationsFromURStaging batch - end');
	    return result;
    }
}