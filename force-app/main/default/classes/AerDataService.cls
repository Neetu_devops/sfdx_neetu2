//LastModified Date : 2017.11.12 
global class AerDataService
{
	public static list<String> AllJSON {get;set;}
	private static list<MajorComponent> majorComponentItems {get;set;}
    private static list<AssetItem> assetItems {get;set;}
    private static list<AssetContractResource> listAssetContracts {get;set;} 
    private static list<LeaseItem> leaseItems  {get;set;}
    private static list<AssemblyItem> assemblyItems  {get;set;}
    private static string response_body        {get;set;}
    private static string customerSlug {get;set;}
    private static string token        {get;set;}
    private static string assetFilter {get;set;}
    private static string endPointUrl  {get;set;}
    private static Process_Request_Log__c ReqLogS;
    private static boolean bSkipAircraft {get;set;}
    private static boolean bSkipEngines {get;set;}
    private static boolean bSkipLeases {get;set;}
    private static string totalAssets         {get;set;}
    private static map<string,Incoming_Data__c> assetIncDataMapping {get;set;}   //AssetId <==> IncomingDate
    public static string errorMessage  {get;set;}
    private static ServiceSetting__c lwServiceSettings {get;set;}
    private static string SERVICENAME    = 'Aerdata';   
    private static string ITEMINFOSTATUS = 'Active - Asset on lease';
    private static final integer REQUESTTIMEOUT = 12000;
    static {
    	system.debug('Static Block +');
    	AllJSON = new list<String>();
    	majorComponentItems = new list<MajorComponent>();
        assetItems  = new list<AssetItem>();
        listAssetContracts = new list<AssetContractResource>(); 
        leaseItems  = new list<LeaseItem>();
        assemblyItems = new list<AssemblyItem>();    
        response_body = totalAssets = errorMessage  = '';
        assetIncDataMapping = new map<string,Incoming_Data__c>();
        
        lwServiceSettings = ServiceSetting__c.getInstance( SERVICENAME );
        bSkipAircraft = false;
        bSkipEngines = false;
        bSkipLeases = false;
        if( lwServiceSettings!=null )    
        {
            token  = lwServiceSettings.Token__c;
            assetFilter = lwServiceSettings.Asset_Filter__c;
            customerSlug = lwServiceSettings.CustomerSlug__c;
            endPointUrl  = lwServiceSettings.EndPointUrl__c;
            bSkipAircraft = lwServiceSettings.Disable_Aircraft_Integration__c;
            bSkipEngines = lwServiceSettings.Disable_Engine_Integration__c;
            bSkipLeases = lwServiceSettings.Disable_Lease_Integration__c;
        } 
        else{
        	errorMessage='SERVICE_SETTING_ISSUE';
        }  
        
        system.debug('Static Block -');    
    }
    
    public AerDataService()
    {                  
    }   

    
    // This method is deprecated.
    webservice static string displayResults(){
        return 'Not in use';
    }   
        
    @TestVisible static string displayContractResults()
    {
    	system.debug('displayContractResults ++');
        if(bSkipLeases)return '';       
        endPointUrl = lwServiceSettings.EndPointUrl__c +  customerSlug + '/contracts/lease';
        
        response_body = sendRequestToAerData();                  // sending request to aviator server
        
        //system.debug('response_body lease==='+response_body);
        
        if(response_body!='')
        {           
            JSONParser parser = JSON.createParser(response_body);
            //do next token five times
            parser.nextToken(); parser.nextToken(); parser.nextToken(); parser.nextToken();parser.nextToken();
            //then skip child
            parser.skipChildren();
            //do next token twice 
            parser.nextToken();parser.nextToken();
            totalAssets  = parser.getText();
            system.debug('totalAssets==Lease==='+totalAssets);
            //do next token twice
            parser.nextToken(); parser.nextToken();
            
            while( parser.nextToken() != null && parser.getCurrentToken() != JSONToken.END_ARRAY )
            {   
                //system.debug('parser.getText()=while====='+parser.getText());
                LeaseItem itemTmp = new LeaseItem();
                itemTmp =(LeaseItem)parser.readValueAs(LeaseItem.class);
                //system.debug('itemTmp====='+itemTmp);         
                leaseItems.add( itemTmp );
            }
        }      
        system.debug('leaseItems===='+leaseItems.size()+'  '+leaseItems); 
        system.debug('displayContractResults --');
        return response_body;
    }   
    
	
    @TestVisible static string fetchAssetsContracts()
    {       
        system.debug('fetchAssetsContracts ++');
        if(bSkipAircraft)return '';

    	endPointUrl = lwServiceSettings.EndPointUrl__c + customerSlug + '/assetsContracts';

        response_body = sendRequestToAerData();// sending request to aviator server
        system.debug('response_body AssetsContracts==='+response_body);
        
        if(response_body!=''){           
            JSONParser parser = JSON.createParser(response_body);
            //do next token five times
            parser.nextToken(); parser.nextToken(); parser.nextToken(); parser.nextToken();parser.nextToken();
            //then skip child
            parser.skipChildren();
            //do next token twice 
            parser.nextToken();parser.nextToken();
            totalAssets  = parser.getText();
            system.debug('totalAssets==assets==='+totalAssets);
            //do next token twice
            parser.nextToken(); parser.nextToken();
            
            while( parser.nextToken() != null && parser.getCurrentToken() != JSONToken.END_ARRAY)
            {       
                //system.debug('parser.getText()=while====='+parser.getText());
                AssetContractResource itemTmp = new AssetContractResource();
                itemTmp =(AssetContractResource)parser.readValueAs(AssetContractResource.class);   
                //system.debug('itemTmp====='+itemTmp);
                listAssetContracts.add( itemTmp );
            }
        }else if(response_body==null ){ // error case
             //errorFlg=true;
             ReqLogS.status__c = 'FAIL';
             ReqLogS.Comments__c = 'Response is empty. - displayAssetResults' + 'Please check with your system administrator. '  ;
             ReqLogS.Exception_Message__c = 'Response is empty. - displayAssetResults' + 'Please check with your system administrator. '  ;
        }
                
        system.debug('assetItems===='+listAssetContracts.size()+'  '+listAssetContracts);       
        system.debug('fetchAssetsContracts --');
        return response_body;

    }

    @TestVisible static string displayAssetResults()
    {       
        system.debug('displayAssetResults ++');
        if(bSkipAircraft)return '';
        
        
        //Need to introduce asset filter based on custom setting. 
        
        system.debug('assetFilter = '+assetFilter);
        
        if(assetFilter==null){
        	endPointUrl = lwServiceSettings.EndPointUrl__c + customerSlug + '/assets';
        }else{
        	endPointUrl = lwServiceSettings.EndPointUrl__c + customerSlug + '/assets?' +assetFilter;
        }
        

        response_body = sendRequestToAerData();                  // sending request to aviator server
        
        //system.debug('response_body asset==='+response_body);
        
        if(response_body!='')
        {           
            JSONParser parser = JSON.createParser(response_body);
            //do next token five times
            parser.nextToken(); parser.nextToken(); parser.nextToken(); parser.nextToken();parser.nextToken();
            //then skip child
            parser.skipChildren();
            //do next token twice 
            parser.nextToken();parser.nextToken();
            totalAssets  = parser.getText();
            system.debug('totalAssets==assets==='+totalAssets);
            //do next token twice
            parser.nextToken(); parser.nextToken();
            
            while( parser.nextToken() != null && parser.getCurrentToken() != JSONToken.END_ARRAY)
            {       
                //system.debug('parser.getText()=while====='+parser.getText());
                AssetItem itemTmp = new AssetItem();
                itemTmp =(AssetItem)parser.readValueAs(AssetItem.class);   
                //system.debug('itemTmp====='+itemTmp);
                assetItems.add( itemTmp );
            }
        } 
        else if(response_body==null ){ // error case
             //errorFlg=true;
             ReqLogS.status__c = 'FAIL';
             ReqLogS.Comments__c = 'Response is empty. - displayAssetResults' + 'Please check with your system administrator. '  ;
             ReqLogS.Exception_Message__c = 'Response is empty. - displayAssetResults' + 'Please check with your system administrator. '  ;
           }
                
        system.debug('assetItems===='+assetItems.size()+'  '+assetItems);       
        system.debug('displayAssetResults --');
        return response_body;
    }  
    
     //New Code For TSN & CSN
     @TestVisible static string displayMajorComponent()
    {
    	system.debug('displayMajorComponent ++');      
        endPointUrl = lwServiceSettings.EndPointUrl__c + customerSlug + '/majorComponents';
        response_body = sendRequestToAerData();                  // sending request to aviator server
        
        
        if(response_body!='')
        {           
            JSONParser parser = JSON.createParser(response_body);
            //do next token five times
            parser.nextToken(); parser.nextToken(); parser.nextToken(); parser.nextToken();parser.nextToken();
            //then skip child
            parser.skipChildren();
            //do next token twice 
            parser.nextToken();parser.nextToken();
            system.debug('Today Parser 1= '+parser);
            totalAssets  = parser.getText();  //ok
            
            system.debug('totalAssets==major Component==='+totalAssets);
            //do next token twice
            parser.nextToken(); parser.nextToken();
            system.debug('Today Parser 2= '+parser);
            
            while( parser.nextToken() != null && parser.getCurrentToken() != JSONToken.END_ARRAY)
            {       
               // system.debug('parser.getText()=while====='+parser.getText());
                MajorComponent majoritemTmp = new MajorComponent();
                majoritemTmp =(MajorComponent)parser.readValueAs(MajorComponent.class);   
               // system.debug('majoritemTmp====='+majoritemTmp);
                majorComponentItems.add( majoritemTmp );
            }
        }  
         else if(response_body==null ){ // error case
             //errorFlg=true;
             ReqLogS.status__c = 'FAIL';
             ReqLogS.Comments__c = 'Response is empty. - displayMajorComponent' + 'Please check with your system administrator. '  ;
             ReqLogS.Exception_Message__c = 'Response is empty. - displayMajorComponent' + 'Please check with your system administrator. '  ;
           }
               
        system.debug('majorComponentItems===='+majorComponentItems.size()+'  '+majorComponentItems);  //ok  
        system.debug('displayMajorComponent --');     
        return response_body;
    } 

    @TestVisible static string getAssemblyData()
    {
    	system.debug('getAssemblyData ++'); 
        if(bSkipEngines)return '';       
        endPointUrl = lwServiceSettings.EndPointUrl__c + customerSlug + '/components';

        response_body = sendRequestToAerData();                  // sending request to aviator server
        
        if(response_body!='')
        {           
            JSONParser parser = JSON.createParser(response_body);
            while(parser.nextToken() != null && parser.getCurrentToken() != JSONToken.START_ARRAY)parser.nextToken();
            while( parser.nextToken() != null && parser.getCurrentToken() != JSONToken.END_ARRAY ){   
                AssemblyItem newAssemblyItem = new AssemblyItem();
                newAssemblyItem =(AssemblyItem)parser.readValueAs(AssemblyItem.class);
                assemblyItems.add(newAssemblyItem);
            }
        }
        
        else if(response_body==null ){ // error case
             //errorFlg=true;
             ReqLogS.status__c = 'FAIL';
             ReqLogS.Comments__c = 'Response is empty. - getAssemblyData' + 'Please check with your system administrator. '  ;
             ReqLogS.Exception_Message__c = 'Response is empty. - getAssemblyData' + 'Please check with your system administrator. '  ;
           }
           
           
		system.debug('getAssemblyData --');
        return response_body;
    }


    public static string refreshDataUsingAssetsContracts(Process_Request_Log__c ReqLog)
    {
		system.debug('refreshDataUsingAssetsContracts ++');

		ReqLogS = ReqLog;
        string strSessionId = ReqLog.Comments__c;
        ReqLog.Comments__c=null;
		
		if(errorMessage.contains('SERVICE_SETTING_ISSUE')){ //Service setting error
			ReqLogS.status__c = 'FAIL';
			ReqLogS.Comments__c = 'Custom settings error. Please check AerData Service Settings.' ;
			ReqLogS.Exception_Message__c = 'Custom settings error. Please check AerData Service Settings.' ;
			return 'Error';
		}
		
		fetchAssetsContracts();
		for(AssetContractResource curAsset: listAssetContracts){
			
		}

        errorMessage = 'OK';
        system.debug('refreshDataUsingAssetsContracts --');
        return errorMessage;
    }
    
    //webservice static string displayResults() { return ''; }  //not in use
    
    public static string refreshData(Process_Request_Log__c ReqLog)
    {
		
		system.debug('refreshData ++');
		ReqLogS = ReqLog;
        string strSessionId = ReqLog.Comments__c;
        ReqLog.Comments__c=null;
		
		if(errorMessage.contains('SERVICE_SETTING_ISSUE')){ //Service setting error
			ReqLogS.status__c = 'FAIL';
			ReqLogS.Comments__c = 'Custom settings error. Please check AerData Service Settings.' ;
			ReqLogS.Exception_Message__c = 'Custom settings error. Please check AerData Service Settings.' ;
			return 'Error';
		}
		
		system.debug('Process_Request_Log__c = '+ ReqLog); //reference of process request log
        //Sending request to Asset service  
        displayAssetResults();
        //Mapping data into Sobject from wrapped Asset response
        for( AssetItem itemInfo :  assetItems )
        {
        	if(itemInfo.reference==null)continue;
            Incoming_Data__c incomingData = new Incoming_Data__c();   
            incomingData.Name = itemInfo.Name;
            incomingData.Session_Id__c = strSessionId; //
            incomingData.MSN__c     = itemInfo.serialNumber;        // MSN
            if( incomingData.MSN__c.containsIgnoreCase('Spares') 
                || (itemInfo.assetModel != null && itemInfo.assetModel.containsIgnoreCase('Asset Package')))continue;
            
            incomingData.Variant__c = itemInfo.assetModel;          //variant
            
            if( itemInfo.lessee!=null ) {      
            	incomingData.CurrentLessee__c = itemInfo.lessee.Name;
            	incomingData.CurrentLesseeShortName__c  = itemInfo.lessee.Name;
            }
            
            incomingData.Registration__c         = itemInfo.registration;
            
            
            //New Code Added on Nov 2017.
            if( itemInfo.dateOfFirstDelivery!=null ) incomingData.Delivery_Date_new__c = date.valueof( itemInfo.dateOfFirstDelivery );
            
            if( itemInfo.dateOfManufacture!=null ){
                incomingData.AircraftVintage__c = date.valueof( itemInfo.dateOfManufacture );
            }else{
                incomingData.AircraftVintage__c = system.today();
            }
            if( itemInfo.dateOfPurchase!=null )
                incomingData.DateofPurchase__c = date.valueof( itemInfo.dateOfPurchase );
            
            if( itemInfo.dateOfFirstDelivery!=null )
                incomingData.In_Service_Date__c = date.valueof( itemInfo.dateOfFirstDelivery );             
            
            if( itemInfo.dateOfLastUtilisation!=null )
                incomingData.Utilization_Current_As_Of__c = date.valueof( itemInfo.dateOfLastUtilisation );
                
            if( itemInfo.engineDesignation!=null )
                incomingData.Engine__c = itemInfo.engineDesignation;          
            
            if( itemInfo.assetOperator!=null ){
                incomingData.Operator__c          = itemInfo.assetOperator.Name;
            }    
            if(itemInfo.netBookValue!=null){
            	incomingData.NBV__c  =  itemInfo.netBookValue ;
            }
            if(itemInfo.assetStatus!=null){
            	incomingData.Contractual_Status__c  =  itemInfo.AssetStatus.name;
            }
            if( itemInfo.Portfolio!=null ){
                incomingData.Portfolio__c = itemInfo.Portfolio.Name;
            }
            
        	assetIncDataMapping.put( itemInfo.reference, incomingData );
        }       
        
        system.debug('assetIncDataMapping==asset=='+assetIncDataMapping.keySet().size()+'  '+assetIncDataMapping);
        
        fetchAssetsContracts();
        Set<String> setAssetIdsWithLease = new Set<String>();
		ContractResource curContract;
		for(AssetContractResource curAsset: listAssetContracts){

	        if(curAsset.reference==null)continue;
			Incoming_Data__c incomingData = assetIncDataMapping.get(curAsset.reference);
			if(incomingData==null)continue;
			if(curAsset.contractItems==null || curAsset.contractItems.size()==0)continue;
system.debug('curAsset ' + curAsset.serialNumber + ' Contract Items Size - ' + curAsset.contractItems.size());			
			setAssetIdsWithLease.add(curAsset.reference);
			curContract=curAsset.contractItems[0].contract;
			for(integer i=0; i<curAsset.contractItems.size(); i++){//start from 0 again to come out of the loop if the first one is active.
				if(curAsset.contractItems[i].contract.isEnabled){
					curContract = curAsset.contractItems[i].contract;
		            incomingData.ContractSummCategory__c = curAsset.contractItems[i].leaseStructure;
					break;
				}
			}
			if(curContract==null)continue;
system.debug('curContract ' + curContract);						
            if(curContract.startDate!=null)incomingData.LeaseStart__c = date.valueof(curContract.startDate);
            if(curContract.endDate!=null)incomingData.LeaseMaturity__c = date.valueof(curContract.endDate);
            if(curContract.lessee!=null){                   
            	incomingData.CurrentLessee__c = curContract.lessee.Name;
                if( curContract.lessee.tradingAlias!=null ){
                    incomingData.CurrentLesseeShortName__c  = curContract.lessee.tradingAlias.name;    
                }
            }
            if( curContract.status!=null ) {
                if( curContract.status.Name == 'Active - Asset on lease' ) incomingData.Status__c = 'Approved';
                else incomingData.Status__c = curContract.status.Name;
            }
            assetIncDataMapping.put(curAsset.reference, incomingData);
		}
        
        //asset with lease incoming data insertion
        if(assetIncDataMapping.size()>0){   
            try{
                LeaseWareUtils.TriggerDisabledFlag = true;    //Disabling Trigger  
                delete [select id from Incoming_Data__c];
                delete [select id from Incoming_Data_Processing_Log__c];
                insert assetIncDataMapping.values();
                LeaseWareUtils.TriggerDisabledFlag = false;    //Enabling Trigger
                errorMessage = 'Data Refreshed Successfully';
            }catch( DMLException ex ){
                for ( Integer i = 0; i < ex.getNumDml(); i++ ) 
                {
                    // Process exception here
                    string errorMsg = ex.getDmlMessage(i)+'/n';
                    system.debug('errorMsg=='+errorMsg);
                    errorMessage = errorMessage + errorMsg;
                }
                ReqLogS.status__c = 'FAIL';
	            ReqLogS.Comments__c = LeaseWareUtils.cutString('Exception while inserting asset with lease incoming data:' + ex.getMessage() , 0, 254 );
	            ReqLogS.Exception_Message__c = 'Exception while inserting asset with lease incoming data: ' + ex.getMessage() + ex.getStackTraceString() ;
	                           
                LeaseWareUtils.createExceptionLog( ex, 'Exception while inserting asset with lease incoming data: ' + errorMessage, null, null,'AerData Integration',false);  
            }catch(exception ex){
                ReqLogS.status__c = 'FAIL';
	            ReqLogS.Comments__c = LeaseWareUtils.cutString('Exception while inserting asset with lease incoming data:' + ex.getMessage() , 0, 254 );
	            ReqLogS.Exception_Message__c = 'Exception while inserting asset with lease incoming data:' + ex.getMessage() + ex.getStackTraceString() ;
	                           
                LeaseWareUtils.createExceptionLog(ex, 'Exception while inserting asset with lease incoming data', null, null,'AerData Integration',false);  
            }finally{
              LeaseWareUtils.TriggerDisabledFlag = false;      //Enabling Trigger    
           	}      
        }else{
            errorMessage = 'No Record Found.';
            ReqLogS.status__c = 'NO DATA FOUND';
            ReqLogS.Comments__c = 'No data returned from AerData.';
        }   
        
        system.debug('Limits.getDMLStatements()===='+'   '+Limits.getDMLStatements()+'  '+Limits.getLimitDMLStatements());
        LeaseWareUtils.insertExceptionLogs();
        system.debug('refreshData --');
        return errorMessage;
        
/*        
        if( assetsWithoutLease )
        {
            incomingDataInfo = new list<Incoming_Data__c>();
                    
            setAllAssets.removeAll(setAssetIdsWithLease);
            
            for( string AssetId : setAllAssets )
            {
               // system.debug('AssetId==='+AssetId);
                if( assetIncDataMapping.get( AssetId )!=null )
                {   
                    Incoming_Data__c incomingData = new Incoming_Data__c();
                    incomingData = assetIncDataMapping.get( assetId );
                    incomingDataInfo.add( incomingData );
                }       
            }
            
            system.debug('incomingDataInfo=withoutlease===='+'   '+incomingDataInfo.size());
           
            //asset without lease incoming data insertion
            if(incomingDataInfo.size()>0){   
                try {  
                    LeaseWareUtils.TriggerDisabledFlag = true;  //Disabling Trigger
          
                    delete [select id from Incoming_Data__c where Session_Id__c != :strSessionId];
                    delete [select id from Incoming_Data_Processing_Log__c];
                    insert incomingDataInfo;
                    
                    LeaseWareUtils.TriggerDisabledFlag = false;    //Enabling Trigger
          
                    errorMessage = 'Data Refreshed Successfully';
                }
                catch( DMLException ex )
                {
                    for ( Integer i = 0; i < ex.getNumDml(); i++ ) 
                    {
                        // Process exception here
                        string errorMsg = ex.getDmlMessage(i)+'/n';
                        system.debug('errorMsg=='+errorMsg);
                        errorMessage = errorMessage + errorMsg;
                    }    
                    
	                ReqLogS.status__c = 'FAIL';
	             	ReqLogS.Comments__c = LeaseWareUtils.cutString('Exception while inserting asset without lease incoming data:' + ex.getMessage() , 0, 254 );
	            	ReqLogS.Exception_Message__c = 'Exception while inserting asset without lease incoming data:' + ex.getMessage() + ex.getStackTraceString() ;
	                
                               
                    LeaseWareUtils.createExceptionLog( ex, 'Exception while inserting asset without lease incoming data: ', null, null,'AerData Integration',false);   
                }   
                finally
                {
                  LeaseWareUtils.TriggerDisabledFlag = false;    //Enabling Trigger    
                }
            }
            else
            {
                errorMessage = 'No Record Found.';
            } 
        }
*/        

    }   
        
    public static string sendRequestToAerData()
    {  
    	system.debug('sendRequestToAerData ++');      

        String responsebody = '';
        HttpResponse response = sendHttpRequest();  
        system.debug('Response : '+response);

        if(response==null){
            errorMessage = 'Response not obtained from AerData.';
            return responsebody;
        }
        string statusMessage = response.getStatus();  
        integer statuscode   = response.getStatusCode();
        if( statuscode != 200 ){   
            errorMessage = statusMessage;
        }   

        system.debug('statuscode=='+statuscode+'  '+statusMessage);
        system.debug('sendRequestToAerData --');
        
        responsebody = response.getBody();  
        AllJSON.add(responsebody);
        return responsebody;
    }
    
    public static HttpResponse sendHttpRequest()
    {   
    	system.debug('sendHttpRequest ++');
		integer tries=0;
   		integer noOfTries=3;
   			
    	if(lwServiceSettings!=null && lwServiceSettings.Maximum_Tries__c!=null) noOfTries = (integer)lwServiceSettings.Maximum_Tries__c;
   		
        HttpRequest request = new HttpRequest();
        Http http1 = new Http();
        HttpResponse response;
        final PageReference theUrl = new PageReference( endPointUrl );
        request.setEndpoint( theUrl.getUrl() );
        request.setMethod( 'GET' );
        request.setTimeout( REQUESTTIMEOUT );
        request.setHeader( 'Authorization', 'Token '+token  );
        request.setHeader( 'content-type','application/json' ); 
        request.setHeader( 'accept','application/json' );
        system.debug('request =='+request  ); 
        
		String responsebody;
		while(tries<noOfTries){
			try{
				response = http1.send( request ); 
				responsebody = response.getBody(); //it will throw an exception, don't comment.
				//system.debug('responsebody '+responsebody);	
				break;
			}
			catch(System.CalloutException ex) {
             //errorFlg=true;
                 system.debug('exception=='+ex.getMessage());
//                 LeaseWareUtils.createExceptionLog( ex, 'CalloutException : '+request, null, null,null,true ); 
                 ReqLogS.status__c = 'FAIL';
                 ReqLogS.Comments__c = LeaseWareUtils.cutString('CalloutException :' + ex.getMessage() , 0, 254 );
                 ReqLogS.Exception_Message__c = 'CalloutException :' + ex.getMessage() + ex.getStackTraceString() ;
                 tries++;
    		}
			catch( Exception exCall ){
	        	tries++;
	            system.debug('exception=='+exCall.getMessage());
//	            LeaseWareUtils.createExceptionLog( exCall , 'Exception while sending http request to AerdataServer: '+request, null, null,null,true ); 
	        
	        	ReqLogS.status__c = 'FAIL';
             	ReqLogS.Comments__c = LeaseWareUtils.cutString('Exception while sending http request to AerdataServer: :' + exCall.getMessage() , 0, 254 );
            	ReqLogS.Exception_Message__c = 'Exception while sending http request to AerdataServer: :' + exCall.getMessage() + exCall.getStackTraceString() ;
	        }
		}
		system.debug('sendHttpRequest --');
        return response;
    }    
    
    //deprecated
    webservice static void executeAerDataBatch()    
    {
    	system.debug('executeAerDataBatch ++');
        //ID batchprocessid = Database.executeBatch( new AerDataServiceBatch(),1 );    // running a batch with batch size 1 
        system.debug('executeAerDataBatch --');       
    }
    
    //Wrapper to parse response LeaseContract Service
    public class LeaseItem
    {
        public Links_Z links {get;set;}
        public Lessee lessee {get;set;}
        public Portfolio portfolio {get;set;}
        public LeaseStatus leaseStatus {get;set;}
        public list<Amount_Z> amounts {get;set;}
        public string contractualDeliveryDate {get;set;}
        public string contractualRedeliveryDate {get;set;}
        public string leaseTypeLegal {get;set;}
        public string location {get;set;}
        public string identifier {get;set;}
        public string referenceId {get;set;}
        public LeaseItem()
        {
            links = new Links_Z();
            lessee = new lessee();      
            referenceId = '';
            identifier = '';            
            portfolio = new Portfolio();            
            leaseStatus = new LeaseStatus();
            amounts = new list<Amount_Z>();
        }
    }

    public class Amount_Z {
        public list<Values> values {get;set;}
        public string name         {get;set;}   
        public  Amount_Z() {
            values = new list<Values>();
            name = '';
        }
    }
    
    public class Values {
        public decimal value {get;set;}
        public Values() {
            value = 0.0;
        }
    }
    
    //Wrapper to parse response for Asset Service
    public class AssetItem
    {
        public Links_Z links {get;set;}
        public string name {get;set;}       
        public string assetDesignation {get;set;}
        public string engineRating {get;set;}
        public boolean isReleased {get;set;}
        public string maintenancePlan {get;set;}
        public string engineDesignation {get;set;}
        public string comment {get;set;}
        public string slug {get;set;}
        public string dateOfLastUtilisation {get;set;}
        public string dateOfFirstDelivery {get;set;}
        public string dateOfPurchase {get;set;}
        public string dateOfFirstFlight {get;set;}
        public string dateOfManufacture {get;set;}
        public string assetType {get;set;}
        public string assetModel {get;set;}
        public string serialNumber {get;set;}
        public string registration {get;set;}
        public string reference {get;set;}
        public string identifier {get;set;}             
        public Lessee lessee {get;set;}
        public AssetOperator assetOperator {get;set;}
        public Portfolio portfolio {get;set;}
        public AssetStatus assetStatus {get;set;}
        public double netBookValue{get; set;}
        public AssetItem()
        {
            links = new Links_Z();
            netBookValue=0;
            assetDesignation = '';
            name = '';
            engineRating = '';
            isReleased = false;
            maintenancePlan = '';
            engineDesignation = '';
            comment = '';
            slug = '';
            dateOfLastUtilisation = '';
            dateOfFirstDelivery = '';
            dateOfPurchase = '';
            dateOfFirstFlight = '';
            dateOfManufacture = '';
            assetType = '';
            assetModel = '';
            serialNumber = '';
            registration = '';
            reference = '';
            identifier = '';            
            lessee = new Lessee();
            assetOperator = new AssetOperator();
            portfolio = new Portfolio();            
            assetStatus = new AssetStatus();
        }
    }

    public class ManufacturerInfo
    {
        public string Name {get; set;}
    }
    
    public class AssemblyItem
    {
        public string Name {get; set;}
        public string Description {get; set;}
        public ManufacturerInfo manufacturer {get; set;}
        public string SerialNumber {get; set;}
        public decimal TSN {get; set;}
        public decimal CSN {get; set;}
        public decimal TSO {get; set;}
        public decimal CSO {get; set;}
        public string Status {get; set;}
        
    }
    
    public class Lessee
    {
        public string name {get;set;}
        public TradingAlias tradingAlias {get;set;} 
    }
    
    public class TradingAlias {
        public string name {get;set;}
    }
    
    public class AssetOperator
    {
        public string name {get;set;}
    }
    
    public class Portfolio
    {
        public string name {get;set;}
    }
    
    public class AssetStatus
    {
        public string name {get;set;}
    }

    public class LeaseStatus
    {
        public string name {get;set;}
		public string customerSlug {get;set;}
    }

    
    public class Links_Z 
    {
        public List<Self> self      {get;set;}
        public List<Self> contract    {get;set;}
        public List<Self> asset  {get;set;}
        public List<Self> lessee {get;set;}
        public List<Self> lessor {get;set;}
        public List<Self> portfolio {get;set;}
        public List<Self> team {get;set;}       
        public List<Self> status {get;set;}
        public Links_Z()
        {
            self = new List<Self>();
            contract = new List<Self>();
            asset = new List<Self>();
            lessee = new List<Self>();
            lessor = new List<Self>();
            portfolio = new List<Self>();
            team = new List<Self>();
            status = new List<Self>();
        }       
    }
    
    
    //Major Component
    public class MajorComponent
    {
        public Links_Z links {get;set;}
		
		public string componentConfigurationGroup {get;set;}
		public string ata{get;set;}
		public double tsn{get;set;}
		public double csn{get;set;}
		public double tso{get;set;}
		public double cso{get;set;}
		public string identifier{get;set;}
		public string assetId{get;set;}
		public string assetMsn{get;set;}
		public string assetMsnSlug{get;set;}
		public string name{get;set;}
		public string remark{get;set;}
		public double quantity{get;set;}
		public string status{get;set;}
		public string serialNumber{get;set;}
		public string partNumber{get;set;}
		public string lastShopVisit{get;set;}
		public string typeCode{get;set;}
		
        public MajorComponent()
        {
            links = new Links_Z();
            
			componentConfigurationGroup='';
			ata='';
			tsn=0;
			csn=0;
			tso=0;
			cso=0;
			identifier='';
			assetId='';
			assetMsn='';
			assetMsnSlug='';
			name='';
			remark='';
			quantity=0;
			status='';
			serialNumber='';
			partNumber='';
			lastShopVisit='';
			typeCode='';
			
        }
    }
    public class Self 
    {
        public String href {get;set;}
    }

//Resources for assetsContracts

	public class AssetContractResource{
		public string serialNumber {get;set;}
		public string reference {get;set;}
		public string assetModel {get;set;}
		public string assetType {get;set;}
		public string registration {get;set;}
		public string identifier {get;set;}
		public string slug {get;set;}
		public AssetStatus assetStatus {get;set;}
		public string comment {get;set;}
		public string engineDesignation {get;set;}
		public boolean isReleased {get;set;}
		public string name {get;set;}
		public string description {get;set;}
		public boolean isEnabled {get;set;}
		public CompanyResource portfolio {get;set;}
		public CompanyResource manufacturer {get;set;}
		public string dateOfManufacture	{get;set;}
		public list<ContractItemResource> contractItems {get;set;}
		public AssetStatusResource status {get;set;}
	}
	
	public class CompanyResource{
		public string name {get;set;}
		public string customerSlug {get;set;}
        public TradingAlias tradingAlias {get;set;} 
	}
	
	public class AssetStatusResource{
		public string name {get;set;}
	}
	
	public class ContractItemResource{
		public string identifier {get;set;}
		public string assetId {get;set;}
		public list<BalanceItem> totalMrFundAmounts {get;set;}
		public ContractResource contract {get;set;}
		public string contractType {get;set;}
		public string leaseStructure {get;set;}
		public string lastModificationDateUtc {get;set;}
	}
	
	public class BalanceItem{
//		public CurrencyResource currency {get;set;} Currency is reserved in SF. Can't use.
		public decimal amount {get;set;}
		public string name {get;set;}
	}
	
/*	public class CurrencyResource{
		public string code {get;set;}
		public string description {get;set;}
	}
*/	
	public class ContractResource{
		public string startDate {get;set;}
		public string endDate {get;set;}
		public string identifier {get;set;}
		public string referenceId {get;set;}
		public CompanyResource lessee {get;set;}
		public CompanyResource lessor {get;set;}
		public LeaseStatus status {get;set;}
		public boolean isEnabled {get;set;}
		public CompanyResource portfolio {get;set;}
		public boolean isReleased {get;set;}
		public string startDateLabel {get;set;}
		public string endDateLabel {get;set;}
		public string lastModificationDateUtc {get;set;}
		public string contractDetailUrl {get;set;}
		public string customerSlug {get;set;}
	}    

}