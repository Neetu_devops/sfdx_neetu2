@isTest
private class TestLeaseUpdate 
{

    static testMethod void testLease()
    {   /*
        // Set up the Account record.
        Aircraft__c a = new Aircraft__c(MSN_Number__c='12345',Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available');
        insert a;
        // Verify that the initial state is as expected.
        a = [SELECT Name, MSN_Number__c
             FROM Aircraft__c
             WHERE Id = :a.Id];
        String msn=a.MSN_Number__c;
        //System.assertEquals(null, msn);
        
        // Set up the Operator record.
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        insert operator;
        
        // Set up the Lease record.
        String LeaseName = 'My Lease';
        Lease__c lease = new Lease__c(Aircraft__c=a.Id, Name=LeaseName, Rent_Type__c='Power By The Hour', Month_Thresholds__c='0,6,12', PBH_Rates__c='145,125,100', 
                    Operator__c=operator.Id, UR_Due_Day__c='10',Lease_Start_Date_New__c=Date.parse('01/01/2010'), 
                    Lease_End_Date_New__c=Date.parse('12/31/2015'), Rent_Date__c=Date.parse('01/01/2010'), Rent_End_Date__c=Date.parse('12/31/2015'));
        // Cause the Trigger to execute.
        insert lease;
        // Verify that the results are as expected.
        lease = [SELECT Name, Lease_End_Date__c, Lease_End_Date_New__c
             FROM Lease__c
             WHERE Id = :lease.Id];
        String leaseName2=l.Name; 
        System.assertEquals(LeaseName, leaseName2);
        System.debug('Lease End Date - ' + lease.Lease_End_Date__c + ', ' + lease.Lease_End_Date_New__c);
       
        lease.Month_Thresholds__c='0,6';
        lease.PBH_Rates__c='145,125';
        lease.Lease_End_Date_New__c=Date.parse('7/31/2016');//lease.Lease_End_Date_New__c.addMonths(1);
        lease.Y_Hidden_Recalc_Buckets__c=true;
        System.debug('Lease End Date - ' + lease.Lease_End_Date_New__c);
        update lease;
        lease = [SELECT Name, Lease_End_Date__c, Lease_End_Date_New__c
             FROM Lease__c
             WHERE Id = :lease.Id];
       System.debug('Lease End Date After Update - ' + lease.Lease_End_Date__c + ', ' + lease.Lease_End_Date_New__c);
        
        
        Lease_Extension_History__c[] lHist = [select id, Lease_End_Date_Time_Changed_To__c from Lease_Extension_History__c where Lease__c = :lease.id];
        try{
            delete  lHist[0];
        }catch(exception e){
            system.debug('Can\'t delete. Expected ' + e);
        }
        
        lHist[0].Lease_End_Date_Time_Changed_To__c=lHist[0].Lease_End_Date_Time_Changed_To__c.addMonths(1);
        try{
            update  lHist[0];
        }catch(exception e){
            system.debug('Can\'t Update. Expected ' + e);
        }
        
        Lease_Extension_History__c newLH = new Lease_Extension_History__c(Name='Dummy', Lease__c=lease.id);
        try{
            insert newLH;
        }catch(exception e){
            system.debug('Can\'t Insert. Expected ' + e);
        }
        
        
        
        try{
            lease.Y_Hidden_Create_Audit_Schedule__c=true;
            update lease;
        }catch(exception e){
            system.debug('Can\'t create audit schedule. Unxpected ' + e);
        }
        
        try{
            lease.Rent_Type__c='Fixed Rent';
            lease.Base_Rent__c=300000;
            lease.Y_Hidden_Create_Rent_Schedule__c=true;
            update lease;
        }catch(exception e){
            system.debug('Can\'t create Rent schedule. Unxpected ' + e);
        }
       
        String caName='12345';
        String caDescription = 'Test';
         
        // Set up the Aircraft record.
        String aircraftMSN = '12345';
        a = new Aircraft__c(MSN_Number__c=aircraftMSN,Date_of_Manufacture__c = System.today(), Aircraft_Type__c='737', Aircraft_Variant__c='300', TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Engine_1_Check__c=1, Status__c='Available');
        insert a;
         
        // Set up the Constt Assmbly record.
        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createTSLookup(mapInOut);
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name=caName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56', Engine_Model__c ='CFM56', Attached_Aircraft__c= a.Id, Description__c =caDescription, TSN__c=0, CSN__c=0, TSLV__c=0, CSLV__c= 0, Type__c ='Engine 1', Serial_Number__c='123', Engine_Thrust__c='-3B1');
        insert consttAssembly;
               
        
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='131';
        update a;
        consttAssembly.Aircraft_Type__c='A321-131';
        consttAssembly.Engine_Thrust__c='V2530-A5';
        consttAssembly.Aircraft_Engine__c='A321-131 @ V2500';
        update consttAssembly;
    
        a.Aircraft_Type__c='A320';
        a.Aircraft_Variant__c='231';
        update a;
        consttAssembly.Aircraft_Type__c='A320-231';
        consttAssembly.Engine_Thrust__c='V2500-A1';
        consttAssembly.Aircraft_Engine__c='A320-231 @ V2500';
        update consttAssembly;
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='132';
        update a;
        consttAssembly.Aircraft_Type__c='A321-132';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A321-132 @ V2500';
        update consttAssembly;
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='132';
        update a;
        consttAssembly.Aircraft_Type__c='A321-132';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A321-132 @ V2500';
        update consttAssembly;
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='111';
        update a;
        consttAssembly.Aircraft_Type__c='A321-111';
        consttAssembly.Engine_Thrust__c='-5B1';
        consttAssembly.Aircraft_Engine__c='A321-111 @ CFM56';
        update consttAssembly;
        
        a.Aircraft_Type__c='A319';
        a.Aircraft_Variant__c='131';
        update a;
        consttAssembly.Aircraft_Type__c='A319-131';
        consttAssembly.Engine_Thrust__c='V2522-A5';
        consttAssembly.Aircraft_Engine__c='A319-131 @ V2500';
        
        a.Aircraft_Type__c='A320';
        a.Aircraft_Variant__c='233';
        update a;
        consttAssembly.Aircraft_Type__c='A320-233';
        consttAssembly.Engine_Thrust__c='V2527E-A5';
        consttAssembly.Aircraft_Engine__c='A320-233 @ V2500';
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='131';
        update a;
        consttAssembly.Aircraft_Type__c='A321-131';
        consttAssembly.Engine_Thrust__c='V2530-A5';
        consttAssembly.Aircraft_Engine__c='A321-131 @ V2500';
          
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='231';
        update a;
        consttAssembly.Aircraft_Type__c='A321-231';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A320-231 @ V2500';
        a.Aircraft_Type__c='A321';
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='132';
        update a;
        consttAssembly.Aircraft_Type__c='A321-132';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A321-132 @ V2500';
        
        a.Aircraft_Type__c='A320';
        a.Aircraft_Variant__c='232';
        update a;
        consttAssembly.Aircraft_Type__c='A320-232';
        consttAssembly.Engine_Thrust__c='V2527-A5';
        consttAssembly.Aircraft_Engine__c='A320-232 @ V2500';
        
        a.Aircraft_Type__c='A320';
        a.Aircraft_Variant__c='233';
        update a;
        consttAssembly.Aircraft_Type__c='A320-233';
        consttAssembly.Engine_Thrust__c='V2527E-A5';
        consttAssembly.Aircraft_Engine__c='A320-233 @ V2500';
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='131';
        update a;
        consttAssembly.Aircraft_Type__c='A321-131';
        consttAssembly.Engine_Thrust__c='V2530-A5';
        consttAssembly.Aircraft_Engine__c='A321-131 @ V2500';
          
        a.Aircraft_Type__c='A320';
        a.Aircraft_Variant__c='231';
        update a;
        consttAssembly.Aircraft_Type__c='A320-231';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A320-231 @ V2500';
        
        a.Aircraft_Type__c='A321';
        a.Aircraft_Variant__c='132';
        update a;
        consttAssembly.Aircraft_Type__c='A321-132';
        consttAssembly.Engine_Thrust__c='V2533-A5';
        consttAssembly.Aircraft_Engine__c='A321-132 @ V2500';
        
        
        //Floating Rent Inputs Trigger
        Floating_Rent_Inputs__c curFRI= new Floating_Rent_Inputs__c();
        curFRI.Lease__c=lease.Id;
        curFRI.Name='';
        curFRI.Approximate_Reset_Date__c=system.today();
        curFRI.From_Period__c=5;
        curFRI.To_Period__c=10;
        insert curFRI;
 
 		try{
 			lease.Aircraft__c=null;
 			update lease;
	 		delete lease;
 			undelete lease;
 		}catch(exception e){
 			system.debug('Expected, can\'t undelete Lease.');
 		}       
      */  
    }
    
    static testMethod void testLeasePrevOperator()
    { 
    	map<String,Object> mapInOut = new map<String,Object>();
    	mapInOut.put('Aircraft__c.msn','MSN1');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id AircraftId1 = (string) mapInOut.get('Aircraft__c.Id');
        
    	mapInOut.put('Aircraft__c.msn','MSN2');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id AircraftId2 = (string) mapInOut.get('Aircraft__c.Id');
        
    	mapInOut.put('Aircraft__c.msn','MSN3');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id AircraftId3 = (string) mapInOut.get('Aircraft__c.Id');
                
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String)mapInOut.get('Operator__c.Id');

        mapInOut.put('Operator__c.Name','Air India1');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId2 = (String)mapInOut.get('Operator__c.Id');
       
        // Set up the Lease record.
        list<Lease__c> listLease = new list<Lease__c>();
		mapInOut.put('Lease__c.Aircraft__c',AircraftId1);
		mapInOut.put('Lease__c.Operator__c',OperatorId1);
		mapInOut.put('Lease__c.Name','Lease1');	 
		mapInOut.put('Lease__c',null);		
		TestLeaseworkUtil.createLease(mapInOut);
		listLease.add((Lease__c)mapInOut.get('Lease__c'));

		mapInOut.put('Lease__c.Aircraft__c',AircraftId2);
		mapInOut.put('Lease__c.Operator__c',OperatorId1);
		mapInOut.put('Lease__c.Name','Lease2');	 
		mapInOut.put('Lease__c',null);				 	
		TestLeaseworkUtil.createLease(mapInOut);
		listLease.add((Lease__c)mapInOut.get('Lease__c'));
		
		mapInOut.put('Lease__c.Aircraft__c',AircraftId3);
		mapInOut.put('Lease__c.Operator__c',OperatorId1);
		mapInOut.put('Lease__c.Name','Lease3');	 
		mapInOut.put('Lease__c',null);				 	
		TestLeaseworkUtil.createLease(mapInOut);
		listLease.add((Lease__c)mapInOut.get('Lease__c'));
		
		for(Lease__c l:listLease){
        	l.Operator__c =OperatorId2;
		}
		Test.StartTest();
		LeaseWareUtils.clearFromTrigger();
        update listLease;
        
        // Verify that the results are as expected.
        Lease__c lease = [SELECT Name, Lease_End_Date_New__c,Previous_Operator__c
             FROM Lease__c
             WHERE Id = :(String)mapInOut.get('Lease__c.Id')];
   		Test.StopTest();
		system.assertEquals(OperatorId1,lease.Previous_Operator__c);
		system.debug('lease.Previous_Operator__c'+ lease.Previous_Operator__c);   	

    }        
          
    
    @isTest(seeAllData=true)
    static void AssemblyMRRateCRUD(){

    	map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c,lease__r.Lease_Start_Date_New__c from Aircraft__c where 
                           MSN_Number__c = :ACName limit 1];

        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
    	
    	Id leaseId = (Id)mapInOut.get('Aircraft__c.lease__c');
    	
        Constituent_Assembly__c consttAssembly = [select id from Constituent_Assembly__c where Attached_Aircraft__c =:AC1.Id and Type__c ='APU'];


        // Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'APU',
            Maintenance_Program__c= mp.Id,
		    Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
        // Set up Project Event
        Assembly_Event_Info__c eventAPU = new Assembly_Event_Info__c();
        eventAPU.Name = 'Test Info';
        eventAPU.Event_Cost__c = 20;
        eventAPU.Constituent_Assembly__c = consttAssembly.Id;
        eventAPU.Maintenance_Program_Event__c = mpe.Id;
        insert eventAPU;

    	Assembly_MR_Rate__c CAMR = new Assembly_MR_Rate__c(name = 'Dummy',Lease__c=leaseId
    											,Base_MRR__c = 100
    											,Assembly_Lkp__c = consttAssembly.id
                                                ,Assembly_Event_Info__c=eventAPU.id
                                                ,Rate_Basis__c = 'Hours' );
    											
    	LeaseWareUtils.clearFromTrigger();insert 	CAMR;	

  	    list<Assembly_MR_Rate__c> CA_MRList = [select id from Assembly_MR_Rate__c where Lease__c = :leaseId];
		Assembly_MR_Rate__c CA_MR = CA_MRList[0];
    	CA_MR.Base_MRR__c = 100;
    	CA_MR.MR_Rate_Start_Date__c= Ac1.lease__r.Lease_Start_Date_New__c;
    	CA_MR.MR_Rate_End_Date__c = Ac1.lease__r.Lease_Start_Date_New__c.addmonths(10);
    	CA_MR.MR_Escalation_Month__c = 'March';
    	CA_MR.Annual_Escalation__c = 10;
    	CA_MR.Rate_Basis__c = 'Hours';	
    	CA_MR.Flat_Rate_Upto_Threshold__c = 10;
    	LeaseWareUtils.clearFromTrigger();update 	CA_MR;	
    	LeaseWareUtils.clearFromTrigger();delete 	CA_MR;						
    	LeaseWareUtils.clearFromTrigger();undelete 	CA_MR;		
    }

    @isTest(seeAllData=true)
    static void AssemblyMRRate_CreateRate(){

    	map<String,Object> mapInOut = new map<String,Object>();
         string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c,lease__r.Lease_Start_Date_New__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
    	
    	Id leaseId = (Id)mapInOut.get('Aircraft__c.lease__c');
    	
        Constituent_Assembly__c consttAssembly = [select id from Constituent_Assembly__c where Attached_Aircraft__c =:AC1.Id and Type__c ='APU'];

		
         // Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'APU',
            Maintenance_Program__c= mp.Id,
			Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
        // Set up Project Event
        Assembly_Event_Info__c eventAPU = new Assembly_Event_Info__c();
        eventAPU.Name = 'Test Info';
        eventAPU.Event_Cost__c = 20;
        eventAPU.Constituent_Assembly__c = consttAssembly.Id;
        eventAPU.Maintenance_Program_Event__c = mpe.Id;
        insert eventAPU;

    	Assembly_MR_Rate__c CAMR = new Assembly_MR_Rate__c(name = 'Dummy',Lease__c=leaseId
    											,Base_MRR__c = 100
    											,MR_Rate_Start_Date__c= Ac1.lease__r.Lease_Start_Date_New__c
    											,MR_Rate_End_Date__c = Ac1.lease__r.Lease_Start_Date_New__c.addmonths(10)
    											,MR_Escalation_Month__c = 'March'
    											,Annual_Escalation__c = 10
    											,Rate_Basis__c = 'Hours'
    											,Assembly_Lkp__c = consttAssembly.id
                                                ,Assembly_Event_Info__c=eventAPU.id);
    											
    	LeaseWareUtils.clearFromTrigger();insert 	CAMR;	
        list<Assembly_MR_Rate__c> CA_MRList = [select id ,Name,Rate_Basis__c,Engine_Template__c,Lease__c,Esc_Periodicity_Mos__c
                                                ,Escalation_Type__c,First_Escalation_Date__c,Ratio_Table__c,Rounding_Method_On_Esc_MR_Rate__c,
                                               Rounding_Method_on_Interpolated_MR_Rate__c,Rounding_Decimals_Interpolated_MR_Rate__c,
                                               Rounding_Decimals_Escalated_MR_Rate__c
                                                from Assembly_MR_Rate__c 
                                                 where Lease__c = :leaseId];
		Assembly_MR_Rate__c CA_MR = CA_MRList[0];
    	CA_MR.Base_MRR__c = 100;
    	CA_MR.MR_Rate_Start_Date__c= Ac1.lease__r.Lease_Start_Date_New__c;
    	CA_MR.MR_Rate_End_Date__c = Ac1.lease__r.Lease_Start_Date_New__c.addmonths(10);
    	CA_MR.MR_Escalation_Month__c = 'March';
    	CA_MR.Annual_Escalation__c = 10;
    	CA_MR.Rate_Basis__c = 'Hours';
    	LeaseWareUtils.clearFromTrigger();update 	CA_MR;	
    	
		LeaseWareUtils.clearFromTrigger();
		AssemblyMRRateTriggerHandler a1= new AssemblyMRRateTriggerHandler();
		a1.Create_MRRateTable(CA_MR);
		list<MR_Rate__c> listMRrate =[select id,Name,MR_Rate_End_Date__c,MR_Rate_Start_Date__c,MR_Rate__c
		                           	from MR_Rate__c
		                           where Assembly_MR_Info__c = :CA_MR.Id
		                           order by MR_Rate_Start_Date__c desc];  
        if(listMRrate.size()>1){
			listMRrate[1].MR_Rate__c = 300;
        	if(listMRrate[0].MR_Rate_End_Date__c!=listMRrate[0].MR_Rate_Start_Date__c && listMRrate[1].MR_Rate_End_Date__c!=listMRrate[1].MR_Rate_Start_Date__c){
            listMRrate[0].MR_Rate_End_Date__c = listMRrate[0].MR_Rate_End_Date__c.addDays(1);
		    listMRrate[1].MR_Rate_Start_Date__c = listMRrate[1].MR_Rate_Start_Date__c.addDays(1);
        }
			LeaseWareUtils.clearFromTrigger(); Update 	listMRrate[0];	
            LeaseWareUtils.clearFromTrigger(); Update 	listMRrate[1];
        }
		
		LeaseWareUtils.clearFromTrigger();a1.Create_MRRateTable(CA_MR);                           		
    }
    
    //test case for stepped rent
    @isTest(seeAllData=true)
    static void SteppedRent(){
    	
    	Lease__c newLeasae=new Lease__c();
    	newLeasae.Name='L1';
        newLeasae.Base_Rent__c=1000;
    	newLeasae.Lease_Start_Date_New__c=system.today();
    	newLeasae.Lease_End_Date_New__c=system.today().addMonths(12);
    	LeaseWareUtils.clearFromTrigger();insert newLeasae;
    	
		Stepped_Rent__c sr=new Stepped_Rent__c();
		sr.Lease__c=newLeasae.Id;
		sr.Name='SR1';
        //sr.DollarAmount__c = false; --> This field is moved to Stepped MultiRent Obj
		sr.Start_Date__c=system.today();
        sr.Rent_End_Date__c=system.today().addMonths(5);
		//sr.Rent_Percent__c=2; --> This field is moved to Stepped MultiRent Obj
			
		LeaseWareUtils.clearFromTrigger();insert sr;	
		
		Stepped_Rent__c sr1=new Stepped_Rent__c();
		sr1.Lease__c=newLeasae.Id;
		sr1.Name='SR2';
        //sr1.DollarAmount__c = true; --> This field is moved to Stepped MultiRent Obj
		sr1.Start_Date__c=system.today();
        sr1.Rent_End_Date__c=system.today().addMonths(5);
		//sr1.Rent_Amount__c=2000; --> This field is moved to Stepped MultiRent Obj
    	LeaseWareUtils.clearFromTrigger();insert sr1;	
        //sr1.Rent_Amount__c=2001; --> This field is moved to Stepped MultiRent Obj
        LeaseWareUtils.clearFromTrigger();update sr1;	
    	LeaseWareUtils.clearFromTrigger();delete sr1;
    }
    @isTest(seeAllData=true)
	static void testExternalIdUpdate1(){
		// External id with aircraft id and operator id
		string ACName = 'TestSeededMSN1';
		Aircraft__c AC1 = [select id,Aircraft_Type__c,Lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
		Lease__c lease1 = [select id,Name,lessee__c, operator__c from Lease__c where id = :AC1.Lease__c limit 1];

		Lease__c lease2 = new Lease__c(Name = 'Lease2',
		                               Lease_Start_Date_New__c = Date.newInstance(2011,07,11),
		                               Lease_End_Date_New__c = Date.newInstance(2016,07,17),
		                               operator__c = lease1.operator__c,
		                               Aircraft__c = AC1.id);

		Test.startTest();
		insert lease2;
		Test.stopTest();

		List<Lease__c> listLease = [select id,Name,Lease_External_Id__c,Lease_Start_Date_New__c from Lease__c where Aircraft__c = :AC1.id ];

		System.debug('Lease :' + listLease.size());
		Lease__c tempLease;
		for(Lease__c curL : listLease) {
			System.debug('Lease Name :' + curL.Lease_External_Id__c);
			if(curL.Lease_Start_Date_New__c == Date.newInstance(2011,07,11)) tempLease = curL;
		}
		System.debug('Lease : tmpLease :' + tempLease.Name);
		String key = String.valueOf(AC1.id)+'-'+String.valueOf( lease1.operator__c)+'-'+ LeasewareUtils.getDatetoString(tempLease.Lease_Start_Date_New__c,'YYYYMMDD');
		System.assertEquals(key,tempLease.Lease_External_Id__c);
	}

	@isTest(seeAllData=true)
	static void testExternalIdUpdate2(){
		// External id with aircraft id
		string ACName = 'TestSeededMSN1';
		Aircraft__c AC1 = [select id,Aircraft_Type__c,Lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
		Lease__c lease1 = [select id,Name,lessee__c, operator__c from Lease__c where id = :AC1.Lease__c limit 1];

		Lease__c lease2 = new Lease__c(Name = 'Lease2',
		                               Lease_Start_Date_New__c = Date.newInstance(2011,07,11),
		                               Lease_End_Date_New__c = Date.newInstance(2016,07,17),
		                               Aircraft__c = AC1.id);

        Test.startTest();
        LeaseWareUtils.clearFromTrigger();
		insert lease2;

		List<Lease__c> listLease = [select id,Name,Lease_External_Id__c,Lease_Start_Date_New__c from Lease__c where Aircraft__c = :AC1.id ];

		System.debug('Lease :' + listLease.size());
		Lease__c tempLease;
		for(Lease__c curL : listLease) {
			System.debug('Lease Name :' +curL.Name +'::'+curL.Lease_External_Id__c);
			if(curL.Lease_Start_Date_New__c == Date.newInstance(2011,07,11)) tempLease = curL;
		}
		System.debug('Lease : tmpLease :' + tempLease.Name);
		String key = String.valueOf(AC1.id)+'-'+ LeasewareUtils.getDatetoString(tempLease.Lease_Start_Date_New__c,'YYYYMMDD');
		System.assertEquals(key,tempLease.Lease_External_Id__c);

		LeaseWareUtils.clearFromTrigger();
		tempLease.Operator__c = lease1.Operator__c;
		templease.Lessee__c = lease1.Lessee__c;
		update tempLease;
		Test.stopTest();

		List<Lease__c> listLease2 = [select id,Name,Lease_External_Id__c,Lease_Start_Date_New__c from Lease__c where id =: tempLease.Id];
		String key2 = String.valueOf(AC1.id)+'-'+String.valueOf( lease1.Lessee__c)+'-'+ LeasewareUtils.getDatetoString(tempLease.Lease_Start_Date_New__c,'YYYYMMDD');
		System.assertEquals(key2,listLease2[0].Lease_External_Id__c);
	}

	@isTest(seeAllData=true)
	static void testExternalIdUpdate3(){
		// External id with aircraft id = null
		string ACName = 'TestSeededMSN1';
         Aircraft__c AC1 = [select id,Aircraft_Type__c,Lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];

		Lease__c lease1 = [select id,Name,lessee__c, operator__c from Lease__c where id = :AC1.Lease__c limit 1];

		Lease__c lease2 = new Lease__c(Name = 'Lease2',
		                               Lease_Start_Date_New__c = Date.newInstance(2011,07,11),
		                               Lease_End_Date_New__c = Date.newInstance(2016,07,17)
		                               );

        Test.startTest();
        LeasewareUtils.clearFromTrigger();
        insert lease2;

		List<Lease__c> listLease = [select id,Name,Aircraft__c,Lease_External_Id__c,Lease_Start_Date_New__c from Lease__c where id =:lease2.id limit 1 ];

        System.debug('Lease :' + listLease.size());
        System.debug('Lease :' + listLease[0].Aircraft__c);
		System.debug('Lease : tmpLease :' + listLease[0].Name +'--'+ listLease[0].Lease_External_Id__c);

		System.assertEquals(null,listLease[0].Lease_External_Id__c);

        // update lease to include aircraft id in external id
        LeasewareUtils.clearFromTrigger();
		lease2.Aircraft__c = AC1.id;
		update lease2;


		List<Lease__c> listLease2 = [select id,Name,Aircraft__c,Lease_External_Id__c,Lease_Start_Date_New__c from Lease__c where id =: lease2.Id];
		for(Lease__c curL : listLease2) {
            System.debug('Lease Name 2:' + curL.Lease_External_Id__c);
            System.debug('Lease 2:' + listLease[0].Aircraft__c);
		}
		String key = String.valueOf(AC1.id)+'-'+ LeasewareUtils.getDatetoString(listLease2[0].Lease_Start_Date_New__c,'YYYYMMDD');
		System.assertEquals(key,listLease2[0].Lease_External_Id__c);

		// duplicate external id ERROR
		Lease__c lease3 = new Lease__c(Name = 'Lease3',
		                               Lease_Start_Date_New__c = Date.newInstance(2011,07,11),
		                               Lease_End_Date_New__c = Date.newInstance(2016,07,17),
		                               Aircraft__c = AC1.id
		                               );

		LeaseWareUtils.clearFromTrigger();
		try{
			insert lease3;
		}catch(Exception e) {
			System.debug('ERROR:' +  e.getMessage());
			System.assertEquals(true,e.getMessage().contains('Lease you are trying to save already exists. External ID should be unique'));
		}
		Test.stopTest();
	}
    
    @isTest public static void testvalidateLeases_Reconciliation() {
        
        Date lastDayOfMonth = System.today().toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                  
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,        
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                        
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = minStartDate,
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = minStartDate,
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        
        insert testAircraft;
        
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        leaseRecord.Aircraft__c = testAircraft.Id; 
        update leaseRecord;
        
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                          Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                                          Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
        LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
        Constituent_Assembly__c consituentAssemblyRecord = new Constituent_Assembly__c();
        consituentAssemblyRecord.Serial_Number__c = '2';
        consituentAssemblyRecord.CSN__c = 3;
        consituentAssemblyRecord.TSN__c = 5;
        consituentAssemblyRecord.Last_CSN_Utilization__c = 3;
        consituentAssemblyRecord.Last_TSN_Utilization__c = 5;
        consituentAssemblyRecord.Attached_Aircraft__c = testAircraft.Id;
        consituentAssemblyRecord.Name ='Testing';
        consituentAssemblyRecord.Type__c ='Airframe'; 
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert consituentAssemblyRecord ;
        LWGlobalUtils.setTriggersflagOff();
        
        Ratio_Table__c testRatioTable = new Ratio_Table__c(Name = 'Test Ratio Table');
        insert testRatioTable;
        
        Assembly_Event_Info__c assemblyEventInfoObj=new Assembly_Event_Info__c(
            Name='Airframe-Airframe-Heavy 1',
            Constituent_Assembly__c = consituentAssemblyRecord.Id
        );
        insert assemblyEventInfoObj;
        
        Assembly_MR_Rate__c assemblyMrRecord = new Assembly_MR_Rate__c();
        assemblyMrRecord.Name ='TestMrRecord';
        assemblyMrRecord.Lease__c =leaseRecord.Id;
        assemblyMrRecord.Base_MRR__c =200;
        assemblyMrrecord.Escalation_Type__c='Advanced';
        assemblyMrRecord.Rate_Basis__c='Monthly';
        assemblyMrRecord.Assembly_Type__c ='Engine 3';
        assemblyMrRecord.Assembly_Lkp__c = consituentAssemblyRecord.Id;
        assemblyMrRecord.Assembly_Event_Info__c = assemblyEventInfoObj.Id;
        assemblyMrRecord.Ratio_Table__c = testRatioTable.Id;
        assemblyMrRecord.Annual_Escalation__c = 1;
        
        insert assemblyMrRecord;  
       
        Utilization_Report__c firstUtilizationRecord = new Utilization_Report__c(
            Name='Testing 2',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Escalation_Factor_LLP__c = 2,
            Month_Ending__c = minStartDate.toStartOfMonth().addMonths(1).addDays(-1),
            Status__c='Approved By Lessor'
        );
        insert firstUtilizationRecord;
        
       // Date startDate = Date.newInstance(lastDayOfMonth.year() - 2, lastDayOfMonth.month() + 1, 1);
        //Date endDate = Date.newInstance(lastDayOfMonth.year() - 1, lastDayOfMonth.month() , Date.daysInMonth(lastDayOfMonth.year(), lastDayOfMonth.month()));
        
       
        Date startDate = Date.newInstance(lastDayOfMonth.year() - 1, lastDayOfMonth.month() + 1, 1);
        Date endDate = lastDayOfMonth;
        
        Reconciliation_Schedule__c rsObj1 = new Reconciliation_Schedule__c();
        rsObj1.Name = startDate.year()+' '+ datetime.newInstance(startDate.year(), startDate.month(),startDate.day()).format('MMM')+' to '+ endDate.addDays(-1).year() +' '+datetime.newInstance(endDate.addDays(-1).year(), endDate.addDays(-1).month(),endDate.addDays(-1).day()).format('MMM');
        rsObj1.From_Date__c = startDate;
        rsObj1.To_Date__c = endDate;
        rsObj1.Status__c = 'Open';
        rsObj1.Disable_Invoice_Auto_Generation__c = false;
        rsObj1.Lease__c = leaseRecord.Id;
        rsObj1.Asset__c = testAircraft.Id;
        rsObj1.Reconciliation_Period__c = '12';
        
        insert rsObj1;
        
        Test.startTest();
        leaseRecord.Lease_End_Date_New__c = Date.newInstance(lastDayOfMonth.year() - 1, lastDayOfMonth.month() + 1, 1);
        LeaseWareUtils.clearFromTrigger();
        update leaseRecord;
        Test.stopTest();
    }

    @isTest 
    public static void testLeaseStartAndEndDateUpdate() {

        Date StartDate = Date.newInstance(2019,03,16);

        Lessor__c testLessor = new Lessor__c(Name='TestLessor', 
				Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
				Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today()
                                     ,Utilization_Staging_Purge_After__c='2');
        insert testLessor;
        
        Operator__c testOperator = new Operator__c(
            Name = 'SA Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = StartDate,
            Lease_End_Date_New__c = StartDate.addYears(2),
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                               
            CSN__c = 1,
            Last_CSN_Utilization__c = 1.00,
            Last_TSN_Utilization__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
        
        
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  
        
        Test.StartTest();
                    
        List<Constituent_Assembly__c> constituentList = new List<Constituent_Assembly__c>();
        for(Integer i=0;i<2;i++){
            Constituent_Assembly__c consituentRecord = new Constituent_Assembly__c();
            consituentRecord.Serial_Number__c = '2';
            consituentRecord.CSN__c = 3;
            consituentRecord.TSN__c = 5;           
            consituentRecord.Last_CSN_Utilization__c = 3;
            consituentRecord.Last_TSN_Utilization__c = 5;
            consituentRecord.Attached_Aircraft__c = testAircraft.Id;
            consituentRecord.Name ='Testing';
            if (i == 0){
                consituentRecord.Type__c ='Airframe'; 
            }
            else{
                consituentRecord.Type__c ='APU'; 
            }
            
            constituentList.add(consituentRecord);
        }
        LWGlobalUtils.setTriggersflagOn(); 
        insert constituentList;
        LWGlobalUtils.setTriggersflagOff();

        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
        Name='Testing-UR-Mar',
        Airframe_Cycles_Landing_During_Month__c= 20.0,
        FH_String__c ='300',
        Aircraft__c = testAircraft.Id,
        Y_hidden_Lease__c = leaseRecord.Id,
        Type__c = 'Actual',
        Status__c='Approved By Lessor',
        Month_Ending__c = StartDate.addMonths(1).toStartOfMonth().addDays(-1)); 
        LeaseWareUtils.TriggerDisabledFlag=true;
        insert utilizationRecord;
        LeaseWareUtils.TriggerDisabledFlag=false;

        Lease__c currLease = [select id,Lease_Start_Date_New__c  from lease__c];
        currLease.Lease_Start_Date_New__c = Date.newInstance(2019,03,10);
        String unexpectedMsg;
        try{
            LeaseWareUtils.clearFromTrigger();
            update currLease; //Should fail since we are changing the lease start date after filing a utilization
            System.debug('After update');
        }catch(DmlException e){
            unexpectedMsg = e.getMessage();
        }
        system.Assert(unexpectedMsg.contains('Cannot change the start date as utilization is already reported for this period. Please delete utilization for this month before changing the date'));

        Test.StopTest();
                
    }
}