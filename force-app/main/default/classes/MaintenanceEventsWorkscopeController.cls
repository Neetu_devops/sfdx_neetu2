public without sharing class MaintenanceEventsWorkscopeController {
    
    //Purpose: Push all Maintenance Event Tasks to all associated Supplemental Rent records
	@auraEnabled
	Public static void PushToLeases(ID MPERecID){
        system.debug('PushToLeases(+)');
        
        map<id, List<Maintenance_Event_Task__c>> mapMPE_METIds = new map<id, List<Maintenance_Event_Task__c>>();  // map<Maintenance Program Event id, List<Maintenance Event Task>>      
        map<id,id> mapMPEIDEngTempID= new map<id,id>();
        //Finding all MET task for that MPE record

        List<Maintenance_Event_Task__c> ListMETRec=[select Id,name, TaskCostEventPrcntg__c,Maintenance_Program_Event__c,Maintenance_Program_Event__r.Engine_Template__c
                                                   from Maintenance_Event_Task__c 
                                                   where Maintenance_Program_Event__c =: MPERecID];
        system.debug('size of ListMETRec='+ListMETRec.size());
        if(ListMETRec.size()>0){
            for(Maintenance_Event_Task__c curMETRec : ListMETRec){
                if( !mapMPE_METIds.containsKey(curMETRec.Maintenance_Program_Event__c)){
                    mapMPE_METIds.put(curMETRec.Maintenance_Program_Event__c,new List<Maintenance_Event_Task__c>());
                }
                mapMPE_METIds.get(curMETRec.Maintenance_Program_Event__c).add(curMETRec);
                if( !mapMPEIDEngTempID.containsKey(curMETRec.Maintenance_Program_Event__c)){
                    mapMPEIDEngTempID.put(curMETRec.Maintenance_Program_Event__c,curMETRec.Maintenance_Program_Event__r.Engine_Template__c);  
                }
            }
        }       
        List<Assembly_MR_Rate__c> listUpdEngingeTempIDInSR= new List<Assembly_MR_Rate__c>();
        // push all Maintenance Event Tasks to all associated Supplemental Rent records
        map<id,Assembly_MR_Rate__c> mapSupplementalRent=new map<id,Assembly_MR_Rate__c>([select id,name, Assembly_Event_Info__r.Maintenance_Program_Event__c, Y_Hidden_EngineTemplateID__c
                                                         from Assembly_MR_Rate__c 
                                                         where RecordType.DeveloperName='MR' 
                                                         and Assembly_Event_Info__c!=Null
                                                         and Assembly_Event_Info__r.Maintenance_Program_Event__c in: mapMPE_METIds.keyset()
                                                        ]);
        system.debug('size of mapSupplementalRent='+mapSupplementalRent.size());
        try{
            if(mapSupplementalRent.size()>0){
                delete [select id, name from Supp_Rent_Event_Reqmnt__c where Supplemental_Rent__c in : mapSupplementalRent.keyset()];
            }
            
            List<Supp_Rent_Event_Reqmnt__c> listSupRentEvntReq=new List<Supp_Rent_Event_Reqmnt__c>(); 
            for(Assembly_MR_Rate__c curSuppmntlRent : mapSupplementalRent.values()){
                if(mapMPE_METIds.containsKey(curSuppmntlRent.Assembly_Event_Info__r.Maintenance_Program_Event__c)){
                    for(Maintenance_Event_Task__c curMETIds :mapMPE_METIds.get(curSuppmntlRent.Assembly_Event_Info__r.Maintenance_Program_Event__c)){
                        listSupRentEvntReq.add(new Supp_Rent_Event_Reqmnt__c(
                            Name=curMETIds.Name,
                            Supplemental_Rent__c=curSuppmntlRent.id,
                            Y_Hidden_METCostEvntPrcntg__c=curMETIds.TaskCostEventPrcntg__c,
                            Maintenance_Event_Task__c=curMETIds.id,
                            Y_Hidden_MPE_LKP__c=curMETIds.Maintenance_Program_Event__c
                        ));
                    }
                    //For internal calculation-Y_Hidden_EngineTemplateID__c used in the Supplemental Rent
                    //Re Genenrate SuppRent Event Requiremnt records,when value of Y_Hidden_EngineTemplateID__c got change. Hence Y_Hidden_EngineTemplateID__c should be sync with MPE'sEngTempID.
                    curSuppmntlRent.Y_Hidden_EngineTemplateID__c= mapMPEIDEngTempID.get(curSuppmntlRent.Assembly_Event_Info__r.Maintenance_Program_Event__c);
                    listUpdEngingeTempIDInSR.add(curSuppmntlRent);
                }
            }
            system.debug('size of listSupRentEvntReq='+listSupRentEvntReq.size());
            //system.debug('listSupRentEvntReq='+ JSON.serializePretty(listSupRentEvntReq));
            if (listSupRentEvntReq.size() > 0) Insert listSupRentEvntReq;
            LeaseWareUtils.TriggerDisabledFlag=true;
            if (listUpdEngingeTempIDInSR.size() > 0) Update listUpdEngingeTempIDInSR; //Updating only Y_Hidden_EngineTemplateID__c
            LeaseWareUtils.TriggerDisabledFlag=false;
        }
        catch(exception e){
            system.debug('Exception ' + e.getMessage());
        }
        system.debug('PushToLeases(-)');
    }
	
    //Purpose: Delete All MET record for that MPE, Once it's deleted set MPE.Engine_Template__c=null
    @AuraEnabled
    Public static void DeleteAllMET(ID MPERecID){
        system.debug('DeleteAllMET(+)='+ MPERecID);
        Maintenance_Program_Event__c curMPE=[select id, Engine_Template__c from Maintenance_Program_Event__c where id=:MPERecID];
        List<Assembly_MR_Rate__c> listUpdEngingeTempIDInSR= new List<Assembly_MR_Rate__c>();
        system.debug('curMPE='+curMPE);
        try{
            delete [select Id,name from Maintenance_Event_Task__c where Maintenance_Program_Event__c=: MPERecID];
            curMPE.Engine_Template__c=null;
            Update curMPE;
            system.debug('curMPE='+curMPE);
            //For internal calculation-Y_Hidden_EngineTemplateID__c used in the Supplemental Rent
            //Once curMPE.Engine_Template__c set null, It's means needs to be set Y_Hidden_EngineTemplateID__c as null wherever the MPE used on those Supp Rent  
            //Y_Hidden_EngineTemplateID__c should be sync with MPE's EngTempID.
            for(Assembly_MR_Rate__c curSuppRent: 
                    [select id,name, Y_Hidden_EngineTemplateID__c from Assembly_MR_Rate__c 
                     where Assembly_Event_Info__c!=Null
                     and Assembly_Event_Info__r.Maintenance_Program_Event__c =: MPERecID]){
                curSuppRent.Y_Hidden_EngineTemplateID__c=Null;
                listUpdEngingeTempIDInSR.add(curSuppRent);
            }
            LeaseWareUtils.TriggerDisabledFlag=true;
            if (listUpdEngingeTempIDInSR.size() > 0) Update listUpdEngingeTempIDInSR; //Update only Y_Hidden_EngineTemplateID__c
            LeaseWareUtils.TriggerDisabledFlag=false;
                                                  
        }
        catch(exception e){
            system.debug('Exception ' + e.getMessage());
            throw new AuraHandledException('Error Message: ' + e.getMessage() + ' at Line Number: ' + e.getLineNumber());
        }
        system.debug('DeleteAllMET(-)');
    }

    //Purpose:Refresh Tasks Creating Supp_Rent_Event_Reqmnt__c based on MPE and it's MET task
    @AuraEnabled
    Public static void RefreshTasks(ID curSuppRentID, ID MPEID){
        system.debug('RefreshTasks(+)');
        system.debug('curSuppRentID='+ curSuppRentID+ '=MPEID='+MPEID);
        try{
            Delete [select id from Supp_Rent_Event_Reqmnt__c where Supplemental_Rent__c = : curSuppRentID];
		}
        catch(exception e){
            system.debug('Exception ' + e.getMessage());
        }
		map<id,id> mapMPEIDEngTempID= new map<id,id>();
        map<id,List<Maintenance_Event_Task__c>> mapMPE_METRec = new map<id,List<Maintenance_Event_Task__c>>();
        List<Maintenance_Event_Task__c> ListMET= new List<Maintenance_Event_Task__c>();
        if(MPEID!=Null){
            ListMET=[ select id,Name,Maintenance_Program_Event__c,TaskCostEventPrcntg__c,Maintenance_Program_Event__r.Engine_Template__c from Maintenance_Event_Task__c 
                        where Maintenance_Program_Event__c =: MPEID
                    ];
        }
        if(ListMET.size()>0){
            for( Maintenance_Event_Task__c curMET: ListMET){
                if( !mapMPE_METRec.containsKey(curMET.Maintenance_Program_Event__c)){
                    mapMPE_METRec.put(curMET.Maintenance_Program_Event__c,new List<Maintenance_Event_Task__c>());
                }
                mapMPE_METRec.get(curMET.Maintenance_Program_Event__c).add(curMET);
                if( !mapMPEIDEngTempID.containsKey(curMET.Maintenance_Program_Event__c)){
                    mapMPEIDEngTempID.put(curMET.Maintenance_Program_Event__c,curMET.Maintenance_Program_Event__r.Engine_Template__c);  
                }
            }
        }
        system.debug('size of mapMPE_METRec='+mapMPE_METRec.size());
        List<Supp_Rent_Event_Reqmnt__c> listSupRentEvntReq=new List<Supp_Rent_Event_Reqmnt__c>(); 
        //Hint: Creating Supp_Rent_Event_Reqmnt__c based on MPE and it's MET task
        
        for(ID curMPEID : mapMPE_METRec.keyset()){
            for(Maintenance_Event_Task__c curMETRec: mapMPE_METRec.get(curMPEID)){ //reterive all LLPs record for that Engine
                listSupRentEvntReq.add(new Supp_Rent_Event_Reqmnt__c(
                    Name=curMETRec.Name,
                    Supplemental_Rent__c=curSuppRentID,
                    Y_Hidden_METCostEvntPrcntg__c=curMETRec.TaskCostEventPrcntg__c,
                    Maintenance_Event_Task__c=curMETRec.id,
                    Y_Hidden_MPE_LKP__c=curMETRec.Maintenance_Program_Event__c
                ));
            }
        }
        Assembly_MR_Rate__c curSuppRent=[select id,Y_Hidden_EngineTemplateID__c,Assembly_Event_Info__r.Maintenance_Program_Event__c from Assembly_MR_Rate__c where id =: curSuppRentID];
        curSuppRent.Y_Hidden_EngineTemplateID__c=mapMPEIDEngTempID.get(curSuppRent.Assembly_Event_Info__r.Maintenance_Program_Event__c);
        system.debug('size of listSupRentEvntReq='+listSupRentEvntReq.size());
        //system.debug('listSupRentEvntReq='+ JSON.serializePretty(listSupRentEvntReq));
        try{
            LeaseWareUtils.TriggerDisabledFlag=true;
            Update curSuppRent; //Update only Y_Hidden_EngineTemplateID__c
            LeaseWareUtils.TriggerDisabledFlag=false;
            if (listSupRentEvntReq.size() > 0) Insert listSupRentEvntReq;
        }
		catch(exception e){
            system.debug('Exception ' + e.getMessage());
        }
        system.debug('RefreshTasks(-)');
    }
    // Internally used in RefreshTasks
    @AuraEnabled
    Public static string GetEngineTemplateID(ID SuppRentRec){
        string MPEID;
        system.debug('SuppRentID='+SuppRentRec);
        List<Assembly_MR_Rate__c> ListSuppRent=[select Assembly_Event_Info__r.Maintenance_Program_Event__c, Assembly_Event_Info__r.Maintenance_Program_Event__r.Engine_Template__c from Assembly_MR_Rate__c where id =:SuppRentRec and RecordType.DeveloperName='MR'];
        if(ListSuppRent!=Null && ListSuppRent.size()>0) MPEID=ListSuppRent[0].Assembly_Event_Info__r.Maintenance_Program_Event__c;
        return MPEID;
    }
}