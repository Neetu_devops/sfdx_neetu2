/**
 * Class TriggerFactoryForStandard
 *
 * Used to instantiate and execute Trigger Handlers associated with sObjects.
 */
public class TriggerFactoryForStandard
{
	private static string eventType;
    /**
     * Public static method to create and execute a trigger handler
     *
     * Arguments:   Schema.sObjectType soType - Object type to process (SObject.sObjectType)
     *
     * Throws a TriggerException if no handler has been coded.
     */
    public static void createHandler(Schema.sObjectType soType)
    {
        // added this here so that even if we do not add at trigger level it will take care at this place.
        if(LeaseWareUtils.isTriggerDisabled(soType)) {return; }
        // Get a handler appropriate to the object being processed
        ITrigger handler = getHandler(soType);
         
        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
        if (handler == null)
        {
            //throw new LeaseWareException('No Trigger Handler registered for Object Type: ' + soType);
        }
         
        // Execute the handler to fulfil the trigger
        try{
			execute(handler); 
		}catch(Exception ex){
			LeaseWareUtils.createExceptionLog( ex, 'Exception in standard object: ' , String.valueOf(soType), null,eventType,true);  
		}
    }
     
    /**
     * private static method to control the execution of the handler
     *
     * Arguments:   ITrigger handler - A Trigger Handler to execute
     */
    private static void execute(ITrigger handler)
    {
        // Before Trigger
        if (Trigger.isBefore)
        {
			eventType = 'Before';
            // Call the bulk before to handle any caching of data and enable bulkification
            handler.bulkBefore();
             
            // Iterate through the records to be deleted passing them to the handler.
            if (Trigger.isDelete)
            {
				eventType = 'Before Delete';
                handler.beforeDelete();
            }
            // Iterate through the records to be inserted passing them to the handler.
            else if (Trigger.isInsert)
            {
				eventType = 'Before Insert';
                handler.beforeInsert();
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate)
            {
				eventType = 'Before Update';
                handler.beforeUpdate();
            }
        }
        else
        {
			// Call the bulk after to handle any caching of data and enable bulkification
			eventType = 'After';
            handler.bulkAfter();
             
            // Iterate through the records deleted passing them to the handler.
            if (Trigger.isDelete)
            {
				eventType = 'After Delete';
                handler.afterDelete();
            }
            // Iterate through the records inserted passing them to the handler.
            else if (Trigger.isInsert)
            {
				eventType = 'After Insert';	
				handler.afterInsert();
            }
            // Iterate through the records updated passing them to the handler.
            else if (Trigger.isUpdate)
            {
				eventType = 'After Update';	
				handler.afterUpdate();
            }
            else if (Trigger.isUnDelete )
            {
				eventType = 'After UnDelete';
                handler.afterUnDelete();
            }            
        }
         
        // Perform any post processing
        handler.andFinally();
    }
     
    /**
     * private static method to get the appropriate handler for the object type.
     * Modify this method to add any additional handlers.
     *
     * Arguments:   Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
     *
     * Returns:     ITrigger - A trigger handler if one exists or null.
     */
    private static ITrigger getHandler(Schema.sObjectType soType)
    {

        system.debug('soType---'+soType);
         /*anjani : Only for Standard Objects*/
    	   
        if(soType == EmailMessage.sObjectType){  return new EmailMessageTriggerHandler();   } 
		if(soType == User.sObjectType){  return new UserTriggerHandler(); }
        if(soType == ContentDocumentLink.sObjectType){  return new ContentDocumentLinkTriggerHandler(); }
        if(soType == ContentDocument.sObjectType){  return new ContentDocumentTriggerHandler(); }
        
        if(soType == Task.sObjectType){  return new TaskHandler(); }
        if (soType == Contact.sObjectType) return new ContactTriggerHandler();
		
        return null;
    }
}