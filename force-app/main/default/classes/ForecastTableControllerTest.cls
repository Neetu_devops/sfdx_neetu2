@isTest
public class ForecastTableControllerTest {
	@testSetup static void TestSetupMethod() {
        
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today(),
            Escalation__c = 43,
            Escalation_Month__c = 'January'
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
     
        lease.Aircraft__c = aircraft.ID;
        update lease;        

        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Rent_Escalation_Rate__c = 50;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Ext_Reserve_Type__c = 'EOLA';
        scenarioInp.Lease__c = lease.Id;
        insert scenarioInp;
        
        Scenario_Component__c scenarioComponent = new Scenario_Component__c(
       		Name = 'Test Scenario Component',
        	Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'test', 
            Type__c = 'Airframe',
            Event_Type__c = '4c/6y', 
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent;
        
        Scenario_Component__c scenarioComponent2 = new Scenario_Component__c(
       		Name = 'Test Scenario Component',
        	Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'test', 
            Type__c = 'General',
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent2;

        Scenario_Component__c scenarioComponent3 = new Scenario_Component__c(
       		Name = 'Test Scenario Component',
        	Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'test', 
            Type__c = 'Engine 1 LLP',
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent3;
        
        Monthly_Scenario__c monthlyScenario = new Monthly_Scenario__c(
        	Name = 'Test Monthly Scenario',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            CSO__c=1,
            Event_Date__c= System.today(),
            Rent_Amount__c =12, 
            Fx_Component_Output__c = scenarioComponent.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            TSO__c = 2,
            End_Date__c = System.today(),
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 20,
            FH__c = 3,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FC__c =4,
            FC_At_SV__c = 10,
            FH_At_SV__c = 5,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40,
            Claim_Month_Period__c = System.today());
        insert monthlyScenario;
        
        Monthly_Scenario__c monthlyScenario2 = new Monthly_Scenario__c(
        	Name = 'Test Monthly Scenario2',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            CSO__c=1,
            End_Date__c = System.today(),
            Event_Date__c= System.today(),
            Rent_Amount__c =11, 
            Fx_Component_Output__c = scenarioComponent2.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            TSO__c = 22,
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 220,
            FH__c = 32,
            FC__c = 14,
            FC_At_SV__c = 1,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FH_At_SV__c = 52,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40,
            Claim_Month_Period__c = System.today());
        insert monthlyScenario2;

        Monthly_Scenario__c monthlyScenario3= new Monthly_Scenario__c(
        	Name = 'Test Monthly Scenario3',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            CSO__c=1,
            End_Date__c = System.today(),
            Event_Date__c= System.today(),
            Rent_Amount__c =11, 
            Fx_Component_Output__c = scenarioComponent2.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            TSO__c = 22,
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 220,
            FH__c = 32,
            FC__c = 14,
            FC_At_SV__c = 1,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FH_At_SV__c = 52,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40,
            Claim_Month_Period__c = System.today(),
            Historical_Month__c = true);
        insert monthlyScenario3;
    }
    
    @isTest
    static void testMethod1(){
        try{
            Scenario_Input__c scenarioInp = [SELECT Id FROM Scenario_Input__c LIMIT 1];
            Test.startTest();
            ForecastTableController.isForecastGenerated(scenarioInp.Id);
            ForecastTableController.getForecastTableData(scenarioInp.Id,TRUE);
            Test.stopTest(); 
        }catch(Exception e){
            System.debug('Error in test1'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }   
    }

    @isTest
    static void testMethod2(){
        try{
            Scenario_Input__c scenarioInp = [SELECT Id FROM Scenario_Input__c LIMIT 1];
            Test.startTest();
            ForecastTableController.getForecastTableData(scenarioInp.Id,FALSE);
            Test.stopTest(); 
        }catch(Exception e){
            System.debug('Error in test2'+e.getMessage()+' __ '+e.getStackTraceString()+' Line Number: '+e.getLineNumber());
        }
        
    }
}