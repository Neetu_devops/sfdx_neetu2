/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestThrustSetting {


	
	// Test insert trigger
    static testMethod void testInsert() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A1');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP1');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
        
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        String description,lookupDescr;
        for(Integer i =1; i<6 ; i++){
        	if(i==2) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= 2500 + i*100, 
	        		Cycle_Used__c=   100 + i*50
	                );
	        listTS.add(newTS);
        }
        insert listTS;	
        // Print value 
        Thrust_Setting__c[] fetchedTS = [select Current_Thrust__c,name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
		
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : fetchedTS){
			system.debug('TestThrustSetting.testInsert : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testInsert : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testInsert : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
		}
		
        
    }
    
	// Test insert trigger : negative testcase using invalid TS lookup
    static testMethod void testInsertInvalidTS() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A1');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP1');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
        
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        String description,lookupDescr;
        for(Integer i =1; i<6 ; i++){
        	if(i==2) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= 2500 + i*100, 
	        		Cycle_Used__c=   100 + i*50
	                );
	        listTS.add(newTS);
        }
        insert listTS;	
        description = 'M1' ; lookupDescr = 'Custom_Lookup_Name.' + description ;
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  2500 ,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= 2500, 
	        		Cycle_Used__c=   100 
	                );
	        try{        
	        	insert newTS ;
        	}catch (System.DmlException e) {
        		System.debug('Expected failure : testInsertInvalidTS : insert done Invoice 2  ' + e);
        		System.assertEquals(true,e.getDmlMessage(0).contains('Invalid Thrust Setting Name'));
    	
    		}  
	        
        
    }
        
	// Test insert trigger : check with NULL
    static testMethod void testInsertNull() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A4');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP2');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
                
        
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        String description,lookupDescr;
        Decimal Cycle_Limit,Cycle_Used ;
        for(Integer i =1; i<6 ; i++){
        	if(i==2) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	if(i==5) {Cycle_Limit = null ;Cycle_Used = null;}
        	else {Cycle_Limit = 2500 + i*100 ;Cycle_Used = 100 + i*50;}
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   Cycle_Used
	                );
	        listTS.add(newTS);
        }
        insert listTS;	
        // Print value 
        Thrust_Setting__c[] fetchedTS = [select Current_Thrust__c,name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
		
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : fetchedTS){
			system.debug('TestThrustSetting.testInsert : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testInsert : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testInsert : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
		}
		
        
    }   
 
      
	// Test insert trigger : check with 0
    static testMethod void testInsertZero() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A4');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP3');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
         
        
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        String description,lookupDescr;
        Decimal Cycle_Limit,Cycle_Used ;
        for(Integer i =1; i<6 ; i++){
        	if(i==2) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	if(i==5) {Cycle_Limit = 0 ;Cycle_Used = 0;}
        	else {Cycle_Limit = 2500 + i*100 ;Cycle_Used = 100 + i*50;}
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   Cycle_Used
	                );
	        listTS.add(newTS);
        }
        insert listTS;	
        // Print value 
        Thrust_Setting__c[] fetchedTS = [select Current_Thrust__c,name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
		
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : fetchedTS){
			system.debug('TestThrustSetting.testInsert : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testInsert : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testInsert : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
		}
		
        
    }   	
	    
	// Test Update trigger
    static testMethod void testUpdate() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A4');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP3');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
         
 		LeaseWareUtils.clearFromTrigger();
        String description,lookupDescr;
        Decimal Cycle_Limit,Cycle_Used ;     
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        description = '5A1'; Cycle_Limit = 2000; Cycle_Used = 400;
		lookupDescr = 'Custom_Lookup_Name.' + description ;
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  Cycle_Limit,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   Cycle_Used
	                );		
	        listTS.add(newTS); 
        description = '5A3'; Cycle_Limit = 1000; Cycle_Used = 500;
		lookupDescr = 'Custom_Lookup_Name.' + description ;
	        Thrust_Setting__c newTS1=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  Cycle_Limit,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   Cycle_Used
	                );		
	        listTS.add(newTS1);	        			

        insert listTS;	
        // Print value 
        Thrust_Setting__c[] fetchedTS = [select name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c  where Life_Limited_Part__c = :LLPid];
		LeaseWareUtils.clearFromTrigger();
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : fetchedTS){

			system.debug('TestThrustSetting.testUpdate : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testInsert : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testUpdate : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testUpdate : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testUpdate : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testUpdate : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			if(currRec.name.contains('5A1')){
				System.assertEquals(currRec.Remaining_Cycle__c, 600);
				System.assertEquals(currRec.Weighted_Cycle_Used__c, 1400);
			}else if(currRec.name.contains('5A3')){
				System.assertEquals(currRec.Remaining_Cycle__c, 300);
				System.assertEquals(currRec.Weighted_Cycle_Used__c, 700);				
			}			
		}
		for(Thrust_Setting__c currRec : fetchedTS){
			if(currRec.name.contains('5A3')){
				currRec.Cycle_Limit__c = 1000; 
				currRec.Cycle_Used__c = 400;
				update currRec;
				
			}else if(currRec.name.contains('5A1')){
				currRec.Cycle_Limit__c = 1000; 
				currRec.Cycle_Used__c = 400; 
				update currRec;		
				
			}
			LeaseWareUtils.clearFromTrigger();			
		}


 		
		system.debug('TestThrustSetting.testUpdate : Cycle_Limit__c change , so inside call to trigger');
		// Create a Matrix based on 
        fetchedTS = [select Current_Thrust__c,name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
		
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : fetchedTS){
			system.debug('TestThrustSetting.testInsert : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testInsert : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testInsert : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
			if(currRec.name.contains('5A1')){
				System.assertEquals(currRec.Remaining_Cycle__c, 200);
				System.assertEquals(currRec.Weighted_Cycle_Used__c, 800);
			}else if(currRec.name.contains('5A3')){
				System.assertEquals(currRec.Remaining_Cycle__c, 200);
				System.assertEquals(currRec.Weighted_Cycle_Used__c, 800);				
			}			
		}
    }    

	// Test delete trigger
    static testMethod void testDelete() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A4');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP3');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
  		LeaseWareUtils.clearFromTrigger();
        
        String description,lookupDescr;
        Decimal Cycle_Limit,Cycle_Used ;     
        Thrust_Setting__c[] fetchedTS = [select Current_Thrust__c,name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
        System.assertEquals(fetchedTS.size(),1);
        
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        
        description = '5A1'; Cycle_Limit = 2000; Cycle_Used = 400;
        lookupDescr = 'Custom_Lookup_Name.' + description ;
  	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  Cycle_Limit,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   Cycle_Used
	                );		
	        listTS.add(newTS); 
        description = '5A3'; Cycle_Limit = 1000; Cycle_Used = 500;
		lookupDescr = 'Custom_Lookup_Name.' + description ;
	        Thrust_Setting__c newTS1=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  Cycle_Limit,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   Cycle_Used
	                );		
	        listTS.add(newTS1);	   
	    description = '5A4'; Cycle_Limit = 5000; Cycle_Used = 1000;
		lookupDescr = 'Custom_Lookup_Name.' + description ;
		Thrust_Setting__c newTS2 =fetchedTS[0];
		newTS2.Cycle_Limit__c = Cycle_Limit;
		newTS2.Cycle_Used__c = Cycle_Used;
	    /*    Thrust_Setting__c newTS2=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  Cycle_Limit,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   Cycle_Used
	                );		
	                */
	        listTS.add(newTS2);	  

        upsert listTS;
        LeaseWareUtils.clearFromTrigger();
		system.debug('Insert Done='+listTS.size());	
		Test.startTest();      
        // Print value 
        fetchedTS = [select Current_Thrust__c,name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
		system.debug('fetchedTS.size='+fetchedTS.size());
		// Create a Matrix based on 
		Thrust_Setting__c DeleteTS;
		for(Thrust_Setting__c currRec : fetchedTS){
			system.debug('TestThrustSetting.testInsert : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testInsert : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testInsert : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testInsert : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testInsert : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
			if(currRec.name.contains('5A4')){
				DeleteTS= currRec;
			}			
		}
		delete DeleteTS;
		LeaseWareUtils.clearFromTrigger();
		system.debug('TestThrustSetting.testDelete : Cycle_Limit__c change , so inside call to trigger');
		// Create a Matrix based on 
		fetchedTS = [select Current_Thrust__c,name,Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
		system.debug('fetchedTS.size='+fetchedTS.size());
		for(Thrust_Setting__c currRec : fetchedTS){

			system.debug('TestThrustSetting.testUpdate : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testUpdate : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testUpdate : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testUpdate : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testUpdate : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			if(currRec.name.contains('5A1')){
				System.assertEquals(currRec.Remaining_Cycle__c, 600);
				System.assertEquals(currRec.Weighted_Cycle_Used__c, 1400);
			}else if(currRec.name.contains('5A2')){
				System.assertEquals(currRec.Remaining_Cycle__c, 300);
				System.assertEquals(currRec.Weighted_Cycle_Used__c, 700);				
			}			
		}
		Test.stopTest();     
    }   

	// Create UR :  negative testcase if Current TS is not present 
    static testMethod void testURCreationUsingTS() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A4');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        
        mapInOut.put('Sub_Components_LLPs__c.Override_Life_Limit_Cycle__c',1000);
        mapInOut.put('Sub_Components_LLPs__c.Override_Cycles_Used__c',100);
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP3');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
  		String LLPName1 =(string)mapInOut.get('Sub_Components_LLPs__c.Name');
        
        String description,lookupDescr;
        Decimal Cycle_Limit,Cycle_Used ;     
        LeaseWareUtils.clearFromTrigger();
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        for(Integer i =1; i<6 ; i++){
        	if(i==2 || i==4) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	if(i==5) {Cycle_Limit = 0 ;Cycle_Used = 0;}
        	else {Cycle_Limit = 2500 + i*100 ;Cycle_Used = 100 + i*50;}
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   0
	                );
	        listTS.add(newTS);
        }
        insert listTS;	     
		LeaseWareUtils.clearFromTrigger();
		Test.startTest();
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : listTS){
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
	
		}
		// 2nd LLP 	
		mapInOut.put('Sub_Components_LLPs__c.Part_Number__c','LLP4');
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP4');
        TestLeaseworkUtil.createLLP(mapInOut);
        LeaseWareUtils.clearFromTrigger();
        ID LLPid2=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
   		String LLPName2 =(string)mapInOut.get('Sub_Components_LLPs__c.Name');
        
        list<Thrust_Setting__c>  listTS2 = new list<Thrust_Setting__c>();
	
        for(Integer i =1; i<6 ; i++){
        	if(i==2 || i==4) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	if(i==5 ) {Cycle_Limit = 0 ;Cycle_Used = 0;}
        	else {Cycle_Limit = 2400 + i*100 ;Cycle_Used = 100 + i*50;}
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid2, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   0
	                );
	        listTS2.add(newTS);
        }
        insert listTS2;	     
	
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : listTS2){
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
	
		}    
		
		// create UR
       		Date MonthEndingDate = date.newinstance(2015, 8, 31);
	       	MonthEndingDate = MonthEndingDate.addMonths(1).toStartOfMonth().addDays(-1);
	       	System.debug(' MonthEndingDate' + MonthEndingDate);
	       	//String fetchedAisrcraftId = fetchedAircraft[0].Id ;
			
	       	Utilization_Report__c testURForCMR2 = new Utilization_Report__c(
	       		Name= 'thisURForCMR', Type__c = 'Actual',Aircraft__c=(string)mapInOut.get('Aircraft__c.Id'), FH_String__c='100:35',
	       		 
	             Airframe_Cycles_Landing_During_Month__c=220, Month_Ending__c= MonthEndingDate  
	                    //,True_Up__c=true
	                    , Status__c='Open'
	             );
	        try{     
	        	insert testURForCMR2;
	        }catch (System.DmlException e) {
        		System.debug('Expected failure : testMRCreationUsingTS : insert UR  ' + e);
        		System.assertEquals(true,e.getDmlMessage(0).contains('does not have Current Thrust Setting'));
    			return ;
    		}  
	        System.debug(' testURForCMR2 created ' );
            Test.stopTest();
	
		
    }   
    
	// Create UR  +ve testcase  
    static testMethod void testMRCreationUsingTS2() {
        // TO DO: implement unit test
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut);  
    	
        TestLeaseworkUtil.createTSLookup(mapInOut);
        mapInOut.put('Constituent_Assembly__c.Current_TS','5A4');
        mapInOut.put('Constituent_Assembly__c.Name','12345');
        TestLeaseworkUtil.createAssembly(mapInOut);
        
        mapInOut.put('Sub_Components_LLPs__c.Override_Life_Limit_Cycle__c',1000);
        mapInOut.put('Sub_Components_LLPs__c.Override_Cycles_Used__c',100);        
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP3');
        mapInOut.put('Sub_Components_LLPs__c.Part_Number__c','LLP3');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
  		String LLPName1 =(string)mapInOut.get('Sub_Components_LLPs__c.Name');
        
        String description,lookupDescr;
        Decimal Cycle_Limit,Cycle_Used ;     
        Test.startTest();
        list<Thrust_Setting__c>  listTS = new list<Thrust_Setting__c>();
        for(Integer i =1; i<6 ; i++){
        	if(i==2||i==4) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	if(i==5) {Cycle_Limit = 0 ;Cycle_Used = 0;}
        	else {Cycle_Limit = 2500 + i*100 ;Cycle_Used = 100 + i*50;}
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   0
	                );
	        listTS.add(newTS);
        }
        insert listTS;	     
	
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : listTS){
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
	
		}
		// 2nd LLP 	
        mapInOut.put('Sub_Components_LLPs__c.Name','LLP4');
        mapInOut.put('Sub_Components_LLPs__c.Part_Number__c','LLP4');
        TestLeaseworkUtil.createLLP(mapInOut);
        ID LLPid2=(string)mapInOut.get('Sub_Components_LLPs__c.Id');
   		String LLPName2 =(string)mapInOut.get('Sub_Components_LLPs__c.Name');
        
        list<Thrust_Setting__c>  listTS2 = new list<Thrust_Setting__c>();

        for(Integer i =1; i<6 ; i++){
        	if(i==2 ||i==4) continue;
        	description = '5A' +i ; lookupDescr = 'Custom_Lookup_Name.' + description ;
        	if(i==5 ) {Cycle_Limit = 0 ;Cycle_Used = 0;}
        	else {Cycle_Limit = 2400 + i*100 ;Cycle_Used = 100 + i*50;}
	        Thrust_Setting__c newTS=new Thrust_Setting__c(
	        		Life_Limited_Part__c=LLPid2, 
	        		Name=description,
	        		Thrust__c =  2500 + i*100,
	        		Thrust_Setting_Name__c =(string)mapInOut.get(lookupDescr),
	        		Cycle_Limit__c= Cycle_Limit, 
	        		Cycle_Used__c=   0
	                );
	        listTS2.add(newTS);
        }
        insert listTS2;	     
		
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : listTS2){
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Life_Limited_Part__c'+ currRec.Life_Limited_Part__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
	
		}    
		
		// create MR
       		Date MonthEndingDate = date.newinstance(2015, 8, 31);
	       	MonthEndingDate = MonthEndingDate.addMonths(1).toStartOfMonth().addDays(-1);
	       	System.debug(' MonthEndingDate' + MonthEndingDate);
	       	//String fetchedAisrcraftId = fetchedAircraft[0].Id ;
			
	       	Utilization_Report__c testURForCMR2 = new Utilization_Report__c(
	       		Name= 'thisURForCMR', Type__c = 'Actual',Aircraft__c=(string)mapInOut.get('Aircraft__c.Id'), FH_String__c='100:35',
	       		 
	             Airframe_Cycles_Landing_During_Month__c=220, Month_Ending__c= MonthEndingDate  
	                    //,True_Up__c=true
	                    , Status__c='Open'
	             );
	        try{     
	        	insert testURForCMR2;
	        }catch (System.DmlException e) {
        		System.debug('Expected failure : testMRCreationUsingTS : insert UR  ' + e);
        		System.assertEquals(true,e.getDmlMessage(0).contains('does not have Current Thrust Setting'));
    			return ;
    		}  
	        System.debug(' testURForCMR2 created ' );
	        LeaseWareUtils.unsetTriggers('dummy');
	        LeaseWareUtils.clearFromTrigger();
        
	        testURForCMR2.status__c = 'Approved By Lessor';
	        
			update testURForCMR2;
			
			
						
		Utilization_Report_List_Item__c[] asmblyURs = [select Constituent_Assembly__c, Cycles_During_Month__c, Running_Hours_During_Month__c from Utilization_Report_List_Item__c where Utilization_Report__c = :testURForCMR2.Id];
                for(Utilization_Report_List_Item__c curAUR: asmblyURs){
                    system.debug('TestThrustSetting.testMRCreationUsingTS : curAUR.Running_Hours_During_Month__c'+ curAUR.Running_Hours_During_Month__c);
                    system.debug('TestThrustSetting.testMRCreationUsingTS : curAUR.curAUR.Cycles_During_Month__c'+ curAUR.Cycles_During_Month__c);
                    //asmblyURs.Running_Hours_During_Month__c = 
                    
                }
        Thrust_Setting__c[] fetchedTS = [select Current_Thrust__c,name,Thrust_Setting_Name__r.name,Life_Limited_Part__r.name, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c = :LLPid ];
		Test.stopTest();
		// Create a Matrix based on 
		for(Thrust_Setting__c currRec : fetchedTS){
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Life_Limited_Part__c.name'+ currRec.Life_Limited_Part__r.name);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Description'+ currRec.Name);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Thrust_Setting_Name__r.name'+ currRec.Thrust_Setting_Name__r.name);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Limit__c'+ currRec.Cycle_Limit__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Cycle_Used__c'+ currRec.Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Remaining_Cycle__c'+ currRec.Remaining_Cycle__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Weighted_Cycle_Used__c'+ currRec.Weighted_Cycle_Used__c);
			system.debug('TestThrustSetting.testMRCreationUsingTS : currRec.Current_Thrust__c'+ currRec.Current_Thrust__c);
			if('5A4'.equals(currRec.Thrust_Setting_Name__r.name)){
                // anjani: will check assertion later
				//system.assertEquals(100,currRec.Cycle_Used__c);// not sure with value
			}
		}			
		
    }       
    
}