public class DrawnDownAmountConfigurationController{

    @AuraEnabled
    public static void saveDrawnDownAmountRecord(Id locdrawndownId,Id locrecordId,Decimal amount,String name){
        LOC_Drawn_Down__c ldd=new LOC_Drawn_Down__c(Id=locdrawndownId);
        ldd.name=name;
        ldd.Amount__c=amount;
		if(locdrawndownId == null){
			ldd.Letter_Of_Credit__c=locrecordId;
        }
		upsert ldd;
    }

    @AuraEnabled
    public static LOCDrawnDownContainerWrapper getDrawnDownAmountRecords(Id recordId){
        List<DrawnDownAmountWrapper> drawnDownAmountWrapperlist = new List<DrawnDownAmountWrapper>();
        for(LOC_Drawn_Down__c lddObj:[select id,name,amount__c,locked__c,Approved_On__c from
                                      LOC_Drawn_Down__c where Letter_Of_Credit__c=:recordId order by name ]){
                                          DrawnDownAmountWrapper ddA=new DrawnDownAmountWrapper(lddObj.id,lddObj.name,lddObj.amount__c,lddObj.locked__c,lddObj.Approved_On__c);
                                          drawnDownAmountWrapperlist.add(ddA);
                                      }
        if(drawnDownAmountWrapperlist.size()==0){
            drawnDownAmountWrapperlist.add(new DrawnDownAmountWrapper(null,'LoC Amount Drawn Down#'+(drawnDownAmountWrapperlist.size()+1),null,false,null));
        }
        else if(drawnDownAmountWrapperlist[drawnDownAmountWrapperlist.size()-1].locked && drawnDownAmountWrapperlist.size()<3){
            drawnDownAmountWrapperlist.add(new DrawnDownAmountWrapper(null,'LoC Amount Drawn Down#'+(drawnDownAmountWrapperlist.size()+1),null,false,null));
        }
        Letter_Of_Credit__c locObj=[select LC_Amount_LC_Currency__c from Letter_Of_Credit__c where id=:recordId];
        LOCDrawnDownContainerWrapper containerWrapperObj=new LOCDrawnDownContainerWrapper(locObj.LC_Amount_LC_Currency__c,drawnDownAmountWrapperlist);
        return containerWrapperObj;
    }
    public class DrawnDownAmountWrapper{
        @AuraEnabled 
        public Id id{get;set;}
        @AuraEnabled 
        public string name{get;set;}
        @AuraEnabled 
        public decimal amount{get;set;}
        @AuraEnabled 
        public boolean locked{get;set;}
        @AuraEnabled 
        public datetime approvedOn{get;set;}
        public DrawnDownAmountWrapper(Id id,string name,decimal amount,boolean locked,datetime approvedOn){
            this.id=id;
            this.name=name;
            this.amount=LeaseWareUtils.zeroIfNull(amount);
            this.locked=locked;
            this.approvedOn=approvedOn;
        }
    }
    public class LOCDrawnDownContainerWrapper{
        @AuraEnabled
        public decimal remainingAmount{get;set;}
        @AuraEnabled
        public list<DrawnDownAmountWrapper> drawnDownAmountRecords{get;set;}
        
        public LOCDrawnDownContainerWrapper(decimal remainingAmount,list<DrawnDownAmountWrapper> drawnDownAmountRecords){
            
            this.remainingAmount=remainingAmount;
            this.drawnDownAmountRecords=drawnDownAmountRecords;
        }
        
    }
}