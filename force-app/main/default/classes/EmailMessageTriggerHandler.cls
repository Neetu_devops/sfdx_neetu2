public class EmailMessageTriggerHandler implements ITrigger
{

    private static map<string, Customer_Interaction_Log__c> mapExistingILs;
    private static boolean bIsThreadingOFF=false;
    private static boolean bIsThreadingOnSubjectOn=false;

    public EmailMessageTriggerHandler()
    {
        string strThreading = LeaseWareUtils.getLWSetup_CS('Email Threading In Integration');
        bIsThreadingOFF = 'OFF'.equalsIgnoreCase(strThreading);
        strThreading = LeaseWareUtils.getLWSetup_CS('Email Threading On Subject');
        bIsThreadingOnSubjectOn = 'ON'.equalsIgnoreCase(strThreading);
    }
    
    /**
* bulkBefore
*
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore()
    {
    }
    
    public void bulkAfter()
    {
        if(bIsThreadingOFF || !trigger.isInsert)return; //Code below this is needed only if threading is on and only during insert.

        set<string> setThreadIdsToLookFor = new set<string>();
        set<string> setEmailSubjectsToLookFor = new set<string>();

        for(EmailMessage curEmailMsg : (list<EmailMessage>)trigger.new){
            setThreadIdsToLookFor.add(curEmailMsg.ThreadIdentifier);
            setEmailSubjectsToLookFor.add(getCleanSubj(curEmailMsg.subject));
        }

        for(Customer_Interaction_Log__c curIL: [select id, Summary__c, Meeting_Notes__c, Email_Thread_Id__c, Related_Email_Id__c, Email_Subject__c, 
                Related_Email_Date__c, Date_Of_Call__c
                from Customer_Interaction_Log__c 
                where Email_Thread_Id__c != null AND (Email_Thread_Id__c in :setThreadIdsToLookFor
                        OR Email_Subject__c in :setEmailSubjectsToLookFor)]){
                    if(mapExistingILs==null)mapExistingILs = new map<string, Customer_Interaction_Log__c>();
                    mapExistingILs.put(curIL.Email_Thread_Id__c, curIL);
                    mapExistingILs.put(curIL.Email_Subject__c, curIL);
        }
    }
    
    public void beforeInsert()
    {   
    }
    
    public void beforeUpdate()
    {
    }
    
    public void beforeDelete()
    {  
    }
    
    public void afterInsert()
    {
        system.debug('EmailMessageTriggerHandler.afterInsert(+)');
        if(LeaseWareUtils.isFromTrigger('DO_NOT_CREATE_IL')) return;
        List<EmailMessage> newList  = (list<EmailMessage>)trigger.new;
        try {
            // Handle insertion = create new Interaction Log under same object the EmailMessage is attached to...
            // only IFF a proper record to attach it to is found, else do NOT fail as EmailMessages could be created not only from
            // the Outlook integration but from other functions!
            List<Customer_Interaction_Log__c> interactionLogsToUpsert = new List<Customer_Interaction_Log__c>();
            list<Related_Email__c> listRelEmailsToInsert = new list<Related_Email__c>();
            String customSettingValue = LeaseWareUtils.getLWSetup_CS('AUTO IL FOR NEW EMAILS');
            if(String.isEmpty(customSettingValue)){
                customSettingValue = 'ONLY WITH RELATED TO';
            }

            for (EmailMessage email : newList) {
                string strCleanSubj = getCleanSubj(email.Subject);
                if(!bIsThreadingOFF & mapExistingILs!=null){//Only if email threading is on and there are existing ILs with thread id present
                    Customer_Interaction_Log__c existingIL = mapExistingILs.get(email.ThreadIdentifier);
                    if(existingIL == null && bIsThreadingOnSubjectOn)existingIL = mapExistingILs.get(strCleanSubj); //Check if an email with the same subject exists.
                    if(existingIL != null){//IL exists for this thread
                        listRelEmailsToInsert.add(new Related_Email__c(Name = email.Subject, Interaction_Log__c = existingIL.id, Message_Date__c = email.MessageDate,
                                Related_Email_Id__c = email.id, Related_Email__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + email.Id));
                        if(existingIL.Related_Email_Date__c < email.MessageDate){
                            existingIL.Related_Email_Id__c = email.id;
                            existingIL.Summary__c = email.Subject; // So some text appears for ILs in the Related List view of parent object
                            existingIL.Date_Of_Call__c = email.MessageDate==null?System.today():email.MessageDate.dateGMT();
                            existingIL.Related_Email_Date__c = email.MessageDate;
                            existingIL.Meeting_Notes__c = email.HtmlBody;
                            interactionLogsToUpsert.add(existingIL);
                        }
                        continue;
                    }
                }
                
                // When the email is attached to a Contact, email.RelatedToId is null. This scenario among standard objects is handled automatically by Salesforce,
                // and emails attached to Contacts appear in the Activity feed of the contact (which hence must be exposed!)
                // ... but there is no way to identify that Contact and create an IL to link to it! So in this scenario, emails (not ILs) are directly attached to Contacts
                if ('OFF'.equals(customSettingValue) || ('ONLY WITH RELATED TO'.equals(customSettingValue) && email.RelatedToId == null ))
                    break;

                // Skip IL creation if setting value is "Always" and current user belongs to LW_Admin Group 
                if('ALWAYS'.equals(customSettingValue) && !email.Incoming ){

                    String[] LWAdminGrpEmailList =new String[]{}; 
                    String[] strMsg=new String[]{};
                    LeaseWareUtils.getEmailsFromGroup( 'LW_Admin_Group','Regular', LWAdminGrpEmailList,strMsg);

                    if(LWAdminGrpEmailList.contains(Userinfo.getUserEmail())) { 
                        break;
                        }
                }
                
                // So the new IL appears under the related list of its parent object, we have to fill the appropriate RelatedTo
                // field. Instead of hard-coding which RelatedTo field to use depending on the type of object the Email is attached to
                // (Operator, Aircraft, etc...) we search all RelatedTo fields in the definition of Interaction Log,
                // and use the one where the types match...
                // ... EXCEPT for Operators for which this doesn't work because there are SEVERAL "Related_To", so need to hard code
                // for that one first
                string relatedToFieldName;
                Map<String, Schema.SObjectField> ILfields = Customer_Interaction_Log__c.sObjectType.getDescribe().fields.getMap();
                for(Schema.sObjectField ILfield : ILfields.values()) {
                    Schema.DescribeFieldResult dfield = ILfield.getDescribe();
                    string strFieldLocalName = dfield.getLocalName();
                    //Skip to the next field if the current field contains _1__c, _2__c etc. IL cannot be created without setting 
                    //one of the primary related to field like Operator, Lessor, Counterparty, Asset, Deal etc.
                    if(!strFieldLocalName.equals(strFieldLocalName.replaceFirst('\\_[0-9]*\\_\\_c', '\\_\\_c')))continue;
                    // First check if the email is related to an Operator and the field I am in is the ONE reference to Operator I am interested in
                    if(email.RelatedToId!=null){
                        if ((email.RelatedToId.getSObjectType().getDescribe().getLocalName() == 'Operator__c' 
                             && strFieldLocalName == 'Related_To_Operator__c') ||
                            (email.RelatedToId.getSObjectType().getDescribe().getLocalName() == 'Counterparty__c' 
                             && strFieldLocalName == 'Counterparty__c')) {
                                 relatedToFieldName = strFieldLocalName;
                                 break;           
                             }
                        // Else, if this is NOT any of the other "Related_To_Operator" fields, check if it's the one where the type of the lookup
                        // match the type of the object the email is attached to
                        else if (!strFieldLocalName.Contains('Related_To_Operator') && 
                                 strFieldLocalName.Contains('Related_To') &&
                                 String.ValueOf(dfield.getType()) == 'Reference' && // Lookup type
                                 !dfield.isNamePointing() && // Lookup to only one object type
                                 dfield.getReferenceTo()[0].getDescribe().getLocalName() ==
                                 email.RelatedToId.getSObjectType().getDescribe().getLocalName()) // Type is the same as what the email is attached to
                        {
                            relatedToFieldName = strFieldLocalName;
                            break;
                        }
                    }        
                }    
                
				if(email.RelatedToId!=null && relatedToFieldName == null)// Skip IL creation if RelatedTo field is not found 
				  {
                    break;
				  } 	
                    
                // If a RelatedTo field has been found, then create the IL and insert it 

                system.debug('Email Attribs - ' +  email.ThreadIdentifier + ' - ' + email.MessageIdentifier + ' - '+ email.Headers + ' - ' + email.ReplyToEmailMessageId );
                system.debug('Email Message - ' + email);
                                
                Customer_Interaction_Log__c newIL = new Customer_Interaction_Log__c();
                newIL.Type_Of_Interaction__c = 'Email';
                system.debug('Email Subject----------------'+ email.Subject);
                if(String.isNotBlank(email.Subject) && email.Subject.length() > 80){
                    newIL.Name = email.Subject.left(77) + '...' ;
                }else{
                    newIL.Name = email.Subject; // Name field on IL is actually labeled as the Subject
                }
                newIL.Related_Email_Id__c = email.id;
                newIL.Summary__c = email.Subject; // So some text appears for ILs in the Related List view of parent object
                newIL.Email_Subject__c = strCleanSubj;
                newIL.Date_Of_Call__c = email.MessageDate==null?System.today():email.MessageDate.dateGMT();
                newIL.Related_Email_Date__c = email.MessageDate;
                newIL.Meeting_Notes__c = email.HtmlBody;
                newIL.Related_Email__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + email.Id;
                newIL.Email_Thread_Id__c = (bIsThreadingOFF?(email.MessageIdentifier!=null?email.MessageIdentifier:string.valueOf(datetime.now())):email.ThreadIdentifier).left(255); //Thread Id must be unique and should have different values when threading is off.

                //Below record is created only to set the external id on the child record to be inserted.
                //Ref : https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/langCon_apex_dml_foreign_keys.htm
                //Creating Parent and Child Records in a Single Statement Using Foreign Keys
                Customer_Interaction_Log__c newILRef = new Customer_Interaction_Log__c(Email_Thread_Id__c = newIL.Email_Thread_Id__c);
                listRelEmailsToInsert.add(new Related_Email__c(Name = email.Subject, Interaction_Log__r = newILRef, Message_Date__c = email.MessageDate,
                        Related_Email_Id__c = email.id, Related_Email__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + email.Id));

                // Copy in the new IL entry, for the found RelatedTo field, the link to the same parent object as the email is attached to
                system.debug('email.RelatedToId--------'+email.RelatedToId);
                if(email.RelatedToId!=null && relatedToFieldName!= null){
                    system.debug('Related To Field Name----'+relatedToFieldName );
                    newIL.put(relatedToFieldName, email.RelatedToId);
                    system.debug('newIL----'+newIL);
                }
                interactionLogsToUpsert.add(newIL);
            }
            if(interactionLogsToUpsert!=null && interactionLogsToUpsert.size()>0)
                upsert interactionLogsToUpsert;
            if(listRelEmailsToInsert!=null && listRelEmailsToInsert.size()>0)
                insert listRelEmailsToInsert;
        }
        catch (DmlException e) {
            
            system.debug('Exception on EmailMessageTriggerHandler: ' + e);
            String exMessage = e.getMessage();
            String customValidationString = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
            if(exMessage.contains(customValidationString)){
                Integer index1 = exMessage.lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                Integer index2 = exMessage.indexOf(': [');
                exMessage= exMessage.substring(index1+customValidationString.length()+1,index2);
                system.debug('exMessage---'+exMessage);
            }
			newList[0].addError('An Interaction Log could not be created for a new email. Error: '+exMessage);
            
            return;
        }
        
		catch (Exception e){
                system.debug('Exception ' + e.getMessage());
                leasewareutils.createExceptionLog(e, 'Exception while creating Interaction Log for new email', null, null,null,true);
            }         
               
        
        system.debug('EmailMessageTriggerHandler.afterInsert(-)');      
    }
    
    public void afterUpdate()
    {   
    }
    
    public void afterDelete()
    {
        system.debug('EmailMessageTriggerHandler.afterDelete(+)');
        
        set<string> setEmailIdsToDel = new set<string>();
        for(EmailMessage curEmail : (list<EmailMessage>)trigger.old){
            setEmailIdsToDel.add(curEmail.id);
        }

      	try{	
            list<Related_Email__c> listRelEmails = [SELECT Id, Interaction_Log__c FROM Related_Email__c 
                                    WHERE Related_Email_Id__c in :setEmailIdsToDel];
            set<id> setILIds = new set<id>();
            for(Related_Email__c curRelEmail : listRelEmails){
                setILIds.add(curRelEmail.Interaction_Log__c);
            }
			if(listRelEmails.size()>0){
				delete listRelEmails;
		    }
            List<Customer_Interaction_Log__c> listILsToDel = new List<Customer_Interaction_Log__c>();
            List<Customer_Interaction_Log__c> listILsToUpdate = new List<Customer_Interaction_Log__c>();
            for(Customer_Interaction_Log__c curIL : [select id, Related_Email_Id__c, Email_Subject__c, Related_Email_Date__c, Summary__c, Date_Of_Call__c, Meeting_Notes__c,
                         (select id, Related_Email_Id__c from Related_Emails__r order by Message_Date__c desc) 
                            from Customer_Interaction_Log__c where id in :setILIds]){
                if(curIL.Related_Emails__r.size()>0){//This is not the last Email associated to this IL.
                     string strCurRelEmailId = curIL.Related_Emails__r[0].Related_Email_Id__c;
                    if(!strCurRelEmailId.equals(curIL.Related_Email_Id__c)){//Latest email in the chain has been deleted. Update the IL with the most recent one available now.
                        EmailMessage email = [select id, Subject, HtmlBody, MessageDate from EmailMessage where id = :strCurRelEmailId limit 1];
                        curIL.Related_Email_Id__c = email.id;
                        curIL.Summary__c = email.Subject; // So some text appears for ILs in the Related List view of parent object
                        curIL.Date_Of_Call__c = email.MessageDate==null?System.today():email.MessageDate.dateGMT();
                        curIL.Related_Email_Date__c = email.MessageDate;
                        curIL.Meeting_Notes__c = email.HtmlBody;
                        listILsToUpdate.add(curIL);
                    }
                }else{//No more associated emails exist. Delete the IL.
                    listILsToDel.add(curIL);
                }
            }
            if(listILsToDel.size()>0){
                delete listILsToDel;
            }
            if(listILsToUpdate.size()>0){
                update listILsToUpdate;
            }
        }catch (Exception e){
                system.debug('Exception ' + e.getMessage());
                leasewareutils.createExceptionLog(e, 'Exception while deleting Interaction Log', null, null,null,true);
        }	
        
        system.debug('EmailMessageTriggerHandler.afterDelete(-)');      
    }
    
    public void afterUnDelete()
    {
    }
    
    /**
* andFinally
*
* This method is called once all records have been processed by the trigger. Use this
* method to accomplish any final operations such as creation or updates of other records.
*/
    public void andFinally()
    {
        // insert any audit records
    }

    private string getCleanSubj(string strSubj){
        if(strSubj==null)return '';

        string strStrippedSubj = strSubj.removeStartIgnoreCase('RE:').removeStartIgnoreCase('FW:').removeStartIgnoreCase('Fwd:');
        if(strStrippedSubj.equals(strSubj))return strStrippedSubj.trim().left(255);
        return getCleanSubj(strStrippedSubj); 
   }

    
}