public class TestNewsReportMockHttpResGen implements HttpCalloutMock{
    
       // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        
        // Create a dummy response
        HttpResponse res ;
        if(req.getEndpoint().endswith('profiles?term=TestOperator')){
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"data":[{"rank":1,"id":503,"type":{"id":1,"name":"Airline"},"iata":"QF","icao":"QFA","name":"TestOperator","url":"https://centreforaviation.com/data/profiles/airlines/TestOperator-airways-qf","country":{"name":"Australia","code":"AU"},"image":"https://images.cdn.centreforaviation.com/profiles/1-503/logo.png?1477525965"}]}');
            return res; 
        //}else if(req.getEndpoint().endswith('/news?profiletype=1&profileid=503')){
        }else if(req.getEndpoint().endswith('&types=1%2C4%2C6')){
            System.debug('MOCK 2-------');
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/json');

            String responstr = '{"data":[{"id":532754,"price":null,"ad":1,"featured":0,"timestamp":1596775440,'
            +'"title":"Airports: Groupe ADP outlines plans based on results","description":"",'
            +'"keypoints":null,"abstract":"<p>First half 2020 TestOperator financial results are being published by airport operators, including for TestOperator, the Paris airports operator with additional interests in West Asia, the Middle East and elsewhere.</p>\r\n<p>Those results have inevitably been affected adversely by the pandemic, but among the minus signs there is some evidence that some business segments have been less severely impacted than others.</p>\n<p>Among them are commercial revenues from retail concessions, which at some airports in Europe were quick to get going again. Data released by the Spanish operator AENA also adds weight to that theory. In AENAs case some retail activities actually gained in revenues for the operator .</p>",'
            +'"premium":1,"source":"NA","url":"https://centreforaviation.com/analysis/reports/airports-groupe-adp-outlines-plans-based-on-1h2020-results-532754",'
            +'"image":"https://images.cdn.centreforaviation.com/stories/ADP.jpg",'
            +'"tags":["france","germany","aig","airport retail","airport concessions","aena aeropuertos internacional","groupe adp","tav airports","aena","covid 19","pandemic"],'
            +'"type":{"id":1,"name":"Analysis"},"words":2802}]}';

            String replaceIllegal= responstr.replaceAll('\n','').replaceAll('\r','');
            
            res.setBody(replaceIllegal);

            return res; 
        }else if(req.getEndpoint().endswith('/articles?num=10&page=1&types=1,4,6')){
            System.debug('MOCK 3-------');
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/json');
            String responstr = '{"data":[{"id":532754,"price":null,"ad":1,"featured":0,"timestamp":1596775440,'
            +'"title":"Airports: Groupe ADP outlines plans based on results","description":"",'
            +'"keypoints":null,"abstract":"<p>First half 2020 TestOperator financial results are being published by airport operators, including for TestOperator, the Paris airports operator with additional interests in West Asia, the Middle East and elsewhere.</p>\r\n<p>Those results have inevitably been affected adversely by the pandemic, but among the minus signs there is some evidence that some business segments have been less severely impacted than others.</p>\n<p>Among them are commercial revenues from retail concessions, which at some airports in Europe were quick to get going again. Data released by the Spanish operator AENA also adds weight to that theory. In AENAs case some retail activities actually gained in revenues for the operator .</p>",'
            +'"premium":1,"source":"NA","url":"https://centreforaviation.com/analysis/reports/airports-groupe-adp-outlines-plans-based-on-1h2020-results-532754",'
            +'"image":"https://images.cdn.centreforaviation.com/stories/ADP.jpg",'
            +'"tags":["france","germany","aig","airport retail","airport concessions","aena aeropuertos internacional","groupe adp","tav airports","aena","covid 19","pandemic"],'
            +'"type":{"id":1,"name":"Analysis"},"words":2802}]}';

            String replaceIllegal= responstr.replaceAll('\n','').replaceAll('\r','');
            
            res.setBody(replaceIllegal);
            
            return res; 
        }
        return res;
    }      
}