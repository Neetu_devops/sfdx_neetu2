/**

Name    :    BidSummaryController
Desc    :    To show Bid summary according to prospects for various aircrafts in a project
Date    :    May 17, 2018
Dev     :    

**/    

public with sharing class BidSummaryController{
    
    @auraEnabled
    public static AircraftContainer getAircraftData(Id projectId){
        
        AircraftContainer container = new AircraftContainer();
        
        String prefix = LeaseWareUtils.getNamespacePrefix();
        
        //Campaign Status
        container.projectStatusOptions = Utility.getPicklistvalues(prefix + 'Aircraft_Proposal__c', prefix + 'Project_Status__c');
        
        //get all editable currency type fields.
        Map <String, Schema.SObjectField> fieldMap =  Aircraft_Proposal__c.sObjectType.getDescribe().fields.getMap();
        Set<String> allEditableCurrencyFields =  getAllCurrencyFields(fieldMap);
        
        //get the fields from field set and allow only editable currency type fields.
        for(Schema.FieldSetMember fieldsetMember : readFieldSet(prefix + 'Currency_Fields_For_Bid_Matrix', prefix + 'Aircraft_Proposal__c')){
            
            if(allEditableCurrencyFields.contains(fieldsetMember.getFieldPath())){
                container.currencyFieldList.add(new FieldWrapper(fieldsetMember.getFieldPath(),fieldsetMember.getLabel(), String.valueOf(fieldsetMember.getType()),fieldMap.get(fieldsetMember.getFieldPath()).getDescribe().getScale()));
            }
        } 
        
        //get the fields from field set of Marketing activity to be edited.
        for(Schema.FieldSetMember fieldsetMember : readFieldSet(prefix + 'Fields_For_Editing_On_Bid_Matrix', prefix + 'Marketing_Activity__c')){
            
            container.marketingActivityFieldList.add(fieldsetMember.getFieldPath());
        } 
        
        //get the fields from field set of Asset to be edited.
        for(Schema.FieldSetMember fieldsetMember : readFieldSet(prefix + 'Fields_For_Editing_On_Bid_Matrix', prefix + 'Aircraft__c')){
            
            container.aircraftFieldList.add(fieldsetMember.getFieldPath());
        } 
        
        
        Map<Id,ProspectWrapper> prospectInfoMap = new Map<Id,ProspectWrapper> ();
        Map<Id, AircraftWrapper> aircraftInfoMap = new Map<Id,AircraftWrapper>();
        String gainlossfieldapiName;
        String fieldSetforEditingAssetInDeal;
        String aircraftQry = 'Select Id, MSN_Number__c, recordTypeId' ;
        
        if(projectId != null){
            
            List<Project__c> projectList = [select id,Fields_to_Keep_on_Collapse__c, inactive__c,Bid_Field__c, Project_Deal_Type__c, project_status_filter__c, Number_of_Max_Bids_Bidders_to_Show__c 
                                            from project__c where id =: projectId];
            
            String apiName ;
            Integer Number_of_Max_Bids = 0;
            
            if(projectList.SIZE()>0){
			
				Number_of_Max_Bids = projectList[0].Number_of_Max_Bids_Bidders_to_Show__c == null ? 2 : Integer.valueof(projectList[0].Number_of_Max_Bids_Bidders_to_Show__c.trim());
                
                if(projectList[0].Fields_to_Keep_on_Collapse__c != null) {
                    container.collapseValue = Integer.valueOf(projectList[0].Fields_to_Keep_on_Collapse__c);
                	if(container.collapseValue > 0)
                        container.collapseValue--;
                }
                
                for(integer i = container.labelsForMaxBidColumns.size() - 1; i>= 2 * Number_of_Max_Bids; i--){
                    container.labelsForMaxBidColumns.remove(i);
                }
                
                container.isActive = !projectList[0].inactive__c;
                
                
                LW_Setup__c campaignSettingToShowMaxBidRows =   LW_Setup__c.getInstance('Show_Max_Bid_Rows_On_Bid_Matrix');
                
                if(campaignSettingToShowMaxBidRows != null && campaignSettingToShowMaxBidRows.Value__c != null 
                	&& campaignSettingToShowMaxBidRows.Value__c == 'No'){

                        container.showMaxBidRows = false;
                }
                List<Project_Configuration__mdt> configurationList = [SELECT Deal_Type__c,Field_Set_For_Bid_Matrix_Columns__c, 
                                                                      Default_Bid_Field__c,Show_Gain_Loss_on_Sale__c, Field_for_Gain_Loss_calculation__c,
                                                                      Field_Set_for_Editing_from_Bid_Matrix__c, Override_Bid_with__c  
                                                                      FROM Project_Configuration__mdt
                                                                      WHERE Deal_Type__c =: projectList[0].Project_Deal_Type__c];
                
                System.debug('getAircraftData configurationList :'+configurationList);                
                container.selectedFilterStatus = projectList[0].project_status_filter__c;              
                    
                apiName = projectList[0].Bid_Field__c;
                gainlossfieldapiName = configurationList[0].Field_for_Gain_Loss_calculation__c;
                
                if(configurationList.size()>0){
                    
                    //whether to show gain/loss on sale or not.
                    container.showLossGain = configurationList[0].Show_Gain_Loss_on_Sale__c;
                    //Saranya: 17/2/21 To override the bid value for a particular picklist type
                    //https://app.asana.com/0/1188810896929291/1199901918925101/f
                    container.overrideBidWith = configurationList[0].Override_Bid_with__c;
                    
                    fieldSetforEditingAssetInDeal = configurationList[0].Field_Set_for_Editing_from_Bid_Matrix__c;
                    
                    SObjectType aircraftType = Schema.getGlobalDescribe().get(prefix + 'Aircraft__c');
                    Map<String,Schema.SObjectField> mapAircraftfields = aircraftType.getDescribe().fields.getMap();
                    
                    //get the fields from field set according to the deal type.
                    for(Schema.FieldSetMember fieldsetMember : readFieldSet(configurationList[0].Field_Set_For_Bid_Matrix_Columns__c, prefix + 'Aircraft__c')){
                        
                        if(fieldsetMember.getFieldPath().toLowerCase() != (prefix + 'msn_number__c')){
                            
                            aircraftQry += ', ' + fieldsetMember.getFieldPath();
                            
                            // to query lookup name.
                            if(String.valueOf(fieldsetMember.getType()).toLowerCase() == 'reference'){
                                
                                aircraftQry += ', ' + mapAircraftfields.get(fieldsetMember.getFieldPath()).getDescribe().getRelationshipName() + '.Name';
                            }
                        }
                        
                        container.fieldList.add(new FieldWrapper(fieldsetMember.getFieldPath(),fieldsetMember.getLabel(),String.valueOf(fieldsetMember.getType()),mapAircraftfields.get(fieldsetMember.getFieldPath()).getDescribe().getScale()));        
                    }
                    
                }else{
                    
                    throw new AuraHandledException('Missing Campaign Configuration metadata for Deal Type ' + projectList[0].Project_Deal_Type__c + '.');
                }    
            } 
            
            apiName = String.isBlank(apiName) ? prefix + 'Sale_Price__c' : apiName;
            
            container.selectedBidField = apiName ;    
            
            String qry = 'select id,recordTypeId,marketing_activity__r.recordTypeId,Project_Comments__c,';
            qry += 'Marketing_activity__r.name,marketing_activity__c,marketing_activity__r.prospect_id__c,aircraft__c,';
            qry += 'aircraft__r.MSN_Number__c, aircraft__r.Accounting_Book_Value__c, prospect__c, Project_Status__c,' ;
            qry += 'Marketing_Activity__r.Prospect__c,Marketing_Activity__r.Counterparty__c,Marketing_Activity__r.Lessor__c,';
            if(gainlossfieldapiName != null){
                qry += 'aircraft__r.'+gainlossfieldapiName+',';}
            qry += apiName  + ' from Aircraft_Proposal__c';
            qry += ' where marketing_activity__r.campaign__c =: projectId AND aircraft__c != null and prospect__c != null order by prospect__c' ;
            
            
            
            List<String> projectStatusList = new List<String>();
            
            if(projectList.size() > 0 && projectList[0].project_status_filter__c != null){
                
                projectStatusList = projectList[0].project_status_filter__c.split(',');
                
                // qry += ' AND Project_status__c In : projectStatusList';
            } 
              
            Set<Id> setAircraftIds = new Set<Id>();
            List<Aircraft_Proposal__c> lstAssetTerm = Database.query(qry);
            
            for( AggregateResult ar : [select Aircraft__c assetId from Aircraft_Proposal__c where id in : lstAssetTerm 
                                          group by Aircraft__c]){
                                        
                setAircraftIds.add((Id)ar.get('assetId'))  ;                      
            }
            
            aircraftQry += ' from aircraft__c where id in : setAircraftIds' ;
            
            Map<Id,Aircraft__c> mapAircraft = new Map<Id,Aircraft__c>((List<Aircraft__c>)Database.query(aircraftQry)) ;
            Map<Id,Map<Decimal,MaxBidWrapper>> maxBidAircraftMap = new Map<Id,Map<Decimal,MaxBidWrapper>>();
            
            Set<String> aircraftProspectSet = new Set<String>();
            
            for(Aircraft_Proposal__c assetTerm : lstAssetTerm){
                
                Id prospectId = assetTerm.marketing_activity__r.prospect_id__c;
                
                if(!prospectInfoMap.containsKey(prospectId)){
                    prospectInfoMap.put(prospectId,new ProspectWrapper(assetTerm.prospect__c, prospectId, assetTerm.marketing_activity__c, 
                                                                       assetTerm.Marketing_activity__r.name,assetTerm.marketing_activity__r.recordTypeId));
                }   
                
                // to collect the combinations of Asset and prospect that is Asset in term.
                aircraftProspectSet.add(assetTerm.aircraft__c + (prospectId + ''));
                
                
                //aircraft wrapper (Aircraft level information) 
                AircraftWrapper aircraftWrapperObj ;
                if(gainlossfieldapiName != null){
                //get dataType of the field mentioned in the custom metadata
                Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get('Aircraft__c');
                Schema.DescribeSObjectResult sobjectResult = sobjectType.getDescribe();
                Schema.DescribeFieldResult fieldType = sobjectResult.fields.getMap().get(gainlossfieldapiName).getDescribe();
                Boolean isGainLoss_Currency;
                if (fieldType.getType() == Schema.DisplayType.Currency){
                    isGainLoss_Currency = true;
                }else{
                    isGainLoss_Currency = false;
                }
                
                Decimal valForGainLossCalc = (Decimal)(assetTerm.Aircraft__r).get(gainlossfieldapiName);
                 
                if(!aircraftInfoMap.containsKey(assetTerm.aircraft__c)){
                    aircraftInfoMap.put(assetTerm.aircraft__c, new AircraftWrapper(isGainLoss_Currency,valForGainLossCalc,assetTerm.aircraft__r.Accounting_Book_Value__c, mapAircraft.get(assetTerm.aircraft__c), Number_of_Max_Bids)); 
                }
            }
            else{
                if(!aircraftInfoMap.containsKey(assetTerm.aircraft__c)){
                    aircraftInfoMap.put(assetTerm.aircraft__c, new AircraftWrapper(true,0,assetTerm.aircraft__r.Accounting_Book_Value__c, mapAircraft.get(assetTerm.aircraft__c), Number_of_Max_Bids)); 
                }
            }
                aircraftWrapperObj = aircraftInfoMap.get(assetTerm.aircraft__c);
                
                //aircraft summary wrapper (aircraft childs).
                AircraftSummaryWrapper summaryObj = new AircraftSummaryWrapper(assetTerm);
                
                //filter data if any matches with the status.
                if(projectStatusList.size() == 0 || (projectStatusList.size() > 0 && projectStatusList.contains(assetTerm.project_status__c))){
                    String value = String.valueOf((Decimal)assetTerm.get(apiName) == null ? 0 : (Decimal)assetTerm.get(apiName));
                    if(value != '0'){
                    Decimal valueInDecimal = Decimal.valueOf(value);
                    summaryObj.scale = fieldMap.get(apiName).getDescribe().getScale();
                    summaryObj.value = String.valueOf(valueInDecimal.setScale(summaryObj.scale));
                    summaryObj.title = summaryObj.value;
                    }
                    else{
                    summaryObj.scale = 0;
                    summaryObj.value = '0';
                    summaryObj.title = '0';   
                    }
                    if((Decimal)assetTerm.get(apiName) != null && (Decimal)assetTerm.get(apiName) != 0){
					System.Debug('Field 1 : '+(Decimal)assetTerm.get(apiName));
						//if value is greater than 0 only then it should count as bid. 
                        aircraftWrapperObj.totalBids += 1 ;
                        prospectInfoMap.get(prospectId).totalBids += 1;
                        
                        if(!maxBidAircraftMap.containsKey(assetTerm.aircraft__c)){
                            maxBidAircraftMap.put(assetTerm.aircraft__c, new Map<Decimal, MaxBidWrapper>());
                        }
                        
                        Map<Decimal, MaxBidWrapper> innerMap = maxBidAircraftMap.get(assetTerm.aircraft__c);
                        Integer bidScale = fieldMap.get(apiName).getDescribe().getScale();
                        Decimal BidAmount = (Decimal)assetTerm.get(apiName);
                        
                        
                        if(!innerMap.containsKey((Decimal)assetTerm.get(apiName))){
                            innerMap.put(BidAmount.setScale(bidScale), new MaxBidWrapper(BidAmount.setScale(bidScale),assetTerm.Id, assetTerm.prospect__c));    
                        }else{
                            MaxBidWrapper wrapperObj = innerMap.get((Decimal)assetTerm.get(apiName));
                            wrapperObj.bidderName += ', '+assetTerm.prospect__c;
                            wrapperObj.moreThanOneBidder = true;
                        }
                  }

                  }else{
                    
                    // in case of filter applied, we will add Asset in Deal but will not show it and include it in summary calculations.
                    summaryObj.isFiltered = false;
                    summaryObj.value = 'Status:[' + (summaryObj.assetTerm.Project_Status__c != null ? summaryObj.assetTerm.Project_Status__c : '') + ']';
                    summaryObj.title = summaryObj.assetTerm.Project_Status__c != null ? summaryObj.assetTerm.Project_Status__c : '';
                    summaryObj.scale = fieldMap.get('Project_Status__c').getDescribe().getScale();
                }
                
                aircraftWrapperObj.summary.add(summaryObj); 
                aircraftInfoMap.put(assetTerm.aircraft__c, aircraftWrapperObj);             
            }
            
            lstAssetTerm = null;
            
            // now get all the max bidders.
            for(AircraftWrapper aircraftWrapperObj : aircraftInfoMap.values()){
                
                if(maxBidAircraftMap.containsKey(aircraftWrapperObj.aircraft.Id)){
                    
                    List<MaxBidWrapper> maxBidList = new List<MaxBidWrapper>();
                    Map<Decimal, MaxBidWrapper> innerMapOfBids =  maxBidAircraftMap.get(aircraftWrapperObj.aircraft.Id);
                    maxBidList.addAll(innerMapOfBids.values());
                    System.debug('SORTING BEFORE maxBidList :'+maxBidList);
                    maxBidList.sort();
                    System.debug('SORTING AFTER maxBidList :'+maxBidList);
                    if(maxBidList.size() > 0){
                        for(integer i = 0; i < Number_of_Max_Bids && i < maxBidList.size() ; i++){
                            aircraftWrapperObj.maxBidList[i] = maxBidList[i];
                        }
                    }
                }
                
                List<ProspectWrapper> prospectWrapperList = prospectInfoMap.values();
                for(Integer i = 0; i < prospectWrapperList.size(); i++){
                
                    if(!aircraftProspectSet.contains(aircraftWrapperObj.aircraft.Id + (prospectWrapperList[i].prospectId + ''))){
                       
                        if(i < aircraftWrapperObj.summary.size()){
                            
                            aircraftWrapperObj.summary.add(i,new AircraftSummaryWrapper(null));
                        }else{
                            
                            aircraftWrapperObj.summary.add(new AircraftSummaryWrapper(null));
                        }
                    }
                }
            }           
        }
        
        if(fieldSetforEditingAssetInDeal == null){
            fieldSetforEditingAssetInDeal = prefix + 'Fields_For_Editing_On_Bid_Matrix';
        }     
        container.fieldSetforEditingAssetInDeal = fieldSetforEditingAssetInDeal; //LW-AKG: 12/17/20 : FieldSet to be used within AID Editor.
        //Asset detail (Asset in deal) fields map.
        container.assetDetailFieldList.add(new FieldWrapper(container.selectedBidField,fieldMap.get(container.selectedBidField).getDescribe().getLabel(), String.valueOf(fieldMap.get(container.selectedBidField).getDescribe().getType()),fieldMap.get(container.selectedBidField).getDescribe().getScale()));
        
        //get the fields for which we need to allow editing when editig a cell (Asset in Deal).
        for(Schema.FieldSetMember fieldsetMember : readFieldSet(fieldSetforEditingAssetInDeal, prefix + 'Aircraft_Proposal__c')){
            
            // skip main bid field in case it has been added into field set.
            if(fieldsetMember.getFieldPath() != container.selectedBidField){
                FieldWrapper fieldWrapperObj = new FieldWrapper(fieldsetMember.getFieldPath(), fieldsetMember.getLabel(), String.valueOf(fieldsetMember.getLabel()),fieldMap.get(fieldsetMember.getFieldPath()).getDescribe().getScale());
                //check if its editable or not.
                fieldWrapperObj.isEditable =  fieldMap.get(fieldsetMember.getFieldPath()).getDescribe().isUpdateable();
                container.assetDetailFieldList.add(fieldWrapperObj);
            }    
        }  
        
        container.ProspectWrapperList.addAll(prospectInfoMap.values());
        container.aircraftList.addAll(aircraftInfoMap.values());  
        System.debug('SORTING BEFORE aircraftList :'+container.aircraftList);
        container.aircraftList.sort();
        System.debug('SORTING BEFORE aircraftList :'+container.aircraftList);
        return  container; 
    }
    
    //get currency type fields from Asset in Deal.
    public static Set<String> getAllCurrencyFields(Map <String, Schema.SObjectField> fieldMap ){
        
        
        Set<String> updateableCurrencyFields = new Set<String>();
        
        
        for(Schema.SObjectField field : fieldMap.Values()){
            schema.describefieldresult result = field.getDescribe();
            if(String.valueOf(result.getType()) == 'Currency' && result.isUpdateable() && !result.isCalculated()){
                updateableCurrencyFields.add(result.getname());
            }  
        }
        
        return updateableCurrencyFields ;
    }   
    
    
    @auraEnabled
    public static AircraftContainer getBidSummary(Id projectId){
    
        AircraftContainer container = new AircraftContainer();
        String prefix = LeaseWareUtils.getNamespacePrefix();
        container.projectStatusOptions = Utility.getPicklistvalues(prefix + 'Aircraft_Proposal__c', prefix + 'Project_Status__c');
        Map<Id,ProspectWrapper> prospectInfoMap = new Map<Id,ProspectWrapper> ();
        
		if(projectId != null){
            
            List<Project__c> projectList = [select id, inactive__c,Bid_Field__c, Project_Deal_Type__c, project_status_filter__c, Number_of_Max_Bids_Bidders_to_Show__c 
                                            from project__c where id =: projectId];
            String apiName ;

            if(projectList.SIZE()>0){

                apiName = projectList[0].Bid_Field__c;
                container.selectedFilterStatus = projectList[0].project_status_filter__c;
            } 
            
            apiName = String.isBlank(apiName) ? prefix + 'Sale_Price__c' : apiName;
  
            String qry = 'select id,recordTypeId,marketing_activity__r.recordTypeId,Project_Comments__c,Marketing_activity__r.name,marketing_activity__c,marketing_activity__r.prospect_id__c,aircraft__c,aircraft__r.MSN_Number__c, aircraft__r.Accounting_Book_Value__c, prospect__c, Project_Status__c,' ;
            qry += apiName  + ' from Aircraft_Proposal__c';
            qry += ' where marketing_activity__r.campaign__c =: projectId AND aircraft__c != null and prospect__c != null' ;
            
            if(projectList.size() > 0 && projectList[0].project_status_filter__c != null){
                List<String> projectStatusList = projectList[0].project_status_filter__c.split(',');
                qry += ' AND Project_status__c In : projectStatusList';
            } 
            
            qry += ' order by prospect__c';
            List<Aircraft_Proposal__c> lstAssetTerm = Database.query(qry);
            
            Map<Id,Decimal> maxBidAircraftMap = new Map<Id,Decimal>();
            for(Aircraft_Proposal__c assetTerm : lstAssetTerm){
                Id prospectId = assetTerm.marketing_activity__r.prospect_id__c;
                
                if(!prospectInfoMap.containsKey(prospectId)){
                    prospectInfoMap.put(prospectId,new ProspectWrapper(assetTerm.prospect__c, prospectId, assetTerm.marketing_activity__c, 
                                                                       assetTerm.Marketing_activity__r.name,assetTerm.marketing_activity__r.recordTypeId));
                }   
                                   
                if((Decimal)assetTerm.get(apiName) != null && (Decimal)assetTerm.get(apiName)>0){
                
                    //for overall summary prospect wise.
                    prospectInfoMap.get(prospectId).sumOfBids += (Decimal)assetTerm.get(apiName);
					//if value is greater than 0 only then it should count as bid. 
                    prospectInfoMap.get(prospectId).totalBids += 1;
                    
                    //set default max bid for an aircraft.
                    if(!maxBidAircraftMap.containsKey(assetTerm.aircraft__c)){
                        maxBidAircraftMap.put(assetTerm.aircraft__c,0);
                    }
                    
                    //check max bid.
                    if((Decimal)assetTerm.get(apiName) > maxBidAircraftMap.get(assetTerm.aircraft__c)){
                        maxBidAircraftMap.put(assetTerm.aircraft__c,(Decimal)assetTerm.get(apiName));
                    }
                } 
            }   
            //now compare the bid amount of each prospect from max bid of an aircraft.
            for(Aircraft_Proposal__c assetTerm : lstAssetTerm){
                Decimal maxBid;
                
                if(maxBidAircraftMap.containsKey(assetTerm.aircraft__c)){
                    maxBid = maxBidAircraftMap.get(assetTerm.aircraft__c);
                } 
                
                Id prospectId = assetTerm.marketing_activity__r.prospect_id__c;
                if(prospectInfoMap.containskey(prospectId)){
                
                    //calculate average, its a one time logic for each prospect.
                    if(prospectInfoMap.get(prospectId).totalBids > 0 && prospectInfoMap.get(prospectId).averageBid == 0){
                        prospectInfoMap.get(prospectId).averageBid = (prospectInfoMap.get(prospectId).sumOfBids / prospectInfoMap.get(prospectId).totalBids).setScale(2);
                    }
                    
                    if((Decimal)assetTerm.get(apiName)>0 && maxBid == (Decimal)assetTerm.get(apiName)){
                        prospectInfoMap.get(prospectId).numberOfMaxBid = (prospectInfoMap.get(prospectId).numberOfMaxBid == null ? 0 : prospectInfoMap.get(prospectId).numberOfMaxBid) + 1;
                    }
                }
            }           
        }

        container.ProspectWrapperList.addAll(prospectInfoMap.values()); 
        System.debug('container :'+container);
        return  container; 
    } 
    
    @auraEnabled
    public static void updateFilterStatus(Id projectId, String filters){
        
        Project__c projectObj = new Project__c(Id = projectId);
        projectObj.Project_Status_Filter__c = filters;
        update projectObj;
    }
    
    
    @auraEnabled
    public static void updateBidPreferredField(Id projectId, String fieldApiName){
        
        List<Project__c> projectList = [select id,Bid_Field__c from project__c where id =: projectId];
        
        if(projectList.SIZE()>0){
            projectList[0].Bid_Field__c = fieldApiName;
            update projectList;
        } 
    }
    
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();   
        
        if(DescribeSObjectResultObj.FieldSets.getMap().containsKey(fieldSetName)){
            
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
            return fieldSetObj.getFields();
            
        }else{
            throw new AuraHandledException('Field Set is missing or incorrectly configured.');
        }    
    } 
    
    
    @auraEnabled
    public static List<ProspectWrapper> getProjectBidData(Id projectId){
        return getAircraftData(projectId).ProspectWrapperList;
    }
    
    @auraEnabled
    public static string validateTradingProfile(Id assetId, Id prospectId){
        
        String prefix = LeaseWareUtils.getNamespacePrefix();
        string sObjectName = String.valueOf(((Id)prospectId).getSObjectType());
        string prospectType;
        
        if(sObjectName.equals(prefix+'Counterparty__c')){
            prospectType = 'Lessor';
        }else if(sObjectName.equals(prefix+'Operator__c')){
            prospectType = 'Operator';
        }else{
            prospectType = 'Counterparty';
        }
        string jsonData = '{"prospectList": [{"prospectType": "'+prospectType+'","prospectID": "'+prospectId+'"}';
        jsonData += '],"assetList": [{"assetID": "'+assetId+'"}]}';
        
        LW_Setup__c campaignSetting =   LW_Setup__c.getInstance('Client Controller Name');
        
        if(campaignSetting != null && campaignSetting.Value__c != null){
        
            TradingProfileDataSource dsc = (TradingProfileDataSource) Type.forName(campaignSetting.Value__c).newInstance();
            string jsonDummi = dsc.getData(jsonData);
            
            ProspectBulkUploadController.ResultWrapper resultWrapperObj = (ProspectBulkUploadController.ResultWrapper) System.JSON.deserialize(jsonDummi, ProspectBulkUploadController.ResultWrapper.class);
            
            if(resultWrapperObj != null && resultWrapperObj.resultList.size() > 0){                
                for(ProspectBulkUploadController.AssetResults assetObj : resultWrapperObj.resultList[0].assetResults){
                    if(assetObj.assetID == assetId & assetObj.profileOk == false){
                        return assetObj.details;
                    }
                }
            }
        }
        return null;
    }
    
    @auraEnabled
    public static void updateAssetDetailRecords(Aircraft_Proposal__c assetDetail, Id prospectId, Id projectId, List<String> fieldsToBeCopied){
        
        List<Aircraft_Proposal__c> assetDetailList = new List<Aircraft_Proposal__c>();
        for(Aircraft_Proposal__c  otherAssetDetail : [select id from Aircraft_Proposal__c where id !=: assetDetail.Id and
                                                      marketing_activity__r.campaign__c =: projectId  and
                                                      (marketing_activity__r.Prospect__c =: prospectId
                                                      or marketing_activity__r.Counterparty__c =: prospectId 
                                                      or marketing_activity__r.Lessor__c =: prospectId)]){
                                                          
                                                          for(String field : fieldsToBeCopied){
                                                              otherAssetDetail.put(field,assetDetail.get(field));
                                                          }
                                                          
                                                          assetDetailList.add(otherAssetDetail); 
                                                      }
        
        assetDetailList.add(assetDetail);
        update assetDetailList;
        
    }
    
    /**
    * @description This function is used to update the Marketing Activity record and then update the Asset in Deal records. 
    *
    * @param maRecord - Marketing_Activity__c, deal record containing all the fields data.
    * @param aidObjList - List<Aircraft_Proposal__c>, list of all Asset In Deal records present with the Deal.
    */
    @AuraEnabled
    public static void updateMAAndAIDRecords(Marketing_Activity__c maRecord, List<Aircraft_Proposal__c> aidObjList) {
        update maRecord;
        
        if (aidObjList == null || aidObjList.isEmpty()) { return; }
        update aidObjList;
    }
    
    //WRAPPER
    public class AircraftContainer{
        
        @auraEnabled public Boolean showLossGain {get;set;}
        @auraEnabled public List<Utility.Option> projectStatusOptions {get;set;}
        @auraEnabled public List<FieldWrapper> fieldList {get;set;} // for colums of aircraft
        @auraEnabled public List<FieldWrapper> currencyFieldList {get;set;}
        @auraEnabled public List<FieldWrapper> assetDetailFieldList {get;set;} // FOR EDITING A CELL.
        @auraEnabled public String selectedBidField{get;set;} // field to be targeted.
        @auraEnabled public List<AircraftWrapper> aircraftList {get;set;}
        @auraEnabled public List<ProspectWrapper> ProspectWrapperList {get;set;} // new list to be added.
        @auraEnabled public String selectedFilterStatus {get;set;}
        @auraEnabled public Boolean isActive {get;set;}
        @auraEnabled public Boolean showMaxBidRows {get;set;}
        @auraEnabled public List<String> marketingActivityFieldList {get;set;}
        @auraEnabled public List<String> aircraftFieldList {get;set;} // for edit aircraft
        @auraEnabled public List<String> labelsForMaxBidColumns {get;set;}
        @auraEnabled public Integer collapseValue {get;set;}
        @auraEnabled public String fieldSetforEditingAssetInDeal {get;set;} //LW_AKG: 12/17/20 FieldSet Name to be used with AID Editor.
        @auraEnabled public String overrideBidWith {get;set;} //Saranya: 17/2/21 To show status instead of 0 for a particular picklist value
        
        public AircraftContainer(){
            aircraftList = new List<AircraftWrapper>();
            ProspectWrapperList  = new List<ProspectWrapper>();
            fieldList = new List<FieldWrapper>();
            currencyfieldList  =  new List<FieldWrapper>();
            projectStatusOptions = new List<Utility.Option>();
            assetDetailFieldList = new List<FieldWrapper>();
            showLossGain = false;
            isActive = false;
            showMaxBidRows = true;
            marketingActivityFieldList = new List<String>();
            aircraftFieldList = new List<String>();
            collapseValue = 0;
            labelsForMaxBidColumns = new List<String>{'Max Bid','Max Bidder','Second Max Bid','Second Max Bidder','Third Max Bid','Third Max Bidder','Fourth Max Bid','Fourth Max Bidder','Fifth Max Bid','Fifth Max Bidder'};
                }
    }
    
    public class ProspectWrapper{
        
        @auraEnabled public String prospect {get;set;}
        @auraEnabled public Id prospectId {get;set;}
        @auraEnabled public String marketingActivityId {get;set;}
        @auraEnabled public Integer totalBids {get;set;}
        @auraEnabled public Decimal sumOfBids {get;set;}
        @auraEnabled public Decimal numberOfMaxBid{get;set;}
        @auraEnabled public Decimal averageBid{get;set;}
        @auraEnabled public String marketingActivityName {get;set;}
        @auraEnabled public String marketingActivityRecordTypeId {get;set;}
        
        public ProspectWrapper (String prospectName, String prospectId, String MAId, String MAName, String recordTypeId){
            sumOfBids  = 0;
            totalBids  = 0;
            numberOfMaxBid = 0;
            averageBid = 0;
            prospect = prospectName;
            this.prospectId =  prospectId;
            marketingActivityId = MAId;
            marketingActivityName = MAName;
            marketingActivityRecordTypeId  = recordTypeId;
        }
        
    }
    
    public class AircraftWrapper implements Comparable {
        
        @auraEnabled public Aircraft__c aircraft {get;set;}
        @auraEnabled public Decimal bookValue {get;set;}
        @auraEnabled public Boolean isGainLoss_Currency {get;set;}
        @auraEnabled public Decimal fieldForGainLossCalc {get;set;}
        @auraEnabled public List<AircraftSummaryWrapper> summary {get;set;}
        @auraEnabled public Integer totalBids {get;set;}
        @auraEnabled public List<MaxBidWrapper> maxBidList {get;set;} // containing max bid and bidder info (sorted by first max, second max and so on.)
        
        
        public AircraftWrapper ( Boolean isGainLoss_Currency, Decimal fieldForGainLossCalc,Decimal bookValue, Aircraft__c aircraft, Integer maxBidInstance){
            summary = new List<AircraftSummaryWrapper>();
            totalBids = 0;
            this.bookValue = (bookValue != null ? bookValue : 0);
            this.aircraft = aircraft;
            this.fieldForGainLossCalc = fieldForGainLossCalc;
            this.isGainLoss_Currency = isGainLoss_Currency;
            maxBidList = new List<MaxBidWrapper>();
            for(Integer i = 0; i < maxBidInstance; i++ ){
                maxBidList.add(new MaxBidWrapper(0,null,''));
            }
        }
        
        //Sort by MSN Alphabetically
        public Integer compareTo(Object objToCompare) {
            
            return aircraft.MSN_Number__c.compareTo(((AircraftWrapper)objToCompare).aircraft.MSN_Number__c);
        }
    }
    
    
    public class AircraftSummaryWrapper{
        @auraEnabled public Aircraft_Proposal__c assetTerm {get;set;} //added
        @auraEnabled public String value {get;set; } // for sale price or purchase price value or status.
        @auraEnabled public String title{get;set; }
        @auraEnabled public Integer scale{get;set; }
        @auraEnabled public Boolean isFiltered{get;set;}
        @auraEnabled public String bgColor{get;set;}
        
        public AircraftSummaryWrapper(Aircraft_Proposal__c  assetTerm ){
            this.assetTerm = assetTerm ;
            isFiltered = true;
        } 
    }
    
    public class FieldWrapper {
        @auraEnabled public String apiName {get;set;}
        @auraEnabled public String label {get;set;}
        @auraEnabled public Boolean selected {get;set;}
        @auraEnabled public String type{get;set;}
        @auraEnabled public Boolean isEditable{get;set;}
        @auraEnabled public Integer scale{get;set;}
        
        public FieldWrapper (String name, String label, String type,Integer scale){
            apiName =  name;
            this.label =  label;
            this.type = type;
            selected = false;
            this.isEditable = true;
            this.scale= scale;
        }
    }
    
    
    public class MaxBidWrapper implements Comparable{
        @auraEnabled public String bidderName {get;set;}
        @auraEnabled public Decimal bidAmount {get;set;}
        @auraEnabled public Id bidId {get;set;}
        @auraEnabled public Boolean moreThanOneBidder {get;set;}
        
        public MaxBidWrapper(Decimal bidAmount,Id bidId, String bidderName){
            this.bidderName = bidderName;
            this.bidAmount = bidAmount;
            this.bidId = bidId;
            this.moreThanOneBidder = false;
        }
        
        public Integer compareTo(Object objToCompare) {
            if(((MaxBidWrapper)objToCompare).bidAmount - bidAmount == 0){
                return 0;
            }
            else if(((MaxBidWrapper)objToCompare).bidAmount - bidAmount > 0){
                return 1;
            }
            else return -1;
        }
    }
    
    @auraEnabled
    public static void saveAssetTerm(Aircraft_Proposal__c  assetTerm){
        update assetTerm;
    }
    
}