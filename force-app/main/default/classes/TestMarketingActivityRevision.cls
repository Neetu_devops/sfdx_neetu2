/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

 
@isTest
private class TestMarketingActivityRevision {
    
    @isTest(SeeAllData = true)
    static void testMARevisionController1() {
        map < String, Object > mapInOut = new map < String, Object > ();

        // Create Operator
        mapInOut.put('Operator__c.Name', 'Air XYZ');
        TestLeaseworkUtil.createOperator(mapInOut);
        Id OperatorId1 = (String) mapInOut.get('Operator__c.Id');

        // create Asset
        mapInOut.put('Aircraft__c.msn', 'MSN2');
        mapInOut.put('Aircraft__c.New_Used__c', 'New');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id');

        // insert for Deal
        Marketing_Activity__c myMA = new Marketing_Activity__c(Name = 'My Deal', Prospect__c = OperatorId1, Deal_Type__c = 'Lease', Deal_Status__c = 'Market Intel');
        //system.debug('myMA: ' + myMA);
        Database.SaveResult result1 = Database.insert(myMA, false);
        System.assert(result1.isSuccess());
        
        // insert for Asset in Deal
        Aircraft_Proposal__c myAcProp = new Aircraft_Proposal__c(Name = 'My AC Proposal', Marketing_Activity__c = myMA.Id, Aircraft__c = Aircraft1,
            Term_Mos__c = 72, Rent__c = 24000, Investment_Timing__c = 'Upfront', Investment_Required__c = 3000000, Evaluation_Approach__c = 'IRR');
        //system.debug('myAcProp : ' + myAcProp);
        Database.SaveResult result2 = Database.insert(myAcProp, false);
        System.assert(result2.isSuccess());
        
        // insert for Commercial Terms
        Pricing_Run_New__c myDealAnalysis = new Pricing_Run_New__c(Name = 'My Commercial Terms', Deal_Type__c = 'Order Position', Analysis_Target__c = 'Rent for Given IRR', Lease_Term__c = '1 Year',
            Engine_Manufacturer__c = 'CFM', Rate_Type__c = 'Fixed/Fixed', Deposit_Type__c = 'LC', IRR__c = 11, Current_Rent__c = 12000, Marketing_Activity__c = myMA.Id);
        //system.debug('myDealAnalysis: ' + myDealAnalysis);
        Database.SaveResult result3 = Database.insert(myDealAnalysis, false);
        System.assert(result3.isSuccess());
        
        // insert for Deal Approval
        Deal_Approval__c myDealApproval = new Deal_Approval__c(Name = 'My Deal Approval',stage__c='Under Review', Marketing_Activity__c = myMA.Id);
        //system.debug('myDealApproval: ' + myDealApproval);
        Database.SaveResult result4 = Database.insert(myDealApproval, false);
        System.assert(result4.isSuccess());
        
        
         // insert for Terms Comparison
        Approval_Comparison_Fields__c myTermCmprison = new Approval_Comparison_Fields__c(Name = 'My Terms Comparison',Rent_Type__c='fixed/fixed', Deal_Approval__c = myDealApproval.Id);
        //system.debug('myTermCmprison: ' + myTermCmprison);
        Database.SaveResult result5 = Database.insert(myTermCmprison, false);
        System.assert(result5.isSuccess());
        
        // insert for Economic Analysis 
        Pricing_Output_New__c myEconomicAnalysis= new Pricing_Output_New__c(Name='My Economic Analysis', Asset_Term__c=myAcProp.id,Pricing_Run__c=myDealAnalysis.id);
        Database.SaveResult result6 = Database.insert(myEconomicAnalysis, false);
        System.assert(result6.isSuccess());
        
        //insert for Deal Team
        Deal_Team__c myDealTeam = new Deal_Team__c(Name='My Deal Team',Uncomplete_on_new_revision__c=True,complete__c=True,Marketing_Activity__c = myMA.Id);
        Database.SaveResult result7 = Database.insert(myDealTeam, false);
        System.assert(result7.isSuccess());
        
        //Create record into Revision
               
        // Attached file into Deal
        ContentVersion conVerMA = new ContentVersion();
        conVerMA.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVerMA.PathOnClient = 'E:/Retrieve_Deployment/Revision/File/Test_pdf.pdf'; // The files name, extension is very important here which will help the file in preview.
        conVerMA.Title = 'Title'; // Display name of the files
        //conver.FileType = 'PDF';
        conVerMA.VersionData = blob.valueof('Test_pdf.pdf'); //blob.toPDF(resBody) ; Blob.valueof(resBody); // converting your binary string to Blog
        insert conVerMA;
    
        ContentVersion coverMA = [select ContentDocumentId from ContentVersion where id = :conVerMA.Id limit 1];
        ContentDocumentLink cDeMA = new ContentDocumentLink();
        cDeMA.ContentDocumentId = coverMA.ContentDocumentId;
        cDeMA.LinkedEntityId = myMA.Id; // you can use objectId,GroupId etc
        cDeMA.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cDeMA.Visibility = 'AllUsers';
        insert cDeMA;
        
        // Attached file into Asset in Deal
        ContentVersion conVerAD = new ContentVersion();
        conVerAD.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVerAD.PathOnClient = 'E:/Retrieve_Deployment/Revision/File/Test_Spreadsheet.xlsx'; // The files name, extension is very important here which will help the file in preview.
        conVerAD.Title = 'Test_Spreadsheet.xlsx'; // Display name of the files
        //conver.FileType = 'PDF';
        conVerAD.VersionData = blob.valueof('Test_Spreadsheet.xlsx'); //blob.toPDF(resBody) ; Blob.valueof(resBody); // converting your binary string to Blog
        insert conVerAD;
    
        ContentVersion coverAD = [select ContentDocumentId from ContentVersion where id = :conVerAD.Id limit 1];
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = coverAD.ContentDocumentId;
        cDe.LinkedEntityId = myAcProp.Id; // you can use objectId,GroupId etc
        cDe.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cDe.Visibility = 'AllUsers';
        insert cDe;    
        
        // Attached file into Commercial Terms
        ContentVersion conVerDA = new ContentVersion();
        conVerDA.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVerDA.PathOnClient = 'E:/Retrieve_Deployment/Revision/File/Test_File.txt'; // The files name, extension is very important here which will help the file in preview.
        conVerDA.Title = 'Test_File.txt'; // Display name of the files
        //conver.FileType = 'PDF';
        conVerDA.VersionData = blob.valueof('Test_File.txt'); //blob.toPDF(resBody) ; Blob.valueof(resBody); // converting your binary string to Blog
        insert conVerDA;
    
        ContentVersion coverDA = [select ContentDocumentId from ContentVersion where id = :conVerDA.Id limit 1];
        ContentDocumentLink cDeDA = new ContentDocumentLink();
        cDeDA.ContentDocumentId = coverDA.ContentDocumentId;
        cDeDA.LinkedEntityId = myDealAnalysis.Id; // you can use objectId,GroupId etc
        cDeDA.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cDeDA.Visibility = 'AllUsers';
        insert cDeDA;        
        
        // Attached file into Deal Approval
        ContentVersion conVerDlApproval = new ContentVersion();
        conVerDlApproval.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVerDlApproval.PathOnClient = 'E:/Retrieve_Deployment/Revision/File/Test_pdf.pdf'; // The files name, extension is very important here which will help the file in preview.
        conVerDlApproval.Title = 'Test_pdf.pdf'; // Display name of the files
        //conver.FileType = 'PDF';
        conVerDlApproval.VersionData = blob.valueof('Test_File.txt'); //blob.toPDF(resBody) ; Blob.valueof(resBody); // converting your binary string to Blog
        insert conVerDlApproval;
    
        ContentVersion coverDlApproval = [select ContentDocumentId from ContentVersion where id = :conVerDlApproval.Id limit 1];
        ContentDocumentLink cDeDApproval = new ContentDocumentLink();
        cDeDApproval.ContentDocumentId = coverDlApproval.ContentDocumentId;
        cDeDApproval.LinkedEntityId = myDealApproval.Id; // you can use objectId,GroupId etc
        cDeDApproval.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cDeDApproval.Visibility = 'AllUsers';
        insert cDeDApproval; 
        
        //MarketingActivityRevision.getFileRecords(myMA.Id);
        
        List<MarketingActivityRevision.MarketingActivityRevisionWrapper> mActivityRevisionList= new List<MarketingActivityRevision.MarketingActivityRevisionWrapper>();
        mActivityRevisionList=MarketingActivityRevision.getFileRecords(myMA.Id).mActivityRevisionList;
     
        system.debug('My Result ='+mActivityRevisionList);
        System.debug('My Result == '+JSON.serializePretty(mActivityRevisionList));
       
        // Purpose of savArchive - Using this method we copy the field value from Deal, Asset in Deal, Detail Term to
        // Deal Revision, Asset in Deal Revision and Detail Term Revision respectively along with attached File as well
        MarketingActivityRevision.saveArchive(myMA.Id,'TestArchive',JSON.serializePretty(mActivityRevisionList));

        //once Deal get deleted, Commercial Terms also get deleted. below test to check Commercial Terms record get deleted or not.
        Delete myMA;
        List < Pricing_Run_New__c > listDA = [select id from Pricing_Run_New__c where Id =: myDealAnalysis.Id];
        System.debug('size: ' + listDA.size());
        System.assertEquals(listDA.size(), 0);

    }

    @isTest(SeeAllData = true)
        // To check is there any editable field intentionally or unintentionally added in Deal or Deal Revision
       static void testMARevisionController2() {

        Map < String, Schema.SObjectField > mapParentFields = Schema.SObjectType.Marketing_Activity__c.fields.getMap();
        Map < String, Schema.SObjectField > mapChildFields = Schema.SObjectType.Marketing_Activity_Revision__c.fields.getMap();
        
        // Add Filed label name LOWER CASE in the set if you want to skip to add in Deal, by defual it's empty
        // reason to add below fields in set is these fields created earlier as Lookup or Foumula field in revision but now want as text field in revision
        Set<String>setSkipMARFld = new Set<String>{'marketing_rep__c', 'country1__c', 'credit_rating1__c', 'interest_coverage_ratio1__c', 'marketing_representative1__c','y_hidden_lessor_name1__c','y_hidden_prospect_name1__c'}; //LOWER CASE field names.
            
        String strQryParent, strQryChild;
        Schema.DescribeFieldResult thisMAFieldDesc;
        string Flag = 'True';
        List<string> ListFieldNotAVL= new List<string>();
        
        for (String Field: mapParentFields.keyset()) {
            thisMAFieldDesc = mapParentFields.get(Field).getDescribe();
            strQryParent += thisMAFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryParent - Deal: ' + strQryParent);

        for (String Field: mapChildFields.keyset()) {
            thisMAFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisMAFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (!thisMAFieldDesc.isCustom() && !thisMAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (thisMAFieldDesc.getLocalName().equalsIgnoreCase('marketing_activity__c')) continue; 
            if (setSkipMARFld.contains(thisMAFieldDesc.getLocalName().toLowercase())) continue;
            if (strQryParent.toLowercase().contains(thisMAFieldDesc.getLocalName().toLowercase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisMAFieldDesc.getLocalName());
            }

        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' not available in Deal object');
        system.assertEquals(Flag, 'True');
        
        // Add Filed name in LOWER CASE in the set if you want to skip to add in Deal Revision, by defual it's empty
        Set<String> setSkipMAFld = new Set<String>{'current_rent_amounts__c','total_rent_across_all_active_leases__c','total_rent_collected_since_lease_start__c',
            'total_amount_requested','total_amount_granted__c','total_amount_granted_net_of_security_dep__c',
            'total_rent_deferral_net_of_security_depo__c','total_rent_relief_granted_as__c','total_rent_relief_requested_as__c',
            'total_rent_to_be_collected_across_leases__c','total_rent_to_be_collected__c','total_security_deposit__c','y_hidden_counterparty_name__c',
	    'date_of_assignment__c','max_deal_aging_date__c','closedealconfirmed__c', 'files_config_on_revision_popup__c',
            'related_payment_deferral_request__c','technical_comment__c','legal_comment__c','marketing_comment__c','contracts_comment__c','final_position__c',
            'potential_amount_lost_total__c', 'effective_amount_lost_total__c','liquidity_and_fleet_metrics__c',
            'latest_loi_summary_updated_by__c','latest_loi_summary_updated_date__c', 'y_hidden_latest_file_doc_id__c',
            'y_hidden_msn_esn_type__c','aircraft_type_qty_list__c'
            												}; //in LOWER CASE
       
                                                                
        Flag = 'True';
        for (String Field: mapChildFields.keyset()) {
            thisMAFieldDesc = mapChildFields.get(Field).getDescribe();
            if (!thisMAFieldDesc.isCustom() && !thisMAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            strQryChild += thisMAFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryChild: Deal Revision' + strQryChild);

        for (String Field: mapParentFields.keyset()) {
            thisMAFieldDesc = mapParentFields.get(Field).getDescribe();
            
            if (thisMAFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (!thisMAFieldDesc.isCustom() && !thisMAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (setSkipMAFld.contains(thisMAFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryChild.toLowerCase().contains(thisMAFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisMAFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL)+ ' not available in Deal Revision Object');
        system.assertEquals(Flag, 'True');

    }

    @isTest(SeeAllData = true)
     // To check is there any editable field intentionally or unintentionally added in Asset in Deal or Asset in Deal Revision
    static void testMARevisionController3() {
        Map < String, Schema.SObjectField > mapParentFields = Schema.SObjectType.Aircraft_Proposal__c.fields.getMap();
        Map < String, Schema.SObjectField > mapChildFields = Schema.SObjectType.Asset_Detail_Revision__c.fields.getMap();
        
        // Add Filed label name in SMALL CASE in the set if you want to skip to add in Asset in Deal, by defual it's empty
        // reason to add below fields in set is these fields created earlier as Lookup or Foumula field in revision object but now want as text field in revision
        Set<String>setSkipADRFld = new Set<String>{'deal_terms_revision__c','ac_deal_summary1__c', 'ac_model1__c', 'aircraft_manufacturer_f1__c', 'asset_family1__c', 'availability_date_n1__c', 'current_operator1__c', 'current_rent_amount1__c', 'deal_summary_c1__c', 'engine_type1__c', 'estimated_nbv_at_delivery_f1__c', 'estimated_nbv_at_end_of_lease1__c', 'monthly_depreciation1__c', 'nbv1__c', 'nbv_as_of_date1__c', 'asset_owner1__c', 'prospect1__c', 'registration_number1__c', 'type_variant1__c'}; //MUST BE IN SMALL CASE
        
        String strQryParent, strQryChild;
        Schema.DescribeFieldResult thisMAFieldDesc;
        string Flag = 'True';
        List<string> ListFieldNotAVL= new List<string>();
        for (String Field: mapParentFields.keyset()) {
            thisMAFieldDesc = mapParentFields.get(Field).getDescribe();
            strQryParent += thisMAFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryParent : ' + strQryParent);

        for (String Field: mapChildFields.keyset()) {
            thisMAFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisMAFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (thisMAFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity_Revision__c')) continue;
            if (!thisMAFieldDesc.isCustom() && !thisMAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (setSkipADRFld.contains(thisMAFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryParent.toLowerCase().contains(thisMAFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisMAFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Asset in Deal Object');
        system.assertEquals(Flag, 'True');
        
        // Add Filed name in the set if you want to skip to add in Asset in Deal Revision, by defual it's empty
        // Rent Holiday Fields to be skipped in revision object
        Set<String> rentHolidayFields = new Set<String>{'request_type__c','months_requested_by_lessee__c','number_of_months__c',
            'payback_period_start_month__c','months_granted_by_lessor__c','y_hidden_payment_relief_granted__c','y_hidden_payment_relief_requested__c','payment_relief_requested__c','payment_relief_granted__c',
            'rent_remaining__c','percent_of_rent_granted_to_be_deferred__c','percent_of_rent_requested_to_be_deferred__c',
            'comments__c','lease__c','current_rent_amount__c','lease_term_remaining__c','rent_collected__c','security_deposit__c',
            'payback_period_start_month__c','interest_to_be_paid__c','admin_fees__c','y_hidden_current_rent_amount__c','y_hidden_rent_collected__c',
            'y_hidden_rent_remaining__c','y_hidden_security_deposit__c'};
        
        Set<String> leaseRestructuringFields = new Set<String>{'MR_Balance__c','Rent_Remaining_To_Collect_As_Requested__c','Rent_Remaining_To_Collect_As_Granted__c',
            													'Potential_Amount_Lost__c','Effective_Amount_Lost__c'};
                
        
        Set<String> setSkipADFld = new Set<String>{'asset_age_on_bid__c', 'lease_remaining_term_on_bid__c', 'initial_bid__c', 'initial_bid_date__c', 'final_bid_date__c', 'percentage_bid_change__c','y_hidden_counterparty_id__c'};
        setSkipADFld.addAll(rentHolidayFields);
        setSkipADFld.addAll(leaseRestructuringFields);
        Flag = 'True';
        for (String Field: mapChildFields.keyset()) {
            thisMAFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisMAFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity_Revision__c')) continue;
            if (!thisMAFieldDesc.isCustom() && !thisMAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            strQryChild += thisMAFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryChild: ' + strQryChild);

        for (String Field: mapParentFields.keyset()) {
            thisMAFieldDesc = mapParentFields.get(Field).getDescribe();
            if (thisMAFieldDesc.getLabel().contains('Record Type'))   continue;
            if (thisMAFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (!thisMAFieldDesc.isCustom() && !thisMAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (thisMAFieldDesc.getLocalName().equalsIgnoreCase('marketing_activity__c')) continue; 
            if (setSkipADFld.contains(thisMAFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryChild.toLowerCase().contains(thisMAFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                 ListFieldNotAVL.add(thisMAFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Asset in Deal Revision object');
       // system.assertEquals(Flag, 'True'); //Commented by Neetu

    }
    
    @isTest(SeeAllData = true)
    // To check is there any editable field intentionally or unintentionally added in Commercial Terms or Commercial Terms Revision
    
    static void testMARevisionController4() {
        Map < String, Schema.SObjectField > mapParentFields = Schema.SObjectType.Pricing_Run_New__c.fields.getMap();
        Map < String, Schema.SObjectField > mapChildFields = Schema.SObjectType.Deal_Terms_Revision__c.fields.getMap();
        
        // Add Filed label name in the set if you want to skip to add in Commercial Terms, by defual it's empty
        Set<String>setSkipDTRFld = new Set<String>{};//must be in SMALL CASE
        
        String strQryParent, strQryChild;
        Schema.DescribeFieldResult thisDTFieldDesc;
        string Flag = 'True';
        List<string> ListFieldNotAVL= new List<string>();
        for (String Field: mapParentFields.keyset()) {
            thisDTFieldDesc = mapParentFields.get(Field).getDescribe();
            strQryParent += thisDTFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryParent : ' + strQryParent);

        for (String Field: mapChildFields.keyset()) {
            thisDTFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisDTFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (thisDTFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity_Revision__c')) continue;
            if (!thisDTFieldDesc.isCustom() && !thisDTFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (setSkipDTRFld.contains(thisDTFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryParent.toLowerCase().contains(thisDTFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisDTFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Commercial Terms Object');
        system.assertEquals(Flag, 'True');
        
        // Add Filed name in the set if you want to skip to add in Commercial Terms Revision, by defual it's empty
        Set<String>setSkipDTFld = new Set<String>{'type_variant__c','pricing_comments__c','lease_term_in_months__c'};
        Flag = 'True';
        for (String Field: mapChildFields.keyset()) {
            thisDTFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisDTFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity_Revision__c')) continue;
            if (!thisDTFieldDesc.isCustom() && !thisDTFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            strQryChild += thisDTFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryChild: ' + strQryChild);

        for (String Field: mapParentFields.keyset()) {
            thisDTFieldDesc = mapParentFields.get(Field).getDescribe();
            if (thisDTFieldDesc.getLabel().contains('Record Type'))   continue;
            if (thisDTFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (!thisDTFieldDesc.isCustom() && !thisDTFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (thisDTFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity__c')) continue;
            if (thisDTFieldDesc.getLabel().equalsIgnoreCase('Z_Unused Marketing Activity Revision')) continue;
            if (setSkipDTFld.contains(thisDTFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryChild.toLowerCase().contains(thisDTFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisDTFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Commercial Terms Revision object');
        system.assertEquals(Flag, 'True');

    }
    
     @isTest(SeeAllData = true)
    // To check is there any editable field intentionally or unintentionally added in Deal Approval or Deal Approval Revision
    
        static void testMARevisionController5() {
        Map < String, Schema.SObjectField > mapParentFields = Schema.SObjectType.Deal_Approval__c.fields.getMap();
        Map < String, Schema.SObjectField > mapChildFields = Schema.SObjectType.Deal_Approval_Revision__c.fields.getMap();
        
        // Add Filed label name in the set if you want to skip to add in Deal Approval, by defual it's empty
        Set<String>setSkipDTRFld = new Set<String>{'prospect1__c','commercial_terms_revision__c'};
        
        String strQryParent, strQryChild;
        Schema.DescribeFieldResult thisDLApprvlFieldDesc;
        string Flag = 'True';
        List<string> ListFieldNotAVL= new List<string>();
        for (String Field: mapParentFields.keyset()) {
            thisDLApprvlFieldDesc = mapParentFields.get(Field).getDescribe();
            strQryParent += thisDLApprvlFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryParent : ' + strQryParent);

        for (String Field: mapChildFields.keyset()) {
            thisDLApprvlFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisDLApprvlFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (thisDLApprvlFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity_Revision__c')) continue;
            if (!thisDLApprvlFieldDesc.isCustom() && !thisDLApprvlFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (setSkipDTRFld.contains(thisDLApprvlFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryParent.toLowerCase().contains(thisDLApprvlFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisDLApprvlFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Deal Approval Object');
        system.assertEquals(Flag, 'True');
        
        // Add Filed name in the set if you want to skip to add in Deal Approval Revision, by defual it's empty
        Set<String>setSkipDTFld = new Set<String>{};//MUST BE IN SMALL CASE
        Flag = 'True';
        for (String Field: mapChildFields.keyset()) {
            thisDLApprvlFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisDLApprvlFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity_Revision__c')) continue;
            if (!thisDLApprvlFieldDesc.isCustom() && !thisDLApprvlFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            strQryChild += thisDLApprvlFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryChild: ' + strQryChild);

        for (String Field: mapParentFields.keyset()) {
            thisDLApprvlFieldDesc = mapParentFields.get(Field).getDescribe();
            if (thisDLApprvlFieldDesc.getLabel().contains('Record Type'))   continue;
            if (thisDLApprvlFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (!thisDLApprvlFieldDesc.isCustom() && !thisDLApprvlFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (thisDLApprvlFieldDesc.getLocalName().equalsIgnoreCase('Marketing_Activity__c')) continue;
            if (setSkipDTFld.contains(thisDLApprvlFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryChild.toLowerCase().contains(thisDLApprvlFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisDLApprvlFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Deal Approval Revision object');
        system.assertEquals(Flag, 'True');

    }
    
     @isTest(SeeAllData = true)
    // To check is there any editable field intentionally or unintentionally added in Terms Comparison or Terms Comparison Revision
    
        static void testMARevisionController6() {
        Map < String, Schema.SObjectField > mapParentFields = Schema.SObjectType.Approval_Comparison_Fields__c.fields.getMap();
        Map < String, Schema.SObjectField > mapChildFields = Schema.SObjectType.Approval_Comparison_Fields_Revision__c.fields.getMap();
        
        // Add Filed label name in the set if you want to skip to add in Terms Comparison, by defual it's empty
        Set<String>setSkipDTRFld = new Set<String>{};//MUST BE IN SMALL CASE
        
        String strQryParent, strQryChild;
        Schema.DescribeFieldResult thisTermCmpFieldDesc;
        string Flag = 'True';
        List<string> ListFieldNotAVL= new List<string>();
        for (String Field: mapParentFields.keyset()) {
            thisTermCmpFieldDesc = mapParentFields.get(Field).getDescribe();
            strQryParent += thisTermCmpFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryParent : ' + strQryParent);

        for (String Field: mapChildFields.keyset()) {
            thisTermCmpFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisTermCmpFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (thisTermCmpFieldDesc.getLocalName().equalsIgnoreCase('Deal_Approval_Revision__c')) continue;
            if (!thisTermCmpFieldDesc.isCustom() && !thisTermCmpFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (setSkipDTRFld.contains(thisTermCmpFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryParent.toLowerCase().contains(thisTermCmpFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisTermCmpFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Terms Comparison Object');
        system.assertEquals(Flag, 'True');
        
        // Add Filed name in the set if you want to skip to add in Terms Comparison Revision, by defual it's empty
        Set<String>setSkipDTFld = new Set<String>{};//MUST BE IN SMALL CASE
        Flag = 'True';
        for (String Field: mapChildFields.keyset()) {
            thisTermCmpFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisTermCmpFieldDesc.getLocalName().equalsIgnoreCase('Deal_Approval_Revision__c')) continue;
            if (!thisTermCmpFieldDesc.isCustom() && !thisTermCmpFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            strQryChild += thisTermCmpFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryChild: ' + strQryChild);

        for (String Field: mapParentFields.keyset()) {
            thisTermCmpFieldDesc = mapParentFields.get(Field).getDescribe();
            if (thisTermCmpFieldDesc.getLabel().contains('Record Type'))   continue;
            if (thisTermCmpFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (!thisTermCmpFieldDesc.isCustom() && !thisTermCmpFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (thisTermCmpFieldDesc.getLocalName().equalsIgnoreCase('Deal_Approval__c')) continue;
            if (setSkipDTFld.contains(thisTermCmpFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryChild.toLowerCase().contains(thisTermCmpFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisTermCmpFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Terms Comparison Revision object');
        system.assertEquals(Flag, 'True');

    }
    
     @isTest(SeeAllData = true)
    // To check is there any editable field intentionally or unintentionally added in Economic Analysis or Economic Analysis Revision
    
        static void testMARevisionController7() {
        Map < String, Schema.SObjectField > mapParentFields = Schema.SObjectType.Pricing_Output_New__c.fields.getMap();
        Map < String, Schema.SObjectField > mapChildFields = Schema.SObjectType.Deal_Term_Asset__c.fields.getMap();
        
        // Add Filed label name in the set if you want to skip to add in Economic Analysis, by defual it's empty
        Set<String>setSkipDTRFld = new Set<String>{}; //MUST BE IN SMALL CASE
        
        String strQryParent, strQryChild;
        Schema.DescribeFieldResult thisEAFieldDesc;
        string Flag = 'True';
        List<string> ListFieldNotAVL= new List<string>();
        for (String Field: mapParentFields.keyset()) {
            thisEAFieldDesc = mapParentFields.get(Field).getDescribe();
            strQryParent += thisEAFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryParent : ' + strQryParent);

        for (String Field: mapChildFields.keyset()) {
            thisEAFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisEAFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (thisEAFieldDesc.getLocalName().equalsIgnoreCase('Pricing_Run__c')) continue;
            if (!thisEAFieldDesc.isCustom() && !thisEAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (setSkipDTRFld.contains(thisEAFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryParent.toLowerCase().contains(thisEAFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisEAFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Economic Analysis Object');
        system.assertEquals(Flag, 'True');
        
        // Add Filed name in the set if you want to skip to add in Economic Analysis Revision, by defual it's empty
        Set<String>setSkipDTFld = new Set<String>{}; //MUST BE IN SMALL CASE
        Flag = 'True';
        for (String Field: mapChildFields.keyset()) {
            thisEAFieldDesc = mapChildFields.get(Field).getDescribe();
            if (thisEAFieldDesc.getLocalName().equalsIgnoreCase('Pricing_Run__c')) continue;
            if (!thisEAFieldDesc.isCustom() && !thisEAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            strQryChild += thisEAFieldDesc.getLocalName() + ', ';
        }
        system.debug('strQryChild: ' + strQryChild);

        for (String Field: mapParentFields.keyset()) {
            thisEAFieldDesc = mapParentFields.get(Field).getDescribe();
                        if (thisEAFieldDesc.getLabel().contains('Record Type'))   continue;
            if (thisEAFieldDesc.isCalculated()) continue; // skip if it is calculated field like formula
            if (!thisEAFieldDesc.isCustom() && !thisEAFieldDesc.getLocalName().equalsIgnoreCase('Name')) continue;
            if (thisEAFieldDesc.getLocalName().equalsIgnoreCase('Pricing_Run__c')) continue;
            if (setSkipDTFld.contains(thisEAFieldDesc.getLocalName().toLowerCase())) continue;
            if (strQryChild.toLowerCase().contains(thisEAFieldDesc.getLocalName().toLowerCase())) continue;
            else {
                Flag = 'False';
                ListFieldNotAVL.add(thisEAFieldDesc.getLocalName());
            }
        }
        if (Flag=='False')
        System.debug('Fields: ' + JSON.serializePretty(ListFieldNotAVL) + ' is not available in Economic Analysis Revision object');
        system.assertEquals(Flag, 'True');

    }
  }