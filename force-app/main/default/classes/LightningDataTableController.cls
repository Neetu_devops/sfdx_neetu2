public without sharing class LightningDataTableController {

	@AuraEnabled
	public static List<Invoice_External__c> getOustandingBills(Integer offset, String orderBy){

        List<Operator__c> operators = UtilityWithSharingController.getOperators();
        
		String qry = 'Select Name, MSN_ESN__c, Paid_Date__c,Period_To__c, Period_From__c, Outstanding_Amount__c, ';
		qry += 'Paid_Amount__c, Invoice_Amount__c,Issue_Date__c, Invoice_Type__c ';
		qry += 'from Invoice_External__c where Outstanding_Amount__c > 0 ';
        qry += 'and Lease__r.lessee__c in : operators ';

		if(String.isNotBlank(orderBy)) {
			qry +=  orderBy;
		}

		qry +=  ' limit 10 offset : offset';

		system.debug('qry --->' + qry);
		return (List<Invoice_External__c>) Database.query(qry);
	}

	@AuraEnabled
	public static List<Invoice_External__c> getPaidBills(Integer offset, String orderBy){

		// Last 12 month date range
		Date toDate = System.today().addMonths(1).toStartOfMonth().addDays(-1);
		Date fromDate = System.today().addMonths(-12).toStartOfMonth(); 


        List<Operator__c> operators = UtilityWithSharingController.getOperators();
        System.debug('operators: '+operators);
		String qry = 'Select Name, MSN_ESN__c, Paid_Date__c,Period_To__c, Period_From__c, Outstanding_Amount__c, ';
		qry += 'Paid_Amount__c, Invoice_Amount__c,Issue_Date__c, Invoice_Type__c ';
		qry += 'from Invoice_External__c where (Outstanding_Amount__c = 0 or Outstanding_Amount__c = null) '; 
        qry += 'and Lease__r.lessee__c in : operators and Period_To__c >=: fromDate and Period_To__c <=: toDate';

		if(String.isNotBlank(orderBy)) {
			qry +=  orderBy;
		}

		qry +=  ' limit 10 offset : offset';

		return (List<Invoice_External__c>) Database.query(qry);

	}
    
    
    @AuraEnabled
    public static String getInvoicePdf(String invoiceId){
        if(invoiceId == null) { return null;}
        TradingProfileDataSource dsc;
        try {
            dsc = (TradingProfileDataSource) Type.forName('InvoicePdfGeneratorImpl').newInstance();
        }
        catch(Exception e) {
            System.debug('getInvoicePdf Exception '+e);
            return null;
        }
        return dsc.getData(invoiceId);
    }
}