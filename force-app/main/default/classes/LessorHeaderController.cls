public without sharing class LessorHeaderController {

    @AuraEnabled
    public static String getCommunityPrefix(){
        String communityPrefix = '/';
        //Try catch added for cases when code is executed outside of Community context
        try {
            communityPrefix = Site.getPathPrefix().removeEndIgnoreCase('/s');
        } catch (Exception e) {}
        return communityPrefix;
    }
}