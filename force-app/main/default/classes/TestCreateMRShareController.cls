/**
    *******************************************************************************
    * Class: TestCreateMRShareController
    * @author Created by Bhavna, Lease-Works, 
    * @date  6/13/2019
    * @version 1.0
    * ----------------------------------------------------------------------------------
    * Purpose/Methods:
    *  - Test all the Methods of CreateMRShareController.
    *
    * History:
    * - VERSION   DEVELOPER NAME    	DATE         	DETAIL FEATURES
    *
    ********************************************************************************
*/
@isTest
public class TestCreateMRShareController {
    
    /**
    * @description Created Test Data to test all the Methods of CreateMRShareController.
    *  	 
    * @return null
    */
    @testSetup 
    static void setup(){
        Date dateField = System.today(); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = lastDayOfMonth
        );
        insert leaseRecord;

        // Set up the Aircraft record
		String aircraftMSN = '12345';
        Aircraft__c ac = new Aircraft__c(MSN_Number__c=aircraftMSN, Date_of_Manufacture__c = System.today(),TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, 
        Status__c='Available', Derate__c=20);
        LeaseWareUtils.clearFromTrigger();
        insert ac;
		
         //update lease reocrd with aircraft
        leaseRecord.Aircraft__c=ac.id;
        LeaseWareUtils.clearFromTrigger();
        update leaseRecord;

        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createTSLookup(mapInOut);

		Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= ac.Id, Description__c ='test description', TSN__c=0, CSN__c=0,Type__c ='Airframe', Serial_Number__c='123');
        LeaseWareUtils.clearFromTrigger();
		insert consttAssembly;
		
		// Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(name='Test',Current_Catalog_Year__c='2020');
        insert mp;
		
		Maintenance_Program_Event__c mpe= new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
			Interval_2_Hours__c=20,
            Event_Type_Global__c = '4C/6Y'
        );
        insert mpe;
		
		
		// Set up Project Event for Airframe
        Assembly_Event_Info__c event = new Assembly_Event_Info__c();
        event.Name = 'Test Info';
        event.Event_Cost__c = 20;
        event.Constituent_Assembly__c = consttAssembly.Id;
        event.Maintenance_Program_Event__c = mpe.Id;
        insert event;
        
        Assembly_MR_Rate__c assemblyRecord = new Assembly_MR_Rate__c(
            Name='Testing',
            Assembly_Lkp__c =consttAssembly.Id,
            Assembly_Event_Info__c=event.id,
            Base_MRR__c = 200,
			Rate_Basis__c='Hours',
            Lease__c = leaseRecord.Id
        );
        insert assemblyRecord;
        
        MR_Share__c mrShareRecord = new MR_Share__c(
            Name='Test MR Share',
            Assembly_MR_Info__c= assemblyRecord.Id,
            Assembly__c='Engine 1',
            Maintenance_Event_Name__c = 'Refurbishment'
            
        );
        insert mrShareRecord;
        
    }
    
    
    /**
    * @description This Methods gives the code coverage of  fetchAssemblyType,fetchEventType
    *  	saveMRShare method of CreateMRShareController
    *
    * @return null
    */
    @isTest static void testAddMRInfoInvoice(){
        Assembly_MR_Rate__c assemblyMRRateList = [select id,Assembly_Lkp__c from  Assembly_MR_Rate__c];
        MR_Share__c mrShareList = [select id,Name,Assembly_MR_Info__c,Assembly__c,Maintenance_Event_Name__c from  MR_Share__c];
        List<String> assemblyType = new  List<String> ();
        assemblyType.add('Engine 1');
        assemblyType.add('Engine 2');
        List<CreateMRShareController.MRShareWrapper > mRShareWrapperList = new List<CreateMRShareController.MRShareWrapper>();
        CreateMRShareController.MRShareWrapper mrShare = new CreateMRShareController.MRShareWrapper();
        mrShare.sNo = 1;
        mrShare.assmeblyMRName='Testing';
        mrShare.isDisable = true;
        mrShare.lstAssemblyType = assemblyType;
        mrShare.mrShareRecord = mrShareList;
        mRShareWrapperList.add(mrShare);
        
        
        CreateMRShareController.fetchAssemblyType(assemblyMRRateList.Id,mrShareList.Id, assemblyType);
        CreateMRShareController.fetchAssemblyType(null,mrShareList.Id,assemblyType);
        CreateMRShareController.fetchEventType(assemblyMRRateList.Id,assemblyMRRateList.Assembly_Lkp__c,assemblyType);
        try{
            CreateMRShareController.saveMRShare(JSON.serialize(mRShareWrapperList));   
        }
        catch(Exception e){
            system.debug('Error');
         }
      //  system.assertEquals(assemblyMRRateList.size(),1);
    }
    
}