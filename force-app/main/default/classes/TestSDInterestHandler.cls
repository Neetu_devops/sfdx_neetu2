/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestSDInterestHandler {

    static testMethod void SDInterestGenerateTest() {
        // TO DO: implement unit test
        
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);
    	TestLeaseworkUtil.insertAircraft(mapInOut); 
    	Id leaseId =  (Id)mapInOut.get('Aircraft__c.lease__c');       
        
        Lease__c leaseRec = [select Id,Y_Hidden_Create_Interest_Schedule__c from Lease__c where Id = :leaseId];
        leaseRec.Deposit_Interest_Rate__c = 10;
        leaseRec.Deposit_Received_Date__c = system.today();
        leaseRec.Deposit_Return_Date__c = system.today().addMonths(12);
        leaseRec.Y_Hidden_Create_Interest_Schedule__c = true;
        update leaseRec;
        
		
	    LeaseWareUtils.clearFromTrigger();
	    try{
	    	leaseRec.Deposit_Interest_Rate__c = null;
        	leaseRec.Deposit_Received_Date__c = null;
        	leaseRec.Deposit_Return_Date__c = null;	    	
	    	update   leaseRec;    
	   	}catch (System.DmlException e) {
        		System.debug('Expected failure   ' + e);
        		System.assertEquals(true,e.getDmlMessage(0).contains('to create Interest payments'));
    			
    	}  	  
		
	    LeaseWareUtils.clearFromTrigger();
	    try{
	    	leaseRec.Deposit_Interest_Rate__c = null;
        	leaseRec.Deposit_Received_Date__c = null;
        	leaseRec.Deposit_Return_Date__c = null;	    	
	    	update   leaseRec;    
	   	}catch (System.DmlException e) {
        		System.debug('Expected failure   ' + e);
        		System.assertEquals(true,e.getDmlMessage(0).contains('to create Interest payments'));
    			
    	}  	  
    	
		
	    LeaseWareUtils.clearFromTrigger();
	    try{
	    	leaseRec.Deposit_Interest_Rate__c = 11;
        	leaseRec.Deposit_Received_Date__c = null;
        	leaseRec.Deposit_Return_Date__c = null;	    	
	    	update   leaseRec;    
	   	}catch (System.DmlException e) {
        		System.debug('Expected failure   ' + e);
        		System.assertEquals(true,e.getDmlMessage(0).contains('to create Interest payments'));
    			
    	}  	  
    	
     	    	        
    }
 
}