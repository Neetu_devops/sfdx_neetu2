@isTest
    private class TestPBHRent {

        @isTest(seeAllData=true)
        static void TestCasePBHMonthly() {
            Lease__c lease = [select id,Name,Lease_Start_Date_New__c,Lease_End_Date_New__c,Aircraft__c,(Select MR_Rate_End_Date__c,MR_Rate_Start_Date__c from Assembly_MR_Rates__r) from lease__c where name like '%TestSeededMSN3%' limit 1];

            lease.Lease_Start_Date_New__c=date.valueOf('2019-01-01');
            lease.Lease_End_Date_New__c=date.valueOf('2022-12-31');
            update lease;
            for(Assembly_MR_Rate__c rec:lease.Assembly_MR_Rates__r)  
            {
                rec.MR_Rate_Start_Date__c =lease.Lease_Start_Date_New__c;
                rec.MR_Rate_End_Date__c=lease.Lease_End_Date_New__c;
            }
            update lease.Assembly_MR_Rates__r;
            Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                                                Name='Test- PBH Monthly',
                                                Lease__c=lease.id,
                                                Rent_Type__c='Power By The Hour',
                                                Rent_Period__c='Monthly',
                                                Base_Rent__c=10000.00,
                                                Start_Date__c=date.valueOf('2019-01-01') , // YYYY-MM-DD
                                                Rent_End_Date__c=date.valueOf('2019-03-31') ,  // YYYY-MM-DD
                                                Invoice_Generation_Day__c='15',
                                                Rent_Due_Type__c='Fixed',
                                                Rent_Due_Day__c=15,
                                                Escalation__c=3,
                                                Escalation_Month__c='April',
                                                Pro_rata_Number_Of_Days__c='Actual number of days in month/year'
                                                );
            insert MultiRentSch;
            RentQueue.GenerateRent(MultiRentSch.id,0);
            system.debug('lease id='+lease.id+'= MultiId='+MultiRentSch.id);
            
            Constituent_Assembly__c CA=[select id,name from Constituent_Assembly__c where  Attached_Aircraft__r.Lease__c =: lease.id and Type__c='Airframe'];
            PBH_Rent_Rate__c PBHRate = new PBH_Rent_Rate__c(
                                        Name='PBH Rent Name',
                                        Associated_Assembly__c=CA.id ,
                                        From_Value__c= 50,
                                        Multi_Rent_Schedule__c = MultiRentSch.id,
                                        PBH_Rate__c = 150,
                                        Selection__c=True ,
                                        Rate_Basis__c='Hourly');
            
            insert PBHRate;
            
            Utilization_Report__c utilizationRecord = new Utilization_Report__c(
                Name='Testing',
                FH_String__c ='100',
                Airframe_Cycles_Landing_During_Month__c= 50.0,
                Aircraft__c = lease.Aircraft__c,
                Y_hidden_Lease__c = lease.Id,
                Type__c = 'Actual',
                Month_Ending__c = Date.newInstance(2019,01,31),
                Status__c='Approved By Lessor'
            );
            insert utilizationRecord;
            
            LeaseWareUtils.unsetTriggers('RentTriggerHandlerBefore');
        	LeaseWareUtils.unsetTriggers('RentTriggerHandlerAfter');
            set<id> setLeaseIds=new set<id>{lease.id};
            set<id> setURIds=new set<id>{utilizationRecord.id};

            Test.StartTest();
            URTriggerHandler.Upd_PBH_Amt_RentSch(setLeaseIds, setURIds,Null,'InsUpd');
            
			List<Rent__c> listRentRec = [select id, Start_Date__c, For_Month_Ending__c,Rent_Type__c, PBH_Amount__c, Rent_Due_Amount__c,RentPayments__c,Stepped_Rent__c,Invoice_Date__c,Y_Hidden_Invoice_Day__c,Y_Hidden_Utilization__c from Rent__c where RentPayments__c=:lease.id and Stepped_Rent__c=:MultiRentSch.id order by Start_Date__c] ;
            system.debug('Size of listRentRec='+listRentRec.size());
            
            List<PBH_Rent_Per_Assembly__c> PBHRPA = [select  Utilization_Unit__c,PBH_Rate_F__c, PBH_Amount__c , Rent_Schedule__c from PBH_Rent_Per_Assembly__c where Rent_Schedule__c=:listRentRec ];// PBH_Rent_Rate__c = : PBHRate.id and and Associated_Assembly_F__c=: CA.id
            system.debug('Size of PBHRPA='+PBHRPA.size());
            
            system.assertEquals(PBHRPA[0].Utilization_Unit__c,100);
            system.assertEquals(PBHRPA[0].PBH_Rate_F__c,150);
            system.assertEquals(PBHRPA[0].PBH_Amount__c,7500);
            system.assertEquals(PBHRPA[0].Rent_Schedule__c,listRentRec[0].id);
 
            system.assertEquals(listRentRec[0].Start_Date__c,date.valueof('2019-01-01'));
            system.assertEquals(listRentRec[0].For_Month_Ending__c,date.valueof('2019-01-31'));
            system.assertEquals(listRentRec[0].PBH_Amount__c,7500);
            system.assertEquals(listRentRec[0].Rent_Due_Amount__c,17500);
            system.assertEquals(listRentRec[0].Y_Hidden_Utilization__c,utilizationRecord.id);
            
            Test.StopTest();

            RentTriggerHandler.CreateInvQuickActionCall(listRentRec[0]);
            
            Invoice__c InvRec=[select id,Invoice_Type__c,Amount__c from Invoice__c where Rent__c =: listRentRec[0].id];
			system.assertEquals(InvRec.Invoice_Type__c,'Rent');
            system.assertEquals(InvRec.Amount__c,17500);
			
			List<Invoice_Line_Item__c> InvLIRec=[select id,name,Assembly__c,Amount_Due__c from Invoice_Line_Item__c where Invoice__c =: InvRec.id order by createddate];
			system.assertEquals(InvLIRec[0].Assembly__c,CA.id);
            system.assertEquals(InvLIRec[0].Amount_Due__c,7500);
            system.assertEquals(InvLIRec[1].name,'PBH Rent Adjustment');
            system.assertEquals(InvLIRec[1].Amount_Due__c,0);
            system.assertEquals(InvLIRec[2].name,'Fixed Rent');
            system.assertEquals(InvLIRec[2].Amount_Due__c,10000);
            system.assertEquals(InvLIRec[3].name,'Total Adjustment');
            system.assertEquals(InvLIRec[3].Amount_Due__c,0);
           
        }
        
        
		@isTest(seeAllData=true)
        static void TestCaseDelUR() {
            Lease__c lease = [select id,Name,Lease_Start_Date_New__c,Lease_End_Date_New__c,Aircraft__c,(Select MR_Rate_End_Date__c,MR_Rate_Start_Date__c from Assembly_MR_Rates__r) from lease__c where name like '%TestSeededMSN3%' limit 1];

            lease.Lease_Start_Date_New__c=date.valueOf('2019-01-01');
            lease.Lease_End_Date_New__c=date.valueOf('2022-12-31');
            update lease;
            for(Assembly_MR_Rate__c rec:lease.Assembly_MR_Rates__r)  
            {
                rec.MR_Rate_Start_Date__c =lease.Lease_Start_Date_New__c;
                rec.MR_Rate_End_Date__c=lease.Lease_End_Date_New__c;
            }
            update lease.Assembly_MR_Rates__r;
            Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                                                Name='Test- PBH Monthly',
                                                Lease__c=lease.id,
                                                Rent_Type__c='Power By The Hour',
                                                Rent_Period__c='Monthly',
                                                Base_Rent__c=10000.00,
                                                Start_Date__c=date.valueOf('2019-01-01') , // YYYY-MM-DD
                                                Rent_End_Date__c=date.valueOf('2019-03-31') ,  // YYYY-MM-DD
                                                Invoice_Generation_Day__c='15',
                                                Rent_Due_Type__c='Fixed',
                                                Rent_Due_Day__c=15,
                                                Escalation__c=3,
                                                Escalation_Month__c='April',
                                                Pro_rata_Number_Of_Days__c='Actual number of days in month/year'
                                                );
            insert MultiRentSch;
            RentQueue.GenerateRent(MultiRentSch.id,0);
            system.debug('lease id='+lease.id+'= MultiId='+MultiRentSch.id);
            
            Constituent_Assembly__c CA=[select id,name from Constituent_Assembly__c where  Attached_Aircraft__r.Lease__c =: lease.id and Type__c='Airframe'];
            PBH_Rent_Rate__c PBHRate = new PBH_Rent_Rate__c(
                                        Name='PBH Rent Name',
                                        Associated_Assembly__c=CA.id ,
                                        From_Value__c= 50,
                                        Multi_Rent_Schedule__c = MultiRentSch.id,
                                        PBH_Rate__c = 150,
                                        Selection__c=True ,
                                        Rate_Basis__c='Hourly');
            
            insert PBHRate;
            
            Utilization_Report__c utilizationRecord = new Utilization_Report__c(
                Name='Testing',
                FH_String__c ='100',
                Airframe_Cycles_Landing_During_Month__c= 50.0,
                Aircraft__c = lease.Aircraft__c,
                Y_hidden_Lease__c = lease.Id,
                Type__c = 'Actual',
                Month_Ending__c = Date.newInstance(2019,01,31),
                Status__c='Approved By Lessor'
            );
            insert utilizationRecord;
            
            LeaseWareUtils.unsetTriggers('RentTriggerHandlerBefore');
        	LeaseWareUtils.unsetTriggers('RentTriggerHandlerAfter');
            set<id> setLeaseIds=new set<id>{lease.id};
            set<id> setURIds=new set<id>{utilizationRecord.id};
            Test.StartTest();
            URTriggerHandler.Upd_PBH_Amt_RentSch(setLeaseIds, setURIds,Null,'InsUpd');
            LeaseWareUtils.unsetTriggers('RentTriggerHandlerBefore');
        	LeaseWareUtils.unsetTriggers('RentTriggerHandlerAfter');
            URTriggerHandler.Upd_PBH_Amt_RentSch(setLeaseIds, setURIds,Null,'Delete');
            
			List<Rent__c> listRentRec = [select id, PBH_Amount__c, Rent_Due_Amount__c,Y_Hidden_Utilization__c from Rent__c where RentPayments__c=:lease.id and Stepped_Rent__c=:MultiRentSch.id order by Start_Date__c] ;
            system.debug('Size of listRentRec='+listRentRec.size());
            system.debug('listRentRec='+listRentRec);
            listRentRec = [select id, PBH_Amount__c, Rent_Due_Amount__c,Y_Hidden_Utilization__c from Rent__c where RentPayments__c=:lease.id and Stepped_Rent__c=:MultiRentSch.id order by Start_Date__c] ;
            system.debug('listRentRec='+listRentRec);
            system.assertEquals(listRentRec[0].PBH_Amount__c,0);
            system.assertEquals(listRentRec[0].Rent_Due_Amount__c,10000);
			system.assertEquals(listRentRec[0].Y_Hidden_Utilization__c,Null);
            Test.StopTest();
        }
		
		@isTest(seeAllData=true)
        static void TestCasePBHQuarterly() {
            Lease__c lease = [select id,Name,Lease_Start_Date_New__c,Lease_End_Date_New__c,Aircraft__c,(Select MR_Rate_End_Date__c,MR_Rate_Start_Date__c from Assembly_MR_Rates__r) from lease__c where name like '%TestSeededMSN3%' limit 1];

            lease.Lease_Start_Date_New__c=date.valueOf('2019-01-01');
            lease.Lease_End_Date_New__c=date.valueOf('2022-12-31');
            update lease;
            for(Assembly_MR_Rate__c rec:lease.Assembly_MR_Rates__r)  
            {
                rec.MR_Rate_Start_Date__c =lease.Lease_Start_Date_New__c;
                rec.MR_Rate_End_Date__c=lease.Lease_End_Date_New__c;
            }
            update lease.Assembly_MR_Rates__r;
            Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                                                Name='Test- PBH Quarterly',
                                                Lease__c=lease.id,
                                                Rent_Type__c='Power By The Hour',
                                                Rent_Period__c='Quarterly',
                                                Base_Rent__c=10000.00,
                                                Start_Date__c=date.valueOf('2019-01-01') , // YYYY-MM-DD
                                                Rent_End_Date__c=date.valueOf('2019-03-31') ,  // YYYY-MM-DD
                                                Invoice_Generation_Day__c='15',
                                                Rent_Due_Type__c='Fixed',
                                                Rent_Due_Day__c=15,
                                                Escalation__c=3,
                                                Escalation_Month__c='April',
                                                Pro_rata_Number_Of_Days__c='Actual number of days in month/year'
                                                );
            insert MultiRentSch;
            RentQueue.GenerateRent(MultiRentSch.id,0);
            system.debug('lease id='+lease.id+'= MultiId='+MultiRentSch.id);
            
            Constituent_Assembly__c CA=[select id,name from Constituent_Assembly__c where  Attached_Aircraft__r.Lease__c =: lease.id and Type__c='Airframe'];
            PBH_Rent_Rate__c PBHRate = new PBH_Rent_Rate__c(
                                        Name='PBH Rent Name',
                                        Associated_Assembly__c=CA.id ,
                                        From_Value__c= 50,
                                        Multi_Rent_Schedule__c = MultiRentSch.id,
                                        PBH_Rate__c = 150,
                                        Selection__c=True ,
                                        Rate_Basis__c='Hourly');
            
            insert PBHRate;
            
            Utilization_Report__c utilizationRecord = new Utilization_Report__c(
                Name='Testing',
                FH_String__c ='100',
                Airframe_Cycles_Landing_During_Month__c= 50.0,
                Aircraft__c = lease.Aircraft__c,
                Y_hidden_Lease__c = lease.Id,
                Type__c = 'Actual',
                Month_Ending__c = Date.newInstance(2019,01,31),
                Status__c='Approved By Lessor'
            );
            LeaseWareUtils.clearFromTrigger();
            insert utilizationRecord;
                                 
            LeaseWareUtils.unsetTriggers('RentTriggerHandlerBefore');
        	LeaseWareUtils.unsetTriggers('RentTriggerHandlerAfter');
           
            set<id> setLeaseIds=new set<id>{lease.id};
            set<id> setURIds=new set<id>{utilizationRecord.id};
            URTriggerHandler.Upd_PBH_Amt_RentSch(setLeaseIds, setURIds,Null,'InsUpd');
            
            utilizationRecord = new Utilization_Report__c(
                Name='Testing',
                FH_String__c ='100',
                Airframe_Cycles_Landing_During_Month__c= 50.0,
                Aircraft__c = lease.Aircraft__c,
                Y_hidden_Lease__c = lease.Id,
                Type__c = 'Actual',
                Month_Ending__c = Date.newInstance(2019,02,28),
                Status__c='Approved By Lessor'
            );
            LeaseWareUtils.unsetTriggers('URTriggerHandlerBefore');
        	LeaseWareUtils.unsetTriggers('URTriggerHandlerAfter');
            insert utilizationRecord;
            
            LeaseWareUtils.unsetTriggers('RentTriggerHandlerBefore');
        	LeaseWareUtils.unsetTriggers('RentTriggerHandlerAfter');
            setURIds=new set<id>{utilizationRecord.id};

            Test.StartTest();
            URTriggerHandler.Upd_PBH_Amt_RentSch(setLeaseIds, setURIds,Null,'InsUpd');
                        
			List<Rent__c> listRentRec = [select id, Start_Date__c, For_Month_Ending__c, PBH_Amount__c, Rent_Due_Amount__c from Rent__c where RentPayments__c=:lease.id and Stepped_Rent__c=:MultiRentSch.id order by Start_Date__c] ;
            system.debug('Size of listRentRec='+listRentRec.size());
            
            List<PBH_Rent_Per_Assembly__c> PBHRPA = [select  Utilization_Unit__c,PBH_Rate_F__c, PBH_Amount__c , Rent_Schedule__c from PBH_Rent_Per_Assembly__c where PBH_Rent_Rate__c = : PBHRate.id  ];
            system.debug('Size of PBHRPA='+PBHRPA.size());
            
            system.assertEquals(PBHRPA[0].Utilization_Unit__c,200);
            system.assertEquals(PBHRPA[0].PBH_Rate_F__c,150);
            system.assertEquals(PBHRPA[0].PBH_Amount__c,22500);
            system.assertEquals(PBHRPA[0].Rent_Schedule__c,listRentRec[0].id);
 
            system.assertEquals(listRentRec[0].Start_Date__c,date.valueof('2019-01-01'));
            system.assertEquals(listRentRec[0].For_Month_Ending__c,date.valueof('2019-03-31'));
            system.assertEquals(listRentRec[0].PBH_Amount__c,22500);
            system.assertEquals(listRentRec[0].Rent_Due_Amount__c,32500);
            Test.StopTest();

        }
    }