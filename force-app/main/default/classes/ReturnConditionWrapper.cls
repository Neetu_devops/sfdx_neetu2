// Wrapper class
public class ReturnConditionWrapper {
    @AuraEnabled public String lastEventUnit{get;set;}
    @AuraEnabled public String reDeliveryUnit{get;set;}
    @AuraEnabled public String type { get; set; }
    @AuraEnabled public Boolean isLLP { get; set; }
    @AuraEnabled public Decimal averageFH { get; set; }
    @AuraEnabled public Decimal averageFC { get; set; }
    @AuraEnabled public Decimal ratioDuringEntireLife { get; set; }
    @AuraEnabled public Decimal ratioSincePR { get; set; }
    @AuraEnabled public Decimal tso { get; set; }
    @AuraEnabled public Decimal totalFH { get; set; }
    @AuraEnabled public Decimal totalRestorationCost { get; set; }
    @AuraEnabled public Decimal rateAtDelivery { get; set; }
    @AuraEnabled public Decimal mrAtReturn { get; set; }
    @AuraEnabled public Decimal csn { get; set; }
    @AuraEnabled public Decimal lessor { get; set; }
    @AuraEnabled public Decimal usageSincePR { get; set; }
    @AuraEnabled public Decimal eola { get; set; }
    @AuraEnabled public Boolean reDeliveryCondition { get; set; }
    @AuraEnabled public Decimal netEOLA { get; set; }
    @AuraEnabled public Decimal costPerFH { get; set; }
    @AuraEnabled public Boolean isEOLA { get; set; }
    @AuraEnabled public String extType { get; set; }
    @AuraEnabled public String reserveType { get; set; } //added on 27-03-2109
    @AuraEnabled public String assemblyType { get; set; }
    @AuraEnabled public String assemblyImage { get; set; }
    
    public ReturnConditionWrapper(){
        this.type = type;
        this.averageFH = 0.0;
        this.averageFC = 0.0;
        this.tso = 0.0;
        this.csn = 0.0;
        this.totalFH = 0.0;
        this.totalFH = 0.0;
        this.ratioDuringEntireLife = 0.0;
        this.totalRestorationCost = 0.0;
        this.rateAtDelivery = 0.0;
        this.mrAtReturn = 0.0;
        this.lessor = 0.0;
        this.eola = 0.0;
        this.netEOLA = 0.0;
        this.costPerFH = 0.0;
        this.isEOLA = false;
        this.reserveType = '';
    }
    
    public class CummulativePerMonthWrapper{
        @AuraEnabled public Decimal investment { get; set; }
        @AuraEnabled public Date investmentDate { get; set; }
        @AuraEnabled public Decimal vpn { get; set; }
        @AuraEnabled public Decimal irr { get; set; }
    }    
    
    public class CashFlowPerMonthMainWrapper{
        @AuraEnabled public List<String> lstHeader { get; set; }
        @AuraEnabled public List<CashFlowPerMonthWrapper> lstCashFlowPerMonthWrapper { get; set; }
        
        public CashFlowPerMonthMainWrapper(){
            this.lstCashFlowPerMonthWrapper = new List<CashFlowPerMonthWrapper>();
        }
    }
    public class CashFlowPerMonthWrapper {
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public Decimal total { get; set; }
        @AuraEnabled public List<Decimal> lstvalue { get; set; }
        
        public CashFlowPerMonthWrapper(){
            this.lstvalue = new List<Decimal>();
        }
    }
}