public without sharing class AircraftRecommendationController {

    public static Map<Integer,String> colorMap = new Map<Integer,String>
    	{1 => '228B22', 2 => '32CD32', 3 => 'FF7F50' , 4 => 'C0C0C0' };
    public static Map<Integer,Integer> sizeMap = new Map<Integer,Integer>
        {1 => 40, 2 => 28, 3 => 22 , 4 => 18 };

	@AuraEnabled
    public static List <Integer> getVintagePicklist(){
        List <Integer> picklist = new List <Integer>();
        Integer curYear = System.today().year();
        for(integer i = curYear - 30; i < curYear + 35; i++ ){
            picklist.add(i);
        }
        return picklist;
    }
    
    @AuraEnabled
    public static String getNameSpacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(String objDetail, string contrfieldApiName,string depfieldApiName) {
        String namespace = getNameSpacePrefix();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(namespace+objDetail); 
        SObject myObj = targetType.newSObject();
        
    	Map<String, List<String>> objRes = DependentFilters.getDependentMap(myObj,contrfieldApiName,depfieldApiName);
        return objRes;
    }

    public static AircraftWrapper.Properties fillAirlineData(AircraftWrapper.Properties aircraftProperties){
        aircraftProperties.color = colorMap.get(aircraftProperties.rank);
        aircraftProperties.size =  sizeMap.get(aircraftProperties.rank);
        return aircraftProperties;
    }
    
    @AuraEnabled
    public static String getOperatorId(String objectId) {
        Id recordId = Id.valueOf(objectId);
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        if(sObjName.contains('Operator__c')) {
            return objectId;
        } else if(sObjName.contains('Marketing_Activity__c')) {
            List<Marketing_Activity__c> dealsList = [select Prospect__c 
                                                     from Marketing_Activity__c 
                                                     where Id =: recordId and Lessor__c = null and 
                                                     Counterparty__c = null and 
                                                     RecordType.Name IN ('Lease', 'Sale')
                                                    ]; 
            if(dealsList.size() > 0) {
                return dealsList[0].Prospect__c;
            }
        }
        return null;
    }
    
    
    @AuraEnabled
    public static List<AircraftWrapper.Properties> getAircraftsRecommended(String operatorId, String aircraftType, String variant, 
                                                                    String engine, integer vintage, 
                                                                    integer vintageRange, integer mtow, integer mtowRange){
        system.debug('getAircraftsRecommended operatorId: '+operatorId);
		system.debug('getAircraftsRecommended '+aircraftType + ', '+variant+ ', '+engine+ ', '
					+vintage+ ', '+vintageRange + ', '+mtow+ ', '+mtowRange);
		//system.debug('getAircraftsRecommended leaseExpiry:'+leaseExpiry);
		
		String recordId = getOperatorId(operatorId);
        system.debug('getAircraftsRecommended recordId:'+recordId);
		if(String.isEmpty(recordId))   
            return null;
        List<AircraftWrapper.Properties> aircraftList = AircraftUtils.getAircraft(recordId,aircraftType, variant, engine,
                                                                                     vintage, vintageRange, mtow, mtowRange);
        if(aircraftList == null) 
            return null;
        for(AircraftWrapper.Properties aircraft : aircraftList) 
            fillAirlineData(aircraft);
        return aircraftList;
    }
    
    @AuraEnabled
    public static String getDealType(){
        string DealType = getLWSetup_CS('APP_Deal_Type');
        system.debug('DealType==' + DealType);
        if (DealType == null) return 'MA';
        else if (DealType == 'MC-ONLY')
            return 'MC';
        else if (DealType == 'MC')  
            return 'MC';
        else 
            return 'MA';
    }
    
    public static string getLWSetup_CS(string TypeStr){
        LW_Setup__c lwSetup = LW_Setup__c.getValues(TypeStr);
        if(!isSetupDisabled(TypeStr) && lwSetup!=null  ) return lwSetup.value__c ;
        return null;
	}     
    
    public static boolean isSetupDisabled(string TypeStr){
        LW_Setup__c lwSetup = LW_Setup__c.getValues(TypeStr);
        if(lwSetup!=null) return lwSetup.Disable__c ;
        return false;
	}
    
    @AuraEnabled
    public static String newMarkActivity(String operatorId , String msn , String fleetId){
        System.debug('createMarketingActivity operatorId:'+operatorId + ' msn:'+msn);
        List<Operator__c> operator = [select Name from Operator__c where Id =: operatorId];
        if(operator.size() <= 0)
            return null;
        String actName = operator[0].Name + ' '+ msn;
        system.debug('createMarketingActivity name ' + actName);
        String status = getDealStatus();
        if(status == null)
            return null;
        String RecordTypeName = 'Lease';
        Map < String, Schema.RecordTypeInfo > Trans_MapByName = Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByName();
        Id recordTypeId_lease = Trans_MapByName.get(RecordTypeName).getRecordTypeId();
        if(recordTypeId_lease == null) return null;
        Marketing_Activity__c activity = new Marketing_Activity__c (
            RecordTypeId = recordTypeId_lease ,
            Name = actName, 
            Prospect__c = operatorId ,
            Deal_Status__c = status,
            Asset_Type__c = 'Aircraft' ,
            Description__c = 'Activity is created from aircraft recommendation' 
        );
        system.debug('createMarketingActivity '+activity); 
        LeaseWareUtils.clearFromTrigger();
        insert activity;
        
        String RecordAircraftTypeName = 'Lease';
        Map < String, Schema.RecordTypeInfo > Trans_MapByNameAsset = Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByName();
        Id recordTypeAssetId_lease = Trans_MapByNameAsset.get(RecordAircraftTypeName).getRecordTypeId();
        if(recordTypeAssetId_lease == null) return activity.Id;
        
        Aircraft_Proposal__c assetTerm = new Aircraft_Proposal__c(
            RecordTypeId = recordTypeAssetId_lease ,
        	Name = msn,
            Marketing_Activity__c = activity.Id, 
            Aircraft__c = fleetId,
            New_Used__c = 'Used'
        );
        LeaseWareUtils.clearFromTrigger();
        insert assetTerm;
        
		return activity.Id;
    }
    
    @AuraEnabled
    public static String isDealAlreadyCreated(String operatorId, String assetId){
        try{
            List<Aircraft_Proposal__c> asset = [
                select Name, Marketing_Activity__c from Aircraft_Proposal__c 
                where 
                Marketing_Activity__c in (select Id from Marketing_Activity__c where Prospect__c = :operatorId) 
                and 
                Aircraft__c = :assetId
            ];
            return asset[0].Marketing_Activity__c;
        }
        catch(Exception e){
            System.debug('isDealAlreadyCreated Exception '+e);
        }
        return null;
    }
    
    
    public static String getDealStatus(){
        Schema.DescribeFieldResult fieldResult =
            Marketing_Activity__c.Deal_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if(ple != null && ple.size() > 0){
            Schema.PicklistEntry f = ple[0];
            system.debug('getDealStatus '+f.getLabel() +' val:'+f.getValue());
            return f.getValue();
        }
        else 
            return null;
    }
    
    @AuraEnabled
    public static AircraftWrapper.Properties getAircraftCriteria(String aircraft){
        system.debug('getAirlineCriteria '+aircraft);
        AircraftWrapper.Properties aircraftDetails = (AircraftWrapper.Properties)JSON.deserialize(aircraft, AircraftWrapper.Properties.class);
        system.debug('getAirlineCriteria aircraftDetails: '+aircraftDetails);
        aircraftDetails.leasesList = getLeases(aircraftDetails.operatorId, aircraftDetails.aircraftType);
        aircraftDetails.leasesVsOwned = getLeasedVsOwned(aircraftDetails.operatorId, aircraftDetails.operatorName,aircraftDetails.aircraftType);
        aircraftDetails.leasedCount = getLeasedCount(aircraftDetails.operatorId, aircraftDetails.operatorName,aircraftDetails.aircraftType);
        aircraftDetails.aircraftTypeCount = getTotalCount(aircraftDetails.operatorId, aircraftDetails.operatorName,aircraftDetails.aircraftType);
        
        
        return aircraftDetails;
    } 
    
    public static Map<String,String> getLeases(String operatorId, String assetType){
        try{
            List<Lease__c> leaseList = [
                select Id, Name, Lease_End_Date_New__c ,Aircraft__r.Aircraft_Type__c, Aircraft__r.Engine_Type__c, 
                Aircraft__r.Vintage__c, Aircraft__r.MTOW_Leased__c,
                Aircraft__r.Date_of_Manufacture__c ,
                Serial_Number__c 
                from Lease__c 
                where Operator__r.Id =: operatorId
                and Aircraft__r.Aircraft_Type__c =: assetType
                order by Lease_Start_Date_New__c desc
            ];
            Map<String,String> leasesMap = new Map<String,String>();
            for(Lease__c lease: leaseList) {
                String params = '';
                if(lease.Lease_End_Date_New__c != null) {
                    Datetime output = lease.Lease_End_Date_New__c;
                    params =  'Lease End Date: '+output.format('MMM-dd');
                }
                if(lease.Aircraft__r.Engine_Type__c != null)
                    params = params + ', Engine: '+ lease.Aircraft__r.Engine_Type__c;
                if(lease.Aircraft__r.Date_of_Manufacture__c != null)
                    params = params + ', Vintage: '+ lease.Aircraft__r.Date_of_Manufacture__c.year() ;
                if(lease.Aircraft__r.MTOW_Leased__c != null)
                    params = params + ', MTOW '+ lease.Aircraft__r.MTOW_Leased__c ;
                String details = lease.Serial_Number__c + ' ('+params+')';
                if(lease.Serial_Number__c == null)
                	details = lease.Name + ' ('+params+')';
                //leaseDetails = leaseDetails + details;
                leasesMap.put(lease.Id, details);
            }
            // System.debug('getLeases leaseDetails: '+leaseDetails);
            if(leaseList.size() <= 0)
                return null;
        	return leasesMap;
        }
        catch(Exception e){return null;}
    }
    
    public static List<String> getLeasedVsOwned(String operatorId, String operatorName, String assetType) {
        try{
            List<AggregateResult> fleetList = [
                select Manager__c owner, count(Operator_ID__c) from Global_Fleet__c where 
                Aircraft_Operator__r.id =: operatorId  and 
                Manager__c !=: operatorName and
                AircraftType__c =: assetType 
                group by Manager__c
                order by count(Operator_ID__c) desc
            ];
            system.debug('getLeasedVsOwned '+operatorId + ' '+operatorName + ' '+assetType);
            system.debug('getLeasedVsOwned '+String.valueOf(fleetList));
            List<String> leasedOwnedDetails = new List<String>();
            if(fleetList.size() > 0) {
                for(AggregateResult sObj: fleetList) {
                    String detail = sObj.get('owner')+ ' ('+sObj.get('expr0')+')';
					leasedOwnedDetails.add(detail);          
                }
                return leasedOwnedDetails;
            }
            return null;
        }
        catch(Exception e) {
            return null;
        }
    }
   
    public static Integer getLeasedCount(String operatorId, String operatorName, String assetType) {
        try{
            List<AggregateResult> fleetList = [
                select Manager__c, count(Operator_ID__c) from Global_Fleet__c where 
                Aircraft_Operator__r.id =: operatorId  and 
                Manager__c !=: operatorName and
                AircraftType__c =: assetType 
                group by Manager__c
                order by count(Operator_ID__c) desc
            ];
            system.debug('getLeasedVsOwned '+fleetList.size());
            if(fleetList.size() > 0) {
                integer count = 0;
                for(AggregateResult sObj: fleetList) {
                    count = count + Integer.valueOf(sObj.get('expr0'));
                }
                return count;
            }
            return 0;
        }
        catch(Exception e) {
            return 0;
        }
    }
    
    public static Integer getTotalCount(String operatorId, String operatorName, String assetType) {
        try{
            AggregateResult fleetCount = [
                select count(Operator_ID__c) totalCount from Global_Fleet__c where 
                Aircraft_Operator__r.id =: operatorId  and 
                AircraftType__c =: assetType 
            ];
            system.debug('getTotalCount '+fleetCount);
            integer count = Integer.valueOf(fleetCount.get('totalCount'));
            return count;
        }
        catch(Exception e) {
            return 0;
        }
    }
    
}