public class CashFlowTriggerHandler implements  ITrigger{
    
    private final string triggerBefore = 'CashFlowTriggerHandlerBefore';
    private final string triggerAfter = 'CashFlowTriggerHandlerAfter';
    
    public CashFlowTriggerHandler (){}
    
    /**
    * bulkBefore
    *
    * This method is called prior to execution of a BEFORE trigger. Use this to cache
    * any data required into maps prior execution of the trigger.
    */
    public void bulkBefore(){}
    
    public void bulkAfter(){}
    
    public void beforeInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('CashFlowTriggerHandler.beforeInsert(+)');
        
        validateCashFlowName(Trigger.new);

        system.debug('CashFlowTriggerHandler.beforeInsert(-)');    
    }
    
    public void beforeUpdate(){
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('CashFlowTriggerHandler.beforeUpdate(+)');
        
        validateCashFlowName(Trigger.new);
        
        system.debug('CashFlowTriggerHandler.beforeUpdate(-)');
    }
    
    /**
    * beforeDelete
    *
    * This method is called iteratively for each record to be deleted during a BEFORE
    * trigger.
    */
    public void beforeDelete(){}
    
    public void afterInsert(){}
    
    public void afterUpdate(){}
    
    public void afterDelete(){}
    
    public void afterUnDelete(){}
    
    public void andFinally(){
        // insert any audit records
        LeaseWareUtils.TriggerDisabledFlag = false; // to make Trigger disabled at apex level  
    }
    
    public static void validateCashFlowName(List<Scenario_Input__c> cashFlowList){
        List<Scenario_Input__c> filteredCashFlows = new List<Scenario_Input__c>();
        
        //filter records.
        for(Scenario_Input__c cashFlowObj : cashFlowList){
            if((Trigger.isUpdate && cashFlowObj.name != ((Scenario_Input__c)Trigger.oldMap.get(cashFlowObj.Id)).name && cashFlowObj.name != null) ||
                (Trigger.isInsert && cashFlowObj.name != null)){
                    filteredCashFlows.add(cashFlowObj);
            }
        }
        
        if(!filteredCashFlows.isEmpty()){
            //query existing ones and exclude filtered ones.
            Set<String> existingCashFlows = new Set<String>();
            String qry = 'SELECT Id, Name FROM Scenario_Input__c';
            if(Trigger.isUpdate){
                qry  +=  ' WHERE Id not IN : filteredCashFlows' ;
            }
            
            for(Scenario_Input__c cashFlowObj : Database.query(qry)){
                existingCashFlows.add(cashFlowObj.name.toLowerCase());
            }
            
            for(Scenario_Input__c cashFlowObj : filteredCashFlows){
                if(existingCashFlows.contains(cashFlowObj.name.toLowerCase())){
                    cashFlowObj.addError('Cashflow scenario with this name already exists. Please use another scenario name.');
                }else{
                    existingCashFlows.add(cashFlowObj.name.toLowerCase());
                }      
            }     
        }
    }
}