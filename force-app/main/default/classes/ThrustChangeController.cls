public class ThrustChangeController {
    
    public class AssemblyWrapper {
        @AuraEnabled
        public Id engineTemplateId {get;set;}
        @AuraEnabled
        public String engineTemplateName {get;set;}
        @AuraEnabled
        public Id assemblyId {get;set;}
        @AuraEnabled
        public Id assetId {get;set;}
        @AuraEnabled
        public Id leaseId {get; set;}
        @AuraEnabled
        public String assemblyName {get;set;}
        @AuraEnabled
        public String assetName {get;set;}
        @AuraEnabled
        public List<String> approvalStatus {get;set;}
        @AuraEnabled
        public String selectedApprovalStatus {get; set;}
        @AuraEnabled
        public Decimal assemblyTSN {get;set;}
        @AuraEnabled
        public Decimal assetTSN {get;set;}
        @AuraEnabled
        public Decimal assemblyCSN {get;set;}
        @AuraEnabled
        public Decimal assetCSN {get;set;}
        @AuraEnabled
        public Id CurrThrustId {get; set;}
        @AuraEnabled
        public String CurrThrustName {get; set;}
        @AuraEnabled
        public List<Custom_Lookup__c> newThrusts {get;set;}
        @AuraEnabled
        public String currentTS {get;set;}
        @AuraEnabled
        public String reason {get;set;}
    }
    
    @AuraEnabled
    public static AssemblyWrapper getAssemblyData(Id recordId) {
        AssemblyWrapper assemblyData = new AssemblyWrapper();
        
        Constituent_Assembly__c assembly = [SELECT Id,Engine_Template_CA__c,Engine_Template_CA__r.Name, Asset__c, 
                                            Name, Asset__r.Name, Asset__r.Lease__c,
                                            TSN__c, CSN__c, Asset__r.TSN__c, Asset__r.CSN__c, 
                                            Engine_Model__c, Current_TS__c, Current_TS__r.Name 
                                            FROM Constituent_Assembly__c 
                                            WHERE ID = :recordId];
        
        List<Custom_Lookup__c> thrusts = [SELECT Id, Name FROM Custom_Lookup__c 
                                          WHERE Lookup_Type__c = 'Engine' 
                                          AND (Sub_LookupType__c = :assembly.Engine_Model__c 
                                          OR Sub_LookupType__c = null) 
                                          AND Active__c = true 
                                          AND ID != :assembly.Current_TS__c];
        
        assemblyData.engineTemplateId = assembly.Engine_Template_CA__c;
        assemblyData.engineTemplateName = assembly.Engine_Template_CA__r.name;
        assemblyData.assemblyId = assembly.Id;
        assemblyData.assetId = assembly.Asset__c;
        assemblyData.leaseId = assembly.Asset__r.Lease__c;
        assemblyData.assemblyName = assembly.Name;
        assemblyData.assetName = assembly.Asset__r.Name;
        List<String> approvalStatus = new List<String>{'Approved','Not Required'};
        assemblyData.approvalStatus = approvalStatus;
        assemblyData.assemblyTSN = assembly.TSN__c;
        assemblyData.assetTSN = assembly.Asset__r.TSN__c;
        assemblyData.assemblyCSN = assembly.CSN__c;
        assemblyData.assetCSN = assembly.Asset__r.CSN__c;
        assemblyData.CurrThrustName = assembly.Current_TS__r.Name;
        assemblyData.CurrThrustId = assembly.Current_TS__c;
        assemblyData.newThrusts = thrusts;
        assemblyData.reason = '';
        
        return assemblyData;
    }
    
    @AuraEnabled
    public static Id saveData(String assemblyData, Date userSelectedDate) {
        
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
        Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        List<Assembly_Utilization__c> assemblyUtilizations = [SELECT TSN_At_Month_Start__c, TSN_At_Month_End__c, 
                                                                      CSN_At_Month_Start__c, CSN_At_Month_End__c
                                                                      FROM Assembly_Utilization__c 
                                                                      WHERE Utilization_Report__r.Aircraft__c = :wrapperData.assetId
                                                                      AND Constituent_Assembly__c = :wrapperData.assemblyId
                                                                      AND (For_The_Month_Ending__c = :lastDayOfMonth
                                                                      OR For_The_Month_Ending__c = :lastDayOfPreviousMonth)
                                                                      AND Utilization_Report__r.Type__c = 'Actual' order by End_Date_F__c desc];
        
        Boolean isTSNValid = false;   
        Boolean isCSNValid = false; 
        Constituent_Assembly__c assembly = new Constituent_Assembly__c();
        
        if(assemblyUtilizations.size() > 0) {
            
            for(Assembly_Utilization__c assemblyUtilization : assemblyUtilizations) {
                
                if(wrapperData.assemblyTSN <= assemblyUtilization.TSN_At_Month_End__c 
                   && wrapperData.assemblyTSN >= assemblyUtilization.TSN_At_Month_Start__c) {
                       isTSNValid = true;
                }
                if(wrapperData.assemblyCSN <= assemblyUtilization.CSN_At_Month_End__c
                   && wrapperData.assemblyCSN >= assemblyUtilization.CSN_At_Month_Start__c) {
                      isCSNValid = true; 
                }
            }
        } else {
            assembly = [SELECT Id, TSN__c, CSN__c, Asset__r.TSN__c, Asset__r.CSN__c
                        FROM Constituent_Assembly__c WHERE ID = :wrapperData.assemblyId];
            
            if(wrapperData.assemblyTSN == assembly.TSN__c) {
                isTSNValid = true;
            }
            if(wrapperData.assemblyCSN == assembly.CSN__c) {
                isCSNValid = true;
            }
        }
        if(!isTSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Thrust Change TSN');  
        }
        if(!isCSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Thrust Change CSN');  
        }
        
        List<Utilization_Report__c> utilizations = [SELECT Current_TSN__c, TSN_At_Month_End__c, Current_CSN__c, CSN_At_Month_End__c 
                                                    FROM Utilization_Report__c 
                                                    WHERE Aircraft__c = :wrapperData.assetId 
                                                    AND (Month_Ending__c = :lastDayOfMonth 
                                                    OR Month_Ending__c = :lastDayOfPreviousMonth)
                                                    AND Type__c = 'Actual' order by End_Date_F__c desc];
        Boolean isACTSNValid = false;
        Boolean isACCSNValid = false;
        
        if(utilizations.size() > 0) {
            
            for(Utilization_Report__c utilization : utilizations) {
                if(wrapperData.assetTSN <= utilization.TSN_At_Month_End__c 
                    && wrapperData.assetTSN >= utilization.Current_TSN__c) {
                       isACTSNValid = true;
                }
                if(wrapperData.assetCSN <= utilization.CSN_At_Month_End__c
                   && wrapperData.assetCSN >= utilization.Current_CSN__c) {
                       isACCSNValid = true;
                }
            }
        } else {
            
            if(String.isEmpty(assembly.Id))
                assembly = [SELECT Asset__r.TSN__c, Asset__r.CSN__c 
                            FROM Constituent_Assembly__c WHERE ID = :wrapperData.assemblyId];
            
            if(wrapperData.assetTSN == assembly.Asset__r.TSN__c) {
                isACTSNValid = true;
            }
            if(wrapperData.assetCSN == assembly.Asset__r.CSN__c) {
                isACCSNValid = true;
            }
        }
        if(!isACTSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Thrust Change AC TSN');  
        }
        if(!isACCSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Thrust Change AC CSN');
        }
        AssemblyTriggerHandler CAInst = new AssemblyTriggerHandler();
        
        
        CAInst.changeThrust(wrapperData.assemblyId,wrapperData.assemblyName,wrapperData.assetId,userSelectedDate,
                            wrapperData.leaseId,wrapperData.assemblyTSN,wrapperData.assemblyCSN,wrapperData.assetTSN,
                            wrapperData.assetCSN,wrapperData.reason,wrapperData.selectedApprovalStatus,wrapperData.currentTS);
        
        return wrapperData.assemblyId;
    }
    
    @AuraEnabled
    public static AssemblyWrapper getTsnCsnValuesOnDateChange(String assemblyData, Date userSelectedDate) {
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
        Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        Decimal oldAssemblyTSN = wrapperData.assemblyTSN;
        Decimal oldAssemblyCSN = wrapperData.assemblyCSN;
        Decimal oldAssetTSN = wrapperData.assetTSN;
        Decimal oldAssetCSN = wrapperData.assetCSN;
        
        wrapperData.assetTSN = null;
        wrapperData.assetCSN = null;
        wrapperData.assemblyTSN = null;
        wrapperData.assemblyCSN = null;
            
        for(Assembly_Utilization__c assemblyUtilization : [SELECT TSN_At_Month_Start__c, TSN_At_Month_End__c, 
                                                                   CSN_At_Month_Start__c, CSN_At_Month_End__c
                                                                   FROM Assembly_Utilization__c 
                                                                   WHERE Utilization_Report__r.Aircraft__c = :wrapperData.assetId
                                                                   AND Constituent_Assembly__c = :wrapperData.assemblyId
                                                                   AND (For_The_Month_Ending__c = :lastDayOfMonth
                                                                   OR For_The_Month_Ending__c = :lastDayOfPreviousMonth)
                                                                   AND Utilization_Report__r.Type__c = 'Actual' order by End_Date_F__c desc LIMIT 1]) {
            
            wrapperData.assemblyTSN = assemblyUtilization.TSN_At_Month_End__c;
            wrapperData.assemblyCSN = assemblyUtilization.CSN_At_Month_End__c;
        }  
        if(wrapperData.assemblyTSN == null || wrapperData.assemblyCSN == null) {
            wrapperData.assemblyTSN = oldAssemblyTSN;
            wrapperData.assemblyCSN = oldAssemblyCSN;
        }
        
        for(Utilization_Report__c utilization : [SELECT TSN_At_Month_End__c, CSN_At_Month_End__c 
                                                 FROM Utilization_Report__c 
                                                 WHERE Aircraft__c = :wrapperData.assetId 
                                                 AND (Month_Ending__c = :lastDayOfMonth 
                                                      OR Month_Ending__c = :lastDayOfPreviousMonth)
                                                 AND Type__c = 'Actual' 
                                                 AND TSN_At_Month_End__c != null 
                                                 AND CSN_At_Month_End__c != null order by End_Date_F__c desc LIMIT 1]) {
            wrapperData.assetTSN = utilization.TSN_At_Month_End__c; 
            wrapperData.assetCSN = utilization.CSN_At_Month_End__c;
               
        }
        if(wrapperData.assetTSN == null || wrapperData.assetCSN == null) {
            wrapperData.assetTSN = oldAssetTSN;
            wrapperData.assetCSN = oldAssetCSN;
        }
        
        return wrapperData;
    }
}