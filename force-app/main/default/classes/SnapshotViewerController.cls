public without sharing class SnapshotViewerController {
    
    private static JsonData singleSnapshot = new JsonData();
    
    /**
     * Get's all the snapshots for a particular record Id 
     * Algo
     * 1) Get's all snapshots 
     * 2) Fetches ContentDocumentLink based on those snapshot Id's and the record Id 
     * 3) Title of the document contains objectName and record Id
    */
    @AuraEnabled
    public static SnapshotWrapper getAllSnapshots(Id recordId, Integer pageNumber, Integer pageSize) {
        System.debug('getDataFromSnapshot recordId: '+recordId);
        if(recordId == null)
            return null;
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        String fileName = sObjName+'-'+recordId;
        
        Set<Id> snapshotIds = new Set<Id>();
        for(Snapshot__c snapshot: [select Id from Snapshot__c])
            snapshotIds.add(snapshot.Id);
        
        System.debug('getDataFromSnapshot snapshots '+snapshotIds.size());
        if(snapshotIds.size() <= 0)
            return null;
        
        Set<Id> snapshotsWithFiles = new Set<Id>();
        String recordIdLike = '%'+recordId;
        List<ContentDocumentLink> ContentDocumentLinks =  [SELECT ContentDocumentId, LinkedEntityId, Id
                                                           FROM ContentDocumentLink WHERE  
                                                           ContentDocument.title like :recordIdLike  AND 
                                                           LinkedEntityId in :snapshotIds];
        if(ContentDocumentLinks != null && ContentDocumentLinks.size() > 0) {
            for(ContentDocumentLink docLink : ContentDocumentLinks)
                snapshotsWithFiles.add(docLink.ContentDocumentId);
            List<ContentVersion> contentList = [select Id, versionData, FirstPublishLocationId from contentVersion
                                                where title like :recordIdLike 
                                                AND ContentDocumentId in :snapshotsWithFiles];
                        
            System.debug('getDataFromSnapshot contentList '+contentList.size());
            //Releasing memory
			ContentDocumentLinks = null;    
            snapshotIds = null;
            snapshotsWithFiles = null;
            
            if(contentList.size() > 0) {
                List<Id> snapshotIdList = new List<Id>();
                for(ContentVersion content: contentList) {
                    snapshotIdList.add(content.FirstPublishLocationId);
                }
                //system.debug('getDataFromSnapshot snapshotIdList: '+snapshotIdList);
                contentList = null;
                
                Integer pSize = pageSize;
                Integer pNumber = pageNumber;
                Integer offset = (pNumber - 1) * pSize;
                Integer totalRecords = [select COUNT() from Snapshot__c where Id in :snapshotIdList];
                Integer recordEnd = pSize * pNumber;
                SnapshotWrapper objDT =  new SnapshotWrapper();  
                objDT.pageSize = pSize;
                objDT.pageNumber = pNumber;
                objDT.recordStart = offset + 1;
                objDT.recordEnd = totalRecords >= recordEnd ? recordEnd : totalRecords;
                objDT.totalRecords = totalRecords;
                
                List<Snapshot__c> snapshotList = [select Id, Name,Comment__c, Type__c, Y_Hidden_Type_API__c, Record_id__c, Date__c 
                                                  from Snapshot__c 
                                                  where Id in :snapshotIdList 
                                                  order by Date__c desc
                                                  LIMIT :pSize OFFSET :offset 
                                                 ];
                snapshotIdList = null;
                List<SnapshotDetails> snapshotDetailsList = new List<SnapshotDetails>();
                for(Snapshot__c snapshot: snapshotList) {
                    SnapshotDetails snapshotDetail = new SnapshotDetails();
                    snapshotDetail.SnapshotId = snapshot.Id;
                    snapshotDetail.SnapshotName = snapshot.Name;
                    snapshotDetail.SnapshotDate = snapshot.Date__c;
                    snapshotDetail.SnapshotComment = snapshot.Comment__c;
                    snapshotDetailsList.add(snapshotDetail);
                }
                snapshotList = null;
                //system.debug('getDataFromSnapshot singleSnapshot: ' +JSON.serializePretty(singleSnapshot));
                objDT.SnapshotItems = snapshotDetailsList;
                return objDT;
            }
        }
        return null;
    }
    

    /**
     * Retrieves all the json under a particular snapshot
     * Algo
     * 1) Get's all json and construct a hierarchy based on the configuration json  
     * 2) Once the hierarchy is constructed number of items for each objects are caluclated
    */    
    @AuraEnabled
    public static List<SnapshotDetails> getSingleSnapshotData(Id snapshotId, Id recordId) {
        List<Snapshot__c> snapshotList = [select Id, Name, Type__c, Comment__c, Y_Hidden_Type_API__c, Record_id__c, Date__c 
                                          from Snapshot__c 
                                          where Id = :snapshotId];
        List<SnapshotDetails> snapshotDetailsList = new List<SnapshotDetails>();
        //have only one item
        for(Snapshot__c snapshot: snapshotList) {
            SnapshotDetails snapshotDetail = new SnapshotDetails();
            snapshotDetail.SnapshotId = snapshot.Id;
            snapshotDetail.SnapshotName = snapshot.Name;
            snapshotDetail.SnapshotDate = snapshot.Date__c;
            snapshotDetail.SnapshotComment = snapshot.Comment__c;
            parseSingleSnapshot(snapshot, recordId);
            countCalculation(singleSnapshot.items);
            snapshotDetail.items = singleSnapshot.items;
            snapshotDetailsList.add(snapshotDetail);
            singleSnapshot.items = new List<JsonData>();
        }
        snapshotList = null;
        //system.debug('getSnapshotData singleSnapshot: ' +JSON.serializePretty(singleSnapshot));
        return snapshotDetailsList;
    }
    
    /**
     * Algo
     * 1) Queries all the json under a snaphot 
     * 2) Stores it in map with object name as key
     * 3) Configuration json is read and parsed based on it
     */
    public static void parseSingleSnapshot(Snapshot__c snapshot, String recordId) {
        RelatedObjectJson relatedObjectMap = getSnapshotConfiguration(snapshot.Id);
        
        String snapshotId = snapshot.Id;
        List<ContentDocumentLink> contentDocumentLinks = [SELECT ContentDocumentId, LinkedEntityId, Id
                                                          FROM ContentDocumentLink 
                                                          where LinkedEntityId =: snapshotId];
        
        Set<Id> snapshotsWithFiles = new Set<Id>();
        if(contentDocumentLinks.size() > 0) {
            for(ContentDocumentLink docLink : contentDocumentLinks)
                snapshotsWithFiles.add(docLink.ContentDocumentId);
        }
        contentDocumentLinks = null;
        List<ContentVersion> contentList = [select Id, versionData, FirstPublishLocationId, Title from contentVersion
                                            where ContentDocumentId in :snapshotsWithFiles];
        if(contentList.size() <= 0)
            return;
        snapshotsWithFiles = null;
        
        Map<String,List<ContentVersion>> snapshotFiles = new Map<String,List<ContentVersion>>();
        for(ContentVersion contentVersion: contentList) {
            if(String.isBlank(contentVersion.Title)) continue;
            
            String[] keyArr = contentVersion.Title.split('-');
            if(contentVersion.Title.contains('-') && keyArr != null && keyArr.size() > 0 ) {
                if(snapshotFiles.get(keyArr[0]) == null) {
                    List<ContentVersion> cv = new List<ContentVersion>();
                    cv.add(contentVersion);
                    snapshotFiles.put(keyArr[0], cv);
                }
                else {
                    List<ContentVersion> cv = snapshotFiles.get(keyArr[0]);
                    cv.add(contentVersion);
                    snapshotFiles.put(keyArr[0], cv);
                }
            }
        }
        contentList = null;
        //System.debug('parseSingleSnapshot snapshotFiles '+snapshotFiles.size());
        
        
        if(relatedObjectMap == null) {
            //directly read object.json and show
            List<RelatedObjectJson> parentObjList = new List<RelatedObjectJson>();
            RelatedObjectJson relatedJson = new RelatedObjectJson();
            relatedJson.obj_api = Id.valueOf(recordId).getSObjectType().getDescribe().getName();
            relatedJson.related_obj_api = '';
            relatedJson.child = null;
            parentObjList.add(relatedJson);
            
            constructJsonForSnapshot(snapshot,parentObjList, recordId, singleSnapshot, snapshotFiles);
        }
        else {
            
            String obj_api = relatedObjectMap.obj_api;
            String rel_obj_api = relatedObjectMap.related_obj_api;
            List<RelatedObjectJson> childList = relatedObjectMap.child;
            
            List<RelatedObjectJson> parentObjList = new List<RelatedObjectJson>();
            parentObjList.add(relatedObjectMap);
            
            constructJsonForSnapshot(snapshot,parentObjList, recordId, singleSnapshot, snapshotFiles);
            
        } 
    }
    
    /**
     * Construction of record hierarchy is done.
     * Based on Configuration json file, child parent relation is constructed for all the records
     * 
     */ 
    public static void constructJsonForSnapshot(Snapshot__c snapshot, List<RelatedObjectJson> childList, String parentId, JsonData snapshotJson, Map<String,List<ContentVersion>> snapshotFiles) {
        if(childList == null) {
            return;
        }
        for(RelatedObjectJson childMap: childList) {
            String obj_api = childMap.obj_api;
            String rel_obj_api = childMap.related_obj_api;
            List<RelatedObjectJson> subChildList = childMap.child;
            
            if(snapshotJson.items == null)
                snapshotJson.items = new List<JsonData>();
            JsonData headerJson = new JsonData();
            
            List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{obj_api}); 
            String obj_label = describeSobjectsResult[0].getLabel();
            
            String snapshotId = snapshot.Id;
            
            if(snapshotFiles != null && snapshotFiles.size() > 0) {
                
                List<ContentVersion> contentList = snapshotFiles.get(obj_api);
                //System.debug('parseSingleSnapshot obj_api '+obj_api);
                //System.debug('parseSingleSnapshot contentList '+contentList);
                String childId;
                if(contentList != null && contentList.size() > 0) {
                    headerJson.label = '#'+obj_label;
                    headerJson.Name = obj_api;
                    headerJson.expanded = true;
//                    snapshotJson.items.add(headerJson);
                    String childRelatedObj = null;
                    for(ContentVersion contentVersion: contentList) {
                        List<SnapshotObjectField> snapshotObjectFieldList;
                        boolean isChild = false;
                        snapshotObjectFieldList = (List<SnapshotObjectField>) JSON.deserialize(contentVersion.versionData.toString(),List<SnapshotObjectField>.class );
                       //  system.debug('parseSingleSnapshot snapshotObjectField '+JSON.serializePretty(snapshotObjectFieldList));
                        JsonData data = new JsonData();
                        for(SnapshotObjectField snap: snapshotObjectFieldList) {
                            if(snap.ApiName.equalsIgnoreCase('Name')) 
                                data.label = snap.Value;
                            if(snap.Label.containsIgnoreCase('Z_Unused'))
                                snap.FieldCurrentStatus = 'Z_Unused';
                            if(snap.Label.containsIgnoreCase('Y_Hidden'))
                                snap.FieldCurrentStatus = 'Y_Hidden';
                            if(snap.ApiName.equalsIgnoreCase('Id')) {
                                data.Id = snap.Value;
                                childId = snap.Value;
                                data.Name = snap.Value;
                            }
                            if(snap.ApiName.equalsIgnoreCase(rel_obj_api) && String.isNotBlank(snap.Value) && snap.Value.equalsIgnoreCase(parentId)) 
                                isChild = true;
                        }
                        
                        //this will happen if it is a parent of all
                        if(String.isBlank(rel_obj_api))
                            isChild = true;
                        data.metatext = '';
                        data.expanded = true;
                        snapshotObjectFieldList.sort();
                        data.snapshotJsonList = snapshotObjectFieldList;
                       // system.debug('parseSingleSnapshot child '+isChild);
                        if(isChild) {
                            if(headerJson.items == null)
                                headerJson.items = new List<JsonData>();
                            headerJson.items.add(data);
                            constructJsonForSnapshot(snapshot,subChildList, childId, data, snapshotFiles); 
                        }
                    }
                    if(headerJson.items != null && headerJson.items.size() > 0)
                        snapshotJson.items.add(headerJson);
                }
            }
        }
    }
    
    
    /***
     * Total number of records for each object is caluclated
     */ 
    public static void countCalculation(List<JsonData> items){
        if(items != null && items.size() > 0) {
            for(integer i = 0; i < items.size(); i++) {
                if(items[i].label.contains('#')) {
                    if(items[i].items != null)
                        items[i].label = items[i].label + ' ('+items[i].items.size()+')';
                }
                countCalculation(items[i].items);
            }
        }
    }
    
    /***
     * Gets the snapshot configuration json file for the snapshot record
     */
    public static RelatedObjectJson getSnapshotConfiguration(Id snapshotId) {
        //Getting snapshot configuration file to know the hierarchy 
        if(snapshotId == null)
            return null;
        List<ContentDocumentLink> ContentDocumentLinks =  [SELECT ContentDocumentId, LinkedEntityId, Id
                                                           FROM ContentDocumentLink WHERE  
                                                           ContentDocument.title = 'SnapshotConfiguration'  AND 
                                                           LinkedEntityId = :snapshotId];
        Set<Id> snapshotsWithFiles = new Set<Id>();
        if(ContentDocumentLinks != null && ContentDocumentLinks.size() > 0) {
            for(ContentDocumentLink docLink : ContentDocumentLinks)
                snapshotsWithFiles.add(docLink.ContentDocumentId);
            
            List<ContentVersion> contentList = [select Id, versionData, FirstPublishLocationId from contentVersion
                                                where title = 'SnapshotConfiguration'
                                                AND ContentDocumentId in :snapshotsWithFiles];
            
            //system.debug('getJsonForRelatedObj contentList: '+contentList);
            if(contentList.size() > 0) {
                RelatedObjectJson relatedObjectMap;
                try {
                    relatedObjectMap = (RelatedObjectJson) JSON.deserialize(contentList[0].versionData.toString(),RelatedObjectJson.class );
                    //system.debug('getJsonForRelatedObj relatedObjectMap '+JSON.serializePretty(relatedObjectMap));
                }
                catch(Exception e1) {
                    system.debug('getJsonForRelatedObj Exception: '+e1);
                    String m = 'Invalid SnapshotConfiguration.json ';
                    AuraHandledException e = new AuraHandledException(m);
                    e.setMessage(m);
                    throw e;
                }
                return relatedObjectMap;
            }
        }
        return null;
    }
    
    public class SnapshotWrapper {
        @AuraEnabled public List<SnapshotDetails> SnapshotItems {get; set;}
        @AuraEnabled public Integer pageSize {get;set;}
        @AuraEnabled public Integer pageNumber {get;set;}
        @AuraEnabled public Integer totalRecords {get;set;}
        @AuraEnabled public Integer recordStart {get;set;}
        @AuraEnabled public Integer recordEnd {get;set;}
    }
    
    public class SnapshotDetails {
        @AuraEnabled public String SnapshotId {get; set;}
        @AuraEnabled public String SnapshotName {get; set;}
        @AuraEnabled public Date SnapshotDate {get; set;}
        @AuraEnabled public String SnapshotComment {get; set;}
        @AuraEnabled public List<JsonData> items {get; set;}
    }
    
    public class JsonData {
        @AuraEnabled public String Id  {get; set;}
        @AuraEnabled public String label  {get; set;}
        @AuraEnabled public String name  {get; set;}
        @AuraEnabled public String metatext  {get; set;}
        @AuraEnabled public Boolean expanded  {get; set;}
        @AuraEnabled public List<JsonData> items  {get; set;}
        @AuraEnabled public List<SnapshotObjectField> snapshotJsonList  {get; set;}
    }
    
    public class SnapshotObjectField implements Comparable {
        @AuraEnabled public String Label  {get; set;}
        @AuraEnabled public String ApiName  {get; set;}
        @AuraEnabled public String RelObjectName  {get; set;}
        @AuraEnabled public String RelObjectFieldValue  {get; set;}
        @AuraEnabled public Schema.DisplayType FieldType  {get; set;}
        @AuraEnabled public String Value  {get; set;}
        @AuraEnabled public String FieldCurrentStatus {get; set;} //To indicate z_unused, y_hidden
        
        public Integer compareTo(Object objToCompare) {
            SnapshotObjectField snap = (SnapshotObjectField)(objToCompare);
            if(snap == null)
                return 1;
            if (this.Label > snap.Label) 
                return 1;
            if (this.Label == snap.Label) 
                return 0;
            
            return -1;
        }    
    }
    
    public class RelatedObjectJson {
        @AuraEnabled public String obj_api  {get; set;}
        @AuraEnabled public String related_obj_api  {get; set;}
        @AuraEnabled public List<RelatedObjectJson> child  {get; set;}
    }
    
}