public class AssemblyURTriggerHandler implements ITrigger
{   
    private final string triggerBefore = 'AssemblyUtilTriggerHandlerBefore';
    private final string triggerAfter = 'AssemblyUtilTriggerHandlerAfter';

    private map<id, Assembly_Utilization__c> mapAssyUtil_After;
    private map<id, List<Utilization_Report_List_Item__c>> mapSupplRentUtil;
    
    public AssemblyURTriggerHandler()
    {
        
    }
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.    */
     
    public void bulkBefore()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        system.debug('AssemblyUtilTriggerHandler.bulkBefore(+)');
        
        
        system.debug('AssemblyUtilTriggerHandler.bulkBefore(-)');
    }

    public void bulkAfter()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        system.debug('AssemblyUtilTriggerHandler.bulkAfter(+)');

        if(trigger.isUpdate)
        {   
            
            mapSupplRentUtil = new map<id, List<Utilization_Report_List_Item__c>>();
            for(Utilization_Report_List_Item__c supplRentUtilRec: [select id,Assembly_Utilization__c, Derate__c
                                                        ,Running_Hours_During_Month__c,Cycles_During_Month__c
                                                        ,TSN_At_Month_Start__c,CSN_At_Month_Start__c from Utilization_Report_List_Item__c
                                                        where Assembly_Utilization__c in :trigger.new])
            {
                if(!mapSupplRentUtil.containskey(supplRentUtilRec.Assembly_Utilization__c))
                    mapSupplRentUtil.put(supplRentUtilRec.Assembly_Utilization__c, new List<Utilization_Report_List_Item__c>());
                mapSupplRentUtil.get(supplRentUtilRec.Assembly_Utilization__c).add(supplRentUtilRec);
            }
           
        }        
        
        system.debug('AssemblyUtilTriggerHandler.bulkAfter(-)');
    }

    public void beforeInsert()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);

        system.debug('AssemblyUtilTriggerHandler.beforeInsert(+)');
        
        Assembly_Utilization__c[] lstAssyUtil= (list<Assembly_Utilization__c>)trigger.new ;

        for (Assembly_Utilization__c assyUtil: lstAssyUtil)
        {
            if(trigger.isInsert && !assyUtil.Is_Created_By_Trigger__c){
                assyUtil.addError('Assembly utilization reports are created automatically. They cannot be created or deleted manually. You may set the flying hours and cycles to zero if the assembly is not currently attached to the aircraft.');
                break;      
            }  
        }

        for(Assembly_Utilization__c curAssyURLI:lstAssyUtil){
            if('Actual'.equals(curAssyURLI.Type_F__c)){
                curAssyURLI.Y_Hidden_Running_Hours__c = LeaseWareUtils.zeroIfNull(curAssyURLI.Running_Hours_During_Month__c);
                curAssyURLI.Y_Hidden_Running_Cycles__c = LeaseWareUtils.zeroIfNull(curAssyURLI.Cycles_During_Month__c); 
            }       
        }

        system.debug('AssemblyUtilTriggerHandler.beforeInsert(-)');      
    }

    public void beforeUpdate()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AssemblyUtilTriggerHandler.beforeUpdate(+)');

        Assembly_Utilization__c[] listnew= (list<Assembly_Utilization__c>)trigger.new ;
        Assembly_Utilization__c OldAssyUtilURLI;
        for(Assembly_Utilization__c urnew : listnew){
            OldAssyUtilURLI = (Assembly_Utilization__c )trigger.oldMap.get(urnew.id);
           
            if(!LeaseWareUtils.isFromTrigger('URTriggerHandlerAfter') && OldAssyUtilURLI.Status_F__c.contains('Approved') && !LeaseWareUtils.isFromTrigger(urnew.Utilization_Report__c+'PROVISION_TO_ACTUAL')  && !LeaseWareUtils.isFromTrigger('SKIP_UR_APPROVED'))
            {
                 urnew.addError('You cannot modify an Assembly Utilization if Utilization is already approved.');
            }
        }

        for(Assembly_Utilization__c curAssyURLI:listnew){
            if('Actual'.equals(curAssyURLI.Type_F__c)){
                curAssyURLI.Y_Hidden_Running_Hours__c = LeaseWareUtils.zeroIfNull(curAssyURLI.Running_Hours_During_Month__c);
                curAssyURLI.Y_Hidden_Running_Cycles__c = LeaseWareUtils.zeroIfNull(curAssyURLI.Cycles_During_Month__c); 
            }       
        }
                
        system.debug('AssemblyUtilTriggerHandler.beforeUpdate(-)');      
    }

    public void beforeDelete()
    {
        system.debug('AssemblyUtilTriggerHandler.beforeDelete(+)');
        
        
        system.debug('AssemblyUtilTriggerHandler.beforeDelete(-)');      
    }
    public void afterInsert()
    {
        system.debug('AssemblyUtilTriggerHandler.afterInsert(+)');
        
        
        system.debug('AssemblyUtilTriggerHandler.afterInsert(-)');      
    }

    public void afterUpdate()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AssemblyUtilTriggerHandler.afterUpdate(+)');

        //Get the supplement rent util records associated to the assembly util records
        List<Utilization_Report_List_Item__c> lstURLIToUpdate = new List<Utilization_Report_List_Item__c>();
        Assembly_Utilization__c[] listNew = (list<Assembly_Utilization__c>)(trigger.new) ;
        for(Assembly_Utilization__c refAssyUtil : listNew)
        {   
            if(mapSupplRentUtil.containskey(refAssyUtil.id)){
                //Update the FH, FC, Derate on supplement rent util records            
                for(Utilization_Report_List_Item__c recToUpdate: mapSupplRentUtil.get(refAssyUtil.id))
                { 
                    recToUpdate.Running_Hours_During_Month__c = refAssyUtil.Running_Hours_During_Month__c;
                    recToUpdate.Cycles_During_Month__c = refAssyUtil.Cycles_During_Month__c;
                    recToUpdate.Derate__c = refAssyUtil.Derate__c;
                    lstURLIToUpdate.add(recToUpdate);
                }
            } 
        }

        system.debug('updateURLIs = '+ lstURLIToUpdate.size());
        if (lstURLIToUpdate.size()>0)update lstURLIToUpdate;

        system.debug('AssemblyUtilTriggerHandler.afterUpdate(-)');      
    }

    public void afterDelete()
    {
        system.debug('AssemblyUtilTriggerHandler.afterDelete(+)');
        
        
        system.debug('AssemblyUtilTriggerHandler.afterDelete(-)');      
    }
    public void afterUnDelete()
    {
        system.debug('AssemblyUtilTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('AssemblyUtilTriggerHandler.afterUnDelete(-)');        
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        leasewareUtils.insertExceptionLogs();
    }
    
    
}