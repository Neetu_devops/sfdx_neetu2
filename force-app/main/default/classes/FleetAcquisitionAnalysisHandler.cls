public class FleetAcquisitionAnalysisHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'FleetAcquisitionAnalysisHandlerBefore';
    private final string triggerAfter = 'FleetAcquisitionAnalysisHandlerAfter';
    public FleetAcquisitionAnalysisHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('FleetAcquisitionAnalysisHandler.beforeInsert(+)');

    	list<Fleet_Acquisition_Analysis__c> newList = (list<Fleet_Acquisition_Analysis__c>)trigger.new;
    	for(Fleet_Acquisition_Analysis__c curRec:newList){
    		Integer nTerm = curRec.Term_Months__c==null?0:curRec.Term_Months__c.intValue();
    		Decimal monthlyInterestRate =curRec.Interest_Rate_F__c == null?0:(curRec.Interest_Rate_F__c/FleetAcquisitionAnalysisCalulcation.numberOfFrequency)/100 ;
    		curRec.Monthly_Repayment_Amount__c = LeaseWareUtils.calculatePMT(monthlyInterestRate,nTerm,curRec.Loan_Principle__c,curRec.Balloon_Payment__c) ;
    		curRec.Monthly_Lease_Payment_FL__c = LeaseWareUtils.calculatePMT(monthlyInterestRate,nTerm,curRec.Remaining_Balance__c,curRec.Balloon_Payment__c) ;
    	}

	    system.debug('FleetAcquisitionAnalysisHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('FleetAcquisitionAnalysisHandler.beforeUpdate(+)');

    	list<Fleet_Acquisition_Analysis__c> newList = (list<Fleet_Acquisition_Analysis__c>)trigger.new;
    	for(Fleet_Acquisition_Analysis__c curRec:newList){
    		Integer nTerm = curRec.Term_Months__c==null?0:curRec.Term_Months__c.intValue();
    		Decimal monthlyInterestRate =curRec.Interest_Rate_F__c == null?0:(curRec.Interest_Rate_F__c/FleetAcquisitionAnalysisCalulcation.numberOfFrequency)/100 ;
    		curRec.Monthly_Repayment_Amount__c = LeaseWareUtils.calculatePMT(monthlyInterestRate,nTerm,curRec.Loan_Principle__c,curRec.Balloon_Payment__c) ;
    		curRec.Monthly_Lease_Payment_FL__c = LeaseWareUtils.calculatePMT(monthlyInterestRate,nTerm,curRec.Remaining_Balance__c,curRec.Balloon_Payment__c) ;
    	}    	
    	
    	system.debug('FleetAcquisitionAnalysisHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('FleetAcquisitionAnalysisHandler.beforeDelete(+)');
    	
    	
    	system.debug('FleetAcquisitionAnalysisHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FleetAcquisitionAnalysisHandler.afterInsert(+)');

    	system.debug('FleetAcquisitionAnalysisHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FleetAcquisitionAnalysisHandler.afterUpdate(+)');
    	
    	
    	system.debug('FleetAcquisitionAnalysisHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FleetAcquisitionAnalysisHandler.afterDelete(+)');
    	
    	
    	system.debug('FleetAcquisitionAnalysisHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FleetAcquisitionAnalysisHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('FleetAcquisitionAnalysisHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


}