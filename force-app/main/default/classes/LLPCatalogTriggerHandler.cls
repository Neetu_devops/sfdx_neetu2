public class LLPCatalogTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    public static final string triggerBefore = 'LLPCatalogTriggerHandlerBefore';
    public static final string triggerAfter = 'LLPCatalogTriggerHandlerAfter';
    private static Id DeafultTSId = null ;
    

    
    public LLPCatalogTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
     
     
	private void setSOQLStaticMAPOrList(){

	}     
    public void bulkBefore()
    {
		setSOQLStaticMAPOrList();
    }
     
    public void bulkAfter()
    {
    	
    }
         
    public void beforeInsert()
    {
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('LLPCatalogTriggerHandler.beforeInsert(+)');
		
		//Constituent_Assembly__c[] listNew = (list<Constituent_Assembly__c>)trigger.new ;

		// Catalog year validation - Start
		Map <Id,List<LLP_Catalog__c>> LLPTemplateAndCatalogs = getLLPTempateAndCatalogsMap(trigger.new);
        for(Id LLPTemplateId : LLPTemplateAndCatalogs.keySet()) {
        	validateCatalogYear(LLPTemplateAndCatalogs.get(LLPTemplateId), LLPTemplateId);
        }
        // Catalog year validation - End
		
	    system.debug('LLPCatalogTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('LLPCatalogTriggerHandler.beforeUpdate(+)');

    	//Catalog year validation - Start
		
		// Updates the LLP catalog price year only if the catalog years would be in sequence after modification.
		// Otherwise updation of catalog year will not happen.
		// E.g. Consider 2016, 2017, 2018 - LLP catalog price records.
		// Updating 2017 to 2019 will break the sequence. so error.
		// But 2016 can be updated t0 2019. Catalog years will be in sequence 2017,2018,2019.
		
        Map <Id,List<LLP_Catalog__c>> LLPTemplateAndCatalogs = getLLPTempateAndCatalogsMap(trigger.new);
        for(Id LLPTemplateId : LLPTemplateAndCatalogs.keySet()) {
        	validateCatalogYear(LLPTemplateAndCatalogs.get(LLPTemplateId), LLPTemplateId);
        }
		//Catalog year validation - End
    	system.debug('LLPCatalogTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	system.debug('LLPCatalogTriggerHandler.beforeDelete(+)');
        
 	    // Catalog year validation - Start
 		// Delete must always be allowed on the latest catalog records.
		Map <Id,List<LLP_Catalog__c>> LLPTemplateAndCatalogs = getLLPTempateAndCatalogsMap(trigger.old);
        for(Id LLPTemplateId : LLPTemplateAndCatalogs.keySet()) {
        	validateCatalogYear(LLPTemplateAndCatalogs.get(LLPTemplateId), LLPTemplateId);
        }
        // Catalog year validation - End
        
    	system.debug('LLPCatalogTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LLPCatalogTriggerHandler.afterInsert(+)');
		updateToParent();
    	updateLLPSummary();
    	system.debug('LLPCatalogTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LLPCatalogTriggerHandler.afterUpdate(+)');
		updateToParent();
    	updateLLPSummary();
    	system.debug('LLPCatalogTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	system.debug('LLPCatalogTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('LLPCatalogTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	system.debug('LLPCatalogTriggerHandler.afterUnDelete(+)');
    	
    	system.assert(false); 
    	
    	system.debug('LLPCatalogTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


	//after insert , after update
	private void updateLLPSummary(){
		LLP_Catalog__c[] listNew = (list<LLP_Catalog__c>)trigger.new ;
		set<id> setENG_DBIDs = new set<id>();
		for(LLP_Catalog__c curRec:listNew){
			setENG_DBIDs.add(curRec.Y_Hidden_Engine_DB_ID__c);
		}
		
		LLP_Catalog_Summary__c[] listLLPSum = [select Engine_Template__c,id,name,status__c from LLP_Catalog_Summary__c where Engine_Template__c = :setENG_DBIDs];
		map<string,LLP_Catalog_Summary__c> mapLLPCatSummary = new map<string,LLP_Catalog_Summary__c>();
		
		
		for(LLP_Catalog_Summary__c currec:listLLPSum){
			mapLLPCatSummary.put(currec.Engine_Template__c + currec.name,currec);
		}

		map<string,LLP_Catalog_Summary__c> mapUpsertLLPCatSummary = new map<string,LLP_Catalog_Summary__c>();

		string strKey;
		for(LLP_Catalog__c curRec:listNew){
			strKey = curRec.Y_Hidden_Engine_DB_ID__c + curRec.name;
			if(mapLLPCatSummary.containsKey(strKey) && !mapUpsertLLPCatSummary.containsKey(strKey)){
				//means  update on Old catalog
				mapUpsertLLPCatSummary.put(strKey,mapLLPCatSummary.get(strKey));
			}else if(!mapUpsertLLPCatSummary.containsKey(strKey)){
				//means New catalog
				mapUpsertLLPCatSummary.put(strKey,new LLP_Catalog_Summary__c(name=curRec.name,Engine_Template__c =curRec.Y_Hidden_Engine_DB_ID__c ));
			}
		}
		
		if(mapUpsertLLPCatSummary.size()>0) upsert mapUpsertLLPCatSummary.values(); // rest of the calcuation will happen in trigger code

		// Update prev LLP catalog record
		Engine_Template__c[] listEngineLLPSum = [select id
								,(select Engine_Template__c,id,name,status__c from LLP_Catalog_Summary__r order by name )//asc order
								from Engine_Template__c
								where id = :setENG_DBIDs];
		Id prevId;	
		listLLPSum = new list<LLP_Catalog_Summary__c>();					
		for(Engine_Template__c curRec:listEngineLLPSum){
			prevId =null;						
			for(LLP_Catalog_Summary__c curChildrec:curRec.LLP_Catalog_Summary__r){
				curChildrec.Prev_LLP_Cat_Sum__c = prevId;
				prevId = curChildrec.Id;
				listLLPSum.add(curChildrec);
			}
		}
		if(listLLPSum.size()>0){
			LeaseWareUtils.TriggerDisabledFlag=true;
		 	upsert listLLPSum; 
		 	LeaseWareUtils.TriggerDisabledFlag=false;
		}
		
	}


	//after insert , after update
	private void updateToParent(){
		LLP_Catalog__c[] listNew = (list<LLP_Catalog__c>)trigger.new ;

		set<Id> LLPTemplateIds = new set<Id>();
		LLP_Catalog__c oldRec;
		for(LLP_Catalog__c curRec:listNew){
			if(curRec.Current_Catalog_Year__c){
				if(trigger.IsInsert){
					LLPTemplateIds.add(curRec.LLP_Template__c);
				}else if(trigger.IsUpdate){
					oldRec = (LLP_Catalog__c)trigger.oldMap.get(curRec.id);
					if(oldRec.Catalog_Price__c!=curRec.Catalog_Price__c){
						LLPTemplateIds.add(curRec.LLP_Template__c);
					}
				}
			}
		}		
		if(LLPTemplateIds.size()>0){
			map<id,LLP_Template__c> mapLLPtempate = new map<id,LLP_Template__c>([select id 
										from LLP_Template__c
										where id in :LLPTemplateIds]);
			
			for(LLP_Catalog__c curRec:listNew){
				if(curRec.Current_Catalog_Year__c){
					if(trigger.IsInsert){
						mapLLPtempate.get(curRec.LLP_Template__c).Replacement_Cost__c   = curRec.Catalog_Price__c;
					}else if(trigger.IsUpdate){
						oldRec = (LLP_Catalog__c)trigger.oldMap.get(curRec.id);
						if(oldRec.Catalog_Price__c!=curRec.Catalog_Price__c){
							mapLLPtempate.get(curRec.LLP_Template__c).Replacement_Cost__c   = curRec.Catalog_Price__c;
						}
					}
				}
			}			
			
			//there is no impact in trigger in change of catalog price. hence disabling trigger
			//LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled for all users at apex level
            //Disabling above TriggerDisabledFlag reason is to once LLP Template record get updated Replacement Cost value want to update Maintenance Event Task record accrodingly

            if(mapLLPtempate.size()>0) update mapLLPtempate.values();
			
            //LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger enabled for all users at apex level	
										
		}

		
	}
    
	// Catalog year validation - Start.
    // Checks if the year sequence is followed when the catalog records are inserted/deleted/updated.
    // Catalog record years are expected to be in sequence and there cannot be gap between years.
    private void validateCatalogYear(List<LLP_Catalog__c> LLPCatalogs, Id LLPTemplateId) {
        
		   // Builds map containing partno and their respective catalog objects that are to be inserted/deleted/updated.
           Map<String, List<LLP_Catalog__c>> mapLLPPartNoAndCatalogs = new Map<String, List<LLP_Catalog__c>>();
           mapLLPPartNoAndCatalogs = getLLPPartNoAndCatalogsMap(LLPCatalogs);
           
		   // Builds map containing partno and their respective catalog objects that are already existing in backend only for the part numbers 
		   // for which records are to be inserted/deleted/updated.
			Set <String> partNoSet = mapLLPPartNoAndCatalogs.keySet();
        
           List <LLP_Catalog__c> LLPCatalogExistingList = [select id,Part_Number_New__c,Name from LLP_Catalog__c where LLP_Template__c=:LLPTemplateId and Part_Number_New__c in :partNoSet order by Name desc];
		   Map <String, List <LLP_Catalog__c>> mapPartNoLLPCatalogsExisting = new map<String , List <LLP_Catalog__c>>();
		   mapPartNoLLPCatalogsExisting = getLLPPartNoAndCatalogsMap(LLPCatalogExistingList);
		   
		
		   //Get the latest and least catalog year for each LLP part number in the existing records.
           Map <String, LLP_Catalog__c> mapPartNoLatestYear = new Map<String , LLP_Catalog__c>();
           Map <String, LLP_Catalog__c> mapPartNoLeastYear = new Map<String , LLP_Catalog__c>();
		   List <LLP_Catalog__c> LLPCatalogForAPartNoList;
           for(String partNo : partNoSet) {
				if(mapPartNoLLPCatalogsExisting.containsKey(partNo)){
					LLPCatalogForAPartNoList = mapPartNoLLPCatalogsExisting.get(partNo);
					if(LLPCatalogForAPartNoList.size() > 0) {
					    mapPartNoLatestYear.put(partNo,LLPCatalogForAPartNoList.get(0));
						mapPartNoLeastYear.put(partNo,LLPCatalogForAPartNoList.get(LLPCatalogForAPartNoList.size() - 1));
					}
				}
		   }
        
           List <LLP_Catalog__c> sortedLLPCatalogList = new List<LLP_Catalog__c>();
           List <LLP_Catalog__c> LLPCatalogToSort = new List<LLP_Catalog__c>();
           LLP_Catalog__c LLPCatalogItemLatest;
           LLP_Catalog__c LLPCatalogItemLeast;
           String catalogYearLatest, catalogYearLeast;
           Integer expectedCatalogYearLatest=0, expectedCatalogYearLeast=0;    
           
           for(String partNo : partNoSet) {
                
               LLPCatalogToSort = mapLLPPartNoAndCatalogs.get(partNo);
               LLPCatalogItemLatest = mapPartNoLatestYear.get(partNo);
               LLPCatalogItemLeast = mapPartNoLeastYear.get(partNo);
               
               //Sort only if multiple catalog price records are inserted/updated/deleted from bulk upload.
               if(LLPCatalogToSort!=null && LLPCatalogToSort.size()>1) {
                    sortedLLPCatalogList = sortLLPCatalogList(LLPCatalogToSort);
               }
               else {
                    sortedLLPCatalogList = LLPCatalogToSort;
               }
               
               if(trigger.isInsert) {
                   // Retrieves the latest and least catalog year.
                   // Calculates the expected catalog year.
                   // For single record, if the inserted record is not same as the expected year, insertion fails with error.
                   // For multiple records, insertion succeeds till the records in sequence and fails for the one with gap.
                   if(LLPCatalogItemLatest != null) {
                       catalogYearLatest = LLPCatalogItemLatest.Name;
                       expectedCatalogYearLatest =  Integer.valueOf(catalogYearLatest)+1;
                   }
                   
                   if(LLPCatalogItemLeast != null){
                       catalogYearLeast = LLPCatalogItemLeast.Name;
                       expectedCatalogYearLeast =  Integer.valueOf(catalogYearLeast)-1;
                   }
                   
                   for(LLP_Catalog__c LLPCatalogObj : sortedLLPCatalogList) {
                     if(expectedCatalogYearLatest == 0 && expectedCatalogYearLeast == 0){
                         catalogYearLatest = LLPCatalogObj.Name;
                         expectedCatalogYearLatest = Integer.valueOf(catalogYearLatest) + 1;
                         catalogYearLeast = LLPCatalogObj.Name;
                         expectedCatalogYearLeast = Integer.valueOf(catalogYearLeast) - 1;
                     }
                     else {
                           if(Integer.valueOf(LLPCatalogObj.Name) != expectedCatalogYearLatest &&
                              Integer.valueOf(LLPCatalogObj.Name) != expectedCatalogYearLeast) { 
                                  LLPCatalogObj.addError('Latest catalog year is '+ catalogYearLatest +' and the oldest catalog year is '+ catalogYearLeast + '.Please add catalog record for ' + expectedCatalogYearLatest + ' or '+expectedCatalogYearLeast+'.');
                                  leasewareUtils.createExceptionLog(null, 'Latest catalog year is '+ catalogYearLatest +' and the oldest catalog year is '+ catalogYearLeast + '.Please add catalog record for ' + expectedCatalogYearLatest + ' or '+expectedCatalogYearLeast+'.' , 'Catalog Year', LLPCatalogObj.Id , 'LLP Catalog Year', false);
                              }
                           else {
                               if(Integer.valueOf(LLPCatalogObj.Name) == expectedCatalogYearLatest) {
                                   catalogYearLatest = LLPCatalogObj.Name;
                                   expectedCatalogYearLatest = Integer.valueOf(catalogYearLatest) + 1;
                               }
                               if(Integer.valueOf(LLPCatalogObj.Name) == expectedCatalogYearLeast) {
                                   catalogYearLeast = LLPCatalogObj.Name;
                                   expectedCatalogYearLeast = Integer.valueOf(catalogYearLeast) - 1;
                               }
                           }
                       }
                   }
               }//End If - trigger insert.
               if(Trigger.isDelete){
                   Integer expectedYearToDel = Integer.valueOf(LLPCatalogItemLatest.Name);
                   for(Integer index=sortedLLPCatalogList.size()-1; index>=0 ; index--){
                       if(Integer.valueOf(sortedLLPCatalogList.get(index).Name) != expectedYearToDel){
                           sortedLLPCatalogList.get(index).addError('Latest catalog year is '+ expectedYearToDel +'.Cannot delete catalog record for ' + Integer.valueOf(sortedLLPCatalogList.get(index).Name) + '.');
                           leasewareUtils.createExceptionLog(null, 'Latest catalog year is '+ expectedYearToDel +'.Cannot delete catalog record for ' + Integer.valueOf(sortedLLPCatalogList.get(index).Name)+'.' , 'Catalog Year', sortedLLPCatalogList.get(index).Id , 'LLP Catalog Year', false);
                       } else {
                           expectedYearToDel = Integer.valueOf(sortedLLPCatalogList.get(index).Name)-1;
                       }
                   }
               }//End If - trigger delete.
               if(trigger.isUpdate){
                   Map <Id,Integer> modifiedLLPCatalogObjMap = new Map<Id,Integer> ();
				   Map <Id,Integer> existingLLPCatalogObjMap = new Map<Id,Integer>();
			
                   // Map containing the LLP catalog object on which the update has to happen and the new catalog year to update.
                   for(LLP_Catalog__c LLPCatalogObj : sortedLLPCatalogList) {
                           modifiedLLPCatalogObjMap.put(LLPCatalogObj.id,Integer.valueOf(LLPCatalogObj.Name));
                   }
                    
                   // Map containing the LLP catalog object on which the update has to happen and the current catalog year to update.
                   List <LLP_Catalog__c> existingLLPCatalogs = sortLLPCatalogList(mapPartNoLLPCatalogsExisting.get(partNo));
                   for(LLP_Catalog__c LLPCatalogObj : existingLLPCatalogs){
                        existingLLPCatalogObjMap.put(LLPCatalogObj.id,Integer.valueOf(LLPCatalogObj.Name));
                   }
                    
				   List <Integer> yearSequence = new List<Integer>();
                   for (Id LLPCatalogObjId : modifiedLLPCatalogObjMap.keySet()) {
                        existingLLPCatalogObjMap.put(LLPCatalogObjId,Integer.valueOf(modifiedLLPCatalogObjMap.get(LLPCatalogObjId)));
                   }
                    
                   yearSequence = existingLLPCatalogObjMap.values();
                    
                   if(!checkOrderWithNoGap(yearSequence)){
                       for (LLP_Catalog__c LLPCatalogModified : sortedLLPCatalogList) {
                           LLPCatalogModified.addError('Cannot update catalog record to ' 
                                                       + LLPCatalogModified.Name + '.Catalog years are not in sequence.');
                           leasewareUtils.createExceptionLog(null, 'Cannot update catalog record to ' 
                                                             + LLPCatalogModified.Name 
                                                             + '.Catalog years are not in sequence.', 'Catalog Year', 
                                                             LLPCatalogModified.Id , 'LLP Catalog Year', false);
                       }    
                   }
               }//End If - trigger update.
           }
    }
    
    // Sorts the LLP catalog price records acoording to the catalog year in ascending order.
    private List<LLP_Catalog__c> sortLLPCatalogList(List<LLP_Catalog__c> unSortedLLPCatalogList) {
        List<LLP_Catalog__c> sortedLLPCatalogList = new List<LLP_Catalog__c>();
        Map<Integer,LLP_Catalog__c> YearAndLLPCatalogObj = new Map<Integer,LLP_Catalog__c>();
        for(LLP_Catalog__c LLPCatalogObj : unSortedLLPCatalogList) {
            YearAndLLPCatalogObj.put(Integer.valueOf(LLPCatalogObj.Name),LLPCatalogObj);
        }
        List<Integer> sortedYearsList = new List<Integer>();
        sortedYearsList.addAll(YearAndLLPCatalogObj.keyset());
        sortedYearsList.sort();
        for(Integer catalogYear : sortedYearsList){
            sortedLLPCatalogList.add(YearAndLLPCatalogObj.get(catalogYear));
        }
        System.debug('sortedLLPCatalogList ' + sortedLLPCatalogList);
		return sortedLLPCatalogList;
    }
    
    // Check the sequence of the LLP catalog price records based on year.
    // returns true - if the years are in sequence, false - otherwise.
     private boolean checkOrderWithNoGap(List<Integer> yearList) {  
        
        Boolean result = false;
        yearList.sort();
        Integer currIndex = 0;
        Integer currYear = yearList.get(currIndex);
        Integer i = 0;
        for(i = currIndex+1; i<yearList.size(); i++) {
            if(yearList.get(i) != currYear+1) {
                break;
            }
            currYear = yearList.get(i);
        }
         if(i == yearList.size()){
             result = true;
         }
        return result;
    }
    
    // If the LLP catalogs are added using dalaloader, there can be multiple catalogs for different LLP Templates.
    // This method returns a map containing LLP template id and their corresponding list of catalog objects.
    public Map<ID, List<LLP_Catalog__c>> getLLPTempateAndCatalogsMap(List<LLP_Catalog__c> CatalogsAddedUpdated) {
        Map <ID, List<LLP_Catalog__c>> LLPTemplateAndCatalogObjects = new Map<ID, List<LLP_Catalog__c>>();
        List<LLP_Catalog__c> LLPCatalogObjList = CatalogsAddedUpdated;
        List<LLP_Catalog__c> LLPCatalogTempList;
        for(LLP_Catalog__c llpCatalog : LLPCatalogObjList) {
            if(LLPTemplateAndCatalogObjects.containsKey(llpCatalog.LLP_Template__c)){
               LLPTemplateAndCatalogObjects.get(llpCatalog.LLP_Template__c).add(llpCatalog);
            }
            else {
                LLPCatalogTempList = new List<LLP_Catalog__c>();
                LLPCatalogTempList.add(llpCatalog);
                LLPTemplateAndCatalogObjects.put(llpCatalog.LLP_Template__c,LLPCatalogTempList);
            }
        }
        return LLPTemplateAndCatalogObjects;
    }
    
    public Map <String, List <LLP_Catalog__c>> getLLPPartNoAndCatalogsMap(List<LLP_Catalog__c> LLPCatalogsList) {
        Map <String, List <LLP_Catalog__c>> mapLLPPartNoAndCatalogs = new Map <String, List <LLP_Catalog__c>>();
        List<LLP_Catalog__c> LLPCatalogTempList;
        
        for(LLP_Catalog__c LLPObj : LLPCatalogsList) {
				if(mapLLPPartNoAndCatalogs.containsKey(LLPObj.Part_Number_New__c)) {
					 mapLLPPartNoAndCatalogs.get(LLPObj.Part_Number_New__c).add(LLPObj);
				} 
				else {
					LLPCatalogTempList = new List<LLP_Catalog__c>();
					LLPCatalogTempList.add(LLPObj);
					mapLLPPartNoAndCatalogs.put(LLPObj.Part_Number_New__c,LLPCatalogTempList);
				}
	       }
        return mapLLPPartNoAndCatalogs;
    }
    // Catalog year validation - End
}