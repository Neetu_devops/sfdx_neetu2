public class AssemblyMRAvailabilityController{
	

     public Assembly_Eligible_Event__c ACEeventUDDate { get; set; }
     public string selectedDateType      {get;set;}
     public List<SelectOption> dateTypes {get;set;} 


     
     private final Assembly_Eligible_Event__c thisPBL;
     
     public string selectedPaymentDate {get; set;}
     public List<SelectOption> PaymentDateoptions {get; set;}
     public list<Reserve_Snapshot__c>   mrlist {get;set;}



     public AssemblyMRAvailabilityController(ApexPages.StandardController controller){   
     	
    	system.debug('Start');
        this.thisPBL = (Assembly_Eligible_Event__c)controller.getRecord();

   		Assembly_Eligible_Event__c ACEeventRec =  [select id, Lease_F__c, Start_Date__c, End_Date__c,Assembly_Type__c
   			,Constituent_Assembly__c,Event_Type__c,RecordType.DeveloperName
   			,(select Payment_Date__c from Constt_Assembly_Eligible_Event_Invoice__r where Payment_Date__c!=null order by Payment_Date__c desc) 
   					from Assembly_Eligible_Event__c where id = :thisPBL.Id];
     	


    	mrlist = [select id,Snapshot_Date__c
    				,Available_Reserve__c,Payment_Date__c
	    		from Reserve_Snapshot__c 
	    		where Lease__c = :ACEeventRec.Lease_F__c
	    		and Assembly__c	= :ACEeventRec.Constituent_Assembly__c
	    		AND ((Assembly__r.Type__c = 'Airframe'  AND Event_Type__c = :ACEeventRec.Event_Type__c) OR  Assembly__r.Type__c != 'Airframe'  )
	    		order by Snapshot_Date__c ,CreatedDate ];
    	
    	system.debug('Total MR snaps records '+ mrlist.size());  //size of list
    	
    	

    	
    	// Initialize Induction Date
    	inductionDate = ACEeventRec.Start_Date__c;
    	MRList_IND = new list<Reserve_Snapshot__c>{new Reserve_Snapshot__c()};
    	
    	//Initialize Release date from Aircraft_Eligible_Event__c Object
    	releaseDate=ACEeventRec.End_Date__c;
    	MRList_REL=new list<Reserve_Snapshot__c>{new Reserve_Snapshot__c()};
    	
    	// Initialize User defined Date
    	ACEeventUDDate = new Assembly_Eligible_Event__c(name='xxx',start_Date__c=system.today());
    	MRList_UD=new list<Reserve_Snapshot__c>{new Reserve_Snapshot__c()};
    	
    	//Initialize Payment Date
    	MRList_PMT=new list<Reserve_Snapshot__c>{new Reserve_Snapshot__c()};
    	    	


        //Date Type Picklist
        dateTypes = new List<SelectOption>();
        selectedDateType = 'Induction Date';
        dateTypes.add( new SelectOption( 'Induction Date','Induction Date' ) );
        dateTypes.add( new SelectOption( 'Release Date','Release Date' ) );
        dateTypes.add( new SelectOption( 'User Defined Date','User Defined Date' ) );
        dateTypes.add( new SelectOption( 'Payment Date','Payment Date' ) );
        dateTypes.add( new SelectOption( 'All','All' ) );
        
        //Payment Date picklist
		PaymentDatePicklistSet(ACEeventRec);
		
    	//Get MR balance for each event
    	getMRValueatDate();		
		
    	system.debug('MRAvailability Controller constructor');                 
    }
    
    //1. Induction Date
    public Date inductionDate{get;set;}
    public list<Reserve_Snapshot__c>   MRList_IND {get;set;}
    	//public Date  NMR_Invoiced_Total_IND {get;set;}
    
    //2. Release Date 
    public Date releaseDate{get;set;}
    public list<Reserve_Snapshot__c>   MRList_REL {get;set;}
    	//public Date  NMR_Released_Total_IND {get;set;}
     
    //3. Payment Date 
    //public Date releaseDate{get;set;}
    public Date paymentDate {get; set;}
    public list<Reserve_Snapshot__c>   MRList_PMT {get;set;}     
    
    
    //4. User Defined Date
    public list<Reserve_Snapshot__c>   MRList_UD {get;set;}
     
	public void getMRValueatDate(){
		system.debug('getMRValueatDate+');
		system.debug('selectedPaymentDate='+selectedPaymentDate);
		Date PMTDate ;
		if(selectedPaymentDate!=null) PMTDate = date.parse(selectedPaymentDate);
		system.debug('PMTDate='+PMTDate);
		
    	for(Reserve_Snapshot__c curRec:mrlist){
    		//1. Induction
    		if(inductionDate!=null){
    			system.debug('inductionDate='+inductionDate);
    			if(inductionDate >= curRec.Snapshot_Date__c){
					copyMRBal(MRList_IND,curRec);
    			}
    		}
    		//2. Release
    		if(releaseDate!=null){
    			system.debug('releaseDate='+releaseDate);
    			if(releaseDate >= curRec.Snapshot_Date__c){
    				 copyMRBal(MRList_REL,curRec);
    			}
    		}
    		//3. Payment
    		if(PMTDate!=null){
    			if(PMTDate >= curRec.Snapshot_Date__c){
    				copyMRBal(MRList_PMT,curRec);
    			}
    		}    		
    		
    		
    		//4. User defined Date
    		if(ACEeventUDDate.Start_Date__c!=null){
    			system.debug('ACEeventUDDate.Start_Date__c='+ACEeventUDDate.Start_Date__c);
    			if(ACEeventUDDate.Start_Date__c >= curRec.Snapshot_Date__c){
    				copyMRBal(MRList_UD,curRec);
    			}
    		}    		
    	}		
		system.debug('getMRValueatDate-');
	}		
    
    
    // copy value
    private void copyMRBal(list<Reserve_Snapshot__c> listMRS , Reserve_Snapshot__c curRec){
		system.debug('curRecSnapshot_Date__c='+curRec.Snapshot_Date__c);

    					listMRS[0].Y_H_MRCol__c=curRec.Available_Reserve__c ;
    					//listMRS[0].Y_H_MRInv__c=curRec.MR_Invoiced__c ;
    					
    					// TODO : LLP need to check with Bobby
    					//listMRS[0].Y_H_NMRCol_Heavy_Maint_2_Airframe__c=curRec.NMR_Collected_Engine_1_LLP__c;
    					//listMRS[0].Y_H_NMRInv_Heavy_Maint_2_Airframe__c=curRec.NMR_Invoiced_Engine_1_LLP__c;
    					
 
    }
    
    
    
    // for user defined date
    public void getMRBALatDate()
    {
    	system.debug('call method called');
    	system.debug('ACEeventUDDate.Start_Date__c='+ACEeventUDDate.Start_Date__c);
    	MRList_UD[0].Y_H_MRCol__c=null ;
    	MRList_UD[0].Y_H_MRInv__c=null ;  
    	for(Reserve_Snapshot__c curRec:mrlist){
   			//4. 
    		if(ACEeventUDDate.Start_Date__c!=null){
    			if(ACEeventUDDate.Start_Date__c >= curRec.Snapshot_Date__c){
    				copyMRBal(MRList_UD,curRec);
    			}
    		}
    	}		    	
    }
    
    //for payment date
    public void getMRBALatPMTDate()
    {
    	system.debug('call method called Payment Date');
    	system.debug('selectedPaymentDate='+selectedPaymentDate);
    	MRList_UD[0].Y_H_MRCol__c=null ;
    	MRList_UD[0].Y_H_MRInv__c=null ;   
    	Date PMTDate = date.parse(selectedPaymentDate);
    	system.debug('PMTDate='+PMTDate);
    	for(Reserve_Snapshot__c curRec:mrlist){
   			//3. 
    		if(PMTDate!=null){
    			if(PMTDate >= curRec.Snapshot_Date__c){
    				copyMRBal(MRList_PMT,curRec);
    			}
    		}
    	}		    	
    }

   private void PaymentDatePicklistSet(Assembly_Eligible_Event__c ACEventRec){
        //dynamic piclist for "payment date" type
        PaymentDateoptions = new List<SelectOption>();
		
		system.debug('Invoice size------------' + ACEventRec.Constt_Assembly_Eligible_Event_Invoice__r.size());
			  	
		set<Date>	checkDupSet= new set<Date>();	  			 
		for (Constt_Assembly_Eligible_Event_Invoice__c  curRec   :ACEventRec.Constt_Assembly_Eligible_Event_Invoice__r)
		{
			if(!checkDupSet.contains(curRec.Payment_Date__c)){
				// Label , API code
				PaymentDateoptions.add(new SelectOption(curRec.Payment_Date__c.format(), curRec.Payment_Date__c.format()));
				checkDupSet.add(curRec.Payment_Date__c);
			}
			if(selectedPaymentDate==null) selectedPaymentDate = curRec.Payment_Date__c.format();
			
		}   	
   } 

   
}