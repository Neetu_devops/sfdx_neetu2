public class SnapshotCreatorBatchController {
    
    //should be always list not set. Need order of insertion.
    public static List<BatchCreateSnapshot.ParentChild> parentChildList;
    
    public static void parseConfigJson(Id recordId, String comment) {
      //  Id recordId = 'a0V6g000001fDtGEAU';
       // String comment= 'Testing batch one Assembiles ';
        //String conf = '{"obj_api":"Aircraft__c","related_obj_api":"","child":[{"obj_api":"Constituent_Assembly__c","related_obj_api":"Asset__c","child":[]},{"obj_api":"Constituent_Assembly__c","related_obj_api":"Attached_Aircraft__c","child":[{"obj_api":"Sub_Components_LLPs__c","related_obj_api":"Constituent_Assembly__c","child":[{"obj_api":"Thrust_Setting__c","related_obj_api":"Life_Limited_Part__c","child":[]}]},{"obj_api":"Assembly_Eligible_Event__c","related_obj_api":"Constituent_Assembly__c","child":[{"obj_api":"Constt_Assembly_Eligible_Event_Invoice__c","related_obj_api":"Constt_Assembly_Eligible_Event__c","child":[]}]},{"obj_api":"Assembly_Event_Info__c","related_obj_api":"Constituent_Assembly__c","child":[]},{"obj_api":"Asset_History__c","related_obj_api":"Assembly__c","child":[]},{"obj_api":"Utilization_Report_List_Item__c","related_obj_api":"Constituent_Assembly__c","child":[]}]},{"obj_api":"Utilization_Snapshot__c","related_obj_api":"Asset__c","child":[{"obj_api":"Assembly_Utilization_Snapshot_New__c","related_obj_api":"Utilization_Snapshot__c","child":[]}]},{"obj_api":"Utilization_Report__c","related_obj_api":"Aircraft__c","child":[{"obj_api":"Invoice__c","related_obj_api":"Utilization_Report__c","child":[{"obj_api":"Invoice_Line_Item__c","related_obj_api":"Invoice__c","child":[{"obj_api":"Payment_Line_Item__c","related_obj_api":"Invoice_Line_Item__c","child":[]}]}]},{"obj_api":"Assembly_Utilization__c","related_obj_api":"Utilization_Report__c","child":[]}]},{"obj_api":"Certificates__c","related_obj_api":"Asset_Name__c","child":[]},{"obj_api":"Commitment__c","related_obj_api":"Associated_Asset__c","child":[]},{"obj_api":"Equipment__c","related_obj_api":"Aircraft__c","child":[]},{"obj_api":"Asset_History__c","related_obj_api":"Aircraft__c","child":[]},{"obj_api":"Registration_History__c","related_obj_api":"Aircraft__c","child":[]},{"obj_api":"Scenario_Input__c","related_obj_api":"Asset__c","child":[]}]}';
       // String conf = '{"obj_api":"Constituent_Assembly__c","related_obj_api":"Attached_Aircraft__c","child":[{"obj_api":"Sub_Components_LLPs__c","related_obj_api":"Constituent_Assembly__c","child":[{"obj_api":"Thrust_Setting__c","related_obj_api":"Life_Limited_Part__c","child":[]}]}]}';
        
        parentChildList = new List<BatchCreateSnapshot.ParentChild>();
        
        Snapshot_Configuration__c conf = getRelatedObjectFromSnapshotConf(recordId);
        if(conf != null && conf.Related_Object__c != null) { 
            RelatedObjectJson relatedObjectMap = (RelatedObjectJson) JSON.deserialize(conf.Related_Object__c , RelatedObjectJson.class );
            
            String obj_api = relatedObjectMap.obj_api;
            String rel_obj_api = relatedObjectMap.related_obj_api;
            List<RelatedObjectJson> childList = relatedObjectMap.child;
            
            List<RelatedObjectJson> parentObjList = new List<RelatedObjectJson>();
            parentObjList.add(relatedObjectMap);
            
            constructParentChild(parentObjList, obj_api);
            //system.debug('parseConfigJson parentObjList: ' + JSON.serializePretty(parentChildList));
            
            String sObjName = recordId.getSObjectType().getDescribe().getName();
            String sObjLabel = recordId.getSObjectType().getDescribe().getLabel();
            
            DateTime dt = DateTime.now();
            String dateTimeStr = dt.format('yyyy-MMM-dd');
            //insert snapshot 
            Snapshot__c snapshot = new Snapshot__c();
            snapshot.Name = sObjLabel + ' ' +dateTimeStr;
            snapshot.Type__c = sObjLabel;
            snapshot.Y_Hidden_Type_API__c = sObjName;
            snapshot.Comment__c = comment;
            insert snapshot;
            String title = sObjName + '-' + recordId; 
            
            Map<String, List<Id>> parentChildIdMap = new Map<String, List<Id>>();
            List<Id> ids = new List<Id>();
            ids.add(recordId);
            parentChildIdMap.put(sObjName, ids);
            
            //executeBatch by using parenChildList 
            if(parentChildList != null && parentChildList.size() > 0) {
                getJsonForRelatedObj(recordId, snapshot.Id);
                Database.executeBatch(new BatchCreateSnapshot(snapshot, parentChildList, parentChildIdMap, null,null), 30);
            }
        }
        
    }
    
    public static void constructParentChild (List<RelatedObjectJson> relatedObjectMapList, String parentApi) {
        if(relatedObjectMapList != null) {
            for(RelatedObjectJson relatedObjectMap: relatedObjectMapList) {
                String childApi = relatedObjectMap.obj_api;
                String rel_obj_api = relatedObjectMap.related_obj_api;
                List<RelatedObjectJson> subChildList = relatedObjectMap.child;
                if(childApi == null) 
                    return;
                
                BatchCreateSnapshot.ParentChild parentChildObj = new BatchCreateSnapshot.ParentChild();
                parentChildObj.parent = parentApi;
                parentChildObj.child = childApi;
                parentChildObj.related_obj_api = rel_obj_api;
                parentChildList.add(parentChildObj);
                
                if(subChildList != null) {
                    constructParentChild(subChildList, childApi);
                }       
            }   
        }
    }
    
    
    /**
     * Gets relatedJson for the given object from Configuration record
     */ 
    @AuraEnabled
    public static Snapshot_Configuration__c getRelatedObjectFromSnapshotConf(Id recordId) {
        //System.debug('getRelatedObjectFromSnapshotConf recordId '+recordId);       
        if(recordId == null)
            return null;
        String sObjName = recordId.getSObjectType().getDescribe().getName();
        //System.debug('getRelatedObjectFromSnapshotConf sObjName '+sObjName);       
        if(sObjName == null)
            return null;
		List<Snapshot_Configuration__c> snapshotConf = [select Id, Related_Object__c from 
                                                        Snapshot_Configuration__c where 
                                                        Primary_Object__c = :sObjName and 
                                                        Active__c = true order by  
                                                        LastModifiedDate desc limit 1 
                                                       ];
        if(snapshotConf.size() > 0) {
            return snapshotConf[0];
        }
        return null;
    }
    
    /*
     * Create Snapshot configuration in snapshot object 
     */
    public static void getJsonForRelatedObj(Id recordId, Id snapshotId){
        Snapshot_Configuration__c snapshotConf = getRelatedObjectFromSnapshotConf(recordId);
        //System.debug('getJsonForRelatedObj snapshotConf '+snapshotConf);
        if(snapshotConf == null || snapshotConf.Related_Object__c == null) 
            return;
        
        //system.debug('getJsonForRelatedObj snapshotConf: '+snapshotConf);
        if(snapshotConf.Related_Object__c != null) {
            RelatedObjectJson relatedObjectMap;
            try {
                relatedObjectMap = (RelatedObjectJson) JSON.deserialize(snapshotConf.Related_Object__c,RelatedObjectJson.class );
                //system.debug('getJsonForRelatedObj relatedObjectMap '+JSON.serializePretty(relatedObjectMap));
                
                //coping configuration json to snapshot object
                String title = 'SnapshotConfiguration';
                ContentVersion contentversionVarient = new ContentVersion();
                contentversionVarient.Title = title;
                contentversionVarient.contentLocation='S';
                contentversionVarient.PathOnClient = title +'.json';
                contentversionVarient.FirstPublishLocationId = snapshotId;
                contentversionVarient.VersionData = Blob.valueOf(snapshotConf.Related_Object__c);
                insert contentversionVarient;
                
            }
            catch(Exception e1) {
                system.debug('getJsonForRelatedObj Exception: '+e1);
                String m = 'Invalid json ';
                AuraHandledException e = new AuraHandledException(m);
                e.setMessage(m);
                throw e;
            }
        }
    }
    
    
    public class RelatedObjectJson {
        @AuraEnabled public String obj_api  {get; set;}
        @AuraEnabled public String related_obj_api  {get; set;}
        @AuraEnabled public List<RelatedObjectJson> child  {get; set;}
    }
    
}