@isTest
public class TestLLPDiskSheetController {

  
     @TestsetUp
    static void loadTestCases() {
        
        Custom_Lookup__c CL = new Custom_Lookup__c();
        CL.Lookup_Type__c = 'Engine';
        CL.Name = 'C1';
        insert CL;
        
        Custom_Lookup__c CL1 = new Custom_Lookup__c();
        CL1.Lookup_Type__c = 'Engine';
        CL1.Name = '5B';
        insert CL1;
        
        Custom_Lookup__c CL2 = new Custom_Lookup__c();
        CL2.Lookup_Type__c = 'Engine';
        CL2.Name = '5B1';
        insert CL2;

        Engine_Template__c ET = new Engine_Template__c();
        ET.Name = 'CFM56-5B';
        ET.Current_Catalog_Year__c = '2019';
        ET.Manufacturer__c = 'CFMI';
        ET.Model__c = 'CFM56-5B';
        ET.Thrust_LP__c = CL.Id;
        insert ET;
        
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Aircraft',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
        Date_of_Manufacture__c = System.Date.today());
        LeaseWareUtils.clearFromTrigger();
        insert aircraft;
        
        Constituent_Assembly__c assembly1 = new Constituent_Assembly__c();
            assembly1.Name = 'TEst Assembly';
            assembly1.Engine_Model__c = 'CFM56';
            assembly1.TSN__c = 1000;
            assembly1.Attached_Aircraft__c = aircraft.Id;
            assembly1.Current_TS__c =CL.Id;
            assembly1.Serial_Number__c = '575910';
            assembly1.CSN__c = 2000;
            insert assembly1;
        
        Sub_Components_LLPs__c llp = new Sub_Components_LLPs__c();
        llp.Constituent_Assembly__c =assembly1.Id;
        llp.Name='Name';
        llp.TSN__c=1100;
        llp.TSLV__c=1000;
        llp.CSLV__c=1000;
        llp.Life_Limit_Cycles__c = 20000;
        llp.Part_Number__c='11232222';
        llp.Serial_Number__c = '11111';
       insert llp;
        
        Sub_Components_LLPs__c llp1 = new Sub_Components_LLPs__c();
        llp1.Constituent_Assembly__c =assembly1.Id;
        llp1.Name='Name';
        llp1.TSN__c=1100;
        llp1.TSLV__c=1000;
        llp1.CSLV__c=1000;
        llp1.Life_Limit_Cycles__c = 20000;
        llp1.Part_Number__c='11232222';
        llp1.Serial_Number__c = '222222';
       insert llp1;
        
        Sub_Components_LLPs__c llp2 = new Sub_Components_LLPs__c();
        llp2.Constituent_Assembly__c =assembly1.Id;
        llp2.Name='Name';
        llp2.TSN__c=1100;
        llp2.TSLV__c=1000;
        llp2.CSLV__c=1000;
        llp2.Life_Limit_Cycles__c = 20000;
        llp2.Part_Number__c='11232222';
        llp2.Serial_Number__c ='3333333';
       insert llp2;
        
        Sub_Components_LLPs__c llp3 = new Sub_Components_LLPs__c();
        llp3.Constituent_Assembly__c =assembly1.Id;
        llp3.Name='Name';
        llp3.TSN__c=1100;
        llp3.TSLV__c=1000;
        llp3.CSLV__c=1000;
        llp3.Life_Limit_Cycles__c = 20000;
        llp3.Part_Number__c='11232222';
        llp3.Serial_Number__c='444444';
       insert llp3;
        
        Thrust_Setting__c newTS =new Thrust_Setting__c(
	        		Life_Limited_Part__c=llp.Id, 
	        		Name='Thrust1',
	        		Thrust__c =  20000,
	        		Thrust_Setting_Name__c =CL.id,
	        		Cycle_Limit__c= 30000, 
	        		Cycle_Used__c=   2000
	                );		
	       
        insert newTS;
        
        Thrust_Setting__c newTS1 =new Thrust_Setting__c(
	        		Life_Limited_Part__c=llp.Id, 
	        		Name='Thrust2',
	        		Thrust__c =  20000,
	        		Thrust_Setting_Name__c =CL.id,
	        		Cycle_Limit__c= 20000, 
	        		Cycle_Used__c=   2000
	                );		
	       
        insert newTS1;
        
        Thrust_Setting__c newTS2 =new Thrust_Setting__c(
	        		Life_Limited_Part__c=llp.Id, 
	        		Name='Thrust3',
	        		Thrust__c =  20000,
	        		Thrust_Setting_Name__c =CL1.id,
	        		Cycle_Limit__c= 20000, 
	        		Cycle_Used__c=   2000
	                );		
	       
        insert newTS2;
        
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                    Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                    Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
                    LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today().addmonths(-6),
            Lease_End_Date_New__c = system.today().addmonths(6),
            Lessee__c = testOperator.Id,
            Operator__c = testOperator.Id
        );
        insert leaseRecord;
 
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                       
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false, 
            RecordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByName().get('Aircraft').getRecordTypeId(),
            TSN__c = 1.00,                               
            CSN__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;

        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord; 
        
        Date dateField = System.today().addmonths(-6); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Status__c='Approved By Lessor',
            Month_Ending__c = lastDayOfMonth//Date.newInstance(2020,02,29)
        );
        //LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
        
        Utilization_Snapshot__c us = new Utilization_Snapshot__c();
        us.Asset__c = testAircraft.Id;
        us.Utilization__c = utilizationRecord.Id;
        us.Snapshot_Date__c = System.today();
        us.TSN_as_of_Date__c = 10;
        us.CSN_As_Of_Date__c = 10;
        insert us;
        
        Assembly_Utilization_Snapshot_New__c ausn = new Assembly_Utilization_Snapshot_New__c();
        ausn.Name = 'Test';
        ausn.Utilization_Snapshot__c = us.Id;
        ausn.Assembly_Lkp__c = assembly1.Id;
        LWGlobalUtils.setTriggersflagOn(); 
        insert ausn;
        LWGlobalUtils.setTriggersflagOff();
        
        
        LLP_Utilization_Snapshot__c llpUst = new LLP_Utilization_Snapshot__c();
        llpUst.Name = 'Test';
        llpUst.Y_Hidden_Thrust_Usage__c = 'C1,3872,300;';
        llpUst.Thrust_Usage__c = 'C1,3872,300;';
        llpUst.Current_Thrust_Setting__c = 'C1';
        llpUst.Component_Description__c = 'Test';
        llpUst.Serial_Number__c = '1';
        llpUst.Y_Hidden_Part_Number__c = '1';
        llpUst.Y_Hidden_Serial_Number__c = '1';
        llpUst.Assembly_Utilization_Snapshot_New__c = ausn.Id;
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert llpUst;
        LWGlobalUtils.setTriggersflagOff();
         
    }    
    @isTest
    public static void testMethods(){
        Test.startTest();
        Constituent_Assembly__c assembly1 = [Select id from Constituent_Assembly__c LIMIT 1];
        Assembly_Utilization_Snapshot_New__c ausn = [Select id from Assembly_Utilization_Snapshot_New__c LIMIT 1];
        LLP_Utilization_Snapshot__c llpUst =  [Select id from LLP_Utilization_Snapshot__c LIMIT 1];
        Custom_Lookup__c CL = [Select id from Custom_Lookup__c where Name='C1' LIMIT 1];
        Thrust_Setting__c newTS = [Select Id from Thrust_Setting__c where Cycle_Limit__c =: 30000];
        LLPDiskSheetController.getAssemblyDetails(assembly1.Id,null);       
        LLPDiskSheetController.AssemblyWrapper aw =  LLPDiskSheetController.getAssemblyDetails(assembly1.Id,ausn.Id);
        LLPDiskSheetController.getPickListValues('Assembly_Utilization_Snapshot_New__c','Snapshot_Status__c');
        LLPDiskSheetController.getEditCondition(ausn.Id);
        //LLPDiskSheetController.deleteLLPUtilizationSnapshots(ausn.Id);
        LLPDiskSheetController.saveUpdateLLPSnapshotDetails(JSON.serialize(aw.LLPWrapper),ausn.Id,true,newTS.Id);
        LLPDiskSheetController.updateCurrentThrust(JSON.serialize(aw.LLPWrapper), assembly1.Id, CL.Id);
        
        LWGlobalUtils.setTriggersflagOn(); 
        LLPDiskSheetController.deleteLLP(llpUst.Id, true);
        LWGlobalUtils.setTriggersflagOff();
        Test.stopTest();
        //LLPDiskSheetController.deleteLLPUtilizationSnapshots(ausn.Id);
    }
    @isTest
    public static void testReset(){
        Assembly_Utilization_Snapshot_New__c ausn = [Select id from Assembly_Utilization_Snapshot_New__c LIMIT 1];
        LLPDiskSheetController.deleteLLPUtilizationSnapshots(ausn.Id);
    }
}