public without sharing class ILModsController {
	
    @AuraEnabled
    public static List<FieldStructure> getColumns(String objectName, String fieldSetName){
        System.debug('getColumns fieldSetName ' + fieldSetName);
        if(String.isBlank(fieldSetName)) {
            List<FieldStructure> fieldList = new List<FieldStructure>();
            /* 
            FieldStructure field1 = new FieldStructure();
            field1.label = 'Id';
            field1.apiName = 'Id';
            field1.fieldType = 'ID';
            fieldList.add(field1);
            */
            FieldStructure field = new FieldStructure();
            field.label = 'Name';
            field.apiName = 'Name';
            field.fieldType = 'String';
            fieldList.add(field);
            
        	return fieldList;
        }
        else {
            List<FieldStructure> fieldList = getFieldSets(fieldSetName,objectName);
            if(fieldList == null)
                return null;
            return fieldList;
        }
    }
         
    public static List<FieldStructure> getFieldSets(String fieldSetName, String objectName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);
        System.debug('getFieldSets '+targetType + ' fieldSetName: '+fieldSetName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        if(''.equals(getNamespacePrefix())) fieldSetName = fieldSetName.remove('leaseworks__');
        System.debug('getFieldSets fieldSetName: '+fieldSetName);
        Schema.FieldSet fieldSetObj = describe.fieldSets.getMap().get(fieldSetName);
        System.debug('getFieldSets describe:'+describe +' fieldSetObj:'+fieldSetObj);
        if(fieldSetObj == null)
            return null;
        List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields();
        List<FieldStructure> fieldList = new List<FieldStructure>();
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
        {
            FieldStructure field = new FieldStructure();
            field.apiName = fieldSetMemberObj.getFieldPath();
            field.label = fieldSetMemberObj.getLabel();
            field.fieldType = String.valueOf(fieldSetMemberObj.getType()).toLowerCase();
            fieldList.add(field);
            //system.debug('Required ====>' + fieldSetMemberObj.getRequired());
            //system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
            system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
        }
        system.debug('getFieldSetNames fieldList' + fieldList); //api name
        return fieldList;
    }

	@AuraEnabled
    public static string getNamespacePrefix(){
        string NamespacePrefixStr=null;
        NamespacePrefixStr = LeaseWareUtils.getNamespacePrefix();
        return NamespacePrefixStr;
    }
    
    
    public class FieldStructure{
        @AuraEnabled public String apiName;
        @AuraEnabled public String label;
        @AuraEnabled public Object value;
        @AuraEnabled public String fieldType;
    }
    
    
}