public class LLPReplacementController {
    
    public LLPReplacementController(ApexPages.StandardSetController controller){   }
    
    @RemoteAction
    public static String confirmChanges(Id recordId){
        System.debug('confirmChanges parentId '+recordId);
        try{
        	return LWGlobalUtils.UpdateLLPsFromAME(recordId);
        }
        catch(Exception e){
            System.debug('confirmChanges: Exception '+e);
        }
        return null;
    }
        
}