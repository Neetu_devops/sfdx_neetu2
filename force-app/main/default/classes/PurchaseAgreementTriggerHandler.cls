public class PurchaseAgreementTriggerHandler implements ITrigger{

    public PurchaseAgreementTriggerHandler(){}
    
    public void beforeInsert()
    {
    }
    map<Id, Order__c> mapPurchaseAgreement = new map<Id, Order__c>();
    Set<Id> PAIds = new Set<Id>();
    private void setSOQLMAPOrList(){
        list<Order__c> purchaseAgreementList = (list<Order__c>)trigger.new;
        list<Order__c> oldPurchaseAgreementList = (list<Order__c>)trigger.old;
         for(Order__c purchaseAgreement :purchaseAgreementList){
         	PAIds.add(purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c);
            PAIds.add(purchaseAgreement.Id);
      	}
        if(oldPurchaseAgreementList!=null){
        	for(Order__c purchaseAgreement :oldPurchaseAgreementList){
         		PAIds.add(purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c);
        	}   
      	}
        mapPurchaseAgreement= new map<Id, Order__c>([select Id, Name,
                                                    (select Id,Name from Batches__r),
                                                    (select Id,Name,Applicable_AC_Type__c,PA_Milestone_Type__c,Comment__c,
                                                     Description__c,Milestone_Date__c,Months_to_Delivery__c,Y_Hidden_Is_Copied_From_Other_PA__c
                                                     from Purchase_Agreement_Milesones__r)
                                    				from Order__c
                                                    where Id IN :PAIds]);
        system.debug('Purchase Agremment MAp---'+mapPurchaseAgreement);
    }
   
    public void afterInsert()
    {
        setSOQLMAPOrList();
        list<Order__c> purchaseAgreementList = (list<Order__c>)trigger.new;
        List<Purchase_Agreement_Milestone__c> milestoneListToBeAdded = new  List<Purchase_Agreement_Milestone__c>();
        for(Order__c purchaseAgreement : purchaseAgreementList){
            if(purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c !=null){
                //Get the milestones for the new purchase agreement id and overwrite the existing
              
                milestoneListToBeAdded = createOrDeleteMileStones(purchaseAgreement,'CREATE');
                
                
            }
        }
        insert milestoneListToBeAdded;
    }
    public void beforeUpdate()
    {
    }
    public void afterUpdate()
    {
        setSOQLMAPOrList();
        list<Order__c> purchaseAgreementList = (list<Order__c>)trigger.new;
       
        	for(Order__c purchaseAgreement : purchaseAgreementList){
                Order__c oldpurchaseAgreement = (Order__c)Trigger.oldMap.get(purchaseAgreement.Id);
                if(oldpurchaseAgreement.Import_Milestones_From_Purchase_Agrement__c != purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c){
                    //Delete existing milestones
                    List<Purchase_Agreement_Milestone__c> existingMilestoneForPA=  mapPurchaseAgreement.get(purchaseAgreement.id).Purchase_Agreement_Milesones__r;
                        if(existingMilestoneForPA.size()>0){
                            List<Purchase_Agreement_Milestone__c> milestoneListToBeDeleted = createOrDeleteMileStones(purchaseAgreement,'DELETE');
                        	system.debug('List Size  of Milestone to be deleted-----'+ milestoneListToBeDeleted.size());
                        	if(milestoneListToBeDeleted.size() >0){
                            	delete milestoneListToBeDeleted;
                        	}
                           //  delete only copied milestone from existingMilestoneForPA;
                        } 
                    //Create milestone if import milestone is not null
                    if(purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c!=null){
                        system.debug('Create milestones with new Import milestone from PA');
                        List<Purchase_Agreement_Milestone__c> milestoneListToBeAdded = createOrDeleteMileStones(purchaseAgreement,'CREATE');
                         	if(milestoneListToBeAdded.size()>0){
                                try{
                        		insert milestoneListToBeAdded;
                                }
                                catch(DMLException e){
                                     String S1 = e.getMessage();
                                    if(s1.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
            					     	S1 = S1.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ' , ': [');
                                    }
									purchaseAgreement.addError('Error occured while importing milestone :'+s1) ;
                                }
                        	}
                    }
                            
                }
    		}
    }
    public void beforeDelete()
    {
    }
    public void afterDelete()
    {
    }
    public void afterUnDelete()
    {
    }
    public void andFinally()
    {
	}
    public void bulkBefore()
    {
       
    }
    public void bulkAfter()
    {
    }
    
   
    private List<Purchase_Agreement_Milestone__c> createOrDeleteMileStones(Order__c purchaseAgreement, String operation ){
        system.debug('purchaseAgreement  ----'+ purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c );
        List<Purchase_Agreement_Milestone__c> milestoneListToBeAdded = new  List<Purchase_Agreement_Milestone__c>();
        
          
    	if(operation.equals('CREATE')){   
            List<Purchase_Agreement_Milestone__c> milestonesToAdd = mapPurchaseAgreement.get(purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c).Purchase_Agreement_Milesones__r;
                
           	system.debug('List Size of Milestones from Purchase Agreement - '+purchaseAgreement.Import_Milestones_From_Purchase_Agrement__c+'----' +milestonesToAdd.size());
        
            if(milestonesToAdd.size()>0){
        	  for(Purchase_Agreement_Milestone__c milestone : milestonesToAdd){
                   Purchase_Agreement_Milestone__c newMilestone = new  Purchase_Agreement_Milestone__c();
                   newMilestone.Purchase_Agreement__c = purchaseAgreement.Id;
                   newMilestone.Applicable_AC_Type__c = milestone.Applicable_AC_Type__c;
                   newMilestone.Comment__c = milestone.Comment__c;
                   newMilestone.Description__c = milestone.Description__c;
                   newMilestone.Milestone_Date__c = milestone.Milestone_Date__c;
                   newMilestone.Months_to_Delivery__c = milestone.Months_to_Delivery__c;
                   newMilestone.PA_Milestone_Type__c = milestone.PA_Milestone_Type__c;
                   newMilestone.Y_Hidden_Is_Copied_From_Other_PA__c = true;
              //   newMilestone.Name = LeasewareUtils.blankIfNull(LeasewareUtils.getDatetoString(milestone.Milestone_Date__c,'DD MON YYYY'))+'-'+leasewareUtils.blankIfNull(milestone.Applicable_AC_Type__c)+'-'+leasewareUtils.blankIfNull(milestone.PA_Milestone_Type__c) ;
                   milestoneListToBeAdded.add(newMilestone);
         		}
            }
            return milestoneListToBeAdded;
        }
        else{
            //Delete Operation
            List<Purchase_Agreement_Milestone__c> milestonesForPA = mapPurchaseAgreement.get(purchaseAgreement.Id).Purchase_Agreement_Milesones__r;
          	List<Purchase_Agreement_Milestone__c> milestonesToDelete = new List<Purchase_Agreement_Milestone__c>();
            for(Purchase_Agreement_Milestone__c pamilestone:milestonesForPA){
                if(pamilestone.Y_Hidden_Is_Copied_From_Other_PA__c){
                    milestonesToDelete.add(pamilestone);
                }
            }
            return  milestonesToDelete;
        }
        
    }
}