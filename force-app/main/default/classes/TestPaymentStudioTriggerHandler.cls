@isTest
Public class TestPaymentStudioTriggerHandler{
    @TestSetup
    static void setData(){
		Date dateField = System.today();
		Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
		Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);

        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
		insert operator;

		Lease__c leaseRecord = new Lease__c(
			Name = 'Testing',
			Lease_Start_Date_New__c = Date.valueOf('2020-03-01'),
			Lease_End_Date_New__c = Date.valueOf('2021-11-12'),
			Threshold_Claim_Amount__c = 5000,
            Lessee__c=operator.Id
			);
		insert leaseRecord;

		// Set up the Aircraft record
		Aircraft__c asset = new Aircraft__c(Date_of_Manufacture__c = System.today(), TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available', Derate__c=20, MSN_Number__c = '28216');
		LeaseWareUtils.clearFromTrigger();
		insert asset;

		// update lease reocrd with aircraft
		leaseRecord.Aircraft__c=asset.id;
		LeaseWareUtils.clearFromTrigger();
		update leaseRecord;

        Cash_Security_Deposit__c cashSD = new Cash_Security_Deposit__c(name ='Test SD',lease__c = leaseRecord.id,Security_Deposit_Cash__c = 100000 );
        insert cashSD;

		Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
		insert legalEntity;

		map<String,Object> mapInOut = new map<String,Object>();
		TestLeaseworkUtil.createTSLookup(mapInOut);

		List<Constituent_Assembly__c> assemblyList = new List<Constituent_Assembly__c>();
		assemblyList.add(new Constituent_Assembly__c(Name='Airframe', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
		                                             TSLV__c=0, CSLV__c= 0, Type__c ='Airframe', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId(),
		                                             Serial_Number__c='Airframe'));
		assemblyList.add(new Constituent_Assembly__c(Name='Engine 1', Attached_Aircraft__c= asset.Id, Description__c ='test description', TSN__c=0, CSN__c=0,
		                                             TSLV__c=0, CSLV__c= 0, Type__c ='Engine 1', RecordTypeId = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Engine').getRecordTypeId(),
		                                             Serial_Number__c='Engine 1'));

		LeaseWareUtils.clearFromTrigger();
		insert assemblyList;

		// Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(name='Test',Current_Catalog_Year__c='2020');
		insert mp;

		List<Maintenance_Program_Event__c> mpeList = new List<Maintenance_Program_Event__c>();
		mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Airframe', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = '4C/6Y'));
		mpeList.add(new Maintenance_Program_Event__c(Assembly__c = 'Engine', Maintenance_Program__c= mp.Id, Interval_2_Hours__c=20, Event_Type_Global__c = 'Performance Restoration'));
		insert mpeList;

		// Set up Project Event for Airframe
		List<Assembly_Event_Info__c> projectedEventList = new List<Assembly_Event_Info__c>();
		projectedEventList.add(new Assembly_Event_Info__c(Name = 'Airframe - 4C/6Y', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[0].Id, Maintenance_Program_Event__c = mpeList[0].Id));
		projectedEventList.add(new Assembly_Event_Info__c(Name = 'Engine 1 - PR', Event_Cost__c = 20, Constituent_Assembly__c = assemblyList[1].Id, Maintenance_Program_Event__c = mpeList[1].Id));
		insert projectedEventList;

		List<Assembly_MR_Rate__c> supplementalRentList = new List<Assembly_MR_Rate__c>();
		supplementalRentList.add(new Assembly_MR_Rate__c(Name='Airframe - 4C/6Y', Assembly_Lkp__c = assemblyList[0].id, Assembly_Event_Info__c = projectedEventList[0].id,
		                                                 Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Induction Date', Rate_Basis__c='Cycles',
		                                                 Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
		supplementalRentList.add(new Assembly_MR_Rate__c(Name='Engine 1 - PR', Assembly_Lkp__c = assemblyList[1].id, Assembly_Event_Info__c = projectedEventList[1].id,
		                                                 Base_MRR__c = 200, Lease__c = leaseRecord.Id, Available_Reference_Date__c = 'Removal Date', Rate_Basis__c='Cycles',
		                                                 Lessor_Contribution__c = 35000.25, Starting_MR_Balance__c=100000));
		 insert supplementalRentList;

         List<Assembly_Eligible_Event__c> historicalEventList = new List<Assembly_Eligible_Event__c>();
		historicalEventList.add(new Assembly_Eligible_Event__c(Name ='Airframe 4C/6Y', Constituent_Assembly__c = assemblyList[0].Id, Projected_Event__c = projectedEventList[0].id,
		                                                       Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
		                                                       Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = '4C/6Y'));
		historicalEventList.add(new Assembly_Eligible_Event__c(Name ='Engine 1 PR', Constituent_Assembly__c = assemblyList[1].Id, Projected_Event__c = projectedEventList[1].id,
		                                                       Date_Of_Removal__c = Date.valueOf('2019-07-01'), Start_Date__c = Date.valueOf('2019-07-15'), End_Date__c = Date.valueOf('2019-08-08'),
		                                                       Date_of_Installation__c = Date.valueOf('2019-08-15'), TSN__c = 50, CSN__c = 10, Event_Type__c = 'Performance Restoration'));
		
        LWGlobalUtils.setTriggersflagOn();
		insert historicalEventList;
		LWGlobalUtils.setTriggersflagOff();


		List<Claims__c> claimList = new List<Claims__c>();
		claimList.add(new Claims__c(Name ='Airframe 4C/6Y', Supplemental_Rent__c = supplementalRentList[0].Id, Approved_Claim_Amount__c = 1000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
		claimList.add(new Claims__c(Name ='Engine 1 PR', Supplemental_Rent__c = supplementalRentList[1].Id, Approved_Claim_Amount__c = 1000, Claim_Status__c = 'Approved', Override_Total_Amount__c = false, Estimated_Claim_Amount__c = 1000));
		insert claimList;

		Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                          Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                                          Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
        LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;

		Utilization_Report__c utilizationRecord1 = new Utilization_Report__c(
            Name='Testing 1',
            FH_String__c ='61',
            Airframe_Cycles_Landing_During_Month__c= 23,
            Aircraft__c = asset.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
			Type__c = 'Actual',
			Status__c='Approved By Lessor',
            Month_Ending__c = Date.newInstance(2020,03,31)          
        );
        
        insert utilizationRecord1;
	}
    @isTest static void methodName() {
        
        
        Date dateField = System.today(); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Aircraft__c acRecord = [Select id, Name, Maintenance_Program__c from Aircraft__c ];
		Lease__c leaseRecord = [Select id, Name from Lease__c];
        Utilization_Report__c utilizationRecord = [select id,Status__c from Utilization_Report__c ];
        operator__c testOperator = [select id from operator__c];

        Test.startTest();
        utilizationRecord.Status__c = 'Approved By Lessor';
        update utilizationRecord;
      
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Amount__c =200,
            Status__c = 'Approved',
            payment_status__c ='Open'
            
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Payment_Receipt__c payRecord = new Payment_Receipt__c(
            Name='Testing',
            Approval_Status__c = 'Approved'
        );
        insert payRecord; 
        
        Payment_Receipt_Line_Item__c paymentLineITemREcord = new Payment_Receipt_Line_Item__c(
            Invoice__c= invoiceRecord.Id,
            Payment_Receipt__c = payRecord.Id,
            Amount_Paid__c = 20,
            Bank_Charges__c = 10
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert paymentLineITemREcord;
        LWGlobalUtils.setTriggersflagOff();
        
        Payment__c paymentRecord = new Payment__c(
            Name = 'Payment Testing',
            Invoice__c = invoiceRecord.Id,
            Amount__c = invoiceRecord.Amount__c
            
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert paymentRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Letter_Of_Credit__c locRecord = new Letter_Of_Credit__c(Name ='loc Domo Record', LC_Number__c ='012345678', Lease__c =leaseRecord.Id, Lessee__c =testOperator.Id, LC_Amount_LC_Currency__c =20000);
        insert locRecord;
        
        LOC_Drawn_Down__c drawnDownRecord = new LOC_Drawn_Down__c(Name = 'Test Drawn Down', LETTER_OF_CREDIT__C =locRecord.Id);
        insert drawnDownRecord;
        
        
        Payment_Receipt__c paymentReciptRecord = new Payment_Receipt__c(
            Name='Testing',
            Approval_Status__c = 'Pending',
            LOC_Drawn_Down__c = drawnDownRecord.Id
        );
        insert paymentReciptRecord; 
        
        LeaseWareUtils.unsetTriggers('PaymentStudioTriggerHandlerBefore');
        LeaseWareUtils.unsetTriggers('PaymentStudioTriggerHandlerAfter');
        List<Payment_Receipt__c> storepaylist = new List<Payment_Receipt__c>();
        List<Payment_Receipt__c> paylist = [select id, Approval_Status__c from Payment_Receipt__c where Approval_Status__c='Pending'];
        
        for(Payment_Receipt__c p : paylist){
            p.Approval_Status__c ='Approved';
            storepaylist.add(p);
        }
        //update storepaylist;
        
        Payment_Receipt__c[] PaymentReceiptlist = [select id, Name from Payment_Receipt__c];
        try {
            LeaseWareUtils.unsetTriggers('PaymentStudioTriggerHandlerBefore');
            LeaseWareUtils.unsetTriggers('PaymentStudioTriggerHandlerAfter');
            Delete PaymentReceiptlist[0];
            
            LeaseWareUtils.unsetTriggers('PaymentStudioTriggerHandlerAfter');
            LWGlobalUtils.setTriggersflagOn(); 
            undelete PaymentReceiptlist[0];
            LWGlobalUtils.setTriggersflagOff();
        }
        catch(Exception ee) { }
        Test.stopTest();
    }
    
    @isTest
    public static void testPaidViaOffsetFeature(){
        Test.startTest();
        Aircraft__c acRecord = [Select id, Name, Maintenance_Program__c from Aircraft__c ];
		Lease__c lease = [Select id, Name from Lease__c];

		Constituent_Assembly__c assembly = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Airframe' LIMIT 1];

		Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: assembly.Id LIMIT 1];

		Assembly_MR_Rate__c mrInfo = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c,Assembly_Lkp__r.Type__c
                                        ,Available_Reference_Date__c,Assembly_Event_Info__r.Event_Type__c
                                        
		                              FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: assembly.Id LIMIT 1];

		Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];

		
		MRClaimController.getAssemblyMRInfo(claim.Id);
		MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
		MRClaimController.getAssemblyMRInfos(claim.Id, ''); 
		MRClaimController.findMRClaimAvailable(mrInfo, claim.Id);

		claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,Supplemental_Rent__r.Lease__r.Lessor__c,
		         Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c,Supplemental_Rent__r.Lease__r.lessee__c,Supplemental_Rent__r.Lease__r.operator__c
		         ,Approved_Claim_Amount__c, Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c,Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c
		         FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];
		LeasewareUtils.clearFromTrigger();
		MRClaimController.calculateTotalInvoiceAmount(claim.Id);
		MRClaimController.createEventInvoice(claim, mrInfo, claim.Approved_Claim_Amount__c, '2019-08-08', 'Removal', 2000.45, 0);
        payable__c py = [select payable_amount__c ,id,status__c,Payable_Due_Date__c,lease__c,lessee__c,claim__c,Payable_Type__c 
                           from payable__c limit 1];
        Utilization_Report__c utilizationRecord = [select id from Utilization_Report__c ];

                        Invoice__c invoiceRecord = new Invoice__c(
                            Name='2019-UI-',
                            Utilization_Report__c = utilizationRecord.Id,
                            Lease__c = lease.Id,
                            Status__c = 'Approved',
                            Amount__c =300,
                            MR_Claim_Offset_Applied__c=0
                        );
                        LWGlobalUtils.setTriggersflagOn(); 
                        insert invoiceRecord;
                        LWGlobalUtils.setTriggersflagOff();

        Payment_Receipt__c payReceiptRecord = new Payment_Receipt__c(
            Name='Testing1',
            Approval_Status__c = 'Pending',
            MR_Claim_Event_Payable__c = py.Id,
            Payment_Amount_Cur__c= 300,
            Payment_Date__c = Date.valueOf('2020-01-31')
        );
        insert payReceiptRecord;
        Payment_Receipt_Line_Item__c paymentLineItem=new Payment_Receipt_Line_Item__c(
            Amount_Paid__c=300,
            Invoice__c=invoiceRecord.Id,
            Payment_Receipt__c=payReceiptRecord.Id,
            Bank_Charges__c = 0
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert paymentLineItem;
        LWGlobalUtils.setTriggersflagOff();
        leasewareutils.clearFromTrigger();
        payReceiptRecord.Approval_Status__c = 'Approved';
        update payReceiptRecord;
        py = [select id,Payable_Amount_Paid_Cash__c,Amount_Paid_via_Offset__c,Payable_type__c,(select id,Line_Payout_Amount__c from payable_line_item__r) from payable__c];
        payout__c po = [select id,Payout_Type__c,Payout_Amount__c from payout__c];
       
        system.assertEquals(py.Payable_Amount_Paid_Cash__c,0);
        system.assertEquals(py.Amount_Paid_via_Offset__c,300);
        system.assertEquals(py.payable_line_item__r[0].Line_Payout_Amount__c,300);
        system.assertEquals(po.Payout_Type__c,'Offset');
        system.assertEquals(po.Payout_Amount__c,300);
        Test.stopTest();
    }

    
    @isTest
    public static void testPrepymt(){
      
        Test.startTest();
        Parent_Accounting_Setup__c ASRec = new Parent_Accounting_Setup__c(Name='Dummy' );
        insert ASRec;
        TestLeaseworkUtil.setCoaPy();
       
        Aircraft__c acRecord = [Select id, Name, Maintenance_Program__c from Aircraft__c ];
		Lease__c leaseRecord = [Select id, Name from Lease__c];
        operator__c testOperator = [select id from operator__c];
        
        leaseRecord.Accounting_Setup_P__c = ASRec.ID;
        update leaseRecord;
        
        Calendar_Period__c CP = new Calendar_Period__c(Name = 'Today',Start_Date__c =system.today().toStartOfMonth(),
                                                       End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1),closed__c=true);
        insert cp;
        
        Account bank = new Account(name='test',RecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Bank_Accounts').getRecordTypeId());
        insert bank;
       
        Payment_Receipt__c payRecord = new Payment_Receipt__c(
            Name='Testing',
            Approval_Status__c = 'Pending',
            Prepayment__c = true,
            Lease__c = leaseRecord.Id,
            Payment_Amount_Cur__c = 100000,
            Bank_Account_Lkp__c = bank.id,
            Accounting_Period__c = cp.id
            
        );
        insert payRecord; 
        List<Journal_Entry__c> lstJE = [Select id,journal_Status__c,name,credit__c,debit__c From Journal_Entry__c where prepayment__c = :payRecord.id and
                                        calendar_Period__c != null];
        
        system.assertEquals(2,lstJE.size());
        system.assertEquals(lstJE.get(0).journal_Status__c,'Draft - Pending Approval');
        for(journal_entry__c je :lstJE){
            system.debug('JE NAME:::'+je.name+' '+' '+je.credit__c+' '+je.debit__c);
            if(je.name=='Cash - Prepayments')system.assertEquals(100000, je.debit__c);
            if(je.name=='Customer Prepayments')system.assertEquals(100000, je.credit__c);
            
        }
        
        LeaseWareUtils.clearFromTrigger(); 
        try{
            payRecord.Approval_Status__c ='Approved';
            payRecord.Payment_Amount_Cur__c = 50000;
            //LWGlobalUtils.setTriggersflagOn(); 
            update payRecord;
            //LWGlobalUtils.setTriggersflagOff();
        }
        catch(Exception e){
            system.debug('e.getMessage()::::'+e.getMessage());
            Boolean expectedExceptionThrown =  e.getMessage().contains('The Payment cannot be Approved as the Accounting Period Today is closed') ? true : false ;
            System.AssertEquals(expectedExceptionThrown, true); 
        }
        
        LeaseWareUtils.clearFromTrigger(); 
        cp.Closed__c = false;
        update cp;
        
        LeaseWareUtils.clearFromTrigger(); 
        update payRecord;
        lstJE = [Select id,journal_Status__c,name,credit__c,debit__c From Journal_Entry__c where prepayment__c = :payRecord.id and
                 calendar_Period__c != null];
        
        system.assertEquals(2,lstJE.size());
        system.assertEquals(lstJE.get(0).journal_Status__c,'Posted');
        for(journal_entry__c je :lstJE){
            system.debug('JE NAME:::'+je.name+' '+' '+je.credit__c+' '+je.debit__c);
            if(je.name=='Cash - Prepayments')system.assertEquals(50000, je.debit__c);
            if(je.name=='Customer Prepayments')system.assertEquals(50000, je.credit__c);
            
        }
        Chart_of_accounts__c coa = new Chart_of_Accounts__c(name='Lessor COA',GL_Code__c='0001',GL_Account_Description__c='Lessor COA');
        insert coa;
        
       
        Payment_Receipt__c payRecord1 = new Payment_Receipt__c(
            Name='Testing',
            Approval_Status__c = 'Pending',
            Prepayment__c = true,
            operator__c = testOperator.id,
            Payment_Amount_Cur__c = 100000,
            Bank_Account_Lkp__c = bank.id,
            Accounting_Period__c = cp.id
        );
        LeaseWareUtils.clearFromTrigger();
        insert payRecord1;
        
        lstJE = [Select id,name,credit__c,debit__c,GL_Account_Code__c From Journal_Entry__c where prepayment__c = :payRecord1.id and
                 calendar_Period__c != null];
        
        //system.assertEquals(2,lstJE.size());
        for(journal_entry__c je :lstJE){
            system.debug('JE NAME:::'+je.name+' '+' '+je.credit__c+' '+je.debit__c);
            if(je.name=='Cash - Prepayments')system.assertEquals(100000, je.debit__c);
            if(je.name=='Customer Prepayments'){
                system.assertEquals(100000, je.credit__c);
                system.assertEquals('0001',je.gl_account_code__c);
            }
        }
        test.stopTest();
        
    }

    @isTest
    public static void testOffsetReversal(){
        
        Aircraft__c acRecord = [Select id, Name, Maintenance_Program__c from Aircraft__c ];
		Lease__c lease = [Select id, Name from Lease__c];

		Constituent_Assembly__c assembly = [SELECT Id, Name, Type__c FROM Constituent_Assembly__c WHERE Name = 'Airframe' LIMIT 1];

		Assembly_Eligible_Event__c historicalEvent = [SELECT Id, Name, Constituent_Assembly__c FROM Assembly_Eligible_Event__c WHERE Constituent_Assembly__c =: assembly.Id LIMIT 1];

		Assembly_MR_Rate__c mrInfo = [SELECT Id, Name, Assembly_Lkp__c, Assembly_Event_Info__c, Lease__c,Assembly_Lkp__r.Type__c
                                        ,Available_Reference_Date__c,Assembly_Event_Info__r.Event_Type__c
                                        
		                              FROM Assembly_MR_Rate__c Where Assembly_Lkp__c =: assembly.Id LIMIT 1];

		Claims__c claim = [SELECT Id, Event__c, Assembly__c FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];

        Test.startTest();
		MRClaimController.getAssemblyMRInfo(claim.Id);
		MRClaimController.updateHEventOnClaim(claim.Id, historicalEvent);
		MRClaimController.getAssemblyMRInfos(claim.Id, ''); 
		MRClaimController.findMRClaimAvailable(mrInfo, claim.Id);
		claim = [SELECT Id, Event__c, Assembly__c, Assembly__r.Type__c, Supplemental_Rent__c, Supplemental_Rent__r.Lease__c,Supplemental_Rent__r.Lease__r.Lessor__c,
		         Supplemental_Rent__r.Assembly_Event_Info__r.Event_Type__c, Event__r.Event_Type__c, Event__r.Event_Cost__c,Supplemental_Rent__r.Lease__r.lessee__c,Supplemental_Rent__r.Lease__r.operator__c
		         ,Approved_Claim_Amount__c, Claim_Status__c, Override_Total_Amount__c, Estimated_Claim_Amount__c,Supplemental_Rent__r.Lease__r.Claims_Payout_Due_Day__c
		         FROM Claims__c Where Supplemental_Rent__c =: mrInfo.Id LIMIT 1];
		LeasewareUtils.clearFromTrigger();
		MRClaimController.calculateTotalInvoiceAmount(claim.Id);
		MRClaimController.createEventInvoice(claim, mrInfo, claim.Approved_Claim_Amount__c, '2019-08-08', 'Removal', 2000.45, 0);
        
        payable__c py = [select payable_amount__c ,id,status__c,Payable_Due_Date__c,lease__c,lessee__c,claim__c,Payable_Type__c 
                           from payable__c limit 1];
        Utilization_Report__c utilizationRecord = [select id from Utilization_Report__c ];

                        Invoice__c invoiceRecord = new Invoice__c(
                            Name='2019-UI-',
                            Utilization_Report__c = utilizationRecord.Id,
                            Lease__c = lease.Id,
                            Status__c = 'Approved',
                            Amount__c =300,
                            MR_Claim_Offset_Applied__c=0
                        );
                        LWGlobalUtils.setTriggersflagOn(); 
                        insert invoiceRecord;
                        LWGlobalUtils.setTriggersflagOff();

        Payment_Receipt__c payReceiptRecord = new Payment_Receipt__c(
            Name='Testing1',
            Approval_Status__c = 'Pending',
            MR_Claim_Event_Payable__c = py.Id,
            Payment_Amount_Cur__c= 300,
            Payment_Date__c = Date.valueOf('2020-01-31')
        );
        insert payReceiptRecord;
        Payment_Receipt_Line_Item__c paymentLineItem=new Payment_Receipt_Line_Item__c(
            Amount_Paid__c=300,
            Invoice__c=invoiceRecord.Id,
            Payment_Receipt__c=payReceiptRecord.Id,
            Bank_Charges__c = 0
        );
        LWGlobalUtils.setTriggersflagOn(); 
        insert paymentLineItem;
        LWGlobalUtils.setTriggersflagOff();
        leasewareutils.clearFromTrigger();
        payReceiptRecord.Approval_Status__c = 'Approved';
        update payReceiptRecord;

        payout__c po = [select id,Payout_Type__c,Payout_Amount__c,status__c,Payment_Line_Item__c from payout__c];
        payment__c  pmt = [select id,amount__c,payment_status__c from payment__c where Y_Hidden_Payment_Std_Line_Item__c = :po.Payment_Line_Item__c];
        LeaseWareUtils.TriggerDisabledFlag=true;               
        po.Status__c = 'Cancelled-Pending';
        update po;
        pmt.payment_status__c = 'Cancelled-Pending';
        update pmt;
        LeaseWareUtils.TriggerDisabledFlag=false;
        LeaseWareUtils.clearFromTrigger();
        system.debug('po status::'+po.status__c);
        po.Status__c = 'Cancelled';
        update po;  
        py = [select id,Payable_Amount_Paid_Cash__c,Amount_Paid_via_Offset__c,Payable_type__c,(select id,Line_Payout_Amount__c from payable_line_item__r) from payable__c];
       
        system.assertEquals(py.Payable_Amount_Paid_Cash__c,0);
        system.assertEquals(py.Amount_Paid_via_Offset__c,0);
        system.assertEquals(py.payable_line_item__r[0].Line_Payout_Amount__c,0);
        Test.stopTest();
    }
}