/**
*******************************************************************************
* Class: TestCommercialTermRelatedListController
* @author Created by Bhavna, Lease-Works, 
* @date  6/12/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Test all the Methods of CommercialTermRelatedListController.
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*
********************************************************************************
*/
@isTest
public class TestCommercialTermRelatedListController {
    
    /**
    * @description Created Test Data to test all the Methods of CommercialTermRelatedListController.
    *    
    * @return null
    */
    @testSetup 
    static void setup() {
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        System.debug('What is the profile id ' + profile1);
        date tDate = date.today();
        date uDate = Date.today().addDays(30);
        
        User u = new User(
            ProfileId = profile1.Id,
            Username = 'testUser123@leaseWorks.com',
            Alias = 'test',
            Email='testUser1@salesforce.com',
            EmailEncodingKey='UTF-8',
            Firstname='testing',
            Lastname='user',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago');
        insert u;
        
        System.runAs(u){           
            string dealRecType;
            string ctRecType;
            Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c');
            
            for(Schema.RecordTypeInfo rti : objectDescribe.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    dealRecType = rti.getRecordTypeId();
                }
            }
            
            Schema.DescribeSObjectResult objectDescribe1 = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
            
            for(Schema.RecordTypeInfo rti : objectDescribe1.getRecordTypeInfos()) {
                if(rti.getName()=='Engine Lease'){
                    ctRecType = rti.getRecordTypeId();
                }
            }
            
            Operator__c testOperator = new Operator__c(
                Name = 'American Airlines',                   
                Status__c = 'Approved',                          
                Rent_Payments_Before_Holidays__c = false,         
                Revenue__c = 0.00,                                
                Current_Lessee__c = true                         
            );
            insert testOperator;
            
            Marketing_Activity__c testMarketingActivity = new Marketing_Activity__c(
                Name = 'Test'+1,
                Deal_Type__c = 'Lease',                                
                Deal_Status__c = 'Pipeline',                           
                Asset_Type__c = 'Engine',                              
                Prospect__c = testOperator.Id,                    
                Marketing_Rep_c__c = 'N/A',                            
                Description__c = '2',                                                
                Inactive_MA__c = false,
                RecordTypeId = dealRecType
            );                             
            insert testMarketingActivity;
            
            List<Aircraft__c> aircraftList = new List<Aircraft__c>();
            for(Integer i=0;i<2;i++){
                Aircraft__c testAircraft = new Aircraft__c();
                testAircraft.Name = 'Test'+i;                        
                testAircraft.MSN_Number__c = '28216'+i;                         
                testAircraft.Date_of_Manufacture__c = system.today();
                testAircraft.Aircraft_Type__c = '737'+i;                           
                testAircraft.Aircraft_Variant__c = '700'+i;                      
                testAircraft.Marked_For_Sale__c = false;                      
                testAircraft.Alert_Threshold__c = 10.00;                        
                testAircraft.Status__c = 'Available';                     
                testAircraft.Awarded__c = false;                                                  
                testAircraft.TSN__c = 1.00;                                 
                testAircraft.CSN__c = 1;
                aircraftList.add(testAircraft);
            }
            insert aircraftList;
            system.debug('------aircraftList'+aircraftList);
            
            Pricing_Run_New__c testPricingRunNew = new Pricing_Run_New__c(
                Name='Test',
                Current_Rent__c= 2000,
                EBT__c = 20,
                IRR__c = 2,
                Lease_Rate_Factor__c = 2,
                Purchase_Price_For_SLBs_Only__c = 20,
                Rate_Type__c = 'Fixed/Fixed',
                Marketing_Activity__c = testMarketingActivity.Id,
                RecordTypeId = ctRecType
            );
            insert testPricingRunNew;
            
            Pricing_Run_New__c testPricingRun = new Pricing_Run_New__c(
                Name='Test1',
                Current_Rent__c= 3000,
                EBT__c = 20,
                IRR__c = 2,
                Lease_Rate_Factor__c = 2,
                Purchase_Price_For_SLBs_Only__c = 20,
                Rate_Type__c = 'Fixed/Fixed',
                Marketing_Activity__c = testMarketingActivity.Id,
                RecordTypeId = ctRecType
            );
            insert testPricingRun;
            
            Aircraft_Proposal__c testAircraftProposal = new Aircraft_Proposal__c(
                Name='Test',
                Marketing_Activity__c = testMarketingActivity.Id,
                Engine_Type_new__c = 'Engine',
                Aircraft__c = aircraftList[0].Id
            );
            insert testAircraftProposal;
            
            Pricing_Output_New__c testPricingOutputNew = new Pricing_Output_New__c(
                Asset_Term__c = testAircraftProposal.Id,
                Pricing_Run__c = testPricingRunNew.Id
            );
            insert testPricingOutputNew;
            
            Pricing_Output_New__c testPricingOutput = new Pricing_Output_New__c(
                Asset_Term__c = testAircraftProposal.Id,
                Pricing_Run__c = testPricingRun.Id
            );
            insert testPricingOutput;
        }
    }
    
    /**
    * @description This Methods gives the code coverage of  getDealCommercialTerms,setAssetInDeal,createAssetInDeal
    *   getPageLayoutFields,getRecordIdPrefix,getDealAnalysisRecTypeId method of CommercialTermRelatedListController
    *
    * @return null
    */
    @isTest static void testCommercialTermRelatedList(){
        Marketing_Activity__c[] newlist = [select id from Marketing_Activity__c];
        List<Aircraft__c> aircraftRecord = [select id,Name from Aircraft__c where MSN_Number__c ='282161'];
        List<Pricing_Output_New__c> pricingOutputNewList =[select id,Asset_Term__c, 
                                                           Asset_Term__r.Aircraft__c 
                                                           from Pricing_Output_New__c];
        Pricing_Run_New__c pricingRunNewList = [select id from Pricing_Run_New__c WHERE Name='Test1'];
        
        List<String> aircraftProposalList = new  List<String>();
        aircraftProposalList.add(aircraftRecord[0].Id);
        String ctRecType;
        Schema.DescribeSObjectResult objectDescribe1 = RLUtility.describeObject(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c');
        
        for(Schema.RecordTypeInfo rti : objectDescribe1.getRecordTypeInfos()) {
            if(rti.getName()=='Engine Lease'){
                ctRecType = rti.getRecordTypeId();
            }
        }
        system.debug('---------aircraftRecord'+aircraftRecord);
        
        
        
        CommercialTermRelatedListController.getDealCommercialTerms(newlist[0].Id, 
                                                                   leasewareutils.getNamespacePrefix() + 'CommercialTermFieldSet','createddate');
        
        List<Aircraft_Proposal__c> assetInDealList = CommercialTermRelatedListController.getAssetsInDeal(String.valueOf(pricingRunNewList.Id));
        
        CommercialTermRelatedListController.processRelatedRecords(pricingRunNewList.Id,
                                                           newlist[0].Id,aircraftRecord,
                                                           '2', aircraftProposalList, 'Clone',assetInDealList, true);
        
        CommercialTermRelatedListController.processRelatedRecords(pricingRunNewList.Id,
                                                           newlist[0].Id,aircraftRecord,
                                                           '2', aircraftProposalList, 'Edit',assetInDealList, false);
        /*
        CommercialTermRelatedListController.setAssetInDeal(pricingRunNewList.Id,
                                                           newlist[0].Id,aircraftRecord,
                                                           pricingOutputNewList);
        CommercialTermRelatedListController.createAssetInDeal(String.valueOf(pricingRunNewList.Id), 
                                                              String.valueOf(newlist[0].Id), '2', aircraftProposalList, 'Clone');
        CommercialTermRelatedListController.createAssetInDeal(String.valueOf(pricingRunNewList.Id), 
                                                              String.valueOf(newlist[0].Id), '2', aircraftProposalList, 'Edit');
        CommercialTermRelatedListController.createEconomicAnalysis(String.valueOf(pricingRunNewList.Id), 
                                                              String.valueOf(newlist[0].Id), assetInDealList);
        */
        CommercialTermRelatedListController.getPageLayoutFields(pricingRunNewList.Id, 
                                                                leasewareutils.getNamespacePrefix() + 'Engine_Type__c');
        CommercialTermRelatedListController.getRecordIdPrefix(pricingRunNewList.Id);
        CommercialTermRelatedListController.getDealAnalysisRecTypeId(newlist[0].Id);
        
        system.assertEquals(newlist.size(),1);
    }
    
    /**
    * @description This Methods gives the code coverage of deleteCommercialTerm  
    *   method of CommercialTermRelatedListController 
    *
    * @return null
    */
    @isTest static void testdelete(){
        Pricing_Run_New__c[] pricingRunNew = [select id from Pricing_Run_New__c WHERE Name='Test1'];
        
        CommercialTermRelatedListController.deleteCommercialTerm(pricingRunNew[0].Id, true, false);
        system.assertEquals(pricingRunNew.size(),1);
    }
    
}