global without sharing class PostInstallClass implements InstallHandler {

    //utility method
    global String getZ_FieldsWithReadPerm(){
        //deprecated
        return 'deprecated' ;
    }
    private void createPkgHistory(InstallContext context){
        insert new Package_History__c(name= ''+system.requestVersion(),Prev_Package_Version__c	= (context.previousVersion()==null?null:''+context.previousVersion()) );
    }

    //==========================================================================






    //March 2020 release : patch
    private string onInstall_10(){
        string strMsg=' Succeeded. ';
        try{
            GenericScriptExecute.callScript(10);
        }catch(exception e){
            strMsg+=' callGenericScriptMethod Failed. ' + e.getMessage();
        }        
        return ' onInstall_10 : GenericScriptExecute execution ' + strMsg;
    }

    // June 2020 release
    private string onInstall_11(){
        string strMsg=' Succeeded.';
        try{
            GenericScriptExecute.callScript(11);
        }catch(exception e){
            strMsg+=' callGenericScriptMethod Failed. ' + e.getMessage();
        }             
        return ' onInstall_11 : GenericScriptExecute execution ' + strMsg;
    }

    // Sept 2020 release
    private string onInstall_12(){
        string strMsg=' Succeeded.';
        try{
            GenericScriptExecute.callScript(12);
        }catch(exception e){
            strMsg+=' callGenericScriptMethod Failed. ' + e.getMessage();
        }             
        return ' onInstall_12 : GenericScriptExecute execution ' + strMsg;
    }    

    // Dec 2020 release
    private string onInstall_13(){
        string strMsg=' Succeeded.';
        try{
            GenericScriptExecute.callScript(13);
        }catch(exception e){
            strMsg+=' callGenericScriptMethod Failed. ' + e.getMessage();
        }             
        return ' onInstall_13 : GenericScriptExecute execution ' + strMsg;
    }    

     // Mar 2021 release
    private string onInstall_14(){
        string strMsg=' Succeeded.';
        try{
            GenericScriptExecute.callScript(14);
        }catch(exception e){
            strMsg+=' callGenericScriptMethod Failed. ' + e.getMessage();
        }             
        return ' onInstall_14 : GenericScriptExecute execution ' + strMsg;
    }    

       // Jun 2021 release
    private string onInstall_15(){
        string strMsg=' Succeeded.';
        try{
            GenericScriptExecute.callScript(15);
        }catch(exception e){
            strMsg+=' callGenericScriptMethod Failed. ' + e.getMessage();
        }             
        return ' onInstall_15 : GenericScriptExecute execution ' + strMsg;
    }    
    //===================================================================================================
    // Actual method call on post deployment
    global void onInstall(InstallContext context) {
        String strBody =  'Package ' + system.requestVersion() +' installed on ' + context.organizationId() + '<BR>Previous Version: ' + context.previousVersion();
        createPkgHistory(context);// package History

        // list of script need to be run in all the deployment.
        LeaseWareSeededData.SeedLWSetup();  // leaseworks custom setting
        LeaseWareSeededData.SeedCustomLookup(); // custom lookup data
        LeaseWareSeededData.SeedLWServiceSetup(); // service custom setting
        LeaseWareSeededData.SeedSequenceNumber();//Invoice Sequencing
        LeaseWareSeededData.SeedSnapshotConfiguration(); // snapshot configuration
        LeaseWareSeededData.SeedRecommendationFactorForRiskAssetView(); //Weightage Configuration
        LeaseWareSeededData.SeedMappingObject();//Mapping Object
        LeaseWareSeededData.SeedAviatorCategories();// creating aviator categories
        LeaseWareSeededData.SeedAviatorRegions(); // createing aviator regions
        
        //===============Called bassed on the package version ===================================================================


        //release : March 2020 : we are checking last prev verion to current prev version
        // example For March 2020 : Perv version should lie between 9-10
        if(context.previousVersion() != null && ((system.requestVersion().major()==10 || system.requestVersion().major()==11) || (context.previousVersion().major()>=9 && context.previousVersion().major()<=10))){
            strBody+='<BR>Executing onInstall_10';
            strBody+='<BR>' + onInstall_10() + '<BR>';
        }           
        //release : June 2020
        if(context.previousVersion() != null && ((system.requestVersion().major()==11 || system.requestVersion().major()==12) || (context.previousVersion().major()>=10 && context.previousVersion().major()<=11) )){
            strBody+='<BR>Executing onInstall_11';
            strBody+='<BR>' + onInstall_11() + '<BR>';
        }   
        //release : Sept 2020
        if(context.previousVersion() != null && ((system.requestVersion().major()==12 || system.requestVersion().major()==13) || (context.previousVersion().major()>=11 && context.previousVersion().major()<=12) )){
            strBody+='<BR>Executing onInstall_12';
            strBody+='<BR>' + onInstall_12() + '<BR>';
        }  

        //release : Dec 2020
        if(context.previousVersion() != null && ((system.requestVersion().major()==13 || system.requestVersion().major()==14) || (context.previousVersion().major()>=12 && context.previousVersion().major()<=13) )){
            strBody+='<BR>Executing onInstall_13';
            strBody+='<BR>' + onInstall_13() + '<BR>';
        }  

        //release : Mar 2021 : It will get called as part of 14 ( major version) as well as 15 if it was not called before.
        if(context.previousVersion() != null && ((system.requestVersion().major()==14 || system.requestVersion().major()==15) || (context.previousVersion().major()>=13 && context.previousVersion().major()<=14) )){
            strBody+='<BR>Executing onInstall_14';
            strBody+='<BR>' + onInstall_14() + '<BR>';
        }  
            //release : Jun 2021 : It will get called as part of 15 ( major version) as well as 16 if it was not called before.
        if(context.previousVersion() != null && ((system.requestVersion().major()==15 || system.requestVersion().major()==16) || (context.previousVersion().major()>=14 && context.previousVersion().major()<=15) )){
            strBody+='<BR>Executing onInstall_15';
            strBody+='<BR>' + onInstall_15() + '<BR>';
        }  
        //=================End ====================================================

        // remove field permission of Z_unused fields
          
        //anjani commented :GenericScriptExecute.callGenericScriptMethod_Z('Z_UnusedRemPermissionOfObject_202003',''+system.requestVersion(),50,true,null);
        // looks like salesforce does not support field permission on post deployment
        strBody+='<BR> ----------------------------------------------------------------------------------------' ;
        strBody+='<BR> Admin, please run the following script' ;
        strBody+='<BR> 1. Execute script to remove permission of Z_Unused Objects/Fields.' ;
        strBody+='<BR>      a) Z_Unused Objects. Script : leaseworks.LWGlobalUtils.callGlobalScript(\'Z_Objects\')' ;
        strBody+='<BR>      b) Z_Unused Fields . Script : leaseworks.LWGlobalUtils.callGlobalScript(\'Z_Fields\')' ;
        strBody+='<BR>      IMP Note: !!! Check with Castlelake CSM before deleting/executing script related to Z_Unused fields/objects.' ;
        strBody+='<BR> 2. (Optional) Execute script to find duplicate fields. Script : leaseworks.LWGlobalUtils.callGlobalScript(\'Dup_F\')' ;
        strBody+='<BR> 3. Execute script to find Y_Hidden/Z_Unused metadata in Layouts. Script : leaseworks.LWGlobalUtils.callGlobalMethod(\'findHiddenAndUnusedFields\',null)' ;
        strBody+='<BR> 4. Execute script to delete records in data migration output which are more than 180 days old.Script : leaseworks.LWGlobalUtils.callGlobalScript(\'deleteOldDmoRecords_202103\')' ;
        strBody+='<BR> 5. Check for DocGen template change (if any) - Check it in the corresponding release deployment Asana task or check it in the post-deployment sheet.' ;
        strBody+='<BR> 6. Verify the status of scripts in package_script_history object.' ;
        strBody+='<BR>     Query:  select id,CreatedDate,name,leaseworks__Script_Name__c, leaseworks__Batch_Size__c,leaseworks__Status__c from leaseworks__Package_Script_History__c  where leaseworks__Status__c != \'Verified\' order by CreatedDate desc ' ;
        strBody+='<BR> ----------------------------------------------------------------------------------------<BR>' ;
        LWGlobalUtils.sendEmailToLWAdmins('Installation of '+ system.requestVersion() +' Complete', strBody);
        
    }



}