global class RentDueCheckScheduler implements Schedulable, Database.Batchable<SObject> {

    public static String strDueLeases='<tr><td>Lease Name</td><td>Rent Period</td><td>Balance Due</td><td>Rent Due Date</td></tr>';
    static String[] toAddresses = new String[]{};
    
    
    global void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new RentDueCheckScheduler());
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        String strQuery='Select Id, Name, Aircraft__c, Rent__c, Rent_Due_Day__c,' + 
        'Overdue_Check_Days__c, Late_Payment_Interest__c, Base_Rent__c, Rent_Date__c, Last_Escalation_Year__c, Rent_Type__c ' + 
        ' from Lease__c where Rent_Date__c <= TODAY and Rent_End_Date__c >= TODAY';
        System.debug(strQuery);
        return Database.getQueryLocator(strQuery); 
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){ 
        Boolean bSendMail;
        Integer nOverdueDays;
        //List<Lease__c> leasesToUpdate = new List<Lease__c>();
        bSendMail = false;
        for(Lease__c curLease : (Lease__c[])scope){ //#ForEachLease

            //List<Rent__c> rentSlipsToInsert = new List<Rent__c>();
            
/* Base Rent and the escalation factor is used to calculate Rent. Rent calculation no longer depends on Rent__c

            if(curLease.Last_Escalation_Year__c==null)curLease.Last_Escalation_Year__c=curLease.Rent_Date__c.Year();

            if(curLease.Last_Escalation_Year__c < Date.today().year() && curLease.Escalation__c != null && curLease.Escalation_Month__c != null){           
                Date dtEscalationDate = Date.newinstance(Date.today().year(), LeasewareUtils.getMonthNumber(curLease.Escalation_Month__c), 1);
                if( Date.today() > dtEscalationDate){
                    if(curLease.Rent__c==null)curLease.Rent__c=LeaseWareUtils.zeroIfNull(curLease.Base_Rent__c);
                    curLease.Rent__c = curLease.Rent__c * (1.0 + LeaseWareUtils.zeroIfNull(curLease.Escalation__c)/100.0);
                    curLease.Last_Escalation_Year__c = Date.today().year(); 
                    leasesToUpdate.add(curLease);
                }
            }
            
*/          
            /* Anjani 28-Feb-2016 : ensureRentSlip method is getting used only for PBH case. Hence commenting below code.
            try{
                if(!curLease.Rent_Type__c.startsWith('Power By The Hour')){// No automatic rent slip for PBH. To be created on UR filing.
                    Rent__c newRentRec=LeaseWareUtils.ensureRentSlip(curLease, Date.today(),null, null, null, 0);
                    if(newRentRec!=null){
                        rentSlipsToInsert.add(newRentRec);
                    }
                }
            }catch(exception e){
                system.debug('Rent Slip creation is not successful due to the exception ' + e);
            }*/
            if(curLease.Overdue_Check_Days__c==null||curLease.Overdue_Check_Days__c <1 || curLease.Overdue_Check_Days__c >28)
                nOverdueDays=10;
            else 
                nOverdueDays=(Integer)curLease.Overdue_Check_Days__c;

System.debug('After Insert for ' + curLease.Name);


			Date dtDateToCheck=date.today().addDays(-(nOverdueDays)); // Date on which last rent overdue check should be done.

			list<Rent__c> listRentToUpdate = new list<Rent__c>();
	 		Rent__c[] rentPending = [select Id, Name, Rent_Due_Date__c, Rent_Due_Amount__c, Rent_Interest__c, Total_Due__c, Balance__c from Rent__c where RentPayments__c =: curLease.Id and Paid_F__c != true and Rent_Due_Date__c < :dtDateToCheck ];

			Invoice__c[] listInv = [select Id,Rent__c from Invoice__c where Rent__c in :rentPending ];
			set<Id> setInv = new set<Id>();
			for(Invoice__c curRec:listInv){
				setInv.add(curRec.Rent__c);
			} 	 	
				
 			for(Rent__c rentRec : rentPending){
 					if(!setInv.contains(rentRec.Id)){
 						listRentToUpdate.add(rentRec);
 					}
		 			bSendMail=true;
		 			//Update interest - 
//					Simple interest formula
//		 			rentRec.Rent_Interest__c = LeaseWareUtils.zeroIfNull(rentRec.Rent_Due_Amount__c) * LeaseWareUtils.zeroIfNull(curLease.Late_Payment_Interest__c) * LeaseWareUtils.getNearestWorkDay(rentRec.Rent_Due_Date__c, curLease.Aircraft__c).daysBetween(date.today()) / 365;

//					Compound interest formula
					Decimal P=LeaseWareUtils.zeroIfNull(rentRec.Rent_Due_Amount__c);
					Decimal r=LeaseWareUtils.zeroIfNull(curLease.Late_Payment_Interest__c)/100;
					Integer n=365;	//Daily Compounding - TODO - Read it from Lease?
					//Decimal t=(rentRec.Rent_Due_Date__c==null)?0.0:((Decimal)(LeaseWareUtils.getNearestWorkDay(rentRec.Rent_Due_Date__c, curLease.Aircraft__c).daysBetween(date.today())) / 365.0);
					// Tilak need to correct this code
					Decimal t=(rentRec.Rent_Due_Date__c==null)?0.0:((Decimal)(rentRec.Rent_Due_Date__c.daysBetween(date.today())) / 365.0);
					Double dTotInterest= P * (Decimal)(1+r/n).pow((Integer)(n*t)) - P;
					System.Debug('Compound Interest for ' +rentRec.Name +' is - ' + dTotInterest.format());
					
					rentRec.Rent_Interest__c = dTotInterest;
					
		 			Decimal dBalDue=LeaseWareUtils.zeroIfNull(rentRec.Rent_Due_Amount__c)+LeaseWareUtils.zeroIfNull(rentRec.Rent_Interest__c);
					strDueLeases=strDueLeases+ '<tr><td>'+curLease.Name + '</td><td>' + dBalDue.setScale(2).format() + '</td><td>' + rentRec.Rent_Due_Date__c.format() + '</td></tr>';

//		 			update rentRec;
 			}
 			update listRentToUpdate;

			//upsert rentSlipsToInsert;
		} // #ForEachLease - End
		
		//if(leasesToUpdate.size() > 0)
			//update leasesToUpdate; 

		if(bSendMail){
    		LeaseWareUtils.sendEmail('Rent Overdue check over for '+ Date.today().format(), 'Overdue Leases :<br><table border="1">'+strDueLeases+'</table>', 'LW_RentSchedulerGroup', 'Regular');
		}
	
	}
	
	global void finish(Database.BatchableContext BC){

/*		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		LeaseWareUtils.getEmailsFromGroup('LW_RentSchedulerGroup', toAddresses);
		mail.setToAddresses(toAddresses); 
		mail.setSubject('Rent Overdue check over for '+ Date.today());
//		mail.setPlainTextBody('Active Leases :' + strActiveLeases + ', Checked Leases :' + strCheckedLeases +', Overdue Leases :'+strDueLeases+'.');
		mail.setPlainTextBody('Rent Overdue Check performed for the day.. (TODO) Just a debug mail, will be removed later.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
*/	}

}