public class UIAPIServices {
    private static final string prefix = leasewareutils.getNamespacePrefix();
    
    @AuraEnabled 
    public static Map<String,List<String>> fetchPicklistValues(String objectName, string fieldName, string recordTypeName) {
        if(String.isBlank( objectName) || String.isBlank(fieldName)){
            return null;
        }
        
        String objectNameNew = prefix +  objectName;
        String fieldNameNew = prefix + fieldName;
        
        String recordTypeId = '';
        if(!String.isBlank(recordTypeName)){
            recordTypeId = Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
        }
		 if( recordTypeId == null || recordTypeId == '' ){
            recordTypeId = Schema.getGlobalDescribe().get( objectName ).getDescribe().getRecordTypeInfosByDeveloperName().get( 'Master' ).getRecordTypeId(); 
        }
        /* else if(!String.isBlank(recordTypeLabelName)){
            //rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType =: objectNameNew and Name =: recordTypeLabelName];
            recordTypeId = Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByName().get(recordTypeLabelName).getRecordTypeId();
            }*/
        Map<string,List<string>> picklistValueMap = new Map<string,List<string>>();
        if(recordTypeId != null && recordTypeId != ''){
            picklistValueMap = getControllingAndDependentValues(objectNameNew, recordTypeId, fieldNameNew);
        }
        return picklistValueMap;
    }
    
    
    public static Map<String, List<String>> getControllingAndDependentValues(String objectType, String recordTypeId, String fieldName) {
      /*  String restUrl = 'callout:UI_API/services/data/v45.0/limits';
        Http h = new Http();
        HttpRequest reques = new HttpRequest();
        reques.setEndpoint(restUrl);
        reques.setMethod('GET');
        HttpResponse resp = h.send(reques);*/
        
        
        //EndPoint
        String endpoint = URL.getSalesforceBaseUrl().toExternalForm();
        //endpoint += 'callout:UI_API/services/data/v45.0';
        endpoint += '/services/data/v45.0';
        endpoint += '/ui-api/object-info/'+objectType+'/picklist-values/'+recordTypeId+'/'+fieldName;
        EncodingUtil.urlEncode(endpoint,'UTF-8');
        
        //HTTP Request send
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'OAuth ' + getSessionIdFromVFPage(Page.SessionId)); 
        req.setHeader('Accept', 'application/json '); 
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        Map<String,String> result = new Map<String,String>();
        Map<String,List<String>> mapControllingWithDependentList = new Map<String,List<String>>();
        Map<Object,String> mapControllingValueWithIndex = new Map<Object,String>();
        Map<String,List<String>> mapPicklistValues = new Map<String,List<String>>();
        
        //Parse response
        /*if(res.getStatusCode() == 302){
            req.setEndpoint(res.getHeader('Location'));
            System.debug('res.getHeader>>>'+res.getHeader('Location'));
            res = new Http().send(req);
        }*/
        
        if( res.getStatusCode() == 200){
            
            Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            // Get all the controlling values from response 
            if(root.containsKey('controllerValues')){
                Map<String, Object> controllingValues = (Map<String, Object>)root.get('controllerValues');
                // Map of all the controlling values with their index 
                for(String cValue: controllingValues.keySet()){
                    mapControllingValueWithIndex.put(controllingValues.get(cValue),cValue);
                }
            }
            
            if(!root.containsKey('values')){ 
                return mapControllingWithDependentList; 
            }
            
            //Get all the dependent values from the response returned with the Validfor attribute
            //Each bit in the bitmap indicates whether this dependent picklist value is "valid for" a corresponding controlling field value
            //The value in the validFor member is a Base64-encoded bitmap. 
            List<Object> pValues = (List<Object>)root.get('values');
            for(Object pValue : pValues){
                Map<String,Object> pValueMap = (Map<String,Object>)pValue;
                result.put((String)pValueMap.get('value'), (String)pValueMap.get('label'));
                for(Object validfor : (List<Object>)pValueMap.get('validFor')){
                    //Map the dependent Values List with their Controlling Value 
                    if(mapControllingValueWithIndex.containsKey(validfor)){
                        if(!mapControllingWithDependentList.containsKey(mapControllingValueWithIndex.get(validfor))){
                            mapControllingWithDependentList.put(mapControllingValueWithIndex.get(validfor),new List<String>());
                        }
                        mapControllingWithDependentList.get(mapControllingValueWithIndex.get(validfor)).add((String)pValueMap.get('label'));    
                    }
                }                
            }
            
            //Map all the controlling values 
            for(String controllingFields : mapControllingValueWithIndex.Values()){
                //Map controllingFields which has no dependent values associated to it
                if(!mapPicklistValues.containsKey(controllingFields)){
                    mapPicklistValues.put(controllingFields,new List<String>());
                }
                //Map controllingFields which has dependent values associated to it
                if(mapPicklistValues.containsKey(controllingFields) && mapControllingWithDependentList.containsKey(controllingFields)){
                    mapPicklistValues.get(controllingFields).addAll(mapControllingWithDependentList.get(controllingFields));
                } 
            }            
        }else{
            System.debug('getStatus>>>'+res.getStatus()+res.getStatusCode());
            System.debug('mapPicklistValues>>>'+JSON.serializePretty(mapPicklistValues));
        }
        //Return the Map of Controlling fields with the List of Dependent fields on the basis of Record Types
        return mapPicklistValues;
    }
    
    
     public static String getSessionIdFromVFPage(PageReference visualforcePage){
            if(!Test.isRunningTest()){
            String content = visualforcePage.getContent().toString();
            Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
            e = content.indexOf('End_Of_Session_Id');
            return content.substring(s, e);
            }
            return UserInfo.getSessionId();
        }
    
}