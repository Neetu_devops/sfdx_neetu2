public with sharing class NewAssembyUtilizationReport {


	public String caName {get; set;}
	public String acName {get; set;}

	Constituent_Assembly__c curCA;
	Id CAId;
	
	public NewAssembyUtilizationReport(){
		CAId = ApexPages.currentPage().getParameters().get('id');
		try{
			curCA=[select Id, name, Attached_Aircraft__c, Attached_Aircraft__r.Name, Attached_Aircraft__r.Id from Constituent_Assembly__c where Id=:CAId limit 1];
			system.debug('curCA.Attached_Aircraft__c ' + curCA.Attached_Aircraft__c);
			caName=curCA.Name;
			acName=curCA.Attached_Aircraft__r.Name;
		}catch(exception e){
			system.debug('Error while getting parent aircraft.');
		}
	}


	
	
	public Date monthEnding;
	public string getMonthEnding(){
		return Date.today().toStartOfMonth().addDays(-1).format();
//		return DateTime.newInstance( Date.today().toStartOfMonth().addDays(-1), Time.newInstance(0, 0, 0, 0) ).format('MM/dd/y');
	}
	public void setmonthEnding(Date d){
		monthEnding=d;
	}
	public Decimal FC {get; set;}
	private Decimal dFH;
	public String FH;
	
	public Decimal getFH(){
		return dFH;
	}
	public void setFH(string strFH){
		try{
				dFH=decimal.valueOf(strFH.trim());
		}catch(exception e){
			try{
				string[] FHParts = strFH.trim().split(':',0);
//				dFH=(decimal.valueOf((FHParts[1]+'0000').left(4))/6000).setscale(2);
				dFH=(decimal.valueOf(FHParts[1])/60).setscale(2);
				if(dFH>=1){
					throw new LeasewareException('Invalid value ' + strFH);
				}
				dFH+=decimal.valueOf(FHParts[0]);
			}catch(exception e2){
				dFH=0;
				system.debug('Can\'t convert FH ' + strFH + ' to number. '+ e2);
				ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Flight Hours: ' + e2.getMessage()));
			}
		}
	}	
	//public Boolean Assumed {get; set;}
	//public Boolean TrueUpGross {get; set;}
	public String UR_Type {get; set;}
	 public List<SelectOption> getTypeOptions(){
	           List<SelectOption> options = new List<SelectOption>();
	           //options.add(new SelectOption('None','--None--'));
	           Schema.DescribeFieldResult fieldResult = Utilization_Report__c.Type__c.getDescribe();
	           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	           for(Schema.PicklistEntry p : ple)
	               options.add(new SelectOption(p.getValue(), p.getValue())); 
	               
	               
	           UR_Type = 'Actual';     
	           return options;
	     }
   
	public PageReference SaveURLI()
	{
		system.debug('FC, FH, date :' + FC + ', ' + FH + ', ' + monthEnding);
		system.debug('curCA. ' + curCA);
		system.debug('curCA.Attached_Aircraft__c ' + curCA.Attached_Aircraft__c);
		system.debug('curCA.Attached_Aircraft__r.Id ' + curCA.Attached_Aircraft__r.Id);
		
		if (ApexPages.hasMessages(ApexPages.Severity.ERROR))return null;
		
		try{
			utilization_report__c newUR =new utilization_report__c(Name= 'x', Aircraft__c=curCA.Attached_Aircraft__r.Id, FH_String__c=dFH.toPlainString(), 
	        			Airframe_Cycles_Landing_During_Month__c=FC, Month_Ending__c=monthEnding, Status__c='Open' , Type__c = UR_Type);
			insert newUR;
			
			Id newUrliId;
			Utilization_Report_List_Item__c[] newURLis = [select Id, Constituent_Assembly__c, Running_Hours_During_Month__c, Cycles_During_Month__c, Report_Item__c
			 from Utilization_Report_List_Item__c where Utilization_Report__c = :newUR.id];
			List<Utilization_Report_List_Item__c> listUrlisToUpdate = new List<Utilization_Report_List_Item__c>();   
			for(Utilization_Report_List_Item__c curUrli: newURLis){
				if(curUrli.Constituent_Assembly__c==CAId){
					newUrliId=curUrli.Id;
				}else if(!curUrli.Report_Item__c.startsWith('Engine')){
					curUrli.Running_Hours_During_Month__c=0;
					curUrli.Cycles_During_Month__c=0;
					listUrlisToUpdate.add(curUrli);
				}
			}
			update listUrlisToUpdate;
			
			return New PageReference('/' +  newUrliId);
		}catch(exception e){
//			ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info,  e.getMessage()));
			system.debug('Unexpected exception - ' + e.getMessage());
		}
		return ApexPages.currentPage();
	}
	public PageReference Cancel()
	{
		return New PageReference('/' + CAId); 
	}
}