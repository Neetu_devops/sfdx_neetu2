public with sharing class ForecastSOQLServices {
    
     public static Aircraft__c getAsset(Id RecordId){
        return [SELECT Id, MSN_Number__c, Name, Lease__c, Utilization_Current_As_Of__c, Lease__r.Name, Lease__r.Lease_End_Date_New__c,Lease__r.Lease_Start_Date_New__c, Lease__r.Active__c FROM Aircraft__c WHERE ID =: RecordId];
    }
    
	//LW_AKG: Use lease__c instead of asset__r.lease__c going forward.
    public static Scenario_Input__c getScenarioInput(Id RecordId){
        return [SELECT Id, Name,NPV_Method__c, Asset__c,Asset__r.MSN_Number__c,NPV__c,IRR__c,Type__c,Forecast_Status__c,Lease__r.Name, Lease__c, Lease__r.Return_Condition__c, Lease__r.Lease_End_Date_New__c,Lease__r.Lease_Start_Date_New__c, Asset__r.Name, Number_Of_Months_For_Projection__c,UF_Fee__c,Cost_Assumption__c,
                Debt__c, Operating_Environment_Val__c, Net_EOLA_Payment__c,Ext_Base_Rent__c,Extension_Assumption__c, Number_of_Months_Of_History_To_Use__c,Interest_Rate__c,Balloon__c,Discount_Rate__c, 
                Ratio_Table__r.Name,Ext_Reserve_Type__c, Base_Rent__c, Estimated_Residual_Value__c,Reserve_Type__c,Residual_Value__c,
                RC_Type__c,Investment_Required_Purchase_Price__c,Rent_Escalation_Month__c,EOLA__c,
                Ascend__c, Avitas__c, IBA__c,historyMRCheck__c, Other__c, Scenario_Number__c,Reserve_Type_Switch_Date__c,Asset__r.Utilization_Current_As_Of__c,Total_FH_CF__c FROM Scenario_Input__c WHERE ID =: RecordId];
    }
    
    public static List<Scenario_Component_Input__c> getScenarioCmpInput(Id scenarioInp){
        List<Scenario_Component_Input__c> lstSecnarioCmp  = [SELECT Id, Name, FH__c, FC__c, User_Defined_Utilization_FH__c, Scenario_Input__c, Scenario_Input__r.Extension_Assumption__c, 
                                                             Operating_Environment_Impact__c,Type__c,Merge_Event__c, Merge_Span__c,Forecast_Type__c,Re_Delivery_Period_MOS__c,Build_Standard__c FROM Scenario_Component_Input__c 
                                                             WHERE Scenario_Input__c =: scenarioInp ORDER BY Type__c];
        return lstSecnarioCmp;
    }
    
    public static List<Fx_Assembly_Event_Input__c> getAssemblyEventInput(List<Scenario_Component_Input__c> lstSecnarioCmp){
        List<Fx_Assembly_Event_Input__c> lstAssemblyEvent = [SELECT Id,FH__c, FC__c, Name,Interval_1_Hours__c,Event_cost__c,Event_Type__c, Assembly__c,Assembly__r.Type__c,Fx_Assembly_Input__r.Name,
                                                             Interval_2_Cycles__c,AVG_FH__c,AVG_FC__c,Fx_Assembly_Input__r.Forecast_Type__c, Fx_Assembly_Input__c,Lessor_Contribution__c,
                                                             Interval_2_Hours__c, Interval_1_Cycles__c, Interval_1_Months__c,Last_Escalation_Date__c,Applicability__c,
                                                             Current_MRR__c, Base_MRR__c,Interval_2_Months__c, RC_FC__c, RC_FH__c, RC_Months__c,
                                                             Annual_Escalation__c, Starting_MR_Balance__c, From_Date__c, To_Date__c,Ratio_Table__c,Ratio_Table__r.Name,Derate__c,Downtime_Months__c,Event_Cost_Escalation__c,Current_Catalog_Year__c  FROM Fx_Assembly_Event_Input__c 
                                                             WHERE Fx_Assembly_Input__c IN : lstSecnarioCmp];
        return lstAssemblyEvent;
    }
    
    public static List<Constituent_Assembly__c> getConstituentAssembly(Id aircraftId){
        Set<String> setRecordType = new Set<String>{'Engine','Landing Gear', 'APU', 'Airframe','Propeller'};
            List<Constituent_Assembly__c> assemblyLst = [SELECT ID, RecordType.Name, Derate__c, Latest_Utilization_Date__c,
                                                         Average_Monthly_Utilization_Cycles__c, Average_Utilization__c, Attached_Aircraft__c,  
                                                         Type__c, TSN__c , CSN__c,  FH_FC__c,  Lease_ID__c,
                                                         Lowest_LLP_Limiter_Current_TS__c, Lowest_Limiter_LLP__r.Name, Re_Delivery_Period_MOS__c,Build_Standard__c
                                                         FROM Constituent_Assembly__c WHERE RecordType.Name IN : setRecordType 
                                                         AND  Attached_Aircraft__c =: aircraftId ORDER BY RecordType.Name];
        return assemblyLst;
    }
    
    public static Lease__c getLease(String leaseId){
        return [SELECT Id, Name, Reserve_Type__c,Base_Rent__c,Escalation__c,Escalation_Month__c,Lease_End_Date_New__c, Lease_Type_Status__c, //Return_Condition__r.RC_Applicability__c
                Return_Condition__c,(Select Id,Escalation_Month__c from Stepped_Rents__r order by Start_Date__c LIMIT 1) FROM Lease__c WHERE ID =: leaseId LIMIT 1];
    }
    
    public static List<Assembly_Event_Info__c> getAssemblyEventInfo(Set<Id> setAssemblies){
        return [SELECT Id, Event_Cost__c, Event_Type__c, Assembly_Type__c, Constituent_Assembly__r.RecordType.Name, 
                Constituent_Assembly__c, Interval_2_Cycles__c, Interval_1_Hours__c, Interval_1_Cycles__c, Interval_2_Hours__c,
                Interval_1_Months__c, Interval_2_Months__c, Event_Due_Cost_Override__c, Event_Due_Date_Override__c,Downtime_Months_F__c,Event_Cost_CY_F__c,Y_Hidden_Current_Catalog_Year__c,Y_Hidden_Event_Cost_Escalation__c FROM Assembly_Event_Info__c
                WHERE Constituent_Assembly__c = :setAssemblies];
    }
    
    public static List<Assembly_MR_Rate__c> getAssemblyMRrates(String leaseId){
        return [SELECT Id, Base_MRR__c,Assembly_Event_Info__r.Event_Cost__c, Assembly_Event_Info__r.Event_Cost_CY_F__c,Current_Rate_Escalated_F__c,Current_Rate_Interpolated__c,Ratio_Table__c, Ratio_Table__r.Name,
                Starting_MR_Balance__c, Annual_Escalation__c, Transactions_Total_Cash__c,Transactions_Total_Unpaid__c,Last_Escalation_Date__c,Lessor_Contribution__c,Assembly_Lkp__c,Assembly_Event_Info__c,Derate__c FROM Assembly_MR_Rate__c 
                WHERE Lease__c = : leaseId  ];
    }
    
    public static List<Scenario_Component_Input__c> getAssemblyEventInfoForecastType(Set<Id> recordId){
        return [SELECT Id, Name, FH__c, FC__c, User_Defined_Utilization_FH__c, Scenario_Input__c, Scenario_Input__r.Extension_Assumption__c, 
                Operating_Environment_Impact__c,Type__c,Merge_Event__c,Merge_Span__c,Forecast_Type__c, New_Extension_Assumptions__c 
                FROM Scenario_Component_Input__c 
                WHERE Scenario_Input__c =: recordId AND Forecast_Type__c = 'Lease Extension'];
    }
    
    public static List<Assembly_RC_Info__c> getReturnCondition(Id returnConditionId){
         return [SELECT Id,Name, RC_FC__c, Event_Type__c, RC_FH__c,RC_Months__c,Assembly_Type__c,Return_Percentage__c,RC_FC_Per__c,RC_FH_Per__c,RC_Months_Per__c,Applicability__c FROM Assembly_RC_Info__c 
                WHERE Return_Condition__c = : returnConditionId  ];
    }
    
    public static List<Scenario_Component__c> getScenarioOutput(Id scenarioInp){
        return [SELECT Id,Name ,Fx_General_Input__r.EOLA__c,TSO_Start_of_Lease__c,Rate_Basis__c, Type__c,Event_Type__c,First_Event__c,
                Assembly_Display_Name__c,Life_Limit_Interval_1_Months__c,Life_Limit_Interval_1_Hours__c,Life_Limit_Interval_1_Cycles__c,Life_Limited_Part_LLP__c,
                RC_Months__c, Life_Limit_Interval_2_Months__c,CSO_Start_of_Lease__c,Life_Limit_Interval_2_Hours__c,Assembly__c,
                RC_FC__c,Life_Limit_Interval_2_Cycles__c, RC_FH__c,MSO_Start_of_Lease__c,Fx_General_Input__r.Ext_Reserve_Type__c,RC_Applicability__c FROM Scenario_Component__c
                WHERE Fx_General_Input__c = : scenarioInp and Life_Limited_part_LLP__c = NULL  ORDER BY Type__c];
    }
    
    public static List<Monthly_Scenario__c> getMonthlyScenario( List<Scenario_Component__c> lstScenario){
        return [SELECT Id,Name,SD_F__c,Cash_Flow_F__c ,MR_OR_EOLA_AMount__c,Hours_Remaining__c,Cycles_Remaining__c,Fx_Component_Output__r.Assembly_Display_Name__c,
                Months_Remaining__c,Net_MR__c,Cost_Per_FH__c,Event_Expense_Amount__c,Fx_Component_Output__r.Cost_Per_FH__c, EOLA_FH_TSO__c ,
                Event_Date__c,RateBasis_Value__c,End_Date__c,Start_Date__c,Rent_F__c, Fx_Component_Output__r.Event_Type__c,Fx_Component_Output__r.Life_Limited_part_LLP__c,
                Fx_Component_Output__r.Fx_General_Input__r.EOLA__c,Shortfall_Month__c,Net_EOLA_Payment_Amount__c, MR_At_Return__c,
                Fx_Component_Output__r.FC__c,Return_Month__c,Return_Condition_Status__c, Fx_Component_Output__r.Rate_Basis__c,Fx_Component_Output__r.FH__c, 
                Claim_F__c,Month_Since_Overhaul__c, PR_Cost_F__c, Acc_MR_F__c,Eola_Amount__c,Lowest_Limiter_LLP_Name__c,
                MR_F__c,Fx_Component_Output__r.Name,FH__c,TSN__c,Shortfall_FC__c,Lowest_Limiter_LLP_Cycle_Remaining__c,Fx_Component_Output__r.Total_PR_Cost_F__c,
                Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c,Eola_Rate__c,
                Shortfall_FH__c, CSN__c, FC__c, Fx_Component_Output__r.Type__c,TSO__c,CSO__c,Historical_Month__c,
                Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c, FC_At_SV__c, Rent_Amount__c, MR_Amount__c,Fx_Component_Output__r.Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c FROM Monthly_Scenario__c
                WHERE Fx_Component_Output__c IN : lstScenario AND Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL 
                ORDER BY Start_Date__c,Fx_Component_Output__r.Type__c  ];
    }
    
    public static List<Monthly_Scenario__c> getMonthlyScenarioAsPerDate(Id scenarioInp,String startDate){
        Date stDate = calcStartDate(startDate);
        Integer numberOfDays = Date.daysInMonth(stDate.year(), stDate.month());
        Date endDate =  stDate.addDays(numberOfDays-1);
      
        return [SELECT Id,Name,SD_F__c ,End_Date__c,Start_Date__c, Fx_Component_Output__r.Event_Type__c,Claim_F__c,Cash_Flow_F__c,
                Fx_Component_Output__r.Fx_General_Input__r.Investment_Required_Purchase_Price__c,Eola_Amount__c,Eola_Rate__c,
                Fx_Component_Output__r.Fx_General_Input__r.Lease__r.Lease_Start_Date_New__c,Rent_F__c,MR_F__c,PR_Cost_F__c,
                Fx_Component_Output__r.Name,Fx_Component_Output__r.Type__c,  Fx_Component_Output__r.Fx_General_Input__r.NPV__c,
                Fx_Component_Output__r.Fx_General_Input__r.IRR__c,MR_Amount__c FROM Monthly_Scenario__c
                WHERE Fx_Component_Output__r.Fx_General_Input__c = : scenarioInp AND 
                Start_Date__c >= : stDate AND End_Date__c <= : endDate AND 
                Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL ];
    }
    
     public static List<Monthly_Scenario__c> getMonthlyScenarioRecords( Id scenarioInp){
        return [SELECT Id,Total_FH_Cumulative__c,SD_F__c,Cash_Flow_F__c , Eola_F__c ,EOLA_FH_TSO__c, MR_At_Return__c,Month_Since_Overhaul__c,CSN__c, CSO__c,Event_Date__c,Rent_F__c, Fx_Component_Output__r.Event_Type__c, Net_MR__c,Start_Date__c,Fx_Component_Output__r.Rate_Basis__c,
                Cost_Per_FH__c,Eola_Amount__c,TSO__c,Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c,Fx_Component_Output__r.Cost_Per_FH__c,Return_Month__c,Return_Condition_Status__c,MR_F__c,Event_Expense_Amount__c,
                Fx_Component_Output__r.Assembly_Display_Name__c,Fx_Component_Output__r.Name,Net_EOLA_Payment_Amount__c,FH__c,FC__c,FC_At_SV__c,FH_At_SV__c,Current_Rate_Escalated__c,MR_Amount__c,Fx_Component_Output__r.Fx_General_Input__r.EOLA__c,
                Fx_Component_Output__r.Type__c,AVG_FH__c,Ratio_Since_last_PR__c,AVG_FC__c FROM Monthly_Scenario__c WHERE Fx_Component_Output__r.Fx_General_Input__c = : scenarioInp  
                AND Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL ORDER BY Fx_Component_Output__r.Type__c, event_date__c];
    }
    
     public static List<Monthly_Scenario__c> getMonthlyScenarioForBarChart( Id scenarioInp){
        return [SELECT Id,Event_Date__c, Fx_Component_Output__r.Event_Type__c,Start_Date__c,Fx_Component_Output__r.Type__c,
                Fx_Component_Output__r.Name,Lessor_Contribution_Utilized__c,Airline_Contribution__c,MR_Contribution__c
                FROM Monthly_Scenario__c WHERE Fx_Component_Output__r.Fx_General_Input__c = : scenarioInp  
                AND Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL AND Event_Date__c != NULL ORDER BY Start_Date__c,Fx_Component_Output__r.Type__c ];
    }
    
    public static List<Monthly_Scenario__c> getMonthlyScenarioForBarChartTable( Id scenarioInp, String startDate){
        Date stDate = calcStartDate(startDate);
        Integer numberOfDays = Date.daysInMonth(stDate.year(), stDate.month());
        Date endDate =  stDate.addDays(numberOfDays-1);
        
        return [SELECT Id,Event_Date__c, Fx_Component_Output__r.Event_Type__c,Start_Date__c,Fx_Component_Output__r.Type__c,
                Fx_Component_Output__r.Name,Month_Since_Last_Event__c ,AVG_FC__c,AVG_FH__c,Event_Reason__c,
                Month_Since_Next_Overhaul_F__c, Event_Expense_Amount__c, Fx_Component_Output__r.Assembly_Event_Type__c, Fx_Component_Output__r.Assembly_Display_Name__c
                FROM Monthly_Scenario__c WHERE Fx_Component_Output__r.Fx_General_Input__c = : scenarioInp AND 
                Start_Date__c >= : stDate AND End_Date__c <= : endDate AND 
                Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL AND 
                Event_Date__c != NULL ORDER BY Start_Date__c,Fx_Component_Output__r.Type__c ];
    }
    
    private static Date calcStartDate(String startDate){
        Map <String, Integer> monthNames = new Map <String, Integer> {'Jan'=>1, 'Feb'=>2, 'Mar'=>3, 'Apr'=>4, 'May'=>5, 'Jun'=>6, 'Jul'=>7, 'Aug'=>8, 'Sep'=>9, 'Oct'=>10, 'Nov'=>11, 'Dec'=>12};
        String sDate = '01 '+startDate;
        String[] data = sDate.split(' ');
        Date stDate =  Date.newInstance(Integer.valueOf(data[2]),monthNames.get(data[1]),Integer.valueOf(data[0]));
        return stDate;
    }
       
}