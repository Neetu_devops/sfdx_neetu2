public class LeaseWorksLicense {

	public enum Modules {ASSET, MATCH, BOTH}
	public enum TravelClass {Economy, Business, First}

	private static map<string, integer> mapHexVals = new map<string, integer>{'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15};
	private static map<integer, string> mapHexChars = new map<integer, string>{10 => 'a', 11 => 'b', 12 => 'c', 13 => 'd', 14 => 'e', 15 => 'f'};

	private static integer getHexVal(string hexChar){
		integer intVal;

		try{
			intVal=integer.valueOf(hexChar);
		}catch(exception e){
			try{
				if(mapHexVals.containsKey(hexChar.toLowerCase()))
					intVal=mapHexVals.get(hexChar.toLowerCase());
				else{
					system.debug('Invalid hex char: ' + hexChar);
					intVal=0;
				}
			}catch(exception e2){
				system.debug('Invalid hex char: ' + hexChar);
				intVal=0;
			}
			
		}
		return intVal;
	}

	private static integer Char2Dec(String charX){
		string strHex;
		integer nDec=0;
		try{
			strHex=EncodingUtil.convertToHex(Blob.valueOf(charX));
			integer nLength=strHex.length();
			for(integer i=nLength-1;i>=0;i--){
				String curChar=strHex.substring(i,i+1);
				nDec+=(integer.valueOf(math.pow(16,nLength-i-1))*getHexVal(curChar));
			}
		}catch(exception e){
				system.debug('Invalid hex char: ' + charX);
				nDec=0;
		}
		
		return nDec;
	}
	
	private static String Dec2Hex(integer intX){
		string strHex='';
		while(intx>0){
			integer intRem = math.mod(intX,16);
			strHex=(intRem<10?string.ValueOf(intRem):mapHexChars.get(intRem))+strHex;
			intX/=16; 
		}
		return strHex;
	}

	private static String GenerateLicense(String OrgID, Modules Mdls, Integer nAircraft, TravelClass tc){
		String strLicense='';

//		system.debug('Org Id ' + OrgID);

		integer key = 184102;
		key*=integer.valueOf(math.pow(nAircraft,4)*math.pow((Mdls.ordinal()+1), 3)*math.pow((tc.ordinal()+1),7));
//		system.debug('Key - ' + KEY);
		for(integer i=OrgID.length()-1;i>=0;i--){
			String curChar=OrgID.substring(i,i+1);
			Integer curCharNum = Char2Dec(curChar);
			curCharNum*=curCharNum;
			key*=((i+1)*(i+2));
			curCharNum^=key;
			if(curCharNum<0)curCharNum*=-1;
			curCharNum=47+math.mod(curCharNum, 74);			
			strLicense+=EncodingUtil.convertFromHex(Dec2Hex(curCharNum)).toString();
		}
//		system.debug('strLicense ' + strLicense);
		return strLicense;
	} 
	
	private static String GenerateLicense(Modules Mdls, Integer nAircraft, TravelClass tc){
		return GenerateLicense(UserInfo.getOrganizationId().left(15),  Mdls, nAircraft, tc);
	} 

	public static boolean isLicenseValid(string strLicense, Modules Mdls, Integer nAircraft, TravelClass tc){
		return strLicense.equals(GenerateLicense(Mdls, nAircraft, tc));
	}

}