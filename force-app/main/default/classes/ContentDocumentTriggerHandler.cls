public class ContentDocumentTriggerHandler implements ITrigger{
    private final string triggerBefore = 'ContentDocumentTriggerHandler';
    private final string triggerAfter = 'ContentDocumentTriggerHandler';
    private static Set<Id> linkedEntityIdSet = new Set<Id>();
    public ContentDocumentTriggerHandler (){}
    
    /**
* bulkBefore
*
* This method is called prior to execution of a BEFORE trigger. Use this to cache
* any data required into maps prior execution of the trigger.
*/
    public void bulkBefore(){}
    
    public void bulkAfter(){}
    
    
    public void beforeInsert(){
        system.debug('ContentDocument Before Insert.............');
    }
    
    public void beforeUpdate(){
    }
    
    /**
* beforeDelete
*
* This method is called iteratively for each record to be deleted during a BEFORE
* trigger.
*/
    public void beforeDelete(){
        system.debug('Before Delete-------------');
        try{
        String updateLatestFile = LeaseWareUtils.getLWSetup_CS('UPDATE_LATEST_FILE_FIELDS');
        system.debug('updateLatestFile---'+ updateLatestFile);
        if(String.isEmpty(updateLatestFile) || !updateLatestFile.equals('ON') )return;
        system.debug('updateLatestFile Fields ON--------');
        List<ContentDocumentLink> cdLinkList = [select Id,LinkedEntityId,ContentDocumentId from ContentDocumentLink where ContentDocumentId IN:trigger.oldMap.keySet() ];
        system.debug('cdLinkList-----'+cdLinkList);
        for(ContentDocumentLink documentLink : cdLinkList){
                
                if(String.valueOf(documentLink.LinkedEntityId.getsobjecttype()) ==leasewareutils.getNamespacePrefix()+ 'Aircraft__c' ||
                   String.valueOf(documentLink.LinkedEntityId.getsobjecttype())==leasewareutils.getNamespacePrefix()+ 'Lease__c' ||
                   String.valueOf(documentLink.LinkedEntityId.getsobjecttype()) ==leasewareutils.getNamespacePrefix()+ 'Marketing_Activity__c'){
                       linkedEntityIdSet.add(documentLink.LinkedEntityId);
                   }
                
        }
        system.debug('linkedEntityIdSet----'+linkedEntityIdSet);
        
            
        }catch(Exception e){
            system.debug('Error ----' + e.getMessage());
            Leasewareutils.createExceptionLog(e, 'Exception Occured while Deleting File' + e.getMessage(), null, null, 'Delete', true);
        }
        
        
    }
    
    public void afterInsert(){
        system.debug('ContentDocumentTriggerHandler.afterInsert(+)');
        system.debug('ContentDocumentLinkTriggerHandler.afterInsert(-)');    
    }
    
    public void afterUpdate(){
        
    }
    
    public void afterDelete(){
        try{
        system.debug('after Delete------' + linkedEntityIdSet);
        
        if( linkedEntityIdSet.size()>0){
            
            map<Id,ContentDocumentLink> mapLinkEntIdCDLinkId = new  map<Id,ContentDocumentLink>();
            List<ContentDocumentLink> cdLinkListToUpdate = new List<ContentDocumentLink>();
            List<Id> acIdsToUpdate = new List<Id>();
            List<Id> leaseIdsToUpdate = new List<Id>();
            List<Id> dealIdsToUpdate= new List<Id>();
            //for the linked entity Id getthe latest Contentdocument link
            for(ContentDocumentLink cdLink : [select Id,LinkedEntityId,ContentDocumentId from ContentDocumentLink where LinkedEntityId IN:linkedEntityIdSet order by SystemModstamp desc] ){
                if(!mapLinkEntIdCDLinkId.containsKey(cdLink.LinkedEntityId)){
                    mapLinkEntIdCDLinkId.put(cdLink.LinkedEntityId,cdLink);
                }
                
            }
            system.debug('mapLinkEntIdCDLinkId-----'+ mapLinkEntIdCDLinkId);
            // for the LinkedEntityId, get the next latest file if not exists, update with null
            for(Id linkId : linkedEntityIdSet){
                system.debug('linkId----'+linkId);
                if(mapLinkEntIdCDLinkId.containsKey(linkId)){
                    cdLinkListToUpdate.add(mapLinkEntIdCDLinkId.get(linkId));
                }else{
                    //update with null as no contntdocument link is found
                    system.debug('update with null values-----');
                    if((String.valueOf(linkId.getsobjecttype())) == leasewareUtils.getNamespacePrefix() + 'Aircraft__c'){
                        acIdsToUpdate.add(linkId);
                    }else if ((String.valueOf(linkId.getsobjecttype())) == leasewareUtils.getNamespacePrefix() + 'Lease__c'){
                        leaseIdsToUpdate.add(linkId);
                    }else if((String.valueOf(linkId.getsobjecttype())) == leasewareUtils.getNamespacePrefix() + 'Marketing_Activity__c'){
                        dealIdsToUpdate.add(linkId);
                    }
                    
                }
                
            }
            system.debug('cdLinkListToUpdate-----'+cdLinkListToUpdate);
           
                if(acIdsToUpdate.size()>0){
                    List<Aircraft__c> acList = [select Id,Name from Aircraft__c where ID IN : acIdsToUpdate];
                    for(Aircraft__c ac : acList){
                        ac.Latest_File_Updated_Date__c = null;
                        ac.Latest_File_Updated_By__c = null;
                        ac.Y_Hidden_Latest_File_Doc_Id__c = null;
                    } 
                    LeasewareUtils.TriggerDisabledFlag = true;  
                    update acList; 
                    LeasewareUtils.TriggerDisabledFlag = false;
                }
                if(leaseIdsToUpdate.size()>0){
                    List<Lease__c> leaseList = [select Id,Name from Lease__c where ID IN : leaseIdsToUpdate];
                    for(Lease__c lease : leaseList){
                        lease.Latest_Lease_Summary_Updated_Date__c = null;
                        lease.Latest_Lease_Summary_Updated_By__c = null;
                        lease.Y_Hidden_Latest_File_Doc_Id__c = null;
                    }
                    LeasewareUtils.TriggerDisabledFlag = true;  
                    update leaseList;
                    LeasewareUtils.TriggerDisabledFlag = false;
                }
                if(dealIdsToUpdate.size()>0){
                    List<Marketing_Activity__c> dealList =  [select Id,Latest_LOI_Marketing_Summary__c,
                                                             Latest_LOI_Summary_Updated_By__c,Latest_LOI_Summary_Updated_Date__c from Marketing_Activity__c where ID IN :dealIdsToUpdate];
                    for(Marketing_Activity__c deal: dealList){
                        
                        deal.Latest_LOI_Summary_Updated_Date__c=null;
                        deal.Latest_LOI_Summary_Updated_By__c =null;
                        deal.Y_Hidden_Latest_File_Doc_Id__c = null;
                    }
                    LeasewareUtils.TriggerDisabledFlag = true;  
                    update dealList;
                    LeasewareUtils.TriggerDisabledFlag = false;
                }
                
                if (cdLinkListToUpdate.size()>0){
                system.debug('Update Next Latest.....');
                Leasewareutils.updateLatestUrlOnParent(cdLinkListToUpdate);
                
                }

        }
        }catch(Exception e){
            system.debug('Error ----' + e.getMessage());
            Leasewareutils.createExceptionLog(e, 'Exception Occured while Deleting File' + e.getMessage(), null, null, 'Delete', true);
        }
    }
    
    public void afterUnDelete(){
    }
    
    public void andFinally(){
    }
    
    
}