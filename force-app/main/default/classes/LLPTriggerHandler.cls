public class LLPTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'LLPTriggerHandlerBefore';
    private final string triggerAfter = 'LLPTriggerHandlerAfter';
    public LLPTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('LLPTriggerHandler.beforeInsert(+)');
		checkDuplicatePartNumber(); 
	    system.debug('LLPTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('LLPTriggerHandler.beforeUpdate(+)');

    	checkDuplicatePartNumber(); 
    	system.debug('LLPTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
    	system.debug('LLPTriggerHandler.beforeDelete(+)');
    	
   	
    	system.debug('LLPTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LLPTriggerHandler.afterInsert(+)');
    	Sub_Components_LLPs__c[] listNew = (list<Sub_Components_LLPs__c>)trigger.new ;
    	boolean TSImpactFlag =false;
    	for(Sub_Components_LLPs__c curLLP : listNew){
    		if(curLLP.Current_Thrust_Setting__c!=null) TSImpactFlag=true;
    	}
    	if(TSImpactFlag && !LeaseWareUtils.isFromTrigger('CalledFromLLPDiskSheet')) InsUpdTSRec();
        LLPBeforeUpdSetNBV();
    	system.debug('LLPTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('LLPTriggerHandler.afterUpdate(+)');
    	Sub_Components_LLPs__c[] listNew = (list<Sub_Components_LLPs__c>)trigger.new ;
    	boolean TSImpactFlag =false;
    	Sub_Components_LLPs__c Oldrec;
    	for(Sub_Components_LLPs__c curLLP : listNew){
    		Oldrec = (Sub_Components_LLPs__c)trigger.OldMap.get(curLLP.Id);
    		if(curLLP.Current_Thrust_Setting__c!=null && (curLLP.Override_Life_Limit_Cycle__c!=Oldrec.Override_Life_Limit_Cycle__c || curLLP.Override_Cycles_Used__c!=Oldrec.Override_Cycles_Used__c || curLLP.Y_Hidden_Thrust_Id__c!=Oldrec.Y_Hidden_Thrust_Id__c)) TSImpactFlag=true;
    	}
    	if(TSImpactFlag) InsUpdTSRec();

		LLPBeforeUpdSetNBV();
    	
    	system.debug('LLPTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	system.debug('LLPTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('LLPTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	system.debug('LLPTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('LLPTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	//after update, after insert
	private void LLPBeforeUpdSetNBV(){
		system.debug('LLPBeforeUpdSetNBV+');
		Sub_Components_LLPs__c[] listNew = (list<Sub_Components_LLPs__c>)trigger.new ;
	    // Get lowest LLP cycle and update Assembly Object .
	    set<Id> setCAIds = new set<Id>();
	    set<Id> setLLPIds = new set<Id>();
	    for(Sub_Components_LLPs__c curLLP : listNew){
	        //Lowest limiter of the CA changes only when Cycles remaining (Life_Limit_Cycles__c - CSN__c) on the LLP is updated.  
	        try{
	            Sub_Components_LLPs__c oldLLP = (Sub_Components_LLPs__c)trigger.oldmap.get(curLLP.Id);
	            //if(curLLP.Cycles_Remaining_TS__c != oldLLP.Cycles_Remaining_TS__c ) 
	            setCAIds.add(curLLP.Constituent_Assembly__c);
	            if(curLLP.Part_Number__c != oldLLP.Part_Number__c ) setLLPIds.add(curLLP.Id);
	        }catch(exception e){
	            system.debug('Exception on LLP while trying to set the lowest limiter. Forcing a recalc on the CA to be safe. - ' + curLLP.Name + ' - ' + e);
	            //This will take care of inserting new LLPs with low cycles remaining.
	            setCAIds.add(curLLP.Constituent_Assembly__c);
	        }
	    }
	    system.debug('setLLPIds='+setLLPIds.size());
		if(!setLLPIds.isEmpty()){
			//If LLP Part_Number__c is changed , update TS record. This will trigger TS.Name which is based on PartNumber.
			update [select Id from Thrust_Setting__c where Life_Limited_Part__c in :setLLPIds];
			
		}
		system.debug('setCAIds='+setCAIds);
	    if(setCAIds.size()>0  && !LeaseWareUtils.isfromFuture() && !LeaseWareUtils.isfromBatch()) LeaseWareUtils.setLowestLimiterOnCAFuture(setCAIDs, true, false, false,null); 
		system.debug('LLPBeforeUpdSetNBV-');
	}

	//Tilak
	private void checkDuplicatePartNumber(){
		Set<Id> setCAIDs=new Set<Id>();
		Sub_Components_LLPs__c[] listNew = (list<Sub_Components_LLPs__c>)trigger.new ; 
	
		//Set<string> setPNs=new Set<string>();  //set to save part number
		for (Sub_Components_LLPs__c currLLP : listNew){
			setCAIDs.add(currLLP.Constituent_Assembly__c); 
		}
	
		List<Sub_Components_LLPs__c > ExistingLLPs = [Select Serial_Number__c,Part_Number__c, Constituent_Assembly__c,Position_LLP__c from Sub_Components_LLPs__c where Constituent_Assembly__c in :setCAIDs AND Id Not in :listNew ];  
	
		Set<String> setMatchingKey=new Set<String>();
		string strKey;
		for(Sub_Components_LLPs__c existingLLP : ExistingLLPs){
			strKey = existingLLP.Constituent_Assembly__c + existingLLP.Part_Number__c + existingLLP.Serial_Number__c +(existingLLP.Position_LLP__c==null?'':''+existingLLP.Position_LLP__c);
			setMatchingKey.add(strKey);
			system.debug('setMatchingKey==========1111===='+setMatchingKey);
		}
		
		boolean errorflag=false;
		for (Sub_Components_LLPs__c currLLP : listNew) {       
			strKey = currLLP.Constituent_Assembly__c + currLLP.Part_Number__c + currLLP.Serial_Number__c +(currLLP.Position_LLP__c==null?'':''+currLLP.Position_LLP__c);  	
			if(setMatchingKey.contains(strKey))
			{
				system.debug('strKey==='+strKey);
				currLLP.addError('Duplicate LLP found with the same Partnumber and Serialnumber: PN:' + currLLP.Part_Number__c + ' SN:' + currLLP.Serial_Number__c);
				errorflag=true;
			}
			setMatchingKey.add(strKey); 
		
	  	}
	  	if(errorflag)return;
	}
	
	// call if Impacted
	//after ins,after upd
	private void InsUpdTSRec(){
		system.debug('InsUpdTS+');
		Sub_Components_LLPs__c[] listNew = (list<Sub_Components_LLPs__c>)trigger.new ;
		list<Thrust_Setting__c> insTS = new list<Thrust_Setting__c>();
		list<Thrust_Setting__c> updTS = new list<Thrust_Setting__c>();
		map<string,Thrust_Setting__c> mapTSWithLLPId = new map<string,Thrust_Setting__c>();
		
		
		
		Thrust_Setting__c[] listExistTS = [select Id,Thrust_Setting_Name__c,Life_Limited_Part__c,Cycle_Limit__c,Cycle_Used__c from Thrust_Setting__c where Life_Limited_Part__c in :listNew];
		for(Thrust_Setting__c CurRec:listExistTS){
			mapTSWithLLPId.put(CurRec.Life_Limited_Part__c +'-'+CurRec.Thrust_Setting_Name__c,CurRec);
		}
		system.debug('=======mapTSWithLLPId=='+mapTSWithLLPId.keySet());	
		Sub_Components_LLPs__c Oldrec;
		Thrust_Setting__c TempTS;
		string thrustId;
		for(Sub_Components_LLPs__c curLLP : listNew){
			
			if(curLLP.Current_Thrust_Setting__c!=null){
				if(trigger.IsInsert){
					insTS.add( new Thrust_Setting__c(Name = 'Default',Cycle_Limit__c=leasewareUtils.zeroIfNull(curLLP.Override_Life_Limit_Cycle__c),Cycle_Used__c=leasewareUtils.zeroIfNull(curLLP.Override_Cycles_Used__c)
							,Life_Limited_Part__c = curLLP.Id,Thrust_Setting_Name__c=curLLP.Y_Hidden_Current_TS_Id__c
							,Thrust__c =leasewareUtils.zeroIfNull(curLLP.Override_Life_Limit_Cycle__c)
							,Current_Thrust__c = true ));
				}
				else{// update
					Oldrec = (Sub_Components_LLPs__c)trigger.OldMap.get(curLLP.Id);
					if(curLLP.Override_Life_Limit_Cycle__c!=Oldrec.Override_Life_Limit_Cycle__c || curLLP.Override_Cycles_Used__c!=Oldrec.Override_Cycles_Used__c || curLLP.Y_Hidden_Thrust_Id__c!=Oldrec.Y_Hidden_Thrust_Id__c){
system.debug('========key Y_Hidden_Current_TS_Id__c='+ (curLLP.Id +'-'+curLLP.Y_Hidden_Current_TS_Id__c));						
						TempTS = mapTSWithLLPId.get(curLLP.Id +'-'+curLLP.Y_Hidden_Current_TS_Id__c);// check if the Thrust rec is present
						if(TempTS!=null){

							if(curLLP.Override_Life_Limit_Cycle__c!=Oldrec.Override_Life_Limit_Cycle__c){
								TempTS.Cycle_Limit__c = leasewareUtils.zeroIfNull(curLLP.Override_Life_Limit_Cycle__c);
							}
							if(curLLP.Override_Cycles_Used__c!=Oldrec.Override_Cycles_Used__c){
								TempTS.Cycle_Used__c = leasewareUtils.zeroIfNull(curLLP.Override_Cycles_Used__c);
							}
							updTS.add(TempTS);

						}else{// not Existss
							insTS.add( new Thrust_Setting__c(Name = 'Default',Cycle_Limit__c=leasewareUtils.zeroIfNull(curLLP.Override_Life_Limit_Cycle__c),Cycle_Used__c=leasewareUtils.zeroIfNull(curLLP.Override_Cycles_Used__c)
								,Life_Limited_Part__c = curLLP.Id,Thrust_Setting_Name__c=curLLP.Y_Hidden_Current_TS_Id__c
								,Thrust__c =leasewareUtils.zeroIfNull(curLLP.Override_Life_Limit_Cycle__c)
								,Current_Thrust__c = true ));							
						}
					}
				}
			}
		}
		system.debug('insTS='+insTS.size() + '=updTS='+updTS.size());
		if(!insTS.IsEmpty()) insert insTS;
		system.debug('insTS Done');

		if(!updTS.IsEmpty()){
			LeaseWareUtils.unsetTriggers('ThrustSettingTriggerHandlerBefore');
			LeaseWareUtils.unsetTriggers('ThrustSettingTriggerHandlerAfter');			
			update updTS;
		}
		system.debug('InsUpdTS-');
	}
}