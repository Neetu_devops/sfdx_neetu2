public with sharing class ShowLLPForecastTable {
    
    /**
     * 
     * @param recordId: Id of Scenario_Input__c (Cashflow)
     * @param fxMonthlyOutputId: Id of Monthly_Scenario__c (Fx Monthly Output)
     */
    @AuraEnabled
    public static LLPForcastWrapper loadInitData(Id recordId, String fxMonthlyOutputId){
        Monthly_Scenario__c msRecord = [SELECT Id, Start_Date__c, End_Date__c, Fx_Component_Output__r.LLP_Build_Standard__c, Fx_Component_Output__r.Type__c, Fx_Component_Output__r.Rate_Basis__c, CSN__c,TSN__c, Lowest_Limiter_LLP_Comp__r.Assembly__r.Serial_Number__c, Lowest_Limiter_LLP_Comp__r.Assembly__r.Engine_Model__c,Shortfall_FC__c,
                                        Hours_Remaining__c,Cycles_Remaining__c,Months_Remaining__c,RateBasis_Label__c,Month_Since_Overhaul__c,TSO__c,CSO__c,FH__c,FC__c,
                                        InterpolatedMRRate__c,Effective_Escalation_Factor_MR__c,Current_Rate_Escalated__c FROM Monthly_Scenario__c WHERE Id =: fxMonthlyOutputId];
        LLPForcastWrapper llpfWrap = new LLPForcastWrapper();
        if(msRecord != null){
            List<Monthly_Scenario__c> msRecordList = [SELECT Id, Start_Date__c, End_Date__c, Fx_Component_Output__r.Type__c, Fx_Component_Output__r.Name, FC__c, CSO__c, Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c ,Fx_Component_Output__r.Life_Limited_Part_LLP__c,
                                                      Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name, Event_Date__c,Event_Expense_Amount__c,Cycles_Remaining__c,Fx_Component_Output__r.Assembly__r.Name,Fx_Component_Output__r.Life_Limited_Part_LLP__r.Part_Number__c,Shortfall_FC__c,
                                                      Fx_Component_Output__r.RC_FC__c
                                                      FROM Monthly_Scenario__c
                                                      WHERE Fx_Component_Output__r.Fx_General_Input__c = : recordId
                                                      AND Start_Date__c >= : msRecord.Start_Date__c
                                                      AND End_Date__c <= : msRecord.End_Date__c
                                                      AND Fx_Component_Output__r.Life_Limited_part_LLP__c != NULL
                                                      AND Fx_Component_Output__r.Type__c =: msRecord.Fx_Component_Output__r.Type__c
                                                      ORDER BY Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name,Start_Date__c,Fx_Component_Output__r.Type__c];
            System.debug('msRecordList  : ------'+msRecord);
            
            llpfWrap.FH = LeaseWareUtils.zeroIfNull(msRecord.FH__c);
            llpfWrap.FC = LeaseWareUtils.zeroIfNull(msRecord.FC__c);

            msRecord.FH__c = LeaseWareUtils.zeroIfNull(msRecord.FH__c);
            
            if(msRecord.FC__c != null && msRecord.FC__c != 0.0){
                llpfWrap.FHDivideFC = msRecord.FH__c/msRecord.FC__c;
            }
            
            
            
            if(msRecord.InterpolatedMRRate__c!=null){
                llpfWrap.mrRate = msRecord.InterpolatedMRRate__c * LeaseWareUtils.zeroIfNull(msRecord.Effective_Escalation_Factor_MR__c);
            }else{
                llpfWrap.mrRate = LeaseWareUtils.zeroIfNull(msRecord.Current_Rate_Escalated__c);	 
            }
            
            
            
            
                      
            if(msRecord.Fx_Component_Output__r.LLP_Build_Standard__c != null){
                llpfWrap.buildStandard = msRecord.Fx_Component_Output__r.LLP_Build_Standard__c;
            }
            if(msRecord.Start_Date__c != null){
                llpfWrap.msMonth = fetchMonth(msRecord.Start_Date__c);
            }
            if(msRecord.CSN__c != null){
                llpfWrap.CSN = msRecord.CSN__c;
            }
            if(msRecord.TSN__c != null){
                llpfWrap.TSN = msRecord.TSN__c;
            }
            if(msRecord.Lowest_Limiter_LLP_Comp__r.Assembly__r.Serial_Number__c != null){
                llpfWrap.serialno = msRecord.Lowest_Limiter_LLP_Comp__r.Assembly__r.Serial_Number__c;
            }
            if(msRecord.Lowest_Limiter_LLP_Comp__r.Assembly__r.Engine_Model__c != null){
                llpfWrap.engineType = msRecord.Lowest_Limiter_LLP_Comp__r.Assembly__r.Engine_Model__c;
            }
            if(!msRecordList.isEmpty()){
                for(Monthly_Scenario__c ms : msRecordList){
                    llpfWrap.msRecordList.add(new MonthlyScenarioWrapper(ms));
                }
            }
        }
        return llpfWrap;
    }
    
    public static String fetchMonth(Date startData){
        Map <Integer, String> monthNames = new Map <Integer, String> {1=>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May', 6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec'};
            return monthNames.get(startData.month())+' '+startData.year();
    }
    
    public class LLPForcastWrapper {
        @AuraEnabled public List<MonthlyScenarioWrapper> msRecordList {get;set;}
        @AuraEnabled public String msMonth {get;set;}
        @AuraEnabled public Decimal CSN {get;set;}
        @AuraEnabled public Decimal TSN {get;set;}
        @AuraEnabled public String serialno {get;set;}
        @AuraEnabled public String engineType {get;set;}
        @AuraEnabled public Decimal buildStandard{get;set;}
        @AuraEnabled public Decimal FH{get;set;}
        @AuraEnabled public Decimal FC{get;set;}
        @AuraEnabled public Decimal mrRate{get;set;}
        @AuraEnabled public Decimal FHDivideFC{get;set;}
        
        public LLPForcastWrapper(){
            this.msRecordList = new List<MonthlyScenarioWrapper>();
            this.msMonth = '';
            this.serialno = '';
            this.engineType = '';
        }
    }
    public class MonthlyScenarioWrapper{
        @AuraEnabled public String llpName {get;set;}
        @AuraEnabled public String llpId{get;set;}
        @AuraEnabled public String partNumber {get;set;}
        @AuraEnabled public Decimal lifeLimit {get;set;}
        @AuraEnabled public Decimal cycleUsed {get;set;}
        @AuraEnabled public Decimal cycleRemaining {get;set;}
        @AuraEnabled public Date isReplaced {get;set;}
        @AuraEnabled public Decimal replacementCost {get;set;}
        @AuraEnabled public Decimal Shortfall_FC {get;set;}
        @AuraEnabled public Decimal reDeliveryCondition {get;set;}
        
        
        public MonthlyScenarioWrapper(Monthly_Scenario__c msRecord){
            this.llpName = msRecord.Fx_Component_Output__r.Life_Limited_Part_LLP__r.Name;
            this.llpId = msRecord.Fx_Component_Output__r.Life_Limited_Part_LLP__c;
            this.partNumber = msRecord.Fx_Component_Output__r.Life_Limited_Part_LLP__r.Part_Number__c;
            this.lifeLimit = msRecord.Fx_Component_Output__r.Life_Limit_Interval_1_Cycles__c;
            this.cycleUsed = msRecord.CSO__c;
            this.Shortfall_FC = msRecord.Shortfall_FC__c;
            this.cycleRemaining = msRecord.Cycles_Remaining__c;
            this.isReplaced = msRecord.Event_Date__c;
            this.replacementCost = msRecord.Event_Expense_Amount__c; 
            this.reDeliveryCondition=msRecord.Fx_Component_Output__r.RC_FC__c;
        }
    }
}