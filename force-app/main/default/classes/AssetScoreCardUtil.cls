public inherited sharing class AssetScoreCardUtil {

    
    public static map<String,String> getAssetScoreCardDetails(String assetId){
        map<String,String> assetScoreMap = new map<String,String>();
        Aircraft__c ac = [select Id,Name,Asset_Type_score__c, Lease_Term_Remaining_Score__c,Asset_Specification_Score__c,MR_EOLA_Score__c,
                          Asset_Overall_Score__c, Asset_Risk_Assessment_Score__c,
                          Aircraft_Type__c,Aircraft_Variant__c,Lease__r.Name,Lease__r.Remaining_Lease_Term_Mos__c,Lease__r.Current_Rent__c,
                          Lease__r.Security_Deposit__c,	
                          Lease__r.Reserve_Type__c,Lease__r.Operator__c 
                          from Aircraft__c where ID = : assetId ];
        if(ac.Asset_Type_score__c ==null || ac.Lease_Term_Remaining_Score__c==null){
            setAssetTypeAndLeaseTermScoreForAsset(ac);
        }
        
        map<String,Decimal> mapWeightage = getWeightagesForAssetScorecard();
        system.debug('mapWeightage----'+mapWeightage);
        
            if(mapWeightage.containsKey('Asset Specification')){
            assetScoreMap.put('Asset_Specification_Weightage', String.valueOf((mapWeightage.get('Asset Specification')*100).round()));
            }
            if(mapWeightage.containsKey('Asset Type')){
                assetScoreMap.put('Asset_Type_Weightage', String.valueOf((mapWeightage.get('Asset Type') *100).round()));
            }
            if(mapWeightage.containsKey('Lease Term Remaining')){
                assetScoreMap.put('Lease_Term_Remaining_Weightage', String.valueOf((mapWeightage.get('Lease Term Remaining') *100).round()));
            }
            if(mapWeightage.containsKey('MR EOLA')){
                assetScoreMap.put('MR_EOLA_Weightage', String.valueOf((mapWeightage.get('MR EOLA') *100).round()));
            }
        
        assetScoreMap.put('Asset_Type_Score',String.valueOf(ac.Asset_Type_score__c));
        assetScoreMap.put('Lease_Term_Remaining_Score',String.valueOf(ac.Lease_Term_Remaining_Score__c));
        assetScoreMap.put('Asset_Specification_Score',String.valueOf(ac.Asset_Specification_Score__c));
        assetScoreMap.put('MR_EOLA_Score',String.valueOf(ac.MR_EOLA_Score__c));
        assetScoreMap.put('Supplemental_Rent',String.valueOf(ac.Lease__r.Reserve_Type__c));
        assetScoreMap.put('Asset_Risk_Assessment_Score__c',String.valueOf(ac.Asset_Risk_Assessment_Score__c));
        assetScoreMap.put('Asset_Overall_Score__c',String.valueOf(ac.Asset_Overall_Score__c));
        assetScoreMap.put('Current_Rent_Amount',String.valueOf(ac.Lease__r.Current_Rent__c));
        assetScoreMap.put('Security_Deposit',String.valueOf(ac.Lease__r.Security_Deposit__c));

        system.debug('assetScoreMap----'+ assetScoreMap);
        return assetScoreMap;
    }

    //Based on Score Values for given Aircraft Type and Variant from Aircraft Type Table assign Assset Type Score
    //Based on Score Values for Lease Term in Custom Look up assign Lease Term Score based on remaining lease term
    public static void setAssetTypeAndLeaseTermScoreForAsset(Aircraft__c ac){
        system.debug('setAssetTypeAndLeaseTermScoreForAsset.....' + ac);
        List<Aircraft_Type__c> aircraftType = [select Id,Aircraft_Type__c,Aircraft_Variant__c,Residual_Value_Rentention__c,Value_for_Money__c,Operational_Performance__c,Remarketing_Potential__c
                                            from Aircraft_Type__c where Aircraft_Type__c = : ac.Aircraft_Type__c and 
                                            Aircraft_Variant__c =: ac.Aircraft_Variant__c limit 1];
        
      
        system.debug('aircraftType-----'+ aircraftType + aircraftType.size());
      	if(aircraftType !=null  && aircraftType.size()>0 ){
                ac.Asset_Type_Score__c = ((LeasewareUtils.zeroIfNull(aircraftType[0].Residual_Value_Rentention__c)+
                LeasewareUtils.zeroIfNull(aircraftType[0].Value_for_Money__c) + 
                LeasewareUtils.zeroIfNull(aircraftType[0].Operational_Performance__c)+
                LeasewareUtils.zeroIfNull(aircraftType[0].Remarketing_Potential__c))/4).round(System.RoundingMode.HALF_DOWN);
     	}
        if(ac.Lease__r !=null){
            
            Map<String,String>	leaseTermScoreMap = new map<String,String>();
            for(Custom_Lookup__c  curScoreRec:getAssetScoreCardLookupDetails('LEASE_TERM_REMAINING_SCORE')){
                leaseTermScoreMap.put(curScoreRec.Sub_LookupType2__c,curScoreRec.Details__c);
            }               
             ac.Lease_Term_Remaining_Score__c = getScoreForValue(ac.Lease__r.Remaining_Lease_Term_Mos__c,leaseTermScoreMap);
             system.debug('Lease Score Set----'+ ac.Lease_Term_Remaining_Score__c);
        
        }
        
        
        try{
            Leasewareutils.triggerDisabledFlag = true;
             update ac;
            Leasewareutils.triggerDisabledFlag = false;
        }catch(DMLException e){
            throw new LeasewareException(e.getMessage());
        }
        
        
       
        
            
	}


    private static Integer getScoreForValue(Decimal value,Map<String,String> leaseTermScoreMap){
        system.debug('Lease Term value----'+ value);
        system.debug('leaseTermScoreMap----'+ leaseTermScoreMap);
        if(value ==null || leaseTermScoreMap==null || leaseTermScoreMap.isEmpty())return 0;
        system.debug('Get Score For Value----' +value);
        Integer retScore;
        
         Integer score1Val = leaseTermScoreMap.containsKey('1')?Integer.valueOf(leaseTermScoreMap.get('1')):0;
         Integer score2Val = leaseTermScoreMap.containsKey('2')?Integer.valueOf(leaseTermScoreMap.get('2')):0;
         Integer score3Val = leaseTermScoreMap.containsKey('3')?Integer.valueOf(leaseTermScoreMap.get('3')):0;
         Integer score4Val = leaseTermScoreMap.containsKey('4')?Integer.valueOf(leaseTermScoreMap.get('4')):0;
         Integer score5Val = leaseTermScoreMap.containsKey('5')?Integer.valueOf(leaseTermScoreMap.get('5')):0;
         Integer score6Val = leaseTermScoreMap.containsKey('6')?Integer.valueOf(leaseTermScoreMap.get('6')):0;
        
         if(score1Val==0 || score2Val==0 ||  score3Val==0 || score4Val==0 || score5Val==0){
             system.debug('Lease Term Score Missing....');
             retScore = 0;
         }else{

        system.debug('Value----'+ value);
        if(value>score6Val && value<= score5Val){
            system.debug('Score6------'+retScore);
            retScore = 6;
        }
        else if(value>score5Val && value<=score4Val){
            system.debug('Score5------'+retScore);
            retScore = 5;
        }
        else if(value>score4Val && value<=score3Val){
            system.debug('Score4------'+retScore);
            retScore = 4;
        }
        else if(value>score3Val && value<=score2Val){
            system.debug('Score3------'+retScore);
            retScore = 3;
        }
        else if(value>score2Val && value<=score1Val){
            system.debug('Score2------'+retScore);
            retScore = 2;
        }
        else if(value>score1Val){
            system.debug('Score1------'+retScore);
            retScore = 1;
        }
    }
        
       system.debug('LeaseRemainingTermScore----'+ retScore);
        return retScore;
    }


    
        
        
    public static map<String,String> getLeaseRemainingTermScores(){
        Map<String,String>	leaseTermScoreMap = new map<String,String>();
        for(Custom_Lookup__c  curScoreRec:getAssetScoreCardLookupDetails('LEASE_TERM_REMAINING_SCORE')){
            leaseTermScoreMap.put(curScoreRec.Name,curScoreRec.Description__c);
        } 
        system.debug('leaseTermScoreMap-----' + leaseTermScoreMap);
        return   leaseTermScoreMap;                     

    }

    public static map<String,String> getThrustWeightScores(){
        Map<String,String>	assetTermScoreMap = new map<String,String>();
        for(Custom_Lookup__c  curScoreRec:getAssetScoreCardLookupDetails('ASSET_SPECIFICATION_SCORE')){
            assetTermScoreMap.put(curScoreRec.Sub_LookupType2__c+'-'+curScoreRec.Sub_LookupType3__c,curScoreRec.Details__c);
        } 
        system.debug('assetTermScoreMap-----' + assetTermScoreMap);
        return   assetTermScoreMap;                     

    }
    public static  map<String,String> getAircraftDetails(Id assetId){
        map<String,String> assetTypeDetailsMap = new map<String,String>();

        Aircraft__c ac = [select Id, Name, Aircraft_Type__c,Aircraft_Variant__c,Type_Variant__c,Lease__c, Lease__r.Name, Lease__r.Remaining_Lease_Term_Mos__c from Aircraft__c where ID =:assetId];
        assetTypeDetailsMap.put('Asset', ac.Name);
        assetTypeDetailsMap.put('Asset_Id', ac.Id);
        assetTypeDetailsMap.put('Asset_Type_and_Variant', ac.Aircraft_Type__c+'-'+ ac.Aircraft_Variant__c);
        List<Aircraft_Type__c> acType = [select Id, Name, Aircraft_Type__c,Aircraft_Variant__c,Residual_Value_Rentention__c,Remarketing_Potential__c,Operational_Performance__c,Value_for_Money__c from Aircraft_Type__c where 
                                    Aircraft_Type__c = : ac.Aircraft_Type__c and Aircraft_Variant__c = :ac.Aircraft_Variant__c limit 1];
        
        Lease__c lease = ac.Lease__r;
        if(lease !=null){
            assetTypeDetailsMap.put('Lease', lease.name);
            assetTypeDetailsMap.put('Lease_Id', ac.Lease__c);
            assetTypeDetailsMap.put('Remaining_Lease_Term_Months' ,  String.valueOf(LeaseWareutils.zeroIfNull(lease.Remaining_Lease_Term_Mos__c)));
        }
        if(acType!=null && acType.size()>0){
           
            
            assetTypeDetailsMap.put('Residual_Value_Rentention', String.valueOf(LeaseWareutils.blankIfNull(acType[0].Residual_Value_Rentention__c)));
            assetTypeDetailsMap.put('Value_for_Money',  String.valueOf(LeaseWareutils.blankIfNull(acType[0].Value_for_Money__c))); 
            assetTypeDetailsMap.put('Operational_Performance',  String.valueOf(LeaseWareutils.blankIfNull(acType[0].Operational_Performance__c)));
            assetTypeDetailsMap.put('Remarketing_Potential',  String.valueOf(LeaseWareutils.blankIfNull(acType[0].Remarketing_Potential__c)));
            
        }

        return assetTypeDetailsMap;
    }

    public static void updateDetailsToAssetRecord(map<String,String> mapValues){
        system.debug('updateDetailsToAssetRecord-----'+mapValues);
        if(mapValues!=null){
            String assetId = mapValues.get('Asset_Id');
            if(String.isNotBlank(assetId)){
                Aircraft__c ac = [select Id,Name,MR_EOLA_Score__c,Asset_Type_Score__c,Lease__r.Operator__c,
                                Lease_Term_Remaining_Score__c,Asset_Specification_Score__c,
                                Asset_Risk_Assessment_Completed_By__c
                                from Aircraft__c where Id = :assetId];

                //Add the code to Check the condition if chnaged then only update
                ac.MR_EOLA_Score__c = mapValues.containsKey('MR_EOLA_Score')?Decimal.valueOf(mapValues.get('MR_EOLA_Score')):0;
                ac.Asset_Type_Score__c =mapValues.containsKey('Asset_Type_Score')?Decimal.valueOf(mapValues.get('Asset_Type_Score')):0;
                ac.Lease_Term_Remaining_Score__c = mapValues.containsKey('Lease_Term_Remaining_Score')?Decimal.valueOf(mapValues.get('Lease_Term_Remaining_Score')):0;
                ac.Asset_Specification_Score__c = mapValues.containsKey('Asset_Specification_Score')?Decimal.valueOf(mapValues.get('Asset_Specification_Score')):0;
                calculateAssetRiskAssmntScore(ac);

            }
        }

    }



    public static void getPandemicInfoForCountry(Id countryId,map<String,String> infoMap){
        
            List<Country__c> countryList = [select Id,Name,Region__c,(select Id,Name,Active_Cases__c,Confirmed_Cases__c,Deaths_1M_Population__c,
            Data_As_Of__c,Fatality_Rate__c,New_Cases__c,New_Deaths__c,Recovered_Cases__c,Recovery_per__c,
            Total_Cases_1M_Population__c,Total_Deaths__c,Country__r.Region__c from Pandemic_Info__r 
            order by CreatedDate desc limit 1 )  from Country__c];

            map<String,Pandemic_Info__c> mapPandemicInfoForCountry= new map<String,Pandemic_Info__c>();
            map<String,List<Pandemic_Info__c>> mapPandemicInfoForRegion= new map<String,List<Pandemic_Info__c>> ();

            system.debug('CountryList---'+  countryList.size());
            List<Pandemic_Info__c> pandemicInfoList;
            for(Country__c country : countryList){
                system.debug('Pandemic Info------'+ country.Pandemic_Info__r);
                if(country.Pandemic_Info__r.size()>0){
                    mapPandemicInfoForCountry.put(country.Id, country.Pandemic_Info__r[0]);

                    if(mapPandemicInfoForRegion.containsKey(country.Region__c)){
                        pandemicInfoList = mapPandemicInfoForRegion.get(country.Region__c);
                    }else{
                        pandemicInfoList =new List<Pandemic_Info__c>();
                    }
                    pandemicInfoList.add(country.Pandemic_Info__r[0]);
                    mapPandemicInfoForRegion.put(country.Region__c,pandemicInfoList);
                }

            }
        system.debug('mapPandemicInfoForCountry------'+ mapPandemicInfoForCountry);
        system.debug('mapPandemicInfoForRegion------' + mapPandemicInfoForRegion);
        
        Pandemic_Info__c info =mapPandemicInfoForCountry.containsKey(countryId)?mapPandemicInfoForCountry.get(countryId):null;
        system.debug('pandemicInfo----'+ info);
        if(info!=null){
            infoMap.put('#_Of_Confirmed_Cases', String.valueOf(info.Confirmed_Cases__c));
            infoMap.put('#_Of_Active_Cases', String.valueOf(info.Active_Cases__c));
            infoMap.put('#_Of_New_Cases', String.valueOf(info.New_Cases__c));
            infoMap.put('Recovery_Rate', String.valueOf(info.Recovery_per__c.setScale(0)));
            infoMap.put('Fatality_Rate', String.valueOf(info.Fatality_Rate__c.setScale(0)));
            infoMap.put('#_Of_Recovered_Cases', String.valueOf(info.Recovered_Cases__c));
            infoMap.put('Total_Cases/1M_Population', String.valueOf(info.Total_Cases_1M_Population__c));
            infoMap.put('Total_Deaths', String.valueOf(info.Total_Deaths__c));
            infoMap.put('New_Deaths', String.valueOf(info.New_Deaths__c));
            infoMap.put('Deaths/1M_Population', String.valueOf(info.Deaths_1M_Population__c));
            infoMap.put('Date_As_Of', LeasewareUtils.getDatetoString(info.Data_As_Of__c,'MON DD, YYYY'));
            system.debug('Region-----'+ info.Country__r.Region__c);
            List<Pandemic_Info__c> regionPandemicList;
            if(info.Country__r.Region__c!=null){
                 regionPandemicList =mapPandemicInfoForRegion.containskey(info.Country__r.Region__c)?mapPandemicInfoForRegion.get(info.Country__r.Region__c):null;
            }
            if(regionPandemicList!=null && regionPandemicList.size()>0){
                Integer noOfCountriesForRegion = regionPandemicList.size();
                Decimal regionfatalityRate=0;
                Decimal regionRecoveryRate =0;
                system.debug('regionPandemicList-----'+ regionPandemicList.size());
                for(Pandemic_Info__c pandInfo : regionPandemicList){
                    system.debug('pandInfo------' + pandInfo);
                    if(pandInfo!=null){
                        regionfatalityRate += pandInfo.Fatality_Rate__c;
                        regionRecoveryRate += pandInfo.Recovery_per__c;
                    }
                   
                }
                infoMap.put('Region_Average_Fatality_Rate',String.valueOf((regionfatalityRate/noOfCountriesForRegion).setScale(0)));
                infoMap.put('Region_Average_Recovery_Rate',String.valueOf((regionRecoveryRate/noOfCountriesForRegion).setScale(0)));
            }
            if(mapPandemicInfoForCountry.size()>0){
                system.debug('Country Size----' +mapPandemicInfoForCountry.size());
                Decimal worldwideFatalityRate = 0;
                Decimal worldwideRecoveryRate = 0;
                for(Pandemic_Info__c worldInfo : mapPandemicInfoForCountry.values()){
                    if(worldInfo!=null){
                        system.debug('worldInfo-----'+  worldInfo);
                        worldwideFatalityRate += worldInfo.Fatality_Rate__c;
                        worldwideRecoveryRate += worldInfo.Recovery_per__c;
                        system.debug('worldwideFatalityRate-----'+  worldwideFatalityRate);
                        system.debug('worldwideRecoveryRate-----'+  worldwideRecoveryRate);
                    }
                }
                system.debug('worldwideFatalityRate----'+ worldwideFatalityRate);
                system.debug('worldwideRecoveryRate----'+ worldwideRecoveryRate);
                infoMap.put('World_Wide_Average_Fatality_Rate',String.valueOf((worldwideFatalityRate/mapPandemicInfoForCountry.size()).setScale(0)));
                infoMap.put('World_Wide_Average_Recovery_Rate',String.valueOf((worldwideRecoveryRate/mapPandemicInfoForCountry.size()).setScale(0)));
            }
            
        }
        
        system.debug('InfoMap------' + infoMap);
        
    }

    private static  map<String,Decimal> getWeightagesForAssetScorecard(){
        List<Recommendation_Factor__c> weightageList = [select Id,Name,Type__c,Weightage_Number__c  from Recommendation_Factor__c where Inactive__c=false and Type__c='Asset Scorecard'];
        map<String,Decimal> mapWeightage = new map<String,Decimal> ();
        Decimal absoluteWeightSum=0;
        if(weightageList!=null && weightageList.size()>0){
            for(Recommendation_Factor__c curRec : weightageList){
                absoluteWeightSum += LeasewareUtils.zeroIfNull(curRec.Weightage_Number__c);
            }
            system.debug('Absolute Weightage-----'+ absoluteWeightSum);
            for(Recommendation_Factor__c curRec : weightageList){
                if(absoluteWeightSum>0){
                    mapWeightage.put(curRec.Name, curRec.Weightage_Number__c/absoluteWeightSum);
                }
            }
        }
        return  mapWeightage;
    }


    private static void calculateAssetRiskAssmntScore(Aircraft__c ac){
        system.debug('calculateAssetRiskAssmntScore.....');
        map<String,Decimal> mapWeightage = getWeightagesForAssetScorecard();
        system.debug('mapWeightage------'+ mapWeightage);
        system.debug('ac.Asset_Type_Score__c======'+ac.Asset_Type_Score__c);
        system.debug('ac.Lease_Term_Remaining_Score__c======'+ac.Lease_Term_Remaining_Score__c);
        system.debug('ac.Asset_Specification_Score__c======'+ac.Asset_Specification_Score__c);
        system.debug('ac.MR_EOLA_Score__c======'+ac.MR_EOLA_Score__c);

        if(ac.Asset_Type_Score__c==null || ac.Asset_Type_Score__c==0 || ac.Lease_Term_Remaining_Score__c==null || ac.Lease_Term_Remaining_Score__c==0  || ac.Asset_Specification_Score__c ==null || ac.Asset_Specification_Score__c==0  && ac.MR_EOLA_Score__c==null|| ac.MR_EOLA_Score__c==0 ){
            ac.Asset_Risk_Assessment_Score__c = null;
        }else{
            ac.Asset_Risk_Assessment_Score__c = ac.Asset_Type_Score__c * LeasewareUtils.zeroIfNull(mapWeightage.get('Asset Type'))+
            ac.Lease_Term_Remaining_Score__c * LeasewareUtils.zeroIfNull(mapWeightage.get('Lease Term Remaining'))+
            ac.Asset_Specification_Score__c * LeasewareUtils.zeroIfNull(mapWeightage.get('Asset Specification'))+
            ac.MR_EOLA_Score__c *  LeasewareUtils.zeroIfNull(mapWeightage.get('MR EOLA'));
            ac.Asset_Risk_Assessment_Completed_Date__c = system.today();
            User currentUser = [select id,Alias, Name FROM User Where id =: userInfo.getUserId() ];
            ac.Asset_Risk_Assessment_Completed_By__c = CurrentUser.Name;
        }
        
       
       
        try{
            LeasewareUtils.triggerDisabledFlag = true;
            update ac;
            LeasewareUtils.triggerDisabledFlag = false;
       }catch(DmlException e){
            throw new LeasewareException(e.getMessage());
       }
        
        

        

    }
    public static map<String,String> getAssetScoreAndPandemicInfoDetails(String assetId){
        system.debug('getAssetScoreAndPandemicInfoDetails------');
        map<String,String> infoMap = new map<String,String>();
        
        Aircraft__c ac = [select Id,Name,Asset_Risk_Assessment_Score__c,Asset_Overall_Score__c,Asset_Risk_Assessment_Completed_By__c,
                          Asset_Risk_Assessment_Completed_Date__c,
                          Lease__r.Operator__c from Aircraft__c where ID = : assetId ];

        system.debug('ac----'+ ac);
        Decimal assetRiskAssessmentScore,assetOverallScore;
        infoMap.put('Asset_Risk_Assessment_Completed_By',ac.Asset_Risk_Assessment_Completed_By__c);
        infoMap.put('Asset_Risk_Assessment_Completed_Date',String.valueOf(ac.Asset_Risk_Assessment_Completed_Date__c));
        infoMap.put('Asset_Overall_Score',String.valueOf(ac.Asset_Overall_Score__c));
        if(ac.Asset_Risk_Assessment_Score__c!=null){
            infoMap.put('Asset_Risk_Assessment_Score',String.valueOf(ac.Asset_Risk_Assessment_Score__c));
            assetRiskAssessmentScore = ac.Asset_Risk_Assessment_Score__c;
            
        }
        
        if(ac.Lease__r.Operator__c !=null){
            Operator__c opr = [select Id, Name,Country_Lookup__c,Country_Lookup__r.Name, Country_Repossession_Risk__c,Financial_Risk_Profile__c,Government_Support1__c from Operator__c where ID = :ac.Lease__r.Operator__c];
            infoMap.put('Operator_Financial_Risk_Profile',opr.Financial_Risk_Profile__c);
            infoMap.put('Operator_Id',opr.Id);
            infoMap.put('Operator_Name',opr.Name);
            Integer financialRiskProfileScore,govtSupportScore,countryReposessionScore ;
            infoMap.put('Government_Support',opr.Government_Support1__c);
             map<String,String> scoreMap = new map<String,String>();
                for(Custom_Lookup__c cl : getAssetScoreCardLookupDetails('%')){
                    scoreMap.put(cl.Sub_LookupType__c+'_'+ cl.Name, cl.Details__c);
                }
               if(String.isNotBlank(opr.Financial_Risk_Profile__c)){
                    if(scoreMap.containsKey('FINANCIAL_RISK_PROFILE_SCORE'+'_'+opr.Financial_Risk_Profile__c)){
                        financialRiskProfileScore =  Integer.valueOf(scoreMap.get('FINANCIAL_RISK_PROFILE_SCORE'+'_'+opr.Financial_Risk_Profile__c));
                    }
                    system.debug('financialRiskProfileScore-----'+ financialRiskProfileScore);
                    
                }
               system.debug('govtSupport----'+ opr.Government_Support1__c);
               if(String.isNotBlank(opr.Government_Support1__c)){
                if(scoreMap.containsKey('GOVERNMENT_SUPPORT_SCORE'+'_'+opr.Government_Support1__c)){
                    govtSupportScore  =  Integer.valueOf(scoreMap.get('GOVERNMENT_SUPPORT_SCORE'+'_'+opr.Government_Support1__c));
                }
                 system.debug('govtSupportScore-----'+ govtSupportScore);
               }
               
    
                if(opr.Country_Lookup__c!=null){
                    
                    if(String.isNotBlank(opr.Country_Repossession_Risk__c)){
                        if(scoreMap.containsKey('COUNTRY_REPOSESSION_RISK_SCORE'+'_'+opr.Country_Repossession_Risk__c)){
                          countryReposessionScore  =  Integer.valueOf(scoreMap.get('COUNTRY_REPOSESSION_RISK_SCORE'+'_'+opr.Country_Repossession_Risk__c));
                        }
                        system.debug('countryReposessionScore-----'+ countryReposessionScore);
                    }
                    infoMap.put('Country_Id',opr.Country_Lookup__c);
                    infoMap.put('Country_Name',opr.Country_Lookup__r.Name);
                    infoMap.put('Country_Repossession_Risk',opr.Country_Repossession_Risk__c);

                    infoMap.put('Country_Repossession_Risk_Score',String.valueOf(countryReposessionScore));
                    infoMap.put('Government_Support_Score',String.valueOf(govtSupportScore));
                    infoMap.put('Financial_Risk_Profile_Score',String.valueOf(financialRiskProfileScore));
                    getPandemicInfoForCountry(opr.Country_Lookup__c,infoMap);
                }
    
                if(financialRiskProfileScore!=null && govtSupportScore!=null &&  countryReposessionScore!=null &&  assetRiskAssessmentScore!=null){
                   assetOverallScore = ((financialRiskProfileScore + govtSupportScore + countryReposessionScore + assetRiskAssessmentScore)/4).setScale(1);
                }
    
            }
            
            if(assetOverallScore != ac.Asset_Overall_Score__c){
                ac.Asset_Overall_Score__c = assetOverallScore;
                infoMap.put('Asset_Overall_Score',String.valueOf(ac.Asset_Overall_Score__c));
                system.debug('assetOverallScore---'+ assetOverallScore);
                system.debug('ac.Asset_Overall_Score__c---'+ ac.Asset_Overall_Score__c);
                try{
                    LeasewareUtils.triggerDisabledFlag = true;
                    update ac;
                    LeasewareUtils.triggerDisabledFlag = false;
                }catch(DmlException e){
                    throw new LeasewareException(e.getMessage());
                    //leasewareutils.createExceptionLog(e, 'Exception while calculating Asset OverallScore.', null, null, null, true);
                }
            }
            
            system.debug('infoMap--------'+ infoMap);
            return infoMap;
            
        }
       
    


    public static List<Custom_Lookup__c> getAssetScoreCardLookupDetails(String sublookupType){
        system.debug('sublookupType----'+sublookupType);

        return  [select id,Name,Lookup_Type__c, Sub_LookupType__c,Details__c,Description__c,Sub_LookupType2__c,Sub_LookupType3__c
        from Custom_Lookup__c where Lookup_Type__c='ASSET_SCORECARD' and Sub_LookupType__c like :sublookupType and Active__c = true order by Order__c asc
        ];
       
    }
}