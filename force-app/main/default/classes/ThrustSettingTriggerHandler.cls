public class ThrustSettingTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'ThrustSettingTriggerHandlerBefore';
    private final string triggerAfter = 'ThrustSettingTriggerHandlerAfter';
    
    static list<Thrust_Setting__c> listTSUpdateAfter = new list<Thrust_Setting__c>();
    
    public ThrustSettingTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('ThrustSettingTriggerHandler.beforeInsert(+)');
        ThrustSettingInsUpdDel();
        system.debug('ThrustSettingTriggerHandler.beforeInsert(-)');    
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('ThrustSettingTriggerHandler.beforeUpdate(+)');
        ThrustSettingInsUpdDel();

        
        system.debug('ThrustSettingTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('ThrustSettingTriggerHandler.beforeDelete(+)');
        ThrustSettingInsUpdDel();
    
        system.debug('ThrustSettingTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('ThrustSettingTriggerHandler.afterInsert(+)');
        AfterUpdateCall();
        system.debug('ThrustSettingTriggerHandler.afterInsert(-)');         
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('ThrustSettingTriggerHandler.afterUpdate(+)');
        
		AfterUpdateCall();        
        system.debug('ThrustSettingTriggerHandler.afterUpdate(-)');         
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('ThrustSettingTriggerHandler.afterDelete(+)');
        
        
        system.debug('ThrustSettingTriggerHandler.afterDelete(-)');         
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('ThrustSettingTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('ThrustSettingTriggerHandler.afterUnDelete(-)');       
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

    //after insert, after update
    private void AfterUpdateCall(){
        
            if(!listTSUpdateAfter.isEmpty()){
 				
    			LeaseWareUtils.TriggerDisabledFlag=true;
       			try{
               		update listTSUpdateAfter;
                    listTSUpdateAfter.clear();
    			}catch(Exception ex){
           			LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger enabled for all users at apex level
           			           
        			throw new LeaseWareException('!!! Unexpected Error, please check with System Administrator :' + ex);
    			}
    			LeaseWareUtils.TriggerDisabledFlag=false;
       			          
            }        
    }
    
    // before delete, before insert, before update
    private void ThrustSettingInsUpdDel(){
		system.debug('ThrustSettingInsUpdDel+');
        // Global variable  
        //map<Id,list<Thrust_Setting__c>> mapTS = new map<Id,list<Thrust_Setting__c>>();
        map<Id,list<Thrust_Setting__c>> mapTSCurrent = new map<Id,list<Thrust_Setting__c>>();
        
        set<Id> setTSRefLLPIDs = new set<Id>(); // this is to get LLP id 
        set<Id> setTSIDs = new set<Id>();// This is to get cuurent TS ID 
        
        Set<String> setTSName = new Set<String>();
        Thrust_Setting__c[] allTS ;
        system.debug('Start Trigger : ThrustSettingInsUpdDel');
        
        if(trigger.isDelete){
            allTS=(list<Thrust_Setting__c>)trigger.old;
        }else{
            allTS=(list<Thrust_Setting__c>)trigger.new;
        }   
        // Get all LLP id in Set
        Thrust_Setting__c OldTS;
        for(Thrust_Setting__c TSRecord : allTS){
            
            // below to check if the update is nothing with cycle Limit and Used then skip the trigger
            if(trigger.isInsert ){
                setTSRefLLPIDs.add(TSRecord.Life_Limited_Part__c);
                TSRecord.Weighted_Cycle_Used__c = null ; 
                TSRecord.Remaining_Cycle__c = null; 
        
                //setTSIDs.add(TSRecord.Id);
            }
            else if(trigger.isUpdate ){
                OldTS = (Thrust_Setting__c)trigger.oldMap.get(TSRecord.id);
                if( OldTS.Cycle_Limit__c != TSRecord.Cycle_Limit__c 
                    ||  OldTS.Cycle_Used__c != TSRecord.Cycle_Used__c
                    ||  OldTS.Thrust_Setting_Name__c != TSRecord.Thrust_Setting_Name__c) {
                    //flag = true ;
                    TSRecord.Weighted_Cycle_Used__c = null ; 
                    TSRecord.Remaining_Cycle__c = null;
                    setTSRefLLPIDs.add(TSRecord.Life_Limited_Part__c);
                    
                    
                }
                setTSIDs.add(TSRecord.Id);
            }else if(trigger.isDelete ) {
                    //flag = true ;
                    setTSRefLLPIDs.add(TSRecord.Life_Limited_Part__c);
                    setTSIDs.add(TSRecord.Id);
            }
        }
        
        // calculation of Remaining cycle and Active TS 
        if(!setTSRefLLPIDs.isEmpty()){
            system.debug('Inside Trigger : ThrustSettingInsUpdDel');
            // fetch all record based on  LLP ID Set
    
            map<Id,Custom_Lookup__c> mapLookup = new map<Id,Custom_Lookup__c>([select Id,name,Lookup_Type__c,Sub_LookupType__c from Custom_Lookup__c where Lookup_Type__c = 'Engine' AND active__c = true]);
            system.debug(''+ mapLookup);
            list<Sub_Components_LLPs__c> listLLP = new list<Sub_Components_LLPs__c>([select Id,Constituent_Assembly__c
                                                ,Constituent_Assembly__r.Current_TS__c,Constituent_Assembly__r.Engine_Model__c ,
                        (select Life_Limited_Part__c, Cycle_Limit__c,Cycle_Used__c,Remaining_Cycle__c,Weighted_Cycle_Used__c,Thrust_Setting_Name__c from Thrust_setting__r
                          where  Id not in :setTSIDs) 
                    from Sub_Components_LLPs__c 
                    where Id in :setTSRefLLPIDs]);
            list<Thrust_setting__c> listTS = new list<Thrust_setting__c>();
            // create a map for current record based on LLP id.
            list<Thrust_Setting__c> listTSNew;
            if(!trigger.isDelete){
                for(Thrust_Setting__c newRec : allTS){
                    if(mapTSCurrent.get(newRec.Life_Limited_Part__c)== null){
                    // New 
                        listTSNew = new list<Thrust_Setting__c>();
                        listTSNew.add(newRec);
                        mapTSCurrent.put(newRec.Life_Limited_Part__c,listTSNew);
                    }
                    else{
                        listTSNew = mapTSCurrent.get(newRec.Life_Limited_Part__c);
                        listTSNew.add(newRec);
                    
                    }           
                }
            }
            Double calWeitage = 0;
            Double temp=0;
            list<Thrust_Setting__c> listTSUpdate = new list<Thrust_Setting__c>();
            for(Sub_Components_LLPs__c  curLLP: listLLP){
                calWeitage = 0;
                // Iterate Existing TS record to calculate weitage 
                setTSName.clear();
                system.debug('=='+curLLP.Id+'=='+curLLP.Thrust_setting__r.size());
                for(Thrust_setting__c curRec: curLLP.Thrust_setting__r){
                    //add all the TS name for checking duplicate TS name
                    setTSName.add(curRec.Thrust_Setting_Name__c);
                    
                    if(curRec.Cycle_Used__c != null && curRec.Cycle_Limit__c != null){
                        temp = 0;
                        try{
                            temp =  curRec.Cycle_Used__c/curRec.Cycle_Limit__c ;
                        }catch(Exception e){
                            temp =0;
                        }                       
                        calWeitage = calWeitage + temp ;
                    }
                }// end of child for loop
                // Iterate Current TS record to calculate weitage               
                list<Thrust_Setting__c> listTSCurrent = null;
                if(mapTSCurrent.containsKey(curLLP.Id)) {
                    listTSCurrent= mapTSCurrent.get(curLLP.Id);// this is new/modified record
                }
                if(listTSCurrent!= null){ 
                    System.debug('=== listTSCurrent' + listTSCurrent); // anjani comment later 
                    System.debug('=== setTSName' + setTSName); // anjani comment later      
                    for(Thrust_Setting__c curRec :listTSCurrent ){
                    	system.debug('curRec.Thrust_Setting_Name__c = ' + curRec.Thrust_Setting_Name__c);
                        //check duplicate
                        if(!setTSName.contains(curRec.Thrust_Setting_Name__c)){
                            setTSName.add(curRec.Thrust_Setting_Name__c);
                        }else{
                            if(mapLookup.size()>0 && mapLookup.containsKey(curRec.Thrust_Setting_Name__c)){
                                curRec.Thrust_Setting_Name__c.addError('Thrust Usage record for thrust \'' + mapLookup.get(curRec.Thrust_Setting_Name__c).name +'\' already exists.');
                            }
                         }
                        // check whether the added/modified TS record is referrring to same engine 
                        if(mapLookup.size()>0 && mapLookup.containsKey(curRec.Thrust_Setting_Name__c)){
                            system.debug('mapLookup.get(curRec.Thrust_Setting_Name__c).Lookup_Type__c = ' + mapLookup.get(curRec.Thrust_Setting_Name__c).Lookup_Type__c);
                            system.debug('mapLookup.get(curRec.Thrust_Setting_Name__c).Sub_LookupType__c ='+ mapLookup.get(curRec.Thrust_Setting_Name__c).Sub_LookupType__c);
                            system.debug('curLLP.Constituent_Assembly__r.Engine_Model__c ='+ curLLP.Constituent_Assembly__r.Engine_Model__c);
                            if(mapLookup.get(curRec.Thrust_Setting_Name__c).Sub_LookupType__c != null && !(mapLookup.get(curRec.Thrust_Setting_Name__c).Sub_LookupType__c == curLLP.Constituent_Assembly__r.Engine_Model__c)){
                                curRec.addError('Invalid Thrust Setting Name : ' + mapLookup.get(curRec.Thrust_Setting_Name__c).name + '. Please pick correct Thrust Setting Name .');
                            }    
                        }
                   
                        // first set checkboc : current TS
                        if(!trigger.IsDelete){
                            if(curLLP.Constituent_Assembly__r.Current_TS__c == curRec.Thrust_Setting_Name__c){
                                curRec.Current_Thrust__c = true;
                            }else{
                                curRec.Current_Thrust__c = false;
                            }   
                        }   
                        // Add  weitage             
                        if(curRec.Cycle_Used__c != null && curRec.Cycle_Limit__c != null){
                            temp =0 ;
                            try{
                                temp =  curRec.Cycle_Used__c/curRec.Cycle_Limit__c ;
                            }catch(Exception e){
                                temp =0;
                            }
                            calWeitage = calWeitage + temp ;
                        }
                    }   
                }// end if(listTSCurrent!= null){   
                // update existing TS with new weitage and add in list
                temp = 0; 
                for(Thrust_setting__c curRec: curLLP.Thrust_setting__r){
                    if(curRec.Cycle_Used__c != null && curRec.Cycle_Limit__c != null){
                        //temp = 0;
                        //temp = calWeitage * curRec.Cycle_Limit__c;
	                    //temp = temp.round(System.RoundingMode.DOWN);
	                    curRec.Weighted_Cycle_Used__c = calWeitage * curRec.Cycle_Limit__c;
	                    curRec.Weighted_Cycle_Used__c = curRec.Weighted_Cycle_Used__c.setscale(2).round(System.RoundingMode.UP); 
                        curRec.Remaining_Cycle__c = curRec.Cycle_Limit__c - curRec.Weighted_Cycle_Used__c ;
                        listTSUpdate.add(curRec);
                    }
                }// end of child for loop
                // update current TS with new weitage 
                temp = 0;
                if(listTSCurrent!= null){
                    for(Thrust_Setting__c curRec :listTSCurrent ){
                        if(curRec.Cycle_Used__c != null && curRec.Cycle_Limit__c != null){
                            //temp = 0;
                            //temp = calWeitage * curRec.Cycle_Limit__c;
	                        //temp = temp.round(System.RoundingMode.DOWN);
	                        curRec.Weighted_Cycle_Used__c = calWeitage * curRec.Cycle_Limit__c;
	                        curRec.Weighted_Cycle_Used__c = curRec.Weighted_Cycle_Used__c.setscale(2).round(System.RoundingMode.UP); 
                            curRec.Remaining_Cycle__c = curRec.Cycle_Limit__c - curRec.Weighted_Cycle_Used__c ;
                        }
                    }           
                }               
            }// end of LLP for loop
            
            // update TS record 
            if(!listTSUpdate.isEmpty()){
                listTSUpdateAfter.addAll(listTSUpdate);
            }
       
        }// end of condition setTSRefLLPIDs.isEmpty()   
        system.debug('ThrustSettingInsUpdDel-');
    }

}