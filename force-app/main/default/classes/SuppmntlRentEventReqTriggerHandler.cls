public class SuppmntlRentEventReqTriggerHandler implements ITrigger{
    // Hint This Handler file is create Historical Event Task record while creating Supplemental Rent Event Requirement record also handling deletion part
    
    // Constructor
    private final string triggerBefore = 'SuppmntlRentEventReqTriggerHandlerBefore';
    private final string triggerAfter = 'SuppmntlRentEventReqTriggerHandlerAfter';
    
    
    
    public SuppmntlRentEventReqTriggerHandler ()
    {   
    }
    //Making functionality more generic.
   
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {

    }
    public void bulkAfter()
    {

    }   
    public void beforeInsert()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('SuppmntlRentEventReqTriggerHandler.beforeInsert(+)');
        DuplRuleNameMETLkp();
        system.debug('SuppmntlRentEventReqTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('SuppmntlRentEventReqTriggerHandler.beforeUpdate(+)');
        DuplRuleNameMETLkp();
        system.debug('SuppmntlRentEventReqTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        
    }
     
    public void afterInsert()
    {

    }
     
    public void afterUpdate()
    {

    }
     
    public void afterDelete()
    {
           
    }

    public void afterUnDelete()
    {
            
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
    }
    
    //Before Insert, Before Update
    //Helps to create Duplicate Rule for Name field and MET Lkp Field
    private void DuplRuleNameMETLkp(){
        list<Supp_Rent_Event_Reqmnt__c> ListSREReq =(list<Supp_Rent_Event_Reqmnt__c>)trigger.new;
        for(Supp_Rent_Event_Reqmnt__c curSREReq: ListSREReq){
            curSREReq.Y_Hidden_Key__c=curSREReq.Supplemental_Rent__c+curSREReq.Name; // Used in Duplicate Rule
            
            // Duplicate Supplemental Rent Event Requirements associated with the same Maintenance Event Task are not allowed
            curSREReq.Y_Hidden_DupMPE__c=curSREReq.Supplemental_Rent__c+''+curSREReq.Maintenance_Event_Task__c; 
        }
    }

}
