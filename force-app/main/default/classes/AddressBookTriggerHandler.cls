public class AddressBookTriggerHandler implements ITrigger{

    private final string  triggerBefore = 'AddressBookTriggerHandlerBefore';
    private final string  triggerAfter = 'AddressBookTriggerHandlerAfter';
    public AddressBookTriggerHandler()
    {
    }
 
 
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('AddressBookTriggerHandler.beforeInsert(+)');
	
		updateFirstLastName();
	    system.debug('AddressBookTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AddressBookTriggerHandler.beforeUpdate(+)');
    	updateFirstLastName();
  		
    	system.debug('AddressBookTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AddressBookTriggerHandler.beforeDelete(+)');
    	
     	
    	//AircraftInDeal_InsUpdDel();
    	system.debug('AddressBookTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AddressBookTriggerHandler.afterInsert(+)');
    	
    	system.debug('AddressBookTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AddressBookTriggerHandler.afterUpdate(+)');
      	
    	system.debug('AddressBookTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AddressBookTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('AddressBookTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AddressBookTriggerHandler.afterUnDelete(+)');
    	
    	
    	
    	system.debug('AddressBookTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


	private void updateFirstLastName(){
		
		list<Address__c> listNew = (list<Address__c>)trigger.new;
		
		string[] NameStr;
		Address__c oldRec;
		for(Address__c curRec   :listNew){
			curRec.name = curRec.name.trim();
			NameStr = curRec.Name.split(' ');
			
			system.debug('NameStr='+NameStr);
			if(NameStr==null || NameStr.size()<1) continue;
			if(trigger.IsInsert){
				if(curRec.Last_Name__c==null && NameStr.size()<2 && curRec.First_Name__c!=null){
					curRec.Last_Name__c = 'NA';
				}else if(curRec.Last_Name__c==null && curRec.First_Name__c==null){
					curRec.Last_Name__c = NameStr[NameStr.size()-1];
					if(NameStr.size()<2) curRec.First_Name__c = null;
					else curRec.First_Name__c = curRec.name.removeEnd(' ' + curRec.Last_Name__c);
				}
					
				
					
				
			}else{// update case
				oldRec = (Address__c)trigger.oldmap.get(curRec.Id);
				if(curRec.Last_Name__c==null && NameStr.size()<2 && curRec.First_Name__c!=null){
					curRec.Last_Name__c = 'NA';
				}
				else if(oldRec.name!=curRec.name){
					curRec.Last_Name__c = NameStr[NameStr.size()-1];
				
					if(NameStr.size()<2) curRec.First_Name__c = null;
					else curRec.First_Name__c = curRec.name.removeEnd(' ' + curRec.Last_Name__c);
				}
				
			}
			
			curRec.name = (curRec.First_Name__c==null?'':curRec.First_Name__c+' ')  + (curRec.Last_Name__c=='NA'?'':curRec.Last_Name__c);
		}
		
		
	}
}