@isTest
public class TestUtilizationTableController {
    @isTest
    public static void testGetData(){
       
        Date lastDayOfMonth = System.today().addMonths(2).toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        Custom_Lookup__c lookup = new Custom_Lookup__c(Name = 'Default', Lookup_Type__c = 'Engine', Active__c = true);
        insert lookup;
        
        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        Lease__c leaseRecord=new Lease__c();
        leaseRecord.Security_Deposit__c=200;
        leaseRecord.Lessee__c=operatorRecord.Id;
        leaseRecord.Lease_Id_External__c='test local';
        leaseRecord.Lease_Start_Date_New__c=minStartDate;
        leaseRecord.Lease_End_Date_New__c=lastDayOfMonth;
        insert leaseRecord;
        
        Id recordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        
        Aircraft__c aircraftRecord=new Aircraft__c();
        aircraftRecord.Name='test aircraft';
        aircraftRecord.MSN_Number__c='123';
        aircraftRecord.Date_of_Manufacture__c = minStartDate;
        aircraftRecord.Aircraft_Type__c='737';
        aircraftRecord.Aircraft_Variant__c='700';
        aircraftRecord.TSN__c=1.00;
        aircraftRecord.CSN__c=1;
        aircraftRecord.RecordTypeId=recordTypeId;
        insert aircraftRecord;
        aircraftRecord.Lease__c = leaseRecord.Id;
        update aircraftRecord;
        
        leaseRecord.Aircraft__c = aircraftRecord.Id; 
        update leaseRecord;
        
        Id recordTypeId1 = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId();
        
        Constituent_Assembly__c assembly=new Constituent_Assembly__c();
        assembly.Name='test assembly';
        assembly.Serial_Number__c='1';
        assembly.RecordTypeId=recordTypeId1;
        assembly.TSO_External__c=2;
        assembly.CSN__c=4;
        assembly.TSN__c=5;
        assembly.CSO_External__c=3;
        assembly.Type__c='Airframe';
        assembly.Attached_Aircraft__c=aircraftRecord.Id;
        insert assembly; 
        
     
        
        Assembly_Utilization_External__c assemblyUtilization=new Assembly_Utilization_External__c();
        assemblyUtilization.Name='test asUtilization';
        assemblyUtilization.Period_End_Date__c=System.today().addDays(10);
        assemblyUtilization.Hours__c=2;
        assemblyUtilization.Cycles__c=1;
        assemblyUtilization.Assembly_Lkp__c=assembly.Id;
        insert assemblyUtilization;
       
        Test.startTest();
        UtilizationTableController.loadData(assembly.Id);
        //UtilizationTableController.getUtilizations(assembly.Id,2020);
        Test.stopTest();
    }
}