public without Sharing class BackgroundDataValidationHelper {
    private final Integer maxNumberOfProcess = 50; // maximum we can go upto 100.
    private BackgroundDataValidationScriptHelper scriptHelper= new BackgroundDataValidationScriptHelper();

    /* delete logfile from Files */
    public void deleteLogFile(id recId){
        LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
        list<ContentDocument> lstCntDocsToDelete = new list<ContentDocument>();

        for(ContentDocumentLink iterator : [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:recId limit 5]) {
            lstCntDocsToDelete.add(new ContentDocument(Id = iterator.ContentDocumentId));
        }
        
        if(!lstCntDocsToDelete.isEmpty()) {
            Database.delete(lstCntDocsToDelete, false);
            Database.emptyRecycleBin(lstCntDocsToDelete);
        }
        LeaseWareUtils.TriggerDisabledFlag = false;
    }
    public void deleteOldRecords(string parentId){
         //cleanup : delete old records
        delete [select id from Admin_Data_Validation_Result__c where Admin_Data_Validation__c =:parentId limit 9500];// keeping 500 for other DML updates
    }
    public void deleteOldRecordsInBatch(string parentId,string processReqId){
        //cleanup : delete old records
        AggregateResult ag = [select count(id) ct from Admin_Data_Validation_Result__c 
                                where Admin_Data_Validation__c =:parentId and Request_Id__c != :processReqId];
        Decimal numberOfOldRecords =  (Decimal) ag.get('ct');                       
        if(numberOfOldRecords>0){
            delete [select id from Admin_Data_Validation_Result__c 
                where Admin_Data_Validation__c =:parentId and Request_Id__c != :processReqId limit 5000];// Expecting we are not creating more than 2k records per batch chunk.
        }   
    }
    public list<Admin_Data_Validation__c> getActiveDataValidationRule(Integer limitParam,string processRequestIdParam,string recId){

        try
        {
            //Added 'For Update' to lock the records since multiple batches can try to spawn new batches at the same time.
            if(recId==null){
                return [select Id, Name, Category__c, Object_Name__c, Last_Run__c, Status__c, Impact__c, Desc__c
                        , Condition__c, Failures__c, CreatedDate,Batch_Size__c, Script_Method_Name__c
                    from Admin_Data_Validation__c 
                    where Active__c=true and Request_Id__c != :processRequestIdParam and Status__c !='Processing'
                    limit :limitParam FOR UPDATE]; 
            }else{
                return [select Id, Name, Category__c, Object_Name__c, Last_Run__c, Status__c, Impact__c, Desc__c
                        , Condition__c, Failures__c, CreatedDate,Batch_Size__c, Script_Method_Name__c
                    from Admin_Data_Validation__c 
                    where Status__c !='Processing' and id=:recId
                    limit :limitParam FOR UPDATE];                 
            }        
        }
        catch(Exception ex)
        {
            //Catch any exception that could occur due to locking of the records.
            System.debug('Exception occured while getting the active validation rules when invoked for ProcessRequestId= ' + processRequestIdParam + ex.getMessage());
            LeaseWareUtils.createExceptionLog(ex, 'Exception while getting active rules for background data validation', null, null,'Background Data Validation',true);  
            return new List<Admin_Data_Validation__c>();
        }
    }


    public void callBatchProcessForDataValidation(string processRequestId,string recId){
        system.debug( 'callBatchProcessForDataValidation - Generated Id='   + processRequestId);
        
        //logic to spawn max at a time , as we are having limitation of open 100 max batch
        Integer numberOfJobRunning = getCurrentRunningBatchJob();        
        Integer numberOfBatchToProcess = maxNumberOfProcess - numberOfJobRunning;   
        System.debug('numberOfJobRunning =' + numberOfJobRunning);
        System.debug('numberOfBatchToProcess =' + numberOfBatchToProcess);

        if(numberOfBatchToProcess < 1) {return;}

        list<Admin_Data_Validation__c> processList = getActiveDataValidationRule(numberOfBatchToProcess,processRequestId,recId);
        System.debug('getActiveDataValidationRule returned = ' + processList.size());
        
        for(Admin_Data_Validation__c process : processList){
            process.Request_Id__c = processRequestId;
            process.Status__c = 'Processing';
            System.debug('STARTING BATCH - ' + process.Script_Method_Name__c);
            createAdminDataValidationBatchJob(process,processRequestId);
        }

        updateAdminDataValidation(processList);
    }

    public void createAdminDataValidationBatchJob(Admin_Data_Validation__c process,string processRequestId){
        BackgroundDataValidation bdv = new BackgroundDataValidation();
        if(processRequestId==null){
            //This must be getting called for single execution. generate new
            bdv.setStandalone();
            bdv.initBackgroundDataValidation();
        } 
        bdv.setBackgroundDataValidation(process,processRequestId);            
        Database.executeBatch(bdv,process.Batch_Size__c==null?200:Integer.valueOf(process.Batch_Size__c)); 
    }

    public void updateAdminDataValidation(list<Admin_Data_Validation__c> processListParam){
        if(processListParam==null || processListParam.IsEmpty()) {return;}
        update processListParam;
    }
    public Integer getCurrentRunningBatchJob(){
        AggregateResult result= [SELECT count(id) Total FROM AsyncApexJob
                where status not in ('Completed','Failed','Aborted') ]; 
        return (Integer)result.get('Total');
    }


    public List<Object> getQueryLocator(string scriptMethodName,map<string,string> mapParam){
        system.debug('BackgroundDataValidationHelper.getQueryLocator Param =' + ' scriptMethodName=' +scriptMethodName  + '=mapParam='+mapParam);
        
        switch on scriptMethodName {
            //TODO - Add new validation process
            when 'FIN_INV_RECORDTYPE_MISMATCH'{
                return scriptHelper.invoiceRecordTypeMismatchStart(mapParam);
            }     
            when 'FIN_INV_HEADERAMT_MISMATCH'{
                return scriptHelper.invoiceAmtMismatchWithLineItemsAmtStart(mapParam);
            }                  
            when 'FIN_INV_LI_MR_SR_SR_UTLZN_NULL'{
                return scriptHelper.mrInvoiceLISRSRUtlznNullStart(mapParam);
            }     
            when 'FIN_PAY_AMT_PAID_AMT_ALLOCATED_MISMATCH'{
                return scriptHelper.pyAmtPaidAmtAllocatedMismatchStart(mapParam);
            }  
            when 'FIN_PYT_LI_MR_SR_SR_UTLZN_NULL'{
                return scriptHelper.mrPaymentLISRSRUtlznNullStart(mapParam);
            }  
            when 'TECH_ASSEMBLY_RECORDTYPE_MISMATCH'{
                return scriptHelper.assemblyRecordTypeMismatchStart(mapParam);
            }  
            when 'FIN_SR_BAL_RUNNING_BAL_MISMATCH'{
                return scriptHelper.supplementRentBalMismatchStart(mapParam);
            }
            when 'NOTIFICATION_SET_UP_MISSING'{
                return scriptHelper.notificationSetupMissingStart(mapParam);
            } 
            when 'NOTIFICATION_SUBSCRIPTION_IN_ERROR'{
                return scriptHelper.notificationSubscriptionInErrorStart(mapParam);
            }
            
			when else{ 
                system.debug('BackgroundDataValidationHelper. Alert!!!!! No Matching method found. Script Name='+scriptMethodName);
            }
        }
        return null;
    }

    public string execute(List<Object> scopeList,string scriptMethodNameParam,map<string,string> mapParam){
        system.debug('BackgroundDataValidationHelper.callExecuteMethod Param =' + ' scopeList=' + scopeList.size() + ' scriptMethodName=' + scriptMethodNameParam + '=mapParam='+mapParam);
        switch on scriptMethodNameParam { 
            //TODO - Add new validation process         
            when 'FIN_INV_RECORDTYPE_MISMATCH'{
                return scriptHelper.invoiceRecordTypeMismatchExecute((list<Invoice__c>)scopeList, mapParam);
            }		
            when 'FIN_INV_HEADERAMT_MISMATCH'{
                return scriptHelper.invoiceAmtMismatchWithLineItemsAmtExecute((list<Invoice__c>)scopeList,mapParam);
            } 				
            when 'FIN_INV_LI_MR_SR_SR_UTLZN_NULL'{
                return scriptHelper.mrInvoiceLISRSRUtlznNullExecute((list<Invoice_line_item__c>)scopeList, mapParam);
            }	
            when 'FIN_PAY_AMT_PAID_AMT_ALLOCATED_MISMATCH'{
                return scriptHelper.pyAmtPaidAmtAllocatedMismatchExecute((list<payment_receipt__c>)scopeList, mapParam);
            } 
            when 'FIN_PYT_LI_MR_SR_SR_UTLZN_NULL'{
                return scriptHelper.mrPaymentLISRSRUtlznNullExecute((list<payment_line_item__c>)scopeList, mapParam);
            }  	
            when 'TECH_ASSEMBLY_RECORDTYPE_MISMATCH'{
                return scriptHelper.assemblyRecordTypeMismatchExecute((list<Constituent_Assembly__c>)scopeList, mapParam);
            } 
            when 'FIN_SR_BAL_RUNNING_BAL_MISMATCH'{
                return scriptHelper.supplementRentBalMismatchExecute((list<Assembly_MR_Rate__c>)scopeList, mapParam);
            }
            when 'NOTIFICATION_SET_UP_MISSING'{
                return scriptHelper.notificationSetupMissingExecute((list<Notifications_Subscription__c>)scopeList, mapParam);
            } 
            when 'NOTIFICATION_SUBSCRIPTION_IN_ERROR'{
                return scriptHelper.notificationSubscriptionInErrorExecute((list<Notifications_Subscription__c>)scopeList, mapParam);
            }	
			when else { 
                system.debug('BackgroundDataValidationHelper. Alert!!!!! No Matching method. Script Name='+scriptMethodNameParam);
                if(mapParam!=null){
                    mapParam.put('ERROR','YES');
                }                  
            }         
        }
        
        return 'BackgroundDataValidationHelper. Alert!!!!! No Matching method. Script Name='+scriptMethodNameParam;
    }


    public void finishMethodOnScriptRun(Admin_Data_Validation__c dataProcessParam,string logMessageStringParam,string exceptionMessage,map<string,string> mapParam){
        string scriptMethodName = dataProcessParam.Script_Method_Name__c;
        string scriptStatus= 'Success';
        if(exceptionMessage!=''){
            scriptStatus= 'Unexpected Error';
        }else if(mapParam!=null && mapParam.containsKey('ERROR')){
            scriptStatus= 'Error';
        }  

        dataProcessParam.Last_Run__c = system.now();
        dataProcessParam.Status__c = scriptStatus;
        updateAdminDataValidation(new list<Admin_Data_Validation__c>{dataProcessParam});


        //Keeping log Info        
        string outputStr ='[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] - ';
        
        if(LeaseWareUtils.isRunningInDevInstance()){
            outputStr += ' Running in Developer Instance : \n';
        }else {
            outputStr +=  ' Package Info=' +  system.requestVersion()   + '\n';
        }
        outputStr += '=> Error Message if any: \n' + exceptionMessage; 
        outputStr += '\n => Information \n' + logMessageStringParam;
        
        // attach file to the record
        try{
            LeaseWareUtils.TriggerDisabledFlag = true; // do not want trigger to be called for internal files.
            LeaseWareStreamUtils.addFileToRecord(scriptMethodName,dataProcessParam.id,outputStr,'.log');
            LeaseWareUtils.TriggerDisabledFlag = false;
        }catch(Exception ex){
            LeaseWareUtils.TriggerDisabledFlag = false;
            outputStr =   '------------------------------------------\n' + ' Got error while atdding the log file to record.'+ex +
                        '------------------------------------------\n' + outputStr;
        }
 
    }    
    //-----------------------------------------------------------------------
    list<Admin_Data_Validation__c> listADV = new list<Admin_Data_Validation__c>();
    set<string> setScriptName = new set<string>();
    public void createAdminDataValidation(){
        getExistingRecs();
        //TODO - Add new validation process
        insertRec('Invoice Record Type Mismatch','Finance','Invoice','Medium','Invoice Record Type and Invoice Type field does not match. This may cause to the data inconsistency and issues reports and calculations. It is strongly recommended to fix identified issues','Invoice Type and Record Type are different',1000,'FIN_INV_RECORDTYPE_MISMATCH' );
        
        insertRec('Invoiced Header Amount and Invoice Line Items','Finance','Invoice','High','Invoiced header amount should be equal to the sum of all Line Items. If not, there\'s a mismatch in the calculation that can cause an issue with invoice processing. It is strongly recommended to fix identified issues','Invoice Header Amount is not equal to sum of all Invoice Line Items (if any)',1000,'FIN_INV_HEADERAMT_MISMATCH' );
                
        insertRec('Invoice Line Item for MR without Supplemental Rent or SR Utilization Link','Finance','Invoice Line Item','High','Invoice Line Item for maintenance reserves should always have a link to the associated Supplemental Rent fund and to the Supplemental Rent Utilization report. If this lookup is not entered, then this Invoice Line item will not show on any Supplemental Rent Transaction history and will not update any fund balance. Therefore this receiveable record is not considered for any MR accumulation','Invoice Line Item record for MR with Supplemental Rent or Supplemental Rent Utilization look up field null',1000,'FIN_INV_LI_MR_SR_SR_UTLZN_NULL' );

        insertRec('Payment Header Amount and Invoice Allocated Amount Paid','Finance','Payment Studio','High','Payment Amount should be equal to sum of Amount Paid of the Invoices Allocated.If not, theres a mismatch in the calculation that can cause an issue with payment processing. It is strongly recommended to fix identified issues.','Payment Amount is not equal to sum of Amounts Paid of the Invoices Allocated',1000,'FIN_PAY_AMT_PAID_AMT_ALLOCATED_MISMATCH' );

        insertRec('Payment Line Item for MR without Supplemental Rent or SR Utilization Link','Finance','Payment Line Item','High','Payment Line Item for maintenance reserves should always have a link to the associated Supplemental Rent fund and to the Supplemental Rent Utilization report. If this lookup is not entered, then this Payment Line item will not show on any Supplemental Rent Transaction history and will not update any fund balance. Therefore this payment record is not considered for any MR refund','Payment Line Item record for MR with Supplemental Rent or Supplemental Rent Utilization look up field null',1000,'FIN_PYT_LI_MR_SR_SR_UTLZN_NULL' );

        insertRec('Assembly Record Type and Assembly Type mismatch','Technical','Assembly','Medium','Assembly Record Type does not match with the Assembly Type (usually due to incorrect data loading). This may cause incorrect fields and layouts to show to the users and unexpected errors when managing those assemblies. It is recommended to fix this','Assembly Type does not match the Record type: The Assembly Type needs to contain the same name as the Record Type',1000,'TECH_ASSEMBLY_RECORDTYPE_MISMATCH' );
        
        insertRec('Supplemental Rent Balance and Running Balance mismatch','Finance','Supplemental Rent','High','Supplemental Rent Transactions records have running balances auto calculated by the system.  And there\'s also Transactions roll-up summary fields on the Supplemental Rent calculating the same balance. The latest running balance and summary field should always match. If not - there\'s a risk of missing transaction record, error in the calculation and incorrect MR balance information. It is strongly recommended to fix identified issues','Transactions Total (Invoiced) not equal to the latest Running Balance (Invoiced) record and Transactions Total (Cash Collected) is not equal to the latest Running Balance (Cash Collected) record',1000,'FIN_SR_BAL_RUNNING_BAL_MISMATCH' );
        
        insertRec('Notification Setting is not set','General','Notification Subscription','High','Notification Subscription feature works only when the Notification Setting record with the Setup information exists. This error shows that Setup is not configured, therefore Notifcations will not be sent. Please follow user manual to configure the Setup record','Notification Setting - no records AND at least one Notification Subscription record exists',1,'NOTIFICATION_SET_UP_MISSING' );
        
        insertRec('Notification Subscription Error','General','Notification Subscription','High','Notification Subscription could not run due to the error. Users were not notified and this can cause a serious business damage. Please inform all recipients of this notification so they can follow a backup plan for notificaitons until notification subscription is fixed.','Notificat subscription Status = Error',1000,'NOTIFICATION_SUBSCRIPTION_IN_ERROR' );
       
        insertScripts();
    }


    private void getExistingRecs(){
        for( Admin_Data_Validation__c p : [select  Script_Method_Name__c from Admin_Data_Validation__c] ){
            setScriptName.add(p.Script_Method_Name__c);
        }
    }

    @testVisible
    private void insertScripts(){
        insert listADV;
        listADV.clear();
    }

    @testVisible
    private void insertRec(string nameParam,string category,string objectName,string impact,string description,string condition,Decimal batchSize,string scriptMethodName ){
        if(setScriptName.contains(scriptMethodName)) {return;}
        listADV.add(new Admin_Data_Validation__c(name = nameParam
            ,Category__c = category
            ,Object_Name__c = objectName
            ,Impact__c = impact
            ,Desc__c = description
            ,Condition__c = condition
            ,Batch_Size__c = batchSize
            ,Script_Method_Name__c  = scriptMethodName
            ,Active__c = true                                     
        ));
    }

}