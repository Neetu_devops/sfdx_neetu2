public with sharing class InvoiceAdjustmentController {

    private static Integer ADD_RECORD = 1;
    private static Integer UPDATE_RECORD = 2;

    
    public static boolean isApprovalProcessRunning() {
        List<ProcessDefinition> ProcessDefinitionList = [SELECT TableEnumOrId FROM ProcessDefinition WHERE TableEnumOrId ='Invoice_Adjustment__c'  and State = 'Active'] ;
        if(ProcessDefinitionList.size() > 0) {
            // It mean approval process is there related to that object.
            return true;
        }
        return false;
    }

    /**
     * Input: Object name, Record name
     * Output: return the picklist value
     * Functionality: Get the picklist values and returns it
     */
    @AuraEnabled
    public static List <String> getPickListOptions(String sobj, string fieldname) {
        string prefix = leasewareutils.getNamespacePrefix();
        string sobjNew = sobj;
        if(sobj.indexof(prefix) == -1) {
            sobjNew = prefix + sobj;
        }
        string fieldNameNew = fieldname;
        if(fieldname.indexof(prefix) == -1) {
            fieldNameNew = prefix + fieldname;
        }
        List <String> optionList = new list <String>();
        SObjectType objType = Schema.getGlobalDescribe().get(sobjNew);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list <Schema.PicklistEntry> values = fieldMap.get(fieldNameNew).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            optionList.add(a.getValue());
        }
        
        return optionList;
    }

    /**
     * Get the record Id and returns the record with has status as pending
     */
    @AuraEnabled
    public static List<Invoice_Adjustment__c> getPendingAdjustment(Id recordId) {
        System.debug('getPendingAdjustment '+recordId);
        String sobjectType = recordId.getSObjectType().getDescribe().getName();
        if(String.isEmpty(sobjectType)) { return null;}
        if(sobjectType.containsIgnoreCase('Invoice__c')) {
            return [select Id from Invoice_Adjustment__c where Linked_Invoice__c =: recordId and Approval_Status__c = 'Pending'];
        }
        return null;
    }
	
    
    /**
     * Get the record Id and returns the record with has status as Approved
     */
    @AuraEnabled
    public static List<Invoice_Adjustment__c> getAdjustmentStatus(Id recordId) {
        System.debug('getPendingAdjustment '+recordId);
        String sobjectType = recordId.getSObjectType().getDescribe().getName();
        if(String.isEmpty(sobjectType)) { return null;}
        if(sobjectType.containsIgnoreCase('Invoice_Adjustment__c')) {
            return [select Id from Invoice_Adjustment__c where Id =: recordId and Approval_Status__c = 'Approved'];
        }
        return null;
    }

    
    /**
     * Input: Record Id
     * Output: Wrapper containing Invoice, Adjustment, Invoice Line items data
     * Based on the record Id the data is retrieved
     * If the record id is Invoice then the component has been launched from NEW button
     * else if it is Invoice Adjustment object then its an EDIT
     */
    @AuraEnabled
    public static InvoiceAdjustmentWrapper getInvoiceData (Id recordId) {
        String sobjectType = recordId.getSObjectType().getDescribe().getName();
        InvoiceAdjustmentWrapper wrapper = new InvoiceAdjustmentWrapper();
        System.debug('getInvoiceData sobjectType '+sobjectType);
        if(String.isEmpty(sobjectType)) { return null; }            
        if(sobjectType.containsIgnoreCase('Invoice__c')) {
            List<Invoice__c> invoiceData = [select Id, Name, Status__c, Payment_Status__c, Invoiced_Amount_Formula__c, Balance_Due__c, Comments__c, 
                                        (select Id, Name, Total_Line_Amount__c ,Balance_Due__c from Invoice_Line_Items__r)
                                        from Invoice__c where Id =:recordId];
            if(invoiceData.size() > 0) {
                wrapper.invoiceId = invoiceData[0].Id;
                wrapper.invoiceName = invoiceData[0].Name;
                wrapper.invoiceAmount = invoiceData[0].Invoiced_Amount_Formula__c;
                wrapper.invoiceBalanceDue = invoiceData[0].Balance_Due__c;
                wrapper.invoiceStatus = invoiceData[0].Status__c;
                wrapper.invoicePaymentStatus = invoiceData[0].Payment_Status__c;
                wrapper.isStatusDisabled = true;
                List<InvoiceAdjustmentLineItemWrapper> lineItemList = new List<InvoiceAdjustmentLineItemWrapper>();
                for(Invoice_Line_Item__c lineItem : invoiceData[0].Invoice_Line_Items__r) {
                    InvoiceAdjustmentLineItemWrapper adjLineItem = new InvoiceAdjustmentLineItemWrapper();
                    adjLineItem.invoiceLineItemId = lineItem.Id; 
                    adjLineItem.invoiceLineItemName = lineItem.Name; 
                    adjLineItem.invoiceLineItemAmount = lineItem.Total_Line_Amount__c ; 
                    adjLineItem.invoiceLineItemBalanceDue = lineItem.Balance_Due__c; 
                    lineItemList.add(adjLineItem);
                }
                wrapper.invoiceAdjustmentLineItemList = lineItemList;
                Invoice_Adjustment__c adj = new Invoice_Adjustment__c();
                adj.Adjustment_Amount__c = 0;
                adj.Linked_Invoice__c = invoiceData[0].Id;
                adj.Adjustment_Date__c = System.today();
                adj.Comments__c = '';
                adj.Approval_Status__c = 'Pending';
                wrapper.invoiceAdjustmentItem = adj;
            }
        }
        else if(sobjectType.containsIgnoreCase('Invoice_Adjustment__c')) {    
            List<Invoice_Adjustment__c> invoiceAdjustmentList = [select Id, Comments__c,Approval_Status__c, Linked_Invoice__c, Linked_Invoice__r.Name ,  
            Linked_Invoice__r.Invoiced_Amount_Formula__c, Linked_Invoice__r.Balance_Due__c, Linked_Invoice__r.Status__c, Linked_Invoice__r.Payment_Status__c, 
            Adjustment_Amount__c, Adjustment_Date__c, 
            (select Id, Name, Line_Adjustment_Amount__c, Linked_Invoice_Line_Item__c,Invoice_Adjustment_Status__c, 
            Linked_Invoice_Line_Item__r.Name, Linked_Invoice_Line_Item__r.Total_Line_Amount__c  , Linked_Invoice_Line_Item__r.Balance_Due__c
            from Invoice_Adjustment_Line_Items__r)
            from 
            Invoice_Adjustment__c where Id =: recordId];

            if(invoiceAdjustmentList.size() > 0 ) {
                wrapper.invoiceId = invoiceAdjustmentList[0].Linked_Invoice__c;
                wrapper.invoiceName = invoiceAdjustmentList[0].Linked_Invoice__r.Name;
                wrapper.invoiceAmount = invoiceAdjustmentList[0].Linked_Invoice__r.Invoiced_Amount_Formula__c;
                wrapper.invoiceBalanceDue = invoiceAdjustmentList[0].Linked_Invoice__r.Balance_Due__c;
                wrapper.invoiceStatus = invoiceAdjustmentList[0].Linked_Invoice__r.Status__c;
                wrapper.invoicePaymentStatus = invoiceAdjustmentList[0].Linked_Invoice__r.Payment_Status__c;
                Invoice_Adjustment__c adj = new Invoice_Adjustment__c();
                adj.Adjustment_Amount__c = invoiceAdjustmentList[0].Adjustment_Amount__c;
                adj.Id = invoiceAdjustmentList[0].Id;
                adj.Comments__c = invoiceAdjustmentList[0].Comments__c ;
                adj.Adjustment_Date__c = invoiceAdjustmentList[0].Adjustment_Date__c;
                adj.Approval_Status__c = invoiceAdjustmentList[0].Approval_Status__c;
                wrapper.invoiceAdjustmentItem = adj;

                List<InvoiceAdjustmentLineItemWrapper> lineItemList = new List<InvoiceAdjustmentLineItemWrapper>();
                for(Invoice_Adjustment_Line_Item__c lineItem : invoiceAdjustmentList[0].Invoice_Adjustment_Line_Items__r) {
                    InvoiceAdjustmentLineItemWrapper adjLineItem = new InvoiceAdjustmentLineItemWrapper();
                    adjLineItem.invoiceLineItemId = lineItem.Linked_Invoice_Line_Item__c; 
                    adjLineItem.invoiceLineItemName = lineItem.Linked_Invoice_Line_Item__r.Name; 
                    adjLineItem.invoiceLineItemAmount = lineItem.Linked_Invoice_Line_Item__r.Total_Line_Amount__c ; 
                    adjLineItem.invoiceLineItemBalanceDue = lineItem.Linked_Invoice_Line_Item__r.Balance_Due__c; 
                    adjLineItem.invoiceLineItemAdjustedAmount= lineItem.Line_Adjustment_Amount__c;
                    adjLineItem.adjInvoiceLineItemId = lineItem.Id;
                    adjLineItem.adjInvoiceItemId = invoiceAdjustmentList[0].Id;
                    lineItemList.add(adjLineItem);
                }
                wrapper.invoiceAdjustmentLineItemList = lineItemList;    
            }
            wrapper.isAprovalProcess = isApprovalProcessRunning();
        }
        return wrapper;
    }

    /**
     * Input: Wrapper, record Id
     * Output: Created or Updated record Id
     * Functionality: Updates or Creates the record with the Adjustment amount.
     */
    @AuraEnabled
    public static String addUpdateAdjustmentDataFromUI(String adjustmentData, Id recordId) {
        String sobjectType = recordId.getSObjectType().getDescribe().getName();
        
        InvoiceAdjustmentWrapper adjustmentDataWrapper = (InvoiceAdjustmentWrapper)System.JSON.deserialize(adjustmentData, InvoiceAdjustmentWrapper.class);
        System.debug('addUpdateAdjustmentDataFromUI sobjectType: '+sobjectType +', adjustmentDataWrapper: '+adjustmentDataWrapper);
        if(String.isEmpty(sobjectType)) {return null;}
        List<Invoice_Adjustment_Line_Item__c> adjLineItemList;
        if(sobjectType.containsIgnoreCase('Invoice__c')) {    
            adjLineItemList = new List<Invoice_Adjustment_Line_Item__c>();
            for(InvoiceAdjustmentLineItemWrapper lineItemWrapper: adjustmentDataWrapper.invoiceAdjustmentLineItemList) {
                Invoice_Adjustment_Line_Item__c lineItem = new Invoice_Adjustment_Line_Item__c();
                lineItem.Linked_Invoice_Line_Item__c = lineItemWrapper.invoiceLineItemId;
                lineItem.Line_Adjustment_Amount__c = lineItemWrapper.invoiceLineItemAdjustedAmount;
                lineItem.Invoice_Adjustment__c = lineItemWrapper.adjInvoiceItemId ;
                adjLineItemList.add(lineItem);
            }
        }
        else if(sobjectType.containsIgnoreCase('Invoice_Adjustment__c')) {    
            adjLineItemList = [select Id, Invoice_Adjustment_Status__c, Line_Adjustment_Amount__c from Invoice_Adjustment_Line_Item__c where Invoice_Adjustment__c =:recordId];
            Map<Id, Invoice_Adjustment_Line_Item__c> itemMap = new Map<Id, Invoice_Adjustment_Line_Item__c>();
            for(Invoice_Adjustment_Line_Item__c item: adjLineItemList) {
                if(!itemMap.containsKey(item.Id)) {
                    itemMap.put(item.Id, item);
                }
            }
            for(InvoiceAdjustmentLineItemWrapper lineItemWrapper: adjustmentDataWrapper.invoiceAdjustmentLineItemList) {
                if(itemMap.get(lineItemWrapper.adjInvoiceLineItemId) != null) {
                    Invoice_Adjustment_Line_Item__c item = itemMap.get(lineItemWrapper.adjInvoiceLineItemId);
                    item.Line_Adjustment_Amount__c = lineItemWrapper.invoiceLineItemAdjustedAmount;
                }
            }
            System.debug('addUpdateAdjustmentDataFromUI itemMap: '+ JSON.serializePretty(itemMap));
            adjLineItemList.clear();
            adjLineItemList = itemMap.values();
        }
        
        System.debug('addUpdateAdjustmentDataFromUI adjLineItemList: '+ JSON.serializePretty(adjLineItemList));
        if(sobjectType.containsIgnoreCase('Invoice__c')) {   
            addUpdateAdjustmentInvoice(adjustmentDataWrapper.invoiceAdjustmentItem, adjLineItemList, ADD_RECORD);
        }
        else if(sobjectType.containsIgnoreCase('Invoice_Adjustment__c')) {   
            addUpdateAdjustmentInvoice(adjustmentDataWrapper.invoiceAdjustmentItem, adjLineItemList, UPDATE_RECORD);
        }
        return adjustmentDataWrapper.invoiceAdjustmentItem.Id;
    }

    public static void addUpdateAdjustmentInvoice(Invoice_Adjustment__c invoiceAdjustmentItem, List<Invoice_Adjustment_Line_Item__c> adjustmentLineItems, Integer type) {
        System.debug('addUpdateAdjustmentInvoice '+invoiceAdjustmentItem);
       
        try{
            LeaseWareUtils.setFromTrigger('INVOICE_ADJ_UPDATE');
            upsert invoiceAdjustmentItem;
            LeaseWareUtils.unsetTriggers('INVOICE_ADJ_UPDATE');
        }
        catch(DmlException ex){
            System.debug(ex);
            string errorMessage='';
            for ( Integer i = 0; i < ex.getNumDml(); i++ ){                
                errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
            }
            system.debug('errorMessage=='+errorMessage);
            LeaseWareUtils.createExceptionLog(ex,'Error inserting invoice adjustment'+invoiceAdjustmentItem,null, null,'Invoice Adjustment', true, '');
            throw new AuraHandledException(errorMessage);
        }catch(Exception ex){
            System.debug(ex);
            LeaseWareUtils.createExceptionLog(ex,'Error inserting invoice adjustment'+invoiceAdjustmentItem,null, null,'Invoice Adjustment', true, '');
            throw new AuraHandledException(ex.getMessage());
        }
        
        if(type == ADD_RECORD)
        {
            for(Invoice_Adjustment_Line_Item__c adjLI: adjustmentLineItems)
            {
                adjLI.Invoice_Adjustment__c = invoiceAdjustmentItem.id;
            }
        }

        try{
            if(String.isBlank(adjustmentLineItems[0].Invoice_Adjustment_Status__c) || 'Pending'.equals(adjustmentLineItems[0].Invoice_Adjustment_Status__c)){
                LeaseWareUtils.setFromTrigger('INVOICE_ADJ_LI_UPDATE');
                upsert adjustmentLineItems;
                LeaseWareUtils.unsetTriggers('INVOICE_ADJ_LI_UPDATE');
            }
            
        }
        catch(DmlException ex){
            System.debug(ex);
            string errorMessage='';
            for ( Integer i = 0; i < ex.getNumDml(); i++ ){                
                errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
            }
            system.debug('errorMessage=='+errorMessage);
            LeaseWareUtils.createExceptionLog(ex,'Error inserting invoice adjustment line items',null, null,'Invoice Adjustment Line Item', true, '');
            throw new AuraHandledException(errorMessage);
        }catch(Exception ex){
            System.debug(ex);
            LeaseWareUtils.createExceptionLog(ex,'Error inserting invoice adjustment line items',null, null,'Invoice Adjustment Line Item', true, '');
            throw new AuraHandledException(ex.getMessage());
        }
    }


    public class InvoiceAdjustmentWrapper{
        @AuraEnabled public String invoiceId {get; set;}
        @AuraEnabled public String invoiceName {get; set;}
        @AuraEnabled public Decimal invoiceAmount {get; set;}
        @AuraEnabled public Decimal invoiceBalanceDue {get; set;}
        @AuraEnabled public Decimal invoiceAdjustedAmount {get; set;}
        @AuraEnabled public String invoiceStatus {get; set;}
        @AuraEnabled public String invoicePaymentStatus {get; set;}
        @AuraEnabled public boolean isStatusDisabled {get; set;}
        @AuraEnabled public boolean isAprovalProcess {get; set;}

        @AuraEnabled public Invoice_Adjustment__c invoiceAdjustmentItem {get; set;}
        @AuraEnabled public List<InvoiceAdjustmentLineItemWrapper> invoiceAdjustmentLineItemList {get; set;}
        
        @AuraEnabled public Decimal adjustmentAmount {get; set;}

        public InvoiceAdjustmentWrapper() {
            invoiceAmount = 0;
            invoiceBalanceDue = 0;
            invoiceAdjustedAmount = 0;
            isStatusDisabled = false;
            isAprovalProcess = false;
        }
    }

    public class InvoiceAdjustmentLineItemWrapper{
        @AuraEnabled public String adjInvoiceItemId {get; set;}
        @AuraEnabled public String adjInvoiceLineItemId {get; set;}
        @AuraEnabled public String invoiceLineItemId {get; set;}
        @AuraEnabled public String invoiceLineItemName {get; set;}
        @AuraEnabled public Decimal invoiceLineItemAmount {get; set;}
        @AuraEnabled public Decimal invoiceLineItemBalanceDue {get; set;}
        @AuraEnabled public Decimal invoiceLineItemAdjustedTotal {get; set;}
        @AuraEnabled public Decimal invoiceLineItemAdjustedAmount {get; set;}
        public InvoiceAdjustmentLineItemWrapper() {
            invoiceLineItemAmount = 0;
            invoiceLineItemAdjustedTotal = 0;
            invoiceLineItemAdjustedAmount = 0;
            invoiceLineItemBalanceDue = 0;
        }
    }
}