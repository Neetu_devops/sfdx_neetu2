@isTest
public class TestMidTermLeaseUpdateController {
@isTest
    static void loadTestCases(){
        Counterparty__c newLessor = new Counterparty__c(name='Lessor For Contact Testing 1',Subsidiary_Entity__c = true);
    		insert newLessor;
        
        Bank__c newCP = new Bank__c(name='Counterparty For Contact Testing 1');
    		insert newCP;
        
        Account newA1 = new Account(name='Chase');
            insert newA1;
        
        Account newA2 = new Account(name='Genepoint');
            insert newA2;
        
        
        
        Aircraft__c aircraft = new Aircraft__c(
        Name= '1123 Aircraft',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        Engine_Type__c = 'V2524',
        MSN_Number__c = '1230',
        MTOW_Leased__c = 134455,
        Aircraft_Type__c = 'A320',
        Aircraft_Variant__c = '400',
        Date_of_Manufacture__c = System.Date.today());
        LeaseWareUtils.clearFromTrigger();
        LeaseWareUtils.TriggerDisabledFlag=true;
         insert aircraft;
         LeaseWareUtils.TriggerDisabledFlag=false;
        
        Lease__c lease = new Lease__c(Name='LeaseName', Aircraft__c=aircraft.Id,
                                  Month_Thresholds__c='0,6,12', PBH_Rates__c='145,125,100', 
                                  UR_Due_Day__c='10',Lease_Start_Date_New__c=system.today().addMonths(-2),
                                  Lease_End_Date_New__c=system.today().addYears(3), Rent_Date__c=system.today(),
                                  Rent_End_Date__c=system.today().addYears(5),
                                  Contractual_Delivery_Date__c =  system.today().addYears(2),
                                  Contractual_Redelivery_Date__c = system.today().addYears(4)   );
        LeaseWareUtils.TriggerDisabledFlag=true;
        insert lease;
        LeaseWareUtils.TriggerDisabledFlag=false;
        
        Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
                                            Name='Rent Schedule Fixed',
                                            Lease__c=lease.id,
                                            Rent_Type__c='Fixed Rent',
                                            Rent_Period__c='Monthly',
                                            Base_Rent__c=10000.00,
    										Start_Date__c=system.today().addYears(1), 
                                           	Rent_End_Date__c=system.today().addYears(2), 
                                            Invoice_Generation_Day__c='1',
                                            //Rent_Period_End_Day__c='',
    										Rent_Due_Type__c='Fixed',
                                            Rent_Due_Day__c=5,
    										Escalation__c=10,
    										Escalation_Month__c='January',
    										Pro_rata_Number_Of_Days__c='Actual number of days in month/year'
    										);
        insert MultiRentSch;
        RentQueue.GenerateRent(MultiRentSch.id,0);
        system.debug('lease id='+lease.id+'= MultiId='+MultiRentSch.id);
        
        
        Date today = System.today();
        Date exStartDAte = System.today().addYears(5);
        Date exEndDate = System.today().addYears(8);
        MidTermLeaseUpdateController.getLeaseInfo(lease.Id);
        MidTermLeaseUpdateController.getNamespacePrefix();
        MidTermLeaseUpdateController.getPickListValues('Lease_Extension_History__c', 'Type__c');
        MidTermLeaseUpdateController.fetchLookupData(newLessor.Id, 'Lessor');
        MidTermLeaseUpdateController.fetchLookupData(newCP.Id, 'Bank');
        MidTermLeaseUpdateController.createLeaseExtension(lease.Id, 'Lease Extension', today, exStartDAte, exEndDate, 12, false, 'Test for LeaseExtension with no rent schedule');
        MidTermLeaseUpdateController.createLeaseExtension(lease.Id, 'Lease Extension', today, exStartDAte, exEndDate, 12, true, 'Test for LeaseExtension with rent schedule');
        MidTermLeaseUpdateController.createLeaseAmendment(lease.Id, 'Lease Amendment', today, 'TEst Amendment', today, 'Test Amendmnet');
        MidTermLeaseUpdateController.createNovation(lease.Id, 'Novation', today, 'Novation1', today, 'Test Novation', newLessor.Id, 'Leaseworks Inc',null, newCP.Id, newCP.Id,newA1.Id,newA2.Id);
        MidTermLeaseUpdateController.createSideLetter(lease.Id,'Side Letter',today,'Test Side LEtter',today,'Test Side LEtter');    
        
         Lease_Extension_History__c newLH = new Lease_Extension_History__c(Name='Dummy', Lease__c=lease.id,
                                                                           Type__c='Lease Extension',
                                                                           Extension_Period_Start_date__c=exStartDAte,
                                                                           Extension_Period_End_date__c=exEndDate,
                                                                           Comments__c='Test for LeaseExtension with no rent schedule');
         insert newLH;
            
        
        MidTermLeaseUpdateController.saveFile(lease.id, 'file-Name', 'The File Upload', '.txt', '');
}
}