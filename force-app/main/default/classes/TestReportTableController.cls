@isTest
private class TestReportTableController {
    
    @isTest(seeAllData=true) static void getReportMetadata_validID(){
        Report aReport = [ SELECT Id, Name, developerName FROM Report LIMIT 1];
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        Account acc = new Account();
        acc.name = uniqueName ;
        insert acc;
        
        Test.startTest();
	ReportTableController.ReportDataWrapper wrapper = ReportTableController.getReportResults(aReport.developerName,acc.Id, true);
        Test.stopTest();

        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(wrapper.data);
        Map<String, Object> reportMetadata = (Map<String, Object>)m.get('reportMetadata');
        System.assertEquals( aReport.Name, reportMetadata.get('name'), 'The correct report should be returned' );
    }
}