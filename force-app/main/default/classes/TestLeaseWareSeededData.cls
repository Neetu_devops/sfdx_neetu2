/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestLeaseWareSeededData {

    static testMethod void SeedDataTest() {
        // TO DO: implement unit test

        LeaseWareSeededData.SeedThrustSettingLookup();
        LeaseWareSeededData.SeedLWSetup();
        LeaseWareSeededData.SeedCustomLookup();
        LeaseWareSeededData.SeedRecommendationFactorForRiskAssetView();
        LeaseWareSeededData.Seed_LeaseWorksSettings_Setup();
        LeaseWareSeededData.SeedSequenceNumber();
        LeaseWareSeededData.SeedMappingObject();

    }
    @IsTest 
    static void testRiskAsssetViewCustomLookup(){
       
        LeaseWareSeededData.SeedCustomLookup();
        LeasewareSeededData.SeedCustomLookup();
        List<Custom_Lookup__c> lookupList =[select id,Name,Lookup_Type__c, Sub_LookupType__c,Details__c,Description__c,Sub_LookupType2__c,Sub_LookupType3__c
        from Custom_Lookup__c where Lookup_Type__c='ASSET_SCORECARD' and  Active__c = true
        ];
        system.assertEquals(24, lookupList.size(), 'Lookup Size added as a part of Asset ScoreCard');
    }

    @isTest
    static void testSeedRecommendationFactorForRiskAssetView(){
        LeaseWareSeededData.SeedRecommendationFactorForRiskAssetView();
        LeaseWareSeededData.SeedRecommendationFactorForRiskAssetView();

         List<Recommendation_Factor__c> rfFactorAdded =[select id,Name from Recommendation_Factor__c where Type__c='Asset Scorecard'];
        system.assertEquals(4, rfFactorAdded.size(), 'Verify already added Weightages are not inserted again');


    }

}