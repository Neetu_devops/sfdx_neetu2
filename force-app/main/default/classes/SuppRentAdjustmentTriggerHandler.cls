public with sharing class SuppRentAdjustmentTriggerHandler implements ITrigger {

	private final string triggerBefore = 'SuppRentAdjustmentTriggerHandlerBefore';
	private final string triggerAfter = 'SuppRentAdjustmentTriggerHandlerAfter';

	public SuppRentAdjustmentTriggerHandler()
	{
	}

	public void bulkBefore()
	{
	}

	public void bulkAfter()
	{
	}

	public void beforeInsert()
	{
		system.debug('SuppRentAdjustmentTriggerHandler.beforeInsert(+)'); 
		Supplemental_Rent_Adjustment__c[] newFundAdjstments = (list<Supplemental_Rent_Adjustment__c>)trigger.new;
		LeaseWare_SequenceId__c adjstmentStartNumber;
		Set<Id> setSRIds = new Set<Id>();
        Map<Id, Assembly_MR_Rate__c> mapSRs = new   Map<Id, Assembly_MR_Rate__c> ();
        List<Supplemental_Rent_Adjustment__c> listToUpdate = new  List<Supplemental_Rent_Adjustment__c>();
        for (Supplemental_Rent_Adjustment__c curAdjstmnt :newFundAdjstments ) {
            setSRIds.add(curAdjstmnt.Supplemental_Rent__c);
        }

		for(Assembly_MR_Rate__c curSR : [Select id, name ,Assembly_Lkp__r.Type__c,Assembly_Lkp__r.Attached_Aircraft__r.MSN_Number__c from Assembly_MR_Rate__c
										 where id in:setSRIds]){
			mapSRs.put(curSR.Id,curSR);
        }

		adjstmentStartNumber = LeaseWare_SequenceId__c.getValues('Sequence_Number_Adjustment');
		if (adjstmentStartNumber != null && adjstmentStartNumber.Active__c) {
			//Get Sequence Number
			try {
				//Row is Locked for UPDATE in getSequnceNumber through out current transaction.
				//Other users trying to do SELECT UPDATE will receive error.
				adjstmentStartNumber = LeaseWareUtils.getSequenceNumber('SuppRentAdjustment');
			}catch (QueryException ex) {
				if(ex.getMessage().contains('UNABLE_TO_LOCK_ROW') )
					newFundAdjstments[0].addError('SuppRentAdjustment Id generation in currently in progress. Please click Save again.');
			}
			Integer sequenceNumber=-1;

			//If Recent Sequence number not there , take Initial value.
			sequenceNumber=adjstmentStartNumber.Sequence_Number_New__c == null ? adjstmentStartNumber.Initial_Value__c.intValue() : adjstmentStartNumber.Sequence_Number_New__c.intValue();
			//Assign Sequence Numbers
			for (Supplemental_Rent_Adjustment__c curFundAdjstmnt :newFundAdjstments ) {
				curFundAdjstmnt.Sequence_Number__c = sequenceNumber++;
				Assembly_MR_Rate__c suppRent = mapSRs.get(curFundAdjstmnt.Supplemental_Rent__c);
                curFundAdjstmnt.Name = suppRent.Assembly_Lkp__r.Type__c +'-'+ suppRent.Assembly_Lkp__r.Attached_Aircraft__r.MSN_Number__c  +'-'+curFundAdjstmnt.Sequence_Number__c ;   
                listToUpdate.add(curFundAdjstmnt);
			}
			try {
				//Update Custom Setting with Latest sequence number.
				LeaseWareUtils.updateSequenceNumber('SuppRentAdjustment',sequenceNumber);
			}catch (DmlException ex) {
				//Both Query Exception and dmlException should be handled
				if(ex.getMessage().contains('The record you are attempting to edit, or one of its related records, is currently being modified by another user'))
					newFundAdjstments[0].addError('SuppRentAdjustment Id generation in currently in progress. Please click Save again.');
			}
		}
		system.debug('SuppRentAdjustmentTriggerHandler.beforeInsert(-)'); 
	}

	public void beforeUpdate()
	{
		if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
		system.debug('SuppRentAdjustmentTriggerHandler.beforeUpdate(+)');   
        system.debug('SuppRentAdjustmentTriggerHandler.beforeUpdate(-)'); 
	}

	public void beforeDelete()
	{
	}

	public void afterInsert()
	{
		if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
		system.debug('SuppRentAdjustmentTriggerHandler.afterInsert(+)');
		createSupplementalRentTransaction();
		system.debug('SuppRentAdjustmentTriggerHandler.afterInsert(-)');
	}

	public void afterUpdate()
	{

		if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
		system.debug('SuppRentAdjustmentTriggerHandler.afterUpdate(+)');
		createSupplementalRentTransaction();
		system.debug('SuppRentAdjustmentTriggerHandler.afterUpdate(-)');
	}

	public void afterDelete()
	{
		system.debug('SuppRentAdjustmentTriggerHandler.afterDelete(+)');
		createSupplementalRentTransaction();
		system.debug('SuppRentAdjustmentTriggerHandler.afterDelete(-)');
	}

	public void afterUnDelete()
	{
	}

	public void andFinally()
	{
	}

	private void createSupplementalRentTransaction(){

		List<Supplemental_Rent_Adjustment__c> listSuppRentAdjstment = (trigger.isDelete ? trigger.old : trigger.new);
		Map<Id,Assembly_MR_Rate__c> mapSupplementalRents = new  Map<Id,Assembly_MR_Rate__c> ();
		List<Supplemental_Rent_Transactions__c> listSupplementalRentTransactions = new  List<Supplemental_Rent_Transactions__c> ();
		Set<Id> setSRIds = new Set<Id>();
			for(Supplemental_Rent_Adjustment__c curRec: listSuppRentAdjstment) {
				Assembly_MR_Rate__c curSR  = new Assembly_MR_Rate__c();
				curSR.Id = curRec.Supplemental_Rent__c;
     			curSR.Snapshot_Event_Record_ID__c = 'A'+CurRec.Id;
				mapSupplementalRents.put(curSR.Id,curSR);
			}
			if(mapSupplementalRents.size()>0) {
				update mapSupplementalRents.values();
			}
	}
}
