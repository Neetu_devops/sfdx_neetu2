@isTest
public class TestLightningDataTableController {
    @isTest
    public static void testGetOustandingBills(){
        Date lastDayOfMonth = System.today().addMonths(2).toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        
        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        Lease__c leaseRecord=new Lease__c();
        leaseRecord.Security_Deposit__c=200;
        leaseRecord.Lessee__c=operatorRecord.Id;
        leaseRecord.Lease_Start_Date_New__c=minStartDate;
        leaseRecord.Lease_End_Date_New__c=lastDayOfMonth;
        insert leaseRecord;
        
        Invoice_External__c invoiceStages=new Invoice_External__c();
        invoiceStages.Name='test invoice external';
        invoiceStages.Period_From__c=System.today();
        invoiceStages.Period_To__c=System.today().addMonths(1);
        invoiceStages.Paid_Date__c=System.today().addDays(10);
        invoiceStages.MSN_ESN__c='test';
        invoiceStages.Outstanding_Amount__c=200;
        invoiceStages.Paid_Amount__c=250;
        invoiceStages.Invoice_Amount__c=250;
        invoiceStages.Issue_Date__c=System.today().addDays(3);
        invoiceStages.Invoice_Type__c='Interest';
        invoiceStages.Lease__c=leaseRecord.Id;
        insert invoiceStages;
        
        Test.startTest();
        LightningDataTableController.getOustandingBills(0,'');
        LightningDataTableController.getPaidBills(0,'');
        Test.stopTest();
        LightningDataTableController.getInvoicePdf(leaseRecord.Id);
    }
}