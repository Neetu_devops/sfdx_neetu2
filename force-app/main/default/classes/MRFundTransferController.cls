public class MRFundTransferController {

    @AuraEnabled
    public static sObject getLeaseRecord(Id recordId){
        String objectApiName = recordId.getSObjectType().getDescribe().getName();
        String qry = 'Select Id,Name from ' + objectApiName + ' where Id =:recordId';
        return database.query(qry);
    }
    
    //Wrapper class contains AssemblyMRInfo Record details
    public class AssemblyMRInfoWrapper {
        
        @AuraEnabled
        public Id recordId{get; set;}
        @AuraEnabled
        public String recordName{get; set;}
        @AuraEnabled
        public Decimal availableAmount{get; set;}
        @AuraEnabled
        public Id leaseId{get; set;}
        @AuraEnabled
        public String serialNumber{get; set;}
        
        public AssemblyMRInfoWrapper(Id recordId, String recordName, Decimal availableAmount, Id leaseId, String serialNumber){
            this.recordId = recordId;
            this.recordName = recordName;
            this.availableAmount = availableAmount;
            this.leaseId = leaseId;
            this.serialNumber = serialNumber;
        }
    }
    
    
    public class MRFundContainerWrapper {
        @AuraEnabled
        public AssemblyMRInfoWrapper sourceRecord{get; set;}
        @AuraEnabled
        public Decimal amountPercent{get; set;}
        @AuraEnabled
        public Decimal transferAmount{get; set;}
        @AuraEnabled
        public AssemblyMRInfoWrapper targetRecord{get; set;}
        @AuraEnabled
        public Id sourceLeaseId{get; set;}
        @AuraEnabled
        public Id targetLeaseId{get; set;}
        @AuraEnabled
        public String sourceLeaseName{get; set;}
        @AuraEnabled
        public String targetLeaseName{get; set;}
        @AuraEnabled
        public Decimal sourceAmountAfterTransfer{get; set;}
        @AuraEnabled
        public Decimal targetAmountAfterTransfer{get; set;}
        
        public MRFundContainerWrapper(AssemblyMRInfoWrapper sourceRecord, Decimal amountPercent, Decimal transferAmount, Id sourceLeaseId, Id targetLeaseId, String sourceLeaseName, String targetLeaseName) {
            this.sourceRecord = sourceRecord;
            if(amountPercent == null) this.amountPercent = 0;
            else this.amountPercent = amountPercent;
            if(transferAmount == null) this.transferAmount = 0;
            else this.transferAmount = transferAmount;
            this.sourceLeaseId = sourceLeaseId;
            this.targetLeaseId = targetLeaseId;
            this.sourceLeaseName = sourceLeaseName;
            this.targetLeaseName = targetLeaseName;
        }
    }
    
    public class MrFundTransferWrapper {
        
        @AuraEnabled
        public List<MRFundContainerWrapper> sourceObjectRecords{get; set;}
        @AuraEnabled
        public Map<Id, AssemblyMRInfoWrapper> mapTargetObjectRecord{get; set;}
        @AuraEnabled
        public String roundUpOrDown{get; set;}// Anjani-Sagar : Not using after change of this option from lease to Supplemental Rent
        
    }
    
    @AuraEnabled
    public static MrFundTransferWrapper getRecordData(Id sourceId, Id targetId, String sourceLeaseName, String targetLeaseName, Date selectedDate) {
        
        MrFundTransferWrapper data = new MrFundTransferWrapper();
        
        List<MRFundContainerWrapper> childs = new List<MRFundContainerWrapper>();
        
        List<Id> ids = new List<Id>{sourceId, targetId};
        
        Map<Id, AssemblyMRInfoWrapper> mapData = new Map<Id, AssemblyMRInfoWrapper>(); 
        
        String dateOfTransfer = String.valueOf(selectedDate);
        List<Assembly_MR_Rate__c> assemblyRates = [SELECT Id, Name, Lease__c, Lease__r.Lease_Start_Date_New__c, 
                                                   Lease__r.Lease_End_Date_New__c, Lease__r.Serial_Number__c, 
                                                   Lease__r.Aircraft__c,
                                                   
                                                   (select id, Running_Balance_Cash_Collected__c from Supplemental_Rent_Transactions__r 
                                                   where Transaction_Date__c <=: dateOfTransfer 
                                                   order by Sequence_Number__c desc limit 1)
                                                   
                                                   FROM Assembly_MR_Rate__c 
                                                   WHERE Lease__c = :ids
                                                   AND RecordTypeId=: Schema.SObjectType.Assembly_MR_Rate__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId()];
        

        for(Assembly_MR_Rate__c aRate : assemblyRates) {
            
            Decimal availableAmount = 0;
            
            If(aRate.Supplemental_Rent_Transactions__r.size() > 0){
                availableAmount = aRate.Supplemental_Rent_Transactions__r[0].Running_Balance_Cash_Collected__c ;
            }
            
            AssemblyMRInfoWrapper assemblyRecord = new AssemblyMRInfoWrapper(aRate.Id, aRate.Name, availableAmount, aRate.Lease__c, aRate.Lease__r.Serial_Number__c);
            
            //If user select same lease in source and target
            if(sourceId == targetId) {
                mapData.put(aRate.Id, assemblyRecord);
                data.mapTargetObjectRecord = mapData;
                MRFundContainerWrapper childData = new MRFundContainerWrapper(assemblyRecord,0.00,0,sourceId,targetId,sourceLeaseName,targetLeaseName);
                childData.sourceAmountAfterTransfer = availableAmount ;
                childs.add(childData);
            }
            //To fill map of target record for dropdown
            else if(aRate.Lease__c == ids[1]) {
                mapData.put(aRate.Id, assemblyRecord);
                data.mapTargetObjectRecord = mapData;
                
            }
            //To fill list of source record for table
            else {
                MRFundContainerWrapper childData = new MRFundContainerWrapper(assemblyRecord,0.00,0,sourceId,targetId,sourceLeaseName,targetLeaseName);
                childData.sourceAmountAfterTransfer = availableAmount;
                childs.add(childData);
            }
        }
        
        if(childs.size() > 0) data.sourceObjectRecords = childs;
       
        return data;
    }
    

    @AuraEnabled
    public static Id saveData(String recordToUpdate, Date transferDate) {
        
        MrFundTransferWrapper wrapperData = new MrFundTransferWrapper();
        wrapperData.sourceObjectRecords = (List<MRFundContainerWrapper>)System.JSON.deserialize(recordToUpdate, List<MRFundContainerWrapper>.class);
        
        if(!wrapperData.sourceObjectRecords.isEmpty()) {
            
            Id sourceLeaseId = wrapperData.sourceObjectRecords[0].sourceLeaseId;
            Id targetLeaseId = wrapperData.sourceObjectRecords[0].targetLeaseId;
            
            MR_Fund_Transfer__c mrFundTransfer = new MR_Fund_Transfer__c(Name = 'MR Fund Transfer-'+transferDate.format(), Date__c = transferDate, Source_Lease__c = sourceLeaseId, Target_Lease__c = targetLeaseId, Status__c = 'Pending');
                       
            insert mrFundTransfer;
            
            List<Assembly_MR_Fund_Transfer__c> assemblyTransferRecords = new List<Assembly_MR_Fund_Transfer__c>();
            
            for(MRFundContainerWrapper child : wrapperData.sourceObjectRecords) {
                
                if(child.targetRecord.recordId != null) {
                    Assembly_MR_Fund_Transfer__c assemblyTransfer = new Assembly_MR_Fund_Transfer__c(MR_Fund_Transfer__c = mrFundTransfer.Id, 
                                                                                                     Source_Assembly_MR_Lookup__c = child.sourceRecord.recordId, 
                                                                                                     Target_Assembly_MR_Lookup__c = child.targetRecord.recordId,
                                                                                                     Source_Available_Amount__c = child.sourceRecord.availableAmount,
                                                                                                     Target_Available_Amount__c = child.targetRecord.availableAmount,
                                                                                                     Transfer_Amount__c = child.transferAmount,
                                                                                                     Transfer_Amt_Percentage__c = child.amountPercent);
                    assemblyTransferRecords.add(assemblyTransfer);
                }
            }
            
            if(assemblyTransferRecords.size() > 0) {
                insert assemblyTransferRecords;
            }

            return mrFundTransfer.Id;
        }
        else return null;
    }
    
    @AuraEnabled
    public static MrFundTransferWrapper getSavedData(Id recordId) {
        
        MrFundTransferWrapper data = new MrFundTransferWrapper();
        List<MRFundContainerWrapper> childs = new List<MRFundContainerWrapper>();
        MR_Fund_Transfer__c mrFund = [SELECT Source_Lease__c, Source_Lease__r.Name, Source_Lease__r.Serial_Number__c, 
                                      Target_Lease__c, Target_Lease__r.Name,  Target_Lease__r.Serial_Number__c FROM 
                                      MR_Fund_Transfer__c WHERE Id =: recordId LIMIT 1];
        
        List<Assembly_MR_Fund_Transfer__c> assemblyFunds = [SELECT Source_Assembly_MR_Lookup__c, 
                                                            Target_Assembly_MR_Lookup__c, 
                                                            Source_Assembly_MR_Lookup__r.Name, 
                                                            Target_Assembly_MR_Lookup__r.Name, 
                                                            Source_Available_Amount__c, Target_Available_Amount__c, 
                                                            Transfer_Amount__c, Transfer_Amt_Percentage__c 
                                                            FROM Assembly_MR_Fund_Transfer__c WHERE 
                                                            MR_Fund_Transfer__c =: recordId];
        
        for(Assembly_MR_Fund_Transfer__c assemblyRecord : assemblyFunds) {
            
            AssemblyMRInfoWrapper sourceAssemblyRate = new AssemblyMRInfoWrapper(assemblyRecord.Source_Assembly_MR_Lookup__c,
                                                                 assemblyRecord.Source_Assembly_MR_Lookup__r.Name,
                                                                 assemblyRecord.Source_Available_Amount__c,
                                                                 mrFund.Source_Lease__c,
                                                                 mrFund.Source_Lease__r.Serial_Number__c);
            
            AssemblyMRInfoWrapper targetAssemblyRate = new AssemblyMRInfoWrapper(assemblyRecord.Target_Assembly_MR_Lookup__c,
                                                                 assemblyRecord.Target_Assembly_MR_Lookup__r.Name,
                                                                 assemblyRecord.Target_Available_Amount__c,
                                                                 mrFund.Target_Lease__c,
                                                                 mrFund.Target_Lease__r.Serial_Number__c);
            
            MRFundContainerWrapper child = new MRFundContainerWrapper(sourceAssemblyRate, assemblyRecord.Transfer_Amt_Percentage__c,
                                                  assemblyRecord.Transfer_Amount__c, mrFund.Source_Lease__c, 
                                                  mrFund.Target_Lease__c, mrFund.Source_Lease__r.Name, mrFund.Target_Lease__r.Name);
            
            child.targetRecord = targetAssemblyRate;
            child.sourceAmountAfterTransfer = assemblyRecord.Source_Available_Amount__c - assemblyRecord.Transfer_Amount__c;
            child.targetAmountAfterTransfer = assemblyRecord.Target_Available_Amount__c + assemblyRecord.Transfer_Amount__c;
        	childs.add(child);
        }
        
        if(childs.size() > 0) data.sourceObjectRecords = childs;
        
        return data;
    }
    
}