@isTest
public class TestLOIComponentController {
    @testSetup
    public static void setup() {
        
        Counterparty__c counterParty = new Counterparty__c(
            Name = 'Leassor Test');
        insert counterParty;
        
        Operator__c operator = new Operator__c (
            Name = 'Airline Test1');
        insert operator;
        
        Project__c project = new Project__c(Name = 'PROJECT Test',
                                            Economic_Closing_Date__c = System.today(),
                                            Project_Deal_Type__c = 'Sale' );
        insert project;
        
        Marketing_Activity__c marketingActivity = new Marketing_Activity__c(
            Name = 'Test MA',
            Prospect__c = operator.Id,
            Lessor__c = counterParty.Id,
            Deal_Type__c = 'Sale',
            Inactive_MA__c = False,
            Campaign__c = project.Id,
            Deal_Status__c = 'LOI');
        insert marketingActivity;
        
        LOI_Revision__c loi = new LOI_Revision__c(Name='Test Loi Revision',LOI_Comments__c='test comment',Deal__c=marketingActivity.Id);
        insert loi;
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        List<String> documentIds = new List<String>();
        documentIds.add(documents[0].Id);
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = loi.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        
        Deal_Team__c deal = new Deal_Team__c(Name='test deal',Marketing_Activity__c =marketingActivity.Id);
        insert deal;
        
        Profile pf= [Select Id from profile where Name='System Administrator'];
        User uu=new User(firstname = 'abcxyz', 
                         lastName = 'XYZ', 
                         email =   'abc@test.org', 
                         Username = 'abcxyz@test.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = 'abc', 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        insert uu;
        
        Deal_Team_Member__c dealmember = new Deal_Team_Member__c(Deal_Team__c=deal.Id,Department__c='Legal',User__c =uu.Id );
        insert dealmember;
    }
    @isTest 
    public static void testLOIMethods(){
        LOI_Revision__c loi = [Select id from LOI_Revision__c];
        Marketing_Activity__c deal = [Select id from Marketing_Activity__c];
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        List<String> documentIds = new List<String>();
        documentIds.add(documents[1].Id);
        ContentVersionDetailController.getContentVersionDetails(contentVersion.Id);
        LOIComponentController.saveLoiRecord(loi.Id,documentIds);
        LOIComponentController.getLoiRevisionData(deal.Id,true);
        LOIComponentController.deleteLoiById(loi.Id);
        LOIComponentController.deleteUploadedRecord(documents[1].Id);
    }
    @isTest
    public static void testsavecomments(){
        LOI_Revision__c loi = [Select id from LOI_Revision__c];
        LOIComponentController.saveComments(loi.Id,'abc');
    }
    @isTest
    public static void testUpdateContentVersion(){
        LOI_Revision__c loi = [Select id from LOI_Revision__c];
        Marketing_Activity__c deal = [Select id from Marketing_Activity__c];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        try{
            LOIComponentController.updateContentVersion(documents[0].Id,deal.Id);
        }catch(Exception ex){
            System.debug('Error occured while testing.');
        }
    }
}