public class BatchDeleteWorldFleet implements 
Database.Batchable<sObject>, Database.Stateful {
    Integer worldFleetRecordsDeleted = 0; 
    List<String> worldFleetRecordsNotDeleted = new List<String>(); 
    Integer fleetSummaryRecordsDeleted = 0; 
    List<String> fleetSummaryRecordsNotDeleted = new List<String>();
    String exceptionMessage = '';
    String logMessageString ='';

    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            [select id,Name,Aircraft_Operator__c from Global_Fleet__c order by Aircraft_Operator__c ]
        );
    }
    
    public void execute(Database.BatchableContext BC, List<Global_Fleet__c> worldFleetList)
    {
      
       
        Savepoint spStart = Database.setSavepoint();
        try{
            executeHelper(worldFleetList);
        }catch(Exception ex){
            Database.rollback(spStart);
            exceptionMessage += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
            system.debug('Exception : ' + ex +  + '===' +exceptionMessage);
        }

        
    } 
    
    public void finish(Database.BatchableContext BC) {
        
        if(String.isEmpty(exceptionMessage)){
        
        logMessageString = logMessageString +  '\n================================================================================================';
        logMessageString += '<BR>' +'\n No of world fleet records deleted = '+ worldFleetRecordsDeleted +'\n';
        logMessageString += '<BR>' +'\n No of  fleet summary records deleted = '+ fleetSummaryRecordsDeleted +'\n';
        
        logMessageString +=  '\n================================================================================================';
       
        if(worldFleetRecordsNotDeleted.size()>0  ||  fleetSummaryRecordsNotDeleted.size()>0){
            String errIdsForWF = '';
            String errIdsForFS = '';
            for(String err :worldFleetRecordsNotDeleted) {
                errIdsForWF = String.isEmpty(errIdsForWF)? err : (errIdsForWF + ',' + err +'<BR>');
                    System.debug(errIdsForWF);
            }
            if(String.isNotEmpty(errIdsForWF)) logMessageString+='<BR>' +'\n Delete Failed for World Fleet Records ---- \n ' + errIdsForWF +'\n';
            
            
            for(String err :fleetSummaryRecordsNotDeleted) {
                errIdsForFS = String.isEmpty(errIdsForFS)? err : (errIdsForFS + ',' + err);
                    System.debug(err);
            }
            if(String.isNotEmpty(errIdsForFS)) logMessageString +='<BR>' +'\n Delete Failed for Fleet Summary records ---\n ' + errIdsForFS ;
            
            
            
        }

        LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
        LeaseWareStreamUtils.emailWithAttachedLog((new String[] { UserInfo.getUserEmail() }) ,'Deletion of World Fleet and Fleet Summary' ,logMessageString,null);
        LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
    }else{
        LeaseWareUtils.sendEmail('Delete World Fleet Update', 'There was an unexpected error deleting World Fleet and Summary . Please contact your LeaseWorks System Administrator. \n \n \n  <br/><br/>Reason :'+ exceptionMessage, UserInfo.getUserEmail());
    }
        
    }
    @AuraEnabled
    public static void deleteFleetRecord(){
        Database.executeBatch(new BatchDeleteWorldFleet(),2000);
    }
    @AuraEnabled
    public static string getNamespacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    
    private void executeHelper(List<Global_Fleet__c> worldFleetList){
     
        map<Id,Global_Fleet__c> mapWF = new map<Id,Global_Fleet__c>();
        Set<id> operatorIds = new Set<Id>();
        for(Global_Fleet__c fleet : worldFleetList){
            mapWF.put(fleet.Id,fleet);
        }
        
        Database.DeleteResult[]  delWorldFleetList = Database.Delete(worldFleetList ,false);
        for (Database.DeleteResult dr : delWorldFleetList) {
            system.debug('delete result---'+dr);
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted: ' + dr.getId());
                Global_Fleet__c fleet = mapWF.get(dr.getId());
                system.debug('fleet-------'+fleet);
                if(fleet!=null && fleet.Aircraft_Operator__c !=null){
                    operatorIds.add(fleet.Aircraft_Operator__c);
                }
               
                worldFleetRecordsDeleted++;
            }else {
                // Operation failed, so get all errors                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Delete did not happen for id---: '+ dr.getId()+'----' + err.getFields());
                    worldFleetRecordsNotDeleted.add(dr.getId() + '-'+ err.getMessage());
                }
            }
        }
       
        
        if(operatorIds.size()>0){  
            List<Fleet_Summary__c> fleetSumaries = [select Id from Fleet_Summary__c where Operator__c IN : operatorIds];
            system.debug('fleetSumaries-------'+fleetSumaries.size());
            if(fleetSumaries.size()>0){
                Database.DeleteResult[]  fleetSummariesList = Database.Delete(fleetSumaries ,false);
                for (Database.DeleteResult dr : fleetSummariesList) {
                    if (dr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully deleted: ' + dr.getId());
                        fleetSummaryRecordsDeleted++;
                    }else{
                        // Operation failed, so get all errors                
                        for(Database.Error err : dr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Delete did not happen for id---: '+ dr.getId()+'----' + err.getFields());
                            fleetSummaryRecordsNotDeleted.add(dr.getId() + '-'+ err.getMessage());
                        }
                    }
                }
            }    
        }
   
    }
    
    
}