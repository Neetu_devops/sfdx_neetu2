public class AircraftUtilizationController{
    @AuraEnabled
    public static AssemblyWrapper getUtilizationRelatedData(String assetId){
	
	// Added By AKG for Testing of SparklineChartComponent in Managed Package : 21 April 2020

        List<Utilization_Report__c> utilList = [Select Id from Utilization_Report__c where Aircraft__c =: assetId order by CreatedDate desc];
        List<ComponentWrapper> cwList = new List<ComponentWrapper>();
        AssemblyWrapper aw = new AssemblyWrapper();
        if(utilList.size() > 0){
            String utilizationId = utilList[0].Id;
            Map<String, List<Decimal>> fhTrendByAssemblyId = new Map<String, List<Decimal>>();
        
        //If Utilization is created
        if(utilizationId != null) {
            Utilization_Report__c utilization = [SELECT Id,Name, Month_Ending__c, Aircraft__r.Lease__c,Aircraft__r.Lease__r.Name,
                                                 Aircraft__r.Lease__r.Lessee__r.Name, Aircraft__r.Lease__r.Lessee__c, Aircraft__c, 
                                                 Prorate__c,Date_of_Submission__c,Aircraft__r.Name,End_Of_Lease_Adjustments_Only_F__c,
                                                 Status__c,Type__c, TSN_At_Month_End__c, CSN_At_Month_End__c  
                                                 FROM Utilization_Report__c 
                                                 WHERE Id =: utilizationId LIMIT 1 ]; 
            
            Date endDate = utilization.Month_Ending__c;
            Date startDate = endDate.addDays(1).addYears(-1).addDays(-1); 
            
            
            Set<String> setAssemblies = new Set<String>();
            //List of Assembly Utilization List Item
            List<Utilization_Report_List_Item__c> lstUtilizationReportItem = [SELECT Id,Name,Running_Hours_During_Month__c, 
                                                                              Constituent_Assembly__c,Assembly_MR_Info__r.Assembly_Event_Info__r.Event_type__c,
                                                                              Constituent_Assembly__r.Type__c,Y_Hidden_FH_String__c, Comments__c,
                                                                              Constituent_Assembly__r.Asset__c,Effective_Rate_Final__c,
                                                                              Cycles_During_Month__c,Assembly_MR_Info__c,Assembly_MR_Info__r.Transactions_Total_Cash__c,
                                                                              Constituent_Assembly__r.Serial_Number__c,                                                                              
                                                                              Constituent_Assembly__r.Attached_Aircraft__c,
                                                                              Constituent_Assembly__r.Assembly_Display_Name__c,
                                                                              Net_Units__c, Rate_Beyond_Threshold_F__c,
                                                                              TSN_At_Month_End__c, Report_Item__c,Utilization_Report__c,
                                                                              CSN_At_Month_End__c,Assembly_MR_Info__r.RecordType.Name,
                                                                              Assembly_Utilization__r.APU_Hours_At_Month_End_F__c,
                                                                              Assembly_Utilization__r.APU_Cycles_At_Month_End_F__c,
                                                                              Assembly_Utilization__r.Y_Hidden_APU_FH_String__c,
                                                                              Assembly_Utilization__r.APU_Cycles__c,
                                                                              Assembly_Utilization__r.APU_Hours__c,
                                                                              Assembly_Utilization__r.Y_Hidden_FH_String__c,
                                                                              Assembly_Utilization__r.TSN_At_Month_End__c,
                                                                              Assembly_Utilization__r.CSN_At_Month_End__c,
                                                                              Total_Maintenance_Reserve__c, 
                                                                              Effective_Unit__c,Derate__c,
                                                                              Threshold_Units__c,Threshold_Amount__c,
                                                                              Rate_Up_To_Threshold__c,Utilization_Report__r.Month_Ending__c,
                                                                              Utilization_Report__r.Aircraft__r.Lease__c,
                                                                              Monthly_Threshold_Guaranteed__c
                                                                              FROM 
                                                                              Utilization_Report_List_Item__c 
                                                                              WHERE 
                                                                              Utilization_Report__c =: utilizationId AND Constituent_Assembly__c!=null];
            
            Set<String> assemblyUtilIds = new Set<String>();
            
            for(Utilization_Report_List_Item__c urli : lstUtilizationReportItem){
                assemblyUtilIds.add(urli.Assembly_Utilization__c);
            }
            List<Assembly_Utilization__c> assemblyUtilList = [SELECT Id, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,
                                                              SystemModstamp, Utilization_Report__c, For_The_Month_Ending__c, 
                                                              Constituent_Assembly__c, Type_F__c, Status_F__c, Comment__c,
                                                              Running_Hours_During_Month__c, Cycles_During_Month__c, TSN_At_Month_Start__c, 
                                                              CSN_At_Month_Start__c, TSN_At_Month_Start_F__c, CSN_At_Month_Start_F__c, 
                                                              TSN_At_Month_End__c, CSN_At_Month_End__c, FH_FC_Ratio__c, APU_Hours__c, 
                                                              APU_Hours_At_Month_Start__c,Constituent_Assembly__r.Serial_Number__c,
                                                              Constituent_Assembly__r.Type__c,Constituent_Assembly__r.Attached_Aircraft__c,
                                                              Constituent_Assembly__r.Asset__c,Constituent_Assembly__r.Assembly_Display_Name__c,
                                                              APU_Hours_At_Month_End_F__c, APU_Cycles__c, 
                                                              APU_Cycles_At_Month_Start__c, APU_Cycles_At_Month_End_F__c, Derate__c, 
                                                              Engine_Thrust__c, Is_Created_By_Trigger__c, APU_Cycles_At_Month_Start_F__c, 
                                                              APU_Hours_At_Month_Start_F__c, Y_Hidden_FH_String__c, Y_Hidden_APU_FH_String__c,
                                                              Y_Hidden_Running_Hours__c, Y_Hidden_Running_Cycles__c, TSI__c, CSI__c, 
                                                              TSN_As_Per_Airline_Records__c, CSN_As_Per_Airline_Records__c, Y_Hidden_Prev_Derate__c,
                                                              Unknown_FH_FC__c, Y_Hidden_Unknown_FH_FC__c FROM Assembly_Utilization__c where Id NOT IN:assemblyUtilIds
                                                              AND Utilization_Report__c =: utilizationId order by Constituent_Assembly__r.Type__c];
            
            
            if(!assemblyUtilList.isEmpty()){
                for(Assembly_Utilization__c au : assemblyUtilList){
                    Utilization_Report_List_Item__c urli = new Utilization_Report_List_Item__c(); 
                    urli.Y_Hidden_FH_String__c = au.Y_Hidden_FH_String__c;
                    urli.Running_Hours_During_Month__c = au.Running_Hours_During_Month__c;
                    urli.Cycles_During_Month__c = au.Cycles_During_Month__c;
                    urli.Assembly_Utilization__r = au;
                    urli.Assembly_Utilization__c = au.Id;
                    urli.Derate__c = au.Derate__c;
                    urli.Constituent_Assembly__r = au.Constituent_Assembly__r;
                    urli.Constituent_Assembly__c = au.Constituent_Assembly__c;
                    //urli.TSN_At_Month_End__c = au.TSN_At_Month_End__c;
                    //urli.CSN_At_Month_End__c = au.CSN_At_Month_End__c;
                    lstUtilizationReportItem.add(urli);
                }
            }
            
            
            if(lstUtilizationReportItem.size() > 0){
                
                
                //Creating Data for Sparkline Charts
                Set<String> assemblyIds = new Set<String>();                
                fhTrendByAssemblyId = new Map<String, List<Decimal>>();
                Map<String, Decimal> airframeFHById = new Map<String, Decimal>();
                
                Set<String> assemblySortingSet = new Set<String> { 'Airframe', 'Engine', 'Propeller', 'APU', 'LDG', 'Others' };
                    Map<String, Utilization_Report_List_Item__c>  urMap = new Map<String, Utilization_Report_List_Item__c>();
                List<Utilization_Report_List_Item__c> urList1 = new List<Utilization_Report_List_Item__c>();
                if(lstUtilizationReportItem != null){
                    for(String key : assemblySortingSet){
                        String tempKey = '';
                        for(Utilization_Report_List_Item__c assemblyUtilization : lstUtilizationReportItem){
                            tempKey = assemblyUtilization.Constituent_Assembly__r.Type__c +'-'+assemblyUtilization.Assembly_Utilization__c;
                            if(assemblyUtilization.Constituent_Assembly__r.Type__c.contains(key)){
                                urMap.put(tempKey, assemblyUtilization);
                                urList1.add(assemblyUtilization);
                            }
                        }
                    }
                    String tempKey = '';
                    for(Utilization_Report_List_Item__c assemblyUtilization : lstUtilizationReportItem){
                        tempKey = assemblyUtilization.Constituent_Assembly__r.Type__c +'-'+assemblyUtilization.Assembly_Utilization__c;
                        if(!urMap.containskey(tempKey)){ 
                            urList1.add(assemblyUtilization);
                        }
                    }
                    
                    lstUtilizationReportItem = urList1;
                }
                
                for(Utilization_Report_List_Item__c assemblyUtilization :lstUtilizationReportItem) { 
                    if(assemblyUtilization.Constituent_Assembly__c != null) {
                        assemblyIds.add(assemblyUtilization.Constituent_Assembly__c);
                    } 
                    
                }
                
                //creating map of FH Data for all the Assemblies utilized in last 12 months
                if(assemblyIds.size() > 0) {
                    for(Constituent_Assembly__c assemblyRecord : [Select Id, Type__c, (Select Id, Running_Hours_During_Month__c, 
                                                                                       Report_Item__c, Utilization_Report__c, 
                                                                                       Utilization_Report__r.Month_Ending__c
                                                                                       from UR_List_Items__r 
                                                                                       where Utilization_Report__r.Month_Ending__c > :startDate 
                                                                                       AND Utilization_Report__r.Month_Ending__c <= :endDate 
                                                                                       AND Utilization_Report__r.Type__c = 'Actual' 
                                                                                       order by Utilization_Report__r.Month_Ending__c) 
                                                                  from Constituent_Assembly__c 
                                                                  where Id IN :assemblyIds]) {
                                                                      Set<Date> monthEndingDates = new Set<Date>();                                              
                                                                      fhTrendByAssemblyId.put(assemblyRecord.Id, new List<Decimal>());
                                                                      
                                                                      for(Utilization_Report_List_Item__c utilizationReport : assemblyRecord.UR_List_Items__r) {
                                                                          if(!monthEndingDates.contains(utilizationReport.Utilization_Report__r.Month_Ending__c)) {
                                                                              if(utilizationReport.Running_Hours_During_Month__c != null) {                               
                                                                                  fhTrendByAssemblyId.get(assemblyRecord.Id).add(utilizationReport.Running_Hours_During_Month__c);
                                                                              }
                                                                              
                                                                              monthEndingDates.add(utilizationReport.Utilization_Report__r.Month_Ending__c);
                                                                          }                                                                          
                                                                      }
                                                                  }
                } 
                
                //creating map of FH Data for related Aircraft utilized in last 12 months
                fhTrendByAssemblyId.put(assetId, new List<Decimal>());
                for(Utilization_Report__c utilization1 : [Select Id, Airframe_Flight_Hours_Month__c 
                                                          from Utilization_Report__c 
                                                          where Aircraft__c = :assetId
                                                          AND Month_Ending__c > :startDate 
                                                          AND Month_Ending__c <= :endDate 
                                                          AND Type__c = 'Actual' 
                                                          order by Month_Ending__c]) {
                                                              if(utilization1.Airframe_Flight_Hours_Month__c != null) {
                                                                  fhTrendByAssemblyId.get(assetId).add(utilization1.Airframe_Flight_Hours_Month__c);
                                                              }
                                                          }
                
                Map<Id, Invoice_Line_Item__c> mapInvoiceLineWithAssemblyId = new Map<Id, Invoice_Line_Item__c>();
                Map<String, List<Payment_Line_Item__c>> mapPaymentLineItem = new Map<String,  List<Payment_Line_Item__c>>();
                List<Invoice__c> invoiceList = [SELECT Id,Name,Amount_Paid__c,Ageing__c,Invoice_Date__c,
                                                (SELECT Id,Assembly_Utilization__c,Amount_Due__c,Comments__c FROM Invoice_Line_Items__r)
                                                FROM Invoice__c WHERE Utilization_Report__c =: utilization.Id 
                                                AND Utilization_Report__r.Status__c = 'Approved By Lessor' LIMIT 1];
                
                if(!invoiceList.isEmpty() && invoiceList[0].Invoice_Line_Items__r.size()>0){
                    for(Invoice_Line_Item__c invoiceItem : invoiceList[0].Invoice_Line_Items__r){
                        mapInvoiceLineWithAssemblyId.put(invoiceItem.Assembly_Utilization__c, invoiceItem);
                    }  
                }
                
                if(!invoiceList.isEmpty()){
                    List<Payment__c> paymentList =[SELECT Id, Name, 
                                                   (SELECT Id, Amount_Paid__c, Assembly_MR_Info__c
                                                    FROM Payment_Line_Items__r) 
                                                   FROM Payment__c WHERE Invoice__c =: invoiceList[0].Id];
                    for(Payment__c payment : paymentList){
                        for(Payment_Line_Item__c paymentLineItem : payment.Payment_Line_Items__r) {
                            String key = paymentLineItem.Assembly_MR_Info__c;
                            
                            if(key != null)
                            {
                                if(!mapPaymentLineItem.containsKey(key)){
                                    mapPaymentLineItem.put(key, new List<Payment_Line_Item__c>());
                                }
                                mapPaymentLineItem.get(key).add(paymentLineItem);
                            }                            
                        }
                    }
                }
                
                
                List<Utilization_Report_List_Item__c> lstEngineAssembly = new List<Utilization_Report_List_Item__c>();
                Decimal totalbill = 0 ;
                Boolean countEola = true;
                
                for(Utilization_Report_List_Item__c assemblyUtilization :lstUtilizationReportItem) {                    
                    ComponentWrapper cw = new ComponentWrapper();
                    cw.isEOLA = Utilization.End_Of_Lease_Adjustments_Only_F__c;
                    cw.attachedAircraft = assemblyUtilization.Constituent_Assembly__r.Attached_Aircraft__c;
                    cw.associatedAircraft =  assemblyUtilization.Constituent_Assembly__r.Asset__c;                                            
                    if(!cw.isEOLA) {
                        countEola = false;
                    }
                    if(cw.associatedAircraft == null && cw.attachedAircraft != null){
                        cw.assocAssetIcon = 'utility:change_record_type';
                    }
                    else{
                        if(cw.associatedAircraft != cw.attachedAircraft){
                            cw.assocAssetIcon = 'utility:change_record_type';   
                        }
                    }
                    if(assemblyUtilization.Constituent_Assembly__r.Type__c.contains('Engine')){
                        cw.showDerate = true;
                        cw.deRate = assemblyUtilization.Derate__c;
                    }
                    if(assemblyUtilization.Assembly_MR_Info__c != null){
                        cw.cummulativeMR = assemblyUtilization.Assembly_MR_Info__r.Transactions_Total_Cash__c;    
                    }
                    if(!setAssemblies.contains(assemblyUtilization.Constituent_Assembly__r.Type__c)) {
                        cw.assembly = assemblyUtilization.Constituent_Assembly__r.Assembly_Display_Name__c;
                        if(cw.assembly == 'APU'){
                            cw.apuTSN = assemblyUtilization.Assembly_Utilization__r.APU_Hours_At_Month_End_F__c;
                            cw.apuCSN = assemblyUtilization.Assembly_Utilization__r.APU_Cycles_At_Month_End_F__c;
                            cw.apuFHM = assemblyUtilization.Assembly_Utilization__r.Y_Hidden_APU_FH_String__c;
                            cw.apuFC = assemblyUtilization.Assembly_Utilization__r.APU_Cycles__c ;
                            if(cw.apuFHM == '' || cw.apuFHM == null){
                                cw.apuFHM = String.valueof(assemblyUtilization.Assembly_Utilization__r.APU_Hours__c) ;
                            }
                        }
                        cw.serialNumber = assemblyUtilization.Constituent_Assembly__r.Serial_Number__c;
                        cw.assemblyId = assemblyUtilization.Constituent_Assembly__c;
                        cw.tsn = assemblyUtilization.Assembly_Utilization__r.TSN_At_Month_End__c;
                        cw.csn = assemblyUtilization.Assembly_Utilization__r.CSN_At_Month_End__c;
                        cw.flightC = assemblyUtilization.Cycles_During_Month__c;
                        cw.flightH = assemblyUtilization.Running_Hours_During_Month__c;
                        
                        if(fhTrendByAssemblyId.containsKey(assemblyUtilization.Constituent_Assembly__c) 
                           && fhTrendByAssemblyId.get(assemblyUtilization.Constituent_Assembly__c).size() > 0) {
                               cw.fhData.addAll(fhTrendByAssemblyId.get(assemblyUtilization.Constituent_Assembly__c));
                           }
                        
                        if(assemblyUtilization.Assembly_Utilization__r.Y_Hidden_FH_String__c != '' && assemblyUtilization.Assembly_Utilization__r.Y_Hidden_FH_String__c != null){
                            cw.flightHM = assemblyUtilization.Assembly_Utilization__r.Y_Hidden_FH_String__c;
                        }
                        else{
                            cw.flightHM = String.valueof(cw.flightH);
                        }
                        
                        if(cw.flightH != null && (cw.flightC !=null && cw.flightC != 0)) {
                            cw.ratio = cw.flightH/cw.flightC; 
                        }
                        else {
                            cw.ratio = 0; 
                        }    
                        setAssemblies.add(assemblyUtilization.Constituent_Assembly__r.Type__c);
                    }
                    else{
                        if(assemblyUtilization.Derate__c != null){
                            cw.deRate = assemblyUtilization.Derate__c;
                        }
                        else{
                            cw.deRate = 0;
                        }
                        cw.hideFH = true;
                        cw.flightC = assemblyUtilization.Cycles_During_Month__c;
                        cw.tsn = assemblyUtilization.TSN_At_Month_End__c;
                        cw.csn = assemblyUtilization.CSN_At_Month_End__c;
                        
                    }
                    
                    cw.assemblyUtilizationRecord = assemblyUtilization;
                    cw.mrEvent = assemblyUtilization.Assembly_MR_Info__r.Assembly_Event_Info__r.Event_type__c; //Could be null.
                    if(cw.mrEvent == '' || cw.mrEvent == null){
                        if(assemblyUtilization.Assembly_MR_Info__r.RecordType.Name == 'PBH'){
                            // cw.mrEvent = 'PBH';
                        }
                    }
                    cw.billed = assemblyUtilization.Total_Maintenance_Reserve__c;
                    
                    cw.rateBasis = assemblyUtilization.Effective_Unit__c;
                    if(String.isNotBlank(cw.rateBasis) && cw.rateBasis.contains('Monthly')) {
                        cw.units = assemblyUtilization.Threshold_Units__c; 
                    }
                    else {
                        cw.units = assemblyUtilization.Net_Units__c; 
                    }
                    
                    cw.rate = assemblyUtilization.Effective_Rate_Final__c == null ? 0 : assemblyUtilization.Effective_Rate_Final__c;
                    cw.comment = assemblyUtilization.Comments__c;                                   
                    
                    if(utilization.Status__c == 'Approved By Lessor') {
                        if(mapInvoiceLineWithAssemblyId.containsKey(assemblyUtilization.Id)) {
                            cw.invoiceRec = mapInvoiceLineWithAssemblyId.get(assemblyUtilization.Id);
                            String strKey = assemblyUtilization.Assembly_MR_Info__c;                            
                            if(strKey != null && mapPaymentLineItem.containsKey(strKey)){
                                for(Payment_Line_Item__c pl : mapPaymentLineItem.get(strKey)) {
                                    cw.paid += pl.Amount_Paid__c; 
                                } 
                            }
                            
                            if(assemblyUtilization.Constituent_Assembly__r.Type__c.contains('Engine')) {
                                if(cw.paid != null && cw.paid != 0){
                                    cw.balance =  (cw.billed==null ? 0 :assemblyUtilization.Total_Maintenance_Reserve__c) - cw.paid;
                                }
                                else{
                                    cw.balance =  cw.billed==null ? 0 :assemblyUtilization.Total_Maintenance_Reserve__c; 
                                }
                            }
                            else {
                                if(cw.paid != null && cw.paid != 0){
                                    cw.balance =  (cw.billed==null ? 0 :cw.billed) - cw.paid;
                                }
                                else{
                                    cw.balance =  cw.billed==null ? 0 :cw.billed; 
                                }
                            }                           
                            cw.days= System.today().daysBetween(invoiceList[0].Invoice_Date__c); 
                        }
                    }
                    
                    cwList.add(cw);                                       
                }
                aw.cwList = cwList;
                aw.assetId = assetId;
                aw.utilizationId = utilizationId;
            }
        }
        }
        
        return aw;
    }
    public class ComponentWrapper {
        @AuraEnabled public Boolean isAirframeInList{get;set;}
        @AuraEnabled public Boolean isEOLA{get;set;}
        @AuraEnabled public Boolean hideFH{get;set;}
        @AuraEnabled public Decimal apuFH{get;set;}
        @AuraEnabled public Decimal apuFC{get;set;}
        @AuraEnabled public Decimal deRate{get;set;}
        @AuraEnabled public Decimal apuTSN {get;set;}
        @AuraEnabled public Decimal apuCSN {get;set;}
        @AuraEnabled public Boolean showDerate{get;set;}
        @AuraEnabled public string apuFHM {get;set;}
        @AuraEnabled public string assembly {get;set;}
        @AuraEnabled public Decimal flightH {get;set;}
        @AuraEnabled public String flightHM {get;set;}
        @AuraEnabled public Decimal flightC {get;set;}
        @AuraEnabled public Decimal ratio {get;set;}
        @AuraEnabled public String serialNumber {get;set;}
        @AuraEnabled public String assemblyId {get;set;}
        @AuraEnabled public Boolean tsnUnknown {get;set;}
        @AuraEnabled public Boolean csnUnknown {get;set;}
        @AuraEnabled public String utilizationStatus {get;set;}
        @AuraEnabled public Decimal tsn {get;set;}
        @AuraEnabled public Decimal csn {get;set;}
        @AuraEnabled public Utilization_Report_List_Item__c assemblyUtilizationRecord {get;set;}
        @AuraEnabled public String mrEvent {get;set;}
        @AuraEnabled public Decimal billed {get;set;}
        @AuraEnabled public String rateBasis {get;set;}
        @AuraEnabled public Decimal units {get;set;}
        @AuraEnabled public Boolean fCUnknown {get;set;}
        @AuraEnabled public String attachedAircraft {get;set;}
        @AuraEnabled public String associatedAircraft {get;set;}
        @AuraEnabled public Boolean fHUnknown {get;set;}
        @AuraEnabled public Decimal rate {get;set;}
        @AuraEnabled public Decimal cummulativeMR {get;set;}
        @AuraEnabled public String comment {get;set;}
        @AuraEnabled public Invoice_Line_Item__c invoiceRec {get;set;}
        @AuraEnabled public Decimal paid {get;set;}
        @AuraEnabled public Decimal balance {get;set;}
        @AuraEnabled public Integer days {get;set;}
        @AuraEnabled public List<Decimal> fhData {get;set;}
        @AuraEnabled public String assocAssetIcon {get;set;}
        
        public ComponentWrapper() {
            this.tsnUnknown = false;
            this.csnUnknown = false;
            this.fCUnknown = false;
            this.fHUnknown = false;
            this.csn = 0;
            this.tsn = 0;
            this.cummulativeMR = 0;
            this.balance = 0;
            this.apuFHM ='';
            this.hideFH = false;
            this.isAirframeInList = true;
            this.isEOLA = false;
            this.showDerate = false;
            this.mrEvent = '';
            this.deRate = 0;
            this.billed = 0;
            this.flightHM ='';
            this.cummulativeMR = 0;
            this.rateBasis ='';
            this.units = 0;
            this.rate = 0;
            this.paid =0;
            this.ratio = 0;
            this.comment='';
            this.fhData = new List<Decimal>();
        }
    }
    public class AssemblyWrapper{
        @AuraEnabled public List<ComponentWrapper> cwList{get;set;}
        @AuraEnabled public String assetId{get;set;}
        @AuraEnabled public String utilizationId{get;set;}
        public AssemblyWrapper(){
            this.cwList = new List<ComponentWrapper>();
        }
    }
}