public with sharing class ForecastStatusController {
    
    
    public ForecastStatusController(ApexPages.StandardController controller) {
        
    }
    
    public PageReference updateStatus() {
        Scenario_Input__c si = new Scenario_Input__c(Id = ApexPages.CurrentPage().getparameters().get('id'));
        si.Forecast_Status__c ='Primary';
        update si;        
        PageReference redirectPage = new PageReference('/'+si.Id);
        redirectPage.setRedirect(true);
        return redirectPage;
    }
    
}