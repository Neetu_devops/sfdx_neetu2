public with sharing class LLPOnNewController extends PaymentNewControllerBase {
    public LLPOnNewController(ApexPages.StandardController ingored) {
        super(Constituent_Assembly__c.SObjectType, Sub_Components_LLPs__c.SObjectType);
    }
    
}