 /************This batch is used to update the World Fleet Lookup on Asset
  * This is Involked from AircraftTriggerHandler Before Insert/Update and also from BatchUpdate World Fleet**** */
  public  class BatchUpdateWFLinkOnAsset implements Database.Batchable<Aircraft__c> ,Database.Stateful{
    
    public  set<string> assetsNotLinkedwithWF= new set<String>(); 
    public Set<String> assetIds; 
    String  exceptionMessage='';
    string logMessageString='';

    
    
    public  BatchUpdateWFLinkOnAsset(Set<String> assetIds) {
        this.assetIds = assetIds;  
    }
    
   
    public  List<Aircraft__c> start(Database.BatchableContext BC) {
        string query ;
        if(assetIds==null || assetIds.isEmpty()){
            query = 'select Id,Name, Y_Hidden_Serial_Number__c,Aircraft_Type__c,World_Fleet_Record__c from Aircraft__c where RecordTypeDevName__c= \'Aircraft\''  +
            (Test.isRunningTest()?' LIMIT 20':'');
        }else{ 
           query = 'select Id,Name, Y_Hidden_Serial_Number__c,Aircraft_Type__c,World_Fleet_Record__c from Aircraft__c where ID in :assetIds  AND RecordTypeDevName__c= \'Aircraft\'' +
           (Test.isRunningTest()?' LIMIT 20':'');
        }  
        return Database.query(query);
    }
    
    public void execute(Database.BatchableContext BC, List <Aircraft__c> assetList) {
        Savepoint spStart = Database.setSavepoint();
        try{
            setWFLinkOnAssets(assetList,true);
        }catch(Exception ex){
            Database.rollback(spStart);
            exceptionMessage += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
            system.debug('Exception : ' + ex +  + '===' +exceptionMessage);
        }
        
    }
    
    public void finish(Database.BatchableContext BC) {
        
        if(String.isEmpty(exceptionMessage)){
            string nWidth='400px';  
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id =:BC.getJobId()];                   
        string strMsg = 'Job with ID ' + BC.getJobId() + ' completed with status ' + a.Status 
                          + '.\n' + a.TotalJobItems + ' batches with '+ a.JobItemsProcessed + ' processed and ' + a.NumberOfErrors + ' failures.';
             
        strMsg+= '<br/> <br/><table border=0><tr><td><b> Total Number of Assets which are not linked with World Fleet Record: ' + assetsNotLinkedwithWF.size() + ' </b></td></tr>';
        strMsg+= '<br/> <tr><td> The following is a list of Aircraft for which no match has been found on the World Fleet table. Please review that aircraft types are correctly configured in the World Fleet records or the mapping table </td></tr><br/>';
        strMsg += '</table>';
        strMsg+= '<table border=1>';
        system.debug('assetsNotLinkedwithWF----'+assetsNotLinkedwithWF);
        for (String str: assetsNotLinkedwithWF){
            strMsg += '<tr><td>' + str + '</td></tr>';
        }
        strMsg += '</table>';
        
        LeaseWareUtils.sendEmailToLWAdmins('LeaseWorks: Aircraft missing in World Fleet', strMsg);
        LeaseWareUtils.sendEmail('LeaseWorks: Aircraft missing in World Fleet', strMsg, a.CreatedBy.Email);

        }else{
            LeaseWareUtils.sendEmail('Update World Fleet on Asset', 'There was an unexpected error updating world fleet on asset. Please contact your LeaseWorks System Administrator. \n \n \n  <br/><br/>Reason :'+ exceptionMessage, UserInfo.getUserEmail());
        }
    }


    public static void setWFLinkOnAssets_Util(List<Aircraft__c> assetIds,boolean isFromBatch){
        BatchUpdateWFLinkOnAsset batch1 = new BatchUpdateWFLinkOnAsset(null);
        batch1.setWFLinkOnAssets(assetIds,false);
    }
/*********************************************************************
    Description: This method updates the World Fleet Lookup on Asset,
    Input Parameters : List of Assets to be updated, flag to indicate whether this is from batch/trigger(Since it is called
    from before trigger no need to write update query ) 
    - Called From : execute method above and from AircraftTriggerHandler updateWFLink(assetList)
***********************************************************************/    
    public  void setWFLinkOnAssets(List<Aircraft__c> assetIds,boolean isFromBatch){
        // processing
        List<Aircraft__c> aircraftListToUpdate  = new  List<Aircraft__c>();
        Map<String,String> acTypeMap =new Map<String,String>();
        Set<String> msnSet = new Set<String>();
        Set<String> acTypeSet = new Set<String>();
        for(Aircraft__c newACs : (Aircraft__c[])assetIds){
            msnSet.add(newACs.Y_Hidden_Serial_Number__c);
            acTypeSet.add(newACs.Aircraft_Type__c);
        }
        system.debug('msnSet----'+msnSet.size());
        for(Mapping__c mapRec: [select Id, Name,Map_To__c from Mapping__c where Name IN: acTypeSet and isActive__c=true]){
            acTypeMap.put(mapRec.Name,mapRec.Map_To__c);
        }
        //For given Serial no, there might exist more than one record, hence  map
        map<String,List<Global_Fleet__c>>  mapSNToFleet = new map<String,List<Global_Fleet__c>>();
        //For given Serial no and Aircraft Type,
        map<String,String>  mapSNTypeToFleet = new map<String,String>();
        List<Global_Fleet__c> gfList ;
        String keyStringSNType ; 
        String acType;
            for(Global_Fleet__c gf : [select Id,Y_Hidden_Serial_Number__c,AircraftType__c from Global_Fleet__c where Y_Hidden_Serial_Number__c  IN: msnSet]){
                //First put ACtype as it is there in Global Fleet AC Type
                keyStringSNType =  gf.Y_Hidden_Serial_Number__c + '_'+ gf.AircraftType__c;
                system.debug('keyStringSNType-----'+ keyStringSNType);
                mapSNTypeToFleet.put(keyStringSNType, gf.Id);
                //Then check if mapping record exists for AC Type in mapping table
                if(acTypeMap.get(gf.AircraftType__c)!=null){
                    keyStringSNType =  gf.Y_Hidden_Serial_Number__c + '_'+acTypeMap.get(gf.AircraftType__c);
                    mapSNTypeToFleet.put(keyStringSNType, gf.Id);
                }
                
                if(mapSNToFleet.get(gf.Y_Hidden_Serial_Number__c) ==null){
                    gfList = new List<Global_Fleet__c>();
                    gfList.add(gf);
                }else{
                    gfList = mapSNToFleet.get(gf.Y_Hidden_Serial_Number__c);
                    gfList.add(gf);
                }
               
                mapSNToFleet.put(gf.Y_Hidden_Serial_Number__c, gfList);
            }
            String wfId;
            for(Aircraft__c curAC :  assetIds){
                system.debug('Match For SN and Type Exists ---'+ mapSNTypeToFleet.get(curAC.Y_Hidden_Serial_Number__c +'_'+curAC.Aircraft_Type__c));
                if(mapSNTypeToFleet.get(curAC.Y_Hidden_Serial_Number__c +'_'+curAC.Aircraft_Type__c)==null){
                    system.debug('Check for Serial Number Only');
                    if(mapSNToFleet.get(curAC.Y_Hidden_Serial_Number__c)!=null){
                        List<Global_Fleet__c> listOfGF = mapSNToFleet.get(curAC.Y_Hidden_Serial_Number__c);
                        system.debug('List of Global Fleet----'+listOfGF);
                        if(listOfGF!=null && listOfGF.size()==1){
                            system.debug('Size of GF is 1, so set this id-----'+  listOfGF[0].id);
                            wfId = listOfGF[0].id;
                        }else{
                            wfId = null;
                        }
                    }else{
                        wfId = null;
                    }
                }
                else{
                    wfId = mapSNTypeToFleet.get(curAC.Y_Hidden_Serial_Number__c +'_'+curAC.Aircraft_Type__c);
                }
                if(wfId==null){
                    assetsNotLinkedwithWF.add(curAC.Name);
                }
                if(curAC.World_Fleet_Record__c !=wfId){
                    curAC.World_Fleet_Record__c =wfId;
                   if(isFromBatch) aircraftListToUpdate.add(curAC);
                }

            }  
            if(isFromBatch){
                if(aircraftListToUpdate!=null && aircraftListToUpdate.size()>0){
                    system.debug('Update Assets......');
                    LeaseWareUtils.TriggerDisabledFlag = true;
                    update aircraftListToUpdate;
                    LeaseWareUtils.TriggerDisabledFlag = false;
                 }
            }
    }
}