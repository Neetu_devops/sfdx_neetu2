public with sharing class Notify_RecordAccessUtility {

    public static String testNotificationConfigurations(String loadData) {
        try {
            System.debug('loadData: ' + loadData);
            Notify_Utility.NotificationSetupWrapper nsWrap = (Notify_Utility.NotificationSetupWrapper) JSON.deserialize(loadData, Notify_Utility.NotificationSetupWrapper.class);
            if (nsWrap != null) {

                String footerStr='<br><br> \n \n \n ----------------------------------------------------------------------';
                /*************************************************************** */

                Map<String, Set<String>> mergeFieldMap = new Map<String, Set<String>>();
                

                Date notificationDate = nsWrap.notifyDays==null?null:system.today().addDays(Integer.valueOf(nsWrap.notifyDays));
                String query = Notify_Utility.prepareQuery_NotifyWrap(nsWrap, mergeFieldMap, true,notificationDate);
                system.debug('Query----'+query);
                List<SObject> recordList = Database.query(query);

                if (recordList == null || recordList.isEmpty()) { return 'Either there are no record or we were not able to find any record based on the filters.'; }
                else {
                Set<String> referencesField = new Set<String>();
                String  objName = nsWrap.selectedObjectName;
                SObjectType sObjType = ((SObject) Type.forName(objName).newInstance())
                                      .getSObjectType();
                Map<String, Schema.SObjectField> Objfields = sobjType.getDescribe().fields.getMap();
                for(Schema.sObjectField field : Objfields.values()) {
                    Schema.DescribeFieldResult dfield = field.getDescribe();
                    if(String.ValueOf(dfield.getType()) == 'Reference'){
                        referencesField.add(dfield.getName().toLowerCase());
                    }
                }

                
                System.debug('mergeFieldMap: ' + mergeFieldMap);
                System.debug('recordList: ' + recordList);

               
                    System.debug('recordList: ' + recordList[0]);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new String[] { UserInfo.getUserEmail()});
                    if (mergeFieldMap.containsKey('TITLE') && !mergeFieldMap.get('TITLE').isEmpty()) {
                        system.debug(' mergeFieldMap.getTitle----'+ mergeFieldMap.get('TITLE'));
                        if (recordList[0] != null ) {
                            String titleMessage = nsWrap.title;
                            String titleMessageVal =  Notify_Utility.getTitleMessageValue( recordList[0] ,  mergeFieldMap.get('TITLE'),referencesField, titleMessage);
                            nsWrap.title= titleMessageVal.unescapeHtml3();
                        }
                    }
                    system.debug('title message------------->'+nsWrap.title);
                    if (mergeFieldMap.containsKey('MESSAGE_BODY') && !mergeFieldMap.get('MESSAGE_BODY').isEmpty()) {
                        String titleMessage = nsWrap.messageBody;
                        String titleMessageVal =  Notify_Utility.getTitleMessageValue( recordList[0] ,  mergeFieldMap.get('MESSAGE_BODY'),referencesField, titleMessage);
                        nsWrap.messageBody= titleMessageVal.unescapeHtml3();
                    }else{
                        nsWrap.messageBody=nsWrap.messageBody==null?'':nsWrap.messageBody;
                    }
                   
                    String href ='';
                    String nsName ='Test Notification Subscription'; 
                    String ownerName = UserInfo.getName();
                    String nsId = String.isEmpty(nsWrap.recordId)?'':nsWrap.recordId;
                    if(String.isNotEmpty(nsWrap.recordId)){
                        nsName =nswrap.name;
                        ownerName = nsWrap.ownerName;
                    }
                    footerStr = Notify_Utility.getFooterData(nsId,nsName,ownerName);
                    mail.setSubject('[Test] ' + nsWrap.title);
                    mail.setSaveAsActivity(false);
                    mail.setHtmlBody(nsWrap.messageBody  + '\n'  + '<br/><div style="color:#C0C0C0">' + footerStr +'</div>');
                    LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
                    if (results[0].success) { 
                        return ' Email was sent according to the rules specified in the notification. Please check your inbox.\n Note: other users did not receive this test message';
                     }
                    else { return results[0].errors[0].message; }
                }
            }
            return 'Something went wrong. Please try again. If the issue continues contact your System Admin.';
        }
        catch (Exception ex) {
            system.debug('Mesage -----'+ex.getMessage() +'ex.getLineNo-----'+ ex.getLineNumber() + '--'+ ex.getStackTraceString());
            if (ex.getMessage().containsIgnoreCase('LIMIT')) { throw new Notify_CustomException('Error: Please check Filters'); }
            else { throw new Notify_CustomException('Error: ' + ex.getMessage() + (Notify_Utility.Notify_Setting_REC?.Capture_Error_Stack_Trace__c ? ' - ' + ex.getStackTraceString() : '')); }
        }
    }
}