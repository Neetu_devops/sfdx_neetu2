public  without sharing class DynamicPicklistApexController {
	static string prefix = leasewareutils.getNamespacePrefix();
    
    // Check if the user has read access 
    public static boolean checkFieldPermission(String objName, String fieldName){
        SObjectType objType = Schema.getGlobalDescribe().get(objName);
        system.debug('objType=='+objType);
        Schema.SobjectField fieldType = objType.getDescribe().fields.getMap().get(fieldName);
        return fieldType.getDescribe().isAccessible();
    }
    
    @AuraEnabled
  	public static map<string,string> getDynPickList(string objectName, string fieldName, string recordTypeName, string recordTypeLabelName) {
    	if(String.isBlank(objectName) || String.isBlank(fieldName)){
            return null;
        }     
      	map<string,string> returnMap = new map<string,string>();
		string objectNameNew = objectName;
        if(objectName.indexof(prefix) == -1) {
            objectNameNew = prefix + objectName;
        }
        
        string fieldNameNew = fieldName;
        if(fieldName.indexof(prefix) == -1) {
            fieldNameNew = prefix + fieldName;
        }
        
        if(!checkFieldPermission(objectNameNew, fieldNameNew)) {
            throw new AuraHandledException('No Permission.');
        }
        
        string matchingUserAlias;
        String matchingUserName; // Oct 05, 2019
        
        if(fieldNameNew.contains('Marketing_Rep_c__c') && objectNameNew.contains('Marketing_Activity__c')){
            //Marketing Rep
            User CurrentUser = [select id,Alias, Name FROM User Where id =: userInfo.getUserId() ];
            system.debug('CurrentUser' +CurrentUser); 
            matchingUserAlias = CurrentUser.Alias ;
            matchingUserName = CurrentUser.Name;
        }
        
        String recordTypeId = getRecordType(objectName, fieldName, recordTypeName, recordTypeLabelName);

        if(recordTypeId != null && recordTypeId != ''){
            Map<string,string> picklistValueMap = LeaseWareUtils.getValues(objectNameNew, recordTypeId, fieldNameNew);
            for(String key : picklistValueMap.keySet()){
                if((matchingUserAlias !=null && picklistValueMap.get(key) == matchingUserAlias) ||
                    (matchingUserName !=null && picklistValueMap.get(key) == matchingUserName)){  //checking wheter loggedin user name is in Marketing rep list
                    returnMap.put('CLWDefaultPicklist-'+key, picklistValueMap.get(key)); //if yes put it it map with default prefix.
                }else{ returnMap.put(key, picklistValueMap.get(key)); }
            }
        }
        else{
            SObjectType objType = Schema.getGlobalDescribe().get(objectNameNew);
            system.debug('objType=='+objType);
            Schema.SobjectField fieldType = objType.getDescribe().fields.getMap().get(fieldNameNew);
            Schema.DescribeFieldResult fieldResult =  fieldType.getDescribe();
            system.debug('fieldResult='+fieldResult);
       		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    		for(Schema.PicklistEntry f : ple){
                system.debug('f= '+f);
                if((matchingUserAlias!=null && f.getValue()== matchingUserAlias) ||
                    (matchingUserName!=null && f.getValue()== matchingUserName)){  //checking wheter loggedin user name is in Marketing rep list
                    returnMap.put('CLWDefaultPicklist-'+f.getLabel(), f.getValue()); //if yes put it it map with default prefix.
                }
                else{
                     returnMap.put(f.getLabel(), f.getValue());
                }
            }
            system.debug('returnMap=='+returnMap.size());
        }     
      	return returnMap;
    }

    public static String getRecordType(string objectName, string fieldName, string recordTypeName, string recordTypeLabelName) {
        if(String.isBlank(objectName) || String.isBlank(fieldName)){
            return null;
        }     
      	map<string,string> returnMap = new map<string,string>();
		string objectNameNew = objectName;
        if(objectName.indexof(prefix) == -1) {
            objectNameNew = prefix + objectName;
        }
        
        string fieldNameNew = fieldName;
        if(fieldName.indexof(prefix) == -1) {
            fieldNameNew = prefix + fieldName;
        }
                
        String recordTypeId = '';
        if(!String.isBlank(recordTypeName)){
            if(Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByDeveloperName().containsKey(recordTypeName) && Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName) != null){
             	recordTypeId = Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
            }
            else if(Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByName().containsKey(recordTypeName) && Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByName().get(recordTypeName) != null){
             	recordTypeId = Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            }
            
        }
        else if(!String.isBlank(recordTypeLabelName)) {
            if(objectNameNew.indexOf('Marketing_Activity') != -1 || objectNameNew.indexOf('Aircraft_Proposal__c') != -1 ) {
                //String assetType = recordTypeLabelName.split('@')[0];
                //String dealType = recordTypeLabelName.split('@')[1];
                String assetType,dealType;
                if(recordTypeLabelName.split('@') != null){
                    if(recordTypeLabelName.split('@').size() == 1){
                        assetType = recordTypeLabelName.split('@')[0];    
                    } else if(recordTypeLabelName.split('@').size() == 2){
                        assetType = recordTypeLabelName.split('@')[0];
                        dealType = recordTypeLabelName.split('@')[1];      
                    }        
                }
                
                if(assetType != null && dealType != null){
                	recordTypeId = Utility.getMARecordType(assetType, dealType, objectNameNew);    
                }
                
            }            
            else {
                recordTypeId = Schema.getGlobalDescribe().get(objectNameNew).getDescribe().getRecordTypeInfosByName().get(recordTypeLabelName).getRecordTypeId();
            }
        }
		return recordTypeId;
    }

    public static PicklistWrapper getPicklistValuesWithDefault(String objectType, String recordTypeId, String fieldName, String defaultValue){
        
        string matchingUserAlias;
        String matchingUserName; // Oct 05, 2019
        
        if(fieldName.contains('Marketing_Rep_c__c') && objectType.contains('Marketing_Activity__c')){
            //Marketing Rep
            User CurrentUser = [select id,Alias, Name FROM User Where id =: userInfo.getUserId() ];
            system.debug('CurrentUser' +CurrentUser); 
            matchingUserAlias = CurrentUser.Alias ;
            matchingUserName = CurrentUser.Name;
        }
        
        //Endpoint
        String endpoint = URL.getSalesforceBaseUrl().toExternalForm();
        endpoint += '/services/data/v41.0';
        endpoint += '/ui-api/object-info/{0}/picklist-values/{1}/{2}';
        endpoint = String.format(endpoint, new String[]{ objectType, recordTypeId, fieldName });
        EncodingUtil.urlEncode(endpoint,'UTF-8');
        
        //HTTP Request send
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'OAuth ' + LeaseWareUtils.getSessionIdFromVFPage(Page.SessionId)); 
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);

        
        PicklistWrapper wrapper = new PicklistWrapper();
        
        Map<String,String> picklistValueMap = new Map<String,String>();
        map<string,string> returnMap = new map<string,string>();
        if(res.getStatusCode() == 200){
            System.debug('res'+res.getBody());            
            if(res != null && res.getBody() != null && res.getBody() != ''){
                Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
                if(!root.containsKey('values')){ return wrapper; }
                
                //to get the picklist values
                List<Object> pValues = (List<Object>)root.get('values');
                for(Object pValue : pValues){
                    Map<String,Object> pValueMap = (Map<String,Object>)pValue;
                    picklistValueMap.put((String)pValueMap.get('label'), (String)pValueMap.get('value'));
                }
                for(String key : picklistValueMap.keySet()){
                    if((matchingUserAlias !=null && picklistValueMap.get(key) == matchingUserAlias) ||
                       (matchingUserName !=null && picklistValueMap.get(key) == matchingUserName)){  //checking wheter loggedin user name is in Marketing rep list
                           returnMap.put('CLWDefaultPicklist-'+key, picklistValueMap.get(key)); //if yes put it it map with default prefix.
                       }else{ returnMap.put(key, picklistValueMap.get(key)); }
                }
                wrapper.pickList = returnMap;
                
                //to get the default picklist value
                Map<String,Object> pDefaultMap = (Map<String,Object>)root.get('defaultValue');
                System.debug('getDefault ' + pDefaultMap);
                String recDefaultVal = null;
                if(pDefaultMap != null)
                    recDefaultVal = (String)pDefaultMap.get('value');
                System.debug('getDefault recDefaultVal ' + recDefaultVal + ' defaultValue: '+defaultValue);
                if(String.isEmpty(recDefaultVal) && pValues != null) {
                    boolean isDefaultExists = false;
                    String firstValue = null;
                    for(Object pValue : pValues) {
                        Map<String,Object> pValueMap = (Map<String,Object>)pValue;
                        String val = (String)pValueMap.get('value');
                        if(firstValue == null) 
                            firstValue = val;
                        if(!String.isEmpty(defaultValue) && defaultValue.equals(val)) {
                            isDefaultExists = true;
                            break;
                        }
                    }
                    System.debug('getDefault firstValue ' + firstValue);
                    if(!isDefaultExists)
                        recDefaultVal = firstValue;
                }
                else
                    recDefaultVal = recDefaultVal;
                wrapper.defaultValue = recDefaultVal;
            }
        }
        return wrapper;
    }

    
    @AuraEnabled
  	public static PicklistWrapper getDynPickListDefault(string objectName, string fieldName, string recordTypeName, string recordTypeLabelName, String defaultValue) {
    	System.debug('getDynPickListDefault fieldName: '+ fieldName);
        if(String.isBlank(objectName) || String.isBlank(fieldName)){
            return null;
        }     
        map<string,string> returnMap = new map<string,string>();
		string objectNameNew = objectName;
        if(objectName.indexof(prefix) == -1) {
            objectNameNew = prefix + objectName;
        }
        
        string fieldNameNew = fieldName;
        if(fieldName.indexof(prefix) == -1) {
            fieldNameNew = prefix + fieldName;
        }
        
        if(!checkFieldPermission(objectNameNew, fieldNameNew)) {
            throw new AuraHandledException('No Permission.');
        }
        
        PicklistWrapper wrapper = new PicklistWrapper();
        String recordTypeId = getRecordType(objectName, fieldName, recordTypeName, recordTypeLabelName);
        String def = null;
        if(recordTypeId != null && recordTypeId != ''){
             wrapper = getPicklistValuesWithDefault(objectNameNew, recordTypeId, fieldNameNew, defaultValue);
        }
        else{
            string matchingUserAlias;
            String matchingUserName; // Oct 05, 2019
            
            if(fieldNameNew.contains('Marketing_Rep_c__c') && objectNameNew.contains('Marketing_Activity__c')){
                //Marketing Rep
                User CurrentUser = [select id,Alias, Name FROM User Where id =: userInfo.getUserId() ];
                system.debug('CurrentUser' +CurrentUser); 
                matchingUserAlias = CurrentUser.Alias ;
                matchingUserName = CurrentUser.Name;
            }

            SObjectType objType = Schema.getGlobalDescribe().get(objectNameNew);
            system.debug('objType=='+objType);
            Schema.SobjectField fieldType = objType.getDescribe().fields.getMap().get(fieldNameNew);
            Schema.DescribeFieldResult fieldResult =  fieldType.getDescribe();
            system.debug('fieldResult='+fieldResult);
       		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            String firstValue = null;
            boolean isDefaultExists = false;
    		for(Schema.PicklistEntry f : ple){
                system.debug('f= '+f);
                if((matchingUserAlias!=null && f.getValue()== matchingUserAlias) ||
                   (matchingUserName!=null && f.getValue()== matchingUserName)){  //checking wheter loggedin user name is in Marketing rep list
                       returnMap.put('CLWDefaultPicklist-'+f.getLabel(), f.getValue()); //if yes put it it map with default prefix.
                       if(firstValue == null)
                           firstValue = f.getValue();
                       if(!String.isEmpty(defaultValue) && defaultValue.equals(f.getValue())) 
                           isDefaultExists = true;
                       
                }
                else{
                    returnMap.put(f.getLabel(), f.getValue());
                    if(firstValue == null)
                        firstValue = f.getValue();
                    if(!String.isEmpty(defaultValue) && defaultValue.equals(f.getValue())) 
                        isDefaultExists = true;
                    
                }
            }
            wrapper.picklist = returnMap;
            if(isDefaultExists == false)
                wrapper.defaultValue = firstValue;
            else
            wrapper.defaultValue = defaultValue;
            system.debug('returnMap=='+returnMap.size());
        }
        return wrapper;
    }

    public class PicklistWrapper {
        @AuraEnabled public String defaultValue  {get; set;}
        @AuraEnabled public map<string,string> pickList  {get; set;}
    }

}