@isTest
public with sharing class TestApplyCreditMemoController {
    
    @testSetup
    static void setup(){
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                          Phone_Number__c='2342', Email_Address__c='a@lease.com',Accounting_enabled__c = false); 
        LeaseWareUtils.clearFromTrigger();
        insert  LWSetUp;
        
        Parent_Accounting_Setup__c ASRec = new Parent_Accounting_Setup__c(Name='Dummy' );
        insert ASRec;
        LeaseWareUtils.clearFromTrigger();
        
        TestLeaseworkUtil.setCoaRent();
        LeaseWareUtils.clearFromTrigger();
        
        TestLeaseworkUtil.setCoaCM();
        
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
        insert aircraft;
        
        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;
        
        Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,Lease_End_Date_New__c=Date.newInstance(2020,12,31),Lease_Start_Date_New__c= Date.NewInstance(2020,01,01),
                                      Accounting_Setup_P__c =ASRec.id);
        LeaseWareUtils.clearFromTrigger();
        insert lease;
        
        Calendar_Period__c CP1 = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1));
        Calendar_Period__c CP2 = new Calendar_Period__c(Name = 'Dummy2',Start_Date__c =Date.NewInstance(2020,01,01),
                                                        End_Date__c = Date.NewInstance(2020,01,31));
        Calendar_Period__c CP3 = new Calendar_Period__c(Name = 'Dummy3',Start_Date__c =Date.NewInstance(2020,02,01),
                                                        End_Date__c = Date.NewInstance(2020,02,29));
        
        list<Calendar_Period__c> cp = new list<Calendar_Period__c>{cp1,cp2,cp3};
            LeaseWareUtils.clearFromTrigger();
        insert cp;
        
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = aircraft.Id,
            Y_hidden_Lease__c = lease.Id,
            Type__c = 'Actual',
            Month_Ending__c = Date.NewInstance(2020,01,31),
            Status__c = 'Approved By Lessor'
        );
        
        LWGlobalUtils.setTriggersflagOn(); 
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = lease.Id,
            Amount__c =200000,
            Status__c = 'Approved'
        );
        
        insert invoiceRecord;
        invoice_line_item__c lineItem = new invoice_line_item__c (name ='Test',amount_due__c = 10000,invoice__c=invoiceRecord.id);
        LWGlobalUtils.setTriggersflagOn(); 
        insert lineItem;
        LWGlobalUtils.setTriggersflagOff();
        
        Stepped_Rent__c MultiRentSch = new Stepped_Rent__c (
            Name='Rent Schedule Fixed',
            Lease__c=lease.Id,
            Rent_Type__c='Fixed Rent',
            Rent_Period__c='Monthly',
            Base_Rent__c=200000.00,
            Start_Date__c=Date.NewInstance(2020,01,01), // YYYY-MM-DD
            Rent_End_Date__c=Date.newInstance(2020,12,31) ,  // YYYY-MM-DD
            Invoice_Generation_Day__c='1',
            Rent_Due_Type__c='Fixed',
            Rent_Due_Day__c=1,
            Escalation__c=3,
            Escalation_Month__c='May',
            Pro_rata_Number_Of_Days__c='Actual number of days in month/year');
        insert MultiRentSch;
        test.startTest();
        rent__c rent2 = new rent__c(RentPayments__c= lease.id,Name='New Rent 1',For_Month_Ending__c=Date.NewInstance(2020,01,31),start_date__c=Date.NewInstance(2020,01,01),Stepped_Rent__c =MultiRentSch.id);
        rent__c rent3 = new rent__c(RentPayments__c= lease.id,Name='New Rent 2',For_Month_Ending__c=Date.NewInstance(2020,02,29),start_date__c=Date.NewInstance(2020,02,01),Stepped_Rent__c =MultiRentSch.id);
        list <rent__c> rentList = new list<rent__c>{rent2,rent3};
            LeaseWareUtils.clearFromTrigger();
        insert rentList;
        
        Invoice__c invoice1 = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rent2.id,amount__c = rent2.Rent_Due_Amount__c,status__c ='Approved',
                                             invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
        Invoice__c invoice2 = new Invoice__c(Invoice_Date__c =system.today(),lease__c = Lease.id,rent__c =  rent3.id,amount__c = rent3.Rent_Due_Amount__c,status__c ='Approved',
                                             invoice_type__c = 'Rent',recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Rent').getRecordTypeId());
        list<invoice__c> invoices = new list<invoice__c>{invoice1,invoice2};
            LeaseWareUtils.clearFromTrigger();
        insert invoices;
        
        list<journal_entry__c> je = [select id,name from journal_entry__c where invoice__c =:invoice1.id];
        system.debug('Size of je:::'+je.size());
        
        credit_memo__c creditMemo = new credit_memo__c(amount__c = 200000.00,
                                                       Credit_Memo_Date__c=system.today(),lessee__c = operator.id,lease__c =lease.id);
        LeaseWareUtils.clearFromTrigger();
        insert creditMemo;
        LeaseWareUtils.clearFromTrigger();
        creditMemo.status__c = 'Approved';
        update creditMemo;
        test.stopTest();
        
    }
    @istest
    public static void testApplyCreditMR(){
        Test.StartTest();
        Calendar_Period__c cp = [select id from Calendar_Period__c where name ='Dummy1' ];
        Credit_memo__c cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c, Credit_Memo_Date__c, Credit_Memo_Reason__c, Due_Date__c, Lease__c, Lessee__c, Memo__c, Status__c, Allocated_Amount__c, Y_Hidden_IsStandalone__c  from Credit_Memo__c  limit 1];
        list<invoice__c> invList = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c,
                                    (select id,name from journal_entries__r ),
                                    (select id,name,Assembly_Utilization__c,Assembly__c,Module__c,Assembly_MR_Info__c from invoice_line_items__r)
                                    from Invoice__c where Utilization_Report__c !=null ];
        ApplyCreditMemoController.CreditMemoWrapper cmWrp = new ApplyCreditMemoController.CreditMemoWrapper();
        cmWrp.creditMemoRec = cm;
        cmWrp.invoiceRec = invList[0];
        cmWrp.amountToCredit = 100000;
        cmWrp.DateOfCredit = system.today(); 
        cmwrp.accountingPeriodRec = cp;
        cmwrp.isMRInvoice = true;
        ApplyCreditMemoController.InvoiceBreakOutWrapper invBrp = new ApplyCreditMemoController.InvoiceBreakOutWrapper();
        invBrp.lineItemRec = invList[0].invoice_line_items__r[0];
        invBrp.amountAllocated = 100;
        list<ApplyCreditMemoController.InvoiceBreakOutWrapper> invLineList = new list<ApplyCreditMemoController.InvoiceBreakOutWrapper>{invBrp};
            cmwrp.mrBreakOut = invLineList;
        ApplyCreditMemoController.applyCreditMemo(JSON.serialize(cmWrp));
        payment__c py = [select id,RecordType.Name,Amount__c,payment_status__c,
                         (select id from journal_entries__r),(select id,Amount_paid__c from Payment_Line_Items__r) from payment__c where invoice__c =:invList[0].id];
        system.assertEquals(100000.0,py.Amount__c,'Incorrect Payment Amount');
        system.assertEquals('Credit',String.ValueOf(py.RecordType.Name),'Incorrect Record Type');
        system.assertEquals(2,invList[0].journal_entries__r.size(),'Incorrect joural size');
        system.assertEquals('Approved',py.payment_status__c,'Incorrect status');
        system.assertEquals(0,py.journal_Entries__r.size(),'Incorrect journal size');
        system.assertEquals(1,py.Payment_Line_Items__r.size(),'Incorrect line item size');
        system.assertEquals(100,py.Payment_Line_Items__r[0].Amount_paid__c,'Incorrect Line item amount');
        
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c,(select id,name,credit__c,debit__c,Journal_Status__c from journal_entries__r) 
              from credit_memo__c where id=:cm.id];
        
        
        system.assertEquals(6,cm.journal_Entries__r.size(),'Incorrect journals size');
        
        Test.stopTest();
    }
    
    @istest
    public static void testApplyCredit(){
        Test.StartTest();
        LeaseWareUtils.clearFromTrigger();
        Calendar_Period__c cp = [select id from Calendar_Period__c where name ='Dummy1' ];
        Credit_memo__c cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c, Credit_Memo_Date__c, Credit_Memo_Reason__c, Due_Date__c, Lease__c, Lessee__c, Memo__c, Status__c, Allocated_Amount__c, Y_Hidden_IsStandalone__c  from Credit_Memo__c  limit 1];
        list<invoice__c> invList = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c,
                                    (select id,name from journal_entries__r )from Invoice__c where rent__c !=null order by rent__r.start_date__c];
        ApplyCreditMemoController.CreditMemoWrapper cmWrp = new ApplyCreditMemoController.CreditMemoWrapper();
        cmWrp.creditMemoRec = cm;
        cmWrp.invoiceRec = invList[0];
        cmWrp.amountToCredit = 100000;
        cmWrp.DateOfCredit = system.today();  
        cmwrp.accountingPeriodRec = cp;
        ApplyCreditMemoController.applyCreditMemo(JSON.serialize(cmWrp));
        
        payment__c py = [select id,RecordType.Name,Amount__c,payment_status__c,(select id from journal_entries__r) from payment__c where invoice__c =:invList[0].id];
        system.assertEquals(100000.0,py.Amount__c,'Incorrect Amount');
        system.assertEquals('Credit',String.ValueOf(py.RecordType.Name),'Incorrect record type');
        system.assertEquals(2,invList[0].journal_entries__r.size(),'Incorrect journal size');
        system.assertEquals(2,invList[1].journal_entries__r.size(),'Incorrect journals size');
        system.assertEquals('Approved',py.payment_status__c,'Incorrect payment status');
        system.assertEquals(0,py.journal_Entries__r.size(),'Incorrect journals size of payments');
        
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c,(select id,name,credit__c,debit__c,Journal_Status__c from journal_entries__r) 
              from credit_memo__c where id=:cm.id];
        
        system.assertEquals(100000.0,cm.Allocated_Amount__c,'Incorrect cm allocated amt');
        system.assertEquals(100000.0,cm.Available_Credit_Memo_Amount_F__c,'Incorrect available credit amount');
        system.assertEquals(6,cm.journal_Entries__r.size(),'Incorrect journals size');
        for(Journal_entry__c je : cm.journal_Entries__r){
            system.debug('je.name:::'+je.name+'::'+je.credit__c+':::'+je.debit__c);
            if(je.name=='AR - Rent')system.assertEquals(100000, je.credit__c,'Incorrect amt');
            else if(je.name=='Credit Memo Revenue Reduction' && je.Journal_Status__c.equals('Credit Memo'))system.assertEquals(200000, je.debit__c,'Incorrect amt');
            else if(je.name=='Unapplied Credit Memo'&& je.Journal_Status__c.equals('Credit Memo'))system.assertEquals(200000, je.credit__c,'Incorrect amt');	
            else if(je.name=='Base Rent Income')system.assertEquals(100000, je.debit__c,'Incorrect amt');	
        }
        //apply same credit memo not another invoice
        LeaseWareUtils.clearFromTrigger();
        ApplyCreditMemoController.CreditMemoWrapper cmWrp1 = new ApplyCreditMemoController.CreditMemoWrapper();
        cmWrp1.creditMemoRec = cm;
        cmWrp1.invoiceRec = invList[1];
        cmWrp1.amountToCredit = 50000;
        cmWrp1.DateOfCredit = system.today();
        cmwrp1.accountingPeriodRec = cp;        
        ApplyCreditMemoController.applyCreditMemo(JSON.serialize(cmWrp1));
        
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c,(select id,name,credit__c,debit__c from journal_entries__r) 
              from credit_memo__c where id=:cm.id];
        system.assertEquals(150000.0,cm.Allocated_Amount__c,'Incorrect cm allocated amt');
        system.assertEquals(50000,cm.Available_Credit_Memo_Amount_F__c,'Incorrect available credit amount');
        system.assertEquals(10,cm.journal_Entries__r.size(),'Incorrect journal size');
        for(Journal_entry__c je : cm.journal_Entries__r){
            system.debug('je.name:::'+je.name+'::'+je.credit__c+':::'+je.debit__c);  
        }
        
        Test.StopTest();
    }
    
    @istest
    public static void testFetchDataFunctions(){
        Test.StartTest();
        LeaseWareUtils.clearFromTrigger();
        Calendar_Period__c cp = [select id from Calendar_Period__c where name ='Dummy1' ];
        Credit_memo__c cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c, Credit_Memo_Date__c, Credit_Memo_Reason__c, Due_Date__c, Lease__c, Lessee__c, Memo__c, Status__c, Allocated_Amount__c, Y_Hidden_IsStandalone__c  from Credit_Memo__c  limit 1];
        list<invoice__c> invList = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c,
                                    (select id,name from journal_entries__r )from Invoice__c where rent__c !=null order by rent__r.start_date__c];
        
        ApplyCreditMemoController.CreditMemoWrapper cmWrp = ApplyCreditMemoController.fetchInitData(cm.Id);
        if(!invList.isEmpty()){
            cmWrp = ApplyCreditMemoController.fetchInitData(invList[0].Id);
            ApplyCreditMemoController.fetchInvoiceLineItemData(JSON.serialize(cmWrp));
        }
        
        invList = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c,
                   (select id,name from journal_entries__r ),
                   (select id,name,Assembly_Utilization__c,Assembly__c,Module__c,Assembly_MR_Info__c from invoice_line_items__r)
                   from Invoice__c where Utilization_Report__c != null];
        
        if(!invList.isEmpty()){
            cmWrp = ApplyCreditMemoController.fetchInitData(invList[0].Id);
            ApplyCreditMemoController.fetchInvoiceLineItemData(JSON.serialize(cmWrp));
        }
        Test.StopTest();
    }

    @istest
    public static void testCreditMemoReversal(){
        Test.StartTest();
        LeaseWareUtils.clearFromTrigger();
        lease__c lease = [select id,Accounting_Setup_P__c from lease__c limit 1];
        lease.Accounting_Setup_P__c = null;
        update lease;

        Credit_memo__c cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c, Credit_Memo_Date__c, Credit_Memo_Reason__c, Due_Date__c, Lease__c, Lessee__c, Memo__c, Status__c, Allocated_Amount__c, Y_Hidden_IsStandalone__c  from Credit_Memo__c  limit 1];
        list<invoice__c> invList = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c
                                   from Invoice__c where rent__c !=null order by rent__r.start_date__c];
        list<invoice__c> invList1 = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c,
                                   (select id,name,Assembly_Utilization__c,Assembly__c,Module__c,Assembly_MR_Info__c from invoice_line_items__r)
                                   from Invoice__c where Utilization_Report__c !=null ];
        ApplyCreditMemoController.CreditMemoWrapper cmWrp = new ApplyCreditMemoController.CreditMemoWrapper();
        cmWrp.creditMemoRec = cm;
        cmWrp.invoiceRec = invList[0];
        cmWrp.amountToCredit = 100000;
        cmWrp.DateOfCredit = system.today();  
        ApplyCreditMemoController.applyCreditMemo(JSON.serialize(cmWrp));
        
        payment__c py = [select id,RecordType.Name,Amount__c,payment_status__c,(select id from journal_entries__r) from payment__c where invoice__c =:invList[0].id];
        system.assertEquals(100000.0,py.Amount__c,'Incorrect Amount');
        system.assertEquals('Credit',String.ValueOf(py.RecordType.Name),'Incorrect record type');
        system.assertEquals('Approved',py.payment_status__c,'Incorrect payment status');
        
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c
              from credit_memo__c where id=:cm.id];
        
        system.assertEquals(100000.0,cm.Allocated_Amount__c,'Incorrect cm allocated amt');
        system.assertEquals(100000.0,cm.Available_Credit_Memo_Amount_F__c,'Incorrect available credit amount');
        
        //apply same credit memo not another invoice
        LeaseWareUtils.clearFromTrigger();
        ApplyCreditMemoController.CreditMemoWrapper cmWrp1 = new ApplyCreditMemoController.CreditMemoWrapper();
        cmWrp1.creditMemoRec = cm;
        cmWrp1.invoiceRec = invList1[0];
        cmWrp1.amountToCredit = 50000;
        cmWrp1.DateOfCredit = system.today(); 
        ApplyCreditMemoController.InvoiceBreakOutWrapper invBrp = new ApplyCreditMemoController.InvoiceBreakOutWrapper();
        invBrp.lineItemRec = invList1[0].invoice_line_items__r[0];
        invBrp.amountAllocated = 5000;
        list<ApplyCreditMemoController.InvoiceBreakOutWrapper> invLineList = new list<ApplyCreditMemoController.InvoiceBreakOutWrapper>{invBrp};
        cmwrp1.mrBreakOut = invLineList;
        ApplyCreditMemoController.applyCreditMemo(JSON.serialize(cmWrp1));
        
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c from credit_memo__c where id=:cm.id];
        system.assertEquals(150000.0,cm.Allocated_Amount__c,'Incorrect cm allocated amt');
        system.assertEquals(50000,cm.Available_Credit_Memo_Amount_F__c,'Incorrect available credit amount');
        
        LeaseWareUtils.TriggerDisabledFlag=true;               
        cm.status__c = 'Cancelled-Pending';
            update cm;
        LeaseWareUtils.TriggerDisabledFlag=false;
        LeaseWareUtils.clearFromTrigger();
        cm.status__c = 'Cancelled';
        update cm;
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c,(select id,payment_status__c from view_payments__r) from credit_memo__c where id=:cm.id];
        system.assertEquals(0.0,cm.Allocated_Amount__c,'Incorrect cm allocated amt');
        system.assertEquals(200000.0,cm.Available_Credit_Memo_Amount_F__c,'Incorrect available credit amount');
        system.assertEquals('Cancelled',cm.view_payments__r[0].payment_status__c);
        system.assertEquals('Cancelled',cm.view_payments__r[1].payment_status__c);
        Test.StopTest();
    }
    @istest
    public static void testCreditReversal(){
        Test.StartTest();
        LeaseWareUtils.clearFromTrigger();
        lease__c lease = [select id,Accounting_Setup_P__c from lease__c limit 1];
        lease.Accounting_Setup_P__c = null;
        update lease;

        Credit_memo__c cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c, Credit_Memo_Date__c, Credit_Memo_Reason__c, Due_Date__c, Lease__c, Lessee__c, Memo__c, Status__c, Allocated_Amount__c, Y_Hidden_IsStandalone__c  from Credit_Memo__c  limit 1];
        list<invoice__c> invList = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c
                                   from Invoice__c where rent__c !=null order by rent__r.start_date__c];
        list<invoice__c> invList1 = [SELECT Id, Lease__c, Lease__r.Lessee__c, Lease__r.Operator__c, Balance_Due__c, RecordType.Name,RecordTypeId,invoice_type__c,
                                   (select id,name,Assembly_Utilization__c,Assembly__c,Module__c,Assembly_MR_Info__c from invoice_line_items__r)
                                   from Invoice__c where Utilization_Report__c !=null ];
        ApplyCreditMemoController.CreditMemoWrapper cmWrp = new ApplyCreditMemoController.CreditMemoWrapper();
        cmWrp.creditMemoRec = cm;
        cmWrp.invoiceRec = invList[0];
        cmWrp.amountToCredit = 100000;
        cmWrp.DateOfCredit = system.today();  
        ApplyCreditMemoController.applyCreditMemo(JSON.serialize(cmWrp));
        
        payment__c py = [select id,RecordType.Name,Amount__c,payment_status__c,(select id from journal_entries__r) from payment__c where invoice__c =:invList[0].id];
        system.assertEquals(100000.0,py.Amount__c,'Incorrect Amount');
        system.assertEquals('Credit',String.ValueOf(py.RecordType.Name),'Incorrect record type');
        system.assertEquals('Approved',py.payment_status__c,'Incorrect payment status');
        
        LeaseWareUtils.clearFromTrigger();
        ApplyCreditMemoController.CreditMemoWrapper cmWrp1 = new ApplyCreditMemoController.CreditMemoWrapper();
        cmWrp1.creditMemoRec = cm;
        cmWrp1.invoiceRec = invList1[0];
        cmWrp1.amountToCredit = 50000;
        cmWrp1.DateOfCredit = system.today(); 
        ApplyCreditMemoController.InvoiceBreakOutWrapper invBrp = new ApplyCreditMemoController.InvoiceBreakOutWrapper();
        invBrp.lineItemRec = invList1[0].invoice_line_items__r[0];
        invBrp.amountAllocated = 5000;
        list<ApplyCreditMemoController.InvoiceBreakOutWrapper> invLineList = new list<ApplyCreditMemoController.InvoiceBreakOutWrapper>{invBrp};
        cmwrp1.mrBreakOut = invLineList;
        ApplyCreditMemoController.applyCreditMemo(JSON.serialize(cmWrp1));
        
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c from credit_memo__c where id=:cm.id];
        system.assertEquals(150000.0,cm.Allocated_Amount__c,'Incorrect cm allocated amt');
        system.assertEquals(50000,cm.Available_Credit_Memo_Amount_F__c,'Incorrect available credit amount');
        
        LeaseWareUtils.TriggerDisabledFlag=true;               
        py.payment_status__c = 'Cancelled-Pending';
            update py;
        LeaseWareUtils.TriggerDisabledFlag=false;
        LeaseWareUtils.clearFromTrigger();
        py.payment_status__c = 'Cancelled';
        update py;
        cm = [SELECT Id, Amount__c, Available_Credit_Memo_Amount_F__c,Allocated_Amount__c,status__c,(select id,payment_status__c from view_payments__r where id != :py.id) from credit_memo__c where id=:cm.id];
        system.assertEquals(50000,cm.Allocated_Amount__c,'Incorrect cm allocated amt');
        system.assertEquals(150000,cm.Available_Credit_Memo_Amount_F__c,'Incorrect available credit amount');
        system.assertEquals('Approved',cm.status__c,'Incorrect status');
        system.assertEquals('Approved',cm.view_payments__r[0].payment_status__c);
        Test.StopTest();
    }
    

}