@isTest
public class TestAccountingSetup {
    
    public static testMethod void testAccsetup(){
        
        LeaseWareUtils.clearFromTrigger();
       	test.startTest();
        parent_Accounting_setup__c accSetup1 = new parent_Accounting_Setup__c(name='test');
        insert accSetup1;
       	
        list<Accounting_setup__c> acclist =  [select id from accounting_setup__c where  parent_Accounting_Setup__c = :accSetup1.id];
        system.assertEquals(9, acclist.size(),'The Size of list is not matching');
        
        
        LeaseWareUtils.clearFromTrigger();
        
        Parent_Accounting_setup__c accSetup2 = new parent_Accounting_Setup__c(name='test',default__c=true);
        insert accSetup2;
        
        
        LeaseWareUtils.clearFromTrigger();
        try{
            accSetup2.Default__c = false;
        	update accSetup2;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Please select another default accounting setup')) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
        }
       
        
        
        LeaseWareUtils.clearFromTrigger();
        accsetup1.Default__c = true;
        update accsetup1;
        
        
        LeaseWareUtils.clearFromTrigger();
        try{
            
        	delete accSetup1;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Please select another default accounting setup before deleting')) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
        }
       
        
        
        Test.stopTest();
        
    }

}