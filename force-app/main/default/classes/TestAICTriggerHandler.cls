@isTest
public class TestAICTriggerHandler {
    
    @testSetup
    public static void testSetup(){
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                   
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,         
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                         
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = system.today().addDays(30),
            LLP_MR_Rate_Configuration__c = 'Per LLP',
            Reserve_Type__c = 'End of Lease Adjustment',
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                         
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                   
            Status__c = 'Available',                  
            Awarded__c = false,  
            TSN__c = 1.00,                               
            CSN__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;

        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;          
        

        Deal__c deal = new Deal__c();
        deal.Aircraft__c = 'Engine 1';
        deal.Name = 'Test';
        insert deal;
        
        Transaction__c tran = new Transaction__c();
        tran.Name ='abc';
        tran.Seller_Airline__c = testOperator.Id;
        tran.Seller_Type__c = 'Airline';
        insert tran;
        
        Aircraft_In_Transaction__c AIT = new Aircraft_In_Transaction__c();
        AIT.CSN_LG_Left__c = 56;
        AIT.Transaction__c = tran.Id;
        insert AIT;
        
        Aircraft_In_Deal__c AIC = new Aircraft_In_Deal__c();
        AIC.Available_By__c = System.today().addDays(30);
        AIC.Name ='def';
        AIC.Aircraft__c = testAircraft.Id;
        AIC.Marketing_Deal__c = deal.Id;
        insert AIC;
        
        Aircraft_In_Deal__c AICnew = new Aircraft_In_Deal__c();
        AICnew.Available_By__c = System.today();
        AICnew.Name ='abc';
        AICnew.Aircraft_In_Transaction__c = AIT.Id;
        AICnew.Next_Operator__c = testOperator.Id;
        AICnew.Marketing_Deal__c = deal.Id;
        insert AICnew;
        
        Deal_Proposal__c proposal = new Deal_Proposal__c();
        proposal.Deal__c = deal.Id;
        proposal.MSN_s_Deal_Proposal__c = 'abc';
        proposal.Interested_MSN_List__c = 'old';
        proposal.Prospect__c = testOperator.Id;
        insert proposal;
        
        MSN_s_Of_Interest__c MOI = new MSN_s_Of_Interest__c();
        MOI.Deal_Proposal__c = proposal.Id;
        MOI.Aircraft__c = AIC.Id;
        insert MOI;
        
        LeaseWareUtils.unsetTriggers('AICTriggerHandlerAfter');
        
        Aircraft_In_Deal__c AICdata = [Select id, Next_Operator__c, Name,Available_By__c from Aircraft_In_Deal__c Limit 1];
        AICdata.Name = 'New';
        AICdata.Next_Operator__c= testOperator.Id;
        AICdata.Available_By__c = System.today().addDays(30);
        update AICdata;
     }
    
    
    @isTest
    public static void insertAircraft_In_Deal_record(){
        
        Deal__c deal = new Deal__c();
        deal.Aircraft__c = 'Engine 2';
        deal.Name = 'Test';
        insert deal; 
        Operator__c testOperator = [select id, name from Operator__c];
        Aircraft_In_Transaction__c testAIT = [SELECT ID ,NAME FROM Aircraft_In_Transaction__c];
        Aircraft_In_Deal__c AICnew = new Aircraft_In_Deal__c();
        AICnew.Available_By__c = System.today();
        AICnew.Name ='abc';
        AICnew.Aircraft_In_Transaction__c = testAIT.Id;
        AICnew.Next_Operator__c = testOperator.Id;
        AICnew.Marketing_Deal__c =deal.Id;
        insert AICnew;
        delete AICnew;
      }
    
    @isTest
    public static void deletedata(){
        List<MSN_s_Of_Interest__c> MSIList = [Select id, name from MSN_s_Of_Interest__c ];
        delete MSIList; 
        List<Aircraft_In_Deal__c> AIDlist = [select id,name from Aircraft_In_Deal__c ];
        System.debug('AIDlist:'+AIDlist);
        delete AIDlist[1];
    }
}