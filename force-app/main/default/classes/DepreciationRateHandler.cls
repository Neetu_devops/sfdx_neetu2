public class DepreciationRateHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'DepreciationRateHandlerBefore';
    private final string triggerAfter = 'DepreciationRateHandlerAfter';
    public DepreciationRateHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('DepreciationRateHandler.beforeInsert(+)');

	    system.debug('DepreciationRateHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('DepreciationRateHandler.beforeUpdate(+)');
    	/*
    	set<Id> setMPIds = new set<Id>();
    	map<Id,Integer> mapCount = new map<Id,Integer>();
    	
    	Depreciation_Rate__c[] listNew = (list<Depreciation_Rate__c>)trigger.new ;
    	
    	for(Depreciation_Rate__c curMP:listNew){
			setMPIds.add(curMP.Depreciation_schedule__c);
		}

		Depreciation_schedule__c[] resultRec = [Select Id,status__c From Depreciation_schedule__c 
                                   		where Id in :setMPIds ] ;
                                 		
		for(Depreciation_schedule__c res:resultRec){
		    if('Active'.equals(res.status__c)){
		    	mapCount.put(res.Id,1);
		    	
		    }
		}    	
    	for(Depreciation_Rate__c curMP:listNew){
			if(mapCount.containsKey(curMP.Depreciation_schedule__c)){
				curMP.addError('The status of Depreciation Schedule is Active. Please change the status to modify/delete.');
			}
			
		}   
    	*/
    	system.debug('DepreciationRateHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('DepreciationRateHandler.beforeDelete(+)');
    	
    	set<Id> setMPIds = new set<Id>();
    	map<Id,Integer> mapCount = new map<Id,Integer>();
    	
    	Depreciation_Rate__c[] listNew = (list<Depreciation_Rate__c>)trigger.old ;
    	
    	for(Depreciation_Rate__c curMP:listNew){
			setMPIds.add(curMP.Depreciation_schedule__c);
		}

		Depreciation_schedule__c[] resultRec = [Select Id,status__c From Depreciation_schedule__c 
                                   		where Id in :setMPIds ] ;
                                 		
		for(Depreciation_schedule__c res:resultRec){
		    if('Active'.equals(res.status__c)){
		    	mapCount.put(res.Id,1);
		    	
		    }
		}    	
    	for(Depreciation_Rate__c curMP:listNew){
			if(mapCount.containsKey(curMP.Depreciation_schedule__c)){
				curMP.addError('The status of Depreciation Schedule is Active. Please change the status to modify/delete.');
			}
			
		}   
    	
    	system.debug('DepreciationRateHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationRateHandler.afterInsert(+)');
    	
    	system.debug('DepreciationRateHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationRateHandler.afterUpdate(+)');
    	
    	
    	system.debug('DepreciationRateHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationRateHandler.afterDelete(+)');
    	
    	
    	system.debug('DepreciationRateHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DepreciationRateHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('DepreciationRateHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


}