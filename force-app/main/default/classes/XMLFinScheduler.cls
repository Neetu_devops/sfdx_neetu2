public with sharing class XMLFinScheduler implements Schedulable, Database.Batchable<SObject>{


	
	final String JournalSource = 'LW';
	final String XML_Header = '<?xml version="1.0" encoding="iso-8859-1"?>\n' ;
	final String userName = 'LEA';

	// map vs Invoice Type to Journal Type
	final private map<String,string> mapJournalType = new map<String,string>{
		'A'=> '9****'	,		//***** CMS Journals *****
		'B'=> '9CAS'	,		//Cash Analysis Journal
		'C'=> '9CLA'	,		//Claims Interface CMS Journal
		'D'=> '9CMA'	,		//CMS Accrual Journal
		'Finance Lease'=> '9CMC'	,		//CMS Standard Journal
		'F'=> '9EXP'	,		//Expenses System Journal
		'G'=> '9NBV'	,		//Netbook Value Depreciation Journal
		'H'=> '9OBA'	,		//Opening Balance Journal
		'I'=> '9REC'	,		//Security Deposit Journal
		'Aircraft MR' => '9ZZZZ'	,
		'Assembly MR' => '9ZZZZ'	,
		'Rent'=> '9ZZZZ'	,		//CMS Cash Allocation Journal
		//'K'=> 'M****	,		//-----CMS/AerData Maintenance Reserve Journals-----
		'L'=> 'MINT'	,		//Maintenance Reserve Interest Journal
		'M'=> 'MOPB'	,		//Maintenance Reserve Opening Balance Journal
		'N'=> 'MREC'			//Maintenance Reserve Reconciliation Journal
            
             };
    
	
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new XMLFinScheduler());
    }
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('select Id from Lessor__c limit 1');
    }
    public void execute(Database.BatchableContext bc, List<sObject> scope) {
        System.debug('XMLFinScheduler.execute : Start()');

		list<Document> documents = new list<Document>();

		// attach as Document
		Document rentalDoc = createPayload(' and Invoice_Type__c = \'Rent\' ') ;
		rentalDoc.name = 'Rental Allocation - '+ System.now() +'.xml' ; 
		documents.add(rentalDoc);

		Document MRDoc = createPayload(' and Invoice_Type__c in (\'Assembly MR\',\'Aircraft MR\') ') ;
		MRDoc.name = 'Maintenance Allocation - '+ System.now() +'.xml' ; 
		documents.add(MRDoc);
		
		Document FLDoc = createPayload(' and Invoice_Type__c = \'Finance Lease\' ') ;
		FLDoc.name = 'Finance Lease - '+ System.now() +'.xml' ; 
		documents.add(FLDoc);
		// Dont send email on test run
		if(! Test.isRunningTest())				
			LeaseWareUtils.sendEmailToLWAdmins('LW: xml','Please find attached the file','TEXT',documents);   			
 
        System.debug('XMLFinScheduler.execute : End()');
    }
    public void finish(Database.BatchableContext bc) {
        System.debug('XMLFinScheduler.finish ');
        // No email or log send
    }
	
	private Document createPayload(String whereClause){
		
		//String userName = UserInfo.getUserName();
		Integer tabCounter=0;
		Boolean recordsFound=false;
		Xmlstreamwriter xmlW = new Xmlstreamwriter();
		
		String invoiceQuery = ' select Id,Name,Invoice_Date__c,Lease__c,Lease__r.Name,Invoice_Type__c,Lease__r.Serial_Number__c, Lease__r.Lessee__r.Name ' 
			 + ' ,Accounting_Description__c 	'
			 + ' ,(select Id,Name,Amount__c,GL_Account_Code__c,Legal_Entity__c,Intercompany__c,Sign__c from Invoice_Accounts__r)	'
			 + ' from Invoice__c  where Status__c in ( \'Approved\', \'Paid\'  )  and XML_sent__c=false	';
		
		String paymentQuery	= ' select Id,Name,Payment_Date__c,Invoice__r.Lease__c,Invoice__r.Lease__r.Name,Invoice_Type__c,Invoice__r.Lease__r.Serial_Number__c, Invoice__r.Lease__r.Lessee__r.Name  '
			 + ' ,Invoice__r.Accounting_Description__c 	 '
			 + ' ,(select Id,Name,Amount__c,GL_Account_Code__c,Legal_Entity__c,Intercompany__c,Sign__c from Payment_Accounts__r) '
			 + ' from Payment__c  where  XML_sent__c=false  ';

		Invoice__c[] listInvoice = Database.query(invoiceQuery + whereClause);		
		
		// payments
		Payment__c[] listPayment = Database.query(paymentQuery + whereClause);
		
		
        if(!listInvoice.isEmpty() || !listPayment.isEmpty()){
        	System.debug('record found');
			
			// xml preperation - start	
			LeaseWareStreamUtils.startNodeXML(xmlW,'SSC',++tabCounter);
				LeaseWareStreamUtils.startNodeXML(xmlW,'User',++tabCounter);
					++tabCounter;
					LeaseWareStreamUtils.addNodeXML(xmlW,'Name',userName,tabCounter);
					tabCounter--;
				LeaseWareStreamUtils.endNodeXML(xmlW,'User',tabCounter--);
				LeaseWareStreamUtils.startNodeXML(xmlW,'SunSystemsContext',++tabCounter);
					tabCounter++;
					LeaseWareStreamUtils.addNodeXML(xmlW,'BusinessUnit','ABC',tabCounter);
					LeaseWareStreamUtils.addNodeXML(xmlW,'BudgetCode','A',tabCounter);
					tabCounter--;
				LeaseWareStreamUtils.endNodeXML(xmlW,'SunSystemsContext',tabCounter--);
				LeaseWareStreamUtils.startNodeXML(xmlW,'MethodContext',++tabCounter);
				LeaseWareStreamUtils.endNodeXML(xmlW,'MethodContext',tabCounter--);
				LeaseWareStreamUtils.startNodeXML(xmlW,'Payload',++tabCounter);
					LeaseWareStreamUtils.startNodeXML(xmlW,'Ledger',++tabCounter);
					// payload for each line
					// Invoice
					for(Invoice__c curRec:listInvoice){
						for(Invoice_Account_Code__c curChild : curRec.Invoice_Accounts__r){
							recordsFound =true;
							curRec.XML_sent__c = true;
							LeaseWareStreamUtils.startNodeXML(xmlW,'Line',++tabCounter);
								tabCounter++;
								LeaseWareStreamUtils.addNodeXML(xmlW,'JournalSource',JournalSource,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'JournalType',mapJournalType.get(curRec.Invoice_Type__c),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionDate',LeaseWareUtils.getDatetoString(curRec.Invoice_Date__c,'DDMMYYYY'),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AccountingPeriod',LeaseWareUtils.getDatetoString(curRec.Invoice_Date__c,'MMMYYYY'),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionReference',curRec.Name,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AccountCode',curChild.GL_Account_Code__c,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'Description',curRec.Accounting_Description__c==null?curRec.Name:curRec.Accounting_Description__c,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'DebitCredit',curChild.Sign__c,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'CurrencyCode','USD',tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionAmount',''+curChild.Amount__c,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode1',curChild.Legal_Entity__c,tabCounter);		
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode2',curChild.Intercompany__c,tabCounter);	
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode3',curRec.Lease__r.Serial_Number__c,tabCounter);	
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode4',(''+curRec.Lease__c).left(15),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode5',curRec.Lease__r.Lessee__r.Name,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode6',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode7',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode8',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode9',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode10',null,tabCounter);
								tabCounter--;												
							LeaseWareStreamUtils.endNodeXML(xmlW,'Line',tabCounter--);	
						}
					}
					//payments		
					for(Payment__c curRec:listPayment){
						for(Payment_Account_Code__c curChild : curRec.Payment_Accounts__r){
							recordsFound =true;
							curRec.XML_sent__c = true;
							LeaseWareStreamUtils.startNodeXML(xmlW,'Line',++tabCounter);
								tabCounter++;
								LeaseWareStreamUtils.addNodeXML(xmlW,'JournalSource',JournalSource,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'JournalType',mapJournalType.get(curRec.Invoice_Type__c),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionDate',LeaseWareUtils.getDatetoString(curRec.Payment_Date__c,'DDMMYYYY'),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AccountingPeriod',LeaseWareUtils.getDatetoString(curRec.Payment_Date__c,'MMMYYYY'),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionReference',curRec.Name,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AccountCode',curChild.GL_Account_Code__c,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'Description',curRec.Name,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'DebitCredit',curChild.Sign__c,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'CurrencyCode','USD',tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionAmount',''+curChild.Amount__c,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode1',curChild.Legal_Entity__c,tabCounter);		
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode2',curChild.Intercompany__c,tabCounter);	
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode3',curRec.Invoice__r.Lease__r.Serial_Number__c,tabCounter);	
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode4',(''+curRec.Invoice__r.Lease__c).left(15),tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode5',curRec.Invoice__r.Lease__r.Lessee__r.Name,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode6',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode7',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode8',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode9',null,tabCounter);
								LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode10',null,tabCounter);
								tabCounter--;												
							LeaseWareStreamUtils.endNodeXML(xmlW,'Line',tabCounter--);	
						}
					}									
					LeaseWareStreamUtils.endNodeXML(xmlW,'Ledger',tabCounter--);
				LeaseWareStreamUtils.endNodeXML(xmlW,'Payload',tabCounter--);					
			LeaseWareStreamUtils.endNodeXML(xmlW,'SSC',tabCounter--);
			// xml preperation - end	
			
			System.debug('XML is ready'); 
 			System.debug(XML_Header + xmlW.getXmlString());
 			

       		    			
        }

 		// need to see how to update paid Invoices : right now getting error
 		LeaseWareUtils.setFromTrigger('XMLFinSchedulerforInvoice');
 		update listInvoice; 
 		LeaseWareUtils.unsetTriggers('XMLFinSchedulerforInvoice');

 		LeaseWareUtils.setFromTrigger('XMLFinSchedulerforPayment');
 		update listPayment; 
 		LeaseWareUtils.unsetTriggers('XMLFinSchedulerforPayment');
 		       
        Document doc = new Document();
       	doc.AuthorId = UserInfo.getUserId();
	   	doc.FolderId = UserInfo.getUserId();
	   	doc.Body = Blob.valueOf(XML_Header + xmlW.getXmlString());
	   	return doc;
	}    
}