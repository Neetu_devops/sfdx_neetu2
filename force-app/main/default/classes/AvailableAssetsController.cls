public without sharing class AvailableAssetsController {

    @AuraEnabled
    public static AssetsOnLeaseController.ContainerWrapper getAvailableAssets(){
        return  AssetsOnLeaseController.getData(true);
    }

}