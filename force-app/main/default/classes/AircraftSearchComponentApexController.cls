public class AircraftSearchComponentApexController {
    @auraEnabled
    public static DependentPicklistWrapper retrievePicklistValues(){
        DependentPicklistWrapper picklistValues = new DependentPicklistWrapper();
        List<String> aircraftTypeValues = new List<String>();
        Schema.DescribeSObjectResult objSchema = Customer_Interaction_Log__c.sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objSchema.fields.getmap();
        for(Schema.Picklistentry item : fieldmap.get('leaseworks__Aircraft_Type__c').getDescribe().getpicklistValues()){
            aircraftTypeValues.add(item.getLabel());
        }
        picklistValues.aircraftTypeValues = aircraftTypeValues;
        
        List<String> vintageRange = new List<String>();
        objSchema = Customer_Interaction_Log__c.sObjectType.getDescribe();
        fieldMap = objSchema.fields.getmap();
        for(Schema.Picklistentry item : fieldmap.get('leaseworks__Vintage_Range__c').getDescribe().getpicklistValues()){
            vintageRange.add(item.getLabel());
        }
        picklistValues.vintageRange = vintageRange;
        
        picklistValues.aircraftVariantValues = DependentPicklistValuesFinder.GetDependentOptions('leaseworks__Customer_Interaction_Log__c','leaseworks__Aircraft_Type__c','leaseworks__Variant__c');
        picklistValues.engineTypeValues = DependentPicklistValuesFinder.GetDependentOptions('leaseworks__Customer_Interaction_Log__c','leaseworks__Aircraft_Type__c','leaseworks__Engine_Type_List__c');
        picklistValues.engineVariantValues = DependentPicklistValuesFinder.GetDependentOptions('leaseworks__Customer_Interaction_Log__c','leaseworks__Engine_Type_List__c','leaseworks__Engine_Variant__c');
        
        LeaseWorksSettings__c custSetting = LeaseWorksSettings__c.getOrgDefaults();
        Integer currentYear = System.Today().year();
        picklistValues.minVintage = custSetting.Max_Vintage_From_Range__c == null ? currentYear - 20 : currentYear - custSetting.Max_Vintage_From_Range__c;
        picklistValues.maxVintage = custSetting.Max_Vintage_To_Range__c == null ? currentYear + 10 : currentYear + custSetting.Max_Vintage_To_Range__c;
        return picklistValues;
    }
    
    @auraEnabled
    public static searchResultsWrapper searchAircraft(String aircraftType,String aircraftVariant, String engineType,String vintage,String vintageRange, Boolean searchTransaction, String leaseDateTo,String leaseDateFrom){
        String SelectClauseforAC = ' select Id,Name,leaseworks__Lease__c,leaseworks__Lease__r.Name,leaseworks__LOPA_URL__c, leaseworks__Lease__r.leaseworks__Rent__c,leaseworks__Registration_Number__c,leaseworks__Maximum_Operational_Take_Lbs__c,   '
            + ' leaseworks__Engine_Type__c,leaseworks__Date_of_Manufacture__c,leaseworks__Aircraft_ID__c,leaseworks__Aircraft_Variant__c '
            + '  from leaseworks__Aircraft__c where leaseworks__Status__c not in (\'Sold\') ' ;
        
        
        String WhereClauseforAC = ' ';
        
        //apply the filters
        if(leaseDateTo != null && leaseDateTo !=''){
            Date leaseDate = Date.valueOf(leaseDateTo);
            WhereClauseforAC +=' AND leaseworks__Lease__r.leaseworks__Lease_End_Date_New__c <=:  leaseDate ';
        }
        if(leaseDateFrom != null && leaseDateFrom !=''){
            Date leaseDate = Date.valueOf(leaseDateFrom);
            WhereClauseforAC += ' AND leaseworks__Lease__r.leaseworks__Lease_End_Date_New__c >=:leaseDate   ';
        }
        if(aircraftType != null && aircraftType != ''){
            WhereClauseforAC += ' AND leaseworks__Aircraft_Type__c =: aircraftType ';
        }
        if(aircraftVariant != null && aircraftVariant !=''){
            WhereClauseforAC += ' AND leaseworks__Aircraft_Variant__c =: aircraftVariant ';
        }
        if(engineType != null && engineType!=''){
            WhereClauseforAC += ' AND leaseworks__Engine_Type__c =: engineType ';
        }
        if(vintage != null && vintage != '' && vintageRange != null && vintageRange != ''){
            Integer intVintage = Integer.valueOf(vintage);
            Integer intVintageRange = Integer.valueOf(vintageRange);
            WhereClauseforAC += ' AND CALENDAR_YEAR(leaseworks__Date_of_Manufacture__c) >=: intVintage  ';
            WhereClauseforAC += ' AND CALENDAR_YEAR(leaseworks__Date_of_Manufacture__c) <=: intVintageRange';
        }
        
        String WhereClauseforAIT = WhereClauseforAC;
        
        searchResultsWrapper resultWrapper = new searchResultsWrapper();
        List<Aircraft__c> listOfAircrafts = database.query(SelectClauseforAC + WhereClauseforAC);
        //List<Aircraft__c> listOfAircrafts = [select id,Name,leaseworks__Aircraft_ID__c,leaseworks__Aircraft_Variant__c from leaseworks__Aircraft__c];
        resultWrapper.listOfAircrafts = listOfAircrafts;
        resultWrapper.listOfAircraftInTransactions = new List<Aircraft_In_Transaction__c>();
        if(searchTransaction){
            String SelectClauseforAIT = ' select Id,Name,leaseworks__Aircraft_Type_New__c,leaseworks__Aircraft__r.leaseworks__Date_of_Manufacture__c,  '
                + ' leaseworks__Price__c, leaseworks__Aircraft_Variant__c ,leaseworks__YoM__c,leaseworks__Engine__c '
                + '  from leaseworks__Aircraft_In_Transaction__c ' 
                + '   ';
            
            WhereClauseforAIT = ' where leaseworks__Status__c=\'Active\' AND leaseworks__Transaction__r.leaseworks__Transaction_Type__c in ( \'Acquisition\') ' ;
            
            //apply the filters
            if(leaseDateTo != null && leaseDateTo !=''){
                Date leaseDate = Date.valueOf(leaseDateTo);
                WhereClauseforAIT +=' AND leaseworks__Lease__r.leaseworks__Lease_End_Date__c <=:  leaseDate ';
            }
            if(leaseDateFrom != null && leaseDateFrom !=''){
                Date leaseDate = Date.valueOf(leaseDateFrom);
                WhereClauseforAIT += ' AND leaseworks__Lease__r.leaseworks__Lease_End_Date__c >=:leaseDate   ';
            }
            if(aircraftType != null && aircraftType != ''){
                WhereClauseforAIT += ' AND leaseworks__Aircraft_Type_New__c =: aircraftType ';
            }
            if(aircraftVariant != null && aircraftVariant !=''){
                WhereClauseforAIT += ' AND leaseworks__Aircraft_Variant__c =: aircraftVariant ';
            }
            
            /*if(vintage != null && vintage != '' && vintageRange != null && vintageRange != ''){
Integer intVintage = Integer.valueOf(vintage);
Integer intVintageRange = Integer.valueOf(vintageRange);
WhereClauseforAC += ' AND CALENDAR_YEAR(leaseworks__YoM__c) >=: intVintage  ';
WhereClauseforAC += ' AND CALENDAR_YEAR(leaseworks__YoM__c) <=: intVintageRange';
}*/
            System.debug(SelectClauseforAIT + WhereClauseforAIT);
            System.debug(database.query(SelectClauseforAIT + WhereClauseforAIT));
            List<Aircraft_In_Transaction__c> listOfAircraftsInTransactions = database.query(SelectClauseforAIT + WhereClauseforAIT);
            //List<Aircraft_In_Transaction__c> listOfAircraftsInTransactions = [select id,Name,leaseworks__Engine__c,leaseworks__Engine_Variant__c from leaseworks__Aircraft_In_Transaction__c];
            resultWrapper.listOfAircraftInTransactions = listOfAircraftsInTransactions;
        }
        return resultWrapper;
    }
    
    @auraEnabled
    public static sObject getRecordDetail(String recId,String objName){
        String queryString = '';
        if(objName =='leaseworks__Aircraft__c'){
            queryString = 'select Id, Name,leaseworks__LOPA_URL__c,leaseworks__Lease__r.leaseworks__Rent__c,leaseworks__Registration_Number__c,leaseworks__Maximum_Operational_Take_Lbs__c,   '
                + ' leaseworks__Engine_Type__c,leaseworks__Date_of_Manufacture__c,leaseworks__Aircraft_ID__c,leaseworks__Aircraft_Variant__c '
                + '  from '+objName+' where id =: recId';
        }else if(objName == 'leaseworks__Aircraft_In_Transaction__c'){
            queryString = 'select Id,Name,leaseworks__Aircraft_Type_New__c,leaseworks__Aircraft__r.leaseworks__Date_of_Manufacture__c,  '
                + ' leaseworks__Price__c, leaseworks__Aircraft_Variant__c ,leaseworks__YoM__c,leaseworks__Engine__c '
                + '  from leaseworks__Aircraft_In_Transaction__c where id =: recId';
        }
        
        return Database.query(queryString);
    }
    
    
    public class DependentPicklistWrapper{
        @auraEnabled public List<String> aircraftTypeValues{get;set;}
        @auraEnabled public Map<String,List<String>> aircraftVariantValues{get;set;}
        @auraEnabled public Map<String,List<String>> engineTypeValues{get;set;}
        @auraEnabled public Map<String,List<String>> engineVariantValues{get;set;}
        @auraEnabled public List<String> vintageRange{get;set;}
        @auraEnabled public Decimal minVintage{get;set;}
        @auraEnabled public Decimal maxVintage{get;set;}
    }
    
    
    public class searchResultsWrapper{
        @auraEnabled public List<Aircraft__c> listOfAircrafts{get;set;}
        @auraEnabled public List<Aircraft_In_Transaction__c> listOfAircraftInTransactions{get;set;}
    }
    
}