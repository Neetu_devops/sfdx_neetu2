@isTest
public with sharing class TestGenericActionOverrideController {
    @isTest public static void allCases(){
        Test.startTest();
        String nameSpacePrifix = LeaseWareUtils.getNamespacePrefix() == null ? '' : LeaseWareUtils.getNamespacePrefix();
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',
            Status__c = 'Approved',
            Rent_Payments_Before_Holidays__c = false,
            Revenue__c = 0.00,         
            Current_Lessee__c = true
        );
        insert testOperator;
        System.assert(testOperator.Id != null);
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = system.today().addDays(+365),
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        System.assert(leaseRecord.Id != null);
		
		Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
            LeasewareUtils.clearFromTrigger();insert legalEntity;
        Parent_Accounting_Setup__c accountingSetup = new Parent_Accounting_Setup__c(Name='TestAS');
            LeasewareUtils.clearFromTrigger();insert accountingSetup;
            
        Calendar_Period__c cp = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1),closed__c=true);
            LeasewareUtils.clearFromTrigger();insert cp;

        Chart_of_accounts__c coa1 = new Chart_of_Accounts__c(name='Lessor COA',GL_Code__c='0001',GL_Account_Description__c='Credit Memo RR');
            Chart_of_accounts__c coa2 = new Chart_of_Accounts__c(name='Lessor COA',GL_Code__c='0002',GL_Account_Description__c='Credit Memo Unapplied');
            list<Chart_of_accounts__c> coalist = new list<Chart_of_accounts__c> {coa1,coa2};
            LeasewareUtils.clearFromTrigger();insert coalist;
			
        Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                              Phone_Number__c='2342', Email_Address__c='a@lease.com',Accounting_enabled__c = true,CM_Rev_Reduction__c = coa1.id,Unapplied_cm__c=coa2.id); 
            LeasewareUtils.clearFromTrigger();insert  LWSetUp;
			
        credit_memo__c creditMemo = new credit_memo__c(amount__c = 20000.00,
                           Credit_Memo_Date__c=system.today(),company__c =legalEntity.id,
                           lessee__c = testOperator.Id);
            LeasewareUtils.clearFromTrigger(); insert creditMemo;
        
        //When nothing is passed. Name will be fetched back.
        GenericActionOverrideController.fetchInitData(null, null, null);
        
        //When only ObjectName is known
        GenericActionOverrideController.fetchInitData(nameSpacePrifix+'Assembly_MR_Rate__c', null, null);
		
        String recordTypeId = '';
        if(Schema.getGlobalDescribe().containsKey(nameSpacePrifix+'Assembly_MR_Rate__c') && Schema.getGlobalDescribe().get(nameSpacePrifix+'Assembly_MR_Rate__c').getDescribe().getRecordTypeInfosByName().containsKey('MR')){
        	recordTypeId = Schema.getGlobalDescribe().get(nameSpacePrifix+'Assembly_MR_Rate__c').getDescribe().getRecordTypeInfosByName().get('MR').getRecordTypeId();
        }
        //When ObjectName and recordTypeId is known
        GenericActionOverrideController.fetchInitData(nameSpacePrifix+'Assembly_MR_Rate__c', recordTypeId, null);

        //When ObjectName, recordTypeId and parentRecordId is known.
        GenericActionOverrideController.fetchInitData(nameSpacePrifix+'Assembly_MR_Rate__c', recordTypeId, testOperator.Id);
        GenericActionOverrideController.fetchInitData(nameSpacePrifix+'Assembly_MR_Rate__c', recordTypeId, leaseRecord.Id);
        GenericActionOverrideController.fetchInitData(nameSpacePrifix+'Credit_Memo_Issuance__c',recordTypeId, creditMemo.Id);
        Test.stopTest();
    }
}