@isTest
public class Notify_MetadataServiceMock implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
            Notify_MetadataService.AsyncResult asyncRes = new Notify_MetadataService.AsyncResult();
            asyncRes.done= false;
            asyncRes.id='id=0Af6w00000676XJCAY';
            asyncRes.state='Queued';
            asyncRes.statusCode=null;

            Notify_MetadataService.deployResponse_element respElement = 
           new Notify_MetadataService.deployResponse_element();
           respElement.result = asyncRes;
            response.put('response_x', respElement); 
   }
}