public class CreditScoreTriggerHandler implements ITrigger {
    
    private final string triggerBefore = 'CreditScoreTriggerHandlerBefore';
    private final string triggerAfter = 'CreditScoreTriggerHandlerAfter';
    
    public CreditScoreTriggerHandler(){
    }
    
    public void bulkBefore(){}
    public void bulkAfter(){} 
    
    public void beforeInsert(){
        system.debug('CreditScoreTriggerHandler.beforeInsert(+)');
        validateCSPeriod();
    }
    
    public void afterDelete(){
        list<Credit_Score__c> csListtoDelete = (list<Credit_Score__c>)trigger.old;
        setLatestCSDetOnOpr(csListtoDelete);
    }
    public void afterUnDelete(){}
    public void andFinally(){}
    public void beforeUpdate(){
        list<Credit_Score__c> newScoreList = (list<Credit_Score__c>)trigger.new;
        for(Credit_Score__c cs: newScoreList){
            Credit_Score__c oldCS = (Credit_Score__c)trigger.oldMap.get(cs.id);
            if(cs.Period_New__c != oldCS.Period_New__c) cs.Period_New__c.addError('You cannot modify the period');
        }
    }
    public void beforeDelete(){}
    public void afterUpdate(){
        list<Credit_Score__c> newScoreList = (list<Credit_Score__c>)trigger.new;
        list<Credit_Score__c> updateLatestScoreList = new list<Credit_Score__c>();
        fetchSOQLMap();
        system.debug('newScoreList-------'+newScoreList);
        Set<Id> operatorId = new Set<id>();
        for(Credit_Score__c cs : newScoreList){
            list<Credit_Score__c> csList = mapOperator.get(cs.Airline__c).Credit_Score__r;
           
            if(cs.Id == csList[0].id){ // If the latest CS Data is modified then update on Operator
                system.debug('Latest Score so update on Operator');
                updateLatestScoreList.add(cs);//latest score
            } 
        }
        system.debug('updateLatestScoreList----'+ updateLatestScoreList);
        if(updateLatestScoreList==null || updateLatestScoreList.size()==0)return;
        setLatestCSDetOnOpr(updateLatestScoreList);
        
    }
    public void afterInsert(){
        // The latest Credit Score has to be updated on operator page
        list<Credit_Score__c> newScoreList = (list<Credit_Score__c>)trigger.new;
        setLatestCSDetOnOpr(newScoreList);
    }
    

    map<id,Operator__c> mapOperator =new map <id,Operator__c>();
    private void fetchSOQLMap(){
        list<Credit_Score__c> newScoreList = (list<Credit_Score__c>)trigger.new;
        Set<ID> operatorId =new Set<ID>();
        for(Credit_Score__c cs : newScoreList){
            operatorId.add(cs.airline__c);
        }
        mapOperator =new map <id,Operator__c>( [select  Id,Name,(select id, Name,Period_New__c,Internal_Credit_Score_Current_Year_New__c, airline__c
        from Credit_Score__r order by Y_Hidden_Period_Year__c desc) from Operator__c where ID IN :operatorId]);

    }
     
     
    /******************************************************************************************
        Description: Method that updates the latest CreditScore Details On Operator
        Called From: After Insert, After Update, After Delete
        IN: Credit Score List 
    ********************************************************************************************/    
   
    private void setLatestCSDetOnOpr( list<Credit_Score__c> newScoreList){
       
        system.debug('newScoreList-------'+newScoreList);
        Set<Id> operatorId = new Set<id>();
        for(Credit_Score__c cs : newScoreList){
            operatorId.add(cs.airline__c);
        }
        
        List<Operator__c> oprListToUpdate = new List<Operator__c>();
        List<Operator__c> oprList = [select Id,Name, (select Id,Name,Financial_Risk_Profile__c,Bloomberg_Rating__c,
                                    S_P_Rating__c,Moody_s_Rating__c,Fitch_Rating__c,LTM_2_Rating__c,LTM_Rating__c,
                                    LTM_1_Rating__c,Internal_Credit_Score_Current_Year_New__c,COVID_19_Government_Support__c,
                                    Internal_Credit_Score_Last_Year_New__c,
                                    Rating_Agency_Equivalent_Score__c,Rating_Status__c,Revenues__c,TAA_Equivalent_Score__c from Credit_Score__r order by Y_Hidden_Period_Year__c desc limit 1)
                                     from Operator__c where ID IN : operatorId];
        
       
        for(Operator__c opr : oprList){
            if(opr.Credit_Score__r==null || opr.Credit_Score__r.size()==0){// In case of delete of all CreditScore,operator fields should be set to blank
                opr.internal_CS_Current_Year__c ='';
                opr.internal_CS_Last_Year__c = '';
                opr.Bloomberg_Rating__c = '';
                opr.Fitch_Rating__c = '' ;
                opr.LTM_1_Rating__c = '';
                opr.LTM_Rating__c = '';
                opr.LTM_2_Rating__c ='';
                opr.Moody_s_Rating__c = '';
                opr.S_P_Rating__c = '' ;
                opr.Financial_Risk_Profile__c ='';
                opr.Rating_Agency_Equivalent_Score_new__c = null;
                opr.Rating_Status_new__c  = '';
                opr.Revenues__c = null;
                opr.TAA_Equivalent_Score_new__c = null ;
                opr.Government_Support1__c = null;
                oprListToUpdate.add(opr);
            }else{
                
            Credit_Score__c cs = opr.Credit_Score__r;
            system.debug('CS----'+cs);
            opr.internal_CS_Current_Year__c = cs.Internal_Credit_Score_Current_Year_New__c;
            opr.internal_CS_Last_Year__c = cs.Internal_Credit_Score_Last_Year_New__c;
            opr.Bloomberg_Rating__c = cs.Bloomberg_Rating__c;
            opr.Fitch_Rating__c = cs.Fitch_Rating__c ;
            opr.LTM_1_Rating__c = cs.LTM_1_Rating__c;
            opr.LTM_Rating__c = cs.LTM_Rating__c;
            opr.LTM_2_Rating__c = cs.LTM_2_Rating__c;
            opr.Moody_s_Rating__c = cs.Moody_s_Rating__c;
            opr.S_P_Rating__c = cs.S_P_Rating__c ;
            opr.Financial_Risk_Profile__c = cs.Financial_Risk_Profile__c;
            opr.Rating_Agency_Equivalent_Score_new__c = cs.Rating_Agency_Equivalent_Score__c==null?null:Integer.valueOf(cs.Rating_Agency_Equivalent_Score__c);
            opr.Rating_Status_new__c  = cs.Rating_Status__c ;
            opr.Revenues__c = cs.Revenues__c;
            opr.Government_Support1__c = cs.COVID_19_Government_Support__c;
            opr.TAA_Equivalent_Score_new__c = cs.TAA_Equivalent_Score__c==null?null:Integer.valueOf(cs.TAA_Equivalent_Score__c);
            oprListToUpdate.add(opr);
            }
        }
        system.debug('oprListToUpdate-----'+ oprListToUpdate);
      
        Leasewareutils.TriggerDisabledFlag = true;
        update oprListToUpdate;
        Leasewareutils.TriggerDisabledFlag = false;
    }
   
     /******************************************************************************************
        Description: Method that Validates the Period of Credit Score.
                    - Credit Score for the same period and for future period is not allowed
        Called From: Before Insert
    ********************************************************************************************/  
    private void validateCSPeriod(){
        list<Credit_Score__c> newScoreList = (list<Credit_Score__c>)trigger.new;
           /* ACG uses Period and local field Version so as to allow creating of Credit Score for the same period with different versions
        but managed package code allows only one Credit Score to be created for a given period 
        So checking if the Version field exists for Credit Score, if exists skiping validation here as Local Trigger Handler
        handle the validation

        https://app.asana.com/0/1134042593512586/1199160924558758
      */

      Map<String, Schema.SObjectField> mapCSField = Schema.SObjectType.Credit_Score__c.fields.getMap();
      Set<String> csFieldsSet = new  Set<String>();
      for(String Field:mapCSField.keyset()){
          Schema.DescribeFieldResult thisFieldDesc = mapCSField.get(Field).getDescribe();
          csFieldsSet.add(thisFieldDesc.getLocalName());
      }
      if(csFieldsSet.contains('Version__c')){
          //Skip validation here as the validation for this is done in Local Trigger on ACG
          return ;
      }else{
        fetchSOQLMap();
        Integer currentYear = System.today().year();
        for(Credit_Score__c newScore : newScoreList){
            //Credit Score for future dates cannot be loaded
            String newPeriodStr = newScore.Period_New__c; 
            if(Integer.valueOf(newScore.Period_New__c) > currentYear){
                newScore.Period_New__c.addError('Credit Score Data cannot be loaded for the future dates');
            }else{
                list<Credit_Score__c> periodList = mapOperator.get(newScore.Airline__c).Credit_Score__r;
                if(periodList!=null){
                    for(Credit_Score__c stored : periodList){
                        String storedPeriodStr = stored.period_New__c==null?'':stored.period_New__c;
                        if(storedPeriodStr.equals(newPeriodStr)){
                            //add Error
                            newScore.Period_New__c.addError('Credit Score Data is already loaded for the year.');
                            break;
                        }  
                    }
                    
                }
               
                newScore.Name = newScore.Name+'-'+newScore.Period_New__c;
                
            }
        }
        }
    }
 
}