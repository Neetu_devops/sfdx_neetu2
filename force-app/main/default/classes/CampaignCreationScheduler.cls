global with sharing class CampaignCreationScheduler implements Schedulable, Database.Batchable<SObject>  {

	public static String strLeases='';
	static String[] toAddresses = new String[]{};
	integer nDays=10;
	
    global void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new CampaignCreationScheduler());
    }

	global Database.QueryLocator start(Database.BatchableContext BC){
		String strQuery='select id, Auto_Create_Campaigns__c, Number_Of_Months_For_Wide_Body__c, Number_Of_Months_For_Narrow_Body__c, ' +
					' Narrowbody_Checked_Until__c, Widebody_Checked_Until__c ' +
					' from lessor__c ' +
					' where Auto_Create_Campaigns__c = true limit 1';
		return Database.getQueryLocator(strQuery); 
	}
	
	global void execute(Database.BatchableContext BC, List<lessor__c> theSetup){ 

		if(theSetup != null && theSetup.size()==1){
			String strCampaignNames = LeaseWareUtils.CreateCampaigns(theSetup[0]);
//			if(!''.equals(strCampaignNames))LeaseWareUtils.sendEmail('Campaigns Created.', strCampaignNames, 'LW_Deal_Proposal_Approval_Queue', 'Queue');
		}
	
	}
	
	global void finish(Database.BatchableContext BC){
		
//20150719 - Send the report every day. Asana - Switch email of Campaign to daily.
//			if(!test.isRunningTest())if(!'Fri'.equals(DateTime.now().format('E')))return;
//			 from Deal__c where CreatedDate = LAST_N_DAYS:7];
		
			boolean bFound=false;
			list<Deal__c> allFreshCampaigns = [select id, Name, (select id, Aircraft__c, Aircraft_Type__c, Engine_Type__c, MSN__c, YOM__c, Lease_End__c, 
									Current_Operator__c, Anticipated_Remarketing_Status__c from Marketed_Aircraft_del__r)
			 from Deal__c where CreatedDate = TODAY];
 
			String strCampaignNames= '<style>'+
				' table, th, td {' +
				       'border: 1px solid #1BA1E2;'+
				       'border-collapse: collapse;' +
				'}' +
				'th {' +
				    'background-color: white;'+ 
				    'color: black;'+ 
				'}'+
				'tr.alt td'+
				'{' +
				'color:#000;'+
				'background-color:lightgrey;'+
				'}'+
				'</style>'+
				'<Table border="1"><TH>Campaign</TH>' +
				'<TH>MSN</TH>' +
                '<TH>Vintage</TH>' +
				'<TH>Type/Engine</TH>' +
				'<TH>Lease Expiry</TH>' +
				'<TH>Current Operator</TH>' +
				'<TH>Anticipated Remarketing</TH>';
			
			integer i=0;	
			for(Deal__c curCpgn : allFreshCampaigns){
				for(Aircraft_In_Deal__c curAiC : curCpgn.Marketed_Aircraft_del__r){
					bFound=true;
					strCampaignNames+=('<TR' + (math.mod(i++,2)==0?'':' class="alt"') + '><TD><a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + curCpgn.id +'">' + curCpgn.Name + '</a></TD>');
					strCampaignNames+=('<TD><a href="'  + URL.getSalesforceBaseUrl().toExternalForm() + '/' + curAiC.id + '">' + curAiC.MSN__c + '</a></TD>');
					string strDate = (curAiC.YOM__c==null?'':curAiC.YOM__c.format()); 
					strCampaignNames+=('<TD>' + strDate + '</TD>');
					strCampaignNames+=('<TD>' + (curAiC.Aircraft_Type__c==null?'':curAiC.Aircraft_Type__c) + '/' + (curAiC.Engine_Type__c==null?'':curAiC.Engine_Type__c) + '</TD>');
					strDate = (curAiC.Lease_End__c==null?'':curAiC.Lease_End__c.format()); 
					strCampaignNames+=('<TD>' + strDate + '</TD>');
					strCampaignNames+=('<TD>' + (curAiC.Current_Operator__c==null?'':curAiC.Current_Operator__c) + '</TD>');
					strCampaignNames+=('<TD>' + (curAiC.Anticipated_Remarketing_Status__c==null?'':curAiC.Anticipated_Remarketing_Status__c) + '</TD>');
					strCampaignNames+=('</TR>');
				}
			}
			system.debug(strCampaignNames);
			if(bFound)LeaseWareUtils.sendEmail('Campaigns created today ('+ Date.today().format() +')', strCampaignNames, 'LW_Deal_Proposal_Approval_Queue', 'Queue');
	}

}