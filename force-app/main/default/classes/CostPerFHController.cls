public class CostPerFHController {
    
    @AuraEnabled
    public static CostPerFHWrapper calculateCostPerFH(Id scenarioInp){
        try{
            CostPerFHWrapper costPerFH = new CostPerFHWrapper();
            List<Scenario_Component__c> lstSC = [SELECT Id, Fx_General_Input__r.Ext_Reserve_Type__c, Cost_Per_FH__c, Assembly_Display_Name__c, Fx_General_Input__r.EOLA__c, Type__c, Assembly_MR_Info__c, (SELECT Id, Historical_Event__c, Historical_Month__c, EOLA_FH_TSO__c, Event_Date__c, Net_MR__c, TSO__c, Return_Month__c, Event_Expense_Amount__c, Net_EOLA_Payment_Amount__c, FH_At_SV__c, Cost_Per_FH__c FROM Monthly_Scenario__r WHERE Event_Date__c != null ORDER BY Event_Date__c) FROM Scenario_Component__c Where Fx_General_Input__c =: scenarioInp AND Life_Limited_part_LLP__c = NULL AND Type__c != 'General' ORDER BY Type__c];

            List<Scenario_Component__c> lstSC1 = [SELECT Id, Fx_General_Input__r.Ext_Reserve_Type__c, Cost_Per_FH__c, Assembly_Display_Name__c, Fx_General_Input__r.EOLA__c, Type__c, Assembly_MR_Info__c, (SELECT Id, Historical_Event__c, Historical_Month__c, EOLA_FH_TSO__c, Event_Date__c, Net_MR__c, TSO__c, Return_Month__c, Event_Expense_Amount__c, Net_EOLA_Payment_Amount__c, FH_At_SV__c, Cost_Per_FH__c FROM Monthly_Scenario__r WHERE Event_Date__c = null AND Return_Month__c = true) FROM Scenario_Component__c Where Fx_General_Input__c =: scenarioInp AND Life_Limited_part_LLP__c = NULL AND Type__c != 'General' ORDER BY Type__c];

            Decimal sumCostPerFH =0.0;
            Integer svCount = 0;

            for(Scenario_Component__c sc : lstSC) {
                if(sc.Monthly_Scenario__r.size() > svCount){
                    svCount = sc.Monthly_Scenario__r.size();
                }
            }
            costPerFH.noOfSV = svCount;

            for(Scenario_Component__c sc : lstSC) {
                Boolean flag = false;
                Integer count = 0;
                TableDataWrapper tdw = new TableDataWrapper();
                tdw.name = sc.Assembly_Display_Name__c;
                tdw.assemblyid = sc.Assembly_MR_Info__c;
                for(Monthly_Scenario__c ms : sc.Monthly_Scenario__r){
                    count++;
                    if(flag == false){
                        String extReserveType = sc.Fx_General_Input__r.Ext_Reserve_Type__c;
                        Boolean isEOLA = sc.Fx_General_Input__r.EOLA__c;
                        if(isEOLA && extReserveType == 'Monthly MR'){
                            costPerFH.reserveType = 'MR';
                        }else if(isEOLA && extReserveType != 'Monthly MR'){
                            costPerFH.reserveType = 'EOLA';
                        }else if(isEOLA == false && extReserveType == 'EOLA'){
                            costPerFH.reserveType = 'EOLA';
                        }else if(isEOLA == false && extReserveType != 'EOLA'){
                            costPerFH.reserveType = 'MR';
                        }
                        flag=true;
                    }
                    tdw.totalCost += (ms.Event_Expense_Amount__c != null ? ms.Event_Expense_Amount__c : 0.0);
                    tdw.totalFH += (ms.FH_At_SV__c != null ? ms.FH_At_SV__c : 0.0);
                    System.debug('Monthly_Scenario: '+ms);
                    tdw.rowVal.add(new svRowDataWrapper(count, ms));
                }

                for(Integer i = sc.Monthly_Scenario__r.size(); i < svCount; i++){
                    tdw.rowVal.add(new svRowDataWrapper(i+1, new Monthly_Scenario__c()));
                }
                
                for(Scenario_Component__c sc1 : lstSC1) {
                    if(sc.Assembly_Display_Name__c == sc1.Assembly_Display_Name__c){
                        tdw.totalCostPerFH = sc1.Cost_Per_FH__c;
                        for(Monthly_Scenario__c ms : sc1.Monthly_Scenario__r){
                            //MR AT Return
                            tdw.endMR = ms.Net_MR__c;
                            if(ms.TSO__c != null && ms.TSO__c != 0.0){
                                tdw.mrRate = ms.Net_MR__c == null ? 0.0 : ms.Net_MR__c / ms.TSO__c;
                            }
                            tdw.tso = ms.TSO__c;
                            
                            //EOLA Values
                            tdw.EOLACost = (ms.Net_EOLA_Payment_Amount__c == null ? 0.0 : ms.Net_EOLA_Payment_Amount__c) * -1;
                            tdw.EOLAFH = ms.EOLA_FH_TSO__c ;
                            if(tdw.EOLACostPerFH != null && ms.EOLA_FH_TSO__c !=null && ms.EOLA_FH_TSO__c != 0.0){
                                tdw.EOLACostPerFH = tdw.EOLACost / ms.EOLA_FH_TSO__c ;
                            }
                        }
                    }
                }
                if(costPerFH.reserveType == 'MR'){
                    tdw.totalCost += (tdw.endMR == null ? 0 : tdw.endMR);
                    tdw.totalFH += (tdw.tso == null ? 0 : tdw.tso);
                }else{
                    tdw.totalCost += (tdw.EOLACost == null ? 0 : tdw.EOLACost);
                    tdw.totalFH += (tdw.EOLAFH  == null ? 0 : tdw.EOLAFH);
                }
                //This is being used in DecisionMakingController.cls and Utility.Cls
                sumCostPerFH += (tdw.totalCostPerFH != null ? tdw.totalCostPerFH : 0);
                costPerFH.sumCostPerFH = sumCostPerFH;
                
                costPerFH.lstTableData.add(tdw);
            }
            return costPerFH;
        }catch(Exception e){
            throw new AuraHandledException('An internal Server Error has Occured: '+e.getMessage()+' Line Number: '+e.getLineNumber());
        }
    }
    
    public class CostPerFHWrapper{
        @auraEnabled public Integer noOfSV { get; set; }
        @auraEnabled public String reserveType { get; set; }
        @auraEnabled public Decimal sumCostPerFH { get; set; }
        @auraEnabled public List<TableDataWrapper> lstTableData { get; set; }
        
        public CostPerFHWrapper(){
            this.lstTableData = new List<TableDataWrapper>();
            this.sumCostPerFH =0.0;
        }
    }
    
    public class TableDataWrapper{
        @auraEnabled public String name { get; set; }
        @auraEnabled public List<svRowDataWrapper> rowVal { get; set; }
        @auraEnabled public Decimal EOLACost { get; set; }
        @auraEnabled public Decimal EOLAFH { get; set; }
        @auraEnabled public Decimal EOLACostPerFH { get; set; }
        @auraEnabled public Decimal totalCost { get; set; }
        @auraEnabled public Decimal totalFH { get; set; }
        @auraEnabled public Decimal totalCostPerFH { get; set; }
        @auraEnabled public Decimal endMR { get; set; }
        @auraEnabled public Decimal tso { get; set; }
        @auraEnabled public Decimal mrRate { get; set; }
        @auraEnabled public String assemblyid { get; set; }
        
        public TableDataWrapper(){
            this.name = name;
            this.rowVal = new List<svRowDataWrapper>();
            this.totalCost = 0.0;
            this.totalFH = 0.0;
            this.totalCostPerFH = 0.0;
            this.endMR =0.0;
            this.mrRate= 0.0;
            this.EOLACostPerFH =0.0;
            this.EOLACost =0.0;
        }
    }
    
    public class svRowDataWrapper{
        @AuraEnabled public String index {get;set;}
        @AuraEnabled public Monthly_Scenario__c msRecord {get;set;}
        
        public svRowDataWrapper(Integer index, Monthly_Scenario__c msRecord){
            this.index = String.valueOf(index);
            this.msRecord = msRecord;
        }
    }
}