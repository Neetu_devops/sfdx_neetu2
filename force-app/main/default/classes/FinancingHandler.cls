public class FinancingHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'FinancingHandlerBefore';
    private final string triggerAfter = 'FinancingHandlerAfter';
    public FinancingHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('FinancingHandler.beforeInsert(+)');
    	Financing__c[] listNew = (list<Financing__c>)trigger.new ;
    	for(Financing__c curRec:listNew){
			curRec.PMT__c = LeaseWareUtils.calculatePMT((curRec.Interest_Rate__c==null?0:curRec.Interest_Rate__c)/(12*100),(curRec.Term_of_the_Loan__c==null?0:curRec.Term_of_the_Loan__c.intValue()),curRec.Initial_Loan_Amount__c,curRec.Ballon_Payment__c);
    	}

	    system.debug('FinancingHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('FinancingHandler.beforeUpdate(+)');

    	Financing__c[] listNew = (list<Financing__c>)trigger.new ;
    	Financing__c oldObj;
    	for(Financing__c curRec:listNew){
    		oldObj = (Financing__c)trigger.oldMap.get(curRec.Id);
    		if(oldObj.Interest_Rate__c != curRec.Interest_Rate__c || oldObj.Term_of_the_Loan__c != curRec.Term_of_the_Loan__c ||
    				oldObj.Initial_Loan_Amount__c != curRec.Initial_Loan_Amount__c || oldObj.Ballon_Payment__c != curRec.Ballon_Payment__c){
    					
				curRec.PMT__c = LeaseWareUtils.calculatePMT((curRec.Interest_Rate__c==null?0:curRec.Interest_Rate__c)/(12*100),(curRec.Term_of_the_Loan__c==null?0:curRec.Term_of_the_Loan__c.intValue()),curRec.Initial_Loan_Amount__c,curRec.Ballon_Payment__c);
    		}
    	}    	
    	
    	system.debug('FinancingHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('FinancingHandler.beforeDelete(+)');
    	Financing__c[] listNew = (list<Financing__c>)trigger.old ;
    	for(Financing__c curRec:listNew){    	
    		if(curRec.Generate_Amortization__c) deleteAmortizationTable(curRec.Id);
    	}
    	system.debug('FinancingHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FinancingHandler.afterInsert(+)');
    	Financing__c[] listNew = (list<Financing__c>)trigger.new ;
    	for(Financing__c curRec:listNew){
    		if(curRec.Generate_Amortization__c){
    			calculateAmortizationTable(curRec.Start_Date__c,curRec.Id,(curRec.Term_of_the_Loan__c==null?0:curRec.Term_of_the_Loan__c.intValue()),curRec.Initial_Loan_Amount__c
    				,(curRec.Interest_Rate__c==null?0:curRec.Interest_Rate__c)/(12*100)
    				,null,curRec.PMT__c,curRec.Ballon_Payment__c);
    		}
    	}    	
    	system.debug('FinancingHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FinancingHandler.afterUpdate(+)');

    	Financing__c[] listNew = (list<Financing__c>)trigger.new ;
    	Financing__c oldObj;
    	Boolean checkFlag = false;
    	for(Financing__c curRec:listNew){
    		oldObj = (Financing__c)trigger.oldMap.get(curRec.Id);
    		if(oldObj.Interest_Rate__c != curRec.Interest_Rate__c || oldObj.Term_of_the_Loan__c != curRec.Term_of_the_Loan__c ||
    				oldObj.Initial_Loan_Amount__c != curRec.Initial_Loan_Amount__c || oldObj.Ballon_Payment__c != curRec.Ballon_Payment__c){
    					
				if(curRec.Generate_Amortization__c ) checkFlag= true;
    		}
    		
    		if(checkFlag ||(curRec.Generate_Amortization__c && (oldObj.Generate_Amortization__c!= curRec.Generate_Amortization__c || curRec.Start_Date__c!= oldObj.Start_Date__c))){
    			calculateAmortizationTable(curRec.Start_Date__c,curRec.Id,(curRec.Term_of_the_Loan__c==null?0:curRec.Term_of_the_Loan__c.intValue()),curRec.Initial_Loan_Amount__c
    				,(curRec.Interest_Rate__c==null?0:curRec.Interest_Rate__c)/(12*100)
    				,null,curRec.PMT__c,curRec.Ballon_Payment__c);
    		}
    		if(oldObj.Generate_Amortization__c!= curRec.Generate_Amortization__c && !curRec.Generate_Amortization__c ){
    			deleteAmortizationTable(curRec.Id);
    		}
    	}        	
    	
    	system.debug('FinancingHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FinancingHandler.afterDelete(+)');
    	
    	
    	system.debug('FinancingHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('FinancingHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('FinancingHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	private void deleteAmortizationTable(Id parentId){
		LeaseWareUtils.setFromTrigger('DeletedFromFinancingHandler');
		delete [select Id from Amortization_Schedule__c where Financing__c = :parentId] ;
		LeaseWareUtils.unsetTriggers('DeletedFromFinancingHandler');		
	}
	private void calculateAmortizationTable(Date startDate,Id parentId,Integer totalTerm,Decimal Loan_Principle,Decimal anuualInterestRateByFrequency,Decimal taxRate,Decimal PMT,Decimal BaloonPMT){
		deleteAmortizationTable(parentId);
		list<Amortization_Schedule__c> listAmortizationTable = new list<Amortization_Schedule__c>();
		//Integer LoanTermPeriod = totalTerm;
		
		Integer tempPeriodSequence;
		Date tempDate;
		String tempName;
		Decimal  tempOutstandingBalance = 0, tempInterestExpense = null, tempPayment = null, tempPrincipal = 0,tempBaloonPayment=null;
		if (totalTerm <1 && PMT== null && PMT<=0 ) return ;
		tempOutstandingBalance = Loan_Principle;
		for(Integer i=0; i<totalTerm; i++ ){
			
			tempPeriodSequence = i+1;
			tempDate = startDate.addMonths(tempPeriodSequence);
			tempOutstandingBalance = tempOutstandingBalance - tempPrincipal;
			tempPayment = PMT;
			tempInterestExpense =tempOutstandingBalance * anuualInterestRateByFrequency;
			tempPrincipal = tempPayment - tempInterestExpense;
			//tempTaxSheild = tempInterestExpense * taxRate/100;
			if(i==(totalTerm -1) && BaloonPMT!=null && BaloonPMT!=0){
				tempBaloonPayment = BaloonPMT * -1; 
			}
			tempName = ''+tempDate.year();
			//tempName = tempName.subString(2);
			tempName = 'Payment ' + tempPeriodSequence + '-' + LeaseWareUtils.mapMonthsName.get(tempDate.month())+'/' +tempName.subString(2);
			
			listAmortizationTable.add(new Amortization_Schedule__c(name = tempName
					, Period_Sequence__c = tempPeriodSequence 
					, Outstanding_Balance__c = tempOutstandingBalance 
					, Interest_Expense__c = tempInterestExpense
					, Payment__c = tempPayment
					, Principal_Repayment__c = tempPrincipal
					, Generate__c = true
					, Financing__c = parentId
					, Baloon_Payment__c  = tempBaloonPayment
					, Due_Date__c = tempDate ));	
			
		}
		LeaseWareUtils.setFromTrigger('InsertedFromFinancingHandler');
		insert listAmortizationTable;
		LeaseWareUtils.unsetTriggers('InsertedFromFinancingHandler');
	}


}