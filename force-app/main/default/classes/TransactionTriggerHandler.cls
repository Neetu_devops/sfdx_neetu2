public class TransactionTriggerHandler implements ITrigger {


    // Constructor
    private final string triggerBefore = 'TransactionTriggerHandlerBefore';
    private final string triggerAfter = 'TransactionTriggerHandlerAfter';



    public TransactionTriggerHandler() {}

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {


    }

    public void bulkAfter() {

    }

    public void beforeInsert() {
        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);

        system.debug('TransactionTriggerHandler.beforeInsert(+)');

		TxnBefInsUpdSetMarkRep();
        system.debug('TransactionTriggerHandler.beforeInsert(+)');
    }

    public void beforeUpdate() {
        //Before check  : keep code here which does not have issue with multiple call .
        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('TransactionTriggerHandler.beforeUpdate(+)');

		TxnBefInsUpdSetMarkRep();

        system.debug('TransactionTriggerHandler.beforeUpdate(-)');
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete() {

        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('TransactionTriggerHandler.beforeDelete(+)');



        system.debug('TransactionTriggerHandler.beforeDelete(-)');

    }

    public void afterInsert() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TransactionTriggerHandler.afterInsert(+)');

        system.debug('TransactionTriggerHandler.afterInsert(-)');
    }

    public void afterUpdate() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TransactionTriggerHandler.afterUpdate(+)');


        system.debug('TransactionTriggerHandler.afterUpdate(-)');
    }

    public void afterDelete() {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TransactionTriggerHandler.afterDelete(+)');

        system.debug('TransactionTriggerHandler.afterDelete(-)');
    }

    public void afterUnDelete() {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TransactionTriggerHandler.afterUnDelete(+)');

        system.assert(false);

        system.debug('TransactionTriggerHandler.afterUnDelete(-)');
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        // insert any audit records

    }

	//before insert, before update
	private void TxnBefInsUpdSetMarkRep(){
		system.debug('TxnBefInsUpdSetMarkRep()+');
		Transaction__c[] listNew = (list < Transaction__c > ) trigger.new;
		set<Id> setOps = new set<Id>();
		set<Id> setLessors = new set<Id>();
		map<Id, Id> mapAddresses = new map<Id, Id>();
	
	
		for(Transaction__c curTxn : listNew){
			if('Airline'.equals(curTxn.Seller_Type__c)){
				if(curTxn.Seller_Airline__c!=null)setOps.add(curTxn.Seller_Airline__c);
			}else if('Lessor'.equals(curTxn.Seller_Type__c)){
				if(curTxn.Seller_Lessor__c!=null)setLessors.add(curTxn.Seller_Lessor__c);
			}
		}
		if(setOps.size()>0){
			for(Operator__c curOp: [select id, Marketing_Representative_Contact__c from Operator__c where id in :setOps]){
				if(curOp.Marketing_Representative_Contact__c!=null)mapAddresses.put(curOp.Id, curOp.Marketing_Representative_Contact__c);
			}
		}
		
		
		
		for(Transaction__c curTxn : listNew){
			if(trigger.isUpdate && ((Transaction__c)trigger.oldMap.get(curTxn.Id)).Seller_Type__c != curTxn.Seller_Type__c)curTxn.Marketing_Representative__c=null;
			if('Airline'.equals(curTxn.Seller_Type__c)){
				if(curTxn.Seller_Airline__c!=null)curTxn.Marketing_Representative__c=mapAddresses.get(curTxn.Seller_Airline__c);
			}
		}

		//set MSN List
		if(trigger.Isupdate){
			system.debug('Inside set MSN List');
			map < id, Transaction__c > mapTxns = new map < id, Transaction__c > ([select Id, MSNs_Marketed_In_This_Transaction__c 
											,(select name from Aircraft_In_Transaction__r)
											from Transaction__c where Id in : listNew]);
			system.debug('mapTxns'+mapTxns.size());	
			system.debug('mapTxns=='+mapTxns);	
			
			string nameStr;		
			for(Transaction__c curTrx: listNew){
				nameStr = null;
				for(Aircraft_In_Transaction__c curAiT: mapTxns.get(curTrx.Id).Aircraft_In_Transaction__r){
					if(nameStr==null)  nameStr = curAiT.Name;
					else nameStr += ', ' + curAiT.Name;
				}
				if(nameStr!=null && nameStr.length()>254) nameStr = nameStr.left(251) + '...';
				curTrx.MSNs_Marketed_In_This_Transaction__c=nameStr;
				system.debug('curTrx.MSNs_Marketed_In_This_Transaction__c=='+curTrx.MSNs_Marketed_In_This_Transaction__c);	
			}
				
		}
		
		system.debug('TxnBefInsUpdSetMarkRep()-');
	}

}