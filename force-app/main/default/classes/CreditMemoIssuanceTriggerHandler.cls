public class CreditMemoIssuanceTriggerHandler implements ITrigger {

 

    private final string triggerBefore ='CreditMemoIssuanceTriggerHandlerBefore';
    private final string triggerAfter ='CreditMemoIssuanceTriggerHandlerAfter';

    public void bulkBefore(){}
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){
              
        system.debug('CreditIssuanceTriggerHandler.beforeInsert(+)');
		 //Set to hold parent Credit Memo with list of Issuance records having Lessee__c as null
		 Set<Id> parentIds=new Set<Id>();
         List<Credit_Memo_Issuance__c> issuanceList=new List<Credit_Memo_Issuance__c>();
         for(Credit_Memo_Issuance__c currec :(list<Credit_Memo_Issuance__c>)trigger.new){//rounding the amount to 2 decimal places
            currec.Amount__c = leaseWareUtils.zeroIfNull(currec.Amount__c).setscale(2,System.RoundingMode.HALF_UP);
          if(currec.Lessee__c==null)
              { 
              issuanceList.add(currec);  
              parentIds.add(currec.Credit_Memo__c);
              }     
        }
       //Retriving the parent Credit Memo
         map<id, Credit_Memo__c> mapParentLessee = new map<id, Credit_Memo__c>([Select id, Lessee__c from credit_memo__c where id =:parentIds]);
		 
       // Updating Issuance records where Lessee field is blank
       for (Credit_Memo_Issuance__c currec : issuanceList){
           currec.Lessee__c = mapParentLessee.get(currec.Credit_Memo__c).Lessee__c;
			}  
        ValidateCummlativeAmount(trigger.new);
        system.debug('CreditIssuanceTriggerHandler.beforeInsert(-)'); 
    }
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){
       Credit_Memo_Issuance__c[] cmList = (list<Credit_Memo_Issuance__c>)trigger.new ; 
         List<Credit_Memo_Issuance__c> cmsList =new List<Credit_Memo_Issuance__c>();
         Credit_Memo_Issuance__c oldRec=null;
        for (Credit_Memo_Issuance__c currec : cmList){
           oldRec= (Credit_Memo_Issuance__c)trigger.oldMap.get(currec.Id);            
			if(currec.Amount__c!= oldRec.Amount__c){
           currec.Amount__c = leaseWareUtils.zeroIfNull(currec.Amount__c).setscale(2,System.RoundingMode.HALF_UP);
              cmsList.add(currec);
			}  
        }  
       ValidateCummlativeAmount(cmsList);  
    }
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){}
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        system.debug('CreditIssuanceTriggerHandler.afterInsert(+)');        
        createSupplementalRentTransaction();
        system.debug('CreditIssuanceTriggerHandler.afterInsert(-)'); 
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        system.debug('CreditIssuanceTriggerHandler.afterUpdate(+)');        
        createSupplementalRentTransaction();
        system.debug('CreditIssuanceTriggerHandler.afterUpdate(-)'); 
    }
    
    public void afterDelete(){
      system.debug('CreditMemoIssuanceTriggerHandler.afterDelete(+)');
      createSupplementalRentTransaction();
      system.debug('CreditMemoIssuanceTriggerHandler.afterDelete(-)');
    }
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
   public  void afterUnDelete(){}   
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
  
    public void andFinally(){}
    
    /* Function to validate the cummulative amount on Credit Memo Issuance records is not greater than parent Credit Memo
      Called in Before Insert/Update */
     private void ValidateCummlativeAmount(list<Credit_Memo_Issuance__c> cmIssuanceList ){
       
       set<id>cmsID=new set<Id>();
       map<id,double> mapCMtotalAmount=new map<id,double>();
       set<id>creditMemoIds = new set<Id>();
       
       //Get  all Credit Memo related Credit Memo Issuance records in trigger.new 
       for(Credit_Memo_Issuance__c record: cmIssuanceList) {           
         creditMemoIds.add(record.Credit_Memo__c);
         cmsID.add(record.id);
       }
        // Map to hold Credit Memo record along with remaining Amount        
      for(Credit_Memo__c record: [select id, Amount__c ,(select id,Amount__c from Credit_Memo_Issuances__r where id not in:cmsID) from Credit_Memo__c where id = :creditMemoIds ]){           
          mapCMtotalAmount.put(record.id,record.Amount__c);  
           for (Credit_Memo_Issuance__c rec : record.Credit_Memo_Issuances__r)
            {
               mapCMtotalAmount.put(record.id,mapCMtotalAmount.get(record.id)-rec.Amount__c);
            }  
         } 
       //Iterate through each Credit MemoIssuance record 
     for(Credit_Memo_Issuance__c record: cmIssuanceList){
        
        if(( mapCMtotalAmount.get(record.Credit_Memo__c)-record.Amount__c)<0) {
          record.addError('Cannot save Credit Memo Issuance with this Amount, sum of all Credit Memo Issuance Amounts cannot be higher that Credit Memo Amount');  
               }
        else {
            mapCMtotalAmount.put(record.Credit_Memo__c,mapCMtotalAmount.get(record.Credit_Memo__c)-record.Amount__c);
        }
       }    
      }                              
       
 

  // aft insert, aft update , // after delete
	private void createSupplementalRentTransaction(){
    system.debug('CreditMemoIssuanceTriggerHandler.createSupplementalRentTransaction(+)');
    List<Credit_Memo_Issuance__c> listCreditMemoIssuance = (trigger.isDelete ? trigger.old : trigger.new);
    Map<Id,Assembly_MR_Rate__c> mapSupplementalRents = new  Map<Id,Assembly_MR_Rate__c> ();
    List<Supplemental_Rent_Transactions__c> listSupplementalRentTransactions = new  List<Supplemental_Rent_Transactions__c> ();
    
    for(Credit_Memo_Issuance__c curRec: listCreditMemoIssuance) {
     // Assembly_MR_Rate__c curSR = mapSupplementalRents.get(curRec.Supplemental_Rent__c);
     Assembly_MR_Rate__c curSR = new Assembly_MR_Rate__c ();
     curSR.Id = curRec.Supplemental_Rent__c;
     curSR.Snapshot_Event_Record_ID__c = 'C'+CurRec.Id;
     mapSupplementalRents.put(curSR.Id,curSR);
  }
    if(mapSupplementalRents.size()>0) {
      //System.debug('listSupplementalRents:' + mapSupplementalRents.values());      
      update mapSupplementalRents.values();
    }
    system.debug('CreditMemoIssuanceTriggerHandler.createSupplementalRentTransaction(-)');
  }
}
