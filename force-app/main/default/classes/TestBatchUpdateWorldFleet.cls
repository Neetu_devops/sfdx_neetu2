/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBatchUpdateWorldFleet {

    static testMethod void Test1() {
        // TO DO: implement unit test
        Global_Fleet__c[] listWF = new list<Global_Fleet__c>();
        	// create record
        listWF.add( new Global_Fleet__c(name='abc' , Operator__c='Indigo' 
        						,Aircraft_Operator__c = null,AircraftType__c ='A320',AircraftSeries__c ='200',AircraftStatus__c='In Service'
                    ,Aircraft_External_ID__c = '77777', AircraftVariant__c = '700', EngineVariant__c ='7B26E', EngineType__c ='A380-Trent',
                                 Manager__c = 'Test' ,MTOW__c=80,SeatTotal__c=20, BuildYear__c='2000'
        						 ));
        insert  listWF;				 
  
  		insert new Operator__c(name='Indigo',Alias__c ='Indigo,Indigo Airways');

        Aircraft_Type__c newAT = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320',Aircraft_Group_GP__c ='A320', Aircraft_Variant__c='700');
        LeaseWareUtils.clearFromTrigger();insert newAT;

        Aircraft_Type__c newAT2 = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320', Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56',Aircraft_Group_GP__c ='A320');
        LeaseWareUtils.clearFromTrigger();insert newAT2;


		Aircraft_Type_Map__c newATM = new Aircraft_Type_Map__c(Name='A330', Aircraft_Type__c=newAT.id);
			newATM.Type__c = 'A330';
			newATM.Engine_Type__c='CF6';
			newATM.Variant__c = '200';
      LeaseWareUtils.clearFromTrigger();
          insert newATM;
        
        
    WF_Field_Summary__c worldfieldRecord = new WF_Field_Summary__c(Aircraft_Type__c ='testAir', Aircraft_Type_Variant__c='testAir Varient',
                                                                      Engine_Type__c='testEngine', Manager__c='TestManager');
        insert worldfieldRecord;
        
        ContentVersion contentversionVarient=new ContentVersion();
        contentversionVarient.Title='AircraftType_Variant';
        contentversionVarient.contentLocation='S';
        contentversionVarient.PathOnClient='AircraftType_Variant.json';
        contentversionVarient.FirstPublishLocationId=worldfieldRecord.Id;
        contentversionVarient.VersionData=Blob.valueOf('{"A300B4":["7B26E"],"A300":["8.00E+05","7B26E"],"739":["7270","970-84EP2"]');
        insert contentversionVarient;

        
        Database.executeBatch(new BatchUpdateWorldFleet());                       
    }

    
    static testMethod void Test2() {
        // TO DO: implement unit test
        Global_Fleet__c[] listWF = new list<Global_Fleet__c>();
        	// create record
        listWF.add( new Global_Fleet__c(name='abc' , Operator__c='Indigo Airlines' 
                    ,Aircraft_Operator__c = null, AircraftType__c ='A320',AircraftSeries__c ='200',AircraftStatus__c='In Service',
                    Aircraft_External_ID__c = '77777', AircraftVariant__c = '700',EngineVariant__c ='7B26E', EngineType__c ='A380-Trent',
                                 Manager__c = 'Test' 
        						 ));
        insert  listWF;				 
  
  		insert new Operator__c(name='Indigo',Alias__c ='Indigo,Indigo Airways',Lessee_Legal_Name__c = 'Indigo Airlines' , World_Fleet_Operator_Name__c = 'IA');

        Aircraft_Type__c newAT = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320',Aircraft_Group_GP__c ='A320');
        LeaseWareUtils.clearFromTrigger();insert newAT;

        Aircraft_Type__c newAT2 = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320', Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56',Aircraft_Group_GP__c ='A320');
        LeaseWareUtils.clearFromTrigger();insert newAT2;


		Aircraft_Type_Map__c newATM = new Aircraft_Type_Map__c(Name='A330', Aircraft_Type__c=newAT.id);
			newATM.Type__c = 'A330';
			newATM.Engine_Type__c='CF6';
			newATM.Variant__c = '200';
			LeaseWareUtils.clearFromTrigger();insert newATM;

        
        Database.executeBatch(new BatchUpdateWorldFleet());                       
    }
    static testMethod void TestCP() {
        // TO DO: implement unit test
        insert new Bank__c(name='Test CP',Alias__c ='Test Counter Party');
        insert new Counterparty__c(name ='Test Lessor');
        // TO DO: implement unit test
        Global_Fleet__c[] listWF = new list<Global_Fleet__c>();
        	// create record
        listWF.add( new Global_Fleet__c(name='abc' , Operator__c='Indigo Airlines' 
                    ,Aircraft_Operator__c = null, AircraftType__c ='A320',AircraftSeries__c ='200',AircraftStatus__c='In Service',
                    Aircraft_External_ID__c = '77777', AircraftVariant__c = '700',EngineVariant__c ='7B26E', EngineType__c ='A380-Trent',
                                 Owner__c = 'Test Counter Party' ,Manager__c = 'Test Lessor'
        						 ));
        insert  listWF;				 
  
  		insert new Operator__c(name='Indigo',Alias__c ='Indigo,Indigo Airways',Lessee_Legal_Name__c = 'Indigo Airlines' , World_Fleet_Operator_Name__c = 'IA');

        Aircraft_Type__c newAT = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320',Aircraft_Group_GP__c ='A320');
        LeaseWareUtils.clearFromTrigger();insert newAT;

        Aircraft_Type__c newAT2 = new Aircraft_Type__c(Name='A320', Aircraft_Type__c='A320', Fleet_Size_To_Consider__c=5, Engine_Type__c='CFM56',Aircraft_Group_GP__c ='A320');
        LeaseWareUtils.clearFromTrigger();insert newAT2;


		Aircraft_Type_Map__c newATM = new Aircraft_Type_Map__c(Name='A330', Aircraft_Type__c=newAT.id);
			newATM.Type__c = 'A330';
			newATM.Engine_Type__c='CF6';
			newATM.Variant__c = '200';
			LeaseWareUtils.clearFromTrigger();insert newATM;

       	Test.startTest(); 
        Database.executeBatch(new BatchUpdateWorldFleet());                       
        Test.stopTest();
        Bank__c bank = [select Id from Bank__c limit 1];
        Counterparty__c cp = [select Id from Counterparty__c limit 1];
        Global_Fleet__c gf= [select Id,Name,Owner__c,Manager__c,Related_To_Counterparty__c,Related_To_Counterparty_Owner__c,Related_Owner__c from Global_Fleet__c limit 1 ];
        system.debug('Fleet Record----'+ gf);
        system.assertEquals(bank.id, gf.Related_To_Counterparty_Owner__c);
        system.assertEquals(cp.id, gf.Related_Owner__c);
    }

    @IsTest
    private static void testUpdateAssetWFLookup(){
        Global_Fleet__c[] listWF = new list<Global_Fleet__c>();
        // create record
        listWF.add( new Global_Fleet__c(name='abc' , Operator__c='Indigo' ,SerialNumber__c='0J54-E190-80972',AircraftManufacturer__c='Embraer'
                    ,Aircraft_Operator__c = null,AircraftType__c ='175',AircraftSeries__c ='200',AircraftStatus__c='In Service'
                ,Aircraft_External_ID__c = '77777', AircraftVariant__c = '700', EngineVariant__c ='7B26E', EngineType__c ='A380-Trent',
                             Manager__c = 'Test' ,MTOW__c=80,SeatTotal__c=20, BuildYear__c='2000'
                             ));
        insert  listWF;	
        
        Global_Fleet__c gf = [select Id from Global_Fleet__c limit 1];
        String gfId = gf.Id;
        
       
        
        
        Test.startTest();
        Database.executeBatch(new BatchUpdateWorldFleet()); 
        Test.stopTest();
        Aircraft__c ac = new Aircraft__c(name='Test AC',MSN_Number__c='0J54-E190-80972', Manufacturer__c='Embraer',Aircraft_Type__c='175',
                                        TSN__c=100,
                                        CSN__c=100);
        insert ac;

      
       
        Aircraft__c fetcheAC = [select Id, World_Fleet_Record__c from Aircraft__c limit 1];
        
        system.assertEquals(fetcheAC.World_Fleet_Record__c,gfId);
                                    
    }

    @isTest
    private static void testUpdateWFRecordSNTYpe(){
          Global_Fleet__c[] listWF = new list<Global_Fleet__c>();
        // create record
        listWF.add( new Global_Fleet__c(name='abc' , Operator__c='Indigo' ,SerialNumber__c='0J54-E190-80972',AircraftManufacturer__c='Embraer'
                    ,Aircraft_Operator__c = null,AircraftType__c ='175',AircraftSeries__c ='200',AircraftStatus__c='In Service'
                ,Aircraft_External_ID__c = '77777', AircraftVariant__c = '700', EngineVariant__c ='7B26E', EngineType__c ='A380-Trent',
                             Manager__c = 'Test' ,MTOW__c=80,SeatTotal__c=20, BuildYear__c='2000'
                             ));
        insert listWF;
        List<String> gfList = new List<String>();
        Global_Fleet__c gf = [select Id,Y_Hidden_Serial_Number__c,AircraftType__c from Global_Fleet__c limit 1];
        String gfId = gf.Id;
        gfList.add(gfId);
        Aircraft__c aircraft = new Aircraft__c(
            Name = 'TestAircraft',
            MSN_Number__c = '0J54-E190-80972',
            Aircraft_Type__c = '175',
            TSN__c = 100,
             
            CSN__c = 100
        	);
        insert aircraft;
        Test.startTest();
        Database.executeBatch(new BatchUpdateWorldFleet()); 
        
        Test.stopTest();
        
        Aircraft__c fetchedAC = [select Id, World_Fleet_Record__c,MSN_Number__c,Aircraft_Type__c from Aircraft__c limit 1];
        system.debug('fetchedAC----'+fetchedAC);
        system.assertEquals(fetchedAC.World_Fleet_Record__c,gfId);

    }
    @isTest
    private static void testUpdateWFRecordSN(){
          Global_Fleet__c[] listWF = new list<Global_Fleet__c>();
        // create record
        listWF.add( new Global_Fleet__c(name='abc' , Operator__c='Indigo' ,SerialNumber__c='0J54-E190-80972',AircraftManufacturer__c='Embraer'
                    ,Aircraft_Operator__c = null,AircraftType__c ='737 (NG)',AircraftSeries__c ='200',AircraftStatus__c='In Service'
                ,Aircraft_External_ID__c = '77777', AircraftVariant__c = '700', EngineVariant__c ='7B26E', EngineType__c ='A380-Trent',
                             Manager__c = 'Test' ,MTOW__c=80,SeatTotal__c=20, BuildYear__c='2000'
                             ));
        insert listWF;
       
        List<String> gfList = new List<String>();
        Global_Fleet__c gf = [select Id,Y_Hidden_Serial_Number__c,AircraftType__c from Global_Fleet__c limit 1];
        String gfId = gf.Id;
        gfList.add(gfId);
        Aircraft__c aircraft = new Aircraft__c(
            Name = 'TestAircraft',
            MSN_Number__c = '0J54-E190-80972',
            Aircraft_Type__c = '737',
            TSN__c = 100,
             
            CSN__c = 100
        	);
        insert aircraft;
        Aircraft__c fetchedAC = [select Id, World_Fleet_Record__c,MSN_Number__c,Aircraft_Type__c from Aircraft__c limit 1];
        fetchedAC.World_Fleet_Record__c =null;
        update fetchedAC;
        Test.startTest();
        Database.executeBatch(new BatchUpdateWorldFleet()); 
       	Test.stopTest();
        
        Aircraft__c fetchedAC1 = [select Id, World_Fleet_Record__c,MSN_Number__c,Aircraft_Type__c from Aircraft__c limit 1];
        system.debug('fetchedAC----'+fetchedAC1);
        system.assertEquals(fetchedAC1.World_Fleet_Record__c,gfId);

    }
}