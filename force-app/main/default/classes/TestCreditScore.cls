@isTest
public class TestCreditScore {
    @testSetup 
    static void initialize(){
        String Opername   = 'Virgin Australia'; string alias='';
        String status = 'Approved';
        Country__c country = new Country__c(name='India');
        insert country;
        Operator__c Oper1 = new Operator__c( Name = Opername , Status__c = status , 
                                           
                                            Country_Lookup__c = country.id
                                           );
        insert Oper1;                                                
        
        
        
    }
    @isTest
    public static void TestCreditScoreValidation(){
        Map<String, Schema.SObjectField> mapCSField = Schema.SObjectType.Credit_Score__c.fields.getMap();
        Set<String> csFieldsSet = new  Set<String>();
        for(String Field:mapCSField.keyset()){
            Schema.DescribeFieldResult thisFieldDesc = mapCSField.get(Field).getDescribe();
            csFieldsSet.add(thisFieldDesc.getLocalName());
        }
        String unexpectedMsg='';
        Credit_Score__c newCredScore = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
                                                        Period_New__c = '2019',
                                                      	Airline__c = [select id from Operator__c limit 1].id
													);
        
        insert newCredScore;
        
        try{
		   Credit_Score__c newCredScore2 = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
                                                        Period_New__c = '2019',
                                                       	Airline__c = [select id from Operator__c limit 1].id
													);
        
        	insert newCredScore2;
        }catch(exception e){
            unexpectedMsg = e.getMessage();
            system.debug('Expected. Cannot insert duplicate Credit score');
        }
         

        
        if(!csFieldsSet.contains('Version__c')){
            system.assert(unexpectedMsg.contains('Credit Score Data is already loaded for the year.'));
        }
        
        
        try{
            Credit_Score__c newCredScore3 = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
                                                            Period_New__c = string.valueOf((System.today().year()+1)),
                                                            
                                                          Airline__c = [select id from Operator__c limit 1].id
                                                        );
            
            insert newCredScore3;
		}catch(exception e){
            unexpectedMsg = e.getMessage();
            system.debug('Expected. Cannot insert future Credit score');
        }
        if(!csFieldsSet.contains('Version__c')){
            system.assert(unexpectedMsg.contains('Credit Score Data cannot be loaded for the future dates'));
        }
    }

    @isTest
    private static void testLatestCreditScoreUpdateOnOpr(){
        Credit_Score__c newCredScore = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
                                                           Period_New__c = '2018',
                                                           Internal_Credit_Score_Current_Year_New__c='A',
                                                           Internal_Credit_Score_Last_Year_New__c='B',
                                                           Airline__c = [select id from Operator__c limit 1].id
                                                           );
        LeaseWareUtils.clearFromTrigger();
        insert newCredScore;

        Credit_Score__c newCredScore2 = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
                                                            Period_New__c = '2019',
                                                            Internal_Credit_Score_Current_Year_New__c='AAA',
                                                            Internal_Credit_Score_Last_Year_New__c='BBB',
                                                            Bloomberg_Rating__c='A',
                                                            Fitch_Rating__c='A',
                                                            LTM_1_Rating__c='A',
                                                            LTM_Rating__c='A',
                                                            LTM_2_Rating__c='A',
                                                            Moody_s_Rating__c='Aaa',
                                                            S_P_Rating__c='A',
                                                            Airline__c = [select id from Operator__c limit 1].id
                                                            );
        LeaseWareUtils.clearFromTrigger();
        insert newCredScore2;


        Operator__c opr = [select Id, Name,Internal_CS_Current_Year__c,Internal_CS_Last_Year__c,Bloomberg_Rating__c,Fitch_Rating__c,LTM_1_Rating__c,LTM_Rating__c,LTM_2_Rating__c,Moody_s_Rating__c,S_P_Rating__c from Operator__c limit 1];
        system.assertEquals(opr.Internal_CS_Current_Year__c , 'AAA');
        system.assertEquals(opr.Internal_CS_Last_Year__c , 'BBB');
        system.assertEquals(opr.Bloomberg_Rating__c,'A');
        system.assertEquals(opr.Fitch_Rating__c,'A') ;
        system.assertEquals(opr.LTM_1_Rating__c ,'A');
        system.assertEquals(opr.LTM_Rating__c , 'A');
        system.assertEquals(opr.LTM_2_Rating__c , 'A');
        system.assertEquals(opr.Moody_s_Rating__c ,'Aaa');
        system.assertEquals(opr.S_P_Rating__c , 'A') ;
       

    }

    @IsTest
    private static void testDeleteCS(){
        Credit_Score__c newCredScore2 = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
        Period_New__c = '2019',
        Internal_Credit_Score_Current_Year_New__c='AAA',
        Internal_Credit_Score_Last_Year_New__c='BBB',
        Bloomberg_Rating__c='A',
        Fitch_Rating__c='A',
        LTM_1_Rating__c='A',
        LTM_Rating__c='A',
        LTM_2_Rating__c='A',
        Moody_s_Rating__c='Aaa',
        S_P_Rating__c='A',
        Airline__c = [select id from Operator__c limit 1].id
        );
        insert  newCredScore2;

        
        Operator__c opr = [select Id, Name,Internal_CS_Current_Year__c,Internal_CS_Last_Year__c,Bloomberg_Rating__c,Fitch_Rating__c,LTM_1_Rating__c,LTM_Rating__c,LTM_2_Rating__c,Moody_s_Rating__c,S_P_Rating__c from Operator__c limit 1];
        system.assertEquals(opr.Internal_CS_Current_Year__c , 'AAA');
        system.assertEquals(opr.Internal_CS_Last_Year__c , 'BBB');
        system.assertEquals(opr.Bloomberg_Rating__c,'A');
        // opr.Final_Credit_Score_new__c = cs.Final_Credit_Score_new__c;
        system.assertEquals(opr.Fitch_Rating__c,'A') ;
        //  opr.Internal_Credit_Score_Last_Year_new__c= cs.Internal_Credit_Score_Last_Year_New__c ;
        system.assertEquals(opr.LTM_1_Rating__c ,'A');
        system.assertEquals(opr.LTM_Rating__c , 'A');
        system.assertEquals(opr.LTM_2_Rating__c , 'A');
        system.assertEquals(opr.Moody_s_Rating__c ,'Aaa');
        system.assertEquals(opr.S_P_Rating__c , 'A') ;

        delete newCredScore2;
         
        Operator__c opr1 = [select Id, Name,Internal_CS_Current_Year__c,Internal_CS_Last_Year__c,Bloomberg_Rating__c,Fitch_Rating__c,LTM_1_Rating__c,LTM_Rating__c,LTM_2_Rating__c,Moody_s_Rating__c,S_P_Rating__c from Operator__c limit 1];
        system.assertEquals(opr1.Internal_CS_Current_Year__c , null);
        system.assertEquals(opr1.Internal_CS_Last_Year__c , null);
        system.assertEquals(opr1.Bloomberg_Rating__c,null);
        system.assertEquals(opr1.Fitch_Rating__c,null) ;
        system.assertEquals(opr1.LTM_1_Rating__c ,null);
        system.assertEquals(opr1.LTM_Rating__c , null);
        system.assertEquals(opr1.LTM_2_Rating__c , null);
        system.assertEquals(opr1.Moody_s_Rating__c ,null);
        system.assertEquals(opr1.S_P_Rating__c , null) ;
    }

    @IsTest
    private static void testPeriodModify(){
        String unexpectedMsg;
        ID  oprId = [select id from Operator__c limit 1].id;
        Credit_Score__c newCredScore = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
        Period_New__c = '2018',
        Internal_Credit_Score_Current_Year_New__c='A',
        Internal_Credit_Score_Last_Year_New__c='B',
        Airline__c = oprId
        );
        LeaseWareUtils.clearFromTrigger();
        insert newCredScore;
        try{

            Credit_Score__c fetchedCS= [select Id,Name,Period_New__c from Credit_Score__c where Airline__c=:oprId];
            fetchedCS.Period_New__c = '2017';
            update fetchedCS;
        }catch(Exception e){
            unexpectedMsg = e.getMessage();

        }
        system.assert(unexpectedMsg.contains('You cannot modify the period'));
    }

    @IsTest
    private static void testUpdateLatestCS(){
        Credit_Score__c newCredScore = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
                                                           Period_New__c = '2018',
                                                           Internal_Credit_Score_Current_Year_New__c='A',
                                                           Internal_Credit_Score_Last_Year_New__c='B',
                                                           Airline__c = [select id from Operator__c limit 1].id
                                                           );
        LeaseWareUtils.clearFromTrigger();
        insert newCredScore;

        Credit_Score__c newCredScore2 = new Credit_Score__c(Name = 'Virgin Australia Credit Score',
        Period_New__c = '2019',
        Internal_Credit_Score_Current_Year_New__c='A',
        Internal_Credit_Score_Last_Year_New__c='B',
        Airline__c = [select id from Operator__c limit 1].id
        );
        LeaseWareUtils.clearFromTrigger();
        insert newCredScore2;

        Credit_Score__c fetchedCS = [select Id,Name,Period_New__c,Internal_Credit_Score_Current_Year_New__c,Internal_Credit_Score_Last_Year_New__c,
                                     Bloomberg_Rating__c,Fitch_Rating__c,LTM_1_Rating__c,LTM_Rating__c, LTM_2_Rating__c,
                                     Moody_s_Rating__c, S_P_Rating__c,Rating_Agency_Equivalent_Score__c,Rating_Status__c,Revenues__c,TAA_Equivalent_Score__c from Credit_Score__c where Id = :newCredScore2.id];

        fetchedCS.Internal_Credit_Score_Current_Year_New__c='AAA';
        fetchedCS.Internal_Credit_Score_Last_Year_New__c='BBB';
        fetchedCS.Bloomberg_Rating__c='A';
        fetchedCS.Fitch_Rating__c='A';
        fetchedCS.LTM_1_Rating__c='A';
        fetchedCS.LTM_Rating__c='A';
        fetchedCS.LTM_2_Rating__c='A';
        fetchedCS.Moody_s_Rating__c='Aaa';
        fetchedCS.S_P_Rating__c='A';
        fetchedCS.Rating_Agency_Equivalent_Score__c= '1';
        fetchedCS.Rating_Status__c='Provisional';
        fetchedCS.Revenues__c=100;
        fetchedCS.TAA_Equivalent_Score__c='2';
                                                            
        LeaseWareUtils.clearFromTrigger();
        update fetchedCS;


        Operator__c opr = [select Id, Name,Internal_CS_Current_Year__c,Internal_CS_Last_Year__c,Bloomberg_Rating__c,Fitch_Rating__c,
                            LTM_1_Rating__c,LTM_Rating__c,LTM_2_Rating__c,Moody_s_Rating__c,
                            S_P_Rating__c, Rating_Agency_Equivalent_Score_new__c,Rating_Status_New__c,Revenues__c,TAA_Equivalent_Score_new__c from Operator__c limit 1];
        system.assertEquals(opr.Internal_CS_Current_Year__c , 'AAA');
        system.assertEquals(opr.Internal_CS_Last_Year__c , 'BBB');
        system.assertEquals(opr.Bloomberg_Rating__c,'A');
        system.assertEquals(opr.Fitch_Rating__c,'A') ;
        system.assertEquals(opr.LTM_1_Rating__c ,'A');
        system.assertEquals(opr.LTM_Rating__c , 'A');
        system.assertEquals(opr.LTM_2_Rating__c , 'A');
        system.assertEquals(opr.Moody_s_Rating__c ,'Aaa');
        system.assertEquals(opr.S_P_Rating__c , 'A') ;
        system.assertEquals(opr.Rating_Agency_Equivalent_Score_new__c , 1) ;
        system.assertEquals(opr.Rating_Status_New__c , 'Provisional') ;
        system.assertEquals(opr.Revenues__c , 100) ;
        system.assertEquals(opr.TAA_Equivalent_Score_new__c ,2) ;

    }
}