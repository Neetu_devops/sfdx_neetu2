public with Sharing class DepartmentDefaultTriggerHandler implements ITrigger{

    
    // Constructor
    private final string triggerBefore = 'DepartmentDefaultTriggerHandlerBefore';
    private final string triggerAfter = 'DepartmentDefaultsTriggerHandlerAfter';
    public DepartmentDefaultTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        for(Department_Default__c curRec : (List<Department_Default__c>)trigger.new){
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Complete__c))curRec.Recipients_on_Complete__c.addError('Invalid email address or comma separated list of emails.');
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Incomplete__c))curRec.Recipients_on_Incomplete__c.addError('Invalid email address or comma separated list of emails.');
     
        }
            
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('PricingAnalysisTriggerHandler.beforeUpdate(+)');
        
        for(Department_Default__c curRec : (List<Department_Default__c>)trigger.new){
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Complete__c))curRec.Recipients_on_Complete__c.addError('Invalid email address or comma separated list of emails.');
            if(!LeaseWareUtils.IsEmailListValid(curRec.Recipients_on_Incomplete__c))curRec.Recipients_on_Incomplete__c.addError('Invalid email address or comma separated list of emails.');
     
        }
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

          

    }
     
    public void afterInsert()
    {
              
    }
     
    public void afterUpdate()
    {
           
    }
     
    public void afterDelete()
    {
            
    }

    public void afterUnDelete()
    {
             
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
    
/*   Bobby 20181110 - BOC requested to remove this validation as 
 * there may be scenarios where the client actually wants more than one department informed when everyone is done.
    //To check the final notification on before update and before insert
     public static void checkFinalNotificationOnDepartments(List<Department_Default__c> departmentDefaultList){
         
         try{
             Set<Id> setSetupId = new Set<Id>();
             Set<Id> setSetupIdsContainsFinal = new Set<Id>();
             for(Department_Default__c dept : departmentDefaultList){
                 setSetupId.add(dept.Setup__c);
             }
             
             List<Department_Default__c> lstDepartments = [SELECT 
                                                            ID,
                                                            Name,
                                                            Setup__c
                                                           FROM 
                                                            Department_Default__c 
                                                           WHERE 
                                                            Final_Notification__c =True
                                                           AND
                                                            Setup__c IN: setSetupId];
             
                 if(lstDepartments.size()>0){
                     for(Department_Default__c dept : lstDepartments){
                         setSetupIdsContainsFinal.add(dept.Setup__c);
                     }
                 }
             
                 for(Department_Default__c deptDefault : departmentDefaultList){
                     System.debug('deptDefault'+deptDefault);
                     if(deptDefault.Final_Notification__c && setSetupIdsContainsFinal.contains(deptDefault.Setup__c)){
                         deptDefault.addError('Only one record can be flagged as Final Notification?');
                     }
                     //Adding the record to set with final Notification True for Records in Bulk 
                     else {
                         setSetupIdsContainsFinal.add(deptDefault.Setup__c);
                     } 
                 }
             
         }catch(Exception e){
             System.debug('Exception Occured-->>'+e.getMessage());
         }
     }
*/
}