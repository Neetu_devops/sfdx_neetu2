public class AssetHistoryTriggerHandler implements ITrigger{
    
    // Constructor
    private final string triggerBefore = 'AssetHistoryTriggerHandlerBefore';
    private final string triggerAfter = 'AssetHistoryTriggerHandlerAfter';
    private LeaseWareUtils.typeString strMessage;
    
    private map<id,Asset_History__c> mapAHs;
    
    public AssetHistoryTriggerHandler()
    {
    }
    
    
    private void setSOQLStaticMAPOrList(){
    }      
    public void bulkBefore()
    {
        
    }
    
    public void bulkAfter()
    {
        
    }
    
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('AssetHistoryTriggerHandler.beforeInsert (+)');
        system.debug('AssetHistoryTriggerHandler.beforeInsert (-)');     
    }
    
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AssetHistoryTriggerHandler.beforeUpdate(+)');
        system.debug('AssetHistoryTriggerHandler.beforeUpdate(-)');
    }
    
    public void beforeDelete()
    {  
        
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AssetHistoryTriggerHandler.beforeDelete(+)');
        system.debug('AssetHistoryTriggerHandler.beforeDelete(-)');         
        
    }
    
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AssetHistoryTriggerHandler.afterInsert(+)');
        calculate_LastInstall_TSI_CSI();
        system.debug('AssetHistoryTriggerHandler.afterInsert(-)');      
    }
    
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('AssetHistoryTriggerHandler.afterUpdate(+)');
        calculate_LastInstall_TSI_CSI();
        system.debug('AssetHistoryTriggerHandler.afterUpdate(-)');      
    }
    
    public void afterDelete()
    {
        system.debug('AssetHistoryTriggerHandler.afterDelete(+)'); 
        system.debug('AssetHistoryTriggerHandler.afterDelete(-)');    
    }
    
    public void afterUnDelete()
    {
        system.debug('AssetHistoryTriggerHandler.afterUnDelete(+)');
        system.debug('AssetHistoryTriggerHandler.afterUnDelete(-)');        
    }
    
    public void andFinally()
    {
        
    }
    
    //  Added by PriyankaC : After Insert,Update & Delete of AssetHistoryTracking record 
    public static void calculate_LastInstall_TSI_CSI(){
        
        /***************************************** TSI - CSI *****************************************************/
        System.debug('calculate_LastInstall_TSI_CSI : IN');
        Asset_History__c[] listcurAsmHistEvents = (list<Asset_History__c>)(trigger.isDelete?trigger.old:trigger.new );
        Set<Id> setCAIds=new set<ID>{};
        
        for(Asset_History__c curCAEE : listcurAsmHistEvents){
            setCAIds.add(curCAEE.Assembly__c);
            
        }
        List<Constituent_Assembly__c> updateCAList = new List<Constituent_Assembly__c>();
        List<String> StatusCOdes = new List<String>{'Installation','Removal'};
        List<Constituent_Assembly__c> listAssemblies = [Select Id ,Name,Date_Installed__c,(Select Id,Name,Assembly__r.Status__c,Assembly__r.Utilization_Status__c,Status_Change_Date__c,
                                                                         Assembly_CSN__c,Assembly_TSN__c,Status__c 
                                                                         from Asset_History__r  
                                                                         where Status__c in: StatusCOdes order by Status_Change_Date__c desc,createdDate desc limit 1) 
                                                        from Constituent_Assembly__c where Id in :setCAIds];
        
        if(listAssemblies.size() > 0){
            System.debug('calculate_LastInstall_TSI_CSI : listAssemblies.size(): '+listAssemblies);
            for(Constituent_Assembly__c assembly : listAssemblies){
                if(assembly.Asset_History__r.size() > 0){
                    if('Installation'.equals(assembly.Asset_History__r[0].Status__c)){
                        System.debug('calculate_LastInstall_TSI_CSI : if Installation');
                        setAssemblyTSI_CSI(assembly,assembly.Asset_History__r[0].Assembly_TSN__c,assembly.Asset_History__r[0].Assembly_CSN__c,assembly.Asset_History__r[0].Status_Change_Date__c);
                    }
                    else if('Removal'.equals(assembly.Asset_History__r[0].Status__c)){
                        System.debug('calculate_LastInstall_TSI_CSI : if Removal');
                        setAssemblyTSI_CSI(assembly,null,null,null);
                    }
                } 
                else{
                    System.debug('calculate_LastInstall_TSI_CSI : else');
                    setAssemblyTSI_CSI(assembly,null,null,null);       
                }
               updateCAList.add(assembly); 
            }
            
            if(updateCAList.size() > 0){
                update updateCAList;
            }
        }
    }
    private static void setAssemblyTSI_CSI(Constituent_Assembly__c assembly, Decimal tsn, Decimal csn, Date dateInstalled){
        System.debug('setAssemblyTSI_CSI : +'+assembly+ ' TSN :'+tsn+ ' CSN :'+csn);
        assembly.TSN_at_Last_Install__c = tsn;
        assembly.CSN_at_Last_Install__c = csn;
        assembly.Date_Installed__c = dateInstalled;
        System.debug('setValuesForTSN_CSN : -');
    }
    
    
    
    
    
    
    
    
}