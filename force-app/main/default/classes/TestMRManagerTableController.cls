@isTest
public class TestMRManagerTableController {
    @testSetup static void TestSetupMethod() {        
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Assembly_RC_Info__c returnInfo = new Assembly_RC_Info__c();
        returnInfo.Return_Condition__c = rec.Id;
        returnInfo.Assembly_Type__c = 'Engine 1';
        returnInfo.Event_Type__c = 'Performance Restoration';
        returnInfo.RC_FC__c=100;
        insert returnInfo;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        lease.Aircraft__c = aircraft.Id;
        update lease;
        
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Type__c = 'MR Profile';
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Ext_Reserve_Type__c = 'EOLA';
        scenarioInp.Lease__c = lease.ID;
        insert scenarioInp;
        
        Scenario_Component_Input__c scenarioComponentInp = new Scenario_Component_Input__c();
        scenarioComponentInp.Name = 'Test Scenario Component Input';
        scenarioComponentInp.FH__c = 20;
        scenarioComponentInp.FC__c =21;
        scenarioComponentInp.Forecast_Type__c = 'Current Lease';
        scenarioComponentInp.Type__c ='Engine';
        scenarioComponentInp.User_Defined_Utilization_FH__c =True;
        scenarioComponentInp.Scenario_Input__c = scenarioInp.Id;
        scenarioComponentInp.Operating_Environment_Impact__c = True;
        insert scenarioComponentInp;
        
        Scenario_Component_Input__c scenarioComponentInp1 = new Scenario_Component_Input__c();
        scenarioComponentInp1.Name = 'Test Scenario Component Input';
        scenarioComponentInp1.FH__c = 20;
        scenarioComponentInp1.FC__c =21;
        scenarioComponentInp1.Forecast_Type__c = 'Lease Extension';
        scenarioComponentInp1.Type__c ='Engine';
        scenarioComponentInp1.User_Defined_Utilization_FH__c =True;
        scenarioComponentInp1.Scenario_Input__c = scenarioInp.Id;
        scenarioComponentInp1.Operating_Environment_Impact__c = True;
        insert scenarioComponentInp1;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7;
        insert constitutentAssembly;
        
        Assembly_RC_Info__c recAssembly = new Assembly_RC_Info__c(
            name='Test',
            RC_FC__c=23, 
            Event_Type__c= 'Performance Restoration',
            RC_FH__c =12,
            RC_Months__c = 2,
            Assembly_Type__c = 'Engine 1',
            Return_Condition__c = rec.Id
        );
        insert recAssembly;
        
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
		    Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
        
        Assembly_Event_Info__c event = new Assembly_Event_Info__c();
        event.Name = 'Test Info';
        event.Event_Cost__c = 20;
        event.Constituent_Assembly__c = constitutentAssembly.Id;
        event.Maintenance_Program_Event__c = mpe.Id;
        insert event;
        
        Fx_Assembly_Event_Input__c assemblyEvent = new Fx_Assembly_Event_Input__c(); 
        assemblyEvent.Name = 'Assembly Evnt Input Test';
        assemblyEvent.Interval_1_Hours__c = 18; 
        assemblyEvent.Interval_2_Cycles__c = 32;
        assemblyEvent.Event_cost__c = 50;
        assemblyEvent.Event_Type__c = 'Performance Restoration';
        assemblyEvent.Interval_2_Hours__c = 11; 
        assemblyEvent.Interval_1_Cycles__c = 12; 
        assemblyEvent.Interval_1_Months__c = 80;
        assemblyEvent.Last_Escalation_Date__c = System.today();
        assemblyEvent.Current_MRR__c = 30;
        assemblyEvent.Base_MRR__c = 11;
        assemblyEvent.Interval_2_Months__c = 2; 
        assemblyEvent.RC_FC__c = 21; 
        assemblyEvent.RC_FH__c = 12; 
        assemblyEvent.RC_Months__c = 21;
        assemblyEvent.Fx_Assembly_Input__c = scenarioComponentInp.Id;
        assemblyEvent.Annual_Escalation__c =1; 
        assemblyEvent.Starting_MR_Balance__c = 12;
        assemblyEvent.Assembly__c = constitutentAssembly.Id;
        assemblyEvent.Projected_Event__c =event.Id; 
        insert assemblyEvent;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Lkp__c = constitutentAssembly.Id, 
            Base_MRR__c =20,
            Assembly_Event_Info__c = event.Id,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
			Rate_Basis__c='Hours',
            Last_Escalation_Date__c = System.today());
        insert assemblyMR;
        
        Fx_SV_Override__c sv = new Fx_SV_Override__c();
        sv.Fx_Assembly_Event_Input__c = assemblyEvent.Id;
        sv.Name = 'Test';
        sv.SV_Number__c =20;
        insert sv;
        
        Scenario_Component__c scenarioComponent = new Scenario_Component__c(
            Name = 'Test Scenario Component',
            Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'Cycle', 
            Type__c = 'Airframe',
            Event_Type__c = '4c/6y', 
            RC_Months__c = 2, 
            Life_Limited_Part_LLP__c = null,
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent;
        
        Scenario_Component__c scenarioComponent2 = new Scenario_Component__c(
            Name = 'Test Scenario Component',
            Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'Monthly', 
            Type__c = 'APU',
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent2;
        
        Scenario_Component__c scenarioComponent3 = new Scenario_Component__c(
            Name = 'Test Scenario Component',
            Fx_General_Input__c = scenarioInp.Id,
            Rate_Basis__c = 'HOURS', 
            Type__c = 'Engine 1 LLP',
            RC_Months__c = 2, 
            Life_Limit_Interval_2_Months__c =2,
            RC_FC__c = 12,
            Life_Limit_Interval_2_Cycles__c = 120, 
            RC_FH__c = 12,
            Life_Limit_Interval_2_Hours__c = 2);
        insert scenarioComponent3;
        
        Monthly_Scenario__c monthlyScenario = new Monthly_Scenario__c(
            Name = 'Test Monthly Scenario',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            Event_Date__c= System.today(),
            Rent_Amount__c =12, 
            Fx_Component_Output__c = scenarioComponent.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            End_Date__c = System.today(),
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 20,
            Historical_Month__c = true,
            FH__c = 3,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FC__c =4,
            FC_At_SV__c = 10,
            FH_At_SV__c = 5,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40,
            Claim_Month_Period__c = System.today());
        insert monthlyScenario;
        
        Monthly_Scenario__c monthlyScenario2 = new Monthly_Scenario__c(
            Name = 'Test Monthly Scenario2',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            End_Date__c = System.today(),
            Event_Date__c= System.today(),
            Rent_Amount__c =11, 
            Fx_Component_Output__c = scenarioComponent2.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
            Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 220,
            FH__c = 32,
            FC__c = 14,
            FC_At_SV__c = 1,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FH_At_SV__c = 52,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40,
            Claim_Month_Period__c = System.today());
        insert monthlyScenario2;
        
        Monthly_Scenario__c monthlyScenario3= new Monthly_Scenario__c(
            Name = 'Test Monthly Scenario3',
            Total_FH_Cumulative__c = 12,
            Security_Deposit__c = 1,
            Cash_Flow__c = 12,
            End_Date__c = System.today(),
            Event_Date__c= System.today(),
            Rent_Amount__c =11, 
            Fx_Component_Output__c = scenarioComponent3.Id, 
            Net_MR__c =30,
            Start_Date__c = System.today(),
            Cost_Per_FH__c = 12,
            Eola_Amount__c = 10,
			Return_Month__c = true,
            Return_Condition_Status__c = True,
            Event_Expense_Amount__c = 220,
            FH__c = 32,
            FC__c = 14,
            FC_At_SV__c = 1,
            MR_Contribution__c = 20,
            Airline_Contribution__c = 40,
            FH_At_SV__c = 52,
            Current_Rate_Escalated__c =  20,
            MR_Amount__c = 40,
            Claim_Month_Period__c = System.today(),
            Historical_Month__c = true);
        insert monthlyScenario3;
    }
    
    @isTest
    static void testMethod1(){
        try{
            Scenario_Input__c scenarioInp = [SELECT Id FROM Scenario_Input__c LIMIT 1];
            Aircraft__c asset = [SELECT Id FROM Aircraft__c LIMIT 1];
            Test.startTest();
            MRManagerTableController.getForecastData(scenarioInp.Id,asset.Id);
            Test.stopTest();
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }
        
    }
    
    @isTest
    static void testMethod2(){
        try{
            Scenario_Input__c scenarioInp = [SELECT Id FROM Scenario_Input__c LIMIT 1];
            Aircraft__c asset = [SELECT Id FROM Aircraft__c LIMIT 1];
            Monthly_Scenario__c ms = [SELECT Id,Name FROM Monthly_Scenario__c WHERE Name = 'Test Monthly Scenario3' LIMIT 1];
            Constituent_Assembly__c cs = [SELECT Id, Name FROM Constituent_Assembly__c LIMIT 1];
            Test.startTest();
            MRManagerTableController.getForecastData(scenarioInp.Id,asset.Id);
            MRManagerTableController.generateMRProfile(scenarioInp.Id);
            MRManagerTableController.isForecastGenerated(scenarioInp.Id);
            Test.stopTest();
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }
}