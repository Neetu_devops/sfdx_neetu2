public class AircraftDepreciationScheduleHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string  triggerBefore = 'AircraftDepreciationScheduleHandlerBefore';
    private final string  triggerAfter = 'AircraftDepreciationScheduleHandlerAfter';
    public AircraftDepreciationScheduleHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('AircraftDepreciationScheduleHandler.beforeInsert(+)');
    	if(!LeaseWareUtils.isFromTrigger('InsertedFromAircraftTriggerHandler') ) {
    		Aircraft_Depreciation_Schedule__c[] listNew = (list<Aircraft_Depreciation_Schedule__c>)trigger.new ;
    		for(Aircraft_Depreciation_Schedule__c curRec:listNew){
    			curRec.addError('You are not allowed to create Depreciation Schedule records.');
    		}
    	}    	
	
	    system.debug('AircraftDepreciationScheduleHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AircraftDepreciationScheduleHandler.beforeUpdate(+)');
    	
    	if(!LeaseWareUtils.isFromTrigger('InsertedFromAircraftTriggerHandler') && !LeaseWareUtils.isFromTrigger('DeletedFromAircraftTriggerHandler')) {
	    	Aircraft_Depreciation_Schedule__c[] listNew = (list<Aircraft_Depreciation_Schedule__c>)trigger.new ;
	
	      	list<String> listFieldsToCheck = new list<String>(); 
	            
		    Map<String, Schema.SObjectField> mapInvField = Schema.SObjectType.Aircraft_Depreciation_Schedule__c.fields.getMap();
		    for(String Field:mapInvField.keyset()){
		    	Schema.DescribeFieldResult thisFieldDesc = mapInvField.get(Field).getDescribe();
		        System.debug('Field Name is ' + thisFieldDesc.getName() + '. Local ' + thisFieldDesc.getLocalName());
		
		        if( !thisFieldDesc.isUpdateable() || (LeaseWareUtils.getNamespacePrefix()+'Impairment_Value__c').equals(thisFieldDesc.getName())  ){
		        	continue; //Add the fields that are always updatable to the list. || ('leaseworks__Comments__c'.equals(thisFieldDesc.getName()))
		        }
		            
		        listFieldsToCheck.add(thisFieldDesc.getName());
			}
	    	for(Aircraft_Depreciation_Schedule__c curRec:listNew){
		        	for(String curField:listFieldsToCheck){
		            	if(trigger.newMap.get(curRec.id).get(curField) !=  trigger.oldMap.get(curRec.id).get(curField)){ 
		                    system.debug(curField +' changed from ' + trigger.oldMap.get(curRec.id).get(curField) + ' to ' + trigger.newMap.get(curRec.id).get(curField) + '.');
		                    curRec.addError('You cannot update other than Impairment Value.');
		                    break;
		                }
		            }    
	    	}   
	    	
	    	// if no error , then check if Impairement value is changed or not , if yes do next process  	
	    	for(Aircraft_Depreciation_Schedule__c curRec:listNew){
	    		if( curRec.Impairment_Value__c!=null &&curRec.Impairment_Value__c != ((Aircraft_Depreciation_Schedule__c)trigger.oldmap.get(curRec.Id)).Impairment_Value__c){
	    			// check if any write off happened in future period. If yes then dont allow to change this.
					Aircraft_Depreciation_Schedule__c[] checkADS = 
							[select ID,Period_Sequence__c,Impairment_Value__c 
									from Aircraft_Depreciation_Schedule__c 
									where Aircraft__c = :curRec.Aircraft__c
									and Impairment_Value__c !=null and Period_Sequence__c > :curRec.Period_Sequence__c];	 
					if(checkADS.size()>0)	curRec.addError('Write off is already done for future Depreciation Period. You cannot set Impairment Value for this Period.');								   			
					if( !Test.isRunningTest())updateADSBasedOnImpairmentFuture(curRec.Aircraft__c,curRec.Period_Sequence__c,curRec.Id);
	    		}
	    	} 
    	}      	
    	system.debug('AircraftDepreciationScheduleHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AircraftDepreciationScheduleHandler.beforeDelete(+)');
    	
    	if(!LeaseWareUtils.isFromTrigger('DeletedFromAircraftTriggerHandler')) {
    		Aircraft_Depreciation_Schedule__c[] listNew = (list<Aircraft_Depreciation_Schedule__c>)trigger.old ;
    		for(Aircraft_Depreciation_Schedule__c curRec:listNew){
    			curRec.addError('You are not allowed to delete Depreciation Schedule records.');
    		}
    	}      	
    	
    	system.debug('AircraftDepreciationScheduleHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftDepreciationScheduleHandler.afterInsert(+)');
    	
    	system.debug('AircraftDepreciationScheduleHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftDepreciationScheduleHandler.afterUpdate(+)');
      	
    	system.debug('AircraftDepreciationScheduleHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftDepreciationScheduleHandler.afterDelete(+)');
    	
    	
    	system.debug('AircraftDepreciationScheduleHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AircraftDepreciationScheduleHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('AircraftDepreciationScheduleHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
	
	@future
	public static void updateADSBasedOnImpairmentFuture(Id ACID,Decimal PeriodSeq,ID ADSID){
		
		Aircraft_Depreciation_Schedule__c[] listADS = 
				[select Id,Aircraft__r.Depreciation_Schedule__r.Number_of_Years__c
						,Period_Sequence__c,Impairment_Value__c 
						,Aircraft__r.Book_Value__c,Aircraft__r.Equipment_Cost__c,Aircraft__r.Residual_Value__c
						,Salvage_Value__c,Depreciation__c,Depr_to_Date__c,New_Residual_Value__c
						from Aircraft_Depreciation_Schedule__c 
						where Aircraft__c = :ACID
						order by Period_Sequence__c];
		
		// Assuming the impairement happen only case for Straight line
		Integer BookStartSeq=0;
		Integer NumberOfYear = listADS[0].Aircraft__r.Depreciation_Schedule__r.Number_of_Years__c.intValue() ;
		for(Aircraft_Depreciation_Schedule__c curRec:listADS){
			if(curRec.Depreciation__c !=null) break;
			BookStartSeq++;
		}
		system.debug('BookStartSeq' +BookStartSeq);
		
		Integer RemainingNumOfPeriod = listADS[0].Aircraft__r.Depreciation_Schedule__r.Number_of_Years__c.intValue()*12 - (PeriodSeq.intValue() - BookStartSeq);
		Integer EndPeriod = listADS[0].Aircraft__r.Depreciation_Schedule__r.Number_of_Years__c.intValue()*12 + BookStartSeq;
		Decimal Depreciation_Rate_local = 100.00/RemainingNumOfPeriod;
		
		system.debug('RemainingNumOfPeriod' +RemainingNumOfPeriod);
		system.debug('EndPeriod' +EndPeriod);
		system.debug('Depreciation_Rate_local' +Depreciation_Rate_local);
		
		Decimal Book_Depr_Cost ;

		Decimal DepreciationCost,NewResidualValue =null;
		
		for(Integer iter=PeriodSeq.intValue()-1;iter<EndPeriod;iter++){	
			if(iter == PeriodSeq.intValue()-1){
				// for  the month on which write off (Impairement) happened
				
				Book_Depr_Cost = LeaseWareUtils.zeroIfNull(listADS[iter].Aircraft__r.Book_Value__c==null?listADS[iter].Aircraft__r.Equipment_Cost__c:listADS[iter].Aircraft__r.Book_Value__c) ;//- LeaseWareUtils.zeroIfNull(listADS[iter].Aircraft__r.Residual_Value__c);
				system.debug('Book_Depr_Cost'+Book_Depr_Cost);
				// if Impairment value is less than residual , set Impairment as new residual value
				if(listADS[iter].Impairment_Value__c < LeaseWareUtils.zeroIfNull(listADS[iter].Aircraft__r.Residual_Value__c)){
					 NewResidualValue = listADS[iter].Impairment_Value__c;
				}
				else {
					NewResidualValue = LeaseWareUtils.zeroIfNull(listADS[iter].Aircraft__r.Residual_Value__c) ;
				}
				
								
				
				listADS[iter].Salvage_Value__c = listADS[iter].Impairment_Value__c  - NewResidualValue;
				listADS[iter].Depr_to_Date__c = Book_Depr_Cost - listADS[iter].Impairment_Value__c;
				if(iter>0) listADS[iter].Depreciation__c = listADS[iter].Depr_to_Date__c - LeaseWareUtils.zeroIfNull(listADS[iter-1].Depr_to_Date__c);
				else listADS[iter].Depreciation__c = listADS[iter].Depr_to_Date__c;
				listADS[iter].New_Residual_Value__c  = NewResidualValue;
				DepreciationCost = listADS[iter].Salvage_Value__c ;
			}
			else{
				// After the month on which write off (Impairement) happened
				listADS[iter].Depreciation__c = DepreciationCost * Depreciation_Rate_local/100;
				listADS[iter].Depr_to_Date__c = listADS[iter-1].Depr_to_Date__c	 + listADS[iter].Depreciation__c ;	
				system.debug('Book_Depr_Cost1'+Book_Depr_Cost);
				system.debug('listADS[iter].Depr_to_Date__c'+listADS[iter].Depr_to_Date__c);			
				listADS[iter].Salvage_Value__c = (Book_Depr_Cost -NewResidualValue ) - listADS[iter].Depr_to_Date__c ;
				listADS[iter].New_Residual_Value__c  = NewResidualValue;
			}			
			
		}		
		
		
		for(Aircraft_Depreciation_Schedule__c curADS:listADS){
			curADS.Salvage_Value__c = curADS.Salvage_Value__c!=null?Math.round(curADS.Salvage_Value__c):null;
			curADS.Depreciation__c = curADS.Depreciation__c!=null?Math.round(curADS.Depreciation__c):null;
			curADS.Depr_to_Date__c = curADS.Depr_to_Date__c!=null?Math.round(curADS.Depr_to_Date__c):null;
			curADS.New_Residual_Value__c = curADS.New_Residual_Value__c!=null?Math.round(curADS.New_Residual_Value__c):null;

		}
		LeaseWareUtils.setFromTrigger('InsertedFromAircraftTriggerHandler');	
		update listADS;		
		LeaseWareUtils.unsetTriggers('InsertedFromAircraftTriggerHandler');	
	}

}