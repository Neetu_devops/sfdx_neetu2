public class JournalListController {

    /* loadFilterValues : Method to fetch the initial picklist options for the filters */
    @AuraEnabled(cacheable=true)
    public static String loadFilterValues(){
        FilterOptionsWrapper filterWrap = new FilterOptionsWrapper();
        List<Calendar_Period__c> calenderPeriodList = new List<Calendar_Period__c>([Select Id,Name, Start_Date__c,End_Date__c From Calendar_Period__c Order By Start_Date__c]);
        
        OptionWrapper filterChildWrap; 
        
        Set<Date> startDateSet = new Set<Date>();
        Set<Date> endDateSet = new Set<Date>();
        /* Populating picklist options for 'To Accounting Period' and 'From Accounting Period' */
        for(Calendar_Period__c calenderPeriod : calenderPeriodList){
            if(calenderPeriod != null){
                if(calenderPeriod.Start_Date__c != null){
                    if(!startDateSet.contains(calenderPeriod.Start_Date__c)){
                        /* Populating data for lightning-combobox
                         * label : CalenderPeriod Name
                         * value : CalenderPeriod Id
                         */ 

                        filterWrap.startDateOptions.add(new OptionWrapper(String.valueOf(calenderPeriod.Name),
                                                                              String.valueOf(calenderPeriod.Id)));
                        startDateSet.add(calenderPeriod.Start_Date__c);
                    }    
                }
                if(calenderPeriod.End_Date__c != null){
                    if(!endDateSet.contains(calenderPeriod.End_Date__c)){
                        filterWrap.endDateOptions.add(new OptionWrapper(String.valueOf(calenderPeriod.Name),
                                                                       String.valueOf(calenderPeriod.Id)));
                        endDateSet.add(calenderPeriod.End_Date__c);
                    } 
                }
            }
        }
        
        //Status Options
        String prefix  = LeaseWareUtils.getNamespacePrefix();
        
        //Add picklist options
        filterWrap.statusOptions.add(new OptionWrapper('All',''));
        filterWrap.statusOptions.addAll(getPicklistMap(prefix + 'Journal_Entry__c', prefix + 'Journal_Status__c'));
        filterWrap.journalSourceOptions.addAll(getPicklistMap(prefix + 'Journal_Entry__c', prefix + 'Transaction_Type__c'));
        
        
        ListView calenderPeriodListView = new ListView();
        calenderPeriodListView = [SELECT Id, Name, DeveloperName, SobjectType FROM ListView where SobjectType =: prefix + 'Calendar_Period__c' AND DeveloperName = 'All' LIMIT 1];
        if(calenderPeriodListView != null){
            filterWrap.listViewId = calenderPeriodListView.Id;
        }
        
        return JSON.serialize(filterWrap);
    }
    
    public static List<OptionWrapper> getPicklistMap(String objectName, String fieldName){
        List<OptionWrapper> options = new List<OptionWrapper>();
        Schema.DescribeSObjectResult r = Schema.getGlobalDescribe().get(objectName).getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            options.add(new OptionWrapper(pickListVal.getLabel(),pickListVal.getValue()));
            } 
        return options;
        }

    @AuraEnabled(cacheable=true)
    public static String loadJournalEntryData(String startAccountingPeriodId, String endAccountingPeriodId, String optionalFilters){
    
        Date startDate, endDate;
        String qry = 'Select Id, Journal_Number__c, Lease__r.Serial_Number__c,Journal_Status__c, Calendar_Period__r.Name,Invoice__r.Name,';
               qry += 'Name, JE_Description__c, Journal_Date__c, Transaction_Type__c, Lease__r.Name,';
               qry += 'lease__r.aircraft__r.MSN_Number__c, GL_Account_Code__c, Legal_Entity__c, Bank_Account__c,'; 
               qry += 'Bank_Account__r.RecordTypeId, Bank_Account__r.Name, Debit__c, payment__r.Name, Credit__c,'; 
               qry += 'Invoice__c, payment__c, Invoice__r.Lease__r.Aircraft__r.Owner.Name,';
               qry += 'Lease__r.Lessee__r.Name,Lease__r.Lessee_Bank_Routing_Number_For_Rent__c,Y_Hidden_Serial_Number__c,'; 
               qry += 'Y_Hidden_Amount__c, Sign__c, Y_Hidden_Trx_Reference__c,XML_Sent__c,Invoice__r.Lease__r.Legal_Entity__r.name,';
               qry += 'Journal_Sent_Date__c, Lease__r.lessor__r.Name, Lease__r.lessor__c, Invoice__r.Lease__r.Aircraft__r.OwnerId,';
               qry += 'PrePayment__c, PrePayment__r.Name, PrePayment__r.Lease__r.lessor__c, PrePayment__r.Lease__r.lessor__r.Name,';
               qry += 'PrePayment__r.Bank_Account_Lkp__c, PrePayment__r.Bank_Account_Lkp__r.RecordTypeId, PrePayment__r.Bank_Account_Lkp__r.Name';
	       qry += ' from Journal_Entry__c where ';
        
        if(startAccountingPeriodId != null && endAccountingPeriodId != null){
        
            for(Calendar_Period__c calenderPeriod : [Select Id,Name, Start_Date__c, End_Date__c 
                                                                    From Calendar_Period__c
                                                                    WHERE Id =: startAccountingPeriodId
                                                                    or Id =: endAccountingPeriodId
																	order by Start_Date__c asc]){
                                                            
                if(calenderPeriod.Id == startAccountingPeriodId){
                    startDate = calenderPeriod.Start_Date__c;
                } 
                
                if(calenderPeriod.Id == endAccountingPeriodId){
                    endDate = calenderPeriod.Start_Date__c;
                }
				
				 if(startDate != null && endDate != null){
                	break;  
                }  
            }
            
            //Handle validation
            if(startDate != null && endDate != null){
                if(endDate < startDate){
                    throw new AuraHandledException('To Accounting Period cannot be before From Accounting Period');
                }
            }
            
            qry += 'Calendar_Period__r.Start_Date__c <= :endDate AND ';  
            qry += 'Calendar_Period__r.Start_Date__c >= :startDate AND ';
            qry += '(Invoice__c != null or payment__c !=null or PrePayment__c != null)';
                
            List<FilterWrapper> optionalFilterList  = (List<FilterWrapper>)JSON.deserialize(optionalFilters, List<FilterWrapper>.class);
            //system.debug('-------optionalFilterList----------');
            String additionalFilters = prepareFilters(optionalFilterList);
            //system.debug(additionalFilters);                                                        
                                                                     
            if(String.isNotBlank(additionalFilters)){
                qry += ' AND ' + additionalFilters;
             }
             
        }else{
             qry += 'Calendar_Period__c =: startAccountingPeriodId AND ' ;
             qry += '(Invoice__c != null or payment__c != null or PrePayment__c != null)';
        }
        
        
        qry += ' order by Lease__r.Name, Invoice__c, JE_Description__c';
        
        
        Id bankAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Bank_Accounts').getRecordTypeId();
        JEWrapper jeWrap = new JEWrapper(); 
        Map<Id,List<Journal_Dimension__c>> jeIdWithJDMap = new Map<Id,List<Journal_Dimension__c>>();
        List<Journal_Entry__c> entries = Database.query(qry);
        jeWrap.jeMap = new Map<Id,Journal_Entry__c>(entries);
         
       
       // System.assert(false,startAccountingPeriodId  + '');
       
        for(Journal_Dimension__c jd : [Select Id, Dimension_Value__c,Journal_Entry__c, 
                                                    Name From Journal_Dimension__c 
                                                    where Journal_Entry__c IN: jeWrap.jeMap.keySet()]){
            //Populate mapJEIdWithJD 
            if(!jeIdWithJDMap.containsKey(jd.Journal_Entry__c)){
                jeIdWithJDMap.put(jd.Journal_Entry__c,new List<Journal_Dimension__c>());
            } 
            jeIdWithJDMap.get(jd.Journal_Entry__c).add(jd);
        }
        jeWrap.jeIdWithJDMap = jeIdWithJDMap;
        
        
        //Column headers from Journal Entry
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('ACCOUNTING PERIOD','accountingPeriodName','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('STATUS', 'Journal_Status__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('INVOICE/PAYMENTS','invoiceURL','url')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('LESSOR','lessorURL','url')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('GL ACCOUNT DESCRIPTION','journalEntryURL','url')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('JE DESCRIPTION', 'JE_Description__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('JOURNAL DATE', 'Journal_Date__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('JOURNAL SENT DATE', 'Journal_Sent_Date__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('JOURNAL SOURCE', 'Transaction_Type__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('JOURNAL NUMBER', 'Journal_Number__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('GL ACCOUNT','glAccountCode','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('LEGAL ENTITY','Legal_Entity__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('INTERCOMPANY AFFILIATE', 'Intercompany__c','text')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('DEBIT','Debit__c','currency')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('CREDIT','Credit__c','currency')));
        jeWrap.dataTableColumns.add((new DataTableColumnsWrapper('BANK ACCOUNT','accountURL','url')));

        
        for (Journal_Entry__c je: jeWrap.jeMap.values()){
            DataWrapper dataWrap = new DataWrapper(je,jeIdWithJDMap, bankAccountRecordTypeId);
            if (je.Debit__c != null) 
                jeWrap.totalDebit += je.Debit__c;
            if (je.Credit__c != null) 
                jeWrap.totalCredit += je.Credit__c;    
            if(je.XML_Sent__c == false){
                jeWrap.xmlNotSentJournalIdSet.add(je.Id);
            }  
            jeWrap.dataWrapList.add(dataWrap);
        }
        if(jeWrap.xmlNotSentJournalIdSet.size() == 0){
            jeWrap.disableSendJournal = true;
        }
        
        DataWrapper dataWrap = new DataWrapper(jeWrap.totalDebit,jeWrap.totalCredit);
        jeWrap.dataWrapList.add(dataWrap);
        //System.debug('jeWrap: ' + jeWrap);
        return JSON.serialize(jeWrap);
    }
    
    private static String prepareFilters(List<FilterWrapper> optionalFilterList){
        String filters = '';
        for(FilterWrapper filter : optionalFilterList){
            filters += filter.fieldApiName + ' = \'' + String.escapeSingleQuotes(filter.value) + '\'';
            filters += ' And ';
        }
        
        if(filters.endsWith(' And ')){
            filters = filters.removeEnd(' And ');
        }
        return filters;
    }
    
    //Required fields for the column object of lightning-datatable
    public class DataTableColumnsWrapper{
        @AuraEnabled public String label{get;set;}
        @AuraEnabled public String fieldName{get;set;}
        @AuraEnabled public String type{get;set;}
        
        DataTableColumnsWrapper(String label,String fieldName,String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
        }
    }
    
    public class DataWrapper{
        //Journal Entry Fields
        @AuraEnabled public String accountingPeriodName{get;set;}
        @AuraEnabled public String invoice{get;set;}
        @AuraEnabled public String glAccountCode{get;set;}
        @AuraEnabled public String lessorName{get;set;}
        
        @AuraEnabled public Map<String,Map<String,String>> typeAttributesMap{get;set;}
        @AuraEnabled public String lessorURL{get;set;}
        @AuraEnabled public String invoiceURL{get;set;}
        @AuraEnabled public String journalEntryURL{get;set;}
        @AuraEnabled public String journalEntryName{get;set;}
        @AuraEnabled public String bankAccount{get;set;}
        @AuraEnabled public String accountURL{get;set;}
        
        @AuraEnabled public Map<String,String> journalEntryFieldNameAndValue{get;set;}
        @AuraEnabled public Map<String,String> jdNameAndValue{get;set;}
        @AuraEnabled public Set<String> journalEntryFields{get;set;} 
        
        
        DataWrapper(Journal_entry__c je,Map<Id,List<Journal_Dimension__c>> jeIdWithJDMap, Id bankAccountRecordTypeId){
            this.journalEntryFields = new Set<String>{'Name','JE_Description__c','Journal_Date__c','Transaction_Type__c','Journal_Number__c', 'Legal_Entity__c','Intercompany__c','Debit__c','Credit__c','Journal_Sent_Date__c','Journal_Status__c'};
            this.journalEntryFieldNameAndValue = new Map<String,String>();
            this.typeAttributesMap = new Map<String,Map<String,String>>();
            if(je.Calendar_Period__c != null && String.isNotBlank(je.Calendar_Period__r.Name)){
                this.accountingPeriodName = je.Calendar_Period__r.Name;
            }	
			
	    	String prefix  = LeaseWareUtils.getNamespacePrefix();	
            
            //Creating journalEntryURL
            if(je.Id != null && String.isNotBlank(je.Name)){
                this.journalEntryName = je.Name;
                this.journalEntryURL = '/lightning/r/'+ prefix +'Journal_Entry__c/' + je.Id + '/view';
            }
            
            //Creating invoiceURL
            if(je.Invoice__c != null){
                if(String.isNotBlank(je.Invoice__r.Name)){
                    this.invoice = je.Invoice__r.Name;    
                }
                this.invoiceURL = '/lightning/r/'+ prefix +'Invoice__c/' + je.Invoice__c + '/view';
                if(je.Lease__c != null && je.Lease__r.lessor__c != null){
                    this.lessorName = je.Lease__r.lessor__r.Name;   
                    this.lessorURL = '/lightning/r/'+ prefix +'Counterparty__c/' + je.Lease__r.lessor__c + '/view';
                }
                if(je.Bank_Account__c != null && je.Bank_Account__r.Name != null && je.Bank_Account__r.RecordTypeId == bankAccountRecordTypeId){
                    this.bankAccount = je.Bank_Account__r.Name;   
                    this.accountURL = '/lightning/r/Account/' + je.Bank_Account__c + '/view';
                }
            }
            else if (je.payment__c != null){
                if(String.isNotBlank(je.payment__r.Name)){
                    this.invoice = je.payment__r.Name;    
                }
                this.invoiceURL = '/lightning/r/'+ prefix +'Payment__c/' + je.payment__c + '/view';
                if(je.Lease__c != null && je.Lease__r.lessor__c != null){
                    this.lessorName = je.Lease__r.lessor__r.Name;   
                    this.lessorURL = '/lightning/r/'+ prefix +'Counterparty__c/' + je.Lease__r.lessor__c + '/view';
                }
                if(je.Bank_Account__c != null && je.Bank_Account__r.Name != null && je.Bank_Account__r.RecordTypeId == bankAccountRecordTypeId){
                    this.bankAccount = je.Bank_Account__r.Name;
                    this.accountURL = '/lightning/r/Account/' + je.Bank_Account__c + '/view';
                }
            }
            else if(je.PrePayment__c != null){
                if(String.isNotBlank(je.PrePayment__r.Name)){
                    this.invoice = je.PrePayment__r.Name;
                }
                this.invoiceURL = '/lightning/r/'+ prefix +'Payment_Receipt__c/' + je.PrePayment__c + '/view';
                if(je.PrePayment__r.Lease__c != null && je.PrePayment__r.Lease__r.lessor__c != null){
                    this.lessorName = je.PrePayment__r.Lease__r.lessor__r.Name;
                    this.lessorURL = '/lightning/r/'+ prefix +'Counterparty__c/' + je.PrePayment__r.Lease__r.lessor__c + '/view';
                }
                if(je.PrePayment__r.Bank_Account_Lkp__c != null && je.PrePayment__r.Bank_Account_Lkp__r.Name != null && je.PrePayment__r.Bank_Account_Lkp__r.RecordTypeId == bankAccountRecordTypeId){
                    this.bankAccount = je.PrePayment__r.Bank_Account_Lkp__r.Name;
                    this.accountURL = '/lightning/r/Account/' + je.PrePayment__r.Bank_Account_Lkp__c + '/view';
                }
            }
            
            Map<String,String> tempTypeAttributesMap = new Map<String,String>();
            tempTypeAttributesMap.put('fieldName','invoice');
            tempTypeAttributesMap.put('target','_blank');
            this.typeAttributesMap.put('invoice',tempTypeAttributesMap);
            //For Lessor
            tempTypeAttributesMap = new Map<String,String>();
            tempTypeAttributesMap.put('fieldName','lessorName');
            tempTypeAttributesMap.put('target','_blank');
            this.typeAttributesMap.put('lessor',tempTypeAttributesMap);
            //For Bank Account
            tempTypeAttributesMap = new Map<String,String>();
            tempTypeAttributesMap.put('fieldName','bankAccount');
            tempTypeAttributesMap.put('target','_blank');
            this.typeAttributesMap.put('bankAccount',tempTypeAttributesMap);
            //For Journal Entry
            tempTypeAttributesMap = new Map<String,String>();
            tempTypeAttributesMap.put('fieldName','journalEntryName');
            tempTypeAttributesMap.put('target','_blank');
            this.typeAttributesMap.put('journalEntry',tempTypeAttributesMap);
            
            
            this.glAccountCode = je.GL_Account_Code__c;
            this.jdNameAndValue = new Map<String,String>();
            
            if(jeIdWithJDMap.containsKey(je.Id) && jeIdWithJDMap.get(je.Id).size() > 0){
                for(Journal_Dimension__c jd : jeIdWithJDMap.get(je.Id)){
                    if(!this.jdNameAndValue.containsKey(jd.Name)){
                        this.jdNameAndValue.put(jd.Name,jd.Dimension_Value__c);
                    }
                }
            }
            
            if(je!= null){
                for(String fieldName : this.journalEntryFields){
                    if(je.isSet(fieldName)){
                        this.journalEntryFieldNameAndValue.put(fieldName,String.valueOf(je.get(fieldName)));
                    }
                }
            }
        }
        
        DataWrapper(Decimal totalDebit, Decimal totalCredit){
            this.accountingPeriodName = '';
            this.invoice = '';
            this.lessorName = '';
            this.lessorURL = '';
            this.bankAccount = '';
            this.accountURL = '';
            this.glAccountCode = '';
            this.invoiceURL = '';
            this.jdNameAndValue = new Map<String,String>();
            this.typeAttributesMap = new Map<String,Map<String,String>>();
            this.journalEntryFieldNameAndValue = new Map<String,String>();
            this.journalEntryFieldNameAndValue.put('Debit__c',String.valueOf(totalDebit));
            this.journalEntryFieldNameAndValue.put('Credit__c',String.valueOf(totalCredit));
        }
    }
    
    public class JEWrapper{
        @AuraEnabled public List<DataTableColumnsWrapper> dataTableColumns{get;set;}
        @AuraEnabled public List<DataWrapper> dataWrapList{get;set;}
        
        @AuraEnabled public Map<String,String> mapJournalType{get;set;}
        @AuraEnabled public Map<Id,Journal_Entry__c> jeMap{get;set;}
        @AuraEnabled public Map<Id,List<Journal_Dimension__c>> jeIdWithJDMap;
        @AuraEnabled public Set<String> xmlNotSentJournalIdSet{get;set;}
        @AuraEnabled public Decimal totalDebit{get;set;}
        @AuraEnabled public Decimal totalCredit{get;set;}
        @AuraEnabled public Boolean disableSendJournal{get;set;}
        
        //Post Method Attributes
        @AuraEnabled public Boolean isSuccess{get;set;}
        @AuraEnabled public String userEmail{get;set;}
        
        JEWrapper(){
            this.isSuccess = false;
            this.disableSendJournal = false;
            this.xmlNotSentJournalIdSet = new Set<String>();
            this.jeIdWithJDMap = new Map<Id,List<Journal_Dimension__c>>();
            this.jeMap = new Map<Id,Journal_Entry__c>();
            this.dataTableColumns = new List<DataTableColumnsWrapper>();
            this.totalDebit = 0.0;
            this.totalCredit = 0.0; 
            this.dataWrapList = new List<DataWrapper>();
            this.mapJournalType = new Map<String,string>{
                'A'=> '9****'  ,    //***** CMS Journals *****
                'B'=> '9CAS'  ,    //Cash Analysis Journal
                'C'=> '9CLA'  ,    //Claims Interface CMS Journal
                'D'=> '9CMA'  ,    //CMS Accrual Journal
                'Finance Lease'=> '9CMC'  ,    //CMS Standard Journal
                'F'=> '9EXP'  ,    //Expenses System Journal
                'G'=> '9NBV'  ,    //Netbook Value Depreciation Journal
                'H'=> '9OBA'  ,    //Opening Balance Journal
                'I'=> '9REC'  ,    //Security Deposit Journal
                'Aircraft MR' => '9ZZZZ'  ,
                'Assembly MR' => '9ZZZZ'  ,
                'Rent'=> '9ZZZZ'  ,    //CMS Cash Allocation Journal
                //'K'=> 'M****  ,    //-----CMS/AerData Maintenance Reserve Journals-----
                'L'=> 'MINT'  ,    //Maintenance Reserve Interest Journal
                'M'=> 'MOPB'  ,    //Maintenance Reserve Opening Balance Journal
                'N'=> 'MREC'      //Maintenance Reserve Reconciliation Journal
            };
        }
    }
    
    @AuraEnabled
    public static void exportToExcel(String jeWrapStr){
        JEWrapper jeWrap = (JEWrapper)JSON.deserialize(jeWrapStr, JEWrapper.class);
        String strRecs = getCSVHeader('export',jeWrap);
        //System.debug('strRecs: ' + strRecs);

        String strCalPeriodName;
        String strCurRec = '';
        for(Journal_Entry__c curJrnl : jeWrap.jeMap.values()){
            if(strCalPeriodName==null)strCalPeriodName = curJrnl.Calendar_Period__r.Name;
            strCurRec = getCommaSeperatedJE(curJrnl,'export',jeWrap);
            strRecs += (strCurRec +'"\n');
            strCurRec = '';
        }
        String strCSVName= 'Journal Transaction Register ' + strCalPeriodName + '.csv';
        blob csvBlob = Blob.valueOf(strRecs);
        
        ContentVersion file = new ContentVersion(
            title = strCSVName,
            versionData = csvBlob,
            pathOnClient = '/strCSVName'
        );
        
        insert file;
		
        //System.debug('exportToExcel::::');
    }
    
     private static String getCSVHeader(String button,JEWrapper jeWrap){
        /* Defining headers for the excel file */
        Set<String> strHeaderSet = new Set<String>{'ACCOUNTING PERIOD', 'STATUS', 'INVOICE', 'LESSOR','GL Account Description', 'JE DESCRIPTION', 'JOURNAL DATE'};
        strHeaderSet.addAll(new Set<String>{'JOURNAL SOURCE','JOURNAL NUMBER','GL ACCOUNT','LEGAL ENTITY','INTERCOMPANY AFFILIATE', 'DEBIT', 'CREDIT', 'BANK ACCOUNT'});
        
        if(button.equalsIgnoreCase('export')) {
            strHeaderSet.add('Journal Sent Date');
        }
        
        /* Adding headers from the Journal_Dimension__c */
        for(Id idJE : jeWrap.jeMap.keySet()){
            if(jeWrap.jeIdWithJDMap.containsKey(idJE) && jeWrap.jeIdWithJDMap.get(idJE).size() > 0){
                for(Journal_Dimension__c objJD : jeWrap.jeIdWithJDMap.get(idJE)){
                    strHeaderSet.add(objJD.Name.toUpperCase());
                }
            }
        }
        List<String> strHeaderList = new List<String>();
        strHeaderList.addAll(strHeaderSet);
        String strHeader = '';
        strHeader = String.join(strHeaderList,',');
        strHeader+= '\n';
        return strHeader;
    }
    
    private static String getCommaSeperatedJE(Journal_Entry__c curJrnl,String button, JEWrapper jeWrap){
        String strCurRec = '';
        Set<String> dvSet = new Set<String>();
        List<String> dvList = new List<String>();
        if(curJrnl != null){
            strCurRec = '"'+(curJrnl.Calendar_Period__c != null ? curJrnl.Calendar_Period__r.Name : '')+'","'+(curJrnl.Journal_Status__c != null ? curJrnl.Journal_Status__c : '')+'","'+(curJrnl.Invoice__c != null ? curJrnl.Invoice__r.Name : '')+'","'+(curJrnl.Lease__c != null && curJrnl.Lease__r.lessor__c != null ? curJrnl.Lease__r.lessor__r.Name : '')+'","'+curJrnl.Name+'","';
            strCurRec += (curJrnl.JE_Description__c != null ? curJrnl.JE_Description__c : '')+'","'+curJrnl.Journal_Date__c;
            
            strCurRec += '","' + (curJrnl.Transaction_Type__c != null ? curJrnl.Transaction_Type__c : '') + '","' + (curJrnl.Journal_Number__c != null ? curJrnl.Journal_Number__c : '') + '","' + (curJrnl.GL_Account_Code__c != null ? curJrnl.GL_Account_Code__c : '')+ '","'+(curJrnl.Legal_Entity__c != null ? curJrnl.Legal_Entity__c : '') +'","';
            strCurRec += (curJrnl.Intercompany__c != null ? curJrnl.Intercompany__c : '') + '","' + curJrnl.Debit__c + '","' + curJrnl.Credit__c + '","' + (curJrnl.Bank_Account__c != null ? curJrnl.Bank_Account__r.Name : '');
            
            //To maintain the order of columns in .csv file, as displayed on the component
            if(button.equalsIgnoreCase('export')) {
                if(curJrnl.Journal_Sent_Date__c != null)
                    strCurRec +='","'+curJrnl.Journal_Sent_Date__c;
                else
                    strCurRec +='","'+' ';
            }
            
            if(jeWrap.jeIdWithJDMap.containsKey(curJrnl.Id) && jeWrap.jeIdWithJDMap.get(curJrnl.Id).size() > 0){
                for(Journal_Dimension__c objJD : jeWrap.jeIdWithJDMap.get(curJrnl.Id)){
                    dvSet.add(objJD.Dimension_Value__c);
                }
            }
        }
        dvList.addAll(dvSet);
        strCurRec += '","' + String.join(dvList,'","');

        return strCurRec;
    }
    
     @AuraEnabled(cacheable=true)
    public static string getNameSpacePrefix(){
     
       return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled
    public static String Post(String jeWrapStr){
        JEWrapper jeWrap = (JEWrapper)JSON.deserialize(jeWrapStr, JEWrapper.class);
       
            String XML_Header = '<?xml version="1.0" encoding="iso-8859-1"?>\n' ;
            String userName = 'LEA';
            String sourceJournal = 'LW';
            String strJournalOutputType = LeaseWareUtils.getLWSetup_CS('Journal_Output_Format');
            if(strJournalOutputType==null || !'XML'.equals(strJournalOutputType))strJournalOutputType='CSV';
            List<Journal_Entry__c> unsentlistJEs = new  List<Journal_Entry__c>();
            String strRecs='';
            String strCalPeriodName;
            Integer tabCounter=0;
            String strCSVName;
            if('XML'.equals(strJournalOutputType)){     
                Xmlstreamwriter xmlW = new Xmlstreamwriter();
                LeaseWareStreamUtils.startNodeXML(xmlW,'SSC',++tabCounter);
                LeaseWareStreamUtils.startNodeXML(xmlW,'User',++tabCounter);
                ++tabCounter;
                LeaseWareStreamUtils.addNodeXML(xmlW,'Name',userName,tabCounter);
                tabCounter--;
                LeaseWareStreamUtils.endNodeXML(xmlW,'User',tabCounter--);
                LeaseWareStreamUtils.startNodeXML(xmlW,'SunSystemsContext',++tabCounter);
                tabCounter++;
                LeaseWareStreamUtils.addNodeXML(xmlW,'BusinessUnit','ABC',tabCounter);
                LeaseWareStreamUtils.addNodeXML(xmlW,'BudgetCode','A',tabCounter);
                tabCounter--;
                LeaseWareStreamUtils.endNodeXML(xmlW,'SunSystemsContext',tabCounter--);
                LeaseWareStreamUtils.startNodeXML(xmlW,'MethodContext',++tabCounter);
                LeaseWareStreamUtils.endNodeXML(xmlW,'MethodContext',tabCounter--);
                LeaseWareStreamUtils.startNodeXML(xmlW,'Payload',++tabCounter);
                LeaseWareStreamUtils.startNodeXML(xmlW,'Ledger',++tabCounter);
                
                //Iterate over Journal_Entry__c record for which XML_Sent__c == false
                for(String journalId : jeWrap.xmlNotSentJournalIdSet){
                    Journal_entry__c curJrnl = jeWrap.jeMap.get(journalId);
                    if(strCalPeriodName==null)strCalPeriodName = curJrnl.Calendar_Period__r.Name;
                    LeaseWareStreamUtils.startNodeXML(xmlW,'Line',++tabCounter);
                    tabCounter++;
                    LeaseWareStreamUtils.addNodeXML(xmlW,'JournalSource',sourceJournal,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'JournalType',jeWrap.mapJournalType.get(curJrnl.Transaction_Type__c),tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'JournalNumber',curJrnl.Journal_Number__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionDate',LeaseWareUtils.getDatetoString(curJrnl.Journal_Date__c,'DDMMYYYY'),tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AccountingPeriod',curJrnl.Calendar_Period__r.Name,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionReference',curJrnl.Y_Hidden_Trx_Reference__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AccountCode',curJrnl.GL_Account_Code__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'Description',curJrnl.JE_Description__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'DebitCredit',curJrnl.Sign__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'CurrencyCode','USD',tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'TransactionAmount',''+curJrnl.Y_Hidden_Amount__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode1',curJrnl.Legal_Entity__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode2',curJrnl.Invoice__r.Lease__r.Legal_Entity__r.Name,tabCounter);      
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode3',curJrnl.Intercompany__c,tabCounter);  
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode4',curJrnl.Y_Hidden_Serial_Number__c,tabCounter);  
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode5',(''+curJrnl.Lease__c).left(15),tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode6',curJrnl.Lease__r.Lessee__r.Name,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode7',curJrnl.Invoice__r.Name,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode8',curJrnl.Lease__r.Serial_Number__c,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode9',null,tabCounter);
                    LeaseWareStreamUtils.addNodeXML(xmlW,'AnalysisCode10',null,tabCounter);
                    
                    //Iterate over Journal_Dimension__c records under Journal_Entry__c 
                    if(jeWrap.jeIdWithJDMap.containsKey(journalId) && jeWrap.jeIdWithJDMap.get(journalId).size() > 0){
                        for(Journal_Dimension__c objJD : jeWrap.jeIdWithJDMap.get(journalId)){
                            LeaseWareStreamUtils.addNodeXML(xmlW,objJD.Name,objJD.Dimension_Value__c,tabCounter);
                        }
                    }
                    
                    tabCounter--;                        
                    LeaseWareStreamUtils.endNodeXML(xmlW,'Line',tabCounter--);
                    curJrnl.XML_Sent__c = true;
                    curJrnl.Journal_Sent_Date__c = date.today();
                    unsentlistJEs.add(curJrnl);
                }
                LeaseWareStreamUtils.endNodeXML(xmlW,'Ledger',tabCounter--);
                LeaseWareStreamUtils.endNodeXML(xmlW,'Payload',tabCounter--);          
                LeaseWareStreamUtils.endNodeXML(xmlW,'SSC',tabCounter--);
                //System.debug('strRecs:' + XML_Header + xmlW.getXmlString());
                strRecs=XML_Header + xmlW.getXmlString();
                strCSVName= 'Journal Transaction Register ' + strCalPeriodName + '.xml';
                
            }else{
                strRecs = getCSVHeader('email', jeWrap);
                String strCurRec = '';
                for(String journalId : jeWrap.xmlNotSentJournalIdSet){
                    Journal_entry__c curJrnl = jeWrap.jeMap.get(journalId);
                    if(strCalPeriodName==null)strCalPeriodName = curJrnl.Calendar_Period__r.Name;
                    strCurRec = getCommaSeperatedJE(curJrnl,'email',jeWrap);
                    strRecs += (strCurRec +'"\n');
                    strCurRec = '';
                    curJrnl.XML_Sent__c = true;
                    curJrnl.Journal_Sent_Date__c = date.today();
                    unsentlistJEs.add(curJrnl);
                }
                strCSVName= 'Journal Transaction Register ' + strCalPeriodName + '.csv';
            }
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(strRecs);
            csvAttc.setFileName(strCSVName);
            csvAttc.setBody(csvBlob);
            //System.debug('csvAttc: ' + csvAttc);
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String subject ='Journals for ' + strCalPeriodName;
            email.setSubject(subject);
            email.setToAddresses(new List<String>{UserInfo.getUserEmail()}); 
            email.setPlainTextBody(subject);
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
            Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
            jeWrap.isSuccess = result[0].isSuccess();
            jeWrap.userEmail = UserInfo.getUserEmail();
           // System.debug('csvAttc: ' + csvAttc);
           // System.debug('result: ' + result);
            update unsentlistJEs;
      

        return JSON.serialize(jeWrap);
    }
    
   public class FilterOptionsWrapper{
        @AuraEnabled public List<OptionWrapper> startDateOptions{get;set;}
        @AuraEnabled public List<OptionWrapper> endDateOptions{get;set;}
        @AuraEnabled public List<OptionWrapper> statusOptions{get;set;}
        @AuraEnabled public List<OptionWrapper> journalSourceOptions{get;set;}
        @AuraEnabled public String listViewId{get;set;}
        FilterOptionsWrapper(){
            this.listViewId = 'Recent';
            this.startDateOptions = new List<OptionWrapper>();
            this.endDateOptions = new List<OptionWrapper>();
            this.statusOptions = new List<OptionWrapper>();
            this.journalSourceOptions = new List<OptionWrapper>(); 
        }
    }
    
    public class OptionWrapper{
        @AuraEnabled public String label{get;set;}
        @AuraEnabled public String value{get;set;}
        
        public OptionWrapper(String label, String value){
            this.label = label;
            this.value = value;
    }
}

    //to hold the filters applied by user on UI.
    public class FilterWrapper{
        @AuraEnabled public String fieldApiName{get;set;}
        @AuraEnabled public String value{get;set;}
        //@AuraEnabled public String operator{get;set;} not needed now but can be used in future.
    }
    
}