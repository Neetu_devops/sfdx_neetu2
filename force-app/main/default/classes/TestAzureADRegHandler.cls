/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAzureADRegHandler {

	static testMethod void testCreateAndUpdateUser() {
	    AzureAD_RegHandler  handler = new AzureAD_RegHandler ();
	    Auth.UserData sampleData = new Auth.UserData('idSomeAzureID', 'testFirst', 'testLast',
	        'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'en_US', 'facebook',
	        null, new Map<String, String>{'language' => 'en_US'});
	    User u = handler.createUser(null, sampleData);
	    
	    u = new User(
	     AzureId__c = 'idSomeAzureID',
	     ProfileId = [SELECT Id FROM Profile where name like 'Lessor%' limit 1].Id,
	     LastName = 'last',
	     Email = 'user01@example.com',
	     Username = 'user01@example.com' + System.currentTimeMillis(),
	     CompanyName = 'TEST',
	     Title = 'title',
	     Alias = 'alias',
	     TimeZoneSidKey = 'America/Los_Angeles',
	     EmailEncodingKey = 'UTF-8',
	     LanguageLocaleKey = 'en_US',
	     LocaleSidKey = 'en_US'	    
	    );
	    
	    insert u;
	    u = handler.createUser(null, sampleData);

	    handler.updateUser(u==null?null:u.id, null, sampleData);
	    
	}


}