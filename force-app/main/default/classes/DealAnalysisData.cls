public with sharing class DealAnalysisData {
    public class RecordInfo implements DealAnalysisDataInterface{
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public decimal Rent {get; set;}
        @AuraEnabled public decimal PurchasePrice {get; set;}
        @AuraEnabled public boolean IsLowest {get; set;}
        @AuraEnabled public boolean IsHighest {get; set;}
        @AuraEnabled public String AircraftTypeVariant {get; set;}
        @AuraEnabled public String OperatorName {get; set;}
        @AuraEnabled public String OperatorId {get; set;}
        @AuraEnabled public Decimal Term {get; set;}
    }
    public class Data implements DealAnalysisDataInterface{
        
        public Data(){
            Recs = new list<RecordInfo>();
        }
        
        @AuraEnabled public String Heading {get; set;}
        @AuraEnabled public String Source {get; set;}
        @AuraEnabled public decimal Lowest {get; set;}
        @AuraEnabled public decimal LowSD {get; set;}
        @AuraEnabled public decimal Mean{ get; set;}
        @AuraEnabled public decimal HighSD {get; set;}
        @AuraEnabled public decimal Highest{ get; set;}
        @AuraEnabled public list<RecordInfo> Recs{ get; set;}
         
    }
        
}