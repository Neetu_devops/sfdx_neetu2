public class AccountingSetupTriggerHandler implements ITrigger {
    
    private final string triggerBefore = 'AccountingSetupTriggerHandlerBefore';
    private final string triggerAfter = 'AccountingSetupTriggerHandlerAfter';
    public AccountingSetupTriggerHandler(){}
    
    public void bulkBefore(){}
     
   
    public void bulkAfter(){}
     
    public void beforeInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('AccountingSetupTriggerHandler.beforeInsert(+)');
        list<parent_Accounting_setup__c> accSetuplist = ( list<parent_Accounting_setup__c>)trigger.new;
        Boolean toUpdate = false;
        Parent_Accounting_setup__c accSetup = null;
            
        list<parent_accounting_setup__c> accList = [select id from parent_accounting_setup__c where default__c =true limit 1];
        if(accList.size()>0){
            accSetup = accList.get(0);
        }
        system.debug('accSetup:::'+accSetup);
            for(Parent_Accounting_setup__c acc :accSetuplist){
                if(accSetup == null){
                    acc.default__c = true;
                }
                else if(accSetup!=null && acc.Default__c==true){
                    toUpdate = true;
                    accSetup.Default__c = false;
                }
               
                
            }
      		if(toupdate) update accSetup; 
       		
        	system.debug('AccountingSetupTriggerHandler.beforeInsert(-)');
    }
     
    
    public void beforeUpdate(){ 
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('AccountingSetupTriggerHandler.beforeUpdate(+)');
        list<Parent_Accounting_setup__c> accSetuplist = ( list<Parent_Accounting_setup__c>)trigger.new;
        Boolean toUpdate = false;
        Parent_Accounting_setup__c accSetup = null;
        list<Parent_accounting_setup__c> accList = [select id from Parent_accounting_setup__c where default__c =true limit 1];
        if(accList.size()>0){
            accSetup = accList.get(0);
            for(Parent_Accounting_setup__c acc :accSetuplist){
                if(accSetup.id !=acc.id && accSetup!=null && acc.Default__c==true){
                    toUpdate = true;
                    accSetup.Default__c = false;
                    
                }
                else if(accSetup.id == acc.id && acc.Default__c==false){
                    acc.addError('Please select another default accounting setup');
                    return;
                }
            }
        }
      		if(toupdate) update accSetup;         
        system.debug('AccountingSetupTriggerHandler.beforeUpdate(-)');
        
    }
 
    public void beforeDelete(){
       system.debug('AccountingSetupTriggerHandler.beforeDelete(+)');
        list<Parent_Accounting_setup__c> accSetuplist = ( list<Parent_Accounting_setup__c>)trigger.old;
        system.debug('AccountingSetupTriggerHandler.beforeDelete(-)');
        for(Parent_Accounting_setup__c acc :accSetuplist){
                if(acc.Default__c==true){
                    acc.addError('Please select another default accounting setup before deleting');
                    return;
                    
                }
            }
    }
 
    public void afterInsert(){
        system.debug('AccountingsetupTriggerHAndler.afterInsert(++++)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
       	list<Accounting_setup__c> subaccList = new list<Accounting_setup__c>();
        list<gl_account_code__c> glList = new list<gl_account_code__c>();
        list<Parent_Accounting_setup__c> accSetuplist = ( list<Parent_Accounting_setup__c>)trigger.new;
        for(Parent_Accounting_setup__c accSetup : accSetuplist){
            Accounting_setup__c BrntDrev = new Accounting_setup__c(name ='Base Rent/Deferred Revenue');
            BrntDrev.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(BrntDrev);
            
            Accounting_setup__c strLine = new Accounting_setup__c(name ='Straight Line');
            strLine.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(strLine);
            
            Accounting_setup__c hlSlR = new Accounting_setup__c(name ='Head Lease Sub Lease Rent');
            hlSlR.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(hlSlR);
            
            Accounting_setup__c Mx = new Accounting_setup__c(name ='MX Invoices');
            Mx.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(Mx);
            
            Accounting_setup__c LateInv = new Accounting_setup__c(name ='Late Interest');
            LateInv.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(LateInv);
            
            Accounting_setup__c hlSlMr = new Accounting_setup__c(name ='Head Lease Sub Lease Mx');
            hlSlMr.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(hlSlMr);
            
            Accounting_setup__c payments = new Accounting_setup__c(name ='Customer Payments');
            payments.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(payments);

            Accounting_setup__c sd = new Accounting_setup__c(name ='Security Deposit');
            sd.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(sd);

            Accounting_setup__c creditMemo = new Accounting_setup__c(name ='Credit Memo');
            creditMemo.Parent_Accounting_Setup__c = accSetup.id;
            subaccList.add(creditMemo);
        }
        try{
            insert subaccList;
        }
        catch(DmlException ex){
            string errorMessage='';
            for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                // Process exception here
                errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
            }
            system.debug('errorMessage=='+errorMessage);
            for(Parent_Accounting_setup__c accSetup : accSetuplist){
                accSetup.addError(errorMessage);
            }
        }
        //insert glList;
        
        system.debug('AccountingsetupTriggerHAndler.afterInsert(---)');
        
    }
 
    public void afterUpdate(){}
 
    
    public void afterDelete(){}
    
    
    public void afterUnDelete(){}    
 
    public void andFinally(){}

}