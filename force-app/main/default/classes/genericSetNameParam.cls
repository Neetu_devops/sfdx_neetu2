public class genericSetNameParam {

    private sObject curObj;
    
    public genericSetNameParam(ApexPages.StandardController curRec) {
        this.curObj = curRec.getRecord();
    }

    public PageReference url() {
        PageReference p = new PageReference('/' + curObj.getSObjectType().getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        m.putAll(ApexPages.currentPage().getParameters());
        m.remove('save_new');
        m.put('Name', '<SYSTEM GENERATED>');
        m.put('nooverride', '1');
        return p;
    }

}