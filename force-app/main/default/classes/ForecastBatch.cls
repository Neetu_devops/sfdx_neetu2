/*
    This batch class generates the component outputs and the monthly scenarios for a given
    forecast id. The monthly scenario generation happens MONTH wise(one month after the other).
    This helps in merging the events and determining the utilization of the assembly as a whole.
    
    This batch also creates a process request log that shows the status of the job.
    It can have the following statuses:
    PROCESSING - Once the job is submitted.
    SUCCESS - After successful execution of both the batches.
    WARNING - If there are any setup related issues, log is updated with appropriate error/warning messages.
    FAIL - If there are any issues during forecast generation, log is updated with error message.

    Start - Creates the component outputs. Decides the number of batches to run
    Execute - For every batch, Create Monthly scenarios for the given start and end dates range
    Finish - If everything goes fine, Invoke the second batch else send a failure email.
*/
public class ForecastBatch implements Database.batchable <Integer>, Database.Stateful { 
    
    private Process_Request_Log__c ReqLog;
    private map<string,list<string>> errorWarnMap;

    string forecastId;
    Integer MaxNumOfRecsInBatch;
    Integer numOfMonthsInABatch;
    Integer currBatchNumber;
    Date startMonth, endMonth;
    ScenarioHelper.PrevMonthScenarioWrapper prevMonthOutputs;
    
    public ForecastBatch(string forecastId, Process_Request_Log__c ReqLog)
    {
        this.forecastId = forecastId;
        /* Assuming a max of 30yrs lease period, approx 150 component outputs,
           the monthly scenario count might cross 50k.Governer limit is 10K for DMLs per batch.
           We have restricted to batch to run for every 5000 records.
        */
        MaxNumOfRecsInBatch = 2000; 
        currBatchNumber = 1;
        errorWarnMap = new map<string,list<string>>();
        this.ReqLog = ReqLog;
    }
        
    public Iterable<Integer> start(Database.BatchableContext bc) 
    {
        List<Integer> lstNumOfBatches = new List<Integer>();
        
        try
        {   
            //Set the forecast status to "In Progress"
            Scenario_Input__c scenarioInpRec = [Select Id, Forecast_Status__c from Scenario_Input__c where id =: forecastId];
            scenarioInpRec.Forecast_Status__c = 'In Progress';
            update scenarioInpRec;

            //Create Scenario_Component_Output for General, all events(for all assemblies).
            errorWarnMap =  ScenarioHelper.CreateComponentOutputs(forecastId);

            //Get number of months
            Date startDate = ScenarioHelper.MonthlyScenarioCommonInputs.StartDate;
            Date endDate = ScenarioHelper.MonthlyScenarioCommonInputs.DateToProject;
            Integer numOfMonths = startDate.monthsBetween(endDate) + 1;
            
            //Compute total number of component outputs, monthly scenarios and number of batches
            Integer totalNumOfOutputs = ScenarioHelper.MonthlyScenarioCommonInputs.NumOfCompOutputs + ScenarioHelper.MonthlyScenarioCommonInputs.NumOfLLPCompOutputs; 
            Integer totalNumOfRecs = totalNumOfOutputs * numOfMonths;
            Integer numOfBatches = Integer.valueOf(Math.ceil(totalNumOfRecs/Decimal.valueof(MaxNumOfRecsInBatch)));

            numOfMonthsInABatch = Integer.valueOf(Math.ceil(numOfMonths/Decimal.valueof(numOfBatches)));
            startMonth = startDate;
            endMonth = startMonth.addMonths(numOfMonthsInABatch - 1); 
            if(endMonth > ScenarioHelper.MonthlyScenarioCommonInputs.DateToProject)
                endMonth = ScenarioHelper.MonthlyScenarioCommonInputs.DateToProject;
            
            //Get the prev month scenario outputs(initialized to null)
            this.prevMonthOutputs = ScenarioHelper.PrevMonthScenarioOutputs;
            
            System.debug('BATCH - TOTAL NUM OF OUTPUTS = ' + ScenarioHelper.MonthlyScenarioCommonInputs.NumOfCompOutputs +',' + ScenarioHelper.MonthlyScenarioCommonInputs.NumOfLLPCompOutputs +',' + totalNumOfOutputs);
            System.debug('BATCH - TOTAL NUM OF MONTHS = ' + numOfMonths);
            System.debug('BATCH - TOTAL NUM OF RECORDS =' + totalNumOfRecs);
            System.debug('BATCH - NUM OF BATCHES =' + Math.ceil(totalNumOfRecs/MaxNumOfRecsInBatch) + ',' + numOfBatches);
            System.debug('BATCH - NUM OF MONTHS IN A BATCH = ' + numOfMonthsInABatch);
    
            for(integer j=1;j<=numOfBatches;j++){
        
                lstNumOfBatches.add(j);
            }
        }
        catch(Exception ex)
        {   
            ForecastBatch2.updateRequestLog(ReqLog,'FAIL', LeaseWareUtils.cutString( ex.getMessage() , 0, 254 )
                    , 'Failed to create component outputs :' + ex.getMessage() + ex.getStackTraceString());	                                                
        }
        
        if(errorWarnMap.size() > 0 || Test.isRunningTest())
        {
            ForecastBatch2.updateRequestLog(ReqLog,'WARNING', 'Errors while creating component outputs.', 'Errors while creating component outputs.');	                                                                                    
        }
        
        return lstNumOfBatches;        
    }
    
   public void execute(Database.batchableContext info, List<Integer> scope)
   {  
        try
        {     
            //Create Monthly scenarios for each batch
            System.debug('BATCH NUMBER - ' + currBatchNumber++);
            System.debug('START MONTH - ' + startMonth);
            System.debug('END MONTH - ' + endMonth);
            System.debug('PREV MONTH OUTPUTS - ' + this.prevMonthOutputs);
            System.debug('HEAP MEMORY AT START OF THE BATCH - ' + Limits.getHeapSize());

            ScenarioHelper.GenerateMonthlyForecast(this.forecastId, this.prevMonthOutputs, startMonth, endMonth);

            //Save the prev month output
            this.prevMonthOutputs = ScenarioHelper.PrevMonthScenarioOutputs;

            //Reset start and end dates for the next batch of records
            startMonth = endMonth.toStartOfMonth().addmonths(1);
            endMonth = startMonth.addMonths(numOfMonthsInABatch - 1);
            if(endMonth > ScenarioHelper.MonthlyScenarioCommonInputs.DateToProject)
                endMonth = ScenarioHelper.MonthlyScenarioCommonInputs.DateToProject;

            System.debug('HEAP MEMORY AT END OF THE BATCH - ' + Limits.getHeapSize());
        }
        catch(Exception ex)
        {   
            //create exception log
            ForecastBatch2.updateRequestLog(ReqLog,'FAIL', LeaseWareUtils.cutString( ex.getMessage() , 0, 254 )
                    , 'Error while creating monthly scenarios for batch number :' + (currBatchNumber-1) + 'Start Date = ' + startMonth + ',' + 'End Date = ' + endMonth + ex.getMessage() + ex.getStackTraceString());	                                                       
        }
            
      }

   public void finish(Database.batchableContext info)
   {   
       if('PROCESSING'.equals(ReqLog.status__c))
       {
            //Invoke second batch 
            Id secondBatchID = Database.executeBatch(new ForecastBatch2(this.forecastId, ReqLog),1);

       }
       else //WARNING OR FAIL
       {    
            //Set the status of the forecast to "Draft"
            Scenario_Input__c scenarioInpRec = [Select Id, Forecast_Status__c from Scenario_Input__c where id =: forecastId];
            scenarioInpRec.Forecast_Status__c = 'Draft';
            update scenarioInpRec;

            string outputStr = ' Status(Error/Warning) , Message ' + '\n';                
            if('WARNING'.equals(ReqLog.status__c))
            {
                for(string errKey : errorWarnMap.keyset())
                {
                    for(string errMsg: errorWarnMap.get(errKey))
                        outputStr += errKey + ',"' +errMsg +  '\n';					
                }                        
            }
            else //In case of FAILURE, dump exception message into ResultsLog.csv
            {                
                outputStr += 'Error' + ',"' + ReqLog.Exception_Message__c +  '\n';					                
            }

            list<Document> documents = ForecastBatch2.CreateAttachment(outputstr, ReqLog);
            ForecastBatch2.SendEmail(forecastId, false, ReqLog, documents);
        }    
        if(Test.isRunningTest()) //code coverage
        {   
            ReqLog.status__c = 'PROCESSING';
            update ReqLog;
            Id secondBatchID = Database.executeBatch(new ForecastBatch2(this.forecastId, ReqLog),1);
        }        
   }

    /******************************************************************************************
        Description: This method creates a process request log and invokes the batch class that
                     generates the forecast output. Returns a message that indicates the process
                     request that is in progress. 
        Inputs: forecastId - Forecast id                
    ********************************************************************************************/
    public static string GenerateOutput(string forecastId)
    {   
        if(forecastId == null) return 'Please provide a forecast Id.';

        string strMessage = '';        

        List<Process_Request_Log__c> lstProcessReqLogs = new List<Process_Request_Log__c>();
        for(Process_Request_Log__c log : [select Name, Parameters__c, Status__c from Process_Request_Log__c where Type__c = 'FORECAST' and Status__c = 'PROCESSING'])
        {
            if(log.Parameters__c.contains(forecastId))
            {
                lstProcessReqLogs.add(log);
            }
        }

        //Check if there a process request log for the forecast id that is in "PROCESSING" stage
        if(lstProcessReqLogs.size() > 0)
        {   
            //This implies the batch is still running. Return error message.
            strMessage += 'There is a running job for the given forecast. Cannot create a new job.' + '\n';
            strMessage += 'Please check the status of the process ' + lstProcessReqLogs[0].Name;            
        }
        else
        {   
            //No running process request. Create a process request log
            string strSessionId = string.valueOf(datetime.now());
            Process_Request_Log__c ReqLog = new Process_Request_Log__c(Status__c ='PROCESSING', Type__c = 'FORECAST', Parameters__c = forecastId, Comments__c = '', Exception_Message__c = ''); 
            insert ReqLog;

            List<Process_Request_Log__c> ReqLogs = [select Name from Process_Request_Log__c where id = :ReqLog.id];            

            //Invoke the batch. Return message with process request log id.            
            Id batchId = Database.executeBatch(new ForecastBatch(forecastId, ReqLog),1);
            strMessage += 'Forecast Job submitted. You will receive an email when the job completes.' + '\n';
            strMessage += 'You can check the status of the process - ' + ReqLogs[0].Name;
        }

        return strMessage;
    }
}


