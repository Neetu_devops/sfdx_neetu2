public without sharing class AssetReviewItemController {
	
    @AuraEnabled
    public static List<SectionObj> getSections(String recordType, String recordId){
        try{
            AggregateResult[] groupedResults = [select Item_Zone__c zone from Asset_Review_Items__c
                                                where RecordType.Name = :recordType and
                                                Asset_Review__c =: recordId
                                                group by Item_Zone__c
                                                            ];
            List<String> sectionList = new List<String>();
            for(AggregateResult obj: groupedResults)
                sectionList.add(obj.get('zone').toString());
            system.debug('getSections query result '+sectionList);
            return getReviewStatus(sectionList, recordId, recordType);
        }
        catch(Exception e) { system.debug('Exception no items '+e);}
        return null;
    }  
    
    @AuraEnabled
    public static string getNamespacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled
    public static List<SectionObj> getReviewStatus(List<String> sections, String recordId, String recordType){
        system.debug('getReviewStatus '+sections);
        try{
            AggregateResult[] groupedResults = [select count(Id) zoneCount , Item_Zone__c zone from Asset_Review_Items__c
                                                where RecordType.Name = :recordType and
                                                Asset_Review__c =: recordId
                                                group by Item_Zone__c 
                                               ];
            Map<String, Integer> sectionReviewList = new Map<String, Integer>();
            for(AggregateResult obj: groupedResults){
                sectionReviewList.put(obj.get('zone').toString(), Integer.valueOf(obj.get('zoneCount')));
            }
            
            system.debug('getReviewStatus query total sectionReviewList '+sectionReviewList);
            
            AggregateResult[] groupedResultsClosed = [select count(Id) zoneCount , Item_Zone__c zone 
                                                      from Asset_Review_Items__c
                                                      where RecordType.Name = :recordType and
                                                      Asset_Review__c =: recordId and 
                                                      Review_Item_Status__c = 'COMPLETED' 
                                                      group by Item_Zone__c 
                                                     ];
            System.debug('getReviewStatus size: '+groupedResultsClosed.size());
            Map<String, Integer> closedCountList = new Map<String, Integer>();
            for(AggregateResult obj: groupedResultsClosed) {
                closedCountList.put(obj.get('zone').toString(), Integer.valueOf(obj.get('zoneCount')));
            }
            List<SectionObj> secList = new List<SectionObj>();
            for ( String zone : sectionReviewList.keySet() ){
                integer totalCount = sectionReviewList.get(zone);
                integer closedCount = 0;
                if(closedCountList.get(zone) != null)
                	closedCount = closedCountList.get(zone);
                
                integer percentage = (closedCount * 100) / totalCount;
                
                SectionObj sec = new SectionObj();
                sec.name = zone;
                sec.reviewStatus = percentage;
                secList.add(sec);
                
            }
            system.debug('getReviewStatus query percentage secList '+secList);
            return secList;
        }
        catch(Exception e){
            System.debug('getReviewStatus Exception:'+e);
        }
        return null;
    }
    
    
    @AuraEnabled
    public static List<FieldStructure> getColumns(String objectName, String fieldSetName){
        //objectName = getNamespacePrefix() + objectName;
        System.debug('getColumns fieldSetName ' + fieldSetName +  ' objectName:'+objectName);
        if(String.isBlank(fieldSetName)) {
            return getDefaultFieldSet();
        }
        else {
            List<FieldStructure> fieldList = getFieldSets(fieldSetName,objectName);
            if(fieldList == null)
                return getDefaultFieldSet();
            return fieldList;
        }
    }
    
    public static List<FieldStructure> getDefaultFieldSet(){
        List<FieldStructure> fieldList = new List<FieldStructure>();
        FieldStructure field = new FieldStructure();
        field.label = 'Name';
        field.apiName = 'Name';
        field.fieldType = 'String';
        fieldList.add(field);
        return fieldList;
    }
    
    
    @AuraEnabled
    public static List<SObject> getRows(String objectName, String mappingCond, String recordId, String fieldSetName, 
                                        String parentReviewType, String sectionField, String section){
        System.debug('getRows fieldSetName : ' + fieldSetName+' section '+section);
        String objectNameStr = objectName;
                                            
		objectName = getNamespacePrefix() + objectName;
		mappingCond = getNamespacePrefix() + mappingCond;	                                            
		sectionField = getNamespacePrefix() + sectionField;                                        
                                            
        String queryStr = 'select ';
        Integer i = 0;
        List<FieldStructure> columns = getColumns(objectNameStr,fieldSetName);
        for(FieldStructure field : columns) {
            if(i != 0)
                queryStr = queryStr + ',' ;
            queryStr = queryStr + ' ' + field.apiName;
            i = 1;
        }
        queryStr = queryStr + ' from '+ objectName + ' where ' + mappingCond +' = :recordId ';
        if(!String.isEmpty(parentReviewType))
        	queryStr = queryStr + ' and RecordType.Name = :parentReviewType'; 
        if(!String.isEmpty(section)) 
            queryStr = queryStr + ' and '+sectionField+' = :section';
        
        system.debug('getRows queryStr '+queryStr);
                                            
		String namespace = getNamespacePrefix();
        queryStr = queryStr.replace(namespace + namespace , namespace);
                                            
        List<SObject> sObjList = Database.query(queryStr);
        system.debug('getRows query result '+sObjList);
        return sObjList;
    }
    
    @AuraEnabled
    public static List<SObject> getRowsBasedOnStatus(String objectName, String mappingCond, String recordId, String fieldSetName, 
                                                     String status, String statusCond){
        System.debug('getRowsBasedOnStatus fieldSetName ' + fieldSetName);
        String objectNameStr = objectName;

		objectName = getNamespacePrefix() + objectName;
		mappingCond = getNamespacePrefix() + mappingCond;	                                            
		statusCond = getNamespacePrefix() + statusCond;                                                              
                                                         
        String queryStr = 'select ';
        Integer i = 0;
        List<FieldStructure> columns = getColumns(objectNameStr,fieldSetName);
        for(FieldStructure field : columns) {
            if(i != 0)
                queryStr = queryStr + ',' ;
            queryStr = queryStr + ' ' + field.apiName;
            i = 1;
        }
        queryStr = queryStr + ' from '+ objectName + ' where ' + mappingCond +' = :recordId and '+statusCond+' =: status ';
        
        system.debug('getRows queryStr '+queryStr);
		String namespace = getNamespacePrefix();
        queryStr = queryStr.replace(namespace + namespace , namespace);                                                         
                                                         
        List<SObject> sObjList = Database.query(queryStr);
        system.debug('getRows query result '+sObjList);
        return sObjList;
    }
    
    @AuraEnabled
    public static List<SObject> getRecordDetails(String objectName, String recordId, String fieldSetName){
        System.debug('getRecordDetails fieldSetName ' + fieldSetName);
        String objectNameStr = objectName;
        objectName = getNamespacePrefix() + objectName;
        
        String queryStr = 'select ';
        Integer i = 0;
        List<FieldStructure> columns = getColumns(objectNameStr,fieldSetName);
        for(FieldStructure field : columns) {
            if(i != 0)
                queryStr = queryStr + ',' ;
            queryStr = queryStr + ' ' + field.apiName;
            i = 1;
        }
        queryStr = queryStr + ' from '+ objectName + ' where Id = :recordId ';
        
        system.debug('getRows queryStr '+queryStr);
        
        String namespace = getNamespacePrefix();
        queryStr = queryStr.replace(namespace + namespace , namespace);
        List<SObject> sObjList = Database.query(queryStr);
        system.debug('getRows query result '+sObjList);
        return sObjList;
    }
    
    
    public static List<FieldStructure> getFieldSets(String fieldSetName, String objectName) {
        objectName = getNamespacePrefix() + objectName;
        String namespace = getNamespacePrefix();
        objectName = objectName.replace(namespace + namespace , namespace);
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);
        System.debug('getFieldSets '+targetType);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        system.debug('--fieldSetName--'+fieldSetName);
        
        //Since fieldset is packaged we are adding namespace to it.
        if(!String.isBlank(namespace)) {
        	fieldSetName = namespace + fieldSetName;
            fieldSetName = fieldSetName.replace(namespace + namespace , namespace);
        }
        
        // this line has been added to work in developer instance.
        //if(''.equals(getNamespacePrefix())) fieldSetName = fieldSetName.remove('leaseworks__');
        
        Schema.FieldSet fieldSetObj = describe.fieldSets.getMap().get(fieldSetName);
        system.debug('--fieldSetName--'+fieldSetName);
        if(fieldSetObj == null)
            return null;
        List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields();
        List<FieldStructure> fieldList = new List<FieldStructure>();
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
        {
            FieldStructure field = new FieldStructure();
            field.apiName = fieldSetMemberObj.getFieldPath();
            field.label = fieldSetMemberObj.getLabel();
            field.fieldType = String.valueOf(fieldSetMemberObj.getType()).toLowerCase();
            fieldList.add(field);
            //system.debug('Required ====>' + fieldSetMemberObj.getRequired());
            //system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
            system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
        }
        system.debug('getFieldSetNames fieldList' + fieldList); //api name
        return fieldList;
    }
    
    
    public class SectionData {
        @AuraEnabled public List<SObject> objList;
        @AuraEnabled public List<FieldStructure> objColumn;
        @AuraEnabled public String sectionLabel;
        @AuraEnabled public Integer totalCount;
        @AuraEnabled public Integer closedCount;
    }
    
    public class FieldStructure{
        @AuraEnabled public String apiName;
        @AuraEnabled public String label;
        @AuraEnabled public Object value;
        @AuraEnabled public String fieldType;
    }
    
    public class ObjectData{
        @AuraEnabled public String name;
        @AuraEnabled public String id;
        @AuraEnabled public List<FieldStructure> fieldList;
    }
    
    public class SectionObj{
        @AuraEnabled public String name;
        @AuraEnabled public Integer reviewStatus;
    }
    
    
}