public with sharing class AccountingCustomTabLWCController {

    @AuraEnabled(cacheable=true)
    public static AccountingSetupWrapper getData(){
        List<Parent_Accounting_Setup__c> accountingSetupList = new List<Parent_Accounting_Setup__c>([SELECT Id FROM Parent_Accounting_Setup__c]);
        
        AccountingSetupWrapper wrapper = new AccountingSetupWrapper();
        wrapper.namespace = LeaseWareUtils.getNamespacePrefix();
        if(accountingSetupList.size() == 1){
            wrapper.loadRecordPage = true;
            wrapper.accountingSetupId = accountingSetupList[0].Id;
        }
        System.debug('wrapper: ' + wrapper);
        
        return wrapper;
    }
    
    public class AccountingSetupWrapper{
        @AuraEnabled public String namespace {get; set;}
        @AuraEnabled public Boolean loadRecordPage {get; set;}
        @AuraEnabled public String accountingSetupId {get; set;}

        public AccountingSetupWrapper(){
            this.loadRecordPage = false;
        }
    }
}