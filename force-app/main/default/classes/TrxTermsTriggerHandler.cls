public class TrxTermsTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'TrxTermsTriggerHandlerBefore';
    private final string triggerAfter = 'TrxTermsTriggerHandlerAfter';
    public TrxTermsTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('TrxTermsTriggerHandler.beforeInsert(+)');


		PreferProspect();

        system.debug('TrxTermsTriggerHandler.beforeInsert(-)');       
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('TrxTermsTriggerHandler.beforeUpdate(+)');
        
        PreferProspect();

        system.debug('TrxTermsTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('TrxTermsTriggerHandler.beforeDelete(+)');
        TxnTermsRefreshProspectSummary();
        
        system.debug('TrxTermsTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxTermsTriggerHandler.afterInsert(+)');
		TxnTermsRefreshProspectSummary();
        system.debug('TrxTermsTriggerHandler.afterInsert(-)');        
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxTermsTriggerHandler.afterUpdate(+)');
		TxnTermsRefreshProspectSummary();
        system.debug('TrxTermsTriggerHandler.afterUpdate(-)');        
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxTermsTriggerHandler.afterDelete(+)');
        
        
        system.debug('TrxTermsTriggerHandler.afterDelete(-)');        
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('TrxTermsTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('TrxTermsTriggerHandler.afterUnDelete(-)');      
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }
    
    // bef ins, bef upd
    private void PreferProspect(){
    	
	    system.debug('PreferProspect+');
	    list<Transaction_Terms__c> newList = trigger.new; 

        /* Mark other Preferred Prospect to false*/
        
		set<Id> AITIdSet = new set<Id>(); 
        for(Transaction_Terms__c curRec:newList){
            if(trigger.IsInsert && curRec.Preferred_Prospect__c){
                if(AITIdSet.contains(curRec.Aircraft_In_Transaction__c))curRec.Preferred_Prospect__c.addError('You can mark only one primary timeline for deal.');
                AITIdSet.add(curRec.Aircraft_In_Transaction__c);              
            }
            else if(trigger.IsUpdate && curRec.Preferred_Prospect__c && !((Transaction_Terms__c)trigger.oldMap.get(curRec.Id)).Preferred_Prospect__c){
                if(AITIdSet.contains(curRec.Aircraft_In_Transaction__c))curRec.Preferred_Prospect__c.addError('You can mark only one primary timeline for deal.');
                AITIdSet.add(curRec.Aircraft_In_Transaction__c);              
            }
        }		    
	    
	    
	    system.debug('PreferProspect-');   	
    	
    }
    
    
    //after Insert, after Update, before Delete
 	private void TxnTermsRefreshProspectSummary(){

	    
	    system.debug('TxnTermsRefreshProspectSummary+');
	    list<Transaction_Terms__c> listTxnTerms = trigger.isDelete?trigger.old:trigger.new;
	    set<Id> setProspects = new set<Id>();
	    set<Id> setAITIds = new set<Id>();
	    Transaction_Terms__c oldRec;
	    
		set<Id> setAITIdsForOnePreferredProspect = new set<Id>();    
	    for(Transaction_Terms__c curTxnTerm : listTxnTerms){
	    	
	        setAITIds.add(curTxnTerm.Aircraft_In_Transaction__c);
	        if(curTxnTerm.Transaction_Prospect__c != null)setProspects.add(curTxnTerm.Transaction_Prospect__c);
			if(curTxnTerm.Preferred_Prospect__c){
	        	setAITIdsForOnePreferredProspect.add(curTxnTerm.Aircraft_In_Transaction__c);
	        }			
	        if(trigger.isUpdate){
	        	
	            oldRec = (Transaction_Terms__c)trigger.oldmap.get(curTxnTerm.Id);
	            if( oldRec.Transaction_Prospect__c != null && (curTxnTerm.Transaction_Prospect__c == null || oldRec.Transaction_Prospect__c != curTxnTerm.Transaction_Prospect__c)){
	                setProspects.add(oldRec.Transaction_Prospect__c);
	            }
	        }
	    }
	
		system.debug('setAITIdsForOnePreferredProspect='+setAITIdsForOnePreferredProspect);
	    // set max Bidder name
	    Aircraft_In_Transaction__c[] listAITs = [select id 
	                                                            ,(select name,id,Bid_M__c,Transaction_Prospect__r.Y_Hidden_Prospect_Name__c
	                                                            	,Preferred_Prospect__c
	                                                                from Transaction_Terms__r )
	                                                        from Aircraft_In_Transaction__c
	                                                        where id in :setAITIds];
	    system.debug('listAITs='+listAITs.size());
	    decimal maxBidAmt=-999999; 
	    list<Transaction_Terms__c> updTrxterm = new  list<Transaction_Terms__c>();                                                
	    for(Aircraft_In_Transaction__c curAIT:listAITs){
	        maxBidAmt=-999999;
	        curAIT.Y_Hidden_Trx_terms_Name__c = null;
	        for(Transaction_Terms__c curTrxTerm:curAIT.Transaction_Terms__r){
	            if(curTrxTerm.Bid_M__c>maxBidAmt){
	                maxBidAmt=curTrxTerm.Bid_M__c;
	                curAIT.Y_Hidden_Trx_terms_Name__c = curTrxTerm.Transaction_Prospect__r.Y_Hidden_Prospect_Name__c;
	            }
	        }
	        // if the changed Trx terms has preferred Prospect
	        if(!trigger.isDelete && setAITIdsForOnePreferredProspect.contains(curAIT.Id)){
	        	system.debug('Inside setAITIdsForOnePreferredProspect=');
		        for(Transaction_Terms__c curTrxTerm:curAIT.Transaction_Terms__r){
		        	system.debug('curTrxTerm.Id=' +curTrxTerm.Id + '=trigger.newmap.containsKey(curTrxTerm.Id)='+trigger.newmap.containsKey(curTrxTerm.Id));
		            if(!trigger.newmap.containsKey(curTrxTerm.Id)){
		            	// if it is not current records and set preferred Prospect on
		            	if(curTrxTerm.Preferred_Prospect__c){
		            		curTrxTerm.Preferred_Prospect__c = false;
		            		updTrxterm.add(curTrxTerm);
		            	}
		            }else{
		            	// if it is a current records
		            	if(curTrxTerm.Preferred_Prospect__c){
		            		curAIT.Y_Hidden_Preferred_Prospect_Term__c = curTrxTerm.Id;
		            		//curAIT.Y_Hidden_Preferred_Bid__c = curTrxTerm.Bid_M__c;
		            	}
		            }
		        }	        	
	        	
	        }
	        
	    }
	    LWGlobalUtils.setTriggersflagOn();   
	    LeaseWareUtils.setFromTrigger('CalledFromTxnTermsRefreshProspectSummary');                                             
	    update listAITs;
	    LeaseWareUtils.unsetTriggers('CalledFromTxnTermsRefreshProspectSummary');  
	    LWGlobalUtils.setTriggersflagOff();
	    
	    LWGlobalUtils.setTriggersflagOn();   
	    update updTrxterm;
	    LWGlobalUtils.setTriggersflagOff();	    
	    
	    if(setProspects.size()>0 && !leasewareUtils.isfromFuture())LeaseWareUtils.RefreshProspectSummary(setProspects);
	    system.debug('TxnTermsRefreshProspectSummary-');

 		
 	}
}