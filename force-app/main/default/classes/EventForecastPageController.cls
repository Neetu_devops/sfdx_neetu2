/* Created By Anjani : 06-April-2016 */

public with sharing class EventForecastPageController {


	private final Aircraft__c thisPBL;
	public static Aircraft__c curPBL {get;set;}
	//private map<String,Maintenance_Program_Event__c> mapMPE = new map<String,Maintenance_Program_Event__c>();
	//private map<String,Base_Readings__c> mapBR= new map<String,Base_Readings__c>();
	//private Component_Return_Condition__c RetCond;
	

	
	
	private static ChartCommonStruct[] currentChart_AF = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_AF_Child = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine1 = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine1_LLP = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine2 = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine2_LLP = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine3 = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine3_LLP = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine4 = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Engine4_LLP = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Propeller1 = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Propeller1_LLP = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Propeller2 = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_Propeller2_LLP = new list<ChartCommonStruct>();
			
	private static ChartCommonStruct[] currentChart_LG = new list<ChartCommonStruct>();
	private static ChartCommonStruct[] currentChart_LG_Child = new list<ChartCommonStruct>();	
	private static ChartCommonStruct[] currentChart_APU = new list<ChartCommonStruct>();	
				
	public static ChartCommonStruct[] getChart_Event_AF(){
		return currentChart_AF;
	}
	public static ChartCommonStruct[] getChart_Event_AF_Child(){
		return currentChart_AF_Child;
	}	
	public static ChartCommonStruct[] getChart_Event_APU(){
		return currentChart_APU;
	}	
	public static ChartCommonStruct[] getChart_Event_Engine1(){
		return currentChart_Engine1;
	}
	public static ChartCommonStruct[] getChart_Event_Engine1_LLP(){
		return currentChart_Engine1_LLP;
	}
	public static ChartCommonStruct[] getChart_Event_Engine2(){
		return currentChart_Engine2;
	}
	public static ChartCommonStruct[] getChart_Event_Engine2_LLP(){
		return currentChart_Engine2_LLP;
	}
	public static ChartCommonStruct[] getChart_Event_Engine3(){
		return currentChart_Engine3;
	}
	public static ChartCommonStruct[] getChart_Event_Engine3_LLP(){
		return currentChart_Engine3_LLP;
	}
	public static ChartCommonStruct[] getChart_Event_Engine4(){
		return currentChart_Engine4;
	}
	public static ChartCommonStruct[] getChart_Event_Engine4_LLP(){
		return currentChart_Engine4_LLP;
	}

	public static ChartCommonStruct[] getChart_Event_Propeller2(){
		return currentChart_Propeller2;
	}
	public static ChartCommonStruct[] getChart_Event_Propeller2_LLP(){
		return currentChart_Propeller2_LLP;
	}
	public static ChartCommonStruct[] getChart_Event_Propeller1(){
		return currentChart_Propeller1;
	}
	public static ChartCommonStruct[] getChart_Event_Propeller1_LLP(){
		return currentChart_Propeller1_LLP;
	}	
	
	public static ChartCommonStruct[] getChart_Event_LG(){
		return currentChart_LG;
	}
	public static ChartCommonStruct[] getChart_Event_LG_Child(){
		return currentChart_LG_Child;
	}								
    public EventForecastPageController(ApexPages.StandardController stdController) {
    	system.debug('Start');
        this.thisPBL = (Aircraft__c)stdController.getRecord();
        Id PNLId = thisPBL.Id;
        try{
        	

             
			
			getAirframeDetail();
			system.debug('anajni3');
			getEngineDetail();
        }catch(exception e){
            //throw new leaseware__LeaseWareException('Invalid Nal data. Please edit and save the Prospect Buy/Lease before opening the NAL Calculation. ' +e);
        }
        system.debug('End');
    }
	private void getEngineDetail(){
		Integer Numbering = 2;
		Integer LGNumbering = 1;
		
            system.debug('anajni1'); 


			Event_Forecast__c[] EFList = [select id ,Y_Hidden_sequence__c
        			 		,Aircraft__c,Assembly__c,Type__c,Life_Limited_Part_LLP__c,Component_Name__c
        			 		,Current_FH_FC__c,Current_MR_Balance_F__c,Remaining_Life__c,Rate_Unit_F__c,Rate__c,Opening_Balance__c
        			 		,MX_Interval__c,Event_Phase_F__c,Rate_Basis_As_Per_Lease__c
        			 		,Next_Event_Date__c,Limiting_Factor__c,Next_Event_Cost__c,TSLV_CSLV__c,Reserve_Balance__c,Difference__c
        			 		,Utilization_End_Of_Lease__c,Limiting_Factor_End_Of_Lease__c,Event_Cost_End_Of_Lease__c,Reserve_Balance_End_Of_Lease__c
        			 	from Event_Forecast__c where aircraft__c = :thisPBL.Id
        			 	order by Y_Hidden_sequence__c,Life_Limited_Part_LLP__c	]; 
			
			list<Event_Forecast__c> AssemblyEFList = new list<Event_Forecast__c>();
			list<Event_Forecast__c> Engine1LLPList = new list<Event_Forecast__c>();
			list<Event_Forecast__c> Engine2LLPList = new list<Event_Forecast__c>();
			list<Event_Forecast__c> Engine3LLPList = new list<Event_Forecast__c>();
			list<Event_Forecast__c> Engine4LLPList = new list<Event_Forecast__c>();
			list<Event_Forecast__c> Propeller1LLPList = new list<Event_Forecast__c>();
			list<Event_Forecast__c> Propeller2LLPList = new list<Event_Forecast__c>();
			
			for(Event_Forecast__c curRec :EFList ){
				
				if(curRec.Assembly__c!=null && !curRec.Type__c.startsWith('LLP')) AssemblyEFList.add(curRec);
				if(LeasewareUtils.isLLPEnableAtVF()){
					if(curRec.Type__c.startsWith('LLP Engine 1')) Engine1LLPList.add(curRec);
					else if(curRec.Type__c.startsWith('LLP Engine 2')) Engine2LLPList.add(curRec);
					else if(curRec.Type__c.startsWith('LLP Engine 3')) Engine3LLPList.add(curRec);
					else if(curRec.Type__c.startsWith('LLP Engine 4')) Engine4LLPList.add(curRec);
					else if(curRec.Type__c.startsWith('LLP Propeller 1')) Propeller1LLPList.add(curRec);
					else if(curRec.Type__c.startsWith('LLP Propeller 2')) Propeller2LLPList.add(curRec);
				}
			}
			string NumberingStr;
			Decimal LGNumberingFixed;
			for(Event_Forecast__c curRec :AssemblyEFList ){
				system.debug('--------------------curRec.Type__c='+curRec.Type__c);
				
		

				if(leasewareutils.C_APU.equals(curRec.Type__c)){
					currentChart_APU.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;', Numbering +'.0&nbsp;&nbsp;APU',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );		
			
				    Numbering ++;						
				} 	
				else if(leasewareutils.C_ENGINE_1.equals(curRec.Type__c)){
					
					currentChart_Engine1.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;', Numbering +'.0&nbsp;&nbsp;Engine 1',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );		
					currentChart_Engine1_LLP.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;',Numbering +'.0&nbsp;&nbsp;Engine 1',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
				    Integer LLPNumbering = 0;					
					for(Event_Forecast__c curLLP :Engine1LLPList){
						
						if(currentChart_Engine1_LLP.size()==1) NumberingStr = ''+Numbering +'.'+1 ;
						else NumberingStr = ''+Numbering +'.1.'+LLPNumbering ;
						currentChart_Engine1_LLP.add(	new ChartCommonStruct( '','&nbsp;&nbsp;'+NumberingStr +'&nbsp;&nbsp;'+curLLP.Component_Name__c,curLLP.Id,curLLP.Event_Phase_F__c,curLLP.MX_Interval__c
				    					,curLLP.Rate__c ,curLLP.Current_FH_FC__c,curLLP.Remaining_Life__c,curLLP.Opening_Balance__c,curLLP.Current_MR_Balance_F__c
				    						,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor__c,curLLP.Next_Event_Cost__c,curLLP.TSLV_CSLV__c,curLLP.Reserve_Balance__c,curLLP.Difference__c ,curLLP.Rate_Unit_F__c
		    					,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor_End_Of_Lease__c,curLLP.Event_Cost_End_Of_Lease__c,curLLP.Utilization_End_Of_Lease__c,curLLP.Reserve_Balance_End_Of_Lease__c) );
						LLPNumbering++;				    												
					}			    								
				    Numbering ++;								
				}else if(leasewareutils.C_ENGINE_2.equals(curRec.Type__c)){
					
					currentChart_Engine2.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;', Numbering +'.0&nbsp;&nbsp;Engine 2',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );		
					currentChart_Engine2_LLP.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;',Numbering +'.0&nbsp;&nbsp;Engine 2',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
				    Integer LLPNumbering = 0;					
					for(Event_Forecast__c curLLP :Engine2LLPList){
						if(currentChart_Engine2_LLP.size()==1) NumberingStr = ''+Numbering +'.'+1 ;
						else NumberingStr = ''+Numbering +'.1.'+LLPNumbering ;						
						currentChart_Engine2_LLP.add(	new ChartCommonStruct( '','&nbsp;&nbsp;'+NumberingStr +'&nbsp;&nbsp;'+curLLP.Component_Name__c,curLLP.Id,curLLP.Event_Phase_F__c,curLLP.MX_Interval__c
				    					,curLLP.Rate__c ,curLLP.Current_FH_FC__c,curLLP.Remaining_Life__c,curLLP.Opening_Balance__c,curLLP.Current_MR_Balance_F__c
				    						,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor__c,curLLP.Next_Event_Cost__c,curLLP.TSLV_CSLV__c,curLLP.Reserve_Balance__c,curLLP.Difference__c ,curLLP.Rate_Unit_F__c
		    					,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor_End_Of_Lease__c,curLLP.Event_Cost_End_Of_Lease__c,curLLP.Utilization_End_Of_Lease__c,curLLP.Reserve_Balance_End_Of_Lease__c) );
						LLPNumbering++;				    												
					}			    								
				    Numbering ++;								
				}else if(leasewareutils.C_ENGINE_3.equals(curRec.Type__c)){
					
					currentChart_Engine3.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;', Numbering +'.0&nbsp;&nbsp;Engine 3',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );		
					currentChart_Engine3_LLP.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;',Numbering +'.0&nbsp;&nbsp;Engine 3',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
				    Integer LLPNumbering = 0;					
					for(Event_Forecast__c curLLP :Engine3LLPList){
						if(currentChart_Engine3_LLP.size()==1) NumberingStr = ''+Numbering +'.'+1 ;
						else NumberingStr = ''+Numbering +'.1.'+LLPNumbering ;						
						currentChart_Engine3_LLP.add(	new ChartCommonStruct( '','&nbsp;&nbsp;'+NumberingStr +'&nbsp;&nbsp;'+curLLP.Component_Name__c,curLLP.Id,curLLP.Event_Phase_F__c,curLLP.MX_Interval__c
				    					,curLLP.Rate__c ,curLLP.Current_FH_FC__c,curLLP.Remaining_Life__c,curLLP.Opening_Balance__c,curLLP.Current_MR_Balance_F__c
				    						,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor__c,curLLP.Next_Event_Cost__c,curLLP.TSLV_CSLV__c,curLLP.Reserve_Balance__c,curLLP.Difference__c ,curLLP.Rate_Unit_F__c
		    					,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor_End_Of_Lease__c,curLLP.Event_Cost_End_Of_Lease__c,curLLP.Utilization_End_Of_Lease__c,curLLP.Reserve_Balance_End_Of_Lease__c) );
						LLPNumbering++;				    												
					}			    								
				    Numbering ++;								
				}else if(leasewareutils.C_ENGINE_4.equals(curRec.Type__c)){
					
					currentChart_Engine4.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;', Numbering +'.0&nbsp;&nbsp;Engine 4',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );		
					currentChart_Engine4_LLP.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;',Numbering +'.0&nbsp;&nbsp;Engine 4',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
				    Integer LLPNumbering = 0;					
					for(Event_Forecast__c curLLP :Engine4LLPList){
						if(currentChart_Engine4_LLP.size()==1) NumberingStr = ''+Numbering +'.'+1 ;
						else NumberingStr = ''+Numbering +'.1.'+LLPNumbering ;						
						currentChart_Engine4_LLP.add(	new ChartCommonStruct( '','&nbsp;&nbsp;'+NumberingStr +'&nbsp;&nbsp;'+curLLP.Component_Name__c,curLLP.Id,curLLP.Event_Phase_F__c,curLLP.MX_Interval__c
				    					,curLLP.Rate__c ,curLLP.Current_FH_FC__c,curLLP.Remaining_Life__c,curLLP.Opening_Balance__c,curLLP.Current_MR_Balance_F__c
				    						,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor__c,curLLP.Next_Event_Cost__c,curLLP.TSLV_CSLV__c,curLLP.Reserve_Balance__c,curLLP.Difference__c ,curLLP.Rate_Unit_F__c
		    					,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor_End_Of_Lease__c,curLLP.Event_Cost_End_Of_Lease__c,curLLP.Utilization_End_Of_Lease__c,curLLP.Reserve_Balance_End_Of_Lease__c) );
						LLPNumbering++;				    												
					}			    								
				    Numbering ++;								
				}else if(leasewareutils.C_PROPELLER_1.equals(curRec.Type__c)){
					
					currentChart_Propeller1.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;', Numbering +'.0&nbsp;&nbsp;Propeller 1',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );		
					currentChart_Propeller1_LLP.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;',Numbering +'.0&nbsp;&nbsp;Propeller 1',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
				    Integer LLPNumbering = 0;					
					for(Event_Forecast__c curLLP :Propeller1LLPList){
						if(currentChart_Propeller1_LLP.size()==1) NumberingStr = ''+Numbering +'.'+1 ;
						else NumberingStr = ''+Numbering +'.1.'+LLPNumbering ;						
						currentChart_Propeller1_LLP.add(	new ChartCommonStruct( '','&nbsp;&nbsp;'+NumberingStr +'&nbsp;&nbsp;'+curLLP.Component_Name__c,curLLP.Id,curLLP.Event_Phase_F__c,curLLP.MX_Interval__c
				    					,curLLP.Rate__c ,curLLP.Current_FH_FC__c,curLLP.Remaining_Life__c,curLLP.Opening_Balance__c,curLLP.Current_MR_Balance_F__c
				    						,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor__c,curLLP.Next_Event_Cost__c,curLLP.TSLV_CSLV__c,curLLP.Reserve_Balance__c,curLLP.Difference__c ,curLLP.Rate_Unit_F__c
		    					,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor_End_Of_Lease__c,curLLP.Event_Cost_End_Of_Lease__c,curLLP.Utilization_End_Of_Lease__c,curLLP.Reserve_Balance_End_Of_Lease__c) );
						LLPNumbering++;				    												
					}			    								
				    Numbering ++;								
				}else if(leasewareutils.C_PROPELLER_2.equals(curRec.Type__c)){
					
					currentChart_Propeller2.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;', Numbering +'.0&nbsp;&nbsp;Propeller 2',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );		
					currentChart_Propeller2_LLP.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;',Numbering +'.0&nbsp;&nbsp;Propeller 2',curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
				    Integer LLPNumbering = 0;					
					for(Event_Forecast__c curLLP :Propeller2LLPList){
						if(currentChart_Propeller2_LLP.size()==1) NumberingStr = ''+Numbering +'.'+1 ;
						else NumberingStr = ''+Numbering +'.1.'+LLPNumbering ;						
						currentChart_Propeller2_LLP.add(	new ChartCommonStruct( '','&nbsp;&nbsp;'+NumberingStr +'&nbsp;&nbsp;'+curLLP.Component_Name__c,curLLP.Id,curLLP.Event_Phase_F__c,curLLP.MX_Interval__c
				    					,curLLP.Rate__c ,curLLP.Current_FH_FC__c,curLLP.Remaining_Life__c,curLLP.Opening_Balance__c,curLLP.Current_MR_Balance_F__c
				    						,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor__c,curLLP.Next_Event_Cost__c,curLLP.TSLV_CSLV__c,curLLP.Reserve_Balance__c,curLLP.Difference__c ,curLLP.Rate_Unit_F__c
		    					,curLLP.Next_Event_Date__c,curLLP.Limiting_Factor_End_Of_Lease__c,curLLP.Event_Cost_End_Of_Lease__c,curLLP.Utilization_End_Of_Lease__c,curLLP.Reserve_Balance_End_Of_Lease__c) );
						LLPNumbering++;				    												
					}			    								
				    Numbering ++;								
				} 										
				else if(curRec.Type__c.contains('Landing Gear')){
					system.debug('curRec.Type__c'+curRec.Type__c);
					if(currentChart_LG.IsEmpty()){
						currentChart_LG.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;', Numbering +'.0&nbsp;&nbsp;Landing Gear',curRec.Aircraft__c,null ,null
				    					,null,null,null,null,null,null,null,null,null,null,null,null
	    					,null,null,null,null,null) );		
						currentChart_LG_Child.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;',Numbering +'.0&nbsp;&nbsp;Landing Gear',curRec.Aircraft__c,null ,null
				    					,null,null,null,null,null,null,null,null,null,null,null,null
	    					,null,null,null,null,null) );
				    	LGNumberingFixed = Numbering;
				    					
					}

					currentChart_LG_Child.add(	new ChartCommonStruct( '','&nbsp;&nbsp;'+LGNumberingFixed +'.'+LGNumbering +'&nbsp;&nbsp;'+curRec.Type__c,curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
				    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
				    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
		    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
				    LGNumbering++;
				    Numbering ++;						
				}								
			}
						
						
						
			system.debug('anjani2');		
	}
	private void getAirframeDetail(){
		
		
		Event_Forecast__c[] EFList = [select id 
        			 		,Aircraft__c,Assembly__c,Type__c,Life_Limited_Part_LLP__c,Component_Name__c
        			 		,Current_FH_FC__c,Current_MR_Balance_F__c,Remaining_Life__c,Rate_Unit_F__c,Rate__c,Opening_Balance__c
        			 		,MX_Interval__c,Event_Phase_F__c,Rate_Basis_As_Per_Lease__c
        			 		,Next_Event_Date__c,Limiting_Factor__c,Next_Event_Cost__c,TSLV_CSLV__c,Reserve_Balance__c,Difference__c
        			 		,Utilization_End_Of_Lease__c,Limiting_Factor_End_Of_Lease__c,Event_Cost_End_Of_Lease__c,Reserve_Balance_End_Of_Lease__c
        			 	from Event_Forecast__c where aircraft__c = :thisPBL.Id
        			 	and Type__c in ('Heavy 1','Heavy 2','Heavy 3','Heavy 4','Other','C-Check')
        			 	order by Type__c	];	
                    		
		//Airframe
		currentChart_AF.add(	new ChartCommonStruct( '&nbsp;[+]&nbsp;','1.0&nbsp;&nbsp;AirFrame',thisPBL.Id,null ,null
	    					,null,null,null,null,null,null,null,null,null,null,null,null
	    					,null,null,null,null,null) );		
		currentChart_AF_Child.add(	new ChartCommonStruct( '&nbsp;[-]&nbsp;','1.0&nbsp;&nbsp;AirFrame',thisPBL.Id,null ,null
	    					,null,null,null,null,null,null,null,null,null,null,null,null
	    					,null,null,null,null,null) );
	    Integer numbering=1;							    					
		for(Event_Forecast__c curRec:EFList){
		
    			currentChart_AF_Child.add(	new ChartCommonStruct( null,'&nbsp;&nbsp;1.'+ numbering +'&nbsp;&nbsp;' + curRec.Component_Name__c ,curRec.Id,curRec.Event_Phase_F__c,curRec.MX_Interval__c
			    					,curRec.Rate__c ,curRec.Current_FH_FC__c,curRec.Remaining_Life__c,curRec.Opening_Balance__c,curRec.Current_MR_Balance_F__c
			    						,curRec.Next_Event_Date__c,curRec.Limiting_Factor__c,curRec.Next_Event_Cost__c,curRec.TSLV_CSLV__c,curRec.Reserve_Balance__c,curRec.Difference__c ,curRec.Rate_Unit_F__c
	    					,curRec.Next_Event_Date__c,curRec.Limiting_Factor_End_Of_Lease__c,curRec.Event_Cost_End_Of_Lease__c,curRec.Utilization_End_Of_Lease__c,curRec.Reserve_Balance_End_Of_Lease__c) );
		
					 
				numbering++;					
					 	 

		}
	}





	
	// class structure
	public class ChartCommonStruct{

		public string s1 {get;set;}
		public string s2 {get;set;} 
		public Id s2_Id {get;set;}
		public string s3 {get;set;}
		public Decimal s4 {get;set;}
		public Decimal s5 {get;set;}
		public Decimal s6 {get;set;}
		public Decimal s7 {get;set;}
		public Decimal s8 {get;set;}
		public Decimal s9 {get;set;}
		public Date s10 {get;set;}
		public string s11 {get;set;}
		public Decimal s12 {get;set;}
		public Decimal s13 {get;set;}
		public Decimal s14 {get;set;}
		public Decimal s15 {get;set;}
		public string s16 {get;set;}		
		public Date s17 {get;set;}
		public string s18 {get;set;}
		public Decimal s19 {get;set;}
		public Decimal s20 {get;set;}
		public Decimal s21 {get;set;}
	
				
        public ChartCommonStruct(String s1,String s2,Id s2_Id,String s3,Decimal s4,Decimal s5,Decimal s6,Decimal s7,Decimal s8
        								,Decimal s9,Date s10,String s11,Decimal s12,Decimal s13,Decimal s14,Decimal s15, string s16
        								,Date s17,String s18,Decimal s19,Decimal s20,Decimal s21){
			
			if(s1 != null) this.s1 = s1;
			if(s2 != null) this.s2 = s2;
			if(s2_Id != null) this.s2_Id = s2_Id;
			if(s3 != null) this.s3 = s3;
			if(s4 != null) this.s4 = s4.setScale(2, RoundingMode.HALF_UP); else this.s4 = null;
			if(s5 != null) this.s5 = s5.setScale(0, RoundingMode.HALF_UP); else this.s5 = null;
			if(s6 != null) this.s6 = s6.setScale(2, RoundingMode.HALF_UP); else this.s6 = null;
			if(s7 != null) this.s7 = s7.setScale(0, RoundingMode.HALF_UP); else this.s7 = null;
			if(s8 != null) this.s8 = s8.setScale(0, RoundingMode.HALF_UP); else this.s8 = null;
			if(s9 != null) this.s9 = s9.setScale(0, RoundingMode.HALF_UP); else this.s9 = null;
			if(s10 != null) this.s10 = s10;//LeasewareUtils.getDatetoString(s10,'MM/DD/YYYY');
			if(s11 != null) this.s11 = s11;
			if(s12 != null) this.s12 = s12.setScale(0, RoundingMode.HALF_UP); else this.s12 = null;
			if(s13 != null) this.s13 = s13.setScale(2, RoundingMode.HALF_UP); else this.s13 = null;	
			if(s14 != null) this.s14 = s14.setScale(0, RoundingMode.HALF_UP); else this.s14 = null;	
						//anjani : mutiplying by -1 to make MR Balance - Event Cost
			if(s15 != null) this.s15 = s15.setScale(0, RoundingMode.HALF_UP); else this.s15 = null;	
			if(s16 != null) this.s16 = s16;	
			
			if(s17 != null) this.s17 = s17;//LeasewareUtils.getDatetoString(s10,'MM/DD/YYYY');
			if(s18 != null) this.s18 = s18;
			if(s19 != null) this.s19 = s19.setScale(0, RoundingMode.HALF_UP); else this.s19 = null;
			if(s20 != null) this.s20 = s20.setScale(2, RoundingMode.HALF_UP); else this.s20 = null;	
			if(s21 != null) this.s21 = s21.setScale(0, RoundingMode.HALF_UP); else this.s21 = null;				
							
			system.debug('s2==' +s2);
			system.debug('s16==' +s16);
        }
        
	}

}