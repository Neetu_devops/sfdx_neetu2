public class LLPUtilSnapshotTriggerHandler implements ITrigger {


	//Contructor
	private final string triggerBefore = 'LLPUtilSnapshotTriggerHandler';
	private final string triggerAfter = 'LLPUtilSnapshotTriggerHandler';

	public void bulkBefore(){
	}

	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkAfter(){
	}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 */
	public void beforeInsert(){

		system.debug('LLPUtilSnapshotTriggerHandler.beforeInsert(+)');

		if(LeaseWareUtils.isFromTrigger('LLPDiskSheet')) return;
		
		LeaseWareUtils.setFromTrigger(triggerBefore);

		List<LLP_Utilization_Snapshot__c> listNew = (List<LLP_Utilization_Snapshot__c>) (trigger.new);
		for(LLP_Utilization_Snapshot__c currec : listNew){
			currec.addError('LLP Utilization Snapshots should be created, modified or deleted only using Snapshots User Interface');
		}

		system.debug('LLPUtilSnapshotTriggerHandler.beforeInsert(-)');
	}

	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 */
	public void beforeUpdate(){

		if(LeaseWareUtils.isFromTrigger('LLPDiskSheet')) return;
		
		LeaseWareUtils.setFromTrigger(triggerBefore);

		List<LLP_Utilization_Snapshot__c> listNew = (List<LLP_Utilization_Snapshot__c>) (trigger.new);
		for(LLP_Utilization_Snapshot__c currec : listNew){
			currec.addError('LLP Utilization Snapshots should be created, modified or deleted only using Snapshots User Interface');
		}
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 */
	public void beforeDelete(){
		system.debug('LLPUtilSnapshotTriggerHandler.beforeDelete(+)');

		if(LeaseWareUtils.isFromTrigger('LLPDiskSheet')) return;
		
		LeaseWareUtils.setFromTrigger(triggerBefore);
		
		List<LLP_Utilization_Snapshot__c> listOld = (List<LLP_Utilization_Snapshot__c>) (trigger.old);
		for(LLP_Utilization_Snapshot__c currec : listOld){
			currec.addError('LLP Utilization Snapshots should be created, modified or deleted only using Snapshots User Interface');
		}

		system.debug('LLPUtilSnapshotTriggerHandler.beforeDelete(-)');
	}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 */
	public void afterInsert(){
	}

	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 */
	public void afterUpdate(){
	}

	public void afterDelete(){
	}

	/**
	 * afterUnDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */
	public void afterUnDelete(){
	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */

	public void andFinally(){
	}
}