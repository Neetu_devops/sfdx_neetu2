global class Notify_NotificationScheduler implements Schedulable, Database.Batchable<sObject>, Database.Stateful{


     /// Batch Code Below
     public Notifications_Subscription__c notifSubRec;
     public String status='Running';
     public String errorMessage;
     public Map<String, Set<String>> mergeFieldMap = new Map<String, Set<String>>();

    public void setNotificationRecord(Notifications_Subscription__c nsRec) {
         this.notifSubRec = nsRec;
     }
   
     //-----Schedule Methods-------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext SC) {
        callStart();
        //call future method
        callOtherAsyncProcess();
    }

   
    public static void callStart(){
        List<Notifications_Subscription__c> activeNotificationSubList = Notify_Utility.getActiveNotificationSub();
        system.debug('activeNotificationSubList-----'+activeNotificationSubList);
        if(activeNotificationSubList != null){
            System.debug('Number of active records: ' + activeNotificationSubList.size());
            for (Notifications_Subscription__c rec : activeNotificationSubList){
                Notify_NotificationScheduler ns = new Notify_NotificationScheduler();
                ns.setNotificationRecord(rec);
                Database.executeBatch(ns, Notify_Utility.LIMIT_NUMBER_OF_NOTIF_BATCH);
            }
        }
        
    }

   /*******************************************Batch Methods*********************************************************************************************** */


    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('notifSubRec: '+notifSubRec);
        Date notificationDate = system.today().addDays(Integer.valueOf(notifSubRec.Notify_Days__c));
        String queryStr = Notify_Utility.prepareQuery(notifSubRec, mergeFieldMap, false,notificationDate);
        System.debug('Query: '+queryStr);
      return Database.getQueryLocator(queryStr);  
       
        
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> sobjList) {
        try {
            
            
            List<Notification_Logging__c> logList = new List<Notification_Logging__c>();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            List<Notifications_Subscription__c> subscriptionListToUpdate = new List<Notifications_Subscription__c>();
                
            List<Notification_Setting__c> nsList = [SELECT Id, OwnerId, IsDeleted, Name, Disable_all_emails__c, Default_CC_Address__c, Reply_To_Address__c, Keep_Log_records_Days__c, From_Adress_Override__c, Email_Footer__c,Max_Emails_Limit__c FROM Notification_Setting__c];
            
            Notify_Utility.createNotifyLogging(new Set<SObject>(sobjList), notifSubRec, logList,mails,nsList[0],subscriptionListToUpdate);
            if(!logList.isEmpty()){
                try{
                    insert logList;
                    if(!mails.isEmpty() && !nsList[0].Disable_all_emails__c){
                        LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
                        Messaging.sendEmail(mails);
                        LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
                    }
                   
                    
                }
                catch(Exception ex){
                    status ='Error';
                    errorMessage = ex.getMessage();
                    throw new Notify_CustomException('Error Occured: ' + ex.getMessage() +'--'+ ex.getStackTraceString());

                }
            }
            

        }
        catch (Exception ex) {
            status ='Error';
            errorMessage = ex.getMessage();
            throw new Notify_CustomException(ex.getMessage() + '---'+ ex.getStackTraceString());
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        //Update the Notification Subscription Status based on the Status of Batch 
       notifSubRec.Status__c = status;
       notifSubRec.Error_Message__c = errorMessage;
       Notify_Utility.setSourceOfCall('INTERNAL_API');
       update notifSubRec;
        callStart();
    }


    @future
    public static void callOtherAsyncProcess(){
        
        //Purging of Records
        deleteLoggingRecords();
        //call validation method of notificationsubscription method with respect to Object and APi name
        validateNotificationSubscription();
    }
    
    public static void deleteLoggingRecords(){
        List<Notification_Setting__c> nsList = [SELECT Id, OwnerId, IsDeleted, Name, Disable_all_emails__c, Default_CC_Address__c, Reply_To_Address__c, Keep_Log_records_Days__c, From_Adress_Override__c,Max_Emails_Limit__c, Email_Footer__c FROM Notification_Setting__c];
        if(nsList.size()==0){ return; }
        Decimal keepLogDays = nsList[0].Keep_Log_records_Days__c==null?30:nsList[0].Keep_Log_records_Days__c;

        String soql = 'SELECT Id, Name,CreatedDate FROM Notification_Logging__c '+
              'WHERE CreatedDate = N_DAYS_AGO:'+keepLogDays +' order by CreatedDate asc';

        List<Notification_Logging__c> delLogList = Database.query(soql);
        if(delLogList!=null || delLogList.size()>0 ){
            String strMsg='';
             try{
                delete delLogList ;
                strMsg ='Number of Logging Records deleted -------'+ delLogList.size();
             }catch(Exception e){
                 strMsg = 'Some Error Occured while deletion of log records' +e.getMessage();
             }
            

            
        }

    }

   /*************************************************************************************** */
   // This method is to check if any of the existing Notification Subscriptions are in error 
   

  /************************************************************************************** */

    public static  void validateNotificationSubscription(){
       
        
        List<Notifications_Subscription__c> nsList = [SELECT Id,Name, Field_API_Name__c,Owner.Email,Object_API_Name__c, Object_Name__c,  Status__c,  OwnerId, Owner.Name,Last_Execution_Date__c,Active__c from Notifications_Subscription__c where Active__c=true];
        List<Notifications_Subscription__c> nsListToUpdate = new List<Notifications_Subscription__c>();
        Set<String> setOfObjects = new Set<String>();
        
        
        for ( Schema.SObjectType obj : Schema.getGlobalDescribe().values() )
        {
            Schema.DescribeSObjectResult objResult = obj.getDescribe();
            setOfObjects.add(objResult.getName());
        }
        system.debug('setOfObjects----'+setOfObjects);
        for(Notifications_Subscription__c ns : nsList){
            if(setOfObjects.contains(ns.Object_API_Name__c)){
                //Check for fields is correct or not
                system.debug('object exists');
                List<String> fieldApiNamesList = new List<String>();
                if(ns.Field_API_Name__c!=null){
                    if(Test.isRunningTest() || !schema.getGlobalDescribe().get(ns.Object_API_Name__c).getDescribe().fields.getMap().containsKey(ns.Field_API_Name__c)){
                        system.debug('error in notification');
                        ns.Status__c = 'Error';
                        ns.Active__c = false;
                        ns.Last_Execution_Date__c = system.now();
                        ns.Error_Message__c = 'Field not exists '+ns.Field_API_Name__c + ' on object ' + ns.Object_API_Name__c;
                       
                        nsListToUpdate.add(ns);
                    }
                }
                
            }else {
                //Set the Notification Subscription as Error 
                system.debug(ns.Object_API_Name__c + '-----error in notification');
                ns.Status__c = 'Error';
                ns.Active__c = false;
                ns.Last_Execution_Date__c = system.now();
                ns.Error_Message__c = 'Object does not exists ' + ns.Object_API_Name__c;
                nsListToUpdate.add(ns);
            }
        }
        
        if(nsListToUpdate.size()>0){
             Notify_Utility.setSourceOfCall('INTERNAL_API');
            update nsListToUpdate;
            String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

            List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();		
            for(Notifications_Subscription__c ns :nsListToUpdate ){

             
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               mail.setSubject('Notification Subscription is in error status');
               mail.setToAddresses(new String[]{ns.Owner.Email});
               if(String.isempty( Notify_Utility.Notify_Setting_REC.From_Adress_Override__c)){
                    mail.setSenderDisplayName('Leaseworks');
                }else{
                    mail.setSenderDisplayName(Notify_Utility.Notify_Setting_REC.From_Adress_Override__c);
                }

                String msgBody = 'You have received this email because something wrong happened to the Notification Subscription <a href=' +baseUrl +'/' +ns.Id + '>'+ns.name +'</a>  and the system cannot send notifications of this type anymore.'
                +'<br/><br/> It is important to understand that users will not receive business notifications from the system until the error is resolved.'
                +'<br/><br/>Please log in to the system and review the errors associated with the Notification Subscription  <a href=' +baseUrl + '/' +ns.Id+ '>'+ns.name +'</a> Then try to activate it again.'
                +'<br/><br/>If this does not help, please reach out to your dedicated support. ';
              
               mail.setHtmlBody(msgBody);
                emailList.add(mail);
                
                
            }
            if(!Test.isRunningTest()) {
            LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');    
            Messaging.sendEmail(emailList);
            LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
            }
           
            
        }
    }
    
   
    global static void triggerHandler(Schema.sObjectType soType){
        Notify_Utility.triggerHandler(soType);
    }
}