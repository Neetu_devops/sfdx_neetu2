/**
*******************************************************************************
* Class: ImageUploadController
* Created by AKG, Leaseworks 01/11/2021
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all aura methods required for ImageUploadComponent
*
********************************************************************************
*/
public class ImageUploadController {
    /**
    * @description query list of ImageSizeOptions for user to select image size
    *
    * @return List<ImageSizeOptions__mdt>, list of image size options
    */
    @AuraEnabled
    public static List<ImageSizeOptions__mdt> getImageOptions() {
        return ([SELECT Option_Name__c, Height__c, Width__c FROM ImageSizeOptions__mdt ORDER BY Option_Name__c]);
    }
    
    
    /**
    * @description Insert image as document chosen by the user
    *
    * @params selectedImage - String, Image selected by the user
    * @params imageName -  String, name of the image to be saved
    *
    * @return String, success message
    */
    @AuraEnabled
    public static String saveChosenImage(String selectedImage, String imageName, String recordId, Boolean generatePublicLink, String sObjectName, String storeImageURLTo) {
        ContentVersion file = new ContentVersion();
        file.VersionData = EncodingUtil.base64Decode(selectedImage);
        file.PathOnClient = '/' + imageName;
        file.FirstPublishLocationId = recordId;
        file.Title = imageName;
        
        System.debug('file::' + file);
        insert file;
        
        if (generatePublicLink == true) {
            ContentDistribution filePublicLink = new ContentDistribution();
            filePublicLink.name = file.Title;
            filePublicLink.ContentVersionId = file.id;
            filePublicLink.RelatedRecordId = recordId;
            filePublicLink.PreferencesAllowOriginalDownload = true;
            filePublicLink.PreferencesAllowViewInBrowser = true;
            filePublicLink.PreferencesLinkLatestVersion = true;
            filePublicLink.PreferencesNotifyOnVisit = false;
            filePublicLink.PreferencesPasswordRequired = false;
            filePublicLink.PreferencesAllowOriginalDownload = true;

            insert filePublicLink;

            if (storeImageURLTo != null && storeImageURLTo != '') {
                try {
                    ContentDistribution[] cdList = [SELECT Id, Name, ContentDocumentId, ContentDownloadUrl, DistributionPublicUrl
                                                    FROM ContentDistribution WHERE Id =: filePublicLink.Id];
                    if (!cdList.isEmpty()) {
                        SObject sObj = (SObject) Type.forName(sObjectName).newInstance();
                        sObj.put('Id', recordId);
                        sObj.put(storeImageURLTo, cdList[0].ContentDownloadUrl);

                        update sObj;
                    }
                }
                catch (Exception ex) {
                    System.debug('Error: '+ Utility.getErrorMessage(ex));
                }
            }
        }
        return 'Image Save Successfully';
    }
}