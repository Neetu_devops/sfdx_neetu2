public class InvoiceAdjustmentTriggerHandler implements ITrigger{
    
    private final string triggerBefore = 'InvoiceAdjustmentTriggerHandlerBefore';
    private final string triggerAfter = 'InvoiceAdjustmentTriggerHandlerAfter';
    
    
    
    public InvoiceAdjustmentTriggerHandler()
    {
    }
    
    public void bulkBefore()
    {
    }
    
    public void bulkAfter()
    {
    }
    
    public void beforeInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('InvoiceAdjustmentTriggerHandler.beforeInsert(+)');

        validateRecordChange();
        setAdjustmentName();
        
        system.debug('InvoiceAdjustmentTriggerHandler.beforeInsert(-)');     
    }
    
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InvoiceAdjustmentTriggerHandler.beforeUpdate(+)');

        validateRecordChange();
        
        system.debug('InvoiceAdjustmentTriggerHandler.beforeUpdate(-)');
    }
    
    public void beforeDelete()
    {  
        system.debug('InvoiceAdjustmentTriggerHandler.beforeDelete(+)');
        
        Set<Id> setLeaseId = new Set<Id>();
        map<id, Invoice_Adjustment__c> mapAdjToDel = new map<id, Invoice_Adjustment__c>([select id, Approval_Status__c, Linked_Invoice__r.Lease__c,
                                                                    (select id, Linked_Invoice_Line_Item__r.Assembly_MR_Info__c from Invoice_Adjustment_Line_Items__r where Linked_Invoice_Line_Item__r.Assembly_MR_Info__c != null LIMIT 1)
                                                                     from Invoice_Adjustment__c where id in :trigger.oldmap.Keyset()]);
        for(Invoice_Adjustment__c adj: (List<Invoice_Adjustment__c>)trigger.old){
            if('Approved'.equals(adj.Approval_Status__c)){
                adj.addError('Approved Invoice Adjustments cannot be deleted');
            }
            else{   
                //Check if there is SR associated with the line item        
                if(mapAdjToDel.get(adj.id).Invoice_Adjustment_Line_Items__r.size() > 0){
                    setLeaseId.add(mapAdjToDel.get(adj.id).Linked_Invoice__r.Lease__c);                                        
                }                     
            }            
        }

        //Call the supplemental rent transactions batch if there is SR
        if(!System.isFuture() && !System.isBatch() && setLeaseId.size()>0){    
            LeaseWareUtils.setFromTrigger('MR_SR_TRANSACTION'); // This is to prevent Transaction batch getting called again from SR trigger       
            System.debug('On deletion, update transactions from InvAdjustment trigger :');
            Database.executeBatch(new SupplementalRentTransactionsBatch(setLeaseId,false),SupplementalRentTransactionsBatch.getBatchSize());
            LeaseWareUtils.unsetTriggers('MR_SR_TRANSACTION');
        }   
        
        
        system.debug('InvoiceAdjustmentTriggerHandler.beforeDelete(-)');                 
    }
    
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InvoiceAdjustmentTriggerHandler.afterInsert(+)');
        
        
        
        system.debug('InvoiceAdjustmentTriggerHandler.afterInsert(-)');      
    }
    
    public void afterUpdate()
    {
        
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InvoiceAdjustmentTriggerHandler.afterUpdate(+)');

        updateAdjustmentLI();
                
        system.debug('InvoiceAdjustmentTriggerHandler.afterUpdate(-)');      
    }
    
    public void afterDelete()
    {
        system.debug('InvoiceAdjustmentTriggerHandler.afterDelete(+)');
        
        
        system.debug('InvoiceAdjustmentTriggerHandler.afterDelete(-)');      
    }
    
    public void afterUnDelete()
    {
        system.debug('InvoiceAdjustmentTriggerHandler.afterUnDelete(+)');
        
        system.debug('InvoiceAdjustmentTriggerHandler.afterUnDelete(-)');        
    }
    
    public void andFinally()
    {
    }
    
    //This method is used to set the name for the invoice adjustment. Format - ADJ 1 INVOICENAME
    //before insert
    private void setAdjustmentName()
    {   
        System.debug('InvoiceAdjustmentTriggerHandler::setAdjustmentName(+)');

        set<id> setInv = new set<id>();
        for(Invoice_Adjustment__c newInvAdj : (List<Invoice_Adjustment__c>)Trigger.new){
            setInv.add(newInvAdj.Linked_Invoice__c);
        }

        map<id, Invoice__c> mapInv = new map<id, Invoice__c>([select id,Name,(select id from Invoice_Adjustments__r) from Invoice__c where id in :setInv]);
        map<id, Integer> mapInvToAdjCnt = new map<id, Integer>();

        for(Invoice_Adjustment__c newInvAdj : (List<Invoice_Adjustment__c>)Trigger.new){
            if(!mapInvToAdjCnt.containsKey(newInvAdj.Linked_Invoice__c)){
                mapInvToAdjCnt.put(newInvAdj.Linked_Invoice__c, mapInv.get(newInvAdj.Linked_Invoice__c).Invoice_Adjustments__r.size());
            }
            String adjName = '';
            adjName = 'ADJ' + ' ' + (mapInvToAdjCnt.get(newInvAdj.Linked_Invoice__c) + 1) + ' ' + mapInv.get(newInvAdj.Linked_Invoice__c).Name;
            
            if(adjName.length()> 79){
                newInvAdj.Name =  adjName.Left(76) + '...';
             }
             else{
                newInvAdj.Name = adjName;
                 }

            System.debug('set adj name -' + newInvAdj.Name);
            mapInvToAdjCnt.put(newInvAdj.Linked_Invoice__c, mapInvToAdjCnt.get(newInvAdj.Linked_Invoice__c) + 1);
        }

        System.debug('InvoiceAdjustmentTriggerHandler::setAdjustmentName(-)');
    }

    //NOTE: Below validations are not needed when adjustments are created using UI. However, added validations
    //at the object level as well.
    //This method performs the following validations:
    //1. Cannot manually change the status from 'Approved'
    //2. Cannot manually change the status from 'Declined'
    //3. Cannot update any field except 'Comments' on an approved/declined adjustment
    //before insert, before update
    private void validateRecordChange()
    {   
        System.debug('InvoiceAdjustmentTriggerHandler::validateRecordChange(+)');

        if(trigger.isInsert){
            set<id> setInv = new set<Id>();
            for(Invoice_Adjustment__c invAdj: (List<Invoice_Adjustment__c>)Trigger.new){
                setInv.add(invAdj.Linked_Invoice__c);
            }
            map<id, Invoice__c> mapInv = new map<id, Invoice__c>([select id, Status__c from Invoice__c where id in :setInv]);
            for(Invoice_Adjustment__c invAdj: (List<Invoice_Adjustment__c>)Trigger.new){
                if(!'Approved'.equals(mapInv.get(invAdj.Linked_Invoice__c).Status__c)){
                    invAdj.addError('Invoice adjustments can only be performed on Approved invoices');
                }
            }
        }
        else if(trigger.isUpdate){

            Map<String, Schema.SObjectField> mapInvAdjField = Schema.SObjectType.Invoice_Adjustment__c.fields.getMap();
            list<String> listFieldsToCheck = new list<String>();
            set<string> setSkipFields = RLUtility.getAllFieldsFromFieldset('Invoice_Adjustment__c','SkipFieldsFromValidations');

            for(String Field:mapInvAdjField.keyset()){
                Schema.DescribeFieldResult thisFieldDesc = mapInvAdjField.get(Field).getDescribe();
                System.debug('Field Name is ' + thisFieldDesc.getName() + '. Local ' + thisFieldDesc.getLocalName());

                if(!thisFieldDesc.isUpdateable() || setSkipFields.contains(leasewareutils.getNamespacePrefix() + thisFieldDesc.getLocalName())) continue; //Add the fields that are always updatable to the list.
                    listFieldsToCheck.add(thisFieldDesc.getLocalName());
            }

            for(Invoice_Adjustment__c newInvAdj : (List<Invoice_Adjustment__c>)Trigger.new)
            {   
                Invoice_Adjustment__c oldInvAdj = (Invoice_Adjustment__c)Trigger.oldMap.get(newInvAdj.id);

                //Check if it is a valid status change            
                if(('Approved'.equals(oldInvAdj.Approval_Status__c) || 'Declined'.equals(oldInvAdj.Approval_Status__c)) && oldInvAdj.Approval_Status__c != newInvAdj.Approval_Status__c){
                    newInvAdj.addError('Cannot update the status from ' + oldInvAdj.Approval_Status__c + ' to ' + newInvAdj.Approval_Status__c);
                    continue;
                }
                
                //Check if any field is changed on 'Approved'/'Declined' adjustments
                for(String curField:listFieldsToCheck){
                    if(newInvAdj.get(curField) !=  oldInvAdj.get(curField)){   
                        Schema.DescribeFieldResult thisFieldDesc = mapInvAdjField.get(leasewareutils.getNamespacePrefix().toLowerCase() + curField).getDescribe();
                        System.debug(thisFieldDesc.getLabel() + ' has changed from' + oldInvAdj.get(curField) + ' to ' + newInvAdj.get(curField));
                        //Adjustment amount can be modified only from the 'Invoice Adjustments' UI
                        if(curField == 'Adjustment_Amount__c' && !LeaseWareUtils.isFromTrigger('INVOICE_ADJ_UPDATE'))   {
                            newInvAdj.addError('Adjustment amount cannot be manually modified. Please utilize ‘Invoice Adjustments’ functionality if you wish to update the amount.');                        
                            continue;
                        } 

                        if(!'Pending'.equals(oldInvAdj.Approval_Status__c) && ('Approved'.equals(newInvAdj.Approval_Status__c) || 'Declined'.equals(newInvAdj.Approval_Status__c))){
                            newInvAdj.addError('Approved/Declined Invoice Adjustment records cannot be modified');                        
                            continue;
                        }              
                    }
                }//end for fields check
            }

        }

        System.debug('InvoiceAdjustmentTriggerHandler::validateRecordChange(-)');
    }

    /* This method updates the adjustment line items which in turn would update the 
    * supplemental rent outstanding invoiced amount. It is applicable ONLY
    * when the adjustment is approved for a MR invoice using the standard approval process.
    * When manually approved, the outstanding invoice amount is updated in InvoiceAdjLineItemTriggerHandler
    * Called in after update
    */
    private void updateAdjustmentLI()
    {   
        System.debug('InvoiceAdjustmentTriggerHandler::updateAdjustmentLI(+)');
        map<id, Invoice_Adjustment__c> mapInvAdj = new map<id, Invoice_Adjustment__c>([select id, Linked_Invoice__r.Invoice_Type__c,
                                                             (select id from Invoice_Adjustment_Line_Items__r) from Invoice_Adjustment__c where id in :trigger.newmap.keyset()]);

        List<Invoice_Adjustment_Line_Item__c> lstAdjLIToUpd = new List<Invoice_Adjustment_Line_Item__c>();

        for(Invoice_Adjustment__c newInvAdj : (List<Invoice_Adjustment__c>)Trigger.new){

            if(LeaseWareUtils.isFromTrigger('INVOICE_ADJ_UPDATE')) continue; //Ignore if the update is from Invoice Adjustments UI

            Invoice_Adjustment__c oldInvAdj = (Invoice_Adjustment__c)Trigger.oldMap.get(newInvAdj.id);
            System.debug('oldInvAdj.Approval_Status__c==' + oldInvAdj.Approval_Status__c + '==newInvAdj.Approval_Status__c==' + newInvAdj.Approval_Status__c +'==newInvAdj.Linked_Invoice__r.Invoice_Type__c==' + newInvAdj.Linked_Invoice__r.Invoice_Type__c);
            if('Pending'.equals(oldInvAdj.Approval_Status__c) && 'Approved'.equals(newInvAdj.Approval_Status__c)){
                    lstAdjLIToUpd.addAll(mapInvAdj.get(newInvAdj.id).Invoice_Adjustment_Line_Items__r);
            }                         
        }
          
        System.debug('Updating Adj LI - ' + lstAdjLIToUpd.size());
        try{
            LeaseWareUtils.setFromTrigger('MR_INVOICE_ADJ_UPDATE');
            if(lstAdjLIToUpd.size() > 0) update lstAdjLIToUpd;
            LeaseWareUtils.unsetTriggers('MR_INVOICE_ADJ_UPDATE');
        }
        catch(DmlException e){                  
            String exMessage = e.getMessage();
            String customValidationString = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
            if(exMessage.contains(customValidationString)){
                    Integer index1 = exMessage.lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                    Integer index2 = exMessage.indexOf(': [');
                    exMessage= exMessage.substring(index1+customValidationString.length()+1,index2);                                           
                    system.debug('Filtered Error Message---'+ exMessage);
            }
            for(Invoice_Adjustment__c tempInvAdj : (List<Invoice_Adjustment__c>)Trigger.new)
            {
                tempInvAdj.addError(exMessage);
            } 
        }
        

        System.debug('InvoiceAdjustmentTriggerHandler::updateAdjustmentLI(-)');
    }

}