public  with sharing class SupplementalRentTransactionsHandler {

	private static List<String> listWithErrors = new List<String>();

	    /* This method is being used from Lightning component RefreshMRTransactions on Supplemental Rent Page to initiate the batch process*/
		@AuraEnabled
		public static void refreshTransactions(Id leaseId){ 
			System.debug('SupplementalRentTransactionsHandler.refreshTransactions(+) --->' + leaseId);
			String prefix = LeaseWareUtils.getNamespacePrefix();
            prefix = prefix.removeEnd('__');
			try {
				ApexClass aC = [SELECT Id,Name FROM ApexClass WHERE Name = 'SupplementalRentTransactionsBatch' AND NamespacePrefix =:prefix];
				System.debug('ApexClass --' + aC);
				// check if there isn't a batch job in status Processing, Queued, Holding or Preparing for this Apex class,only then initiate this batch
				// This is done to avoid concurrent batch runs on refresh of page. 
				if([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND  ApexClassID =:aC.Id AND (Status = 'Processing' OR Status = 'Preparing' OR  Status = 'Queued' OR  Status = 'Holding')] < 1){
					SupplementalRentTransactionsBatch snapshotBatch = new SupplementalRentTransactionsBatch(new Set<Id>{leaseId},true);
					Database.executeBatch(snapshotBatch,SupplementalRentTransactionsBatch.getBatchSize());				
				}
			   } catch (Exception e) {
				System.debug('ERROR: ' + e.getMessage());
				throw new AuraHandledException(e.getMessage());
			}
			System.debug('SupplementalRentTransactionsHandler.refreshTransactions(-)');
		}

	public static List<Supplemental_Rent_Transactions__c> createTransactions( Set<Assembly_MR_Rate__c> setSupplRents, Map<Id,List<Invoice_Adjustment_Line_Item__c>> mapSRIdToInvAdjstmntLineItems){

		System.debug(' SupplementalRentTransactionsHandler.createTransactions(+)');

		System.debug('setSupplRents:' + setSupplRents.size() +'::'+ setSupplRents);
		map<String, list<Supplemental_Rent_Transactions__c> > mapSnapshotsByType = new map<String, list<Supplemental_Rent_Transactions__c> >();
		map<String, list<Supplemental_Rent_Transactions__c> > mapSnapshotsForContrbution = new map<String, list<Supplemental_Rent_Transactions__c> >();

		String recordTypeName ='';
		String nameSpacePrifix = LeaseWareUtils.getNamespacePrefix();
		Map<String,Supplemental_Rent_Transactions__c> mapExistingTransactions = new Map<String,Supplemental_Rent_Transactions__c>();
		list<Supplemental_Rent_Transactions__c> listTransactionToUpdate = new list<Supplemental_Rent_Transactions__c>();
		list<Supplemental_Rent_Transactions__c> listAdjstmentTransacToUpdate = new list<Supplemental_Rent_Transactions__c>();
		list<Supplemental_Rent_Transactions__c> listTransactionsToDelete = new list<Supplemental_Rent_Transactions__c>();
		list<SortTransactionWrapper> listTransactionsToSort = new list<SortTransactionWrapper>();
		set<Id> setInvLineItemsId = new Set<Id>();
		Map<Id,Schema.RecordTypeInfo> paymentFieldMap = new Map<Id,Schema.RecordTypeInfo>();
		Map<Id,Schema.RecordTypeInfo> invoiceFieldMap = new Map<Id,Schema.RecordTypeInfo>();
		map<id, id> invLIToSupplRentUtilMap = new map<id, id>(); //Key: Inv LI Id, Value: Related Suppl Rent Util Id 
		Map<String,Supplemental_Rent_Transactions__c> mapFinalTransactionsToInsert = new Map<String,Supplemental_Rent_Transactions__c>();
		Map<String,Supplemental_Rent_Transactions__c> mapAdjustmentTransactionUpdate = new Map<String,Supplemental_Rent_Transactions__c>();
		Map<String,Supplemental_Rent_Transactions__c> mapAdjustmentTransactions = new Map<String,Supplemental_Rent_Transactions__c>();

		for(Assembly_MR_Rate__c curMRInfo : setSupplRents) {
			for(Invoice_Line_Item__c curInv: curMRInfo.Invoice_Line_items__r) {
				setInvLineItemsId.add(curInv.Id);
			}
		}
		paymentFieldMap = schema.getGlobalDescribe().get(nameSpacePrifix+'Payment__c').getDescribe().getRecordTypeInfosById();
		invoiceFieldMap = schema.getGlobalDescribe().get(nameSpacePrifix+'Invoice__c').getDescribe().getRecordTypeInfosById();

		for(Assembly_MR_Rate__c curMRInfo : setSupplRents) {
			System.debug('listPaymntLineItem for SR :' + curMRInfo.Payment_Line_Items__r.size() +'::'+ curMRInfo.Payment_Line_Items__r);
			System.debug('creating transaction for SR :' + curMRInfo.Name +'::'+ curMRInfo);

			invLIToSupplRentUtilMap.clear(); 
			
			List<Supplemental_Rent_Transactions__c> listDuplicateTrnsctions = new List<Supplemental_Rent_Transactions__c>();
			for(Supplemental_Rent_Transactions__c curRec: curMRInfo.Supplemental_Rent_Transactions__r) {
					String key = '';
					key = setTransactionkey(curRec);

					if(curRec.transaction_Type__c.equals('Adjustment')) {
						mapAdjustmentTransactions.put(key,curRec);
					}
					if(mapExistingTransactions.containsKey(key)){
						listDuplicateTrnsctions.add(curRec);
					}else{
						mapExistingTransactions.put(key,curRec);
					}
				}

				// This is to ensure that if for some reason a duplicate transaction gets created , Refresh MR Transactions deletes those.  
			if(listDuplicateTrnsctions.size()>0){
				System.debug('listDuplicateTrnsctions :' + listDuplicateTrnsctions);
				LeaseWareUtils.TriggerDisabledFlag = true;
				try{
					delete listDuplicateTrnsctions ;
				}catch(Exception e){
					System.debug('Error deleting duplicate transaction : ' + e.getMessage());
					LeaseWareUtils.createExceptionLog(e, e.getMessage(),'Supplemental Rent Transactions','','', true);
				}
				LeaseWareUtils.TriggerDisabledFlag = false;
			}
			System.debug('mapExistingTransactions :' + mapExistingTransactions.size());
			System.debug('mapExistingTransactions :' + mapExistingTransactions);
			System.debug('mapAdjustmentTransactions :' + mapAdjustmentTransactions.size());

			//---------------------OPENING BALANCE-----------------------------------------
			System.debug('---------------------OPENING BALANCE---------------');
			SortTransactionWrapper newSRT = new SortTransactionWrapper ();
			newSRT.documentId = curMRInfo.Id;
			if(curMRInfo.Opening_Balance_Date_Override__c != null){
				newSRT.Name = LeaseWareUtils.getDatetoString(curMRInfo.Opening_Balance_Date_Override__c,'YYYY-MM-DD') +'-'+'Opening Balance';
				newSRT.documentDate = curMRInfo.Opening_Balance_Date_Override__c;
			}else{
				newSRT.Name = LeaseWareUtils.getDatetoString(curMRInfo.Lease__r.Lease_Start_Date_New__c,'YYYY-MM-DD') +'-'+'Opening Balance';
				newSRT.documentDate = curMRInfo.Lease__r.Lease_Start_Date_New__c;
			}
			newSrt.createdDate = curMRInfo.createdDate;
			newSRT.transactionType = 'Opening Balance';
			listTransactionsToSort.add(newSRT);
			System.debug('SR---listTransactionsToSort--' + listTransactionsToSort);
			//----------------------------------MR Invoice Adjustment-------------------------
			if(!mapSRIdToInvAdjstmntLineItems.isEmpty() && mapSRIdToInvAdjstmntLineItems.containsKey(curMRInfo.Id)){
				for(Invoice_Adjustment_Line_Item__c curInvAdjtLineItm : mapSRIdToInvAdjstmntLineItems.get(curMRInfo.Id)){
					System.debug('--------------------MR Invoice Adjustment--------------');
						SortTransactionWrapper newInvAdjstLI = new SortTransactionWrapper ();
						newInvAdjstLI.Name = LeaseWareUtils.getDatetoString(curInvAdjtLineItm.Invoice_Adjustment__r.Adjustment_Date__c,'YYYY-MM-DD')+'-'+'MR Invoice Adjustment';
						newInvAdjstLI.documentId = curInvAdjtLineItm.Id;
						newInvAdjstLI.documentDate = curInvAdjtLineItm.Invoice_Adjustment__r.Adjustment_Date__c;
						newInvAdjstLI.createdDate = curInvAdjtLineItm.createdDate;
						newInvAdjstLI.transactionType = 'MR Invoice Adjustment';
						listTransactionsToSort.add(newInvAdjstLI);
						System.debug('listTransactionsToSort--' + listTransactionsToSort);
				}	
			}
			//---------------------MR Payment-----------------------------------------
				for(Payment_Line_Item__c curPaymnt : curMRInfo.Payment_Line_Items__r) {
					System.debug('---------------------MR Payment---------------');
					System.debug('curPaymnt:' + curPaymnt);
					String transactionType = '';
					recordTypeName = paymentFieldMap.get(curPaymnt.Payment__r.RecordTypeId).getDeveloperName();
					System.debug('RecordTypeName :' + recordTypeName);
					if('Credit_Memo_MR'.equals(recordTypeName) && curPaymnt.Payment__r.Payment_Type__c.equals('CN_Payment')) {
						System.debug('Check Credit Memo :' + curPaymnt.Payment__r.Credit_Memo__r.Name +'---'+curPaymnt.Payment__r.Credit_Memo__r.Y_Hidden_IsStandalone__c);
						if(curPaymnt.Payment__r.Credit_Memo__c != null){													
							if(curPaymnt.Payment__r.Credit_Memo__r.Y_Hidden_IsStandalone__c){
								transactionType = 'MR Payment Credit Applied';
							}else{
								transactionType = 'MR Invoice Credit (Adjustment)';
							}
						}
					}else if(String.isNotBlank(curPaymnt.Payment__r.Payment_Type__c) && (curPaymnt.Payment__r.Payment_Type__c.equals('Cash'))) {
						transactionType = 'MR Cash Payment';
					}else if(String.isNotBlank(curPaymnt.Payment__r.Payment_Type__c) && (curPaymnt.Payment__r.Payment_Type__c.contains('Offset'))) {
						transactionType = 'MR Payment Offset';
					}
					System.debug('MR Payment: TYpe:' + transactionType);
					SortTransactionWrapper newPymntSRT = new SortTransactionWrapper ();
					newPymntSRT.Name = LeaseWareUtils.getDatetoString(curPaymnt.Payment_Date__c,'YYYY-MM-DD')+'-'+transactionType;
					newPymntSRT.documentId = curPaymnt.Id;
					newPymntSRT.documentDate = curPaymnt.Payment_Date__c;
					newPymntSRT.createdDate = curPaymnt.createdDate;
					newPymntSRT.transactionType = transactionType;
					listTransactionsToSort.add(newPymntSRT);
				}
			//---------------------MR Invoice-----------------------------------------
				for(Invoice_Line_Item__c curInv : curMRInfo.Invoice_Line_Items__r) {
					System.debug('---------------------MR Invoice---------------');
					recordTypeName = invoiceFieldMap.get(curInv.Invoice__r.RecordTypeId).getDeveloperName();
					System.debug('Invoice RecordTypeName :' + recordTypeName);
					String transactionType = '';
					if(recordTypeName.equals('MR_Reconciliation')){
						transactionType = 'MR Invoice Reconciliation';
					}else{
						transactionType = 'MR Invoice';						
					}
					System.debug('Invoice Transaction type :' + transactionType);
					invLIToSupplRentUtilMap.put(curInv.Id, curInv.Assembly_Utilization__c);
					SortTransactionWrapper newInvSRT = new SortTransactionWrapper ();
					newInvSRT.Name =  LeaseWareUtils.getDatetoString(curInv.Invoice_Date__c,'YYYY-MM-DD')+'-'+transactionType;
					newInvSRT.documentId = curInv.Id;
					newInvSRT.documentDate = curInv.Invoice_Date__c;
					newInvSRT.createdDate = curInv.createdDate;
					newInvSRT.transactionType = transactionType;					
					listTransactionsToSort.add(newInvSRT);
				}
			//---------------------MR Claim-----------------------------------------
				System.debug('---------------------MR Claim---------------');
				for(Payable__c curEventInv: curMRInfo.Payables__r){
					System.debug('Event Payable Id :' + curEventInv.Id);
					SortTransactionWrapper newEventInvSRT = new SortTransactionWrapper ();
					newEventInvSRT.Name =  LeaseWareUtils.getDatetoString(curEventInv.Payable_Date__c,'YYYY-MM-DD')+'-'+'MR Claim';
					newEventInvSRT.documentId = curEventInv.Id;
					newEventInvSRT.documentDate = curEventInv.Payable_Date__c;
					newEventInvSRT.createdDate = curEventInv.createdDate;
					newEventInvSRT.transactionType = 'MR Claim';
					listTransactionsToSort.add(newEventInvSRT);

					System.debug('Contribution Amount :' + curEventInv.Lessor_Contribution_Committed__c);
					String key2 = curEventInv.Id+'-'+'MR Contribution Applied';
					if(mapExistingTransactions.containsKey(key2) || (curEventInv.Lessor_Contribution_Committed__c != null && curEventInv.Lessor_Contribution_Committed__c != 0.0)) {
						System.debug('---------------------MR Contribution Applied---------------');
						SortTransactionWrapper newEventInvSRT2 = new SortTransactionWrapper ();
						newEventInvSRT2.Name =  LeaseWareUtils.getDatetoString(curEventInv.Payable_Date__c,'YYYY-MM-DD')+'-'+'MR Contribution Applied';
						newEventInvSRT2.documentId = curEventInv.Id;
						newEventInvSRT2.documentDate = curEventInv.Payable_Date__c;
						newEventInvSRT2.createdDate = curEventInv.createdDate;
						newEventInvSRT2.transactionType = 'MR Contribution Applied';
						listTransactionsToSort.add(newEventInvSRT2);

						if('Paid'.equals(curEventInv.Payment_Status__c) && (curEventInv.Payable_Balance__c == 0)){
							System.debug('---------------------MR Contribution Utilized---------------');
							SortTransactionWrapper newEventInvSRT3 = new SortTransactionWrapper ();
							newEventInvSRT3.Name =  LeaseWareUtils.getDatetoString(curEventInv.Payable_Date__c,'YYYY-MM-DD')+'-'+'MR Contribution Used';
							newEventInvSRT3.documentId = curEventInv.Id;
							newEventInvSRT3.documentDate = curEventInv.Last_Payout_Date__c;
							newEventInvSRT3.createdDate = curEventInv.createdDate;
							newEventInvSRT3.transactionType = 'MR Contribution Used';
							listTransactionsToSort.add(newEventInvSRT3);
						}
					}
				}

				System.debug('---------------------MR Claim Refund/MR Claim Offset---------------');
				for(Payout__c curEventPO: curMRInfo.Payouts__r){
					System.debug('Event Payout Id :' + curEventPO.Id);

					if('Cash'.equals(curEventPO.Payout_Type__c)){
						System.debug('---------------------MR Claim Refund---------------');
						SortTransactionWrapper newEventInvSRT4 = new SortTransactionWrapper ();
						newEventInvSRT4.Name =  LeaseWareUtils.getDatetoString(curEventPO.Payout_Date__c,'YYYY-MM-DD')+'-'+'MR Claim Refund';
						newEventInvSRT4.documentId = curEventPO.Id;
						newEventInvSRT4.documentDate = curEventPO.Payout_Date__c;
						newEventInvSRT4.createdDate = curEventPO.createdDate;
						newEventInvSRT4.transactionType = 'MR Claim Refund';
						listTransactionsToSort.add(newEventInvSRT4);
					}
					else if('Offset'.equals(curEventPO.Payout_Type__c)){
						System.debug('---------------------MR Claim Offset---------------');
						SortTransactionWrapper newEventInvSRT5 = new SortTransactionWrapper ();
						newEventInvSRT5.Name =  LeaseWareUtils.getDatetoString(curEventPO.Payout_Date__c,'YYYY-MM-DD')+'-'+'MR Claim Refund';
						newEventInvSRT5.documentId = curEventPO.Id;
						newEventInvSRT5.documentDate = curEventPO.Payout_Date__c;
						newEventInvSRT5.createdDate = curEventPO.createdDate;
						newEventInvSRT5.transactionType = 'MR Claim Offset';
						listTransactionsToSort.add(newEventInvSRT5);
					}					
				}
				
			//-----------------------MR Fund Transfer--------------------------------
				System.debug('---------------------MR Fund Transfer Target---------------');
				for(Assembly_MR_Fund_Transfer__c targetTrnsfer : curMRInfo.Assembly_MR_Fund_Transfers1__r) {

					SortTransactionWrapper targetTrnsc = new SortTransactionWrapper ();
					targetTrnsc.Name =  LeaseWareUtils.getDatetoString(targetTrnsfer.MR_Fund_Transfer__r.Date__c,'YYYY-MM-DD')+'-'+'MR Transfer';
					targetTrnsc.documentId = targetTrnsfer.Id;
					targetTrnsc.documentDate = targetTrnsfer.MR_Fund_Transfer__r.Date__c;
					targetTrnsc.createdDate = targetTrnsfer.createdDate;
					targetTrnsc.transactionType = 'MR Transfer';
					listTransactionsToSort.add(targetTrnsc);
				}
				System.debug('---------------------MR Fund Transfer Source---------------');
				for(Assembly_MR_Fund_Transfer__c sourceTrnsfer : curMRInfo.Assembly_MR_Fund_Transfers__r) {

					SortTransactionWrapper sourceTrnsc = new SortTransactionWrapper ();
					sourceTrnsc.Name =  LeaseWareUtils.getDatetoString(sourceTrnsfer.MR_Fund_Transfer__r.Date__c,'YYYY-MM-DD')+'-'+'MR Transfer';
					sourceTrnsc.documentId = sourceTrnsfer.Id;
					sourceTrnsc.documentDate = sourceTrnsfer.MR_Fund_Transfer__r.Date__c;
					sourceTrnsc.createdDate = sourceTrnsfer.createdDate;
					sourceTrnsc.transactionType = 'MR Transfer';
					listTransactionsToSort.add(sourceTrnsc);
				}
			//---------------------Adjustment-----------------------------------------
				System.debug('---------------------Adjustment---------------');
				for(Supplemental_Rent_Adjustment__c curAdjustmnt : curMRInfo.Supplemental_Rent_Adjustments__r) {
					SortTransactionWrapper newAdjustment = new SortTransactionWrapper ();
					newAdjustment.Name =  LeaseWareUtils.getDatetoString(curAdjustmnt.Date__c,'YYYY-MM-DD')+'-'+'Adjustment';
					newAdjustment.documentId = curAdjustmnt.Id;
					newAdjustment.documentDate = curAdjustmnt.Date__c;
					newAdjustment.createdDate = curAdjustmnt.createdDate;
					newAdjustment.transactionType = 'Adjustment';
					listTransactionsToSort.add(newAdjustment);
				}
			//---------------------MR Invoice Overpayment Credit-----------------------------------------
				System.debug('---------------------MR Invoice Overpayment Credit---------------');
				for(Credit_memo_Issuance__c curCreditMemo : curMRInfo.Credit_Memo_Issuances__r) {
					SortTransactionWrapper newCreditMemo = new SortTransactionWrapper ();
					newCreditMemo.Name =  LeaseWareUtils.getDatetoString(curCreditMemo.Credit_memo__r.Credit_memo_Date__c,'YYYY-MM-DD')+'-'+'MR Invoice Overpayment Credit';
					newCreditMemo.documentId = curCreditMemo.Id;
					newCreditMemo.documentDate = curCreditMemo.Credit_memo__r.Credit_memo_Date__c;
					newCreditMemo.createdDate = curCreditMemo.createdDate;
					newCreditMemo.transactionType = 'MR Invoice Overpayment Credit';
					listTransactionsToSort.add(newCreditMemo);
				}
			//sort the final insert list on Document Date/created Date
			List<TransactionWrapper> transactionList = new List<TransactionWrapper>();
			for(SortTransactionWrapper curTransac : listTransactionsToSort) {	
				transactionList.add(new TransactionWrapper(curTransac));
			}
			System.debug('UNSorted list :' + listTransactionsToSort);
			transactionList.sort();
			listTransactionsToSort.clear();
			for(TransactionWrapper curRec:transactionList) {
				listTransactionsToSort.add(curRec.curTransaction);
			}
			//create Supplemental Rent TRansaction records from Wrapper

			createdSortedTransactionList(listTransactionsToSort,listTransactionToUpdate,curMRInfo, invLIToSupplRentUtilMap);
			System.debug('Sorted list 3:' + listTransactionToUpdate);
			// set sequence number on the sorted final list records
			for(integer i=0; i<listTransactionToUpdate.size(); i++) {
				String key = '';
				Supplemental_Rent_Transactions__c curRec = listTransactionToUpdate[i];
				key = setTransactionkey(curRec);
				curRec.sequence_Number__c = i;

				System.debug('Adding rec to final map Key :' + key);
				System.debug('Adding rec to final map :' + curRec);
				mapFinalTransactionsToInsert.put(key, curRec);

				if(mapExistingTransactions.containsKey(key)) {
					System.debug('Transaction existing in map :');
					Supplemental_Rent_Transactions__c oldTransac = mapExistingTransactions.get(key);
					if(oldTransac != null) {
						System.debug('oldTransac:' + oldTransac.sequence_Number__c +'---'+ oldTransac);
						System.debug('currentTransac:' + curRec.sequence_Number__c +'---'+ curRec);
						if((oldTransac.sequence_Number__c == curRec.sequence_Number__c)) {
							mapFinalTransactionsToInsert.remove(key);
							continue;
						}else if(oldTransac.Transaction_Type__c.equals('Adjustment')) {
							System.debug('Adjustment transaction--- updating old one----');
							mapFinalTransactionsToInsert.remove(key);
							oldTransac.sequence_Number__c = curRec.sequence_Number__c;
							System.debug('oldTransac:' + oldTransac.sequence_Number__c);
							mapAdjustmentTransactionUpdate.put(key,oldTransac);
						}else if(!oldTransac.Transaction_Type__c.equals('Adjustment')) {
							listTransactionsToDelete.add(oldTransac);
							System.debug('Added to delete list :' + listTransactionsToDelete.size());
							System.debug('Added to delete list :' + listTransactionsToDelete);
						}
					}
				}
				System.debug('Updated final map :' + mapFinalTransactionsToInsert);
			}

			// check for extra transaction records in existing transactions and add them to delete
			if(mapExistingTransactions.values().size() > listTransactionToUpdate.size()) {
				System.debug('----Some old transaction to be deleted----');
				Map<String,Supplemental_Rent_Transactions__c> mapNewSet = new Map<String,Supplemental_Rent_Transactions__c>();
				for(Supplemental_Rent_Transactions__c newTransac : listTransactionToUpdate) {
					String key = setTransactionkey(newTransac);
					mapNewSet.put(key,newTransac);
				}
				for(String oldTransac : mapExistingTransactions.keySet()) {
					if(!mapNewSet.keyset().contains(oldTransac)) {
						listTransactionsToDelete.add(mapExistingTransactions.get(oldTransac));
						System.debug('Added to delete list :' + mapExistingTransactions.get(oldTransac));
					}
				}
			}
			// System.debug('Final delete list :' + listTransactionsToDelete.size());
			System.debug('Final delete list :' + listTransactionsToDelete);
			// System.debug('mapFinalTransactionsToInsert:' + mapFinalTransactionsToInsert.size());
			System.debug('mapFinalTransactionsToInsert:' + mapFinalTransactionsToInsert);
			System.debug('mapAdjustmentTransactionUpdate:' + mapAdjustmentTransactionUpdate);
			listTransactionToUpdate.clear();
			listTransactionsToSort.clear();
		}
		if(mapAdjustmentTransactionUpdate.size()>0) {
			//LeaseWareUtils.TriggerDisabledFlag = true;
			try{
				update mapAdjustmentTransactionUpdate.values();
			}catch(Exception e) {
				System.debug('ERROR:' + e);
				LeaseWareUtils.createExceptionLog(e,e.getMessage(),'Supplemental Rent Transactions','','Updating Adjustment transactions',true);
			}
			//LeaseWareUtils.TriggerDisabledFlag = false;
		}
		if(listTransactionsToDelete.size()>0) {
			LeaseWareUtils.TriggerDisabledFlag = true;
			try{
				delete listTransactionsToDelete;
			}catch(Exception e) {
				System.debug('ERROR:' + e);
				LeaseWareUtils.createExceptionLog(e,e.getMessage(),'Supplemental Rent Transactions','','Deleting incorrect transactions',true);
			}
			LeaseWareUtils.TriggerDisabledFlag = false;
		}
		System.debug(' SupplementalRentTransactionsHandler.createTransactions(-)');
		return mapFinalTransactionsToInsert.values();
	}
// Method to update Previous Transaction link and running balance amount on all Transactions
// Called from SupplementalRentTransactionsBatch.execute method
// called from AssemblyMRRate.aftInsUpd method.
	public static void updatePreviousTransaction(Set<Id> setSupplementalRentsId){
		System.debug('SupplementalRentTransactionsBatch.updatePreviousTransaction(+)');
		Map<Id,Supplemental_Rent_Transactions__c> mapTransactionToUpdate = new Map<Id, Supplemental_Rent_Transactions__c>();
		List<Supplemental_Rent_Transactions__c> listSupplementalRentTransactions = new List<Supplemental_Rent_Transactions__c>();
		try{
			//System.QueryException: Inline query has too many rows for direct assignment, use FOR loop ---
			// this exception comes when assigning query directly to a list
			for(Assembly_MR_Rate__c curSR : [SELECT Id, Name,Fund_Code__c,(Select id, Name,
											Amount_Invoiced__c,Amount_Cash__c,Running_Balance_Invoiced__c,Running_Balance_Cash_Collected__c,
											Running_Balance_Cash__c,Running_Balance_Invoice__c,Amount_cash_Sum__c,Amount_Invoiced_Sum__c,
											Transaction_Type__c,Previous_Transaction__c,Sequence_Number__c,Supplemental_Rent__c
											FROM Supplemental_Rent_Transactions__r order by Sequence_Number__c asc) FROM Assembly_MR_Rate__c where RecordType.DeveloperName ='MR'
											AND Id in :setSupplementalRentsId FOR UPDATE ]) {
				System.debug('Setting prev transaction ------');
				List<Supplemental_Rent_Transactions__c>  listSRT = new List<Supplemental_Rent_Transactions__c>();
				List<Supplemental_Rent_Transactions__c>  listExistingSRT = curSR.Supplemental_Rent_Transactions__r;
				System.debug('listExistingSRT :' + listExistingSRT);
				if(listExistingSRT.size()>0) {
					listExistingSRT[0].Running_Balance_Cash__c = listExistingSRT[0].Amount_cash_Sum__c;
					listExistingSRT[0].Running_Balance_Invoice__c = listExistingSRT[0].Amount_Invoiced_Sum__c;
					mapTransactionToUpdate.put(listExistingSRT[0].Id,listExistingSRT[0]);
				}
				for(integer i = 1; i<listExistingSRT.size(); i++) {
					System.debug('currentTransac :' + i);
					Supplemental_Rent_Transactions__c currentTransac = listExistingSRT[i];
					currentTransac.Previous_Transaction__c = listExistingSRT[i-1].Id;
					currentTransac.Running_Balance_Cash__c = LeasewareUtils.zeroIfnull(listExistingSRT[i-1].Running_Balance_Cash__c) + LeasewareUtils.zeroIfnull(currentTransac.Amount_cash_Sum__c);
					currentTransac.Running_Balance_Invoice__c = LeasewareUtils.zeroIfnull(listExistingSRT[i-1].Running_Balance_Invoice__c) + LeasewareUtils.zeroIfnull(currentTransac.Amount_Invoiced_Sum__c);
					mapTransactionToUpdate.put(currentTransac.Id,currentTransac);
				}
			}
		}catch (Exception ex)
        {
            System.debug('Exception occured from updatePreviousTransaction method in SR Transaction Batch :'+ ex.getMessage());
            LeaseWareUtils.createExceptionLog(ex, 'Exception occured from updatePreviousTransaction method in SR Transaction Batch : '+ ex.getMessage(), null, null,'Supplemental Rent Transactions',true);
        }
		LeaseWareUtils.TriggerDisabledFlag = true;
		if(mapTransactionToUpdate.values().size()>0) {
			System.debug('mapTransactionToUpdate from updatePreviousTransaction:' + mapTransactionToUpdate.size() +'::'+mapTransactionToUpdate );
			update mapTransactionToUpdate.values();
		}
		LeaseWareUtils.TriggerDisabledFlag = false;
		System.debug('SupplementalRentTransactionsBatch.updatePreviousTransaction(-)');
	}

	private static List<Supplemental_Rent_Transactions__c> createdSortedTransactionList(List<SortTransactionWrapper> listTransactionsToSort,
	                                                                                    List<Supplemental_Rent_Transactions__c> listTransactionToUpdate,Assembly_MR_Rate__c curMRInfo,
																						map<id, id> invLIToSupplRentUtilMap){
		for(SortTransactionWrapper sortedTransc : listTransactionsToSort) {
			if(sortedTransc.transactionType.equals('Opening Balance')) {
				System.debug('---------Setting Opening Balance--------------');
				Supplemental_Rent_Transactions__c newSRT1 = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					RecordId__c = String.valueOf(curMRInfo.id),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTRansc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newSRT1);
			}else if(sortedTransc.transactionType.equals('MR Cash Payment') || sortedTransc.transactionType.equals('MR Payment Credit Applied') || 
			sortedTransc.transactionType.equals('MR Invoice Credit (Adjustment)' )
			         || sortedTransc.transactionType.equals('MR Payment Offset')) {
				System.debug('---------Setting MR Payment--------------' + sortedTransc.transactionType);
				Supplemental_Rent_Transactions__c newPymmntSRT1 = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Payment_Line_Item__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTRansc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newPymmntSRT1);
			}else if(sortedTransc.transactionType.equals('MR Invoice') || sortedTransc.transactionType.equals('MR Invoice Reconciliation')) {
				System.debug('---------Setting MR Invoice--------------');
				Supplemental_Rent_Transactions__c newInvSRT1 = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Invoice_Line_Item__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTRansc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c,
					Supplemental_Rent_Utilization__c = invLIToSupplRentUtilMap.get(sortedTransc.documentId));
				listTransactionToUpdate.add(newInvSRT1);
			}else if(sortedTransc.transactionType.equals('MR Contribution Applied') || sortedTransc.transactionType.equals('MR Claim')
			         || sortedTransc.transactionType.equals('MR Contribution Used')){
				Supplemental_Rent_Transactions__c newEventSRT1 = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Event_Payable__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTRansc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newEventSRT1);
			}else if (sortedTransc.transactionType.equals('MR Claim Refund') || sortedTransc.transactionType.equals('MR Claim Offset')) {
				System.debug('---------Setting MR Claim Refund/Offset--------------');
				Supplemental_Rent_Transactions__c newEventSRT2 = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Event_Payout__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTRansc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newEventSRT2);
			}else if(sortedTransc.transactionType.equals('MR Transfer')) {
				System.debug('---------Setting MR Transfer--------------');
				Supplemental_Rent_Transactions__c newTransferSRT = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Assembly_MR_Fund_Transfer__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					//MR_Fund_Transfer__c = sortedTransc.MR_Fund_Transfer__c,
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTRansc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newTransferSRT);
			}else if(sortedTransc.transactionType.equals('Adjustment')) {
				System.debug('---------Setting Adjustment--------------');
				Supplemental_Rent_Transactions__c newAdjstmntSRT = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Supplemental_Rent_Adjustment__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTransc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newAdjstmntSRT);
			}else if(sortedTransc.transactionType.equals('MR Invoice Overpayment Credit')) {
				System.debug('---------MR Invoice Overpayment Credit--------------');
				Supplemental_Rent_Transactions__c newCreditMemoSRT = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Credit_Memo_Issuance__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTransc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newCreditMemoSRT);
			}else if(sortedTransc.transactionType.equals('MR Invoice Adjustment')) {
				System.debug('---------MR Invoice Adjustment--------------');
				Supplemental_Rent_Transactions__c newInvoiceAdjustment = new Supplemental_Rent_Transactions__c(
					Name = sortedTRansc.Name,
					Supplemental_Rent__c = curMRInfo.id,
					Invoice_Adjustment_Line_Item__c = sortedTransc.documentId,
					RecordId__c = String.valueOf(sortedTransc.documentId),
					Fund_Code_Lkp__c = curMRInfo.Fund_Code__c,
					Transaction_Type__c = sortedTransc.transactionType,
					Lease_Start_Date__c = curMRInfo.Lease__r.Lease_Start_Date_New__c);
				listTransactionToUpdate.add(newInvoiceAdjustment);
				System.debug('Adding InvAdjLI :' + listTransactionToUpdate);
			}
		}
		return listTransactionToUpdate;
	}

	private static string setTransactionkey(Supplemental_Rent_Transactions__c transac){
		String key = '';
		if(transac.Transaction_Type__c.equals('Opening Balance')) {key = transac.Supplemental_Rent__c+'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('MR Cash Payment') || transac.Transaction_Type__c.equals('MR Payment Credit Applied') || transac.Transaction_Type__c.equals('MR Invoice Credit (Adjustment)')
		   || transac.Transaction_Type__c.equals('MR Payment Offset')  ) {key = transac.Payment_Line_Item__c +'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('MR Invoice') || transac.Transaction_Type__c.equals('MR Invoice Reconciliation')) {key = transac.Invoice_Line_Item__c+'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('MR Invoice Overpayment Credit')) {key = transac.Credit_Memo_Issuance__c+'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('MR Contribution Applied') ||transac.Transaction_Type__c.equals('MR Contribution Used')
		   || transac.Transaction_Type__c.equals('MR Claim'))
		{key = transac.Event_Payable__c+'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('MR Claim Refund') || transac.Transaction_Type__c.equals('MR Claim Offset')){key = transac.Event_Payout__c+'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('Adjustment')) {key = transac.Supplemental_Rent_Adjustment__c+'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('MR Transfer')) {key = transac.Assembly_MR_Fund_Transfer__c+'-'+transac.Transaction_Type__c;}
		if(transac.Transaction_Type__c.equals('MR Invoice Adjustment')) {key = transac.Invoice_Adjustment_Line_Item__c+'-'+transac.Transaction_Type__c;}
		return key;
	}


	private class TransactionWrapper implements Comparable {

		public SortTransactionWrapper curTransaction;

		public TransactionWrapper (SortTransactionWrapper curTransaction){
			//System.debug('---In compare to constructor-------');
			this.curTransaction = curTransaction;
		}
		public Integer compareTo(Object compareTo){
			//System.debug('---In compare to-------');
			TransactionWrapper compareToTransaction = (TransactionWrapper)compareTo;
			if(curTransaction.documentDate > compareToTransaction.curTransaction.documentDate) {
				return 1;
			}else if(curTransaction.documentDate < compareToTransaction.curTransaction.documentDate) {
				return -1;
			}else {
				//System.debug('---In compare to Else-------');
				// TransactionDate is same so consider createdDate
				if(curTransaction.createdDate > compareToTransaction.curTransaction.createdDate) {
					return 1;
				}else if(curTransaction.createdDate < compareToTransaction.curTransaction.createdDate) {
					return -1;
				}else{
					return 0;
				}
			}
		}
	}

	    /*Method to check transactions created for all Leases, and correct or create the wrong and missing ones
    Executes the batch class SupplementalRentTransactionsBatch for creating Supplemental Rent Transactions
	Called from UpdateRentOnLeaseScheduler to be executed everyday*/
    public static void updateTransactions(){
        system.debug('SupplementalRentTransactionsHandler.updateTransactions (+)');
        Map<Id,Lease__c> mapLeases = new Map<Id,Lease__c>([select id from lease__c where Active__c = true]);
        SupplementalRentTransactionsBatch snapshotBatch = new SupplementalRentTransactionsBatch(mapLeases.keySet(),true);
        Database.executeBatch(snapshotBatch,SupplementalRentTransactionsBatch.getBatchSize());   
        system.debug('SupplementalRentTransactionsHandler.updateTransactions (-))');  
    } 

	@TestVisible
	public class SortTransactionWrapper {
		@TestVisible public SortTransactionWrapper(){
		}
		@TestVisible String Name;
		@TestVisible String documentId;
		@TestVisible String transactionType;
		@TestVisible Date documentDate;
		@TestVisible Datetime createdDate;
	}

	@TestVisible
	public class FailedRecord {
		@TestVisible public FailedRecord(){
		}
		@TestVisible public Id DocumentName  {get; set;}
		@TestVisible public String Name {get; set;}
		@TestVisible public String SupplementalRentId {get; set;}
		@TestVisible public String SupplementalRentName {get; set;}
		@TestVisible public String DocumentDate {get; set;}
		@TestVisible public String DocumentStatus {get; set;}
		@TestVisible public String Type {get; set;}
		@TestVisible public String FailedReason {get; set;}
	}
}