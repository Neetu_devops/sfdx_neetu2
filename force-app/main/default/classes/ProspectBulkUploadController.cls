public class ProspectBulkUploadController {
    
    @auraEnabled
    public static ProspectDataWrapper getProspectData(Boolean airlineCheck,Boolean lessorCheck,Boolean counterPartyCheck){
        
        ProspectDataWrapper dataWrapper = new ProspectDataWrapper();        
        
        if(airlineCheck){
            List<operator__c> airlines = [select id, Name from operator__c Order by Name ];
            
            if(airlines.size()>0){
                List<AirLineWrapper> arlineWrapperList = new List<AirLineWrapper>();
                
                for(operator__c obj : airlines){
                    AirLineWrapper wrapperObj = new AirLineWrapper(obj.Id, obj.Name);
                    arlineWrapperList.add(wrapperObj);
                }
                dataWrapper.airlines = arlineWrapperList;
            }
        }
        
        if(lessorCheck){
            List<counterParty__c> lessors = [select id, Name from counterParty__c Order by Name];
            
            if(lessors.size()>0){
                List<LessorWrapper> lessorWrapperList = new List<LessorWrapper>();
                
                for(counterParty__c obj : lessors){
                    LessorWrapper wrapperObj = new LessorWrapper(obj.Id, obj.Name);
                    lessorWrapperList.add(wrapperObj);
                }
                dataWrapper.lessors = lessorWrapperList;
            }
        }
        
        if(counterPartyCheck){
            List<bank__c> counterparties = [select id, Name from bank__c Order by Name];
            
            if(counterparties.size()>0){
                List<CounterPartyWrapper> counterPartyWrapperList = new List<CounterPartyWrapper>();
                
                for(bank__c obj : counterparties){
                    CounterPartyWrapper wrapperObj = new CounterPartyWrapper(obj.Id, obj.Name);
                    counterPartyWrapperList.add(wrapperObj);
                }
                dataWrapper.counterparties = counterPartyWrapperList;
            }
        }
        system.debug('dataWrapperdataWrapper'+dataWrapper);
        return dataWrapper;
    }
    // Check Trading Profile of Prospect
    @auraEnabled
    public static Map<string, TradingProfileWrapper> CheckTradingProfileofProspect(String airlines, String lessors, String counterparties, String aircrafts){
        
        List<AirLineWrapper> airlinesList = (List<AirLineWrapper>) JSON.deserialize(airlines, List<AirLineWrapper>.class);
        List<LessorWrapper> lessorsList = (List<LessorWrapper>) JSON.deserialize(lessors, List<LessorWrapper>.class);
        List<CounterPartyWrapper> counterPartiesList = (List<CounterPartyWrapper>) JSON.deserialize(counterparties, List<CounterPartyWrapper>.class);
        List<Aircraft__c> aircraftsList = (List<Aircraft__c>) JSON.deserialize(aircrafts, List<Aircraft__c>.class);
      
        // Created Json to Send
        string jsonData = '{"prospectList": [';
        
        if(airlinesList.size()>0){
            for(AirLineWrapper obj : airlinesList){
                jsonData += '{"prospectType": "Operator","prospectID": "'+obj.recordId+'"},';
            }
        }
        
        if(lessorsList.size()>0){
            for(LessorWrapper obj : lessorsList){
                jsonData += '{"prospectType": "Lessor","prospectID": "'+obj.recordId+'"},';
            }
        }
        
        if(counterPartiesList.size()>0){
            for(CounterPartyWrapper obj : counterPartiesList){
                jsonData += '{"prospectType": "Counterparty","prospectID": "'+obj.recordId+'"},';
            }
        }
        
        if (jsonData.endsWith(',')){
            jsonData = jsonData.removeEnd(',');
        }
        
        jsonData += '],"assetList": [';
        
        for(Aircraft__c obj : aircraftsList){
            jsonData += '{"assetID": "'+obj.Id+'"},';
        }
        
        if (jsonData.endsWith(',')){
            jsonData = jsonData.removeEnd(',');
        }
        
        jsonData += ']}';
        
        LW_Setup__c campaignSetting =   LW_Setup__c.getInstance('Client Controller Name');
        
        if(campaignSetting != null && campaignSetting.Value__c != null){
            TradingProfileDataSource dsc = (TradingProfileDataSource) Type.forName(campaignSetting.Value__c).newInstance();
            string jsonDummi = dsc.getData(jsonData);
            
            map<string, TradingProfileWrapper> checkprofileMap = new Map<string, TradingProfileWrapper>();
            ResultWrapper resultWrapperObj = (ResultWrapper) System.JSON.deserialize(jsonDummi, ResultWrapper.class);
            
            for(ResultListWrapper obj : resultWrapperObj.resultList){
                checkprofileMap.put(obj.prospectID, new TradingProfileWrapper(obj.profileOk, obj.details));
            }
            return checkprofileMap;
        }
        return null;
    }
    
    public class ProspectDataWrapper {
        
        @auraEnabled public List<AirLineWrapper> airlines {get;set;}
        @auraEnabled public List<LessorWrapper> lessors {get;set;}
        @auraEnabled public List<CounterPartyWrapper> counterparties{get;set;}
        
        public ProspectDataWrapper(){
            airlines = new List<AirLineWrapper>();
            lessors = new List<LessorWrapper>();
            counterparties = new List<CounterPartyWrapper>();
        }
    } 
    
    public class AirLineWrapper{
        @auraEnabled public Id recordId {get;set;}
        @auraEnabled public String recordName {get;set;}
        @auraEnabled public boolean isChecked {get;set;}
        
        public AirLineWrapper(Id recordId,String recordName){
            this.recordId = recordId;
            this.recordName = recordName;
            this.isChecked = false;
        }
    }
    
    public class LessorWrapper{
        @auraEnabled public Id recordId {get;set;}
        @auraEnabled public String recordName {get;set;}
        @auraEnabled public boolean isChecked {get;set;}
        
        public LessorWrapper(Id recordId,String recordName){
            this.recordId = recordId;
            this.recordName = recordName;
            this.isChecked = false;
        }
    }
    
    public class CounterPartyWrapper{
        @auraEnabled public Id recordId {get;set;}
        @auraEnabled public String recordName {get;set;}
        @auraEnabled public boolean isChecked {get;set;}
        
        public CounterPartyWrapper(Id recordId,String recordName){
            this.recordId = recordId;
            this.recordName = recordName;
            this.isChecked = false;
        }
    }
    
    public class TradingProfileWrapper {
        @auraEnabled public boolean profileOk {get;set;}
        @auraEnabled public string details {get;set;}
        
        public TradingProfileWrapper(boolean profileOk,string details){
            this.profileOk = profileOk;
            this.details = details;
            
        }
        
    }
    
    public class ResultWrapper {
        public List<ResultListWrapper> resultList;
    }
    
    public class ResultListWrapper {
    public Id prospectID;
    public Boolean profileOk;
    public String details;
    public List<AssetResults> assetResults;
  }
    
    public class AssetResults {
    public Id assetID;
    public Boolean profileOk;
    public String details;
  }
    
}