@isTest
public class TestGenerateMRMangerController {
    @testSetup static void TestSetupMethod() {
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Component_Return_Condition__c rec = new Component_Return_Condition__c(name ='test');
        insert rec;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Return_Condition__c = rec.Id,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
        );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        
        aircraft.Lease__c = lease.ID;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;
        
        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40; 
        scenarioInp.Lease__c = lease.ID;
        insert scenarioInp;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Constituent_Assembly__c; 
        Map<String,Schema.RecordTypeInfo> AssemblyRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AssemblyRecordTypeInfo .get('Engine').getRecordTypeId();
        
        Custom_Lookup__c lookup = new Custom_Lookup__c();
        lookup.name ='Default';
        lookup.Lookup_Type__c = 'Engine';
        lookup.active__c = true;
        //lookup.Sub_LookupType__c= 'Test';
        insert lookup;
        
        Constituent_Assembly__c constitutentAssembly = new Constituent_Assembly__c();
        constitutentAssembly.Name ='Test Assembly';
        constitutentAssembly.Derate__c =12; 
        constitutentAssembly.RecordTypeId=rtId;
        constitutentAssembly.Current_TS__c = lookup.Id;
        constitutentAssembly.Average_Monthly_Utilization_Cycles__c =20;
        constitutentAssembly.Average_Utilization__c =12;
        constitutentAssembly.Attached_Aircraft__c = aircraft.Id; 
        constitutentAssembly.Type__c = 'Engine';
        constitutentAssembly.Serial_Number__c = 'Test 12';
        constitutentAssembly.TSN__c = 40;
        constitutentAssembly.CSN__c = 7; 
        insert constitutentAssembly;
        
        Assembly_RC_Info__c recAssembly = new Assembly_RC_Info__c(
            name='Test',
            RC_FC__c=23, 
            Event_Type__c= 'Performance Restoration',
            RC_FH__c =12,
            RC_Months__c = 2,
            Assembly_Type__c = 'Engine 1',
            Return_Condition__c = rec.Id
        );
        insert recAssembly;
        
        Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
		    Interval_2_Hours__c=20,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe;
        
        Assembly_Event_Info__c assemblyEventInfo = new Assembly_Event_Info__c( 
            Name = 'Test Info',
            Event_Cost__c = 20,
            Maintenance_Program_Event__c =  mpe.Id,
            Constituent_Assembly__c = constitutentAssembly.Id);
        insert assemblyEventInfo;
        
        Assembly_MR_Rate__c assemblyMR = new Assembly_MR_Rate__c(
            Assembly_Lkp__c = constitutentAssembly.Id, 
            Base_MRR__c =20,
            Assembly_Event_Info__c = assemblyEventInfo.Id,
            Starting_MR_Balance__c =20,
            Lease__c = lease.Id,
            Annual_Escalation__c = 60, 
            Rate_Basis__c='Hours',
            Last_Escalation_Date__c = System.today());
        insert assemblyMR; 
    }
    
    @isTest
    static void testMethod1(){
        Test.startTest();
        Aircraft__c asset = [SELECT Id FROM Aircraft__c LIMIT 1];
        Scenario_Input__c scenarioInp = [SELECT Id FROM Scenario_Input__c LIMIT 1];
        
        GenerateMRMangerController.fetchMRManager(asset.Id);
        GenerateMRMangerController.generateMRProfile(scenarioInp.Id);
        Test.stopTest();
    }
}