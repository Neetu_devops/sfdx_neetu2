public class RemoveAssemblyController {
	
    public class AssemblyWrapper {
        @AuraEnabled
        public Id assemblyId {get; set;}
        @AuraEnabled
        public Id assetId {get; set;}
        @AuraEnabled
        public Id leaseId {get; set;}
        @AuraEnabled
        public String assemblyName {get; set;}
        @AuraEnabled
        public String assetName {get; set;}
        @AuraEnabled
        public List<String> approvalStatus {get; set;}
        @AuraEnabled
        public String selectedApprovalStatus {get; set;}
        @AuraEnabled
        public Decimal assemblyTSN {get; set;}
        @AuraEnabled
        public Decimal assetTSN {get; set;}
        @AuraEnabled
        public Decimal assemblyCSN {get; set;}
        @AuraEnabled
        public Decimal assetCSN {get; set;}
        @AuraEnabled
        public String reason {get; set;}
    }
    
    @AuraEnabled
    public static AssemblyWrapper getAssemblyData(Id recordId, Date todayDate) {
        AssemblyWrapper assemblyData = new AssemblyWrapper();
        
        Constituent_Assembly__c assembly = [SELECT Id, Asset__c, Name, Asset__r.Name, Asset__r.Lease__c,
                                            TSN__c, CSN__c, Asset__r.TSN__c, Asset__r.CSN__c 
                                            FROM Constituent_Assembly__c WHERE ID = :recordId];
        
        List<AggregateResult> history = [SELECT Assembly__c, MAX(Assembly_CSN__c)maxAssemCsn, MAX(Assembly_TSN__c)maxAssemTsn, 
                                         MAX(Aircraft_CSN__c)maxAssetCsn, MAX(Aircraft_TSN__c)maxAssetTsn
                                         FROM Asset_History__c 
                                         WHERE Assembly__c = :recordId 
                                         AND Status__c ='Installation' 
                                         AND Status_Change_Date__c = :todayDate 
                                         GROUP BY Assembly__c LIMIT 1];
        if(history.size() > 0) {
            assemblyData.assemblyTSN = (Decimal)history[0].get('maxAssemTsn');
            assemblyData.assetTSN = (Decimal)history[0].get('maxAssetTsn');
            assemblyData.assemblyCSN = (Decimal)history[0].get('maxAssemCsn');
            assemblyData.assetCSN = (Decimal)history[0].get('maxAssetCsn');
        } else {
            assemblyData.assemblyTSN = assembly.TSN__c;
            assemblyData.assetTSN = assembly.Asset__r.TSN__c;
            assemblyData.assemblyCSN = assembly.CSN__c;
            assemblyData.assetCSN = assembly.Asset__r.CSN__c;
        }
        assemblyData.assemblyId = assembly.Id;
        assemblyData.assetId = assembly.Asset__c;
        assemblyData.leaseId = assembly.Asset__r.Lease__c;
        assemblyData.assemblyName = assembly.Name;
        assemblyData.assetName = assembly.Asset__r.Name;
        List<String> approvalStatus = new List<String>{'Not Required','Approved'};
        assemblyData.approvalStatus = approvalStatus;
        assemblyData.reason = '';
        
        return assemblyData;
    }
    
    @AuraEnabled
    public static void saveData(String assemblyData, Date userSelectedDate) {
        
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
		Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        List<Assembly_Utilization__c> assemblyUtilizations = [SELECT TSN_At_Month_Start__c, TSN_At_Month_End__c, 
                                                                      CSN_At_Month_Start__c, CSN_At_Month_End__c
                                                                      FROM Assembly_Utilization__c 
                                                                      WHERE Utilization_Report__r.Aircraft__c = :wrapperData.assetId
                                                                      AND Constituent_Assembly__c = :wrapperData.assemblyId
                                                                      AND (For_The_Month_Ending__c = :lastDayOfMonth
                                                                      OR For_The_Month_Ending__c = :lastDayOfPreviousMonth)
                                                                      AND Utilization_Report__r.Type__c = 'Actual' order by End_Date_F__c desc];
        system.debug('Utilization_Report__r.Aircraft__c-->'+wrapperData.assetId+'Constituent_Assembly__c-->'+wrapperData.assemblyId+'For_The_Month_Ending__c-->'+lastDayOfMonth);
        Boolean isTSNValid = false;   
        Boolean isCSNValid = false;
        Constituent_Assembly__c assembly = new Constituent_Assembly__c();
        
        if(assemblyUtilizations.size() > 0) {
            
            for(Assembly_Utilization__c assemblyUtilization : assemblyUtilizations) {
                
                if(wrapperData.assemblyTSN <= assemblyUtilization.TSN_At_Month_End__c 
                   && wrapperData.assemblyTSN >= assemblyUtilization.TSN_At_Month_Start__c) {
                       isTSNValid = true;
                }
                if(wrapperData.assemblyCSN <= assemblyUtilization.CSN_At_Month_End__c
                   && wrapperData.assemblyCSN >= assemblyUtilization.CSN_At_Month_Start__c) {
                       isCSNValid = true;
                }
            }            
        } else {
            assembly = [SELECT Id, TSN__c, CSN__c, Asset__r.TSN__c, Asset__r.CSN__c
                        FROM Constituent_Assembly__c WHERE ID = :wrapperData.assemblyId];
            
            if(wrapperData.assemblyTSN == assembly.TSN__c) {
                isTSNValid = true;
            }
            if(wrapperData.assemblyCSN == assembly.CSN__c) {
                isCSNValid = true;
            }
        }
        
        if(!isTSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Removal TSN');  
        }
        if(!isCSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Removal CSN');  
        }
        
        List<Utilization_Report__c> utilizations = [SELECT Current_TSN__c, TSN_At_Month_End__c, Current_CSN__c, CSN_At_Month_End__c 
                                                    FROM Utilization_Report__c 
                                                    WHERE Aircraft__c = :wrapperData.assetId 
                                                    AND (Month_Ending__c = :lastDayOfMonth 
                                                    OR Month_Ending__c = :lastDayOfPreviousMonth)
                                                    AND Type__c = 'Actual' order by End_Date_F__c desc];
        Boolean isACTSNValid = false;
        Boolean isACCSNValid = false;
            
        if(utilizations.size() > 0) {
            
            for(Utilization_Report__c utilization : utilizations) {
                if(wrapperData.assetTSN <= utilization.TSN_At_Month_End__c 
               		&& wrapperData.assetTSN >= utilization.Current_TSN__c) {
                        isACTSNValid = true;
            	}
                if(wrapperData.assetCSN <= utilization.CSN_At_Month_End__c
                   && wrapperData.assetCSN >= utilization.Current_CSN__c) {
                       isACCSNValid = true;
                }
            }
        } else {
            
            if(String.isEmpty(assembly.Id))
            	assembly = [SELECT Asset__r.TSN__c, Asset__r.CSN__c 
                            FROM Constituent_Assembly__c WHERE ID = :wrapperData.assemblyId];
            
            if(wrapperData.assetTSN == assembly.Asset__r.TSN__c) {
                isACTSNValid = true;
            }
            if(wrapperData.assetCSN == assembly.Asset__r.CSN__c) {
                isACCSNValid = true;
            }
        }
        
        if(!isACTSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Removal AC TSN');  
        }
        if(!isACCSNValid) {
            throw new AuraHandledException('Reported utilization for '+wrapperData.assemblyName+' do not match Assembly Removal AC CSN');
        }
        System.debug('wrapperData.leaseId: '+wrapperData.leaseId);
        
    }
    
    @AuraEnabled
    public static String proceedToSave(String assemblyData, Date userSelectedDate){
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        AssemblyTriggerHandler CAInst = new AssemblyTriggerHandler();
        
        CAInst.removedAssembly(wrapperData.assemblyId,wrapperData.assemblyName,wrapperData.assetId,userSelectedDate,
                               wrapperData.leaseId,wrapperData.assemblyTSN,wrapperData.assemblyCSN,wrapperData.assetTSN,
                               wrapperData.assetCSN,wrapperData.reason,wrapperData.selectedApprovalStatus);
        
        return wrapperData.assemblyId;
        
    }
    @AuraEnabled
    public static AssemblyWrapper getTsnCsnValuesOnDateChange(String assemblyData, Date userSelectedDate) {
        AssemblyWrapper wrapperData = new AssemblyWrapper();
        wrapperData = (AssemblyWrapper)System.JSON.deserialize(assemblyData, AssemblyWrapper.class);
        Date selectedDate = userSelectedDate;
        
        Integer numberOfDays = Date.daysInMonth(selectedDate.year(), selectedDate.month());
		Date lastDayOfMonth = Date.newInstance(selectedDate.year(), selectedDate.month(), numberOfDays);
        
        Date lastDayOfPreviousMonth = selectedDate.toStartOfMonth().addDays(-1);
        
        Decimal oldAssemblyTSN = wrapperData.assemblyTSN;
        Decimal oldAssemblyCSN = wrapperData.assemblyCSN;
        Decimal oldAssetTSN = wrapperData.assetTSN;
        Decimal oldAssetCSN = wrapperData.assetCSN;
        
        wrapperData.assetTSN = null;
        wrapperData.assetCSN = null;
        wrapperData.assemblyTSN = null;
        wrapperData.assemblyCSN = null;
            
        for(Assembly_Utilization__c assemblyUtilization : [SELECT TSN_At_Month_Start__c, TSN_At_Month_End__c, 
                                                                   CSN_At_Month_Start__c, CSN_At_Month_End__c
                                                                   FROM Assembly_Utilization__c 
                                                                   WHERE Utilization_Report__r.Aircraft__c = :wrapperData.assetId
                                                                   AND Constituent_Assembly__c = :wrapperData.assemblyId
                                                                   AND (For_The_Month_Ending__c = :lastDayOfMonth
                                                                   OR For_The_Month_Ending__c = :lastDayOfPreviousMonth)
                                                                   AND Utilization_Report__r.Type__c = 'Actual' order by End_Date_F__c desc LIMIT 1]) {
            
            wrapperData.assemblyTSN = assemblyUtilization.TSN_At_Month_End__c;
            wrapperData.assemblyCSN = assemblyUtilization.CSN_At_Month_End__c;
        }  
        if(wrapperData.assemblyTSN == null || wrapperData.assemblyCSN == null) {
            wrapperData.assemblyTSN = oldAssemblyTSN;
            wrapperData.assemblyCSN = oldAssemblyCSN;
        }
        
        
        for(Utilization_Report__c utilization : [SELECT TSN_At_Month_End__c, CSN_At_Month_End__c 
                                                 FROM Utilization_Report__c 
                                                 WHERE Aircraft__c = :wrapperData.assetId 
                                                 AND (Month_Ending__c = :lastDayOfMonth 
                                                      OR Month_Ending__c = :lastDayOfPreviousMonth)
                                                 AND Type__c = 'Actual' 
                                                 AND TSN_At_Month_End__c != null 
                                                 AND CSN_At_Month_End__c != null order by End_Date_F__c desc LIMIT 1]) {
            wrapperData.assetTSN = utilization.TSN_At_Month_End__c; 
            wrapperData.assetCSN = utilization.CSN_At_Month_End__c;
               
        }
        if(wrapperData.assetTSN == null || wrapperData.assetCSN == null) {
            wrapperData.assetTSN = oldAssetTSN;
            wrapperData.assetCSN = oldAssetCSN;
        }
        
        return wrapperData;
    }
}