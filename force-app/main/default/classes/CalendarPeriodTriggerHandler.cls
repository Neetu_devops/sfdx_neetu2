public class CalendarPeriodTriggerHandler { 
/* commenting after discussion with Bobby 08 June 2016
implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'CalendarPeriodTriggerHandlerBefore';
    private final string triggerAfter = 'CalendarPeriodTriggerHandlerAfter';
    public CalendarPeriodTriggerHandler()
    {
    }
 

    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('CalendarPeriodTriggerHandler.beforeInsert(+)');

	    system.debug('CalendarPeriodTriggerHandler.beforeInsert(-)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('CalendarPeriodTriggerHandler.beforeUpdate(+)');
    	list<Calendar_Period__c> newList = (list<Calendar_Period__c>)trigger.new;
    	set<Id> CPIDsSetRec = new set<id>();
    	
		for(Calendar_Period__c curRec:newList){
			
			if(curRec.Closed__c && curRec.Closed__c!=((Calendar_Period__c)trigger.oldmap.get(curRec.Id)).Closed__c){
				CPIDsSetRec.add(curRec.Id);
			}
		}
		
		// validation for Closing Period
		if(CPIDsSetRec.size()>0){
			set<id> CPIDsSet = new set<id>();
	    	list<AggregateResult> aggregateResList = [select Calendar_Period__c CPID from Invoice_Account_Code__c where (not Invoice__r.Status__c in ('Paid') )
							and Calendar_Period__c = :CPIDsSetRec
	 						group by Calendar_Period__c ]	;
							
	 		for(AggregateResult curRec:aggregateResList){
	 			CPIDsSet.add((Id)curRec.get('CPID'));
	 		}

			for(Calendar_Period__c curRec:newList){
				
				if(curRec.Closed__c && curRec.Closed__c!=((Calendar_Period__c)trigger.oldmap.get(curRec.Id)).Closed__c){
					if(CPIDsSet.contains(curRec.Id)) curRec.addError('Period ( '+ curRec.Name + ' ) cannot be closed due to some Invoices are not in Paid status.');
				}
			}
	 		
		}
 						
    	system.debug('CalendarPeriodTriggerHandler.beforeUpdate(-)');
    }
     

    public void beforeDelete()
    {  

    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('CalendarPeriodTriggerHandler.beforeDelete(+)');
    	
    	
    	system.debug('CalendarPeriodTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CalendarPeriodTriggerHandler.afterInsert(+)');
    	
    	system.debug('CalendarPeriodTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CalendarPeriodTriggerHandler.afterUpdate(+)');
    	
    	
    	system.debug('CalendarPeriodTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CalendarPeriodTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('CalendarPeriodTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('CalendarPeriodTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('CalendarPeriodTriggerHandler.afterUnDelete(-)');     	
    }
     

    public void andFinally()
    {
        // insert any audit records

    }
*/

}