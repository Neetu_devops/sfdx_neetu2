public class AircraftMRAvailabilityController{
	
/* anjani : Not required after airframe breakout

     public Aircraft_Eligible_Event__c ACEeventUDDate { get; set; }
     public string selectedDateType      {get;set;}
     public List<SelectOption> dateTypes {get;set;} 


     
     private final Aircraft_Eligible_Event__c thisPBL;
     
     public string selectedPaymentDate {get; set;}
     public List<SelectOption> PaymentDateoptions {get; set;}
     public list<MR_Snapshot__c>   mrlist {get;set;}

     public string EventTypeStr;


	     
*/     
     public AircraftMRAvailabilityController(ApexPages.StandardController controller){   
/*     	
    	system.debug('Start');
        this.thisPBL = (Aircraft_Eligible_Event__c)controller.getRecord();

   		Aircraft_Eligible_Event__c ACEeventRec =  [select id,Y_Hidden_Lease_F__c,	Start_Date__c,	End_Date__c,Event_Type__c 
   						,(select Payment_Date__c from Eligible_Event_Invoices__r where Payment_Date__c!=null order by Payment_Date__c desc)
   					from Aircraft_Eligible_Event__c where id = :thisPBL.Id];
     	
     	
    	mrlist = [select id, Month_Ending__c
    				, NMR_Invoiced_Heavy_Maint_1_Airframe__c, NMR_Collected_Heavy_Maint_1_Airframe__c
    				, NMR_Invoiced_Heavy_Maint_2_Airframe__c, NMR_Collected_Heavy_Maint_2_Airframe__c
    				, NMR_Invoiced_Heavy_Maint_3_Airframe__c, NMR_Collected_Heavy_Maint_3_Airframe__c
    				, NMR_Invoiced_Heavy_Maint_4_Airframe__c, NMR_Collected_Heavy_Maint_4_Airframe__c
    				, NMR_Invoiced_C_Check__c,NMR_Collected_C_Check__c
    				, Payment_Date__c  
	    		from MR_Snapshot__c 
	    		where Lease__c = :ACEeventRec.Y_Hidden_Lease_F__c
	    		order by Month_Ending__c ,Sequence_Number__c ];
    	
    	system.debug('Total MR snaps records '+ mrlist.size());  //size of list
    	
    	EventTypeStr = ACEeventRec.Event_Type__c;
    	system.debug('EventTypeStr= '+ EventTypeStr);  //size of list
    	
    	// Initialize Induction Date
    	inductionDate = ACEeventRec.Start_Date__c;
    	MRList_IND = new list<MR_Snapshot__c>{new MR_Snapshot__c()};
    	
    	//Initialize Release date from Aircraft_Eligible_Event__c Object
    	releaseDate=ACEeventRec.End_Date__c;
    	MRList_REL=new list<MR_Snapshot__c>{new MR_Snapshot__c()};
    	
    	// Initialize User defined Date
    	ACEeventUDDate = new Aircraft_Eligible_Event__c(name='xxx',start_Date__c=system.today());
    	MRList_UD=new list<MR_Snapshot__c>{new MR_Snapshot__c()};
    	
    	//Initialize Payment Date
    	MRList_PMT=new list<MR_Snapshot__c>{new MR_Snapshot__c()};
    	
    	



        
        
        //Date Type Picklist
        dateTypes = new List<SelectOption>();
        selectedDateType = 'Induction Date';
        dateTypes.add( new SelectOption( 'Induction Date','Induction Date' ) );
        dateTypes.add( new SelectOption( 'Release Date','Release Date' ) );
        dateTypes.add( new SelectOption( 'User Defined Date','User Defined Date' ) );
        dateTypes.add( new SelectOption( 'Payment Date','Payment Date' ) );
        dateTypes.add( new SelectOption( 'All','All' ) );
        
        //Payment Date picklist
		PaymentDatePicklistSet(ACEeventRec);
		
    	//Get MR balance for each event
    	getMRValueatDate();		
		
    	system.debug('MRAvailability Controller constructor');        
    	*/         
    }
/*    
    //1. Induction Date
    public Date inductionDate{get;set;}
    public list<MR_Snapshot__c>   MRList_IND {get;set;}
    	//public Date  NMR_Invoiced_Total_IND {get;set;}
    
    //2. Release Date 
    public Date releaseDate{get;set;}
    public list<MR_Snapshot__c>   MRList_REL {get;set;}
    	//public Date  NMR_Released_Total_IND {get;set;}
     
    //3. Payment Date 
    //public Date releaseDate{get;set;}
    public Date paymentDate {get; set;}
    public list<MR_Snapshot__c>   MRList_PMT {get;set;}     
    
    
    //4. User Defined Date
    public list<MR_Snapshot__c>   MRList_UD {get;set;}
     
	public void getMRValueatDate(){
		system.debug('getMRValueatDate+');
		system.debug('selectedPaymentDate='+selectedPaymentDate);
		Date PMTDate ;
		if(selectedPaymentDate!=null) PMTDate = date.parse(selectedPaymentDate);
		system.debug('PMTDate='+PMTDate);
		
    	for(MR_Snapshot__c curRec:mrlist){
    		//1. Induction
    		if(inductionDate!=null){
    			system.debug('inductionDate='+inductionDate);
    			if(inductionDate >= curRec.Month_Ending__c){
					copyMRBal(MRList_IND,curRec);
    			}
    		}
    		//2. Release
    		if(releaseDate!=null){
    			system.debug('releaseDate='+releaseDate);
    			if(releaseDate >= curRec.Month_Ending__c){
    				 copyMRBal(MRList_REL,curRec);
    			}
    		}
    		//3. Payment
    		if(PMTDate!=null){
    			if( PMTDate >= curRec.Month_Ending__c){
    				copyMRBal(MRList_PMT,curRec);
    			}
    		}    		
    		
    		
    		//4. User defined Date
    		if(ACEeventUDDate.Start_Date__c!=null){
    			system.debug('ACEeventUDDate.Start_Date__c='+ACEeventUDDate.Start_Date__c);
    			if(ACEeventUDDate.Start_Date__c >= curRec.Month_Ending__c){
    				copyMRBal(MRList_UD,curRec);
    			}
    		}    		
    	}		
		system.debug('getMRValueatDate-');
	}		
    
    
    // copy value
    private void copyMRBal(list<MR_Snapshot__c> listMRS , MR_Snapshot__c curRec){
		system.debug('curRecMonth_Ending__c='+curRec.Month_Ending__c);
    				if('Heavy Maintenance 1'.equals(EventTypeStr)){
    					listMRS[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=curRec.NMR_Collected_Heavy_Maint_1_Airframe__c ;
    					listMRS[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=curRec.NMR_Invoiced_Heavy_Maint_1_Airframe__c ;
    				}else if('Heavy Maintenance 2'.equals(EventTypeStr)){
    					listMRS[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=curRec.NMR_Collected_Heavy_Maint_2_Airframe__c ;
    					listMRS[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=curRec.NMR_Invoiced_Heavy_Maint_2_Airframe__c ;
    				}else if('Heavy Maintenance 3'.equals(EventTypeStr)){
    					listMRS[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=curRec.NMR_Collected_Heavy_Maint_3_Airframe__c ;
    					listMRS[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=curRec.NMR_Invoiced_Heavy_Maint_3_Airframe__c ;
    				}else if('Heavy Maintenance 4'.equals(EventTypeStr)){
    					listMRS[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=curRec.NMR_Collected_Heavy_Maint_4_Airframe__c ;
    					listMRS[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=curRec.NMR_Invoiced_Heavy_Maint_4_Airframe__c ;
    				}else if('C-Check'.equals(EventTypeStr)){
    					listMRS[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=curRec.NMR_Collected_C_Check__c ;
    					listMRS[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=curRec.NMR_Invoiced_C_Check__c ;
    				}else{
    					listMRS[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=null ;
    					listMRS[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=null ;    					
    				}
    }
    
    
    
    // for user defined date
    public void getMRBALatDate()
    {
    	system.debug('call method called');
    	system.debug('ACEeventUDDate.Start_Date__c='+ACEeventUDDate.Start_Date__c);
    	MRList_UD[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=null ;
    	MRList_UD[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=null ;  
    	for(MR_Snapshot__c curRec:mrlist){
   			//4. 
    		if(ACEeventUDDate.Start_Date__c!=null){
    			if(ACEeventUDDate.Start_Date__c >= curRec.Month_Ending__c){
    				copyMRBal(MRList_UD,curRec);
    			}
    		}
    	}		    	
    }
    
    //for payment date
    public void getMRBALatPMTDate()
    {
    	system.debug('call method called Payment Date');
    	system.debug('selectedPaymentDate='+selectedPaymentDate);
    	MRList_UD[0].Y_H_NMRCol_Heavy_Maint_1_Airframe__c=null ;
    	MRList_UD[0].Y_H_NMRInv_Heavy_Maint_1_Airframe__c=null ;  
    	Date PMTDate = date.parse(selectedPaymentDate);
    	system.debug('PMTDate='+PMTDate);
    	for(MR_Snapshot__c curRec:mrlist){
   			//3. 
    		if(PMTDate!=null){
    			if(PMTDate >= curRec.Month_Ending__c){
    				copyMRBal(MRList_PMT,curRec);
    			}
    		}
    	}		    	
    }

   private void PaymentDatePicklistSet(Aircraft_Eligible_Event__c ACEventRec){
        //dynamic piclist for "payment date" type
        PaymentDateoptions = new List<SelectOption>();
	   	//list<AggregateResult> datelist = [select Payment_Date__c pdc from Aircraft_Eligible_Event_Invoice__c where Aircraft_Eligible_Event__c =:thisPBL.Id group by Payment_Date__c order by Payment_Date__c desc];
		
		system.debug('Invoice size------------' + ACEventRec.Eligible_Event_Invoices__r.size());
			  	
		set<Date>	checkDupSet= new set<Date>();	  			 
		for (Aircraft_Eligible_Event_Invoice__c  curRec   :ACEventRec.Eligible_Event_Invoices__r)
		{
			if(!checkDupSet.contains(curRec.Payment_Date__c)){
				// Label , API code
				PaymentDateoptions.add(new SelectOption(curRec.Payment_Date__c.format(), curRec.Payment_Date__c.format()));
				checkDupSet.add(curRec.Payment_Date__c);
			}
			if(selectedPaymentDate==null) selectedPaymentDate = curRec.Payment_Date__c.format();
			
		}   	
   } 

   */
}