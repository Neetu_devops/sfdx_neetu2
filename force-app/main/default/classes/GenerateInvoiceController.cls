public class GenerateInvoiceController{
    
    //To insert Rent Invoice.
    @auraEnabled
    public static Invoice__c saveRentInvoice(Invoice__c invoiceObj, String recordTypeDeveloperName){
    
        List<Rent__c> lstRent = [select RentPayments__c from rent__c where id =: invoiceObj.rent__c];
        
        if(lstRent.size()>0){
            invoiceObj.Lease__c= lstRent[0].RentPayments__c;
        }
    
        return insertRecord(recordTypeDeveloperName,invoiceObj);
    }
    
    //To insert MR Invoice.
    @auraEnabled
    public static Invoice__c saveMRInvoice(Invoice__c invoiceObj, String recordTypeDeveloperName){
    
        List<Utilization_Report__c> lstUtilization = [select Y_hidden_Lease__c from Utilization_Report__c where id =: invoiceObj.Utilization_Report__c];
        if(lstUtilization.size()>0){
            invoiceObj.Lease__c = lstUtilization[0].Y_hidden_Lease__c;
        }
    
        return insertRecord(recordTypeDeveloperName,invoiceObj);
    }
    
    //To insert Security Deposit Invoice.
    @auraEnabled
    public static Invoice__c saveSDInvoice(Invoice__c invoiceObj, String recordTypeDeveloperName){
    
        List<Cash_Security_Deposit__c> lstSecurityDeposit = [select Lease__c,Security_Deposit_Cash__c from Cash_Security_Deposit__c where id =: invoiceObj.Cash_Security_Deposit__c];
        if(lstSecurityDeposit.size()>0){
            invoiceObj.Lease__c = lstSecurityDeposit[0].Lease__c;
            invoiceObj.amount__c = lstSecurityDeposit[0].Security_Deposit_Cash__c;
        }
    
        return insertRecord(recordTypeDeveloperName,invoiceObj);
    }
    
    //common method to set record type and insert invoice.
    private static Invoice__c insertRecord(String recordTypeDeveloperName, Invoice__c invoiceObj){
        String prefix = LeaseWareUtils.getNamespacePrefix();
        String objName = prefix+'Invoice__c';
        Id recordTypeId;
        
        if(Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfosByDeveloperName().containsKey(recordTypeDeveloperName) && Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName) != null){
            recordTypeId = Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName).getRecordTypeId();
        }
        invoiceObj.RecordTypeId = recordTypeId; 
        insert invoiceObj;
        return invoiceObj;
    }
    
    @auraEnabled
    public static DataContainerWrapper getData(Id recordId){
        
        String prefix = LeaseWareUtils.getNamespacePrefix();
        DataContainerWrapper dataWrapper = new DataContainerWrapper();
        
        if(recordId != null){
            dataWrapper.objectRecord = getObjectType(recordId);
        }
        
        Map<String, String> recordTypeMap = new Map<String, String>();
        String objName = prefix+'Invoice__c';

        if(objName != null && objName != ''){
            for(Schema.RecordTypeInfo rtInfo : Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfos()){
                recordTypeMap.put(rtInfo.getDeveloperName(), rtInfo.getName());
                
                if(rtInfo.getDeveloperName() == 'Manual_MR_Credit_Note'){
                    dataWrapper.is_Manual_MR_Credit_Note_Accessible = rtInfo.isAvailable(); 
                }
            }
        }
        dataWrapper.recordTypeMap = recordTypeMap;
       
        return dataWrapper;
    }
    
    private static ObjectRecordWrapper getObjectType(Id recordId){
       
        ObjectRecordWrapper wrapperObj = new ObjectRecordWrapper();
        wrapperObj.objectType = recordId.getSObjectType().getDescribe().getName();
        String qry = 'Select Id,Name from ' + wrapperObj.objectType + ' where Id =:recordId';
        wrapperObj.record = database.query(qry);
        return wrapperObj;
    }

    //this method can be used for normal type of Invoice for eg manual MR, other etc.
    @auraEnabled
    public static Invoice__c saveInvoice(Invoice__c invoiceObj, String recordTypeDeveloperName, String assemblyMRInfoList){
        
        //set invoice data
        invoiceObj = insertRecord(recordTypeDeveloperName,invoiceObj);
        
        if(String.isNotBlank(assemblyMRInfoList)){
        	invoice__c invoice= [select id,status__c from invoice__c where id =:invoiceObj.id];
            //set invoice line item data
            List<Invoice_Line_Item__c> invoiceLineItemList = new List<Invoice_Line_Item__c>();
            List<AssemblyMRInfoWrapper> MRInfoList = (List<AssemblyMRInfoWrapper>)JSON.deserialize(assemblyMRInfoList, List<AssemblyMRInfoWrapper>.class);

            String invLIMRRecTypeId = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByDeveloperName().get('Monthly_Utilization').getRecordTypeId();
            for(AssemblyMRInfoWrapper assMRInfoRecord : MRInfoList){
            
                Invoice_Line_Item__c invoiceLineItem = new Invoice_Line_Item__c(Name = (assMRInfoRecord.Name + ' ' +assMRInfoRecord.eventType), Invoice__c = invoiceObj.Id, 
                                                                Assembly_MR_Info__c = assMRInfoRecord.MRInfoRecordId,
                                                                //Event_Type__c = assMRInfoRecord.eventType,
                                                                RecordTypeId = invLIMRRecTypeId,
                                                                Rate_Basis__c = assMRInfoRecord.rateBasis,
                                                                Comments__c = assMRInfoRecord.comments,
                                                                Assembly__c = assMRInfoRecord.AssemblyType,
                                                                Amount_Due__c = assMRInfoRecord.amount,
                                                                Utilization__c = assMRInfoRecord.utilization,
                                                                //Projected_Event__c = assMRInfoRecord.projEventId,
                                                                Rate__c = assMRInfoRecord.rate);
               
                
                invoiceLineItemList.add(invoiceLineItem);
            }
            if(invoiceLineItemList.size()>0){
                if(invoice!=null && 'Approved'.equals(invoice.Status__c)){
                     LeaseWareUtils.setFromTrigger('MR_INVOICE_UPDATE');
                     insert invoiceLineItemList;
                     LeaseWareUtils.setFromTrigger('MR_INVOICE_UPDATE');
                }
                else{
                    insert invoiceLineItemList;
                }
                 
            }
           
        }
        
        return invoiceObj;
    }
    
    @auraEnabled
    public static List<AssemblyMRInfoWrapper> getAssemblyMRInfo(Id leaseId){
    
        List<AssemblyMRInfoWrapper> lstAssemblyMRInfo = new List<AssemblyMRInfoWrapper>();
        
        for(Assembly_MR_Rate__c mrInfo : [select id,Name,Assembly_Event_Info__c,Assembly_Event_Info__r.Event_Type__c,Lease__r.Aircraft__r.MSN_Number__c,Rate_Basis__c,
                                            Current_Rate_Escalated_F__c,Assembly_Lkp__c
                                            from Assembly_MR_Rate__c where lease__c =: leaseId]){
                                            
            lstAssemblyMRInfo.add(new AssemblyMRInfoWrapper(mrInfo.Id, mrInfo.name,mrInfo.Assembly_Event_Info__c, mrInfo.Assembly_Event_Info__r.Event_Type__c, mrInfo.Lease__r.Aircraft__r.MSN_Number__c,
                                                            mrInfo.Rate_Basis__c, mrInfo.Current_Rate_Escalated_F__c,mrInfo.Assembly_Lkp__c));
        }
        
        return lstAssemblyMRInfo;
    }
    
    public class AssemblyMRInfoWrapper{
        
        @auraEnabled public String MRInfoRecordId {get;set;}
        @auraEnabled public String name {get;set;}
        @auraEnabled public String projEventId {get;set;}
        @auraEnabled public String eventType {get;set;}
        @auraEnabled public String serialNo {get;set;}
        @auraEnabled public String rateBasis {get;set;}
        @auraEnabled public Decimal rate {get;set;}
        @auraEnabled public Decimal utilization {get;set;}
        @auraEnabled public Decimal amount {get;set;}
        @auraEnabled public String comments {get;set;}
        @auraEnabled public Boolean flag {get;set;}
        @auraEnabled public String assemblyType {get;set;}
        public AssemblyMRInfoWrapper(String MRInfoRecordId, String name, string projEventId, String eventType, String serialNo, String rateBasis, Decimal rate, String assemblyType){
            
            this.MRInfoRecordId = MRInfoRecordId;
            this.name = name;
            this.projEventId = projEventId;
            this.eventType = eventType;
            this.serialNo = serialNo;
            this.rateBasis = rateBasis;
            this.rate= rate;
            flag = false;
            this.assemblyType = assemblyType ;
        }
    }     
      
    public class DataContainerWrapper{
    
        @auraEnabled public Map<String, String> recordTypeMap{get;set;}
        @auraEnabled public ObjectRecordWrapper objectRecord{get;set;}
        @auraEnabled public Boolean is_Manual_MR_Credit_Note_Accessible {get;set;}
    }
    
    public class ObjectRecordWrapper{
    
        @auraEnabled public sObject record {get;set;}
        @auraEnabled public String objectType {get;set;}
    }
    

    /*@auraEnabled
    public static List<RadioGroupWrapper> getRecordTypes(){
        
        List<RadioGroupWrapper> lstRadioGroupWrapper = new List<RadioGroupWrapper>();

        RadioGroupWrapper group1 = new RadioGroupWrapper('Rent' , 'Rent');
        group1.lstChild.add(new RadioWrapper('Rent Invoice', 'Rent Invoice'));
        group1.lstChild.add(new RadioWrapper('Rent Credit Note', ' Rent Credit Note'));

        RadioGroupWrapper group2 = new RadioGroupWrapper('MR' , 'MR');
        group2.lstChild.add(new RadioWrapper('MR Invoice', 'MR Invoice'));
        group2.lstChild.add(new RadioWrapper('MR Credit Note', ' MR Credit Note'));
        
        lstRadioGroupWrapper.add(group1);
        lstRadioGroupWrapper.add(group2);

        return lstRadioGroupWrapper;
    }

    public Class RadioGroupWrapper{

        @auraEnabled public String label {get;set;}
        @auraEnabled public String value {get;set;}
        @auraEnabled List<RadioWrapper> lstChild {get;set;}

        public RadioGroupWrapper(String label, String value){
            lstChild = new List<RadioWrapper>();
            this.label = label;
            this.value =  value;
        }
    }
    
    public class RadioWrapper{
    
        @auraEnabled public String label {get;set;}
        @auraEnabled public String value {get;set;}

        public RadioWrapper(String label, String value){
            this.label = label;
            this.value = value;
        }
    }*/
    
  
}