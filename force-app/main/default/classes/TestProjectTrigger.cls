@isTest
private class TestProjectTrigger{

    @isTest
    private static void test_method(){
    
        Test.startTest();
        
        Project__c prjObj = new Project__c();
        prjObj.name = 'Test Project';
        prjObj.Project_Deal_Type__c = 'Sale';
        insert prjObj;
        
        Project__c prjObj2 = new Project__c();
        prjObj2.name = 'Test Project';
        prjObj2.Project_Deal_Type__c = 'Sale';
   
        
        Project__c prjObj3 = new Project__c();
        prjObj3.name = 'Test Project1';
        prjObj3.Project_Deal_Type__c = 'Sale';
        
        database.insert(new List<Project__c>{prjObj2,prjObj3},false);
        prjObj.bid_field__c = leasewareutils.getNamespacePrefix()+'Sale_Price__c';
      
        database.update(new List<Project__c>{prjObj,prjObj2,prjObj3},false);
        Test.stopTest();
    } 
}