public without sharing class MaintenenceProgUpdateController {
    
    //Landing-Gear Nose not added as it is same in PE and MPE
    private static Map <String,List<String>> mapLG_AssemblyTypes = new Map <String,List<String>>{'Landing Gear - Main' => new List<String>{'Landing Gear - Left Main','Landing Gear - Right Main'}, 'Landing Gear - Wing' =>new List<String> {'Landing Gear - Left Wing','Landing Gear - Right Wing'} };

    @AuraEnabled
    public static string getNamespacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    // fetch the current Asset's existing MPName.
    @AuraEnabled
    public static Maintenance_Program__c getMPName(String recordId){
        System.debug('getMPName :'+recordId);
        List<Aircraft__c> assetList = [SELECT id,Name, Maintenance_Program__c
                                   FROM Aircraft__c 
                                   WHERE id = : recordId and Maintenance_Program__c != null];
        if(assetList.size() > 0){
            List<Maintenance_Program__c> mpList = [SELECT Id,Name 
                                               FROM Maintenance_Program__c 
                                               WHERE Id=:assetList[0].Maintenance_Program__c ];
            if(mpList.size() > 0){
                return mpList[0];
            }
        }
        return null;
    }
    
    // fetch the current Asset's details.
    @AuraEnabled
    public static Aircraft__c getAssetName(String recordId){
        System.debug('getAssetName :'+recordId);
        return [SELECT id,Name FROM Aircraft__c WHERE id = : recordId];        
    }
    
    //Get Existing MP data for table1
    @AuraEnabled
    public static List<DatasetWrapper> getTable1Data(String refrenceId){
        System.debug('getTable1Data : assetId :'+refrenceId);
        Constituent_Assembly__c[] assemblylist = [SELECT Id, Name, Serial_Number__c,Attached_Aircraft__c,Type__c,Attached_Aircraft__r.Maintenance_Program__c,
                                                  (SELECT Id,Name,Maintenance_Program_Event__c,Maintenance_Program_Event__r.Name,
                                                   Maintenance_Program_Event__r.Event_Type_Global__c,Event_Type__c,Assembly_Type__c 
                                                   FROM Assembly_Event_Info__r)
                                                  FROM Constituent_Assembly__c 
                                                  WHERE Attached_Aircraft__c =: refrenceId  ORDER BY Type__c ]; 
        System.debug('assemblylist size'+assemblylist.size());
        List<DatasetWrapper> returnDataList = new List<DatasetWrapper>();
            for(Constituent_Assembly__c asm : assemblylist ){
                DatasetWrapper info = new DatasetWrapper();
                info.AssemblyId = asm.Id;
                info.AssemblyName = asm.Name;
                info.AssemblyType = asm.Type__c;
                List<ProjectedEvntWrapper> peWrapperList = new List<ProjectedEvntWrapper>();
                List<Assembly_Event_Info__c> peList = asm.Assembly_Event_Info__r;
                if(peList != null && peList.size() > 0) {
                    for(Assembly_Event_Info__c pe: peList) {
                        ProjectedEvntWrapper peWrapper = new ProjectedEvntWrapper();
                        peWrapper.PEId = pe.Id;
                        peWrapper.PEName = pe.Name;
                        peWrapper.PEEventType = pe.Event_Type__c;
                        peWrapper.PEAssemblyType = pe.Assembly_Type__c;
                        peWrapper.MPEId = pe.Maintenance_Program_Event__c;
                        peWrapper.MPEName = pe.Maintenance_Program_Event__r.Name;
                        peWrapper.MPEventType = pe.Maintenance_Program_Event__r.Event_Type_Global__c;
                        List<SupplementalRentWrapper> srWrapperList = new List<SupplementalRentWrapper>();
                        List<Assembly_MR_Rate__c > srList = [SELECT Id, Name,Assembly_Event_Info__c,Lease__r.Name 
                                                             FROM Assembly_MR_Rate__c 
                                                             WHERE
                                                             Assembly_Event_Info__c =: pe.Id];
                        if(srList != null && srList.size() > 0) {
                            for(Assembly_MR_Rate__c  sr: srList) {
                                SupplementalRentWrapper srWrapper = new SupplementalRentWrapper();
                                srWrapper.SRId = sr.Id;
                                srWrapper.SRName = sr.Name;
                                srWrapper.LeaseName = sr.Lease__r.Name;
                                srWrapper.ProjectdEvnt = sr.Assembly_Event_Info__c;
                                srWrapperList.add(srWrapper);
                            }
                        }
                        peWrapper.SupplementalRentWrap =srWrapperList;
                        peWrapperList.add(peWrapper);
                    }
                }
                info.PEWrap = peWrapperList;
                returnDataList.add(info);
            }
            System.debug('returnDataList : '+returnDataList);
       return returnDataList;
    }  
    
    //to fetch new MP2 data for table2
    @AuraEnabled
    public static List<DatasetWrapper> getTable2Data(String recordId, String assetId) {
        System.debug('getTable2Data mpId : '+recordId);
        List<DatasetWrapper> finalDataList = new List<DatasetWrapper>();
        List<DatasetWrapper> data1List = getTable1Data(assetId);
        List<Maintenance_Program_Event__c> mpeDataList = ftechMPEList(recordId);
        Set<Maintenance_Program_Event__c> newMpe = new Set<Maintenance_Program_Event__c>();
        Map<String,String> mapnewMpe = new Map<String,String>();
        List<String> mpeleftofPE = new List<String>();
        Map<String,String> mapFromSet = LeaseWareUtils.getMappingData('Assembly Type Event');        
        for(DatasetWrapper data1:data1List){
            DatasetWrapper info = new DatasetWrapper();
            info.AssemblyId = data1.AssemblyId;
            info.AssemblyName = data1.AssemblyName;
            info.AssemblyType = data1.AssemblyType;
            info.PEWrap = data1.PEWrap;
            List<ProjectedEvntWrapper> peWrapperList = new List<ProjectedEvntWrapper>();

            for(Maintenance_Program_Event__c mpe : mpeDataList){   
                mpeleftofPE.add(mpe.Event_Type_Global__c);
                if(data1.PEWrap.size()>0){
                    for(ProjectedEvntWrapper pe:data1.PEWrap){
                        if(pe.PEAssemblyType.contains(mpe.Assembly__c) || (mapFromSet.get(pe.PEAssemblyType) == mpe.Assembly__c )|| 
                           (mapLG_AssemblyTypes.containsKey(mpe.Assembly__c) && mapLG_AssemblyTypes.get(mpe.Assembly__c).contains(pe.PEAssemblyType) )){
                            System.debug('pe.PEAssemblyType : '+pe.PEAssemblyType+' m.Assembly__c :'+mpe.Assembly__c);
                               if(mpe.Event_Type_Global__c == pe.MPEventType ){
                                   System.debug('mpe.Event_Type_Global__c :'+mpe.Event_Type_Global__c+'pe.MPEventType :'+pe.MPEventType);
                                   pe.NewMPEId = mpe.Id;
                                   pe.NewMPEName = mpe.Name;
                                   pe.NewMPEventType = mpe.Event_Type_Global__c;
                                   
                                   mapnewMpe.put(mpe.Id+info.AssemblyType+mpe.Event_Type_Global__c,mpe.Id); 
                               }
                               
                               else{
                                   System.debug(' ELSE m.Event_Type_Global__c :'+mpe.Event_Type_Global__c);
                                   newMpe.add(mpe);
                               }
                               
                           }
                    }
                    System.debug('newMPE :'+newMPE);
                    if(newMPE.size()>0){
                        if(info.AssemblyType.contains(mpe.Assembly__c) ||  (mpe.Assembly__c == mapFromSet.get(info.AssemblyType)) ||
                          (mapLG_AssemblyTypes.containsKey(mpe.Assembly__c) && mapLG_AssemblyTypes.get(mpe.Assembly__c).contains(info.AssemblyType) )){
                               System.debug('newMPE :'+info.AssemblyType+ ' '+mpe.Assembly__c);
                               if( !mapnewMpe.containsKey(mpe.Id+info.AssemblyType+mpe.Event_Type_Global__c) ){
                                   System.debug('newMPE if2');
                                   ProjectedEvntWrapper peWrapper = new ProjectedEvntWrapper();
                                   peWrapper.PEId = '';
                                   peWrapper.PEName = '';
                                   peWrapper.PEEventType = '';
                                   peWrapper.PEAssemblyType = '';
                                   peWrapper.MPEId = '';
                                   peWrapper.MPEName = '';
                                   peWrapper.MPEventType = '';
                                   
                                   List<SupplementalRentWrapper> srWrapperList = new List<SupplementalRentWrapper>();
                                   SupplementalRentWrapper srWrapper = new SupplementalRentWrapper();
                                   srWrapper.SRId = '';
                                   srWrapper.SRName = '';
                                   srWrapper.ProjectdEvnt = '';
                                   srWrapper.LeaseName='';
                                   srWrapperList.add(srWrapper);  
                                   peWrapper.SupplementalRentWrap =srWrapperList;
                                   peWrapper.NewMPEId = mpe.Id;
                                   peWrapper.NewMPEName = mpe.Name;
                                   peWrapper.NewMPEventType = mpe.Event_Type_Global__c;  
                                   info.PEWrap.add(peWrapper);
                               }       
                           }         
                    
                    }
                }else
                    if(data1.PEWrap.size()==0){
                        System.debug('getTable2Data PEWrap size == 0');
                        System.debug('getTable2Data PEWrap size == 0'+mpe.Event_Type_Global__c);
                        if(data1.AssemblyType.contains(mpe.Assembly__c)  ||  (mpe.Assembly__c == mapFromSet.get(data1.AssemblyType)) ||
                           (mapLG_AssemblyTypes.containsKey(mpe.Assembly__c) && mapLG_AssemblyTypes.get(mpe.Assembly__c).contains(data1.AssemblyType) )){
                                  
                               ProjectedEvntWrapper peWrapper = new ProjectedEvntWrapper();
                               peWrapper.PEId = '';
                               peWrapper.PEName = '';
                               peWrapper.PEEventType = '';
                               peWrapper.PEAssemblyType = '';
                               peWrapper.MPEId = '';
                               peWrapper.MPEName = '';
                               peWrapper.MPEventType = '';
                               List<SupplementalRentWrapper> srWrapperList = new List<SupplementalRentWrapper>();
                               SupplementalRentWrapper srWrapper = new SupplementalRentWrapper();
                               srWrapper.SRId = '';
                               srWrapper.SRName = '';
                               srWrapper.LeaseName='';
                               srWrapper.ProjectdEvnt = '';    
                               srWrapperList.add(srWrapper);  
                               peWrapper.SupplementalRentWrap =srWrapperList;
                               peWrapper.NewMPEId = mpe.Id;
                               peWrapper.NewMPEName = mpe.Name;
                               peWrapper.NewMPEventType = mpe.Event_Type_Global__c; 
                               peWrapperList.add(peWrapper);
                              // mapPEtoMPE.put(data1.AssemblyType,peWrapper);
                              // info.PEWrap.add(peWrapper);
                           }
                    }
                if(peWrapperList.size() > 0){
                   info.PEWrap =  peWrapperList;
                }
            }
            System.debug('getTable2Data list of mpe :'+mpeleftofPE);
            finalDataList.add(info);
        }
        
        
        System.debug('finalDataList :'+finalDataList);
        return finalDataList;
    }   
    
   //return list of MPE for given MP2
    @AuraEnabled
    public static List<Maintenance_Program_Event__c> ftechMPEList(String mpId){
        System.debug('ftechMPEList mpId : '+mpId);
         return [SELECT Id,NAme,Assembly__c,Event_Type_Global__c,Maintenance_Program__c
                 FROM Maintenance_Program_Event__c
                 WHERE Maintenance_Program__c =: mpId order by Assembly__c];
    }
    
    //chnge asset's MP lookup with new selected MP
    @AuraEnabled
    public static void changeMPonAsset(String mpId,String assetId){
        System.debug('changeMPonAsset : mpId :'+mpId+' assetId :'+assetId);
        List<Aircraft__c> assetList = [SELECT id,Name, Maintenance_Program__r.Name,Maintenance_Program__c
                                   FROM Aircraft__c 
                                   WHERE id = : assetId];
        if(assetList.size()> 0){
            String oldmpId = assetList[0].Maintenance_Program__r.Id;
            assetList[0].Maintenance_Program__c = mpId;
            
            update assetList[0];
            
            updateProjEvnt(mpId,oldmpId,assetId);   
            updateHistEvnt(mpId,oldmpId,assetId);    
        }
    }
    
    //update PE after MP change on Asset 
    private static void updateProjEvnt(String NewMPId, String OldMPId,String AssetId){
       System.debug('updateProjEvnt  start:');
            LeaseWareUtils.TriggerDisabledFlag=true;    
            LeaseWareUtils.projectedEventOperations( new set<Id>{NewMPId}, new set<Id>{AssetId});
            LeaseWareUtils.TriggerDisabledFlag=false; 
       System.debug('---updateProjEvnt End----');  
    }
    
    //update HE after MP change on Asset 
    private static void updateHistEvnt(String NewMPId, String OldMPId,String AssetId){
        System.debug('---updateHistEvnt Start----');
        List<Assembly_Eligible_Event__c> listHEtoUpdate = new List<Assembly_Eligible_Event__c>();
        List<Assembly_Eligible_Event__c> recs = [SELECT Id,Constituent_Assembly__c,Event_Type__c,Y_Hidden_Maintenance_Program__c,Assembly_Type__c,Y_Hidden_Maintenance_Program_Event__c
                                                 FROM Assembly_Eligible_Event__c
                                                 WHERE
                                                 Y_Hidden_Maintenance_Program__c =: NewMPId];
        
        Set<Id> CAIds=new set<ID>();
        Set<Id> MPIds = new Set<Id>();
        Set<String> setEventType = new Set<String>();
        
        
        Map<Id,Maintenance_Program__c> mapHEtoMP = new Map<Id,Maintenance_Program__c>([SELECT id,
                                                                                       (SELECT Id,Event_Type__c,Event_Type_Global__c,Assembly__c,Maintenance_Program__c
                                                                                        FROM Maintenance_Program_Event__r )
                                                                                       FROM Maintenance_Program__c
                                                                                       WHERE id =:NewMPId]);
        
        for(Assembly_Eligible_Event__c curCAEE : recs){
            CAIds.add(curCAEE.Constituent_Assembly__c);
            
            if(!mapHEtoMP.containsKey(curCAEE.Y_Hidden_Maintenance_Program__c))
                continue;
            for(Maintenance_Program_Event__c mpe : mapHEtoMP.get(curCAEE.Y_Hidden_Maintenance_Program__c).Maintenance_Program_Event__r){
                if(mpe.Event_Type_Global__c == curCAEE.Event_Type__c ){
                    if(curCAEE.Assembly_Type__c.remove('Right ').remove('Left ').contains(mpe.Assembly__c)){
                        curCAEE.Y_Hidden_Maintenance_Program_Event__c = mpe.Id;
                        listHEtoUpdate.add(curCAEE);
                    }
                }
            }
            
        }
        System.debug('---updateHistEvnt '+listHEtoUpdate);
        update listHEtoUpdate;
        System.debug('---updateHistEvnt ENd----');
    }
    
    public class DatasetWrapper{
        @AuraEnabled public Id AssemblyId {get; set;}
        @AuraEnabled public String AssemblyName {get; set;}
        @AuraEnabled public String AssemblyType {get; set;}
        @AuraEnabled public LIst<ProjectedEvntWrapper> PEWrap {get; set;}
    }
    
    public class ProjectedEvntWrapper {
        @AuraEnabled public String PEId {get; set;}
        @AuraEnabled public String PEName {get; set;}
        @AuraEnabled public String MPEId {get; set;}
        @AuraEnabled public String MPEName {get; set;}
        @AuraEnabled public Id NewMPEId {get; set;}
        @AuraEnabled public String NewMPEName {get; set;}
        @AuraEnabled public String ActionMessage {get; set;}
        @AuraEnabled public List<SupplementalRentWrapper> SupplementalRentWrap {get; set;}
        @AuraEnabled public String MPEventType {get; set;}
        @AuraEnabled public String PEEventType {get; set;}
        @AuraEnabled public String PEAssemblyType {get; set;}
        @AuraEnabled public String NewMPEventType {get; set;}
    } 
    
    public class SupplementalRentWrapper {
        @AuraEnabled public String MPId {get; set;}
        @AuraEnabled public String SRId {get; set;}
        @AuraEnabled public String SRName {get; set;}
        @AuraEnabled public String LeaseName {get; set;}
        @AuraEnabled public String ProjectdEvnt {get; set;}
    }
    
    public class MPWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public List<MPEWrapper> MPEWrapper {get; set;}
    }
    
    public class MPEWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public String MPId {get; set;}
        @AuraEnabled public String EventType {get; set;}
        @AuraEnabled public String Assembly {get; set;}
        @AuraEnabled public ProjectedEvntWrapper ProjEvnt {get; set;}
    }
}