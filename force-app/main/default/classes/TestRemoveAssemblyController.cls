@isTest
public class TestRemoveAssemblyController {
    

    @isTest(seeAllData=true)
    static void testMethod1() {
        
        map<String,Object> mapInOut = new map<String,Object>();
        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
        
        List<Constituent_Assembly__c> constitutentAssembly = [Select Id from Constituent_Assembly__c where Attached_Aircraft__c =: AC1.Id];
        // Assumed UR
        system.debug('constitutentAssemblyconstitutentAssembly----->'+constitutentAssembly);
        Date ForMonthEnding = Date.parse('05/31/2019'); // Lease start Date date.newinstance(2013, 8, 17)
        /*anjani : creating with 'Approved By Lessor' to avoid soql issue */
       
        
        List<Custom_Lookup__c> CLRec = [select name from Custom_Lookup__c where name='Default' LIMIT 1] ;
        Utilization_Report__c URActual = new Utilization_Report__c(
            Name= 'Actual', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100',Type__c = 'Actual', 
             Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= ForMonthEnding,  
                     Status__c='Approved By Lessor'
                    ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
                    ,Maintenance_Reserve_Engine_1__c = 20000
                    ,Airframe_Flight_Hours_Month__c = 150
                    
             );
        
        LeaseWareUtils.clearFromTrigger();insert URActual;
       
        //Utilization_Report__c URActual =[select id from Utilization_Report__c where Aircraft__c =: AC1.Id limit 1];
        //URActual.Type__c = 'Actual';
        //update URActual;
        
        Utilization_Report_List_Item__c[] asmblyURs = [select Constituent_Assembly__c, Cycles_During_Month__c, Running_Hours_During_Month__c from Utilization_Report_List_Item__c where Utilization_Report__c = :URActual.Id];
        for(Utilization_Report_List_Item__c curAUR: asmblyURs){
            system.debug('TestThrustSetting.testMRCreationUsingTS : curAUR.Running_Hours_During_Month__c'+ curAUR.Running_Hours_During_Month__c);
            system.debug('TestThrustSetting.testMRCreationUsingTS : curAUR.curAUR.Cycles_During_Month__c'+ curAUR.Cycles_During_Month__c);
            curAUR.Running_Hours_During_Month__c = 100;
            curAUR.Cycles_During_Month__c = 50;
        }
        
        system.debug('asmblyURs---?>'+asmblyURs);
        update asmblyURs;
        
        RemoveAssemblyController.AssemblyWrapper wrapperObj = RemoveAssemblyController.getAssemblyData(constitutentAssembly[0].Id, ForMonthEnding);
        RemoveAssemblyController.getTsnCsnValuesOnDateChange(JSON.serialize(wrapperObj), ForMonthEnding.addDays(1));
        RemoveAssemblyController.saveData(JSON.serialize(wrapperObj), ForMonthEnding); 
    }
    
}