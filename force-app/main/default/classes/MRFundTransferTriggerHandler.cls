public class MRFundTransferTriggerHandler implements ITrigger {
    
    private final string triggerBefore = 'MRFundTransferTriggerHandlerBefore';
    private final string triggerAfter = 'MRFundTransferTriggerHandlerAfter';

    public MRFundTransferTriggerHandler(){}
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    public void bulkAfter(){}
    
    
    public void beforeInsert()
    {
         
    }
     
    public void beforeUpdate()
    {
        
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    { 
        system.debug('MRFundTransferTriggerHandler.beforeDelete(+)');  
      
        List<MR_Fund_TRansfer__c> listMRFundTRansfers = (list<MR_Fund_Transfer__c>)trigger.old;

        if(listMRFundTRansfers.size() > 0) {   
            delete[Select id FROM Assembly_MR_Fund_Transfer__c WHERE MR_Fund_Transfer__c IN :listMRFundTRansfers];      
        }
        system.debug('MRFundTransferTriggerHandler.beforeDelete(-)');
    }
     
    public void afterInsert()
    {
        
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        
        system.debug('MRFundTransferTriggerHandler.afterUpdate(+)');
        List<MR_Fund_TRansfer__c> listMRFundTRansfers = (list<MR_Fund_Transfer__c>)trigger.new;
        Set<Id> setMRFundId = new Set<Id>();
        if(listMRFundTRansfers.size() > 0) { 
            for(MR_Fund_Transfer__c curRec: listMRFundTRansfers){
                MR_Fund_Transfer__c oldRec = (MR_Fund_transfer__c)trigger.oldMap.get(currec.Id);
                if(((curRec.Status__c != oldRec.status__c)&& !curRec.Status__c.equals('Approved')) ||
                   (curRec.Date__c != oldRec.Date__c)){ // transaction sequence should get updated if the date on record changes
                    setMRFundId.add(curRec.Id);
                }
            }
        }
        System.debug('setMRFundId:' + setMRFundId);
        System.debug('setMRFundId:' + [Select id FROM Assembly_MR_Fund_Transfer__c WHERE MR_Fund_Transfer__c IN :setMRFundId]);
        update[Select id FROM Assembly_MR_Fund_Transfer__c WHERE MR_Fund_Transfer__c IN :setMRFundId];     
        sumTransferAmount(Trigger.new);

        system.debug('MRFundTransferTriggerHandler.afterUpdate(-)');         
    }
     
    public void afterDelete()
    {
  
    }
    public void afterUnDelete()
    {
           
    }
     
    public void andFinally(){
        // insert any audit records
        LeaseWareUtils.TriggerDisabledFlag = false; // to make Trigger disabled at apex level  
    }
    
    public static void sumTransferAmount(List<MR_Fund_Transfer__c> fundTransfers) {
        
        Set<Id> setFundTransferId = new Set<Id>();
        
        for(MR_Fund_Transfer__c transferRecord : fundTransfers) {
            if(Trigger.isUpdate && transferRecord.Status__c != ((MR_Fund_Transfer__c)Trigger.oldMap.get(transferRecord.Id)).Status__c 
                && transferRecord.Status__c == 'Approved') {                
                    setFundTransferId.add(transferRecord.Id);
                }
        }
        
        if(setFundTransferId.size() > 0) {
            
             Map<Id, TransferToSnapshotMapper> mapTransferAmount = new Map<Id, TransferToSnapshotMapper>();
            TransferToSnapshotMapper transferMapSource ;
            TransferToSnapshotMapper transferMapTarget ;
            
            for(Assembly_MR_Fund_Transfer__c assemblyRecord : [SELECT Id, Source_Assembly_MR_Lookup__c, 
                                                               Target_Assembly_MR_Lookup__c, Transfer_Amount__c,
                                                               MR_Fund_Transfer__r.id,MR_Fund_Transfer__r.Date__c
                                                               FROM Assembly_MR_Fund_Transfer__c 
                                                               WHERE MR_Fund_Transfer__c IN :setFundTransferId
                                                               AND Source_Assembly_MR_Lookup__c != NULL
                                                               AND Target_Assembly_MR_Lookup__c != NULL
                                                               AND Transfer_Amount__c != NULL]) {
                
                Decimal totalAmount;
                
                Id sourceId = assemblyRecord.Source_Assembly_MR_Lookup__c;
                Id targetId = assemblyRecord.Target_Assembly_MR_Lookup__c;
                
				transferMapSource = new TransferToSnapshotMapper(assemblyRecord.id,0.00,assemblyRecord.MR_Fund_Transfer__r.Date__c);  
                transferMapTarget = new TransferToSnapshotMapper(assemblyRecord.id,0.00,assemblyRecord.MR_Fund_Transfer__r.Date__c); 

                if(!mapTransferAmount.containsKey(sourceId)) {
                    mapTransferAmount.put(sourceId,transferMapSource);
                }
                
                totalAmount = mapTransferAmount.get(sourceId).transferAmount;
                totalAmount -= assemblyRecord.Transfer_Amount__c;
                transferMapSource.transferAmount = totalAmount;
                mapTransferAmount.put(sourceId,transferMapSource);
                
                
                if(!mapTransferAmount.containsKey(targetId)) {
                    mapTransferAmount.put(targetId, transferMapTarget);
                }
                
                totalAmount = mapTransferAmount.get(targetId).transferAmount;
                totalAmount += assemblyRecord.Transfer_Amount__c;
                transferMapTarget.transferAmount = totalAmount;
                mapTransferAmount.put(targetId,transferMapTarget);
                
            }
            
            List<Assembly_MR_Rate__c> assemblyMRInfos = new List<Assembly_MR_Rate__c>();
            for(Assembly_MR_Rate__c assemblyInfo : [SELECT Id ,Snapshot_Event_Record_ID__c
                                                    FROM Assembly_MR_Rate__c 
                                                    WHERE Id IN: mapTransferAmount.keyset()]) {
                
                
                if(mapTransferAmount.containsKey(assemblyInfo.Id)) {
                	assemblyInfo.Snapshot_Event_Record_ID__c = 'T'+ mapTransferAmount.get(assemblyInfo.Id).transferRecord; 
                	assemblyMRInfos.add(assemblyInfo);  
                }                                                              
            }
            
            if(assemblyMRInfos.size() > 0) {
                update assemblyMRInfos;
            }
        }
    }
	private class TransferToSnapshotMapper{
        
        public Id transferRecord;
        public Double transferAmount;
        public Date transferDate;
        
        TransferToSnapshotMapper(Id transferRecord,Decimal transferAmount,Date transferDate){
            this.transferRecord = transferRecord;
            this.transferAmount = transferAmount;
            this.transferDate = transferDate;
        }        
    
	}
}