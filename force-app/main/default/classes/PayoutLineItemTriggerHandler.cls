public inherited sharing class PayoutLineItemTriggerHandler implements ITrigger {
    private final string triggerBefore = 'PayoutLineItemTriggerHandlerBefore';
    private final string triggerAfter = 'PayoutLineItemTriggerHandlerAfter';
    
    public PayoutLineItemTriggerHandler() {

    }
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){}
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){

    }
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){

        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);

        system.debug('PayoutLineItemTriggerHandler.beforUpdate(+)');
        Map<String, Schema.SObjectField> mapInvField = Schema.SObjectType.Payout_Line_Item__c.fields.getMap();
        set<String> setSkipFields = RLUtility.getAllFieldsFromFieldset('Payout_Line_Item__c','SkipFieldsFromValidations');
        for(payout_line_item__c poLI :(list<payout_line_item__c>)trigger.new){
            if(LeaseWareUtils.isFromTrigger('PayoutTriggerHandlerAfter'))continue;
            payout_line_item__c oldPoLI = (payout_line_item__c)trigger.oldMap.get(poLI.Id);
            for(String Field:mapInvField.keyset()){
                Schema.DescribeFieldResult thisFieldDesc = mapInvField.get(Field).getDescribe();
                if(setSkipFields.contains(thisFieldDesc.getLocalName()))continue;
                if(poLI.get(thisFieldDesc.getLocalName()) !=  oldPoLI.get(leasewareutils.getNamespacePrefix() + thisFieldDesc.getLocalName())){ 
                    system.debug('Local Name::'+thisFieldDesc.getLocalName());
                    poLI.addError('\'Payout Line Item\' record cannot be manually modified');
                    break; 
                }
            }

        }
            
        system.debug('PayoutLineItemTriggerHandler.beforUpdate(-)');
    }
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){
    }
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        system.debug('PayoutLineItemTriggerHandler.afterInsert(+)');
        LeaseWareUtils.setFromTrigger(triggerAfter);
        updatePayableLineItemBalance();
        system.debug('PayoutLineItemTriggerHandler.afterInsert(-)');
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        system.debug('PayoutLineItemTriggerHandler.afterUpdate(+)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        updatePayableLineItemBalance();
        system.debug('PayoutLineItemTriggerHandler.afterUpdate(-)');
    }
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(){}
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterUnDelete(){}    
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){}

    /***
     * updates the balance due field on the payable line item
     * called from after update
     */
     private void updatePayableLineItemBalance(){
         system.debug('updatePayableLineItemBalance(+)');
         set<id> setPayoutID =  new set<id>();
         list<payout_line_item__c> newList =(list<payout_line_item__c>)trigger.new;
         map<id,list<payout_line_item__c>> mapPayoutPyLI= new map<id,list<payout_line_item__c>>();
         list<payout_line_item__c> payLIList = null;
         map<id,list<id>> mapPayablePayouts =  new map<id,list<id>> ();
         list<py_line_item__c> payableLIToUpdate =  new list<py_line_item__c>();
         for(payout_line_item__c poutLI : newlist){
             if('Approved'.equals(poutLI.status__c)){
                payLIList = mapPayoutPyLI.get(poutLI.Payouts__c);
                if( payLIList == null){
                    payLIList = new list<payout_line_item__c>();
                }
                payLIList.add(poutLI);
                mapPayoutPyLI.put(poutLI.Payouts__c,payLIList);//payouts and list of payout line items: currently only one line item
             }
         }
         if(mapPayoutPyLI.size()>0){
            for(payable_line_item__c pyLI :[select id,payout__c,payable__c from payable_line_item__c where payout__c IN:mapPayoutPyLI.keySet()]){
                list<id> poList =  mapPayablePayouts.get(pyLI.payable__c);
                if(poList ==  null){poList = new list<id>();}
                polist.add(pyLi.payout__c);
                mapPayablePayouts.put(pyLI.payable__c,polist);//map of payables and corresponding payouts;
            }
            map<id,payable__c> mapPayable = new  map<id,payable__c>([select id,(select id,Line_Payout_Amount__c from payable_line_item__r) from payable__c where id IN :mapPayablePayouts.keyset()]); 
            for(Id payableId :mapPayablePayouts.keySet()){
                py_line_item__c payLI = mapPayable.get(payableId).payable_line_item__r[0];//expecting only one record with current design
                list<Id> payoutList = mapPayablePayouts.get(payableId);
                for(Id poId:payoutList ){
                    payout_line_item__c poLI = mapPayoutPyLI.get(poId)[0];//expecting only 1 payout line item.
                    payLI.Line_Payout_Amount__c = leaseWareUtils.zeroIfNull(payLI.Line_Payout_Amount__c)+leaseWareUtils.zeroIfNull(poLI.Payout_Line_Amount__c);
                }
                payableLIToUpdate.add(payLI);
            }
             if(payableLIToUpdate.size()>0){
                LeasewareUtils.setTriggersAndWFsOff(); 
                    update payableLIToUpdate;
                LeasewareUtils.setTriggersAndWFsOn(); 
             }
         }
         system.debug('updatePayableLineItemBalance(-)');
     }

}    