public class DealProposalTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'DealProposalTriggerHandlerBefore';
    private final string triggerAfter = 'DealProposalTriggerHandlerAfter';
    public DealProposalTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('DealProposalTriggerHandler.beforeInsert(+)');

        Set<ID> setOpIds = new Set<ID>();
        Set<ID> setMCampIds = new Set<ID>();
        list<Deal_Proposal__c> existingRec;
        map<id,set<id>> mapOperIds = new map<id,set<id>>();
        list<Deal_Proposal__c> newRec = (list<Deal_Proposal__c>)trigger.new ;
        for (Deal_Proposal__c curDP:newRec) {
            setOpIds.add(curDP.Prospect__c);
            setMCampIds.add(curDP.Deal__c);
        }
        
        existingRec = [select id,Prospect__c,Deal__c from Deal_Proposal__c where Deal__c in :setMCampIds and Id not in :newRec] ;

        if(!LeaseWareUtils.isSetupDisabled('Allow_Campaigns_With_Same_Prospect')){
            //checkDuplicates :  passing mapOperIds is pass by reference, we can use it later part of code.
            checkDuplicates(existingRec,newRec,mapOperIds);
        }
    
        map<Id, Operator__c> mapOpRep = new map<id, Operator__c>([select id, Marketing_Representative_Contact__c 
                from Operator__c where id in :setOpIds]);
                
        for (Deal_Proposal__c curDP:newRec) {
            try{
                curDP.Marketing_Representative__c = mapOpRep.get(curDP.Prospect__c).Marketing_Representative_Contact__c;
            }catch(exception e){
                system.debug('Ignoring exception ' + e.getMessage());
            }
        }

        system.debug('DealProposalTriggerHandler.beforeInsert(-)');     
    }
     
    public void beforeUpdate()
    {
        //Before check  : keep code here which does not have issue with multiple call .
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('DealProposalTriggerHandler.beforeUpdate(+)');

        Set<ID> setOpIds = new Set<ID>();
        Set<ID> setMCampIds = new Set<ID>();
        list<Deal_Proposal__c> existingRec;
        map<id,set<id>> mapOperIds = new map<id,set<id>>();
        list<Deal_Proposal__c> newRec = (list<Deal_Proposal__c>)trigger.new ;
        for (Deal_Proposal__c curDP:newRec) {
            setOpIds.add(curDP.Prospect__c);
            setMCampIds.add(curDP.Deal__c);
        }
        
        existingRec = [select id,Prospect__c,Deal__c from Deal_Proposal__c where Deal__c in :setMCampIds and Id not in :newRec] ;

        if(!LeaseWareUtils.isSetupDisabled('Allow_Campaigns_With_Same_Prospect')){
            //checkDuplicates :  passing mapOperIds is pass by reference, we can use it later part of code.
            checkDuplicates(existingRec,newRec,mapOperIds);
        }
    
        map<Id, Operator__c> mapOpRep = new map<id, Operator__c>([select id, Marketing_Representative_Contact__c 
                from Operator__c where id in :setOpIds]);
                
        for (Deal_Proposal__c curDP:newRec) {
            try{
                curDP.Marketing_Representative__c = mapOpRep.get(curDP.Prospect__c).Marketing_Representative_Contact__c;
            }catch(exception e){
                system.debug('Ignoring exception ' + e.getMessage());
            }
        }       

        system.debug('DealProposalTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('DealProposalTriggerHandler.beforeDelete(+)');

  		Deal_Proposal__c[] newProspects = (list<Deal_Proposal__c>)trigger.old;
  		for(Deal_Proposal__c curRec  :newProspects){
  			system.debug('curRec.Campaign_Active__c='+curRec.Campaign_Active__c);
  			if(!curRec.Campaign_Active__c){
  				curRec.addError('You cannot delete prospects from an inactive marketing campaign.');
  			}
  		}        
        
        system.debug('DealProposalTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DealProposalTriggerHandler.afterInsert(+)');

        set<Id> setDealIds = new set<Id>();
        list<Deal_Proposal__c> newRec = (list<Deal_Proposal__c>)trigger.new ;
        //Get the list of Deal Ids
        for(Deal_Proposal__c curDP:newRec){
            setDealIds.add(curDP.Deal__c);
        } 
        
        //Get the list of all aircraft on the Deals
        list<Aircraft_In_Deal__c> listACsInDeal = new list<Aircraft_In_Deal__c>([select Id, Aircraft__c, MSN__c, Marketing_Deal__c 
                from Aircraft_In_Deal__c 
                where Marketing_Deal__c in :setDealIds]); 
        
        list<MSN_s_Of_Interest__c> listDealTermsToInsert = new list<MSN_s_Of_Interest__c>();
        for(Deal_Proposal__c curDP:newRec){
            list<Aircraft_In_Deal__c> listRelatedACsInDeal = new list<Aircraft_In_Deal__c>();
            listRelatedACsInDeal.clear();
            for(Aircraft_In_Deal__c curACInDeal: listACsInDeal){
                if(curACInDeal.Marketing_Deal__c==curDP.Deal__c){
                    listRelatedACsInDeal.add(curACInDeal);
                }
            }
            
            //Ensure that at least one aircraft is present on the campaign before allowing proposal creation.
            //It doesn't make sense to allow proposal creation if there are no ACs on the Campaign.
            
            if(listRelatedACsInDeal.size()==0){
                curDP.addError('Please market at least one aircraft before creating proposal.');
                continue;
            }       
            //Auto create MoI If new prospect get added
            for(Aircraft_In_Deal__c CurAIC:listRelatedACsInDeal){
            	listDealTermsToInsert.add(
            			new MSN_s_Of_Interest__c(Name='Dummy - Will be replaced by WF/Field Update', Deal_Proposal__c=curDP.Id, Aircraft__c=CurAIC.Id)
            		);
            }
            
        } 
        if(!LeaseWareUtils.isFromTrigger('CalledFromCanvass')){
            try{
				insert listDealTermsToInsert;
            }catch(System.DmlException e){
                //if(e.getMessage().contains('Terms Communicated is selected on Marketing Prospect. Please enter Rent'))
                //newRec[0].Terms_Communicated__c.addError('Please uncheck Terms Communicated while adding Prospects.');
                leasewareUtils.createExceptionLog(e, 'Error while auto creation of terms', 'Prospect - Create Term', newRec[0].Id, ' ', true);
            }
        }
        
        system.debug('DealProposalTriggerHandler.afterInsert(-)');      
    }
     
    public void afterUpdate()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DealProposalTriggerHandler.afterUpdate(+)');
        
		//Start Nagaveni_14_May_2018
    	//Task - Adding Weekly Summary on Main page does not add IL? Also Adding new IL does not update Weekly Summary?
    	//Create Interaction Log when Weekly Summary note on Prospect is changed.
        createILIfWSChanged();
        //End Nagaveni_14_May_2018
        
		list<Deal_Proposal__c> newRecs;
        try{
		    //Check the Custom setting and skip if the Update_Rent_Term_To_Proposed_Terms's Disable is TRUE
			if(LeaseWareUtils.isSetupDisabled('Update_Rent_Term_To_Proposed_Terms')) return;
			

            newRecs = (list<Deal_Proposal__c>)trigger.new;
            map<Id, MSN_s_Of_Interest__c> mapTerms2Upd = new map<id, MSN_s_Of_Interest__c>();
            
            map<id, Deal_Proposal__c> mapProspsWithTerms = new map<id, Deal_Proposal__c>([select id, 
                    (select id, Rent_000__c, Term_Mos__c from Deal_Proposal_Aircraft__r) 
                    from Deal_Proposal__c 
                    where id in :newRecs]);
            for(Deal_Proposal__c curProsp : newRecs){
                Deal_Proposal__c oldRec = (Deal_Proposal__c)(trigger.oldMap.get(curProsp.id));
                decimal dNewRent = LeaseWareUtils.ZeroIfNull(curProsp.Rent__c);
                decimal dOldRent = LeaseWareUtils.ZeroIfNull(oldRec.Rent__c);
                decimal dNewTerm = LeaseWareUtils.ZeroIfNull(curProsp.Term_Mos__c);
                decimal dOldTerm = LeaseWareUtils.ZeroIfNull(oldRec.Term_Mos__c);
				
                system.debug('curProsp.Y_Hidden_Count_Of_Terms__c = '+curProsp.Y_Hidden_Count_Of_Terms__c);
				
				//The "only one term" check has been removed. For futher details check the below asana
				//Copy Proposed Terms to all Aircraft under a Prospect in a Campaign
				//https://app.asana.com/0/786168502502271/1108579460181935/f
            
                	if(curProsp.Copy_Rent_Term_To_Proposed_Terms__c == true && (dNewRent != dOldRent || dNewTerm != dOldTerm)){
						for(MSN_s_Of_Interest__c curTerm : mapProspsWithTerms.get(curProsp.id).Deal_Proposal_Aircraft__r){
                            if(LeaseWareUtils.ZeroIfNull(curTerm.Rent_000__c) == dOldRent || LeaseWareUtils.ZeroIfNull(curTerm.Rent_000__c) == 0){
								curTerm.Rent_000__c = dNewRent;
								system.debug('dNewRent final ='+dNewRent);
								mapTerms2Upd.put(curTerm.Id, curTerm);
							}
							if(LeaseWareUtils.ZeroIfNull(curTerm.Term_Mos__c) == dOldTerm || LeaseWareUtils.ZeroIfNull(curTerm.Term_Mos__c) == 0){
								curTerm.Term_Mos__c = dNewTerm;
								system.debug('dNewTerm final ='+dNewTerm);
								mapTerms2Upd.put(curTerm.Id, curTerm);
							}
						}
					}
				
				
				
            }
            if(mapTerms2Upd.size()>0)update mapTerms2Upd.values();
            
        }
        catch(DmlException  e){
            system.debug('DmlException  '+ e.getMessage());
            if(newRecs != null || newRecs.size() > 0)
            	newRecs[0].addError('Please enter the mandatory data on the Commercial Terms before proceeding with the Rent and Terms update.');
        }
        catch(exception e){
            system.debug('Ignore '+ e.getMessage());
        }
            
        
        system.debug('DealProposalTriggerHandler.afterUpdate(-)');      
    }
     
    public void afterDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DealProposalTriggerHandler.afterDelete(+)');
        
        
        system.debug('DealProposalTriggerHandler.afterDelete(-)');      
    }

    public void afterUnDelete()
    {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('DealProposalTriggerHandler.afterUnDelete(+)');
        
        // code here
        
        system.debug('DealProposalTriggerHandler.afterUnDelete(-)');        
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

    private void checkDuplicates(list<Deal_Proposal__c> existingRec,list<Deal_Proposal__c> newrecs,map<id,set<id>> mapOperIds){

        // set to map
        if(existingRec!=null || existingRec.size()>0){
            for(Deal_Proposal__c curDP:existingRec){
                if(mapOperIds.containsKey(curDP.Deal__c)){
                    ((Set<ID>)mapOperIds.get(curDP.Deal__c)).add(curDP.Prospect__c);
                }
                else{
                    mapOperIds.put(curDP.Deal__c, new Set<ID>{curDP.Prospect__c});
                }
            }
        }
        // check duplicates
            for(Deal_Proposal__c curDP:newrecs){
                if(mapOperIds.containsKey(curDP.Deal__c)){
                    if(((Set<ID>)mapOperIds.get(curDP.Deal__c)).contains(curDP.Prospect__c)){
                        // found duplicate
                        curDP.Prospect__c.addError('The prospect ' + curDP.Y_Hidden_Prospect_Customer__c + ' already exists under this campaign. You can add a prospect to a campaign only once.');
                    }
                }
            }       
        
    }
    
    //Start Nagaveni_14_May_2018
    //Task - Adding Weekly Summary on Main page does not add IL? Also Adding new IL does not update Weekly Summary?
    //Called afterUpdate
    //When Weekly Summary Notes is updated when Prospect is getting added , interaction log is created. 
    
    public void createILIfWSChanged(){

    	system.debug('DealProposalTriggerHandler.createILIfWSChanged - Enter');  
        
        if( !( 'ALWAYS ON'.equals(LeaseWareUtils.getLWSetup_CS('AUTO_IL_CREATE')) || 'ON EXCEPT DEAL CREATION'.equals(LeaseWareUtils.getLWSetup_CS('AUTO_IL_CREATE')) ) ){
            return ;// no need to create any IL
        }
 
        
		list<Deal_Proposal__c> newList = (list<Deal_Proposal__c>)trigger.new;
		Deal_Proposal__c oldrec;
		list<Customer_Interaction_Log__c> insertIL = new list<Customer_Interaction_Log__c>();
            
		for(Deal_Proposal__c curRec:newList){
			oldrec = (Deal_Proposal__c)trigger.oldMap.get(curRec.Id);
		    if(
		    	(
		    		(curRec.Weekly_Summary_Notes__c!=null &&  oldrec.Weekly_Summary_Notes__c==null) 
		    		|| (curRec.Weekly_Summary_Notes__c!=null && curRec.Weekly_Summary_Notes__c.stripHtmlTags()!=oldrec.Weekly_Summary_Notes__c.stripHtmlTags()) 
		    	)
		    	
		      ){
                	insertIL.add( new Customer_Interaction_Log__c(
                    				name= 'Meeting with ' + curRec.Name + ' on ' + system.today().format()
									
									,Date_Of_Call__c = system.today()
									,Related_To_Deal_Proposal__c = curRec.ID
									,Summary__c = curRec.Weekly_Summary_Notes__c
								)
							);		    	
		        }//end of if
		}//end of for
            
        if(!insertIL.isEmpty()) {
        	LeasewareUtils.setFromTrigger('DO_NOT_UPDATE_DEAL_PROPOSAL_REC');
            insert insertIL;
            LeasewareUtils.unsetTriggers('DO_NOT_UPDATE_DEAL_PROPOSAL_REC');
        }
            
		system.debug('DealProposalTriggerHandler::createILIfWSChanged - Exit');  
	}
    //End Nagaveni_14_May_2018
    
}