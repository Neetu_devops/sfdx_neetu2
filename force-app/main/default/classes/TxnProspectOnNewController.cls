public class TxnProspectOnNewController extends PaymentNewControllerBase {

    public TxnProspectOnNewController(ApexPages.StandardController ingored) {
        super(Transaction__c.SObjectType, Transaction_Proposal__c.SObjectType);
    }

}