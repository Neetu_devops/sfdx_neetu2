/**
*******************************************************************************
* Class: GenericRelatedListController
* @author Created by Bhavna, Lease-Works, 
* @date 30/05/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to create a generic related list for the given Object
*
* History:
* - VERSION   DEVELOPER NAME    	DATE         	DETAIL FEATURES
*
********************************************************************************
*/
public with sharing class GenericRelatedListController {
    /**
    * @description Retreives details related to given child object based on given fieldset with respect to given parent
    *
    * @param parentId - Id, contains selected parent's Id
    * @param parentField - String, contains Parent's fieldname for the given Child Object
    * @param relatedObjectName - String, Related Child Object name
    * @param fieldSetName - String, fieldSet name to query data for specific fields given in FieldSet
    *
    * @return rlw - RelatedListWrapper, RelatedListWrapper record
    */
    @AuraEnabled
    public static RelatedListWrapper getRelatedListData(Id parentId, String parentField, String relatedObjectName, 
                                                        String fieldSetName, String sortOrderField) {
        System.debug('parentId :'+parentId+'parentField :'+parentField+'relatedObjectName :'+relatedObjectName+'fieldSetName :'+fieldSetName+'sortOrderField :'+sortOrderField);                                                    
        List<ColumnWrapper> colList = new List<ColumnWrapper>();       
                
        //retrieving Fieldset for given object
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(relatedObjectName);
        Schema.FieldSet fieldSet = RLUtility.describeFieldSet(objectDescribe, fieldSetName);
        
        List<String> objList = new List<String>{relatedObjectName};
        Map<String,Schema.SObjectField> mapfields = Schema.describeSObjects(objList)[0].fields.getMap(); 
        
        Map<String, Schema.DescribeFieldResult> mapFldDesc = new Map<String, Schema.DescribeFieldResult>();
        for(String fld : mapfields.keySet()){
            mapFldDesc.put(fld, mapfields.get(fld).getDescribe());
        }

        //retrieving Icon name for the object
        String iconName = RLUtility.findIconName(relatedObjectName);
        
        //Dynamic query for given related child object
        String query = 'Select Id';        
        for(Schema.FieldSetMember fsm : fieldSet.getFields()) {
            Integer precisionDigit;
            if(fsm.getType() == Displaytype.Currency || fsm.getType() == Displaytype.Percent){
                precisionDigit = mapFldDesc.get(fsm.getFieldPath().toLowercase()).getScale();
            }
            if (fsm.getType() == Displaytype.reference){
                if (fsm.getFieldPath().contains('__c')){
                	query += ',' + fsm.getFieldPath().replace('__c','__r') + '.Name' ;
                }
                else {
                    query += ',' + fsm.getFieldPath().left(String.valueOf(fsm.getFieldPath()).length() - 2) + '.Name' ;
                }                
            }
            
            query += ', ' + fsm.getFieldPath();
            
            //creating columnwrapper list for DataTable headers
            ColumnWrapper cw = new ColumnWrapper(fsm.getLabel(), fsm.getFieldPath(), String.valueOf(fsm.getType()),precisionDigit);            
            colList.add(cw);
        }        
        query += ' from ' + relatedObjectName + ' where ' + parentField + '= :parentId';
        
        if (sortOrderField != null && sortOrderField != ''){
            query += ' order by '+sortOrderField;
        }
        else{
            query += ' order by CreatedDate';
        }

        //Querying Related Child Object Records
        System.debug('Query : '+query);
        List<sObject> objRecordList = Database.query(query);
        
        //creating RelatedListWrapper
        RelatedListWrapper rlw = new RelatedListWrapper(colList, objRecordList, iconName, objectDescribe.getLabelPlural());
        
        return rlw;
    }
    //Overide base method for AssetINDeal component Related table 
    @AuraEnabled
    public static  RelatedListWrapper getAssetInDealData(Id parentId, String parentField,String relatedObjectName,
                                                         String fieldSetName) {
        System.debug('parentId :'+parentId+'parentField :'+parentField+'relatedObjectName :'+relatedObjectName+'fieldSetName :'+fieldSetName);                                                    
        List<ColumnWrapper> colList = new List<ColumnWrapper>();       
                
        //retrieving Fieldset for given object
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(relatedObjectName);
        Schema.FieldSet fieldSet = RLUtility.describeFieldSet(objectDescribe, fieldSetName);
        
            List<String> objList = new List<String>{relatedObjectName};
            Map<String,Schema.SObjectField> mapfields = Schema.describeSObjects(objList)[0].fields.getMap(); 
                                                            
            Map<String, Schema.DescribeFieldResult> mapFldDesc = new Map<String, Schema.DescribeFieldResult>();
            for(String fld : mapfields.keySet()){
                mapFldDesc.put(fld, mapfields.get(fld).getDescribe());
            }
        //retrieving Icon name for the object
        String iconName = RLUtility.findIconName(relatedObjectName);
        System.debug('relatedObjectName : '+relatedObjectName+ ' iconName : '+iconName);
         
        //Dynamic query for given related child object
        String query = 'SELECT Id ';        
        for(Schema.FieldSetMember fsm : fieldSet.getFields()) {
            Integer precisionDigit;
            if(fsm.getType() == Displaytype.Currency || fsm.getType() == Displaytype.Percent){
                if(mapFldDesc.containsKey(fsm.getFieldPath())){
                    precisionDigit = mapFldDesc.get(fsm.getFieldPath().toLowercase()).getScale();
                }
            }
            if (fsm.getType() == Displaytype.reference){
                if (fsm.getFieldPath().contains('__c')){
                	query += ',' + fsm.getFieldPath().replace('__c','__r') + '.Name' ;
                }
                else {
                    query += ',' + fsm.getFieldPath().left(String.valueOf(fsm.getFieldPath()).length() - 2) + '.Name' ;
                }                
            }
            
            query += ', ' + fsm.getFieldPath();
            
            //creating columnwrapper list for DataTable headers
            ColumnWrapper cw = new ColumnWrapper(fsm.getLabel(), fsm.getFieldPath(), String.valueOf(fsm.getType()),precisionDigit);            
            colList.add(cw);
        }        
        List <Aircraft_Proposal__c> listOfAssets = [SELECT Id, Aircraft__c 
                                                    FROM Aircraft_Proposal__c
                                                    WHERE Id =: parentId ];
        System.debug('listOfAssets :'+listOfAssets);
        System.debug('getRecords parameters : assetId : '+parentId);
        Set<Id> aircraft = new Set<Id>();
        Boolean active = False;                                                     
        for(Aircraft_Proposal__c asset :listOfAssets){
            aircraft.add(asset.Aircraft__c);
        }     
        System.debug('List of aircraft :'+aircraft);
        query += ' FROM ' + relatedObjectName + ' WHERE ' + parentField + ' IN:  aircraft  AND Inactive_AT__c ='+active;
        query += ' ORDER BY CreatedDate';
        

        //Querying Related Child Object Records
        System.debug('Query : '+query);
        List<sObject> objRecordList = Database.query(query);
        System.debug('objRecordList : '+objRecordList);
                                                             
        //creating RelatedListWrapper
        RelatedListWrapper rlw = new RelatedListWrapper(colList, objRecordList, iconName, objectDescribe.getLabelPlural());
        System.debug('rlw : '+rlw);
        return rlw;
    }
    
    /**
    * @description Deletes given child record 
    *
    * @param delRecordId - String, contains related child record Id to be deleted
    * @param objectName - String, contains child object name whose record need to be deleted
    *
    * @return String, message of success or failure
    */
    @AuraEnabled
    public static String deleteRecord(String delRecordId, String objectName) {
        try {
            String query = 'Select Id from ' + objectName + ' where Id = :delRecordId';
            sObject objRecord = Database.query(query);
            
            delete objRecord;
            
            return 'Success';
        } catch(DMLException e) {
            return e.getDmlMessage(0);
        }
    }
   
    
    /**
    * @description Retreives details of the Page layout assigned to logged in user for RelatedList record 
    *  	when RelatedList record is inserted, edited or cloned
    *
    * @param recordId - String, contains Parent's Id for New RelatedList record or selected RelatedList record's Id for 
    *  	Edit or Cloning of RelatedList record
    *
    * @return plw - PageLayoutWrapper, PageLayoutWrapper record
    */
    @AuraEnabled 
    public static String getPageLayoutFields(String recordId, String objectName, String recordTypeId) {
        sObject objRecord;
        String Result;
        //Finding RecordId prefix
        LOISectionWrapper loiWrapper  = getRecordIdPrefix(objectName);
        String prefix = loiWrapper.prefix;
                
        //Querying RelatedList record if RecordId is of RelatedList record
        if(recordId != null) {
            if(prefix == String.valueOf(recordId).left(3)) {
                Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(objectName);
                System.debug('object:' + objectDescribe);
                Map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap();
                
                
                String query = 'Select Id';         
                for(String fieldName : fieldMap.keySet()) { 
                    if(fieldName != 'Id') {
                        query += ', ' + fieldName;
                    }            
                }
                query += ' from ' + objectName + ' where Id = : recordId';
                     
                objRecord = Database.query(query);
                System.debug('objRecord :' +objRecord);  
            }
        }
        
        //Finding Deal's RecordTypeId        
        String idRecType;
        String layoutname;
        String packageType;
        
        if(recordTypeId != null && recordTypeId != '') {            
            idRecType = recordTypeId;
            System.debug('if idRecType :' +idRecType);  
            
        }
        else {
            idRecType = getObjectRecTypeId(recordId, objectName);
            System.debug('else idRecType :' +idRecType);  
        }
                
        
        //Finding PageLayout name for given RecordTypeId for current user
        if(!Test.isRunningTest()) {
            try {
                String objectId = [Select Id, DurableId from EntityDefinition where QualifiedApiName = :objectName].DurableId;
                System.debug('objectId :' +objectId);
                if(idRecType.indexOf('Master') != -1) {    
                    System.debug('Inside if');
                    String body = LeaseWareStreamUtils.toolingAPISOQL('Select Layout.Name from ProfileLayout where TableEnumOrId=\'' + objectId + '\' AND Profile.Name=\'System Administrator\''); 
                    System.debug('outside if'+body);
                    layoutname = body.substringBetween('"Name":"', '"');  
                    System.debug('if layoutname :' +layoutname);
                    idRecType = null; 
                }
                else {
                    layoutname =  LeaseWareStreamUtils.getLayoutNameForCurrentUserProfile(idRecType);
                    System.debug('else layoutname :' +layoutname);
                }
                
                String body =  LeaseWareStreamUtils.toolingAPISOQL('Select ManageableState from Layout where TableEnumOrId=\'' + objectId + '\' AND Layout.Name=\'' + layoutname + '\''); 
                packageType = body.substringBetween('"ManageableState":"', '"');
                
            } catch(System.CalloutException exp) {
                 //leasewareUtils.createExceptionLog(exp,exp.getMessage(), null, null, null, true);
                 throw new AuraHandledException('API error, please contact your LeaseWorks administrator [Exception: ' + exp.getMessage() + ']');
            }
            
            if(packageType == 'unmanaged') {
                layoutname = objectName + '-' + layoutname; 
                System.debug('layoutname :'+layoutname);
            }
            else {
                layoutname = objectName + '-' + leasewareutils.getNamespacePrefix() + layoutname; 
            }     
        }
        else {
            layoutname = leasewareutils.getNamespacePrefix() +'Pricing_Run_New__c-' + leasewareutils.getNamespacePrefix() + 'Economic Analysis Request - MA Layout'; 
        }
        system.debug('getPageLayoutFields Layoutname=' + layoutname);
        
        //You can give multiple page layout names here as well
        List<String> componentNameList = new List<String>{layoutname};
            //Retrieve Page layout details 
            List<Metadata.Metadata> components = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, componentNameList);
            //Missing Layout error Handling - DEV
            if(!components.isEmpty()){
                Metadata.Layout economicLayout = (Metadata.Layout) components.get(0);
                List<LayoutSection> lstSections = new List<LayoutSection>();
                
                //We are going to find the fields names and will keep them according to columns so, 
                //we can show them as per page layout 
                for(Metadata.LayoutSection ls : economicLayout.layoutSections) {           
                    LayoutSection section = new LayoutSection(ls.layoutColumns.size());
                    section.label = ls.label;
                    System.debug('Name****** '+section.label);
                    
                    List<LayoutColumn> lstColumns = new List<LayoutColumn>();
                    Integer maxFieldsInColumn = 0;
                    
                    for(Metadata.LayoutColumn lc : ls.layoutColumns) {                
                        LayoutColumn column = new LayoutColumn();
                        
                        //check if there are fields available in that column
                        if(lc.layoutItems != null) { 
                            //Get the max number of fields in a column to preserve the alignment 
                            if(maxFieldsInColumn < lc.layoutItems.size()) {
                                maxFieldsInColumn = lc.layoutItems.size();
                            }
                            
                            for(Metadata.LayoutItem li : lc.layoutItems) {
                                //if(li.behavior != Metadata.UiBehavior.ReadOnly ) {
                                //Pass the LayoutItem object in the LayoutField consturctor	    
                                column.lstFields.add( new LayoutField( li ) );
                                //}  
                            }
                        }
                        
                        //No need to add a column in the section if there is no field available 
                        if(column.lstFields.size() > 0) {
                            lstColumns.add( column );
                        }
                    }
                    
                    //Now, we need to arrange the fields in section so we can use them in the iteration 
                    //on the component so we will have to arrange them in the order 
                    if(maxFieldsInColumn > 0) {
                        for(Integer i = 0; i < maxFieldsInColumn; i++) {
                            for(Integer j = 0; j < lstColumns.size(); j++) {
                                if(lstColumns[j].lstFields.size() > i) {
                                    section.lstFields.add(lstColumns[j].lstFields[i]);    
                                }    
                                else {
                                    section.lstFields.add(new LayoutField());
                                }
                            }    
                        }    
                    }
                    
                    Leasewareutils.insertExceptionLogs();
                    System.debug('lstSections');
                    lstSections.add(section);
                }
                
                PageLayoutWrapper plw = new PageLayoutWrapper(lstSections, objRecord, idRecType);
                Result= JSON.serialize(plw);
            }
            else{ //Setting the reult Null In case there is no page layout found.
                Result = null;
            }
        
        return Result;
    }
    
    
    /**
    * @description find given recordId object prefix to check either record is of Parent or RelatedList record
    *
    * @param recordId - Id, contains Parent's Id or selected RelatedList record Id
    *
    * @return loiWrapper - LOISectionWrapper, LOISectionWrapper record
    */
    public static LOISectionWrapper getRecordIdPrefix(String objectName) {
        LOISectionWrapper loiWrapper = new LOISectionWrapper();
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(objectName);
        String keyPrefix = objectDescribe.getKeyPrefix();
        
        loiWrapper.prefix = keyPrefix;
        
        return loiwrapper;
    }


    /**
    * @description find matching recordTypeId from Child object based on Parent recordType Name 
    *
    * @param parentId - Id, contains Parent's Id 
    *
    * @return relatedObjectName - contains nmae of the related list object to check for matching recordtype Name.
    */
    @AuraEnabled
    public static String fetchMatchingRTId(String parentId, string relatedObjectName) {
        String parentRTId = '';
        String recordTypeId = '';
        
        String parentObjectName = ID.valueOf(parentId).getSobjectType().getDescribe().getName();

        String query = 'SELECT id, recordTypeId FROM '+parentObjectName+' WHERE Id =:parentId';

        List<sObject> objRecords = Database.query(query);
        parentRTId = (String) objRecords[0].get('recordTypeId');

        if(parentRTId != null){
            if(Schema.getGlobalDescribe().get(parentObjectName).getDescribe().getRecordTypeInfosById().get(parentRTId) != null){
                String parentRTName = Schema.getGlobalDescribe().get(parentObjectName).getDescribe().getRecordTypeInfosById().get(parentRTId).getDeveloperName();
                
                if(Schema.getGlobalDescribe().get(relatedObjectName).getDescribe().getRecordTypeInfosByDeveloperName().containsKey(parentRTName) && Schema.getGlobalDescribe().get(relatedObjectName).getDescribe().getRecordTypeInfosByDeveloperName().get(parentRTName) != null){
                    recordTypeId = Schema.getGlobalDescribe().get(relatedObjectName).getDescribe().getRecordTypeInfosByDeveloperName().get(parentRTName).getRecordTypeId();
                }
            }
        }

        if(recordTypeId != null && recordTypeId != ''){
            return recordTypeId;
        }

        return 'Error';
    }
    
    
    
    /**
    * @description find RelatedList's RecordTypeId with respect to given Parent's RecordTypeId
    *
    * @param MAID - String, contains selected Parent's Id
    *
    * @return loiWrapper - LOISectionWrapper, LOISectionWrapper record
    */
    @AuraEnabled
    public static String getObjectRecTypeId(String recordId, String objectName) {
        String idRecType;
        String query = '';
        
        if(recordId != null && recordId != '') {
            query = 'Select Id, RecordTypeId from ' + objectName + ' where Id = :recordId';           
        }
        
        try {
            if(query != '') {
                System.debug('query::' + query);
                List<sObject> objRecords = Database.query(query);
                idRecType = (String)objRecords[0].get('RecordTypeId');
                if(idRecType == null){
                    idRecType = getRecordType(objectName);
                }
            }   
            else {
                idRecType = getRecordType(objectName);
            }
        } catch(Exception e) {
            idRecType = getRecordType(objectName);
        }    
        return idRecType;
    }    
    public static String getRecordType(String objectName){
        Schema.DescribeSObjectResult objectDescribe = RLUtility.describeObject(objectName);
        String idRecType;
        for(Schema.RecordTypeInfo rti : objectDescribe.getRecordTypeInfos()) {
            if(rti.isDefaultRecordTypeMapping()) {
                if(rti.getName() != 'Master') {
                    idRecType = rti.getRecordTypeId();
                }
                else {
                    idRecType = rti.getRecordTypeId() + '#Master';
                }
                
            }
        }
        return idRecType;
    }
       
    /**
    * @description LOISectionWrapper to hold details related to RelatedList record Name and given RecordId's object prefix
    *
    */
    public class LOISectionWrapper {
        @AuraEnabled public String prefix {get; set;}
    }
    
    
    /**
    * @description PageLayoutWrapper to hold details of selected RelatedList record PageLayout and 
    *  	RelatedList record if RelatedList is getting cloned
    *
    */
    public class PageLayoutWrapper {
        @AuraEnabled public List<LayoutSection> lstSections {get; set;}
        @AuraEnabled public sObject selectedRecord {get; set;}
        @AuraEnabled public String recordTypeId {get; set;}
        
        public PageLayoutWrapper(List<LayoutSection> lstSections, sObject selectedRecord, String recordTypeId) {
            this.lstSections = lstSections;
            this.selectedRecord = selectedRecord;
            this.recordTypeId = recordTypeId;
        }
    }
    
    
    /**
    * @description LayoutSection to hold details related to selected RelatedList record PageLayout Sections and 
    *  	associated fields with each sections
    *
    */
    public class LayoutSection {
        @AuraEnabled public String label;
        @AuraEnabled public List<LayoutField> lstFields;       
        @AuraEnabled public Integer totalColumns;
        
        public LayoutSection(Integer totalColumns) {
            this.totalColumns = totalColumns;
            this.lstFields = new List<LayoutField>();            
        }
    }
    
    
    /**
    * @description LayoutColumn to hold details related to fields of each Section of RelatedList record Page Layout
    *
    */
    private class LayoutColumn {
        private List<LayoutField> lstFields;  
        
        public LayoutColumn() {
            this.lstFields = new List<LayoutField>();
        }
    }
    
    
    /**
    * @description LayoutField to hold details related to each field on RelatedList record Page Layout
    *
    */
    public class LayoutField {
        @AuraEnabled public String fieldName;
        @AuraEnabled public Boolean isRequired;
        @AuraEnabled public Boolean isReadOnly;
        @AuraEnabled public String fieldValue;
        
        public LayoutField() {}
        
        public LayoutField(Metadata.LayoutItem li) {
            System.debug('this.fieldName>>' + li.field + '----'+ Leasewareutils.getNamespacePrefix());
            this.fieldName = li.field;          
            
            LeasewareUtils.createExceptionLog(null, 'Field Name'+ li.field , null, null,null,false);
            
            if(li.behavior == Metadata.UiBehavior.Required) {
                this.isRequired = true;
            }
            else if(li.behavior == Metadata.UiBehavior.ReadOnly ) {
                this.isReadOnly = true;
            }   
        }
    }   
    
    
    /**
    * @description RelatedListWrapper to hold given Child Data with headers and object label and icon name
    *
    */
    public class RelatedListWrapper {
        @AuraEnabled public List<ColumnWrapper> headers {get; set;}
        @AuraEnabled public List<sObject> recordList {get; set;}
        @AuraEnabled public String iconName {get; set;}
        @AuraEnabled public String relatedListName {get; set;}
        
        public RelatedListWrapper(List<ColumnWrapper> headers, List<sObject> recordList, String iconName, String relatedListName) {
            this.headers = headers;
            this.recordList = recordList;
            this.iconName = iconName;
            this.relatedListName = relatedListName;
        }
    }
    
    
    /**
    * @description ColumnWrapper to hold details related to headers of the Given Child Object Data
    *
    */
    public class ColumnWrapper {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String fieldName {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public String sortIcon {get; set;}
        @AuraEnabled public Integer precisionDigit {get; set;}
        
        public ColumnWrapper(String label, String fieldName, String type,Integer precisionDigit) {
            this.label = label;
            this.fieldName = fieldName;
            this.type = type.tolowercase();
            this.sortIcon = 'arrowdown';
            this.precisionDigit = precisionDigit;
        }
    }           
}