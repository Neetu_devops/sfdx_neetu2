/**
@ Developed by  : Arjun Srivastava
@ name            LeaseWorksMapCntrl
@ date            2016.05.01 5 PM
@ description     The controller for map page, contains logic for fetching address data and makes operators & lessors.
*/
public class LeaseWorksMapCntrl    {
    public string vintage                             {get;set;}
    public boolean boolRefreshSupplyBtn               {get;set;}
    public String zoom                                {get;set;}
    public String height                              {get;set;}
    public String width                               {get;set;}
    transient public List<Address> addresslist        {get;set;}
    public string aircraftTypeId                      {get;set;}
    public integer number_of_airlines                 {get;set;}
    public boolean searchclicked                      {get;set;}
    public Aircraft_Type__c aircraft                  {get;set;}
    public list<Aircraft_Type__c> aircrafttypes       {get;set;}    
    public Operator__c operator                       {get;set;}
    //public boolean showLessors                        {get;set;}
    public string Namespaceprefix_temp                {get;set;}  
    public list<Lease_Expiration__c> leaseExpirations {get;set;}
    public list<Current_Acquisition_Pipeline__c> aitSupply              {get;set;}
    public static final string NONEWSFOUNDMSG = NewsFeedServices.NONEWSFOUNDMSG;
    public string aircraftType;
    public string aviatorToken {get;set;}
    public static final String IL_REQUESTTYPE = 'Aircraft Lease';
	private static final string DEMANDPOPUPHEADER = 'Market Intelligence';
	private static final string SUPPLYPOPUPHEADER = 'Aircraft Availability - Current Fleet';
	private static final string SUPPLYPOPUPSUBHEADER = ' Lease Expirations';
    public LeaseWorksMapCntrl(ApexPages.StandardController controller)
    {    
        boolRefreshSupplyBtn = true;
        vintage = null;
        aviatorToken = null;
        //showLessors = false;
        aircraft = new Aircraft_Type__c();
        operator = new Operator__c();           
        number_of_airlines = 0;
        width ='1200';  
        searchclicked=false;
        zoom = '2';
        aircraftTypeId = ApexPages.currentPage().getParameters().get('id');
        //if(aircraftTypeId!=null)
        //{
            height ='600';
            //showLessors = true;
        //}
        //else
            //height ='650';  
        
        Namespaceprefix_temp = '';
        List<ApexClass> cls = [SELECT NamespacePrefix FROM ApexClass where NamespacePrefix='leaseworks' limit 1]; 
        if(cls .size()>0) 
        { 
            Namespaceprefix_temp  = cls[0].NamespacePrefix + '__'; 
        }
        
        aviatorToken = NewsFeedServices.requestAviatorApiToken();
        showOperators();    
    }          
    
   public void aircraftTypeChanged()
   {
       if( !aircrafttypes.isEmpty() )
       {           
           if( aircraft.Aircraft_Type__c!= aircrafttypes[0].Aircraft_Type__c )
           {       
                boolRefreshSupplyBtn = false;   
           }
           else
               boolRefreshSupplyBtn = true; 
       }    
          else 
            boolRefreshSupplyBtn = false;   
    
        system.debug('boolRefreshSupplyBtn=='+boolRefreshSupplyBtn);
   }   
    
   public void clearsearch()
   {
     system.debug('searchclicked=='+searchclicked);
     if( searchclicked )
     {
        vintage = null;
        aircraftTypeId = ApexPages.currentPage().getParameters().get('id');
        //showLessors   = true;
        searchclicked = false;
        zoom = '2';
        aircraft = new Aircraft_Type__c();
        operator = new Operator__c();
        showOperators();
     }
   }

   public void refreshLeaseExpiration()
   {
        system.debug('aircraftTypeId=='+aircraftTypeId);
        Aircraft_Type__c ATRec = [select id,Name,Aircraft_Type__c,Aircraft_Variant__c,Engine_Type__c,Acquisition_Type__c,Lease_Expiration_Date__c  from Aircraft_Type__c where Id = :aircraftTypeId];
        AircraftTypeHandler.createLeaseExpirations(ATRec);
        system.debug('call showoperator method');
        searchclicked=true;
        clearsearch(); // added to refresh the pafge after button click         
   }
   
   private string createAircraftTypeSoql(  )
   {
       string soqlQuery = 'SELECT id, Name,'+Namespaceprefix_temp+'Aircraft_Type__c,'+Namespaceprefix_temp+'Engine_Type__c,'
                           + Namespaceprefix_temp+'Engine_Variant__c,'+'( SELECT Id,'+Namespaceprefix_temp+'Aircraft__c,'+Namespaceprefix_temp
                           + 'Aircraft_Type__c,'+Namespaceprefix_temp+'Aircraft__r.'+Namespaceprefix_temp+'Aircraft_Variant__c,'+Namespaceprefix_temp
                           + 'Aircraft__r.'+Namespaceprefix_temp+'Engine_Type__c,'+Namespaceprefix_temp+'Aircraft__r.'+Namespaceprefix_temp+'Date_of_Manufacture__c,'
                           + Namespaceprefix_temp+'Aircraft__r.'+Namespaceprefix_temp+'Aircraft_Type__c,'+Namespaceprefix_temp+'Aircraft__r.'
                           + Namespaceprefix_temp+'Lease__r.'+Namespaceprefix_temp + 'Operator__r.'+Namespaceprefix_temp
                           +'Location__Latitude__s,'+Namespaceprefix_temp+'Aircraft__r.'+Namespaceprefix_temp
                           + 'Lease__r.'+Namespaceprefix_temp+'Operator__r.'+Namespaceprefix_temp+'Location__Longitude__s,'+Namespaceprefix_temp
                           + 'Operator_F__c,'+Namespaceprefix_temp+'Operator_Id_F__c,'+Namespaceprefix_temp+'Lease_F__c,' 
                           + Namespaceprefix_temp+'Aircraft__r.'+Namespaceprefix_temp+'Lease__r.Name,'+Namespaceprefix_temp+'Lease_End_Date__c,'
                           + Namespaceprefix_temp+'Aircraft__r.Name,'+Namespaceprefix_temp+'Aircraft__r.'+Namespaceprefix_temp+'Lease__r.'+
                           + Namespaceprefix_temp+'Operator__r.Name'
                           + ' FROM '+Namespaceprefix_temp+'Lease_Expirations__r )'
                           + ' FROM '+Namespaceprefix_temp+'Aircraft_Type__c';
       return soqlQuery;
   }
   
   private string createIlogSoql(  )
   {
       string soqlQuery = 'SELECT id, '+Namespaceprefix_temp+'Aircraft_Type__c,'+Namespaceprefix_temp+'Related_To_Operator__r.Name,'
                           +Namespaceprefix_temp+'Related_To_Operator__r.'+Namespaceprefix_temp+'Location__Latitude__s,'
                           +Namespaceprefix_temp+'Variant__c,'+Namespaceprefix_temp+'Related_To_Operator__r.'+Namespaceprefix_temp+'Location__Longitude__s,'
                           +Namespaceprefix_temp+'Vintage_From__c,'+Namespaceprefix_temp+'Rent_000__c,'+Namespaceprefix_temp+'Term_Mos__c,'
                           +Namespaceprefix_temp+'Comment__c,'+Namespaceprefix_temp+'Related_To_Operator__c'
                           +' FROM '+Namespaceprefix_temp+'Customer_Interaction_Log__c'
                           +' WHERE '+Namespaceprefix_temp+'Type_Of_Request__c=\''+IL_REQUESTTYPE+'\''
                           +' AND '+Namespaceprefix_temp+'Related_To_Operator__r.'+Namespaceprefix_temp+'Location__Latitude__s!=null'
                           +' AND '+Namespaceprefix_temp+'Related_To_Operator__r.'+Namespaceprefix_temp+'Location__Longitude__s!=null';                    
       return soqlQuery;
   }
   
   public void showOperators() 
   {     
        aitSupply = new list<Current_Acquisition_Pipeline__c>();
        addresslist = new List<Address>();
        aircrafttypes = new list<Aircraft_Type__c>();
        List<Counterparty__c> lessors = new list<Counterparty__c>();
        list<Customer_Interaction_Log__c> iLogs = new list<Customer_Interaction_Log__c>();      
        Map<Id,list<Customer_Interaction_Log__c>> iLogsGrouped = new Map<Id,list<Customer_Interaction_Log__c>>();  
        List<List<sObject>> querylst = new  List<List <sObject>>();
        leaseExpirations = new list<Lease_Expiration__c>();
        Map<Id,list<Lease_Expiration__c>> leaseExpirationDtlGRP = new Map<Id,list<Lease_Expiration__c>>();
        number_of_airlines = 0;
        
        system.debug('options=='+aircraft.Aircraft_Type__c+'   '+aircraft.Engine_Type__c+'   '+operator.Country__c);
        
        string soqlQuery = createAircraftTypeSoql();
        
        if( aircraftTypeId !=null && !searchclicked )
           soqlQuery = soqlQuery + ' WHERE Id=\''+aircraftTypeId+'\'';
       
        if( aircraft.Aircraft_Type__c!=null && aircraft.Aircraft_Type__c !='' && searchclicked )
            soqlQuery = soqlQuery +' WHERE '+Namespaceprefix_temp+'Aircraft_Type__c=\''+aircraft.Aircraft_Type__c+'\'';
        
        /*if( aircraft.Engine_Type__c!=null && aircraft.Engine_Type__c !='' && searchclicked )
            soqlQuery = soqlQuery +' AND '+Namespaceprefix_temp+'Engine_Type__c=\''+aircraft.Engine_Type__c+'\'';
        
        if( aircraft.Aircraft_Variant__c!=null && aircraft.Aircraft_Variant__c !='' && searchclicked )
            soqlQuery = soqlQuery +' AND '+Namespaceprefix_temp+'Aircraft_Variant__c=\''+aircraft.Aircraft_Variant__c+'\''; */
        
        system.debug('soqlQuery=='+soqlQuery);
        
        aircrafttypes = database.query(soqlQuery);
        
        system.debug('aircrafttypes=='+aircrafttypes.size()+'  '+aircrafttypes);
       
        
        if( !aircrafttypes.isEmpty() )
        {
            aircraftTypeId = aircrafttypes[0].Id;
            aircraft.Aircraft_Type__c = aircrafttypes[0].Aircraft_Type__c;
            for( Aircraft_Type__c aircraftType : aircrafttypes)         
            {
                for( Lease_Expiration__c leaseExpiration: aircraftType.Lease_Expirations__r)    
                {
                    leaseExpirations.add(leaseExpiration);
                }
            }
            
            aitSupply = [ SELECT Id,Name,Aircraft_In_Transaction__c,Aircraft_In_Transaction__r.Name,Acquisition_Type_F__c,Aircraft_Type__c,
                           Aircraft_Type__r.Name,Aircraft_In_Transaction__r.Aircraft__c,Aircraft_In_Transaction__r.Aircraft__r.Aircraft_Type__c,
                           Aircraft_In_Transaction__r.Aircraft__r.Aircraft_Variant__c,Aircraft_In_Transaction__r.Aircraft__r.Engine_Type__c,
                           Aircraft_In_Transaction__r.Aircraft__r.Date_of_Manufacture__c,Aircraft_In_Transaction__r.Aircraft__r.Name

                          FROM Current_Acquisition_Pipeline__c 
                          WHERE Aircraft_Type__c=:aircrafttypes[0].Id ];
        //}       
        
        system.debug('aitSupply=='+aitSupply.size());
        
        /*iLogs = [ SELECT Id,Aircraft_Type__c,Related_To_Operator__r.Name,Related_To_Operator__r.Location__Latitude__s,Variant__c,
                    Related_To_Operator__r.Location__Longitude__s,Vintage_From__c,Rent_000__c,Term_Mos__c,Comment__c
                    FROM Customer_Interaction_Log__c 
                    WHERE Aircraft_Type__c=:aircraft.Aircraft_Type__c 
                    AND Type_Of_Request__c=:IL_REQUESTTYPE 
                    AND Related_To_Operator__r.Location__Latitude__s!=null AND Related_To_Operator__r.Location__Longitude__s!=null ];*/
        
        String strQueryforIL = createIlogSoql();
        
        if( aircraft.Aircraft_Type__c!=null && aircraft.Aircraft_Type__c !='' )
            strQueryforIL = strQueryforIL +' AND '+Namespaceprefix_temp+'Aircraft_Type__c=\''+aircraft.Aircraft_Type__c+'\'';
        
        if( aircraft.Engine_Type__c!=null && aircraft.Engine_Type__c !='' )
            strQueryforIL = strQueryforIL +' AND '+Namespaceprefix_temp+'Engine_Type_List__c=\''+aircraft.Engine_Type__c+'\'';
        
        if( aircraft.Aircraft_Variant__c!=null && aircraft.Aircraft_Variant__c !='' )
            strQueryforIL = strQueryforIL +' AND '+Namespaceprefix_temp+'Variant__c=\''+aircraft.Aircraft_Variant__c+'\'';
        
        if( Vintage!=null && Vintage !='' && searchclicked )
            strQueryforIL = strQueryforIL +' AND '+Namespaceprefix_temp+'Vintage_From__c=\''+Vintage+'\'';
        
        
        iLogs = database.Query( strQueryforIL );
        
        iLogsGrouped = LeaseWareutils.Ids( 'Related_To_Operator__c',iLogs );
        
        //adding Market Intelligence demand data
        for( Id operator : iLogsGrouped.keySet() )
        {
            String popup = '<br/>';
            
            popup = popup + '<br/><h3 style=font-style:italic class=popupHd2>'+DEMANDPOPUPHEADER+'</h3>';
            
            popup = popup + '<table style=width:360px><tr><th class=popupTh>Type</th><th class=popupTh>Vintage</th>'+
                    '<th class=popupTh>Rent</th><th class=popupTh>Term</th>'+
                    '<th class=popupTh>Comments</th></tr>';
                    
            for( Customer_Interaction_Log__c il : iLogsGrouped.get( operator ) )
            {
                string type,comments,vintage,term,rent;
                type = comments  = term = vintage = rent = '';

                if( il.Aircraft_Type__c!=null )
                    type      = il.Aircraft_Type__c;
                
                if( il.Variant__c!=null )
                    type      = type +' - '+ il.Variant__c;
                                    
                if( il.Term_Mos__c!=null )
                    term   = string.valueof(il.Term_Mos__c);   
                
                if( il.Rent_000__c!=null )
                    rent   = '$'+currency(string.valueof(il.Rent_000__c));   

                if( il.Comment__c!=null )
                    comments = il.Comment__c;  
                 
                if( il.Vintage_From__c!=null )
                    vintage = il.Vintage_From__c; 
                  
                 popup = popup + '<tr><td class=popupTd>'+'<a href=/'+ il.Id +' target=_blank>' 
                              + type +'</a>'+'</td><td class=popupTd>'+vintage+'</td>'+
                              '<td class=popupTd>'+rent+'</td><td class=popupTd>'+term
                              + '</td><td class=popupTd>'+comments+'</td></tr>';                                       

                system.debug(' il == '+ il.Related_To_Operator__c );  
                
            }
            popup = popup + '</table>';  
            addresslist.add( new Address( String.valueof(iLogsGrouped.get(operator)[0].Related_To_Operator__r.Location__Latitude__s) ,
                                            String.valueof(iLogsGrouped.get(operator)[0].Related_To_Operator__r.Location__Longitude__s) ,
                                            '<h3 style=font-style:italic class=popupHd1><a href=/' + iLogsGrouped.get(operator)[0].Related_To_Operator__c + ' target=_blank>' + 
                                            iLogsGrouped.get(operator)[0].Related_To_Operator__r.Name  +' - '+aircraft.Aircraft_Type__c+ '</a></h3>'+popup, 'Demand', 
                                            100 ,iLogsGrouped.get(operator)[0].Related_To_Operator__c ) );              
        }

        system.debug('iLogsGrouped=='+iLogsGrouped.size());
            
       system.debug('leaseExpirations=='+leaseExpirations.size());
       
       if( !leaseExpirations.isEmpty() )
           leaseExpirationDtlGRP = LeaseWareutils.ids( 'Operator_Id_F__c', leaseExpirations );
        
        system.debug('leaseExpirationDtlGRP=='+leaseExpirationDtlGRP.keyset());
        
        //Adding Aircraft Supply data on lease
        for( Id operatorId : leaseExpirationDtlGRP.KeySet() )
        {    
            if( operatorId!=null )  {
                
            String popup = '<br/>';
            
            popup = popup + '<br/><h3 style=font-style:italic class=popupHd2>'+SUPPLYPOPUPHEADER+'</h3>';
            
            popup = popup+'<table style=width:360px><tr><th class=popupTh>Aircraft</th><th class=popupTh>Lease</th><th class=popupTh>Lease End Date</th></tr>';
                        
            if( leaseExpirationDtlGRP.get( operatorId )!=null )
            {
                system.debug('operatorId=='+operatorId+'   '+leaseExpirationDtlGRP.get( operatorId ));
                for( Lease_Expiration__c leaseExpiration : leaseExpirationDtlGRP.get( operatorId ) )       
                {
                    string leaseEndDate = System.today().format();
                    if( leaseExpiration.Lease_End_Date__c!=null )
                        leaseEndDate = leaseExpiration.Lease_End_Date__c.format();
                    
                    popup = popup+'<tr><td class=popupTd>'+'<a href=/'+ leaseExpiration.Aircraft__c +' target=_blank>'   
                              + leaseExpiration.Aircraft__r.Name + '</a>'+'</td><td class=popupTd>'+'<a href=/' + leaseExpiration.Aircraft__r.Lease__c 
                              + ' target=_blank>' + leaseExpiration.Aircraft__r.Lease__r.Name + '</a>'+'</td><td class=popupTd>'
                              +leaseEndDate+'</td></tr>';
                }
                
                popup = popup + '</table>';
                
                addresslist.add( new Address( String.valueof(leaseExpirationDtlGRP.get( operatorId )[0].Aircraft__r.Lease__r.Operator__r.Location__Latitude__s) ,
                            String.valueof(leaseExpirationDtlGRP.get( operatorId )[0].Aircraft__r.Lease__r.Operator__r.Location__Longitude__s) ,
                            '<h3 style=font-style:italic class=popupHd1><a href=/' + leaseExpirationDtlGRP.get( operatorId )[0].Aircraft__r.Lease__r.Operator__c + ' target=_blank>' + 
                            leaseExpirationDtlGRP.get( operatorId )[0].Aircraft__r.Lease__r.Operator__r.Name  +' - '+aircraft.Aircraft_Type__c+' Lease Expirations'+ '</a></h3>'+popup, 'Supply', 
                            100 ,leaseExpirationDtlGRP.get( operatorId )[0].Aircraft__r.Lease__r.Operator__c) );
            }
          }     
        }
        
        Lessor__c lessor = [ SELECT Id,Name,Y_Hidden_Location__Latitude__s,Y_Hidden_Location__Longitude__s 
                              FROM Lessor__c LIMIT 1 ];
        
        String popup = '<br/>';
        
        //Adding Aircraft Supply data available for leasing
        if( leaseExpirationDtlGRP.get( null )!=null )
        {    
            popup = popup + '<br/><h3 style=font-style:italic class=popupHd2>Unassigned Aircraft</h3>';
                
            popup = popup+'<table style=width:633px;><tr><th class=popupTh>Aircraft</th><th class=popupTh>Aircraft Type</th>'
                    +'<th class=popupTh>Variant</th><th class=popupTh>Engine Type</th>'
                    +'<th class=popupTh>Vintage</th></tr>';
            
            for( Lease_Expiration__c leaseExpiration : leaseExpirationDtlGRP.get( null ) )
            {      
                string vintageYear,aircraftType,variant,engineType;
				vintageYear = aircraftType = variant = engineType = '';
				 
                if( leaseExpiration.Aircraft__r.Date_of_Manufacture__c!=null )
                    vintageYear = string.valueof( leaseExpiration.Aircraft__r.Date_of_Manufacture__c.year() );
                
				if( leaseExpiration.Aircraft__r.Aircraft_Type__c!=null )
					aircraftType = leaseExpiration.Aircraft__r.Aircraft_Type__c;
				
				if( leaseExpiration.Aircraft__r.Aircraft_Variant__c!=null )
					variant = leaseExpiration.Aircraft__r.Aircraft_Variant__c;
				
				if( leaseExpiration.Aircraft__r.Engine_Type__c!=null )
					engineType = leaseExpiration.Aircraft__r.Engine_Type__c;
				
                popup = popup+'<tr><td class=popupTd>'+'<a href=/' + leaseExpiration.Aircraft__c +' target=_blank>'   
                          + leaseExpiration.Aircraft__r.Name + '</a></td>'



                          + '<td class=popupTd>'+ aircraftType +'</td>'
                          + '<td class=popupTd>'+ variant +'</td>'
                          + '<td class=popupTd>'+ engineType +'</td>'
                          + '<td class=popupTd>'+ vintageYear +'</td>'+'</tr>';
            }                               
        }
        
        popup = popup + '</table>';

        if( !aitSupply.IsEmpty() )  
        {
            popup = popup + '<br/>';
        
            popup = popup + '<br/><h3 style=font-style:italic>New Orders</h3>';
                       
            popup = popup+'<table style=width:633px;><tr><th class=popupTh>Order</th>'
                    + '<th class=popupTh>Aircraft In Transaction</th>'
                    + '<th class=popupTh>Acquisition Type</th>'
                    + '<th class=popupTh>Aircraft Type</th>'
                    + '<th class=popupTh>Variant</th>'
                    + '<th class=popupTh>Engine Type</th>'
                    + '<th class=popupTh>Vintage</th></tr>';
        }
            
         //Adding AIT Supply
         for( Current_Acquisition_Pipeline__c ait : aitSupply )
         {
            string aircraftType,vintageYear,variant,engineType;
            aircraftType = vintageYear = variant = engineType= '';
            
            if( ait.Aircraft_In_Transaction__r.Aircraft__c!=null )
                aircraftType = '<a href=/' +ait.Aircraft_In_Transaction__r.Aircraft__c
                                + ' target=_blank>' + ait.Aircraft_In_Transaction__r.Aircraft__r.Aircraft_Type__c + '</a>';
            
            if( ait.Aircraft_In_Transaction__r.Aircraft__r.Date_of_Manufacture__c!=null )
                vintageYear = string.valueof( ait.Aircraft_In_Transaction__r.Aircraft__r.Date_of_Manufacture__c.year() );   
            
            if( ait.Aircraft_In_Transaction__r.Aircraft__r.Aircraft_Variant__c!=null )
                variant = ait.Aircraft_In_Transaction__r.Aircraft__r.Aircraft_Variant__c;
            
            if( ait.Aircraft_In_Transaction__r.Aircraft__r.Engine_Type__c!=null )
                engineType = ait.Aircraft_In_Transaction__r.Aircraft__r.Engine_Type__c;
            
             popup = popup +'<tr><td class=popupTd>'+'<a href=/'+ ait.Id +' target=_blank>'   
                      + ait.Name + '</a>'+'</td><td class=popupTd>'+'<a href=/' +ait.Aircraft_In_Transaction__c 
                      + ' target=_blank>' + ait.Aircraft_In_Transaction__r.Name + '</a>'+'</td><td class=popupTd>'
                      + ait.Acquisition_Type_F__c+'</td>'
                      + '<td class=popupTd>'+aircraftType+'</td>'
                      + '<td class=popupTd>'+ variant +'</td>'
                      + '<td class=popupTd>'+ engineType +'</td>'
                      + '<td class=popupTd>'+ vintageYear +'</td>'
                      + '</tr>';
            
         }
            
         if( !aitSupply.IsEmpty() )
             popup = popup + '</table>';
         
        addresslist.add( new Address( String.valueof( lessor.Y_Hidden_Location__Latitude__s ) ,
                                     String.valueof( lessor.Y_Hidden_Location__Longitude__s ) ,
                                     '<h3 style=font-style:italic class=popupHd1><a href=/' + lessor.Id + ' target=_blank>' + 
                                     lessor.Name  +' - '+aircraft.Aircraft_Type__c+ '</a></h3>'+popup, 'lessor', 
                                     100 , lessor.Id) );
        
        //Adding counterParty lessor data 
        /*if( showLessors && !aircrafttypes.isEmpty() )
        {        
           lessors = [ SELECT Id,name,Location__Latitude__s,Location__Longitude__s 
                        FROM Counterparty__c 
                        WHERE Location__Latitude__s != null  
                        AND Fleet_Aircraft_Of_Interest__c includes (:aircraft.Aircraft_Type__c)
                        AND Location__Longitude__s!=null ORDER by lastmodifieddate ];
            
            //Adding lessors to Map
            for( Counterparty__c lessorTmp : lessors )
            {            
                addresslist.add( new Address( String.valueof(lessorTmp.Location__Latitude__s), String.valueof(lessorTmp.Location__Longitude__s),
                                            '<div style=width:360px><h3 style=font-style:italic class=popupHd1><a href=/' + lessorTmp.id + ' target=_blank>' + lessorTmp.name +
                                            '</a></h3></div>', 'Otherlessor', 100 ,lessorTmp.id) );               
            }
        }

        system.debug('lessors===='+'   '+lessors.size());       */ 

        number_of_airlines = addresslist.size();
        system.debug('addresslist=='+addresslist.size()+'  '+addresslist);
        
        //if( !aircrafttypes.IsEmpty() )
            boolRefreshSupplyBtn = true;
        }
        else    {
            boolRefreshSupplyBtn = false;
            aircraftTypeId = null;
        }
    }  
    
     public static String currency(String i) {
        String s = ( Decimal.valueOf(i==null||i.trim()==''?'0':i).setScale(2) + 0.001 ).format();
        return s.substring(0,s.length()-1);
    }
    
    @RemoteAction
    public static string populateNewsArticles( Id operatorId, String aircraftTypeTmp, String token  )
    {       
        String lessorPrefix  = Lessor__c.sobjecttype.getDescribe().getKeyPrefix();

        string newsContent = '';
        if( String.valueof(operatorId).substring(0,3) == lessorPrefix )   
            newsContent = '<br/><h3 style=font-style:italic class=popupHd2>News</h3><table style=margin-right:4px;><tbody>';
        else
            newsContent = '<br/><h3 style=font-style:italic class=popupHd2>News</h3><table style=margin-right:4px;max-width:380px><tbody>';
        
        list<NewsArticle__c> artilces = new list<NewsArticle__c>();

        NewsFeedServices newsService  = new NewsFeedServices();
        artilces = newsService.getNewsArticles( operatorId , aircraftTypeTmp , token ,false);
        
        if( artilces.isEmpty() )
        {
            newsContent = newsContent+'<tr><td style=text-align:center;min-width:620px;>'+NONEWSFOUNDMSG+'</td></tr>';
        }
        else
        {       
            for( NewsArticle__c article : artilces )
            {
                //string tDate = datetime.newInstance(article.Date__c.year(), article.Date__c.month(),article.Date__c.day()).format('dd/MM/yyyy');
                if( article.link__c == null || article.link__c == '')
                    newsContent = newsContent+'<tr><td class=tdToolTip>'+'<text title="'+article.tip__c+'">' + article.article__c+ '</text>'+
                                  '</td></tr>';
                else
                    newsContent = newsContent+'<tr><td class=tdToolTip>'+'<a style=text-decoration:none; href='+ article.link__c + ' target=_blank title="'+article.tip__c+'">' + 
                                  article.article__c+ '</a>'+'</td></tr>';
            }
        }        
        return newsContent + '</tbody></table>';    
    }
    
    //Address model which contains the addresses info like info, lat, long and type
    public class Address
    {
        public string  lat       {get;set;}
        public string  lng       {get;set;}
        public string  info      {get;set;}
        public Integer inService {get;set;}
        public string  type      {get;set;}
        public string  opid      {get;set;}
        public Address( string lat, string lng, string info,string type , Integer inService ,string opid )
        {
            this.lat  = lat;
            this.lng  = lng;
            this.info = info;
            this.type = type;
            this.inService = inService;
            this.opid = opid;
            system.debug('info=='+info);
        }
    }
}