public class EquipmentDepreciationScheduleHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string  triggerBefore = 'EquipmentDepreciationScheduleHandlerBefore';
    private final string  triggerAfter = 'EquipmentDepreciationScheduleHandlerAfter';
    public EquipmentDepreciationScheduleHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('EquipmentDepreciationScheduleHandler.beforeInsert(+)');
    	Equipment_Depreciation_Schedule__c[] listNew = (list<Equipment_Depreciation_Schedule__c>)trigger.new ;
    	for(Equipment_Depreciation_Schedule__c curRec:listNew){
    		if(!LeaseWareUtils.isFromTrigger('InsertedFromAircraftTriggerHandler')) {
    			curRec.addError('You are not allowed to create Equipment Depreciation Schedule records.');
    		}
			
    	}    	
    	
		
	    system.debug('EquipmentDepreciationScheduleHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('EquipmentDepreciationScheduleHandler.beforeUpdate(+)');
    	Equipment_Depreciation_Schedule__c[] listNew = (list<Equipment_Depreciation_Schedule__c>)trigger.new ;
    	for(Equipment_Depreciation_Schedule__c curRec:listNew){
    			curRec.addError('You are not allowed to modify Equipment Depreciation Schedule records.');
    			
    	}     	
    	
    	system.debug('EquipmentDepreciationScheduleHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('EquipmentDepreciationScheduleHandler.beforeDelete(+)');
    	Equipment_Depreciation_Schedule__c[] listNew = (list<Equipment_Depreciation_Schedule__c>)trigger.old ;
    	for(Equipment_Depreciation_Schedule__c curRec:listNew){
    		if(!LeaseWareUtils.isFromTrigger('DeletedFromAircraftTriggerHandler')) {
    			curRec.addError('You are not allowed to delete Equipment Depreciation Schedule records.');
    		}
			
    	}      	
    	
    	system.debug('EquipmentDepreciationScheduleHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('EquipmentDepreciationScheduleHandler.afterInsert(+)');
    	
    	system.debug('EquipmentDepreciationScheduleHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('EquipmentDepreciationScheduleHandler.afterUpdate(+)');
    	
    	
    	system.debug('EquipmentDepreciationScheduleHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('EquipmentDepreciationScheduleHandler.afterDelete(+)');
    	
    	
    	system.debug('EquipmentDepreciationScheduleHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('EquipmentDepreciationScheduleHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('EquipmentDepreciationScheduleHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


}