@isTest
public with sharing class TestCreditMemoTriggerHandler {
   
        @testSetup
        static void setup(){
            
            LeaseWareUtils.clearFromTrigger(); 
            Parent_Accounting_Setup__c accountingSetup = new Parent_Accounting_Setup__c(Name='TestAS');
            insert accountingSetup;
            
            TestLeaseworkUtil.setCoaCM();
            
            LeaseWareUtils.clearFromTrigger(); 
            Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
            insert aircraft;

            Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
            insert legalEntity;

            Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
            insert operator;

            LeaseWareUtils.clearFromTrigger(); 
            Lease__c lease = new Lease__c(Name='TestLease',aircraft__c =aircraft.id, Lessee__c=operator.Id,Legal_Entity__c=legalEntity.Id,Lease_End_Date_New__c=Date.today().addYears(10),Lease_Start_Date_New__c=Date.today(),
                                         Accounting_Setup_P__c =accountingSetup.id);
            insert lease;

            LeaseWareUtils.clearFromTrigger();
            Chart_of_accounts__c coa1 = new Chart_of_Accounts__c(name='Lessor COA',GL_Code__c='0001',GL_Account_Description__c='Credit Memo RR');
            Chart_of_accounts__c coa2 = new Chart_of_Accounts__c(name='Lessor COA',GL_Code__c='0002',GL_Account_Description__c='Credit Memo Unapplied');
            list<Chart_of_accounts__c> coalist = new list<Chart_of_accounts__c> {coa1,coa2};
            insert coalist;
            
            Lessor__c LWSetUp = new Lessor__c(Name='TestSetup', Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                                              Phone_Number__c='2342', Email_Address__c='a@lease.com',Accounting_enabled__c = true,CM_Rev_Reduction__c = coa1.id,Unapplied_cm__c=coa2.id); 
            LeaseWareUtils.clearFromTrigger();
            insert  LWSetUp;

            
            
            
        }
        @isTest
        static void testLesseeCreditMemo(){
            Test.startTest();
            Boolean expectedExceptionThrown =  false;
            Lease__c lease = [select id from lease__c limit 1];
            operator__c lessee = [select id from operator__c limit 1];
            legal_entity__c le = [select id,name from legal_entity__C limit 1];
            Calendar_Period__c cp = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1),closed__c=true);
            credit_memo__c creditMemo = new credit_memo__c(amount__c = 200000.00,
                           Credit_Memo_Date__c=system.today(),company__c =le.id,
                           lessee__c = lessee.id);
            try{
                insert creditMemo;

            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Accounting Period is not found for') ? true : false ;
            }
			System.AssertEquals(expectedExceptionThrown, true,'Incorrect exception'); 
            expectedExceptionThrown = false;
            LeaseWareUtils.clearFromTrigger();
            insert cp;
            insert creditMemo;
            creditMemo = [select status__c,Y_Hidden_IsStandalone__c,Available_Credit_Memo_Amount_F__c,
                          (select id,name,credit__c,debit__c,journal_status__c from
                            journal_entries__r ) from credit_memo__c limit 1];
            System.assertEquals(200000, creditMemo.Available_Credit_Memo_Amount_F__c,'Incorrect cm available amt');
            System.assertEquals(true,creditMemo.Y_Hidden_IsStandalone__c,'Incorrect standalone value');
            System.assertEquals('Pending',creditMemo.status__c,'Incorrect creditMemo status');
            System.assertEquals(2,creditMemo.journal_entries__r.size(),'Incorrect credit memo journal size');

            for(journal_entry__c je :creditMemo.journal_entries__r){
                system.debug('JE NAME:::'+je.name+' '+' '+je.credit__c+' '+je.debit__c);
                if(je.name=='Credit Memo RR')system.assertEquals(200000, je.debit__c,'Incorrect amt');
                if(je.name=='Credit Memo Unapplied')system.assertEquals(200000, je.credit__c,'Incorrect amt');
                system.assertEquals('Draft - Pending Approval', je.journal_status__c,'Incorrect amt');
               
            }
            creditMemo.amount__c  = 100000;
            LeaseWareUtils.clearFromTrigger();
            update creditMemo;
            creditMemo = [select status__c,Y_Hidden_IsStandalone__c,Available_Credit_Memo_Amount_F__c,(select id,name,credit__c,debit__c from
                            journal_entries__r ) from credit_memo__c limit 1];
            System.assertEquals(100000, creditMemo.Available_Credit_Memo_Amount_F__c,'Incorrect Avaialble Balance');
            for(journal_entry__c je :creditMemo.journal_entries__r){
                system.debug('JE NAME:::'+je.name+' '+' '+je.credit__c+' '+je.debit__c);
                if(je.name=='Credit Memo RR')system.assertEquals(100000, je.debit__c,'Incorrect amt');
                if(je.name=='Credit Memo Unapplied')system.assertEquals(100000, je.credit__c,'Incorrect amt');
               
            }
            
            creditMemo.status__c ='Approved';
            LeaseWareUtils.clearFromTrigger();
            try{
                update creditMemo;

            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Accounting Period for Dummy1 is closed. Please reopen the period or choose another date') ? true : false ;
               
            }
            System.AssertEquals(expectedExceptionThrown, true,'Incorrect exception'); 
            expectedExceptionThrown = false;
            LeaseWareUtils.clearFromTrigger();
            cp.closed__c =  false;
            update cp;

            LeaseWareUtils.clearFromTrigger();
            update creditMemo;
            creditMemo = [select id,status__c,Y_Hidden_IsStandalone__c,Available_Credit_Memo_Amount_F__c,
                          (select id,name,credit__c,debit__c,journal_status__c from
                            journal_entries__r ) from credit_memo__c limit 1];
            System.assertEquals('Approved',creditMemo.status__c,'Incorrect cm status');
            System.assertEquals('Posted',creditMemo.journal_entries__r[0].journal_status__c,'Incorrect cm journals');

            creditMemo.amount__c  = 500000;
            LeaseWareUtils.clearFromTrigger();
            try{
                update creditMemo;

            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Update not allowed on Approved Credit Memos') ? true : false ;
                //System.AssertEquals(expectedExceptionThrown, true); 
            }
            System.AssertEquals(expectedExceptionThrown, true,'Incorrect Exception'); 
            expectedExceptionThrown = false;
            LeaseWareUtils.clearFromTrigger();
            try{
                 delete creditMemo;

            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Only pending credit memos can be deleted.') ? true : false ;
                //System.AssertEquals(expectedExceptionThrown, true); 
            }
			System.AssertEquals(expectedExceptionThrown, true,'Incorrect exception'); 
            expectedExceptionThrown = false;
            Test.stopTest();

        }
        @isTest
        static void testLeaseCreditMemo(){
            Test.startTest();
            Lease__c lease = [select id from lease__c limit 1];
            legal_entity__c le = [select id,name from legal_entity__C limit 1];
            operator__c lessee = [select id from operator__c limit 1];
            Calendar_Period__c cp = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1));
            insert cp;
            credit_memo__c creditMemo = new credit_memo__c(amount__c = 200000.00,Lease__c = lease.id,
                           Credit_Memo_Date__c=system.today(),company__c =le.id,
                           lessee__c = lessee.id);
            
            insert creditMemo;
            creditMemo = [select id,status__c,
                            (select id,name,credit__c,debit__c,journal_status__c from
                            journal_entries__r ) from credit_memo__c limit 1];
            for(journal_entry__c je :creditMemo.journal_entries__r){
                system.debug('JE NAME:::'+je.name+' '+' '+je.credit__c+' '+je.debit__c);
                if(je.name=='Credit Memo Revenue Reduction')system.assertEquals(200000, je.debit__c,'Incorrect Amount');
                if(je.name=='Unapplied Credit Memo')system.assertEquals(200000, je.credit__c,'Incorrect Amount');
                system.assertEquals('Draft - Pending Approval', je.journal_status__c,'Incorrect status');
               
            }
            LeaseWareUtils.clearFromTrigger();
            delete creditmemo;
            test.stopTest();

        }
        @isTest
        static void testCreditMemoNonStandalone(){
            Test.startTest();
            Boolean expectedExceptionThrown = false;
            Lease__c lease = [select id from lease__c limit 1];
            legal_entity__c le = [select id,name from legal_entity__C limit 1];
            operator__c lessee = [select id from operator__c limit 1];
            Calendar_Period__c cp = new Calendar_Period__c(Name = 'Dummy1',Start_Date__c =system.today().toStartOfMonth(),
                                                        End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1));
            insert cp;
            credit_memo__c creditMemo = new credit_memo__c(amount__c = 100000.00,Lease__c = lease.id,
                           Credit_Memo_Date__c=system.today(),company__c =le.id,
                           lessee__c = lessee.id,Y_Hidden_IsStandalone__c = false);
            
            insert creditMemo;
            Invoice__c invoice =new Invoice__c(Lease__c=lease.id, Name='Test Inv', RecordTypeId=Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Other').getRecordTypeId(),Invoice_date__c=Date.today(), 
                                             Invoice_Type__c='Other',amount__c =200000,status__c='Approved');
            
            insert invoice;

            Payment__c newPayment = new Payment__c(Name='Test', Invoice__c=invoice.id, payment_date__c=date.today(), accounting_period__c =cp.id,
                                        Amount__c=100000,payment_status__c='Approved',credit_memo__c =creditMemo.id);
            insert newPayment;

            newPayment = [select id,payment_status__c,credit_memo__c,amount__c from payment__c where id =:newPayment.id];
            LeaseWareUtils.clearFromTrigger();
            try{
                newpayment.amount__c = 200000;
                update newPayment;
            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Approved Payments/Credits cannot be edited') ? true : false ;
                //System.AssertEquals(expectedExceptionThrown, true); 
            }
            System.AssertEquals(expectedExceptionThrown, true,'Incorrect error'); 
            expectedExceptionThrown = false;
            LeaseWareUtils.clearFromTrigger();
            creditmemo.amount__c = 50000;
            try{
                update creditmemo;
            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('This user action is not permitted. This is a system generated credit memo; only the status can be changed. To change the information on this credit memo, please delete and generate a new one.') ? true : false ;
                //System.AssertEquals(expectedExceptionThrown, true); 
            }
            System.AssertEquals(expectedExceptionThrown, true,'Incorrect Exception'); 
            expectedExceptionThrown = false;
            LeaseWareUtils.clearFromTrigger();
            creditmemo.amount__c = 100000;
            creditmemo.status__c = 'Approved';
            update creditMemo;
            newPayment = [select id,payment_status__c from payment__c where id =:newPayment.id];
            system.assertEquals('Approved',newPayment.payment_status__c,'Incorrect payment status');

            Test.stopTest();
        }

        @isTest
        public static void testUpdateValidations(){
            Test.startTest();
            Boolean expectedExceptionThrown =  false;
            Lease__c lease = [select id from lease__c limit 1];
            lease.Accounting_Setup_P__c = null;
            update lease;
            operator__c lessee = [select id from operator__c limit 1];
            legal_entity__c le = [select id,name from legal_entity__C limit 1];
           
            credit_memo__c creditMemo = new credit_memo__c(amount__c = 200000.00,
                           Credit_Memo_Date__c=system.today(),company__c =le.id,
                           lessee__c = lessee.id,lease__c = lease.id);
            insert creditMemo;

            creditmemo.status__c = 'Cancelled-Pending';
            LeaseWareUtils.clearFromTrigger();
            try{
                update creditmemo;
            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Manual update to Cancelled/Cancelled-Pending is not allowed') ? true : false ;
            }
            System.AssertEquals(expectedExceptionThrown, true,'Incorrect Exception'); 
            expectedExceptionThrown = false;
            creditmemo.status__c = 'Approved';
            LeaseWareUtils.clearFromTrigger();
            update creditmemo;
            CreditMemoTriggerHandler.bIsUndoCancellation = true;        
            creditmemo.status__c = 'Cancelled-Pending';
            LeaseWareUtils.clearFromTrigger();
           
            try{
                update creditmemo;
            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('This action can only be performed on credit memos in Cancelled-Pending status') ? true : false ;
            }
            System.AssertEquals(expectedExceptionThrown, true,'Incorrect Exception'); 
            expectedExceptionThrown = false;
            CreditMemoTriggerHandler.bIsUndoCancellation = false; 
            CreditMemoTriggerHandler.bIsCancelCm = true;        
            creditmemo.status__c = 'Approved';
            LeaseWareUtils.clearFromTrigger();
            update creditmemo;

            creditmemo = [select id,status__c from  credit_memo__c limit 1];
            system.assertEquals('Cancelled-Pending',creditMemo.status__c);
            CreditMemoTriggerHandler.bIsUndoCancellation = false; 
            CreditMemoTriggerHandler.bIsCancelCm = false;  
            creditmemo.amount__c = 250000;
            LeaseWareUtils.clearFromTrigger();
            try{
                update creditmemo;
            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Update not allowed on Cancelled-Pending Credit Memos') ? true : false ;
            }
            system.AssertEquals(expectedExceptionThrown, true,'Incorrect Exception'); 
            expectedExceptionThrown = false;
            creditmemo.amount__c = 200000.0;
            creditmemo.status__c ='Cancelled';
            LeaseWareUtils.clearFromTrigger();
            update creditmemo;
            creditmemo.amount__c = 250000;
            try{
                update creditmemo;
            }
            catch(Exception e){
                system.debug('e.getMessage()::::'+e.getMessage());
                expectedExceptionThrown =  e.getMessage().contains('Update not allowed on Cancelled Credit Memos') ? true : false ;
            }

        }
    
}