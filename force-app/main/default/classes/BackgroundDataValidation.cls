/*

BackgroundDataValidation b= new BackgroundDataValidation();
b.execute(null);
*/

global without Sharing class BackgroundDataValidation implements Database.Batchable<sObject> , Database.AllowsCallouts, 
                        Schedulable , Database.Stateful{
    private BackgroundDataValidationHelper helper = new  BackgroundDataValidationHelper();                      
    private string processRequestId;
    private boolean standalone=false; // whether we want to call other batch jobs on finish. False -> it is not standalone, so call others.
    public void setStandalone(){
        standalone =true;
    }
    public void initBackgroundDataValidation(){
        DateTime dt = system.now();
        processRequestId =  dt.format('yyyyMMddhhmm');
    }   
    public void setBackgroundDataValidation(Admin_Data_Validation__c dataProcess,string processRequestId){// default contructor
        this.dataProcess = dataProcess;
        this.processRequestId = processRequestId;
    }      
    //-------Constructor-------------------------------


    //-----Schedule Methods-------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext sc){
        updateBlockedRecords();
        helper.createAdminDataValidation();
        initBackgroundDataValidation();
        createBatch(null);
    }

    private void updateBlockedRecords(){
        String prefix = LeaseWareUtils.getNamespacePrefix();
        List<ApexClass> classList = [SELECT Id, name 
                                    FROM ApexClass 
                                    WHERE Name = 'BackgroundDataValidation' AND NamespacePrefix =:prefix];
        if(classList.isEmpty())  return;

        list<AsyncApexJob> listOfJobs = [SELECT id,CreatedDate FROM AsyncApexJob
                where status not in ('Completed','Failed','Aborted') and JobType = 'BatchApex'
                and ApexClassId =:classList[0].id order by CreatedDate asc];
        

        list<Admin_Data_Validation__c> blockedRecordList;
        if(listOfJobs.isEmpty()){
            blockedRecordList = [select Id, Name, Script_Method_Name__c
                                    from Admin_Data_Validation__c 
                                    where  Status__c ='Processing'];
        }else{
            blockedRecordList = [select Id, Name, Script_Method_Name__c
                                    from Admin_Data_Validation__c 
                                    where  Status__c ='Processing' and lastModifiedDate < :listOfJobs[0].CreatedDate];
        }
        for(Admin_Data_Validation__c rec:blockedRecordList){
            rec.Status__c = 'Open';
        }       
        if(!blockedRecordList.isEmpty()) {update blockedRecordList; }    
    }
    private void createBatch(string recId){
        system.debug( 'callBatchProcessForDataValidation - Generated Id='   + processRequestId);
        
        helper.callBatchProcessForDataValidation(processRequestId,recId);
    }
    public static void executeQuickAction(){
        // 1. Execute Quick Action
        if(trigger.new[0].getQuickActionName() == Schema.Admin_Data_Validation__c.QuickAction.Execute){
            try{     
                BackgroundDataValidation bgv = new BackgroundDataValidation();
                bgv.createBatch(trigger.new[0].id);       
                //BackgroundDataValidationHelper.createAdminDataValidationBatchJob((Admin_Data_Validation__c)trigger.new[0],null);
            }catch(Exception ex){
                trigger.new[0].addError(ex);
            }
        }        
    }
    //---------------Batch Methods--------------------------------------------------------------------------------------
    private Admin_Data_Validation__c dataProcess;
    string exceptionMessage='';
    string logMessageString='';
    map<string,string> mapParam = new map<string,string>();

    public Database.QueryLocator start(Database.BatchableContext bc){
        string scriptMethodName = dataProcess.Script_Method_Name__c;
        system.debug('ScriptName =' + scriptMethodName);
        if(mapParam==null) {mapParam = new map<string,string>();}
        mapParam.put('PARENT_ID',dataProcess.id);   
        mapParam.put('PROCESS_REQ_ID',dataProcess.Request_Id__c);   
        helper.deleteLogFile(dataProcess.id);    
        helper.deleteOldRecords(dataProcess.id);
        string queryString;
        list<object> retSObject = helper.getQueryLocator(scriptMethodName,mapParam);
        if(retSObject!=null && retSObject.size()>0){
            queryString = (String)retSObject[0];// first element
        }

        if(queryString==null){
            exceptionMessage = 'Could not find the method for this script. Script name:' + scriptMethodName;
            queryString='select id,name from ApexClass where name= \'DoNotMatchAnyClassDummy\'  ';
        }else if(queryString.startsWith('ERROR')){
            exceptionMessage = queryString;
            queryString='select id,name from ApexClass where name= \'DoNotMatchAnyClassDummy\'  ';            
        }else if(Test.isRunningTest()){
            queryString += ' Limit 5 ';
        }

        return Database.getQueryLocator(queryString);
    }

    public void execute(Database.BatchableContext bc, List<sObject> scopeList){
        
        string scriptMethodName = dataProcess.Script_Method_Name__c;
        logMessageString = logMessageString +  '\n================================================================================================';
        logMessageString = logMessageString +  '\n - Number of records=' + scopeList.size();
        system.debug('BackgroundDataValidation ' + scriptMethodName + '(+) - Number of records=' + scopeList.size());
        Savepoint spStart = Database.setSavepoint();
        try{
            helper.deleteOldRecordsInBatch(dataProcess.id,dataProcess.Request_Id__c);

            if(String.IsBlank(exceptionMessage)){
                logMessageString = logMessageString +  '\n' + helper.execute(scopeList,scriptMethodName,mapParam);
            }else{
                logMessageString = logMessageString +  '\n - Not Executed this batch. Reason is ' + exceptionMessage;
            }
        }catch(Exception ex){
            Database.rollback(spStart);
            exceptionMessage += '\n'  +'Unexpected error :' + ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage();
            system.debug('Exception : ' + ex +  + '===' +exceptionMessage);
        }
        system.debug('BackgroundDataValidation ' + scriptMethodName + '(-)' );
    }

    public void finish(Database.BatchableContext bc){
        string scriptMethodName = dataProcess.Script_Method_Name__c;

        
        try{
            helper.finishMethodOnScriptRun(dataProcess,logMessageString,exceptionMessage,mapParam);
        }catch(Exception ex){
            LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),'Unexpected Error' + ' : Script Executed :'+scriptMethodName ,ex.getStackTraceString() +'=='+ ex.getLineNumber() +'=='+ ex.getMessage(),null);
        }        
        
        // Spawn new batch if any
        System.debug ('IN FINISH OF -' + scriptMethodName);
        if(!standalone) createBatch(null);
        System.debug('END OF FINISH - AFTER SPAWNING NEW BATCHES - ' + scriptMethodName);
    }
}