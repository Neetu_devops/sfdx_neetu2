@isTest
private class TestDimensionValue {
    
    @testSetup
    static Void testSetup(){
      	map<String,Object> mapInOut = new map<String,Object>();
        
        mapInOut.put('Aircraft__c.msn','MSN1');
        mapInOut.put('Aircraft__c.New_Used__c','New');
        TestLeaseworkUtil.createAircraft(mapInOut);
        Id Aircraft1 = (Id) mapInOut.get('Aircraft__c.Id'); 
       
        // Create Operator
        mapInOut.put('Operator__c.Name','Air India');
        TestLeaseworkUtil.createOperator(mapInOut);
       
  	}
    
   
    
    @isTest
    static void testDimensionValueCreationForMSNESN() {
      Dimension__c dimension = new Dimension__c(name='MSN/ESN');
      insert dimension;
      List<Dimension_Value__c> dimensionValue = [select Id, Name from Dimension_Value__c ];
      system.assertEquals(1, dimensionValue.size());
  	}
    
    
    
    @isTest
    static void testDimensionValueCreationForLessee() {
      Dimension__c dimension = new Dimension__c(name='Lessee');
      insert dimension;
      List<Dimension_Value__c> dimensionValue = [select Id, Name from Dimension_Value__c ];
      system.assertEquals(1, dimensionValue.size());
  	}
    
    
   
}