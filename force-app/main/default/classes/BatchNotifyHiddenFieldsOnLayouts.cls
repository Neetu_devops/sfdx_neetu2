public class BatchNotifyHiddenFieldsOnLayouts implements Database.Batchable<String>, Database.Stateful, Database.AllowsCallouts {
    public List<String> customObjList = new List<String>();
    public Map<String,String> fieldApiLableMap = new Map<String,String>();
    public List<String> csvRowValuesOfYFields = new List<String>();
    public List<String> csvRowValuesOfZFields = new List<String>();
    
    
    public BatchNotifyHiddenFieldsOnLayouts(List<String> objList, List<String> rowValuesYFields, List<String> rowValuesZFields ) {
        
        customObjList.addAll(objList);
        if(rowValuesYFields.size() > 0) {
            csvRowValuesOfYFields.addAll(rowValuesYFields);
        }
        if(rowValuesZFields.size() > 0) {
            csvRowValuesOfZFields.addAll(rowValuesZFields);
        }
        
    }
    
    public Iterable<String> start(Database.BatchableContext BC) {
        
        String objId='';
        String query='SELECT ID FROM CustomObject WHERE DeveloperName=';
        List<String> pageLayouts = new List<String>();
        if(!String.isBlank(LeaseWareUtils.getNamespacePrefix())) {
            query += '\''+customObjList[0].removeStartIgnoreCase(LeaseWareUtils.getNamespacePrefix()).removeEndIgnoreCase('__c') +'\'' +' AND NamespacePrefix=' +'\''+LeaseWareUtils.getNamespacePrefix().removeEnd('__')+'\'';
        } else {
            query +='\''+customObjList[0].removeEnd('__c') +'\'';
        }
        JSONParser parser = JSON.createParser(LeaseWareStreamUtils.toolingAPISOQL(query));
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == 'Id')) {
                    // Get the value.
                    parser.nextToken();
                    objId = parser.getText();
                    break;
                }
        }
        
        if(objId != null) {
            
            JSONParser parserLayout = JSON.createParser(LeaseWareStreamUtils.toolingAPISOQL('SELECT NAME, ManageableState FROM Layout WHERE TableEnumOrId='+'\''+objId+'\''));
            String pageLayout='';
            while (parserLayout.nextToken() != null) {
                
                if ((parserLayout.getCurrentToken() == JSONToken.FIELD_NAME)) {
                    if (parserLayout.getText() == 'Name') {
                        // Get the value.
                        parserLayout.nextToken();
                        //pageLayouts.add(parserLayout.getText()) ;
                        pageLayout=parserLayout.getText();
                    } 
                    if(parserLayout.getText() == 'ManageableState') {
                        parserLayout.nextToken();
                        if(parserLayout.getText() == 'installed'){
                            pageLayout = LeaseWareUtils.getNamespacePrefix()+pageLayout;
                        }
                        pageLayouts.add(pageLayout);
                    }
                }}
        }
        
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(customObjList[0]).getDescribe().fields.getMap();
        
        for (String fieldName: fieldMap.keySet()) {
            
            fieldApiLableMap.put(fieldName.ToLowerCase(),fieldMap.get(fieldName).getDescribe().getLabel());
        }
        
        return pageLayouts;
    }
    
    public void execute(Database.BatchableContext BC, List<String> scope){
        
        for(String layoutName : scope ) {
            
            //layoutName = String.isBlank(LeaseWareUtils.getNamespacePrefix()) ? layoutName : LeaseWareUtils.getNamespacePrefix()+layoutName;
            List<String> componentNameList = new List<String>{customObjList[0]+'-'+layoutName};
                
                List<Metadata.Metadata> components = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, componentNameList);
            
            if(components.size()>0) {
                
                Metadata.Layout layout = (Metadata.Layout) components.get(0);
                
                List<Metadata.Metadata> layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout,  new List<String> {customObjList[0]+'-'+layoutName});
                Metadata.Layout layoutMd = (Metadata.Layout) layouts.get(0);
                
                for (Metadata.LayoutSection section : layoutMd.layoutSections) {
                    for (Metadata.LayoutColumn column : section.layoutColumns) {
                        if (column.layoutItems != null) {
                            for (Metadata.LayoutItem item : column.layoutItems) {
                                if(item.field != null){
                                    if(fieldApiLableMap.containsKey(item.field.toLowerCase())) {
                                        if(fieldApiLableMap.get(item.field.toLowerCase()).startsWithIgnoreCase('Y_') || fieldApiLableMap.get(item.field.toLowerCase()).startsWith('{'))  {
                                            
                                            String rowStr = Schema.getGlobalDescribe().get(customObjList[0]).getDescribe().getLabel() +',' + customObjList[0] + ',' + layoutName +','+ fieldApiLableMap.get(item.field.toLowerCase()).escapeCsv() +','+ item.field + ',' + 'Detail';
                                            csvRowValuesOfYFields.add(rowStr);
                                        }
                                        else if(fieldApiLableMap.get(item.field.toLowerCase()).startsWithIgnoreCase('Z_')) {
                                            String rowStr = Schema.getGlobalDescribe().get(customObjList[0]).getDescribe().getLabel() +',' +customObjList[0] + ',' + layoutName +','+ fieldApiLableMap.get(item.field.toLowerCase()).escapeCsv() +','+ item.field + ',' + 'Detail';
                                            csvRowValuesOfZFields.add(rowStr);
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
                if(layoutMd.relatedLists != null) {
                    for(Metadata.RelatedListItem relatedItem: layoutMd.relatedLists) {
                        if(relatedItem.fields != null) {
                            for(String field : relatedItem.fields ) {
                                if(Schema.getGlobalDescribe().get(relatedItem.relatedList.substringBefore('.')) != null  && field.endsWithIgnoreCase('__c')) {
                                    String objApiName = relatedItem.relatedList.substringBefore('.');
                                    if(field.contains('.')) {
                                       objApiName = field.substringBefore('.');
                                       field = field.substringAfter('.');
                                    }
                                    
                                    //fields can belong to diffrent different objects that's why we need to get fieldMap every time.
                                    Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objApiName).getDescribe().fields.getMap();
                                    
                                    if(fieldMap.containsKey(field)) {
                                        if(fieldMap.get(field).getDescribe().getLabel().startsWithIgnoreCase('Y_') || fieldMap.get(field).getDescribe().getLabel().startsWith('{')) {
                                            String rowStr = Schema.getGlobalDescribe().get(customObjList[0]).getDescribe().getLabel() +',' + customObjList[0]  + ',' + layoutName + ',' + fieldMap.get(field).getDescribe().getLabel().escapeCsv() + ',' + field + ',' + 'Related List';
                                            csvRowValuesOfYFields.add(rowStr);
                                        } else if(fieldMap.get(field).getDescribe().getLabel().startsWithIgnoreCase('Z_')) {
                                            String rowStr = Schema.getGlobalDescribe().get(customObjList[0]).getDescribe().getLabel() +',' + customObjList[0]  + ',' + layoutName + ',' + fieldMap.get(field).getDescribe().getLabel().escapeCsv() + ',' + field + ',' + 'Related List' ;
                                            csvRowValuesOfZFields.add(rowStr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
    }
    public void finish(Database.BatchableContext BC){
        
        customObjList.remove(0);
        
        if(customObjList.size() > 0) {
            BatchNotifyHiddenFieldsOnLayouts batch = new BatchNotifyHiddenFieldsOnLayouts(customObjList, csvRowValuesOfYFields, csvRowValuesOfZFields);
            Database.executeBatch(batch , 1);
        } else {
            String csvColumnHeader = 'Object Label, Object API Name, PageLayout Name, Field Label, Field API Name, Location \n';
            String csvFileForYFields = csvColumnHeader + String.join(csvRowValuesOfYFields, '\n');
            String csvFileForZFields =  csvColumnHeader + String.join(csvRowValuesOfZFields, '\n');
            sendEmailWithCSVAttachment(csvFileForYFields,csvFileForZFields); 
        }
    }
    
    public void sendEmailWithCSVAttachment(String csvFileForYFields, String csvFileForZFields) {
        
        list<Messaging.SingleEmailMessage> emailsToSent = new list<Messaging.SingleEmailMessage>();
        String textEmailBody = '\n';
        textEmailBody +='Organization Name : '+UserInfo.getOrganizationName() + '\n';
        textEmailBody +='Organization Id : '+UserInfo.getOrganizationId() + '\n';
        textEmailBody += '\n';
        textEmailBody +='Please find the attachments.';
       
        Messaging.SingleEmailMessage emailTobeSent = new Messaging.SingleEmailMessage();
        
        Messaging.EmailFileAttachment csvForYFields = new Messaging.EmailFileAttachment();
        csvForYFields.setFileName('Y_HiddenFieldsOnPageLayouts.csv');
        csvForYFields.setBody(Blob.valueOf(csvFileForYFields));
        csvForYFields.setContentType('text/csv');
        
        Messaging.EmailFileAttachment csvForZFields = new Messaging.EmailFileAttachment();
        csvForZFields.setFileName('Z_UnusedFieldsOnPageLayouts.csv');
        csvForZFields.setBody(Blob.valueOf(csvFileForZFields));
        csvForZFields.setContentType('text/csv');
        
        emailTobeSent.targetobjectid=UserInfo.getUserId();
        emailTobeSent.setSaveAsActivity(FALSE);
        emailTobeSent.setPlainTextBody(textEmailBody);    
        emailTobeSent.setSubject('Y_Hidden And Z_Unused Fields On Layouts');
        emailTobeSent.setFileAttachments(new Messaging.EmailFileAttachment[] {csvForYFields,csvForZFields});
        emailsToSent.add(emailTobeSent);

        LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
        Messaging.sendEmail(emailsToSent);
        LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
        
    }
    
    
    public static String findHiddenAndUnusedFields() {
        

    Set<String>  objectSet = new Set<string>();
    list<String> objectList = new List<String>();
        
        for(Schema.SObjectType objTyp : Schema.getGlobalDescribe().Values()){
            
            //if(!objectSet.contains(objTyp.getDescribe().getName())){
                
                if(objTyp.getDescribe().isCustom() && !objTyp.getDescribe().getLabel().startsWithIgnoreCase('Z_')){
                    
                    if(String.isBlank(LeaseWareUtils.getNamespacePrefix()) || 
                       objTyp.getDescribe().getName().startsWith(LeaseWareUtils.getNamespacePrefix()) ) {
                           objectSet.add(objTyp.getDescribe().getName());
                    }
                }
            //}
        }
      
        objectList.addAll(objectSet);
        
        if(Test.isRunningTest()) {
            BatchNotifyHiddenFieldsOnLayouts batch = new BatchNotifyHiddenFieldsOnLayouts(new List<String>{LeaseWareUtils.getNamespacePrefix()+'Operator__c'},new list<String>(),new list<String>());
            Database.executeBatch(batch , 1);
        }else {
            BatchNotifyHiddenFieldsOnLayouts batch = new BatchNotifyHiddenFieldsOnLayouts(objectList,new list<String>(),new list<String>());
            Database.executeBatch(batch , 1);
        }
        return null;
    }

}