public class DimensionHandler implements ITrigger {
    
    private final string triggerBefore = 'DimensionHandlerBefore';
    private final string triggerAfter = 'DimensionHandlerAfter';
    public DimensionHandler(){
        system.debug('INSIDE DIMENSION HANDLER');
    }
    
     /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void  bulkBefore(){
    }
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void  bulkAfter(){
    }
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void  beforeInsert(){}
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void  beforeUpdate(){}
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void  beforeDelete(){
        
    }
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void  afterInsert(){
        system.debug('DimensionHandler.afterInsert(++++)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        list<Dimension__c> newList =  (list<Dimension__c>)trigger.New ;
        
        insertDimensionValues(newList);
        
        system.debug('DimensionHandler.afterInsert(------)');
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        
    }
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void  afterDelete(){
         
    }
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void  afterUnDelete(){}   
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
   public void  andFinally(){}
    
    private void insertDimensionValues(List<Dimension__c> dimensionList){
        List<Dimension_Value__c> dimensionValueList = new List<Dimension_Value__c>();
        for(Dimension__c dimension: dimensionList){
        system.debug('getDimensionValues---' + dimension.Name);
      	if(dimension.Name  == 'MSN/ESN'){
           // Get the serial Numbers of active assets
            system.debug('MSN');
            List<String> excludeAssets = new List<String>{'Sold','Cancelled','Terminated'};
           List<Aircraft__c> aircraftList = [select Id,MSN_Number__c from Aircraft__c where Contractual_Status__c NOT IN :excludeAssets];
           system.debug('aircraftList Size---'+ aircraftList.size());
            for(Aircraft__c asset : aircraftList){
                Dimension_Value__c dimensionValue = new Dimension_Value__c();
                dimensionValue.Name = asset.MSN_Number__c;
                dimensionValue.Dimension__c= dimension.id;
              	dimensionValueList.add(dimensionValue);
          	}
        }else if(dimension.Name  == 'Lessee'){
            List<Operator__c> operatorList = [select Id,Name from Operator__c where Inactive__c=false];
             system.debug('operatorList Size---'+ operatorList.size());
            for(Operator__c operator : operatorList){
                Dimension_Value__c dimensionValue = new Dimension_Value__c();
                dimensionValue.Name = operator.Name;
                dimensionValue.Dimension__c= dimension.id;
              	dimensionValueList.add(dimensionValue);
          	}
        }else if(dimension.Name  == 'Lease Name'){
            List<Lease__c> leaseList = [select Id,Name from Lease__c];
             system.debug('leaseList Size---'+ leaseList.size());
            for(Lease__c lease : leaseList){
                Dimension_Value__c dimensionValue = new Dimension_Value__c();
                dimensionValue.Name = lease.Name;
                dimensionValue.Dimension__c= dimension.id;
              	dimensionValueList.add(dimensionValue);
          	}
        }
        }
        insert dimensionValueList;
    }

}