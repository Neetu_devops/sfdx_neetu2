@isTest
private class TestBulkInvoicingController {
    static Lessor__c lessor                             {get;set;}
    static Rent__c incSchedule1,incSchedule2;
    static Utilization_Report__c ur1,ur2,ur3,ur4;
    static Aircraft__c aircraft1;//,aircraft2;
    static Operator__c operator1                       {get;set;}
    //static Operator__c operator2                       {get;set;}
    static Counterparty__c lessor1                      {get;set;}
    static Counterparty__c lessor2                      {get;set;}    
    static lease__c lease1;//,lease2;
    static Invoice__c newInvoice1,newInvoice2;
    
    static void dataSetup() { 
        
        lessor = [select id,name,Phone_Number__c,Email_Address__c,Lease_Logo__c
                  ,AviatorApiKey__c,Address_Street__c,City__c,Zip_Code__c
                  ,Country__c
                  from Lessor__c];
        map<String,Object> mapInOut = new map<String,Object>();
        
         Country__c country;
        try{
        	country = [select Id,Name from Country__c limit 1];
        }catch(QueryException e){
             country =new Country__c(Name='Test Country');
        	insert country;
        }

        // Create Operator1
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());      
        Operator__c[] opList = [select id,Name,City__c,Zip_Code__c,Country_Lookup__c from Operator__c];
        operator1 = opList[0];
        //operator2 = opList[1];
        
        lessor1 = new Counterparty__c(Address_Street__c='1240/29',City__c='Faridabad',Zip_Code__c='121008',Country_Lookup__c=country.id,
                                      Fleet_Aircraft_Of_Interest__c='A320',Status__c='Approved',Location__Latitude__s = 23.45555555,
                                      Location__Longitude__s = 73.23233232,
                                      Main_Email_Address__c='test1@lws.com',
                                      Subsidiary_Entity__c = true,
                                      Billing_Email_Address__c='test2@lws.com');
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());  
        insert lessor1;
        
        LeaseWareUtils.unsetTriggers('CounterpartyTrg');
        lessor2 = new Counterparty__c(Address_Street__c='1240/29',City__c='Faridabad',Zip_Code__c='121008',Country_Lookup__c=country.id,
                                      Fleet_Aircraft_Of_Interest__c='A320',Status__c='Approved',Location__Latitude__s = 23.45555555,
                                      Location__Longitude__s = 73.23233232,
                                      Main_Email_Address__c='test1@lws.com',
                                      Subsidiary_Entity__c = true,
                                      Billing_Email_Address__c='test1@lws.com');
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMAP());  
        insert lessor2;
        
        mapInOut.put('Aircraft__c',null);
        mapInOut.put('Aircraft__c.msn','123');
        createAircraft(mapInOut);
        aircraft1 = (Aircraft__c)mapInOut.get('Aircraft__c');
        
        //lease with operator populated
        mapInOut.put('Lease__c.Lessor__c',lessor1.Id); 
        mapInOut.put('Lease__c.Aircraft__c',aircraft1.Id); 
        mapInOut.put('Lease__c.Operator__c',operator1.Id);
        mapInOut.put('Lease__c.Name','Lease2'); 
        mapInOut.put('Lease__c',null);          
        createLease(mapInOut);
        lease1 = (Lease__c)mapInOut.get('Lease__c');
    }
    
    static void dataSetup_SendInv(){
        map<String,Object> mapInOut = new map<String,Object>();
        Integer numberOfDays = Date.daysInMonth( system.today().year(), system.today().month() );
        Date monthEnding  = Date.newInstance( system.today().year(), system.today().month(), numberOfDays );
        
        mapInOut.put('Rent__c',null) ;
        mapInOut.put('Rent__c.For_Month_Ending__c',monthEnding);
        mapInOut.put('Aircraft__c.lease__c',lease1.Id);
        createRent(mapInOut);
        
        mapInOut.put('Invoice__c.Aircraft MR',null) ;
        mapInOut.put('Invoice__c.Assembly MR',null) ;
        mapInOut.put('Invoice__c.Rent',null);
        setRecordTypeOfInvoice(mapInOut);
        
        incSchedule1 = (Rent__c)mapInOut.get('Rent__c') ;   //Income Schedule with operator on lease
        
        ur1 = new Utilization_Report__c();
        //
        LeaseWareUtils.clearFromTrigger();
        mapInOut.put('Utilization_Report__c',null) ;
        mapInOut.put('Aircraft__c.Id',Aircraft1.Id);
        mapInOut.put('Utilization_Report__c.Month_Ending__c',system.today().addMonths(-2)); //lease start date
        mapInOut.put('Utilization_Report__c.Status__c','Approved By Lessor');
        createUR(mapInOut);      
        ur1 =(Utilization_Report__c) mapInOut.get('Utilization_Report__c');  // Monthly Utilization
        
        //list<Invoice__c> inv = new list<Invoice__c>();      
        
        newInvoice1 = new Invoice__c();
        newInvoice1.Lease__c        = lease1.Id;
        newInvoice1.Invoice_Date__c = System.Today();
        newInvoice1.Invoice_Type__c = 'Aircraft MR'; 
        newInvoice1.Status__c       = 'Pending';             
        newInvoice1.RecordTypeId    = (Id)mapInOut.get('Invoice__c.Aircraft MR');
        newInvoice1.Month_Ending__c = monthEnding;
        newInvoice1.Utilization_Report__c = ur1.Id;
        //inv.add( newInvoice1 ); 
        
        insert newInvoice1;
        
        mapInOut.put('Rent__c',null) ;
        mapInOut.put('Rent__c.For_Month_Ending__c',monthEnding);
        mapInOut.put('Aircraft__c.lease__c',lease1.Id);
        createRent(mapInOut);
        incSchedule2 = (Rent__c)mapInOut.get('Rent__c') ;   //Income Schedule with lessee on lease
    }
    
    @isTest(seeAllData=true)
    static void  SendMultipleInvoiceCntrl_Test() {
        dataSetup();
        User user = [ select id from user where profile.name='System Administrator' 
                     AND isactive = true limit 1 ];
        
        system.runAs( user ){
            dataSetup_SendInv();
        }
        Test.startTest();
        BulkInvoicingController cont = new BulkInvoicingController();
        BulkInvoicingController.fetchInvoice('2019-01-01', '2019-31-12', 'Rent', 'No Invoice', aircraft1.Id, false);
        BulkInvoicingController.fetchInvoice('2019-01-01', '2019-31-12', 'Aircraft MR', 'Pending', aircraft1.Id, false);
        BulkInvoicingController.fetchInvoice('2019-01-01', '2019-31-12', 'Rent', 'Approved', aircraft1.Id, false);
        BulkInvoicingController.fetchInvoice('2019-01-01', '2019-31-12', 'All', 'All', aircraft1.Id, true);
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    static void  SendMultipleInvoiceCntrl_Test2() {
        dataSetup();
        User user = [ select id from user where profile.name='System Administrator' 
                     AND isactive = true limit 1 ];
        system.runAs( user ){
            dataSetup_SendInv();
        }
        Test.startTest();
        BulkInvoicingController cont = new BulkInvoicingController();
        List<Id> ids= new List<Id>();
        ids.add(newInvoice1.Id);
        
        Stepped_Rent__c SteppedRentRec1 = new Stepped_Rent__c (
            Name='Dummy for SteppedRentRec1',
            Rent_Type__c='Fixed Rent',
            Rent_Period__c='Monthly',
            Base_Rent__c=7000.00,
            Rent_Amount__c=6000.00,
            Start_Date__c=system.today(), // YYYY-MM-DD
            //Start_Date__c=listleaserec[0].Lease_Start_Date_New__c,
            Rent_End_Date__c=system.today().addMonths(2) ,  // YYYY-MM-DD
            //Rent_End_Date__c=listleaserec[0].Lease_End_Date_New__c,
            Invoice_Generation_Day__c='31',
            Rent_Period_End_Day__c='6',
            Rent_Due_Type__c='Relative Based On Invoice Date',
            Rent_Due_Day__c=20,
            Lease__c=lease1.id,
            status__c=true
        );
        insert SteppedRentRec1 ;          
        
        
        List<String> records = new List<String>();
        Rent__c newRent = new Rent__c();
        newRent.RentPayments__c = lease1.Id;
        newRent.Stepped_Rent__c =SteppedRentRec1.Id;
        newRent.For_Month_Ending__c = system.today();
        LWGlobalUtils.setTriggersflagOn(); 
        insert newRent;
        LWGlobalUtils.setTriggersflagOff();
        records.add(newRent.Id);
       
        records.add(newInvoice1.Id);
        //Test.startTest();
        BulkInvoicingController.processInvoices(records, '2019-01-01', '2019-05-01', 'All', 'No Invoice', aircraft1.Id,true );
        BulkInvoicingController.processInvoices(records, '2019-01-01', '2019-05-01', 'All', 'Pending', aircraft1.Id,true );
        BulkInvoicingController.processInvoices(records, '2019-01-01', '2019-05-01', 'All', 'Approved', aircraft1.Id,true );
        BulkInvoicingController.defaultInvoice(date.today(), 'Generate Utilization', 'Aircraft MR', newInvoice1.Id);
        BulkInvoicingController.defaultInvoice(date.today(), 'Generate Invoice', 'Aircraft MR', newRent.Id);
        BulkInvoicingController.defaultInvoice(date.today(), 'Generate Income Schedule', 'Rent', lease1.Id);
        BulkInvoicingController.defaultInvoice(date.today(), 'Generate Invoice', 'Rent', newRent.Id);
        
        Test.stopTest();
    }
    
    public static void createRent(map<String,Object> mapInOut){ 
        //
        LeaseWareUtils.clearFromTrigger();      
        Date endDate;
        if(mapInOut.containsKey('Rent__c.For_Month_Ending__c')) endDate = (Date)mapInOut.get('Rent__c.For_Month_Ending__c'); 
        else endDate = system.today();
        System.debug('endDate'+ endDate);  
        Stepped_Rent__c SteppedRentRec1 = new Stepped_Rent__c (
            Name='Dummy for SteppedRentRec1',
            Rent_Type__c='Fixed Rent',
            Rent_Period__c='Monthly',
            Base_Rent__c=7000.00,
            Rent_Amount__c=6000.00,
            Start_Date__c=system.today(), // YYYY-MM-DD
            //Start_Date__c=listleaserec[0].Lease_Start_Date_New__c,
            Rent_End_Date__c=system.today().addMonths(2) ,  // YYYY-MM-DD
            //Rent_End_Date__c=listleaserec[0].Lease_End_Date_New__c,
            Invoice_Generation_Day__c='31',
            Rent_Period_End_Day__c='6',
            Rent_Due_Type__c='Relative Based On Invoice Date',
            Rent_Due_Day__c=20,
            Lease__c=(String)mapInOut.get('Aircraft__c.lease__c'),
            status__c=true
        );
        insert SteppedRentRec1 ;          
        
        Rent__c insRent = new Rent__c(
            RentPayments__c=(String)mapInOut.get('Aircraft__c.lease__c')
            ,name = 'Rent1'
            ,For_Month_Ending__c = endDate,Stepped_Rent__c =SteppedRentRec1.Id);
        insert insRent;
        mapInOut.put('Rent__c.Id',insRent.Id);
        if(mapInOut.containsKey('Rent__c')) {
            mapInOut.put('Rent__c',insRent) ;
        }       
    }  
    
    public static void setRecordTypeOfInvoice(map<String,Object> mapInOut){
        //
        LeaseWareUtils.clearFromTrigger();      
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName();
        mapInOut.put('Invoice__c.Aircraft MR',rtMapByName.get('MR').getRecordTypeId()) ;
        mapInOut.put('Invoice__c.Assembly MR',rtMapByName.get('Assembly MR').getRecordTypeId()) ;
        mapInOut.put('Invoice__c.Rent',rtMapByName.get('Rent').getRecordTypeId())   ;
    }
    
    public static void createUR(map<String,Object> mapInOut){
        //
        LeaseWareUtils.clearFromTrigger();      
        // Set up the Constt Assmbly record.
        string UrType ='Actual' ;
        Date endDate;
        if(mapInOut.containsKey('Utilization_Report__c.True_Up__c') && mapInOut.get('Utilization_Report__c.True_Up__c')!=null ) UrType ='Manual Credits/Adjustments'; 
        if(mapInOut.containsKey('Utilization_Report__c.Month_Ending__c')) endDate =(Date)mapInOut.get('Utilization_Report__c.Month_Ending__c'); 
        else endDate = system.today();
        System.debug('endDate'+ endDate);
        endDate = endDate.addMonths(1).toStartOfMonth().addDays(-1);
        System.debug('endDate'+ endDate);
        
        Utilization_Report__c testURForCMR2 = new Utilization_Report__c(
            Name= 'thisURForCMR', Aircraft__c=(String)mapInOut.get('Aircraft__c.Id'), FH_String__c='100:35',Type__c = UrType, 
            Airframe_Cycles_Landing_During_Month__c=120, Month_Ending__c= endDate,  
            Status__c='Open'
            ,Maint_Reserve_Heavy_Maint_1_Airframe__c=100000
            ,Maintenance_Reserve_Engine_1__c = 20000
            ,Airframe_Flight_Hours_Month__c = 150  
        );
        insert testURForCMR2;
        
        //
        LeaseWareUtils.clearFromTrigger();
        
        mapInOut.put('Utilization_Report__c.Id',testURForCMR2.Id)   ;
        if(mapInOut.containsKey('Utilization_Report__c.Status__c')){
            Utilization_Report__c UR1 = [select id from Utilization_Report__c where id=:testURForCMR2.Id];
            UR1.status__c = 'Approved By Lessor';
            UR1.Maint_Reserve_Heavy_Maint_1_Airframe__c = 100000;
            UR1.Maintenance_Reserve_Engine_1__c = 20000;
            LeaseWareUtils.clearFromTrigger();update UR1;
        }
        System.debug('testcreateUR : insert done testURForCMR2 ! ');        
        if(mapInOut.containsKey('Utilization_Report__c'))   {
            mapInOut.put('Utilization_Report__c',testURForCMR2) ;
        }       
        
    }    
    // Create Aircraft using Aircraft Object
    public static  void createAircraft(map<String,Object> mapInOut) {
        
        //
        LeaseWareUtils.clearFromTrigger();
        String msn = '54321'; 
        String NewUsed ;  
        if(mapInOut.containsKey('Aircraft__c.msn')) msn = (string)mapInOut.get('Aircraft__c.msn');  
        if(mapInOut.containsKey('Aircraft__c.New_Used__c')) NewUsed = (string)mapInOut.get('Aircraft__c.New_Used__c');  
        
        Date tempDate = system.today();            
        Aircraft__c a = new Aircraft__c(MSN_Number__c=msn, TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000
                                        ,New_Used__c = NewUsed
                                        , Status__c='Available',Date_of_Manufacture__c=tempDate.toStartOfMonth());
        if( !mapInOut.containsKey('Aircraft__c.Insert') ){
            insert a;
            mapInOut.put('Aircraft__c.Id',a.Id);
            system.debug('!Aircraft__c.Insert');
        }
        
        if(mapInOut.containsKey('Aircraft__c')) {
            mapInOut.put('Aircraft__c',a);
            system.debug('Aircraft__c');
        }   
    } 
    
    public static  void createLease(map<String,Object> mapInOut) {
        LeaseWareUtils.clearFromTrigger();
        String Aircraft1 ,LeaseName='Dummy',OperatorId,LessorId,lesseeId; 
        
        Aircraft1 = (string)mapInOut.get('Lease__c.Aircraft__c');
        OperatorId = (string)mapInOut.get('Lease__c.Operator__c');  
        LessorId = (string)mapInOut.get('Lease__c.Lessor__c');  
        lesseeId = (string)mapInOut.get('Lease__c.Lessee__c');                   
        if(mapInOut.containsKey('Lease__c.Name')) LeaseName = (string)mapInOut.get('Lease__c.Name');    
        
        Lease__c l = new Lease__c(Aircraft__c=Aircraft1, Name=LeaseName, Rent_Type__c='Power By The Hour',
                                  Operator__c=OperatorId, UR_Due_Day__c='10',Lease_Start_Date_New__c=system.today().addMonths(-2),Lessor__c = LessorId,Lessee__c = lesseeId,
                                  Lease_End_Date_New__c=system.today().addYears(2), Rent_Date__c=system.today(), Rent_End_Date__c=system.today().addYears(2));
        if( !mapInOut.containsKey('Lease__c.Insert') ){
            insert l;
            mapInOut.put('Lease__c.Id',l.Id);
        }       
        if(mapInOut.containsKey('Lease__c'))    {
            mapInOut.put('Lease__c',l);
        }    
        lease__c le = [select id,Accounting_Setup_P__c from lease__c where id = :l.id];
        if(le.Accounting_Setup_P__c!=null)
        le.Accounting_Setup_P__c = null;
        LWGlobalUtils.setTriggersAndWFsOff();
        update le;
        LWGlobalUtils.setTriggersAndWFsOn();    
    }
}