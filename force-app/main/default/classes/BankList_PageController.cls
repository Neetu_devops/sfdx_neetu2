public class BankList_PageController {
   
    
    public List<wrapperBank> LstBank1 {   get;  set;  }

    
    public BankList_PageController(){
            system.debug('start=');  
        
        String Type = 'Bank';
        List<Bank__c> BankList= [SELECT Name,Company_Logo_URL__c, id 
                                 ,(select id,Issuance__r.Name, Relationship_Type__c from Banks__r)
                                 FROM Bank__c WHERE Type_F__c= 'Bank'] ;
        //List<List_Of_Banks__c> IssuancesList = [SELECT Id,Issuance__c, Relationship_Type__c from List_Of_Banks__c];
        LstBank1 = new List<wrapperBank>();
        
        wrapperBank wpnew;
        for(integer i=0;;){
            // For first column
            if(i>= bankList.size()) break;
            else{
                wpnew = new wrapperBank();
                wpnew.BankName1 = bankList[i].name;
                wpnew.IMG1 = bankList[i].Company_Logo_URL__c ;
                wpnew.issuances1 = new List<wrapperIssuance>();
                for(List_Of_Banks__c  CurIssuance  : bankList[i].Banks__r){
                    wpnew.issuances1.add(new wrapperIssuance(CurIssuance.Issuance__r.Name, CurIssuance.Relationship_Type__c));
                }
                
                LstBank1.add(wpnew);
            }
            i=i+1;
            
            // For 2nd column
            if(i>= bankList.size()) break;
            else{
                wpnew.BankName2 = bankList[i].name;
                wpnew.IMG2 = bankList[i].Company_Logo_URL__c ;
                wpnew.issuances2 = new List<wrapperIssuance>();
                for(List_Of_Banks__c  CurIssuance  : bankList[i].Banks__r){
                    wpnew.issuances2.add(new wrapperIssuance(CurIssuance.Issuance__r.Name, CurIssuance.Relationship_Type__c));
                }                
            }            
            i=i+1;
            
            // For 3rd column
            if(i>= bankList.size()) break;
            else{
                wpnew.BankName3 = bankList[i].name;
                wpnew.IMG3 = bankList[i].Company_Logo_URL__c ;
                wpnew.issuances3 = new List<wrapperIssuance>();
                for(List_Of_Banks__c  CurIssuance  : bankList[i].Banks__r){
                    wpnew.issuances3.add(new wrapperIssuance(CurIssuance.Issuance__r.Name, CurIssuance.Relationship_Type__c));
                }                
            }            
            i=i+1;       
            
            // For 4th column
            if(i>= bankList.size()) break;
            else{
                wpnew.BankName4 = bankList[i].name;
                wpnew.IMG4 = bankList[i].Company_Logo_URL__c ;
                wpnew.issuances4 = new List<wrapperIssuance>();
                for(List_Of_Banks__c  CurIssuance  : bankList[i].Banks__r){
                    wpnew.issuances4.add(new wrapperIssuance(CurIssuance.Issuance__r.Name, CurIssuance.Relationship_Type__c));
                }                   
            }            
            i=i+1;             
        }
          

    }
    
    public class wrapperBank{
        public string BankName1{   get;  set;  }
        public string BankName2{   get;  set;  }
        public string BankName3{   get;  set;  }
        public string BankName4{   get;  set;  }
        public List<wrapperIssuance> issuances1 {get; set;}
        public List<wrapperIssuance> issuances2 {get; set;}
        public List<wrapperIssuance> issuances3 {get; set;}
        public List<wrapperIssuance> issuances4 {get; set;}
        public string IMG1{   get;  set;  }
        public string IMG2{   get;  set;  }
        public string IMG3{   get;  set;  }        
        public string IMG4{   get;  set;  }      
        
        public wrapperBank(){
            
        }
    }
    public class wrapperIssuance{
        public string IssuanceType{   get;  set;  }
        public string RelatioShip{   get;  set;  }
        public wrapperIssuance(string IssuanceType,string RelatioShip){
            this.IssuanceType = IssuanceType;
            this.RelatioShip = RelatioShip;
        }
    }
}