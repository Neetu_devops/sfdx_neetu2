@isTest
private class TestAircraftController2{
	static testMethod void testACController() {
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);           
        
               // Set up the Operator record.
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c
			(Name=operatorName, Status__c='Approved'); 
			
        LeaseWareUtils.clearFromTrigger();insert operator;
        // Set up the Lease record
        //Date dateToday = Date.parse('01/30/2009');
        Lease__c lease = new Lease__c(Name='testLease', 

Operator__c=operator.Id, UR_Due_Day__c='10', 

Lease_Start_Date__c=Date.parse('01/01/2010'), 

Lease_End_Date__c=Date.parse('12/31/2012'), 
Lease_Start_Date_New__c=Date.parse('01/01/2010'), 

Lease_End_Date_New__c=Date.parse('12/31/2012'), 
Rent_Date__c=Date.parse('01/01/2010'), 

Rent_End_Date__c=Date.parse('12/31/2012'));
        LeaseWareUtils.clearFromTrigger();insert lease;
        // Set up the Aircraft record.
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c

(MSN_Number__c=aircraftMSN, Aircraft_Type__c='737', Date_of_Manufacture__c = System.today(),

Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, 

Threshold_for_C_Check__c=1000, Status__c='Available');
        LeaseWareUtils.clearFromTrigger();insert a;
        
        lease.aircraft__c=a.id;
        LeaseWareUtils.clearFromTrigger();update lease;
        
         Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');    
        Aircraft_Eligible_Event__c assEE= new 

Aircraft_Eligible_Event__c(Name= 'ca test', 
        Aircraft__c=a.Id, Start_Date__c=dateStart, 

End_Date__c=dateEnd, InitClaimAmt_MR_Engine_1__c=1000000,TSN_N__c = 100, CSN__c = 100,Event_Type__c='Heavy Maintenance 1');
                LeaseWareUtils.clearFromTrigger();insert assEE;
                Aircraft_Eligible_Event_Invoice__c invoice = 

new Aircraft_Eligible_Event_Invoice__c

(Invoice_Date__c=Date.parse('02/12/2009'), 

Aircraft_Eligible_Event__c = assEE.Id);
                LeaseWareUtils.clearFromTrigger();insert invoice;
        
        
        System.debug

('***************************************8 AC is '+a);
        ApexPages.currentPage().getParameters().put('id', 

a.Id);
        AircraftController2 controller = new AircraftController2();
        controller.getEquipment(a);
        controller.getparentAC();
        controller.getLease();
        controller.getConstitutenAssemblyEngine(a);
        controller.getOperator();
        controller.getEquipment();
        controller.getEquipment(a);
        controller.getACEligible_Event();
        controller.getAircraft_Eligible_Event(a);
        controller.getConstitutenAssemblyApu(a);
        controller.getConstitutenAssemblyGear(a);
        controller.getConstAssembEngine();
        controller.getConstAssembGear();
        controller.getConstAssembApu();
        controller.getConstAssembGear();
        controller.getLogo_URL();
        controller.email='';
        //controller.sendPdf();
        try{
        System.assertEquals(controller.sendPdf(), null);
        }catch(exception e){
            System.debug('Expected.. Ignore');
        }    
          
    }
 }