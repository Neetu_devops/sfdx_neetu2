public class DeskHelpController {

    string pageType;
    public string getPageType() {
        return pagetype;
    }
    public void setPageType(string p) {

        system.debug('-----pagetype' + p);
        pageType = p;

        system.debug('pagetype' + pagetype);
        //param_url = Aircraft_Overview;
        if ('Aircraft'.equals(pagetype)) {
            param_url = Aircraft_Overview;
        } else if ('Lease'.equals(pagetype)) {
            param_url = Lease_overview;
        } else if ('Home'.equals(pagetype)) {
            param_url = Home;
        }
        system.debug('param_url' + param_url);

    }
    public string Aircraft_Vid {
        get;
        set;
    }
    public string Aircraft_Vid_1 {
        get;
        set;
    }
    public string Aircraft_Vid_2 {
        get;
        set;
    }
    public string Aircraft_Vid_3 {
        get;
        set;
    }
    public string Aircraft_Vid_4 {
        get;
        set;
    }

    public string Login {
        get;
        set;
    }
    public string param_url {
        get;
        set;
    }
    public string Home {
        get;
        set;
    }
    public string Aircraft_Overview {
        get;
        set;
    }
    public string Create_new_Aircraft {
        get;
        set;
    }
    public string Add_Edit_Aircraft_details {
        get;
        set;
    }
    public string Generate_spec_sheet {
        get;
        set;
    }
    public string New_Engine_Assembly {
        get;
        set;
    }
    public string Assemblies {
        get;
        set;
    }
    public string Add_LLPs_and_thrust_usage {
        get;
        set;
    }
    public string LLP_bulk_upload {
        get;
        set;
    }
    public string New_Maintenance_Program {
        get;
        set;
    }


    public string Engine_Template {
        get;
        set;
    }
    public string Monthly_Utilization {
        get;
        set;
    }
    public string Monthly_Utilization_Invoice_and_payment {
        get;
        set;
    }
    public string Airframe_maintenance_event {
        get;
        set;
    }
    public string Airframe_maintenance_event_Invoice_and_Payment {
        get;
        set;
    }
    public string Assembly_maintenance_event {
        get;
        set;
    }
    public string Assembly_maintenance_event_Invoice_and_Payment {
        get;
        set;
    }
    public string Forecast_Scenario {
        get;
        set;
    }
    public string Operator_overview {
        get;
        set;
    }
    public string Add_Edit_Operator_details {
        get;
        set;
    }
    public string Address_Book {
        get;
        set;
    }
    public string Interaction_Log {
        get;
        set;
    }
    public string Lease_overview {
        get;
        set;
    }
    public string Add_Edit_Lease_details {
        get;
        set;
    }
    public string Lease_buttons {
        get;
        set;
    }
    public string Create_income_schedule {
        get;
        set;
    }
    public string Rent_invoice_and_payment {
        get;
        set;
    }
    public string Audit {
        get;
        set;
    }
    public string Lease_term_history {
        get;
        set;
    }
    public string Set_up_Assembly_MR_rates {
        get;
        set;
    }
    public string Set_up_LLP_MR_rate_as_per_catalog_price {
        get;
        set;
    }
    public string Marketing_Campaign_overview {
        get;
        set;
    }
    public string Add_new_Marketing_Campaign {
        get;
        set;
    }
    public string Marketing_Campaign_Interaction_Log {
        get;
        set;
    }
    public string New_Aircraft_in_Campaign {
        get;
        set;
    }
    public string Prospect_overview {
        get;
        set;
    }
    public string Add_new_prospect {
        get;
        set;
    }
    public string Auto_create_prospects {
        get;
        set;
    }
    public string Transaction_Overview {
        get;
        set;
    }
    public string Marketing_Activity {
        get;
        set;
    }
    public string Canvas {
        get;
        set;
    }


    public DeskHelpController() {

        system.debug('Test .....');


        Aircraft_Vid = 'https://www.youtube.com/embed/IYRgS9F6-WU';
        Aircraft_Vid_1 = 'https://www.youtube.com/embed/v8-ER5aO0_M';
        Aircraft_Vid_2 = 'https://www.youtube.com/embed/hr3ZI1uFqts';
        Aircraft_Vid_3 = 'https://www.youtube.com/embed/hr3ZI1uFqts?rel=0&amp;controls=0&amp;showinfo=0';
        Aircraft_Vid_4 = 'https://www.youtube.com/embed/v8-ER5aO0_M?rel=0&amp;showinfo=0';


        Login = 'https://www.youtube.com/embed/wt6GBXLal3I?rel=0&amp;showinfo=0';
        Home = 'https://www.youtube.com/embed/swxTEdkbzho';

        Aircraft_Overview = 'https://www.youtube.com/embed/IYRgS9F6-WU';
        Create_new_Aircraft = 'https://www.youtube.com/embed/v8-ER5aO0_M';
        Add_Edit_Aircraft_details = 'https://www.youtube.com/embed/hr3ZI1uFqts';
        Generate_spec_sheet = 'https://www.youtube.com/embed/2a-h2YUGkOA';
        New_Engine_Assembly = 'https://www.youtube.com/embed/FPL7jmQkU54';
        Assemblies = 'https://www.youtube.com/embed/PsCpdJXj5u0';
        Add_LLPs_and_thrust_usage = 'https://www.youtube.com/embed/MqC9yv-8FfQ';
        LLP_bulk_upload = 'https://www.youtube.com/embed/53jiiqD-C6Y';

        New_Maintenance_Program = 'https://www.youtube.com/embed/ALMf2ABt0cY';
        Engine_Template = 'https://www.youtube.com/embed/Icz-Z-qn2EY';
        Monthly_Utilization = 'https://www.youtube.com/embed/t7tUPcDAGWo';
        Monthly_Utilization_Invoice_and_payment = 'https://www.youtube.com/embed/HbpKfYrgPPg';
        Airframe_maintenance_event = 'https://www.youtube.com/embed/-TNWig0kMKo';
        Airframe_maintenance_event_Invoice_and_Payment = 'https://www.youtube.com/embed/zqNHWCe21Vg';
        Assembly_maintenance_event = 'https://www.youtube.com/embed/L6DiSCizTfU';
        Assembly_maintenance_event_Invoice_and_Payment = 'https://www.youtube.com/embed/WJ9f94O9U5M';
        Forecast_Scenario = 'https://www.youtube.com/embed/qz0nLVpgezU';

        Operator_overview = 'https://www.youtube.com/embed/t5iQ4blY_S4';
        Add_Edit_Operator_details = 'https://www.youtube.com/embed/cj9iBun-wYk';

        Address_Book = 'https://www.youtube.com/embed/YQc7ypO5LuI';
        Interaction_Log = 'https://www.youtube.com/embed/BkAf5Cr6_pY';

        Lease_overview = 'https://www.youtube.com/embed/mglWzR-Ckno';
        Add_Edit_Lease_details = 'https://www.youtube.com/embed/-ZSdZFfRfnE';
        Lease_buttons = 'https://www.youtube.com/embed/BkAf5Cr6_pY';
        Create_income_schedule = 'https://www.youtube.com/embed/ASVhIHRNd9k';
        Rent_invoice_and_payment = 'https://www.youtube.com/embed/Rtou22RajaM';
        Audit = 'https://www.youtube.com/embed/SeIjhY65xqY';
        Lease_term_history = 'https://www.youtube.com/embed/iiZ76FjczLg';
        Set_up_Assembly_MR_rates = 'https://www.youtube.com/embed/Q6hZbdGhKdI';
        Set_up_LLP_MR_rate_as_per_catalog_price = 'https://www.youtube.com/embed/M6R2S7iqBGA';

        Marketing_Campaign_overview = 'https://www.youtube.com/embed/25_p_c8dNAc';
        Add_new_Marketing_Campaign = 'https://www.youtube.com/embed/7lUyOjAr_iw';
        Marketing_Campaign_Interaction_Log = 'https://www.youtube.com/embed/-reGOuHmYHk';
        New_Aircraft_in_Campaign = 'https://www.youtube.com/embed/i6RPjtoLPxY';

        Prospect_overview = 'https://www.youtube.com/embed/uzJdgKxCYiI';
        Add_new_prospect = 'https://www.youtube.com/embed/oISLX1HNSPU';
        Auto_create_prospects = 'https://www.youtube.com/embed/5AUOffCB1Ds';
        Transaction_Overview = 'https://www.youtube.com/embed/wcjZDnViwLw';
        Marketing_Activity = 'https://www.youtube.com/embed/DE9XRJ375Cs';
        Canvas = 'https://www.youtube.com/embed/lFIrC-gL9ZA';




    }

	
   @AuraEnabled
    public static string getHelpDeskURL(string vParameter){
        string urlstr='https://lease-works.desk.com'; 
        system.debug('vParameter ='+vParameter);       
       
        // for default URL Link call
       	list<Custom_Lookup__c> homeRecord=[SELECT Id
        			, Lookup_Type__c,Sub_LookupType__c,Sub_LookupType2__c, Details__c
                FROM Custom_Lookup__c where Lookup_Type__c ='LW_HELP' AND Active__c=True
                AND Sub_LookupType__c= 'HOME'];
        if(homeRecord.size()>0){
           if(homeRecord[0].Details__c!=null) urlstr= homeRecord[0].Details__c;
        }
                
                
        string vParam = vParameter;
        if(vParam==null || vParam=='') return urlstr;
        else vParam=vParam.toLowerCase().remove('leaseworks__');

        system.debug('SOQL for getting help video data from custom lookup Object: '+vParam);
        list<Custom_Lookup__c> allRecord=[SELECT Id
        			, Lookup_Type__c,Sub_LookupType__c,Sub_LookupType2__c, Details__c
                FROM Custom_Lookup__c where Lookup_Type__c ='LW_HELP' AND Active__c=True
                AND Sub_LookupType__c= :vParam AND Y_Hidden_Header__c='HEADER'];
        
        system.debug('allRecord ='+allRecord.size());
        if(allRecord.size()>0){
           if(allRecord[0].Details__c!=null) urlstr= allRecord[0].Details__c;
        }
       system.debug('urlstr ='+urlstr); 
       return urlstr;
    }
	
	
	
    // This will get called from Lightning Component LWHelp page

    @AuraEnabled

    public static list < Custom_Lookup__c > getHelpVideo(string vParameter) {

        system.debug('apex class LeaseworkHelpController');
        system.debug('vParameter =' + vParameter);

        string vParam = vParameter;
        boolean Nofilter = false;
        if (vParam == null || vParam == '') Nofilter = true;
        else vParam = vParam.toLowerCase().remove('leaseworks__');

        list < Custom_Lookup__c > retrurnList = new list < Custom_Lookup__c > ();
        system.debug('SOQL for getting help video data from custom lookup Object: ' + vParam);

        //generaic        

        list < Custom_Lookup__c > allRecord = [SELECT Id, Name, Y_Hidden_Final_Name__c, Y_Hidden_Header__c, Short_Name__c, Lookup_Type__c, Sub_LookupType__c, Sub_LookupType2__c, Details__c, Description__c, Order__c
            FROM Custom_Lookup__c where Lookup_Type__c = 'LW_HELP'
            AND Active__c = True
            order by Y_Hidden_Video_Order__c ASC NULLS LAST
        ];

        system.debug('Nofilter: ' + Nofilter);
        if (Nofilter) return allRecord;

        //to find menu mapping based on object

        list < Custom_Lookup__c > allMappingRecord = [SELECT Id, Name, Y_Hidden_Final_Name__c, Y_Hidden_Header__c
            , Short_Name__c, Lookup_Type__c, Sub_LookupType__c, Sub_LookupType2__c
            , Details__c, Description__c, Order__c
            FROM Custom_Lookup__c
            where Lookup_Type__c = 'LW_HELP_MAP'
            and Sub_LookupType__c != null AND Active__c = True
            order by Y_Hidden_Video_Order__c ASC NULLS LAST
        ];



        set < string > mappingSet = new set < string > ();
        mappingSet.add(vParam.toLowerCase());
        for (Custom_Lookup__c currCusLook: allMappingRecord) {
            if (currCusLook.Sub_LookupType__c.toLowerCase() == vParam.toLowerCase()) {
                mappingSet.add(currCusLook.Name.toLowerCase());
            }
        }

        system.debug('mappingSet==' + mappingSet);
        for (Custom_Lookup__c crRec: allRecord) {
            system.debug('crRec.Sub_LookupType__c.toLowerCase()==' + crRec.Sub_LookupType__c.toLowerCase());
            if (mappingSet.contains(crRec.Sub_LookupType__c.toLowerCase())) retrurnList.add(crRec);

        }
        system.debug('retrurnList==' + retrurnList.size());
        if (retrurnList.size() == 0) return allRecord;

        return retrurnList;

    }

    @AuraEnabled
    public static map < string, string > getDealType_Apex() {
        //1. Get Deal Type
        string DealType = LeaseWareUtils.getLWSetup_CS('HelpLinkImage');
        if (DealType == null) DealType = 'IMG';
        system.debug('DealType==' + DealType);
        map < string, string > returnMap = new map < string, string > ();
        returnMap.put('DEAL_TYPE', DealType);


        return returnMap;
    }
    
 
    


}