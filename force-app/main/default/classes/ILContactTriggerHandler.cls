public class ILContactTriggerHandler implements ITrigger{
    
     // Constructor
     private final string  triggerBefore = 'ILContactTriggerHandlerBefore';
     private final string  triggerAfter = 'ILContactTriggerHandlerAfter';
   
	public void bulkBefore(){}
    
    public void bulkAfter(){}
    
    public void beforeInsert(){
         //validate duplicate attendee
         list<IL_Contact__c> newList = (list<IL_Contact__c>)trigger.new;
         set<id> ILIdSet = new set<id>();
         for(IL_Contact__c curRec : newList){
             ILIdSet.add(curRec.Interaction_Log__c);
         }
         map<Id,List<ID>> mapILContacts = getMapOfContactJOsForIL(ILIdSet);
         for(IL_Contact__c curRec: newList){
             if(mapILContacts.containsKey(curRec.Interaction_Log__c)){
                 List<Id> contactIdList = mapILContacts.get(curRec.Interaction_Log__c);
                 if(contactIdList.contains(curRec.Contact__c)){
                     curRec.addError('This Interaction log is already related to this Contact');
                 }
             }
         }
    }
    
    public void afterInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        updateIL();
    }
    
    public void beforeUpdate(){}
   
    public void afterUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        updateIL();
    }
   
    public void beforeDelete() {}
	
    public void afterDelete(){
        updateIL();
    }
    
    public void afterUnDelete(){}
    
    public void andFinally(){}


    /***************************************************************************************** */
    /* This method is to get the List of ContactIds for the IL
    /********************************************************************************************** */
    private  map<Id,List<Id>> getMapOfContactJOsForIL(Set<Id> ILIdSet){
        map<Id,List<Id>> mapILContacts = new map<Id,List<Id>>();
        List<Id> contactIdList=null;
        List<IL_Contact__c> contactJOList = [select Id, Interaction_Log__c,Contact__c,Contact__r.Name from IL_Contact__c where Interaction_Log__c IN :ILIdSet order by Id];
        for(IL_Contact__c curRec : contactJOList){
            if(mapILContacts.containsKey(curRec.Interaction_Log__c)){
                contactIdList = mapILContacts.get(curRec.Interaction_Log__c);
               
            }else{
                contactIdList = new List<id>();
            }
            contactIdList.add(curRec.Contact__c);
           
            mapILContacts.put(curRec.Interaction_Log__c,contactIdList);
        }
        return mapILContacts;
    }

    /**************************************************************************************************** */
    /* This method updates the Attendee Look ups on IL and also Hidden Field Attendee List
    Called From : After Insert/ After Update / After Delete
    /**************************************************************************************************** */

    private void updateIL(){
        list<IL_Contact__c> newList = (list<IL_Contact__c>)(trigger.isDelete?trigger.old:trigger.new);
        system.debug('newList---'+newList);
        set<id> ILIdSet = new set<id>();
        for(IL_Contact__c curRec : newList){
            ILIdSet.add(curRec.Interaction_Log__c);
        }
        String attendeeNames;
        List<Customer_Interaction_Log__c> cLogList =[select Id, Name , Meeting_Attendee_1__c,Meeting_Attendee_2__c,Meeting_Attendee_3__c,
        Meeting_Attendee_4__c,Meeting_Attendee_5__c,Meeting_Attendee_6__c,Meeting_Attendee_7__c,Meeting_Attendee_8__c,Meeting_Attendee_9__c,Meeting_Attendee_10__c,(select Id,Name,Contact__c,Contact__r.name from ILContacts__r order by CreatedDate) from Customer_Interaction_Log__c where ID IN : ILIdSet];
       
        
        for(Customer_Interaction_Log__c curRec: cLogList){
            clearContacts(curRec);
             attendeeNames = '';
             List<IL_Contact__c> ctList = curRec.ILContacts__r;
            for(Integer i =0;i<ctList.size() ;i++){
                switch on i{
                    when 0{
                        curRec.Meeting_Attendee_1__c = ctList[0].Contact__c;
                        attendeeNames = ctList[0].Contact__r.name;
                    }
                    when 1{
                        curRec.Meeting_Attendee_2__c = ctList[1].Contact__c;
                        attendeeNames +=', '+ ctList[1].Contact__r.name;
                    }
                    when 2{
                        curRec.Meeting_Attendee_3__c = ctList[2].Contact__c; 
                        attendeeNames +=', '+ ctList[2].Contact__r.name;
                    }
                    when 3{
                        curRec.Meeting_Attendee_4__c = ctList[3].Contact__c; 
                        attendeeNames +=', '+ ctList[3].Contact__r.name;
                    }
                    when 4{
                        curRec.Meeting_Attendee_5__c = ctList[4].Contact__c; 
                        attendeeNames +=', '+ ctList[4].Contact__r.name;
                    }
                    when 5{
                        curRec.Meeting_Attendee_6__c = ctList[5].Contact__c;
                        attendeeNames +=', '+ ctList[5].Contact__r.name; 
                    }
                    when 6{
                        curRec.Meeting_Attendee_7__c = ctList[6].Contact__c; 
                        attendeeNames +=', '+ ctList[6].Contact__r.name;
                    }
                    when 7{
                        curRec.Meeting_Attendee_8__c = ctList[7].Contact__c; 
                        attendeeNames +=', '+ ctList[7].Contact__r.name;
                    }
                    when 8{
                        curRec.Meeting_Attendee_9__c = ctList[8].Contact__c;
                        attendeeNames +=', '+ ctList[8].Contact__r.name; 
                    }
                    when 9{
                        curRec.Meeting_Attendee_10__c = ctList[9].Contact__c; 
                        attendeeNames +=', '+ ctList[9].Contact__r.name;
                    }
                    when else{
                        attendeeNames +=', '+ ctList[i].Contact__r.name;
                    }
                    

                }
            }
            curRec.Attendees_List__c = attendeeNames ;
        }
      
        try{
            LeasewareUtils.TriggerDisabledFlag = true;
            update cLogList;
            LeasewareUtils.TriggerDisabledFlag = false;
        }catch(DmlException e){
            String exMessage = e.getMessage();
             	String customValidationString = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
             	if(exMessage.contains(customValidationString)){
                    	Integer index1 = exMessage.lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                    	Integer index2 = exMessage.indexOf(': [');
            			exMessage= exMessage.substring(index1+customValidationString.length()+1,index2);
                        system.debug('exMessage---'+exMessage);
                       
            			system.debug('Filtered Error Message---'+ exMessage);
                 }
            		newList[0].addError(exMessage);
        }
       
      	
    }

    private void clearContacts(Customer_Interaction_Log__c curRec){
        curRec.Meeting_Attendee_1__c = null;
        curRec.Meeting_Attendee_2__c =null;
        curRec.Meeting_Attendee_3__c =null;
        curRec.Meeting_Attendee_4__c = null;
        curRec.Meeting_Attendee_5__c = null;
        curRec.Meeting_Attendee_6__c = null;
        curRec.Meeting_Attendee_7__c = null;
        curRec.Meeting_Attendee_8__c = null;
        curRec.Meeting_Attendee_9__c = null;
        curRec.Meeting_Attendee_10__c = null;
    }
}