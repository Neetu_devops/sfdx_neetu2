@isTest
public class TestLegalEntityController {
    
    
    public static testMethod void testgetLegalEntityData(){
        Aircraft__c aircraft= new Aircraft__c(Name='new Aircraft',MSN_Number__c='234234234234',
                                               Date_of_Manufacture__c=System.today(),Aircraft_Type__c = 'A320',
                                               Engine_Type__c='BR700', Aircraft_Variant__c='700', CSN__c=23454, TSN__c= 11246.00);
        insert aircraft;
        
        lease__c leaseObject =  new lease__c(Name='New Lease',
                                             Lease_Start_Date_New__c = Date.newInstance(2013, 7, 6),Lease_End_Date_New__c = Date.newInstance(2030, 6, 6),
                                             Aircraft__c=aircraft.Id);
        
        legal_entity__c leA = new legal_Entity__c(name='A');
        legal_entity__c leb = new legal_Entity__c(name='B');
        legal_entity__c lec = new legal_Entity__c(name='C');
        list<legal_entity__c>  legalEntities = new list<legal_entity__c>{leA,leB,leC};
        insert legalEntities;
        
        counterparty__c lessor = new counterparty__c(name='ABC Leasing',code__c='404',Subsidiary_Entity__c=true,legal_entity__c =lea.Id);
        counterparty__c headlessor = new counterparty__c(name='DEF Leasing',code__c='505',Subsidiary_Entity__c=true,legal_entity__c =leb.Id);
        counterparty__c HeadlessorII = new counterparty__c(name='GHI Leasing',code__c='606',Subsidiary_Entity__c=true,legal_entity__c =lec.Id);
        list<counterParty__c> lessorList = new list<counterParty__c>();
        lessorlist.add(lessor); lessorlist.add(headlessor); lessorlist.add(HeadlessorII);
		insert lessorList;
        
        leaseObject.lessor__c = lessor.id;
        leaseObject.head_lessor__c = headLessor.id;
        leaseObject.head_lessor_II__c = headLessorII.id;
        
        insert leaseObject;
        
        
	    LeaseWareUtils.clearFromTrigger();
        Test.startTest();  
        List<LegalEntityController.LegalEntityData> legalEntity = LegalEntityController.getLegalEntityData(String.ValueOf(leaseObject.id));
        system.assertEquals(legalEntity.size(), 2);
        list<LegalEntityController.LegalEntityData> leList = new list<LegalEntityController.LegalEntityData>(); 
        LegalEntityController.LegalEntityData hl = new LegalEntityController.LegalEntityData(headlessor.id,'HeadLessor','DEF Leasing','Percentage',98);
        LegalEntityController.LegalEntityData hl2 = new LegalEntityController.LegalEntityData(headlessorII.id,'HeadLessorII','GHI Leasing','Amount',98000);
        leList.add(hl); leList.add(hl2);
        LegalEntityController.saveLeaseHierarchy(JSON.serialize(leList), String.ValueOf(leaseObject.id));
        list<lease_hierarchy__c> hrcylist = [select id,name,RentPayable__c from lease_hierarchy__c where lease__c = :leaseObject.id];
        system.assertEquals(hrcylist.size(), 3);
        Test.stopTest();
        
        
    }
}