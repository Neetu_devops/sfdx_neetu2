public class AsyncUtil{

    public static void aynchPETriggerHandlerCall(){
    
        set<id> listIds;
        for(AsyncCallPE__e eventRec: (list<AsyncCallPE__e>)trigger.new){

            // deserialize the Ids.
            if(eventRec.Impacted_Ids__c!=null || eventRec.Impacted_Ids__c!=''){
                listIds = (set<id>)JSON.deserialize(eventRec.Impacted_Ids__c,set<id>.class);
            }

            //call method based on Type
            switch on eventRec.Method_To_Call__c {
                when  'CallABCMethod'{
                    //XYZ.CallABCMethod(listIds);
                    system.debug('AsyncUtil.aynchPETriggerHandlerCall :  calling XYZ.CallABCMethod(listIds) ');
                }
                when 'updateLatestPaymentOnLeaseFuture'
                {   
                    //Added as part of https://app.asana.com/0/1146992764355337/1173411059757377/f
                    //invoking a future method from platform event since the impact of making the method synchronous needs review.                  
                    Set<String> idStrs = (Set<String>)JSON.deserialize(JSON.serialize(listIds), Set<String>.class);
                    LeasewareUtils.updateLatestPaymentOnLeaseFuture(idStrs);
                    system.debug('AsyncUtil.aynchPETriggerHandlerCall :  calling LeasewareUtils.updateLatestPaymentOnLeaseFuture(listIds) ');
                }
                when 'insertParallelReportingObject'
                {    
                    //Added for creating Parallel Reporting Object for Asana : https://app.asana.com/0/1181547498580321/1196789536350713/f       
                    OperationalTransactionRegisterHandler.manageOperationTransactionRegisterRecord(listIds);
                    system.debug('AsyncUtil.aynchPETriggerHandlerCall :  calling OperationalTransactionRegisterHandler.manageOperationTransactionRegisterRecord ');
                }
                when else {
                    system.debug('!!! AsyncUtil.aynchPETriggerHandlerCall :   No Matching method');
                }
            }
            
        }
    
    
    }


}