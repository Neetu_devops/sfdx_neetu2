@isTest
public class TestSnapshotCreatorControllerIndex {
    
    @isTest
    static void loadTestCases() {
        Date myDate1 = Date.newInstance(2010, 2, 17);
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'V2524',
            MSN_Number__c = '1230',
            MTOW_Leased__c = 134455,
            Aircraft_Type__c = 'A320',
            Aircraft_Variant__c = '400',
            Date_of_Manufacture__c = myDate1 );
        LeaseWareUtils.clearFromTrigger();
        insert aircraft;
        
        Custom_Lookup__c CL = new Custom_Lookup__c();
        CL.Lookup_Type__c = 'Engine';
        CL.Name = 'Default';
        insert CL;
        
        Constituent_Assembly__c assembly1 = new Constituent_Assembly__c();
        assembly1.Name = 'TEst Assembly';
        assembly1.Engine_Model__c = 'CFM56';
        assembly1.TSN__c = 1000;
        assembly1.Attached_Aircraft__c = aircraft.Id;
        assembly1.Current_TS__c =CL.Id;
        assembly1.Serial_Number__c = '575910';
        assembly1.CSN__c = 2000;
        insert assembly1;
        
        Sub_Components_LLPs__c llp = new Sub_Components_LLPs__c();
        llp.Constituent_Assembly__c =assembly1.Id;
        llp.Name='Name';
        llp.TSN__c=1100;
        llp.TSLV__c=1000;
        llp.CSLV__c=1000;
        llp.Life_Limit_Cycles__c = 20000;
        llp.Part_Number__c='11232222';
        llp.Serial_Number__c = '11111';
        insert llp;
        
        Thrust_Setting__c newTS =new Thrust_Setting__c(
            Life_Limited_Part__c=llp.Id, 
            Name='Thrust1',
            Thrust__c =  20000,
            Thrust_Setting_Name__c = CL.id,
            Cycle_Limit__c= 20000, 
            Cycle_Used__c=   2000
        );
        insert newTS;
        
        String mapJson = '{"obj_api": "Aircraft__c","related_obj_api": "","child":[{"obj_api": "Constituent_Assembly__c","related_obj_api": "Attached_Aircraft__c","child": [{"obj_api": "Sub_Components_LLPs__c","related_obj_api": "Constituent_Assembly__c","child": [{"obj_api": "Thrust_Setting__c","related_obj_api": "Life_Limited_Part__c","child": []}]}]},{"obj_api": "Equipment__c","related_obj_api": "Aircraft__c","child":[]}]}';
        String namespace = LeaseWareUtils.getNamespacePrefix();
        if(!String.isBlank(namespace))
            mapJson = '{"obj_api":"leaseworks__Aircraft__c","related_obj_api":"","child":[{"obj_api":"leaseworks__Constituent_Assembly__c","related_obj_api":"leaseworks__Attached_Aircraft__c","child":[{"obj_api":"leaseworks__Sub_Components_LLPs__c","related_obj_api":"leaseworks__Constituent_Assembly__c","child":[{"obj_api":"leaseworks__Thrust_Setting__c","related_obj_api":"leaseworks__Life_Limited_Part__c","child":[]}]}]},{"obj_api":"leaseworks__Equipment__c","related_obj_api":"leaseworks__Aircraft__c","child":[]}]}';
        Snapshot_Configuration__c snapshot = new Snapshot_Configuration__c();
        snapshot.Name = 'Asset-' +System.today().format();
        snapshot.Active__c = true;
        snapshot.Primary_Object__c = 'Aircraft__c';
        snapshot.Related_Object__c = mapJson;
        insert snapshot;
        
        
        ContentVersion contentversionVarient = new ContentVersion();
        contentversionVarient.Title = 'RelatedObject';
        contentversionVarient.contentLocation='S';
        contentversionVarient.PathOnClient = 'RelatedObject.json';
        contentversionVarient.FirstPublishLocationId = snapshot.Id;
        contentversionVarient.VersionData = Blob.valueOf(mapJson);
        insert contentversionVarient;
        
        
        SnapshotCreatorControllerIndex.createSnapshots(aircraft.Id, 'Test class');
        // SnapshotCreatorController.getJsonForRelatedObj(aircraft.Id, snapshot.Id);
    }
    
    
}