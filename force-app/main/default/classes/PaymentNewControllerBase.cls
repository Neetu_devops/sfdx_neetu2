public abstract class PaymentNewControllerBase {
    private SObjectType parentSobType;
    private SObjectType childSobType;
    private string fieldName ;
    private string Defaultvalue ='Acquisition';
    public PaymentNewControllerBase (SObjectType parentSobType, SObjectType childSobType) {
        this.parentSobType = parentSobType;
        this.childSobType = childSobType;
    }
    public PaymentNewControllerBase (SObjectType parentSobType, SObjectType childSobType,string Defaultvalue) {
        this.parentSobType = parentSobType;
        this.childSobType = childSobType;
        this.Defaultvalue = Defaultvalue;
    }    
    public PaymentNewControllerBase (SObjectType parentSobType, SObjectType childSobType,string fieldname,string Defaultvalue) {
        this.parentSobType = parentSobType;
        this.childSobType = childSobType;
        this.fieldname = fieldname;
        this.Defaultvalue = Defaultvalue;
    }    
    public PageReference url() {
        PageReference p = new PageReference('/' + childSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        m.putAll(ApexPages.currentPage().getParameters());
        m.put('Name', '<SYSTEM GENERATED>');
        m.put('nooverride', '1');
        m.put('RecordType', getChildRecordTypeId(getParentSObjectId(m)));
        return p;
    }
    
    /* This will set based on Field Value */
/* Not using right now    
    public PageReference Url_FieldBased() {
        PageReference p = new PageReference('/' + childSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        m.putAll(ApexPages.currentPage().getParameters());
        m.put('Name', '<SYSTEM GENERATED>');
        m.put('nooverride', '1');
        m.put('RecordType', getChildRecordTypeId_Fieldbased(getParentSObjectId(m)));
        return p;
    }    

    private Id getChildRecordTypeId_Fieldbased(Id parentId) {

			system.debug('Parent Id  -' + parentId + '==fieldname  -' + fieldname + '==Defaultvalue=='+ Defaultvalue);
		string StrFieldName = leasewareUtils.getNamespacePrefix() + fieldname ;
		string StrQuery = 'select ' + StrFieldName +  ' from ' + String.valueOf(parentSobType)
                + ' where Id = :parentId' ;
			system.debug('StrQuery  -' + StrQuery);  
			              
        SObject parent = Database.query(StrQuery);
        String fieldValue;
        try{
        	system.debug('===='+ parent.get(StrFieldName));
            fieldValue = (String) parent.get(StrFieldName);
            system.debug('Parent - ' + parent + '==fieldValue=='+fieldValue);
            if(fieldValue==null) fieldValue=Defaultvalue ;
        }catch(exception e){
            //Parent's record type is not set. Default to Acquisition.
            system.debug(' Error msg :  -' + e);
            fieldValue=Defaultvalue ;
        }    
        return [select Id from RecordType where SObjectType = :String.valueOf(childSobType)
                and Name = :fieldValue].Id;
    }
*/

    
    private Id getChildRecordTypeId(Id parentId) {

		system.debug('Parent Id  -' + parentId);
        SObject parent = Database.query('select RecordType.Name from ' + String.valueOf(parentSobType)
                + ' where Id = :parentId');
        String parentDeveloperName;
        try{
            parentDeveloperName = (String) parent.getSobject('RecordType').get('Name');
            system.debug('Parent - ' + parentDeveloperName + ' Child - ' + String.valueOf(childSobType));
        }catch(exception e){
            //Parent's record type is not set. Default to Acquisition.
            //parentDeveloperName='Acquisition';
            parentDeveloperName=Defaultvalue;
        }    
        return [select Id from RecordType where SObjectType = :String.valueOf(childSobType)
                and Name = :parentDeveloperName].Id;
    }
    private Id getParentSObjectId(Map<String, String> m) {
        for (String key : m.keySet()) {
            if (key.endsWith('_lkid')) {
                return m.get(key);
            }
        }
        return null;
    }


    public PageReference setRecType() {
        PageReference p = new PageReference('/' + childSobType.getDescribe().getKeyPrefix() + '/e');
        Map<String, String> m = p.getParameters();
        m.putAll(ApexPages.currentPage().getParameters());
        m.put('nooverride', '1');
        m.put('RecordType', getChildRecordTypeId(getParentSObjectId(m)));
        return p;
    }


}