/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDepreciation {

    static testMethod void DepreciationCRUDTest() {
        // TO DO: implement unit test
        
        Depreciation_schedule__c recDS= new Depreciation_schedule__c(name = 'Test'
        	,Status__c = 'New');
        insert recDS;
        recDS.Description__c = 'test desc';
        update recDS;
        Depreciation_Rate__c recDR1 = new Depreciation_Rate__c(name = 'Test'
        	,Depreciation_Rate__c = 20
        	,Depreciation_schedule__c = recDS.Id
        	,Period_Sequence__c = 1)	;
        Depreciation_Rate__c recDR2 = new Depreciation_Rate__c(name = 'Test2'
        	,Depreciation_Rate__c = 80
        	,Depreciation_schedule__c = recDS.Id
        	,Period_Sequence__c = 2)	;        	
        insert recDR1;
        insert recDR2;

		
	    LeaseWareUtils.clearFromTrigger();
        
        delete recDR1;
        delete recDR2;
        
		
	    LeaseWareUtils.clearFromTrigger();
	    
	    undelete recDR2  ;   
        
        delete recDS;
        undelete recDS ;
        
        
    }
    
    static testMethod void DepreciationUpdateTest() {
        // TO DO: implement unit test
        
        Depreciation_schedule__c recDS= new Depreciation_schedule__c(name = 'Test'
        	,Status__c = 'New');
        insert recDS;
        recDS.Description__c = 'test desc';
        update recDS;
        Depreciation_Rate__c recDR1 = new Depreciation_Rate__c(name = 'Test'
        	,Depreciation_Rate__c = 20
        	,Depreciation_schedule__c = recDS.Id
        	,Period_Sequence__c = 1)	;
        Depreciation_Rate__c recDR2 = new Depreciation_Rate__c(name = 'Test2'
        	,Depreciation_Rate__c = 80
        	,Depreciation_schedule__c = recDS.Id
        	,Period_Sequence__c = 2)	;        	
        insert recDR1;
        insert recDR2;

		
	    LeaseWareUtils.clearFromTrigger();

		recDR1.Period_Sequence__c = 2;
		recDR2.Period_Sequence__c = 1;
        update recDR1;
        update recDR2;		
        
		recDS.Status__c = 'Active';
		update recDS;
        
        
    }    
}