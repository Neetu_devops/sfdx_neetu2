/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 */
@isTest
private class TestContact {

    static testMethod void testCreateUpdateContact(){
    	Contact ctSalesRep;
    	Operator__c newOp;
    	Counterparty__c newLessor;
    	Bank__c newCP;

    	try{
    		newOp = new Operator__c(Name='Operator For Contact Testing 1', Alias__c='Alias 1 for Op 1, Alias 2 for Op 1',Aircraft_Type_Of_Interest_gp__c ='747');
    		insert newOp;
    	}catch(exception e){
    		try{
    			system.debug('Exception ' + e);
    			newOp = [select id from Operator__c where Name = 'Operator For Contact Testing 1' limit 1];
    		}catch(exception e2){
    			system.debug('Operator creation failed. Exception ' + e2);
    		}
    	}

    	try{
    		newLessor = new Counterparty__c(name='Lessor For Contact Testing 1');
    		insert newLessor;
    	}catch(exception e){
    		try{
    			system.debug('Exception ' + e);
    			newLessor = [select id from Counterparty__c where Name = 'Lessor For Contact Testing 1' limit 1];
    		}catch(exception e2){
    			system.debug('Lessor creation failed. Exception ' + e2);
    		}
    	}
    	
		LeaseWareUtils.clearFromTrigger();
    	try{
    		newCP = new Bank__c(name='Counterparty For Contact Testing 1');
    		insert newCP;
    	}catch(exception e){
    		try{
    			system.debug('Exception ' + e);
    			newCP = [select id from Bank__c where Name = 'Counterparty For Contact Testing 1' limit 1];
    		}catch(exception e2){
    			system.debug('Counterparty creation failed. Exception ' + e2);
    		}
    	}

system.debug('Operator, Lessor and CP Ids ' + newOP.id + ', ' + newLessor.Id + ', ' + newCP.id);
    	
		LeaseWareUtils.clearFromTrigger();
		try{
	        ctSalesRep = new contact(firstname='Test FN', lastname='Test LN', Company__c='Operator For Contact Testing 1');
	        insert ctSalesRep;
	        ctSalesRep = [select Operator__c, Lessor__c, Counterparty__c from Contact where id = :ctSalesRep.id];
system.debug('Op ID ' + ctSalesRep.Operator__c + ' - ' + (newOp==null?null:newOp.id));
	        system.assert(ctSalesRep.Lessor__c == null);
	        system.assert(ctSalesRep.Counterparty__c == null);
	        system.assert(ctSalesRep.Operator__c == (newOp==null?null:newOp.id));
		}catch(exception e){
			system.debug('Exception ' + e);
		}

		LeaseWareUtils.clearFromTrigger();
		try{
	        ctSalesRep.Company__c = 'Lessor For Contact Testing 1';
	        update ctSalesRep;
	        ctSalesRep = [select Operator__c, Lessor__c, Counterparty__c from Contact where id = :ctSalesRep.id];
system.debug('Lessor ID ' + ctSalesRep.Lessor__c + ' - ' + (newOp==null?null:newLessor.id));
	        system.assert(ctSalesRep.Operator__c == null);
	        system.assert(ctSalesRep.Counterparty__c == null);
	        system.assert(ctSalesRep.Lessor__c == (newLessor==null?null:newLessor.id));
		}catch(exception e){
			system.debug('Exception ' + e);
		}

		LeaseWareUtils.clearFromTrigger();
		try{
	        ctSalesRep.Company__c = 'Counterparty For Contact Testing 1';
    	    update ctSalesRep;
	        ctSalesRep = [select Operator__c, Lessor__c, Counterparty__c from Contact where id = :ctSalesRep.id];
system.debug('CP ID ' + ctSalesRep.Counterparty__c + ' - ' + (newOp==null?null:newCP.id));
	        system.assert(ctSalesRep.Operator__c == null);
	        system.assert(ctSalesRep.Lessor__c == null);
	        system.assert(ctSalesRep.Counterparty__c == (newCP==null?null:newCP.id));
		}catch(exception e){
			system.debug('Exception ' + e);
		}
        
		LeaseWareUtils.clearFromTrigger();
		try{
	        ctSalesRep.Company__c = 'Alias 2 for Op 1';
	        update ctSalesRep;
	        ctSalesRep = [select Operator__c, Lessor__c, Counterparty__c from Contact where id = :ctSalesRep.id];
system.debug('Op ID ' + ctSalesRep.Operator__c + ' - ' + (newOp==null?null:newOp.id));
	        system.assert(ctSalesRep.Lessor__c == null);
	        system.assert(ctSalesRep.Counterparty__c == null);
	        system.assert(ctSalesRep.Operator__c == (newOp==null?null:newOp.id));
		}catch(exception e){
			system.debug('Exception ' + e);
		}

/*		LeaseWareUtils.clearFromTrigger();
		try{
	        ctSalesRep.Company__c = null;
	        update ctSalesRep;
	        ctSalesRep = [select Operator__c, Lessor__c, Counterparty__c from Contact where id = :ctSalesRep.id];
	        system.assert(ctSalesRep.Operator__c == null);
	        system.assert(ctSalesRep.Lessor__c == null);
	        system.assert(ctSalesRep.Counterparty__c == null);
		}catch(exception e){
			system.debug('Exception ' + e);
		}
*/
		LeaseWareUtils.clearFromTrigger();
		try{
	        delete ctSalesRep;
		}catch(exception e){
			system.debug('Exception ' + e);
		}

		LeaseWareUtils.clearFromTrigger();
		try{
	        undelete ctSalesRep;
		}catch(exception e){
			system.debug('Exception ' + e);
		}

    }
    
    static testMethod void testCRUDOperator(){
    	    Operator__c newOp = new Operator__c(Name='Operator For Contact Testing 1', Alias__c='Alias 1 for Op 1, Alias 2 for Op 1',Aircraft_Type_Of_Interest_gp__c =null);
    		insert newOp;
    		LeaseWareUtils.clearFromTrigger();
    		newOp.Contacted_For_AC_Types_gp__c ='747';
    		newOp.Aircraft_Type_Of_Interest_gp__c ='747';
    		update newOp;    		
    }
    
     static testMethod void Test_OperatorCommaEmail() {
    	Test.startTest();
    	list<Operator__c> listOperator = new list<Operator__c>();
    	
    	Operator__c operator1 = new Operator__c();
    	operator1.Name = 'New Operator1';
    	operator1.Main_Email_Address_New__c='abc,def';
    	operator1.Billing_Email_Address_New__c = 'ghi, jkl';
    	
    	listOperator.add(operator1);
    	
    	Operator__c operator2 = new Operator__c();
    	operator2.Name = 'New Operator1';
    	operator2.Main_Email_Address_New__c='abc@gmail.com,def@gmail.com';
    	operator2.Billing_Email_Address_New__c = 'ghi, jkl';
    	
    	listOperator.add(operator2);
    	
    	Operator__c operator3 = new Operator__c();
    	operator3.Name = 'New Operator1';
    	operator3.Main_Email_Address_New__c='abc@gmail.com,def@gmail.com';
    	operator3.Billing_Email_Address_New__c = 'ghi@gmail.com, jkl@gmail.com';
    	
    	listOperator.add(operator3);
    	
    	List<Database.SaveResult> listDBRes = Database.insert(listOperator,false);
    	system.debug('listDBRes.size() ='+listDBRes.size());
    	
    	Integer noOfOps=0;
    	for (Database.SaveResult sr : listDBRes) {
		    if (sr.isSuccess()) {
		        noOfOps=noOfOps+1;
		    }
		}
		System.assertEquals(noOfOps, 1);
    	Test.stopTest();
    
    }

    @isTest
    static void testCmpNameUpdateOnOpLessorCPChange(){
     	Operator__c newOp = new Operator__c(Name='Test Operator', Alias__c='Alias 1 for Op 1, Alias 2 for Op 1',Aircraft_Type_Of_Interest_gp__c ='747');
    	insert newOp;
        Counterparty__c newLessor = new Counterparty__c(name='Test Lessor');
        insert newLessor;
        Contact contact =  new Contact(firstname='Test FN', lastname='Test LN', Company__c='Test Operator');
        insert contact;
        Contact insertedContact = [select Id,Name,Operator__c from contact limit 1];
       	insertedContact.Operator__c = null;
        insertedContact.Lessor__c = newLessor.id;
        update insertedContact;
        Contact updatedContact = [select Id,Name,Company__c from contact limit 1];
        system.assertEquals(updatedContact.Company__c, newLessor.name);
        
	}
	
	@IsTest
	static void updateCmpIfAccountIdPresent(){
		Account acc= new Account(name='Test Account');
		insert acc;
		Contact con = new Contact(LastName='Test Contact');
		insert con;
		Contact con1 = [select Id,Name,AccountId from Contact limit 1];
		con1.AccountId = acc.id;
		update con1;

		Contact contact = [select Id, Name,AccountId,Company__c from  Contact limit 1];
		system.assertEquals(contact.Company__c,acc.Name);

	}

	@IsTest
	private  static void testCPAliasNameUpdate(){
		Bank__c cp1 = new Bank__c(name='Counterparty For Contact Testing 1',alias__c='CP1');
		insert cp1;
		Contact contact =  new Contact(firstname='Test FN', lastname='Test LN', Company__c='CP1');
		insert contact;
		Contact ct = [select Id,Name, Counterparty__c from Contact limit 1];
		system.assertEquals(ct.Counterparty__c,cp1.id);
		
	}

	@IsTest
	private static void testAccOnOperatorCreationWithSettingOn(){
		LW_Setup__c setup = new LW_Setup__c(name = 'Auto_Creation_of_Accounts', Value__c = 'ON', Disable__c = false, Description__c = 'When ON: a script will be ran to create all existing Operators/Lessors/Counterparties as Account and the workflow will be activated (to create an account every time a new Operator/Lessor/Counterparty is created by the user.Default Value: OFF');
        insert setup;
		Operator__c newOp = new Operator__c(Name='Test Operator ACC', Alias__c='Alias 1 for Op 1, Alias 2 for Op 1',Aircraft_Type_Of_Interest_gp__c ='747');
		insert newOp;
		List<Account> acc = [select Id,Name from Account where Name='Test Operator ACC'];
		system.assertEquals(1, acc.size());
	}

	@IsTest
	private static void testAccOnLessorCreationWIthSettingOn(){
		LW_Setup__c setup = new LW_Setup__c(name = 'Auto_Creation_of_Accounts', Value__c = 'ON', Disable__c = false, Description__c = 'When ON: a script will be ran to create all existing Operators/Lessors/Counterparties as Account and the workflow will be activated (to create an account every time a new Operator/Lessor/Counterparty is created by the user.Default Value: OFF');
        insert setup;
		
		Counterparty__c newLessor = new Counterparty__c(Name='Test Lessor ACC');
		insert newLessor;
		List<Account> acc = [select Id,Name from Account where Name='Test Lessor ACC'];
		system.assertEquals(1, acc.size());
	}

	@IsTest
	private static void testAccOnCPCreationWithSettingOn(){
		LW_Setup__c setup = new LW_Setup__c(name = 'Auto_Creation_of_Accounts', Value__c = 'ON', Disable__c = false, Description__c = 'When ON: a script will be ran to create all existing Operators/Lessors/Counterparties as Account and the workflow will be activated (to create an account every time a new Operator/Lessor/Counterparty is created by the user.Default Value: OFF');
        insert setup;
		
		Bank__c newCP = new Bank__c(Name='Test CP ACC');
		insert newCP;
		List<Account> acc = [select Id,Name from Account where Name='Test CP ACC'];
		system.assertEquals(1, acc.size());
	}

	@IsTest
	private static void testAccOnCPCreationWithSettingFF(){
		LW_Setup__c setup = new LW_Setup__c(name = 'Auto_Creation_of_Accounts', Value__c = 'OFF', Disable__c = false, Description__c = 'When ON: a script will be ran to create all existing Operators/Lessors/Counterparties as Account and the workflow will be activated (to create an account every time a new Operator/Lessor/Counterparty is created by the user.Default Value: OFF');
        insert setup;
		
		Bank__c newCP = new Bank__c(Name='Test CP ACC');
		insert newCP;
		List<Account> acc = [select Id,Name from Account where Name='Test CP ACC'];
		system.assertEquals(0, acc.size());
	}
	@isTest
	private static void duplicateCheckOnAccount(){
		LW_Setup__c setup = new LW_Setup__c(name = 'Auto_Creation_of_Accounts', Value__c = 'ON', Disable__c = false, Description__c = 'When ON: a script will be ran to create all existing Operators/Lessors/Counterparties as Account and the workflow will be activated (to create an account every time a new Operator/Lessor/Counterparty is created by the user.Default Value: OFF');
        insert setup;
		
		String unexpectedMessage;
		Counterparty__c newLessor = new Counterparty__c(Name='TestACC');
		insert newLessor;
		Bank__c newCP = new Bank__c(Name='TestACC');
		insert newCP;
		List<Account> acc = [select Id,Name,Type from Account where Name='TestACC'];
		system.assertEquals(1, acc.size());
        system.assertEquals('Lessor', acc[0].Type);
		}

		@isTest
		private static void testCompanyFilledOnContactCreation(){
			Operator__c opr = new Operator__c(name='TestOpr');
			insert opr;
			
			Contact ct = new Contact(LastName='TestUser',Operator__c =opr.Id);
			insert ct;
			Contact c1 = [select Id,Company__c from Contact];
			system.assertEquals('TestOpr', c1.Company__c, 'Set Operator Name on Company Field of COntact');
		}


	@IsTest
	static void testContactCreationFromOpr(){
		Account acc= new Account(name='Test Account');
		insert acc;

		Operator__c opr = new Operator__c(name='TestOpr');
		insert opr;
        Counterparty__c lessor = new Counterparty__c(name='TestLessor');
		insert lessor;
        Bank__c cp = new Bank__c(name='TestCP');
		insert cp;
			
		Contact ct = new Contact(LastName='TestUser',Operator__c =opr.Id,AccountId=acc.id,Lessor__c = lessor.id,Counterparty__c = cp.id);
		insert ct;
		//If Opr/Lessor/CP is already present while insert of contact the same has to be retained
		Contact fetchCT = [select Id,Company__c,Operator__c,Lessor__c,Counterparty__c from Contact ];
		system.assertEquals(fetchCT.Company__c , 'Test Account');
		system.assertEquals(fetchCT.Operator__c , opr.id);
        system.assertEquals(fetchCT.Lessor__c , lessor.id);
        system.assertEquals(fetchCT.Counterparty__c , cp.id);

		//On update of Company, Opr/Lessor/CP should update based on this 
		fetchCT.Company__c= 'TestLessor';
        update fetchCT;
        
        Contact fetchCT1 = [select Id,Company__c,Operator__c,Lessor__c,Counterparty__c from Contact ];
        system.assertEquals(fetchCT1.Operator__c ,null);
        system.assertEquals(fetchCT1.Lessor__c ,lessor.id);
        system.assertEquals(fetchCT1.Counterparty__c ,null);
        
        

	}

}