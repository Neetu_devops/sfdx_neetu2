public class InvoiceDiffApexController {
    

    @AuraEnabled
    public static wrapperClass initMethod(String recId, String objectName){
    
        wrapperClass wrapperClassObject;
        
        if(objectName.toLowerCase().contains('line')){
            wrapperClassObject = forInvoiceLineLevel(recId);
        }else{
            wrapperClassObject = forInvoiceHeaderLevel(recId);
        }
        
        
        //July 23, 2019
        String prefix = LeaseWareUtils.getNamespacePrefix() ;
 
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType invoiceSchema = schemaMap.get(prefix + 'Invoice_Line_Item__c');
        Map<String, Schema.SObjectField> fieldMap = invoiceSchema.getDescribe().fields.getMap();
        wrapperClassObject.mrReconciledAmountLabel = fieldMap.get(prefix + 'MR_Reconciled__c').getDescribe().getLabel();
        wrapperClassObject.mrOriginalAmountLabel = fieldMap.get(prefix + 'MR_Original__c').getDescribe().getLabel();
      
        return wrapperClassObject ;
   
    }
 
    private static Set<String> getOriginalURTypes(String reconciliationType){
        
        if(reconciliationType == 'Assumed Ratio'){
        
            return new Set<String>{'Actual','True Up Gross'};
            
        }else{
        
            return new Set<String>{'Assumed/Estimated'};
        }
    }
    
    private static Set<String> getReconciledURTypes(String reconciliationType){
    
    
        if(reconciliationType == 'Assumed Ratio'){
        
            return new Set<String>{'MR Reconciliation'};
            
        }else{
        
            return new Set<String>{'Actual','True Up Gross'};
        }
    
    }
 
    private static wrapperClass forInvoiceHeaderLevel(String recId){
        
        wrapperClass returnwrapperClass = new  wrapperClass ();   
        
        List<Invoice__c> invRecList = [select id, Reconciliation_Start_Date__c,Reconciliation_End_Date__c,
                                         Lease__c, Lease__r.Aircraft__c,MR_Reconciled__c,MR_Original__c,
                                         Invoiced_Amount_Formula__c, Reconciliation_Schedule__r.type__c
                                         from Invoice__c 
                                         where id = :recId
                                         And (Reconciliation_Schedule__r.type__c = 'Assumed FH and FC'
                                                   Or Reconciliation_Schedule__r.type__c = 'Assumed Ratio')];
        
        if(invRecList.size()>0){ 
        
        
            returnwrapperClass.reconciliationType = invRecList[0].Reconciliation_Schedule__r.type__c;
            
            map<id,Utilization_Report__c> URList_Actual = new map<id,Utilization_Report__c>([select id ,Month_Ending__c
                                    ,Total_Maintenance_Reserve__c
                        from Utilization_Report__c 
                        where Type__c in : getOriginalURTypes(invRecList[0].Reconciliation_Schedule__r.type__c) and Status__c = 'Approved By Lessor'
                            and Aircraft__c = :invRecList[0].Lease__r.Aircraft__c  
                            and Month_Ending__c >= :invRecList[0].Reconciliation_Start_Date__c 
                            and Month_Ending__c <= :invRecList[0].Reconciliation_End_Date__c                                                                 
                            and Y_Hidden_Lease__c = :invRecList[0].Lease__c                                                                
                        ORDER BY End_Date_F__c DESC, CreatedDate Asc // ascending order (Do not change order otherwise it will ruin true ups handling)
                        ]); 
                        
                        
            map<id,Utilization_Report__c> URList_Recon = new map<id,Utilization_Report__c>([select id ,Month_Ending__c
                                    ,Total_Maintenance_Reserve__c
                        from Utilization_Report__c 
                        where Type__c in : getReconciledURTypes(invRecList[0].Reconciliation_Schedule__r.type__c) and Status__c = 'Approved By Lessor'
                            and Aircraft__c = :invRecList[0].Lease__r.Aircraft__c 
                            and Month_Ending__c >= :invRecList[0].Reconciliation_Start_Date__c 
                            and Month_Ending__c <= :invRecList[0].Reconciliation_End_Date__c                                                                 
                            and Y_Hidden_Lease__c = :invRecList[0].Lease__c                                                                 
                        ORDER BY End_Date_F__c DESC, CreatedDate Asc// ascending order (Do not change order otherwise it will ruin true ups handling)
                        ]); 
            
            
            
            
            map<string,MRInnerClass> mapMRInnerclass = new map<string,MRInnerClass>();
            string keyStr;
            MRInnerClass mrInnerClassTemp;
    
            // MR Original
            for(Utilization_Report__c curUR: URList_Actual.values()){
                keyStr = curUR.Month_Ending__c.format();
                if(mapMRInnerclass.containsKey(keyStr)){
                    mrInnerClassTemp = mapMRInnerclass.get(keyStr);
                }else{
                    mrInnerClassTemp = new MRInnerClass(curUR.Month_Ending__c);
                }
                mrInnerClassTemp.MROriginal = curUR.Total_Maintenance_Reserve__c;
                mrInnerClassTemp.MROriginalLink = curUR.Id;
                mapMRInnerclass.put(keyStr,mrInnerClassTemp);
    
            }
            // MRReconcilled
            for(Utilization_Report__c curUR: URList_Recon.values()){
                keyStr = curUR.Month_Ending__c.format();
                if(mapMRInnerclass.containsKey(keyStr)){
                    mrInnerClassTemp = mapMRInnerclass.get(keyStr);
                }else{
                    mrInnerClassTemp = new MRInnerClass(curUR.Month_Ending__c);
                }
                mrInnerClassTemp.MRReconcilled = curUR.Total_Maintenance_Reserve__c;
                mrInnerClassTemp.MRReconcilledLink = curUR.Id;
                mapMRInnerclass.put(keyStr,mrInnerClassTemp);
    
            }
            system.debug('=='+JSON.serializePretty(mapMRInnerclass));
           
            returnwrapperClass.lstContact = mapMRInnerclass.values();
            returnwrapperClass.MROriginalSUM = invRecList[0].MR_Original__c; 
            returnwrapperClass.MRReconcilledSUM = invRecList[0].MR_Reconciled__c;
            returnwrapperClass.DifferenceSUM =  invRecList[0].Invoiced_Amount_Formula__c;
            
            system.debug('initMethod-');
        
        }
        return returnwrapperClass;          
    }

    private static wrapperClass forInvoiceLineLevel(string recId){
    
        wrapperClass returnwrapperClass = new  wrapperClass ();


        List<Invoice_Line_Item__c> invRecList = [select id, Invoice__r.Reconciliation_Start_Date__c,Assembly__r.Type__c,
                                        Invoice__r.Reconciliation_End_Date__c, Assembly__c, Invoice__r.Lease__c,
                                        Invoice__r.Lease__r.Aircraft__c,MR_Reconciled__c,MR_Original__c,
                                        Total_Line_Amount__c, Invoice__r.Reconciliation_Schedule__r.type__c,
                                        Reconciliation_line_item__r.Supplemental_Rent__c
                                        from Invoice_Line_Item__c 
                                        where id = :recId
                                        And (Invoice__r.Reconciliation_Schedule__r.type__c = 'Assumed FH and FC'
                                                   Or Invoice__r.Reconciliation_Schedule__r.type__c = 'Assumed Ratio')]; // ,MR_Original_LLP_If_Applicable__c,MR_Reconciled_LLP_If_Applicable__c

        
        
        if(invRecList.size()>0){
        
            returnwrapperClass.reconciliationType = invRecList[0].Invoice__r.Reconciliation_Schedule__r.type__c;
             
            decimal PrevFHFCRatio_v,PrevInterpolatedRate_v,ReconFHFCRatio_v,ReconInterpolatedRate_v;
        
            map<id,Utilization_Report_List_Item__c> URList_Actual = new map<id,Utilization_Report_List_Item__c>([select id ,For_The_Month_Ending__c
                                    ,Total_Maintenance_Reserve__c,Effective_FH_FC_Ratio__c 
                                    ,Y_Hidden_MR__c,Interpolated_MR_Rate_F__c,Effective_Escalation_Factor__c,MR_Rate_Esc__c,Assembly_MR_Info__r.ratio_table__c
                        from Utilization_Report_List_Item__c 
                        where Utilization_Report__r.Type__c in : getOriginalURTypes(invRecList[0].Invoice__r.Reconciliation_Schedule__r.type__c)
                            and Utilization_Report__r.Status__c = 'Approved By Lessor'
                            and Utilization_Report__r.Aircraft__c = :invRecList[0].Invoice__r.Lease__r.Aircraft__c  
                            and For_The_Month_Ending__c >= :invRecList[0].Invoice__r.Reconciliation_Start_Date__c 
                            and For_The_Month_Ending__c <= :invRecList[0].Invoice__r.Reconciliation_End_Date__c 
                            and Assembly_MR_Info__c = :invRecList[0].Reconciliation_line_item__r.Supplemental_Rent__c 
                            and Utilization_Report__r.Y_Hidden_Lease__c = :invRecList[0].Invoice__r.Lease__c
                        ORDER BY End_Date_F__c DESC, Utilization_Report__r.CreatedDate // ascending order (Do not change order otherwise it will ruin true ups handling)
                        ]); 
                        
            map<id,Utilization_Report_List_Item__c> URList_Recon = new map<id,Utilization_Report_List_Item__c>([select id ,For_The_Month_Ending__c
                                    ,Total_Maintenance_Reserve__c ,Effective_FH_FC_Ratio__c 
                                    ,Y_Hidden_MR__c,Interpolated_MR_Rate_F__c,Effective_Escalation_Factor__c,MR_Rate_Esc__c,Assembly_MR_Info__r.ratio_table__c
                        from Utilization_Report_List_Item__c 
                        where Utilization_Report__r.Type__c in :  getReconciledURTypes(invRecList[0].Invoice__r.Reconciliation_Schedule__r.type__c)
                            and Utilization_Report__r.Status__c = 'Approved By Lessor'
                            and Utilization_Report__r.Aircraft__c = :invRecList[0].Invoice__r.Lease__r.Aircraft__c  
                            and For_The_Month_Ending__c >= :invRecList[0].Invoice__r.Reconciliation_Start_Date__c 
                            and For_The_Month_Ending__c <= :invRecList[0].Invoice__r.Reconciliation_End_Date__c   
                            and Assembly_MR_Info__c = :invRecList[0].Reconciliation_line_item__r.Supplemental_Rent__c 
                            and Utilization_Report__r.Y_Hidden_Lease__c = :invRecList[0].Invoice__r.Lease__c
                        ORDER BY End_Date_F__c DESC, Utilization_Report__r.CreatedDate Asc// ascending order (Do not change order otherwise it will ruin true ups handling)
                        ]); 
            
               
            
            if(invRecList[0].Assembly__r.Type__c!=null && (invRecList[0].Assembly__r.Type__c.contains('Engine') || invRecList[0].Assembly__r.Type__c.contains('Propeller'))){
                returnwrapperClass.AssemblyType = 'ENG-PROP';
            }
            if(URList_Actual.values().size()>0){
                PrevFHFCRatio_v = URList_Actual.values()[0].Effective_FH_FC_Ratio__c;
                if(PrevFHFCRatio_v!=null) PrevFHFCRatio_v = LeasewareUtils.setScale(PrevFHFCRatio_v,2,null);
                if(URList_Actual.values()[0].MR_Rate_Esc__c == null || URList_Actual.values()[0].MR_Rate_Esc__c == 0.0){//for records before the Esc the interpolate change uses Interpolated_MR_Rate_F__c field and after the change new field MR_Rate_Esc__c is used.
                    PrevInterpolatedRate_v = leasewareutils.ZeroIfnull(URList_Actual.values()[0].Interpolated_MR_Rate_F__c) * leasewareutils.ZeroIfnull(URList_Actual.values()[0].Effective_Escalation_Factor__c);
                    PrevInterpolatedRate_v = LeasewareUtils.setScale(PrevInterpolatedRate_v,2,null);
                }
                else{
                    PrevInterpolatedRate_v = URList_Actual.values()[0].Assembly_MR_Info__r.ratio_table__c==null?0.0:URList_Actual.values()[0].MR_Rate_Esc__c;
                    PrevInterpolatedRate_v = LeasewareUtils.setScale(PrevInterpolatedRate_v,2,null);
                }

            }
            if(URList_Recon.values().size()>0){
                ReconFHFCRatio_v = URList_Recon.values()[0].Effective_FH_FC_Ratio__c;
                if(ReconFHFCRatio_v!=null) ReconFHFCRatio_v = LeasewareUtils.setScale(ReconFHFCRatio_v,2,null);
                if(URList_Recon.values().MR_Rate_Esc__c == null || URList_Recon.values().MR_Rate_Esc__c == 0.0){
                    ReconInterpolatedRate_v = leasewareutils.ZeroIfnull(URList_Recon.values()[0].Interpolated_MR_Rate_F__c) * leasewareutils.ZeroIfnull(URList_Recon.values()[0].Effective_Escalation_Factor__c);
                    ReconInterpolatedRate_v = LeasewareUtils.setScale(ReconInterpolatedRate_v,2,null);
                }
                else{
                    ReconInterpolatedRate_v = URList_Recon.values()[0].Assembly_MR_Info__r.ratio_table__c==null?0.0:URList_Recon.values()[0].MR_Rate_Esc__c;
                    ReconInterpolatedRate_v = LeasewareUtils.setScale(ReconInterpolatedRate_v,2,null);
                }
            }        
            
            
            map<string,MRInnerClass> mapMRInnerclass = new map<string,MRInnerClass>();
            string keyStr;
            MRInnerClass mrInnerClassTemp;
    
            // MR Original
            for(Utilization_Report_List_Item__c curUR: URList_Actual.values()){
                keyStr = curUR.For_The_Month_Ending__c.format();
                if(mapMRInnerclass.containsKey(keyStr)){
                    mrInnerClassTemp = mapMRInnerclass.get(keyStr);
                }else{
                    mrInnerClassTemp = new MRInnerClass(curUR.For_The_Month_Ending__c);
                }
                mrInnerClassTemp.MROriginal = curUR.Y_Hidden_MR__c;
               // mrInnerClassTemp.MROriginalLLP = curUR.Y_Hidden_MR_LLP__c;
                system.debug('curUR.Id=' + curUR.Id);
                mrInnerClassTemp.MROriginalLink = curUR.Id;
                mapMRInnerclass.put(keyStr,mrInnerClassTemp);
    
            }
            // MRReconcilled
            for(Utilization_Report_List_Item__c curUR: URList_Recon.values()){
                keyStr = curUR.For_The_Month_Ending__c.format();
                if(mapMRInnerclass.containsKey(keyStr)){
                    mrInnerClassTemp = mapMRInnerclass.get(keyStr);
                }else{
                    mrInnerClassTemp = new MRInnerClass(curUR.For_The_Month_Ending__c);
                }
                mrInnerClassTemp.MRReconcilled = curUR.Y_Hidden_MR__c;
              //mrInnerClassTemp.MRReconcilledLLP = curUR.Y_Hidden_MR_LLP__c;
                mrInnerClassTemp.MRReconcilledLink = curUR.Id;
                mapMRInnerclass.put(keyStr,mrInnerClassTemp);
    
            }
            
           
            returnwrapperClass.lstContact = mapMRInnerclass.values();
            returnwrapperClass.MROriginalSUM = invRecList[0].MR_Original__c; 
            //returnwrapperClass.MROriginalLLPSUM = invRec.MR_Original_LLP_If_Applicable__c; 
            returnwrapperClass.MRReconcilledSUM = invRecList[0].MR_Reconciled__c;
            //returnwrapperClass.MRReconcilledLLPSUM = invRec.MR_Reconciled_LLP_If_Applicable__c;
            returnwrapperClass.DifferenceSUM =  invRecList[0].Total_Line_Amount__c;
            returnwrapperClass.PrevFHFCRatio = PrevFHFCRatio_v;
            returnwrapperClass.PrevInterpolatedRate = PrevInterpolatedRate_v;
            returnwrapperClass.ReconFHFCRatio = ReconFHFCRatio_v;
            returnwrapperClass.ReconInterpolatedRate = ReconInterpolatedRate_v;
            system.debug('initMethod-');
        }
        
        return returnwrapperClass;          
    }
    
    // wrapper or Inner class with @AuraEnabled {get;set;} properties*    
    public class wrapperClass{
        @AuraEnabled public List<MRInnerClass> lstContact{get;set;}
        
        @AuraEnabled public Double MROriginalSUM{get;set;}
        //@AuraEnabled public Double MROriginalLLPSUM{get;set;}
        @AuraEnabled public Double MRReconcilledSUM{get;set;}
        //@AuraEnabled public Double MRReconcilledLLPSUM{get;set;}
        @AuraEnabled public Double DifferenceSUM{get;set;}
        @AuraEnabled public Double PrevFHFCRatio{get;set;}
        @AuraEnabled public Double PrevInterpolatedRate{get;set;}
        @AuraEnabled public Double ReconFHFCRatio{get;set;}
        @AuraEnabled public Double ReconInterpolatedRate{get;set;}
        @AuraEnabled public string AssemblyType{get;set;}
        @AuraEnabled public String mrReconciledAmountLabel {get;set;}
        @AuraEnabled public String mrOriginalAmountLabel {get;set;}
        @AuraEnabled public String reconciliationType{get;set;}
    }
    
    public class MRInnerClass{
        Date forPeriodEnding{get;set;}
        @AuraEnabled public string forPeriodEndingStr{get;set;}
        @AuraEnabled public string MROriginalLink{get;set;}
        @AuraEnabled public Double MROriginal{get;set;}
        //@AuraEnabled public Double MROriginalLLP{get;set;}
        @AuraEnabled public string MRReconcilledLink{get;set;}
        @AuraEnabled public Double MRReconcilled{get;set;}
        //@AuraEnabled public Double MRReconcilledLLP{get;set;}
        @AuraEnabled public Double getDifference(){
            return leasewareutils.ZeroIfNull(MRReconcilled) - leasewareutils.ZeroIfNull(MROriginal);
        }
        
        MRInnerClass(Date forPeriodEnding){
            this.forPeriodEnding=forPeriodEnding;
            forPeriodEndingStr = leasewareUtils.getDatetoString(forPeriodEnding,'MON YYYY');
        }
    }    
    
    
}