public with sharing class AircraftController {

    private final Aircraft__c parentAC;
    private List<Aircraft_Eligible_Event__c> ACEligible_Event= new List<Aircraft_Eligible_Event__c>();
    
    private List<Equipment__c> Equipment = new List<Equipment__c>();
    
    public List<Constituent_Assembly__c> ConstAssemb = new List<Constituent_Assembly__c>();
    public List<Constituent_Assembly__c> ConstAssembEngine = new List<Constituent_Assembly__c>();
    public List<Constituent_Assembly__c> ConstAssembGear = new List<Constituent_Assembly__c>();
    public List<Constituent_Assembly__c> ConstAssembApu = new List<Constituent_Assembly__c>();
     
   
    private final Lease__c Lease;
    private final Operator__c Operator;
    
    private ID parentAC_ID; 
    private ID ACEligible_Event_ID;
    private ID Lease_ID;
    private ID Lessor_ID;
    private ID OperatorID;
    
    public string Logo_URL;
    public String email {get;set;}
    public String sub {get;set;}
    public String ebody {get;set;}
     
    public AircraftController () {
    
        Id id = ApexPages.currentPage().getParameters().get('id');
        
        Logo_URL =  LeaseWareUtils.getLogoURL();
        
        
       
  
              
        List<Aircraft__c>   Ac_spec = [select id,Logo__c, Aircraft_Type__c, Assigned_To__c, Country_Of_Registration__c, 
                                                  CSN__c, Lease__c,Approach_Landing_Cat__c, 
                                                  Number_of_engines__c, Registration_Number__c,ETOPS__c,Line_Num__c, 
                                                  Time_Remaining_To_Check__c,TSN__c, Aircraft_Variant__c,Date_of_Manufacture__c,
                                                  Auxiliary_Tank_Capacity_Gallons__c, Auxiliary_Tank_Capacity_Kilograms__c, 
                                                  Auxillary_Tank_Capacity_Pounds__c, Maximum_Landing_Weight_Max__c,
                                                  Maximum_Landing_Weight_Operational__c, Maximum_Landing_Wt_Purchased_Leased__c,
                                                  Maximum_Operational_Take__c, Maximum_Take_Off_Wt_Max_Design__c,
                                                  Maximum_Take_Off_Weight_Operational__c, Maximum_Zero_Fuel_Wt_Max_Design__c,
                                                  Maximum_Zero_Fuel_Wt_Operational__c, Maximum_Zero_Fuel_Wt_Purchased_Leased__c,
                                                  Standard_Fuel_Capacity_Gallons__c, Standard_Fuel_Capacity_KG__c, 
                                                  Standard_Fuel_Capacity_Pounds__c, MSN_Number__c 
                                             from Aircraft__c where id =  :ApexPages.currentPage().getParameters().get('id')];
                                                                                                          
       if (Ac_spec.size() > 0)
            {
                parentAC = Ac_spec.get(0);
                parentAC_ID = parentAC.id;
                
                Lease_ID = parentAC.Lease__c;
                              
            }
            
      
                  
       List<Lease__c>   Lease_List = [select id, Operator__c 
                                           from Lease__c where id =  :Lease_ID];
        
       if (Lease_List.size() > 0)
            {
                Lease = Lease_List.get(0);
                OperatorID = Lease.Operator__c;
                
            }  
                
      List<Operator__c>   OperatorList = [select id, Country__c,Name,Base_Of_Operations__c
                                           from Operator__c  where id =  :OperatorID];  
     
      if(OperatorList.size() > 0)
            {
               Operator = OperatorList.get(0);
             }
             
     ConstAssembEngine = getConstitutenAssemblyEngine(parentAC); 
     
     ConstAssembGear = getConstitutenAssemblyGear(parentAC);
     
     ConstAssembApu = getConstitutenAssemblyApu(parentAC);
     
     ACEligible_Event = getAircraft_Eligible_Event(parentAC);
     
     Equipment = getEquipment(parentAC);
     
     
   }
   
   
    
    public Aircraft__c getparentAC() {
        return parentAC;
    }
    
     public Lease__c getLease() {
        return Lease;
    }
    
     public Operator__c getOperator() {
        return Operator;
    }
    
   public List<Aircraft_Eligible_Event__c> getAircraft_Eligible_Event(Aircraft__c aircraft)
    {
        List<Aircraft_Eligible_Event__c> Eligible_Event= new List<Aircraft_Eligible_Event__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            Eligible_Event = [select id, Event_Type__c, CSN__c, End_Date__c, Start_Date__c, TSN_N__c from 
                           Aircraft_Eligible_Event__c where Aircraft__c = :aircraftID  
                           Order By LastModifiedDate Desc];  
                           
        }  
           
        return Eligible_Event;
    }
    
    public List<Aircraft_Eligible_Event__c> getACEligible_Event() {
        return ACEligible_Event;
    }
    
     public List<Equipment__c> getEquipment(Aircraft__c aircraft)
    {
        List<Equipment__c> Equipment = new List<Equipment__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;

            Equipment = [select id, ATA_Text__c, Description__c, Part_Number__c, Serial_Number__c, Vendor__c, 
            Family__c, Quantity__c 
            from Equipment__c where Aircraft__c = :aircraftID 
                           Order By ATA_Text__c Asc];  
// AND (ATA_Text__c = '21' or ATA_Text__c = '22' or ATA_Text__c = '23' or ATA_Text__c = '31'  or ATA_Text__c = '34' )
                           
        }  
           
        return Equipment;
    }
    
    public string getLogo_URL()
    {
      
        return Logo_URL;
    }
    
       
    public List<Equipment__c> getEquipment(){
        return Equipment;
    }
    
  
    
    
    public List<Constituent_Assembly__c> getConstitutenAssemblyEngine(Aircraft__c aircraft)
    {
        List<Constituent_Assembly__c> constAssemblies = new List<Constituent_Assembly__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            constAssemblies = [select id,name,Part_Number__c,Serial_Number__c, Model__c, Type__c, CSN__c, TSN__c, CSLV__c, TSLV__c, Life_Limit__c from 
                           Constituent_Assembly__c where Attached_Aircraft__c = :aircraftID AND
                           (Type__c = 'Engine 1' or Type__c = 'Engine 2')Order By LastModifiedDate Desc];  
                           
        }  
                 
       List<Constituent_Assembly__c> constAssembliesToReturn = new List<Constituent_Assembly__c>();
        Set<String> setType = new Set<String>();
        for(Constituent_Assembly__c obj : constAssemblies)
             {
                if(setType.add(obj.Type__c))
                     {
                         constAssembliesToReturn.add(obj);
                     }
             }
               
        return constAssembliesToReturn;
    }
    
    public List<Constituent_Assembly__c> getConstitutenAssemblyGear(Aircraft__c aircraft)
    {
        List<Constituent_Assembly__c> constAssemblies = new List<Constituent_Assembly__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            constAssemblies = [select id,name,Part_Number__c,Serial_Number__c, Model__c, Type__c, CSN__c, TSN__c, CSLV__c, TSLV__c, Life_Limit__c from 
                           Constituent_Assembly__c where Attached_Aircraft__c = :aircraftID AND
                           (Type__c = 'Landing Gear - Center' or Type__c = 'Landing Gear - Left Main'
                             or Type__c = 'Landing Gear - Right Main' or Type__c = 'Landing Gear - Nose' or Type__c = 'Landing Gear - Left Wing'
                             or Type__c = 'Landing Gear - Right Wing')
                             Order By LastModifiedDate Desc];  
   
        }           
        List<Constituent_Assembly__c> constAssembliesToReturn = new List<Constituent_Assembly__c>();
        Set<String> setType = new Set<String>();
        for(Constituent_Assembly__c obj : constAssemblies)
             {
                if(setType.add(obj.Type__c))
                     {
                         constAssembliesToReturn.add(obj);
                     }
             }
               
        return constAssembliesToReturn;
    }
   
   public List<Constituent_Assembly__c> getConstitutenAssemblyApu(Aircraft__c aircraft)
    {
        List<Constituent_Assembly__c> constAssemblies = new List<Constituent_Assembly__c>();
       if (aircraft!=null)
        {
            Id aircraftID = aircraft.ID;
            constAssemblies = [select id,name,Part_Number__c,Serial_Number__c, Model__c, Type__c, CSN__c, TSN__c, CSLV__c, TSLV__c, Life_Limit__c from 
                           Constituent_Assembly__c where Attached_Aircraft__c = :aircraftID AND
                           Type__c = 'APU' ];  
                           
        }  
                 
       List<Constituent_Assembly__c> constAssembliesToReturn = new List<Constituent_Assembly__c>();
        Set<String> setType = new Set<String>();
        for(Constituent_Assembly__c obj : constAssemblies)
             {
                if(setType.add(obj.Type__c))
                     {
                         constAssembliesToReturn.add(obj);
                     }
             }
               
        return constAssembliesToReturn;
    } 
    
   public List<Constituent_Assembly__c> getConstAssembEngine(){
   
    return ConstAssembEngine;
   }
   
   public List<Constituent_Assembly__c> getConstAssembGear(){
       return ConstAssembGear;
   } 
   
   public List<Constituent_Assembly__c> getConstAssembApu(){
       return ConstAssembApu;
   }  
   
    
    static testMethod void testACController() {
    	map<String,Object> mapInOut = new map<String,Object>();
    	TestLeaseworkUtil.createLessor(mapInOut);        
        
               // Set up the Operator record.
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        LeaseWareUtils.clearFromTrigger();
        insert operator;
        
        // Set up the Lease record
        //Date dateToday = Date.parse('01/30/2009');
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10', Lease_Start_Date__c=Date.parse('01/01/2010'), Lease_Start_Date_New__c=Date.parse('01/01/2010'), Lease_End_Date__c=Date.parse('12/31/2012'),Lease_End_Date_New__c=Date.parse('12/31/2012'), Rent_Date__c=Date.parse('01/01/2010'), Rent_End_Date__c=Date.parse('12/31/2012'));
        LeaseWareUtils.clearFromTrigger();
        insert lease;
        // Set up the Aircraft record.
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN,Date_of_Manufacture__c = System.today(), Aircraft_Type__c='737', Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, Threshold_for_C_Check__c=1000, Status__c='Available');
        LeaseWareUtils.clearFromTrigger();
        insert a;
        system.debug('Before update lease');
        lease.aircraft__c=a.id;
        LeaseWareUtils.clearFromTrigger();
        update lease;
        
         Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');    
        Aircraft_Eligible_Event__c assEE= new Aircraft_Eligible_Event__c(Name= 'ca test', 
        Aircraft__c=a.Id, Start_Date__c=dateStart, End_Date__c=dateEnd, InitClaimAmt_MR_Engine_1__c=1000000
        	,TSN_N__c = 100, CSN__c = 100,Event_Type__c='Heavy Maintenance 1');
        		LeaseWareUtils.clearFromTrigger();
                insert assEE;
                Aircraft_Eligible_Event_Invoice__c invoice = new Aircraft_Eligible_Event_Invoice__c(Invoice_Date__c=Date.parse('02/12/2009'), Aircraft_Eligible_Event__c = assEE.Id);
                LeaseWareUtils.clearFromTrigger(); insert invoice;
        
        
        System.debug('***************************************8 AC is '+a);
        ApexPages.currentPage().getParameters().put('id', a.Id);
        AircraftController controller = new AircraftController();
        controller.getEquipment(a);
        controller.getLease();
        controller.getConstitutenAssemblyEngine(a);
        controller.getOperator();
        controller.getEquipment();
        controller.getEquipment(a);
        controller.getACEligible_Event();
        controller.getAircraft_Eligible_Event(a);
        controller.getConstitutenAssemblyApu(a);
        controller.getConstitutenAssemblyGear(a);
        controller.getConstAssembEngine();
        controller.getConstAssembGear();
        controller.getConstAssembApu();
        controller.getparentAC();
        controller.getLogo_URL();
        controller.sub = '';
        controller.ebody = '';
        
        //controller.sendPdf();
        try{
        System.assertEquals(controller.sendPdf(), null);
        }catch(exception e){
            System.debug('Expected.. Ignore');
        }    
          
    }
   
    public PageReference sendPdf() {
     
    PageReference pdf = Page.ACspecsheet;
    // add parent id to the parameters for standardcontroller
    pdf.getParameters().put('id',parentAC.id);
 
    // the contents of the attachment from the pdf
    Blob body;
 
    // returns the output of the page as a PDF
    body = !Test.isRunningTest() ? pdf.getContent() : Blob.ValueOf('dummy text');
    
 
    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
    attach.setContentType('application/pdf');
    attach.setFileName('AcSpecSheet.pdf');
    attach.setInline(false);
    attach.Body = body;
 
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setUseSignature(false);

    try{
        String[] mailAddresses =email.deleteWhitespace().split('[,; ]',0); 
        mail.setToAddresses(mailAddresses);
    
        mail.setSubject(sub);
        mail.setplainTextBody(ebody);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
     
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }catch(exception e){
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Email could not be sent. Invalid email address. Multiple emails can be separated by comma or semicolon.'+email));
        return null;
    }
 
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with spec sheet sent to '+email));
 
    return null;
 
  }

}