/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMaintenanceProgramEventHandler {
	public static final String C_LG_M = 'Landing Gear - Main' ;   
    static testMethod void testMPAfterAssociatedWithAircraft()  {
    	
    	map<String,Object> mapInOut = new map<String,Object>();
    	mapInOut.put('Maintenance_Program__c.Historical_Utilization_Months__c',10) ;
    	mapInOut.put('Maintenance_Program__c',null);
    	TestLeaseworkUtil.createMP(mapInOut); 
    	Maintenance_Program__c mp1= (Maintenance_Program__c)mapInOut.get('Maintenance_Program__c');
		Id MPId = (Id)mapInOut.get('Maintenance_Program__c.Id');
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Engine') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
		mapInOut.put('Maintenance_Program_Event__c',null);
    	TestLeaseworkUtil.createMPDetail(mapInOut); 
    	Maintenance_Program_Event__c mpe1= (Maintenance_Program_Event__c)mapInOut.get('Maintenance_Program_Event__c');
    	
    	TestLeaseworkUtil.createLessor(mapInOut);
    	mapInOut.put('Aircraft__c',null);
    	TestLeaseworkUtil.insertAircraft(mapInOut); 
		
	    LeaseWareUtils.clearFromTrigger();
	        	
    	Aircraft__c aircraftRec1 =  (Aircraft__c)  mapInOut.get('Aircraft__c'); 
    	aircraftRec1.Maintenance_Program__c = 	MPId;
    	system.debug('aircraftRec1.Maintenance_Program__c'+ aircraftRec1.Maintenance_Program__c);
    	update aircraftRec1;
    	
        
	    LeaseWareUtils.clearFromTrigger();

       
        Test.startTest(); 
		// modify MP : expected errror
			mp1.Historical_Utilization_Months__c = 20;
			try{
				update mp1;
			}catch(System.DmlException e){
        		System.debug('Expected failure 1: testMPAfterAssociatedWithAircraft   ' + e);
        		System.assertEquals(e.getDmlMessage(0).contains('This Maintenance Program is already associated with Aircraft.You are not allowed to modify this record'),true);
    					
			}
			
		// modify MPE : expected errror
			mpe1.Never_Exceed_Period_Cycles__c = 200;
			try{
				update mpe1;
			}catch(System.DmlException e){
        		System.debug('Expected failure 2: testMPAfterAssociatedWithAircraft   ' + e);
        		System.assertEquals(e.getDmlMessage(0).contains('This Maintenance Program is already associated with Aircraft.You are not allowed to modify this record'),true);
    					
			}    	
    	Test.stopTest();

    }     
    
     static testMethod void testMPCRUDOperation()  {
     	
     	map<String,Object> mapInOut = new map<String,Object>();
    	mapInOut.put('Maintenance_Program__c.Historical_Utilization_Months__c',10) ;
    	mapInOut.put('Maintenance_Program__c',null);
    	TestLeaseworkUtil.createMP(mapInOut); 
    	Maintenance_Program__c mp1= (Maintenance_Program__c)mapInOut.get('Maintenance_Program__c');
		Id MPId = (Id)mapInOut.get('Maintenance_Program__c.Id');
		mapInOut.put('Maintenance_Program_Event__c.Assembly__c','Engine') ;// APU , Landing Gear
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Cycles__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Hours__c',2000); 
		mapInOut.put('Maintenance_Program_Event__c.Never_Exceed_Period_Months__c',50);
		mapInOut.put('Maintenance_Program_Event__c.Event_Cost__c',20000);
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Cycles__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Hours__c',3000) ;
		//mapInOut.put('Maintenance_Program_Event__c.Interval_2_Months__c',40);
		mapInOut.put('Maintenance_Program_Event__c',null);
    	TestLeaseworkUtil.createMPDetail(mapInOut); 
    	Maintenance_Program_Event__c mpe1= (Maintenance_Program_Event__c)mapInOut.get('Maintenance_Program_Event__c');
 
 
 		Test.startTest(); 
 			
	    	LeaseWareUtils.clearFromTrigger();
	    	// update 
 			mp1.Historical_Utilization_Months__c = 20;
 			update mp1;
			mpe1.Never_Exceed_Period_Cycles__c = 200;
			update mpe1; 			
 			
         	
	    	LeaseWareUtils.clearFromTrigger();	
	    	// delete undelete		
 			delete  mpe1;
         	
	    	LeaseWareUtils.clearFromTrigger(); 			
 			undelete  mpe1; 			

         	
	    	LeaseWareUtils.clearFromTrigger();	
	    	// delete undelete		
 			delete  mp1;
         	
	    	LeaseWareUtils.clearFromTrigger(); 			
 			undelete  mp1; 	
 		Test.stopTest();

     }
     
     static testMethod void MPD_Catalog_CRUD_Test()  {
     	
     	map<String,Object> mapInOut = new map<String,Object>();

    	
		Maintenance_Program__c newMP = new Maintenance_Program__c(Name='Dummy'
            ,Aircraft_Type__c = null, Historical_Utilization_Months__c = 10, Variant__c = null,Current_Catalog_Year__c='2020');
        
        LeaseWareUtils.clearFromTrigger();insert  newMP;		

		list<Maintenance_Program_Event__c> insListMPE = new list<Maintenance_Program_Event__c>();
        string AssemblyVar = null,eventTypeVar;
        Decimal int1Hours,int1Cycle,int1Period,int2Hours,int2Cycle,int2Period,eventCost;
		
		eventTypeVar = 'Overhaul';
		AssemblyVar = C_LG_M;
		int1Hours = 2000;int1Cycle =1000;int1Period =30;eventCost=20000;
		insListMPE.add(new Maintenance_Program_Event__c(
            Assembly__c = AssemblyVar, Clearance_Period_Critreia__c = null, Event_Cost__c = eventCost
            ,Event_Type__c = eventTypeVar , Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            ,Maintenance_Program__c = newMP.id, Event_Type_Global__c=eventTypeVar));

		AssemblyVar = 'APU';eventTypeVar = 'Performance Restoration';
		insListMPE.add(new Maintenance_Program_Event__c(
            Assembly__c = AssemblyVar, Clearance_Period_Critreia__c = null, Event_Cost__c = eventCost
            ,Event_Type__c = eventTypeVar , Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            ,Maintenance_Program__c = newMP.id, Event_Type_Global__c=eventTypeVar));

		LeaseWareUtils.clearFromTrigger();insert  insListMPE;		
		
 
 		
 
 		Test.startTest();
 			list<Maintenance_Program_Event_Catalog__c> insListMPDCatalog = new list<Maintenance_Program_Event_Catalog__c>();
			for(Maintenance_Program_Event__c curRec   :insListMPE){
				insListMPDCatalog.add(new Maintenance_Program_Event_Catalog__c(name='2015',Maintenance_Program_Event__c=curRec.Id
						,Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            			));
				insListMPDCatalog.add(new Maintenance_Program_Event_Catalog__c(name='2016',Maintenance_Program_Event__c=curRec.Id
						,Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            			));            			
			}
			
			LeaseWareUtils.clearFromTrigger();insert  insListMPDCatalog;	
			LeaseWareUtils.clearFromTrigger();update 	insListMPDCatalog;
			newMP.Current_Catalog_Year__c ='2015';
			LeaseWareUtils.clearFromTrigger();update 	newMP;
			LeaseWareUtils.clearFromTrigger();delete 	insListMPDCatalog;
			
 		Test.stopTest();

     }
     
     @isTest(seeAllData=true)
     static  void MPD_AddAC_Test()  {
     	
     	map<String,Object> mapInOut = new map<String,Object>();

    	
		Maintenance_Program__c newMP = new Maintenance_Program__c(Name='Dummy'
            ,Aircraft_Type__c = null, Historical_Utilization_Months__c = 10, Variant__c = null,Current_Catalog_Year__c ='2015');
        
        LeaseWareUtils.clearFromTrigger();insert  newMP;		

		list<Maintenance_Program_Event__c> insListMPE = new list<Maintenance_Program_Event__c>();
        string AssemblyVar = null,eventTypeVar;
        Decimal int1Hours,int1Cycle,int1Period,int2Hours,int2Cycle,int2Period,eventCost;
		
		eventTypeVar = 'Overhaul';
		AssemblyVar = C_LG_M;
		int1Hours = 2000;int1Cycle =1000;int1Period =30;eventCost=20000;
		insListMPE.add(new Maintenance_Program_Event__c(
            Assembly__c = AssemblyVar, Clearance_Period_Critreia__c = null, Event_Cost__c = eventCost
            ,Event_Type__c = eventTypeVar , Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            ,Maintenance_Program__c = newMP.id, Event_Type_Global__c=eventTypeVar));

		AssemblyVar = 'APU';eventTypeVar = 'Performance Restoration';
		insListMPE.add(new Maintenance_Program_Event__c(
            Assembly__c = AssemblyVar, Clearance_Period_Critreia__c = null, Event_Cost__c = eventCost
            ,Event_Type__c = eventTypeVar , Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            ,Maintenance_Program__c = newMP.id, Event_Type_Global__c=eventTypeVar));

		LeaseWareUtils.clearFromTrigger();insert  insListMPE;		
		
 
 		
 
 		Test.startTest();
 			list<Maintenance_Program_Event_Catalog__c> insListMPDCatalog = new list<Maintenance_Program_Event_Catalog__c>();
			for(Maintenance_Program_Event__c curRec   :insListMPE){
				insListMPDCatalog.add(new Maintenance_Program_Event_Catalog__c(name='2015',Maintenance_Program_Event__c=curRec.Id
						,Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            			));
				insListMPDCatalog.add(new Maintenance_Program_Event_Catalog__c(name='2016',Maintenance_Program_Event__c=curRec.Id
						,Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
            			));            			
			}
			
			LeaseWareUtils.clearFromTrigger();insert  insListMPDCatalog;	

	    	Aircraft__c[] AClist = [select id,lease__c from Aircraft__c where name like 'TestSeededMSN1%' limit 1];    	
    	
    		AClist[0].Maintenance_Program__c = 	newMP.Id;
    	
    		LeaseWareUtils.clearFromTrigger();update AClist[0];
			newMP.Current_Catalog_Year__c ='2016';
			LeaseWareUtils.clearFromTrigger();update 	newMP;    		
    		
			
 		Test.stopTest();

     }          
          
}