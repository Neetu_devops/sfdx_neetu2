/*
This helper class holds the utility methods needed for asset marketing portal.
Defined the class as without sharing since the Lessee should be able to query for the records
irrespective of the access.
*/
public without sharing class PortalAssetHelper {
   

    private static final String C_SV = 'SV' ;
    private static final String C_USED = 'USED' ;

    /*
    This method searches the system based on the request parameters
    STEP 1: Fetch the assets that have the input asset type and asset variant
    STEP 2: Get all the projected events of the constituent assemblies
    STEP 3: Check the asset availability based on the lease dates
    STEP 4: Check the availability based on the event due date set on the projected event for the assembly
    STEP 5: Create the portal asset selected objects
    */
    public static List<AssetPortalController.PortalAssetSelectedWrapper> searchAssets(Portal_Asset_Request__c request)
    {   
        List<AssetPortalController.PortalAssetSelectedWrapper> lstPAS = new List<AssetPortalController.PortalAssetSelectedWrapper>();
        map<id, Aircraft__c> mapACs;
        map<id, List<Lease__c>> mapActiveAndFutureLeases = new map<id, List<Lease__c>>(); //Key - AC Id, Value - List of active and futute leases

        //STEP 1: Fetch the assets that have the input asset type and asset variant
        if(request.Asset_Variant__c == null)
        {
            mapACs = new map<id, Aircraft__c>([select id, Name, Location__c,(select id, Engine_Template_CA__c, Remaining_Life_FH_Calculated__c,Remaining_Life_FC_Calculated__c from Constituent_Assemblies__r where RecordType.DeveloperName = 'Engine'),
            (select id, Active__c, Redelivery_Date__c, Lease_Start_Date_New__c, Lease_End_Date_New__c from Leases__r order by Lease_Start_Date_New__c) from Aircraft__c where Aircraft_Type__c = :request.Asset_Type__c]);
        }
        else 
        {
            mapACs = new map<id, Aircraft__c>([select id, Name,Location__c,(select id, Engine_Template_CA__c, Remaining_Life_FH_Calculated__c,Remaining_Life_FC_Calculated__c from Constituent_Assemblies__r where RecordType.DeveloperName = 'Engine'),
            (select id, Active__c, Redelivery_Date__c, Lease_Start_Date_New__c, Lease_End_Date_New__c from Leases__r order by Lease_Start_Date_New__c) from Aircraft__c where Aircraft_Type__c = :request.Asset_Type__c and Aircraft_Variant__c = :request.Asset_Variant__c]);
        }

        System.debug('Number of ACs ==' + mapACs.size());

        if(mapACs.size() == 0) return null;

        for(Aircraft__c tempAC: mapACs.values())
        {   
            List<lease__c> tempACLease = new List<Lease__c>();
            for(Lease__c AClease: tempAC.Leases__r)
            {
                if(AClease.Active__c == true || (AClease.Active__c == false && AClease.Lease_Start_Date_New__c > System.Today()))
                {
                    tempACLease.add(AClease);
                }
            }
            mapActiveAndFutureLeases.put(tempAC.id, tempACLease);
        }

        System.debug('AC to Lease map--' + mapActiveAndFutureLeases);

        List<ContentDocumentLink> lstFiles = [SELECT ContentDocumentId, Id, LinkedEntityId, ContentDocument.Title FROM ContentDocumentLink WHERE LinkedEntityId in :mapACs.keyset() and ContentDocument.Title like '%Spec%'];

        List<User> userList = [select contact.accountid from user where id =:UserInfo.getUserId()];
        Id accountId = userList[0].contact.accountid; //Null for internal users, populated for lessee requests

        //Check if there are any PAS records previously requested for the same asset whose request status is 'Open'
        List<Portal_Asset_Selected__c> lstOldPAS;
        if(accountId != null)
        {
            lstOldPAS = [select id,Asset__c, Portal_Asset_Request__r.Date_End__c, Portal_Asset_Request__r.Date_Start__c from Portal_Asset_Selected__c 
            where Asset__c in :mapACs.keyset() and Portal_Asset_Request__r.Status__c = 'Open' and Portal_Asset_Request__r.Account__c = :accountId order by Asset__c, Portal_Asset_Request__r.Date_Start__c];
        }
        
        //STEP 2: Get all the projected events of the constituent assemblies
        set<Id> setAssyIds = new set<Id>();
        for(Aircraft__c curAC: mapACs.values()){
            if(curAC.Constituent_Assemblies__r.size() > 0){
                System.debug('Engine Assembly==' + curAC.Constituent_Assemblies__r[0].id);
                setAssyIds.add(curAC.Constituent_Assemblies__r[0].id);
            }
        }

        map<Id, Constituent_Assembly__c> mapAssy;
        if(setAssyIds.size() > 0) 
        {
            mapAssy = new map<id, Constituent_Assembly__c>([select id, Lease_ID__c, Engine_Template_CA__c, Remaining_Life_FH_Calculated__c,Remaining_Life_FC_Calculated__c,(select id, Last_Event_Date__c, Event_Due_Date__c, Life_Remaining_Cycles__c, Downtime_Months_F__c,Constituent_Assembly__c from Assembly_Event_Info__r) from Constituent_Assembly__c where id in :setAssyIds]);
        }
        
        Integer tolerance = Integer.valueOf(LeaseWareUtils.zeroIfNull(request.Lease_Term_Tolerance__c));
        Date requestStartDate = request.Date_Start__c;
        Date requestEndDate = request.Date_End__c;
        
        System.debug('Start Date==' + requestStartDate + 'End Date==' + requestEndDate);
        for(Aircraft__c curAC: mapACs.values())
        {   
            //STEP 3: Check the asset availability based on the lease dates            
            Integer numOfLeases = mapActiveAndFutureLeases.get(curAC.id).size();
            Boolean isAvailable = false;
            Date availableDateBasedOnLease, afterSVDate, latestLastEventDate;
            List<Date> lstEligibleStartDates = new List<Date>();
            System.debug('Checking for Asset==' + curAC.Name);
            System.debug('Number of leases ==' + numOfLeases);
            if(numOfLeases > 0)
            {                   
                if(checkRequestEndDateWithTolerance(requestStartDate, requestEndDate, tolerance, mapActiveAndFutureLeases.get(curAC.id)[0].Lease_Start_Date_New__c, lstEligibleStartDates))                    
                {   
                    System.debug('Request end date is prior to the first lease start date.');
                    availableDateBasedOnLease = System.Today();                    
                    isAvailable = true;
                }
                else if(CheckRequestStartDateWithTolerance(requestStartDate, tolerance, getLeaseEndDate(mapActiveAndFutureLeases.get(curAC.id)[numOfLeases -1]), lstEligibleStartDates))                 
                {   
                    System.debug('Request start date is after the last lease end date.');
                    availableDateBasedOnLease = getLeaseEndDate(mapActiveAndFutureLeases.get(curAC.id)[numOfLeases -1]);
                    isAvailable = true;
                }                  
                if(numOfLeases > 1 && !isAvailable) //Multi-lease case
                {    
                    //Check if the request period is between the lease end date and next lease's start date
                    for(Integer idx = 1; idx < numOfLeases; idx++)
                    {   
                        System.Debug('Checking between the dates==' + getLeaseEndDate(mapActiveAndFutureLeases.get(curAC.id)[idx-1])+ '==' + mapActiveAndFutureLeases.get(curAC.id)[idx].Lease_Start_Date_New__c);                        
                        for(Integer tolIdx =-tolerance; tolIdx <=tolerance; tolIdx++)
                        {
                            if(requestStartDate.adddays(tolIdx) >= getLeaseEndDate(mapActiveAndFutureLeases.get(curAC.id)[idx-1]) && requestEndDate.adddays(tolIdx) <= mapActiveAndFutureLeases.get(curAC.id)[idx].Lease_Start_Date_New__c)
                            {   
                                System.debug('Available for the period--' + requestStartDate.addDays(tolIdx) + '  TO ' + requestEndDate.addDays(tolIdx));
                                
                                isAvailable = true;
                                availableDateBasedOnLease = getLeaseEndDate(mapActiveAndFutureLeases.get(curAC.id)[idx-1]);
                                lstEligibleStartDates.add(requestStartDate.adddays(tolIdx));                                                          
                            }
                        }
                        
                        if(isAvailable) break;
                    }
                }
            }
            else 
            {                
                isAvailable = true;
                availableDateBasedOnLease = System.Today();
                for(Integer idx = -tolerance; idx <= tolerance; idx++)
                {               
                    lstEligibleStartDates.add(requestStartDate.addDays(idx));                       
                }    
            }

            System.Debug('Final Availability based on lease check==' + isAvailable + '== Lease Available Date==' + availableDateBasedOnLease);

            if(!isAvailable) continue;

            //STEP 4: Check the availability based on the event due date set on the projected event for the assembly
            
            //Check if the asset would be under maintenance during the start of the lease            
            Constituent_Assembly__c assembly;
            if(curAC.Constituent_Assemblies__r.size() > 0)
            {
                assembly = mapAssy.get(curAC.Constituent_Assemblies__r[0].id);

                //If we find atleast one date range which does not overlap with the projected events , the asset is available 
                System.debug('List of eligible start dates==' + lstEligibleStartDates);
                for(Date eligibleStartDate : lstEligibleStartDates)
                {   
                    System.debug('Checking for start date=' + eligibleStartDate);
                    afterSVDate = null;                                        
                    for(Assembly_Event_Info__c tempProjEvnt: assembly.Assembly_Event_Info__r)
                    {   
                        if(latestLastEventDate == null || tempProjEvnt.Last_Event_Date__c > latestLastEventDate)
                        {
                            latestLastEventDate = tempProjEvnt.Last_Event_Date__c;
                        }

                        if(tempProjEvnt.Event_Due_Date__c == null) continue;
                        if(tempProjEvnt.Event_Due_Date__c >= eligibleStartDate) continue;

                        //There is an event between the available date to proposed start date. Add downtime and check if it exceeds the start date
                        System.debug('There is a projected event after the available date--' + tempProjEvnt.Event_Due_Date__c);
                        Integer downTimeMos = LeasewareUtils.zeroIfNull(tempProjEvnt.Downtime_Months_F__c) == 0? 1: Integer.valueof(tempProjEvnt.Downtime_Months_F__c);
                        Date afterDownTimeDate = tempProjEvnt.Event_Due_Date__c.addMonths(downTimeMos);                                                       
                        
                        if(afterDownTimeDate < availableDateBasedOnLease) continue;
                        
                        if(afterDownTimeDate <= eligibleStartDate)
                        {
                            isAvailable = true;                                
                            if(afterSVDate == null || afterDownTimeDate > afterSVDate)
                            {
                                afterSVDate = afterDownTimeDate;
                            }  
                        } 
                        else               
                        {
                            isAvailable = false;
                            afterSVDate = null;
                            break;
                        }                                                
                    }

                    System.debug('Availability for start date=' + eligibleStartDate + ' is ' + isAvailable + '  SV Date=' + afterSVDate);
                    if(isAvailable) break;
                }
            }//End If (assysize > 0)

            System.Debug('Final Availability based on projected event check==' + isAvailable + '==SV Available Date==' + afterSVDate + '==LatestLastEventDate' + latestLastEventDate);
            if(isAvailable)
            {                
                //STEP 5: Create the portal asset selected objects
                String engMaintCondn = (afterSVDate != null) ? C_SV: getEngineMaintCondition(availableDateBasedOnLease, latestLastEventDate, curAC.Leases__r);
                String engAvailability = getEngineCurrAvailability(assembly,mapActiveAndFutureLeases.get(curAC.id));

                Decimal leaseTerm = (request.Date_Start__c.daysbetween(request.Date_End__c)/30.0).setscale(1, System.RoundingMode.HALF_UP);

                Id assyId = (assembly == null)?null:assembly.id;

                Portal_Asset_Selected__c pas = new Portal_Asset_Selected__c(Name = curAC.Name, Available_Date__c = (afterSVDate == null ? availableDateBasedOnLease:afterSVDate),
                Status__c = 'Under Review', Asset__c = curAC.id, Assembly__c = assyId, Requested__c = true, Rent_Up_To_Unit__c = request.Rent_Up_To__c, Lease_Term_MO__c = leaseTerm,
                Rent_Up_To_Amount__c = request.Rent_Up_To_Amount__c, Engine_Condition__c = engAvailability+'/'+engMaintCondn);

                boolean assetHasSpecSheet = false;
                for(ContentDocumentLink file: lstFiles)
                {
                    if(file.LinkedEntityId == curAC.id)
                    {
                        assetHasSpecSheet = true;
                        break;
                    }
                }

                AssetPortalController.PortalAssetSelectedWrapper assetWrapper = new AssetPortalController.PortalAssetSelectedWrapper(pas, assetHasSpecSheet, hasHalfLifeRemaining(assembly));
                if(assembly == null)
                {
                    assetWrapper.EstRemLifeFHFC = 'NONE/NONE';
                    assetWrapper.EstRemLifeMO = 0;
                }
                else
                {
                    assetWrapper.EstRemLifeFHFC = (assembly.Remaining_Life_FC_Calculated__c==null?'NONE':String.valueof(assembly.Remaining_Life_FC_Calculated__c)) + '/' + (assembly.Remaining_Life_FH_Calculated__c==null?'NONE':string.valueof(assembly.Remaining_Life_FH_Calculated__c));
                    assetWrapper.EstRemLifeMO = (Leasewareutils.zeroifnull(request.Assumed_Monthly_FH__c)==0 || Leasewareutils.zeroifnull(request.Assumed_Monthly_FC__c) == 0)? 0: Integer.valueof(Math.min(LeasewareUtils.zeroIfNull(assembly.Remaining_Life_FH_Calculated__c)/request.Assumed_Monthly_FH__c,LeasewareUtils.zeroIfNull(assembly.Remaining_Life_FC_Calculated__c)/request.Assumed_Monthly_FC__c));
                }
                
                assetWrapper.Location = Leasewareutils.blankIfNull(curAC.Location__c);
                assetWrapper.IsPreRequested = CheckIfPreRequested(curAC, lstOldPAS, requestStartDate, requestEndDate);
                lstPAS.add(assetWrapper);
            }
        }

        return lstPAS;
    }

    private static boolean CheckIfPreRequested(Aircraft__c curAC, List<Portal_Asset_Selected__c> lstOldPAS, Date requestStartDate, Date requestEndDate)
    {
        if(lstOldPAS == null) return false;
          
        for(Portal_Asset_Selected__c tempPAS: lstOldPAS)
        {   
            //Check if there is any overlap of the date ranges with the old PAS records
            if(tempPAS.Asset__c == curAC.id && 
            ((requestStartDate >= tempPAS.Portal_Asset_Request__r.Date_Start__c && requestStartDate <= tempPAS.Portal_Asset_Request__r.Date_End__c) ||
             (requestEndDate >= tempPAS.Portal_Asset_Request__r.Date_Start__c && requestEndDate <= tempPAS.Portal_Asset_Request__r.Date_End__c) ||
             (requestStartDate <= tempPAS.Portal_Asset_Request__r.Date_Start__c && requestEndDate >= tempPAS.Portal_Asset_Request__r.Date_End__c)))
            {   
                return true;
            }                
        }
        return false;
    }

    //This method returns 'SV'/'USED' by checking if there is a lease starting/ending between the latestLastEventDate and availableDate
    private static string getEngineMaintCondition(Date availableDate, Date latestLastEventDate, List<Lease__c> lstAllLeasesForAC)
    {   

        if(latestLastEventDate == null) return C_USED;

        //Check if there is any lease between the latestLastEventDate and availableDate.
        //If there is no lease, the maintenance condition is 'SV'
        //If there is a lease, the maintenance condition is 'USED'
        for(Lease__c tempLease: lstAllLeasesForAC)
        {
            if(tempLease.Lease_Start_Date_New__c > availableDate) continue;
            if((tempLease.Lease_Start_Date_New__c >= latestLastEventDate && tempLease.Lease_Start_Date_New__c <= availableDate) ||
                (tempLease.Lease_End_Date_New__c >= latestLastEventDate && tempLease.Lease_End_Date_New__c <= availableDate))
            {
                return C_USED;
            }
        }

        return C_SV;        
    }

    private static String getEngineCurrAvailability(Constituent_Assembly__c assembly, List<Lease__c> lstLeases)
    {   
        //If today there is maintenance action(Shop Visit), set to 'Under Maintenace'
        Date todayDate = System.Today();
        if(assembly != null)
        {
            for(Assembly_Event_Info__c tempProjEvnt: assembly.Assembly_Event_Info__r){
                if(tempProjEvnt.Last_Event_Date__c != null){
                    Integer downTimeMos = LeasewareUtils.zeroIfNull(tempProjEvnt.Downtime_Months_F__c) == 0? 1: Integer.valueof(tempProjEvnt.Downtime_Months_F__c);
                    Date inductionDate = tempProjEvnt.Last_Event_Date__c.addMonths(-downTimeMos);
                    if(todayDate  >= inductionDate  && todayDate <= tempProjEvnt.Last_Event_Date__c){
                        return 'Under Maintenance';
                    }
                }
            }
        }

        //If today it is assigned to a lease, set to 'In Operation'        
        for(Lease__c tempLease: lstLeases){
            if(todayDate >= tempLease.Lease_Start_Date_New__c  && todayDate <= getLeaseEndDate(tempLease)){
                return 'In Operation';
            }
        }

        //If today the engine is available, set to 'Available'
        return 'Available';
    }

    private static boolean hasHalfLifeRemaining(Constituent_Assembly__c assembly)
    {   
        Integer minRemainingLife = 999999;
        if(assembly != null)
        {
            for(Assembly_Event_Info__c tempProjEvnt: assembly.Assembly_Event_Info__r)
            {   
                if(tempProjEvnt.Life_Remaining_Cycles__c != null && Integer.valueof(tempProjEvnt.Life_Remaining_Cycles__c) < minRemainingLife)
                {
                    minRemainingLife = Integer.valueOf(tempProjEvnt.Life_Remaining_Cycles__c);
                }
            }
        }
        
        if(minRemainingLife > 50)
            return true;
        else
            return false;
    }

    //Returns the lease re-delivery date, if not present, returns the lease end date
    private static Date getLeaseEndDate(Lease__c lease)
    {
        if(lease.Redelivery_Date__c == null)
            return lease.Lease_End_Date_New__c;
        else
            return lease.Redelivery_Date__c;
    }
    
    //If the request ends before the first lease's start date, it is a valid request
    //Apply tolerance to the request end date and check if it is a valid request
    private static boolean checkRequestEndDateWithTolerance(Date requestStartDate, Date requestEndDate, Integer tolerance, Date firstLeaseStartDate, List<Date> lstEligibleStartDates)
    {   
        Boolean isValidReq = false;
        for(Integer idx = -tolerance; idx <= tolerance; idx++)
        {               
            if(requestEndDate.addDays(idx) <= firstLeaseStartDate)
            {
                lstEligibleStartDates.add(requestStartDate.addDays(idx));
                isValidReq = true;
            }
        }         
        return isValidReq;
    }

    //If the request starts after the last lease's end date, it is a valid request
    //Apply tolerance to the request start date and check if it is a valid request
    private static boolean CheckRequestStartDateWithTolerance(Date requestStartDate, Integer tolerance, Date lastLeaseEndDate, List<Date> lstEligibleStartDates)
    {   
        Boolean isValidReq = false;
        for(Integer idx = -tolerance; idx <= tolerance; idx++)
        {         
            if(requestStartDate.addDays(idx) >= lastLeaseEndDate)    
            {   
                lstEligibleStartDates.add(requestStartDate.addDays(idx));
                isValidReq = true;
            }                              
        }         
        return isValidReq;
    }

    public static Map<String,String> savePortalAssetsSelected(Portal_Asset_Request__c request, List<Portal_Asset_Selected__c> lstAssetsSelected)
    {
        Map<String,String> resultMap = new Map<String,String>();
        try{

            List<User> userList = [select contact.accountid from user where id =:UserInfo.getUserId()];
            request.Account__c = userList[0].contact.accountid; //Null for internal users, populated for lessee requests

            request.Name = request.Asset_Type__c + '_' + Leasewareutils.getDatetoString(System.Today(), 'YYYY-MM-DD');
            insert request;

            for(Portal_Asset_Selected__c assetSelected: lstAssetsSelected)
            {
                assetSelected.Portal_Asset_Request__c = request.id;
            }

            insert lstAssetsSelected;
            resultMap.put('message', 'Asset Request successfully created');
            resultMap.put('code', '200');
            return resultMap;
        }
        catch(DmlException ex){
            System.debug(ex);
            LeaseWareUtils.createExceptionLog(ex,'Error inserting Portal Assets Selected'+lstAssetsSelected,null, null,'Portal Asset Search', true, '');            
        }catch(Exception ex){
            System.debug(ex);
            LeaseWareUtils.createExceptionLog(ex,'Error inserting Portal Assets Selected'+lstAssetsSelected,null, null,'Portal Asset Search', true, '');            
        }
        resultMap.put('code', '400');
        resultMap.put('message', 'An unexpected error occurred while creating the Asset Request');
        return resultMap;
    }
}