public with sharing class ReportTableController {
    @AuraEnabled
    public static ReportDataWrapper getReportResults (String reportAPIName, Id recordId, Boolean isReportTypeCustom ){
    
        Reports.ReportResults results;
        ReportDataWrapper wrapperObj = new ReportDataWrapper();
        
        List<Report> reportList = [select id from report where developerName =: reportAPIName];
        
        if(reportList.size()>0){
        
            if(String.isNotBlank(recordId)) {
                
                //describe report
                Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(reportList[0].Id);
                Reports.ReportMetadata reportMd = describe.getReportMetadata();
                
                Reports.ReportFilter RF;
                
                //create new filter.
                if(isReportTypeCustom == true){
                    
                    Schema.SObjectType objType = recordId.getSObjectType();
                    String sobjectName = objType.getDescribe().getName();
                    
                    Reports.ReportFilter reportFilter = new Reports.ReportFilter(sobjectName+'.Id', 'equals', recordId);
                    RF = reportFilter;
                    wrapperObj.reportFilter = sobjectName + '.Id';
                    
                }else{
                    
                    Reports.ReportFilter reportFilter = new Reports.ReportFilter('CUST_ID', 'equals', recordId); 
                    RF = reportFilter;
                    wrapperObj.reportFilter = 'CUST_ID';
                }
                 
                
                list<Reports.ReportFilter>allfilters = new list<Reports.ReportFilter> {RF};
                allfilters.addAll(reportMd.getReportFilters());
                
                
                reportMd.setReportFilters(allfilters);
                if(!Test.isRunningTest()){
                results = Reports.ReportManager.runReport(reportList[0].Id, reportMd, true); 
                }else{
                    results = Reports.ReportManager.runReport(reportList[0].Id, true);
                }    
    
            }else{
                 results = Reports.ReportManager.runReport(reportList[0].Id, true);
            }
         } 
        
        wrapperObj.data = JSON.serializePretty(results);
        return wrapperObj;
    }
    

    public class ReportDataWrapper {
        @AuraEnabled public String data {get;set;}
        @AuraEnabled public String reportFilter {get;set;}
    }
}