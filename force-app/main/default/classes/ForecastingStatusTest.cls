@isTest
public class ForecastingStatusTest {
    @testSetup static void TestSetupMethod() {
        
        Ratio_Table__c ratioTable = new Ratio_Table__c();
        ratioTable.Name= 'Test Ratio';
        insert ratioTable;
        
        Lease__c lease = new Lease__c(
            Reserve_Type__c= 'Maintenance Reserves',
            Base_Rent__c = 21,
            Lease_Start_Date_New__c = System.today()-2,
            Lease_End_Date_New__c = System.today()
            );
        insert lease;
        
        Aircraft__c aircraft = new Aircraft__c(
            Name= '1123 Test',
            Est_Mtx_Adjustment__c = 12,
            Half_Life_CMV_avg__c=21,
            Engine_Type__c = 'AE 3007',
            MSN_Number__c = '1230');
        insert aircraft;
        
        aircraft.Lease__c = lease.ID;
        update aircraft;

        lease.Aircraft__c = aircraft.ID;
        update lease;        

        Scenario_Input__c scenarioInp = new Scenario_Input__c();
        scenarioInp.Name ='Test Secanrio';
        scenarioInp.Asset__c = aircraft.Id;
        scenarioInp.Forecast_Status__c = 'Complete';
        scenarioInp.Number_Of_Months_For_Projection__c = 12; 
        scenarioInp.UF_Fee__c = 1 ;
        scenarioInp.Debt__c= 12;
        scenarioInp.Operating_Environment_Val__c= 1;
        scenarioInp.Number_of_Months_Of_History_To_Use__c =  -12;
        scenarioInp.Interest_Rate__c = 23;
        scenarioInp.Balloon__c =100;
        scenarioInp.Ratio_Table__c =ratioTable.Id;
        scenarioInp.Base_Rent__c = 40;
        scenarioInp.Estimated_Residual_Value__c =60;
        scenarioInp.RC_Type__c = 'Base Case';
        scenarioInp.Investment_Required_Purchase_Price__c=12;
        scenarioInp.Rent_Escalation_Month__c= 'January';
        scenarioInp.Ascend__c = 10; 
        scenarioInp.Avitas__c = 20; 
        scenarioInp.IBA__c = 30; 
        scenarioInp.Other__c = 40;
        scenarioInp.Lease__c = lease.ID;
        insert scenarioInp;
        
    }
    
     @isTest
    static void testMethod1(){
        Scenario_Input__c scenarioInp = [SELECT Id FROM Scenario_Input__c LIMIT 1];
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(scenarioInp);
        ForecastStatusController fc = new ForecastStatusController(sc);
        ApexPages.CurrentPage().getparameters().put('id', String.valueOf(scenarioInp.Id));
        fc.updateStatus();
        Test.StopTest();
    }
}