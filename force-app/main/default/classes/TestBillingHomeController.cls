@isTest
public class TestBillingHomeController {
    @isTest
    public static void testGetBillingData(){
        Date lastDayOfMonth = System.today().addMonths(2).toStartOfMonth().addDays(-1);
        Date minStartDate = Date.newInstance(lastDayOfMonth.year()-4, lastDayOfMonth.month(), 1);
        
        Operator__c operatorRecord=new Operator__c();
        operatorRecord.Name='test operator';
        operatorRecord.Current_Lessee__c=true;
        operatorRecord.Status__c='Approved';
        insert operatorRecord;
        
        
        
        Lease__c leaseRecord=new Lease__c();
        leaseRecord.Security_Deposit__c=200;
        leaseRecord.Lessee__c=operatorRecord.Id;
        leaseRecord.Lease_Start_Date_New__c=minStartDate;
        leaseRecord.Lease_End_Date_New__c=lastDayOfMonth;
        leaseRecord.Lease_Status_External__c = 'Active';
        insert leaseRecord;
        
        Invoice_External__c invoiceStages=new Invoice_External__c();
        invoiceStages.Name='test invoice external';
        invoiceStages.Period_From__c=System.today();
        invoiceStages.Period_To__c=System.today().addMonths(1);
        invoiceStages.Paid_Date__c=System.today().addDays(10);
        invoiceStages.MSN_ESN__c='test';
        invoiceStages.Outstanding_Amount__c=200;
        invoiceStages.Paid_Amount__c=250;
        invoiceStages.Invoice_Amount__c=250;
        invoiceStages.Issue_Date__c=System.today().addDays(3);
        invoiceStages.Invoice_Type__c='Interest';
        invoiceStages.Lease__c=leaseRecord.Id;
        insert invoiceStages;
        
        Id recordTypeId = Schema.SObjectType.Aircraft__c.getRecordTypeInfosByDeveloperName().get('Aircraft').getRecordTypeId();
        
        Aircraft__c aircraftRecord=new Aircraft__c();
        aircraftRecord.Name='test aircraft';
        aircraftRecord.MSN_Number__c='123';
        aircraftRecord.Date_of_Manufacture__c = minStartDate;
        aircraftRecord.Aircraft_Type__c='737';
        aircraftRecord.Aircraft_Variant__c='700';
        aircraftRecord.TSN__c=1.00;
        aircraftRecord.CSN__c=1;
        aircraftRecord.RecordTypeId=recordTypeId;
        insert aircraftRecord;
        aircraftRecord.Lease__c = leaseRecord.Id;
        update aircraftRecord;
        
        leaseRecord.Aircraft__c = aircraftRecord.Id; 
        update leaseRecord;
        
        Test.startTest();
        BillingHomeController.getBillingData();
        Test.stopTest();
    }
}