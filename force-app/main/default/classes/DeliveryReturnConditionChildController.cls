public class DeliveryReturnConditionChildController {
    
    // Load data to be displayed on DeliveryReturnCondition Table on Summary Tab.
    @auraEnabled
    public static List<ReturnConditionWrapper> getDeliveryReturnData(Id scenarioInp){
        try{
            List<Monthly_Scenario__c> lstMonthlyScenario = [SELECT Id, Eola_F__c, MR_At_Return__c, Month_Since_Overhaul__c, CSN__c, CSO__c, TSO__c,
                                                            Fx_Component_Output__r.Rate_Basis__c, Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c,
                                                            Fx_Component_Output__r.Cost_Per_FH__c, Return_Condition_Status__c, Fx_Component_Output__r.Total_PR_Cost_F__c,
                                                            Fx_Component_Output__r.Assembly_Display_Name__c, Fx_Component_Output__r.Name, 
                                                            Fx_Component_Output__r.Assembly_MR_Info__r.Rate_Basis__c,Net_EOLA_Payment_Amount__c,
                                                            Fx_Component_Output__r.Fx_Assembly_Event_Input_LP__r.Projected_Event__r.Maintenance_Program_Event__r.Primary_Unit__c,
                                                            Total_FH_Cumulative__c, FC__c, Current_Rate_Escalated__c, Fx_Component_Output__r.Fx_General_Input__r.EOLA__c,
                                                            Fx_Component_Output__r.Type__c, AVG_FH__c, Ratio_Since_last_PR__c, AVG_FC__c, FH__c
                                                            FROM Monthly_Scenario__c
                                                            WHERE Fx_Component_Output__r.Fx_General_Input__c =: scenarioInp
                                                            AND Fx_Component_Output__r.Life_Limited_part_LLP__c = NULL 
                                                            AND Return_Month__c = True 
                                                            AND Fx_Component_Output__r.Type__c != 'General' 
                                                            ORDER BY Fx_Component_Output__r.Type__c];
            
            List<ReturnConditionWrapper> lstRCW = new List<ReturnConditionWrapper>();
            Map<String,List<Monthly_Scenario__c>> mapMonthlyScenarioAsPerType = new Map<String, List<Monthly_Scenario__c>>();
            
            
            Map<String, Scenario_Component__c> mapComponentOutput = new Map<String, Scenario_Component__c>();
            
            
            for(Monthly_Scenario__c ms : lstMonthlyScenario){
                if(!mapMonthlyScenarioAsPerType.containsKey(ms.Fx_Component_Output__r.Id)){
                    mapMonthlyScenarioAsPerType.put(ms.Fx_Component_Output__r.Id,new List<Monthly_Scenario__c>()); 
                    mapComponentOutput.put(ms.Fx_Component_Output__r.Id, ms.Fx_Component_Output__r);
                }
                
                mapMonthlyScenarioAsPerType.get(ms.Fx_Component_Output__r.Id).add(ms);
                
            } 
            List<String> lstTotal = new List<String>();
            for(String componentId : mapMonthlyScenarioAsPerType.keySet()){
                ReturnConditionWrapper rcw = new ReturnConditionWrapper();
                Integer count = 0;
                Decimal sumFHandFC = 0.0, totalFC = 0.0, totalCSO = 0.0, netEOLAamount = 0.0;
                rcw.type = mapComponentOutput.get(componentId).Assembly_Display_Name__c;
                rcw.assemblyType = mapComponentOutput.get(componentId).type__c;
                Scenario_Component__c sc = mapComponentOutput.get(componentId);
                
                if(sc != null && sc.Fx_Assembly_Event_Input_LP__r != null && sc.Fx_Assembly_Event_Input_LP__r.Projected_Event__r != null && sc.Fx_Assembly_Event_Input_LP__r.Projected_Event__r.Maintenance_Program_Event__r != null){
                    String lastEventUnit = mapComponentOutput.get(componentId).Fx_Assembly_Event_Input_LP__r.Projected_Event__r.Maintenance_Program_Event__r.Primary_Unit__c;
                   if(lastEventUnit != null){
                       
                    if(lastEventUnit == 'Hours'){
                        rcw.lastEventUnit = 'Hrs';
                    }
                    else if(lastEventUnit == 'Cycles'){
                        rcw.lastEventUnit = 'Cyc';
                    }
                    else if(lastEventUnit == 'Months'){
                        rcw.lastEventUnit = 'Mo';
                    }
                   }
                } 
                
                if(sc != null && sc.Assembly_MR_Info__r != null){
                    String rateBasis = sc.Assembly_MR_Info__r.Rate_Basis__c;
                    if(rateBasis != '' && rateBasis != null){
                        if(rateBasis == 'Hours'){
                            rcw.reDeliveryUnit = 'per hr';
                        }
                        else if(rateBasis == 'Cycles'){
                            rcw.reDeliveryUnit = 'per cyc';
                        }
                        else if(rateBasis == 'APU H'){
                            rcw.reDeliveryUnit = 'per hr';
                        }
                        else if(rateBasis == 'APU C'){
                            rcw.reDeliveryUnit = 'per cyc';
                        }
                        else if(rateBasis == 'Daily'){
                            rcw.reDeliveryUnit = 'per day';
                        }
                        else if(rateBasis == 'Monthly'){
                            rcw.reDeliveryUnit = 'per month';
                        }
                    }
                }
                
                if (rcw.assemblyType =='Airframe'){
                    rcw.assemblyImage ='Airframe.svg'; 
                }
                else if(rcw.assemblyType.contains('Engine')){
                    rcw.assemblyImage ='ENGINE.svg';
                    if (rcw.type.contains('LLP')){
                        rcw.assemblyImage ='LLP.svg';
                    }
                }
                else if (rcw.assemblyType =='APU'){
                    rcw.assemblyImage ='APU.svg';
                }
                else if(rcw.assemblyType.contains('Landing Gear')){
                    rcw.assemblyImage ='LDG.svg'; 
                    if(rcw.assemblyType.contains('Nose')){
                        rcw.assemblyImage ='NLG.svg';
                    }
                } 
                else if(rcw.assemblyType.contains('Propeller')){
                    rcw.assemblyImage ='PROPELLER.svg'; 
                    if (rcw.type.contains('LLP')){
                        //Assign image for Propeller LLP
                    }
                }
                
                if(mapMonthlyScenarioAsPerType.containsKey(componentId)){
                    for(Monthly_Scenario__c ms : mapMonthlyScenarioAsPerType.get(componentId)){
                        if((ms.FC__c != null && ms.FC__c != 0) && (ms.FH__c != null && ms.FH__c != 0)){
                            count = count + 1;
                            sumFHandFC = sumFHandFC + (ms.FH__c/ms.FC__c );
                        }
                        totalFC = ms.FC__c; 
                        totalCSO = ms.CSO__c;
                        netEOLAamount = ms.Net_EOLA_Payment_Amount__c;
                        
                        rcw.averageFH = ms.AVG_FH__c;
                        rcw.averageFC = ms.AVG_FC__c;
                        rcw.ratioSincePR = ms.Ratio_Since_last_PR__c;
                        if(ms.Fx_Component_Output__r.Rate_Basis__c == 'Hours'){ rcw.usageSincePR = ms.TSO__c; }
                        else if(ms.Fx_Component_Output__r.Rate_Basis__c == 'Cycles'){ rcw.usageSincePR = ms.CSO__c; }
                        else{ rcw.usageSincePR = ms.Month_Since_Overhaul__c; }
                        
                        rcw.tso = ms.TSO__c;
                        rcw.totalFH = ms.Total_FH_Cumulative__c;
                        rcw.csn = ms.CSN__c;
                        rcw.totalRestorationCost += (ms.Fx_Component_Output__r.Total_PR_Cost_F__c != null ? ms.Fx_Component_Output__r.Total_PR_Cost_F__c : 0.0);
                        
                        rcw.rateAtDelivery = ms.Current_Rate_Escalated__c ;
                        rcw.reDeliveryCondition = ms.Return_Condition_Status__c;
                        
                        rcw.mrAtReturn  = ms.MR_At_Return__c;
                        rcw.lessor = rcw.lessor;
                        
                        rcw.eola = (ms.Eola_F__c  != null ? ms.Eola_F__c : 0.0);
                        rcw.costPerFH =  ms.Fx_Component_Output__r.Cost_Per_FH__c;
                        
                        rcw.isEOLA = ms.Fx_Component_Output__r.Fx_General_Input__r.EOLA__c;
                        rcw.extType = ms.Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c != null ? ms.Fx_Component_Output__r.Fx_General_Input__r.Ext_Reserve_Type__c : '';
                        
                        if(rcw.isEOLA && rcw.extType != 'Monthly MR'){ rcw.reserveType = 'EOLA'; }
                        else if(rcw.isEOLA && rcw.extType == 'Monthly MR'){ rcw.reserveType = 'BOTH'; }
                        else if(!rcw.isEOLA && rcw.extType != 'EOLA'){ rcw.reserveType = 'MR'; }
                        else if(!rcw.isEOLA && rcw.extType == 'EOLA'){ rcw.reserveType = 'BOTH'; }
                    }
                }  
                if(rcw.totalFH > 0 && totalFC > 0){ rcw.ratioDuringEntireLife = rcw.totalFH/totalFC; }
                rcw.netEOLA = netEOLAamount;
                lstTotal.add('>>> ' + mapComponentOutput.get(componentId).Assembly_Display_Name__c + ' >>> ' + String.valueOf(netEOLAamount));
                
                lstRCW.add(rcw);
            }
            System.debug(' >>> lstTotal  >>> ' + String.join(lstTotal, ', '));
            //System.debug('List-->>'+JSON.serialize(lstRCW));
            return lstRCW;
        }catch(Exception e){
            throw new AuraHandledException('Error: '+e.getMessage()+' Line Number: '+e.getLineNumber());
        }
    }
    
}