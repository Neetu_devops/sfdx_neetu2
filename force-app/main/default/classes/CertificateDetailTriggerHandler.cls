public class CertificateDetailTriggerHandler implements ITrigger{
    //Hint: create CertificateDetailTriggerHandler
    // Constructor
    private final string triggerBefore = 'CertificateDetailTriggerHandlerBefore';
    private final string triggerAfter =  'CertificateDetailTriggerHandlerAfter';
       
    
    public CertificateDetailTriggerHandler ()
    {   
    }
    //Making functionality more generic.
   
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {

    }
    public void bulkAfter()
    {

    }   
    public void beforeInsert()
    {   
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('CertificateDetailTriggerHandler.beforeInsert(+)');
        updateStartDtEndDtDefault();
        system.debug('CertificateDetailTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('CertificateDetailTriggerHandler.beforeUpdate(+)');
        updateStartDtEndDtDefault();
        system.debug('CertificateDetailTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        LeaseWareUtils.setFromTrigger(triggerBefore);
    
        system.debug('CertificateDetailTriggerHandler.beforeDelete(+)');
        system.debug('CertificateDetailTriggerHandler.beforeDelete(-)');
    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('CertificateDetailTriggerHandler.afterInsert(+)');   
        updateCertIDOnStipulatiVarSch();     
        system.debug('CertificateDetailTriggerHandler.afterInsert(-)');

    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('CertificateDetailTriggerHandler.afterUpdate(+)');
        updateCertIDOnStipulatiVarSch();
        autoUpdCert();
        system.debug('CertificateDetailTriggerHandler.afterUpdate(-)');
    }
     
    public void afterDelete()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('CertificateDetailTriggerHandler.afterDelete(+)');
        updateCertIDOnStipulatiVarSch();
        system.debug('CertificateDetailTriggerHandler.afterDelete(-)');
    }

    public void afterUnDelete()
    {
            
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
    }
    
    //Before Insert/Update
	// To update defualt CertificateDetail start and end date as parent Certificate start date and end date if it's null.
	private void updateStartDtEndDtDefault(){
        system.debug('CertificateDetailTriggerHandler.updateStartDtEndDtDefault(+)');
		list<Certificate_Detail__c> listCertDetail = (list<Certificate_Detail__c>)trigger.new;
        map<string,date> mapCertToStartDTEndDate =new map<string,date>();
        set<ID> setCertIds =new set<ID>();
		for(Certificate_Detail__c curCertDetail: listCertDetail){
			setCertIds.add(curCertDetail.Insurance_Certificate__c);
        }

        map<id,integer> mapCertIDCntDetail = new map<id,integer>();
        map<string,string> mapCertIDToType =new map<string,string>();
        for(Insurance_Cert__c curCert : [select id, name, Start_Date__c, Expiry_Date__c, Insurance_Type__c,
                                                (select id, name, Insurance_Type__c from Certificate_Details__r)     
                                                from Insurance_Cert__c where id in : setCertIds] ){
            mapCertToStartDTEndDate.put(curCert.id+'-SD',curCert.Start_Date__c);  //parentobject.startdate
			mapCertToStartDTEndDate.put(curCert.id+'-ED',curCert.Expiry_Date__c); //parentobject.expirydate
            for(Certificate_Detail__c curCertDetail : curCert.Certificate_Details__r){
                if(curCertDetail.Insurance_Type__c==Null) continue; //skipping those Certificate Detail records
                mapCertIDToType.put(curCert.id +'=='+curCertDetail.Insurance_Type__c,curCertDetail.Insurance_Type__c );
            }
            if(curCert.Insurance_Type__c!=null){
                mapCertIDCntDetail.put(curCert.id,curCert.Certificate_Details__r.size());        
            }            
        }
        system.debug('size of mapCertToStartDTEndDate='+ mapCertToStartDTEndDate.size());
        system.debug('size of mapCertIDToType='+mapCertIDToType.size());
        Certificate_Detail__c oldDetail;
        for(Certificate_Detail__c curCertDetail: listCertDetail){
            //Insert Detail: Allow to create multiple manual details records when it's parent Certificate's Insurance Iype is set as null
            if(trigger.isInsert){            
                if(mapCertIDCntDetail.containskey(curCertDetail.Insurance_Certificate__c) && mapCertIDCntDetail.get(curCertDetail.Insurance_Certificate__c)>0){   
                    curCertDetail.adderror('A \'Certificate Detail\' record is auto created when the Insurance Certificate has the Insurance Type specified. Please remove the Insurance Type on Insurance Certificate to be able to manually create a Certificate Detail record.');
                }
            }            
            System.debug('Defaulting start date and end date=');
			if(curCertDetail.Start_Date__c==Null) curCertDetail.Start_Date__c= mapCertToStartDTEndDate.get(curCertDetail.Insurance_Certificate__c+'-SD');
            if(curCertDetail.Expiry_Date__c==Null) curCertDetail.Expiry_Date__c= mapCertToStartDTEndDate.get(curCertDetail.Insurance_Certificate__c+'-ED');
            // for update of same reord with same Insurance Type then skip those recrds
            if(trigger.isUpdate){
                oldDetail= (Certificate_Detail__c)trigger.oldMap.get(curCertDetail.Id);
                if(oldDetail.Insurance_Type__c==curCertDetail.Insurance_Type__c) continue;
            }
            if(mapCertIDToType.containsKey(curCertDetail.Insurance_Certificate__c+'=='+curCertDetail.Insurance_Type__c)){
                curCertDetail.addError('Insurance Certificate Details are already entered in the system for '+ curCertDetail.Insurance_Type__c +' insurance. Please enter the details for another type.');
                continue;
            }
        }        
		system.debug('CertificateDetailTriggerHandler.updateStartDtEndDtDefault(+)');
    }

    //After Insert/Update/Delete
    //To Update Current and Next Certificate id on Stipulation and Variability Schedule
    private void updateCertIDOnStipulatiVarSch(){
        system.debug('CertificateDetailTriggerHandler.updateCertIDOnStipulatiVarSch(+)');

        list<Certificate_Detail__c> listCertDetail =(list<Certificate_Detail__c>) (trigger.isDelete ? trigger.old : trigger.new);
        set<id> setIDs=new set<id>();
        Certificate_Detail__c oldDetail;
        for(Certificate_Detail__c curDetail: listCertDetail){
            if(trigger.isInsert){
                setIDs.add(curDetail.id);
            }
            else if(trigger.isUpdate){
                oldDetail= (Certificate_Detail__c)trigger.oldMap.get(curDetail.Id);
                if(curDetail.Start_Date__c!= oldDetail.Start_Date__c || curDetail.Expiry_Date__c!= oldDetail.Expiry_Date__c || curDetail.Insurance_Type__c!=oldDetail.Insurance_Type__c || curDetail.Agreed_Value__c != oldDetail.Agreed_Value__c){
                    setIDs.add(curDetail.id);
                }
            }
            else{ //Delete case
                system.debug('Delete case');
                setIDs.add(curDetail.Insurance_Certificate__c);
            } 
        }
        system.debug('size of setIDs='+setIDs.size());
        set<id> setAssetID =new set<id>();
        for(Certificate_Detail__c curDetail :[select id,name, Insurance_Certificate__r.Asset__c from Certificate_Detail__c where id in : setIDs]){
            setAssetID.add(curDetail.Insurance_Certificate__r.Asset__c);
        }
        for(Insurance_Cert__c cert :[select id,name, Asset__c from Insurance_Cert__c where id in : setIDs]){
            setAssetID.add(cert.Asset__c);
        }
        system.debug('size of setAssetID='+ setAssetID.size());
        // Calling Batch job to update Current and Nnext Certificate id on Stipulation and Variability Schedule
        if(!setAssetID.isEmpty() && !LeaseWareUtils.isfromFuture() && !LeaseWareUtils.isfromBatch() && !Test.isRunningTest()){
            system.debug('calling CertificateUpdateBatch');
            Database.executeBatch(new CertificateUpdateBatch(setAssetID));
        }
        system.debug('CertificateDetailTriggerHandler.updateCertIDOnStipulatiVarSch(-)');
    }

    //After Update    
    //If only a single Certificate Detail is present, parent and child should always talk to each other. Any update made on parent or child should flow to the other.
    //If there are multiple Certificate Details, then any update made on parent or child should flow to the other.
	private void autoUpdCert(){
        system.debug('CertificateDetailTriggerHandler.autoUpdCert(+)');
        Certificate_Detail__c[] listDetail = (list<Certificate_Detail__c>)trigger.new;
        set<id> setCertID = new set<id>();
		set<string> setInsurType= new set<string>(); //set of Detail's Insurance Type
		map<string, Certificate_Detail__c> mapTypeDetail= new map<string, Certificate_Detail__c>();
        List<Insurance_Cert__c> listUpdCertRec =new List<Insurance_Cert__c>();		
        boolean IsCalledFrmCertTrigger=false;
		Certificate_Detail__c CertDetail;
        if(LeaseWareUtils.isFromTrigger('InsuranceCertificateTriggerHandlerAfter')){
            IsCalledFrmCertTrigger=true;
        }
        Certificate_Detail__c oldRec;
        for(Certificate_Detail__c curDetail: listDetail){
            if(trigger.isUpdate && !String.isBlank(curDetail.Insurance_Type__c) && !IsCalledFrmCertTrigger){
				oldRec = (Certificate_Detail__c)trigger.oldmap.get(curDetail.Id);				
				if(oldRec.Start_Date__c!= curDetail.Start_Date__c || oldRec.Expiry_Date__c !=  curDetail.Expiry_Date__c || oldRec.Agreed_Value__c!=  curDetail.Agreed_Value__c || oldRec.Insurance_Type__c!= curDetail.Insurance_Type__c || oldRec.Deductible_Amount__c!=  curDetail.Deductible_Amount__c || oldRec.Aggregate_limit__c!= curDetail.Aggregate_limit__c){
					system.debug('Update on Detail record');
                    setCertID.add(curDetail.Insurance_Certificate__c);
                    setInsurType.add(oldRec.Insurance_Type__c);
                    mapTypeDetail.put(curDetail.Insurance_Certificate__c+'$$$$$'+oldRec.Insurance_Type__c,curDetail); //Finding all Cert id, which child Detail which has Insurance Type and update case and not calling from parent, else skipping
				}	
			}
        }
        system.debug('size of setCertID='+ setCertID.size());
        system.debug('size of setInsurType='+ setInsurType.size());
		system.debug('size of mapTypeDetail='+ mapTypeDetail.size());
		Boolean flagUpdCert;
        list<Insurance_Cert__c> listCert=[select id,name,Start_Date__c, Expiry_Date__c, Agreed_Value__c, Insurance_Type__c, Deductible_Amount__c, Aggregate_limit__c
                                            from Insurance_Cert__c where id in:setCertID and Insurance_Type__c in : setInsurType];
        for(Insurance_Cert__c curCert:listCert){
            /*if(String.isBlank(curCert.Insurance_Type__c)){
                system.debug('Auto update should not allow for manual creation of detail, Please check Insurance Type of Certificate and help text');
                continue; //Skipping because Cert has 1 to many relation ship with detail then should not allow to auto update
            }*/
			flagUpdCert=false;
			CertDetail= mapTypeDetail.get(curCert.id+'$$$$$'+curCert.Insurance_Type__c);
            
			if(curCert.Start_Date__c != CertDetail.Start_Date__c){
				curCert.Start_Date__c = CertDetail.Start_Date__c;
				flagUpdCert=true;
			}
			if(curCert.Expiry_Date__c != CertDetail.Expiry_Date__c){
				curCert.Expiry_Date__c = CertDetail.Expiry_Date__c;
				flagUpdCert=true;
			}
			if(curCert.Agreed_Value__c != CertDetail.Agreed_Value__c){
				curCert.Agreed_Value__c = CertDetail.Agreed_Value__c;
				flagUpdCert=true;
			}
			if(curCert.Insurance_Type__c != CertDetail.Insurance_Type__c){
				curCert.Insurance_Type__c = CertDetail.Insurance_Type__c;
				flagUpdCert=true;
			}
			if(curCert.Deductible_Amount__c != CertDetail.Deductible_Amount__c){
				curCert.Deductible_Amount__c = CertDetail.Deductible_Amount__c;
				flagUpdCert=true;
			}
			if(curCert.Aggregate_limit__c != CertDetail.Aggregate_limit__c){
				curCert.Aggregate_limit__c = CertDetail.Aggregate_limit__c;
				flagUpdCert=true;
			}
			if(flagUpdCert) listUpdCertRec.add(curCert);            
        } //Loop ending - Insurance Cert
		
        system.debug('size of listUpdCertRec='+listUpdCertRec.size());       
        if(listUpdCertRec.size()>0){
            upsert listUpdCertRec;
        }
        system.debug('CertificateDetailTriggerHandler.autoUpdCert(-)');
    }
}