@isTest
private class TestPortfolioAnalyzer {

     @testSetup static void setup() {
         
         Aircraft__c Testaircraft = new Aircraft__c(
         Name='Test789',
         Country_Of_Registration__c = 'Hungary',
         Aircraft_Type__c ='727',
         Est_Mtx_Adjustment__c = 11,
         Half_Life_CMV_avg__c=20,
         MSN_Number__c = '1130',
         Marked_For_Sale__c =false
         );
        insert Testaircraft;

        Aircraft__c Testaircraft2 = new Aircraft__c(
        Name='Test89',
        Country_Of_Registration__c = 'Russia',
        Est_Mtx_Adjustment__c = 12,
        Half_Life_CMV_avg__c=21,
        MSN_Number__c = '1230',
        Aircraft_Type__c ='727',
        Marked_For_Sale__c =false
        );
        insert Testaircraft2;

        Aircraft__c Testaircraft3 = new Aircraft__c(
        Name='Test893',
        Country_Of_Registration__c = 'Russia',
        Est_Mtx_Adjustment__c = 123,
        Half_Life_CMV_avg__c=29,
        MSN_Number__c = '1220',
        Aircraft_Type__c ='727',
        Marked_For_Sale__c =false
        );
        insert Testaircraft3;

        Operator__c testOperator = new Operator__c(
        Name = 'American Airlines',                   
        Status__c = 'Approved',                          
        Rent_Payments_Before_Holidays__c = false,         
        Revenue__c = 0.00,                                
        Current_Lessee__c = true                         
        );
        insert testOperator;

        Country__c country =new Country__c(Name='Hungary');
        insert country;

        Operator__c testOperator2 = new Operator__c(
        Name = 'Air Asia ',                   
        Status__c = 'Assigned',                          
        Rent_Payments_Before_Holidays__c = false,         
        Revenue__c = 0.00,
      //Country__c ='Hungary', 
        Country_Lookup__c = country.id,                        
        Current_Lessee__c = true                         
        );
        insert testOperator2;

        Operator__c testOperator3 = new Operator__c(
        Name = 'Air Canada',                   
        Status__c = 'Assigned',                          
        Rent_Payments_Before_Holidays__c = false,         
        Revenue__c = 0.00,                                
        Current_Lessee__c = true                         
        );
        insert testOperator3;

        Marketing_Activity__c dealProjectedTest1 = new Marketing_Activity__c(
        Name = 'Test'+1,
        Deal_Type__c = 'Lease',                                
        Deal_Status__c = 'Pipeline',                           
        Asset_Type__c = 'Aircraft',                                                 
        Marketing_Rep_c__c = 'N/A',
        Prospect__c = testOperator.Id,                            
        Description__c = '2',                                                
        Inactive_MA__c = false
        );
        insert dealProjectedTest1;

        Marketing_Activity__c dealProjectedTest2 = new Marketing_Activity__c(
        Name = 'Test'+1,
        Deal_Type__c = 'Lease',                                
        Deal_Status__c = 'Pipeline',                           
        Asset_Type__c = 'Engine',                                           
        Marketing_Rep_c__c = 'N/A', 
        Prospect__c = testOperator2.Id,                           
        Description__c = '2',                                                
        Inactive_MA__c = true
        );
        insert dealProjectedTest2;
            
        Marketing_Activity__c dealProjectedTest3 = new Marketing_Activity__c(
        Name = 'Test'+1,
        Deal_Type__c = 'Lease',                                
        Deal_Status__c = 'Pipeline',                           
        Asset_Type__c = 'Aircraft',                                           
        Marketing_Rep_c__c = 'N/A', 
        Prospect__c = testOperator2.Id,                           
        Description__c = '2',                                                
        Inactive_MA__c = false
        );
        insert dealProjectedTest3;

     }
    @isTest
        static void TestGetAssetbyLocation() {
            String jsonassetbyLoc = PortfolioAnalyzerController.GetAssetByLocation();
            List<Object> AssetbyLoc = (List<Object>)JSON.deserializeUntyped(jsonassetbyLoc);
            System.assertEquals(2,AssetbyLoc.size());
        }

     @isTest
        static void TestGetAssetbyOperator(){
            String jsonprojectedop = PortfolioAnalyzerController.GetProjectedOperators();
            List<Object> projectedOp = (List<Object>)JSON.deserializeUntyped(jsonprojectedop);
        
            String jsonprojectedasset= PortfolioAnalyzerController.GetProjectedAssetByLocation();
            List<Object> projectedAsset = (List<Object>)JSON.deserializeUntyped(jsonprojectedasset);
            String jsoncurrentops=  PortfolioAnalyzerController.GetCurrentOperators();
        }
}