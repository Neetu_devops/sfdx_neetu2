public class CreateMRShareController {
    
    //This method fetch the Assembly Name
    @AuraEnabled
    public static MRShareWrapper fetchAssemblyType(Id assemblyMRId, Id mrShareId, List<String> lstAddedMRShare_p) {
        try{
            System.debug(assemblyMRId+'<<ID>>'+mrShareId);
            MRShareWrapper createMRShareWrapper = new MRShareWrapper();
            Set<String> setAddedMRShare_p = new Set<String>();
            
            //Creating set of Added MR Share types
            if(lstAddedMRShare_p.size() > 0) {
                setAddedMRShare_p.addAll(lstAddedMRShare_p);
            }
                        
            if(assemblyMRId != null){
                createMRShareWrapper.sNo = 1;
                createMRShareWrapper.isDisable =true;
                List<Assembly_MR_Rate__c> lstAssemblyMRRate = [SELECT Id, Name, Lease__c, Assembly_Lkp__c,Assembly_Event_Info__c, Lease__r.Aircraft__r.Maintenance_Program__c,Assembly_Lkp__r.Type__c,Assembly_Event_Info__r.Event_Type__c,
                                                               (select id,name,Assembly__c,Maintenance_Event_Name__c from MR_Shares__r)
                                                               FROM Assembly_MR_Rate__c 
                                                               WHERE ID=: assemblyMRId 
                                                               AND RecordTypeId=: Schema.SObjectType.Assembly_MR_Rate__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId()
                                                               LIMIT 1];
                Assembly_MR_Rate__c curAsmblyMRRec = new Assembly_MR_Rate__c();
                if(lstAssemblyMRRate.size()> 0){
                    curAsmblyMRRec = lstAssemblyMRRate[0];
                    createMRShareWrapper.assmeblyMRName = curAsmblyMRRec.Name;
                }   
                createMRShareWrapper.lstAssemblyType =  new List<String>(AssemblyMRRateTriggerHandler.getAssemblyType(curAsmblyMRRec, setAddedMRShare_p));
                createMRShareWrapper.mrShareRecord = new MR_Share__c();
                createMRShareWrapper.mrShareRecord.Assembly_MR_Info__c= assemblyMRId;
            }
            else if(mrShareId != null){
                createMRShareWrapper.sNo = 1;
                createMRShareWrapper.isDisable =false;
                List<MR_Share__c> lstMRShare = [SELECT Id,Name, Assembly__c,Maintenance_Event_Name__c, Max_Cap_in_Prcnt__c, Max_Cap_in_Cash__c, Assembly_MR_Info__c FROM MR_Share__c WHERE ID=:mrShareId LIMIT 1 ]; 
                if(lstMRShare.size()>0){
                    List<Assembly_MR_Rate__c> lstAssemblyMRRate = [SELECT Id, Name, Lease__c, Assembly_Lkp__c,Assembly_Event_Info__c, Lease__r.Aircraft__r.Maintenance_Program__c,Assembly_Lkp__r.Type__c,Assembly_Event_Info__r.Event_Type__c,
                                                               		(select id,name,Assembly__c,Maintenance_Event_Name__c from MR_Shares__r) 
                                                                   FROM Assembly_MR_Rate__c 
                                                                   WHERE ID=: lstMRShare[0].Assembly_MR_Info__c
                                                                   AND RecordTypeId=: Schema.SObjectType.Assembly_MR_Rate__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId()
                                                                   LIMIT 1];
                    Assembly_MR_Rate__c curAsmblyMRRec = new Assembly_MR_Rate__c();
                    if(lstAssemblyMRRate.size()> 0){
                        curAsmblyMRRec = lstAssemblyMRRate[0]; 
                        createMRShareWrapper.assmeblyMRName = curAsmblyMRRec.Name;
                    }   
                    createMRShareWrapper.lstAssemblyType =  new List<String>(AssemblyMRRateTriggerHandler.getAssemblyType(curAsmblyMRRec,null));
                    createMRShareWrapper.mrShareRecord = lstMRShare[0];
                }
            }
            return createMRShareWrapper; 
        }catch(Exception e){
            System.debug('Error Occured>>'+e.getStackTraceString()+'Line Number: '+e.getLineNumber());
            throw new AuraHandledException('Error Occured>>'+e.getStackTraceString()+'Line Number: '+e.getLineNumber());
        }
    }
    
    @AuraEnabled
    public static List<String> fetchEventType(Id assemblyMRId, String assemblyType, List<String> lstAddedMRShare_p) {
        try{
            System.debug('assemblyMRId>>'+assemblyMRId+assemblyType);
            
            Set<String> setAddedMRShare_p = new Set<String>();
            
            //Creating set of Added MR Share types
            if(lstAddedMRShare_p.size() > 0) {
                setAddedMRShare_p.addAll(lstAddedMRShare_p);
            }
            
            List<Assembly_MR_Rate__c> lstAssemblyMRRate = [SELECT Id, Name, Lease__c,Lease__r.Aircraft__r.Maintenance_Program__c, Assembly_Lkp__c,Assembly_Event_Info__c,Assembly_Lkp__r.Type__c,Assembly_Event_Info__r.Event_Type__c,
                                                               (select id,name,Assembly__c,Maintenance_Event_Name__c from MR_Shares__r) 
                                                           FROM Assembly_MR_Rate__c 
                                                           WHERE ID=: assemblyMRId 
                                                           AND RecordTypeId=: Schema.SObjectType.Assembly_MR_Rate__c.getRecordTypeInfosByDeveloperName().get('MR').getRecordTypeId()
                                                           LIMIT 1];
            Assembly_MR_Rate__c curAsmblyMRRec = new Assembly_MR_Rate__c();
            if(lstAssemblyMRRate.size()> 0){
                curAsmblyMRRec = lstAssemblyMRRate[0]; 
            }   
            List<String> lstEventType =  new List<String>(AssemblyMRRateTriggerHandler.getEventType(curAsmblyMRRec, assemblyType, setAddedMRShare_p));
            return lstEventType; 
        }catch(Exception e){
            System.debug('Error Occured>>'+e.getStackTraceString()+'Line Number: '+e.getLineNumber());
            throw new AuraHandledException('Error Occured>>'+e.getStackTraceString()+'Line Number: '+e.getLineNumber());
        }
    }
    
    @AuraEnabled
    public static String saveMRShare(String stringMRShareWrapper) {
        try{
            List<MRShareWrapper> listMRShareWrapper = (List<MRShareWrapper>) JSON.deserialize(stringMRShareWrapper, List<MRShareWrapper>.class);
           
            List<MR_Share__c> listMRShare = new List<MR_Share__c>(); 
            
            for(MRShareWrapper mrShareWrapper : listMRShareWrapper){
                listMRShare.add(mrShareWrapper.mrShareRecord);
                 System.debug('mrShareWrapper.mrShareRecord>>'+mrShareWrapper.mrShareRecord);
            }
            return AssemblyMRRateTriggerHandler.SaveRecords(listMRShare);
        }catch(Exception e){
            //System.debug('Error Occured>>'+e.getStackTraceString()+'Line Number: '+e.getLineNumber());
            throw new AuraHandledException('Error Occured>>'+e.getStackTraceString()+'Line Number: '+e.getLineNumber());
        }
    }
    
    public Class MRShareWrapper{
        @AuraEnabled public Integer sNo {get; set;}
        @AuraEnabled public String assmeblyMRName {get; set;}
        @AuraEnabled public Boolean isDisable {get; set;}
        @AuraEnabled public List<String> lstAssemblyType {get; set;}
        @AuraEnabled public MR_Share__c mrShareRecord {get; set;}
        
    }
}