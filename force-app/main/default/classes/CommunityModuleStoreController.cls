/*****************************************************************************************************************
*	    Date:		    05 April 2021
*       Author:         Denis Karpenko – Leaseworks
*       Description:    Controller class for the communityModuleStore LWC
******************************************************************************************************************/
public with sharing class CommunityModuleStoreController {

    public static String namespacePrefix = leasewareutils.getNamespacePrefix();
    public static String portalAssetObject = namespacePrefix + 'Portal_Asset__c';
    
    //Retrieving Asset Templates based on the picklist and multipicklist selections by the user
    @AuraEnabled
    public static List<Portal_Asset_Template__c> getTemplateInfo(String assetClass, List<String> assetTypeList, List<String> assetVariantList) {
        
        assetClass = String.escapeSingleQuotes(assetClass);
        
        String templateQuery = 'SELECT Id, Name, Asset_Class__c, Asset_Type__c, Asset_Variant__c, Thrust_applicable__c, EGT_Marting_applicable__c, Asset_Template_Image__c, Asset_Template_Icon__c, Remaining_Cycles_applicable__c, Remaining_Days_applicable__c, Remaining_Hours_applicable__c FROM Portal_Asset_Template__c WHERE ';
        templateQuery += '(Remaining_Cycles_applicable__c = true OR Remaining_Days_applicable__c = true OR Remaining_Hours_applicable__c = true) ';
        templateQuery += 'AND Asset_Class__c = ' + '\'' + assetClass + '\' ';
        
        if (!assetTypeList.isEmpty()) {
            templateQuery += 'AND (';
            for (String variant : assetTypeList) {
                variant = String.escapeSingleQuotes(variant);
                templateQuery += '(Asset_Type__c LIKE '+ '\'' + variant + '%' + '\'' + ') OR ' ;
            }
            templateQuery = templateQuery.left(templateQuery.length()-3);
            templateQuery += ') ';
        }
        
        if (!assetVariantList.isEmpty()) {
            templateQuery += 'AND (Asset_Variant__c IN (';
            for (String variant : assetVariantList) {
                variant = String.escapeSingleQuotes(variant);
                templateQuery += '\'' + variant + '\'' + ',' ;
            }
            templateQuery = templateQuery.left(templateQuery.length()-1);
            templateQuery += ') ) ';
        }
        
        templateQuery += 'ORDER BY Asset_Variant__c';
                
        List<Portal_Asset_Template__c> templateList = Database.query(templateQuery);
                
        return templateList;
    }

    //Method for retrieving datatable columns from the Field Set named after the Asset Class (or default one)
    @AuraEnabled
    public static String getDatatableColumns(String assetClass){
        try {
            assetClass = assetClass.replace(' ', '_');
            Schema.DescribeSObjectResult objInfo = Schema.getGlobalDescribe().get(portalAssetObject).getDescribe();
            Map<String, Schema.FieldSet> fsMap = objInfo.fieldSets.getMap();
            Schema.FieldSet columnFieldSetObj = fsMap.get(namespacePrefix + 'Default_Columns');
            if (fsMap.get(assetClass + '_Columns') != null) {
                columnFieldSetObj = fsMap.get(assetClass + '_Columns');
            }
            
            String columnsJSON = '[ ';
            for (Schema.FieldSetMember field: columnFieldSetObj.getFields()) {
                if (!field.getFieldPath().endsWithIgnoreCase('Serial_Number__c') || assetClass == 'Engine_Module') {
                    columnsJSON += '{ "label": ' + '\"' + field.getLabel() + '\"' + ', "fieldName": ' + '\"' + field.getFieldPath() + '\"';
                    String fieldType = '' + field.getType();
                    
                    if (fieldType == 'Picklist' || fieldType == 'String' || fieldType == 'Boolean') {
                        columnsJSON += ', "type": \"text\" ,"sortable": true';
                    } else if (fieldType == 'Double' || fieldType == 'Integer') {
                        columnsJSON += ', "type": \"number\" ,"sortable": true';
                    } else if (fieldType == 'Url') {
                        columnsJSON += ', "type":' + '\"' + fieldType.toLowerCase() + '\"' + ' , "typeAttributes": { "label": "Click Here" }';
                    } else {
                        columnsJSON += ', "type": ' + '\"' + fieldType.toLowerCase() + '\" ,"sortable": true';
                    }
                    columnsJSON += ' , "cellAttributes": { "alignment": \"center\" }},';
                } else {
                    columnsJSON += '{ "label": ' + '\"' + field.getLabel() + '\"' + ', "fieldName": \"Link_To_Asset\"';
                    columnsJSON += ', "type":' + '\"url\"' + ' , "typeAttributes": { "label": { "fieldName": "' + namespacePrefix + 'Serial_Number__c" } }'; //{ fieldName: 'website' }
                    columnsJSON += ' , "cellAttributes": { "alignment": \"center\" }},';
                }
            }
            columnsJSON = columnsJSON.left(columnsJSON.length()-1);
            columnsJSON += ' ]';
            return columnsJSON;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    //Method that retrieves datatable filters from the Field Set named after the Asset Class (or from default one)
    @AuraEnabled
    public static List<DynamicAssetFilter> getDatatableFilters(String assetClass){
        try {
            assetClass = assetClass.replace(' ', '_');
            Schema.DescribeSObjectResult objInfo = Schema.getGlobalDescribe().get(portalAssetObject).getDescribe();
            Map<String, Schema.FieldSet> fsMap = objInfo.fieldSets.getMap();
                Schema.FieldSet filterFieldSetObj = fsMap.get(namespacePrefix + 'Default_Filters');
                if (fsMap.get(assetClass + '_Filters') != null) {
                    filterFieldSetObj = fsMap.get(assetClass + '_Filters');
                    if (filterFieldSetObj.getFields().isEmpty()) {
                        return null;
                    }
                }

                Schema.sObjectType objType = Portal_Asset__c.getSObjectType();
                Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
                Map<String, Schema.SObjectField> objFieldMap = objDescribe.fields.getMap();

                List<DynamicAssetFilter> filterFieldsList = new List<DynamicAssetFilter>();
                for (Schema.FieldSetMember field: filterFieldSetObj.getFields()) {
                    String fieldpath = field.getFieldPath();
                    if (!fieldpath.endsWithIgnoreCase('Asset_Type__c') && !fieldpath.endsWithIgnoreCase('Asset_Variant__c') && !fieldpath.endsWithIgnoreCase('Asset_Class__c')) {
                        DynamicAssetFilter filter = new DynamicAssetFilter();
                        filter.fieldName = fieldpath;
                        filter.fieldLabel = field.getLabel();
                        String fieldType = '' + field.getType();
                        if (fieldType == 'Picklist') {
                            fieldType = 'picklist';
                        } else if (fieldType == 'Boolean') {
                            fieldType = 'boolean';
                        } else if (fieldType == 'String') {
                            fieldType = 'text';
                        } else if (fieldType == 'Integer') {
                            fieldType = 'number';
                        } else if (fieldType == 'Double') {
                            fieldType = 'number';
                        } else if (fieldType == 'Date') {
                            fieldType = 'date';
                        } else if (fieldType == 'Url') {
                            fieldType = 'url';
                        }
                        filter.fieldType = fieldType;
                        if (filter.fieldType == 'picklist'){
                            List<PicklistWrapper> picklistValueList= new List<PicklistWrapper>();
                            List<Schema.PicklistEntry> picklistValues = objFieldMap.get(filter.fieldName).getDescribe().getPickListValues();
                            for (Schema.PicklistEntry pe : picklistValues) {
                                PicklistWrapper pickWrap = new PicklistWrapper();
                                pickWrap.picklistFieldLabel = pe.getLabel();
                                pickWrap.picklistFieldValue = pe.getValue();
                                picklistValueList.add(pickWrap);
                            }
                            filter.picklistValues = picklistValueList;
                        }
                        filterFieldsList.add(filter);
                    }
                }
                return filterFieldsList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    //Retrieves todays date for the Delivery Date picklist field
    @AuraEnabled
    public static Date getTodaysDate() {
        return Date.today();
    }

    //Retrieves picklist values for Asset Type and Asset Variant based on existing database records for given Asset Class
    @AuraEnabled
    public static AssetPicklistWrapper getAssetPicklistValues(String assetClass) {
        
        assetClass = String.escapeSingleQuotes(assetClass);

        AssetPicklistWrapper assetPickWrap = new AssetPicklistWrapper();

        List<PicklistWrapper> assetTypeValues = new List<PicklistWrapper>();
        List<PicklistWrapper> assetVariantValues = new List<PicklistWrapper>();

        for (AggregateResult aggRes: [SELECT Asset_Type__c assettype FROM Portal_Asset_Template__c WHERE Asset_Class__c =: assetClass GROUP BY Asset_Type__c]) {
            PicklistWrapper pickWrap = new PicklistWrapper();
            pickWrap.picklistFieldLabel = String.valueOf(aggRes.get('assettype'));
            pickWrap.picklistFieldValue = String.valueOf(aggRes.get('assettype'));
            assetTypeValues.add(pickWrap);
        }

        for (AggregateResult aggRes: [SELECT Asset_Variant__c assetvar FROM Portal_Asset_Template__c WHERE Asset_Class__c =: assetClass GROUP BY Asset_Variant__c]) {
            PicklistWrapper pickWrap = new PicklistWrapper();
            pickWrap.picklistFieldLabel = String.valueOf(aggRes.get('assetvar'));
            pickWrap.picklistFieldValue = String.valueOf(aggRes.get('assetvar'));
            assetVariantValues.add(pickWrap);
        }

        assetPickWrap.assetTypeValues = assetTypeValues;
        assetPickWrap.assetVariantValues = assetVariantValues;

        return assetPickWrap;
    }
    
    //Retrieving Assets based on the Asset Class, Asset Template filter inputs, presented datatable columns and filter inputs
    //All incoming fields are escapeSingleQuotes protected from SOQL injection
    @AuraEnabled
    public static String getPortalAssetData(String assetClass, List<String> assetTypeList, List<String> assetVariantList, String filterListString, String fieldsetFilterString) {

        assetClass = String.escapeSingleQuotes(assetClass);

        List<AssetFilter> filterList = new List<AssetFilter>();
        List<String> columnsHeaderList = new List<String>();
        List<DynamicAssetFilter> filterAssetList = new List<DynamicAssetFilter>();
        if (filterListString != null) {
            filterList = (List<AssetFilter>)System.JSON.deserialize(filterListString, List<AssetFilter>.class);
        }
        if (fieldsetFilterString != null) {
            fieldsetFilterString = fieldsetFilterString.replace('equals', '=').replace('greater', '>=').replace('lesser', '<=').replace('"Yes"', 'true').replace('"No"', 'false').replace('"Both"', 'null');
            filterAssetList = (List<DynamicAssetFilter>)System.JSON.deserialize(fieldsetFilterString, List<DynamicAssetFilter>.class);
        }

        String assetQuery = 'SELECT Id, '+ namespacePrefix + 'Asset_Type__c, '+namespacePrefix + 'Asset_Class__c, '+ namespacePrefix +'Asset_Variant__c, '+namespacePrefix + 'EGT_Margin__c, '+
        namespacePrefix + 'PMA_DER__c, '+namespacePrefix + 'Portal_Asset_Template__c, '+
        namespacePrefix + 'Serial_Number__c, '+namespacePrefix + 'Link_To_Asset__c ';

        String utilAssetClass = assetClass.replace(' ', '_');
        Schema.DescribeSObjectResult objInfo = Schema.getGlobalDescribe().get(portalAssetObject).getDescribe();
        Map<String, Schema.FieldSet> fsMap = objInfo.fieldSets.getMap();
            Schema.FieldSet columnFieldSetObj = fsMap.get(namespacePrefix + 'Default_Columns');
            if (fsMap.get(utilAssetClass + '_Columns') != null) {
                columnFieldSetObj = fsMap.get(utilAssetClass + '_Columns');
            }
            for (Schema.FieldSetMember field: columnFieldSetObj.getFields()) {
                if (!assetQuery.contains(field.getFieldPath())) {
                    assetQuery += ', ' + field.getFieldPath();
                }  
            }
        
        assetQuery += ' FROM '+ namespacePrefix + 'Portal_Asset__c WHERE '+namespacePrefix + 'Inactive__c = false ';
        if (assetClass != null) {
            assetQuery += 'AND '+namespacePrefix + 'Asset_Class__c =' + '\'' + assetClass + '\' ';
        } 
        if (!assetTypeList.isEmpty()) {
            assetQuery += ' AND (';
            for (String type : assetTypeList) {
                assetQuery += '('+namespacePrefix + 'Asset_Type__c LIKE '+ '\'' + String.escapeSingleQuotes(type) + '%' + '\'' + ') OR ' ;
            }
            assetQuery = assetQuery.left(assetQuery.length()-3);
            assetQuery += ') ';
        }
        
        if (!assetVariantList.isEmpty()) {
            assetQuery += ' AND ('+namespacePrefix + 'Asset_Variant__c IN (';
            for (String variant : assetVariantList) {
                assetQuery += '\'' + String.escapeSingleQuotes(variant) + '\'' + ',' ;
            }
            assetQuery = assetQuery.left(assetQuery.length()-1);
            assetQuery += ') ) ';
        }

        if (!filterAssetList.isEmpty()) {
            assetQuery += ' AND ( '+namespacePrefix + 'Inactive__c = false ';
            for (DynamicAssetFilter dAssFill : filterAssetList) {
                if (dAssFill.fieldValue != null && dAssFill.fieldValue != '') {
                    assetQuery += ' AND ' + String.escapeSingleQuotes(dAssFill.fieldName).replace(' ', '') + String.escapeSingleQuotes(dAssFill.comparisonSign);
                    if (dAssFill.fieldType == 'number' || dAssFill.fieldType == 'boolean') {
                        assetQuery +=  String.escapeSingleQuotes(dAssFill.fieldValue);
                    } else {
                        assetQuery += '\'' + String.escapeSingleQuotes(dAssFill.fieldValue)  + '\'';
                    }
                }
            }
            assetQuery += ' ) ';
        }

        List<String> filterStrings = new List<String>();
        
        if (!filterList.isEmpty()) {
            assetQuery += ' AND (';
            for (AssetFilter filter : filterList) {
                String criteria = ' ('+namespacePrefix + 'Portal_Asset_Template__c = ' + '\'' + filter.templateId + '\''; 
                criteria += ' AND ' + filter.limiterField + ' >= ' + filter.limiterRange;
                
                if (filter.egtMargin != null) {
                    criteria += ' AND '+namespacePrefix + 'EGT_Margin__c >= ' + filter.egtMargin;
                }
                criteria += ') ';
                filterStrings.add(criteria);
            }
            assetQuery +=  String.join(filterStrings, 'OR ');
            assetQuery += ' ) ';
        }
        
        assetQuery += ' ORDER BY '+namespacePrefix + 'Asset_Type__c, '+
        namespacePrefix + 'Asset_Variant__c, '+namespacePrefix + 'Inactive__c, '+
        namespacePrefix + 'Available_From__c, '+namespacePrefix + 'Remaining_Cycles__c DESC';
        System.Debug('AssetQuery'+assetQuery);
        List<Portal_Asset__c> assetList = Database.query(assetQuery);
        
        if (assetClass != 'Engine Module') {
            for (Portal_Asset__c asset : assetList) {
                if(asset.Link_To_Asset__c == null){
                    asset.Link_To_Asset__c = '#';
                }
            }
        }
        
        String assetListJSON = JSON.serialize(assetList);

        if (assetClass != 'Engine Module') {
            String communityPrefix = UtilityWithoutSharingController.getCommunityPrefix();
            String msnURL;
            
            if(assetClass == 'Aircraft'){
                msnURL = communityPrefix + '/s/aircraft-details?recordId=';                                 
            } else {
                msnURL = communityPrefix + '/s/engine-details?recordId=';                                 
            }                                  
            
            Pattern p = Pattern.compile('("Link_To_Asset__c":"([0-9a-zA-Z]*)")');      
            Matcher m = p.matcher(assetListJSON);
            while (m.find() == true) { 
                assetListJSON = assetListJSON.replace(m.group(1), ' "Link_To_Asset": "' + msnURL + UtilityWithoutSharingController.encodeData(m.group(2)) + '"');
            } 
            assetListJSON = assetListJSON.replace('"Link_To_Asset__c":"#"', '"Link_To_Asset":"#"');
        }
        
        assetListJSON = assetListJSON.replace('\":true', '\":\"Yes\"').replace('\":false', '\":\"No\"');

        return assetListJSON;
    }

    //Retrieving Asset Template Parts data based on the search result selected rows Template Ids
    @AuraEnabled
    public static List<AssetTemplateWithParts> getPortalAssetPartsData(List<String> selectedTemplateIdList) {
        List<AssetTemplateWithParts> assetWrapList = new List<AssetTemplateWithParts>();
        
        Set<Id> templateIdSet = new Set<Id>();
        for (String templateId : selectedTemplateIdList) {
            templateIdSet.add(templateId);
        }
        
        List<Portal_Asset_Template__c> templateList = [SELECT Id, Name, Asset_Template_Icon__c, (SELECT Id, Name, Part_Name__c, Portal_Asset_Template__c, Remaining_Cycles_applicable__c, Remaining_Days_applicable__c, Remaining_Hours_applicable__c FROM Portal_Asset_Template_Parts__r) FROM Portal_Asset_Template__c WHERE Id IN :templateIdSet];
        for (Portal_Asset_Template__c template : templateList) {
            if (template.Portal_Asset_Template_Parts__r != null) {
                AssetTemplateWithParts assetWrap = new AssetTemplateWithParts();
                List<AssetPartWrapper> partsWrapList = new List<AssetPartWrapper>();
                assetWrap.templateId = template.Id;
                for (Portal_Asset_Template_Parts__c part : template.Portal_Asset_Template_Parts__r) {
                    AssetPartWrapper partWrap = new AssetPartWrapper();
                    partWrap.partId = part.Id;
                    partWrap.partName = part.Part_Name__c;
                    partWrap.cyclesApplicable = part.Remaining_Cycles_applicable__c;
                    partWrap.daysApplicable = part.Remaining_Days_applicable__c;
                    partWrap.hoursApplicable = part.Remaining_Hours_applicable__c;
                    partsWrapList.add(partWrap);
                }
                assetWrap.partRecordsList = partsWrapList;
                assetWrap.iconType = template.Asset_Template_Icon__c;
                assetWrap.templateName = template.Name;
                assetWrapList.add(assetWrap);
            }
        }
        return assetWrapList;
    }
    
    
    //Method for generating requests and creating records based on user input on Submit (includes email notification)
    @AuraEnabled
    public static void generateRequest(String reqStr) {
        Request req = (Request)System.JSON.deserialize(reqStr, Request.class);
        System.debug('generateRequest req '+JSON.serializePretty(req));
        Id userId = UserInfo.getUserId();
        User u = [SELECT Id, Contact.AccountId, Contact.Account.Name FROM User WHERE Id =: userId];


        Portal_Asset_Request__c par = new Portal_Asset_Request__c();
        par.Name = req.Name + '-' + u.Contact?.Account.Name + '-' + req.requestType;
        par.Asset_Class__c = req.assetClass;
        par.Request_Type__c = req.requestType;
        if(req.dateStart == null) {
            par.Date_Start__c = Date.today();
        }
        else {
            par.Date_Start__c = Date.valueOf(req.dateStart);        
        }
        
        par.Service_Type__c = req.serviceType;
        par.Additional_Request__c = req.additionalRequest;
        par.Account__c = u.Contact?.AccountId;
        insert par;
        
        Set<String> requestType = new Set<String>();
        Set<String> requestVariant = new Set<String>();

        List<Asset_Request_Details__c> exchangeArdList = new List<Asset_Request_Details__c>();
        if (req.exchangeDetails != null) {
            for (RequestDetail detail: req.exchangeDetails) {
                exchangeArdList.add(generateRequestDetail(detail, par, null, req.requestType));
                requestType.add(detail.assetType);
                requestVariant.add(detail.assetVariant);
            }
            insert exchangeArdList;
        }
        
        List<Asset_Request_Details__c> ardList = new List<Asset_Request_Details__c>();
        Integer i = 0;
        for (RequestDetail detail: req.details) {
            Asset_Request_Details__c exchangeArd = null;
            if (!exchangeArdList.isEmpty()) {
                exchangeArd = exchangeArdList[i];
            }
            ardList.add(generateRequestDetail(detail, par, exchangeArd, req.requestType));
            requestType.add(detail.assetType);
            requestVariant.add(detail.assetVariant);
            i++;
        }
        insert ardList;
        
        List<Portal_Asset_Parts__c> papList = new List<Portal_Asset_Parts__c>();
        if (req.exchangeDetails != null) {
            Integer j = 0;
            for (RequestDetail detail: req.exchangeDetails) {
                Asset_Request_Details__c ard = exchangeArdList[j];
                for (RequestDetailPart part: detail.parts) {
                    papList.add(generateParts(part, ard));
                }
                j++;
            }
        }
        insert papList;
        String requestTypeStr = String.join((Iterable<String>)requestType, ', ');
        String requestVariantStr = String.join((Iterable<String>)requestVariant, ', ');
        if(String.isNotEmpty(requestTypeStr) || String.isNotEmpty(requestVariantStr)) {
            List<Portal_Asset_Request__c> parUpdate = [select Id, Y_Hidden_Asset_Type__c, Y_Hidden_Asset_Variant__c from Portal_Asset_Request__c where Id =: par.Id];
            if(parUpdate.size() > 0) {
                parUpdate[0].Y_Hidden_Asset_Type__c = requestTypeStr;
                parUpdate[0].Y_Hidden_Asset_Variant__c = requestVariantStr;
                update parUpdate[0];
            }
        }

        sendRequestEmail(par, ardList, exchangeArdList, true);
        sendRequestEmail(par, ardList, exchangeArdList, false);
        
    }

    @AuraEnabled
    public static String getSpecSheet(String portalAssetID){
        if(portalAssetID == null) { return null;}
        TradingProfileDataSource dsc;
        try {
            dsc = (TradingProfileDataSource) Type.forName('AssetSpecSheetGeneratorImplLocal').newInstance();
        }
        catch(Exception e) {
            System.debug('getSpecSheet Exception '+e);
            return null;
        }
        return dsc.getData(portalAssetID);
    }

    //Method for sending email based on the inputs
    private static void sendRequestEmail(Portal_Asset_Request__c par,List<Asset_Request_Details__c> ardList, List<Asset_Request_Details__c> exchangeArdList, boolean isExternalUser) {
        //Template is selected based on Asset Class name (e.g. Engine_Asset_Request_Submitted) and reverts to default if no specific templates were found
        String templateName = par.Asset_Class__c + '_Asset_Request_Submitted';
        List<EmailTemplate> templateList = new List<EmailTemplate>();
        templateList = [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName =: templateName];
        if (templateList.isEmpty()) {
            templateList = [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName = 'Asset_Request_Submitted'];
        }
        LW_Setup__c recipients = [SELECT Id, Value__c FROM LW_Setup__c WHERE Name = 'Module Store Email Recipients' LIMIT 1];
        List<String> toAddresses = new List<String>();
        if(!isExternalUser) {
            if(recipients != null && recipients.Value__c != null) {
                for (String email: recipients.Value__c.split(',')) {
                    toAddresses.add(email);
                }
            }
        } else { 
            toAddresses.add(UserInfo.getUserEmail());
        }

        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(templateList[0].Id, null, par.Id);
        mail.setToAddresses(toAddresses);
        mail.setSaveAsActivity(false);
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        allmsg.add(mail);
        
        String body = mail.getPlainTextBody();
        
        if(!isExternalUser) {
            String Recordlink = URL.getOrgDomainUrl().toExternalForm()+'/'+par.Id;
            body += '<br/><br/>Click <a href="'+Recordlink+'">here</a> to view the record in Salesforce.';
        }

        // Modules
        body += '<br/><br/>Requested Modules:';
        for (Asset_Request_Details__c ard: ardList) {
            body+= '<br/> \u2022 ';
            if (ard.Serial_Number__c != null) {
                body+= ard.Serial_Number__c + ' (' + ard.Asset_Variant__c + ') ';
            } else {
                body+= ard.Asset_Variant__c ;
            }
        }
        
        //Exchange Modules
        if (!exchangeArdList.isEmpty()) {
            List<Asset_Request_Details__c> exchangeDetails = [SELECT Id, Serial_Number__c, (SELECT Id, Serial_Number__c, Remaining_Cycles__c FROM Portal_Asset_Parts__r) FROM Asset_Request_Details__c WHERE Id IN :exchangeArdList]; 
	        Integer em = 1;
	        body += '<br/><br/>Exchanged Modules: ';
            Set<String> moduleSet = new Set<String>();
            for (Asset_Request_Details__c ard: exchangeArdList) {
                moduleSet.add(ard.Asset_Variant__c);
            }
            body += String.join((Iterable<String>)moduleSet,', ');
        }

        //Additional Information
        if (par.Additional_Request__c != null) {
            body += '<br/><br/>Additional Information:<br/>';
            body+= par.Additional_Request__c + '<br/>';                    
        }

        body+= '<br/><br/>Thanks!';
        System.debug(' mail content '+body);
        mail.setHtmlBody(body);

        try {
            Messaging.SendEmailResult result = Messaging.sendEmail(allmsg,false)[0];
            System.debug(result.isSuccess());
            for (Messaging.SendEmailError err: result.getErrors()) {
                System.debug(err.getMessage());
                System.debug(err);
            }
            return;
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }
    
    //Helper method for generating Request Details
    private static Asset_Request_Details__c generateRequestDetail(RequestDetail detail, Portal_Asset_Request__c par, Asset_Request_Details__c exchangedBy, String requestType) {
        Asset_Request_Details__c ard = new Asset_Request_Details__c();
        if (exchangedBy == null && requestType.equalsIgnoreCase('Exchange')) {
            ard.Asset_Request_for_Exchange__c = par.Id;
        } else {
            ard.Asset_Request__c = par.Id;
            if(exchangedBy != null) {
                ard.Exchanged_By__c = exchangedBy.Id;
            }
            ard.Portal_Asset__c = detail.assetId;
            ard.Serial_Number__c = detail.serialNumber;
        }
        ard.Name = detail.name + '-' + detail.assetType;
        ard.Asset_Type__c = detail.assetType;
        ard.Asset_Variant__c = detail.assetVariant;  
        ard.EGT_Margin__c = detail.egtMargin;
        ard.PMA_DER__c = detail.pmaDer;
        return ard;
    }
    
    //Helper method for generating Portal Asset Parts
    private static Portal_Asset_Parts__c generateParts(RequestDetailPart part, Asset_Request_Details__c ard) {
        Portal_Asset_Parts__c pap = new Portal_Asset_Parts__c();
        pap.Asset_Request_Details__c = ard.Id;
        pap.Name = part.name;
        pap.Serial_Number__c = part.serialNumber;
        pap.Remaining_Cycles__c = part.remainingCycles;
        return pap;
    }

    //Filter inputs for template
    public class AssetFilter{
        @AuraEnabled
        public String templateId{get;set;}
        @AuraEnabled
        public Double limiterRange{get;set;}
        @AuraEnabled
        public Double egtMargin{get;set;} 
        @AuraEnabled
        public String limiterField{get;set;} 
    }

    //Dynamic Field Set Filter inputs
    public class DynamicAssetFilter{
        @AuraEnabled
        public String fieldName{get;set;}
        @AuraEnabled
        public String fieldLabel{get;set;}
        @AuraEnabled
        public String fieldType{get;set;}
        @AuraEnabled
        public List<PicklistWrapper> picklistValues{get;set;}
        @AuraEnabled
        public String comparisonSign{get;set;}
        @AuraEnabled
        public String fieldValue{get;set;}
    }

    //Picklist Values 
    public class PicklistWrapper {
        @AuraEnabled 
        public String picklistFieldLabel;
        @AuraEnabled 
        public String picklistFieldValue;
    }

    //Picklist Values for Asset Type and Asset Variant
    public class AssetPicklistWrapper {
        @AuraEnabled 
        public List<PicklistWrapper> assetTypeValues;
        @AuraEnabled 
        public List<PicklistWrapper> assetVariantValues;
    }
    
    //Parent Wrapper that groups parts by the Template
    public class AssetTemplateWithParts{
        @AuraEnabled
        public String templateId{get;set;}
        @AuraEnabled
        public String iconType{get;set;}
        @AuraEnabled
        public String templateName{get;set;}
        @AuraEnabled
        public List<AssetPartWrapper> partRecordsList{get;set;}
    }
    
    //Child Wrapper for Asset Template Parts and future input fields
    public class AssetPartWrapper{
        @AuraEnabled
        public String partId{get;set;}
        @AuraEnabled
        public String partName{get;set;}
        @AuraEnabled
        public Boolean cyclesApplicable{get;set;}
        @AuraEnabled
        public Boolean daysApplicable{get;set;}
        @AuraEnabled
        public Boolean hoursApplicable{get;set;}
        @AuraEnabled
        public String cyclesRemaining{get;set;}
        @AuraEnabled
        public String daysRemaining{get;set;} 
        @AuraEnabled
        public String hoursRemaining{get;set;}
    }
    
    //Request wrapper for user inputs
    public class Request {
        @AuraEnabled
        public String name = 'Asset Request';
        @AuraEnabled
        public String assetClass{get;set;}
        @AuraEnabled
        public String requestType{get;set;}
        @AuraEnabled
        public String dateStart{get;set;}
        @AuraEnabled
        public String serviceType{get;set;}
        @AuraEnabled
        public String additionalRequest{get;set;}
        @AuraEnabled
        public String requestedLocation{get;set;}
        @AuraEnabled
        public List<RequestDetail> exchangeDetails{get;set;}
        @AuraEnabled
        public List<RequestDetail> details{get;set;} 
    }
    
    //Request Detail wrapper for user inputs
    public class RequestDetail {
        @AuraEnabled
        public String name = 'Request Detail';
        @AuraEnabled
        public String assetId{get;set;}
        @AuraEnabled
        public String assetType{get;set;}
        @AuraEnabled
        public String assetVariant{get;set;}
        @AuraEnabled
        public String serialNumber{get;set;}
        @AuraEnabled
        public Double egtMargin{get;set;}
        @AuraEnabled
        public Boolean pmaDer{get;set;}
        @AuraEnabled
        public List<RequestDetailPart> parts{get;set;}
    }
    
    //Request Detail Part wrapper for user inputs
    public class RequestDetailPart {
        @AuraEnabled
        public String name{get;set;}
        @AuraEnabled
        public String serialNumber{get;set;}
        @AuraEnabled
        public Double remainingCycles{get;set;}
    }
}