@isTest
public class TestInvoiceAdjustmentController {
    
    @isTest(seeAllData=true)
    public static void loadTestCase() {
       
        Id recId = createInvoice();

        Boolean isApproval = InvoiceAdjustmentController.isApprovalProcessRunning();
        System.assertEquals(isApproval, false);
        
        String namespace = LeaseWareUtils.getNamespacePrefix();
        List <String> pickList = InvoiceAdjustmentController.getPickListOptions(namespace+'Invoice_Adjustment__c',namespace+'Approval_Status__c');
        System.assertEquals(pickList.contains('Approved'), true);       
        
        InvoiceAdjustmentController.getPendingAdjustment(recId);
        List<Invoice_Adjustment__c> data = InvoiceAdjustmentController.getAdjustmentStatus(recId);
        System.assertEquals(data, null);       

        InvoiceAdjustmentController.InvoiceAdjustmentWrapper wrapper = InvoiceAdjustmentController.getInvoiceData(recId);
        String dataString1 = JSON.serialize(wrapper);
        System.debug('loadCase '+JSON.serializePretty(dataString1));
        Invoice__c invoice = [select Id from Invoice__c where Id=: recId]; 
        invoice.Status__c = 'Approved';
        update invoice;
        
        if(wrapper != null && wrapper.invoiceAdjustmentItem != null ) {
            try {
                Id recordId = InvoiceAdjustmentController.addUpdateAdjustmentDataFromUI(dataString1,recId);
                InvoiceAdjustmentController.getInvoiceData(recordId);
            }
            catch(Exception e) {
                String msg = e.getMessage();
                System.assertEquals(msg.contains('Script-thrown exception'), true);  
            }
        }
        
        List<Invoice_Adjustment__c> adj = [select Id from Invoice_Adjustment__c where Linked_Invoice__c =: recId];
        if(adj.size() > 0) {
            Id rId = InvoiceAdjustmentController.addUpdateAdjustmentDataFromUI(dataString1,adj[0].Id);
            System.assertNotEquals(rId, null);  
            
        }
    }

    public static Id createInvoice() {
        
        LeaseWareUtils.clearFromTrigger(); 
        Operator__c operator = new Operator__c(Name='TestOperator1', Bank_Routing_Number_For_Rent__c='Test RN1');
        insert operator;

	    LeaseWareUtils.clearFromTrigger(); 
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft1', MSN_Number__c='TestMSN11');
        insert aircraft;

	    LeaseWareUtils.clearFromTrigger(); 
        Lease__c lease = new Lease__c(Name='TestLease1', aircraft__c = aircraft.id, Lessee__c = operator.Id, Lease_End_Date_New__c = Date.today().addYears(15),Lease_Start_Date_New__c = Date.today());
        insert lease; 

        Invoice__c invoiceRecord = new Invoice__c(Invoice_Date__c = System.today(), lease__c = lease.id, status__c = 'Pending',payment_status__c = null, Amount__c = 20000,
                                             invoice_type__c = 'Other', recordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByDeveloperName().get('Other').getRecordTypeId());
        
        leasewareUtils.clearFromTrigger();
        insert invoiceRecord;
        
        return invoiceRecord.Id;
    }
}