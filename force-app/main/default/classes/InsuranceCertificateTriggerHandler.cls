public class InsuranceCertificateTriggerHandler implements ITrigger{
    // Constructor
    private final string triggerBefore = 'InsuranceCertificateTriggerHandlerBefore';
    private final string triggerAfter =  'InsuranceCertificateTriggerHandlerAfter';
       
    
    public InsuranceCertificateTriggerHandler ()
    {   
    }
    //Making functionality more generic.
   
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {

    }
    public void bulkAfter()
    {

    }   
    public void beforeInsert()
    {   
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InsuranceCertificateTriggerHandler.beforeInsert(+)');
      
        system.debug('InsuranceCertificateTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('InsuranceCertificateTriggerHandler.beforeUpdate(+)');

        system.debug('InsuranceCertificateTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        LeaseWareUtils.setFromTrigger(triggerBefore);
    
        system.debug('InsuranceCertificateTriggerHandler.beforeDelete(+)');
        delDetailRec();
        system.debug('InsuranceCertificateTriggerHandler.beforeDelete(-)');
    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InsuranceCertificateTriggerHandler.afterInsert(+)');
        createChildIfClone();   
        autoInsUpdDetail();
        system.debug('InsuranceCertificateTriggerHandler.afterInsert(-)');

    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InsuranceCertificateTriggerHandler.afterUpdate(+)');
        autoInsUpdDetail();
        system.debug('InsuranceCertificateTriggerHandler.afterUpdate(-)');
    }
     
    public void afterDelete()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('InsuranceCertificateTriggerHandler.afterDelete(+)');
      
        system.debug('InsuranceCertificateTriggerHandler.afterDelete(-)');
    }

    public void afterUnDelete()
    {
            
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
    }
    
    // AfterInsert only
    //Hint: When Clone Certificate that copies the Detail related list information too.
    private void createChildIfClone(){        
        Insurance_Cert__c[] newCert = (list<Insurance_Cert__c>)trigger.new;
 
        map<ID,Insurance_Cert__c> IdsNeedToCloneMap = new map<ID,Insurance_Cert__c>();
        set<ID> SourceIdsSet = new set<ID>();
        for(Insurance_Cert__c curRec: newCert){
            if(curRec.isClone()){
                SourceIdsSet.add(curRec.getCloneSourceId());
                IdsNeedToCloneMap.put(curRec.getCloneSourceId(),curRec);
            }
        }
        
        if(!IdsNeedToCloneMap.isEmpty()){
            Insurance_Cert__c[] ParentCloneList = [select id
                                             ,(select name,
                                                Aggregate_Limit__c,
											    Agreed_Value__c,
											    Comments__c,
											    Deductible_Amount__c,
											    Insurance_Type__c											   
                                               from Certificate_Details__r
                                              )
                                             
                                             from Insurance_Cert__c
                                             where id in :SourceIdsSet ];
            
            list<sObject> listInsertChilds = new list<sObject>();
            
            for(Insurance_Cert__c curParent:ParentCloneList){
                for(Certificate_Detail__c curChild:curParent.Certificate_Details__r){
                    Certificate_Detail__c cloneChild = curChild.clone(false, true, false, false);
                    cloneChild.Insurance_Certificate__c = IdsNeedToCloneMap.get(curParent.Id).ID;
                    listInsertChilds.add(cloneChild);
                }
            }
            if(!listInsertChilds.isEmpty()){
                LeaseWareUtils.clearFromTrigger();
                insert listInsertChilds;
            }
            system.debug('size of listInsertChilds='+listInsertChilds.size());
        }
    } 
    //Auto create Certificate Details record for every Certificate record that has 'Insurance Type' filled in. 
    //Update the Certificate Details record whenever a change is made on the Certificate record.This should happen only when there is on Certificate Details record under the related list.(One to one relation)
    //If only a single Certificate Detail is present, parent and child should always talk to each other. Any update made on parent or child should flow to the other.
    //If there are multiple Certificate Details, then any update made on parent (Where Insurance Type is blank) or child should not flow to the other.
    //After Insert, After Update
    private void autoInsUpdDetail(){
        Insurance_Cert__c[] listCert = (list<Insurance_Cert__c>)trigger.new;        
        set<id> setCertificateIDs = new set<id>();
        List<Certificate_Detail__c> listInsUpdDetRec =new List<Certificate_Detail__c>();
        for(Insurance_Cert__c curCert: listCert){
            setCertificateIDs.add(curCert.id);            
        }
        system.debug('size of setCertIDTypeBlank='+setCertificateIDs.size());
        
        map<id,list<Certificate_Detail__c>> mapCertIDListOfDetail = new map<id,list<Certificate_Detail__c>>();
        map<id,Certificate_Detail__c> mapfetchDetailRec= new map<id,Certificate_Detail__c>();
        for(Insurance_Cert__c curCert : [select id, name, Start_Date__c, Expiry_Date__c, Insurance_Type__c,
                                                (select id, name, Start_Date__c, Expiry_Date__c, Insurance_Type__c, Insurance_Certificate__c,Agreed_Value__c,Deductible_Amount__c,Aggregate_limit__c from Certificate_Details__r)     
                                                from Insurance_Cert__c where id in : setCertificateIDs] ){
            mapCertIDListOfDetail.put(curCert.id,curCert.Certificate_Details__r);
            for(Certificate_Detail__c childrec:  curCert.Certificate_Details__r){
                mapfetchDetailRec.put(childrec.id,childrec);
            }   
        }
        system.debug('size of mapCertIDListOfDetail='+mapCertIDListOfDetail.size());
        boolean IsCalledFrmDetailTrigger=false;
        if(LeaseWareUtils.isFromTrigger('CertificateDetailTriggerHandlerAfter')){
            IsCalledFrmDetailTrigger=true;
        }
        Insurance_Cert__c oldRec;
        Boolean flagUpdDetail,errorFlag=false;
        for(Insurance_Cert__c curCert: listCert){        
            if(trigger.isInsert && !curCert.isClone()){ //Fresh Insert certificate- Detail records needs to be create if Certificate has Insurance Type
				if(String.isBlank(curCert.Insurance_Type__c)){
                    system.debug('Auto creation of Detail record would not be happened because Insurance Type is blank in the Certificate');
                    continue;
                }
                system.debug('Cert insert case,then auto create Detail record');
                listInsUpdDetRec=certDetailRec(curCert);
            }
            else if(trigger.isUpdate &&  !IsCalledFrmDetailTrigger){
                errorFlag=false;
                oldRec=(Insurance_Cert__c)trigger.oldmap.get(curCert.Id);
                flagUpdDetail=false;
                /* In below case is the part of Manual case where cert and detail has 1-M relationship
                 and When Insurance Type getting change from null (Manual) to other(Auto) but if has detail record, should not allow to change the Insurance Type*/
                if(mapCertIDListOfDetail.get(curCert.id).size()>1){
                    if(String.isBlank(oldRec.Insurance_Type__c) && !String.isBlank(curCert.Insurance_Type__c)){ 
                        system.debug('Changing Cert-Insurance Type from Null to other and has already Details record. Wont be allow to change Type');
                        curCert.Insurance_Type__c.adderror('This Certificate covers multiple Insurance Types as specified in the associated Certificate Detail records. Please leave the Insurance Type blank to indicate that the certificate covers multiple types.');
                        errorFlag=true;                        
                    }	
                }  // In below case is the part of Auto case where cert and detail has 1-1 relationship
                else if(oldRec.Start_Date__c!= curCert.Start_Date__c || oldRec.Expiry_Date__c !=  curCert.Expiry_Date__c || oldRec.Agreed_Value__c!=  curCert.Agreed_Value__c || oldRec.Insurance_Type__c!= curCert.Insurance_Type__c || oldRec.Deductible_Amount__c!=  curCert.Deductible_Amount__c || oldRec.Aggregate_limit__c!= curCert.Aggregate_limit__c){
                    if(String.isBlank(curCert.Insurance_Type__c)){
                        system.debug('Changing Cert-Insurance Type from other to null and has already Details record. Wont be allow to Update Detail records');
                    }
                    else if(mapCertIDListOfDetail.get(curCert.id).size()==0 && (String.isBlank(oldRec.Insurance_Type__c) && !String.isBlank(curCert.Insurance_Type__c))){
                        system.debug('create Detail record');
                        listInsUpdDetRec=certDetailRec(curCert);
                    }
                    else{
                        for(Certificate_Detail__c certDetail : mapCertIDListOfDetail.get(curCert.id)){//Finding Detail record based on previous Insurance Type value of Certificate
                            certDetail= mapfetchDetailRec.get(certDetail.id);
                            if(String.isBlank(oldRec.Insurance_Type__c) && !String.isBlank(curCert.Insurance_Type__c) && CertDetail.Insurance_Type__c!=curCert.Insurance_Type__c){
                                system.debug('Changing Cert-Insurance Type from Null to other and has already Details record. Wont be allow to change Type');
                                curCert.Insurance_Type__c.adderror('Based on the Insurance Type entered on the Insurance Certificate, Insurance Type and other details on the Certificate Detail record will be updated. Please check the records to make sure the Types match.');
                                errorFlag=true;
                                break;
                            }                        
                            system.debug('Cert update case,then auto update Detail record by Certificate');

                            if(CertDetail.Name != curCert.Name){
                                CertDetail.Name = curCert.Name ;
                                flagUpdDetail=true;
                            }
                            if(CertDetail.Start_Date__c != curCert.Start_Date__c){
                                CertDetail.Start_Date__c = curCert.Start_Date__c ;
                                flagUpdDetail=true;
                            }
                            if(CertDetail.Expiry_Date__c != curCert.Expiry_Date__c){
                                CertDetail.Expiry_Date__c = curCert.Expiry_Date__c;
                                flagUpdDetail=true;
                            }
                            if(CertDetail.Agreed_Value__c != curCert.Agreed_Value__c){
                                CertDetail.Agreed_Value__c = curCert.Agreed_Value__c;
                                flagUpdDetail=true;
                            }
                            if(CertDetail.Insurance_Type__c != curCert.Insurance_Type__c){
                                CertDetail.Insurance_Type__c = curCert.Insurance_Type__c;
                                flagUpdDetail=true;
                            }
                            if(CertDetail.Deductible_Amount__c != curCert.Deductible_Amount__c){
                                CertDetail.Deductible_Amount__c = curCert.Deductible_Amount__c;
                                flagUpdDetail=true;
                            }
                            if(CertDetail.Aggregate_limit__c != curCert.Aggregate_limit__c){
                                CertDetail.Aggregate_limit__c = curCert.Aggregate_limit__c;
                                flagUpdDetail=true;
                            }                  
                            if(flagUpdDetail){
                                listInsUpdDetRec.add(CertDetail);
                            }                        
                        }
                    }
                }
                if(errorFlag) break; 
            }            
        }
        system.debug('size of listInsUpdDetRec='+listInsUpdDetRec.size());
        if(listInsUpdDetRec.size()>0) upsert listInsUpdDetRec;
    }
    //create Detail record
    private static List<Certificate_Detail__c> certDetailRec(Insurance_Cert__c curCert){
        system.debug('prepare detail record list');
        List<Certificate_Detail__c> listInsDetailRec =new List<Certificate_Detail__c>();
        listInsDetailRec.add(new Certificate_Detail__c(
            Name = curCert.Name,
            Insurance_Certificate__c= curCert.id,
            Start_Date__c= curCert.Start_Date__c,
            Expiry_Date__c= curCert.Expiry_Date__c,
            Agreed_Value__c= curCert.Agreed_Value__c,
            Insurance_Type__c= curCert.Insurance_Type__c,
            Deductible_Amount__c= curCert.Deductible_Amount__c,
            Aggregate_limit__c= curCert.Aggregate_limit__c
        ));
        return listInsDetailRec;
    }
    //Before delete 
    //Hint: Before detele Certificate, Detail records shoulld be delete
    private void delDetailRec(){
		system.debug('InsuranceCertificateTriggerHandler.delDetailRec(+)');
		list<Insurance_Cert__c> listDelCert = ( list<Insurance_Cert__c>)trigger.old;
		list<Certificate_Detail__c> listDelDetailRec = [select id from Certificate_Detail__c where Insurance_Certificate__c IN :listDelCert];
		if(listDelDetailRec!= null && listDelDetailRec.size()>0) delete listDelDetailRec;
        system.debug('InsuranceCertificateTriggerHandler.delDetailRec(-)');
	}	
        
}