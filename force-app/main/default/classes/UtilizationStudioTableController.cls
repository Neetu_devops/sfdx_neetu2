/**
*******************************************************************************
* Class: UtilizationStudioTableController
* @author Created by Sonal, Lease-Works, 
* @date 10/06/2019
* @version 1.0
* ----------------------------------------------------------------------------------
* Purpose/Methods:
*  - Contains all custom business logic to review Aircraft utilization, its impact on the MR calculation and impact 
*
* History:
* - VERSION   DEVELOPER NAME        DATE            DETAIL FEATURES
*   1.0       Bhavna                06/11/2019      Updated UtilizationStudioTableController for UI Data updates, 
                                                    bugs fixing, sparkline charts and Cummulative MR
********************************************************************************
*/
global class UtilizationStudioTableController { 
    @AuraEnabled
    global static ListView getListViews() {
        return null;
    }
    
    @AuraEnabled
    global static UtilizationWrapper fetchUtilizationData(String assetId, Id utilizationId) {
        return null;
    }
    
    @AuraEnabled
    global static UtilizationWrapper saveUtilization(UtilizationWrapper utilizationWrapper) {
        //No more in use. (28/08/2019)
        return null;
    }
    
    global static List<ComponentWrapper> fillComponentWrapper(UtilizationWrapper uw, List<Utilization_Report_List_Item__c> listAssemblyUtilization) {
        return null;
    }
    
    @AuraEnabled
    global static void saveCommentOnListItems(sObject listItemRecord) {
    }
    
    @AuraEnabled
    global static String submitUtilizationApprovalRequest(String utilizationId) {
        return null;
    }
    
    /**
    * @description dummy method as global method cannot be removed from managed application.
    * 
    */
    global static Decimal calculateCummulativeMR(String leaseId, String assemblyType, String eventType, String assetId) {
    	return null;
    }
	
    global class UtilizationWrapper{
        @AuraEnabled global String leaseId{get; set;}
        @AuraEnabled global String errorMsg{get; set;}
        @AuraEnabled global String nameSpacePrefix{get; set;}
        @AuraEnabled global List<ComponentWrapper> lstComponentWrapper{get;set;}
        @AuraEnabled global Utilization_Report__c utilizationRec{get;set;}
        @AuraEnabled global String isUtilizationCreated{get; set;}
        @AuraEnabled global Decimal totalFH{get; set;}
        @AuraEnabled global String totalFHM{get; set;}
        @AuraEnabled global Decimal totalFC{get; set;}
        @AuraEnabled global Decimal totalBilled{get; set;}
        @AuraEnabled global Decimal totalBalance{get; set;}
        @AuraEnabled global Decimal totalPaid{get; set;}
        @AuraEnabled global Decimal totalRatio{get; set;}
        @AuraEnabled global Decimal totalTSN{get; set;}
        @AuraEnabled global Decimal totalCSN{get; set;}
        @AuraEnabled global Decimal totalCummulativeMR {get;set;}
        @AuraEnabled global String utilizationName{get; set;}
        @AuraEnabled global String leaseName{get; set;}
        @AuraEnabled global String type{get; set;}
        @AuraEnabled global String status{get; set;}
        @AuraEnabled global String pItem{get; set;}
        @AuraEnabled global String utilizationId{get; set;}
        @AuraEnabled global String aircraft{get; set;}
        @AuraEnabled global String lesseeName{get; set;}
        @AuraEnabled global Date periodEnding{get; set;}
        @AuraEnabled global Date recieved{get; set;}
        @AuraEnabled global Decimal proRate{get; set;}
        @AuraEnabled global String showSubmitBtn {get;set;}
        @AuraEnabled global String updateBtn {get;set;}
        @AuraEnabled global Boolean isAssemblyUtilizationCreated {get;set;}
        @AuraEnabled global Boolean firstUtilization {get;set;}
        @AuraEnabled global Boolean isEOLA {get;set;}
        @AuraEnabled global String invoiceName {get;set;}
        @AuraEnabled global String invoiceId {get;set;}
        @AuraEnabled global String isInvoiceCreated {get;set;}
        @AuraEnabled global String showInputField {get;set;}
        @AuraEnabled global String hideOuputField {get;set;}
        @AuraEnabled global String ccEmailAddress {get;set;}
        @AuraEnabled global List<Decimal> fhAssetData {get;set;}
        
        global UtilizationWrapper() {
            this.pItem = '';
            this.totalFH = 0;
            this.totalFHM = '0:00';
            this.totalFC = 0;
            this.totalRatio = 0;
            this.totalTSN = 0;
            this.totalCSN = 0;
            this.totalBilled = 0;
            this.totalCummulativeMR = 0;
            this.totalPaid = 0;
            this.totalBalance =0;
            this.isUtilizationCreated = 'utilizationNotCreated';
            this.isInvoiceCreated ='InvoiceNotCreated';
            this.lstComponentWrapper = new List<ComponentWrapper>();
            this.ccEmailAddress= UserInfo.getUserEmail();
            this.showSubmitBtn = 'hidebtn';
            this.fhAssetData = new List<Decimal>();
            this.isEOLA = false;
            this.firstUtilization = false;
        }
    }
    
    global class ComponentWrapper {
        @AuraEnabled global string showHelpText{get;set;}
        @AuraEnabled global string staticResourceVal{get;set;}
        @AuraEnabled global Boolean isEOLA{get;set;}
        @AuraEnabled global boolean showSameData{get;set;}
        @AuraEnabled global string hideInputForSameType{get;set;}
        @AuraEnabled global string assembly {get;set;}
        @AuraEnabled global Decimal flightH {get;set;}
        @AuraEnabled global String flightHM {get;set;}
        @AuraEnabled global Decimal flightC {get;set;}
        @AuraEnabled global Decimal ratio {get;set;}
        @AuraEnabled global String serialNumber {get;set;}
        @AuraEnabled global String assemblyId {get;set;}
        @AuraEnabled global Decimal tsn {get;set;}
        @AuraEnabled global Decimal csn {get;set;}
        @AuraEnabled global Utilization_Report_List_Item__c assemblyUtilizationRecord {get;set;}
        @AuraEnabled global String mrEvent {get;set;}
        @AuraEnabled global Decimal billed {get;set;}
        @AuraEnabled global String rateBasis {get;set;}
        @AuraEnabled global Decimal units {get;set;}        
        @AuraEnabled global Decimal rate {get;set;}
        @AuraEnabled global Decimal cummulativeMR {get;set;}
        @AuraEnabled global String comment {get;set;}
        @AuraEnabled global Invoice_Line_Item__c invoiceRec {get;set;}
        @AuraEnabled global Decimal paid {get;set;}
        @AuraEnabled global Decimal balance {get;set;}
        @AuraEnabled global Integer days {get;set;}
        @AuraEnabled global String revenueType {get;set;}
        @AuraEnabled global String glAccount {get;set;}
        @AuraEnabled global List<Decimal> fhData {get;set;}
        
        global ComponentWrapper() {
            this.showSameData =true;
            this.mrEvent = '';
            this.billed = 0;
            this.cummulativeMR = 0;
            this.rateBasis ='';
            this.units = 0;
            this.rate = 0;
            this.paid =0;
            this.comment='';
            this.fhData = new List<Decimal>();
        }
    }  
}