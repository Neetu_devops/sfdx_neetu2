/*
    Purpose: This apex class will get used for streaming related utility methods like email,json,xml,service etc.
        Example : emailWithAttachedLog() or addNodeToXML(a1,a2)
        
    Reference document: 
        https://docs.google.com/spreadsheets/d/1ggYKejNX0pazlZYXXF-wBTg2s9ChQLtgGqLKZs91Hyw/edit#gid=1447814031    



*/
public without sharing class LeaseWareStreamUtils {

	final static string nextLine = '\n', tabspace = '  ';
	public static void startNodeXML(Xmlstreamwriter writer,String node,Integer tabCounter){
		//system.debug('startNode'+tabCounter);
		for(Integer i=1;i<tabCounter;i++) writer.writeCharacters(tabspace);
		writer.writeStartElement(null,node,null);
		writer.writeCharacters(nextLine);
		
	}
	public static void endNodeXML(Xmlstreamwriter writer,String node,Integer tabCounter){
		//system.debug('endNode'+tabCounter);
		for(Integer i=1;i<tabCounter;i++) writer.writeCharacters(tabspace);
		writer.writeEndElement();
		writer.writeCharacters(nextLine);
	}	
	public static void addNodeXML(Xmlstreamwriter writer,String node,String data,Integer tabCounter){
		//system.debug('addNode'+tabCounter);
		for(Integer i=1;i<tabCounter;i++) writer.writeCharacters(tabspace);
		writer.writeStartElement(null,node,null);
		if(data!=null) writer.writeCharacters(data);
		writer.writeEndElement();
		writer.writeCharacters(nextLine);
		
	}
    
    // Call Tooling apiusing Named credential
     private static String restGetNamed(String endPoint, String method) {

		If(!Test.isRunningTest()){
			 List<NamedCredential> namedCredentialList = [SELECT Id, Endpoint FROM NamedCredential where DeveloperName = 'CallSFAPI'];
			 if(namedCredentialList.isEmpty()){
				 throw new LeaseWareException('API error, please contact your LeaseWorks administrator [No Named Credential "CallSFAPI" defined]');
			 }else {
				 String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
				 if(namedCredentialList[0].Endpoint != baseURL){
					 throw new LeaseWareException('API error, please contact your LeaseWorks administrator [Incorrect URL for Named Credential "CallSFAPI"]');
				 }
			 }
        }
		
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:CallSFAPI'+endPoint);
        req.setMethod(method);
        Http http = new Http();
        HTTPResponse r = http.send(req);
        if(r.getStatusCode() != 200){
            throw new LeaseWareException('API error, please contact your LeaseWorks administrator [Status Code : ' + r.getStatusCode() + ']' );
        }
        return r.getBody();
       
    }    
    
    public static String toolingAPISOQL( String query) {
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
    
        return restGetNamed( 
            '/services/data/v41.0/tooling/query?q='+ (query.replace(' ', '+'))
            , 'GET' );        
    } 
    
    // get the layout name based on recordTypeid and Sys admin profile
    public static string getLayoutNameForCurrentUserProfile( Id recordTypeId ) {
        string profilename = 'System Administrator';
        //'select Layout.Name from ProfileLayout where ProfileId = \'' + UserInfo.getProfileId() + '\' AND RecordTypeId = \'' + [ select RecordTypeId  from Account where id = :recordId].RecordTypeId + '\'');
        String body = toolingAPISOQL('select Layout.Name from ProfileLayout where  RecordTypeId= \'' + recordTypeId + '\' and Profile.Name = \'' +  profilename +'\' ' );
        //System.debug(LoggingLevel.ERROR, '@@@ v: ' + body );
        String name = body.substringBetween('"Name":"', '"');
        //System.debug(LoggingLevel.ERROR, '@@@ v: ' + name );
        return name;
    }    


    //utility method to attach a file to record.
    public static void addFileToRecord(string titleName,Id recId,String fileStr,string typeOfFile){
        string fileName = titleName + '__'+  system.now().format('yyMMddHHmmss') ;
        ContentVersion contentversionVarient=new ContentVersion();
        contentversionVarient.Title= fileName;
        contentversionVarient.contentLocation='S';
        contentversionVarient.PathOnClient= fileName + typeOfFile; //;
        contentversionVarient.FirstPublishLocationId = recId;
        contentversionVarient.VersionData=Blob.valueOf(fileStr);
        
		//To skip sharing with Library.
		LeaseWareUtils.setFromTrigger('FileUploadedFromBackEnd');
        insert contentversionVarient;        
		LeaseWareUtils.unsetTriggers('FileUploadedFromBackEnd');
    }

    public static list<string>  getEmailAddressforScript(){
        list<string> toAddresses = new list<string>{};
		try{
			if(LeaseWareUtils.isRunningInDevInstance()){
                toAddresses.add(UserInfo.getUserEmail());
                //toAddresses.add('anjani.prasad@lease-works.com');
			}else{
				String distoList = 'LW_Admin_Group';// groupName
				String qType = 'Regular'; //qType = Queue, Regular
				String[] strMsg=new String[]{}; // return message
				LeaseWareUtils.getEmailsFromGroup(distoList, qType, toAddresses, strMsg);		
			}
        }catch(Exception ex){system.debug('!!!Unexpected Exception ');}
        
        list<string> finalToAddresses = new list<string>{'anjani.prasad@lease-works.com','bob@lease-works.com'};
        for(string emailStr : toAddresses){
            if(emailStr.containsIgnorecase('@lease-works.com')) {finalToAddresses.add(emailStr);}
        }
        return finalToAddresses;
    }

    public static void emailWithAttachedLog(list<string> toAddresses,string subjectName,string emailBody,list<Document> documents){

		if(toAddresses==null) toAddresses = new list<string>{UserInfo.getUserEmail()};
        //if(finalToAddresses.size()==0) {finalToAddresses.add('anjani.prasad@lease-works.com'); // if no email was found, set this.
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses );
        mail.setSubject( '[' + System.UserInfo.getOrganizationName() + '(' + System.UserInfo.getOrganizationId()  + ')] - ' + subjectName );
        mail.setHtmlBody(emailBody );
        

        if(documents!=null && documents.size()>0){
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            // Add to attachment file list
            for(Document attachfile:documents){
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(attachfile.Name);
                efa.setBody(attachfile.body);
                fileAttachments.add(efa);
            }
            mail.setFileAttachments(fileAttachments);    
        } 

        try{
            LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
            LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
        }catch(exception e){
            system.debug('Cannot send email- Sub:' + subjectName +  '\n' + e);
        } 
    }

    public static Document createDoc(string doc,string docTitle){
        Document attachedOutputFile = new Document();
        attachedOutputFile.AuthorId = UserInfo.getUserId();
        attachedOutputFile.FolderId = UserInfo.getUserId();
        attachedOutputFile.Body = Blob.valueOf(doc);
        attachedOutputFile.name = docTitle ; 
        return attachedOutputFile;
    }    
//------------------Create method based on Leaseworks standards. ---------------------------------------------------------------------------

}