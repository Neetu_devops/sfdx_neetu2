public with sharing class PDP_Structure_Line_Handler {

    public void updatePDPSchecules() {

        List<PDP_Payment_Schedule__c> toInsert = new List<PDP_Payment_Schedule__c>();
        List<PDP_Payment_Schedule__c> allOldPayments = new List<PDP_Payment_Schedule__c>();
        List<PDP_Structure_Line__c> newLines = (List<PDP_Structure_Line__c>) trigger.new;
        Map<Id,Id> lineIdStructId = new Map<Id,Id>();
        for(PDP_Structure_Line__c line: newLines ) {
            lineIdStructId.put(line.id,line.PDP_Structure__c);
        }
        Map<Id,Id> batchStructIdMap = new Map<Id,Id>();
        Map<Id,Batch__c>  batchList = new Map<id,Batch__c>([SELECT Id, PDP_Structure__c  From Batch__c WHERE PDP_Structure__c IN : lineIdStructId.values()]); 
        for(Batch__c b : batchList.values() ) {
            batchStructIdMap.put(b.id,b.PDP_Structure__c);
        }   
        
        Map<Id, Structure__c> structuresMap = new Map<Id, Structure__c>([SELECT (SELECT RecordType.DeveloperName, Fixed_Date__c, Fixed_Payment__c, Month_Prior_Delivery__c, Payment__c, Signature__c FROM PDP_Structure_Lines__r)
                                            FROM Structure__c WHERE Id IN: batchStructIdMap.values()]);

        List<Pre_Delivery_Payment__c> pdps= [SELECT Id, Order_Sign_Date__c,Delivery_Date__c,Batch__c FROM Pre_Delivery_Payment__c WHERE Batch__c IN: batchStructIdMap.keyset() ];
        Set<Id> pdpId = new Set<Id>();
        for (Pre_Delivery_Payment__c pdp : pdps) {
            System.debug(pdp.ID);
            String relatedStructure;
            if ( batchList.get(pdp.Batch__c) != null && batchList.get(pdp.Batch__c).PDP_Structure__c != null ) {
                relatedStructure = batchList.get(pdp.Batch__c).PDP_Structure__c;
            } else {
                relatedStructure = null;
            }
            if(relatedStructure != null) {
                pdpId.add(pdp.id);
                for (PDP_Structure_Line__c strLine : structuresMap.get(relatedStructure).PDP_Structure_Lines__r) {
                    System.debug('strLine');

                    PDP_Payment_Schedule__c newItem = new PDP_Payment_Schedule__c();
                    newItem.PDP__c              = pdp.Id;
                    newItem.From_Structure__c   = true;
                    newItem.Name                = getScheduleLineName(strLine, pdp);
                    if (strLine.RecordType.DeveloperName == 'Variable') {
                        newItem.Percentage_of_PDPRP__c = strLine.Payment__c;
                        newItem.Payment_Months_Before_Delivery__c = strLine.Month_Prior_Delivery__c;
                       

                    } else {
                        if (strLine.Signature__c) {
                            newItem.Date__c     = pdp.Order_Sign_Date__c;
                        } else {
                            newItem.Date__c     = strLine.Fixed_Date__c;
                        }
                        if (strLine.Fixed_Payment__c != null) {
                            newItem.Total_Payment_Due_mm__c = strLine.Fixed_Payment__c;
                            
                        } else if ( strLine.Payment__c != null ){
                            System.debug(strLine.Payment__c);

                            newItem.Percentage_of_PDPRP__c = strLine.Payment__c;
                        }
                    }
                 
                    toInsert.add(newItem);
                    
                }

            }
        }

        if ( pdpId.size () > 0) {
             allOldPayments = [ SELECT Id FROM PDP_Payment_Schedule__c WHERE PDP__c IN: pdpId];
            System.debug('delete');

        }


        if ( allOldPayments != null ) {
            delete allOldPayments;
        }
        System.debug('toInsert');

        insert toInsert;
    }
    public String getScheduleLineName(PDP_Structure_Line__c structureLine, Pre_Delivery_Payment__c pdp) {

        if (structureLine.RecordType.DeveloperName == 'Variable') {
            Integer monthsToSubstract = Integer.valueOf(structureLine.Month_Prior_Delivery__c) * (-1);
            return returnMonth(pdp.Delivery_Date__c.addMonths(monthsToSubstract)) + ' Payment';
        } else {
            if(structureLine.Signature__c) {
                return returnMonth(pdp.Order_Sign_Date__c) + ' Payment';
            } else {
                return returnMonth(structureLine.Fixed_Date__c) + ' Payment';
            }
        }
        
    }

    public String returnMonth( Date deliveryDate ) {

        String year = (String.valueOf(deliveryDate.year())).substring(2,4);

        Map<Integer,String> allMonths = new Map<Integer,String>{1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May',
        6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'};

        return (allMonths.get(deliveryDate.month()) + '-' + year);
    }


}