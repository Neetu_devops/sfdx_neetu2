public without sharing class AirlineRecommendationController {

    public static Map<Integer,String> colorMap = new Map<Integer,String> {1 => '#228B22', 2 => '#FFA500', 3 => '#FF0000' ,  0 => '#bfbfbf'}; // 3- High 2- Moderate 1- Low
    public static Map<Integer,Integer> sizeMap = new Map<Integer,Integer> {1 => 22, 2 => 20, 3 => 18 , 4 => 16 };
            
    @AuraEnabled
    public static String getColumns(String fieldSetName){
        System.debug('getColumns fieldSetName ' + fieldSetName);
        if(String.isBlank(fieldSetName)) {
            fieldSetName = 'AirlineRecoList';
        }
        List<RLUtility.FieldStructure> fieldStructure = RLUtility.getFieldSets(fieldSetName, 'Operator__c');	
        if(fieldStructure == null) {
            String msg = 'Please contact your admin to set this up';
            AuraHandledException e = new AuraHandledException(msg);
            e.setMessage(msg);
            throw e;
        } 
        return JSON.serialize(fieldStructure);
    }
        
    @AuraEnabled        
	public static String getOperatorDetails(String operatorStr, String fieldSet) {
        List<AirlineListWrapper> operatorsList = (List<AirlineListWrapper>)System.JSON.deserialize(operatorStr, List<AirlineListWrapper>.class);
        System.debug('getOperatorDetails Ids: '+operatorStr + ' , fieldSet: '+fieldSet);
        if(String.isBlank(fieldSet)) {
            fieldSet = 'AirlineRecoList';
        }
        Map<String, Integer> operatorMap = new Map<String, Integer>();
    	for(AirlineListWrapper wrp: operatorsList) {
            operatorMap.put(wrp.operatorId, wrp.weight);
        }
        List<RLUtility.FieldStructure> fieldStructure = RLUtility.getFieldSets(fieldSet, 'Operator__c');	 
        Set<String> fields = new Set<String>();
        for(RLUtility.FieldStructure field: fieldStructure) {
            if('Reference'.equalsIgnoreCase(field.type)) {
                string fieldApi = Utility.replaceRefField(field.fieldName, 'c', 'r.name');  
                field.fieldName = fieldApi;
                fields.add(fieldApi);      
            }
            else {
                fields.add(field.fieldName);
            }
        }
        fields.add('Id');
        Set<String> operatorIds = operatorMap.keySet();
        String field =  String.join(new List<String>(fields),',');
        String queryStr = 'select '+field+' from Operator__c where Id in :operatorIds';
        
        System.debug('getOperatorDetails queryStr: '+queryStr);
        List<Operator__c> operatorList = database.query(queryStr);
        
        System.debug('getOperatorDetails operatorList: '+operatorList.size());
        if(operatorList.size() > 0) {
            List<AirlineListWrapper> wrapperList = new List<AirlineListWrapper>();
            for(Operator__c operator : operatorList) {
                AirlineListWrapper aid = new AirlineListWrapper();
                List<RLUtility.FieldStructure> fieldStructData = new List<RLUtility.FieldStructure>();
                for(RLUtility.FieldStructure fieldSt: fieldStructure) {
                    RLUtility.FieldStructure fieldStr = new RLUtility.FieldStructure();
                    if(fieldSt.fieldName.contains('__r')) {
                        String data = fieldSt.fieldName;
                        String[] refData = data.split('\\.');
                        if(operator.getSObject(refData[0]) != null) {
                            String value = String.valueOf(operator.getSObject(refData[0]).get(refData[1]));
                            fieldStr.value = value;
                            if('Name'.equalsIgnoreCase(refData[1])) {
                                fieldStr.referenceId =  String.valueOf(operator.getSObject(refData[0]).get('Id'));
                            }
                        }
                    }
                    else {
                        fieldStr.value = operator.get(fieldSt.fieldName);
                    }
                    fieldStr.fieldName = fieldSt.fieldName;
                    fieldStr.label = fieldSt.label;
                    fieldStr.type = fieldSt.type;
                    fieldStructData.add(fieldStr);
                }
                aid.fields = fieldStructData;
                aid.operatorId = operator.Id;
                aid.weight = operatorMap.get(String.valueOf(operator.Id).substring(0, 15)) == null ? 0 : operatorMap.get(String.valueOf(operator.Id).substring(0, 15));
                wrapperList.add(aid);
            }
            wrapperList.sort();
            return JSON.serialize(wrapperList);
        }
        return null;
    }            
    
    public class AirlineListWrapper implements Comparable{
        @AuraEnabled public String operatorId {get; set;}
        @AuraEnabled public List<RLUtility.FieldStructure> fields {get; set;}
        @AuraEnabled public Integer weight {get; set;}
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(((AirlineListWrapper)objToCompare).weight - weight);
        }    
    }
            
    @AuraEnabled
    public static List <Integer> getVintagePicklist(){
        List <Integer> picklist = new List <Integer>(); 
        Integer curYear = System.today().year();
        for(integer i = curYear - 10; i < curYear + 25; i++ ){
            picklist.add(i);
        }
        return picklist;
    }

    @AuraEnabled
    public static SeatData getSeatData() {
        List<WF_Field_Summary__c> fleetList = [ select Seat__c from WF_Field_Summary__c Order by LastModifiedDate DESC limit 1];
        if(fleetList.size() > 0) {
            WF_Field_Summary__c fleet = fleetList[0];
            
            if(String.isNotBlank(fleet.Seat__c)) {
                List<String> seatList = fleet.Seat__c.split(','); 
                SeatData seatData = null; 
                if(seatList.size() > 0) {
                    seatData = new SeatData(); 
                    seatData.seatFrom = seatList.get(0);
                    seatData.seatTo = seatList.get(seatList.size()-1);
                }
                System.debug('getSeatData size ' +seatData);
                return seatData;
            }
        }
        return null; 
    }
    
    @AuraEnabled
    public static List<String> getRegionPicklist () {
        list<WF_Field_Summary__c> worldFieldSummaryList = [select OperatorArea__c from WF_Field_Summary__c];
        system.debug('getRegionPicklist '+worldFieldSummaryList);
        if(worldFieldSummaryList.size()>0 && String.isNotBlank(worldFieldSummaryList[0].OperatorArea__c)) {
            
            List<String> regionList = worldFieldSummaryList[0].OperatorArea__c.split(',');
            regionList.sort();
            return regionList;
        }else{
            return null;
        }
    }

    @AuraEnabled
    public static List<String> getManagerPicklist () {
        try{
            List<WF_Field_Summary__c> fleetList = [ select Manager__c from WF_Field_Summary__c Order by LastModifiedDate DESC limit 1];
            if(fleetList != null && fleetList.size() > 0) {
                WF_Field_Summary__c fleet = fleetList[0];
                
                if(String.isNotBlank(fleet.Manager__c)) {
                    List<String> managerList = fleet.Manager__c.split(','); 
                    Set<String> newSet = new Set<String>();
                    for(String temp: managerList) {
                        newSet.add(temp.replaceFirst('^\\s*', ''));
                    }
                    List<String> lessorList = new List<String>();
                    lessorList.addAll(newSet);
                    lessorList.sort();
                    return lessorList;
                }
            }
        }
        catch(Exception e){system.debug('getLessor: Exception no lessor data '+e);}
        return null; 
    }

    
	public static AirlineRecommendation.Properties fillAirlineData(AirlineRecommendation.Properties airlineProperties){
        airlineProperties.color = colorMap.get(airlineProperties.rank);
        airlineProperties.size =  sizeMap.get(airlineProperties.rank);
        return airlineProperties;
    }
    
    
    @AuraEnabled
    public static String getNameSpacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(String objDetail, string contrfieldApiName,string depfieldApiName) {
        String namespace = getNameSpacePrefix();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(namespace+objDetail); 
        SObject myObj = targetType.newSObject();
        
    	Map<String, List<String>> objRes = DependentFilters.getDependentMap(myObj,contrfieldApiName,depfieldApiName);
        return objRes;
    }
    
    @AuraEnabled 
    public static Aircraft__c getAssetForRecord(String recordId){
        List<Aircraft__c> assetList = [
            select Id, Aircraft_Type__c, Aircraft_Variant__c, Engine_Type__c, 
            Vintage__c, MTOW_Leased__c, MSN_Number__c
            from Aircraft__c  where id =: recordId
        ];
        System.debug('asset '+assetList);
        if(assetList.size() > 0) 
        	return assetList[0];
        return null;
    }
    
    
	//@AuraEnabled        
    //public static List<AirlineRecommendation.Properties> getAirlinesRecommendedList(String recordId,Integer leaseExpiry){
    @AuraEnabled        
    public static List<AirlineRecommendation.Properties> getAirlinesRecommendedList(String aircraftType, String variant, 
                                                                                    String engine, integer vintage, 
                                                                                    integer vintageRange, integer mtow, integer mtowRange, string manager, string region, String scoreFilter, Integer seatFrom, Integer seatTo){
        system.debug('getAirlinesRecommended '+aircraftType + ', '+variant+ ', '+engine+ ', '
                     +vintage+ ', '+vintageRange + ', '+mtow+ ', '+mtowRange+ ','+manager+', '+scoreFilter+', '+seatFrom+', '+seatTo);
		//system.debug('getAirlinesRecommended new leaseExpiry:'+leaseExpiry);
        List<AirlineRecommendation.Properties> airlineList = AirlineUtils.getAirline(aircraftType, variant, engine, 
                                                                                     vintage, vintageRange, mtow, mtowRange,manager,region, scoreFilter,seatFrom,seatTo);
        
		if(airlineList != null) {                                                                                   
                                                                                        
        	airlineList = AirlineUtils.populateXYAxis(airlineList);
            for(AirlineRecommendation.Properties airline : airlineList) 
            	fillAirlineData(airline);
            
        }
        system.debug('getAirlinesRecommended final list : '+airlineList);
            
        return airlineList; 
    }
    
    @AuraEnabled
    public static String getDealType(){
        string DealType = getLWSetup_CS('APP_Deal_Type');
        system.debug('DealType==' + DealType);
        if (DealType == null) return 'MA';
        else if (DealType == 'MC-ONLY')
            return 'MC';
        else if (DealType == 'MC')  
            return 'MC';
        else 
            return 'MA';
    }
    
    public static string getLWSetup_CS(string TypeStr){
        LW_Setup__c lwSetup = LW_Setup__c.getValues(TypeStr);
        if(!isSetupDisabled(TypeStr) && lwSetup!=null  ) return lwSetup.value__c ;
        return null;
	}     
    
    public static boolean isSetupDisabled(string TypeStr){
        LW_Setup__c lwSetup = LW_Setup__c.getValues(TypeStr);
        if(lwSetup!=null) return lwSetup.Disable__c ;
        return false;
	}

    @AuraEnabled
    public static Aircraft__c getAssetDetail(String recordId) {
        List<Aircraft__c> assetList = [
            select Id, Name, MSN_Number__c
            from Aircraft__c  where id =: recordId
        ];
        if(assetList.size() > 0)
            return assetList[0];
        return null;
    } 
    
    @AuraEnabled
    public static String newMarkActivity(String operatorId, String operatorName, String aircraftType, String msn, Id fleetId){
        
        if(fleetId == null)
            return null;
        String sObjName = fleetId.getSObjectType().getDescribe().getName();
        Id parentId = null;
        if(sObjName.containsIgnoreCase('Aircraft__c') )
        	parentId = fleetId;
        else if(sObjName.containsIgnoreCase('Aircraft_Proposal__c') ) {
            List<Aircraft_Proposal__c> AID = [select Aircraft__c from Aircraft_Proposal__c where Id =: fleetId];
            if(AID.size() > 0) 
            	parentId = AID[0].Aircraft__c;
            else {
                String m = 'AID no asset';
                AuraHandledException e = new AuraHandledException(m);
                e.setMessage(m);
                throw e;
            }
        }
        if(parentId == null) {
            String m = 'No asset Id';
            AuraHandledException e = new AuraHandledException(m);
            e.setMessage(m);
            throw e;
        }
        	
        String actName = operatorName + ' '+ aircraftType;
        String status = getDealStatus();
        if(status == null)
            return null;
        String RecordTypeName = 'Lease';
        Map < String, Schema.RecordTypeInfo > Trans_MapByName = Schema.SObjectType.Marketing_Activity__c.getRecordTypeInfosByName();
        Id recordTypeId_lease = Trans_MapByName.get(RecordTypeName).getRecordTypeId();
      	if(recordTypeId_lease == null) return null;
        Marketing_Activity__c activity = new Marketing_Activity__c (
            RecordTypeId = recordTypeId_lease ,
            Name = actName, 
            Prospect__c = operatorId ,
            Deal_Status__c = status,
            Asset_Type__c = 'Aircraft' ,
            Description__c = 'Activity is created from airline recommendation' 
        );
        system.debug('createMarketingActivity '+activity); 
        LeaseWareUtils.clearFromTrigger();
        insert activity;
        
        String RecordAircraftTypeName = 'Lease';
        Map < String, Schema.RecordTypeInfo > Trans_MapByNameAsset = Schema.SObjectType.Aircraft_Proposal__c.getRecordTypeInfosByName();
        Id recordTypeAssetId_lease = Trans_MapByNameAsset.get(RecordAircraftTypeName).getRecordTypeId();
        if(recordTypeAssetId_lease == null) return activity.Id;
        
        Aircraft_Proposal__c assetTerm = new Aircraft_Proposal__c(
            RecordTypeId = recordTypeAssetId_lease ,
        	Name = msn,
            Marketing_Activity__c = activity.Id, 
            Aircraft__c = parentId,
            New_Used__c = 'Used'
        );
        LeaseWareUtils.clearFromTrigger();
        insert assetTerm;
        
        return activity.Id;
    }
    
    @AuraEnabled
    public static String isDealAlreadyCreated(String operatorId, Id assetId){
        String sObjName = assetId.getSObjectType().getDescribe().getName();
        if(sObjName.containsIgnoreCase('Aircraft__c') ) {
            List<Aircraft_Proposal__c> asset = [
                select Name, Marketing_Activity__c from Aircraft_Proposal__c 
                where 
                Marketing_Activity__c in (select Id from Marketing_Activity__c where Prospect__c = :operatorId) 
                and 
                Aircraft__c = :assetId
            ];
            if(asset.size() > 0)
                return asset[0].Marketing_Activity__c;
        }
        else if(sObjName.containsIgnoreCase('Aircraft_Proposal__c')) {
            List<Aircraft_Proposal__c> AID = [select Aircraft__c from Aircraft_Proposal__c where Id =: assetId];
            Id parentId = null;
            if(AID.size() > 0) {
            	parentId = AID[0].Aircraft__c;
                List<Aircraft_Proposal__c> asset = [
                    select Name, Marketing_Activity__c from Aircraft_Proposal__c 
                    where 
                    Marketing_Activity__c in (select Id from Marketing_Activity__c where Prospect__c = :operatorId) 
                    and 
                    Aircraft__c = :parentId
                ];
                if(asset.size() > 0)
                    return asset[0].Marketing_Activity__c;
            }
            
        }
        return null;
    }
    
    
    public static String getDealStatus(){
        Schema.DescribeFieldResult fieldResult =
            Marketing_Activity__c.Deal_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if(ple != null && ple.size() > 0){
            Schema.PicklistEntry f = ple[0];
            system.debug('getDealStatus '+f.getLabel() +' val:'+f.getValue());
            return f.getValue();
        }
        else 
            return null;
    }
    
    @AuraEnabled
    public static AirlineRecommendation.Properties getAirlineCriteria(String airline){
        system.debug('getAirlineCriteria '+airline);
        AirlineRecommendation.Properties airlineDetails = (AirlineRecommendation.Properties)JSON.deserialize(airline, AirlineRecommendation.Properties.class);
      //  system.debug('getAirlineCriteria airlineDetails: '+airlineDetails);
        airlineDetails.leasesList = getLeases(airlineDetails.operatorId, airlineDetails.aircraftType);
        airlineDetails.leasesVsOwned = getLeasedVsOwned(airlineDetails.operatorId, airlineDetails.operatorName,airlineDetails.aircraftType);
        airlineDetails.leasedCount = getLeasedCount(airlineDetails.operatorId, airlineDetails.operatorName,airlineDetails.aircraftType);
        airlineDetails.delivery = getDeliveryDetails(airlineDetails.operatorId, airlineDetails.operatorName, airlineDetails.aircraftType);
        airlineDetails.totalLeasedVsOwned = getTotalCountLeasedVsOwned(airlineDetails.operatorName);
        airlineDetails.creditScore = getCreditScore(airlineDetails.operatorId);
        airlineDetails.numberOfAircraft = getAircraftCount(airlineDetails.operatorId);
        airlineDetails.numberOfDeals = getActiveDealsCount(airlineDetails.operatorId);
        airlineDetails.NumberOfSimilarType = getSimilarAircraft(airlineDetails.operatorId);
        system.debug('getAirlineCriteria + '+airlineDetails);
        return airlineDetails;
    } 
    
    public static String getCreditScore(String operatorId){
        if(operatorId == null)
            return null;
        
        List<Operator__c> creditDataList = [
            select Financial_Risk_Profile__c, Name from Operator__c where 
            Id = :operatorId
        ]; 
        if(creditDataList.size() > 0)
            return creditDataList[0].Financial_Risk_Profile__c;
        return null;
    }

    public static Integer getAircraftCount(String operatorId){
        if(operatorId == null)
            return 0;
        List<Aircraft__c> aircraftList = [select Id 
                                          from Aircraft__c 
                                          where Lease__r.Operator__c =: operatorId and 
                                          Lease__r.Active__c = true	
                                         ];
        return aircraftList.size();
    }
    
    public static Integer getActiveDealsCount(String operatorId){
        if(operatorId == null)
            return 0;
        List<Marketing_Activity__c> dealList = [select Id from Marketing_Activity__c 
                                                where Prospect__c =: operatorId and 
                                                Inactive_MA__c = false 
                                               ];
        return dealList.size();
    }
    
    public static Integer getSimilarAircraft(String operatorId){
        if(operatorId == null)
            return 0;
        return 0;
    }
    
    public static integer getDeliveryDetails(String operatorId,String operatorName,String aircraftType){
        Date dt_24Months = system.today().addMonths(24);
        List<Aircraft_Proposal__c>  item =
            [select Id  
             from Aircraft_Proposal__c
             where Inactive_AT__c = false
             and Marketing_Activity__r.Inactive_MA__c = false
             and Marketing_Activity__r.Prospect__c = :operatorId
             and Marketing_Activity__r.Aircraft_Type_F__c = :aircraftType
             and Status__c = 'Delivered' 
             and Delivery_Date_N__c >= :dt_24Months
            ];
        system.debug('getDeliveryDetails list '+item);
        if(item != null && item.size() > 0) {
            //String data = '    -  Within the last 24 months '+operatorName+' has taken delivery of '+item.size()+' '+aircraftType+'<br/>';
            return item.size();
        }
        return 0;
    }
    
    public static Map<String,String> getLeases(String operatorId, String assetType){
        try{
            List<Lease__c> leaseList = [
                select Id, Name, Lease_End_Date_New__c ,Aircraft__r.Aircraft_Type__c, Aircraft__r.Engine_Type__c, 
                Aircraft__r.Vintage__c, Aircraft__r.MTOW_Leased__c,
                Aircraft__r.Date_of_Manufacture__c 
                from Lease__c 
                where Operator__r.Id =: operatorId
                and Aircraft__r.Aircraft_Type__c =: assetType
                order by Lease_Start_Date_New__c desc
            ];
            // System.debug('getLeases leaseList: '+leaseList.size());
           // String leaseDetails = '2) Our leases with this customer <br/>';
            Map<String,String> leasesMap = new Map<String,String>();
            for(Lease__c lease: leaseList) {
                String params = '';
                if(lease.Lease_End_Date_New__c != null) {
                    Datetime output = lease.Lease_End_Date_New__c;
                    params =  'Lease End Date: '+output.format('MMM-yy');
                }
                if(lease.Aircraft__r.Engine_Type__c != null)
                    params = params + ', Engine: '+ lease.Aircraft__r.Engine_Type__c;
                if(lease.Aircraft__r.Date_of_Manufacture__c != null)
                    params = params + ', Vintage: '+ lease.Aircraft__r.Date_of_Manufacture__c.year() ;
                if(lease.Aircraft__r.MTOW_Leased__c != null)
                    params = params + ', MTOW '+ lease.Aircraft__r.MTOW_Leased__c ;
                
                String details = lease.Name + ' ('+params+')';
                leasesMap.put(lease.Id, details);
            }
            // System.debug('getLeases leaseDetails: '+leaseDetails);
            if(leaseList.size() <= 0)
                return null;
        	return leasesMap;
        }
        catch(Exception e){return null;}
    }
    
    public static List<String> getLeasedVsOwned(String operatorId, String operatorName, String assetType) {
        try{
            List<AggregateResult> fleetList = [
                select Manager__c owner, count(Operator__c) from Global_Fleet__c where 
                Operator__c =: operatorName  and 
                Manager__c !=: operatorName 
                group by Manager__c
                order by count(Operator__c) desc
            ];
            system.debug('getLeasedVsOwned '+operatorId + ' '+operatorName + ' '+assetType);
            system.debug('getLeasedVsOwned '+String.valueOf(fleetList));
            List<String> leasedOwnedDetails = new List<String>();
            if(fleetList.size() > 0) {
                for(AggregateResult sObj: fleetList) {
                    String owner = String.valueOf(sObj.get('owner'));
                    if(String.isBlank(owner))
                        owner = 'Not available';
                    String detail = owner + ' ('+sObj.get('expr0')+')';
					leasedOwnedDetails.add(detail);          
                }
                return leasedOwnedDetails;
            }
            return null;
        }
        catch(Exception e) {
            return null;
        }
    }
    
    public static Integer getLeasedCount(String operatorId, String operatorName, String assetType) {
        try{
            List<AggregateResult> fleetList = [
                select Manager__c, count(Operator__c) from Global_Fleet__c where 
                Operator__c =: operatorName  and 
                Manager__c !=: operatorName 
                group by Manager__c
                order by count(Operator__c) desc
            ];
            if(fleetList.size() > 0) {
                integer count = 0;
                for(AggregateResult sObj: fleetList) {
                    count = count + Integer.valueOf(sObj.get('expr0'));
                }
                return count;
            }
            return 0;
        }
        catch(Exception e) {
            return 0;
        }
    }
   
    public static Integer getTotalCountLeasedVsOwned(String operatorName) {
        try{
            List<AggregateResult> fleet = [
                select count(Operator__c) totalCount from Global_Fleet__c where 
                Operator__c =: operatorName  
            ];
            if(fleet.size() > 0)
                return Integer.valueOf(fleet[0].get('totalCount'));
            return 0;
        }
        catch(Exception e) {
            return 0;
        }
    }
    
    public class SeatData {
        @AuraEnabled public String seatFrom {get; set;}
        @AuraEnabled public String seatTo {get; set;}
    }
}