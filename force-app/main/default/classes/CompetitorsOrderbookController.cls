public without sharing class CompetitorsOrderbookController implements CompetitorsOrderbookInterface{
    public static Map<String, List<Integer>> managerSet = new Map<String, List<Integer>>();
    public static List<Integer> unplacedCount = new List<Integer>();
    public static List<Integer> placedCount = new List<Integer>();
    public static List<Integer> totalDataCount = new List<Integer>();
    
    @AuraEnabled
    public static String getNameSpacePrefix(){
        return LeaseWareUtils.getNamespacePrefix();
    }
    
    @AuraEnabled 
    public static String getAssetType(String recordId){
        List<Aircraft_Proposal__c> apList = [Select Id, Aircraft__c,Aircraft__r.Aircraft_Type__c from Aircraft_Proposal__c where Id=: recordId];
        if(apList.size() > 0 && apList[0].Aircraft__c != null)
            return apList[0].Aircraft__r.Aircraft_Type__c;
        else
            return '';
    }

    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(String objDetail, string contrfieldApiName,string depfieldApiName) {
        String namespace = getNameSpacePrefix();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(namespace+objDetail); 
        SObject myObj = targetType.newSObject();
        
        Map<String, List<String>> objRes = DependentFilters.getDependentMap(myObj,contrfieldApiName,depfieldApiName);
        return objRes;
    }
    
    @AuraEnabled
    public static List <Integer> getDatePicklist(Integer deliveryDate){
        List <Integer> picklist = new List <Integer>();
        Integer curYear = System.today().year();
        integer yrLimit = curYear + 25;
        if(deliveryDate < curYear) 
            curYear = deliveryDate;
        for(integer i = curYear; i < yrLimit; i++ ){
            picklist.add(i);
        }
        return picklist;
    }
    
    @AuraEnabled
    public static Aircraft_Proposal__c  getAssetDetails(String recordId){
        System.debug('getAssetDetails recordId  '+recordId);
        
        List<Aircraft_Proposal__c> assetAID = [select Asset_Family__c , Engine_Type__c , 
                                              New_Used__c,Delivery_Date_N__c,
                                              Aircraft__c,World_Fleet_Aircraft__c,
                                              World_Fleet_Aircraft__r.AircraftType__c,
                                              Aircraft__r.Aircraft_Type__c,
                                              World_Fleet_Aircraft__r.EngineType__c
                                               from Aircraft_Proposal__c where Id = :recordId];
        if(assetAID.size() > 0){
            return assetAID[0];
        }
        return null;
    }
    
    @AuraEnabled
    public static String getAssetState(String recordId){
        System.debug('getAssetState recordId  '+recordId);
        List<Aircraft_Proposal__c> assetState = [select Asset_Family__c ,New_Used__c
                                               from Aircraft_Proposal__c where Id = :recordId];
        if(assetState.size() > 0) {
            System.debug(' assetState :'+assetState[0].New_Used__c);
            return assetState[0].New_Used__c;
        }
        return null;
    }    
    
    
    @AuraEnabled
    public static List<String> getLessor() {
        try{
            List<WF_Field_Summary__c> fleetList = [ select Manager__c from WF_Field_Summary__c Order by LastModifiedDate DESC limit 1];
            if(fleetList != null && fleetList.size() > 0) {
                WF_Field_Summary__c fleet = fleetList[0];
                
                if(String.isNotBlank(fleet.Manager__c)) {
                    List<String> managerList = fleet.Manager__c.split(','); 
                    Set<String> newSet = new Set<String>();
                    for(String temp: managerList) {
                        newSet.add(temp.replaceFirst('^\\s*', ''));
                    }
                    List<String> lessorList = new List<String>();
                    lessorList.addAll(newSet);
                    lessorList.sort();
                    return lessorList;
                }
            }
        }
        catch(Exception e){system.debug('getLessor: Exception no lessor data '+e);}
        return null; 
    }
    
    @AuraEnabled
    public static List<OrderbookData> getCompetitorsOrderbook(String aircraftType, String engine, String dateOfManufacture, Integer quarterRange, String lessor,String state) {
        System.debug('getCompetitorsOrderbook: aircraftType:'+aircraftType+ ', engine:'+engine+
                     ' dateOfManufacture:'+dateOfManufacture+ ' quarterRange:'+quarterRange+' lessor:'+lessor+ ' state :'+state);
        List<OrderbookData> orderbookList = new List<OrderbookData>();
        List<String> typeList = getAircraftGroup(aircraftType, engine);
        List<String> domList = new List<String>();
        try{
            integer manfDate = Integer.valueOf(dateOfManufacture);
            Integer currentYr = System.Today().year();
            System.debug('getCompetitorsOrderbook '+manfDate + ' ,currentYr:'+currentYr);
            integer minRange = 0;
            if(manfDate - currentYr > 0) 
                minRange = manfDate - currentYr;
            if(minRange > quarterRange)
                minRange = quarterRange;
            
            Integer rangeLimit = (quarterRange + minRange) + 1;
            Integer startDomInt = Integer.valueOf(dateOfManufacture) - minRange;
            String startDom = String.valueOf(startDomInt);
            String rangeDom = startDom;
            String aircraftState = state;
            System.debug('getCompetitorsOrderbook: startDom '+startDom);
            for(integer i = 0; i < rangeLimit; i++) {
                unplacedCount.add(0);
                placedCount.add(0);
                totalDataCount.add(0);
            }
            for(integer i = 0; i < rangeLimit; i++) { 
                domList.add(rangeDom);
                OrderbookData orderbookData = getDataForDOM(typeList,engine, rangeDom,lessor,state, i, rangeLimit);
                orderbookList.add(orderbookData);
                Integer domInt = Integer.valueOf(rangeDom) + 1;
                rangeDom = String.valueOf(domInt);
            }
        }
        catch(Exception e) {
            System.debug('getCompetitorsOrderbook: exception '+e);
            orderbookList.add(getDataForDOM(typeList,engine, dateOfManufacture,lessor,state, 0, 1));
        }
        
        system.debug('getCompetitorsOrderbook: orderbookList size '+orderbookList.size());
        system.debug('getCompetitorsOrderbook: orderbookList '+orderbookList);
        
        return orderbookList;
    }
    
    public static List<String> getAircraftGroup(String aircraftType, String engine){
        
        String queryStr = 'select Type__c type from Aircraft_Type_Map__c where '+
            +'Aircraft_Type__r.Aircraft_Type__c =: aircraftType ';
        if(engine != null)
            queryStr = queryStr + ' and Aircraft_Type__r.Engine_Type__c =: engine';
        queryStr = queryStr + ' group by Type__c ';    
        
        System.debug('getAircraftGroup: query: '+queryStr);
        List<String> typeList = new List<String>();
        typeList.add(aircraftType);
        try{
            List<AggregateResult> aircraftTypeList = database.query(queryStr);
            for(AggregateResult aggRes: aircraftTypeList) 
                typeList.add((String)aggRes.get('type'));
        }
        catch(Exception e) {
            System.debug('getAircraftGroup: Exception '+e);
        }
        System.debug('getAircraftGroup: result: '+typeList);
        return typeList;
    }
    
    //String aircraftState == indicates whether it is a new or Used aircraft 
    public static OrderbookData getDataForDOM(List<String> aircraftTypeList, String engine, String dateOfManufacture, String lessor,String aircraftState, Integer position, Integer totalLimit) {
        system.debug('getDataForDOM: dom '+dateOfManufacture+ ' aircraftTypeList :'+aircraftTypeList+' engine : '+engine+' dateOfManufacture : '+dateOfManufacture+' lessor :'+lessor+' aircraftState : '+aircraftState);
        OrderbookData data = new OrderbookData();
        List<Global_Fleet__c> fleetData = null;
        String status = 'On Order';
        String condition = '';
        String projection = 'select BuildYear__c , Operator__c , Related_Owner__c , Manager__c ,Lease_Expiry_Year__c,'+
            +'AircraftType__c, AircraftVariant__c, EngineType__c, AircraftStatus__c ';
        if(aircraftState == 'New')
            condition = ' from Global_Fleet__c where BuildYear__c = :dateOfManufacture  AND AircraftStatus__c = :status ';
        else if(aircraftState == 'Used')
            condition = ' from Global_Fleet__c where Lease_Expiry_Year__c = :dateOfManufacture  AND AircraftStatus__c != :status ';
        else
            condition = ' from Global_Fleet__c where Lease_Expiry_Year__c = :dateOfManufacture ';
        condition = condition + ' and AircraftType__c in :aircraftTypeList ';
        
        if(engine != null) 
            condition = condition + ' and EngineType__c = :engine ';
        if(lessor != null && !lessor.equalsIgnoreCase('ALL')) 
            condition = condition + ' and Manager__c = :lessor ';
                
        String strQuery = projection + condition + ' and Operator__c = null ';
        System.debug('getDataForDOM: Query to get fleet '+strQuery);
        //unplaced fleet data
        try{
            fleetData = database.query(strQuery);
            System.debug('getDataForDOM: Query fleetData '+fleetData);
        }
        catch(Exception e) {
            //There is no data for the mentioned filter
            System.debug('getDataForDOM: There is no data for the mentioned filter '+e);
        }  
        
        integer unplacedOrder = 0;
        if(fleetData != null)
            unplacedOrder = fleetData.size();
        
        String projectionTotalCount = 'select count(Id) totalCount ';
        String strQueryTotal = projectionTotalCount + condition;
        
        integer totalCount = 0;
        try{
            System.debug('getDataForDOM: Query to get totalCount Order: '+strQueryTotal);
            AggregateResult aggRes = database.query(strQueryTotal);
            totalCount  = (integer)aggRes.get('totalCount');
            System.debug('getDataForDOM: Query to get totalCount Order: '+totalCount +' dateOfManufacture: ' +dateOfManufacture);
        }
        catch(Exception e){ System.debug('getDataForDOM: There is no order for DOM: '+dateOfManufacture + 'Exception: '+e);}
        
        integer placedOrder = totalCount - unplacedOrder;
        if(placedOrder < 0)
            placedOrder = 0;
        System.debug('getDataForDOM: Fleetcount:'+totalCount +' unplacedOrder:'+unplacedOrder+ ' placedOrder:'+placedOrder);
        data.dateOfManufacture = dateOfManufacture;
        
        unplacedCount.set(position, unplacedOrder);
        placedCount.set(position, placedOrder);
        totalDataCount.set(position, totalCount);
        
        if(fleetData != null)
            data.fleetData = groupBasedOnLessor(fleetData, position, totalLimit);
        else 
            data.fleetData  = null;
        //data.fleetData = fleetData;
        data.placedCount = placedOrder;
        data.unplacedCount = unplacedOrder;
        data.totalCount = totalCount;
        return data;
    }
    
    public static List<Fleet> groupBasedOnLessor(List<Global_Fleet__c> globalFleetList, Integer position, Integer totalLimit){
        Map<String, Fleet> fleetMap = new Map<String, Fleet>();
        for(Global_Fleet__c globalFleet : globalFleetList) {
            Fleet fleet;
            if(fleetMap.get(globalFleet.Manager__c) != null) {
                fleet = fleetMap.get(globalFleet.Manager__c);
                fleet.count++;
            }
            else {
                fleet = new Fleet();
                fleet.BuildYear = globalFleet.BuildYear__c;
                fleet.RelatedOwner = globalFleet.Related_Owner__c;
                fleet.Operator = globalFleet.Operator__c;
                fleet.Manager = globalFleet.Manager__c;
                fleet.AircraftType = globalFleet.AircraftType__c;
                fleet.AircraftVariant = globalFleet.AircraftVariant__c;
                fleet.EngineType = globalFleet.EngineType__c;
                fleet.Count = 1; 
                fleet.LeaseExpiryYear = globalFleet.Lease_Expiry_Year__c;
            }
            fleetMap.put(globalFleet.Manager__c,fleet);
        }
        List<Fleet> fleetList = new List<Fleet>();
        for ( String aID : fleetMap.keySet() ){
            fleetList.add(fleetMap.get(aID));
            if(managerSet.get(aID) == null) {
                List<Integer> domcount = new List<Integer>();
                for(integer i=0; i < totalLimit; i++) {
                    domcount.add(0);
                }
                domcount.set(position, fleetMap.get(aID).count);
                managerSet.put(aID, domcount);
            }
            else {
                List<Integer> domcount = managerSet.get(aID);
                domcount.set(position, fleetMap.get(aID).count);
                managerSet.put(aID, domCount);
            }
        }
        fleetList.sort();
        return fleetList;
    }
    
    public class StackedData {
        @AuraEnabled public Map<String, List<Integer>> managerSet;
        @AuraEnabled public List<String> domList;
        @AuraEnabled public List<Integer> unplacedCount;
        @AuraEnabled public List<Integer> placedCount;
        @AuraEnabled public List<Integer> totalDataCount;
    }
    
    public class OrderbookData {
        @AuraEnabled public String dateOfManufacture {get; set;}
        @AuraEnabled public List<Fleet> fleetData {get; set;}
        @AuraEnabled public Integer placedCount {get; set;}
        @AuraEnabled public Integer unplacedCount {get; set;}
        @AuraEnabled public Integer totalCount {get; set;} //is based on lessor filter
    }
    
    public class Fleet  implements Comparable {
        @AuraEnabled public String BuildYear {get; set;}
        @AuraEnabled public String LeaseExpiryYear {get; set;}
        @AuraEnabled public String RelatedOwner {get; set;}
        @AuraEnabled public String Operator {get; set;}
        @AuraEnabled public String Manager {get; set;}
        @AuraEnabled public String AircraftType {get; set;}
        @AuraEnabled public String AircraftVariant {get; set;}
        @AuraEnabled public String EngineType {get; set;}
        @AuraEnabled public integer Count {get; set;}
        
         public Integer compareTo(Object objToCompare) {
            Fleet flt = (Fleet)(objToCompare);
            if(flt == null)
                return 1;
            if (this.Count < flt.Count) 
                return 1;
            if (this.Count == flt.Count) 
                return 0;
            
            return -1;
        }    
        
    }    
}