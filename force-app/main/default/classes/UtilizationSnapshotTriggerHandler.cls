public class UtilizationSnapshotTriggerHandler implements ITrigger {


	//Contructor
	private final string triggerBefore = 'UtilizationSnapshotTriggerHandler';
	private final string triggerAfter = 'UtilizationSnapshotTriggerHandler';

	public void bulkBefore(){
	}

	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkAfter(){
	}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 */
	public void beforeInsert(){
		if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
	}

	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 */
	public void beforeUpdate(){
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 */
	public void beforeDelete(){
		system.debug('UtilizationSnapshotTriggerHandler.beforeDelete(+)');
      
		List<Utilization_Snapshot__c> listOldUtilSnapsht =  (list<Utilization_Snapshot__c>)trigger.old;
		List<Assembly_Utilization_Snapshot_New__c> listToDelete = new List<Assembly_Utilization_Snapshot_New__c> ();

        if(listOldUtilSnapsht.size() > 0) {   
            List<Assembly_Utilization_Snapshot_New__c> listOld = [Select id,Assembly_Lkp__c,Assembly_Lkp__r.IsLLPUpdated__c,Assembly_Lkp__r.Assembly_Utilization_Snapshot_New__c,Utilization_Snapshot__c FROM Assembly_Utilization_Snapshot_New__c WHERE Utilization_Snapshot__c IN :listOldUtilSnapsht];

			Map<Id,List<Assembly_Utilization_Snapshot_New__c>> mapAssemblySnapshot = new Map<Id,List<Assembly_Utilization_Snapshot_New__c>>();

			for(Assembly_Utilization_Snapshot_New__c curSnpsht: listOld){				
				if(mapAssemblySnapshot.keyset().contains(curSnpsht.Utilization_Snapshot__c)){
					List<Assembly_Utilization_Snapshot_New__c> list2  = mapAssemblySnapshot.get(curSnpsht.Utilization_Snapshot__c) ;
					list2.add(curSnpsht);
					mapAssemblySnapshot.put(curSnpsht.Utilization_Snapshot__c,list2);
				}else{
					List<Assembly_Utilization_Snapshot_New__c> list2  = new List<Assembly_Utilization_Snapshot_New__c>();
					list2.add(curSnpsht);
					mapAssemblySnapshot.put(curSnpsht.Utilization_Snapshot__c,list2);
				}
			}	

			for(Utilization_Snapshot__c curSnpsht: listOldUtilSnapsht){
				if(mapAssemblySnapshot.size()>0 && mapAssemblySnapshot.get(curSnpsht.Id) != null){
					for(Assembly_Utilization_Snapshot_New__c curRec: mapAssemblySnapshot.get(curSnpsht.Id)){
						if(curRec.Assembly_Lkp__c != null){
							System.debug('Assembly:' + curRec.Assembly_Lkp__c + ':' + curRec.Assembly_Lkp__r.IsLLPUpdated__c);
							if(curRec.Assembly_Lkp__r.IsLLPUpdated__c && (curRec.Assembly_Lkp__r.Assembly_Utilization_Snapshot_New__c == curRec.Id )){
								curSnpsht.addError('Cannot delete Assembly Utilization Snapshot because LLP Utilization records exists. Please reset them manually and proceed');
							}else{
								listToDelete.add(curRec);
							}
						}	
					}
				}
			}
			if(listToDelete.size()>0){
				delete listToDelete; 
			}	              
		}   		
		system.debug('UtilizationSnapshotTriggerHandler.beforeDelete(-)');
	}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 */
	public void afterInsert(){
	}

	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 */
	public void afterUpdate(){
	}

	public void afterDelete(){
	}

	/**
	 * afterUnDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */
	public void afterUnDelete(){
	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */

	public void andFinally(){
	}

	/* Function to update the Y_Hidden key field used in the Matching Rule to avoid duplicate Adjusted Interval(Ratio) records.
	   Its being called during Before Insert/Update */

}