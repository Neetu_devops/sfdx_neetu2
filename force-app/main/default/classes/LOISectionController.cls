public class LOISectionController {
    
    @AuraEnabled 
    public static String getDealId(String recordId){
        if(ID.valueOf(recordId).getSobjectType().getDescribe().getName() == leasewareutils.getNamespacePrefix() +'Pricing_Run_New__c'){
            for(Pricing_Run_New__c dealAnalysis : [SELECT Id, Marketing_Activity__c, Marketing_Activity__r.Name FROM Pricing_Run_New__c WHERE ID =: recordId 
                                                   AND Marketing_Activity__c != NULL]){
                                                       recordId = JSON.serialize(dealAnalysis.Marketing_Activity__r);
                                                       break;
                                                   }
            return recordId;
        }
        for (Marketing_Activity__c ma : [SELECT Id, Name FROM Marketing_Activity__c WHERE Id =: recordId]) {
            recordId = JSON.serialize(ma);
            break;
        }
        return recordId;
    }
    //To cehck the recordId Prefix to detect the Id is of Commercial Terms or Deal
    @AuraEnabled
    public static LOISectionWrapper getRecordIdPrefix(Id dealId){
        String keyPrefix = Schema.getGlobalDescribe().get(leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c').getDescribe().getKeyPrefix();
        LOISectionWrapper loiwrapper = new LOISectionWrapper();
        if(keyPrefix == String.valueOf(dealId).left(3)){
            List<Pricing_Run_New__c> lstDealAnalysis = [SELECT Id, Name FROM Pricing_Run_New__c WHERE ID =: dealId];
            loiwrapper.dealanalysisName = lstDealAnalysis[0].Name;
        }else{
            loiwrapper.dealanalysisName = '';
        }
        loiwrapper.prefix = keyPrefix;
        return loiwrapper;
    }
    
    @AuraEnabled
    public static List<AssetTermOnDealAnalysisController.AssetTermWrapper> getAssetTypeForSelectedDealAnalysis(Id dealAnalysisId){
        return AssetTermOnDealAnalysisController.getDealAnalysisData(dealAnalysisId);
    }
    
    @AuraEnabled
    public static List<AircraftWrapper> getManagedAssetType(Id recId){
        
        LOISectionWrapper loiwrapper  = getRecordIdPrefix(recId);
        String prefix = loiwrapper.prefix;
        List<AircraftWrapper> lstManagedAssetType = new List<AircraftWrapper>();
        System.debug('recordId'+recId+'***'+prefix);
        // commercial term Id
        if(recId != null){
            
            if(prefix == String.valueOf(recId).left(3)){ // commercial term edit case
                for(Pricing_Output_New__c  ea : [Select id,Asset_Term__c,Asset_Term__r.Name from Pricing_Output_New__c
                                                 where Pricing_Run__c =: recId and Asset_Term__c != null]){
                                                     
                                                     AircraftWrapper aw = new AircraftWrapper(ea.Asset_Term__c,ea.Asset_Term__r.Name);
                                                     lstManagedAssetType.add(aw);
                                                 } 
                
            }
            else{ // deal case
                for(Aircraft_Proposal__c ap : [SELECT Id, Name FROM Aircraft_Proposal__c WHERE 
                                               Marketing_Activity__c =: recId AND Deal_Analysis__c = NULL]){
                                                   AircraftWrapper aw = new AircraftWrapper(ap.Id,ap.Name);
                                                   lstManagedAssetType.add(aw);
                                                   System.debug('lstManagedAssetTypeINN-->>>'+lstManagedAssetType);
                                               }
            }
        }
        
        System.debug('lstManagedAssetType-->>>'+lstManagedAssetType);
        return lstManagedAssetType;
    }
    
    @AuraEnabled
    public static String saveEconoicAnalysis(Id dealId,String dealName,List<Id> assetIdLst){
        String msg ='';
        String errorMsg ='';
        System.debug('dealId'+dealId+'dealName'+dealName+'assetIdLst'+assetIdLst);
        try{
            // anjani : updating recordType id for Commercial Terms record.	
            /*Pricing_Run_New__c DealAnRec = [select id,Marketing_Activity__c from Pricing_Run_New__c where id = :dealId];
DealAnRec.RecordTypeId = getDealAnalysisRecTypeId(DealAnRec.Marketing_Activity__c);
Update DealAnRec;
system.debug('DealAnRec.RecordTypeId==' + DealAnRec.RecordTypeId);*/
            
            
            List<Pricing_Output_New__c> lstEconomicAnalysis = new List<Pricing_Output_New__c>();
            
            Map<Id,Pricing_Output_New__c> mapEconomicAnalysis = new Map<Id,Pricing_Output_New__c>([SELECT Id,Asset_Term__c FROM Pricing_Output_New__c WHERE Pricing_Run__c =: dealId]); 
            Set<Id> existingAssetTerms = new Set<Id>();
            
            Set<Id> addAssetTermIds = new Set<Id>();
            Set<Id> removedAssetTermIds = new Set<Id>(); 
            
            if (mapEconomicAnalysis.size() == 0 ){ // if there is no existing records.
                for(Id assetId : assetIdLst ){
                    addAssetTermIds.add( assetId ); 
                }
            }
            else {
                for(Pricing_Output_New__c economicAnalysis : mapEconomicAnalysis.values()){
                    existingAssetTerms.add(economicAnalysis.Asset_Term__c); // 1,2
                } 
                
                for(Id assetId : assetIdLst ){
                    if (!existingAssetTerms.contains(assetId)){ // if assetId is NOT in existing AssetId
                        addAssetTermIds.add( assetId ); // 1, 3
                    }
                }
                
                for (Id assetId : existingAssetTerms){
                    if (!assetIdLst.contains(assetId)){
                        removedAssetTermIds.add(assetId);
                    }
                }
            }
            system.debug('addAssetTermIds' + addAssetTermIds);
            system.debug('removedAssetTermIds' + removedAssetTermIds);
            system.debug('existingAssetTerms' + existingAssetTerms);
            
            
            
            //Insert NULL To Commercial Terms lookup
            List<Aircraft_Proposal__c> lstAssetTerm = new List<Aircraft_Proposal__c>(); 
            
            if(removedAssetTermIds.size()>0){
                for(Aircraft_Proposal__c assetTerm : [SELECT Id, Deal_Analysis__c FROM Aircraft_Proposal__c
                                                      WHERE ID IN: removedAssetTermIds]){
                                                          
                                                          assetTerm.Deal_Analysis__c = null;             
                                                          lstAssetTerm.add(assetTerm); 
                                                          
                                                      }
                if(lstAssetTerm.size()>0){
                    update lstAssetTerm;
                }
                DELETE [SELECT Id,Asset_Term__c FROM Pricing_Output_New__c WHERE Pricing_Run__c =: dealId AND Asset_Term__c in :removedAssetTermIds]; 
            }
            
            
            for(Id ap : addAssetTermIds){
                Pricing_Output_New__c ecomomicAnalysis = new Pricing_Output_New__c();
                ecomomicAnalysis.Name = dealName;
                ecomomicAnalysis.Pricing_Run__c = dealId;
                ecomomicAnalysis.Asset_Term__c = ap ;
                lstEconomicAnalysis.add(ecomomicAnalysis);
            }
            
            if(lstEconomicAnalysis.size() >0 )
                insert lstEconomicAnalysis;
            
            
            msg ='Ecomoic Analysis Records are created';
            return msg;
        }catch(Exception e){
            System.debug('Exception'+e.getMessage());
            errorMsg = e.getMessage();
            return errorMsg;
        }
        
    }
    
    @AuraEnabled
    public static id getDealAnalysisRecTypeId(string MAID){
        Marketing_Activity__c[] marec = [select id,RecordType.Name from Marketing_Activity__c where id = :MAID];
        system.debug('marec='+marec[0].RecordType.Name);
        Id IdRecType;
        try{
            IdRecType =  Schema.SObjectType.Pricing_Run_New__c.getRecordTypeInfosByName().get(marec[0].RecordType.Name).getRecordTypeId();    
        }catch(Exception ex){
            //If error, set for Lease
            IdRecType =  Schema.SObjectType.Pricing_Run_New__c.getRecordTypeInfosByName().get('Lease').getRecordTypeId();   
        }
        system.debug('IdRecType='+IdRecType);
        return IdRecType;
    }    
    // recordId is Marketting activity record Id
    @AuraEnabled 
    public static List<LayoutSection> getPageLayoutFields(string recordId) {
        String componentRecordName = ID.valueOf(recordId).getSobjectType().getDescribe().getName();
        LOISectionWrapper loiwrapper  = getRecordIdPrefix(recordId);
        Boolean errorMsg = false;
        String prefix = loiwrapper.prefix;
        if(recordId != null){
            if(prefix == String.valueOf(recordId).left(3)){
                List<Pricing_Run_New__c> dealAnalysis = [SELECT 
                                                         Id,
                                                         Marketing_Activity__c 
                                                         FROM 
                                                         Pricing_Run_New__c 
                                                         WHERE 
                                                         ID =: recordId 
                                                         AND 
                                                         Marketing_Activity__c != NULL 
                                                         LIMIT 1];
                recordId = dealAnalysis[0].Marketing_Activity__c;
            }
        }
        
        system.debug('getPageLayoutFields recordId='+recordId);
        Id IdRecType = getDealAnalysisRecTypeId(recordId);
        system.debug('getPageLayoutFields IdRecType='+IdRecType);
        String Layoutname ;
        String packageType;
        String objectName = leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c';
        
        //Finding PageLayout name for given RecordTypeId for current user
        if(!Test.isRunningTest()){
            try{
                
                String objectId = [Select Id, DurableId from EntityDefinition where QualifiedApiName =: objectName  ].DurableId;
                
                layoutname =  LeaseWareStreamUtils.getLayoutNameForCurrentUserProfile(IdRecType);
                
                String body =  LeaseWareStreamUtils.toolingAPISOQL('Select ManageableState from Layout where TableEnumOrId=\'' + objectId + '\' AND Layout.Name=\'' + layoutname + '\''); 
                packageType = body.substringBetween('"ManageableState":"', '"');
                
            }catch(System.CalloutException exp) {
                //leasewareUtils.createExceptionLog(exp,exp.getMessage(), null, null, null, true);
                throw new AuraHandledException('API error, please contact your LeaseWorks administrator [Exception: ' + exp.getMessage() + ']');
            }
            
            if(packageType == 'unmanaged') {
                layoutname = objectName + '-' + layoutname; 
            }
            else {
                layoutname = objectName + '-' + leasewareutils.getNamespacePrefix() + layoutname; 
            }     
        }
        else {
            layoutname = leasewareutils.getNamespacePrefix() +'Pricing_Run_New__c-' + leasewareutils.getNamespacePrefix() + 'Economic Analysis Request - MA Layout'; 
        }
        
        
	/*if(!Test.isRunningTest()){
	try{
	    Layoutname =  LeaseWareStreamUtils.getLayoutNameForCurrentUserProfile(IdRecType); 
	}catch(Exception ex){
	    system.debug('Unexpected error while calling Tooling API using Named crdential' +ex);
	    leasewareUtils.createExceptionLog(ex, 'Exception while trying to get Layout Name using Tooling API call', null, null,null,true);
	}
	if(Layoutname!=null) 
	    Layoutname = leasewareutils.getNamespacePrefix() + 'Pricing_Run_New__c-' + leasewareutils.getNamespacePrefix() + Layoutname;

	}else{
	    Layoutname = leasewareutils.getNamespacePrefix() +'Pricing_Run_New__c-' + leasewareutils.getNamespacePrefix() + 'Economic Analysis Request - MA Layout'; 
	} */
            
        system.debug('getPageLayoutFields Layoutname='+Layoutname);
        
        // You can give multiple page layout names here as well
        List<String> componentNameList = new List<String>{Layoutname};
            // Retrieve page layout details 
            List<Metadata.Metadata> components = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, componentNameList);
        List <LayoutSection> lstSections =new List<LayoutSection>();
        if(!components.isEmpty()){
            Metadata.Layout economicLayout = (Metadata.Layout) components.get(0);
            
            // We are going to find the fields names and will keep them according to columns so 
            // we can show them as per page layout 
            for( Metadata.LayoutSection ls : economicLayout.layoutSections ) {
                
                LayoutSection section = new LayoutSection( ls.layoutColumns.size() );
                section.label = ls.label;
                section.editHeading = ls.editHeading; //LW:AKG : 01/19/2021 : Store section header visibility on the edit screen.

                System.debug('Name****** '+section.label);
                List<LayoutColumn> lstColumns = new List<LayoutColumn>();
                Integer maxFieldsInColumn = 0;
                for( Metadata.LayoutColumn lc : ls.layoutColumns ) {
                    
                    LayoutColumn column = new LayoutColumn();
                    // check if there are fields available in that column
                    if( lc.layoutItems != null ) { 
                        // Get the max number of fields in a column to preserve the alignment 
                        if( maxFieldsInColumn < lc.layoutItems.size() ) {
                            maxFieldsInColumn = lc.layoutItems.size();
                        }
                        for( Metadata.LayoutItem li : lc.layoutItems ) {
                            system.debug('getPageLayoutFields *************** section.label '+section.label+' : '+li + ' , li.field '+li.field);
                            //Saranya: 5/2/21 added as a part of https://app.asana.com/0/1181547498580321/1129455770860566/f
                            //We are showing readonly fields in case of editing the record not on creation.
                            if(componentRecordName == leasewareutils.getNamespacePrefix() +'Pricing_Run_New__c') {
                            	column.lstFields.add( new LayoutField( li ) );
                            } 
                            //LW:AKG : 01/19/2021 : Added a check to include Parent field even if it is Real Only on Page layout.
                            else if(li.behavior != Metadata.UiBehavior.ReadOnly || li.field == leasewareutils.getNamespacePrefix() + 'Marketing_Activity__c'){
                                // Pass the LayoutItem object in the LayoutField consturctor
                                column.lstFields.add( new LayoutField( li ) );
                            } 
                        }
                    }
                    // No need to add a column in the section if there is no field available 
                    if( column.lstFields.size() > 0 ) {
                        lstColumns.add( column );
                    }
                }
                
                // Now, we need to arrange the fields in section so we can use them in the iteration 
                // on the component so we will have to arrange them in the order 
                if( maxFieldsInColumn > 0 ) {
                    for( Integer i = 0; i < maxFieldsInColumn; i++ ) {
                        for( Integer j = 0; j < lstColumns.size(); j++ ){
                            if( lstColumns[j].lstFields.size() > i ) {
                                section.lstFields.add( lstColumns[j].lstFields[i] );    
                            }    
                            else {
                                section.lstFields.add( new LayoutField() );
                            }
                        }    
                    }    
                }
                Leasewareutils.insertExceptionLogs();
                System.debug('lstSections');
                //If there is no Column in the Section dont not show the header/section in the layout 
                if(section != null && section.lstFields != null && section.lstFields.size() > 0) {
                	lstSections.add( section );
                }
            }
        }
        else{
            lstSections = null;
        }
        return lstSections;
    }
    
    public class LOISectionWrapper{
        @AuraEnabled public String dealanalysisName{get; set;}
        @AuraEnabled public String prefix{get; set;}
        public LOISectionWrapper(){
            this.dealanalysisName = dealanalysisName;
            this.prefix = prefix;
        }
    }
    
    public class AircraftWrapper{
        @AuraEnabled public Id Id{get; set;}
        @AuraEnabled public String Name{get; set;}
        public AircraftWrapper(Id Id,String Name ){
            this.Id = Id;
            this.Name = Name;
        }
    }
    
    public class LayoutSection {
        @AuraEnabled public String label;
        @AuraEnabled public Boolean editHeading; //LW:AKG : 01/19/2021 : Attribute to hold section header visibility on the edit screen.
        @AuraEnabled public List<LayoutField> lstFields;
        @AuraEnabled public Integer totalColumns;
        public LayoutSection( Integer totalColumns ) {
            this.totalColumns = totalColumns;
            this.lstFields = new List<LayoutField>();
        }
    }
    
    private class LayoutColumn {
        private List<LayoutField> lstFields;    
        public LayoutColumn() {
            this.lstFields = new List<LayoutField>();
        }
    }
    
    public class LayoutField {
        @AuraEnabled public String fieldName;
        @AuraEnabled public Boolean isRequired;
        @AuraEnabled public Boolean isReadOnly;
        
        public LayoutField() {}
        
        public LayoutField( Metadata.LayoutItem li ) {
            
            system.debug('this.fieldName>>' + li.field + '----'+ Leasewareutils.getNamespacePrefix());
            this.fieldName = li.field;
            if (Leasewareutils.getNamespacePrefix() == 'lwdev__'){
                if (li.field != null && li.field.endswith('__c')){
                    this.fieldName = Leasewareutils.getNamespacePrefix() + li.field;
                }
                
            }
            
            
            if( li.behavior == Metadata.UiBehavior.Required ) {
                this.isRequired = true;
            }
            else if( li.behavior == Metadata.UiBehavior.ReadOnly ) {
                this.isReadOnly = true;
            }
        }
    }   
    
}