@isTest
public class TestMaintenenceProgUpdateController {
    
     
    static testMethod void TestMPChangeCase1(){
        Date dateField = System.today(); 
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
         // Operator Creation
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        insert operator;
        
        Lease__c leaseRecord = new Lease__c(Name='testLease', Operator__c=operator.Id, 
                                            UR_Due_Day__c='10', Lease_Start_Date_New__c=Date.parse('01/01/2010'),
                                            Lease_End_Date_New__c=Date.parse('12/31/2012'));
        insert leaseRecord;

        // Set up Maintenance Program
		Maintenance_Program__c mp = new Maintenance_Program__c(name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        // Set up the Aircraft record
		String aircraftMSN = '12345';
        Aircraft__c ac = new Aircraft__c(MSN_Number__c=aircraftMSN, Aircraft_Type__c='737', Date_of_Manufacture__c = System.today(),Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, 
            Threshold_for_C_Check__c=1000, Maintenance_Reserve_Heavy_Maintenance_1__c=10000, Maintenance_Reserve_Heavy_Maintenance_2__c=20000,
            Maintenance_Reserve_Engine_1__c=30299, Maintenance_Reserve_Engine_1_LLP__c=400000,  
            Maintenance_Reserve_Engine_2__c=30299, Maintenance_Reserve_Engine_2_LLP__c=400000,  
            Maintenance_Reserve_Engine_3__c=30299, Maintenance_Reserve_Engine_3_LLP__c=400000,  
            Maintenance_Reserve_Engine_4__c=30299, Maintenance_Reserve_Engine_4_LLP__c=400000,
            Maintenance_Reserve_LG_Left_Main__c=35700, Maintenance_Reserve_APU__c = 34290,Maintenance_Program__c = mp.Id,  
            Status__c='Available');
        LeaseWareUtils.clearFromTrigger();
        insert ac;
        
         //update lease reocrd with aircraft
        leaseRecord.Aircraft__c=ac.id;
        LeaseWareUtils.clearFromTrigger();
        update leaseRecord;

        map<String,Object> mapInOut = new map<String,Object>();
        TestLeaseworkUtil.createTSLookup(mapInOut);
        
        Id idRecordType=Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Airframe').getRecordTypeId();
        
		Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='ca test name', Attached_Aircraft__c= ac.Id, Description__c ='test description', TSN__c=0, CSN__c=0, Type__c ='Airframe', Serial_Number__c='123',RecordTypeId = idRecordType);
        System.debug('consttAssembly '+consttAssembly.RecordTypeId);
        LeaseWareUtils.clearFromTrigger();
		insert consttAssembly;
		
		
		
		Maintenance_Program_Event__c mpe= new Maintenance_Program_Event__c(
            Assembly__c = 'Airframe',
            Maintenance_Program__c= mp.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = '4C/6Y'
        );
        insert mpe;
		
		
		// Set up Project Event for Airframe
        Assembly_Event_Info__c event = new Assembly_Event_Info__c();
        event.Name = 'Test Info';
        event.Event_Cost__c = 20;
        event.Constituent_Assembly__c = consttAssembly.Id;
        event.Maintenance_Program_Event__c = mpe.Id;
        insert event;
        
        Assembly_MR_Rate__c assemblyRecord = new Assembly_MR_Rate__c(
            Name='Testing',
            Assembly_Lkp__c =consttAssembly.Id,
            Assembly_Event_Info__c=event.id,
            Base_MRR__c = 200,
			Rate_Basis__c='Hours',
            Lease__c = leaseRecord.Id
        );
        insert assemblyRecord;
        
        MR_Share__c mrShareRecord = new MR_Share__c(
            Name='Test MR Share',
            Assembly_MR_Info__c= assemblyRecord.Id,
            Assembly__c='Engine 1',
            Maintenance_Event_Name__c = 'Refurbishment'
            
        );
        insert mrShareRecord;
        
        
        MaintenenceProgUpdateController.getMPName(ac.Id);
      MaintenenceProgUpdateController.getAssetName(ac.Id);  
      MaintenenceProgUpdateController.getTable1Data(ac.Id);
        // New MP selected.
        Maintenance_Program__c mp1 = new Maintenance_Program__c(
            name='TestNew',Current_Catalog_Year__c='2020');
        insert mp1;
        
        
        //Assembly 1 MPE : Overhaul
        Maintenance_Program_Event__c mpe1 = new Maintenance_Program_Event__c(
            Assembly__c = 'Landing Gear - Main',
            Maintenance_Program__c= mp1.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Overhaul'
        );
        insert mpe1;
        
        MaintenenceProgUpdateController.ftechMPEList(mp1.Id);
        MaintenenceProgUpdateController.getTable2Data(mp1.Id, ac.Id);
        MaintenenceProgUpdateController.changeMPonAsset(mp1.Id, ac.Id);
        MaintenenceProgUpdateController.getNamespacePrefix();
    }
    
    
    static testMethod void TestMP2Change() {

        //Lessor Creation
        map<String,Object> mapInOut = new map<String,Object>();
        mapInOut.put('Aircraft__c.lessorName','LW');   
        TestLeaseworkUtil.createLessor(mapInOut);

        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');    
        Test.startTest();
        
        // Operator Creation
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        insert operator;

        // Lease Creation
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10', Lease_Start_Date_New__c=Date.parse('01/01/2010'),Lease_End_Date_New__c=Date.parse('12/31/2012'));
        insert lease;

        // Set up Maintenance Program1
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        //Asset Creation
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN, Aircraft_Type__c='737', Date_of_Manufacture__c = System.today(),Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, 
            Threshold_for_C_Check__c=1000, Maintenance_Reserve_Heavy_Maintenance_1__c=10000, Maintenance_Reserve_Heavy_Maintenance_2__c=20000,
            Maintenance_Reserve_Engine_1__c=30299, Maintenance_Reserve_Engine_1_LLP__c=400000,  
            Maintenance_Reserve_Engine_2__c=30299, Maintenance_Reserve_Engine_2_LLP__c=400000,  
            Maintenance_Reserve_Engine_3__c=30299, Maintenance_Reserve_Engine_3_LLP__c=400000,  
            Maintenance_Reserve_Engine_4__c=30299, Maintenance_Reserve_Engine_4_LLP__c=400000,
            Maintenance_Reserve_LG_Left_Main__c=35700, Maintenance_Reserve_APU__c = 34290,Maintenance_Program__c = mp.Id,  
            Status__c='Available');
        insert a;
        
        LeaseWareUtils.TriggerDisabledFlag=false;
        //  Assembly 1:Landing Gear:Left
        TestLeaseworkUtil.createTSLookup(mapInOut);
        Id idRecordType=Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Landing_Gear').getRecordTypeId();
        
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='testLG', Aircraft_Type__c='737-300',
         Aircraft_Engine__c='737-300 @ CFM56', Engine_Model__c ='CFM56', Attached_Aircraft__c= a.Id, RecordTypeId = idRecordType,
         Description__c ='testtt', TSN__c=0, CSN__c=0, Type__c ='Landing Gear - Left Main', Serial_Number__c='123',
         Engine_Thrust__c='-3B1');
        insert consttAssembly;
        
        		
        //Assembly 1 MPE : Overhaul
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Landing Gear - Main',
            Maintenance_Program__c= mp.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Overhaul'
        );
        insert mpe;
                //Assembly 1 MPE : Overhaul
        Maintenance_Program_Event__c mpe4 = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'LLP Replacement'
        );
        insert mpe4;
        
        //Assembly 1 Projected Event: Overhaul
        Assembly_Event_Info__c eventLG = new Assembly_Event_Info__c();
        eventLG.Name = 'Test Info';
        eventLG.Event_Cost__c = 20;
        eventLG.Constituent_Assembly__c = consttAssembly.Id;
        eventLG.Maintenance_Program_Event__c = mpe.Id;
        insert eventLG;
        
                
		//Assembly 1 : Historical Event : Overhaul
        Assembly_Eligible_Event__c aEE= new Assembly_Eligible_Event__c( Projected_Event__c =eventLG.id,
            Constituent_Assembly__c=consttAssembly.Id, Start_Date__c=dateStart, End_Date__c=dateEnd,  TSN__c = 100, CSN__c = 100);
        insert aEE;
        
      MaintenenceProgUpdateController.getMPName(a.Id);
      MaintenenceProgUpdateController.getAssetName(a.Id);  
      MaintenenceProgUpdateController.getTable1Data(a.Id);
       
        // New MP selected.
        Maintenance_Program__c mp1 = new Maintenance_Program__c(
            name='TestNew',Current_Catalog_Year__c='2020');
        insert mp1;
        
        
        //Assembly 1 MPE : Overhaul
        Maintenance_Program_Event__c mpe1 = new Maintenance_Program_Event__c(
            Assembly__c = 'Landing Gear - Main',
            Maintenance_Program__c= mp1.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Overhaul'
        );
        insert mpe1;
        
        Maintenance_Program_Event__c mpe2 = new Maintenance_Program_Event__c(
            Assembly__c = 'Landing Gear - Main',
            Maintenance_Program__c= mp1.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Unplanned Event'
        );
        insert mpe2;
        
        
        Maintenance_Program_Event__c mpe3 = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp1.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe3;
        
        MaintenenceProgUpdateController.ftechMPEList(mp1.Id);
        MaintenenceProgUpdateController.getTable2Data(mp1.Id, a.Id);
        MaintenenceProgUpdateController.changeMPonAsset(mp1.Id, a.Id);
        MaintenenceProgUpdateController.getNamespacePrefix();
    
    

}
    
    
    static testMethod void TestMP3Change() {

        //Lessor Creation
        map<String,Object> mapInOut = new map<String,Object>();
        mapInOut.put('Aircraft__c.lessorName','LW');   
        TestLeaseworkUtil.createLessor(mapInOut);

        Date dateStart = Date.parse('01/30/2009');
        Date dateEnd = Date.parse('02/26/2009');    
        Test.startTest();
        
        // Operator Creation
        String operatorName = 'TestThisAirline';
        Operator__c operator = new Operator__c(Name=operatorName, Status__c='Approved'); 
        insert operator;

        // Lease Creation
        Lease__c lease = new Lease__c(Name='testLease', Operator__c=operator.Id, UR_Due_Day__c='10', Lease_Start_Date_New__c=Date.parse('01/01/2010'),Lease_End_Date_New__c=Date.parse('12/31/2012'));
        insert lease;

        // Set up Maintenance Program1
		Maintenance_Program__c mp = new Maintenance_Program__c(
            name='Test',Current_Catalog_Year__c='2020');
        insert mp;
        
        //Asset Creation
        String aircraftMSN = '12345';
        Aircraft__c a = new Aircraft__c(MSN_Number__c=aircraftMSN, Aircraft_Type__c='737', Date_of_Manufacture__c = System.today(),Aircraft_Variant__c='300',TSN__c=0, CSN__c=0, 
            Threshold_for_C_Check__c=1000, Maintenance_Reserve_Heavy_Maintenance_1__c=10000, Maintenance_Reserve_Heavy_Maintenance_2__c=20000,
            Maintenance_Reserve_Engine_1__c=30299, Maintenance_Reserve_Engine_1_LLP__c=400000,  
            Maintenance_Reserve_Engine_2__c=30299, Maintenance_Reserve_Engine_2_LLP__c=400000,  
            Maintenance_Reserve_Engine_3__c=30299, Maintenance_Reserve_Engine_3_LLP__c=400000,  
            Maintenance_Reserve_Engine_4__c=30299, Maintenance_Reserve_Engine_4_LLP__c=400000,
            Maintenance_Reserve_LG_Left_Main__c=35700, Maintenance_Reserve_APU__c = 34290,Maintenance_Program__c = mp.Id,  
            Status__c='Available');
        insert a;
            
        //  Assembly 1:Landing Gear:Left
        TestLeaseworkUtil.createTSLookup(mapInOut);
        Id idRecordType=Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName().get('Landing_Gear').getRecordTypeId();
        
        Constituent_Assembly__c consttAssembly = new Constituent_Assembly__c(Name='testLG', Aircraft_Type__c='737-300',
         Aircraft_Engine__c='737-300 @ CFM56', Engine_Model__c ='CFM56', Attached_Aircraft__c= a.Id, RecordTypeId = idRecordType,
         Description__c ='testtt', TSN__c=0, CSN__c=0,Type__c ='Landing Gear - Left Main', Serial_Number__c='123',
         Engine_Thrust__c='-3B1');
        insert consttAssembly;
		
		
        //Assembly 1 MPE : Overhaul
        Maintenance_Program_Event__c mpe = new Maintenance_Program_Event__c(
            Assembly__c = 'Landing Gear - Main',
            Maintenance_Program__c= mp.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Overhaul'
        );
        insert mpe;
        
      MaintenenceProgUpdateController.getMPName(a.Id);
      MaintenenceProgUpdateController.getAssetName(a.Id);  
      MaintenenceProgUpdateController.getTable1Data(a.Id);
       
        // New MP selected.
        Maintenance_Program__c mp1 = new Maintenance_Program__c(
            name='TestNew',Current_Catalog_Year__c='2020');
        insert mp1;
        
        
        //Assembly 1 MPE : Overhaul
        Maintenance_Program_Event__c mpe1 = new Maintenance_Program_Event__c(
            Assembly__c = 'Landing Gear - Main',
            Maintenance_Program__c= mp1.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Overhaul'
        );
        insert mpe1;
        
        Maintenance_Program_Event__c mpe2 = new Maintenance_Program_Event__c(
            Assembly__c = 'Landing Gear - Main',
            Maintenance_Program__c= mp1.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Unplanned Event'
        );
        insert mpe2;
        
        
        Maintenance_Program_Event__c mpe3 = new Maintenance_Program_Event__c(
            Assembly__c = 'Engine',
            Maintenance_Program__c= mp1.Id,
            Interval_2_Hours__c = 12000,
            Event_Type_Global__c = 'Performance Restoration'
        );
        insert mpe3;
        
        MaintenenceProgUpdateController.ftechMPEList(mp1.Id);
        MaintenenceProgUpdateController.getTable2Data(mp1.Id, a.Id);
        MaintenenceProgUpdateController.changeMPonAsset(mp1.Id, a.Id);
        MaintenenceProgUpdateController.getNamespacePrefix();
    
    

}
    
}