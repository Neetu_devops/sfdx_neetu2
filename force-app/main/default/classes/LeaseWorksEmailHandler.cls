//@last updated by : Arjun : 2016.12.14 1 PM
global class LeaseWorksEmailHandler implements Messaging.InboundEmailHandler 
{
  private set<Id> fetchAirlines( Set<string> params )
  {
      set<Id> airlineIds = new set<Id>();
      list<Operator__c> operators = new list<Operator__c> ();
      operators = [ SELECT Id,Name,Website__c,Alias__c FROM Operator__c Limit:Limits.getLimitDmlRows() ];

      for( string param : params )  
      {
          if( param!=null && param!='' )
          {                                                                   
              for( Operator__c operator : operators )
              {
                  if( param.containsIgnoreCase( operator.Name )  )
                      airlineIds.add( operator.Id );
                  else  if( operator.Website__c!=null 
                            && ( param.containsIgnoreCase( operator.Website__c ) || operator.Website__c.containsIgnoreCase( param ) ) ) 
                            airlineIds.add( operator.Id );
                  else  if( operator.Alias__c!=null )
                  {
                      list<string> aliasNames = operator.Alias__c.split(',');
                      boolean aliasNameMatch = false;
                      for( String aliasnName : aliasNames )
                      {
                      if( param.containsIgnoreCase( aliasnName ) )  
                        aliasNameMatch = true;  
                      }                    
                      if( aliasNameMatch )
                      airlineIds.add( operator.Id );
                   }  
              } 
            }
      }
      system.debug('airlineIds===='+airlineIds);
      
      return airlineIds;
  }  
  
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
  {
    Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    
    string subject,lessorDomain;
    list<string> emailIds     = new list<string>();
    Set<string> emaildomains = new set<string>();
    set<Id> airlines         = new set<Id>();
    Id selectedOperatorId    = null;
    subject = lessorDomain = '';
        
    subject = email.subject; 
    system.debug('envelope.fromAddress==='+envelope.fromAddress);
    system.debug('envelope.ToAddress==='+envelope.ToAddress);
    system.debug('email.fromAddress==='+email.fromAddress);
    system.debug('email.ccAddresses==='+email.ccAddresses);
    system.debug('email.toAddresses==='+email.toAddresses);
    
    if( email.toAddresses!=null ) {
        emailIds.addAll( email.toAddresses );
    }   
    if( email.ccAddresses!=null )   {
        emailIds.addAll( email.ccAddresses );
    }   
    
    try    {    
        list<Customer_Interaction_Log__c>  interactionLogs = new list<Customer_Interaction_Log__c>();

        List<Address__c> addressLookups = new List<Address__c>();
        addressLookups = [ SELECT Name,Email_Address__c,Id,Operator__c
                              FROM Address__c 
                              WHERE Email_Address__c in :emailIds ];
                              
        Lessor__c lessor = [ SELECT Id,Email_Address__c FROM Lessor__c limit 1 ]; 
        
        if( lessor.Email_Address__c!=null && lessor.Email_Address__c!='' )
            lessorDomain = lessor.Email_Address__c.split('@')[1];
        
        System.debug('addressLookups=== '+addressLookups+'   '+lessorDomain+'    '+subject);
        
        //airlines matching Name,website or operator within subject.
        airlines = fetchAirlines( new Set<string>{ subject } );
                        
        system.debug('airlines=== '+airlines);
        
        for( string emailTmp : emailIds )
        {
            Integer index = emailTmp.indexOf('@');
            string emailDomain = emailTmp.substring(index+1,emailTmp.length());
            emaildomains.add( emailDomain );
        }
        
        Set<Id> airlinesMatchingDomain = new Set<Id>();
        airlinesMatchingDomain = fetchAirlines( emaildomains );
        airlines.addAll( airlinesMatchingDomain );
        
    if( subject.length()>30 )
      subject = subject.substring(0,30)+'..';
    
    //string nameStr = subject+'  '+email.fromName + ' (' + DateTime.now() + ')';
    string nameStr = subject;
    
    string fromAddress = 'From: '+email.fromName +' ('+email.fromAddress+') ';
    string toAddress = 'To: '+email.toAddresses;
    string ccAddress = 'CC: ';
    if( email.ccAddresses!=null )
        ccAddress = ccAddress + email.ccAddresses;
    
    string emailDate = 'Date:'+ ' (' + DateTime.now() + ')';
    
    //making emailBody
    string emailBody = fromAddress + '\n';
    emailBody = emailBody  + toAddress + '\n';
    emailBody = emailBody  + ccAddress + '\n';
    emailBody = emailBody  + emailDate + '\n';
    emailBody = emailBody  + '\n'+ email.plainTextBody; 
    
    if( nameStr.length()>80 )
      nameStr = nameStr.substring(0,80);

        if( addressLookups.size() > 0 )
        {               
            for( Address__c contact : addressLookups )
            {           
                if( !contact.Email_Address__c.contains( lessorDomain ) & lessorDomain!='' )
                {   
                    Customer_Interaction_Log__c newIlog = new Customer_Interaction_Log__c();
                    newIlog.Name = nameStr;
                    newIlog.Comments__c            = emailBody;
                    //newIlog.Related_To_Contact__c = contact.Id;
                    newIlog.Related_To_Operator__c = contact.Operator__c;
                    newIlog.Date_Of_Call__c        = System.Today();
                    newIlog.Type_Of_Interaction__c = 'Email';
                    interactionLogs.add( newIlog );
                    selectedOperatorId = contact.Operator__c;
                    break;  
                }   
            }               
        }
        
        //Iterating airlines mentioned in subject
        for( Id operatorId : airlines )
        {
            if( selectedOperatorId==null || ( selectedOperatorId!=null && operatorId != selectedOperatorId ) )   
            {   
                Customer_Interaction_Log__c newIlog = new Customer_Interaction_Log__c();
                newIlog.Name = nameStr;
                newIlog.Comments__c            = emailBody;
                newIlog.Related_To_Operator__c = operatorId;
                newIlog.Date_Of_Call__c        = System.Today();
                newIlog.Type_Of_Interaction__c = 'Email';
                interactionLogs.add( newIlog ); 
            }
        }           
            
        system.debug('interactionLogs=== '+interactionLogs.size()+'    '+interactionLogs);
        
        list<ContentVersion> contentVersions = new list<ContentVersion>();
        list<ContentDocumentLink> contentDocumentLinks = new list<ContentDocumentLink>();
        
        if( !interactionLogs.isEmpty() )
        {
            try {
                insert interactionLogs;
            }
            catch( DMLException ex )
            {      
                result.message = 'Error while creating Interaction Log records in salesforce : '+ ex.getMessage(); 
                result.success = false;
                LeaseWareUtils.createExceptionLog(ex, 'Exception while creating Interaction Log records into salesforce: ', null, null,null,true);     
            }   
            
            // Save textAttachments, if any 
            if ( email.textAttachments != null && email.textAttachments.size() > 0 ) 
            {
                for ( Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments ) 
                {
                   for( Customer_Interaction_Log__c ilog : interactionLogs )
                   {                             
                     ContentVersion conV = new ContentVersion();
                     conV.Description    = ilog.id;
                     conV.VersionData    = Blob.valueOf(tAttachment.body);
                     conV.PathOnClient   = '/'+tAttachment.filename;
					 conV.Description = 'Leaseworks Internal';
                     contentVersions.add( conV );
                   }
                }         
            }
        
            // Save binaryAttachments, if any 
            if ( email.binaryAttachments != null && email.binaryAttachments.size() > 0 ) 
            {
                for ( Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments )
                {
                   for( Customer_Interaction_Log__c ilog : interactionLogs )
                   {                        
                        ContentVersion conV = new ContentVersion();
                        conV.Description    = ilog.id;
                        conV.VersionData    = bAttachment.body;
                        conV.PathOnClient   = '/'+bAttachment.filename;
						conV.Description = 'Leaseworks Internal';
                        contentVersions.add( conV );
                   }
                }
            }
            
            if( !contentVersions.isEmpty() )    
            {
                try {
				
					//To skip sharing with Library.
					LeaseWareUtils.setFromTrigger('FileUploadedFromBackEnd');
                    insert contentVersions;
					LeaseWareUtils.unsetTriggers('FileUploadedFromBackEnd');
                }
                catch( DMLException ex )
                {
                    result.message = 'Error while Attachment Creation into salesforce  : '+ ex.getMessage();   
                    result.success = false; 
                    LeaseWareUtils.createExceptionLog(ex, 'Exception while Attachment Creation into salesforce: ', null, null,null,true);                    
                }
                
                Map<Id,ContentVersion> contentVersionsMap = new Map<Id,ContentVersion>([ SELECT Id,ContentDocumentId,Description 
                                                                                            FROM ContentVersion 
                                                                                            WHERE Id In:contentVersions ]);
                
                for( ContentVersion cv : contentVersions )  {
                    ContentDocumentLink  cdl = new ContentDocumentLink ();
                    cdl.ContentDocumentId = contentVersionsMap.get( cv.Id ).ContentDocumentId;
                    cdl.LinkedEntityId    = contentVersionsMap.get( cv.Id ).Description;
                    cdl.ShareType  = 'v';
                    cdl.Visibility = 'AllUsers';
                    contentDocumentLinks.add( cdl );
                }
                
                if( !contentDocumentLinks.isEmpty() )   {
                    try {
                        insert contentDocumentLinks;
                    }
                    catch( DMLException ex )
                    {     
                        result.message = 'Error while relating attachment to record in salesforce : '+ ex.getMessage();    
                        result.success = false;
                        LeaseWareUtils.createExceptionLog(ex, 'Exception while Prelating attachment to record in salesforce: ', null, null,null,true);                      
                    }
                }   
                
                system.debug('contentDocumentLinks=2='+contentDocumentLinks.size()+'  '+contentVersions.size());            
            }            
        }   
        else
        {
            //insert record in Incoming Email Review Phase 2
            //  Name = Subject + Sender + date of email , To , Body , Attachments           
        }   
         result.success = true;
    }      
    catch( Exception ex ) 
    {
        result.success = false;
        result.message = 'Exception while Parsing Email Response: '+ex.getMessage();
        LeaseWareUtils.createExceptionLog(ex, 'Exception while Parsing EmailService Response: ', null, null,null,true);
    }
    return result;
  }
}