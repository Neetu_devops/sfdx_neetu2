@isTest
private class TestCustomLookUpController {
    @isTest static void test_lookup() {
        //Create Contact Data
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Contact';
        insert con;
        system.assertEquals('Test', con.FirstName);
        
        Test.startTest();
        CustomLookUpController.fetchLookUpValues('Test', 'Contact','','','');
        CustomLookUpController.fetchLookUpValues('Test', 'Contact','LastName = \'Contact\'','','');
        CustomLookUpController.fetchLookUpValues('Test', 'Account;Contact','','Contact:AccountId','Contact: Id =\''+con.Id+'\'');
		CustomLookUpController.fetchLookUpValuesWithAdditionalFields('Test', 'Contact','LastName = \'Contact\'', null);
        CustomLookUpController.getResults('Contact', 'FirstName', 'Test', null);
        Test.stopTest();
    }
  }