public class TaskHandler implements ITrigger {
    
    public void bulkBefore()
    {
        
    }
    public void bulkAfter()
    {
    }
    public void beforeInsert()
    {
    }
    public void afterInsert()
    {
    }
    public void beforeUpdate()
    {
    }
    public void afterUpdate()
    {
    }
    public void beforeDelete()
    {
        system.debug('TaskHandler.BeforeDelete(+)');
        List<EmailMessage > updateEmailMessage = new List<EmailMessage >();
        for(Task delTask : (List<Task>)Trigger.old){
            system.debug('Task Id.....'+delTask.Id);
            
            try{
                EmailMessage emailMessage = [select Id from EmailMessage  where ActivityId =:delTask.Id];
                emailMessage.TaskId__c = delTask.Id;
                updateEmailMessage.add(emailMessage);
            }catch(exception e){
                system.debug('Ignoring Exception ' + e.getMessage());
            }
        }
        if(updateEmailMessage.size()>0){
            
            update updateEmailMessage;
            
        }
        
        
    }
    public void afterDelete()
    {
        system.debug('TaskHandler.afterDelete(+)');
        List<EmailMessage > deleteEmailIds = new List<EmailMessage >();
        for(Task delTask : (List<Task>)Trigger.old){
            system.debug('After Delete Task ID.....'+delTask.Id);
            try{
                EmailMessage emailMessage = [select Id from EmailMessage  where TaskId__c =:delTask.Id];
                system.debug('emailMessage-----'+ emailMessage);
                deleteEmailIds.add(emailMessage);
            }catch(exception e){
                system.debug('Ignoring Exception ' + e.getMessage());
            }
        }
        if(deleteEmailIds.size()>0){
            delete deleteEmailIds;
        }
    }
    public void afterUnDelete()
    {
    }
    public void andFinally()
    {
    }
}