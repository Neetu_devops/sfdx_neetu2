public class MaintenanceEventTaskTriggerHandler implements ITrigger{
    // Hint This Handler file is create Supplemental Rent Event Requirement record while creating Maintenance Event Task also handling deletion
    
    // Constructor
    private final string triggerBefore = 'MaintenanceEventTaskTriggerHandlerBefore';
    private final string triggerAfter = 'MaintenanceEventTaskTriggerHandlerAfter';
    
    
    
    public MaintenanceEventTaskTriggerHandler ()
    {   
    }
    //Making functionality more generic.
   
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {

    }
    public void bulkAfter()
    {

    }   
    public void beforeInsert()
    {   
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('MaintenanceEventTaskTriggerHandler.beforeInsert(+)');

        //When User create Event Task Manually and calculate Task cost Event value using Task cost Percentage and Event Cost
        DuplRule_CalTaskCost();
        system.debug('MaintenanceEventTaskTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('MaintenanceEventTaskTriggerHandler.beforeUpdate(+)');
        DuplRule_CalTaskCost();
        system.debug('MaintenanceEventTaskTriggerHandler.beforeUpdate(-)');
    }
    
     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
    
        system.debug('MaintenanceEventTaskTriggerHandler.beforeDelete(+)');
        CheckRefSuppRentEvntReq();
        system.debug('MaintenanceEventTaskTriggerHandler.beforeDelete(-)');
    }
     
    public void afterInsert()
    {

    }
     
    public void afterUpdate()
    {

    }
     
    public void afterDelete()
    {
           
    }

    public void afterUnDelete()
    {
            
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records
    }
    
    //Before Insert, Before Update
    //When User create Event Task Manually, In that case calculate Task cost Event value using Task cost Percentage and Event Cost
    private void DuplRule_CalTaskCost(){
        list<Maintenance_Event_Task__c> ListMET =(list<Maintenance_Event_Task__c>)trigger.new;

        //When User create Event Task Manually and calculate Task cost Event value using Task cost Percentage and Event Cost
        for(Maintenance_Event_Task__c curMETask: ListMET){
            //Only for Insert, Y_Hidden_EngineTemplate__c will be null in case of manual entry of MET
            if(curMETask.Y_Hidden_EngineTemplate__c==Null && curMETask.Y_Hidden_EventCost__c!=Null && curMETask.TaskCostEventPrcntg__c!=Null){ 
                curMETask.Y_Hidden_TaskCostEvent__c=curMETask.TaskCostEventPrcntg__c*curMETask.Y_Hidden_EventCost__c/100;
            }
            else{
                curMETask.Y_Hidden_TaskCostEvent__c=Null;
            }
            curMETask.Y_Hidden_Key__c=curMETask.Maintenance_Program_Event__c+curMETask.Name; // Used in Duplicate Rule
        }
    }

    //Before Delete
    //Purpose: Before delete Maintenance Event Task(Including Manual or via Changing EngineTemplate ID), cheking is it refered in SuppRentEvntReq
    //Ithen throw error, not allow to delete MET record
    private void CheckRefSuppRentEvntReq(){
        Maintenance_Event_Task__c[] ListOldMETIds = (list < Maintenance_Event_Task__c >)trigger.old ;
        
        system.debug('size of ListOldMETIds='+ListOldMETIds.size());
        set<id> setMETID= new set<id>();
        if(ListOldMETIds.size()>0){
            for(Supp_Rent_Event_Reqmnt__c currSRER : 
                        [select id,Maintenance_Event_Task__c from Supp_Rent_Event_Reqmnt__c 
                        where Maintenance_Event_Task__c in : ListOldMETIds]){
                setMETID.add(currSRER.Maintenance_Event_Task__c);
            }
        }

        for( Maintenance_Event_Task__c curMET: ListOldMETIds){
            if(setMETID.contains(curMET.id)) {
                curMET.adderror('Cannot delete Maintenance Event Task due to associated Supplemental Rent Event Requirements exists. Remove association first.');
            }  
        } 
    }
}