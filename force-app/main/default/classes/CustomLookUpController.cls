public class CustomLookUpController {
    
	
	@AuraEnabled
	public static List < sObject > fetchLookUpValuesWithAdditionalFields(String searchKeyWord, String ObjectName, String whereClause,
																	   String fields) {
	   
	   String searchKey = searchKeyWord + '%';
	  List < sObject > returnList = new List < sObject > ();
	  
	  
	   // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
	   String sQuery =  'select id, Name';
	   
	   //query additional fields.
	   if(String.isNotBlank(fields)){
		  sQuery += ',' + fields;
	   }
	   
	   sQuery += ' from ' +ObjectName + ' where Name LIKE: searchKey';

	   if(String.isNotBlank(whereClause)){
		 sQuery += ' AND ' + whereClause;
	   }

	   sQuery += ' order by createdDate DESC limit 5';

	   List < sObject > lstOfRecords = Database.query(sQuery);
	   
	   for (sObject obj: lstOfRecords) {
		   returnList.add(obj);
	   }
	   return returnList;
	}
	
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, String whereClause,String additionalFields, String whereClause2) {
        String searchKey = searchKeyWord + '%';
        List<sObject> returnList = new List<sObject>();

        if(ObjectName.indexOf(';') != -1){
            map<string,string> ObjectNameTofieldsNameMap = new map<string,string>();
            map<string,string> whereClauseMap = new map<string,string>();
            if(String.isNotBlank(AdditionalFields)){
                
                for(String objString:AdditionalFields.split(';'))
                {
                    List <String> lstStringObjectAndFieldsBreak=objString.split(':');
                    ObjectNameTofieldsNameMap.put(lstStringObjectAndFieldsBreak[0],lstStringObjectAndFieldsBreak[1]);                
                }
            }
            if(String.isNotBlank(whereClause2)){
                for(String objString:whereClause2.split(';'))
                {
                    List <String> lstStringObjectAndFieldsBreak=objString.split(':');
                    whereClauseMap.put(lstStringObjectAndFieldsBreak[0],lstStringObjectAndFieldsBreak[1]);                
                }
            }
            fetchMultipleLookupValues(searchKey, ObjectName, returnList,ObjectNameTofieldsNameMap, whereClauseMap);
            
        }
        else{
            // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
            String sQuery =  'select id, Name ';

            if(String.isNotBlank(AdditionalFields)){
                String fields=AdditionalFields.substringAfter(':');
                sQuery += ',' + fields;
                //sQuery += ',' + AdditionalFields;
            }

            sQuery+= ' from '+ObjectName + ' where Name LIKE: searchKey';

            if(String.isNotBlank(whereClause)){
                sQuery += ' AND ' + whereClause;
            }
            sQuery += ' order by createdDate DESC limit 5';
            system.debug('sQuery>>>>>'+sQuery);
            List<sObject> lstOfRecords = Database.query(sQuery);
            for (sObject obj: lstOfRecords) {
                returnList.add(obj);
            }
        }
        return returnList;
        }
	
	@AuraEnabled
    public static void fetchMultipleLookupValues(String searchKey, String ObjectName, List<sObject> returnList,map<String,String> AdditionalFieldsMap, map<String,String> whereClauseMap) {
        
        String key = ObjectName.substringBefore(';');     
       
        String tmpkey = ObjectName.substringAfter(';');
       
      
        if(key != null && key != ''){
			String sQuery =  'select id, Name';
       
            if(AdditionalFieldsMap.containsKey(key) && AdditionalFieldsMap.get(key) != ''){
                sQuery += ',' + AdditionalFieldsMap.get(key);
			}
       
            sQuery +=' from ' +key + ' where Name LIKE: searchKey';
            
            if(whereClauseMap.containsKey(key)){

                sQuery += ' AND ' + whereClauseMap.get(key);
			}
            sQuery += ' order by createdDate DESC limit 5';

			List < sObject > lstOfRecords = Database.query(sQuery);
       
		   for (sObject obj: lstOfRecords) {
			   returnList.add(obj);
		   }
            
            if(tmpkey != null && tmpkey != ''){
                fetchMultipleLookupValues(searchKey, tmpkey, returnList, AdditionalFieldsMap, whereClauseMap);
            }
		}	
   }
   
   
   public static RecordTypeSelectorController.RecordTypeWrapper getObjectRecTypeId(String objectName) {
        return RecordTypeSelectorController.getRecordTypes(objectName);
    }
	
	//this is method is used by LWC lookup component.
    @AuraEnabled(cacheable=true)
    public static List<SObJectResult> getResults(String ObjectName, String fieldName, String value, String whereClause) {
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        
        String searchKey = value + '%';
        String qry = 'Select Id,' + fieldName + ' FROM '+ ObjectName + ' where ' + fieldName + ' LIKE: searchKey';

        if(String.isNotBlank(whereClause)){
            qry +=  ' And ' + whereClause;
        }
        
        qry += ' order by createdDate DESC limit 5';
        
        for(sObject so : Database.Query(qry)) {
            String fieldvalue = (String)so.get(fieldName);
            sObjectResultList.add(new SObjectResult(fieldvalue, so.Id));
        }
        
        return sObjectResultList;
    }
    
    public class SObJectResult {
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        
        public SObJectResult(String recNameTemp, Id recIdTemp) {
            recName = recNameTemp;
            recId = recIdTemp;
        }
    }
}