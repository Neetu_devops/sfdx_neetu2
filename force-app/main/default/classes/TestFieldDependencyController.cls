@isTest
private class TestFieldDependencyController {
     @isTest static void test_getPicklistValues() {
         Test.startTest(); 
         
         Map<String,String> responseHeader = new Map<String,String>{ 'Authorization' => 'OAuth '+UserInfo.getSessionId() };            
             
         TestSingleRequestMock fakeResponse = new TestSingleRequestMock(200);
         Test.setMock(HttpCalloutMock.class, fakeResponse);
         
         FieldDependencyController.getPicklistValues('', '', '');
         FieldDependencyController.getPicklistValues('Aircraft_Proposal__c', 'Asset_Type__c' ,'Aircraft');
         
         Test.stopTest();
     }
    
    @isTest static void test_dynamicPicklistResponse404() {
        Test.startTest();
            
        TestSingleRequestMock fakeResponse = new TestSingleRequestMock(302);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        FieldDependencyController.getPicklistValues('', '', '');
        FieldDependencyController.getPicklistValues('Aircraft_Proposal__c', 'Asset_Type__c' ,'Aircraft');
        
        Test.stopTest();
    }
    
     public class TestSingleRequestMock implements HttpCalloutMock {
         Integer statusCode;
         public TestSingleRequestMock(Integer statusCode ){
             this.statusCode = statusCode;
         }
        public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse resp = new HttpResponse();
            if(this.statusCode == 200 ){
            	resp.setStatusCode(this.statusCode);
            	resp.setBody('{ "controllerValues" : { "Engine" : 0, "BR700" : 1, "CF6" : 2, "CFM56" : 3, "GE90" : 4, "GE9X" : 5, "GEnx" : 6, "GP7200" : 7, "JT3" : 8, "JT3D" : 9, "JT8D" : 10, "JT9D" : 11, "LEAP" : 12, "PW1000G" : 13, "PW2000" : 14, "PW4000" : 15, "PW6000" : 16, "RB.183 Tay" : 17, "RB211" : 18, "Trent" : 19, "V2500" : 20 }, "defaultValue" : null, "eTag" : "a25c94100e421c4d14d4afb08d6f10e0", "url" : "/services/data/v45.0/ui-api/object-info/Aircraft_Proposal__c/picklist-values/0121U000000UfvgQAC/Asset_Type__c", "values" : [ { "attributes" : null, "label" : "727", "validFor" : [ 1, 2, 3 ], "value" : "727" }, { "attributes" : null, "label" : "Engine", "validFor" : [ 1 ], "value" : "Engine" } ] }');
            }
            else {
            	resp.setStatusCode(this.statusCode);
            	resp.setBody('');    
            }
            return resp;
        }
    }
}