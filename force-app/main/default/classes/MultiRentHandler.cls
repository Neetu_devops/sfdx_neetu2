public class MultiRentHandler implements ITrigger{
	/*Created by PriyankaC : */
    // Constructor
    private final string triggerBefore = 'MultiRentHandlerBefore';
    private final string triggerAfter = 'MultiRentHandlerAfter';
    private map<ID,Stepped_Rent__c> mapToMultiRent = new map<ID,Stepped_Rent__c>();
    
    public MultiRentHandler(){   
    }
    //Making functionality more generic.
    private void setSOQLStaticMAPOrList(){
        system.debug('MultiRentHandler::setSOQLStaticMAPOrList - Enter');
        Stepped_Rent__c[] newMultiRentList = (list<Stepped_Rent__c>)(trigger.IsDelete?trigger.old:trigger.new);
        system.debug('MultiRentHandler::setSOQLStaticMAPOrList - Exit');
    }
    
    public void bulkBefore(){
        setSOQLStaticMAPOrList();
    }
    
    public void bulkAfter(){
        setSOQLStaticMAPOrList();
    } 
    
    public void beforeInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);  
        system.debug('RentTriggerHandler.beforeInsert(+)');
        UpdateRentFlags();             
        system.debug('RentTriggerHandler.beforeInsert(-)');
    }
     
    public void beforeUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);  
        system.debug('RentTriggerHandler.beforeUpdate(+)');
         
        UpdateRentFlags();
        system.debug('RentTriggerHandler.beforeUpdate(-)');
    }
    
    public void beforeDelete(){  
		
    }
     
    public void afterInsert(){
		if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);   
        system.debug('RentTriggerHandler.afterInsert(+)');
        system.debug('RentTriggerHandler.afterInsert(-)');

    }
     
    public void afterUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter); 
        system.debug('RentTriggerHandler.afterUpdate(+)');
    	system.debug('RentTriggerHandler.afterUpdate(-)');
    }
     
    public void afterDelete(){
           
    }

    public void afterUnDelete(){
            
    }
     
    public void andFinally(){
    }
   
    public void UpdateRentFlags(){
        System.debug('UpdateRentFlags');
        //List<Stepped_Rent__c> steppedRentList = new List<Stepped_Rent__c>();
        Stepped_Rent__c[] newMultiRentList = (list<Stepped_Rent__c>)trigger.new;
        
        for(Stepped_Rent__c sr : newMultiRentList){
            
            if(Trigger.isInsert){
                System.debug('UpdateRentFlags isInsert'+sr.Id+ ' : '+sr.Name);
                sr.IsNewSchedule__c = True;
                System.debug('UpdateRentFlags isInsert : '+sr.IsNewSchedule__c );
                
            }
            
           else if(Trigger.isUpdate){
                System.debug('UpdateRentFlags isUpdate'+sr.Id+ ' : '+sr.Name);
                sr.IsUpdatedSchedule__c = True;
                System.debug('UpdateRentFlags isUpdate :'+sr.IsUpdatedSchedule__c);
                }
        }
        
    }
}