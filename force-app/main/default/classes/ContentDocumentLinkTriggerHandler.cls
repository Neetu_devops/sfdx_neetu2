public class ContentDocumentLinkTriggerHandler implements  ITrigger {

private final string triggerBefore = 'ContentDocumentLinkTriggerHandlerBefore';
private final string triggerAfter = 'ContentDocumentLinkTriggerHandlerAfter';

public ContentDocumentLinkTriggerHandler (){}

/**
 * bulkBefore
 *
 * This method is called prior to execution of a BEFORE trigger. Use this to cache
 * any data required into maps prior execution of the trigger.
 */
public void bulkBefore(){}
    
public void bulkAfter(){}


public void beforeInsert(){
}
    
public void beforeUpdate(){
}
    
/**
 * beforeDelete
 *
 * This method is called iteratively for each record to be deleted during a BEFORE
 * trigger.
 */
public void beforeDelete(){
    
}
    
public void afterInsert(){

    //Unlike the usual triggers, we need to keep this as we are inserting
    //records of the same object from After Insert. 
    if(LeaseWareUtils.isFromTrigger(triggerAfter)) return; 
    
    system.debug('ContentDocumentLinkTriggerHandler.afterInsert(+)---------');
    
    Boolean isFromBackEnd = LeaseWareUtils.isFromTrigger('FileUploadedFromBackEnd');
    if(!isFromBackEnd){
        after_insert_call();
    }
    String updateLatestFile = LeaseWareUtils.getLWSetup_CS('UPDATE_LATEST_FILE_FIELDS');
    if(String.isEmpty(updateLatestFile) || !updateLatestFile.equals('ON') )return;
    LeasewareUtils.updateLatestUrlOnParent(trigger.new);
    system.debug('ContentDocumentLinkTriggerHandler.afterInsert(-)');    
}
    
public void afterUpdate(){
    String updateLatestFile = LeaseWareUtils.getLWSetup_CS('UPDATE_LATEST_FILE_FIELDS');
    if(String.isEmpty(updateLatestFile) || !updateLatestFile.equals('ON') )return;
    LeasewareUtils.updateLatestUrlOnParent(trigger.new);
}
    
public void afterDelete(){
}

public void afterUnDelete(){
}
    
public void andFinally(){
}

private void addDocLinksOnIL(List<ContentDocumentLink> documentLinkList, string workspaceId){

    set<string> strILFilters = new set<string>();
    for(ContentDocumentLink documentLink : (List<ContentDocumentLink>) Trigger.new){
        if('EmailMessage'.equals(documentLink.LinkedEntityId.getSObjectType().getDescribe().getName())){
            strILFilters.add('%' + documentLink.LinkedEntityId);
        }
    }
    map<id, id> mapEmailToIL = new map<id, id>();
    for(Customer_Interaction_Log__c curIL : [select id, Related_Email__c from Customer_Interaction_Log__c where Related_Email__c like :strILFilters]){
        mapEmailToIL.put(curIL.Related_Email__c.remove(URL.getSalesforceBaseUrl().toExternalForm() + '/'), curIL.id);
    }

    for(ContentDocumentLink curLink : (List<ContentDocumentLink>) Trigger.new){
        if(!'EmailMessage'.equals(curLink.LinkedEntityId.getSObjectType().getDescribe().getName()))continue;
        documentLinkList.add(new ContentDocumentLink(LinkedEntityId = mapEmailToIL.get(curLink.LinkedEntityId),
                                                     ContentDocumentId = curLink.ContentDocumentId, 
                                                     Visibility = 'AllUsers', ShareType= 'I'));
    }
}    
    
private void after_insert_call(){
    try{

        List<ContentDocumentLink> documentLinkList = new List<ContentDocumentLink>();
        
        
        String workspaceId;
        
        //in test class we won't get workspace Id.
        if(Test.isRunningTest()){
            workspaceId =  [Select Id from Account limit 1].Id;
        }else{
            for(ContentWorkspace workSpace : [select Id from ContentWorkspace where Name = 'All Files' limit 1]){
                workspaceId =  workSpace.Id;
            }
        }
        
        addDocLinksOnIL(documentLinkList, workspaceId); //Bobby20210526 - https://app.asana.com/0/7934919496421/1200053912407671

        if(workspaceId != null){
            
            Set<Id> documentIds = new Set<Id>();
            for(ContentDocumentLink documentLink : (List<ContentDocumentLink>) Trigger.new){
                documentIds.add(documentLink.ContentDocumentId);
            }
            
            //remove the documents Id which are already shared with the library.
            for(ContentDocumentLink documentLink : [Select Id,ContentDocumentId from ContentDocumentLink where ContentDocumentId in : documentIds
                                                    and LinkedEntityId =: workspaceId]){
                                                        
                                                        documentIds.remove(documentLink.ContentDocumentId);
                                                    }
            
            
            for(Id documentId : documentIds){
                
                documentLinkList.add(new ContentDocumentLink(LinkedEntityId = workspaceId,
                                                                ContentDocumentId = documentId, 
                                                                Visibility = 'AllUsers', ShareType= 'I'));
                
            }
            
        }    
        if(documentLinkList.size()>0){
            system.debug('documentLinkList ---->' + documentLinkList);
            LeaseWareUtils.setFromTrigger(triggerAfter);
            insert documentLinkList;
            LeaseWareUtils.unsetTriggers(triggerAfter);
        }

    }catch(Exception exp){
    leasewareUtils.createExceptionLog(exp, 'Exception in ContentDocumentLink Trigger while sharing the files', null, null,null,true); 
    }     
} 




}