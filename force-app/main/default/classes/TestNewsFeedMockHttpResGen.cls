public class TestNewsFeedMockHttpResGen implements HttpCalloutMock{
    
       // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        
        // Create a dummy response
        HttpResponse res ;
        if(req.getEndpoint().endswith('profiles?term=TestOperator')){
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"data":[{"rank":1,"id":503,"type":{"id":1,"name":"Airline"},"iata":"QF","icao":"QFA","name":"TestOperator","url":"https://centreforaviation.com/data/profiles/airlines/TestOperator-airways-qf","country":{"name":"Australia","code":"AU"},"image":"https://images.cdn.centreforaviation.com/profiles/1-503/logo.png?1477525965"}]}');
            return res; 
        }else if(req.getEndpoint().contains('profileid=503')){
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            res.setBody('{"data":[{"id":1016495,"title":"Qantas and TestOperator apply to ACCC for Extended Joint Coordination Agreement","url":"https://centreforaviation.com/news/qantas-and-china-eastern-airlines-apply-to-accc-for-extended-joint-coordination-agreement-1016495","tags":["australia","china"],"date":1597015680,"article":"<p><strong>Australian Competition &amp; Consumer Commission</strong> (ACCC) stated (31-Jul-2020) <strong>Qantas Airways</strong> and <strong>China Eastern Airlines</strong>"},{"id":1016495,"title":"Qantas and TestOperator apply to ACCC for Extended Joint Coordination Agreement","url":"https://centreforaviation.com/news/qantas-and-china-eastern-airlines-apply-to-accc-for-extended-joint-coordination-agreement-1016495","tags":["australia","china"],"date":1589288053	,"article":"<p><strong>Australian Competition &amp; Consumer Commission</strong> (ACCC) stated (31-Jul-2020) <strong>Qantas Airways</strong> and <strong>China Eastern Airlines</strong>"},{"id":1016345,"title":"Virgin Australia CEO: Virgin Australia will be very different as a business",'+            '"url":"https://centreforaviation.com/news/virgin-australia-ceo-virgin-australia-will-be-very-different-as-a-business-1016345","tags":["australia","competition"],"date":1596766800,"article":"<p><strong>Virgin Australia</strong> CEO Paul Scurrah said Virgin Australia and TestOperator will be very different as a business and will be coming out of administration as the best value airline in the country (<em>Sky News</em>, 06-Aug-2020)"}]}');
            return res; 
        }else if(req.getEndpoint().endswith('profiles?term=Qantas')){
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');           
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"data":[{"rank":1,"id":503,"type":{"id":1,"name":"Airline"},"iata":"QF","icao":"QFA","name":"Qantas","url":"https://centreforaviation.com/data/profiles/airlines/TestOperator-airways-qf","country":{"name":"Australia","code":"AU"},"image":"https://images.cdn.centreforaviation.com/profiles/1-503/logo.png?1477525965"}]}');
            return res; 
        }else if(req.getEndpoint().endswith('profiles?term=QA')){
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');           
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"data":[{"rank":1,"id":523,"type":{"id":1,"name":"Airline"},"iata":"QF","icao":"QFA","name":"QA","url":"https://centreforaviation.com/data/profiles/airlines/TestOperator-airways-qf","country":{"name":"Australia","code":"AU"},"image":"https://images.cdn.centreforaviation.com/profiles/1-503/logo.png?1477525965"}]}');
            return res; 
        }else if(req.getEndpoint().endsWith('profileid=523')){
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            res.setBody('{"data":[{"id":1017000,"title":"QA and TestOperator apply to ACCC for Extended Joint Coordination Agreement","url":"https://centreforaviation.com/news/qantas-and-china-eastern-airlines-apply-to-accc-for-extended-joint-coordination-agreement-1016495","tags":["australia","china"],"date":1597015680,"article":"<p><strong>Australian Competition &amp; Consumer Commission</strong> (ACCC) stated (31-Jul-2020) <strong>Qantas Airways</strong> and <strong>China Eastern Airlines</strong>"},{"id":107644,"title":"Virgin Australia CEO: Virgin Australia will be very different as a business",'+
            '"url":"https://centreforaviation.com/news/virgin-australia-ceo-virgin-australia-will-be-very-different-as-a-business-1016345","tags":["australia","competition"],"date":1596766800,"article":"<p><strong>Virgin Australia</strong> CEO Paul Scurrah said Virgin Australia and TestOperator will be very different as a business and will be coming out of administration as the best value airline in the country (<em>Sky News</em>, 06-Aug-2020)"}]}');
            return res; 
        }
        else if(req.getEndpoint().equals('https://centreforaviation.com/login.php')){
            System.debug('---Redirect mock---');
            res = new HttpResponse();
            res.setStatusCode(302);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            res.setBody('<p>Found. Redirecting to <a href="https://centreforaviation.com/shared?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImYwYjdlY2U1ZWZiOTJkZGI3ZWRmZTI5NGRjOGVkMTA1NzdmMzFhOGIwYjBjMjRiMDUwNGNiMWQzNzJjZ000fcb4'+
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQ4MTg1NzYxNzI3YWY3MzhiOWUyN2ZjNTY0NmRmYWFlZDczOTU4OGQ5OWUxMDNhZjJiMTc4OTI5MDFmZWNiMDc2ODAzYWI0NWFiMWEzYmRmIn0.eyJhdWQiOiJmcm9udGVuZCIsImp0aSI6ImQ4MTg1NzYxNzI3YWY3MzhiOWUyN2ZjNTY0NmRmYWFlZDczOTU4OGQ5OWUxMDNhZjJiMTc4OTI5MDFmZWNiMDc2ODAzYWI0NWFiMWEzYmRmIiwiaWF0IjoxNjAyNTgxMzk4LCJuYmYiOjE2MDI1ODEzOTgsImV4cCI6MTYwMjYxMDE5Nywic3ViIjoiMTAyOTA5Iiwic2NvcGVzIjpbInVzZXIiXX0.u38UfxFfHSFHkKjzX_YT6un6RA89XnuQ5uGkuFm5hgOmmVI4LDYpvIMHyeUny94M4CG0kvxnsdEsss-wyro3O338-mWGsBTjz'+
            '&amp;expires_in=28800&amp;redirect=https%3A%2F%2Fcentreforaviation.com%2Fnews%2Fair-india-to-resume-services-connecting-mumbai-to-seven-domestic-destinations-in-sep-2020-1022622">https://centreforaviation.com/shared?access_token=eyJ0eXAiOiJK23430b5a23d3a132000cf25140ef089390a4c0b233b10276f49acbbab5ba53ab0985a4eb97999036a9f17774bea8d475c92ba27fe2411bdc5b52bb2b2000fcb4&amp;expires_in=28800&amp;redirect=https%3A%2F%2Fcentreforaviation.com%2Fnews%2Fair-india-to-resume-services-connecting-mumbai-to-seven-domestic-destinations-in-sep-2020-1022622</a></p>');
            return res; 
        }
        else{
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"data":[{"id":503,"title":"TestOperator\u00a0forecasts moderate to zero traffic growth in FY2019 due to 737 MAX grounding",'
                        +'"url":"https://centreforaviation.com/data/profiles/airlines/qantas-airways-qf",'
                        +'"tags":["outlook","forecast","traffic","europe","norway","avinor","737 max"],'
                        +'"date":1566989700,"article":"<p><strong>TestOperator&nbsp;</strong>stated (28-Aug-2019) it forecasts&nbsp;moderate to no traffic growth in FY2019 due to the&nbsp;<strong>Boeing</strong> 737 MAX grounding."}]}');
            return res; 
        }
    }      
}