public class DealGroupAssignmentHandler implements ITrigger {
    public DealGroupAssignmentHandler() {

    }
    private final string triggerBefore = 'DealGroupAssignmentHandlerBefore';
    private final string triggerAfter = 'DealGroupAssignmentHandlerAfter';
    public void bulkBefore()
    {
        
    }
     
    public void bulkAfter()
    {
    }
    public void beforeUpdate(){

    }
    public void beforeInsert()
    {
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('DealGroupAssignmentHandler.beforeInsert(+)');
        list<Deal_And_Group_Assignment__c> newList = (list<Deal_And_Group_Assignment__c>)trigger.new;
        set<string> deals = new set<string>();
        set<Id> dealAndGroup = new set<Id>();

        for(Deal_And_Group_Assignment__c dealGroupAssignment: newList ){
            deals.add(dealGroupAssignment.Deals__c);
        }   
       
        LeaseWareUtils.TriggerDisabledFlag=true;
        try{
          List<Deal_And_Group_Assignment__c> DealGroupsList = [SELECT Deal_Group__r.Name,Deal_Group__r.Id,
            Deal_Group__r.Target_Completion_Date__c,Deal_Group__r.Comment__c
            from Deal_And_Group_Assignment__c where Deals__r.Id in: deals];
        system.debug('the new list is----'+newList);
        //running a check to see if the new record being entered for a specific deal to deal group already exists
            for(Deal_And_Group_Assignment__c rec : DealGroupsList){
            dealAndGroup.add(rec.Deal_Group__r.Id);
            }
            for(Deal_And_Group_Assignment__c currec : newList){
                    if(dealAndGroup.contains(currec.Deal_Group__c)){    
                        currec.Deal_Group__c.addError('Duplicate record found for this Deal Group Assignment');
                    }
            }    
        }catch(Exception ex){
           LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger enabled for all users at apex level
                    
        throw new LeaseWareException('!!! Unexpected Error, please check with System Administrator :' + ex);
        }
    LeaseWareUtils.TriggerDisabledFlag=false;
    
       
}
public void beforeDelete(){

    }

public void afterDelete(){

    }
    public void afterUpdate(){

    }
    public void afterUndelete(){

    }
    
    public void afterInsert(){

    }
public void andFinally()
    {
        // insert any audit records

    }
}
