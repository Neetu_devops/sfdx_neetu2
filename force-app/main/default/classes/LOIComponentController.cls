public with sharing class LOIComponentController {
    public static string LIBRARY_NAME = 'LOI Library';
    
    // Method to update the content version of an existing content document. 
    @AuraEnabled
    public static String updateContentVersion(String uploadId, String recordId){
        String parentId = '';
        String userId = UserInfo.getUserId();
        ContentVersion newCV = new ContentVersion();
        Savepoint sp = Database.setSavepoint();
        String sObjectName = LeaseWareUtils.getNamespacePrefix() + 'LOI_Revision__c';
        try{
            // Get the parentId of the content document which is recently uploaded
            List<ContentDocumentLink> parentIdList = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink where ContentDocumentId =: uploadId];
            for(ContentDocumentLink cdLink : parentIdList){
                if(ID.valueOf(cdLink.LinkedEntityId).getSobjectType().getDescribe().getName() != 'User' && ID.valueOf(cdLink.LinkedEntityId).getSobjectType().getDescribe().getName() == sObjectName){
                    parentId = String.valueOf(cdLink.LinkedEntityId);
                }
            }
            
            if(parentId != null && parentId != ''){
                // Insert the content version for the existing content document
                List<ContentDocumentLink> contentDocumentFile = [Select ContentDocumentId from ContentDocumentLink where LinkedEntityId=:parentId];
                for (ContentVersion cv : [SELECT Id, ContentDocumentId,LastModifiedById, ContentBodyId, VersionNumber,
                                          Title, PathOnClient, OwnerId, FileType, VersionData, ContentSize,
                                          FileExtension, FirstPublishLocationId, Origin, ContentLocation,
                                          Checksum FROM ContentVersion
                                          Where ContentDocumentId =: uploadId ORDER By VersionNumber])
                {
                    newCV = cv.clone();
                }
                
                newCV.Id = null;
                newCV.ContentBodyId = null;
                newCV.ContentDocumentId = contentDocumentFile[0].ContentDocumentId;
                newCV.FirstPublishLocationId = null;
				
                insert newCV; 
                
                //Update Department of LOI Record when a new version is created
                List<Deal_Team_Member__c> dtmList = [select Department__c from Deal_Team_Member__c where Deal_Team__r.Marketing_Activity__c =: recordId AND User__c =: userId ];
                
                LOI_Revision__c deptLoi = new  LOI_Revision__c();
                deptLoi.Id = parentId;
                
                if(!dtmList.isEmpty() && dtmList[0].Department__c != null){
                    deptLoi.Y_Hidden_Department__c = dtmList[0].Department__c;
                }
                else{
                    deptLoi.Y_Hidden_Department__c = '';
                }
                update deptLoi; 
                
                // Delete ContentDocumentLink of the new file uploaded under the LOI record
                delete [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: parentId AND ContentDocumentId =: uploadId];
            }
            else{
                // Delete ContentDocument of the uploaded file
                delete [SELECT Id FROM ContentDocument WHERE Id =: uploadId];
            }
        }
        catch(Exception ex){
            System.debug('Error Message: '+ex.getMessage()+ 'Line Number: '+ex.getLineNumber()+ 'Stack Track: '+ex.getStackTraceString());
            Database.rollback(sp);
            throw new AuraHandledException(ex.getLineNumber()+' '+ex.getMessage());
        }
        return parentId;
    }
    
    // Method to save new LOI record with the uploaded files 
    @AuraEnabled
    public static void saveLoiRecord (String loi, List<String> uploadList) {
        Savepoint sp =Database.setSavepoint();
        ContentVersion cvUpdate = new ContentVersion();
        try{
            // Upload the inserted content documents under new LOI record.
            List<ContentDocumentLink> documentLinks = new List<ContentDocumentLink>();
            for(String documentid : uploadList){
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = documentid;
                cdl.LinkedEntityId = loi;
                cdl.ShareType = 'V';
                documentLinks.add(cdl);
            }
            if(!documentLinks.isEmpty()){
                insert documentLinks;
            }
            
            //Retrieve the comment of recently created LOI record and make that description of the content version.
            LOI_Revision__c loiComment = [Select id, LOI_Comments__c from LOI_Revision__c where id=:loi];
            ContentDocumentLink docId = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: loi LIMIT 1];
            ContentVersion cv =[Select id, Description from ContentVersion where ContentDocumentId=:docId.ContentDocumentId LIMIT 1];
            
            // Updating description field of content version with the value of recently created LOI record. 
            cvUpdate.Id = cv.Id;
            cvUpdate.Description = loiComment.LOI_Comments__c;
            update cvUpdate;
        }
        catch(Exception ex){
            System.debug('Error Message: '+ex.getMessage()+ 'Line Number: '+ex.getLineNumber()+ 'Stack Track: '+ex.getStackTraceString());
            Database.rollback(sp);
            throw new AuraHandledException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }
    
    // Method to get the LOI revisons data on the basis of deal recordid.
    @AuraEnabled
    public static LoiDataWrapper getLoiRevisionData(String recordId, Boolean showAllLoi){
        LoiDataWrapper ldw = new LoiDataWrapper();
        try{
            Integer listSize;
            Integer listSizeforUI;
            String userId = UserInfo.getUserId();
            
            // Map to link LOI Revision records with respective content document Ids
            Map<String,ContentDocumentLink> loiLinkMap = new Map<String,ContentDocumentLink>();
            Map<Id, ContentVersion> cvMap = new Map<Id, ContentVersion>();
            List<SubLoiDataWrapper> cdWrapperList = new List<SubLoiDataWrapper>();
            
            Set<Id> loiSet = new Set<Id>();
            Set<Id> cdIdSet = new Set<Id>();
            
            // List of Revision records related to Deal
            List<LOI_Revision__c> loilist = [Select Id, Name, Department__c,LOI_Comments__c  from LOI_Revision__c where Deal__c=:recordId order by Name asc];
            listSize = loilist.size();
            if(!showAllLoi){
                loilist = [Select Id, Name, Department__c,LOI_Comments__c from LOI_Revision__c where Deal__c=:recordId order by LastModifiedDate desc LIMIT 5];
            }
            for(LOI_Revision__c l :loilist ){
                loiSet.add(l.Id);
            }
            listSizeforUI = loilist.size();
            
            // Check if the Deal record has any LOI Revison record or not
            if(loiSet.size()>0){
                // Map to link LOI Revision records with respective content document Ids
                List<ContentDocumentLink> cdlist = [SELECT ContentDocumentId,ContentDocument.Title, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId IN: loiSet];
                for(ContentDocumentLink cdl : cdlist) {
                    loiLinkMap.put(cdl.LinkedEntityId , cdl);
                    cdIdSet.add(cdl.ContentDocumentId);
                }
                
                // Map of ContentDocumentId with respect to Latest contentVersion
                List<ContentVersion> versionData = [SELECT Id, ContentDocumentId,Description, VersionNumber,FileExtension, Title, LastModifiedBy.Name, LastModifiedDate FROM ContentVersion Where ContentDocumentId IN: cdIdSet ORDER By VersionNumber asc ];
                for(ContentVersion cv : versionData){
                    cvMap.put(cv.ContentDocumentId, cv);
                }
                
                // Inserting data into SubLoiDataWrapper
                for(LOI_Revision__c loi : loilist){
                    SubLoiDataWrapper sldw = new SubLoiDataWrapper();
                    sldw.loi = loi;
                    if(loiLinkMap.containskey(loi.Id) && loiLinkMap.get(loi.Id) != null){
                        sldw.cdData = loiLinkMap.get(loi.Id);
                    }
                    if(cvMap.containskey(sldw.cdData.ContentDocumentId) && cvMap.get(sldw.cdData.ContentDocumentId) != null){
                        sldw.cvData = cvMap.get(sldw.cdData.ContentDocumentId);
                        if(sldw.cvData!=null && sldw.cvData.Description!=null && sldw.cvData.Description!='' &&sldw.cvData.Description.length()>40){
                            sldw.description = sldw.cvData.Description.substring(0,40) + '...';
                        }
                        else{
                            sldw.description = sldw.cvData.Description;
                        }
                    }  
                    cdwrapperlist.add(sldw);
                }
                
                // Sort the list of wrapper according to LastModifiedDate of ContentVersion
                sortWrapperData(cdWrapperList);
                
                // Get the Id of library on which all records are uploaded if we have to use content library.
                getLibraryDetails(recordId, ldw, userId);
                
                //Get Department of current logged in user on load
                getDepartment(recordId, ldw, userId);
                
                ldw.totalrecords = ListSize;
                ldw.totalDisplayRecords = ListSizeforUI;
                ldw.scdw = cdwrapperlist;
            }
            
            //set the value of department and record id on which new files get uploaded in case no LOI record is available.
            else{
                // Get Department of current logged in user
                getDepartment(recordId, ldw, userId);
                
                // Check if we have to use content library or not.
                getLibraryDetails(recordId, ldw, userId);
                ldw.totalrecords =0;
            }
        }
        catch(Exception ex){
            System.debug('Error Message: '+ex.getMessage()+ 'Line Number: '+ex.getLineNumber()+ 'Stack Track: '+ex.getStackTraceString());
            throw new AuraHandledException(ex.getLineNumber()+' '+ex.getMessage());
        }
        return ldw; 
    }
    
    @AuraEnabled
    public static void deleteUploadedRecord(String uploadId){
        try{
            delete[Select Id from ContentDocument where Id=:uploadId];
        }
        catch(Exception ex){
            throw new AuraHandledException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }
    // Method to delete LOI Revison record.
    @AuraEnabled
    public static void deleteLoiById(String loi){
        try{
            delete[select Id from LOI_Revision__c where Id =: loi ];
        }
        catch(Exception ex){
            throw new AuraHandledException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }
    
    // Method to update description of content version whenever a new version is uploaded.
    @AuraEnabled
    public static void saveComments(String loi, String comment){
        try{
            List<ContentDocumentLink> cdlList = [Select ContentDocumentId from ContentDocumentLink where LinkedEntityId=:loi];
            
            // Fetch the latest content version of the content document under LOI record
            ContentVersion cv = [SELECT Id, ContentDocumentId,Description, VersionNumber,FileExtension, Title, LastModifiedById, LastModifiedDate FROM ContentVersion Where ContentDocumentId =: cdlList[0].ContentDocumentId ORDER By VersionNumber desc LIMIT 1];
            cv.Description = comment;
            update cv;
        }
        catch(Exception ex){
            System.debug('Error Message: '+ex.getMessage()+ 'Line Number: '+ex.getLineNumber()+ 'Stack Track: '+ex.getStackTraceString());
            throw new AuraHandledException(ex.getLineNumber()+' '+ex.getMessage());
        }
    }
    
    // Sort the list of wrapper according to LastModifiedDate of ContentVersion
    public static void sortWrapperData(List<SubLoiDataWrapper> cdWrapperList){
        SubLoiDataWrapper sortedWrapperList = new SubLoiDataWrapper();
        for(Integer i=0; i<cdWrapperList.size();i++){
            for(Integer j=0 ; j<cdWrapperList.size();j++){
                if(cdWrapperList[i].cvData.LastModifiedDate > cdWrapperList[j].cvData.LastModifiedDate){
                    sortedWrapperList = cdWrapperList[i];  
                    cdWrapperList[i] = cdWrapperList[j];
                    cdWrapperList[j] = sortedWrapperList;
                }
            }
        }
        System.debug('CdWrapperList:'+cdWrapperList);
    }
    
    // Get the department of current logged in user.
    public static void getDepartment(String recordId, LoiDataWrapper ldw, String userId){
        List<Deal_Team_Member__c> dtmList = [select Department__c from Deal_Team_Member__c where Deal_Team__r.Marketing_Activity__c =: recordId AND User__c =: userId ];
        
        if( dtmList.size() > 0 && dtmList[0].Department__c != null){
            ldw.department = dtmList[0].Department__c;    
        }
        else{
            ldw.department = '';    
        }
    }
    
    //Get the recordId on which new LOI revision files will be uploaded.
    public static void getLibraryDetails(String recordId, LoiDataWrapper ldw, String userId){
        // Check if we have to use content library or not.
        if(!LeaseWareUtils.isSetupDisabled('Use Content Library')){
            List<ContentWorkspace> workspaceIdList = [SELECT Id FROM ContentWorkspace WHERE Name =: LIBRARY_NAME];
            if(workspaceIdList.size()>0){
                ldw.recordID = workspaceIdList[0].Id;
                List<ContentWorkspaceMember> cwsmList = [SELECT Id, ContentWorkspace.Name, ContentWorkspacePermission.Name, MemberId, MemberType FROM ContentWorkspaceMember where MemberId=:userId AND ContentWorkspaceId=:workspaceIdList[0].Id ];
                if(cwsmList.size()>0 && cwsmList[0].ContentWorkspacePermission.Name == 'Viewer'){
                    ldw.isViewer = true;
                }
            }
            else{
                ldw.error = true;
            }   
        }
        else {
            ldw.recordID = recordId;
        }
    }
    
    // Wrapper to store details of each LOI Revision record.
    public class SubLoiDataWrapper{
        @AuraEnabled public LOI_Revision__c loi{get;set;}
        @AuraEnabled public String lastModifiedDate{get;set;}
        @AuraEnabled public String description{get;set;}
        @AuraEnabled public ContentVersion cvData{get;set;}
        @AuraEnabled public ContentDocumentLink cdData{get;set;}
        public SubLoiDataWrapper(){
            this.lastModifiedDate = '';
        }
    }
    public class LoiDataWrapper{
        @AuraEnabled public Boolean isViewer{get;set;}
        @AuraEnabled public Boolean error{get;set;}
        @AuraEnabled public String recordID{get;set;}
        @AuraEnabled public String department{get;set;}
        @AuraEnabled public Decimal totalrecords{get;set;}
        @AuraEnabled public Decimal totalDisplayRecords{get;set;}
        @AuraEnabled public List<SubLoiDataWrapper> scdw{get;set;}
        public LoiDataWrapper(){
            this.error = false;
            this.isViewer = false;
            this.recordID = '';
            this.department = '';
            this.totalrecords = 0;
            this.totalDisplayRecords = 0;
            this.scdw = new List<subloidatawrapper>();
        }
    }
    
}