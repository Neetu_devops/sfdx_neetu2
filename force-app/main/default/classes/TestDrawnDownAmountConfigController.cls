@isTest
public class TestDrawnDownAmountConfigController{
    @testSetup public static void testData(){
        Date dateField = System.today();
        Integer numberOfDays = Date.daysInMonth(dateField.year(), dateField.month());
        Date lastDayOfMonth = Date.newInstance(dateField.year(), dateField.month(), numberOfDays);
        
        Operator__c testOperator = new Operator__c(
            Name = 'American Airlines',                  
            Status__c = 'Approved',                          
            Rent_Payments_Before_Holidays__c = false,        
            Revenue__c = 0.00,                                
            Current_Lessee__c = true                        
        );
        insert testOperator;
        
        Lease__c leaseRecord = new Lease__c(
            Name = 'Testing',
            Lease_Start_Date_New__c = system.today(),
            Lease_End_Date_New__c = lastDayOfMonth,
            Lessee__c = testOperator.Id
        );
        insert leaseRecord;
        
        Aircraft__c testAircraft = new Aircraft__c(
            Name = 'Test',                    
            MSN_Number__c = '2821',                      
            Date_of_Manufacture__c = system.today(),
            Aircraft_Type__c = '737',                        
            Aircraft_Variant__c = '700',                    
            Marked_For_Sale__c = false,                  
            Alert_Threshold__c = 10.00,                  
            Status__c = 'Available',                  
            Awarded__c = false,                                              
            TSN__c = 1.00,                              
            CSN__c = 1
        );
        insert testAircraft;
        
        testAircraft.Lease__c = leaseRecord.Id;
        update testAircraft;
            
        leaseRecord.Aircraft__c = testAircraft.ID;
        update leaseRecord;  
        
                
        Utilization_Report__c utilizationRecord = new Utilization_Report__c(
            Name='Testing',
            Airframe_Flight_Hours_Month__c = 20.0,
            Airframe_Cycles_Landing_During_Month__c= 20.0,
            FH_String__c ='20',
            Aircraft__c = testAircraft.Id,
            Y_hidden_Lease__c = leaseRecord.Id,
            Type__c = 'Actual',
            Escalation_Factor__c = 3,
            Month_Ending__c = lastDayOfMonth
        );
        LWGlobalUtils.setTriggersflagOn();
        insert utilizationRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        utilizationRecord.Status__c = 'Approved By Lessor';
        update utilizationRecord;
        
        Invoice__c invoiceRecord = new Invoice__c(
            Name='Testing',
            Utilization_Report__c = utilizationRecord.Id,
            Lease__c = leaseRecord.Id,
            Status__c = 'Approved'
        );
        LWGlobalUtils.setTriggersflagOn();
        insert invoiceRecord;
        LWGlobalUtils.setTriggersflagOff();
        
        Letter_Of_Credit__c locRecord=new Letter_Of_Credit__c();
        locRecord.Name='Loc Demo';
        locRecord.LC_Amount_LC_Currency__c=200;
        locRecord.Lease__c=leaseRecord.Id;
        locRecord.Lessee__c=testOperator.Id;
        locRecord.LC_Number__c='123';
        insert locRecord;
        
        LOC_Drawn_Down__c locDrawnDownRecord=new LOC_Drawn_Down__c();
        locDrawnDownRecord.Name='LOC Drawn Down Amount#1';
        locDrawnDownRecord.Amount__c=100;
        locDrawnDownRecord.Locked__c=false;
        locDrawnDownRecord.Approved_On__c=datetime.newInstance(2014, 10, 15, 12, 30, 0);
        locDrawnDownRecord.Letter_Of_Credit__c=locRecord.Id;
        insert locDrawnDownRecord;
    }
    @isTest public static void testGetDrawnDownAmountRecords(){
        list<LOC_Drawn_Down__c> locDrawnDownList=[select id,name,amount__c,Letter_Of_Credit__c,locked__c,approved_on__c from LOC_Drawn_Down__c];
        system.assertEquals(locDrawnDownList.size(),1);
        Test.startTest();
        DrawnDownAmountConfigurationController.getDrawnDownAmountRecords(locDrawnDownList[0].Letter_Of_Credit__c);
        DrawnDownAmountConfigurationController.saveDrawnDownAmountRecord(locDrawnDownList[0].id,locDrawnDownList[0].Letter_Of_Credit__c,locDrawnDownList[0].amount__c,locDrawnDownList[0].name);
        Test.stopTest();
    }
}