@isTest
private class TestJournalListController {
    @testSetup
    static void setup(){
        LeaseWareUtils.clearFromTrigger();
        Parent_Accounting_Setup__c accountingSetup = new Parent_Accounting_Setup__c(Name='TestAS');
        insert accountingSetup;
        Calendar_Period__c calendarPeriod = new Calendar_Period__c(Name='TestCP');
        insert calendarPeriod;
        Legal_Entity__c legalEntity = new Legal_Entity__c(Name='TestLegalEntity');
        insert legalEntity;
        Operator__c operator = new Operator__c(Name='TestOperator',Bank_Routing_Number_For_Rent__c='Test RN');
        insert operator;
        
        Lease__c lease = new Lease__c(Name='TestLease',Lessee__c=operator.Id,Legal_Entity__c=legalEntity.Id,Lease_End_Date_New__c=Date.today().addYears(1),Lease_Start_Date_New__c=Date.today());
        
        LWGlobalUtils.setTriggersflagOn();
        insert lease;
        LWGlobalUtils.setTriggersflagOff();
        
        Aircraft__c aircraft = new Aircraft__c(Name='TestAircraft',MSN_Number__c='TestMSN1');
        insert aircraft;
        
        aircraft.Lease__c = lease.Id;
        update aircraft;
        
        lease.Aircraft__c = aircraft.ID;
        update lease;
        
        
        Utilization_Report__c utiReport = new Utilization_Report__c(Name='TestUR',Y_hidden_Lease__c=lease.id,FH_String__c='Test FH',Airframe_Cycles_Landing_During_Month__c=5,Aircraft__c=aircraft.Id,Status__c='Approved By Lessor');
        LWGlobalUtils.setTriggersflagOn();
        insert utiReport;
        LWGlobalUtils.setTriggersflagOff();
        
        Invoice__c invoice = new Invoice__c(Name='TestInvoice',Lease__c=lease.Id,Utilization_Report__c=utiReport.Id);
        insert invoice;
        
        //Creating 1 Journal_Entry__c under Calendar_Period__c
        Journal_Entry__c journalEntry = new Journal_Entry__c(Name='TestJournalEntry',JE_Description__c='Test Journal Description',Journal_Date__c=Date.today().addMonths(-1),
                                                            Transaction_Type__c='Aircraft Sale',Lease__c=lease.Id,Legal_Entity__c='Test Legal Entity',Intercompany__c='Test Inter Company',
                                                            Debit__c=1000.00,Credit__c=500.00,Invoice__c=invoice.Id,Sign__c='Test',
                                                            XML_Sent__c=false,Journal_Sent_Date__c=Date.today(),Calendar_Period__c=calendarPeriod.Id);
        insert journalEntry;
        
        insert new LW_Setup__c(Name = 'Journal_Output_Format',Value__c='XML');

    }
    
    @isTest
    static void test_class_methods(){
        
        JournalListController.FilterWrapper filter = new JournalListController.FilterWrapper();
         filter.fieldApiName = 'Name';
         filter.value = 'TestCP';
        
        List<JournalListController.FilterWrapper> filterList = new List<JournalListController.FilterWrapper>{filter};

        
        JournalListController.loadFilterValues();
        Calendar_Period__c calendarPeriod = [Select Id From Calendar_Period__c];
        JournalListController.loadJournalEntryData(calendarPeriod.Id , calendarPeriod.Id, JSON.serialize(filterList));
        String initData = JournalListController.loadJournalEntryData(calendarPeriod.Id, null , null);
        JournalListController.exportToExcel(initData);
        JournalListController.Post(initData);
    }
    
 
}