public class AssemblyMRFundTransferTriggerHandler implements ITrigger {
    
    private final string triggerBefore = 'AssemblyMRFundTransferTriggerHandler';
    private final string triggerAfter = 'AssemblyMRFundTransferTriggerHandler';

    public AssemblyMRFundTransferTriggerHandler(){}
    
    public void bulkBefore(){
    }
     
    public void bulkAfter(){}
    
    
    public void beforeInsert()
    {
         
    }
     
    public void beforeUpdate()
    {
        
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    { 
        System.debug('AssemblyMRFundTransferTriggerHandler.Before Delete(+)');
        updateSRForTransactions();      
        System.debug('AssemblyMRFundTransferTriggerHandler.Before Delete(-)');   
    }
     
    public void afterInsert()
    {
        LeaseWareUtils.setFromTrigger(triggerAfter);
        System.debug('AssemblyMRFundTransferTriggerHandler.afterInsert(+)');
        updateSRForTransactions();
        System.debug('AssemblyMRFundTransferTriggerHandler.afterInsert(-)');
    }
     
    public void afterUpdate()
    {
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        System.debug('AssemblyMRFundTransferTriggerHandler.afterUpdate(+)');
        updateSRForTransactions();
        System.debug('AssemblyMRFundTransferTriggerHandler.afterUpdate(-)');
    }
    
     public void afterDelete()
    {

    }

    public void afterUnDelete()
    {
           
    }
     
    public void andFinally(){

    }

    /* Used to call update on SR to create or delete supplemental Rent TRansactions
    */
    private void updateSRForTransactions(){
        List<Assembly_MR_Fund_Transfer__c> listAssemblyRecord = (list<Assembly_MR_Fund_Transfer__c>)(trigger.isDelete?trigger.old:trigger.new);
        String Id = '';
        Set<Id> assemblyMRInfoIds = new  Set<Id>();
        List<Assembly_MR_Rate__c> listSRToUpdate = new List<Assembly_MR_Rate__c>();
        for(Assembly_MR_Fund_Transfer__c assemblyRecord: listAssemblyRecord){
            assemblyMRInfoIds.add(assemblyRecord.Source_Assembly_MR_Lookup__c);
            assemblyMRInfoIds.add(assemblyRecord.Target_Assembly_MR_Lookup__c);  
            Id = assemblyRecord.Id ;
        }
        for(Assembly_MR_Rate__c curSR : [Select id, name from Assembly_MR_Rate__c where id in :assemblyMRInfoIds]){
             curSR.Snapshot_Event_Record_ID__c = 'T'+ Id;
             listSRToUpdate.add(curSR);
        }

        System.debug('listSRToUpdate :' + listSRToUpdate);
        if(listSRToUpdate.size()>0){
            update listSRToUpdate ;
        }
    }
}