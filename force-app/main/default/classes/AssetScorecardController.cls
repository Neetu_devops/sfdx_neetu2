public without sharing class AssetScorecardController {

    
    @AuraEnabled
    public static CovidWrapper getPandamicDetails(String recordId) {
        map<String,String> covidMap = AssetScoreCardUtil.getAssetScoreAndPandemicInfoDetails(recordId);
        CovidWrapper wrapper =  new CovidWrapper();
        if(covidMap != null ){
            wrapper.dataAsOf = covidMap.get('Date_As_Of');
            wrapper.confirmedCases = covidMap.get('#_Of_Confirmed_Cases');
            wrapper.activeCases = covidMap.get('#_Of_Active_Cases');
            wrapper.newCases = covidMap.get('#_Of_New_Cases');
            wrapper.recoveryRate = covidMap.get('Recovery_Rate');
            wrapper.fatalityRate = covidMap.get('Fatality_Rate');
            wrapper.recoveredCases = covidMap.get('#_Of_Recovered_Cases');
            wrapper.totalCases = covidMap.get('Total_Cases/1M_Population');
            wrapper.totalDeaths = covidMap.get('Total_Deaths');
            wrapper.newDeaths = covidMap.get('New_Deaths');
            wrapper.deaths1MPopulation = covidMap.get('Deaths/1M_Population');
            wrapper.countryId = covidMap.get('Country_Id');
            wrapper.countryName = covidMap.get('Country_Name');
            wrapper.regionRecoveryRate = covidMap.get('Region_Average_Recovery_Rate');
            wrapper.worldwideRecoveryRate = covidMap.get('World_Wide_Average_Recovery_Rate');
            wrapper.regionFatalityRate = covidMap.get('Region_Average_Fatality_Rate');
            wrapper.worldwideFatalityRate = covidMap.get('World_Wide_Average_Fatality_Rate');
        }
        System.debug('getPandamicDetails wrapper '+wrapper);
        return wrapper;
    }

    @AuraEnabled
    public static AssetScorecardWrapper getAssetScoreDetails(Id recordId) {
        map<String,String> assetScoreMap = AssetScoreCardUtil.getAssetScoreAndPandemicInfoDetails(recordId);
        AssetScorecardWrapper wrapper = new AssetScorecardWrapper();
        System.debug('getAssetScoreDetails assetScoreMap: '+assetScoreMap);
        List<Aircraft__c> ac = [select Id,Name, MSN_Number__c from Aircraft__c where Id = :recordId];
        if(ac.size() > 0) {
            wrapper.assetId = ac[0].Id;
            wrapper.assetName = ac[0].Name;
            wrapper.assetMSN = ac[0].MSN_Number__c;
        }

        if(assetScoreMap != null) {
            wrapper.countryRepossession = assetScoreMap.get('Country_Repossession_Risk');
            wrapper.countryRepossessionScore = assetScoreMap.get('Country_Repossession_Risk_Score');
            wrapper.operatorRiskFactor = assetScoreMap.get('Operator_Financial_Risk_Profile');
            wrapper.operatorRiskFactorScore= assetScoreMap.get('Financial_Risk_Profile_Score');
            wrapper.govSupport = assetScoreMap.get('Government_Support');
            wrapper.govSupportScore = assetScoreMap.get('Government_Support_Score');
            wrapper.countryId = assetScoreMap.get('Country_Id');
            wrapper.countryName = assetScoreMap.get('Country_Name');
            wrapper.operatorId = assetScoreMap.get('Operator_Id');
            wrapper.operatorName = assetScoreMap.get('Operator_Name');
            wrapper.assetOverallScore = assetScoreMap.get('Asset_Overall_Score');
            wrapper.assetRiskAssessment = assetScoreMap.get('Asset_Risk_Assessment_Score');
            wrapper.completedBy = assetScoreMap.get('Asset_Risk_Assessment_Completed_By');
            wrapper.completedOn = assetScoreMap.get('Asset_Risk_Assessment_Completed_Date');

        }
        return wrapper;
    }
        
    @AuraEnabled
    public static AssetRiskAssessmentWrapper getAssetAssessmentDetails(Id recordId) {
        
        map<String,String> assetScoreMap = AssetScoreCardUtil.getAssetScoreCardDetails(recordId);
        map<String,String> assetTypeDetailsMap = AssetScoreCardUtil.getAircraftDetails(recordId);
        System.debug('getAssetAssessmentDetails assetTypeDetailsMap '+assetTypeDetailsMap);
        AssetRiskAssessmentWrapper wrapper = new AssetRiskAssessmentWrapper(); 
        AssetScoreWrapper scoreWrapper = new AssetScoreWrapper();
        System.debug('getAssetAssessmentDetails assetScoreMap '+assetScoreMap);
        if(assetScoreMap != null) {
            scoreWrapper.assetTypeScore = assetScoreMap.get('Asset_Type_Score');
            scoreWrapper.leaseTermRemainingScore = assetScoreMap.get('Lease_Term_Remaining_Score');
            scoreWrapper.assetSpecScore = assetScoreMap.get('Asset_Specification_Score');
            scoreWrapper.MREOLAScore = assetScoreMap.get('MR_EOLA_Score'); 
            wrapper.supplementalRent = assetScoreMap.get('Supplemental_Rent');
            wrapper.securityDepositAmount = assetScoreMap.get('Security_Deposit');
            wrapper.rentAmount = assetScoreMap.get('Current_Rent_Amount');
            wrapper.assetTypeWeight = assetScoreMap.get('Asset_Type_Weightage');
            wrapper.leaseTermWeight = assetScoreMap.get('Lease_Term_Remaining_Weightage');
            wrapper.assetSpecWeight = assetScoreMap.get('Asset_Specification_Weightage');
            wrapper.securityPackageWeight = assetScoreMap.get('MR_EOLA_Weightage');

        }
        if(assetTypeDetailsMap != null) {
            wrapper.leaseName = assetTypeDetailsMap.get('Lease');
            wrapper.leaseId = assetTypeDetailsMap.get('Lease_Id');
            wrapper.remainingLeaseTermMonth = assetTypeDetailsMap.get('Remaining_Lease_Term_Months');
            wrapper.assetTypeVariant = assetTypeDetailsMap.get('Asset_Type_and_Variant');
            wrapper.assetName = assetTypeDetailsMap.get('Asset');
            wrapper.assetId = assetTypeDetailsMap.get('Asset_Id');
            scoreWrapper.assetId = assetTypeDetailsMap.get('Asset_Id');
            wrapper.residualValueRentention = assetTypeDetailsMap.get('Residual_Value_Rentention');
            wrapper.valueForMoney = assetTypeDetailsMap.get('Value_for_Money');
            wrapper.operationalPerformance = assetTypeDetailsMap.get('Operational_Performance');
            wrapper.remarketingPotential = assetTypeDetailsMap.get('Remarketing_Potential');
        }
        wrapper.scoreWrapper = scoreWrapper;
        wrapper.leaseTermMap = AssetScoreCardUtil.getLeaseRemainingTermScores();
        wrapper.thrustWeightMap = AssetScoreCardUtil.getThrustWeightScores();
        if(wrapper.thrustWeightMap != null) {
            wrapper.weightKeySet = new Set<String>();
            wrapper.thrustKeySet = new Set<String>();
            for (String key : wrapper.thrustWeightMap.keySet()) {
                String[] keyArray = key.split('-');
                if(keyArray != null && keyArray.size() > 1){
                    wrapper.thrustKeySet.add(keyArray[1]);
                    wrapper.weightKeySet.add(keyArray[0]);
                }
            }
        }
        return wrapper;
    }
	
    @AuraEnabled
    public static void updateAsssessmentDetails(String assessmentDataString) {
        System.debug('updateAsssessmentDetails assessmentDataString: ' +assessmentDataString);
        AssetScoreWrapper assessmentData = (AssetScoreWrapper) JSON.deserialize(assessmentDataString, AssetScoreWrapper.class);
        map<String,String> assetScoreMap = new map<String,String>();
        if(assessmentData != null) {
            assetScoreMap.put('Asset_Id', assessmentData.assetId);
            assetScoreMap.put('Asset_Type_Score', assessmentData.assetTypeScore);
            assetScoreMap.put('Lease_Term_Remaining_Score', assessmentData.leaseTermRemainingScore);
            assetScoreMap.put('Asset_Specification_Score', assessmentData.assetSpecScore);
            assetScoreMap.put('MR_EOLA_Score', assessmentData.MREOLAScore);
            AssetScoreCardUtil.updateDetailsToAssetRecord(assetScoreMap);
        }
    }

    public class AssetScoreWrapper {
        @AuraEnabled public String assetTypeScore {get; set;}
        @AuraEnabled public String leaseTermRemainingScore {get; set;}
        @AuraEnabled public String assetSpecScore {get; set;}
        @AuraEnabled public String MREOLAScore {get; set;}
        @AuraEnabled public String assetId {get; set;}

    }
	
    public class AssetRiskAssessmentWrapper {
        @AuraEnabled public AssetScoreWrapper scoreWrapper {get; set;}
        @AuraEnabled public String leaseId {get; set;}
        @AuraEnabled public String leaseName {get; set;}
        @AuraEnabled public String remainingLeaseTermMonth {get; set;}
        @AuraEnabled public String assetTypeVariant {get; set;}
        @AuraEnabled public String assetId {get; set;}
        @AuraEnabled public String assetName {get; set;}
        @AuraEnabled public String residualValueRentention {get; set;}
        @AuraEnabled public String valueForMoney {get; set;}
        @AuraEnabled public String operationalPerformance {get; set;}
        @AuraEnabled public String remarketingPotential {get; set;}
        @AuraEnabled public String supplementalRent {get; set;}
        @AuraEnabled public String securityDepositType {get; set;}
        @AuraEnabled public String securityDepositAmount {get; set;}
        @AuraEnabled public String rentAmount {get; set;}
        @AuraEnabled public map<String,String> leaseTermMap {get; set;}
        @AuraEnabled public map<String,String> thrustWeightMap {get; set;}
        @AuraEnabled public set<String> thrustKeySet {get; set;}
        @AuraEnabled public set<String> weightKeySet {get; set;}
        @AuraEnabled public String assetTypeWeight {get; set;}
        @AuraEnabled public String leaseTermWeight {get; set;}
        @AuraEnabled public String assetSpecWeight {get; set;}
        @AuraEnabled public String securityPackageWeight {get; set;}

    }
    
    public class AssetScorecardWrapper {
        @AuraEnabled public String assetOverallScore {get; set;}
        @AuraEnabled public String assetRiskAssessment {get; set;}
        @AuraEnabled public String operatorRiskFactor {get; set;}
        @AuraEnabled public String operatorRiskFactorScore {get; set;}
        @AuraEnabled public String countryRepossession {get; set;}
        @AuraEnabled public String countryRepossessionScore {get; set;}
        @AuraEnabled public String govSupport {get; set;}
        @AuraEnabled public String govSupportScore {get; set;}
        @AuraEnabled public String countryId {get; set;}
        @AuraEnabled public String countryName {get; set;}
        @AuraEnabled public String assetId {get; set;}
        @AuraEnabled public String assetName {get; set;}
        @AuraEnabled public String assetMSN {get; set;}
        @AuraEnabled public String completedBy {get; set;}
        @AuraEnabled public String completedOn {get; set;}
        @AuraEnabled public String operatorId {get; set;}
        @AuraEnabled public String operatorName {get; set;}
    }
   
    public class CovidWrapper {
        @AuraEnabled public String dataAsOf {get; set;}
        @AuraEnabled public String confirmedCases {get; set;}
        @AuraEnabled public String activeCases {get; set;}
        @AuraEnabled public String newCases {get; set;}
        @AuraEnabled public String recoveryRate {get; set;}
        @AuraEnabled public String fatalityRate {get; set;}
        @AuraEnabled public String recoveredCases {get; set;}
        @AuraEnabled public String totalCases {get; set;}
        @AuraEnabled public String totalDeaths {get; set;}
        @AuraEnabled public String newDeaths {get; set;}
        @AuraEnabled public String deaths1MPopulation {get; set;}
        @AuraEnabled public String countryId {get; set;}
        @AuraEnabled public String countryName {get; set;}
        @AuraEnabled public String regionRecoveryRate {get; set;}
        @AuraEnabled public String worldwideRecoveryRate {get; set;}
        @AuraEnabled public String regionFatalityRate {get; set;}
        @AuraEnabled public String worldwideFatalityRate {get; set;}

    }
}