public class PDPScheduleTriggerHandler implements ITrigger{
  public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
               
    }
     
    public void beforeUpdate()
    {
        SObjectType accountType = Schema.getGlobalDescribe().get(LeaseWareUtils.getNamespacePrefix() + 'PDP_Payment_Schedule__c');
		Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
        Map<Id,PDP_Payment_Schedule__c> maps = (Map<Id,PDP_Payment_Schedule__c>)Trigger.oldMap;
        for(PDP_Payment_Schedule__c payment: (List<PDP_Payment_Schedule__c>)Trigger.new) {
            if(maps.get(payment.Id).Paid_In_Full__c){
                for( String field: mfields.keySet() ) {
                    if( mfields.get(field).getDescribe().isCustom() && !mfields.get(field).getDescribe().isCalculated() && mfields.get(field).getDescribe().isUpdateable()){
                        payment.put(field, maps.get(payment.Id).get(field));   
                    }
                }
            }
        }
        
    }

    
    public void beforeDelete()
    {  

        

    }
     
    public void afterInsert()
    {
    
         
    }
     
    public void afterUpdate()
    {

          
    }
     
    public void afterDelete()
    {

    }

    public void afterUnDelete()
    {
            
    }
     
    public void andFinally()
    {
        // insert any audit records

    }
}