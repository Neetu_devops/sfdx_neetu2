public class LessorHandler implements ITrigger {

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();

    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();

    // Constructor

    private final string validationERRORMSG = 'A Lessor is already present. You cannot add a new one.';
    private final string triggerBefore = 'LessorHandlerBefore';
    private final string triggerAfter = 'LessorHandlerAfter';
    public LessorHandler() {}

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }

    public void bulkAfter() {}

    public void beforeInsert() {
        if (LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);

        system.debug('LessorHandler.beforeInsert(+)');

        validationLessorEnsureOnlyOneRec();
		EmailFieldValidations();
        system.debug('LessorHandler.beforeInsert(+)');
    }

    public void beforeUpdate() {
        //Before check  : keep code here which does not have issue with multiple call .
        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('LessorHandler.beforeUpdate(+)');
        EmailFieldValidations();
        system.debug('LessorHandler.beforeUpdate(-)');
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete() {

        //if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        //LeaseWareUtils.setFromTrigger(triggerBefore);

        system.debug('LessorHandler.beforeDelete(-)');

    }

    public void afterInsert() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LessorHandler.afterInsert(+)');
        
		callSeedDataMethods();
        system.debug('LessorHandler.afterInsert(-)');
    }

    public void afterUpdate() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LessorHandler.afterUpdate(+)');

		// calling quick action : Generate_Income_Schedule
        QuickActionCall();
        system.debug('LessorHandler.afterUpdate(-)');
    }

    public void afterDelete() {
        //if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        //LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('LessorHandler.afterDelete(+)');

        system.debug('LessorHandler.afterDelete(-)');
    }

    public void afterUnDelete() {
        if (LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);

        system.debug('LessorHandler.afterUnDelete(+)');

        validationLessorEnsureOnlyOneRec();

        system.debug('LessorHandler.afterUnDelete(-)');
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        // insert any audit records

    }


    // before insert, after undelete
    private void validationLessorEnsureOnlyOneRec() {
        Lessor__c[] curCount = [select id, name from Lessor__c];

        if ((Trigger.isInsert && curCount.size() == 0) || (curCount.size() == 1 && trigger.isUnDelete)) {
            //There will be only one record after insert/undelete.
            return;
        }

        Trigger.new[0].addError(validationERRORMSG);
    }



	// after insert
	private void callSeedDataMethods(){
		
		LeaseWareSeededData.SeedThrustSettingLookup();
		LeaseWareSeededData.SeedLWSetup();
		LeaseWareSeededData.SeedCustomLookup();
		LeaseWareSeededData.Seed_LeaseWorksSettings_Setup();
        LeaseWareSeededData.SeedLWServiceSetup();
        LeaseWareSeededData.SeedMappingObject();
		//Start Nagaveni_21_8_2018
        //Task - Sequential invoicing numbering
        //Insert default values in custom setting LeaseWare_SequenceId__c
        LeaseWareSeededData.SeedSequenceNumber();
        //End Nagaveni_21_8_2018
	}
	
	private void QuickActionCall(){

		list<Lessor__c> listNew =  (list<Lessor__c>)trigger.new;
		

		
		//2.Refresh_Aging
		//Predefined field update
		
	  	//3. Update_World_Fleet_References
		/*if(listNew[0].getQuickActionName() == Schema.Lessor__c.QuickAction.Update_World_Fleet_References){
			string returnMessage = LWGlobalUtils.UpdateWorldFleetReferences();
			
			if(!returnMessage.contains('Job with Id')){
				listNew[0].addError(returnMessage);
			}
		}*/
		
		
		
		
	 	 //4. Refresh_Asset_Details
	  	if(listNew[0].getQuickActionName() == Schema.Lessor__c.QuickAction.Refresh_Asset_Details){
			try{
			AerDataServiceBatch.executeAerDataBatch();
			}
			catch(Exception ex){
				listNew[0].addError(ex);
			}
		}
	}

	//before insert, before update
	private void EmailFieldValidations(){
	
	    Lessor__c theSetup = (Lessor__c)trigger.new[0];
	
	    if(theSetup.Notification_Emails__c==null)return;
	    list<string> listEmailIds = theSetup.Notification_Emails__c.split(',', 0);
	    
	    Pattern emailPattern = Pattern.compile('^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,})$');
	    for(string strEmail : listEmailIds){
	
	        Matcher emailMatcher = emailPattern.matcher(strEmail.trim());
	
	        if(!emailMatcher.matches()){
	            theSetup.Notification_Emails__c.addError('Invalid email address or comma separated list of emails.');
	            break;
	        }
		}	
	}   
}