public class AdjustedIntRatioTriggerHandler implements ITrigger{
    /* This is trigger handler class for Adjusted Interval (Ratio) object.*/
    
    
    //Contructor
    private final string triggerBefore = 'AdjustedIntRatioTriggerHandlerBefore';
    private final string triggerAfter = 'AdjustedIntRatioTriggerHandlerAfter';

    public void bulkBefore(){}
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        
        system.debug('AdjustedIntRatioTriggerHandler.beforeInsert(+)');
        
        UpdateKeyField();
        system.debug('AdjustedIntRatioTriggerHandler.beforeInsert(+)'); 
    }
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);        
        system.debug('AdjustedIntRatioTriggerHandler.beforeUpdate(+)');        
        UpdateKeyField();
        system.debug('AdjustedIntRatioTriggerHandler.beforeUpdate(+)'); 
    }
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){}
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){}
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){}
    
    public void afterDelete(){}
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
   public  void afterUnDelete(){}   
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
  
    public void andFinally(){}
    
    /* Function to update the Y_Hidden key field used in the Matching Rule to avoid duplicate Adjusted Interval(Ratio) records.
       Its being called during Before Insert/Update */
        
    private void UpdateKeyField(){
           Adjusted_Interval_Ratio__c[] listNew = (list<Adjusted_Interval_Ratio__c>)trigger.new ;
        for(Adjusted_Interval_Ratio__c AIrec : listNew) {
            // Since the combined string length is less than 255 characters, we are not checking length.
            AIrec.Composite_Key_Field__c= AIrec.Maintenance_Program_Event_Catalog__c + '-' + AIrec.Name ; 
        }
    } 
}