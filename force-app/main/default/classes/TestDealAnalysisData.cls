@isTest
public class TestDealAnalysisData {

    @isTest
    public static void loadTestCases() {
    	DealAnalysisData.Data test1 =  new DealAnalysisData.Data();
        test1.Heading = null;
        test1.Source = null;
        test1.Lowest = 0;
        test1.LowSD = 0;
        test1.Mean = 0;
        test1.HighSD = 0;
        test1.Highest = 0;
        test1.Recs = null;
        
        DealAnalysisData.RecordInfo rec =  new DealAnalysisData.RecordInfo();
        rec.Id = null;
        rec.Name = null;
        rec.Rent = 0; 
        rec.IsLowest = false;
        rec.IsHighest = false;
        rec.AircraftTypeVariant = null;
        rec.OperatorName = null;
        rec.OperatorId = null;
        rec.Term = null;
    }
    
}