public with sharing class PayoutTriggerHandler implements ITrigger {
    private final string triggerBefore = 'PayoutTriggerHandlerBefore';
    private final string triggerAfter = 'PayoutTriggerHandlerAfter';
    @testvisible
    private static boolean bIsCancelPayout = false;
    @testvisible
    private static boolean bIsUndoCancellation = false;
    public PayoutTriggerHandler() {

    }
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore(){
        system.debug('Bulk before(+)');
        if(trigger.isDelete){return;}
        for(Payout__c po : (list<payout__c>)trigger.new){
            if(po.getQuickActionName() == Schema.payout__c.QuickAction.Undo_Cancellation){bIsUndoCancellation = true;}
            else if(po.getQuickActionName() == Schema.payout__c.QuickAction.Cancel_Payout){bIsCancelPayout = true;}
        }
        system.debug('Bulk before(-)');
    }
     
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter(){}
     
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(){

    }
     
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('payoutTriggerHandler.beforeUpdate(+)');
        if(bIsCancelPayout || bIsUndoCancellation) {
            quickActionCall();
            return;
        }
        validatePayout();
        
        system.debug('payoutTriggerHandler.beforeUpdate(+)');
    }
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(){
        if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
        LeaseWareUtils.setFromTrigger(triggerBefore);
        system.debug('PayoutTrigger.beforeDelete(+)');
            list<payout__c> oldList =  (list<payout__c>)trigger.old ;
            boolean isError = false;
            set<id> pyStdLIIds =new set<Id>(); 
            for(payout__c pout : oldList){
                if(!('Pending'.equals(pout.status__c)||'Cancelled'.equals(pout.status__c)||'Declined'.equals(pout.status__c))&&!isError ){
                    pout.addError('Payouts that are Approved and are awaiting Cancellations cannot be deleted');
                    isError = true;
                    break;
                }
                if('Offset'.equals(pout.payout_type__c)){
                    pyStdLIIds.add(pout.Payment_Line_Item__c);
                }                
            }
            if(!isError){
                try{
                    delete [select id from payable_line_item__c where payout__c IN :oldList];
                    if(pyStdLIIds.size() > 0 && !LeaseWareUtils.isFromTrigger('PaymentTriggerHandlerBefore')){
                        delete [select id from Payment__c where Y_Hidden_Payment_Std_Line_Item__c in :pyStdLIIds];
                    }                    
                }
                catch(DmlException ex){
                    System.debug(ex);
                    string errorMessage='';
                    for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                        // Process exception here
                        errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
                    }
                    system.debug('errorMessage=='+errorMessage);
                    for(payout__c py : oldList){
                        py.addError(errorMessage);
                    }
                   
                }
            }
            createDeleteParallelReportingRecord();
            callSRTransactionsBatch();
        system.debug('PayoutTrigger.beforeDelete(-)');
    }
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(){
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        system.debug('PayoutTriggerHandler.afterInsert(+)');  
        createDeleteParallelReportingRecord();
        callSRTransactionsBatch();
        system.debug('PayoutTriggerHandler.afterInsert(-)');
    }
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(){
        system.debug('PayoutTriggerHandler.afterUpdate(+)');
        if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
        LeaseWareUtils.setFromTrigger(triggerAfter);
        list<payout__c> newList = (list<payout__c>)trigger.new;
        set<id> poIdsAmtChange = new set<id>();
        map<id,payout__c> approvedSdPoMap = new map<id,payout__c> (); 
        map<id,payout__c> approvedPoMap =  new map<id,payout__c>();
        list<payout__c> cancelledPoList =  new list<payout__c>();
        map<id,payout__c> cancelledSdPoMap = new map<id,payout__c> (); 
        map<id, Payout__c> cancelledClaimPoMap = new map<id, Payout__c>();
        payout__c oldPy ;
        if(bIsCancelPayout || bIsUndoCancellation ){
            setOffsetPaymentStatus(newList);
            return;
        }
        for(payout__c po :newList){
            oldPy = (payout__c)trigger.oldMap.get(po.Id);
            if(('Pending'.equals(oldPy.status__c)&& 'Approved'.equals(po.status__c))){
                approvedPoMap.put(po.id, po);
                if('Security Deposit Refund'.equals(po.Payables_Type__c)){
                    approvedSdPoMap.put(po.id,po);
                }              
            }
            else if('Pending'.equals(po.status__c) && po.payout_amount__c != oldPy.payout_amount__c){//updates the amount to line item even in pending status
                poIdsAmtChange.add(po.id);
            }
            if('Cancelled-Pending'.equals(oldPy.status__c)&& 'Cancelled'.equals(po.status__c) ){
                cancelledPoList.add(po);
                if('Security Deposit Refund'.equals(po.Payables_Type__c)){
                    cancelledSdPoMap.put(po.id,po);
                }
                else if('MR Claim Event'.equals(po.Payables_Type__c)){
                    cancelledClaimPoMap.put(po.id, po);
                }
            }
        }
        try{
            system.debug('approvedPoMap::'+approvedPoMap.size());
            if(approvedPoMap.size()>0){                    
                    updatePayableOnApproval(approvedPoMap);
                    updateBalanceOnPoLI(approvedPoMap.values());//update line item balance when payout is approved
                    if(approvedSdPoMap.size()>0){
                        CashSecurityDepositUtil.updateCashSDOnPayoutApproval(approvedSdPoMap.keySet(),approvedSdPoMap);
                        CashSecurityDepositUtil.createSdHistoryPayout(approvedSdPoMap.keySet());
                    }
            }
            if(poIdsAmtChange.size()>0){
                updateAmountOnLineItem(poIdsAmtChange);
            }
            if(cancelledPoList.size()>0){
                updatePayablePLIOnCancellation(cancelledPoList); //updates the payable and payable line item 
                if(cancelledSdPoMap.size()>0){
                    CashSecurityDepositUtil.updateSDOnCancellation(cancelledSdPoMap);
                    CashSecurityDepositUtil.createSDTHOnCancellation(cancelledSdPoMap);
                }
                if(cancelledClaimPoMap.size() > 0){
                    updateEventClaimOnCancellation(cancelledClaimPoMap);
                }
            }
        }
        catch(DmlException ex){
            System.debug('errorMessage=='+ ex);
            string errorMessage='';
            for ( Integer i = 0; i < ex.getNumDml(); i++ ){
                // Process exception here
                errorMessage = errorMessage == '' ? ex.getDmlMessage(i) : errorMessage+' \n '+ex.getDmlMessage(i);
            }
            system.debug('errorMessage=='+errorMessage);
            for(payout__c py :  newList){
                py.addError(errorMessage);
            }
        }
        callSRTransactionsBatch();
        system.debug('PayoutTriggerHandler.afterUpdate(-)');
    }
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(){}
    
    /**
     * afterUnDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterUnDelete(){}    
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){}

    
    /*****
     * update the line item amount if amount is updated.
     * called from after update.
     */
    private static void updateAmountOnLineItem(set<id> poIdsAmtChange){
        system.debug('updateAmountOnLineItem(+)');
        list<payout_line_item__c> poutLIs = new list<payout_line_item__c>();
        map<id,payout__c> payoutMap = new map<id,payout__c>([select id,payout_amount__c,(select id,payout_line_amount__c from  payout_line_item__r) from payout__c where id IN :poIdsAmtChange]);
        for(payout__c po :payoutMap.values()){
            if(po.payout_line_item__r.size()>0){
                po.payout_line_item__r[0].payout_line_amount__c = po.payout_amount__c;//expecting only one line item per payout.
                poutLIs.add(po.payout_line_item__r[0]);
            }
        }
        if(poutLIs.size()>0){
            try{
                update poutLIs;
            }
            catch(DmlException ex){
                throw  new DmlException(ex.getMessage());
            }
            system.debug('updateAmountOnLineItem(-)');
        }
      
    }

    /***
     * update the Payout Line Item Balance due
     * called from :after update
     */
    private void updateBalanceOnPoLI(list<payout__c> poLIList){
        system.debug('inside updateBalanceOnPoLI(+)');
        set<id> poIds =  new set<id>();
        for(payout__c po: poLIList){
            poIds.add(po.id);
        }
        map<id,payout__c> poMap = new map<id,payout__c>([select id,
                                                        (select id,Payout_Line_Amount__c from payout_line_item__r) 
                                                        from payout__c where id IN :poIds] );
        list<payout_line_item__c> listToUpdate = new list<payout_line_item__c>();
        for(payout__c  po :poLIList){
            payout__c oldPo = (payout__c)trigger.oldMap.get(po.id);
            if(po.status__c !=oldPo.status__c || po.payout_amount__c !=oldPo.payout_amount__c){
                payout_line_item__c poLI = (poMap.get(po.id))?.payout_line_item__r[0];//expecting only one Line item
                if(poLI!=null){
                    poLI.Payout_Line_Amount__c = po.payout_amount__c;
                    listToUpdate.add(poLI);
                }
            }
        }
        if(!listToUpdate.isEmpty()){
            update listToUpdate;
        }
        system.debug('inside updateBalanceOnPoLI(-)');
    }

         /*Method to create Prallel Reporting object on afterInsert and delete from beforeDelete     */
         private void createDeleteParallelReportingRecord(){
            System.debug('PayoutTriggerHandler.createDeleteParallelReportingRecord(+)');
            list<payout__c> listToUpdate = (list<payout__c>)(trigger.isDelete?trigger.old:trigger.new);
            List<Operational_Transaction_Register__c> listToDelete = new List<Operational_Transaction_Register__c>();
            Set<Id> setIds = new Set<Id>();
            for(payout__c payout : listToUpdate){
                setIds.add(payout.Id);
            }
            if(trigger.isDelete){   
                 System.debug('Deleting record ---- ');
                // delete, cant handle that from future event , as record is already deleted by the time platform event is fired
                System.debug('came here ---' + setIds);
                for(Operational_Transaction_Register__c rec: [Select id, Name from Operational_Transaction_Register__c where Payouts__c in:setIds]){   
                    listToDelete.add(rec);
                }
            }else{
                LeasewareUtils.CreatePlatformEvent('insertParallelReportingObject', JSON.serialize(setIds));    
            }
            
            if(listToDelete.size()>0){
                try{
                    System.debug('listToDelete:' + listToDelete);
                     delete listToDelete;   // dont have any trigger for this or no other related trigger will be called, so not disabling trigger here 
                }    
                catch(Exception e){
                     System.debug('Exception e: ' + e.getMessage());
                     LeasewareUtils.createExceptionLog(e,e.getMessage(),'Operational_Transaction_Register__c','', 'Delete Operational_Transaction_Register__c',true);
                }  
            }
            System.debug('PayoutTriggerHandler.createDeleteParallelReportingRecord(-)');
        }  
        
        /***
         * validates Payout status changes and field updates
         * Callled from before Update
         */
        private void validatePayout(){
            system.debug('validatePayout....(+)');
            payout__c oldPo =  null;
            map<id,payout__c> approvedPos = new map<id,payout__c>();
            map<id,id> payoutPayableMap = new map<id,id> ();
            Map<String, Schema.SObjectField> mapPyFields = Schema.SObjectType.payout__c.fields.getMap();
            //set<string> setSkipFields = new set<String>{'Comments__c'};//add fields that should be skipped always
            set<string> setSkipFields = RLUtility.getAllFieldsFromFieldset('payout__c','SkipFieldsFromValidations');
            list<String> listFieldsToCheck = new list<String>();//add fields that needs to be checked
            for(String Field:mapPyFields.keyset()){
                    Schema.DescribeFieldResult thisFieldDesc = mapPyFields.get(Field).getDescribe();
                    System.debug('Field Name is ' + thisFieldDesc.getName() + '. Local ' + thisFieldDesc.getLocalName());
                    if(!thisFieldDesc.isUpdateable() || setSkipFields.contains(leasewareutils.getNamespacePrefix() + thisFieldDesc.getLocalName())) {continue;} //Add the fields that are always updatable to the list.
                        listFieldsToCheck.add(thisFieldDesc.getLocalName());
            }
            for(payout__c po :(list<payout__c>)trigger.new){
                oldPo = (payout__c)trigger.oldMap.get(po.id);
                if(oldPo!=null && ('Approved').equals(po.status__c)&& ('Pending').equals(oldPo.status__c)){
                    approvedPos.put(po.id,po);
                }
                else if( po.lease__c!=oldPo.lease__c ||po.lessee__c!=oldPo.lessee__c||po.Payables_Type__c!=oldPo.Payables_Type__c||po.company__c!=oldPo.company__c
                                    ||po.claim__c!=oldPo.claim__c||po.Supplemental_Rent__c!=oldPo.Supplemental_Rent__c ||po.security_deposit__c!=oldPo.security_deposit__c){
                    po.addError('The field/s you are attempting to modify are either calculated or logic driven.These field values cannot be changed manually.');
                }
                else if('Cancelled-Pending'.equals(oldPo.status__c) && po.status__c !=oldpo.status__c && !'Cancelled'.equals(po.status__c)){
                    po.addError('Cancellation Pending Payouts cannot be modified');
                }
                else if('Pending'.equals(oldPo.status__c) && po.status__c !=oldpo.status__c && !('Approved'.equals(po.status__c)||'Declined'.equals(po.status__c))){
                    po.addError('Manual update to Cancelled/Cancelled-Pending is not allowed. Please use \'Cancel Payout\' button');
                }
                else if(!'Pending'.equals(oldpo.status__c)){
                    for(String curField:listFieldsToCheck){
                        if('Cancelled-Pending'.equals(oldPo.status__c) && 'Status__c'.equals(curField)) {continue;}
                        if(po.get(curField) !=  oldPo.get(curField)){ 
                            po.addError(oldPo.status__c +' payouts cannot be modified.');
                            return; 
                        }
                    }
                }
            }
            if(approvedPos.size()>0){
                for(payable_line_item__c pli : [select id,payable__c,payout__c from payable_line_item__c
                                                        where payout__c In :approvedPos.keySet()]){
                    payoutPayableMap.put(pli.payout__c,pli.payable__c);
    
                }
                map<id,payable__c > payablesMap = new map<id,payable__c >([select id,Payable_Balance__c,status__c from payable__c where id IN :payoutPayableMap.Values()]);
                for(payout__c po :approvedPos.values()){
                    system.debug('approvedPos::'+approvedPos.size());
                    payable__c py = payablesMap.get(payoutPayableMap.get(po.id));
                    if(py!=null){
                        system.debug('po.Payout_Amount__c::'+po.Payout_Amount__c);
                        system.debug('py.Payable_Balance__c::'+py.Payable_Balance__c);
                        if('Cancelled-Pending'.equals(py.status__c)){
                            po.addError('There is currently a Pending Cancellation on the underlying Payable. Please cancel or undo the cancellation before proceeding with the payout');
                            break;
                        }
                        if(po.Payout_Amount__c > py.Payable_Balance__c){
                            system.debug('Error');
                            po.addError('The payout amount cannot be greater than payable balance.');
                            break;
                        }

                    }
                }
            }
            system.debug('validatePayout....(-)');
        }
      
    /****
      * Update the Approval Status and Cancellation date OR sets error based on status: 
      ***Called From beforeUpdate
      */
      @testvisible
      private void quickActionCall(){
        system.debug('Inside quickActionCall(+)');
        list<payout__c> poList = (list<payout__c>)trigger.new;
        //expecting only one record on quick action
        if(poList[0]!=null){
            payout__c oldpo = (payout__c)trigger.oldMap.get(poList[0].id);
            boolean isError = checkStatusForCancelUndoCancel(poList[0],oldpo);
            if(bIsCancelPayout){
                if(isError){
                    if('Cancelled'.equals(oldpo.status__c)){//specific error message when trying to cancel already cancelled payout
                        poList[0].addError('You are trying to cancel an already cancelled transaction. Please review.'); 
                    }
                    else{
                        poList[0].addError('Only Approved payouts can be Cancelled.');
                    }
                    return;
                }
                else{
                    poList[0].status__c ='Cancelled-Pending';                     
                }
            }
            else if(bIsUndoCancellation){
                if(isError){
                    poList[0].addError('This action can only be performed on payouts in Cancelled-Pending status');
                    return;
                }
                else{
                    poList[0].status__c ='Approved';
                    poList[0].Cancellation_Date__c = null;                    
                }
            }
        }
        system.debug('Inside quickActionCall(-)');
    }

    /**
     * updates the below fields on payable and payable line item on payout Cancellation
     * Last Payout Date
     * Total Payout Amount
     * Line Payout Amount
     * Called from after update
     */
    private void updatePayablePLIOnCancellation(List<payout__c> cancelledPos){
        system.debug('updatePayablePLIOnCancellation (+) ');
        set<id> setPoIds = new set<id>();
        map<id,id> payoutPayableIdMap = new map<id,id>();
        map<id,list<payout__c>> payablePayoutsMap = new map<id,list<payout__c>>();
        list<py_line_item__c> pyLIList = new list<py_line_item__c>();
        list<payout__c> payoutList = null;
        set<id> setPyStdLIIds =  new set<id>();
        map<id,payment__c> mapOffsetPayments =new  map<id,payment__c>();
        for(payout__c po : cancelledPos){
            setPoIds.add(po.id);
            if(po.Payment_Line_Item__c !=null){setPyStdLIIds.add(po.Payment_Line_Item__c);}//getting all the py std LI for offset to be cancelled.
        }
        for(payable_line_item__c pyLI : [select payable__c,payout__c,payout__r.Payout_Date__c from payable_line_item__C where payout__c IN :setPoIds]){
            payoutPayableIdMap.put(pyLI.payout__c,pyLI.payable__c);
        }
        for(payable_line_item__c pyLI : [select payable__c,payout__c,payout__r.Payout_Date__c,payout__r.status__c from payable_line_item__C where payable__c IN :payoutPayableIdMap.Values()]){
           
            payoutList = payablePayoutsMap.get(pyLI.payable__c);
            if(payoutList == null){payoutList = new list<payout__c>(); }
            payoutList.add(new payout__c(id=pyLI.payout__c,Payout_Date__c = pyLI.payout__r.Payout_Date__c,status__c=pyLI.payout__r.status__c));//need this map to find the last payout date after cancellation
            payablePayoutsMap.put(pyLI.payable__c,payoutList);
        }
        map<id,payable__c> payableMap = new map<id,payable__c>([select id,Last_Payout_Date__c,Payable_Amount_Paid_Cash__c,Amount_Paid_via_Offset__c,
                                                        (select id,Line_Payout_Amount__c from payable_line_item__r) from payable__c where Id IN :payoutPayableIdMap.values()]);
        if(setPyStdLIIds.size()>0){
           for(payment__c py :[select id,payment_status__c,amount__c,Y_Hidden_Payment_Std_Line_Item__c from payment__c where Y_Hidden_Payment_Std_Line_Item__c IN :setPyStdLIIds]){
                mapOffsetPayments.put(py.Y_Hidden_Payment_Std_Line_Item__c,py);
           }
        }
        system.debug('size of payoutPayableIdMap::'+payoutPayableIdMap.size());  
        system.debug('size of payablePayoutsMap::'+payablePayoutsMap.size());                                                
        for(payout__c po :cancelledPos){
            payable__c py = payableMap.get(payoutPayableIdMap.get(po.id));

            if('Offset'.equalsIgnoreCase(po.Payout_Type__c)){
                py.Amount_Paid_via_Offset__c  = leasewareUtils.ZeroIfNull( py.Amount_Paid_via_Offset__c ) -  leasewareUtils.ZeroIfNull( po.payout_amount__c);
                payment__c pmt = mapOffsetPayments.get(po.Payment_Line_Item__c);//for cancelling corresponding payment for Paid via Claims offset
                if(pmt !=null){
                    pmt.payment_status__c = 'Cancelled';
                }
            }
            else{ //Cash
                py.Payable_Amount_Paid_Cash__c = leasewareUtils.ZeroIfNull( py.Payable_Amount_Paid_Cash__c) -  leasewareUtils.ZeroIfNull( po.payout_amount__c);
            }
            
            //expecting only 1 line item
            if(null !=py.payable_line_item__r[0]){
                py.payable_line_item__r[0].Line_Payout_Amount__c = leasewareUtils.ZeroIfNull( py.payable_line_item__r[0].Line_Payout_Amount__c) -  leasewareUtils.ZeroIfNull( po.payout_amount__c);
                pyLIList.add(py.payable_line_item__r[0]);
            }
            py.Last_Payout_Date__c = getLastPayoutDate(payablePayoutsMap.get(py.id));
            system.debug('py.id::'+py.id);
            system.debug('py.Payable_Amount_Paid_Cash__c::'+py.Payable_Amount_Paid_Cash__c);
            system.debug('py.Line_Payout_Amount__c::'+py.payable_line_item__r[0].Line_Payout_Amount__c);
            system.debug('py.Last_Payout_Date__c::'+py.Last_Payout_Date__c);
        }
       
        try{
            LeaseWareUtils.TriggerDisabledFlag=true;//only trigger needs to be off to update payable, WF needs to be run to update status
            if(payableMap.size()>0){update payableMap.values();}
            LeaseWareUtils.TriggerDisabledFlag=false;
            
            LeasewareUtils.setTriggersAndWFsOff(); 
            LeaseWareUtils.TriggerDisabledFlag=true;
                if(pyLIList.size()>0){update pyLIList;}
            LeasewareUtils.setTriggersAndWFsON(); 
            LeaseWareUtils.TriggerDisabledFlag=false;
            
            if(mapOffsetPayments.values().size()>0){
                update mapOffsetPayments.values();//calling update to perform the cancellation on payments.
            }
        }
        catch(DMLException e){
            throw new DMLException(e);
        }
        system.debug('updatePayablePLIOnCancellation (-) ');
    }

    /*
    This method invokes the UpdateEventClaims method on Payable Trigger to update the Total Payout amount 
    on the claim record
    Called from after Update
    */
    private void updateEventClaimOnCancellation(map<id, Payout__c> cancelledPos){
        system.debug('updateEventClaimOnCancellation (+) ');
        set<Id> setPyIds = new set<Id>();
        for(payable_line_item__c pyLI : [select payable__c,payout__c from payable_line_item__c where payout__c IN :cancelledPos.keyset()]){
            setPyIds.add(pyLI.payable__c);
        }

        if(setPyIds.size() > 0){
            List<Payable__c> lstClaimPayables = [select id, Payable_Type__c,Claim__c from Payable__c where id in :setPyIds];
            PayableTriggerHandler.updateEventClaims(lstClaimPayables);            
        }      
        system.debug('updateEventClaimOnCancellation (-) ');
    }

    /***
     * gets the latest payout date from the list of payouts
     * Called from updatePayablePLIOnCancellation
     */
    private Date getLastPayoutDate(list<payout__c> payouts){
        system.debug('Inside getLastPayoutDate(+)');
        Date lastPyDate = null;
        for(payout__c po : payouts){
            system.debug('po.status__c::'+po.status__c);
            if('Cancelled'.equals(po.status__c) || 'Pending'.equals(po.status__c))continue;
            if(lastPyDate == null)lastPyDate = po.Payout_Date__c;
            else if(po.Payout_Date__c > lastPyDate)lastPyDate = po.payout_date__c;
        }
        system.debug('Inside getLastPayoutDate(+)');
        return lastPyDate;
        
    }

    /***
     * Updates payable on Approval
     * Called from after Update and after Insert
     */

    public static void updatePayableOnApproval(map<id,payout__c > payoutMap){
        system.debug('updatePayableOnApproval(+)');
        map<id,list<payout__c> > payablesListMap = new map<id,list<payout__c> >();
        list<payable__c> listToUpdate = new list<payable__c>();
        set<id> payableIds = new set<id>();

        map<id,payable_line_item__c> lineItemsMap = new  map<id,payable_line_item__c>([select id,payable__c,payout__c,payable__r.Cash_Security_Deposit__c from payable_line_item__c
                                                        where payout__c In :payoutMap.keySet()]);
        for(payable_line_item__c pli : lineItemsMap.Values()){
            payableIds.add(pli.payable__c);
        }
        map<id,payable__c > payablesMap = new map<id,payable__c >([select id,Cash_Security_Deposit__c,Payable_Amount_Paid_Cash__c,Amount_Paid_via_Offset__c,Last_Payout_Date__c from payable__c where id IN :payableIds]);
        for(payable_line_item__c pli : lineItemsMap.Values()){
            payout__c payout = payoutMap.get(pli.payout__c);
            list<payout__c> payoutList = payablesListMap.get(pli.payable__c);
            if(payoutList == null){
                payoutList = new list<payout__c>();
            }
            payoutList.add(payout);
            payablesListMap.put(pli.payable__c,payoutList);//all the payouts of the payable
        }
        system.debug('Size of payablesListMap::'+payablesListMap.size());

        for(Id pby :payablesListMap.keySet()){
            list<payout__c> poList = payablesListMap.get(pby);
            payable__c payable = payablesMap.get(pby);
            if(payable !=null){
                Decimal amountPaid = 0.0, amountOffset = 0.0;
                Date  lastPyDate = null;
                if(poList!=null && poList.size()>0){
                    for(payout__c po : poList){
                        system.debug('Payables_Type__c:'+po.Payout_Type__c);
                        system.debug('Payout_Date__c:'+po.Payout_Date__c);
                        if('Offset'.equalsIgnoreCase(po.Payout_Type__c)){
                            amountOffset += LeaseWareUtils.zeroIfNull(po.Payout_Amount__c);
                        }
                        else{ //Cash                            
                            amountPaid += LeaseWareUtils.zeroIfNull(po.Payout_Amount__c);
                        }
                        if(lastPyDate == null)lastPyDate = po.Payout_Date__c;
                        else if(po.Payout_Date__c > lastPyDate)lastPyDate = po.payout_date__c;
                    }
                    system.debug('Amount Paid ::'+amountPaid);
                    system.debug('lastPyDate::'+lastPyDate);
                    //payable__c payable = new payable__c(id = pby);
                    payable.Payable_Amount_Paid_Cash__c = LeaseWareUtils.zeroIfNull(payable.Payable_Amount_Paid_Cash__c)+amountPaid;
                    payable.Amount_Paid_via_Offset__c = LeaseWareUtils.zeroIfNull(payable.Amount_Paid_via_Offset__c )+amountOffset;
                    if(payable.Last_Payout_Date__c !=null && payable.Last_Payout_Date__c < lastPyDate)payable.Last_Payout_Date__c = lastPyDate;
                    else if(payable.Last_Payout_Date__c == null)payable.Last_Payout_Date__c = lastPyDate;
                    listToUpdate.add(payable);
                }//end of poList
            }//pby null ends
        }
        if(!listToUpdate.isEmpty()){
            try{
                update listToUpdate;
            }
            catch(DmlException e){
                System.debug(e);
                throw new DmlException(e.getMessage());
            }
        }
        system.debug('updatePayableOnApproval(-)');
    }

       /***
     * This method is to check the payout status before moving the status to Cancelled-Pending/Approved.
     * called from before update.
     */
    @testVisible
    private static boolean checkStatusForCancelUndoCancel(payout__c curPo,payout__c oldPo){
        System.debug('checkAndSetStatus:(+)');
        
        boolean isError = false;
        system.debug('oldPo.status__c::'+oldPo.status__c);
        if(oldPo!=null){
            if(bIsCancelPayout){
                if(!('Approved'.equals(oldPo.status__c))){
                    system.debug('Error in status');
                    isError = true;
                }
            }
            if(bIsUndoCancellation){
                if(curPo.status__c != oldPo.status__c || !'Cancelled-Pending'.equals(oldPo.status__c)){
                    system.debug('Error in status');
                    isError = true;
                }
            }
        }
        System.debug('checkAndSetStatus:(-)');
        return isError;
    }
    /*********
     * set the status of the payment corresponding to the Offset payout when Cancelled or Undo Cancelled.
     * Called from afterUpdate.
     */
    private void  setOffsetPaymentStatus(list<payout__c>  cancelList){
        payout__c payout = cancelList[0]; // this is called from quick action so expecting only one record.
        if('Offset'.equals(payout.payout_type__c)){
            payment__c payment = [select id,payment_status__c,Date_of_Reversal__c from payment__c where Y_Hidden_Payment_Std_Line_Item__c = : payout.Payment_Line_Item__c] ;
            if(payment!=null ){
                if(bIsCancelPayout){
                    payment.payment_status__c = 'Cancelled-Pending';
                    payment.Date_of_Reversal__c = payout.cancellation_date__c;
                }
                if(bIsUndoCancellation){
                    payment.payment_status__c = 'Approved';
                    payment.Date_of_Reversal__c = null;
                } 
                LeaseWareUtils.TriggerDisabledFlag=true;
                        update payment;
                LeaseWareUtils.TriggerDisabledFlag=false;
            }
        }
    }

    /*
    * This method invokes the supplemental rent transactions batch in case of creation and status update
    * of the event payouts. 
    * Called from after insert, after update, before delete
    */
    private void callSRTransactionsBatch(){

        List<Payout__c> lstPO = (List<Payout__c>)(trigger.isDelete?trigger.old:trigger.new);
        Set<Id> setLeaseId = new Set<Id>();
    
        if(trigger.isInsert || trigger.isDelete){
            for(Payout__c PO: lstPO){
                if('MR Claim Event'.equals(PO.Payables_Type__c)){
                    setLeaseId.add(PO.Lease__c);
                }
            }
        }
        else if(trigger.isUpdate){
            for(Payout__c PO: lstPO){
                if(!'MR Claim Event'.equals(PO.Payables_Type__c)) continue;
                Payout__c oldPO = (Payout__c)trigger.oldMap.get(PO.id);                        
                if((oldPO.Status__c != PO.Status__c) && ('Approved'.equals(PO.Status__c) || 'Cancelled'.equals(PO.Status__c))){                        
                    setLeaseId.add(PO.Lease__c);
                }
            }
        }

        if(!System.isFuture() && !System.isBatch() && !Test.isRunningTest() && setLeaseId.size()>0){           
            System.debug('Creating transactions from Payout trigger :');
            Database.executeBatch(new SupplementalRentTransactionsBatch(setLeaseId,false),SupplementalRentTransactionsBatch.getBatchSize());            
        } 
    }
    
}