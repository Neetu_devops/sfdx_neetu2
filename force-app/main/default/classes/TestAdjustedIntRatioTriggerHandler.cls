@isTest
public class TestAdjustedIntRatioTriggerHandler {
          
    @testSetup
     static void CRUD_AdjustedIntervalRecord(){
        
		//Create Maintenance Program
        Maintenance_Program__c newMP = new Maintenance_Program__c(Name='TestMPN'
            ,Aircraft_Type__c = '737-300', Historical_Utilization_Months__c = 10, Variant__c = null,Current_Catalog_Year__c ='2018');
         LeaseWareUtils.clearFromTrigger();
         insert  newMP;      
        
        //Create Maintenance Program Event
        Maintenance_Program_Event__c insListMPE = new Maintenance_Program_Event__c();
	      	
            insListMPE.Assembly__c = 'Engine';
            insListMPE.Clearance_Period_Critreia__c = null;
            insListMPE.Event_Cost__c = 20000;
            insListMPE.Never_Exceed_Period_Cycles__c = 1000;
            insListMPE.Never_Exceed_Period_Hours__c =  2000;
            insListMPE.Never_Exceed_Period_Months__c = 30;
            insListMPE.Maintenance_Program__c = newMP.id;
            insListMPE.Event_Type_Global__c='LLP Replacement'; 
        LeaseWareUtils.clearFromTrigger();                  
        insert  insListMPE; 
        
        //Create Maintenance Program Event Catalog   
        Maintenance_Program_Event_Catalog__c MPECatalog = new Maintenance_Program_Event_Catalog__c();
            MPECatalog.Maintenance_Program_Event__c= insListMPE.Id;
            MPECatalog.Name='2018';
            MPECatalog.Event_Cost__c=1000;
            MPECatalog.Event_Count__c='Modular';
            MPECatalog.MP_Applicability__c='CF6-80C2-A2/B2'; 
        LeaseWareUtils.clearFromTrigger();      
        insert MPECatalog;     
        
		
        //Create  Adjusted Interval(Ratio) 
        Adjusted_Interval_Ratio__c ratiorec = new Adjusted_Interval_Ratio__c();
            ratiorec.Maintenance_Program_Event_Catalog__c= MPECatalog.Id;
            ratiorec.Name='1.0';
            ratiorec.Adjusted_Interval_Hours__c=1000;        
        LeaseWareUtils.clearFromTrigger(); 
        insert  ratiorec;
         
        //Read
        Adjusted_Interval_Ratio__c AIRec = [select id,name from Adjusted_Interval_Ratio__c  where id =:ratiorec.id];
         
        //Update
        AIRec.name = '12' ;
        LeaseWareUtils.clearFromTrigger(); 
        Update  AIRec;

        //Delete
        LeaseWareUtils.clearFromTrigger(); 
        delete  AIRec;            
 
    }
    
    
    static testMethod void testDuplicateCheck() {
        
            Maintenance_Program_Event_Catalog__c MPErec = [select id from Maintenance_Program_Event_Catalog__c];
           //Create  Adjusted Interval(Ratio) 
            Adjusted_Interval_Ratio__c ratiorec = new Adjusted_Interval_Ratio__c();
                ratiorec.Maintenance_Program_Event_Catalog__c= MPErec.Id;
                ratiorec.Name='1.0';
                ratiorec.Adjusted_Interval_Hours__c=1000;
             LeaseWareUtils.clearFromTrigger(); 
             insert  ratiorec;
        
            Adjusted_Interval_Ratio__c AIRrec = new Adjusted_Interval_Ratio__c();
                AIRrec.Maintenance_Program_Event_Catalog__c= MPErec.Id;
                AIRrec.Name='1.0';
                AIRrec.Adjusted_Interval_Hours__c=2000; 
        
           try{           
                                  
                    LeaseWareUtils.clearFromTrigger();
                    insert  AIRrec;
                    String str=AIRrec.Maintenance_Program_Event_Catalog__c + '-' + AIRrec.Name ;
                    System.assertEquals(AIRrec.Composite_Key_Field__c, str);
             
              }catch(Exception e){
                System.debug('e.getMessage() ' + e.getMessage());
                String expectedError = 'DUPLICATES_DETECTED';
                    if(e.getMessage().contains(expectedError)) 
                        system.assert(true);
                    else 
                        system.assert(false);              
               }
            
            AIRrec.Name='2.0';
            LeaseWareUtils.clearFromTrigger();
            insert  AIRrec;
            Adjusted_Interval_Ratio__c currec = [select id,Name,Maintenance_Program_Event_Catalog__c
                                                          from Adjusted_Interval_Ratio__c where Name ='2.0'];
          try{     
                currec.Name = '1.0';
                LeaseWareUtils.clearFromTrigger();
                update currec;
            } catch(Exception e){
                System.debug('e.getMessage() ' + e.getMessage());
                String expectedError = 'DUPLICATES_DETECTED';
                    if(e.getMessage().contains(expectedError))
                        System.assert(true);
                    else 
                        System.assert(false);                    
                }
    }
    
}