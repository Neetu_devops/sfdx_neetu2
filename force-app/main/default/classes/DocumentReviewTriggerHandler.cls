public class DocumentReviewTriggerHandler implements ITrigger{

    // Member variable to hold the Id's of Accounts 'in use'
    //private Set<Id> m_inUseIds = new Set<Id>();
     
    // Member variable to record Audit records
    //private List<Audit__c> m_audits = new List<Audit__c>();
     
    // Constructor
    private final string triggerBefore = 'DocumentReviewTriggerHandlerBefore';
    private final string triggerAfter = 'DocumentReviewTriggerHandlerAfter';
    public DocumentReviewTriggerHandler()
    {
    }
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    }
         
    public void beforeInsert()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('DocumentReviewTriggerHandler.beforeInsert(+)');
			
	    system.debug('DocumentReviewTriggerHandler.beforeInsert(-)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('DocumentReviewTriggerHandler.beforeUpdate(+)');

    	
    	system.debug('DocumentReviewTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	//if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		//LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('DocumentReviewTriggerHandler.beforeDelete(+)');
    	
    	
    	system.debug('DocumentReviewTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DocumentReviewTriggerHandler.afterInsert(+)');
    		InsUpdAftCall();
    	system.debug('DocumentReviewTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DocumentReviewTriggerHandler.afterUpdate(+)');
    		InsUpdAftCall();
    	
    	system.debug('DocumentReviewTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DocumentReviewTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('DocumentReviewTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('DocumentReviewTriggerHandler.afterUnDelete(+)');
    	
    	// code here
    	
    	system.debug('DocumentReviewTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }

	// event : After Insert, After Update
	private void InsUpdAftCall(){
		
		list<AC_Document_Review__c> newList = (list<AC_Document_Review__c>)trigger.new;
		list<Aircraft_Document_Review_List__c> insDocRevChecklist = new list<Aircraft_Document_Review_List__c>();
		for(AC_Document_Review__c curRec: newList){
			if((trigger.IsInsert && curRec.Document_Template__c !=null) || (trigger.IsUpdate && curRec.Document_Template__c !=((AC_Document_Review__c)trigger.oldmap.get(curRec.Id)).Document_Template__c  )){
				
				delete [select id from Aircraft_Document_Review_List__c where Aircraft_Record_Review__c = :curRec.Id];
				Document_Template_Checklist__c[] listDocTempChecklist = [select id from Document_Template_Checklist__c where Document_Template__c =:curRec.Document_Template__c AND Active__c=true];
				for(Document_Template_Checklist__c   DocTempRec :listDocTempChecklist){
					insDocRevChecklist.add(new Aircraft_Document_Review_List__c(
							Aircraft_Record_Review__c=curRec.Id
							,Document_Template_Checklist__c = DocTempRec.Id
						));
				}
				
			}
		}
		if(!insDocRevChecklist.IsEmpty()) insert insDocRevChecklist;
		
	}

}