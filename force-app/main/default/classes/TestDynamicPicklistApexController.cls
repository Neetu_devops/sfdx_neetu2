@isTest
private class TestDynamicPicklistApexController {
    @isTest static void test_dynamicPicklist() {
        Test.startTest();
        DynamicPicklistApexController.getDynPickListDefault('', '', null, null,'');
        DynamicPicklistApexController.getDynPickListDefault('Aircraft__c', 'Aircraft_Type__c', null, null,'');
        DynamicPicklistApexController.getDynPickListDefault('Marketing_Activity__c', 'Marketing_Rep_c__c', null, null,''); 
        
        DynamicPicklistApexController.getDynPickList('', '', null, null);
        DynamicPicklistApexController.getDynPickList('Aircraft__c', 'Aircraft_Type__c', null, null);
        DynamicPicklistApexController.getDynPickList('Marketing_Activity__c', 'Marketing_Rep_c__c', null, null); 
        
        String resBody = '{"controllerValues":{},"defaultValue":null,"url":"/services/data/v41.0/ui-api/object-info/Aircraft_Proposal__c/picklist-values/Asset_Type__c/0121U000000UfvgQAC","values":[{"attributes":null,"label":"Other","validFor":[],"value":"Other"},{"attributes":null,"label":"737-300","validFor":[],"value":"737-300"}]}';
        Map<String,String> responseHeader = new Map<String,String>{ 'Authorization' => 'OAuth '+UserInfo.getSessionId() };            
            
        TestSingleRequestMock fakeResponse = new TestSingleRequestMock(200,resBody,responseHeader);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        DynamicPicklistApexController.getDynPickListDefault('Marketing_Activity__c', 'Deal_Status__c', null, 'Engine@Lease', 'New');
        DynamicPicklistApexController.getDynPickListDefault('Aircraft_Proposal__c', 'Aircraft_Type__c', 'Aircraft', '', '');
        DynamicPicklistApexController.getDynPickListDefault('Aircraft_Proposal__c', 'Aircraft_Type__c', '', 'Engine', '');
        
        DynamicPicklistApexController.getDynPickList('Marketing_Activity__c', 'Deal_Status__c', null, 'Engine@Lease');
        DynamicPicklistApexController.getDynPickList('Aircraft_Proposal__c', 'Aircraft_Type__c', 'Aircraft', '');
        DynamicPicklistApexController.getDynPickList('Aircraft_Proposal__c', 'Aircraft_Type__c', '', 'Engine');
        
        Test.stopTest();
    }
    
    public class TestSingleRequestMock implements HttpCalloutMock {
        protected Integer code;
        protected String bodyAsString;
        protected Map<String, String> responseHeaders;
        
        public TestSingleRequestMock(Integer code, String body, Map<String, String> responseHeaders) {
            this.code = code;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setBody(bodyAsString);
            
            if (responseHeaders != null) {
                for (String key : responseHeaders.keySet()) {
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            return resp;
        }
    }
}