public class AICTriggerHandler implements ITrigger{
   
       
    public AICTriggerHandler()
    {
    }
 //Added new commnt
 
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
        // If this a delete trigger Cache a list of Account Id's that are 'in use'
    }
     
    public void bulkAfter()
    {
    	
    	 
    }
         
    public void beforeInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	
    	system.debug('AICTriggerHandler.beforeInsert(+)');
	
		AircraftInDeal_InsUpdDel();
		getCMLRFromWF();
	    system.debug('AICTriggerHandler.beforeInsert(+)');   	
    }
     
    public void beforeUpdate()
    {
    	//Before check  : keep code here which does not have issue with multiple call .
    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AICTriggerHandler.beforeUpdate(+)');
    	
  		AircraftInDeal_InsUpdDel();
  		getCMLRFromWF();
    	system.debug('AICTriggerHandler.beforeUpdate(-)');
    }
     
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete()
    {  

    	if(LeaseWareUtils.isFromTrigger(triggerBefore)) return;
		LeaseWareUtils.setFromTrigger(triggerBefore);
    	system.debug('AICTriggerHandler.beforeDelete(+)');
    	
     	
    	AircraftInDeal_UpdateMSNs();
    	system.debug('AICTriggerHandler.beforeDelete(-)');         

    }
     
    public void afterInsert()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AICTriggerHandler.afterInsert(+)');
    	
    	list<Aircraft_In_Deal__c> newList = (list<Aircraft_In_Deal__c>)trigger.new;
    	set<string> setIds = new set<string>();
    	for(Aircraft_In_Deal__c currec:newList){
    		setIds.add(currec.Marketing_Deal__c);
    	}
    	list<Deal_Proposal__c> listExistngProspect = [select id,Deal__c from Deal_Proposal__c where Deal__c in :setIds];
    	// if it not getting called from canvass
    	if(!LeaseWareUtils.isFromTrigger('CalledFromCanvass')){
    		createProspectTerm(listExistngProspect);
    	}
    	AircraftInDeal_UpdateMSNs();
    	system.debug('AICTriggerHandler.afterInsert(-)');     	
    }
     
    public void afterUpdate()
    {
    	if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AICTriggerHandler.afterUpdate(+)');
      	AircraftInDeal_UpdateMSNs();
    	system.debug('AICTriggerHandler.afterUpdate(-)');     	
    }
     
    public void afterDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AICTriggerHandler.afterDelete(+)');
    	
    	
    	system.debug('AICTriggerHandler.afterDelete(-)');     	
    }

    public void afterUnDelete()
    {
    	//if(LeaseWareUtils.isFromTrigger(triggerAfter)) return;
		//LeaseWareUtils.setFromTrigger(triggerAfter);
    	system.debug('AICTriggerHandler.afterUnDelete(+)');
    	
    	
    	
    	system.debug('AICTriggerHandler.afterUnDelete(-)');     	
    }
     
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally()
    {
        // insert any audit records

    }


	//after insert
	private void createProspectTerm(list<Deal_Proposal__c> listExistngProspect){
		list<Aircraft_In_Deal__c> newList = (list<Aircraft_In_Deal__c>)trigger.new;
		
		map<string,list<Aircraft_In_Deal__c>> newMapAIC = new map<string,list<Aircraft_In_Deal__c>>();
		list<Aircraft_In_Deal__c> tempListAIC;
		for(Aircraft_In_Deal__c curRec: newList){
			if(newMapAIC.containsKey(curRec.Marketing_Deal__c)){
				tempListAIC = newMapAIC.get(curRec.Marketing_Deal__c);
			}else{
				tempListAIC = new list<Aircraft_In_Deal__c>();
			}
			tempListAIC.add(curRec);
			newMapAIC.put(curRec.Marketing_Deal__c,tempListAIC);
		}
		list<MSN_s_Of_Interest__c> listDealTermsToInsert = new list<MSN_s_Of_Interest__c>();
		if(listExistngProspect.size()>0){
			for(Deal_Proposal__c prospectRec  :listExistngProspect){
				for(Aircraft_In_Deal__c curAIC:newMapAIC.get(prospectRec.Deal__c)){
					listDealTermsToInsert.add(
					            			new MSN_s_Of_Interest__c(Name='Dummy - Will be replaced by WF/Field Update', Deal_Proposal__c=prospectRec.Id, Aircraft__c=curAIC.Id)
					            		);					
				}
			}
		}
		if(listDealTermsToInsert.size()>0) insert listDealTermsToInsert;
		
	}


	//before delete, before insert, before update
	private void AircraftInDeal_InsUpdDel() {

	    for(Aircraft_In_Deal__c curAiD : (list<Aircraft_In_Deal__c>)trigger.new){
	        
	        if(curAiD.aircraft__c==null && curAiD.Aircraft_In_Transaction__c == null){
	            curAiD.aircraft__c.addError('Please select either Aircraft or Aircraft In Transaction. One of the values is required.');
	            curAiD.Aircraft_In_Transaction__c.addError('Please select either Aircraft or Aircraft In Transaction. One of the values is required.');
	        }
			
			//Start Nagaveni_26_6_18
	    	//Task - Enforcement of duplicate MC for same aircraft does not work for New Orders?
        	//Check for not choosing both Aircraft and Aircraft in Transaction. 
            if (curAiD.aircraft__c != NULL && curAiD.Aircraft_In_Transaction__c != NULL){
                curAiD.addError('Cannot have both Aircraft and Aircraft in Transaction in Campaign, Choose either Aircraft or Aircraft In Transaction.');
	            continue; 
            }
             //End Nagaveni_26_6_18
	    
	    }  
	}
	
	
	//before insert, before update
	private void getCMLRFromWF()
	{
		
		Aircraft_In_Deal__c[] newProspects = (list<Aircraft_In_Deal__c>)trigger.new;

		set<string> setMSNs = new set<string>();
		for(Aircraft_In_Deal__c curAIC: newProspects)setMSNs.add(curAIC.MSN__c);
		
		map<string, Global_Fleet__c> mapWordFleet = new map<string, Global_Fleet__c>();
		list<Global_Fleet__c> listWF = [select id, CurrentMarketLeaseRate_m__c, AircraftType__c, SerialNumber__c
			 from Global_Fleet__c where SerialNumber__c in :setMSNs];
		system.debug('setMSNs='+setMSNs);
		
		list<Custom_Lookup__c> listCustomLookup = [select id, name,Lookup_Type__c, Sub_LookupType__c, Sub_LookupType2__c
            					from Custom_Lookup__c
            					where Lookup_Type__c = 'AT_MAP'
        				];
        map<string,string>	mapATType = new map<string,string>();			
		for(Custom_Lookup__c  curRec:listCustomLookup){
			mapATType.put(curRec.name,curRec.Sub_LookupType__c);
		}	 
		
		string AircraftTypeNew;
		for(Global_Fleet__c curWF : listWF){
			AircraftTypeNew = curWF.AircraftType__c;
			// converting AT based on Custom Lookup : e.g 737(NG) to 737
			if(mapATType.containsKey(AircraftTypeNew)) AircraftTypeNew = mapATType.get(AircraftTypeNew);
			mapWordFleet.put(AircraftTypeNew + '-' + curWF.SerialNumber__c, curWF);
		}
		system.debug('mapWordFleet='+mapWordFleet.size());
		Global_Fleet__c WFMatchTemp;
		for(Aircraft_In_Deal__c curAIC: newProspects){
			if(curAIC.Current_Market_Lease_Rate_M__c == null){
				WFMatchTemp= mapWordFleet.get(curAIC.Aircraft_Type__c + '-' + curAIC.MSN__c);
				if(WFMatchTemp != null)curAIC.Current_Market_Lease_Rate_M__c = WFMatchTemp.CurrentMarketLeaseRate_m__c;
			}
		}		
	}

	//before delete, after insert, after update
	private void AircraftInDeal_UpdateMSNs(){

	    list<Aircraft_In_Deal__c> cuAiDList = (trigger.isDelete?trigger.old:trigger.new);
	    
	    map<id, Aircraft_In_Deal__c> mapAllCampaigns = new map<id, Aircraft_In_Deal__c>();
		
		//Start Nagaveni_26_6_18
        //Task - Enforcement of duplicate MC for same aircraft does not work for New Orders?
        //AIT that is already part of Active campaign cannot be associated with another active campaign 
        map<id, String> mapAIT_to_Campaigns = new map<id, String>();
        //End Nagaveni_26_6_18
		
	    map<id, string> mapXCampaigns = new map<id, string>();
	    set<id> setUnawardedACs = new set<id>();
	    set<id> setNewAiCs = new set<id>();
	    set<id> setLeaseIds = new set<id>();
	    set<id> deletedIDs = new set<id>();
	    map<id, id> mapLeaseIds = new map<id, id>();
	    map<id,String> mapCampName = new map<id,String>();
	    map<id,String> mapCampCurOperator = new map<id,String>();
	    map<id,String> mapCampCurLessee = new map<id,String>();
	    
	    //Start Nagaveni_26_6_18
	    //Task - Enforcement of duplicate MC for same aircraft does not work for New Orders?
        //AIT that is already part of Active campaign cannot be associated with another active campaign 
	    //Added AIT to the Query
	    list<Aircraft_In_Deal__c> listAllAiCs = [select Id, aircraft__c, Aircraft_In_Transaction__c, aircraft__r.MSN_Number__c, aircraft__r.lease__c,
	    			Operator__c,MSN__c, Lessee__c, Awarded__c,  
	                Marketing_Deal__c, Marketing_Deal__r.Name,
	                Marketing_Deal__r.Campaign_Active__c, IsDeleted 
	                from Aircraft_In_Deal__c ALL ROWS ];
        //End Nagaveni_26_6_18
	
	    for(Aircraft_In_Deal__c curAiC : cuAiDList){
	    	setNewAiCs.add(curAiC.Id);
	    	if(trigger.isDelete) deletedIDs.add(curAiC.Id);
	    }
	
	    for(Aircraft_In_Deal__c curAiC : listAllAiCs){
	    	//Start Nagaveni_26_6_18
            //Task - Enforcement of duplicate MC for same aircraft does not work for New Orders?
        	//AIT that is already part of Active campaign cannot be associated with another active campaign 
            //Campaign is for either
                        
           	if(!curAiC.IsDeleted && curAiC.Awarded__c==false && !setNewAiCs.contains(curAiC.Id) ){
				if (curAiC.Aircraft_In_Transaction__c != NULL) {
					setUnawardedACs.add(curAiC.Aircraft_In_Transaction__c);
				}else {
					setUnawardedACs.add(curAiC.aircraft__c);
				}
				system.debug(curAiC.aircraft__c + ' Unawarded on ' + curAiC.Marketing_Deal__r.Name + '(' + curAiC.Marketing_Deal__c + ')');
	    	}
				
	    }
	
	    for(Aircraft_In_Deal__c curAiC : listAllAiCs){ //ALL ROWS to get all campaigns including the current one if the last AiC was deleted.
	        if(curAiC.aircraft__c != null) {
	            //system.debug('Adding ' + curAiC.aircraft__r.MSN_Number__c+ ' - ' + curAiC.Marketing_Deal__c + ' - ' + curAiC.Marketing_Deal__r.Name);
	            string strXCampaigns = '';
	            if(mapXCampaigns.containsKey(curAiC.aircraft__c)){
					//Start Nagaveni_12_7_18
                    //Changed <BR> to \n , In Lightning view line break is not working with <BR> , error message shows as <BR> appended.
	                strXCampaigns=mapXCampaigns.get(curAiC.aircraft__c) + '\n';
					//End Nagaveni_12_7_18
	            }
	            strXCampaigns+=(curAiC.Marketing_Deal__r.Name + ' (ID: ' + curAiC.Marketing_Deal__c + ')');
	            if(curAiC.Marketing_Deal__r.Campaign_Active__c && !curAiC.isDeleted) {
	            	//Insert into the map only if it is not awarded. Awarded can be added to other campaigns even if the Campaign is active.
	                if(setUnawardedACs.contains(curAiC.aircraft__c))mapXCampaigns.put(curAiC.aircraft__c, strXCampaigns);
	            }
	            mapAllCampaigns.put(curAiC.Marketing_Deal__c, curAiC);
	            if(curAiC.aircraft__r.lease__c != null){
	                setLeaseIds.add(curAiC.aircraft__r.lease__c);
	                mapLeaseIds.put(curAiC.id, curAiC.aircraft__r.lease__c);
	            }           
	        }
			
			//Start Nagaveni_26_6_18
            //Task - Enforcement of duplicate MC for same aircraft does not work for New Orders?
        	//AIT that is already part of Active campaign cannot be associated with another active campaign 
        	
            if (curAiC.Aircraft_In_Transaction__c != null){
                String strCampaignDetails = '';
                if (mapAIT_to_Campaigns.containsKey(curAiC.Aircraft_In_Transaction__c) ){
                    // AIT present in another campaign
                    strCampaignDetails=mapAIT_to_Campaigns.get(curAiC.Aircraft_In_Transaction__c)  +'\n' ;  
                }
                
	            strCampaignDetails+=(curAiC.Marketing_Deal__r.Name + ' (ID: ' + curAiC.Marketing_Deal__c + ') ');
                
	            if(curAiC.Marketing_Deal__r.Campaign_Active__c && !curAiC.isDeleted) {
	            	//Insert into the map only if it is not awarded. Awarded can be added to other campaigns even if the Campaign is active.
	                if(setUnawardedACs.contains(curAiC.Aircraft_In_Transaction__c))mapAIT_to_Campaigns.put(curAiC.Aircraft_In_Transaction__c, strCampaignDetails);
	            }
	            mapAllCampaigns.put(curAiC.Marketing_Deal__c, curAiC);
                                 
            }
            //End Nagaveni_26_6_18
			
	        if(!curAiC.IsDeleted){
	            mapCampName.put(curAiC.Marketing_Deal__c,(curAiC.MSN__c != null?(curAiC.Operator__c!=null?curAiC.MSN__c+'-'+curAiC.Operator__c:curAiC.MSN__c):((curAiC.Operator__c!=null?'N/A'+'-'+curAiC.Operator__c:'N/A'))));
	            mapCampCurOperator.put(curAiC.Marketing_Deal__c,curAiC.Operator__c);
	            mapCampCurLessee.put(curAiC.Marketing_Deal__c,curAiC.Lessee__c);
	        }
	    }
	
	
	    map<id, lease__c> mapLeases = new map<id, lease__c>([select Id, Next_Operator__c, Y_Hidden_Next_Operator__c 
	            from lease__c  where id in :setLeaseIds]);
	    map<id, operator__c> mapOps = new map<id, operator__c>([select Id, Name from operator__c]);
	    
	    list<lease__c> listLeasesToUpd = new list<lease__c>();
	    set<id> setCampaignIds = new set<id>();
	
	    for(Aircraft_In_Deal__c curAiC : cuAiDList){
	    	setCampaignIds.add(curAiC.Marketing_Deal__c);
	        if(curAiC.aircraft__c != null) {
	            system.debug('Checking ' + curAiC.Marketing_Deal__c + ' - ' + mapAllCampaigns.get(curAiC.Marketing_Deal__c));   
	            if(mapAllCampaigns.get(curAiC.Marketing_Deal__c).Marketing_Deal__r.Campaign_Active__c==false){
	                curAiC.addError('You cannot modify an aircraft in an inactive campaign.');
	                continue;
	            }
	            
	            if(!trigger.isDelete){
			//Start Nagaveni_12_7_18
	                //Changed <BR> to \n , In Lightning view line break is not working with <BR> , error message shows as <BR> appended.
	                if(!LeaseWareUtils.isSetupDisabled('Allow_Campaigns_With_Same_Prospect')){
				if(mapXCampaigns.containsKey(curAiC.aircraft__c) && mapXCampaigns.get(curAiC.aircraft__c).contains('\n')){//At least two campaigns including the current one.
					//End Nagaveni_12_7_18
					system.debug('This campaign is ' + mapAllCampaigns.get(curAiC.Marketing_Deal__c).Marketing_Deal__r.Name + ' (ID: ' + curAiC.Marketing_Deal__c + ')');
					curAiC.addError('An aircraft can be added to only one active campaign. This aircraft is already present in ' 
								+ mapXCampaigns.get(curAiC.aircraft__c).remove(mapAllCampaigns.get(curAiC.Marketing_Deal__c).Marketing_Deal__r.Name + ' (ID: ' + curAiC.Marketing_Deal__c + ')'), false);
					continue;
			
				}
	                }
	            }
	
	            if(!mapLeaseIds.containsKey(curAiC.Id))continue;
	            lease__c curLease = mapLeases.get(mapLeaseIds.get(curAiC.Id));
	            if(trigger.isDelete || (trigger.isUpdate && curAiC.Next_Operator__c != ((Aircraft_In_Deal__c)trigger.oldMap.get(curAiC.Id)).Next_Operator__c) 
	                    || curLease.Next_Operator__c != curAiC.Next_Operator__c){
	                curLease.Next_Operator__c=(trigger.isDelete?null:curAiC.Next_Operator__c);
	               	if(curLease.Next_Operator__c!=null)	curLease.Y_Hidden_Next_Operator__c=mapOps.get(curAiC.Next_Operator__c).Name;
	               	else curLease.Y_Hidden_Next_Operator__c =null;
	                listLeasesToUpd.add(curLease);
	            }
	            
	        }
			
			//Start Nagaveni_26_6_18
             //Task - Records are not locked for an Inactive Campaign if it is an AiT on AiC 
            //Cannot change AIT when campaign is inactive.
            
           if ( curAiC.Aircraft_In_Transaction__c != NULL ) {
           		if(mapAllCampaigns.get(curAiC.Marketing_Deal__c).Marketing_Deal__r.Campaign_Active__c==false){
	                curAiC.addError('You cannot modify an Aircraft In Transaction in an inactive campaign.');
	                continue;
	            }
                        
                if(!trigger.isDelete){
                    if( mapAIT_to_Campaigns.containsKey(curAiC.Aircraft_In_Transaction__c) && mapAIT_to_Campaigns.get(curAiC.Aircraft_In_Transaction__c).contains('\n') ){
                        	//At least two campaigns including the current  Aircraft in Transaction.
	                    	curAiC.addError('Aircraft in Transaction can be added to only one active campaign. This Aircraft in Transaction is already present in ' 
	                               				+ mapAIT_to_Campaigns.get(curAiC.Aircraft_In_Transaction__c).remove(mapAllCampaigns.get(curAiC.Marketing_Deal__c).Marketing_Deal__r.Name + ' (ID: ' + curAiC.Marketing_Deal__c + ')'), false) ;
                        	continue; 
                    }
                }
            }
	        
	    }

		try{
			//update MSN in Deal_Proposal__c
			system.debug('Update Related Term MSN+');
			// if the AIT Name is getting changed, then update MSN list and Awarded list on Prospect.
			if(trigger.isupdate && LeaseWareUtils.IsFromTrigger('UpdateProspectOnChangeOf_MSN_Name')){
				string msnStr,awardedMSNStr;	
				set<string> setDealprospect = new set<string>();
				MSN_s_Of_Interest__c[] listOfTerms = [select id,Deal_Proposal__c from MSN_s_Of_Interest__c where Aircraft__c in :cuAiDList];
				for(MSN_s_Of_Interest__c curTerm:listOfTerms){
					setDealprospect.add(curTerm.Deal_Proposal__c);
				}
				if(!setDealprospect.isEmpty()){
					Deal_Proposal__c[] listDealProp = [select Interested_MSN_List__c,MSN_s_Deal_Proposal__c,Id 
								,(select MSN_of_Interest__c,Id,Awarded__c from Deal_Proposal_Aircraft__r) 
								from  Deal_Proposal__c where Id in :setDealprospect];
					for(Deal_Proposal__c curRec:listDealProp){
						msnStr = null;
						awardedMSNStr = null;
						curRec.MSN_s_Deal_Proposal__c = '';
						curRec.Interested_MSN_List__c = '';
						for(MSN_s_Of_Interest__c curMSN:curRec.Deal_Proposal_Aircraft__r){
							if(curMSN.Awarded__c){
								if(awardedMSNStr==null)  awardedMSNStr = curMSN.MSN_of_Interest__c;
								else awardedMSNStr += ', ' + curMSN.MSN_of_Interest__c;						
							}	
							if(msnStr==null)  msnStr = curMSN.MSN_of_Interest__c;
							else msnStr += ', ' + curMSN.MSN_of_Interest__c;					
							
						}
						if(msnStr!=null && msnStr.length()>254) msnStr = msnStr.left(251) + '...';
						curRec.Interested_MSN_List__c=msnStr;
		
						if(awardedMSNStr!=null && awardedMSNStr.length()>254) awardedMSNStr = awardedMSNStr.left(251) + '...';
						curRec.MSN_s_Deal_Proposal__c=awardedMSNStr;
														
					}
					
					LeaseWareUtils.TriggerDisabledFlag=true;// to make Trigger disabled
					update listDealProp;
					LeaseWareUtils.TriggerDisabledFlag=false;// to make Trigger disabled
				}
				system.debug('Update Related Term MSN-');
			}			
		}catch(exception ex){
	        system.debug('Commercial terms couldn\'t be updated due to exception '+ ex.getMessage() + '\n' + ex.getStackTraceString());
	        if(trigger.isUpdate) LeaseWareUtils.createExceptionLog(ex,' Commercial terms couldn\'t be updated due to exception' ,'Aircraft_In_Deal__c','','Update',true);
	        else if(trigger.isInsert) LeaseWareUtils.createExceptionLog(ex,' Commercial terms couldn\'t be updated due to exception' ,'Aircraft_In_Deal__c','','Insert',true);
	        else LeaseWareUtils.createExceptionLog(ex,'Commercial terms couldn\'t be updated due to exception' ,'Aircraft_In_Deal__c','','Delete',true);
	        
	    }

	    
	    try{
			system.debug('Update MSN+');
	        string strOp;
	        list<Aircraft_In_Deal__c> listAiCs;
	        map<id, Deal__c> mapCampaigns = new map<id, Deal__c>([select Id, Name, MSN_s_Marketed_In_This_Deal__c, Y_Hidden_Current_Operator__c,
	        				Y_Hidden_Current_Lessee__c  
	        				,(select id,MSN__c from Marketed_Aircraft_del__r where id not in :deletedIDs)
	        			from Deal__c where Id in :setCampaignIds]);

			string nameStr;	
			map<string,Decimal> DuplicateCountCheck = new map<string,Decimal>();
			string keyStr;
			for(Deal__c curCampaign: mapCampaigns.values()){
				nameStr = null;
				for(Aircraft_In_Deal__c curAiC:curCampaign.Marketed_Aircraft_del__r){
					if(nameStr==null)  nameStr = curAiC.MSN__c;
					else nameStr += ', ' + curAiC.MSN__c;
					
					// set count
					keyStr = curCampaign.Id+ curAiC.MSN__c;
					if(DuplicateCountCheck.containskey(keyStr)) DuplicateCountCheck.put(keyStr,DuplicateCountCheck.get(keyStr)+1);
					else DuplicateCountCheck.put(keyStr,1);
				}
				if(nameStr!=null && nameStr.length()>254) nameStr = nameStr.left(251) + '...';
				curCampaign.MSN_s_Marketed_In_This_Deal__c=nameStr;
				system.debug('curCampaign.MSN_s_Marketed_In_This_Deal__c=='+curCampaign.MSN_s_Marketed_In_This_Deal__c);	
			}
			



			
	        Deal__c parentDeal;
	        if(trigger.isInsert){
	            for(Aircraft_In_Deal__c curAID : cuAiDList){
	            	keyStr = curAID.Marketing_Deal__c+ curAID.MSN__c;
	            	system.debug('keyStr='+keyStr);
	            	system.debug('DuplicateCountCheck='+DuplicateCountCheck);
                    if(!LeaseWareUtils.isSetupDisabled('Allow_Campaigns_With_Same_Prospect')){
			if(DuplicateCountCheck.containsKey(keyStr) && DuplicateCountCheck.get(keyStr)>1){
				curAID.addError('MSN ' + curAID.MSN__c + ' is already present in the deal.');
				continue;
			}	
                    }           	
	                parentDeal = mapCampaigns.get(curAID.Marketing_Deal__c);
	                /*
	                system.debug(parentDeal.MSN_s_Marketed_In_This_Deal__c + ', ' + curAID.MSN__c );
	                if((parentDeal.MSN_s_Marketed_In_This_Deal__c + ', ').contains(curAID.MSN__c + ', ')){
	                    curAID.addError('MSN ' +curAID.MSN__c + ' is already present in the campaign.');
	                    return;
	                }
	                parentDeal.MSN_s_Marketed_In_This_Deal__c=(parentDeal.MSN_s_Marketed_In_This_Deal__c==null?'': (parentDeal.MSN_s_Marketed_In_This_Deal__c + ', ' )) + curAID.MSN__c;
	                */
	                // Override camp name
	                parentDeal.name = (curAID.MSN__c != null?(curAID.Lessee__c!=null?curAID.MSN__c+'-'+curAID.Lessee__c:curAID.MSN__c):((curAID.Lessee__c!=null?'N/A'+'-'+curAID.Lessee__c:'N/A')));
	                parentDeal.Y_Hidden_Current_Operator__c = curAID.Operator__c;
	                parentDeal.Y_Hidden_Current_Lessee__c = curAID.Lessee__c;
	                mapCampaigns.put(parentDeal.id, parentDeal);
	            }
	            listAiCs=trigger.new;
	            strOp = 'Insert';
	            
	        }else if(trigger.isUpdate){
	// 20150709 - Prospect list will be created only on AiC create. User would have pruned the auto-created list            
	//          list<Aircraft_In_Deal__c> AIDsToDel = new list<Aircraft_In_Deal__c>(); 
	//          list<Aircraft_In_Deal__c> AIDsToIns = new list<Aircraft_In_Deal__c>();
				Aircraft_In_Deal__c OldAICRec ;
	            for(Aircraft_In_Deal__c curAID : cuAiDList){
	            	keyStr = curAID.Marketing_Deal__c+ curAID.MSN__c;
	            	system.debug('keyStr='+keyStr);
	            	system.debug('DuplicateCountCheck='+DuplicateCountCheck);
	            	if(!LeaseWareUtils.isSetupDisabled('Allow_Campaigns_With_Same_Prospect')){
				if(DuplicateCountCheck.containsKey(keyStr) && DuplicateCountCheck.get(keyStr)>1){
					curAID.addError('MSN ' + curAID.MSN__c + ' is already present in the deal.');
					continue;
				}		            	
			}
	            	OldAICRec = (Aircraft_In_Deal__c)trigger.oldMap.get(curAID.Id);
	                parentDeal = mapCampaigns.get(curAID.Marketing_Deal__c);
	                //if(parentDeal==null) continue;
	                //system.debug(parentDeal.MSN_s_Marketed_In_This_Deal__c + ' - ' + curAID.MSN__c + ' - Old map ' + OldAICRec.MSN__c );
	                //if( !OldAICRec.MSN__c.equals(curAID.MSN__c) && (parentDeal.MSN_s_Marketed_In_This_Deal__c + ', ').contains(curAID.MSN__c + ', ')){
	                    //curAID.addError('MSN ' +curAID.MSN__c + ' is already present in the deal.');
	                    //return;
	                //}
	                //parentDeal.MSN_s_Marketed_In_This_Deal__c=(parentDeal.MSN_s_Marketed_In_This_Deal__c==null?'': (parentDeal.MSN_s_Marketed_In_This_Deal__c.replace(OldAICRec.MSN__c ,curAID.MSN__c)));
	                
					
					if(parentDeal.name.contains(curAID.MSN__c) || !OldAICRec.Aircraft__c.equals(curAID.Aircraft__c)){
                       parentDeal.name = (curAID.MSN__c != null?(curAID.Lessee__c!=null?curAID.MSN__c+'-'+curAID.Lessee__c:curAID.MSN__c):((curAID.Lessee__c!=null?'N/A'+'-'+curAID.Lessee__c:'N/A')));
                       parentDeal.Y_Hidden_Current_Operator__c = curAID.Operator__c;
                       parentDeal.Y_Hidden_Current_Lessee__c= curAID.Lessee__c;
                   }
	                
	                /*if(!OldAICRec.MSN__c.equals(curAID.MSN__c) || (OldAICRec.Operator__c==null && curAID.Operator__c!=null)   || (OldAICRec.Operator__c!=null && !OldAICRec.Operator__c.equals(curAID.Operator__c))){
	                    parentDeal.name = (curAID.MSN__c != null?(curAID.Operator__c!=null?curAID.MSN__c+'-'+curAID.Operator__c:curAID.MSN__c):((curAID.Operator__c!=null?'N/A'+'-'+curAID.Operator__c:'N/A')));
	                    parentDeal.Y_Hidden_Current_Operator__c = curAID.Operator__c;
	                    parentDeal.Y_Hidden_Current_Operator__c = curAID.Lessee__c;
	                }*/
	                mapCampaigns.put(parentDeal.id, parentDeal);
	// 20150709 - Prospect list will be created only on AiC create. User would have pruned the auto-created list            
	//              AIDsToDel.add(trigger.oldMap.get(curAID.Id));
	//              AIDsToIns.add(curAID);
	            }
	// 20150709 - Prospect list will be created only on AiC create. User would have pruned the auto-created list            
	//          if(AIDsToDel.size()>0)LeasewareUtils.upsertDPs(AIDsToDel, 'Delete');
	//          if(AIDsToIns.size()>0)LeasewareUtils.upsertDPs(AIDsToIns, 'Insert');
	        }else{ //Delete
	            for(Aircraft_In_Deal__c curAID : cuAiDList){
	                parentDeal = mapCampaigns.get(curAID.Marketing_Deal__c);
	                if(parentDeal.MSN_s_Marketed_In_This_Deal__c!=null){
	                    /*if(parentDeal.MSN_s_Marketed_In_This_Deal__c.equals(curAID.MSN__c)){
	                        parentDeal.MSN_s_Marketed_In_This_Deal__c=null;
	                    }else if(parentDeal.MSN_s_Marketed_In_This_Deal__c.startsWith(curAID.MSN__c + ', ')){
	                        parentDeal.MSN_s_Marketed_In_This_Deal__c=parentDeal.MSN_s_Marketed_In_This_Deal__c.remove(curAID.MSN__c + ', ');
	                    }else{
	                        parentDeal.MSN_s_Marketed_In_This_Deal__c=parentDeal.MSN_s_Marketed_In_This_Deal__c.remove(', ' + curAID.MSN__c);
	                    }*/
	                    if(parentDeal.name.contains(curAID.MSN__c)){
	                        if(mapCampName.containsKey(curAID.Marketing_Deal__c)) parentDeal.name = mapCampName.get(curAID.Marketing_Deal__c);
	                        else parentDeal.name = 'N/A';
	                    }
	                    if(parentDeal.Y_Hidden_Current_Operator__c != null && parentDeal.Y_Hidden_Current_Operator__c.contains(curAID.Operator__c)){
	                        if(mapCampCurOperator.containsKey(curAID.Marketing_Deal__c)) parentDeal.Y_Hidden_Current_Operator__c = mapCampCurOperator.get(curAID.Marketing_Deal__c);
	                        else parentDeal.Y_Hidden_Current_Operator__c = null;
	                    }
	                    if(parentDeal.Y_Hidden_Current_Lessee__c != null && parentDeal.Y_Hidden_Current_Lessee__c.contains(curAID.Lessee__c)){
	                        if(mapCampCurLessee.containsKey(curAID.Marketing_Deal__c)) parentDeal.Y_Hidden_Current_Lessee__c = mapCampCurLessee.get(curAID.Marketing_Deal__c);
	                        else parentDeal.Y_Hidden_Current_Lessee__c = null;
	                    }
	                    mapCampaigns.put(parentDeal.id, parentDeal);
	                }
	            }
	            listAiCs=trigger.old;
	            strOp = 'Delete';
	        }
	        system.debug('Update MSNmapCampaigns.values() ='+mapCampaigns.values().size());
	        update mapCampaigns.values();
	        system.debug('Update MSN-');
	        if(listLeasesToUpd.size()>0)database.update(listLeasesToUpd, false);
	         
	        if(strOp!=null)LeasewareUtils.upsertDPs(listAiCs, strOp);
	    }catch(exception ex){
	        system.debug('Parent deal couldn\'t be updated due to exception '+ ex.getMessage() + '\n' + ex.getStackTraceString());
	        if(trigger.isUpdate) LeaseWareUtils.createExceptionLog(ex,' Parent deal couldn\'t be updated due to exception' ,'Aircraft_In_Deal__c','','Update',true);
	        else if(trigger.isInsert) LeaseWareUtils.createExceptionLog(ex,' Parent deal couldn\'t be updated due to exception' ,'Aircraft_In_Deal__c','','Insert',true);
	        else LeaseWareUtils.createExceptionLog(ex,' Parent deal couldn\'t be updated due to exception' ,'Aircraft_In_Deal__c','','Delete',true);
	        
	    }

		
	}

}