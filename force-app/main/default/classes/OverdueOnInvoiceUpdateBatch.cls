public class OverdueOnInvoiceUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {
   
    string exceptionMessage='';
    //Hint: executing everyday to update overdue check box on invoice
    public static void callOverdueUpdateBatch() {
            Database.executeBatch(new OverdueOnInvoiceUpdateBatch());
        }        

        public Database.QueryLocator start(Database.BatchableContext BC) {
            // collect the all records 
            return Database.getQueryLocator(
            [select id, overdue__c,Date_of_MR_Payment_Due__c from invoice__c where status__c  IN ('Approved','Pending','Cancelled-Pending') AND payment_status__c IN('Partially Paid','Open') and  balance_due__c > 0]
            );
        }
        public void execute(Database.BatchableContext BC, List<invoice__c> invoiceList) {
            system.debug('execute of OverdueOnInvoiceUpdateBatch(+)');
            list<invoice__c> listToUpdate = new list<invoice__c>();
            system.debug('Invoice list size()::'+invoiceList.size());
            for(invoice__c inv : invoiceList){
                if(inv.Date_of_MR_Payment_Due__c!=null && inv.Date_of_MR_Payment_Due__c < system.today() ){
                   if(!inv.overdue__c){
                        inv.overdue__c = true;
                        listToUpdate.add(inv);
                   }
                }
                else{
                    if(inv.overdue__c){
                        inv.overdue__c = false;
                        listToUpdate.add(inv);
                    }
                }
                
            }
            /** if any of the invoices with a -ve balance due is marked as overdue,unchecking the same. */
            for(invoice__c inv :[select id,overdue__c from invoice__c where balance_due__c < 0 and overdue__c = true]){
                inv.overdue__c =  false;
                listToUpdate.add(inv);
            }
            if(!listToUpdate.isEmpty()){
                LeaseWareUtils.TriggerDisabledFlag=true; // trigger disabled
                database.SaveResult[] res = database.update( listToUpdate, false);
                for(database.SaveResult sr : res){
                    if (!sr.isSuccess()){
                        for(Database.Error err : sr.getErrors()) {
                            exceptionMessage += '\n\n'+'Unexpected error:'+ err.getStatusCode() + ': ' + err.getMessage() + err.getFields();
                        }                    
                    }
                }                  
                LeaseWareUtils.TriggerDisabledFlag=false; // trigger enabled
            }
            system.debug('execute of OverdueOnInvoiceUpdateBatch(-)');
        }
        
        public void finish(Database.BatchableContext BC) {
            // execute any post-processing operations like sending email
            if(exceptionMessage!=''){
                //sending email
                string scriptMethodName = 'OverdueOnInvoiceUpdateBatch';
                string subjectName = 'Unexpected error on batch Script OverdueOnInvoiceUpdateBatch: on Date '+ system.today().format() ;
                string emailBody = 'Please find the attachment.';
                list<Document> documents = new list<Document>();
                //log file
                documents.add(LeaseWareStreamUtils.createDoc(exceptionMessage,scriptMethodName + '__'+  system.now().format('yyMMddHHmmss')   + '.txt'));
                
                LeaseWareStreamUtils.emailWithAttachedLog(LeaseWareStreamUtils.getEmailAddressforScript(),subjectName ,emailBody,documents);
            }
            // Executing everyday to update the supplemental rent transactions on all Active leases
            SupplementalRentTransactionsHandler.updateTransactions(); 
        }
}
