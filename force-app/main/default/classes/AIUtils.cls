public without sharing class AIUtils {

/* A class for all utility methods related to AI features of LeaseWorks
 * 
 * 
 * 
 *
*/    
    /* Method: GetDealAnalysisData
     * Params :
     * string AircraftType 
     * int YearOfManufacture
     * int VintageRange
     * String CreditRating
     * int Criteria: 1 - Greater Than or Equal To, 2 - Equal To, 3 - Less Than Or Equal To. ('>=' => 1,'==' => 2, '<=' => 3)
     * int DataSource: 1 - Portfolio Acquisitions, 2 - Closed Deals, 3 - All Deals,
     * 					4 - Current Leases, 5 - Market Intel, 6 - World Fleet.
     * 
     * 
     *
	*/
    
    
    private class RentAndTerm{
        public String Id;
        public String Name;
        public decimal dRent;
        public decimal dTerm;
        public decimal dPurchasePrice;
        public String AircraftTypeVariant {get; set;}
        public String OperatorName {get; set;}
        public String OperatorId {get; set;}
    }
    
    private static List<Integer> getTermPeriod(String recordType) {
        Custom_Lookup__c[] termPeriodList = [select Id, Name, Description__c 
                                             from Custom_Lookup__c
                                             where Lookup_Type__c = 'CompsPanel' and 
                                             Sub_LookupType__c = :recordType];
        System.debug('getTermPeriod termPeriodList: '+termPeriodList);
        List<Integer> termPeriod = new List<Integer>();
        try{
            if(termPeriodList != null && termPeriodList.size() == 4){
                for(Custom_Lookup__c lookup : termPeriodList)
                    termPeriod.add(Integer.valueOf(lookup.Name));
                return termPeriod;
            }
        }
        catch(Exception e) {
            System.debug('getTermPeriod Exception: '+e);
        }
        return null;
    }
    
	public static list<DealAnalysisData.Data> GetDealAnalysisData(string AircraftType, decimal YearOfManufacture, 
                                                                  	decimal VintageRangeDec,
                                                                 	List<String> financialList, 
                                                                    integer DataSource, Integer leaseDateRange, String recordType, String variant)
    {
		
        system.debug('AIUtils:GetDealAnalysisData : AircraftType - ' + AircraftType + ' YearOfManufacture - ' + YearOfManufacture +  ' VintageRange - ' + VintageRangeDec + ' FinanacialRisk - '+ financialList + ' DataSource - ' + DataSource);
        system.debug('AIUtils:GetDealAnalysisData:  leaseDateRange - '+leaseDateRange +' recordType - '+recordType+ ' variant - '+variant);
        
        List<DealAnalysisData.Data> listDAData = new List<DealAnalysisData.Data>();
        map<integer, list<RentAndTerm>> mapDAData = new map<integer, list<RentAndTerm>>();
        
        list<RentAndTerm> listAllTerms = new list<RentAndTerm>();
        list<RentAndTerm> list01Terms = new list<RentAndTerm>();
        list<RentAndTerm> list02Terms = new list<RentAndTerm>();
        list<RentAndTerm> list03Terms = new list<RentAndTerm>();
        list<RentAndTerm> list04Terms = new list<RentAndTerm>();
        list<RentAndTerm> list05Terms = new list<RentAndTerm>();
        
        list<Integer> termPeriod = getTermPeriod(recordType);
        System.debug('termperiod before '+termPeriod);
        if(termPeriod == null) {
            termPeriod = new List<Integer>();
            if(recordType == 'Aircraft') {
                termPeriod.add(48);
                termPeriod.add(60);
                termPeriod.add(72);
                termPeriod.add(120);    
            }
            else {
                termPeriod.add(6);
                termPeriod.add(12);
                termPeriod.add(18);
                termPeriod.add(24);
            }
        }
        termPeriod.sort();
        System.debug('termperiod after '+termPeriod);
        
        if(leaseDateRange == null) 
            leaseDateRange = 0;
                
        Integer iStartYear, iEndYear;
        Integer VintageRange = Integer.valueOf(VintageRangeDec);
        string strQuery;
        datetime  leaseStartDate = date.today().addYears(-leaseDateRange);
            
        if(YearOfManufacture==null){
            iStartYear = 1900;
            iEndYear = date.today().year();
        }
        else{
			iStartYear = Integer.valueOf(YearOfManufacture);
			iEndYear = Integer.valueOf(YearOfManufacture);
            if(VintageRange==null)
                VintageRange=0;
               system.debug('AIUtils - Start and End Years ' + iStartYear +', ' + iEndYear + ', ' + VintageRange);
            iStartYear = iStartYear - VintageRange;
                system.debug('AIUtils - Start and End Years ' + iStartYear +', ' + iEndYear);
            iEndYear = iEndYear + VintageRange;
        }
        system.debug('AIUtils - Start and End Years ' + iStartYear +', ' + iEndYear);

        switch on DataSource{
            when 1{
                strQuery =  'select id, Name, Term_Mos__c,Current_Rent_Amount__c,Rent__c, Lease_Term__c,Marketing_Activity__c, ' +
                    		' Marketing_Activity__r.Name, Current_Operator__c, MSN__c ,' +
                    		' Purchase_Price__c ' +
                            ' from Aircraft_Proposal__c ' + 
                            ' where Deal_Type__c = \'Purchase\' and' +
                    		+ (recordType == 'Aircraft' ? (' Asset_Type__c = \'' + AircraftType+'-'+variant +'\' ') :
                            + (' Type_Variant__c = \'' + AircraftType+'-'+variant +'\' ')) + 
                    		' and CALENDAR_YEAR(convertTimezone(Date_Of_Manufacture__c)) >= '+ iStartYear + 
                            ' and CALENDAR_YEAR(convertTimezone(Date_Of_Manufacture__c)) <= ' + iEndYear + 
                            + (financialList== null?(''):(' and Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c in :financialList ')) +
                    		' order by Lease_Term__c';
                system.debug('DataSource 1 recordType: '+recordType + ' strQuery: ' +strQuery);
                list<Aircraft_Proposal__c> listDealAnalysis = database.query(strQuery);
                System.debug('listDealAnalysis :'+listDealAnalysis);
                for(Aircraft_Proposal__c curDA: listDealAnalysis){
                    RentAndTerm curRentAndTerm = new RentAndTerm();
                    curRentAndTerm.Id = curDA.Marketing_Activity__c;
                    curRentAndTerm.Name = curDA.Marketing_Activity__r.Name;
                    curRentAndTerm.dRent = (LeaseWareUtils.zeroIfNull(curDA.Rent__c) / 1000 ).setScale(0);
                    if(curDA.Term_Mos__c!=null)
                        curRentAndTerm.dTerm = curDA.Term_Mos__c;
                    else
                        curRentAndTerm.dTerm = 0;
                    curRentAndTerm.AircraftTypeVariant = curDA.MSN__c;
                    curRentAndTerm.OperatorName = curDA.Current_Operator__c;
                    curRentAndTerm.dPurchasePrice = curDA.Purchase_Price__c;
                    listAllTerms.add(curRentAndTerm);
                    if(curRentAndTerm.dTerm <= termPeriod[0]){
	                    list01Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[1]){
	                    list02Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[2]){
	                    list03Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[3]){
	                    list04Terms.add(curRentAndTerm);
                    }
                    else if(curRentAndTerm.dTerm > termPeriod[3]){
                        list05Terms.add(curRentAndTerm);
                    }
                }
            }
            when 2{ 
                List<String> statusList = new List<String>();
                String closedDealStatus = LeaseWareUtils.getLWSetup_CS('COMPS_PANEL_CLOSING_DEAL_STATUS');
                if(String.isNotBlank(closedDealStatus)) {
                    statusList = closedDealStatus.trim().split('\\s*,\\s*'); 
                }
                String activeDealsCondition = ''; 
                String activeDealString = LeaseWareUtils.getLWSetup_CS('COMPS_PANEL_CLOSED_DEAL_INACTIVE_CHECK');
                if(String.isNotBlank(activeDealString) && 'Active'.equals(activeDealString)) {
                    activeDealsCondition = 'Inactive_AT__c = false and ';
                } //If Blank, fetch both active and inactive deals

                System.debug('DataSource 2 closedDealStatus: '+closedDealStatus+ ', statusList '+statusList);
                strQuery = 'select id, Name, Term_Mos__c,Current_Rent_Amount__c,Rent__c, Lease_Term__c,Marketing_Activity__c, Marketing_Activity__r.Name, ' + 
                    		' MSN__c, Current_Operator__c '+
                            ' from Aircraft_Proposal__c ' + 
                            ' where Marketing_Activity__r.Deal_Status__c in :statusList and ' + activeDealsCondition + 
                    		+ (recordType == 'Aircraft' ? (' Asset_Type__c = \'' + AircraftType+'-'+variant +'\' ') :
                            + (' Type_Variant__c = \'' + AircraftType+'-'+variant +'\' ')) + 
                    		' and CALENDAR_YEAR(convertTimezone(Date_Of_Manufacture__c)) >= '+ iStartYear + 
                            ' and CALENDAR_YEAR(convertTimezone(Date_Of_Manufacture__c)) <= ' + iEndYear + 
                            + (financialList==null?(''):(' and Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c in :financialList ')) +
                    		' order by Lease_Term__c';
                system.debug(strQuery);
                system.debug('DataSource 2 recordType: '+recordType + ' strQuery: ' +strQuery);
                list<Aircraft_Proposal__c> listDealAnalysis = database.query(strQuery);
                
                for(Aircraft_Proposal__c curDA: listDealAnalysis){
                    RentAndTerm curRentAndTerm = new RentAndTerm();
                    curRentAndTerm.Id = curDA.Marketing_Activity__c;
                    curRentAndTerm.Name = curDA.Marketing_Activity__r.Name;
                    curRentAndTerm.dRent = (LeaseWareUtils.zeroIfNull(curDA.Rent__c) / 1000 ).setScale(0);
                    if(curDA.Term_Mos__c!=null)
                        curRentAndTerm.dTerm = curDA.Term_Mos__c;
                    else
                        curRentAndTerm.dTerm = 0;
                    curRentAndTerm.AircraftTypeVariant = curDA.MSN__c;
                    curRentAndTerm.OperatorName = curDA.Current_Operator__c;
                    listAllTerms.add(curRentAndTerm);
                    if(curRentAndTerm.dTerm <= termPeriod[0]){
	                    list01Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[1]){
	                    list02Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[2]){
	                    list03Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[3]){
	                    list04Terms.add(curRentAndTerm);
                    }
                    else if(curRentAndTerm.dTerm > termPeriod[3]){
                        list05Terms.add(curRentAndTerm);
                    }
                }
            }
            when 3{
                strQuery = 'select id, Name, MSN__c,Term_Mos__c, Rent__c, Current_Rent_Amount__c, Lease_Term__c,Current_Operator__c,  ' +
                    		' Marketing_Activity__c, Marketing_Activity__r.Name '+
                            ' from Aircraft_Proposal__c where Term_Mos__c > 0 and ' + 
                    		+ (recordType == 'Aircraft' ? (' Asset_Type__c = \'' + AircraftType+'-'+variant +'\' ') :
                            + (' Type_Variant__c = \'' + AircraftType+'-'+variant +'\' ')) + 
                    		' and CALENDAR_YEAR(convertTimezone(Date_Of_Manufacture__c)) >= '+ iStartYear + 
                            ' and CALENDAR_YEAR(convertTimezone(Date_Of_Manufacture__c)) <= ' + iEndYear + 
                            + (financialList== null?(''):(' and Marketing_Activity__r.Prospect__r.Financial_Risk_Profile__c in :financialList ')) +
                    		' order by Term_Mos__c';
                system.debug(strQuery);
                system.debug('DataSource 3 recordType: '+recordType + ' strQuery: ' +strQuery);
                list<Aircraft_Proposal__c> listDealAnalysis = database.query(strQuery);
                
                for(Aircraft_Proposal__c curDA: listDealAnalysis){
                    RentAndTerm curRentAndTerm = new RentAndTerm();
                    curRentAndTerm.Id = curDA.Id;
                    curRentAndTerm.Name = curDA.Name;
                    curRentAndTerm.dRent = (LeaseWareUtils.zeroIfNull(curDA.Rent__c) / 1000 ).setScale(0);
                    if(curDA.Term_Mos__c!=null)
                        curRentAndTerm.dTerm = curDA.Term_Mos__c;
                    else
                        curRentAndTerm.dTerm = 0;
                    curRentAndTerm.AircraftTypeVariant = curDA.MSN__c;
                    curRentAndTerm.OperatorName = curDA.Current_Operator__c;
                    
                    listAllTerms.add(curRentAndTerm);
                    if(curRentAndTerm.dTerm <= termPeriod[0]){
	                    list01Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[1]){
	                    list02Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[2]){
	                    list03Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[3]){
	                    list04Terms.add(curRentAndTerm);
                    }
                    else if(curRentAndTerm.dTerm > termPeriod[3]){
                        list05Terms.add(curRentAndTerm);
                    }
                }
            }
            when 4{
                strQuery = 'select id, Name, Base_Rent__c, Lease_End_Date_New__c,Aircraft__c, Lessee__c, Operator__c, Current_Rent__c,Lease_Term__c ,Operator__r.Name, Aircraft__r.Aircraft_Type__c, Aircraft__r.Aircraft_Variant__c ' + 
                            ' from Lease__c where' + 
                            + (recordType == 'Aircraft' ? (' Aircraft__r.Aircraft_Type__c =: AircraftType ') :
                            + (' Aircraft__r.Aircraft_Type__c =:AircraftType')) +
                    		' and '+ (recordType == 'Aircraft' ? (' Aircraft__r.Aircraft_Variant__c =: variant') :
                            + (' Aircraft__r.Aircraft_Variant__c =:variant'))+
                            ' and CALENDAR_YEAR(convertTimezone(Aircraft__r.Date_of_Manufacture__c)) >= '+ iStartYear + 
                            ' and CALENDAR_YEAR(convertTimezone(Aircraft__r.Date_of_Manufacture__c)) <= ' + iEndYear + 
                    		+ (leaseDateRange== 0?(''):(' and Lease_Start_Date_New__c >= '+leaseStartDate.format('yyyy-MM-dd'))) + 
                            + (financialList== null?(''):(' and Operator__r.Financial_Risk_Profile__c in :financialList ')) +
                    		' order by Lease_Term__c';
                system.debug(strQuery);
                system.debug('DataSource 4 recordType: '+recordType + ' strQuery: ' +strQuery);
                list<Lease__c> listLeases = database.query(strQuery);
                List<Lease__c> finalListofLeases = new List<Lease__c>();
                for(Lease__c ls : listLeases){
                    if(ls.Lease_End_Date_New__c >= System.today() && ls.Aircraft__c != null && (ls.Operator__c != null || ls.Lessee__c != null)){
                        finalListofLeases.add(ls);
                    }
                }
                System.debug('List Leases: '+listLeases);
                System.debug('Final List: '+finalListofLeases);
                System.debug('Old List Leases : Final List Leases: '+listLeases.size()+' : '+finalListofLeases.size());
                for(Lease__c curLease: finalListofLeases){
                    RentAndTerm curRentAndTerm = new RentAndTerm();
                    curRentAndTerm.Id = curLease.Id;
                    curRentAndTerm.Name = curLease.Name;
                    curRentAndTerm.dRent = (LeaseWareUtils.zeroIfNull(curLease.Current_Rent__c) / 1000 ).setScale(0);
                    if(curLease.Lease_Term__c!=null)
                        curRentAndTerm.dTerm = curLease.Lease_Term__c;
                    else
                        curRentAndTerm.dTerm = 0;
                    curRentAndTerm.AircraftTypeVariant = curLease.Aircraft__r.Aircraft_Type__c +' '+curLease.Aircraft__r.Aircraft_Variant__c;
                    curRentAndTerm.OperatorName = curLease.Operator__r.Name;
                    listAllTerms.add(curRentAndTerm);
                    if(curRentAndTerm.dTerm <= termPeriod[0]){
	                    list01Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[1]){
	                    list02Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[2]){
	                    list03Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[3]){
	                    list04Terms.add(curRentAndTerm);
                    }
                    else if(curRentAndTerm.dTerm > termPeriod[3]){
                        list05Terms.add(curRentAndTerm);
                    }
                }
            }
            when 5{
                if(recordType == 'Aircraft'){
                strQuery = 'select id, Name, Rent__c, Term_Mos__c, Type__c, Aircraft_Need__r.Intel_Quality__c, '+
                    		' Interaction_Log__r.Intel_Quality__c ,Aircraft_Type__c, Aircraft_Type__r.Aircraft_Type__c, Variant__c ' + 
                            ' from Consolidated_Intel__c ' + 
                            ' where Aircraft_Type__r.Aircraft_Type__c =: AircraftType ' +
                    		' and Aircraft_Type__r.Aircraft_Variant__c =: variant'+
                            ' and Vintage__c >= \''+ iStartYear + '\'' + 
                            ' and Vintage__c <= \'' + iEndYear + '\'' + 
                    		+ (financialList== null?(''):(' and Interaction_Log__r.Related_To_Operator__r.Financial_Risk_Profile__c in :financialList  ')) +
                    		' order by Term_Mos__c';
                }
                else{
                strQuery = 'select id, Name, Rent__c, Term_Mos__c , Type__c, Aircraft_Need__r.Intel_Quality__c, '+
                    		' Interaction_Log__r.Intel_Quality__c ,Engine_Type__c, Engine_Variant__c ' + 
                            ' from Consolidated_Intel__c ' + 
                            ' where Aircraft_Type__r.Engine_Type__c =: AircraftType' +
                    		' and Aircraft_Type__r.Engine_Variant__c =:variant'+
                            ' and Vintage__c >= \''+ iStartYear + '\'' + 
                            ' and Vintage__c <= \'' + iEndYear + '\'' + 
                    		+ (financialList==null?(''):(' and Interaction_Log__r.Related_To_Operator__r.Financial_Risk_Profile__c in :financialList ')) +
                    		' order by Term_Mos__c';
                }
                system.debug(strQuery);
                system.debug('DataSource 5 recordType: '+recordType + ' strQuery: ' +strQuery);
                list<Consolidated_Intel__c> listConsIntel = database.query(strQuery);
                for(Consolidated_Intel__c curIntel: listConsIntel){
                    RentAndTerm curRentAndTerm = new RentAndTerm();
                    curRentAndTerm.Id = curIntel.Id;
                    curRentAndTerm.Name = curIntel.Name;
                    curRentAndTerm.dRent = (LeaseWareUtils.zeroIfNull(curIntel.Rent__c) / 1000 ).setScale(0);
                    if(curIntel.Term_Mos__c!=null)
                        curRentAndTerm.dTerm = curIntel.Term_Mos__c;
                    else
                        curRentAndTerm.dTerm = 0;
                    if(curIntel.Type__c.equalsIgnoreCase('MI')) 
                        curRentAndTerm.OperatorName = curIntel.Aircraft_Need__r.Intel_Quality__c;
                    else if(curIntel.Type__c.equalsIgnoreCase('IL'))
                        curRentAndTerm.OperatorName = curIntel.Interaction_Log__r.Intel_Quality__c;
                    if(recordType.equalsIgnoreCase('Aircraft'))
                        curRentAndTerm.AircraftTypeVariant = curIntel.Aircraft_Type__r.Aircraft_Type__c + ' '+curIntel.Variant__c;
                    else
                        curRentAndTerm.AircraftTypeVariant = curIntel.Engine_Type__c + ' '+curIntel.Engine_Variant__c;
                        
                    listAllTerms.add(curRentAndTerm);
                    if(curRentAndTerm.dTerm <= termPeriod[0]){
	                    list01Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[1]){
	                    list02Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[2]){
	                    list03Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[3]){
	                    list04Terms.add(curRentAndTerm);
                    }
                    else if(curRentAndTerm.dTerm > termPeriod[3]){
                        list05Terms.add(curRentAndTerm);
                    }
                }
            }
            when 6{ //TODO change Lease_Expiry__c to Lease_From__c
                if(recordType == 'Aircraft'){
                strQuery = 'select id, Name, Current_Rent__c, Lease_Expiry__c, '+
                    		' AircraftVariant__c, AircraftType__c,Aircraft_Operator__c, Aircraft_Operator__r.Name from Global_Fleet__c ' +
                            ' where AircraftType__c =: AircraftType ' +
                    		' and AircraftVariant__c =: variant'+
                            ' and BuildYear__c >= \''+ string.valueof(iStartYear) + '\'' + 
                            ' and BuildYear__c <= \'' + string.valueof(iEndYear) + '\'' +
                    		+ (leaseDateRange== 0?(''):(' and Lease_Expiry__c >= '+leaseStartDate.format('yyyy-MM-dd'))) + 		
                            (financialList==null?(''):(' and Aircraft_Operator__r.Financial_Risk_Profile__c in :financialList ')) +
                            ' order by Lease_Expiry__c';
                }
                else{
                strQuery = 'select id, Name, Current_Rent__c, Lease_Expiry__c, '+
                    		' AircraftVariant__c, AircraftType__c, Aircraft_Operator__c, Aircraft_Operator__r.Name from Global_Fleet__c ' +
                            ' where EngineType__c =: AircraftType' +
                    		' and EngineVariant__c =: variant '+
                            ' and BuildYear__c >= \''+ string.valueof(iStartYear) + '\'' + 
                            ' and BuildYear__c <= \'' + string.valueof(iEndYear) + '\'' +
                    		+ (leaseDateRange== 0?(''):(' and Lease_Expiry__c >= '+leaseStartDate.format('yyyy-MM-dd'))) + 		
                            (financialList==null?(''):(' and Aircraft_Operator__r.Financial_Risk_Profile__c in :financialList ')) +
                            ' order by Lease_Expiry__c';
                }    
                system.debug(strQuery);
                system.debug('DataSource 6 recordType: '+recordType + ' strQuery: ' +strQuery);
                list<Global_Fleet__c> worldFleet = database.query(strQuery);
                for(Global_Fleet__c curWF : worldFleet){
                    RentAndTerm curRentAndTerm = new RentAndTerm();
                    curRentAndTerm.Id = curWF.Id;
                    curRentAndTerm.Name = curWF.Name;
                    curRentAndTerm.dRent = (LeaseWareUtils.zeroIfNull(curWF.Current_Rent__c) / 1000 ).setScale(0);
                    if(curWF.Lease_Expiry__c!=null){
                        curRentAndTerm.dTerm = date.today().monthsBetween(curWF.Lease_Expiry__c);
                    }
                    else
                        curRentAndTerm.dTerm = 0;
                    curRentAndTerm.AircraftTypeVariant = curWF.AircraftType__c +' '+curWF.AircraftVariant__c;
                    curRentAndTerm.OperatorName = curWF.Aircraft_Operator__r.Name;
                    curRentAndTerm.OperatorId = curWF.Aircraft_Operator__c;
                    listAllTerms.add(curRentAndTerm);
                    
                    if(curRentAndTerm.dTerm <= termPeriod[0]){
	                    list01Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[1]){
	                    list02Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[2]){
	                    list03Terms.add(curRentAndTerm);
                    }else if(curRentAndTerm.dTerm <= termPeriod[3]){
	                    list04Terms.add(curRentAndTerm);
                    } 
                    else if(curRentAndTerm.dTerm > termPeriod[3]){
                        list05Terms.add(curRentAndTerm);
                    }
                }
            }
            when else{
            }
        }
        DealAnalysisData.Data dataAll = new DealAnalysisData.Data();                
        dataAll.Heading = 'All';
        decimal dTotal=0;
        integer lowIndex=0, highIndex=0, count=0;
        for(RentAndTerm curRT : listAllTerms){
            if(curRT.dRent== null || curRT.dRent==0)continue; //Consider only non-zero, non-null values.
            DealAnalysisData.RecordInfo curRec = new DealAnalysisData.RecordInfo();
            curRec.Id = curRT.Id;
            curRec.Name = curRT.Name;
            curRec.Rent = curRT.dRent;
            curRec.Term = curRT.dTerm;
            curRec.PurchasePrice = curRT.dPurchasePrice;
            curRec.AircraftTypeVariant = curRT.AircraftTypeVariant;
            curRec.OperatorName = curRT.OperatorName;
            curRec.OperatorId = curRT.OperatorId;
            dataAll.Recs.add(curRec);
            if(dataAll.Lowest == null || dataAll.Lowest > curRT.dRent){
            	dataAll.Lowest = curRT.dRent;
                lowIndex = count;
            }
            if(dataAll.Highest == null || dataAll.Highest < curRT.dRent){
                dataAll.Highest = curRT.dRent;
                highIndex = count;
            }
            dTotal += curRT.dRent;
            count++;
        }
        if(count>0){
            dataAll.Recs[lowIndex].IsLowest=true;
            dataAll.Recs[highIndex].IsHighest=true;
            dataAll.Mean = (dTotal/count).setScale(0);
        }
        listDAData.add(dataAll);
        
        DealAnalysisData.Data data01 = new DealAnalysisData.Data();                
        data01.Heading = '0 - '+termPeriod[0];
        dTotal=0;
        count=0;
        for(RentAndTerm curRT : list01Terms){
            if(curRT.dRent== null || curRT.dRent==0)continue; //Consider only non-zero, non-null values.
            DealAnalysisData.RecordInfo curRec = new DealAnalysisData.RecordInfo();
            curRec.Id = curRT.Id;
            curRec.Name = curRT.Name;
            curRec.Rent = curRT.dRent;
            curRec.Term = curRT.dTerm;
            curRec.PurchasePrice = curRT.dPurchasePrice;
            curRec.AircraftTypeVariant = curRT.AircraftTypeVariant;
            curRec.OperatorName = curRT.OperatorName;
            curRec.OperatorId = curRT.OperatorId;
            
            data01.Recs.add(curRec);
            if(data01.Lowest == null || data01.Lowest > curRT.dRent){
                lowIndex = count;
                data01.Lowest = curRT.dRent;
            }
            if(data01.Highest == null || data01.Highest < curRT.dRent){
                highIndex = count;
                data01.Highest = curRT.dRent;
            }
            dTotal += curRT.dRent;
            count++;
        }
        if(count>0){
            data01.Recs[lowIndex].IsLowest=true;
            data01.Recs[highIndex].IsHighest=true;
            data01.Mean = (dTotal/count).setScale(0);
        }
        System.debug('data01 :'+data01);
        listDAData.add(data01);
        
        DealAnalysisData.Data data02 = new DealAnalysisData.Data();                
        data02.Heading = termPeriod[0]+' - '+termPeriod[1];
        dTotal=0;
        count=0;
        for(RentAndTerm curRT : list02Terms){
            if(curRT.dRent== null || curRT.dRent==0)continue; //Consider only non-zero, non-null values.
            DealAnalysisData.RecordInfo curRec = new DealAnalysisData.RecordInfo();
            curRec.Id = curRT.Id;
            curRec.Name = curRT.Name;
            curRec.Rent = curRT.dRent;
            curRec.Term = curRT.dTerm;
            curRec.PurchasePrice = curRT.dPurchasePrice;
            curRec.AircraftTypeVariant = curRT.AircraftTypeVariant;
            curRec.OperatorName = curRT.OperatorName;
            curRec.OperatorId = curRT.OperatorId;
            
            data02.Recs.add(curRec);
            if(data02.Lowest == null || data02.Lowest > curRT.dRent){
                lowIndex = count;
                data02.Lowest = curRT.dRent;
            }
            if(data02.Highest == null || data02.Highest < curRT.dRent){
                highIndex = count;
                data02.Highest = curRT.dRent;
            }
            dTotal += curRT.dRent;
            count++;
        }
        if(count>0){
            data02.Recs[lowIndex].IsLowest=true;
            data02.Recs[highIndex].IsHighest=true;
            data02.Mean = (dTotal/count).setScale(0);
        }
        System.debug('data02 :'+data02);
        listDAData.add(data02);
        
        DealAnalysisData.Data data03 = new DealAnalysisData.Data();                
        data03.Heading = termPeriod[1]+' - '+termPeriod[2];
        dTotal=0;
        count=0;
        for(RentAndTerm curRT : list03Terms){
            if(curRT.dRent== null || curRT.dRent==0)continue; //Consider only non-zero, non-null values.
            DealAnalysisData.RecordInfo curRec = new DealAnalysisData.RecordInfo();
            curRec.Id = curRT.Id;
            curRec.Name = curRT.Name;
            curRec.Rent = curRT.dRent;
            curRec.Term = curRT.dTerm;
            curRec.PurchasePrice = curRT.dPurchasePrice;
            curRec.AircraftTypeVariant = curRT.AircraftTypeVariant;
            curRec.OperatorName = curRT.OperatorName;
            curRec.OperatorId = curRT.OperatorId;
            
            data03.Recs.add(curRec);
            if(data03.Lowest == null || data03.Lowest > curRT.dRent){
                lowIndex = count;
                data03.Lowest = curRT.dRent;
            }
            if(data03.Highest == null || data03.Highest < curRT.dRent){
                highIndex = count;
                data03.Highest = curRT.dRent;
            }
            dTotal += curRT.dRent;
            count++;
        }
        if(count>0){
            data03.Recs[lowIndex].IsLowest=true;
            data03.Recs[highIndex].IsHighest=true;
            data03.Mean = (dTotal/count).setScale(0);
        }
        System.debug('data03 :'+data03);
        listDAData.add(data03);
        
        DealAnalysisData.Data data04 = new DealAnalysisData.Data();                
        data04.Heading = termPeriod[2]+' - '+termPeriod[3];
        dTotal=0;
        count=0;
        for(RentAndTerm curRT : list04Terms){
            if(curRT.dRent== null || curRT.dRent==0)continue; //Consider only non-zero, non-null values.
            DealAnalysisData.RecordInfo curRec = new DealAnalysisData.RecordInfo();
            curRec.Id = curRT.Id;
            curRec.Name = curRT.Name;
            curRec.Rent = curRT.dRent;
            curRec.Term = curRT.dTerm;
            curRec.PurchasePrice = curRT.dPurchasePrice;
            curRec.AircraftTypeVariant = curRT.AircraftTypeVariant;
            curRec.OperatorName = curRT.OperatorName;
            curRec.OperatorId = curRT.OperatorId;
            
            data04.Recs.add(curRec);
            if(data04.Lowest == null || data04.Lowest > curRT.dRent){
                lowIndex = count;
                data04.Lowest = curRT.dRent;
            }
            if(data04.Highest == null || data04.Highest < curRT.dRent){
                highIndex = count;
                data04.Highest = curRT.dRent;
            }
            dTotal += curRT.dRent;
            count++;
        }
        if(count>0){
            data04.Recs[lowIndex].IsLowest=true;
            data04.Recs[highIndex].IsHighest=true;
            data04.Mean = (dTotal/count).setScale(0);
        }
        System.debug('data04 :'+data04);
        listDAData.add(data04);
        
        DealAnalysisData.Data data05 = new DealAnalysisData.Data();                
        data05.Heading = '> '+termPeriod[3];
        dTotal=0;
        count=0;
        for(RentAndTerm curRT : list05Terms){
            if(curRT.dRent== null || curRT.dRent==0)continue; //Consider only non-zero, non-null values.
            DealAnalysisData.RecordInfo curRec = new DealAnalysisData.RecordInfo();
            curRec.Id = curRT.Id;
            curRec.Name = curRT.Name;
            curRec.Rent = curRT.dRent;
            curRec.Term = curRT.dTerm;
            curRec.PurchasePrice = curRT.dPurchasePrice;
            curRec.AircraftTypeVariant = curRT.AircraftTypeVariant;
            curRec.OperatorName = curRT.OperatorName;
            curRec.OperatorId = curRT.OperatorId;
            
            data05.Recs.add(curRec);
            if(data05.Lowest == null || data05.Lowest > curRT.dRent){
                lowIndex = count;
                data05.Lowest = curRT.dRent;
            }
            if(data05.Highest == null || data05.Highest < curRT.dRent){
                highIndex = count;
                data05.Highest = curRT.dRent;
            }
            dTotal += curRT.dRent;
            count++;
        }
        if(count>0){
            data05.Recs[lowIndex].IsLowest=true;
            data05.Recs[highIndex].IsHighest=true;
            data05.Mean = (dTotal/count).setScale(0);
        }
        System.debug('data01 :'+data05);
        listDAData.add(data05);
        
        system.debug('AIUtils - Data to be returned :' +listDAData);
        
        return listDAData;
    }   
}