/* Dont ship this class file
  This is for creating records for testcase. 
  
Method to call
TestDonotAddAsManagedPackage.deleteTestRecord_new();
TestDonotAddAsManagedPackage.createSetup_new();
TestDonotAddAsManagedPackage.createACOperLease_new();
TestDonotAddAsManagedPackage.createCA_new();  


***********
Add following in testcase to get Aircraft and Lease
@isTest(seeAllData=true)

        string ACName = 'TestSeededMSN1';
        Aircraft__c AC1 = [select id,lease__c from Aircraft__c where MSN_Number__c = :ACName limit 1];
        mapInOut.put('Aircraft__c.Id',AC1.Id);
        mapInOut.put('Aircraft__c.lease__c',AC1.lease__c);
  
  */
@isTest  
public class TestDonotAddAsManagedPackage{
    static string testNname = 'TestSeeded%';
    static string ACName1 = 'TestSeededMSN1';
    static string ACName2 = 'TestSeededMSN2';
    static string ACName3 = 'TestSeededMSN3';

   public static void deleteTestRecord_new(){
        LeaseWareUtils.TriggerDisabledFlag=true;
        
        List<Assembly_MR_Rate__c> SuppRent = New List<Assembly_MR_Rate__c>();
        SuppRent=[select id,name from Assembly_MR_Rate__c where lease__r.Aircraft__r.MSN_Number__c like :testNname];
        if(SuppRent.size()>0) Delete SuppRent;  
       
        list<lease__c> listLease = [select id,Aircraft__c from lease__c where Aircraft__r.MSN_Number__c like :testNname];
        for(lease__c curLease:  listLease){
            curLease.Aircraft__c =null;
        }
        update listLease;

        list<Aircraft__c> listAC = [select id from Aircraft__c where MSN_Number__c like :testNname];
        for(Aircraft__c curAC:  listAC){
            curAC.Lease__c =null;
            curAC.Status__c ='Available';
        }        
        update listAC;
        
        Operator__c[] listOper = [select id from Operator__c where name like :testNname];
        
        delete [select id from Invoice__c where lease__c in :listLease];
        delete [select id from Utilization_report__c where Aircraft__c in :listAC];
        delete [select id from Aircraft_Proposal__c where Aircraft__c in :listAC];
        delete [select id from Marketing_Activity__c where Prospect__c in :listOper];        
        
        delete listLease;
        
        delete [select id from Constituent_Assembly__c where Serial_Number__c like :testNname];
        
        delete [select id from Aircraft_Match__c where Aircraft__r.MSN_Number__c like :testNname];

        delete listAC;

        delete listOper;

        delete [select id from Lessor__c where name like :testNname];


        list<Maintenance_Program__c> ListMP =[select id,name from Maintenance_Program__c where name like :testNname];
        for(Maintenance_Program__c curMP:  ListMP){
            curMP.Name = 'MP-' + system.today();
        }
        update ListMP;        
        LeaseWareUtils.TriggerDisabledFlag=false;
        
    }
    
    // Utility Method to send the Email.
    
    public static void emailObjectDetails(){
    
    list<string> listFields = new list<string>();
    Map<String, Schema.SObjectType> mapAllSObjects = Schema.getGlobalDescribe();
           
        for(Schema.SObjectType curSObj : mapAllSObjects.values()){
            Schema.DescribeSObjectResult DescObjRes = curSObj.getDescribe();
            if(DescObjRes.getLabel().containsIgnoreCase('Z_Unused'))continue;
            string strList = '';
            for(SObjectField curSOField: DescObjRes.fields.getMap().values()){
               Schema.DescribeFieldResult curField = curSOField.getDescribe();
               string curFieldLabel = curField.getLabel();
               
               if(curField.getName().containsIgnoreCase('__c')  && !curField.getLabel().containsIgnoreCase('Z_Unused') ){
                   strList+=(DescObjRes.getName() + ','+DescObjRes.getLabel()+',' +curField.getName() + ','+ curField.getLabel()+'\n');
               }
            }
            listFields.add(strList);
        }
         
           
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses( new String[] { UserInfo.getUserEmail() } );
        mail.setSubject( 'Leaseworks Object Details' + ' on Dated '+ system.today().format() );
        mail.setHtmlBody( 'Hi LW, Please find the attached csv file.' );
        String contentCSV = 'Object API name, Object Label Name, Field API Name, Field Label Name'+'\n';
    
        for(string strList : listFields){
            if(strList.length()>0)
             contentCSV += strList;
        }
        
        String content = contentCSV;
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setFileName( 'LWObjectDetails'+ system.today().format()+ '.csv' );
        attachment.setBody( Blob.valueOf( content ) );
        mail.setFileAttachments( new Messaging.EmailFileAttachment[]{ attachment } );

        LeasewareUtils.setFromTrigger('DO_NOT_CREATE_IL');
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
        LeasewareUtils.unsetTriggers('DO_NOT_CREATE_IL');
        
    
 }
 
 

    public static void createSetup_new(){
        map<String,Object> mapInOut = new map<String,Object>();
        string setupName = 'TestSeededLW';
        Lessor__c LWSetUp;
        try{
            LWSetUp = [select id from Lessor__c ];
        }catch(Exception ex){
            if(LWSetUp==null){
                LWSetUp = new Lessor__c(Name=setupName, Lease_Logo__c='http://abcd.lease-works.com/a.gif', 
                    Phone_Number__c='2342', Email_Address__c='a@lease.com', Auto_Create_Campaigns__c=true, Number_Of_Months_For_Narrow_Body__c=24,
                    Number_Of_Months_For_Wide_Body__c=24, Narrowbody_Checked_Until__c=date.today(), Widebody_Checked_Until__c=date.today());
                    LeaseWareUtils.clearFromTrigger();insert  LWSetUp;
            } 
        }
        
        Custom_Lookup__c[] listCustomLookup = [select id from Custom_Lookup__c where name in ('5A1','5A3','5A4') ];
        if(listCustomLookup.size()<3){
            system.debug('All records are not there');
            createTSLookup(mapInOut);
        }
    
        LeaseWareSeededData.SeedLWSetup();
        LeaseWareSeededData.SeedCustomLookup();
        LeaseWareSeededData.SeedLWServiceSetup();        

    }

    public static void createACOperLease_new(){
        
    // 1. Create Maintenance Program and Maintenance Events
        
        ID mpID = createMaintenanceProgramAndEvents();

    // 2. Create Aircraft
        Date tempDate = system.today(); 
        string NewUsed='New';
        string ACName = ACName1;
        string AircraftType ='737';
        Aircraft__c AC1;
        string Variant = '200';
        try{
            AC1 = [select id from Aircraft__c where MSN_Number__c = :ACName limit 1];
        }catch(Exception ex){
            AC1 = new Aircraft__c(MSN_Number__c=ACName, TSN__c=200, CSN__c=200, Threshold_for_C_Check__c=1000
                                                ,New_Used__c = NewUsed,Aircraft_Type__c = AircraftType, Aircraft_Variant__c = Variant
                                                , Status__c='Available',Date_of_Manufacture__c=tempDate.toStartOfMonth());
                                                        
            LeaseWareUtils.clearFromTrigger();insert AC1;
        }
        ACName = ACName2;
        Aircraft__c AC2;
        try{
            AC2 = [select id from Aircraft__c where MSN_Number__c = :ACName limit 1];
        }catch(Exception ex){
            AC2 = new Aircraft__c(MSN_Number__c=ACName, TSN__c=200, CSN__c=200, Threshold_for_C_Check__c=1000
                                                ,New_Used__c = NewUsed,Aircraft_Type__c = AircraftType, Aircraft_Variant__c = Variant
                                                , Status__c='Available',Date_of_Manufacture__c=tempDate.toStartOfMonth());
                                                        
            LeaseWareUtils.clearFromTrigger();insert AC2;
        }
        
        ACName = ACName3;
        Aircraft__c AC3;
        try{
         AC3 = [select id from Aircraft__c where MSN_Number__c = :ACName limit 1];
        }catch(Exception ex){
            AC3 = new Aircraft__c(MSN_Number__c=ACName, TSN__c=200, CSN__c=200, Last_TSN_Utilization__c=200, Last_CSN_Utilization__c =200,Threshold_for_C_Check__c=1000
                                                ,New_Used__c = NewUsed,Aircraft_Type__c = AircraftType, Aircraft_Variant__c = Variant
                                                , Status__c='Available',Date_of_Manufacture__c=tempDate.toStartOfMonth());
            //Associate maintenance program details to aircraft
            AC3.Maintenance_Program__c = mpID;                                         
            LeaseWareUtils.clearFromTrigger();insert AC3;
        }   

        Country__c country;
        try{
        	country = [select Id,Name from Country__c limit 1];
        }catch(QueryException e){
             country =new Country__c(Name='Test Country');
        	insert country;
        }
        
    // 3. Create Operator
            String Opername   = 'TestSeededOPER'; string alias='';
            String status = 'Approved';
            String region = 'Asia';
            String street  = '9115 Hague Rd';
            String city    = 'Indianapolis';
            String zipcode = '46256';
         // String country = 'United States';       
            decimal lat  = null;
            decimal lng = null; 
        Opername   = 'TestSeededOPER1';  
        Operator__c Oper1;
        try{
            Oper1 = [select id from Operator__c where name = :Opername  limit 1 ];
        }catch(Exception ex){
                      
            Oper1 = new Operator__c( Name = Opername , Status__c = status , 
                                                    Region__c = region ,
                                                    Revenue__c = 100.00 ,
                                                    Employees__c = 100 ,                                                
                                                    Address_Street__c = street ,
                                                    City__c = city ,
                                                    Zip_Code__c = zipcode ,
                                                   // Country__c = country,
                                                    Country_Lookup__c = country.id,
                                                    Location__Latitude__s = lat,
                                                    Location__Longitude__s = lng,Alias__c = alias);
            LeaseWareUtils.clearFromTrigger();insert Oper1;                                             
        
        }
        
        Opername   = 'TestSeededOPER2';  
        Operator__c Oper2;
        try{
            Oper2 = [select id from Operator__c where name = :Opername  limit 1 ];
        }catch(Exception ex){
          
            Oper2 = new Operator__c( Name = Opername , Status__c = status , 
                                                    Region__c = region ,
                                                    Revenue__c = 100.00 ,
                                                    Employees__c = 100 ,                                                
                                                    Address_Street__c = street ,
                                                    City__c = city ,
                                                    Zip_Code__c = zipcode ,
                                                 // Country__c = country,
                                                    Country_Lookup__c = country.id,
                                                    Location__Latitude__s = lat,
                                                    Location__Longitude__s = lng,Alias__c = alias);
            LeaseWareUtils.clearFromTrigger();insert Oper2;         
        }
        
        Opername   = 'TestSeededOPER3';  
        Operator__c Oper3;
        try{    
            Oper3 = [select id from Operator__c where name = :Opername  limit 1];
        }catch(Exception ex){
              
            Oper3 = new Operator__c( Name = Opername , Status__c = status , 
                                                    Region__c = region ,
                                                    Revenue__c = 100.00 ,
                                                    Employees__c = 100 ,                                                
                                                    Address_Street__c = street ,
                                                    City__c = city ,
                                                    Zip_Code__c = zipcode ,
                                                 // Country__c = country,
                                                    Country_Lookup__c = country.id,
                                                    Location__Latitude__s = lat,
                                                    Location__Longitude__s = lng,Alias__c = alias);
            LeaseWareUtils.clearFromTrigger();insert Oper3;     
        }   
                
    // 4. Create Lease  
        Date TodatDate= system.today();
        //Counterparty__c LessorId;
        ID LessorId = null;
        string LeaseName = 'TestSeededLEASE1';  
        
        Lease__c Lease1 ;
        try{
            Lease1 = [select id from Lease__c where name = :LeaseName  limit 1];
        }catch(Exception ex){
            Lease1 = new Lease__c(Aircraft__c=AC1.Id, Name=LeaseName, Rent_Type__c='Power By The Hour',
                        Operator__c=Oper1.Id, UR_Due_Day__c='10',Lease_Start_Date_New__c=TodatDate.addMonths(-2),Lessor__c = LessorId,Lessee__c = Oper1.Id,
                        Lease_End_Date_New__c=TodatDate.addYears(2), Rent_Date__c=TodatDate, Rent_End_Date__c=TodatDate.addYears(2));
            LeaseWareUtils.clearFromTrigger();insert Lease1;        
        }
        
        LeaseName = 'TestSeededLEASE2';  
        Lease__c Lease2 ;
        try{    
            Lease2 = [select id from Lease__c where name = :LeaseName limit 1];
        }catch(Exception ex){
            Lease2 = new Lease__c(Aircraft__c=AC2.Id, Name=LeaseName, Rent_Type__c='Power By The Hour', 
                        Operator__c=Oper2.Id, UR_Due_Day__c='10',Lease_Start_Date_New__c=TodatDate.addMonths(-2),Lessor__c = LessorId,Lessee__c = Oper2.Id,
                        Lease_End_Date_New__c=TodatDate.addYears(2), Rent_Date__c=TodatDate, Rent_End_Date__c=TodatDate.addYears(2));
            LeaseWareUtils.clearFromTrigger();insert Lease2;
        }
        
        LeaseName = 'TestSeededLEASE3';  
        Lease__c Lease3;
        try{    
            Lease3 = [select id from Lease__c where name = :LeaseName limit 1];
        }catch(Exception ex){
            Lease3 = new Lease__c(Aircraft__c=AC3.Id, Name=LeaseName, Rent_Type__c='Power By The Hour', Threshold_Claim_Amount__c = 50000,
                        Operator__c=Oper3.Id, UR_Due_Day__c='10',Lease_Start_Date_New__c=TodatDate.addMonths(-2),Lessor__c = LessorId,Lessee__c = Oper3.Id,
                        Lease_End_Date_New__c=TodatDate.addYears(2), Rent_Date__c=TodatDate, Rent_End_Date__c=TodatDate.addYears(2));
            LeaseWareUtils.clearFromTrigger();insert Lease3;
        }       
        


        
    }
     
    private static void removeAccountingSetup(set<id> ids){
        list<lease__c> leaseList = [select id,Accounting_Setup_P__c,name from lease__c where id IN :ids];
        for(lease__c lease : leaseList){
            if(lease.Accounting_Setup_P__c !=null){ 
                lease.Accounting_Setup_P__c = null;
            }
        }
        LWGlobalUtils.setTriggersAndWFsOff();
        update leaseList;
        LWGlobalUtils.setTriggersAndWFsOn();
    }

    public static void createCA_new(){

        map<String,Object> mapEngineRecTypes = new map<String,Object>();
        setRecordTypeOfEngine(mapEngineRecTypes);
        ID recTypeId=(ID)(mapEngineRecTypes.get('Engine'));

        list<Custom_Lookup__c> listCustomLookup = [select id,name,Sub_LookupType__c from Custom_Lookup__c] ;
        for(Custom_Lookup__c curCL  :listCustomLookup){
            mapEngineRecTypes.put('Custom_Lookup_Name.'+ curCL.Name + curCL.Sub_LookupType__c,curCL.Id);
        }

        
                
        list<Aircraft__c> listAC = [select id,MSN_Number__c from Aircraft__c where MSN_Number__c like 'TestSeeded%'];   
		// 2.1 Add Assembly
        list<Constituent_Assembly__c> listCA = new list<Constituent_Assembly__c>();
        string AssemblyName;
        string EngineModel='CFM56';
        string CurrentTS = '5A1';string Current_TS_Id = (Id)mapEngineRecTypes.get('Custom_Lookup_Name.'+CurrentTS + EngineModel);
        
        string ManufacturerList = null;
        string CAtype;
        for(integer i=0;i<listAC.size();i++){
            AssemblyName = listAC[i].MSN_Number__c + '-E';

            CAtype = 'Airframe';
            recTypeId=(ID)(mapEngineRecTypes.get('Airframe'));
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
                , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
                TSN__c=0, CSN__c=0, Last_TSN_Utilization__c=0, Last_CSN_Utilization__c =0,Date_of_Manufacture__c=system.today()
                , Type__c = CAtype, Serial_Number__c=AssemblyName,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
            
            CAtype = 'Engine 1';
            recTypeId=(ID)(mapEngineRecTypes.get('Engine'));
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
                , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
                TSN__c=0, CSN__c=0, Last_TSN_Utilization__c=0, Last_CSN_Utilization__c =0,Date_of_Manufacture__c=system.today()
                ,  Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
                ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
                    
            CAtype = 'Engine 2';
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
                , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
                TSN__c=0, CSN__c=0, Last_TSN_Utilization__c=0, Last_CSN_Utilization__c =0,Date_of_Manufacture__c=system.today()
                , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
                ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));

                
            CAtype = 'APU';
            recTypeId=(ID)(mapEngineRecTypes.get('APU'));
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
                , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
                TSN__c=0, CSN__c=0, Last_TSN_Utilization__c=0, Last_CSN_Utilization__c =0,Date_of_Manufacture__c=system.today()
                , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
                ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
                
            CAtype = 'Landing Gear - Left Main';
            recTypeId=(ID)(mapEngineRecTypes.get('Landing_Gear'));
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
                , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
                TSN__c=0, CSN__c=0, Last_TSN_Utilization__c=0, Last_CSN_Utilization__c =0,Date_of_Manufacture__c=system.today()
                , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
                ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));
                
            CAtype = 'Landing Gear - Right Main';
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
                , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
                TSN__c=0, CSN__c=0, Last_TSN_Utilization__c=0, Last_CSN_Utilization__c =0,Date_of_Manufacture__c=system.today()
                , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
                ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));  
                
            CAtype = 'Landing Gear - Nose';
            listCA.add(new Constituent_Assembly__c(
                Name=AssemblyName, Aircraft_Type__c='737-300', Aircraft_Engine__c='737-300 @ CFM56'
                , Engine_Model__c =EngineModel, Attached_Aircraft__c= listAC[i].ID, Description__c ='Test', 
                TSN__c=0, CSN__c=0, Last_TSN_Utilization__c=0, Last_CSN_Utilization__c =0,Date_of_Manufacture__c=system.today()
                , Type__c = CAtype, Serial_Number__c=AssemblyName, Engine_Thrust__c='-3B1', Current_Thrust_Setting__c=CurrentTS
                ,Current_TS__c =  Current_TS_Id,Manufacturer_List__c = ManufacturerList, RecordTypeId=recTypeId));                                                          
                                                                        
        }
        LeaseWareUtils.clearFromTrigger();
        LWGlobalUtils.setTriggersAndWFsOff();
        insert  listCA; 
		LWGlobalUtils.setTriggersAndWFsOn();          
        list<Sub_Components_LLPs__c> insLLPs = new list<Sub_Components_LLPs__c>();
        for(Constituent_Assembly__c curCA   :listCA){
            if(curCA.Type__c.contains('Engine')){
                insLLPs.add(new Sub_Components_LLPs__c(Name='LLPTest', Serial_Number__c='1234'
                        , Constituent_Assembly__c=curCA.Id,part_number__c='P100'
                        , Life_Limit_Cycles__c=1000, TSN__c=0, CSN__c=0, TSLV__c=0, CSLV__c= 0));
            }
        }

        LeaseWareUtils.clearFromTrigger();insert  insLLPs; 

        Id mpID = [select id from Maintenance_Program__c where Name='TestSeededMPN' limit 1].Id;
        //create Assembly MR Rate
        set<id> setLeaseId = new  set<id>();
        set<id> allLeases = new set<id>();
        for(Lease__c rec: [select id,aircraft__r.MSN_Number__c from lease__c where aircraft__r.MSN_Number__c like :testNname]){
            if(rec.aircraft__r.MSN_Number__c==ACName3) setLeaseId.add(rec.Id);
            allLeases.add(rec.Id);
        }


        createProjectedEvnt(mpID);
        createLeaseAssemblyRates(setLeaseId,mpID);
        removeAccountingSetup(allLeases);       
        
    } 
    
    //Create Maintenance Program and Events
    private static ID createMaintenanceProgramAndEvents(){
        //Create Maintenance Program
        Maintenance_Program__c newMP = new Maintenance_Program__c(Name='TestSeededMPN'
            ,Aircraft_Type__c = '737-300', Historical_Utilization_Months__c = 10, Variant__c = null,Current_Catalog_Year__c ='2015');
        
        LeaseWareUtils.clearFromTrigger();
        insert  newMP;      
        
        //Create Maintenance events for the maintenance program.
        
        list<Maintenance_Program_Event__c> insListMPE = new list<Maintenance_Program_Event__c>();
        string AssemblyVar = null,eventTypeVar;
        Decimal int1Hours,int1Cycle,int1Period,int2Hours,int2Cycle,int2Period,eventCost;
        
        list<String> listAssemblyVar =  new list<string>{'Airframe','APU', 'Engine', 
                 'Landing Gear - Nose', 'Landing Gear - Main','Propeller'};     
        int1Hours = 2000;int1Cycle =1000;int1Period =30;eventCost=20000;
        for (String strAssemVar :listAssemblyVar ) {
            
            if (strAssemVar.equals('Airframe')) { 
                eventTypeVar = 'Heavy 1'; 
            }else if (strAssemVar.contains('Landing') ) {
                eventTypeVar = 'Overhaul';
            }else if (strAssemVar.equals('Engine') || strAssemVar.equals('APU')) {
                eventTypeVar = 'Performance Restoration';
            }
            
        	insListMPE.add(new Maintenance_Program_Event__c(
	            Assembly__c = strAssemVar, Clearance_Period_Critreia__c = null, Event_Cost__c = eventCost
	            , Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
	            ,Maintenance_Program__c = newMP.id, Event_Type_Global__c=eventTypeVar));

			if (strAssemVar.equals('Engine') ) {
                eventTypeVar = 'LLP Replacement';
	        	insListMPE.add(new Maintenance_Program_Event__c(
		            Assembly__c = strAssemVar, Clearance_Period_Critreia__c = null, Event_Cost__c = eventCost
		            , Never_Exceed_Period_Cycles__c = int1Cycle, Never_Exceed_Period_Hours__c = int1Hours, Never_Exceed_Period_Months__c = int1Period
		            ,Maintenance_Program__c = newMP.id, Event_Type_Global__c=eventTypeVar));                   
            }
            
 
            
        }

        LeaseWareUtils.clearFromTrigger();
        insert  insListMPE; 
                
        return newMP.Id;
        
    }
	
	private static void createProjectedEvnt(id newMPId){
	
		// Set up Maintenance Program Event
		map<string,id> mapMPE= new map<string,id>();
		list<Maintenance_Program_Event__c>  ListMPE= [select id,Assembly__c,Event_Type_Global__c from Maintenance_Program_Event__c where Maintenance_Program__c =: newMPId];
		for(Maintenance_Program_Event__c curMPE: ListMPE){
			mapMPE.put( curMPE.Assembly__c+'-'+curMPE.Event_Type_Global__c,curMPE.id );
		}
		system.debug('mapMPE='+ JSON.serializePretty(mapMPE));
		// Set up Project Event
		list<Assembly_Event_Info__c> insListProjEvnt = new list<Assembly_Event_Info__c>();
		list<Aircraft__c> listAC = [select id,MSN_Number__c from Aircraft__c where MSN_Number__c like 'TestSeeded%'];
        system.debug('=listAC='+listAC);
        list<Constituent_Assembly__c> listCA =[select name,id,Type__c from Constituent_Assembly__c where Attached_Aircraft__c=: listAC and Name Like '%TestSeededMSN3%'];
		system.debug('=listCA='+ JSON.serializePretty(listCA));

        string eventTypeVar,AssmblyType;
        for (Constituent_Assembly__c curCA :listCA ) {
            if(curCA.Type__c.contains('Engine')) {
                AssmblyType='Engine';
            }else if(curCA.Type__c.contains('Main')) {
                AssmblyType='Landing Gear - Main';
            } else AssmblyType=curCA.Type__c;
                
			if (curCA.Type__c.equals('Airframe')) { 
                eventTypeVar = 'Heavy 1'; 
            }else if (curCA.Type__c.contains('Landing') ) {
                eventTypeVar = 'Overhaul';
            }else if (curCA.Type__c.contains('Engine') || curCA.Type__c.equals('APU')) {
                eventTypeVar = 'Performance Restoration';
            }
            
            insListProjEvnt.add(new Assembly_Event_Info__c(
			Name = curCA.Type__c+'-'+eventTypeVar,
			Event_Cost__c = 20,
			Constituent_Assembly__c = curCA.Id,
			Maintenance_Program_Event__c = mapMPE.get(AssmblyType+'-'+ eventTypeVar)));
			if (curCA.Type__c.contains('Engine') ) {
                eventTypeVar = 'LLP Replacement';
                insListProjEvnt.add(new Assembly_Event_Info__c(
                    Name = curCA.Type__c+'-'+eventTypeVar,
                    Event_Cost__c = 20,
                    Constituent_Assembly__c = curCA.Id,
                    Maintenance_Program_Event__c = mapMPE.get('Engine'+'-'+ eventTypeVar)));
			}
		}
        system.debug('insListProjEvnt='+JSON.serializePretty(insListProjEvnt));
		if(insListProjEvnt.size()>0) insert insListProjEvnt;	
	}
	   
    private static void createLeaseAssemblyRates(set<id> setLeaseId,id mpID){
        system.debug('createLeaseAssemblyRates(+)- Lease ID'+ setLeaseId);
        system.debug('mpID='+mpID);
        list<Assembly_MR_Rate__c> MRRates = new list<Assembly_MR_Rate__c>();

        list<Maintenance_Program_Event__c> listMPEIds= [select id from Maintenance_Program_Event__c where Maintenance_Program__c=:mpID];
        system.debug('listMPEIds size='+listMPEIds.size());
        system.debug('=listMPEIds='+ JSON.serializePretty(listMPEIds));
		List<Assembly_Event_Info__c> listAssemblyVar= [Select Id, Constituent_Assembly__c,Assembly_Type__c,Event_Type__c from Assembly_Event_Info__c where Maintenance_Program_Event__c in :listMPEIds and (Constituent_Assembly__r.Name Like '%TestSeededMSN3%')];
		system.debug('listAssemblyVar size='+listAssemblyVar.size());
        system.debug('listAssemblyVar='+ JSON.serializePretty(listAssemblyVar));
        string rateBasis = 'Hours';
        for(Id leaseId:setLeaseId){        
            for(Assembly_Event_Info__c strEvent : listAssemblyVar){
                String eventTypeVar;
                if (strEvent.Assembly_Type__c.equals('Airframe')) { 
                    rateBasis = 'Monthly';
                }else if (strEvent.Assembly_Type__c.contains('Landing') ) {
                    rateBasis = 'Monthly';
                }else if (strEvent.Assembly_Type__c.contains('Engine') || strEvent.Assembly_Type__c.contains('APU')) {
                    rateBasis  = 'Hours';
                } 
                if(!strEvent.Event_Type__c.contains('LLP')){
                    MRRates.add(new Assembly_MR_Rate__c(Name=strEvent.Assembly_Type__c+'-'+strEvent.Event_Type__c , Assembly_Lkp__c = strEvent.Constituent_Assembly__c , Assembly_Event_Info__c = strEvent.id,
                        Base_MRR__c = 100, Lease__c=leaseId,Monthly_Threshold__c = 1, Monthly_Threshold_Amount__c=100,
                        Rate_Basis__c = rateBasis,Lessor_Contribution__c=100000,LC_Depletes_With_First_Event__c=false));
                }
                if(strEvent.Event_Type__c.contains('LLP')){
                    eventTypeVar = 'LLP Replacement';
                    rateBasis = 'Cycles';   
	                MRRates.add(new Assembly_MR_Rate__c(Name=strEvent.Assembly_Type__c+'-'+strEvent.Event_Type__c  , Assembly_Lkp__c = strEvent.Constituent_Assembly__c , Assembly_Event_Info__c = strEvent.id,
	                        Base_MRR__c = 100, Lease__c=leaseId,Monthly_Threshold__c = 1, Monthly_Threshold_Amount__c=100,
	                        Rate_Basis__c = rateBasis,Lessor_Contribution__c=100000,LC_Depletes_With_First_Event__c=false));                             
                } 

            }
        }
        insert MRRates;     
        system.debug('createLeaseAssemblyRates(-)');
        
    }
    
    
    
    public static void createTSLookup(map<String,Object> mapInOut){
        
        LeaseWareUtils.clearFromTrigger();      
        String [] SublookupType = new List<String> {'CFM56','CFM56','CFM56','V2500','V2500',null
                                        ,'CFM56','CFM56','CFM56','CFM56'
                                        ,'CFM56','CFM56','CFM56'
                                        ,'V2500','V2500'
                                         ,'CF6','CF6'
                                        ,'OTHER'
                                        ,'JT8','JT8'
                                        ,null , null , null ,null ,null};
        String [] lookupcode = new List<String> {'A','B','C','V2524-A5','V2527-A5','Default'
                                        ,'5A1','5A3','5A4','5A5'
                                        ,'3B-1','3B-2','3C-1'
                                        ,'2950','2650'
                                        ,'M1','M2'
                                        ,'None'
                                        ,'T8','T9'
                                        ,'Master' , 'Landing Gear' , 'Engine V2500' ,'Engine CFM56' ,'APU'};
        String [] lookupType = new List<String> {'Engine','Engine','Engine','Engine','Engine','Engine'
                                        ,'Engine','Engine','Engine','Engine'
                                        ,'Engine','Engine','Engine'
                                        ,'Engine','Engine'
                                        ,'Engine','Engine'
                                        ,'Engine'
                                        ,'Engine','Engine'
                                        ,'Master' , 'Landing Gear' , 'Engine V2500' ,'Engine CFM56' ,'APU'};
                                        
        //delete [select id from Custom_Lookup__c] ;
        for(Integer i=0;i<lookupType.size();i++){
            Custom_Lookup__c p1 = new Custom_Lookup__c(name = lookupcode[i], Lookup_Type__c = lookupType[i], Sub_LookupType__c = SublookupType[i],Description__c = 'Seeded');
            LeaseWareUtils.clearFromTrigger();insert p1;
            mapInOut.put('Custom_Lookup_Name.'+ p1.Name + p1.Sub_LookupType__c,p1.Id);
        }       

    }    
 
    public static void setRecordTypeOfEngine(map<String,Object> mapInOut){
        
        LeaseWareUtils.clearFromTrigger();      
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Constituent_Assembly__c.getRecordTypeInfosByDeveloperName();

        //mapInOut.put('Engine CFM56',rtMapByName.get('Engine CFM56').getRecordTypeId())  ;
        mapInOut.put('Airframe',rtMapByName.get('Airframe').getRecordTypeId())  ;
        mapInOut.put('Engine',rtMapByName.get('Engine').getRecordTypeId())  ;
        //mapInOut.put('Other',rtMapByName.get('Other').getRecordTypeId())    ;
        mapInOut.put('Landing_Gear',rtMapByName.get('Landing_Gear').getRecordTypeId())    ;
        mapInOut.put('APU',rtMapByName.get('APU').getRecordTypeId())    ;


    } 
 
}