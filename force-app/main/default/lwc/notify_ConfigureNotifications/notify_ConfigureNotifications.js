import { LightningElement, wire, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import LWJavaScriptLib from '@salesforce/resourceUrl/LWJavaScriptLib';//Error Handling 
import Notify_JSZIP from '@salesforce/resourceUrl/Notify_JSZIP';//JS file to zip the trigger files

import NOTIFICATION_SUB_OBJECT from '@salesforce/schema/Notifications_Subscription__c';
import NOTIFICATION_TYPE from '@salesforce/schema/Notifications_Subscription__c.Notification_Type__c';
//Apex Imports
import getLoadData from '@salesforce/apex/Notify_NotificationSetupController.getLoadData';
import existingTriggerCheck from '@salesforce/apex/Notify_NotificationSetupController.existingTriggerCheck';
import deployMetadata from '@salesforce/apex/Notify_NotificationSetupController.deployMetadata';
import checkAsyncRequest from '@salesforce/apex/Notify_NotificationSetupController.checkAsyncRequest';
import checkNotificationSetting from '@salesforce/apex/Notify_NotificationSetupController.checkNotificationSetting';
import testNotificationData from '@salesforce/apex/Notify_NotificationSetupController.testNotificationData';
import saveNotificationData from '@salesforce/apex/Notify_NotificationSetupController.saveNotificationData';
import validateQuery from '@salesforce/apex/Notify_NotificationSetupController.validateQuery';
import checkIsSysAdmin from '@salesforce/apex/Notify_NotificationSetupController.checkIsSysAdmin';
import checkNameUpdateable from '@salesforce/apex/Notify_NotificationSetupController.checkNameUpdateable';
import validateMergeFieldValues from '@salesforce/apex/Notify_NotificationSetupController.validateMergeFieldValues';
import Id from '@salesforce/user/Id';




// Object icons To use.
const USER_ICON = 'standard:user';
const GROUP_ICON = 'standard:groups';
const PROFILE_ICON = 'standard:job_profile';

export default class Notify_ConfigureNotifications extends NavigationMixin(LightningElement) {

    userId = Id;

    dateOptions = [{
            label: '-- Select --',
            value: ''
        },
        {
            label: 'After',
            value: 'After'
        },
        {
            label: 'Before',
            value: 'Before'
        }
    ]
    formats = ['font', 'size', 'bold', 'italic', 'underline', 'strike', 'list', 'indent', 'align', 'link', 'clean', 'table', 'header', 'color', 'background', 'code', 'code-block', 'script', 'blockquote', 'direction'];
    jsLibLoaded = false;
    loadMergeFieldPopup = false;
    showTestResultModal = false;
    testNotificationResponse = '';
    hasNotificationSetting = true;
    maxSelectionSize = 10;
    objectApiName;
    validating = false;
    objectList = [{
            label: 'User',
            value: 'User',
            iconName: USER_ICON,
            strQueryField: 'Name, isActive',
            filterFieldAPIName: 'Name',
            whereClause: ' isActive = true',
            isDisabled: false
        },
        {
            label: 'Group',
            value: 'Group',
            iconName: GROUP_ICON,
            strQueryField: 'Name',
            filterFieldAPIName: 'Name',
            whereClause: 'name like  \'%\'',
            isDisabled: false
        },
        {
            label: 'Profile',
            value: 'Profile',
            iconName: PROFILE_ICON,
            strQueryField: 'Name',
            filterFieldAPIName: 'Name',
            whereClause: '',
            isDisabled: false
        },
    ];
    selectedRecords = [];

    @track isProcessing = false;
    @track notifyDayType = false;
    @track selectedObject;
    @track showFieldsSection = false;
    @track userEditAccess = false;
    @track showFilterField = false;
    @track title = '';
    @track isLoaded = false;
    @track isSysAdminFlag = true;
    @track disableActiveToggle = false;
    @track updatetable = false;


    @api loadData;
    @api recordId;


    @wire(getObjectInfo, {
        objectApiName: NOTIFICATION_SUB_OBJECT
    }) objectInfo;
    @wire(getObjectInfo, {
        objectApiName: '$objectApiName'
    }) selectedObjectInfo;
    @wire(getPicklistValues, {
        recordTypeId: '$objectInfo.data.defaultRecordTypeId',
        fieldApiName: NOTIFICATION_TYPE
    }) notificationTypeOptions;

    @wire(CurrentPageReference) pageRef() {
        if (this.isLoaded) {
            this.checkNotificationSetting();
        }
    };

    renderedCallback() {
        if (this.jsLibLoaded) {
            return;
        }
        this.jsLibLoaded = true;

        Promise.all([
            loadScript(this, LWJavaScriptLib),
            loadScript(this, Notify_JSZIP)
        ]).then(() => {
            this.updateElementStyle();
        }).catch(error => {
            this.showToast('Error loading external JS.', error.message, 'error');
        });
    }

    updateElementStyle() {
        let element = this.template.querySelector("[data-id='manualStyle']");
        if (element !== null && element !== undefined) {
            const style = document.createElement('style');
            style.innerText = `c-notify_-configure-notifications .radioInline .slds-form-element__control { display: flex; }`;
            element.appendChild(style);
        }
    }

    connectedCallback() {
        // console.log('Connected Callback Called');
        this.checkNotificationSetting();
    }

    moveToNotificationSetting() {
        this.hasNotificationSetting = true;
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Notification_Setting__c',
                actionName: 'list'
            },
            state: {
                filterName: 'Recent'
            },
        });
    }

    checkNotificationSetting() {
        checkNotificationSetting()
            .then(result => {
                this.hasNotificationSetting = result;
                if (this.hasNotificationSetting) {
                    this.isSysAdmin(this.userId);

                }
            })
            .catch(error => {
                this.handleMessage(error.message);
            })
    }

    loadInitialData() {
        this.isProcessing = true;
        //master source change to 

        if (this.isCloneMode) {
            //packaged code 
            // this.recordId = this.getParamValue('leaseworks__Notify_Notification_Configuration', 'c__Id'); 
            this.recordId = this.getParamValue('Notify_Notification_Configuration', 'c__Id');
        }
        console.log('recordId: ', this.recordId);

        getLoadData({
                recordId: this.recordId
            })
            .then((result) => {
                console.log('Result: ', JSON.parse(result));
                result = JSON.parse(result);
                if (result.selectedObjectName) {
                    this.objectApiName = result.selectedObjectName;

                    this.disableActiveCheckbox(result.selectedObjectName);

                }

                this.selectedRecords = [];
                if (result.selectedUserRecords) {
                    let selectedRecord = JSON.parse(result.selectedUserRecords);
                    if (selectedRecord) {
                        selectedRecord.map(record => {
                            if (record.sObjectType === 'User') {
                                record.icon = USER_ICON
                            } else if (record.sObjectType === 'Group') {
                                record.icon = GROUP_ICON
                            } else if (record.sObjectType === 'Profile') {
                                record.icon = PROFILE_ICON
                            }
                            this.selectedRecords.push(record);
                        });
                    }
                }
                if (result.notificationType === 'On Date') {
                    this.showFieldsSection = true;
                    this.notifyDayType = true;
                    this.showFilterField = true
                } else if (result.notificationType === 'On Change') {
                    this.showFieldsSection = true;
                    this.showFilterField = false
                } else {
                    this.showFilterField = false;
                }
                this.userEditAccess = result.userEditAccess;
                if (this.isCloneMode) {
                    this.userEditAccess = true;
                }
                if (!this.isUndefined(result.title)) {
                    this.title = result.title;
                }
                this.loadData = result;

                if (!this.userEditAccess) {
                    this.handleMessage("You don't have enough access to edit the record. Try Cloning the record instead.");
                }
                if (!this.isEditMode && !this.isCloneMode) {
                    this.setNotificationTitle();
                }
                this.isProcessing = false;
                this.isLoaded = true;
            })
            .catch((error) => {
                console.log(error);
                this.handleMessage(error.message);
                this.isProcessing = false;
            });
    }

    get isCloneMode() {
        //packaged code
        // return this.isUndefined(this.getParamValue('leaseworks__Notify_Notification_Configuration', 'c__isClone')) ? false : true;
        return this.isUndefined(this.getParamValue('Notify_Notification_Configuration', 'c__isClone')) ? false : true;
    }

    get isEditMode() {
        if (this.isCloneMode) {
            return false;
        }
        return !this.isUndefined(this.recordId) ? true : false;
    }

    getParamValue(tabName, paramName) {
        let url = window.location.href;
        let allParams = url.substr(url.indexOf(tabName) + tabName.length + 1).split('&');
        let paramValue = '';
        allParams.map(param => {
            if (param.split('=')[0] == paramName) {
                paramValue = param.split('=')[1];
            }
        });
        return paramValue;
    }

    handleObjectChange(event) {
        if (this.isEditMode) {
            return;
        }
        if (this.isCloneMode) {
            this.loadData.asyncJobId = '';
        }

        let selectedObject = this.loadData.sObjectList.find(sObj => sObj.value === event.target.value);
        if (selectedObject) {
            this.loadData.selectedObjectName = selectedObject.value;
            this.loadData.selectedObjectLabel = selectedObject.label;
            this.objectApiName = selectedObject.value;
            this.disableActiveCheckbox(selectedObject.value);
            this.setNotificationTitle();
            this.loadData.fieldAPIName = '';
            this.loadData.fieldName = '';
            this.isNameUpdateable(this.objectApiName);
        }
        // console.log('handleObjectChange.loadData: ', this.loadData);
    }

    handleSelectionChange(event) {
        let selectedRecords = event.detail;
        if (selectedRecords != undefined) {
            this.loadData.selectedUserGroupIds = '';
            this.loadData.selectedUserGroup = '';
            selectedRecords.map(record => {
                this.loadData.selectedUserGroupIds += (this.loadData.selectedUserGroupIds != '' ? ',' : '') + record.id;
                this.loadData.selectedUserGroup += (this.loadData.selectedUserGroup != '' ? ', ' : '') + record.name;
            })
        }
        // console.log('User Ids: ', this.loadData.selectedUserGroupIds);
        // console.log('User Names: ', this.loadData.selectedUserGroup);
    }

    handleNotificationChange(event) {
        if (this.isEditMode) {
            return;
        }
        this.disableActiveCheckbox(this.loadData.selectedObjectName);
        if (event.target.value === 'On Date') {
            this.showFieldsSection = true;
            this.notifyDayType = true;
            this.showFilterField = true;
        } else if (event.target.value === 'On Change') {
            this.showFieldsSection = true;
            this.notifyDayType = false;
            this.showFilterField = false;
        } else {
            this.showFieldsSection = false;
            this.notifyDayType = false;
            this.showFilterField = false;
        }
        this.loadData.fieldAPIName = '';
        this.loadData.fieldName = '';
        this.loadData.notificationType = event.target.value;
        this.setNotificationTitle();
        // console.log('handleNotificationChange.loadData: ', this.loadData);
    }

    handleisActiveChange(event) {
        this.loadData.isActive = event.target.checked;
        // console.log('handleNotificationChange.loadData: ', this.loadData);
    }

    get fieldOptions() {
        let fieldList = [];
        if (this.selectedObjectInfo && this.selectedObjectInfo.data && this.selectedObjectInfo.data.fields) {
            for (let key in Object.entries(this.selectedObjectInfo.data.fields)) {
                let fieldObj = Object.entries(this.selectedObjectInfo.data.fields)[key][1];
                if (fieldObj.label === 'System Modstamp' || fieldObj.label.includes('Y_Hidden')) {
                    continue;
                }
                if (this.loadData.notificationType === 'On Date' && (fieldObj.dataType === 'Date' || fieldObj.dataType === 'DateTime')) {
                    fieldList.push({
                        label: fieldObj.label,
                        value: fieldObj.apiName
                    });
                } else if (this.loadData.notificationType === 'On Change') {
                    fieldList.push({
                        label: fieldObj.label,
                        value: fieldObj.apiName
                    });
                }
            }
        }
        return fieldList.sort((a, b) => (a.label > b.label) ? 1 : -1);
    }

    handleFieldChange(event) {
        if (this.isEditMode) {
            return;
        }

        let fieldOptionList = this.fieldOptions;
        let selectedField = fieldOptionList.find(field => field.value === event.target.value);
        if (selectedField) {
            this.loadData.fieldAPIName = selectedField.value;
            this.loadData.fieldName = selectedField.label;
        }
        // console.log('handleObjectChange.loadData: ', this.loadData);
    }

    handleDayTypeChange(event) {
        this.loadData.notifyDayType = event.target.value;
        this.setNotificationTitle();
        // console.log('handleNotificationChange.loadData: ', this.loadData);
    }

    handleNotifyDaysChange(event) {
        this.loadData.notifyDays = event.target.value;
        this.setNotificationTitle();
        // console.log('handleNotificationChange.loadData: ', this.loadData);
    }

    handleRecordFilterCriteriaChange(event) {
        this.loadData.recordFilterCriteria = event.target.value;
        // console.log('handleNotificationChange.loadData: ', this.loadData);
    }

    isSysAdmin(userId) {
        checkIsSysAdmin({
                userId: userId
            })
            .then((result) => {
                console.log('user is sys admin? ---' + result);
                this.isSysAdminFlag = result;
                this.loadInitialData();
            }).catch((error) => {
                console.log('error---' + error);
                this.loadInitialData();
            });

    }
    isNameUpdateable(objAPIName) {
        console.log(' isNameUpdateable objectAPIName---' + objAPIName);
        checkNameUpdateable({
                objectAPIName: objAPIName
            })
            .then((result) => {
                console.log('is updatetable? ---' + result);
                this.updatetable = result;
            }).catch((error) => {
                console.log('error---' + error);
            });

    }
    disableActiveCheckbox(objectName) {
        console.log('disableActiveCheckbox');
        this.isProcessing = true;

        existingTriggerCheck({
                triggerName: this.generateTriggerName(objectName)
            })
            .then((result) => {
                console.log('result---' + result);
                if (result == null || result.length == 0) {
                    if (!this.isSysAdminFlag && this.loadData.notificationType != 'On Date') {
                        //If Trigger is not present and user is not sysadmin deactivate the checkbox  
                        //deactiviateCheckBox
                        this.loadData.isActive = false;
                        this.disableActiveToggle = true;
                    } else {
                        this.disableActiveToggle = false;
                    }
                } else {
                    this.disableActiveToggle = false;

                }
                this.isProcessing = false;
            }).catch((error) => {
                console.log('error---' + error);
                let errorMessage = error.message;
                if (!this.isUndefined(error.body.message)) {
                    errorMessage = error.body.message;
                }
                this.handleMessage(errorMessage);
                this.isProcessing = false;
            });

    }
    setNotificationTitle() {
        let title = '';

        if (!this.isUndefined(this.loadData.selectedObjectName)) {
            title += this.loadData.selectedObjectLabel + ' - ';
        }
        if (!this.isUndefined(this.loadData.notificationType)) {
            title += this.loadData.notificationType + ' - ';
            if (this.loadData.notificationType == 'On Date') {
                title += '<';
                if (!this.isUndefined(this.loadData.notifyDayType)) {
                    title += this.loadData.notifyDayType + ' - ';
                }
                if (!this.isUndefined(this.loadData.notifyDays)) {
                    title += this.loadData.notifyDays + (this.loadData.notifyDays <= 1 ? ' Day' : ' Days');
                }
                title += '> - ';
            }
        }
        if (!this.isUndefined(title)) {
            this.loadData.title = title + '{{name}}';
        }
        this.title = this.loadData.title;
    }


    handleTitleChange(event) {
        this.loadData.title = event.target.value;
        this.title = this.loadData.title;
        // console.log('handleTitleChange.loadData: ', this.loadData);
    }

    handleMessageChange(event) {
        this.loadData.messageBody = event.target.value;
        // console.log('handleMessageChange.loadData: ', this.loadData);
    }

    handleMergeFields() {
        if (this.loadData.selectedObjectName == '') {
            this.handleMessage('Please select an Object to Subscribe to the Notification.');
            return;
        }
        this.loadMergeFieldPopup = true;
    }

    closeModal() {
        this.loadMergeFieldPopup = false;
    }

    testNotificationConfig() {
        console.log('Load Data: ', this.loadData);
        if (this.loadData.selectedObjectName == '') {
            this.handleMessage('Please select an Object to Subscribe to the Notification.');
            return;
        }
        if (this.loadData.title == '') {
            this.handleMessage('Please provide a suitable title for the notification.');
            return;
        }
        if ((this.loadData.notificationType == 'On Date' || this.loadData.notificationType == 'On Change') && this.loadData.fieldAPIName == '') {
            this.handleMessage('Please choose a field for the notification to be based on.');
            return;
        }
        if (this.loadData.notificationType == 'On Date' && this.loadData.notifyDayType == '') {
            this.handleMessage('Please choose a Notify Type.');
            return;
        }
        if (this.loadData.notificationType == 'On Date' && (parseInt(this.loadData.notifyDays) > 999)) {
            this.handleMessage('The Limit for the Notify Days is 999');
            return;
        }
        if (this.loadData.selectedUserGroupIds.split(',').length > 10) {
            this.handleMessage('Please select only 10 users.');
            return;
        }
        this.isProcessing = true;
        console.log('testNotificationConfig');

        if (this.loadData.messageBody != null || this.loadData.title!=null) {
            console.log('validateMergeFieldValues--');
            validateMergeFieldValues({
                    messageBody: this.loadData.messageBody,
                    title:this.loadData.title,
                    objName: this.loadData.selectedObjectName
                })
                .then((result) => {
                    result = JSON.parse(result);

                    console.log('validateMergeFieldValues---- result----' + result);
                    if (result) {

                        if (this.loadData.notificationType == 'On Date') {
                            if (this.loadData.recordFilterCriteria != '' && this.loadData.recordFilterCriteria != undefined && this.loadData.recordFilterCriteria != null) {
                                validateQuery({
                                        objName: this.loadData.selectedObjectName,
                                        filter: this.loadData.recordFilterCriteria
                                    })
                                    .then(result => {
                                        console.log('Query Result: ', result);
                                        if (result) {
                                            testNotificationData({
                                                    loadData: JSON.stringify(this.loadData)
                                                })
                                                .then((result) => {
                                                    this.testNotificationResponse = result;
                                                    this.showTestResultModal = true;
                                                    this.isProcessing = false;
                                                })
                                                .catch((error) => {
                                                    this.testNotificationResponse = error.message;
                                                    if (!this.isUndefined(error.body.message)) {
                                                        this.testNotificationResponse = error.body.message;
                                                    }
                                                    this.showTestResultModal = true;
                                                    this.isProcessing = false;
                                                });
                                        } else {
                                            this.handleMessage('Please provide a valid filter condition');
                                            this.isProcessing = false;
                                        }
                                    })
                                    .catch(error => {
                                        this.testNotificationResponse = error.message;
                                        if (!this.isUndefined(error.body.message)) {
                                            this.testNotificationResponse = error.body.message;
                                        }
                                        this.showTestResultModal = true;
                                        this.isProcessing = false;
                                    })
                            } else {
                                testNotificationData({
                                        loadData: JSON.stringify(this.loadData)
                                    })
                                    .then((result) => {
                                        this.testNotificationResponse = result;
                                        this.showTestResultModal = true;
                                        this.isProcessing = false;
                                    })
                                    .catch((error) => {
                                        this.testNotificationResponse = error.message;
                                        if (!this.isUndefined(error.body.message)) {
                                            this.testNotificationResponse = error.body.message;
                                        }
                                        this.showTestResultModal = true;
                                        this.isProcessing = false;
                                    });
                            }
                        } else {
                            testNotificationData({
                                    loadData: JSON.stringify(this.loadData)
                                })
                                .then((result) => {
                                    this.testNotificationResponse = result;
                                    this.showTestResultModal = true;
                                    this.isProcessing = false;
                                })
                                .catch((error) => {
                                    this.testNotificationResponse = error.message;
                                    if (!this.isUndefined(error.body.message)) {
                                        this.testNotificationResponse = error.body.message;
                                    }
                                    this.showTestResultModal = true;
                                    this.isProcessing = false;
                                });
                        }
                    } else {
                        this.handleMessage('Please check your Title and Message Body for correct fields. Some fields do not belong to the selected object therefore Notification cannot be saved');
                        this.isProcessing = false;
                    }

                }).catch((error) => {
                    this.handleMessage(error.message);
                    this.isProcessing = false;
                });
        }
    }

    closeTestResultModal() {
        this.showTestResultModal = false;
    }

    validateAndSaveRecord() {
        console.log('loadData', this.loadData);
        if (this.validating) {
            return
        };
        this.validating = true;

        let updatedData = this.loadData;
        if (updatedData.notificationType != 'On Date') {
            updatedData.notifyDayType = '';
            updatedData.notifyDays = 0;
            updatedData.recordFilterCriteria = '';
            if (updatedData.notificationType != 'On Change') {
                updatedData.fieldAPIName = '';
                updatedData.fieldName = '';
            }
            this.loadData = updatedData;
        }
        console.log(' --> Updated Data', this.loadData);

        if (this.loadData) {
            if (this.loadData.selectedObjectName == '') {
                this.validating = false;
                this.handleMessage('Please select an Object to Subscribe to the Notification.');
                return;
            }
            if (this.isUndefined(this.loadData.selectedUserGroupIds)) {
                this.validating = false;
                this.handleMessage('Please select Users who will get the Notification upon subscription of the Notification.');
                return;
            }
            if (this.loadData.selectedUserGroupIds.split(',').length > this.maxSelectionSize) {
                this.validating = false;
                this.handleMessage('Please limit the number of users to recieve the Notification.');
                return;
            }
            if (this.loadData.title == '') {
                this.validating = false;
                this.handleMessage('Please provide a suitable title for the notification.');
                return;
            }
            if ((this.loadData.notificationType == 'On Date' || this.loadData.notificationType == 'On Change') && this.loadData.fieldAPIName == '') {
                this.validating = false;
                this.handleMessage('Please choose a field for the notification to be based on.');
                return;
            }
            if (this.loadData.notificationType == 'On Date' && this.loadData.notifyDayType == '') {
                this.validating = false;
                this.handleMessage('Please choose a Notify Type.');
                return;
            }
            if (this.loadData.notificationType == 'On Date' && this.loadData.notifyDays == '') {
                this.validating = false;
                this.handleMessage('Please enter Number of days to Notify.');
                return;
            }

            if (this.loadData.notificationType == 'On Date' && (parseInt(this.loadData.notifyDays) > 999)) {
                this.validating = false;
                this.handleMessage('The Limit for the Notify Days is 999');
                return;
            }

            this.isProcessing = true;

            if (this.loadData.messageBody != null || this.loadData.title != null) {
                validateMergeFieldValues({
                        messageBody: this.loadData.messageBody,
                        title:this.loadData.title,
                        objName: this.loadData.selectedObjectName
                    })
                    .then((result) => {
                        result = JSON.parse(result);

                        console.log('validateMergeFieldValues---- result----' + result);
                        if (result) {
                            console.log('this.loadData.status---' + this.loadData.status);
                            console.log('this.loadData.isActive---' + this.loadData.isActive);
                            console.log('this.isSysAdminFlag---' + this.isSysAdminFlag);
                            if (this.loadData.notificationType != 'On Date') {
                                if (this.isEditMode) {

                                    if (!this.isUndefined(this.loadData.asyncJobId) && this.loadData.status == 'Pending') {
                                        console.log('this.loadData.asyncJobId', this.loadData.asyncJobId);
                                        this.checkAsyncJobStatus(this.loadData.asyncJobId, this.loadData.isActive, true);
                                    } else if (this.loadData.status == 'Not Running' && this.loadData.isActive && this.isSysAdminFlag) {
                                        console.log('Deploy Metadata......');
                                        this.checkExistingTriggerAndDeploy(this.loadData.selectedObjectName);
                                    } else {
                                        this.saveNotificationRecord();
                                    }
                                } else {
                                    this.checkExistingTriggerAndDeploy(this.loadData.selectedObjectName);
                                }
                            } else {
                                this.saveNotificationRecord();
                            }


                        } else {
                            this.handleMessage('Please check your Title and Message Body for correct fields. Some fields do not belong to the selected object therefore Notification cannot be saved');
                            this.isProcessing = false;
                        }

                    }).catch((error) => {
                        this.handleMessage(error.message);
                        this.isProcessing = false;
                    });
                this.validating = false;
            }
            

        }

    }


    generateTestClassName(triggerName) {

        let testClass = 'Test_' + triggerName.replace('_O_', '').replace('_AutoTrigger', '').replace('Local', '').replace('leaseworks', 'LW_');


        if (testClass != 'undefined' && testClass.length > 40) {
            testClass = testClass.substr(0, 40);
        }
        if (testClass.endsWith('_')) {
            testClass = testClass.slice(0, -1);
        };

        return testClass;
    }
    checkExistingTriggerAndDeploy(objectAPIName) {
        console.log('objectAPIName-----' + objectAPIName);
        let triggerName = this.generateTriggerName(this.loadData.selectedObjectName);
        existingTriggerCheck({
                triggerName: triggerName
            })
            .then((result) => {
                if (result != null && result.length != 0) {
                    this.saveNotificationRecord();
                } //trigger exists just save the record
                else {
                    //trigger doesnot exists , hence deploy the trigger
                    console.log('createObjectTrigger Name----');

                    let testClass = this.generateTestClassName(triggerName);
                    if (this.isSysAdminFlag) {

                        console.log('sys admin so deploy thhe metadata');
                        deployMetadata({
                                data: this.createObjectTrigger(this.loadData.selectedObjectName),
                                testClassName: testClass
                            })
                            .then((result) => {
                                this.loadData.asyncJobId = result;
                                this.checkAsyncJobStatus(this.loadData.asyncJobId, this.loadData.isActive, true);
                            })
                            .catch((error) => {
                                let errorMessage = error.message;
                                if (!this.isUndefined(error.body.message)) {
                                    errorMessage = error.body.message;
                                }
                                this.handleMessage(errorMessage);
                                this.isProcessing = false;
                            });
                    } else {
                        console.log('Not sysadmin');
                        this.saveNotificationRecord();
                    }
                }
            })
            .catch((error) => {
                console.log('error---' + error);
                let errorMessage = error.message;
                if (!this.isUndefined(error.body.message)) {
                    errorMessage = error.body.message;
                }
                this.handleMessage(errorMessage);
                this.isProcessing = false;
            });
    }
    generateTriggerName(objAPIName) {
        let triggerName = '';
        let namespace = 'Local';
        let objName = objAPIName.split('__');
        let objectName = '';
        if (objName.length === 3) {
            namespace = objName[0];
        }
        console.log('namespace-----' + namespace);
        console.log(objAPIName.includes(namespace));
        if (objAPIName.includes(namespace)) {
            objectName = objAPIName.replace(namespace + '__', '').replace('__c', '');
        } else {
            objectName = objAPIName.replace('__c', '');
        }

        console.log('objectName-----' + objectName);
        triggerName = triggerName = 'Notify_' + namespace + '_O_' + objectName + '_' + 'AutoTrigger';
        console.log('triggerName---' + triggerName);
        return triggerName;
    }


    createObjectTrigger(objAPIName) {

        let triggerName = this.generateTriggerName(objAPIName);
        console.log('TriggerName Notify----------' + triggerName);
        if (triggerName != '') {
            var testClassName = this.generateTestClassName(triggerName);
            console.log('testClassName---' + testClassName);
            var testName = testClassName + '.cls';
            let packageXML = `<?xml version="1.0" encoding="UTF-8"?> <Package xmlns="http://soap.sforce.com/2006/04/metadata"> <types> <members>` + triggerName + `</members> <name>ApexTrigger</name> </types><types> <members>` + testClassName + `</members> <name>ApexClass</name></types><version>50.0</version> </Package>`;
            //WHen pushing to master source uncomment this and comment the below line
            // let triggerBody = `trigger ` + triggerName + ` on ` + objAPIName + ` (before insert, before update, before delete, after insert, after update, after delete, after undelete) { leaseworks.Notify_NotificationScheduler.triggerHandler(` + objAPIName + `.sObjectType); }`;

            let triggerBody = `trigger ` + triggerName + ` on ` + objAPIName + ` (before insert, before update, before delete, after insert, after update, after delete, after undelete) { Notify_NotificationScheduler.triggerHandler(` + objAPIName + `.sObjectType); }`;
            let triggerBodyMetaData = `<?xml version="1.0" encoding="UTF-8"?> <ApexTrigger xmlns="http://soap.sforce.com/2006/04/metadata"> <apiVersion>50.0</apiVersion> <status>Active</status> </ApexTrigger>`;

            //check the object Name is editable,based on that define the class body
            let classBodyNameUpdateable = ` system.debug('Insert'); `;
            console.log('isUpdatetable----' + this.updatetable);
            if (this.updatetable) {
                classBodyNameUpdateable = objAPIName + ` testObj =  new ` + objAPIName + ` (); testObj.name ='test' ; \n insert testObj;  `;
            }
            let testClassBody = ` @isTest(SeeAllData=True) \n public class ` + testClassName + `{  \n @isTest \n static void testCRUD(){ \n // insert scenario test \n try{ \n ` + classBodyNameUpdateable + `\n }catch(exception e){} \n //update scenario test \n   try{ \n ` + objAPIName + ` testObj =  ` + ` [select Id,name from ` + objAPIName + ` limit 1 ];\n update testObj ;\n }\n  catch(Exception e){} \n //delete scenario test \n  try{ ` + objAPIName + ` testObj =  ` + ` [select Id,name from ` + objAPIName + ` limit 1 ]; \n delete testObj;} \n catch(Exception e){}\n } \n  }`;
            console.log(testClassBody);
            let testClassBodyMetadata = `<?xml version="1.0" encoding="UTF-8"?> <ApexClass xmlns="http://soap.sforce.com/2006/04/metadata"> <apiVersion>48.0</apiVersion> <status>Active</status> </ApexClass>`;


            let zipFile = new JSZip();
            zipFile.file('package.xml', packageXML);

            zipFile.file('triggers/' + triggerName + '.trigger', triggerBody);
            zipFile.file('triggers/' + triggerName + '.trigger-meta.xml', triggerBodyMetaData);

            zipFile.file('classes/' + testName, testClassBody);

            zipFile.file('classes/' + testName + '-meta.xml', testClassBodyMetadata);

            console.log('zipFile----' + zipFile.generate());

            return zipFile.generate();
        }

    }



    checkAsyncJobStatus(jobId, isActive, proceedWithSave) {
        checkAsyncRequest({
                asyncId: jobId
            })
            .then((result) => {
                result = JSON.parse(result);

                console.log('checkAsyncJobStatus---- result----' + result);

                if (result.isDeployed) {

                    if (result.errorMessage) {
                        this.loadData.status = 'Error';
                        this.loadData.deploymentMsg = result.errorMessage;
                        this.handleMessage(this.loadData.status + ' - ' + this.loadData.deploymentMsg);
                    } else {
                        this.loadData.status = 'Running';
                    }
                }

                this.isProcessing = false;
                if (proceedWithSave) {
                    this.saveNotificationRecord();
                }
            })
            .catch((error) => {
                this.handleMessage(error.message);
                this.isProcessing = false;
            });
    }

    saveNotificationRecord() {
        console.log(' --> Final----', this.loadData);

        if (this.loadData.notificationType == 'On Date') {
            this.isProcessing = true;
            if (this.loadData.recordFilterCriteria != undefined && this.loadData.recordFilterCriteria != null && this.loadData.recordFilterCriteria != '') {
                validateQuery({
                        objName: this.loadData.selectedObjectName,
                        filter: this.loadData.recordFilterCriteria
                    })
                    .then(result => {
                        console.log('Query Result: ', result);
                        if (result) {
                            this.saveData();
                        } else {
                            this.handleMessage('Please provide a valid filter condition');
                            this.isProcessing = false;
                        }
                    })
                    .catch(error => {
                        this.handleMessage(error.body.message);
                        this.isProcessing = false;
                    })
            } else {
                this.saveData();
            }
        } else {
            this.saveData();
        }

    }

    saveData() {
        saveNotificationData({
                recordId: this.recordId,
                loadData: JSON.stringify(this.loadData),
                isClone: this.isCloneMode
            })
            .then((result) => {
                if (result.Status__c == 'Pending') {
                    this.handleMessage('Your notification subscription was saved. It may take up to 20 minutes to enable it and we will switch this to "Running" automatically once completed', 'Warning');
                } else {
                    if (this.isEditMode) {
                        this.handleMessage('Notification record updated successfully', 'Success');
                    } else {
                        this.handleMessage('Notification record created successfully', 'Success');
                    }

                }


                this.navigateToRecordPage(result.Id);
                this.isProcessing = true;
            })
            .catch((error) => {
                this.handleMessage(error.body.message);
                this.isProcessing = false;
            });
    }

    closeConfigWindow() {
        this.navigateToRecordPage(this.recordId);
    }

    navigateToRecordPage(recordId) {
        if (!this.isUndefined(recordId)) {
            setTimeout(() => {
                window.open("/" + recordId, "_self");
            }, 1500);
        } else {
            this.isProcessing = false;
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: NOTIFICATION_SUB_OBJECT.objectApiName,
                    actionName: 'home'
                },
            })
        }

    }

    isUndefined(input) {
        if (input != undefined && input != '' && input != null) {
            return false;
        }
        return true;
    }

    showToast(title, message, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }

    //Show the error toast when any error occured
    handleMessage(errors, type, duration) {
        //duration = '10000'; // time to display the Toast message. (default: 7000, min: 5000)
        if (_jsLibrary) {
            _jsLibrary.handleErrors(errors, type, duration); // Calling the JS Library function to handle the error
        }
    }
}