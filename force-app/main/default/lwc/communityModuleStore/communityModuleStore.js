import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import getTemplateInfo from "@salesforce/apex/CommunityModuleStoreController.getTemplateInfo";
import getAssetPicklistValues from "@salesforce/apex/CommunityModuleStoreController.getAssetPicklistValues";
import getDatatableColumns from "@salesforce/apex/CommunityModuleStoreController.getDatatableColumns";
import getDatatableFilters from "@salesforce/apex/CommunityModuleStoreController.getDatatableFilters";
import getTodaysDate from "@salesforce/apex/CommunityModuleStoreController.getTodaysDate";
import getPortalAssetData from "@salesforce/apex/CommunityModuleStoreController.getPortalAssetData";
import getPortalAssetPartsData from "@salesforce/apex/CommunityModuleStoreController.getPortalAssetPartsData";
import generateRequest from "@salesforce/apex/CommunityModuleStoreController.generateRequest";
import REQUEST_OBJECT from '@salesforce/schema/Portal_Asset_Request__c';
import DEAL_TYPE_FIELD from '@salesforce/schema/Portal_Asset_Request__c.Request_Type__c';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { LightningElement, api, wire, track } from "lwc";

export default class CommunityModuleSearch extends LightningElement {

    @api assetClassValues;
    assetClassOptions = [];
    displayAssetClass = false;

    @api serviceTypeValues;
    serviceTypeOptions = [];

    @api assetClassLabel;
    @api assetTypeLabel;
    @api assetVariantLabel;
    @api deliveryDateLabel;
    @api serviceTypeLabel;
    @api dealTypeLabel;
    @api locationLabel;
    @api pmaderLabel;
    @api serviceTypeRequired;
    @api searchresultsLabel;
    @api searchdescriptionLabel;
    @api isCompSignAvailable;
    @api isCustomBuild;

    assetTypeOptions = [];
    assetVariantOptions = [];

    //Method calls Controller to retrieve picklist values for the Asset Type and Asset Variant inputs
    getAssetPicklistData(){
        getAssetPicklistValues({ assetClass: this.inputAssetClass})
            .then(data =>{
                let arrayType = [];
                let arrayVariant = [];
                data.assetTypeValues.forEach(assetType => {

                    let picklistValue = {
                        label: assetType.picklistFieldValue,
                        value: assetType.picklistFieldLabel
                    }
            
                    arrayType.push(picklistValue);
                });       
                data.assetVariantValues.forEach(assetVar => {

                    let picklistValue = {
                        label: assetVar.picklistFieldValue,
                        value: assetVar.picklistFieldLabel
                    }
            
                    arrayVariant.push(picklistValue);
                });  
                this.assetTypeOptions = arrayType;
                this.assetVariantOptions = arrayVariant;
             }).catch(error => {
            this.error = error;
            console.log(error);
            this.inputFailureToast("Failed to get Picklist Values");
        });
    }
    
    availableModuleColumns = [];

    //Retrieves datatable columns data for search result display
    getDatatableColumnsData(){
        getDatatableColumns({ assetClass: this.inputAssetClass})
            .then(data =>{
                this.availableModuleColumns = JSON.parse(data);
                this.getAssetSearchResults();
            }).catch(error => {
            this.error = error;
            console.log(error);
            this.inputFailureToast("Failed to get Datatable Columns Data");
        });
    }

    dynamicDatatableFilters;

    //Retrieves dynamic datatable filters for search result adjustment
    getDatatableFiltersData(){
        getDatatableFilters({ assetClass: this.inputAssetClass})
            .then(data =>{
                this.dynamicDatatableFilters = data;
        }).catch(error => {
            this.error = error;
            console.log(error);
            this.inputFailureToast("Failed to get Datatable Filters Data");
        });
    }

    //Method tracking if Exchange picklist value is selected
    get isExchangeSelected() {
        return this.inputDealType == "Exchange"
    }

    //Method for dynamically making input field eligible for validation if it is displayed
    get assetclassClass() {
        if (this.displayAssetClass == true) {
           return "searchvalidation submitvalidation"
        } else {
            return null
        }
    }

    //Method for dynamically making input field eligible for validation if it is displayed
    get serviceTypeClass() {
        if (this.serviceTypeLabel != undefined || this.serviceTypeLabel != null) {
           return "searchvalidation submitvalidation"
        } else {
            return null
        }
    }

    //Sets picklist values and minimal delivery date on connect
    connectedCallback() {
        this.setPicklistValues();
        this.getAssetPicklistData();
        this.getTodaysDateData();
        this.getDatatableColumnsData();
        this.getDatatableFiltersData();
    }

    //Sets picklist values for all the input picklist and multipicklists based on the configuration
    setPicklistValues() {
        let assetClassValuesArray = [];
        assetClassValuesArray = this.assetClassValues.split(",");
        assetClassValuesArray.forEach((item) =>
        {
            let picklistValue = {
                value: item,
                label: item,
            }

        this.assetClassOptions.push(picklistValue);
        })

        if (this.assetClassOptions.length > 1) {
            this.displayAssetClass = true;
        } else {
            this.inputAssetClass = this.assetClassValues;
        }

        let serviceTypeValuesArray = [];
        serviceTypeValuesArray = this.serviceTypeValues.split(",");
        serviceTypeValuesArray.forEach((item) =>
        {
            let picklistValue = {
                value: item,
                label: item,
            }

        this.serviceTypeOptions.push(picklistValue);
        this.inputServiceType = this.serviceTypeOptions[0].value;
        })
    }

    todaysDate;

    //Retrieves minimal delivery date
    getTodaysDateData(){
        getTodaysDate()
            .then(data =>{
                this.todaysDate = data;
                this.inputDeliveryDate = this.todaysDate;
        }).catch(error => {
            this.error = error;
            this.inputFailureToast("The minimal date was not retrieved");
        });
    }


    //Following method sets picklist values for non-configurable picklists by retrieving their values from the system
    @wire(getObjectInfo, {objectApiName: REQUEST_OBJECT})
    requestInfo;

    @track dealTypeOptions;

    @wire(getPicklistValues, {recordTypeId:'$requestInfo.data.defaultRecordTypeId', fieldApiName: DEAL_TYPE_FIELD })
    dealTypeOptions({ data, error }) {
        if (data) {
            this.dealTypeOptions = data.values;
            this.inputDealType = this.dealTypeOptions[0].value}
        if (error) {
            this.error = error;
        }
    }

    @track inputAssetClass;
    @track inputAssetType = [];
    @track inputAssetVariant = [];
    @track inputDeliveryDate;
    @track inputServiceType;
    @track inputDealType;
    @track inputLocation;
    @track inputAddRequest;

    //Method for tracking the inputs in the input fields
    handleAssetInput(event) {
        if (event.target.name === 'assetclass') {
            this.inputAssetClass = event.target.value;
        } else if (event.target.name === 'assettype') {
            this.inputAssetType = event.target.value;
        } else if (event.target.name === 'assetvariant') {
            this.inputAssetVariant = event.target.value;
        } else if (event.target.name === 'delivdate') {
            this.inputDeliveryDate = event.target.value;
        } else if (event.target.name === 'servicetype') {
            this.inputServiceType = event.target.value;
        }  else if (event.target.name === 'exchpurch') {
            this.inputDealType = event.target.value;
        }  else if (event.target.name === 'location') {
            this.inputLocation = event.target.value;
        }  else if (event.target.name === 'additinfo') {
            this.inputAddRequest = event.target.value;
        }

        let eventName = event.target.name;
        if(eventName === 'assetclass' || eventName === 'assettype' || eventName === 'assetvariant') {
            if (this.inputAssetClass != null && this.inputAssetType.length > 0 && this.inputAssetVariant.length > 0) {
                this.getTemplateData();
            }
        }   
    }

    assetTemplateData;

    //Retrieves template data for filter input
    getTemplateData(){
        getTemplateInfo({ assetClass: this.inputAssetClass, assetTypeList: this.inputAssetType, assetVariantList: this.inputAssetVariant })
            .then(data =>{
                if(data != null) {
                    console.log('getTemplateData '+JSON.stringify(data));
                }
                this.assetTemplateData = data;
                if (this.isCustomBuild == true) {
                    this.getCustomBuildPartsData();
                    this.isSubmitButtonDisabled = false;
                }
                if (this.portalAssetData != undefined && this.portalAssetData.length > 0) {
                    //this.portalAssetData = undefined;
                    this.isSearchClicked = false;
                    if (this.templateAssetPartsData != undefined && this.templateAssetPartsData.length > 0) {
                        this.templateAssetPartsData = undefined;
                    }
                }
        }).catch(error => {
            this.error = error;
            this.inputFailureToast("Failed to get Template Information");
        });
    }

    filterList = [];
    dynamicfilterList = [];
    isSearchClicked = false;

    //Method that checks validity of all the input fields and calls the search if all inputs are valid
    submitSearch() {
        this.filterList = [];
        let isValid = true;
        let compArray = [];
        let filterArray = [];

        isValid = isValid && this.checkSearchInput();

        compArray = this.template.querySelectorAll('c-community-module-template');

        compArray.forEach((comp) => {
            isValid = isValid && comp.checkTemplateValidity();
        })

        this.dynamicfilterList = [];
        filterArray = this.template.querySelectorAll('c-community-module-filter');
        filterArray.forEach((filter) => {
            this.dynamicfilterList.push(JSON.parse(filter.returnFiltersData()));
        })

        if (isValid == false) {
            this.inputFailureToast("One of the required fields is empty or contains the wrong inputs");
        } else {
            compArray.forEach((comp) => {
                this.filterList.push(JSON.parse(comp.returnFiltersData()));
            })
            this.getAssetSearchResults();
            this.isSubmitButtonDisabled = false;
            this.isSearchClicked = true;
        }
    }
    
    //Method for checking validity in Search input fields
    checkSearchInput(){
        const isInputsCorrect = [...this.template.querySelectorAll('.searchvalidation')]
        .reduce((validSoFar, inputField) => {
            inputField.reportValidity();
            return validSoFar && inputField.checkValidity();
        }, true)

        return isInputsCorrect;
    }

    //Method for checking validity on Submit
    checkSubmitInput(){
        const isInputsCorrect = [...this.template.querySelectorAll('.submitvalidation')]
        .reduce((validSoFar, inputField) => {
            inputField.reportValidity();
            return validSoFar && inputField.checkValidity();
        }, true)

        return isInputsCorrect;
    }

    //Method for showing error message on invalid inputs
    inputFailureToast(message){
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Error on data input',
                message: message,
                variant: 'error',
            }),
        );
    }

    @track portalAssetData;

    //Method that calls the search based on inputs and retrieves the data
    getAssetSearchResults() {
        getPortalAssetData({assetClass: this.inputAssetClass, assetTypeList: this.inputAssetType, assetVariantList: this.inputAssetVariant, filterListString: JSON.stringify(this.filterList), fieldsetFilterString: JSON.stringify(this.dynamicfilterList)})
        .then(data =>{
            this.portalAssetData = JSON.parse(data);
    }).catch(error => {
        this.error = error;
        console.log(error);
        this.inputFailureToast("Failed to retrieve Portal Asset Data");
    });
    }

    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;

    // Used to sort the datatable columns
    sortBy(field, reverse, primer) {
        const key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    //Method for sorting the datatable columns based on clicking the headers
    onHandleSort(event) {
        // field name
        this.sortedBy = event.detail.fieldName;
        // sort direction
        this.sortDirection = event.detail.sortDirection;
        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.portalAssetData));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1: -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.portalAssetData = parseData;

    }

    templateIdsList = [];
    
    //Retrieves the data of the selected datatable rows
    getSelectedAssets() {
        this.templateIdsList = [];
        let rows = [];
        rows = this.template.querySelector('.searchresulttable').getSelectedRows();
        rows.forEach((row) =>
        {
            let templateId = row.Portal_Asset_Template__c;
            this.templateIdsList.push(templateId);
        })
        this.getAssetPartsData();
    }

    templateAssetPartsData;

    //Method that retrieves Asset Parts data based on selected rows
    getAssetPartsData() {
        getPortalAssetPartsData({selectedTemplateIdList: this.templateIdsList })
            .then(data =>{
                this.templateAssetPartsData = data;
        }).catch(error => {
        this.error = error;
            this.inputFailureToast("Failed to retrieve Portal Asset Parts Data");
        });
    }

    //Method for updating exchange table values based on child component inputs and events
    updatePartValues(event) {
        try {
            if (typeof event.detail == 'string'){
                var partInput = JSON.parse(event.detail);
                var tempdata = this.templateAssetPartsData;
                for (let tdindex in tempdata) {
                    let td = tempdata[tdindex];
                    var data = td.partRecordsList;
                    for (let index in data) {
                        let d = data[index];
                        if (d.partId == partInput.partId) {
                            d.cyclesRemaining = partInput.cyclesRemaining;
                            break;
                    }
                }
            }
        }
        } catch (error) {
            this.inputFailureToast("Failed to update Asset Parts Data");
        }
    }

    isSubmitButtonDisabled = false;

    //Method for submitting all the inputs (if valid) and commiting data into Salesforce
    handleFinalSubmit() {
        let isValid = true;
        let compArray = [];

        //Checks Initial input (Asset Type, Asset Variant, etc.)
        isValid = isValid && this.checkSearchInput();

        //Checks Template Filter input by template
        compArray = this.template.querySelectorAll('c-community-module-template');

        compArray.forEach((comp) => {
            isValid = isValid && comp.checkTemplateValidity();
        });

        //Checks if any rows have been selected in Search Results
        //In Custom build checks if there are returned templates
        isValid = isValid && this.templateIdsList.length > 0;

        if (isValid == false) {
            this.inputFailureToast("Check that all inputs (including the exchangeable parts) are correct/in place before submit");
        } else {
            console.log("Everything is fine");

            var request = {
                assetClass: this.inputAssetClass,
                requestType: this.inputDealType,
                dateStart: this.inputDeliveryDate,
                serviceType: this.inputServiceType,
                additionalRequest: this.inputAddRequest,
                requestedLocation: this.inputLocation,
            };

            if (this.isCustomBuild == true) {
                this.filterList = [];
                let tempArray = [];
                tempArray = this.template.querySelectorAll('c-community-module-template');
                tempArray.forEach((comp) => {
                    this.filterList.push(JSON.parse(comp.returnFiltersData()));
                })
            }

            /********** EXCHANGE START **********/
            if (this.isExchangeSelected == true) {
                let requestDetails = [];
                let exchangeRequestDetails = [];
    
                if (this.isCustomBuild == true) {
                    this.filterList.forEach((filter) => {
                        var requestDetail = {};
                        var exchangeRequestDetail = {};

                        requestDetail = {
                            assetType: filter.assetType,
                            assetVariant: filter.assetVariant,
                            egtMargin: filter.egtMargin,
                        };

                        exchangeRequestDetail = {
                            assetType: filter.assetType,
                            assetVariant: filter.assetVariant,
                            egtMargin: filter.egtMargin,
                            parts: []
                        }

                        let pmader = false;
                        this.dynamicfilterList.forEach((filter) => {
                            if (filter.fieldName == 'PMA_DER__c' && filter.fieldValue == "Yes") {
                                pmader = true;
                            }
                        })

                        if (pmader == true) {
                            requestDetail.pmaDer = true;
                            exchangeRequestDetail.pmaDer = true;
                        } else {
                            requestDetail.pmaDer = false;
                            exchangeRequestDetail.pmaDer = false;
                        }

                        this.templateAssetPartsData.forEach((template) => {
                            if (template.templateId == filter.templateId) {
                                template.partRecordsList.forEach((part) => {
                                    let assetpart = {
                                        name: part.partName,
                                        remainingCycles: part.cyclesRemaining
                                    };
                                    exchangeRequestDetail.parts.push(assetpart);
                                });
                            }
                        });

                        requestDetails.push(requestDetail);
                        exchangeRequestDetails.push(exchangeRequestDetail);

                    })
                } else {
                    var templateDict = {}
    
                    for (let tIndex in this.templateAssetPartsData) {
                        let template = this.templateAssetPartsData[tIndex];
                        templateDict[template.templateId] = template;
                    }
        
                    var selectedAssets = this.template.querySelector('.searchresulttable').getSelectedRows();
                    for (let aIndex in selectedAssets) {
                        var sAsset = selectedAssets[aIndex];
        
                        var requestDetail = {
                            assetId: sAsset.Id,
                            assetType: sAsset.Asset_Type__c,
                            assetVariant: sAsset.Asset_Variant__c,
                            serialNumber: sAsset.Serial_Number__c,
                            egtMargin: sAsset.EGT_Margin__c,
                            pmaDer: sAsset.PMA_DER__c,
                        };
                        if(sAsset.PMA_DER__c == 'Yes'){
                            requestDetail.pmaDer = true;
                        }
                        else {
                            requestDetail.pmaDer = false;
                        }
                        requestDetails.push(requestDetail);
        
                        let template = templateDict[sAsset.Portal_Asset_Template__c];
                        var exchangeRequestDetail = {
                            assetId: sAsset.Id,
                            assetType: sAsset.Asset_Type__c,
                            assetVariant: sAsset.Asset_Variant__c,
                            serialNumber: sAsset.Serial_Number__c,
                            egtMargin: sAsset.EGT_Margin__c,
                            pmaDer: sAsset.PMA_DER__c,
                            parts: []
                        }
                        if(sAsset.PMA_DER__c == 'Yes'){
                            exchangeRequestDetail.pmaDer = true;
                        }
                        else {
                            exchangeRequestDetail.pmaDer = false;
                        }

                        for (let pIndex in template.partRecordsList) {
                            let partRecord = template.partRecordsList[pIndex];
                            let part = {
                                name: partRecord.partName,
                                serialNumber: partRecord.partName,
                                remainingCycles: partRecord.cyclesRemaining
                            };
                            exchangeRequestDetail.parts.push(part);
                        }
                        exchangeRequestDetails.push(exchangeRequestDetail);
                    }
                }
                
                request.details = requestDetails;
                request.exchangeDetails = exchangeRequestDetails;
            }
            /********** EXCHANGE END **********/

            /********** PURCHASE START **********/
            else {
                let requestDetails = [];

                if (this.isCustomBuild == true) {
                    this.filterList.forEach((filter) => {
                        var requestDetail = {};

                        requestDetail = {
                            assetType: filter.assetType,
                            assetVariant: filter.assetVariant,
                            egtMargin: filter.egtMargin,
                        };

                        let pmader = false;
                        this.dynamicfilterList.forEach((filter) => {
                            if (filter.fieldName == 'PMA_DER__c' && filter.fieldValue == "Yes") {
                                pmader = true;
                            }
                        })

                        if (pmader == true) {
                            requestDetail.pmaDer = true;
                        } else {
                            requestDetail.pmaDer = false;
                        }

                        requestDetails.push(requestDetail);
                    });
                } else {
                    let rows = [];
                    rows = this.template.querySelector('.searchresulttable').getSelectedRows();
                    rows.forEach((row) => {
                        var requestDetail = {
                            assetId: row.Id,
                            assetType: row.Asset_Type__c,
                            assetVariant: row.Asset_Variant__c,
                            serialNumber: row.Serial_Number__c,
                            egtMargin: row.EGT_Margin__c,
                            pmaDer: row.PMA_DER__c,
                        };
                        if(row.PMA_DER__c == 'Yes'){
                            requestDetail.pmaDer = true;
                        }
                        else {
                            requestDetail.pmaDer = false;
                        }
                        requestDetails.push(requestDetail);
                    })
                }
                request.details = requestDetails;
            }
            /********** PURCHASE END **********/
            this.generateSubmitRequest(request);
            if (this.isCustomBuild == false) {
                this.isSubmitButtonDisabled = true;
            }
        }
    }

    //Method for calling generate request Apex Method
    generateSubmitRequest(request) {
        //console.log('generateSubmitRequest request: ' +JSON.stringify(request));
        generateRequest({reqStr: JSON.stringify(request) })
            .then(data =>{
                this.submitSuccessToast('The request has been submitted successfully!');
        })  .catch(error => {
                this.error = error;
                this.inputFailureToast("The request was not saved");
        });
    }

    getCustomBuildPartsData(){
        this.templateIdsList = [];
        this.assetTemplateData.forEach((template) =>
        {
            let templateId = template.Id;
            this.templateIdsList.push(templateId);
        })
        this.getAssetPartsData();
    }

    //Method for showing success message on submit
    submitSuccessToast(message){
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: message,
                variant: 'success',
            }),
        );
    }
}