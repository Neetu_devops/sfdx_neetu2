import { LightningElement, track, api, wire } from "lwc";
import formFactorPropertyName from "@salesforce/client/formFactor";
import getNewsFeed from "@salesforce/apex/NewsArticleLWCClass.getNewsFeed";
import getRedirectUrl from "@salesforce/apex/NewsArticleLWCClass.getRedirectUrl";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getOperatorFilterValue from "@salesforce/apex/NewsArticleLWCClass.getOperatorFilterValue";
import getNamespace from "@salesforce/apex/NewsArticleLWCClass.getNamespace";
import getCategory from "@salesforce/apex/NewsArticleLWCClass.getAviatorCategories";
import getRegion from "@salesforce/apex/NewsArticleLWCClass.getAviatorRegions";

export default class newsFeed extends LightningElement {

  @api namespace;
  @api objectApiName;
  
  // for news
  @api newsSource;
  @api showTitle;
  @api iconSource = "Standard Image";
  @api iconImage = "standard:article";
  @api titleName;
  @api compHeight;
  @api pageSize;
  @api recordId;
  @track news;
  @api showMore;
  @track isProcessing = false;
  @api showDate = false;
  @api disableNext = false;
  @api operatorSearchField;
  // for search
  @track searchKey = "";
  @track startDateValue = "";
  @track endDateValue = "";
  @track startDateOptions;
  @track endDateOptions;

  //pagination
  @api pageNumber = 1;
  @api clickedButtonLabel = false;
  @track currentpage = 1; //this is initialize for 1st page
  @track startingRecord = 1; //start record position per page
  @track endingRecord = 0; //end record position per page
  // @track pagesize; //default value we are assigning
  @track totalRecordsCount = 0; //total record count received from all retrieved records
  @track totalPageCount = 0; //total number of page is needed to display all records

  //for preview
  @api showPreview = false;
  @api previewData;
  @api previewTitle = "News";
  @api previewLinkText = "Link to the news";
  @api previewArtcileUrl;
  @api showArticleLink = false;
  @api redirectUrl;
  //localStorage.setItem('testObject', JSON.stringify());

  @api responseData;
  @api expiresIn;
  @api isFilterOn = false; 
  @api showMissedText = false;
  @api missedText = 0;
  @api regionPicklist;
  @api categoryPicklist;

  @wire(getRegion) 
  getRegionPicklist({data,error}) {
    if(data){ 
        this.regionPicklist = data.map(record => ({ label: record, value: record }));
        this.regionPicklist.unshift({'label':'All Regions','value':''});
        console.log('regionPicklist '+JSON.stringify(this.regionPicklist));
    } else {
        if(error){
          console.log("getRegion error: " +JSON.stringify(error));
          this.showToast("Error", error.message, "error");
        }
    }
  }
  
  @wire(getCategory) 
  getCategoryPicklist({data,error}) {
    if(data){ 
        this.categoryPicklist = data.map(record => ({ label: record, value: record }));
        this.categoryPicklist.unshift({'label':'All Categories','value':''});
        console.log('categoryPicklist '+JSON.stringify(this.regionPicklist));
    } else {
        if(error){
          console.log("categoryPicklist error: " +JSON.stringify(error));
          this.showToast("Error", error.message, "error");
        }
    }
  }

  @track columns = [
    {
      label: "Date",
      initialWidth: 100,
      fieldName: "articleDate",
      type: "date",
      typeAttributes: {
        day: "2-digit",
        month: "short",
        year: "numeric"
      }
    },
    {
      type: "button-icon",
      fixedWidth: 50,
      typeAttributes: {
        iconName: "utility:preview",
        name: "preview_record",
        title: "Preview",
        variant: "border-filled",
        alternativeText: "edit",
        disabled: false
      }
    },
    {
      label: "Article",
      fieldName: "title",
      type: "text"
    }
  ];

  @track columnAviator = [
    {
      label: "Date",
      initialWidth: 100,
      fieldName: "articleDate",
      type: "date",
      typeAttributes: {
        day: "2-digit",
        month: "short",
        year: "numeric"
      }
    },
    {
      type: "button-icon",
      fixedWidth: 50,
      typeAttributes: {
        iconName: "utility:preview",
        name: "preview_record",
        title: "Preview",
        variant: "border-filled",
        alternativeText: "edit",
        disabled: false
      }
    },
    {
      label: "Article",
      fieldName: "article",
      type: "text"
    }
  ];

  @track error;
  @track data;
  @track initialData;
  @track tempData;

  @track customHeight;
  @track customHeightCss;
  @track regionVal = '';
  @track categoriesVal = '';

  @api
  get newsColumns() {
    console.log("newsColumn " + this.newsSource);
    if (this.newsSource == "Aviator") return this.columnAviator;
    else return this.columns;
  }

  connectedCallback() {
    console.log("connectedCallback height " + this.compHeight);

    

    if (
      this.compHeight != null &&
      this.compHeight != undefined &&
      this.compHeight > 0
    ) {
      document.documentElement.style.setProperty(
        "--heightCust",
        this.compHeight + "px"
      );
      this.customHeightCss = "applyHeight";
    } else this.customHeightCss = "defaultHeight";
    console.log("connectedCallback height " + this.customHeightCss);

    if (this.newsSource == "Aviator"){
      this.showDate = true;
    }
    else{
      this.showDate = false;
    }

    this.getNamespaceFromServer();
    this.getOperatorFilter();

    this.loadNewsFeeds(
      this.newsSource,
      this.recordId,
      this.clickedButtonLabel,
      0,
      this.pageSize,
      this.searchKey,
      this.startDateValue,
      this.endDateValue
    );

    console.log(
      "renderedCallback formFactorPropertyName ==> " + formFactorPropertyName
    );
  
  }

  getNamespaceFromServer() {
    getNamespace()
      .then((result) => {
        console.log("getNamespace result- :" + result);
          this.namespace = result;
          this.applyCss();
      })
      .catch((error) => {
        console.log("Error :" + error.body.message);
      });
  }

  applyCss() {
    var namespaceLocal = 'c';
    if(this.namespace != null && this.namespace != '' && this.namespace != undefined) {
      namespaceLocal = this.namespace.replace('__','');
    }
    console.log('final namespace '+ namespaceLocal + ' this.namespace - '+this.namespace );

    const style = document.createElement("style");
    style.innerText = namespaceLocal+`-news-Feed  .slds-scrollable_x {
      overflow-x: hidden !important;
    } `;

    let element = this.template.querySelector("lightning-datatable");
    if (element !== null && element !== undefined) {
      element.appendChild(style);
    }

    const style1 = document.createElement("style");
    style1.innerText = namespaceLocal+`-news-Feed  lightning-calendar {
        position: fixed !important;
        z-index:999 !important;
      } `;

    let element1 = this.template.querySelector("lightning-input");
    if (element1 !== null && element1 !== undefined) {
      element1.appendChild(style1);
    }
  }

  getOperatorFilter() {
    getOperatorFilterValue()
      .then((result) => {
        console.log("getOperatorFilter result- :" + result);
        if(result != null) {
          this.isFilterOn = result;
        }
        else {
          this.isFilterOn = false;
        }
      })
      .catch((error) => {
        console.log("Error :" + error.body.message);
        this.isFilterOn = false;
        this.showToast("Error", error.body, "error");
      });
  }

  loadNewsFeeds() {
    this.isProcessing = true;
    console.log("loadNewsFeeds");
    var pgNumber = 0 ;
    if(this.pageNumber > 0)
      pgNumber = this.pageNumber - 1;
    if(this.newsSource == "Aviator")  
      pgNumber = pgNumber + 1;
    console.log('loadNewsFeeds startDateValue: '+this.startDateValue);  
    console.log('loadNewsFeeds this.pageSize: '+this.pageSize);  
    console.log('loadNewsFeeds this.pgNumber: '+pgNumber);
    console.log('loadNewsFeeds this.operatorSearchField: '+this.operatorSearchField);
    console.log('loadNewsFeeds this.region: === '+this.regionVal);
    console.log('loadNewsFeeds this.category: '+this.categoriesVal);  
    if(this.regionVal && this.regionVal == 'All Regions')
      this.regionVal = '';
    if(this.categoriesVal && this.categoriesVal == 'All Categories')
      this.categoriesVal = '';
      console.log('loadNewsFeeds this.region: '+this.regionVal);
      console.log('loadNewsFeeds this.category: '+this.categoriesVal);  
  
    getNewsFeed({
      newsSource: this.newsSource,
      recordId: this.recordId,
      showMore: this.clickedButtonLabel,
      pageNumber: pgNumber,
      pageSize: this.pageSize,
      searchKeywrd: this.searchKey,
      startDt: this.startDateValue,
      endDt: this.endDateValue,
      fieldName: this.operatorSearchField,
      region:this.regionVal,
      category: this.categoriesVal
    })
      .then((result) => {
        this.initialData = result;
        this.tempData = result;
        //console.log("Incoming Data- :" + result);
        if(result != null && this.initialData.length > 0) {
          //console.log("Incoming Data- :" + JSON.stringify(result));
          this.totalRecordsCount = this.initialData.length;
          console.log("Page size- :" + this.pageSize);
          console.log("size:" + this.pagesize + "--" + this.totalRecordsCount);
          if (this.totalRecordsCount !== 0 && !isNaN(this.totalRecordsCount)) {
            this.totalPageCount = Math.ceil(
              this.totalRecordsCount / this.pagesize
            );
            this.data = this.initialData.slice(0, this.pagesize);
            this.endingRecord = this.pagesize;
          } else {
            this.totalPageCount = 1;
            this.totalRecordsCount = 0;
          }

          console.log("this.initialData[0].totalEntries "+this.initialData[0].totalEntries); 
          console.log("this.initialData[0].isLastPage "+this.initialData[0].isLastPage); 
          if (this.newsSource == "Aviator") {
            var totalPageLoaded = (this.pageNumber)* this.pageSize;
            console.log("totalPageLoaded "+totalPageLoaded); 
            console.log("pageNumber - pageSize "+this.pageNumber +" - "+this.pageSize); 
            if(totalPageLoaded >= this.initialData[0].totalEntries) 
              this.disableNext = true;
            else 
             this.disableNext = false;
          }
          else {
            this.disableNext = this.initialData[0].isLastPage;
          }
          if(this.isFilterOn == true){
            var missedCount = this.initialData[0].articlesNotReturned; 
            console.log('missedCount '+ missedCount);
            if(missedCount > 0) {
              if(missedCount == 1) {
                this.missedText = missedCount +" result that was not an exact match to the company's name has been removed";
              }
              else { 
                this.missedText = missedCount +" results that were not an exact match to the company's name have been removed";
              }
              this.showMissedText = true;
            }
            else {
              this.showMissedText = false;  
            }
          }
          else {
            this.showMissedText = false;
          }
        }
        else{
          this.data = '';
          this.disableNext = true;
          this.showMissedText = false;
        }
        this.isProcessing = false;

      })
      .catch((error) => {
        this.showToast("Error", error.body.message, "error");
        this.error = error;
        this.isProcessing = false;
        console.log("Error :" + error.body.message);
      });
  }

  
  handleKeyChange(event) {
    if (this.searchKey !== event.target.value) {
      this.searchKey = event.target.value;
      this.currentpage = 1;
    }
  }
  handelActionRow(event) {
    this.articleTitle = "";
    this.articleContent = "";
    this.showPreview = true;
    var actionName = event.detail.action.name;
    console.log("handelActionRow: " + actionName);
    var data = event.detail.row;
    console.log("On click row " + JSON.stringify(data));
    if (actionName === "preview_record") {
      if (data != null && data != undefined) {
        if (data.source == "Aviator") {
          this.previewTitle = "Abstract";
          this.previewLinkText = "Link to full content";
        } else if (data.source == "CAPA-News") {
          this.previewTitle = "News Brief";
          this.previewLinkText = "Link to article in CAPA website";
        } else if (data.source == "CAPA-Reports") {
          this.previewTitle = "Report Summary";
          this.previewLinkText = "Link to full report in CAPA website";
        } else {
          this.previewTitle = "News";
          this.previewLinkText = "Link to the news";
        }
        this.previewData = data;
        this.previewArtcileUrl = this.previewData.articleUrl;
        if(this.previewData.articleUrl == null || this.previewData.articleUrl == undefined || this.previewData.articleUrl == '') {
          this.showArticleLink = false;
        }
        else {
          this.showArticleLink = true;
        }
      }
    }
  }

  closeModal(event) {
    this.showPreview = false;
  }

  handleRedirect(event) {
    this.isProcessing = true;
    this.showPreview = false;
    console.log('handleRedirect');
    var data = this.previewData;
    var self = this;
    if(data != null && data.source != null && data.source.includes('CAPA')) {
      getRedirectUrl({
        url: data.articleUrl
      })
        .then((response) => {
          console.log("Response :" + response);
          if(response != null) {
            console.log("Response :" + JSON.stringify(response));
            var statusCode = response.statusCode;
            if(statusCode >= 200 && statusCode < 400) {
              self.responseData = response;
              self.openUrl(response.restResponse + data.articleUrl);
            } else{
              //auto login failed
              self.openUrl(data.articleUrl);
            } 
          }
        })
        .catch((error) => {
          this.isProcessing = false;
          this.showToast("Error", error.body.message, "error");
        });
    }
    else {
      self.openUrl(data.articleUrl);
    }
  }

  openUrl(url){
    console.log('openUrl');
    this.isProcessing = false;
    window.open(url);
  }

  handleClick(event) {
    this.clickedButtonLabel = true;
    this.searchKey = undefined;
    this.startDateValue = undefined;
    this.endDateValue = undefined;
    this.regionVal = '';
    this.categoriesVal = '';
    this.pageNumber = 1;
    this.loadNewsFeeds(
      this.newsSource,
      this.recordId,
      this.clickedButtonLabel,
      this.pageNumber,
      this.pageSize,
      this.searchKey,
      this.startDateValue,
      this.endDateValue
    );
  }

  handleStartDateChange(event) {
    this.startDateValue = event.detail.value;
  }
  handleEndDateChange(event) {
    this.endDateValue = event.detail.value;
  }
  
  handleRegionChange(event) {
    this.regionVal = event.detail.value;
  }
  handleCategoryChange(event) {
    this.categoriesVal = event.detail.value;
  }

  handleSearch() {
    if (
      !this.startDateValue  &&
      !this.endDateValue  &&
      !this.searchKey &&
      (!this.regionVal || this.regionVal == 'All Regions') &&
      (!this.categoriesVal || this.categoriesVal == 'All Categories' )
    ) {
      this.showToast("Error", "Please enter a value to search", "error");
    } else if (
      (!this.startDateValue && this.endDateValue) ||
      (this.startDateValue && !this.endDateValue)
    ) {
      this.showToast("Error", "Please fill From date and To date", "error");
    } else if (
      this.startDateValue &&
      this.endDateValue &&
      this.startDateValue > this.endDateValue
    ) {
      this.showToast("Error", "From Date cannot be before To Date ", "error");
    } else { 
      this.pageNumber = 1;
      this.loadNewsFeeds(
        this.newsSource,
        this.recordId,
        this.clickedButtonLabel,
        this.pageNumber,
        this.pageSize,
        this.searchKey,
        this.startDateValue,
        this.endDateValue
      );
    }
  }

  // getter
  get disablePrevButton() {
    if (this.pageNumber === 1) {
      return true;
    }
    return false;
  }
  // getter
  get disableNextButton() {
    return this.disableNext;
  }

  handleClear() {
    this.startDateValue = undefined;
    this.endDateValue = undefined;
    this.searchKey = undefined;
    this.categoriesVal = '';
    this.regionVal = '';
    this.pageNumber = 1;
    this.loadNewsFeeds(
      this.newsSource,
      this.recordId,
      this.clickedButtonLabel,
      this.pageNumber,
      this.pageSize,
      this.searchKey,
      this.startDateValue,
      this.endDateValue
    ); 
  }

  handlePrevious() {
    console.log("handlePrevious " +this.pageNumber);
    if (this.pageNumber > 1) {
      this.pageNumber = this.pageNumber - 1;
      this.loadNewsFeeds(
        this.newsSource,
        this.recordId,
        this.clickedButtonLabel,
        this.pageNumber,
        this.pageSize
      );
    }
    console.log("current page:" + this.pageNumber);
  }

  handleNext() {
    console.log(
      "Current pageNumber:" + (this.pageNumber + 1) + "--" + this.pageSize
    );
    this.pageNumber = this.pageNumber + 1;
    this.loadNewsFeeds(
      this.newsSource,
      this.recordId,
      this.clickedButtonLabel,
      this.pageNumber,
      this.pageSize
    );

    console.log("Next pageNumber:" + this.pageNumber + "--" + this.pageSize);
  }

  get hasUrl() {
    //console.log("isonSource:" + this.iconSource);
    if (this.iconSource != undefined && this.iconSource === "URL") return true;
    else return false;
  }

  
  showToast(title, message, variant) {
    const event = new ShowToastEvent({
      title: title,
      message: message,
      variant: variant
    });
    this.dispatchEvent(event);
  }

  renderedCallback() {
    console.log("RENDERED CALLBACK CALLED");
  }
}