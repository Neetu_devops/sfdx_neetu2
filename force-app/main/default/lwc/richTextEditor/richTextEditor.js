import { LightningElement, wire, api, track } from 'lwc';
import getFieldData from '@salesforce/apex/RichTextEditorController.getFieldData';
import saveFieldData from '@salesforce/apex/RichTextEditorController.saveFieldData';
import deleteFiles from '@salesforce/apex/RichTextEditorController.deleteFiles';
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class LwcCustomLookup extends LightningElement {
    @api fieldLabel;
    @api fieldAPIName;
    @api viewHeight = '';

    formats = ['font', 'size', 'bold', 'italic', 'underline',
        'strike', 'list', 'indent', 'align', 'link',
        'image', 'clean', 'table', 'header', 'color',
        'background', 'code', 'code-block', 'script', 'blockquote',
        'direction'];

    @api recordId;
    @api fieldData;
    @track viewMode = true;
    @track isProcessing = false;
    @track fieldLength;
    @track fieldValue;
    filesToDelete = new Set();
    filesInserted = new Set();
    styleApplied = false;

    @wire(getFieldData) fieldData;

    connectedCallback() {
        this.loadFieldData(this.recordId, this.fieldAPIName);
    }

    loadFieldData() {
        this.isProcessing = true;
        getFieldData({ recordId: this.recordId, fieldAPIName: this.fieldAPIName })
            .then((result) => {
                this.fieldData = result;
                this.fieldValue = result.fieldValue;
                this.fieldLength = result.fieldLength;
                this.isProcessing = false;
            })
            .catch((error) => {
                console.log("Error :", error.body.message);
                this.showToast("Error", error.body.message, "error");
                this.isProcessing = false;
            });
    }

    editFieldValue() {
        this.viewMode = false;
    }

    onValueChange(event) {
        let oldValue = this.fieldValue;
        let newValue = event.target.value;
        let filesIds = new Set();
        let filesInserted = new Set();

        if (this.filesInserted.length > 0) { this.filesInserted.forEach(e => { filesInserted.add(e) }); }
        if (this.filesToDelete.length > 0) { this.filesToDelete.forEach(e => { filesIds.add(e) }); }
        if (oldValue != undefined && oldValue.includes('<img ')) {
            let oldIds = oldValue.match(/<img(.*?)>/g).map(function (val) {
                if (val.includes('/version/download/')) {
                    return val.substring(val.indexOf('/version/download/') + 18, val.indexOf('?'));
                }
            });
            oldIds.forEach(e => { filesIds.add(e); });
        }

        if (newValue != undefined && newValue.includes('<img ')) {
            let newIds = newValue.match(/<img(.*?)>/g).map(function (val) {
                if (val.includes('/version/download/')) {
                    return val.substring(val.indexOf('/version/download/') + 18, val.indexOf('?'));
                }
            });
            newIds.forEach(e => {
                if (filesIds.has(e)) {
                    filesIds.delete(e);
                }
                else { filesInserted.add(e); }
            });
        }
        this.filesToDelete = [...filesIds];
        this.filesInserted = [...filesInserted];
        this.fieldValue = event.target.value;
    }

    saveFieldValue() {
        this.isProcessing = true;
        if (this.fieldValue.length > this.fieldLength) {
            this.showToast("Error", 'The content exceeds the max-size(' + this.fieldLength + ') of the field. (Content-Size:' + this.fieldValue.length + ')', "error");
            this.isProcessing = false;
        }
        else {
            saveFieldData({ recordId: this.recordId, fieldAPIName: this.fieldAPIName, fieldValue: this.fieldValue, filesToDelete: JSON.stringify(this.filesToDelete) })
                .then((result) => {
                    this.fieldData = result;
                    this.fieldValue = result.fieldValue;
                    this.fieldLength = result.fieldLength;
                    this.viewMode = true;
                    this.filesInserted = new Set();
                    this.filesToDelete = new Set();
                    this.isProcessing = false;
                })
                .catch((error) => {
                    console.log("Error :", error.body.message);
                    this.showToast("Error", error.body.message, "error");
                    this.isProcessing = false;
                });
        }
    }

    resetFieldValue() {
        this.viewMode = true;
        this.fieldValue = this.fieldData.recordData[this.fieldAPIName];
        if (this.filesInserted.length > 0) {
            deleteFiles({ filesToDelete: JSON.stringify(this.filesInserted) })
                .then((result) => {
                    this.filesInserted = new Set();
                    this.filesToDelete = new Set();
                    this.isProcessing = false;
                })
                .catch((error) => {
                    this.showToast("Error", error.body.message, "error");
                    this.isProcessing = false;
                });
        }
    }

    showToast(title, message, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }

    renderedCallback() {
        if (this.viewMode) {
            var divblock = this.template.querySelector('[data-id="viewDiv"]');
            if (divblock) {
                if (this.viewHeight == undefined || this.viewHeight == '') {
                    this.template.querySelector('[data-id="viewDiv"]').style.height = '400px';
                }
                else {
                    this.template.querySelector('[data-id="viewDiv"]').style.height = this.viewHeight;
                }
            }
        }
        else {
            if (!this.styleApplied) {
                const style = document.createElement('style');
                if (this.viewHeight == undefined || this.viewHeight == '') {
                    style.innerText = `c-rich-text-editor .slds-rich-text-area__content { min-height:400px; }`;
                }
                else {
                    style.innerText = `c-rich-text-editor .slds-rich-text-area__content { min-height:` + this.viewHeight + ` !important; }`;
                }
                let element = this.template.querySelector('[data-id="manualStyle"]');
                if (element !== null && element !== undefined) {
                    element.appendChild(style);
                }
                this.styleApplied = true;
            }
            let element = this.template.querySelector('[data-id="richtext"]');
            if (element !== null && element !== undefined) {
                element.focus();
            }
        }
    }
}