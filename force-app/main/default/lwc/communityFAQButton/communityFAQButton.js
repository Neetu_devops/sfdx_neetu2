import { LightningElement, api, wire, track } from "lwc";
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';

export default class CommunityFAQButton extends NavigationMixin(LightningElement) {

    @api downloadURL;
    @api downloadLabel;

    handleOpenLink() {
        window.open(this.downloadURL, '_bottom');
    }
}