/* eslint no-console: */
import { LightningElement, wire, api, track } from 'lwc';
import loadJournalEntryData from '@salesforce/apex/JournalListController.loadJournalEntryData';
import loadFilterValues from '@salesforce/apex/JournalListController.loadFilterValues';
import post from '@salesforce/apex/JournalListController.Post';
import exportToExcel from '@salesforce/apex/JournalListController.exportToExcel';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
//import { refreshApex } from '@salesforce/apex';
__
export default class JournalList extends NavigationMixin(LightningElement) {
    @api recordId;
    @track isDetailMode = false;
    @track columns = [];
    @track dataList = [];
    @track initData;
    @track disableExportButton = true;
    @track disableSendJournal = false;

    @track isTableLoaded = false;
    @track isProcessing = false;

    /* Properties for filters */
    startDateValue;
    endDateValue;
    listViewId;
    dataMap= new Map(); // this map holds the other optional filters.
    

    //options
    @track startDateOptions;
    @track endDateOptions;
    @track statusOptions;
    @track journalSourceOptions;
    

    // initialize component
    connectedCallback() {

        // check the access mode, whether its a detail page or list view.
        if(this.recordId == undefined || this.recordId == null){

            this.isProcessing = true;

            loadFilterValues()
            .then(result => {

                let parsedData = JSON.parse(result);
                this.startDateOptions = parsedData.startDateOptions;
                this.endDateOptions = parsedData.endDateOptions;
                this.statusOptions = parsedData.statusOptions;
                this.journalSourceOptions = parsedData.journalSourceOptions;
                
                if(parsedData.listViewId !== undefined){
                    this.listViewId = parsedData.listViewId;
                }
                this.isProcessing = false;
            })
            .catch(error => {
                this.error = error;
                this.showToast('Error',error.body.message,'error');
                this.isProcessing = false;
            }); 
        }else{

            this.isDetailMode = true;
            this.loadJournalEntryData();
        }
    }

    loadJournalEntryData(){
        this.isProcessing = true;

        //prepare filter wrapper if its list view.
        var filters = []; 
        if(this.startDateValue != undefined && this.dataMap.size>0){

            for (let key of this.dataMap.keys()) {
                var val = this.dataMap.get(key);
                if( val != undefined && val != ""){
                    filters.push({fieldApiName : key, value : val});
                }
            }
        }

        loadJournalEntryData({
            startAccountingPeriodId : this.startDateValue == undefined ? this.recordId : this.startDateValue,
            endAccountingPeriodId : this.endDateValue,
            optionalFilters : JSON.stringify(filters)
        })
        .then(result => {
            this.initData = result;
            let parsedData = JSON.parse(result);
            this.disableSendJournal = parsedData.disableSendJournal;
            
            let dataWrapList = parsedData.dataWrapList;
            //console.log('Length: ', dataWrapList.length);
            if(dataWrapList.length === 1){
                this.disableExportButton = true;
                this.disableSendJournal = true;
                //console.log(this.isTableLoaded);
                this.isTableLoaded = false;
                this.isProcessing = false;
                this.showToast('Error','No records found','error');
                return;
            }
            //console.log('Disable Button Value: ',this.disableExportButton);
            this.disableExportButton = false;
            let columnList = parsedData.dataTableColumns;

            for(let dataWrap of dataWrapList){
                if(dataWrap !== undefined){
                    if(dataWrap.jdNameAndValue !== undefined && Object.keys(dataWrap.jdNameAndValue).length !== 0){
                        for(let key of Object.keys(dataWrap.jdNameAndValue)){
                            dataWrap[key] = dataWrap.jdNameAndValue[key];
                            let headerObj = {};
                            let alreadyExist = false;

                            headerObj.type = 'text';
                            headerObj.fieldName = key;
                            headerObj.label = key.toUpperCase();
                            
                            /* Iterating over columns */
                            for(let column of columnList){

                                /* Adjusting width of columns */
                                column.initialWidth = 100;

                                if(column.label === headerObj.label){
                                    alreadyExist = true;
                                }
                                if(column.type === 'url' && dataWrap.typeAttributesMap !== undefined){
                                    let tA = {};
                                    let tempTA = {};
                                    if(column.label === 'INVOICE/PAYMENTS'){
                                        tempTA.fieldName = dataWrap.typeAttributesMap.invoice.fieldName;
                                        tA.label = tempTA;
                                        tA.target = dataWrap.typeAttributesMap.invoice.target;
                                        column.typeAttributes = tA;
                                    }
                                    if(column.label === 'LESSOR'){
                                        tempTA.fieldName = dataWrap.typeAttributesMap.lessor.fieldName;
                                        tA.label = tempTA;
                                        tA.target = dataWrap.typeAttributesMap.lessor.target;
                                        column.typeAttributes = tA;
                                    }
                                    if(column.label === 'BANK ACCOUNT'){
                                        tempTA.fieldName = dataWrap.typeAttributesMap.bankAccount.fieldName;
                                        tA.label = tempTA;
                                        tA.target = dataWrap.typeAttributesMap.bankAccount.target;
                                        column.typeAttributes = tA;
                                    }
                                    if(column.label === 'GL ACCOUNT DESCRIPTION'){
                                        tempTA.fieldName = dataWrap.typeAttributesMap.journalEntry.fieldName;
                                        tA.label = tempTA;
                                        tA.target = dataWrap.typeAttributesMap.journalEntry.target;
                                        column.typeAttributes = tA;
                                    }
                                }
								if(column.label === 'CREDIT' || column.label === 'DEBIT'){
									column.cellAttributes =  {alignment : 'left'};
								}
                            }
                            if(!alreadyExist){
                                columnList.push(headerObj);
                            }
                        }  
                    } else{ //When Journal_Entry__c has no Journal_Dimension__c child records
                        for(let column of columnList){

                            /* Adjusting width of columns */
                            column.initialWidth = 100;

                            if(column.type === 'url' && dataWrap.typeAttributesMap !== undefined && Object.keys(dataWrap.typeAttributesMap).length !== 0){
                                let tA = {};
                                let tempTA = {};
                                if(column.label === 'INVOICE/PAYMENTS'){
                                    tempTA.fieldName = dataWrap.typeAttributesMap.invoice.fieldName;
                                    tA.label = tempTA;
                                    tA.target = dataWrap.typeAttributesMap.invoice.target;
                                    column.typeAttributes = tA;
                                }
                                if(column.label === 'LESSOR'){
                                    tempTA.fieldName = dataWrap.typeAttributesMap.lessor.fieldName;
                                    tA.label = tempTA;
                                    tA.target = dataWrap.typeAttributesMap.lessor.target;
                                    column.typeAttributes = tA;
                                }
                                if(column.label === 'BANK ACCOUNT'){
                                    tempTA.fieldName = dataWrap.typeAttributesMap.bankAccount.fieldName;
                                    tA.label = tempTA;
                                    tA.target = dataWrap.typeAttributesMap.bankAccount.target;
                                    column.typeAttributes = tA;
                                }
                                if(column.label === 'GL ACCOUNT DESCRIPTION'){
                                    tempTA.fieldName = dataWrap.typeAttributesMap.journalEntry.fieldName;
                                    tA.label = tempTA;
                                    tA.target = dataWrap.typeAttributesMap.journalEntry.target;
                                    column.typeAttributes = tA;
                                }
                            }
							if(column.label === 'CREDIT' || column.label === 'DEBIT'){
                                column.cellAttributes =  {alignment : 'left'};
                            }
                        }
                    }
                    if(dataWrap.journalEntryFieldNameAndValue !== undefined){
                        for(let key of Object.keys(dataWrap.journalEntryFieldNameAndValue)){
                            dataWrap[key] = dataWrap.journalEntryFieldNameAndValue[key];
                        }
                    }
                }
            }

            this.dataList = dataWrapList;
            this.columns = columnList;
            //console.log('COLUMNS: ',this.columns);
            this.isTableLoaded = true;
            this.isProcessing = false;
        })
        .catch(error => {
            this.disableExportButton = true;
            this.disableSendJournal = true;
            this.showToast('Error',error.body.message,'error');
            this.isProcessing = false;
            this.isTableLoaded = false;
        });
    }

    handleStartDateChange(event){
        this.startDateValue = event.detail.value;
    }
    handleEndDateChange(event){
        this.endDateValue = event.detail.value;
    }

    //this is common handler for optional fields present on the UI except Lookup.
    handleChangeEvent(event){
        var value = event.detail.value;
        var fieldName = event.currentTarget.dataset.fieldname;
        this.dataMap.set(fieldName, value);
        console.log(this.dataMap);
    }

    handleSearch(){
        if(this.startDateValue === undefined || this.endDateValue === undefined){
            this.showToast('Error','Please fill start date and end date','error');    
        }else{
            this.loadJournalEntryData();
        }
    }

    handleClear(){
        this.disableExportButton = true;
        this.isTableLoaded = false;
        this.startDateValue = undefined;
        this.endDateValue = undefined;
        this.dataMap.clear();
        console.log(this.dataMap);
    }



    handleSendJournals(){
        this.isProcessing = true;
        post({
            jeWrapStr : this.initData
        })
        .then(result => {
            //console.log('result', result);
            let parsedResult = JSON.parse(result);
            if(parsedResult.isSuccess === true){
                this.showToast('Success','Successfully sent Journals to ' + parsedResult.userEmail,'success');
                this.disableSendJournal = true;
            } else{
                this.showToast('Error','Error sending Journals. Please contact your administrator.','error');
            }
            this.error = undefined;
            this.isProcessing = false;
            //refreshApex(this.wiredActivities);
        })
        .catch(error => {
            this.error = error;
            this.result = undefined;
            this.isProcessing = false;
            this.showToast('Error',error.body.message,'error');
        });
    }
        
    handleExportToExcel(){
        this.isProcessing = true;
        exportToExcel({
            jeWrapStr : this.initData
        })
        .then(result => {
                this[NavigationMixin.Navigate]({
                    type: 'standard__objectPage',
                    attributes: {
                        objectApiName: 'ContentDocument',
                        actionName: 'home'
                    }
                });
            this.error = undefined;
            this.isProcessing = false;
        })
        .catch(error => {
            console.log('error: ',error);
            this.error = error;
            this.result = undefined;
            this.isProcessing = false;
        });
    }

    handleCancel(){
    
    	if(this.recordId != undefined){
        
        	this[NavigationMixin.Navigate]({
        
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.recordId,
                    objectApiName: 'Calendar_Period__c',
                    actionName: 'view'
                },
            });
        
        }else{
        
        	this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'Calendar_Period__c',
                    actionName: 'list'
                },
                state: {
                    filterName: this.listViewId !== undefined ? this.listViewId : 'Recent'
                },
        	});
        
        }
        
    }
    showToast(title,message,variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }
    
    renderedCallback(){
        console.log('RENDERED CALLBACK CALLED');

        const style = document.createElement('style');
        style.innerText = `c-journal-list .slds-table_bordered tbody td {
            box-shadow: none !important;
        }
        c-journal-list .slds-table_bordered tbody th {
            box-shadow: none !important;
        }
        c-journal-list .slds-has-focus .slds-th__action {
            box-shadow: none !important;
        }
        c-journal-list .slds-has-focus.slds-is-resizable .slds-th__action {
            box-shadow: none !important;
        }
        c-journal-list .slds-has-focus.slds-is-resizable .slds-th__action, .slds-has-focus.slds-is-resizable .slds-th__action:focus, .slds-has-focus.slds-is-resizable .slds-th__action:hover, .slds-has-focus.slds-is-resizable .slds-th__action:focus:hover, .slds-is-resizable .slds-th__action:focus, .slds-is-resizable .slds-th__action:focus:hover {
            box-shadow: none !important;
        }
        c-journal-list .slds-table th:focus, .slds-table th.slds-has-focus, .slds-table [role=gridcell]:focus, .slds-table [role=gridcell].slds-has-focus{
            box-shadow: none !important;
        }`;
        
        let element = this.template.querySelector('lightning-datatable');
        if(element !== null && element !== undefined){
            element.appendChild(style);
        }
 
    }

    //this is the common handler for all the 3 lookups.
    lookupEventHandler(event) {

        var selectRecId = event.detail.currentRecId;
        var objectName = event.detail.objectName;

        if(objectName =='Operator__c'){
            this.dataMap.set('Lease__r.Lessee__c', selectRecId);
        }else if(objectName == 'Counterparty__c'){
            this.dataMap.set('Lease__r.Lessor__c', selectRecId);
        }else{
            //lease
            this.dataMap.set(objectName, selectRecId);
        }

        console.log(this.dataMap);

    }

    

}