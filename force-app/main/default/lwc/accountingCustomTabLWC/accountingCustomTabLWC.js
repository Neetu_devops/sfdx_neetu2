/*eslint no-console:*/
import { LightningElement, wire, api, track  } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { CurrentPageReference } from 'lightning/navigation';
import accountingSetupData from '@salesforce/apex/AccountingCustomTabLWCController.getData';

export default class AccountingCustomTabLWC extends NavigationMixin(LightningElement) {
    @api result;
    @wire(accountingSetupData)
    wiredAccount({ error, data }) {
        if (data) {
            console.log('data: ', data);
            this.result = data;
        }
    }

    @wire(CurrentPageReference)
    pageRef;

    accountingSetup() {
        if (this.result.loadRecordPage) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.result.accountingSetupId,
                    actionName: 'view'
                },
            })
        }
        else {
            let pageReference = {
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: this.result.namespace + 'Parent_Accounting_Setup__c',
                    actionName: 'home'
                }
            }
            this[NavigationMixin.Navigate](pageReference);
        }
    }

    legalEntries() {
        let pageReference = {
            type: 'standard__objectPage',
            attributes: {
                objectApiName: this.result.namespace + 'Legal_Entity__c',
                actionName: 'home'
            }
        }
        this[NavigationMixin.Navigate](pageReference);
    }

    chartOfAccounts() {
        let pageReference = {
            type: 'standard__objectPage',
            attributes: {
                objectApiName: this.result.namespace + 'Chart_Of_Accounts__c',
                actionName: 'home'
            }
        }
        this[NavigationMixin.Navigate](pageReference);
    }

    accountingPeriod() {
        let pageReference = {
            type: 'standard__objectPage',
            attributes: {
                objectApiName: this.result.namespace + 'Calendar_Period__c',
                actionName: 'home'
            }
        }
        this[NavigationMixin.Navigate](pageReference);
    }

    dimensions() {
        let pageReference = {
            type: 'standard__objectPage',
            attributes: {
                objectApiName: this.result.namespace + 'Dimension__c',
                actionName: 'home'
            }
        }
        this[NavigationMixin.Navigate](pageReference);
    }
}