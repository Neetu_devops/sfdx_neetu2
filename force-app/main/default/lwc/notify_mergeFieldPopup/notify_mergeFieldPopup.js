import { LightningElement, track, api, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

export default class Notify_mergeFieldPopup extends LightningElement {
    @api objectApiName;
    fieldName = '';
    mergeFieldText;
    disableHyperlink = true;
    addHyperlink = false;

    @wire(getObjectInfo, { objectApiName: '$objectApiName' }) selectedObjectInfo;

    get fieldOptions() {
        this.isProcessing = true;
        let fieldList = [];
        if (this.selectedObjectInfo && this.selectedObjectInfo.data && this.selectedObjectInfo.data.fields) {
            for (let key in Object.entries(this.selectedObjectInfo.data.fields)) {
                let fieldObj = Object.entries(this.selectedObjectInfo.data.fields)[key][1];
                fieldList.push({ label: fieldObj.label, value: fieldObj.apiName.toLowerCase(), dataType: fieldObj.dataType });
            }
        }
        this.isProcessing = false;
        return fieldList.sort((a, b) => (a.label > b.label) ? 1 : -1);
    }

    handleMergeField(event) {
        this.disableHyperlink = false;
        this.fieldName = event.target.value;
        let fieldOptionList = this.fieldOptions;
        let selectedField = fieldOptionList.find(field => field.value === event.target.value);
        let value1 = selectedField.value;
        if(selectedField.value== 'createdbyid'){ value1 = 'createdby.name';}
        if(selectedField.value== 'lastmodifiedbyid'){ value1 = 'lastModifiedby.name';}
        if (this.addHyperlink) {
            this.mergeFieldText = '<a href="{{' + (selectedField.dataType == 'Reference' ? selectedField.value : 'id') + '}}" target="blank">{{' + (selectedField.dataType == 'Reference' ? this.replaceRefFieldString(selectedField.value,'c', 'r.name').replace('byid','by.name'): value1) + '}}</a>';
        }
        else { this.mergeFieldText = '{{' + (selectedField.dataType == 'Reference' ? this.replaceRefFieldString(selectedField.value,'c', 'r.name').replace('byid','by.name') : value1) + '}}'; }
    }

    handleAddHyperlink(event) {
        console.log('handleHyperlink');
        let fieldOptionList = this.fieldOptions;
        let selectedField = fieldOptionList.find(field => field.value === this.fieldName);
        let value1 = selectedField.value;
        if(selectedField.value== 'createdbyid'){ value1 = 'createdby.name';}
        if(selectedField.value== 'lastmodifiedbyid'){ value1 = 'lastModifiedby.name';}
        if (event.target.checked) {
            console.log('selectedField.dataType=---'+selectedField.dataType);
            this.mergeFieldText = '<a href="{{' + (selectedField.dataType == 'Reference' ? selectedField.value : 'id') + '}}" target="blank">{{' + (selectedField.dataType == 'Reference' ? this.replaceRefFieldString(selectedField.value,'c', 'r.name').replace('byid','by.name') : value1) + '}}</a>';
        }
        else { this.mergeFieldText = '{{' + (selectedField.dataType == 'Reference' ? this.replaceRefFieldString(selectedField.value,'c', 'r.name').replace('byid','by.name') : value1) + '}}'; }
        console.log(' this.mergeFieldText----'+  this.mergeFieldText);
        this.addHyperlink = event.target.checked;
    }

    handleCopyToClipboard(event) {
        var selectedMergeField = this.template.querySelector('[data-id="mergeField"]');
        if (selectedMergeField) {
            const el = document.createElement('textarea');
            el.value = selectedMergeField.value;
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
            this.closeModal();
        }
    }

    closeModal() {
        this.disableHyperlink = true;
        const selectedEvent = new CustomEvent('closemodal');
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    replaceRefFieldString(source,findStr,replaceWith){
        
        let parts = source.split('__');
        console.log('parts---'+parts[0] + '----'+parts.length);
        if(parts.length==1)return source;
        if(parts.length==2) return parts[0] +'__'+ parts[1].replace(findStr,replaceWith);
        if(parts.length==3) return parts[0] + '__' +parts[1]+'__'+ parts[2].replace(findStr,replaceWith);
    }

    // addMergeField() {
    //     const selectedEvent = new CustomEvent('addmergefield', { detail: { fieldName: this.fieldName } });
    //     // Dispatches the event.
    //     this.dispatchEvent(selectedEvent);
    // }
}