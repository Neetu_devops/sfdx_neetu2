import { LightningElement, api, wire, track } from "lwc";
import BOOLEAN_YES from '@salesforce/label/c.ModuleStoreBooleanYesLabel';
import BOOLEAN_NO from '@salesforce/label/c.ModuleStoreBooleanNoLabel';
import BOOLEAN_BOTH from '@salesforce/label/c.ModuleStoreBooleanBothLabel';

export default class CommunityModuleFilter extends LightningElement {

    @api filterField;
    @api isCompSign;

    filterInput;
    inputFilter = 'equals';
    
    //Method for tracking the inputs in the input fields
    handleFilterInput(event) {
        if (event.target.name === 'compsign') {
            this.inputFilter = event.target.value;
        } else if (event.target.name == this.filterField.fieldName) {
            this.filterInput = event.target.value;
        }
    }

    //Method determines if a combobox or standard input field is supposed shown
    get isPicklist() {
        if (this.filterField.fieldType == 'boolean' || this.filterField.fieldType == 'picklist') {
           return true
        } else {
            return false
        }
    }
    
    //Method determines if an additional combobox for comparison sign should appear
    get isDateOrNumber() {
        if (this.filterField.fieldType == 'date' || this.filterField.fieldType == 'number') {
            this.inputFilter = 'greater';
           return true
        } else {
            return false
        }
    }

    //Method assigns picklist option based on the field type
    get picklistOptions() {
        if (this.isPicklist == true) {
            if (this.filterField.picklistValues != null) {
                let options = [];
                this.filterField.picklistValues.forEach(pickVal => {
                   options.push({
                     label: pickVal.picklistFieldLabel,
                     value: pickVal.picklistFieldValue,
               })
            });
            return options;
            } else {
                return this.booleanOptions
            }
        }
    }

    //Options for Boolean fields including Custom Labels
    booleanOptions =[{
        value: "Yes",
        label: BOOLEAN_YES,
    }, {
        value: "No",
        label: BOOLEAN_NO,
    },
    {
        value: "Both",
        label: BOOLEAN_BOTH,
    }];

    //Options for Comparison Sign combobox
    filterOptions =[{
        value: "greater",
        label: ">=",
    }, {
        value: "equals",
        label: "=",
    },
    {
        value: "lesser",
        label: "<=",
    }];

    //Method for retrieving filter inputs and returning them to parent component
    @api returnFiltersData() {

        let filterField = {
            fieldName: this.filterField.fieldName,
            fieldLabel: this.filterField.fieldLabel,
            fieldType: this.filterField.fieldType,
            fieldValue: this.filterInput,
            comparisonSign: this.inputFilter,
        }

        return JSON.stringify(filterField);
    }
}