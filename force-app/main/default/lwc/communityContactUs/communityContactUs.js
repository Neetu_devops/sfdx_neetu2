import { LightningElement, api, wire, track } from "lwc";
import getContactUsInfo from '@salesforce/apex/CommunityContactUs.getContactUsInfo';

export default class CommunityContactUs extends LightningElement {
    @track userData;
    @api department;
    @api mailsubject;

    connectedCallback() {
        this.getContactUsEmail();
    }

    getContactUsEmail() {
        this.userData = 'mailto:';
        getContactUsInfo({ department: this.department,mailSubject: this.mailsubject})
            .then(data => {
                console.log('getContactUsEmail data: ',data);
                if(data != null) {
                    this.userData = 'mailto:' + data;
                }
        }).catch(error => {
            this.error = error;
            console.log('getContactUsEmail error ', error);
        });
    }

}