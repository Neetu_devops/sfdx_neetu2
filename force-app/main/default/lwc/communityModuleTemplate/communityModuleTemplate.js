import { LightningElement, api, wire, track } from "lwc";

export default class CommunityModuleTemplate extends LightningElement {

    @api portalTemplateRecord;
    @api isCustom;

    @track inputLimiterRange;
    @track inputEGTMargin;

    connectedCallback() {
        this.inputLimiterRange = 0;
        if(this.portalTemplateRecord.EGT_Marting_applicable__c == true)
            this.inputEGTMargin = 0;
    } 

    //Method for tracking the inputs in the input fields
    handlePartInput(event) {
        if (event.target.name === 'limrange') {
            this.inputLimiterRange = event.target.value;
        } else if (event.target.name === 'egtmargin') {
            this.inputEGTMargin = event.target.value;
        }
    }

    //Method for dynamically making input field eligible for validation if it is displayed
    get egtClass() {
        if (this.portalTemplateRecord.EGT_Marting_applicable__c == true) {
           return "filtervalidation"
        } else {
            return null
        }
    }
    
    limFieldName;

    get limrangeLabel() {
        if (this.portalTemplateRecord.Remaining_Cycles_applicable__c == true) {
            this.limFieldName = 'Remaining_Cycles__c';
            return 'Limiter Minimum (Cycles)'
        } else if (this.portalTemplateRecord.Remaining_Days_applicable__c == true) {
            this.limFieldName = 'Remaining_Days__c';
            return 'Limiter Minimum (Days)'
        } else if (this.portalTemplateRecord.Remaining_Hours_applicable__c == true) {
            this.limFieldName = 'Remaining_Hours__c';
            return 'Limiter Minimum (Hours)'
        } else {
            this.limFieldName = 'Remaining_Cycles__c';
            return 'Limiter Minimum'
        }
    }

    //Method for validating inputs
    validateInputs(){
        const isInputsCorrect = [...this.template.querySelectorAll('.filtervalidation')]
        .reduce((validSoFar, inputField) => {
            inputField.reportValidity();
            return validSoFar && inputField.checkValidity();
        }, true)

        return isInputsCorrect;
    }
    
    //Method for validating inputs from the parent component
    @api checkTemplateValidity() {
        const isInputsValidated = this.validateInputs();
        return isInputsValidated;
    }

    //Method for retrieving input data from the parent component
    @api returnFiltersData() {
        console.log(' returnFiltersData: inputLimiterRange: '+this.inputLimiterRange + ' , this.inputEGTMargin: '+this.inputEGTMargin);
        let filtersData = {
            templateId: this.portalTemplateRecord.Id,
            limiterRange: this.inputLimiterRange,
            limiterField: this.limFieldName,
            egtMargin: this.inputEGTMargin,
        }

        if(this.isCustom == true) {
            filtersData.assetType = this.portalTemplateRecord.Asset_Type__c;
            filtersData.assetVariant = this.portalTemplateRecord.Asset_Variant__c;
        }

        return JSON.stringify(filtersData);
    }

}