import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import search from '@salesforce/apex/Notify_LookupUtilityController.search';

export default class multiSelectUtilityParent extends LightningElement {
    // Use alerts instead of toasts (LEX only) to notify user
    @api notifyViaAlerts = false;
    @api lookupError = 'An error occured while searching the data for the Object.';
    @api requiredSelectionError = 'Please complete this field.';
    @api maxSelectionError = 'You have selected max record.';
    @api searchError = 'An error occured while searching the data for the Object.';
    @api required = false;
    @api recordCount = '5';
    @api placeholder = 'Search User';
    @api label;
    isMultiRecords = true;
    isMultiEntry = true;
    @api maxSelectionSize;
    @api initialSelection = [];
    errors = [];
    recentlyViewed = [];
    @api objectList = [];
    /**
     * Loads recently viewed records and set them as default lookpup search results (optional)
     */

    connectedCallback() {
        this.initLookupDefaultResults();
    }

    /**
     * Initializes the lookup default results with a list of recently viewed records (optional)
     */
    initLookupDefaultResults() {
        // Make sure that the lookup is present and if so, set its default results
        const lookup = this.template.querySelector('c-notify_lookup-utility-child');
        if (lookup) {
            lookup.setDefaultResults(this.recentlyViewed);
        }
    }

    /**
     * Handles the lookup search event.
     * Calls the server to perform the search and returns the resuls to the lookup.
     * @param {event} event `search` event emmitted by the lookup
     */
    handleLookupSearch(event) {
        // Call Apex endpoint to search for records and pass results to the lookup
        search(event.detail)
            .then((results) => {
                this.template.querySelector('c-notify_lookup-utility-child').setSearchResults(results);
            })
            .catch((error) => {
                console.log(error);
                this.notifyUser('Lookup Error', this.lookupError, 'error');
                // eslint-disable-next-line no-console
                this.errors = [error];
            });
    }

    /**
     * Handles the lookup selection change
     * @param {event} event `selectionchange` event emmitted by the lookup.
     * The event contains the list of selected ids.
     */
    // eslint-disable-next-line no-unused-vars
    handleLookupSelectionChange(event) {
        this.checkForErrors();
        this.dispatchEvent(new CustomEvent('selectedrecords', { detail: event.detail }));
    }

    // All functions below are part of the sample app form (not required by the lookup).

    handleLookupTypeChange(event) {
        this.initialSelection = [];
        this.errors = [];
        this.isMultiEntry = event.target.checked;
    }

    handleMaxSelectionSizeChange(event) {
        this.maxSelectionSize = event.target.value;
    }

    handleClear() {
        this.initialSelection = [];
        this.errors = [];
    }

    checkForErrors() {
        this.errors = [];
        const selection = this.template.querySelector('c-notify_lookup-utility-child').getSelection();
        // Custom validation rule
        if (this.isMultiEntry && selection.length > this.maxSelectionSize) {
            this.errors.push({ message: this.maxSelectionError + '(Max Selection: ' + this.maxSelectionSize + ')' });
        }
        // Enforcing required field
        if (selection.length === 0) {
            this.errors.push({ message: this.requiredSelectionError });
        }
    }

    notifyUser(title, message, variant) {
        if (this.notifyViaAlerts) {
            // Notify via alert
            // eslint-disable-next-line no-alert
            alert(`${title}\n${message}`);
        } else {
            // Notify via toast (only works in LEX)
            const toastEvent = new ShowToastEvent({ title, message, variant });
            this.dispatchEvent(toastEvent);
        }
    }

    notifyUserFromChild(event) {
        this.notifyUser(event.detail.title, event.detail.message, event.detail.variant);
    }

    updatePlaceholder(event) {
        this.placeholder = 'Search ' + event.detail.placeholder;
        if (!event.detail.multiEntryRecords) {
            this.handleClear();
        }
    }

    handleSearch(event) {
        // Call Apex endpoint to search for records and pass results to the lookup
        search(event.detail)
            .then((results) => {
                this.template.querySelector('c-notify_lookup-utility-child').setSearchResults(results);
            })
            .catch((error) => {
                console.log(error);
                this.notifyUser('Lookup Error', this.searchError, 'error');
                // eslint-disable-next-line no-console
                console.error('Lookup error', JSON.stringify(error));
            });
    }
}