import { LightningElement,wire,api,track } from 'lwc';
import getResults from '@salesforce/apex/CustomLookUpController.getResults';

export default class LwcCustomLookup extends LightningElement {
    @api objectName = 'Account';
    @api fieldName = 'Name';
    @api selectRecordId = '';
    @api selectRecordName;
    @api whereClause = '';
    @api fieldLabel;
    @api searchRecords = [];
    @api required = false;
    @api iconName = 'standard:account'
    @track messageFlag = false;
    @track recordSelected = false;
    @track showSpinner = false;
   
 
    searchField(event) {
        var currentText = event.target.value;
        this.showSpinner = true;

        getResults({ ObjectName: this.objectName, fieldName: this.fieldName, value: currentText, whereClause : this.whereClause  })
        .then(result => {
            this.searchRecords= result;
            this.showSpinner = false;

            if(currentText.length > 0 && result.length == 0) {
                this.messageFlag = true;
            }
            else {
                this.messageFlag = false;
            }

            this.template.querySelector('[data-id="lookupContainer"]').classList.remove('slds-is-close');
            this.template.querySelector('[data-id="lookupContainer"]').classList.add('slds-is-open');
        })
        .catch(error => {
            console.log('-------error-------------'+error);
            console.log(error);
        });
        
    }
    
   setSelectedRecord(event) {

        this.closeResultSection(event);

        var currentRecId = event.currentTarget.dataset.id;
        var selectName = event.currentTarget.dataset.name;
       
        this.recordSelected = true;
        this.selectRecordName = event.currentTarget.dataset.name;
        this.selectRecordId = currentRecId;
        
        
        var objectName = this.objectName;
        var selectedEvent = new CustomEvent('selected', { detail: {selectName, currentRecId, objectName}});
        
        
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
        
      
    }
    
    resetData(event) {
        this.selectRecordName = "";
        this.selectRecordId = "";
        this.recordSelected = false;

        var objectName = this.objectName;
        var selectedEvent = new CustomEvent('selected', { detail: {undefined, undefined, objectName}});
        
        this.closeResultSection(event);
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    closeResultSection(event) {

        this.template.querySelector('[data-id="lookupContainer"]').classList.add('slds-is-close');
        this.template.querySelector('[data-id="lookupContainer"]').classList.remove('slds-is-open');
       
    }
}