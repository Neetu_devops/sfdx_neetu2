import { LightningElement, api, wire, track } from "lwc";

const exchangeColumns = [
    { label: 'Parts', fieldName: 'partName', type: 'text'},
    { label: 'Cycles Remaining', fieldName: 'cyclesRemaining', type: 'text', editable: true },
];


export default class CommunityModulePartTemplate extends LightningElement {

    @api portalTemplatePartRecord;

    exchangeColumns = exchangeColumns;

    myValues;

    //Method for handling datatable inline edits and overwriting data in the parent component
    handleChange(event) {
        var draft = this.template.querySelector("lightning-datatable").draftValues[0];
            if (draft.cyclesRemaining !=  undefined && draft.cyclesRemaining !=  null) {
            var cyclesRem = {};

                cyclesRem.partId = draft.partId;
                cyclesRem.cyclesRemaining = draft.cyclesRemaining;

            const updateValuesEvent = new CustomEvent('change', { detail: JSON.stringify(cyclesRem) });
            this.dispatchEvent(updateValuesEvent);
            this.template.querySelector("lightning-datatable").draftValues = [];
            }
    }
}