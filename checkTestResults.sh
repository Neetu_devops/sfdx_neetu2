#!/bin/bash
#Script to get the list of commits since last build
set -x

export workspace=$BITBUCKET_REPO_OWNER
export repository=$BITBUCKET_REPO_SLUG
export buildNumber=$BITBUCKET_BUILD_NUMBER
export commitHash=$BITBUCKET_COMMIT
export gitBranch=$BITBUCKET_BRANCH
export gitOrigin=$BITBUCKET_GIT_HTTP_ORIGIN
export buildUrl="https://bitbucket.org/$workspace/$repository/addon/pipelines/home#!/results/$buildNumber"

# Get Git commit info
echo $buildNumber
if [ $buildNumber == 1 ]
then
echo "Getting commits for $commitHash"
git log --pretty=oneline $commitHash > git-commits.log
else
previousbuildNumber=$(expr $buildNumber - 1)
baseURL="https://api.bitbucket.org/2.0/repositories/$workspace/$repository/pipelines"


prevCommitHashURL="$baseURL/$previousbuildNumber"
#prevFullHash=$(curl -u $BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD -s -X GET "$prevCommitHashURL" | jq '.target.commit.hash')
prevFullHash=$(curl -s -X GET "$prevCommitHashURL" | jq '.target.commit.hash')
prevHash="${prevFullHash%\"}"
prevHash="${prevFullHash#\"}"
commitHash="${commitHash%\"}"
commitHash="${commitHash#\"}"

prevHash=$(echo $prevHash | cut -b 1-7)
commitHash=$(echo $commitHash | cut -b 1-7)

echo "Comparing between $prevHash and $commitHash"
git log --pretty=oneline "$prevHash"..$commitHash > git-commits.log
#git log --pretty=oneline "$prevHash..$commitHash" > git-commits.log
fi